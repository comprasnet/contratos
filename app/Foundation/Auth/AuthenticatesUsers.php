<?php

namespace App\Foundation\Auth;

use App\Http\Traits\Users;
use App\Http\Traits\UsuarioAcessoGovBrTrait;
use App\Rules\ValidarLogin;
use GuzzleHttp\Client;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;

trait AuthenticatesUsers
{
    use RedirectsUsers, ThrottlesLogins, Users, UsuarioAcessoGovBrTrait;

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('auth.login');
    }


    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        //Ocultar erro por nível conta GovBR
        session()->put('error_nivel', false);

        $this->validateLogin($request);
        $habilitarRecaptcha = env('HABILITAR_RECAPTCHA');
        //PRODUÇÃO
        if ($habilitarRecaptcha) {
        // TESTES: força a verificação do recaptcha
        // if (true) {
            $recaptchaResult = $this->verificarRecaptcha($request);

            // Verificar se o Recaptcha foi bem-sucedido
            if (!$recaptchaResult['success']) {
                // Recaptcha falhou
                return $this->sendFailedLoginResponse(
                    $request,
                    'Falha na verificação do Recaptcha.',
                    $recaptchaResult['score']
                );
            }

            $recaptchaVersion = $request->input('recaptcha-version');
            if ($recaptchaVersion == '3') {
                // Verificar se a pontuação é suficiente
                if (!$this->verificarScoreRecaptcha($recaptchaResult['score'])) {
                    // Pontuação do Recaptcha insuficiente
                    Log::info(
                        $request->input('cpf') . " - Pontuação do Recaptcha insuficiente: " . $recaptchaResult['score']
                    );
                    return $this->sendFailedLoginResponse(
                        $request,
                        'Pontuação do Recaptcha insuficiente. Tente Novamente.',
                        $recaptchaResult['score']
                    );
                }
            }
        }
        //the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            if ($this->verificarUsuarioTemApenasPerfilFornecedor()) {
                $this->logout($request);
                return $request->wantsJson()
                    ? new Response('', 204)
                    : redirect('/login')->withErrors([
                        'erro_usuario_compras' => 'Usuário com perfil Fornecedor deverá fazer o login por meio do
                         Sistema do Compras, não estando disponível seu login direto pelo Sistema de Contratos.'
                    ]);
            }

            $usuarioPerfilAdministrador = $this->usuarioPertenceGrupoAdministradorDesenvolvedor();

            if (!$usuarioPerfilAdministrador) {
                auth()->logout();

                $urlGovBr = route('acessogov.autorizacao');
                $rota = $this->retornarNomeRotaLoginUsuario(true);

                return redirect($rota)->withErrors(
                    [
                        'erro_usuario_compras' =>
                            "Entre com seu <a href='{$urlGovBr}'>gov.br</a>"
                    ]
                );
            }

            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }
    private function verificarRecaptcha(Request $request)
    {
        $recaptchaSecret = env('RECAPTCHA_SECRET_KEY');

        $recaptchaVersion = $request->input('recaptcha-version');
        if ($recaptchaVersion != '3')
            $recaptchaSecret = env('RECAPTCHA_SECRET_KEY_V2');

        $recaptchaResponse = $request->input('g-recaptcha-response');

        $client = new Client();
        $response = $client->post(
            'https://www.google.com/recaptcha/api/siteverify',
            [
                'form_params' => [
                    'secret' => $recaptchaSecret,
                    'response' => $recaptchaResponse,
                ],
            ]
        );

        $responseData = json_decode($response->getBody()->getContents());

        $score = $responseData->score ?? null;

        return [
            'success' => $responseData->success,
            'score' => $score,
        ];
    }
    private function verificarScoreRecaptcha($score)
    {
        //PRODUÇÃO
        return $score > env('SCORE_LIMIT_RECAPTCHA', 0.5);
        // TESTES: força bloqueio recaptcha v3
        //return $score > 1;
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => [
                'required',
                new ValidarLogin()
            ],
            'password' => 'required|string',
        ]);
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only($this->username(), 'password');
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user())
                ?: redirect()->intended($this->redirectPath());
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        //
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function sendFailedLoginResponse(Request $request, $message = '', $score = 0)
    {
        throw ValidationException::withMessages([
            $this->username() => [$message ?: trans('auth.failed')],
            'score' => $score,
        ]);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'email';
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect('/');
    }

    /**
     * The user has logged out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    protected function loggedOut(Request $request)
    {
        //
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }
}
