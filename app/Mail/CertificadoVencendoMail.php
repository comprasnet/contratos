<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CertificadoVencendoMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $diasRestantesCertificadoSiafi;

    public function __construct($diasRestantesCertificadoSiafi)
    {
        $this->diasRestantesCertificadoSiafi = $diasRestantesCertificadoSiafi;
    }

    public function build()
    {
        $ambiente = config('app.app_amb');

        return $this->from('contratos@no-reply.com', 'Equipe Contratos')
            ->subject("Faltam {$this->diasRestantesCertificadoSiafi} dias para o certificado SIAFI de $ambiente expirar")
            ->markdown('emails.certificado-vencendo', [
                'diasRestantesCertificadoSiafi' => $this->diasRestantesCertificadoSiafi,
            ]);
    }
}
