<?php

namespace App\Jobs;

use App\services\Indices\Implementations\ImportacaoDadosBacenWebserviceToLocalTableService;
use App\services\IPEA\Implementations\IpeaRest;
use App\services\Indices\Implementations\ImportacaoIST;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class RotinaatualizaindicesbacenJobs implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //IMPORTAÇÃO DAS SÉRIES DO BANCO CENTRAL
        $importacao = new ImportacaoDadosBacenWebserviceToLocalTableService();
        $importacao->handle();

        //IMPORTAÇÃO DAS SÉRIES IPEA
        $importacao = new IpeaRest();
        $importacao->handle();

        //IMPORTAÇÃO DAS SÉRIES DA ANATEL
        $importacao = new ImportacaoIST();
        $importacao->handle();
    }
}
