<?php

namespace App\Jobs;

use App\Models\MinutaEmpenho;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Exception;
use Log;

class ProcessarLoteEmpenhoPNCPJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $registros;

    /**
     * @param mixed $registros
     */
    public function __construct($registros)
    {
        $this->registros = $registros;
    }

    /**
     * Processa cada lote de registros de empenho PNCP.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->registros as $empenho) {
            try {
                // Localiza a MinutaEmpenho com base no ID do registro
                $minuta = MinutaEmpenho::select('minutaempenhos.*')
                    ->join('codigoitens', 'codigoitens.id', '=', 'minutaempenhos.situacao_id')
                    ->where('minutaempenhos.id', $empenho->pncpable_id)
                    ->where('codigoitens.descricao', 'EMPENHO EMITIDO')
                    ->first();

                // Processa cada registro individualmente
                if ($minuta && !file_exists($minuta->serializaArquivoPNCP())) {
                    $this->pdfEmpenho($minuta->id);
                }
            } catch (Exception $e) {
                // Registra um log de erro caso ocorra uma exceção
                Log::error($e->getMessage());
            } finally {
                // Comentário opcional ou operações adicionais após o processamento de cada registro
            }
        }
    }
}
