<?php

namespace App\Jobs;

use App\Http\Controllers\Admin\ImportacaoCrudController;
use App\Http\Traits\Formatador;
use App\Http\Traits\Users;
use App\Models\BackpackUser;
use App\Models\Importacao;
use App\Models\Unidade;
use App\Notifications\PasswordUserNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Spatie\Permission\Models\Role;

class InserirAutoridadeSignatariaEmMassaJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $array_dado;
    protected $unidade_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $array_dado, int $unidade_id)
    {
        $this->array_dado = $array_dado;
        $this->unidade_id = $unidade_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $executa = new ImportacaoCrudController();
        $executa->executaInsercaoMassaAutoridadeSignataria($this->array_dado, $this->unidade_id);
    }
}
