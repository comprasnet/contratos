<?php

namespace App\Jobs;

use App\Http\Controllers\SICAF;
use App\Models\Fornecedor;
use App\services\Fornecedor\FornecedorSicafService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AtualizaFornecedorSicafJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $fornecedor;
    private $contrato_id;

    public function __construct(Fornecedor $fornecedor, $contrato_id = null)
    {
        $this->fornecedor = $fornecedor;
        $this->contrato_id = $contrato_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $fornecedorSicafService = new FornecedorSicafService(new SICAF());

        DB::beginTransaction();
        if (is_null($this->contrato_id)) {
            try {
                $fornecedorSicafService->consultaSicaf($this->fornecedor);
                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                Log::info($e->getMessage());
            }
        } else {
            try {
                $fornecedorSicafService->retornaConsultaSicafeRFB($this->fornecedor->id, $this->contrato_id);
                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                Log::info('Fornecedor ' . $this->fornecedor->id . ' Contrato ' . $this->contrato_id . ' não retornou dados.');
                Log::info($e->getMessage());
            }
        }
    }
}
