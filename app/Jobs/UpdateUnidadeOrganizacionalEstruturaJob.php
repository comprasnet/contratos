<?php

namespace App\Jobs;

use App\Http\Traits\Formatador;
use App\Models\ContatoSiorg;
use App\Models\EnderecoSiorg;
use App\Models\JobUser;
use App\services\SIORG\Implementations\SiorgRest;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UpdateUnidadeOrganizacionalEstruturaJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Formatador;

    protected $estrutura;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $estrutura)
    {
        $this->estrutura = $estrutura;
    }

    private function alterarStatus(int $idStatus, bool $status)
    {
        # Recupera o registro do JOB na tabela intermediária
        $jobUser = JobUser::where("job_id", $idStatus)->first();

        # Se não for encontrado o job, sai do método
        if (empty($jobUser)) {
            return ;
        }

        # Altera o status para processando
        $jobUser->processando = $status;
        $jobUser->save();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $bool = null;
        $this->alterarStatus($this->job->getJobId(), true);
        DB::beginTransaction();
        try {
            $bool = true;
            foreach ($this->estrutura['unidades'] as $unidade) {
                preg_match('/\d+/', $unidade['codigoUnidade'], $codigoUnidade);
                preg_match('/\d+/', $unidade['codigoUnidadePai'], $codigoUnidadePai);
                preg_match('/\d+/', $unidade['codigoOrgaoEntidade'], $codigoOrgaoEntidade);

                $unidadeIndividual = SiorgRest::getUnidadeOrganizacionalEstruturaIndividualCompleta($codigoUnidade[0]);

                $enderecoContato = SiorgRest::getEnderecoContato($codigoUnidade[0]);

                $categoria = $this->getLastPathComponent($unidadeIndividual['unidade']['codigoCategoriaUnidade']);
                $esfera = $this->getLastPathComponent($unidadeIndividual['unidade']['codigoEsfera']);
                $poder = $this->getLastPathComponent($unidadeIndividual['unidade']['codigoPoder']);
                $natureza_juridica = $this->getLastPathComponent($unidadeIndividual['unidade']['codigoNaturezaJuridica']);
                $sub_natureza_juridica = $this->getLastPathComponent($unidadeIndividual['unidade']['codigoSubNaturezaJuridica']);

                \App\Models\Siorg::updateOrCreate([
                    'codigo' => $codigoUnidade[0],
                    'codigo_pai' => $codigoUnidadePai[0],
                    'codigo_principal' => $codigoOrgaoEntidade[0]
                ],
                    [
                        'sigla' => $unidade['sigla'],
                        'denominacao' => $unidade['nome'],
                        'categoria' => $categoria,
                        'nivel_normatizacao' => $unidade['nivelNormatizacao'],
                        'esfera' => $esfera,
                        'poder' => $poder,
                        'natureza_juridica' => $natureza_juridica,
                        'subnatureza_juridica' => $sub_natureza_juridica,
                        'cnpj' => $unidadeIndividual['unidade']['cnpj'],
                        'competencia' => $unidadeIndividual['unidade']['competencia'],
                        'finalidade' => $unidadeIndividual['unidade']['finalidade'],
                        'missao' => $unidadeIndividual['unidade']['missao'],
                        'created_at' => now()
                    ]);

                $this->updateOrInsertContato($enderecoContato, $codigoUnidade[0]);
            }

            DB::commit();
        } catch (\Exception $e) {
            $bool = false;
            DB::rollback();
            Log::info($e->getMessage());
        }

        return $bool;
    }

    private function updateOrInsertContato($enderecoContato, $codigoUnidade)
    {

        EnderecoSiorg::updateOrCreate([
            'codigo_siorg' => $codigoUnidade
        ], [
            'cep' => $enderecoContato['endereco'][0]['cep'],
            'logradouro' => $enderecoContato['endereco'][0]['logradouro'],
            'numero' => $enderecoContato['endereco'][0]['numero'],
            'complemento' => $enderecoContato['endereco'][0]['complemento'],
            'bairro' => $enderecoContato['endereco'][0]['bairro'],
            'municipio' => $enderecoContato['endereco'][0]['municipio'],
            'uf' => $enderecoContato['endereco'][0]['uf'],
            'pais' => $enderecoContato['endereco'][0]['pais'],
            'horario_funcionamento' => $enderecoContato['endereco'][0]['horarioDeFuncionamento'],
            'tipo_endereco' => $enderecoContato['endereco'][0]['tipoEndereco']
        ]);

        $site = !empty($enderecoContato['contato'][0]['site']) ? $enderecoContato['contato'][0]['site'][0] : null;
        $email = !empty($enderecoContato['contato'][0]['email']) ? $enderecoContato['contato'][0]['email'][0] : null;
        $telefone = !empty($enderecoContato['contato'][0]['telefone']) ? $enderecoContato['contato'][0]['telefone'][0] : null;

        ContatoSiorg::updateOrCreate([
            'codigo_siorg' => $codigoUnidade
        ], [
            'telefone' => $telefone,
            'email' => $email,
            'site' => $site['site']
        ]);
    }
}
