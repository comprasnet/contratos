<?php

namespace App\Jobs;

use App\Http\Controllers\Apropriacao\ApropriacaoFaturaSiafiController;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Http\Request;
use App\Models\BackpackUser;
use Exception;
use Illuminate\Support\Facades\Log;

class EnviarApropriacaoParaSiafiJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $sfpadrao_id;
    protected $user_id;

    public function __construct($sfpadrao_id, $user_id)
    {
        $this->sfpadrao_id = $sfpadrao_id;
        $this->user_id = $user_id;
    }

    public function handle()
    {
        try {
            $user = BackpackUser::find($this->user_id);
            $request = new Request();
            $request->merge(['sfpadrao_id' => $this->sfpadrao_id]);

            $apropriacaoFaturaSiafiController = new ApropriacaoFaturaSiafiController();
            $apropriacaoFaturaSiafiController->enviarApropriacaoFaturaSiafi($request, $user);
        } catch (Exception $e) {
            $errorMessage = 'Erro ao processar o job EnviarApropriacaoParaSiafiJob: ' . 'sfpadrao_id: ' . $this->sfpadrao_id . $e->getMessage() . ' | Arquivo: ' . $e->getFile() . ' | Linha: ' . $e->getLine();
            Log::channel('api-apropriacao')->error($errorMessage);
        }
    }
}
