<?php

namespace App\Jobs;

use App\Http\Controllers\Apropriacao\ApropriacaoFaturaSiafiController;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use App\Models\BackpackUser;
use App\Repositories\Nonce\NonceRepository;
use Exception;
use Illuminate\Support\Facades\Log;

class CancelarApropriacaoJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $sfpadrao_id;
    protected $user_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $sfpadrao_id, $user_id)
    {
        $this->sfpadrao_id = $sfpadrao_id;
        $this->user_id = $user_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $user = BackpackUser::find($this->user_id);

            $request = new Request();
            $request->merge(['sfpadrao_id' => $this->sfpadrao_id]);

            $apropriacaoSiafi = new ApropriacaoFaturaSiafiController();
            $apropriacaoSiafi = $apropriacaoSiafi->cancelarApropriacaoFaturaSiafi($request, null, $user);
        } catch (Exception $e) {
            Log::error($e->getMessage() . "na linha " . $e->getLine() . "no arquivo " . $e->getFile());
        }
    }
}
