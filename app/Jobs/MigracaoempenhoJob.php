<?php

namespace App\Jobs;

use App\Http\Traits\Busca;
use App\Http\Traits\Formatador;
use App\Models\Empenho;
use App\Models\Empenhodetalhado;
use App\Models\Fornecedor;
use App\Models\Naturezadespesa;
use App\Models\Naturezasubitem;
use App\Models\Planointerno;
use App\Models\Unidade;
use App\services\STA\STAUrlFetcherService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class MigracaoempenhoJob implements ShouldQueue
{
    use Busca, Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Formatador;

    public $timeout = 7200;

    protected $ug_codigo;
    protected $ano;
    protected $dia;
    const LOG_CHANNEL = 'migracaoempenho';


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $ug_codigo, $ano, $dia = null)
    {
        $this->ug_codigo = $ug_codigo;
        $this->ano = $ano;
        $this->dia = $dia;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $idDoProcesso = $id = Str::uuid();
        $prefix = __CLASS__ . ' | ' . $idDoProcesso . ' | ';

        $unidade = Unidade::where('codigosiafi', $this->ug_codigo)->first();
        $migracao_url = config('migracao.api_sta');

        $param = [
            'ano' => $this->ano,
            'ug' => $unidade->codigosiafi,
            'dia' => $this->dia
        ];

        $url = $migracao_url . $this->replaceRouteParam(config('rotas-sta.empenho-por-ano-ug-dia'), $param);

        Log::channel(self::LOG_CHANNEL)
            ->info($prefix.'Requisição à URL para trazer os empenhos da unidade ' . $this->ug_codigo . ': ' . $url);

        $urlFetchService = new STAUrlFetcherService($url);
        $dados = $urlFetchService->fetchDataJson();

        if ($urlFetchService->isError($dados)) {

            $error = $urlFetchService->getError($dados);

            Log::channel(self::LOG_CHANNEL)->error($prefix.' Erro '.$error->error_code.' ao executar a url: '
                . $error->error_message);

            return;
        }

        if (!$dados) {
            $dados = [];
        }

        Log::channel(self::LOG_CHANNEL)->info($prefix.'Quantidade de empenhos retornados ' .
            'para a unidade ' . $this->ug_codigo . ': '. count($dados));

        if (count($dados) > 0) {
            foreach ($dados as $d) {

                $credor = $this->buscaFornecedor($d);

                if ($d['picodigo'] != "") {
                    $pi = $this->buscaPi($d);
                }

                $pi_id = null;
                if (isset($pi->id)) {
                    $pi_id = $pi->id;
                }

                $naturezadespesa = Naturezadespesa::where('codigo', $d['naturezadespesa'])
                    ->where('sistema_origem', $d['nd_sistema_origem'])
                    ->first();

                if (!$naturezadespesa || empty($naturezadespesa)) {
                    continue;
                }

                $empenho = Empenho::where('numero', '=', trim($d['numero']))
                    ->where('unidade_id', '=', $unidade->id)
                    ->withTrashed()
                    ->first();

                if (!isset($empenho->id)) {
                    $empenho = Empenho::create([
                        'numero' => trim($d['numero']),
                        'unidade_id' => $unidade->id,
                        'fornecedor_id' => $credor->id,
                        'planointerno_id' => $pi_id,
                        'naturezadespesa_id' => $naturezadespesa->id,
                        'fonte' => trim($d['fonte']),
                        'informacao_complementar' => trim($d['informacaocomplementar']),
                        'sistema_origem' => trim($d['sistemaorigem']),
                        'id_sistema_origem' => trim($d['id_sistema_origem']),
                        'data_emissao' => trim($d['emissao']),
                        'ano_emissao' => (int)trim($d['ano_emissao']),
                        'modalidade_licitacao_siafi' => trim($d['modalidade_licitacao_siafi']),
                        'ptres' => trim($d['ptres']),
                    ]);
                } else {
                    $empenho->fornecedor_id = $credor->id;
                    $empenho->planointerno_id = $pi_id;
                    $empenho->naturezadespesa_id = $naturezadespesa->id;
                    $empenho->fonte = trim($d['fonte']);
                    $empenho->informacao_complementar = trim($d['informacaocomplementar']);
                    $empenho->sistema_origem = trim($d['sistemaorigem']);
                    $empenho->id_sistema_origem = trim($d['id_sistema_origem']);
                    $empenho->data_emissao = trim($d['emissao']);
                    $empenho->ano_emissao = (int)trim($d['ano_emissao']);
                    $empenho->modalidade_licitacao_siafi = trim($d['modalidade_licitacao_siafi']);
                    $empenho->ptres = trim($d['ptres']);
                    $empenho->deleted_at = null;
                    $empenho->save();
                }

                foreach ($d['itens'] as $item) {

                    $naturezasubitem = Naturezasubitem::where('codigo', $item['subitem'])
                        ->where('naturezadespesa_id', $naturezadespesa->id)
                        ->first();

                    if (isset($naturezasubitem->id)) {

                        try {
                            $empenhodetalhado = Empenhodetalhado::withTrashed()
                                ->where('empenho_id', $empenho->id)
                                ->where('naturezasubitem_id', $naturezasubitem->id)
                                ->first();

                            # Se o registro existir
                            if ($empenhodetalhado) {
                                # Se o registro estiver soft deleted, restaura-o
                                if ($empenhodetalhado->trashed()) {
                                    $empenhodetalhado->restore();
                                } else {
                                    # Se o registro não estiver soft deleted, atualiza-o se necessário
                                    # Até o momento, não há outros campos importados em um empenhodetalhado
                                    # que não seja empenho_id e naturezasubitem_id
                                    $empenhodetalhado->save();
                                }
                            } else {
                                # Se o registro não existir, cria um novo
                                Empenhodetalhado::create([
                                    'empenho_id' => $empenho->id,
                                    'naturezasubitem_id' => $naturezasubitem->id
                                ]);
                            }
                        } catch (\Exception $e) {
                            // Tratar exceção, se necessário
                        }
                    }

                }
            }
        }
    }


    public function buscaFornecedor($credor)
    {
        $tipoFornecedor = [18 => 'JURIDICA', 14 => 'FISICA', 6 => 'UG'];
        $tipo = $tipoFornecedor[strlen($credor['cpfcnpjugidgener'])] ?? 'IDGENERICO';
        try {
            $fornecedor = Fornecedor::firstOrCreate(
                [
                    'cpf_cnpj_idgener' => $credor['cpfcnpjugidgener']
                ],
                [
                    'tipo_fornecedor' => $tipo,
                    'nome' => strtoupper($credor['nome'])
                ]
            );
        } catch (\Exception $e) {
            $fornecedor = Fornecedor::where('cpf_cnpj_idgener', $credor['cpfcnpjugidgener'])->first();
        }

        return $fornecedor;
    }

    public function buscaPi($pi)
    {
        # Verifica se já existe um registro com o código e o sistema de origem fornecidos
        $planointerno = Planointerno::where('codigo', $pi['picodigo'])
            ->where('sistema_origem', $pi['pi_sistema_origem'])
            ->first();

        # Se existir, atualiza a descrição e a situação
        if ($planointerno) {
            $planointerno->update([
                'descricao' => strtoupper($pi['pidescricao']),
                'situacao' => true,
            ]);
            return $planointerno;
        }

        # Se não existir, cria um novo registro
        return Planointerno::create([
            'codigo' => $pi['picodigo'],
            'descricao' => strtoupper($pi['pidescricao']),
            'situacao' => true,
            'sistema_origem' => $pi['pi_sistema_origem'] ?? 'SIAFI',
        ]);
    }

}
