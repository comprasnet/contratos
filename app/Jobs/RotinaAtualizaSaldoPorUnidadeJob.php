<?php

namespace App\Jobs;

use App\Http\Controllers\AdminController;
use App\Http\Controllers\Execfin\EmpenhoCrudController;
use App\Models\Unidade;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class RotinaAtualizaSaldoPorUnidadeJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 2400;

    protected $unidade;
    protected $ano;
    protected $offset;
    protected $limit;
    protected $rp;
    protected $rotinaAutomatica;

    protected const LOG_CHANNEL = 'atualizasaldone';

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Unidade $unidade, $ano, $offset = null, $limit = null, $rp = false, $rotinaAutomatica = false)
    {
        $this->unidade = $unidade;
        $this->ano = $ano;
        $this->offset = $offset;
        $this->limit = $limit;
        $this->rp = $rp;
        $this->rotinaAutomatica = $rotinaAutomatica;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $idDoProcesso = $id = Str::uuid();
        $prefix = __CLASS__ . ' | ' . $idDoProcesso . ' | ';
        Log::channel(self::LOG_CHANNEL)
            ->info(
                $prefix .
                'Início da execução do Job para a unidade ' .
                $this->unidade->codigo . ': ' .
                date('d/m/Y H:i:s'));

        $rotinaEmpenho = new EmpenhoCrudController();

        if($this->rotinaAutomatica){
            $rotinaEmpenho->executaAtualizaSaldosEmpenhosPorUnidade(
                $this->unidade,
                true,
                null,
                $this->offset,
                $this->limit,
                true
            );
            return true;
        }else{
            if ($this->rp) {
                $rotinaEmpenho->executaAtualizaSaldosEmpenhosPorUnidade(
                    $this->unidade,
                    true,
                    null,
                    $this->offset,
                    $this->limit
                );
                return true;
            }

            $rotinaEmpenho->executaAtualizaSaldosEmpenhosPorUnidade(
                $this->unidade,
                false,
                $this->ano,
                $this->offset,
                $this->limit
            );
            return true;
        }
    }
}
