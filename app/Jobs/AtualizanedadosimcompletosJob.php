<?php

namespace App\Jobs;

use App\Http\Traits\Busca;
use App\Models\Empenho;
use App\Models\Unidade;
use App\services\STA\STAUrlFetcherService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class AtualizanedadosimcompletosJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Busca;

    public $timeout = 1200;

    protected $unidade;
    protected $url;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Unidade $unidade)
    {
        $this->unidade = $unidade;
        $this->url = config('migracao.api_sta') . config('rotas-sta.empenho-dados-basicos');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $empenhos = Empenho::whereNull('modalidade_licitacao_siafi')
            ->where(DB::raw('left(numero,4)'), '>=', '2021')
            ->where('unidade_id', $this->unidade->id)
            ->get();

        foreach ($empenhos as $empenho) {
            $url = $this->url . $empenho->unidade->codigo . $empenho->unidade->gestao . $empenho->numero;


            $urlFetchService = new STAUrlFetcherService($url);
            $dados = $urlFetchService->fetchDataJson();

            if ($urlFetchService->isError($dados)) {
                $error = $urlFetchService->getError($dados);
                logger()->info(__METHOD__ . ' - Erro ao fazer busca no STA '.
                    '[' . $error->error_code . '] ' . $error->error_message);
            }

            if (count($dados)) {
                $empenho->informacao_complementar = $dados['informacaocomplementar'];
                $empenho->sistema_origem = $dados['sistemaorigem'];
                $empenho->data_emissao = $dados['emissao'];
                if($empenho->rp){
                    $empenho->ano_emissao = (int) date('Y');
                }else{
                    $empenho->ano_emissao = (int) $dados['ano_emissao'];
                }
                $empenho->modalidade_licitacao_siafi = $dados['modalidade_licitacao_siafi'];
                $empenho->ptres = $dados['ptres'];
                $empenho->save();
            }
        }
    }
}
