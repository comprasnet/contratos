<?php

namespace App\Jobs;

use Alert;
use App\Http\Controllers\AdminController;
use App\Models\Naturezadespesa;
use App\Models\Naturezasubitem;
use App\services\STA\STAUrlFetcherService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class AtualizaNaturezaDespesasJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    const LOG_CHANNEL = 'atualizanaturezadespesa';

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $idDoProcesso = $id = Str::uuid();
        $prefix = __CLASS__ . ' | ' . $idDoProcesso . ' | ';

        Log::channel(self::LOG_CHANNEL)->info($prefix . 'Início da execução do Job: ' . date('d/m/Y H:i:s'));

        $migracao_url = config('migracao.api_sta');
        $url = $migracao_url . config('rotas-sta.estrutura-natureza-despesa');
        Log::channel(self::LOG_CHANNEL)->info($prefix . 'Requisição à URL: ' . $url);

        $urlFetchService = new STAUrlFetcherService($url);
        $dados = $urlFetchService->fetchDataJson();

        if ($urlFetchService->isError($dados)) {

            $error = $urlFetchService->getError($dados);

            Log::channel(self::LOG_CHANNEL)
                ->error($prefix . 'Erro ' . $error->error_code . ' ao executar a url: ' . $error->error_message);

            return;
        }

        if (!$dados) {
            $dados = [];
        }

        Log::channel(self::LOG_CHANNEL)
            ->info($prefix . 'Quantidade de registros retornados: ' . count($dados));
        if (count($dados) > 0) {
            foreach ($dados as $dado) {
                $nd = new Naturezadespesa();
                $busca_nd = $nd->buscaNaturezaDespesa($dado);

                $subitem = new Naturezasubitem();
                $busca_si = $subitem->buscaNaturezaSubitem($dado, $busca_nd);

            }
        }
        Log::channel(self::LOG_CHANNEL)
            ->info($prefix . 'Fim da execução do Job: ' . date('d/m/Y H:i:s'));
    }
}
