<?php

namespace App\DTO;

/**
 *
 */
class Termo14133PNCDTO
{
    /**
     * @var
     */
    public $contratohistoricoId;

    /**
     * @var
     */
    public $contratoHistoricoTipoId;

    /**
     * @var
     */
    public $contratoId;

    /**
     * @param $contratohistoricoId
     * @param $contratoHistoricoTipoId
     * @param $contratoId
     */
    public function __construct($contratohistoricoId, $contratoHistoricoTipoId, $contratoId)
    {
        $this->contratohistoricoId = $contratohistoricoId;
        $this->contratoHistoricoTipoId = $contratoHistoricoTipoId;
        $this->contratoId = $contratoId;
    }
}
