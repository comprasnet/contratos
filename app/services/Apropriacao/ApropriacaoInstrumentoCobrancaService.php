<?php

namespace App\services\Apropriacao;

use App\Http\Traits\BuscaCodigoItens;
use App\Models\ApropriacaoFaturas;
use App\Models\Codigoitem;
use App\Models\CompraItemMinutaEmpenho;
use App\Models\Contrato;
use App\Models\Contratofatura;
use App\Models\ContratoItemMinutaEmpenho;
use App\Models\SaldoContabil;
use App\Models\Unidade;
use Illuminate\Support\Facades\DB;

class ApropriacaoInstrumentoCobrancaService
{
    use BuscaCodigoItens;

    private $apropriacaoFaturas;
    private $contratofatura;

    public function __construct(ApropriacaoFaturas $apropriacaoFaturas, Contratofatura $contratofatura)
    {
        $this->apropriacaoFaturas = $apropriacaoFaturas;
        $this->contratofatura = $contratofatura;
    }

    public function consultaApropriacaoContrato(int $contratoId, $naoAdministrador, $unidadesDescentralizadas)
    {
        if(!empty($naoAdministrador)){
            $contrato = $this->validaPermissaoContrato($contratoId, $naoAdministrador);
            if(count($contrato) == 0){
                return response()->json(['status' => 'error', 'message' => 'Usuário sem permissão no contrato.']);
            }
        }

        $faturaContratoApropriacao = $this->contratofatura
            ->select(
                'id',
                'contrato_id'
            )
            ->where('contrato_id', $contratoId)
            ->when(!empty($naoAdministrador), function ($query) use ($naoAdministrador, $unidadesDescentralizadas) {
                $query->whereHas('contrato', function ($q) use ($naoAdministrador, $unidadesDescentralizadas) {
                    $q->whereIn('unidadeorigem_id', $naoAdministrador);
                    if (!empty($unidadesDescentralizadas)) {
                        $q->orWhereExists(function ($query) use ($unidadesDescentralizadas) {
                            $query->select(DB::raw(1))
                                ->from('contratounidadesdescentralizadas')
                                ->whereColumn('contratounidadesdescentralizadas.contrato_id', 'contratos.id')
                                ->whereIn('contratounidadesdescentralizadas.unidade_id', $unidadesDescentralizadas);
                        });
                    }
                });
            })
            ->whereHas('apropriacaoContratoFaturasApropriacao')
            ->with([
                'contrato:id,numero',
                'apropriacaoContratoFaturasApropriacao.apropriacao:id,valor,fase_id',
                'apropriacaoContratoFaturasApropriacao.sfpadrao:id,codugemit,numdh,codtipodh,user_id,msgretorno',
                'apropriacaoContratoFaturasApropriacao.sfpadrao.usuario:id,name,cpf,id_sistema_origem',
                'apropriacaoContratoFaturasApropriacao.sfpadrao.usuario.sistemaOrigem:id,id_sistema_origem',
                'apropriacaoContratoFaturasApropriacao.sfpadrao.dadosBasicos:id,sfpadrao_id,dtemis,vlr',
                'apropriacaoContratoFaturasApropriacao.sfpadrao.pco:id,sfpadrao_id,codsit',
                'apropriacaoContratoFaturasApropriacao.sfpadrao.pco.pcoItens:id,sfpco_id,numempe,vlr,codsubitemempe',
                'apropriacaoContratoFaturasApropriacao.sfpadrao.pco.pcoItens.empenho:id,numero,unidade_id',
            ])
            ->get()

            ->values();
            
        return $this->formatarFaturaContratoApropriacao($faturaContratoApropriacao);

    }

    public function formatarDados($faturaApropriacao)
    {

        $apropriacoesFormatadas = [];

        foreach ($faturaApropriacao['apropriacaoContratoFaturasApropriacao'] as $key => $apropriacaoContratoFaturasApropropriacao) {
            $dadosEmpenho = $this->formatarDadosEmpenho($apropriacaoContratoFaturasApropropriacao['sfpadrao']['pco'], $apropriacaoContratoFaturasApropropriacao['sfpadrao']['codugemit']);
            
            $apropriacoes = $this->obterApropriacoes($faturaApropriacao, $apropriacaoContratoFaturasApropropriacao);
        
            $apropriacao = [
                'id_apropriacao_inst_cobranca' => $apropriacaoContratoFaturasApropropriacao['apropriacao']['id'],
                'inst_cobranca' => $apropriacoes,
                'situacao_apropriacao' => $this->retornaDescCodigoItem(trim($apropriacaoContratoFaturasApropropriacao['apropriacao']['fase_id'])),
                'mensagem_siafi' => $apropriacaoContratoFaturasApropropriacao['sfpadrao']['msgretorno'],
                'emissao' => $apropriacaoContratoFaturasApropropriacao['sfpadrao']['dadosBasicos']['dtemis'],
                'sistema_origem' => $apropriacaoContratoFaturasApropropriacao['sfpadrao']['usuario']['sistemaOrigem']['id_sistema_origem'],
                'tipo_dh' => $this->retornaDescricaoPorDescres(trim($apropriacaoContratoFaturasApropropriacao['sfpadrao']['codtipodh'])) ?? null,
                'numero_dh_siafi' => $apropriacaoContratoFaturasApropropriacao['sfpadrao']['numdh'],
                'ug_emitente' => $apropriacaoContratoFaturasApropropriacao['sfpadrao']['codugemit'],
                'valor_documento' => $apropriacaoContratoFaturasApropropriacao['sfpadrao']['dadosBasicos']['vlr'],
                'dados_empenho' => $dadosEmpenho
            ];

            $apropriacoesFormatadas[$apropriacao['id_apropriacao_inst_cobranca']] = $apropriacao;
        }

        return $apropriacoesFormatadas;
    }

    public function formatarDadosEmpenho($pcoItens, $codugemit)
    {
        $unidade = Unidade::where('codigo', $codugemit)->select('id')->first();

        return collect($pcoItens)
            ->flatMap(function ($pco) {
                return collect(data_get($pco, 'pcoItens', []))->flatMap(function ($pcoItem) {
                    $empenhos = data_get($pcoItem, 'empenho', []);
                    $vlr = data_get($pcoItem, 'vlr');
                    $sub_elemento_item = data_get($pcoItem, 'codsubitemempe');

                    return collect($empenhos)->map(function ($empenho) use ($vlr, $sub_elemento_item) {
                        return [
                            'id' => $empenho['id'],
                            'numero' => $empenho['numero'],
                            'unidade_id' => $empenho['unidade_id'],
                            'sub_elemento_item' => $sub_elemento_item,
                            'valor_empenho' => $vlr,
                        ];
                    });
                });
            })
            ->filter(function ($empenho) use ($unidade) {
                return $empenho['unidade_id'] == $unidade->id;
            })
            ->map(function ($empenho) {
                unset($empenho['unidade_id']);
                return $empenho;
            })
            ->values();
    }

    public function obterApropriacoes($faturaApriacao, $apropriacaoContratoFaturasApropriacao)
    {
        return Contratofatura::join(
            'apropriacoes_faturas_contratofaturas',
            'contratofaturas.id',
            '=',
            'apropriacoes_faturas_contratofaturas.contratofaturas_id'
        )
            ->where('contrato_id', $faturaApriacao['contrato_id'])
            ->where('apropriacoes_faturas_contratofaturas.sfpadrao_id', $apropriacaoContratoFaturasApropriacao['sfpadrao']['id'])
            ->where('apropriacoes_faturas_contratofaturas.apropriacoes_faturas_id', $apropriacaoContratoFaturasApropriacao['apropriacao']['id'])
            ->select('contratofaturas.id as id_inst_cobranca', 'contratofaturas.valor as valor_apropriacao_inst_cobranca')
            ->get();
    }

    public function formatarFaturaContratoApropriacao($faturaContratoApropriacao)
    {
        $formattedData = [];
        foreach ($faturaContratoApropriacao as $faturaApropriacao) {
            $apropriacoes = $this->formatarDados($faturaApropriacao);

            foreach ($apropriacoes as $id_apropriacao => $apropriacao) {
                $formattedData[$id_apropriacao] = $apropriacao;
            }
        }

        if (empty($formattedData)) {
            return response()->json(['status' => 'error', 'message' => 'Apropriação não encontrada.']);
        }

        return [
            'id_contrato' => $faturaContratoApropriacao[0]->contrato_id,
            'numero_contrato' => $faturaContratoApropriacao[0]->contrato->numero,
            'apropriacao' => array_values($formattedData)
        ];
    }

    public function validaPermissaoContrato($contratoId, $unidadeId)
    {
        return Contrato::leftJoin(
            'contratounidadesdescentralizadas',
            'contratos.id',
            '=',
            'contratounidadesdescentralizadas.contrato_id'
        )
            ->where('contratos.id', $contratoId)
            ->where(function ($query) use ($unidadeId) {
                $query->whereIn('contratounidadesdescentralizadas.unidade_id', $unidadeId)
                    ->orWhereIn('contratos.unidade_id', $unidadeId);
            })
            ->get();
    }

    public function getItens($minuta)
    { 
        $tipo = $minuta->empenho_por;
        
        if ($tipo === "Contrato") {

            return ContratoItemMinutaEmpenho::join('contratoitens', 'contratoitens.id', '=', 'contrato_item_minuta_empenho.contrato_item_id')
            ->join('minutaempenhos', 'minutaempenhos.id', '=', 'contrato_item_minuta_empenho.minutaempenho_id')
            ->join('contratos', 'contratos.id', '=', 'minutaempenhos.contrato_id')
            ->join('codigoitens', 'codigoitens.id', '=', 'contratoitens.tipo_id')
            ->join('catmatseritens', 'catmatseritens.id', '=', 'contratoitens.catmatseritem_id')
            ->join('minutaempenhos_remessa', 'minutaempenhos_remessa.id', '=', 'contrato_item_minuta_empenho.minutaempenhos_remessa_id')
            ->join('naturezasubitem', 'naturezasubitem.id', '=', 'contrato_item_minuta_empenho.subelemento_id')
            ->where('contrato_item_minuta_empenho.minutaempenho_id', $minuta->id)
                ->where('minutaempenhos_remessa.remessa', 0)
                ->distinct()
                ->select([
                    DB::raw('naturezasubitem.codigo AS "subelementoItem"'),
                    DB::raw('codigoitens.descricao AS "tipoItem"'),
                    DB::raw('catmatseritens.codigo_siasg AS "catmat"'),
                    DB::raw('contratoitens.numero_item_compra AS "numeroItem"'),
                    DB::raw('catmatseritens.descricao AS "descricao"'),
                    DB::raw("CASE WHEN contratoitens.descricao_complementar != 'undefined'
                                            THEN contratoitens.descricao_complementar
                                        ELSE ''
                                    END  AS \"descricaoDetalhada\""),
                    'contrato_item_minuta_empenho.numseq'
                ])
                ->orderBy('contrato_item_minuta_empenho.numseq', 'asc')
                ->get()
                ->toArray();
        }

        if ($tipo === "Compra" || $tipo === "Suprimento") {

            return CompraItemMinutaEmpenho::join('compra_items', 'compra_items.id', '=', 'compra_item_minuta_empenho.compra_item_id')
            ->join('compra_item_fornecedor', 'compra_item_fornecedor.compra_item_id', '=', 'compra_items.id')
            ->join('compra_item_unidade', 'compra_item_unidade.compra_item_id', '=', 'compra_items.id')
            ->join('codigoitens', 'codigoitens.id', '=', 'compra_items.tipo_item_id')
            ->join('catmatseritens', 'catmatseritens.id', '=', 'compra_items.catmatseritem_id')
            ->join('minutaempenhos_remessa', 'minutaempenhos_remessa.id', '=', 'compra_item_minuta_empenho.minutaempenhos_remessa_id')
            ->join('compras', 'compras.id', '=', 'compra_items.compra_id')
            ->join('naturezasubitem', 'naturezasubitem.id', '=', 'compra_item_minuta_empenho.subelemento_id')
            ->join('codigoitens as mod', 'mod.id', '=', 'compras.modalidade_id')
            ->where('compra_items.situacao', true)
            ->where('compra_item_fornecedor.situacao', true)
            ->where('compra_item_unidade.situacao', true)
            ->where('compra_item_minuta_empenho.minutaempenho_id', $minuta->id)
                ->where('compra_item_unidade.unidade_id', $minuta->unidade_id)
                ->where('minutaempenhos_remessa.remessa', 0)
                ->distinct()
                ->select([
                    DB::raw('naturezasubitem.codigo AS "subelementoItem"'),
                    DB::raw('codigoitens.descricao AS "tipoItem"'),
                    DB::raw('catmatseritens.codigo_siasg AS "catmat"'),
                    DB::raw('compra_items.numero AS "numeroItem"'),
                    DB::raw('catmatseritens.descricao AS "descricao"'),
                    DB::raw('compra_items.descricaodetalhada AS "descricaoDetalhada"'),
                    'compra_item_minuta_empenho.numseq'
                ])
                ->orderBy('compra_item_minuta_empenho.numseq', 'asc')
                ->get()
                ->toArray();
        }
    }


    public function getNaturezaDespeza($minuta_id)
    {
        return SaldoContabil::join('minutaempenhos', 'minutaempenhos.saldo_contabil_id', '=', 'saldo_contabil.id')
        ->select(
            DB::raw('SUBSTRING(saldo_contabil.conta_corrente,18,6) AS "ND"'),
        )
            ->where('minutaempenhos.id', $minuta_id)
            ->first()->ND;
    }
}
