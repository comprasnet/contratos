<?php

namespace App\services;

use App\Models\CompraItemMinutaEmpenho;
use App\Models\ContratoItemMinutaEmpenho;
use App\Models\ContratoMinutaEmpenhoPivot;
use App\Models\MinutaEmpenho;
use App\Repositories\MinutaRepository;
use App\services\PNCP\PncpService;
use Illuminate\Support\Facades\DB;

class MinutaService
{
    protected $minutaRepository;

    protected $pncpService;

    public function __construct(MinutaRepository $minutaRepository, PncpService $pncpService)
    {
        $this->minutaRepository = $minutaRepository;
        $this->pncpService = $pncpService;
    }

    public function getItens($minutaEmpenho){
        $tipo = $minutaEmpenho->empenho_por;
        $fornecedor_id = $minutaEmpenho->fornecedor_empenho_id;
        $fornecedor_compra_id = $minutaEmpenho->fornecedor_compra_id;

        switch ($tipo) {
            case 'Contrato':
                return $this->minutaRepository->getItensContrato($minutaEmpenho);
                break;
            case 'Compra':
            case 'Suprimento':
                return $this->minutaRepository->getItensCompra($minutaEmpenho, $fornecedor_id, $fornecedor_compra_id);
                break;
        }
    }

    /**
     * Responsável em recuperar as minutas de empenho substitutiva de contrato
     *
     * issue 741
     * @return mixed
     */
    public function getMinutasModalCriarContratoTipoEmpenho()
    {
        return $this->minutaRepository->getMinutasModalCriarContratoTipoEmpenho();
    }

    /**
     *
     * Verifica se o arquivo da minuta de empenho está salva na pasta storage
     *
     * @param array $dados_empenho
     * @param $retornaCaminho
     * @return bool|string
     */
    public function verificaArquivoMinutaEmpenho(Array $dados_empenho, $retornaCaminho = null)
    {
        $nome_arquivo = $dados_empenho['ugemitente'] . '_' . $dados_empenho['anoempenho'] . 'NE'
            .  str_pad($dados_empenho['numempenho'], 6, "0", STR_PAD_LEFT);
        $path = 'empenhos_pdf/'.$dados_empenho['ugemitente'] . '_' . $dados_empenho['anoempenho'] . 'NE'
            .  str_pad($dados_empenho['numempenho'], 6, "0", STR_PAD_LEFT).'/';

        if ($retornaCaminho) {
            return $path.$nome_arquivo.".pdf";
        }

        return file_exists(config('app.app_path')."storage/app/" . $path.$nome_arquivo . ".pdf");
    }

    public function getMinutaByEmpenho($empenho, $fornecedor, $unidade){
       return $this->minutaRepository->getMinutaByEmpenho($empenho, $fornecedor, $unidade);
    }
}
