<?php

namespace App\services\ExcessoesPNCP\Handlers\Interfaces;

/**
 *
 */
interface Handler
{
    /**
     * @param Handler $handler
     * @return Handler
     */
    public function setNext(Handler $handler): Handler;

    /**
     * @param int $status
     * @param string $message
     * @param string $cnpj
     * @param null $data
     * @return mixed
     */
    public function handle(int $status, string $message, string $cnpj, $data = null, $exception = null);
}
