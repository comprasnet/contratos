<?php

namespace App\services\ExcessoesPNCP\Handlers\InserirContrato;


use App\services\ExcessoesPNCP\Handlers\AbstractHandler;
use App\services\ExcessoesPNCP\Handlers\Interfaces\Handler;
use GuzzleHttp\Exception\ClientException;

/**
 *
 */
class ManipuladorExcecoesInserirContrato implements Handler
{
    /**
     * @var AbstractHandler
     */
    private $handlerChain;

    private $unauthorizedHandler;
    private $unprocessableEntityHandler;
    #private $termoWithoutContratoHandler;

    /**
     *
     */
    public function __construct(
        UnauthorizedHandler        $unauthorizedHandler,
        UnprocessableEntityHandler $unprocessableEntityHandler,
        TermoWithoutCompraHandler  $termoWithoutCompraHandler
    )
    {
        $this->unauthorizedHandler = $unauthorizedHandler;
        $this->unprocessableEntityHandler = $unprocessableEntityHandler;
        $this->termoWithoutCompraHandler = $termoWithoutCompraHandler;
        $this->buildHandlerChain();
    }

    /**
     * @return void
     */
    private function buildHandlerChain()
    {
        $this->handlerChain = $this->unauthorizedHandler;

        $this->handlerChain->setNext($this->unprocessableEntityHandler);
        # O atributo termoWithoutContratoHandler não foi implementado
        #$this->unprocessableEntityHandler->setNext($this->termoWithoutContratoHandler);
        $this->unprocessableEntityHandler->setNext($this->termoWithoutCompraHandler);
        // Adicione mais manipuladores conforme necessário
    }

    /**
     * @param int $status
     * @param string $message
     * @param string $cnpj
     * @param null $data
     * @return mixed
     */
    public function handle(int $status, string $message, string $cnpj, $data = null, $exception = null)
    {
        return $this->handlerChain->handle($status, $message, $cnpj, $data, $exception);
    }

    /**
     * @param Handler $handler
     * @return Handler
     */
    public function setNext(Handler $handler): Handler
    {
        $this->handlerChain->setNext($handler);
        return $handler;
    }

    public function setException(\Exception $exception)
    {
        $this->exception = $exception;
    }
}
