<?php

namespace App\services\ExcessoesPNCP\Handlers\InserirContrato;

use App\Http\Controllers\Api\PNCP\ContratoControllerPNCP;
use App\Http\Traits\EnviaPncpTrait;
use App\Models\Codigoitem;
use App\Models\Contratohistorico;
use App\Models\EnviaDadosPncp;
use App\Models\Orgao;
use App\Models\PncpUsuario;
use App\Models\Unidade;
use App\services\ExcessoesPNCP\Handlers\AbstractHandler;
use App\services\PNCP\DataPncpContrato;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Exception;

/**
 *
 */
class UnprocessableEntityHandler extends AbstractHandler
{
    use EnviaPncpTrait;

    /**
     * @param int $status
     * @param string $message
     * @param string $cnpj
     * @param null $data
     * @return null
     * @throws \Exception
     */
    public function handle(int $status, string $message, string $cnpj, $data = null, $exception = null)
    {
        if (preg_match(
            '/Inclusão não permitida pois já existe contrato para os parâmetros informados/',
            $message)
        ) {
            if (get_class($data['envio']) === 'App\Models\Contratohistorico') {
                if ($this->inclusionContractNotAllowed($data)) {
                    return $this->exceptionToResponseSuccess($exception);
                }
            }
        }

        return parent::handle($status, $message, $cnpj, $data, $exception);
    }

    /**
     * @param $data
     * @return void
     */
    private function inclusionContractNotAllowed($data): bool
    {
        $envio = $data['envio'];
        $cnpj = $data['cnpj'];


        $enviaDadosPncp = EnviaDadosPncp::where('pncpable_id', $data['envio']['id'])
            ->where('pncpable_type', '=', Contratohistorico::class)
            ->first();

        if ($enviaDadosPncp) {

            $codigoUnidade = Unidade::select('codigo')->find($data['envio']['unidade_id'])->codigo;

            if ($codigoUnidade) {

                $sequencialSearcher = new DataPncpContrato($data['envio']->contrato);

                $dataContrato = $sequencialSearcher->search();

                if (empty($sequencialSearcher->dataContrato)) {
                    $mensagem = "Compra válida mas não existe registro na base de dados (tabela compras).
                    ID Envia dados PNCP: {$enviaDadosPncp->id}";
                    Log::error($mensagem);

                    return false;
                }

                $linkPncp = $sequencialSearcher->getLinkCreator()->create();

                if (!is_null($linkPncp)) {
                    EnviaDadosPncp::where('id', $enviaDadosPncp->id)->update([
                        'situacao' => $this->codigoItem(null, 'SUCESSO')->id,
                        'retorno_pncp' => '',
                        'link_pncp' => $linkPncp,
                        'sequencialPNCP' => $dataContrato->numeroControlePNCP
                    ]);
                    return true;
                }
            }
        }
        return false;
    }

    private function validationUnidadeBasePNCP(string $cnpj)
    {
        $orgao = Orgao::select(
            'orgaos.id',
            'unidades.nome as nome_unidade',
            'unidades.codigo as codigo_unidade',
            'municipios.codigo_ibge'
        )
            ->join('unidades', 'orgaos.id', '=', 'unidades.orgao_id')
            ->join('municipios', 'unidades.municipio_id', '=', 'municipios.id')
            ->where('orgaos.cnpj', $cnpj)
            ->whereNotNull('unidades.municipio_id')
            ->orderBy('unidades.updated_at', 'DESC')
            ->first();

        if ($orgao) {
            $response = $this->get(
                $this->montaUrl("v1/orgaos/$cnpj/unidades", null),
                $this->montaHeader(true, [])
            );

            $collection = new Collection(json_decode($response->getBody()->getContents()));

            if (!$this->checaUnidadeExistente($collection, $orgao->codigo_unidade, $orgao->nome_unidade,
                $orgao->codigo_ibge)) {
                return $orgao;
            }
        }

        return null;
    }

    private function sendContratoErroUnidadeCadastradaPNCP($cnpj, $data)
    {
        $dadosUnidade = $this->validationUnidadeBasePNCP($cnpj);

        if (!empty($dadosUnidade)) {
            if ($data) {
                $contratoControllerPNCP = new ContratoControllerPNCP();
                $contratoControllerPNCP->inserirContrato($data['envio'], $data['cnpj'], $data['id_unico'], $cnpj);
            }

            Log::info("sendContratoErroUnidadeCadastradaPNCP: $cnpj", ['dados_enviados' => $data]);
        }
    }

    /**
     * @param $cnpj
     * @param $data
     * @return void
     * @throws Exception
     */
    private function unregisteredUnitCode($cnpj, $data)
    {
        $dadosUnidade = $this->validationUnidadeBasePNCP($cnpj);

        if (!empty($dadosUnidade)) {
            # Montar as informações necessárias para vincular a unidade ao CNPJ no PNCP
            $body = json_encode([
                "codigoIBGE" => $dadosUnidade->codigo_ibge,
                "codigoUnidade" => $dadosUnidade->codigo_unidade,
                "nomeUnidade" => $dadosUnidade->nome_unidade
            ]);

            # Enviar a requisição
            $this->post(
                $this->montaUrl("v1/orgaos/$cnpj/unidades", null),
                $this->montaHeader(true, $this->headerPadrao),
                $body
            );

            if ($data) {
                $contratoControllerPNCP = new ContratoControllerPNCP();
                $contratoControllerPNCP->inserirContrato($data['envio'], $data['cnpj'], $data['id_unico'], $cnpj);
            }

            Log::info("Nova unidade cadastrada para o CNPJ: $cnpj", ['dados_enviados' => $body]);
        }

//        $orgao = Orgao::select(
//            'orgaos.id',
//            'unidades.nome as nome_unidade',
//            'unidades.codigo as codigo_unidade',
//            'municipios.codigo_ibge'
//        )
//            ->join('unidades', 'orgaos.id', '=', 'unidades.orgao_id')
//            ->join('municipios', 'unidades.municipio_id', '=', 'municipios.id')
//            ->where('orgaos.cnpj', $cnpj)
//            ->whereNotNull('unidades.municipio_id')
//            ->orderBy('unidades.updated_at', 'DESC')
//            ->first();
//
//        if ($orgao) {
//            $response = $this->get($this->montaUrl("v1/orgaos/$cnpj/unidades", null), $this->montaHeader(true, []));
//
//            $collection =  new Collection(json_decode($response->getBody()->getContents()));
//
//            if (!$this->checaUnidadeExistente($collection, $orgao->codigo_unidade, $orgao->nome_unidade, $orgao->codigo_ibge)) {
//
//                $body = json_encode([
//                    "codigoIBGE" => $orgao->codigo_ibge,
//                    "codigoUnidade" => $orgao->codigo_unidade,
//                    "nomeUnidade" => $orgao->nome_unidade
//                ]);
//
//                $this->post($this->montaUrl("v1/orgaos/$cnpj/unidades", null), $this->montaHeader(true, $this->headerPadrao), $body);
//
//                if ($data) {
//                    $contratoControllerPNCP = new ContratoControllerPNCP();
//                    $contratoControllerPNCP->inserirContrato($data['envio'], $data['cnpj'], $data['id_unico'], $cnpj);
//                }
//
//                Log::info("Nova unidade cadastrada para o CNPJ: $cnpj", ['dados_enviados' => $body]);
//            }
//        }
    }

    /**
     * @param Collection $collection
     * @param $codigoUnidade
     * @param $nomeUnidade
     * @param $codigoIbge
     * @return bool
     */
    private function checaUnidadeExistente(Collection $collection, $codigoUnidade, $nomeUnidade, $codigoIbge)
    {
        return $collection->contains(function ($item) use ($codigoUnidade, $nomeUnidade, $codigoIbge) {
            return $item->codigoUnidade === $codigoUnidade
                && $item->nomeUnidade === $nomeUnidade
                && $item->municipio->codigoIbge === $codigoIbge;
        });
    }

    /**
     * @param Collection $collection
     * @param $codigoUnidade
     * @return mixed|string
     */
    private function checaContratoEnviado(Collection $collection, $codigoUnidade)
    {
        $index = $collection->search(function ($item) use ($codigoUnidade) {
            return $item['unidade_codigo'] === $codigoUnidade;
        });

        return $index !== false && $index !== null ? $index : '';
    }

    /**
     * @param $id
     * @param $descres
     * @return mixed
     */
    public function codigoItem($id = null, $descres = null)
    {
        return Codigoitem::select('id', 'descres', 'descricao')
            ->when($id, function ($query, $id) {
                $query->where('id', $id);
            })
            ->when($descres, function ($query, $descres) {
                $query->where('descres', $descres);
            })
            ->first();
    }
}
