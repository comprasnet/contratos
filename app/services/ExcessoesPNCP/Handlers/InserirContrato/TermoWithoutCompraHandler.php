<?php

namespace App\services\ExcessoesPNCP\Handlers\InserirContrato;

use App\Http\Controllers\Api\PNCP\ContratoControllerPNCP;
use App\Http\Traits\EnviaPncpTrait;
use App\Models\Codigoitem;
use App\Models\Contratohistorico;
use App\Models\EnviaDadosPncp;
use App\Models\Orgao;
use App\Repositories\CodigoItemRepository;
use App\services\ExcessoesPNCP\Handlers\AbstractHandler;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

/**
 *
 */
class TermoWithoutCompraHandler extends AbstractHandler
{
    use EnviaPncpTrait;

    /**
     * @param int $status
     * @param string $message
     * @param string $cnpj
     * @param null $data
     * @return null
     * @throws \Exception
     */
    public function handle(int $status, string $message, string $cnpj, $data = null, $exception = null)
    {
        if (preg_match(
            '/Compra não encontrada/',
            $message)
        ) {
            if (get_class($data['envio']) === 'App\Models\Contratohistorico') {
                if ($this->setIncPen($data)) {
                    return $this->exceptionToResponseSuccess($exception, ['action' => 'REMOVED']);
                }
            }
        }

        return parent::handle($status, $message, $cnpj, $data, $exception);
    }

    /**
     * @param $data
     * @return void
     */
    private function setIncPen($data): bool
    {
        /* @var Contratohistorico $contrato */
        $contrato = $data['envio'];
        $enviadados = EnviaDadosPncp::query()
            ->where('pncpable_type', '=', Contratohistorico::class)
            ->where('pncpable_id', '=', $contrato->id)->first();
        if ($enviadados) {
            $enviadados->delete();
            return true;
        }
        return false;
    }

}
