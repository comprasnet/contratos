<?php

namespace App\services\ExcessoesPNCP\Handlers\InserirContrato;

use App\Http\Controllers\Api\PNCP\ContratoControllerPNCP;
use App\services\ExcessoesPNCP\Handlers\AbstractHandler;
use Illuminate\Support\Collection;

/**
 *
 */
class UnauthorizedHandler extends AbstractHandler
{
    /**
     * @param int $status
     * @param string $message
     * @param string $cnpj
     * @param null $data
     * @return null
     * @throws \Exception
     */
    public function handle(int $status, string $message, string $cnpj, $data = null,$exception=null)
    {
        if ($status === 401) {

            if (!empty($message)) {

                $pattern = '/[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}/';

                if (preg_match($pattern, $message, $matches)) {

                    $uuid = $matches[0];

                    $response = $this->get($this->montaUrl("v1/usuarios?login=$uuid", null), $this->montaHeader(true, []));

                    $response =  json_decode($response->getBody()->getContents())[0];

                    if ($response->id) {

                        $collection = new Collection($response->entesAutorizados);

                        if (!$collection->contains('cnpj', $cnpj)) {

                            $body = json_encode([
                                'entesAutorizados' => [
                                    $cnpj
                                ]
                            ]);

                            $this->post($this->montaUrl("v1/usuarios/{$response->id}/orgaos", null), $this->montaHeader(true, $this->headerPadrao), $body);

                            if ($data) {
                                $contratoControllerPNCP = new ContratoControllerPNCP();
                                $contratoControllerPNCP->inserirContrato($data['envio'], $data['cnpj'], $data['id_unico'], $cnpj);
                            }
                        }
                    }
                }
            }
        }

        return parent::handle($status, $message, $cnpj, $data,$exception);
    }
}
