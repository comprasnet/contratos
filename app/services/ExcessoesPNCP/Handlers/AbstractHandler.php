<?php

namespace App\services\ExcessoesPNCP\Handlers;

use App\Http\Controllers\Api\PNCP\PncpController;
use App\services\ExcessoesPNCP\Handlers\Interfaces\Handler;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Stream;

/**
 *
 */
abstract class AbstractHandler extends PncpController implements Handler
{
    /**
     * @var
     */
    private $nextHandler;


    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param Handler $handler
     * @return Handler
     */
    public function setNext(Handler $handler): Handler
    {
        $this->nextHandler = $handler;
        return $handler;
    }

    /**
     * @param int $status
     * @param string $message
     * @param string $cnpj
     * @param null $data
     * @return null
     */
    public function handle(int $status, string $message, string $cnpj, $data = null, $exception = null)
    {
        if ($this->nextHandler) {
            return $this->nextHandler->handle($status, $message, $cnpj, $data, $exception);
        }
        throw $exception;
    }

    public function exceptionToResponseSuccess($exception)
    {

        $response = new Response();
        /*
         * Altera o response para sucesso, pois ao chamar esse metodo
         * significa que Alguma Classe da nossa cadeia de tratamento
         * conseguiu tratar o erro com sucesso
         */
        $newBody = "{}";
        $streamBody = fopen('data://application/json,' . $newBody,'r');
        $newResponse = $response->withBody(new Stream($streamBody));

        $newResponse->JSONEnviado = $exception->body;
        return $newResponse;
    }


}
