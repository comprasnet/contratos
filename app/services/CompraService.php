<?php

namespace App\services;

use App\Http\Controllers\CNBS;
use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\CompraTrait;
use App\Models\Catmatseritem;
use App\Models\Compra;
use App\Repositories\CompraRepository;
use App\XML\ApiSiasg;

class CompraService
{
    use BuscaCodigoItens;
    use CompraTrait;

    //TODO FAZER USO CORRETO DO REPOSITORY
    //TODO REFATORAR OS METODOS AQUI PRESENTE PARA FAZER O ACESSO AO BANCO PELO REPOSITORY
    protected $compraRepository;

    protected $amparoLegalService;

    public function __construct(CompraRepository $compraRepository, AmparoLegalService $amparoLegalService)
    {
        $this->compraRepository = $compraRepository;
        $this->amparoLegalService = $amparoLegalService;
    }

    public function updateOrCreateCompra($request)
    {
        $request = (object)$request;
        $compraPertenceLei14133Derivadas = $this->compraPertenceLei14133Derivadas($request->lei);

        if ($compraPertenceLei14133Derivadas) {
            $sisrpId = $this->retornaIdCodigoItem('Tipo Compra', 'SISRP');

            //Não atualiza compra caso SISRP e LEI14133. A compra é atualizada no gestão de atas
            if ($request->tipo_compra_id == $sisrpId) {
                return Compra::where('unidade_origem_id', (int)$request->unidade_origem_id)
                    ->where('modalidade_id', (int)$request->modalidade_id)
                    ->where('numero_ano', $request->numero_ano)
                    ->where('tipo_compra_id', $request->tipo_compra_id)
                    ->first();
            }
        }

        return Compra::updateOrCreate(
            [
                'unidade_origem_id' => (int)$request->unidade_origem_id,
                'modalidade_id'     => (int)$request->modalidade_id,
                'numero_ano'        => $request->numero_ano,
                'tipo_compra_id'    => $request->tipo_compra_id
            ],
            [
                'unidade_subrrogada_id' => $request->unidade_subrrogada_id,
                'tipo_compra_id'        => $request->tipo_compra_id,
                'inciso'                => $request->inciso,
                'lei'                   => $request->lei,
                'artigo'                => $request->artigo,
                'id_unico'              => $request->id_unico,
                'cnpjOrgao'             => $request->cnpjOrgao,
                'origem'                => $request->origem ?? '2',
                'numero_contratacao'    => $request->numero_contratacao ?? null
            ]
        );
    }

    /**
     * Retorna o tipo de origem da compra
     * @param $retornoSiasg
     * @return int
     */
    public function getApiOrigem($retornoSiasg): int
    {
        if (isset($retornoSiasg->data->api_origem)) {
            switch ($retornoSiasg->data->api_origem) {
                case 'novo_divulgacao_compra':
                    return 1;
                case 'pncp':
                    return 3;
                default:
                    return 2;
            }
        }

        return 2; //siasg
    }

    public function atribuirCatmatpdmToItems($compra, $retornoSiasg): void
    {
        $itensSemPdm = $compra->getItensMaterialSemCatmatpdm();
        if (isset($retornoSiasg->data->linkSisrpCompleto)) {
            $consultaCompra = new ApiSiasg();
            $linkSisrpCompleto = $retornoSiasg->data->linkSisrpCompleto;
            foreach ($itensSemPdm as $index => $item) {
                foreach ($linkSisrpCompleto as $urlItem) {
                    if (strpos($urlItem->linkSisrpCompleto, "&numeroItem=" . $item->numero) !== false) {
                        $dadosItemCompra = ($consultaCompra->consultaCompraByUrl($urlItem->linkSisrpCompleto));
                        $catmatpdm_id = $this->getCatmatpdm_id((object)$dadosItemCompra['data']['dadosAta']);
                        Catmatseritem::where('id', $item->catmatseritem_id)->update(['catmatpdm_id' => $catmatpdm_id]);
                        break;
                    }
                }
            }
            return;
        }

        //caso os itens não venham do SIASG
        foreach ($itensSemPdm as $index => $item) {
            $pdm = CNBS::getPdmMaterialByCode($item->codigo_siasg);
            if (isset($pdm)) {
                $obj = new stdClass;
                $obj->tipo = 'M';
                $obj->pdm = str_pad($pdm->codigoPdm, 5, 0, STR_PAD_LEFT);
                $obj->nomePdm = $pdm->nomePdm;
                $catmatpdm_id = $this->getCatmatpdm_id($obj);
                Catmatseritem::where('id', $item->catmatseritem_id)->update(['catmatpdm_id' => $catmatpdm_id]);
            }
        }
    }

    public function compraPertenceLei14133Derivadas(string $leiCompra)
    {
        return $this->amparoLegalService->existeAmparoLegalEmLeiCompra($leiCompra);
    }

    public function recuperarAmparoLegalFormatadoPorLeiCompra(string $leiCompra)
    {
        return $this->amparoLegalService->recuperarAmparoLegalFormatadoPorLeiCompra($leiCompra);
    }
}
