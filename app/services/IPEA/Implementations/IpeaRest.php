<?php

namespace App\services\IPEA\Implementations;

use App\Models\IndiceValores;
use Illuminate\Support\Facades\DB;

use function GuzzleHttp\json_decode;
use function GuzzleHttp\json_encode;

class IpeaRest
{

    protected  $client;

    public function client($url)
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $url);
        $result =  (string) $response->getBody();
        return json_decode($result);
    }

    function getJson(){
        $json_acumulado = $this->client("http://www.ipeadata.gov.br/api/odata4/ValoresSerie(SERCODIGO='DIMAC_ICTI1')")->value;//acumulado
        $json_variacao = $this->client("http://www.ipeadata.gov.br/api/odata4/ValoresSerie(SERCODIGO='DIMAC_ICTI2')")->value;//variacao
        return [
            'acumulado' => $json_acumulado,
            'variacao' => $json_variacao
        ];
    }

    function handle(){
        DB::transaction(function () {
            $tipo_indice = 9997;
            $json = $this->getJson();
           //dd($json);
           $retorno_acumulado = $json['acumulado'];
           $retorno_variacao = $json['variacao']; 
           $resultado = array();

            foreach ($retorno_acumulado as $imported_value) {                 
                //$i = key($imported_value);
                
                $date = explode('-', date('Y-m-d H:i:s', strtotime($imported_value->VALDATA)));
                $month = $date[1];
                $year = $date[0];                
                $acumulado = $imported_value->VALVALOR;
                $resultado[$year.$month] = [
                    'ano' => $year,
                    'mes' => $month,
                    'acumulado' => $acumulado,                    
                    'tipo_indices_codigo' => $tipo_indice
                ];
            }

            foreach ($retorno_variacao as $imported_value) {                 
                
                $date = explode('-', date('Y-m-d H:i:s', strtotime($imported_value->VALDATA)));
                $month = $date[1];
                $year = $date[0];                
                $variacao = $imported_value->VALVALOR;
                
                if (!isset($resultado[$year.$month])){
                $resultado[$year.$month] = [
                    'ano' => $year,
                    'mes' => $month,
                    'acumulado' => 0,
                    'tipo_indices_codigo' => $tipo_indice,
                    'variacao' => $variacao
                ]; 
                } else                             
                $resultado[$year.$month]['variacao'] = $variacao;                        
            }

            //print_r (collect($resultado)->toJson()); die();            

            foreach($resultado as $item){
                
                IndiceValores::firstOrCreate([
                    'ano' => $item['ano'],
                    'mes' => $item['mes'],
                    'acumulado_mes' => $item['acumulado'],
                    'variacao' => $item['variacao'],
                    'tipo_indices_codigo' => $tipo_indice
                ]);
            }
        });
        
    }

}
