<?php

namespace App\services;

use App\Models\Empenho;
use App\Models\Empenhodetalhado;
use App\Models\Fornecedor;
use App\Models\MinutaEmpenho;
use App\Models\Naturezadespesa;
use App\Models\Naturezasubitem;
use App\Models\Planointerno;
use App\Models\Unidade;
use App\Repositories\EmpenhoRepository;
use App\Repositories\MinutaEmpenhoRepository;
use App\services\STA\STAUrlFetcherService;
use App\XML\Execsiafi;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Traits\Users;
use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\ContratoItemMinutaEmpenhoTrait;

class EmpenhoService
{
    use Users, BuscaCodigoItens, ContratoItemMinutaEmpenhoTrait;

    /**
     * @var MinutaEmpenhoRepository
     */
    protected $minutaEmpenhoRepository;

    public function __construct(MinutaEmpenhoRepository $minutaEmpenhoRepository, EmpenhoRepository $empenhoRepository)
    {
        $this->minutaEmpenhoRepository = $minutaEmpenhoRepository;
        $this->empenhoRepository = $empenhoRepository;
    }

    /**
     * Busca informações do empenho pelo id enviado na rota
     * @param $empenhoId
     * @return \Illuminate\Http\JsonResponse
     */
    public function consultarEmpenhoPorId($empenhoId): \Illuminate\Http\JsonResponse
    {
        try {

            $booPossuiPermissaoParaEmpenho = true;

            $arrEmpenhoIdValidate = $this->validarEmpenhoId($empenhoId);

            if (!$arrEmpenhoIdValidate['isValid']) {
                return $this->responseJson($arrEmpenhoIdValidate['message'], Response::HTTP_BAD_REQUEST);
            }

            $arrParams = [
                'situacao' => $this->retornaIdCodigoItem('Situações Minuta Empenho', 'EMPENHO EMITIDO')
            ];

            $minutaDoEmpenho = MinutaEmpenho::select('minutaempenhos.*')
                ->join('saldo_contabil', 'saldo_contabil.id', '=', 'minutaempenhos.saldo_contabil_id')
                ->join('empenhos', function ($join) {
                    $join->on('empenhos.numero', '=', 'minutaempenhos.mensagem_siafi')
                        ->on('empenhos.unidade_id', '=', 'saldo_contabil.unidade_id');
                })
                ->where('minutaempenhos.situacao_id', $arrParams['situacao'])
                ->where('empenhos.id', $empenhoId)
                ->first();

            $empenho = $this->minutaEmpenhoRepository->recuperarEmpenhoParaConsultaEmpenhoAPI($empenhoId);

            #Empenho não encontrado
            if (!$empenho) {
                return $this->responseJson(config('mensagens.empenho-not-found'), Response::HTTP_NOT_FOUND);
            }

            #se nao for admin
            if (!$this->userIsAdmin()) {
                $booPossuiPermissaoParaEmpenho = $this->usuarioTemPermissaoParaEmpenho($empenho->ug_emitente_empenho);
            }

            #Usuário sem permissão para consultar empenho
            if (!$booPossuiPermissaoParaEmpenho) {
                return $this->responseJson(
                    config('mensagens.empenho-user-unauthorized'),
                    Response::HTTP_FORBIDDEN
                );
            }

            #Retorno com sucesso
            $arrayDadosMinuta = $empenho->toArray();
            #seto atributo da minuta para poder usar na funcao tratarItensMinutaEmpenho
            if ($minutaDoEmpenho) {
                $minutaDoEmpenho->id_minuta = $minutaDoEmpenho->id;
                $minutaDoEmpenho->tipo_minuta = $minutaDoEmpenho->tipo_empenhopor->descricao;
            }

            $itensMinuta = $this->tratarItensMinutaEmpenho($minutaDoEmpenho, true);

            #atribui os itens da minuta
            $arrayDadosMinuta['itens_minuta'] = $itensMinuta;

            return response()->json($arrayDadosMinuta, Response::HTTP_OK, [], JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);


        } catch (\Exception $e) {
            $message = $e->getMessage();
            $code = Response::HTTP_UNPROCESSABLE_ENTITY;

            #verifica se o valor do id ultrapassou o range do integer e retorna erro
            if (strpos($e->getMessage(), 'Numeric value out of range') !== false) {
                $message = config('mensagens.empenho-invalid-numeric-id');
                $code = Response::HTTP_BAD_REQUEST;
            }

            return $this->responseJson($message, $code);
        }
    }

    private function validarEmpenhoId($empenhoId)
    {
        $validador = Validator::make(['id' => $empenhoId], [
            'id' => 'required|integer|min:1',
        ], [
            'id.integer' => config('mensagens.empenho-invalid-id'),
            'id.min' => config('mensagens.empenho-min-id'),
        ]);

        if ($validador->fails()) {
            return [
                'isValid' => false,
                'message' => $validador->errors()->getMessages()['id'][0]
            ];
        }

        return [
            'isValid' => true,
            'message' => ''
        ];
    }

    private function responseJson($message, $code, $status = 'error')
    {

        return response()->json(
            ['message' => $message, 'status' => $status],
            $code,
            [],
            JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT
        );

    }

    public function getEmpenho(string $numeroEmpenho, Fornecedor $fornecedor, Unidade $unidade): ?Empenho
    {
        return $this->empenhoRepository->getEmpenho($numeroEmpenho, $fornecedor->id, $unidade->id);
    }

    public function getEmpenhoSTA(string $numeroEmpenho, Fornecedor $fornecedor, Unidade $unidade): bool
    {

        $sta_host = env('API_STA_HOST');
        $uri = $sta_host . config('rotas-sta.empenho-por-numero') . $unidade->codigosiafi . $unidade->gestao . $numeroEmpenho;

        $urlFetchService = new STAUrlFetcherService($uri);
        $retornoSTA = $urlFetchService->fetchDataJson();

        if ($urlFetchService->isError($retornoSTA)) {
            $error = $urlFetchService->getError($retornoSTA);
            logger()->info(__METHOD__ . ' - Erro ao buscar o empenho no STA ' .
                '[' . $error->error_code . '] ' . $error->error_message);

            return false;
        }

        if (!$retornoSTA) {
            logger()->info(__METHOD__ . ' - Erro ao buscar o empenho no STA ' .
                '[' . 404 . '] ' . 'Empenho não encontrado');

            return false;
        }

        $cpf_cnpj_api = preg_replace('/\D/', '', $retornoSTA['cpfcnpjugidgener']);
        $fornecedorCnpjCpf = preg_replace('/\D/', '', $fornecedor->cpf_cnpj_idgener);

        if ($fornecedorCnpjCpf != $cpf_cnpj_api) {
            logger()->info(__METHOD__ . ' - Erro ao buscar o empenho no STA ' .
                '[- NO CODE -] ' . "Fornecedor informado: ($fornecedorCnpjCpf) não corresponde ao do empenho no STA: ($cpf_cnpj_api) ");

            return false;
        }

        if (!$this->saveEmpenhoSTA($numeroEmpenho, $fornecedor, $unidade, $retornoSTA)) {
            return false;
        };

        return true;
    }

    public function getEmpenhoSIAFI(string $numeroEmpenho, Fornecedor $fornecedor, Unidade $unidade): bool
    {
        $execsiafi = new Execsiafi();
        $user = backpack_user();
        $user->cpf = env('USUARIO_SIAFI');
        $user->senhasiafi = env('SENHA_SIAFI');
        $amb = config('app.ambiente_siafi');
        $ano = substr($numeroEmpenho, 0, 4);

        if ($execsiafi->validarAnoSiafi($ano) === false) {
            return false;
        }
        $retorno = $execsiafi->empenhoDetalhado($user, $unidade->codigosiafi, $amb, $ano, $numeroEmpenho);

        if ($retorno['situacao'] == 'ERRO' || $retorno === false) {
            return false;
        }

        $cpf_cnpj_api = preg_replace('/\D/', '', $retorno['codFavorecido']);
        $fornecedorCnpjCpf = preg_replace('/\D/', '', $fornecedor->cpf_cnpj_idgener);

        if ($fornecedorCnpjCpf == $cpf_cnpj_api) {
            logger()->info(__METHOD__ . ' - Erro ao buscar o empenho no SIAFI ' .
                '[- NO CODE -] ' . "Fornecedor informado: ($fornecedorCnpjCpf) não corresponde ao do empenho no STA: ($cpf_cnpj_api) ");

            return false;
        }

        if (!$this->saveEmpenhoSIAFI($numeroEmpenho, $fornecedor, $unidade, $retorno)) {
            return false;
        }

        return true;
    }

    private function saveEmpenhoSTA(string $empenho, Fornecedor $fornecedor, Unidade $unidade, array $retornoSTA): bool
    {

        DB::beginTransaction();
        try {
            $planoInterno = Planointerno::where('codigo', $retornoSTA['picodigo'])
                ->get()
                ->first();
            $naturezaDespesa = Naturezadespesa::where('codigo', $retornoSTA['naturezadespesa'])
                ->where('sistema_origem', $retornoSTA['sistemaorigem'])
                ->get()
                ->first();
            // Caso não tenha a ND na nossa base, ele cadastra
            if (!$naturezaDespesa) {
                unset($naturezaDespesa);

                $naturezaDespesa = Naturezadespesa::create([
                    'codigo' => $retornoSTA['naturezadespesa'],
                    'descricao' => $retornoSTA['naturezadespesadescricao'],
                    'situacao' => true,
                    'sistema_origem' => $retornoSTA['sistemaorigem']
                ]);
            }
            $objEmpenho = Empenho::updateOrCreate(
                [
                    'numero' => $empenho,
                    'unidade_id' => $unidade->id,
                    'fornecedor_id' => $fornecedor->id
                ],
                [
                    'planointerno_id' => $planoInterno->id,
                    'naturezadespesa_id' => $naturezaDespesa->id,
                    'fonte' => $retornoSTA['fonte'],
                    'informacao_complementar' => $retornoSTA['informacaocomplementar'],
                    'sistema_origem' => $retornoSTA['sistemaorigem'],
                    'data_emissao' => $retornoSTA['emissao'],
                    'ptres' => $retornoSTA['ptres'],
                    'created_at' => now(),
                    'updated_at' => now()
                ]
            );

            if (!empty($retornoSTA['itens'])) {
                foreach ($retornoSTA['itens'] as $item) {
                    $naturezaSubitem = Naturezasubitem::where('naturezadespesa_id', $naturezaDespesa->id)
                        ->where(
                            'codigo',
                            str_pad($item['subitem'], 2, "0", STR_PAD_LEFT)
                        )
                        ->get()
                        ->first();

                    // Caso não encontre o item na nossa base, ele cadastra
                    if (!$naturezaSubitem) {
                        $naturezaSubitem = Naturezasubitem::create([
                            'naturezadespesa_id' => $naturezaDespesa->id,
                            'codigo' => str_pad($item['subitem'], 2, "0", STR_PAD_LEFT),
                            'descricao' => $item['subitemdescricao'],
                            'situacao' => true,
                        ]);
                    } else {
                        Naturezasubitem::updateOrCreate(
                            [
                                'naturezadespesa_id' => $naturezaDespesa->id,
                                'codigo' => str_pad($item['subitem'], 2, "0", STR_PAD_LEFT),
                            ],
                            [
                                'descricao' => trim($item['subitemdescricao'])
                            ]
                        );
                    }

                    Empenhodetalhado::updateOrCreate(
                        [
                            'empenho_id' => $objEmpenho->id,
                            'naturezasubitem_id' => $naturezaSubitem->id
                        ],
                        [
                            'created_at' => now(),
                            'updated_at' => now()
                        ]
                    );
                }
            }
            DB::commit();
            return true;
        } catch (Exception $e) {
            Log::error('erro em buscaInsereEmpenhoSTA: ' . $e->getMessage());
            DB::rollBack();
            return false;
        }
    }

    private function saveEmpenhoSIAFI(string $empenho, Fornecedor $fornecedor, Unidade $unidade, array $retornoSIAFI): bool
    {
        DB::beginTransaction();
        try {

            $planoInterno = Planointerno::where(['codigo' => trim($retornoSIAFI['codPlanoInterno'])])
                ->get()
                ->first();
            $naturezaDespesa = Naturezadespesa::where(['codigo' => $retornoSIAFI['naturezaDespesa']])
                ->get()
                ->first();

            // Caso não tenha a ND na nossa base, ele cadastra
            if (!$naturezaDespesa) {
                unset($naturezaDespesa);

                $naturezaDespesa = Naturezadespesa::create([
                    'codigo' => $retornoSIAFI['naturezaDespesa'],
                    'descricao' => $retornoSIAFI['descricao'],
                    'situacao' => true,
                    'sistema_origem' => $retornoSIAFI['sistemaOrigem']
                ]);
            }

            $objEmpenho = Empenho::updateOrCreate(
                [
                    'numero' => $empenho,
                    'unidade_id' => $unidade->id,
                    'fornecedor_id' => $fornecedor->id
                ],
                [
                    'planointerno_id' => $planoInterno->id,
                    'naturezadespesa_id' => $naturezaDespesa->id,
                    'fonte' => $retornoSIAFI['fonte'],
                    'informacao_complementar' => $retornoSIAFI['infoComplementar'],
                    'sistema_origem' => $retornoSIAFI['sistemaOrigem'],
                    'data_emissao' => $retornoSIAFI['dataEmissao'],
                    'ptres' => $retornoSIAFI['codPTRES'],
                    'created_at' => now(),
                    'updated_at' => now()
                ]
            );

            if (!empty($retorno['natureza_subitem_desc'])) {
                foreach ($retorno['natureza_subitem_desc'] as $subitem) {

                    $naturezaSubItem = Naturezasubitem::where(['codigo' => str_pad($subitem['codSubElemento'], 2, "0", STR_PAD_LEFT)])
                        ->where(['naturezadespesa_id' => $naturezaDespesa->id])
                        ->get()
                        ->first();

                    // Caso não encontre o item na nossa base, ele cadastra
                    if (!$naturezaSubItem) {
                        $naturezaSubItem = Naturezasubitem::create([
                            'naturezadespesa_id' => $naturezaDespesa->id,
                            'codigo' => str_pad($subitem['codSubElemento'], 2, "0", STR_PAD_LEFT),
                            'descricao' => $subitem['descricao'],
                            'situacao' => true,

                        ]);
                    }
                    Empenhodetalhado::updateOrCreate(
                        [
                            'empenho_id' => $objEmpenho->id
                        ],
                        [
                            'naturezasubitem_id' => $naturezaSubItem->id,
                            'created_at' => now(),
                            'updated_at' => now()
                        ]
                    );
                }
            }
            DB::commit();
            return true;

        } catch (Exception $e) {
            Log::info('erro em buscaInsereEmpenhoSIAFI: ' . $e->getMessage());
            DB::rollBack();
            return false;
        }
    }

    public static function validaAnoEmpenho(string $numero, int $anoMinimo): bool
    {
        $anoAtual = now()->year;
        $anoEmpenho = self::getAnoEmpenho($numero);

        return $anoEmpenho >= $anoMinimo && $anoEmpenho <= $anoAtual;
    }

    /**
     * Verifica se os 4 primeiros caracteres do empenho são números inteiros.
     */
    public static function checkIfEmpenhoIsInt($numero)
    {
        return ctype_digit(substr($numero, 0, 4));
    }

    /**
     * Retorna os 4 primeiros caracteres do empenho (ano).
     */
    public static function getAnoEmpenho($numero)
    {
        return substr($numero, 0, 4);
    }
}
