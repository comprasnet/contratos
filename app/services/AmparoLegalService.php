<?php

namespace App\services;

use App\Http\Traits\Formatador;
use App\Repositories\AmparoLegalRepository;

class AmparoLegalService
{
    use Formatador;

    protected $amparoLegalRepository;

    public function __construct(AmparoLegalRepository $amparoLegalRepository)
    {
        $this->amparoLegalRepository = $amparoLegalRepository;
    }

    public function amparoLegalLei14133(?array $idAmparoLegal): bool
    {
        if (empty($idAmparoLegal)) {
            return false;
        }

        $amparoLegalLei14133 = $this->amparoLegalRepository->getAmparoPodeEnviarPNCPPorId($idAmparoLegal);

        return $amparoLegalLei14133->count() > 0;
    }

    public function existeAmparoLegal14133PorContrato(int $idContrato)
    {
        return $this->amparoLegalRepository->existeAmparoLegal14133PorContrato($idContrato);
    }
    public function consultarAmparoLegal(
        $modalidade = null,
        $ato_normativo = null,
        $artigo = null,
        $paragrafo = null,
        $inciso = null,
        $alinea = null
    ) {
        return $this->amparoLegalRepository->getPorFiltros(
            $modalidade,
            $ato_normativo,
            $artigo,
            $paragrafo,
            $inciso,
            $alinea
        );
    }

    public function verificarAmparoLegal($retornoSiasg, array $arrayAmparoLegal)
    {
        if (isset($retornoSiasg->data->compraSispp->lei)) {
            $lei = $retornoSiasg->data->compraSispp->lei;

            $leiCompraContemAmparoLegalInformado = $this->amparoLegalInformadoPertenceLeiCompra($lei, $arrayAmparoLegal);

            if (!$leiCompraContemAmparoLegalInformado) {
                return "O campo amparo legal está incorreto, a compra pertence a {$lei}.";
            }
        }

        return true;
    }

    public function amparoLegalInformadoPertenceLeiCompra(string $lei, array $arrayAmparoLegal)
    {
        $arrayAmparoLegalFormatadoLeiCompra = array();
        foreach ($arrayAmparoLegal as $amparoLegal) {
            $amparoLegal =  $this->amparoLegalRepository->getAmparoLegalUnico($amparoLegal);
            $leiCompraPorAmparo = $this->converterAmparoLegalEmLeiCompra($amparoLegal->ato_normativo);
            array_push($arrayAmparoLegalFormatadoLeiCompra, $leiCompraPorAmparo);
        }

        return in_array($lei, $arrayAmparoLegalFormatadoLeiCompra);
    }

    public function converterAmparoLegalEmLeiCompra(string $atoNormativo)
    {
        $pattern = '/(\d[\d.]*)\s*\/.*/';
        $arrayAtoNormativo = explode(' ', $atoNormativo);
        $nomeAtoNormativo = $arrayAtoNormativo[0];
        $arrayNumeroAtoNormativo = explode('/', $arrayAtoNormativo[1]);
        $somenteNumeroAtoNormativo = $this->retornaSomenteNumeros($arrayNumeroAtoNormativo[0]);

        $leiFormatada = $nomeAtoNormativo;

        if ($nomeAtoNormativo === 'DECRETO') {
            $leiFormatada = 'D';
        }

        if($nomeAtoNormativo === 'CONTRATAÇÃO'){
            if (preg_match($pattern, $atoNormativo, $match)) {
                $somenteNumeroAtoNormativo = str_replace('.', '', $match[1]);
                $leiFormatada = 'LEI';
            }
        }

        return "$leiFormatada$somenteNumeroAtoNormativo";
    }

    public function existeAmparoLegalEmLeiCompra(string $lei)
    {
        $lei = $this->formataLeiDecreto($lei);

        return $this->queryBaseAmparoLegalPorLeiCompra($lei)->exists();
    }

    public function recuperarAmparoLegalFormatadoPorLeiCompra(string $lei)
    {
        return $this->queryBaseAmparoLegalPorLeiCompra($lei)
            ->selectRaw(
                'DISTINCT REGEXP_REPLACE(SPLIT_PART(ato_normativo, \'/\', 1), \'[.\s]\', \'\', \'g\')
                as ato_normativo_formatado'
            )->pluck('ato_normativo_formatado')->toArray();
    }

    public function queryBaseAmparoLegalPorLeiCompra(string $lei)
    {
        return $this->amparoLegalRepository->getAmparoLegal14133Base()
            ->whereRaw('REGEXP_REPLACE(SPLIT_PART(ato_normativo, \'/\', 1), \'[.\s]\', \'\', \'g\') = ?', [$lei]);
    }
}
