<?php

namespace App\services\ErrosPNCP;

use App\Models\EnviaDadosPncp;

class TrataErrosContratoTermoService
{
    /**
     * Trata os erros de contratos PCNP.
     *
     * @param int $id
     * @return mixed
     */
    public static function errosContratosPncp($id)
    {
        $enviaDadosPncp = EnviaDadosPncp::find($id);
        $contratoEnviado = EnviaDadosPncp::where('envia_dados_pncp.contrato_id', $enviaDadosPncp->contrato_id)
            ->join('codigoitens', 'codigoitens.id', '=', 'envia_dados_pncp.situacao')
            ->whereIn('envia_dados_pncp.tipo_contrato', config('api-pncp.tipos_contrato_aceitos_pncp'))
            ->whereNotIn('codigoitens.descres',['EXCLUIDO', 'INCPEN'])
            ->whereNotNull('sequencialPNCP')
            ->first();
        if ($contratoEnviado) {
            return true;
        }
        return false;
    }
}
