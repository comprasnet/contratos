<?php

namespace App\services;

use App\Http\Controllers\classSEI;
use App\Http\Traits\EnviaPncpTrait;
use App\Http\Traits\LogTrait;
use App\Http\Traits\ValidarTermoPNCP;
use App\Models\Codigoitem;
use App\Models\Contrato;
use App\Models\Contratoarquivo;
use App\Models\ContratoMinuta;
use App\Repositories\ContratoArquivoRepository;
use App\services\PNCP\PncpService;
use Exception;
use Illuminate\Http\Request;

class ContratoArquivoService
{
    use LogTrait;
    use EnviaPncpTrait;
    use ValidarTermoPNCP;
    use EnviaPncpTrait;

    protected $contratoArquivoRepository;
    protected $pncpService;
    protected $amparoLegalService;

    public function __construct(
        ContratoArquivoRepository $contratoArquivoRepository,
        PncpService               $pncpService,
        AmparoLegalService        $amparoLegalService
    )
    {
        $this->contratoArquivoRepository = $contratoArquivoRepository;
        $this->pncpService = $pncpService;
        $this->amparoLegalService = $amparoLegalService;
    }

    public function alterarSituacaoRestritoArquivoContrato(bool $restrito, int $idContratoArquivo, string $nomeUsuario)
    {
        $textoMensagem = "Status alterado com sucesso, o documento está público";
        if ($restrito) {
            $textoMensagem = "Status alterado com sucesso, o documento está restrito";
        }
        $dadosAtualizacao = $this->retornaModeloDeRestricao($restrito, $nomeUsuario);
        try {
            $arquivo = $this->contratoArquivoRepository->getContratoArquivoUnico($idContratoArquivo);
            $instrumentoInicial = $arquivo->contrato->historico()->oldest()->first();
            $this->contratoArquivoRepository->atualizarSituacaoRestrito($idContratoArquivo, $dadosAtualizacao);
            //contrato em elaboracao não pode enviar pncp
            if (!is_null($instrumentoInicial)) {
                $idAmparoLegal = $instrumentoInicial->amparolegal->pluck('id')->toArray();
                $poderEnviarPncp = $this->pncpService->verificarEnviarArquivoPNCP($idAmparoLegal, $instrumentoInicial);
                $response = $this->recuperarDadosPNCP($idContratoArquivo, $restrito, $poderEnviarPncp);
                # Se estava tentando publicar e não conseguiu, retorna para o status restrito
                if (!$restrito && !empty($response->code) && $response->code >= 400) {
                    $dadosAtualizacao = [
                        'restrito' => !$restrito,
                        'envio_pncp_pendente' => null,
                        'justificativa_exclusao' => null
                    ];
                    $this->contratoArquivoRepository->atualizarSituacaoRestrito(
                        $idContratoArquivo,
                        $dadosAtualizacao
                    );
                    \Alert::error($response->message)->flash();
                    return $response;
                }
            }
            \Alert::success($textoMensagem)->flash();
        } catch (Exception $exception) {
            \Alert::error('Erro ao salvar o arquivo')->flash();
            $this->inserirLogCustomizado('pncp', 'error', $exception);
        }
        return $textoMensagem;
    }

    private function retornaModeloDeRestricao($restrito, $nomeUsuario)
    {
        $justificativa = null;
        $dadosAtualizacao = [
            'restrito' => $restrito,
            'justificativa_exclusao' => $justificativa,
            'envio_pncp_pendente' => 'INCARQ'
        ];
        if ($restrito) {
            $justificativa .= "Documento removido pelo usuário: {$nomeUsuario}";
            $dadosAtualizacao = [
                'restrito' => $restrito,
                'justificativa_exclusao' => $justificativa,
                'envio_pncp_pendente' => 'DELARQ'
            ];
        }
        return $dadosAtualizacao;
    }

    public function save(Request $request, $local)
    {
        return $this->contratoArquivoRepository->save($request, $local);
    }

    public function listaTipoMeuContrato()
    {
        return $this->contratoArquivoRepository->listaTipoMeuContrato();
    }

    public function listaTipo()
    {
        return $this->contratoArquivoRepository->listaTipo();
    }

    public function colunas()
    {
        $colunas = [
            [
                'name' => 'created_at',
                'label' => 'Data', // Table column heading
                'type' => 'datetime',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'getContrato',
                'label' => 'Número do instrumento', // Table column heading
                'type' => 'model_function',
                'function_name' => 'getContrato', // the method in your Model
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
                'searchLogic' => function ($query, $column, $searchTerm) {
                    $query->orWhereHas('contrato', function ($q) use ($searchTerm) {
                        $q->where('numero', 'like', '%' . $searchTerm . '%');
                    });
                }
            ],
            [
                'name' => 'getTipo',
                'label' => 'Tipo', // Table column heading
                'type' => 'model_function',
                'function_name' => 'getTipo', // the method in your Model
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
                'searchLogic' => function ($query, $column, $searchTerm) {
                    $query->orWhereHas('codigoItem', function ($q) use ($searchTerm) {
                        $q->where('descricao', 'like', '%' . $searchTerm . '%');
                    });
                }
            ],
            [
                'name' => 'processo',
                'label' => 'Processo',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'sequencial_documento',
                'label' => 'Nº SEI / Chave Acesso Sapiens',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'getLinkAnexo',
                'label' => 'Arquivos',
                'type' => 'model_function_sei',
                'function_name' => 'getLinkAnexo',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
                'searchLogic' => function ($query, $column, $searchTerm) {
                    $query->orWhere('descricao', 'like', '%' . $searchTerm . '%');
                }
            ],
            [
                'name' => 'getOrigemAnexo',
                'label' => 'Origem', // Table column heading
                'type' => 'model_function',
                'function_name' => 'getOrigemAnexo', // the method in your Model
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
                'searchLogic' => function ($query, $column, $searchTerm) {
                    $tipos['PDF'] = 0;
                    $tipos['SEI'] = 1;
                    $tipos['Minuta'] = 2;
                    $searchTerm = strtoupper($searchTerm);
                    if (array_key_exists($searchTerm, $tipos)) {
                        $query->orWhere('origem', 'like', '%' . $tipos[$searchTerm] . '%');
                    }
                }
            ],
            [
                'name' => 'restrito',
                'label' => 'Restrito',
                'type' => "model_function",
                'function_name' => 'formatarTextoRestricao',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
            ],
        ];

        return $colunas;
    }

    public function campos($con, $amparoLegal = null, $num_processo, $elaboracao = false, $local)
    {
        $descricaoTab = (backpack_user()->hasRole('Administrador')) ? 'anexo' : 'arquivo';
        $campos = [
            [ // select_from_array
                'name' => 'contrato_id',
                'label' => "Número do instrumento",
                'type' => 'select_from_array',
                'options' => $con,
                'allows_null' => false,
                'attributes' => [
                    'readonly' => "true"
                ]
            ],
            [
                'name' => 'processo',
                'label' => 'Número Processo',
                'type' => 'numprocesso',
                'default' => $num_processo,
                'attributes' => [
                    'readonly' => "true"
                ]
            ],
            [   // Upload
                'name' => 'arquivos',
                'label' => 'Arquivos',
                'type' => 'tableSeiArquivo',
                'upload' => true,
                'disk' => 'local', // if you store files in the /public folder, please ommit this; if you store them in /storage or S3, please specify it;
                'tab' => "Envio local de {$descricaoTab}(s)",
                'options' => [
                    'tipos' => $local == "meus-contratos" ? $this->listaTipoMeuContrato() : $this->listaTipo(),
                    'tiposVinculadosNoPncp' => Contrato::getDescricoesTiposArquivosVinculadosPncp(),
                ],
            ],
        ];

        if (
            backpack_user()->hasRole('Administrador') ||
            backpack_user()->hasRole('Administrador Unidade') ||
            backpack_user()->hasRole('Administrador Órgão') ||
            backpack_user()->hasRole('Setor Contratos') ||
            backpack_user()->hasRole('Administrador Suporte') ||
            backpack_user()->hasRole('Responsável por Contrato')
        ) {
            $campos[] = [
                'name' => 'link_sei',
                'type' => 'tableSei',
                'tab' => 'Vinculação de Documento(s) do SEI',
                'options' => $local == "meus-contratos" ? $this->listaTipoMeuContrato() : $this->listaTipo(),
            ];
        }

        if ($local != "meus-contratos") {
            $idAmparoLegal = $amparoLegal->pluck('id')->toArray();
            $contratoPertenceLei14133 = $this->amparoLegalService->amparoLegalLei14133($idAmparoLegal);

            $campos[] = [
                'name' => 'contratoLei14133',
                'type' => 'hidden',
                'value' => $contratoPertenceLei14133
            ];

            $campos[] = [
                'name' => 'elaboracao',
                'type' => 'hidden',
                'value' => $elaboracao,
                'attributes' => [
                    'id' => 'elaboracao'
                ]
            ];
        }
        return $campos;
    }

    public function atualizacaoSituacaoArquivoMinuta($id, $ondeVeio)
    {
        // Verificar se o documento vinculado com origem de Minuta do Comprasgovbr já está assinado no SUPER(SEI)

        $arquivo_minuta = Contratoarquivo::find($id);

        //Verifica se origem do arquivo é Minuta
        if ($arquivo_minuta->origem == '2') {
            //Obtém o id_documento através do link armazenado no campo arquivos
            $id_documento = explode('&', $arquivo_minuta->arquivos);
            $id_documento = (str_replace('id_documento=', '', $id_documento[1]));

            //Obtém o documento_formatado na tb contrato_minutas buscando pelo id_documento
            $minuta_documento = ContratoMinuta::join('codigoitens', "codigoitens.id", "=", "codigoitens_id")
                ->where('id_documento', '=', $id_documento)
                ->select("contrato_minutas.*", "codigoitens.descricao")
                ->get();

            //Consulta serviço SEI consultarDocumento passando o documento_formatado
            try {
                $sei = new classSEI();

                $documento = $sei->consultarDocumento(null, $minuta_documento[0]->documento_formatado);

                if (is_null($documento) == true) {
                    throw new \Exception('Documento informado não encontrado no SUPER(SEI). Exclua o vínculo.');
                    return redirect('/gescon/contrato/' . $arquivo_minuta->contrato_id . '/arquivos');
                } else {
                    $qtd_assinaturas = count($documento->Assinaturas);
                    //Verifica se a qtd de assinaturas retornadas é igual à qtd_min_assinaturas salva na minuta de documento vinculada
                    if ($qtd_assinaturas >= $minuta_documento[0]->qtd_min_assinaturas) {
                        //Atualiza registro tb contrato_arquivos: origem = 1 e qtd de assinaturas
                        $arquivo_minuta->assinaturas_documento = $qtd_assinaturas;
                        $arquivo_minuta->origem = 1; //Origem = SUPER(SEI)
                        $codigo_itens = ['Termo de Apostilamento', 'Termo de Rescisão', 'Termo Aditivo', 'Contrato'];
                        $tipo_arquivos = Codigoitem::whereIn('descricao', $codigo_itens)->pluck('id')->toArray();

                        if (in_array($arquivo_minuta->tipo, $tipo_arquivos)) {
                            $arquivo_minuta->restrito = false;
                        }

                        $dadosContratoHistorico = $this->retornarDadosContratoHistorico(
                            $minuta_documento[0]->descricao,
                            $minuta_documento[0]->numero_termo_contrato,
                            $minuta_documento[0]->contrato_id
                        );

                        if (!empty($dadosContratoHistorico)) {
                            $arquivo_minuta->contratohistorico_id = $dadosContratoHistorico->id;
                        }
                        $arquivo_minuta->update();

                        \Alert::success('Situação da minuta de documento alterada com sucesso!')->flash();

                        return redirect('/gescon/' . $ondeVeio . '/' . $arquivo_minuta->contrato_id . '/arquivos');
                    } else {
                        $arquivo_minuta->assinaturas_documento = $qtd_assinaturas;
                        $arquivo_minuta->update();

                        \Alert::warning('Não foi possível atualizar a situação do documento: a quantidade de assinaturas constante no documento não corresponde ao mínimo informado no momento da criação da minuta. Caso necessário, vincule o documento através da opção Adicionar Arquivo do Contrato')->flash();
                        return redirect()->back();
                    }
                }
            } catch (\Exception $e) {
                \Alert::warning($e->getMessage())->flash();
                return redirect()->back();
            }
        }
    }

    public function validaTamanhoArquivoSEI($request)
    {
        if (isset($request->nomeSei)) {
            $items = array_map(function ($link, $nome) {
                return ['link' => $link, 'nomeSei' => $nome];
            }, $request->get('linkAnexoSei', []), $request->get('nomeSei', []));
        
            $errors = [];
        
            foreach ($items as $item) {
                $url = $item['link'];
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HEADER, true);
        
                curl_exec($ch);
                $headerSize = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
                $actualSize = curl_getinfo($ch, CURLINFO_SIZE_DOWNLOAD);
        
                curl_close($ch);
        
                $totalSize = $headerSize + $actualSize;
                $fileSizeMB = $totalSize / (1024 * 1024);
        
                if ($fileSizeMB > 30) {
                    $errors[] = $item['nomeSei'];
                }
            }
        
            if (!empty($errors)) {
                return ['error' => true, 'errors' => $errors];
            }
        }
        
        return ['error' => false];
    }
}
