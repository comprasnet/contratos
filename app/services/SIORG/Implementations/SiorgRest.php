<?php

namespace App\services\SIORG\Implementations;

use App\Http\Traits\Busca;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\Log;

class SiorgRest
{
    use Busca;

    private $codigo;
    const MAIN_URL = 'http://estruturaorganizacional.dados.gov.br';
    const URL_ORGANIZACIONAL = 'http://estruturaorganizacional.dados.gov.br/doc/estrutura-organizacional';
    const URL_UNIDADE_ORGANIZACIONAL = 'http://estruturaorganizacional.dados.gov.br/doc/unidade-organizacional';
    const COMPLETA = '/completa';

    const ESTRUTURA = '/estrutura';
    const FILHA = '/filha';
    const RETORNAR_FILHOS = 'retornarOrgaoEntidadeVinculados=SIM';
    const NAO_RETORNAR_FILHOS = 'retornarOrgaoEntidadeVinculados=NAO';

    const ESFERA = '/id/esfera';

    const PODER = '/id/poder';

    const ENDERECO_CONTATO =  '/endereco-contato';

    const NATUREZA_JURIDICA = '/id/natureza-juridica';

    const CATEGORIA_UNIDADE = '/doc/categoria-unidade';

    public function __construct($codigo)
    {
        $this->codigo = $codigo;

    }

    public function getUnidadeOrganizacionalEstruturaCompleta()
    {
        $url = self::URL_ORGANIZACIONAL . self::COMPLETA . '?codigoPoder=1&codigoEsfera=1&codigoUnidade=' . $this->codigo . '&' . self::RETORNAR_FILHOS;

        $client = new Client([
            'timeout' => 1800,
            'verify' => false,
        ]);

        try {
            $response = $client->get($url,[
                RequestOptions::VERIFY => false,
                RequestOptions::TIMEOUT => 1800,
            ]);
            $data = json_decode($response->getBody(), true);

            return $data;
        } catch (\Exception $e) {
            return ['error' => $e->getMessage()];
        }
        //return $this->buscaDadosFileGetContents($url);
    }

    public static function getUnidadeOrganizacionalEstruturaIndividualCompleta($codigo)
    {
        $url = self::URL_UNIDADE_ORGANIZACIONAL . "/$codigo" . self::COMPLETA;

        $siorgRestInstance = new SiorgRest($codigo);

        return $siorgRestInstance->buscaDadosFileGetContents($url);
    }

    public static function getCategoriaUnidadeDescricao($codigo)
    {
        $codigo = "$codigo?ativo=SIM";

        $retorno = self::getDescricao(self::CATEGORIA_UNIDADE, $codigo);

        return $retorno['categoriaUnidade'][0]['descricaoCategoriaUnidade'];
    }

    public static function getEsferaDescricao($id)
    {
        $retorno = self::getDescricao(self::ESFERA, $id);

        return $retorno['esfera'][0]['descricaoEsfera'];
    }

    public static function getPoderDescricao($id)
    {
        $retorno = self::getDescricao(self::PODER, $id);

        return $retorno['poder'][0]['descricaoPoder'];
    }

    public static function getNaturezaJuridicaDescricao($id)
    {
        $retorno = self::getDescricao(self::NATUREZA_JURIDICA, $id);

        return $retorno['naturezaJuridica'][0]['descricaoNaturezaJuridica'];
    }

    private static function getDescricao($tipo, $id)
    {
        $url = self::MAIN_URL . $tipo . "/$id";

        $siorgRestInstance = new SiorgRest($id);

        return $siorgRestInstance->buscaDadosFileGetContents($url);
    }

    public static function getEnderecoContato($codigo){
        $url = self::URL_UNIDADE_ORGANIZACIONAL . "/$codigo" . self::ENDERECO_CONTATO;

        $siorgRestInstance = new SiorgRest($codigo);

        return $siorgRestInstance->buscaDadosFileGetContents($url);
    }

}
