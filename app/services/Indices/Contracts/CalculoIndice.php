<?php

namespace App\services\Indices\Contracts;

use App\Models\TipoIndices;
use App\services\Indices\Contracts\Interfaces\IndiceCalculavel;

 abstract class CalculoIndice implements IndiceCalculavel{
    protected $valor_reajustado = array();
	protected $data_inicio;
	protected $data_fim;
	protected $valor;
	protected $tipo_indice;
	protected $result_html;
	protected $tipo_calculo;
	protected $forma_calculo;

	public function __construct($inicio = '', $fim = '', $valor = '', $tipo_indice = '')
	{
		$indice = TipoIndices::where('codigo', $tipo_indice)->first();
		$this->forma_calculo = $indice->forma_calculo;

		$this->setDataInicio($inicio);
		$this->setDataFim($fim);
		$this->setValor($valor);
		$this->setTipoIndice($tipo_indice);
	}

	function formaCalculo(){
		return $this->forma_calculo;
	}
	function setDataInicio($valor)
	{
		$this->data_inicio = $valor;
	}
	function setDataFim($valor)
	{
		$this->data_fim = $valor;
	}
	function setValor($valor)
	{
		$this->valor = $valor;
	}

	public function setTipoIndice($value)
	{
		$this->tipo_indice = $value;
	}

	function getDataInicio()
	{
		return $this->data_inicio;
	}
	function getDataFim()
	{
		return $this->data_fim;
	}
	function getValor()
	{
		return $this->valor;
	}
	function getTipoIndice()
	{
		return $this->tipo_indice;
	}


	 public function getResultRendered(){
		 return $this->result_html;
	 }

	 public function setTipoCalculoMensal(){
		 $this->tipo_calculo = 1;
	 }

	 public function setTipoCalculoGlobal(){
		$this->tipo_calculo = 2;
	}

	public function getTipoCalculo(){
		return $this->tipo_calculo;
	}
	//array com resultado temporário dos calculos realizados pelos índices
	public function getValorReajustado(){
		return $this->valor_reajustado;
	}

	//return type Retorna o incremento de um mes
	public function newDate($dataBase, $return, $incremento = "+1 month")
	{		
		$novaData = strtotime("$incremento", strtotime($dataBase));
		return date("$return", $novaData);
	}
	//return type Retorna array com intervalo de meses
	public function arrayMeses($mInic, $mFim)
	{

		$dataInicio = $this->newDate($mInic, "Y-m-d", "-1 month"); //date($mInicInvertida);
		$dataFim    = $this->newDate($mFim, "Y-m-d", "-1 month");

		$newDate = $dataInicio;
		$mesAtual[] = date("m/Y", strtotime($newDate));
		while ($newDate < $dataFim) {
			$newDate = $this->newDate($newDate, "Y-m-d", "+1 month");
			$mesAtual[] = date("m/Y", strtotime($newDate));
		}
		
		return $mesAtual;
	}
}