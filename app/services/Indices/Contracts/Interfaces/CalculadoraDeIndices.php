<?php

namespace App\services\Indices\Contracts\Interfaces;

interface CalculadoraDeIndices {
    function __construct($inicio, $fim, $valor, $tipo_indice);
    
    function setIndiceCalculavel(IndiceCalculavel $indice);
    function getIndiceCalculavel():IndiceCalculavel;
    function handle();
}