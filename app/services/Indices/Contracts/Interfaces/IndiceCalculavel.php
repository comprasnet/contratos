<?php

namespace App\services\Indices\Contracts\Interfaces;

use Carbon\Carbon;

interface IndiceCalculavel {
    function __construct($marco_inicial = '', $marco_final = '', $valor = '', $tipo_indice = '');
    function setDataInicio($data_inicio);
    function setDataFim($data_fim);
    function setValor($valor);
    function setTipoIndice($valor);
    function formaCalculo();
    function getDataInicio();
    function getDataFim();
    function getValor();
    function getTipoIndice();

    function calcular();
    function getMemoriaCalculo();
}