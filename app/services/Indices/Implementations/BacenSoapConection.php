<?php

namespace App\Services\Indices\Implementations;
use SoapClient;

class BacenSoapConection{
	protected $data_source;

	function __construct(){
		$wsdl_url = config('app.wsdl_bacen');

		$context = stream_context_create([
			'http' => [
				'user_agent' => 'PHPSoapClient'
			]
		]);

		$soapClientOptions = [
			'stream_context' => $context,
			'cache_wsdl'     => WSDL_CACHE_NONE
		];

		$this->data_source = new SoapClient($wsdl_url, $soapClientOptions);
	}

	private function loadXmlStringAsArray($xmlString)
	{
		$array = (array) @simplexml_load_string($xmlString);
		if (!$array) {
			$array = (array) @json_decode($xmlString, true);
		} else {
			$array = (array)@json_decode(json_encode($array), true);
		}
		return $array;
	}

	function buscaPorTipoIndice($tipo){
		try {
			$serie = $this->data_source->getValoresSeriesXML(['codigoSeries' => $tipo, 'dataInicio' => '2020-01-01', 'dataFim' => '2020-01-01']);
			$retorno = $this->loadXmlStringAsArray($serie);
			return $retorno['SERIE'][0]['ITEM'];
		} catch (\Exception $e) {
			return $e->getMessage();
		}
	}
}
