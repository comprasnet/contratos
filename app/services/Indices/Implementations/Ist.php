<?php

namespace App\Services\Indices\Implementations;
use App\services\Indices\Contracts\CalculoIndice;
use Carbon\Carbon;
use DB;
use Http;

class Ist extends CalculoIndice {	
	
    public function calcular(){		
		$marco_inicial = $this->marco_inicial;
		$marco_final = $this->marco_final;
		
		$MarcoInicialCarbon 	= Carbon::createFromFormat('d/m/Y H:i:s',$marco_inicial.' 00:00:00');
		$MarcoFinalCarbon  	= Carbon::createFromFormat('d/m/Y H:i:s',$marco_final.' 24:00:00');
		
		$qtdeMeses = $MarcoInicialCarbon->diffInMonths($MarcoFinalCarbon);
		$valor = $this->valor;				


		//reajuste pela fórmula do manual
		$filtro_marco_inicial = $this->newDate($marco_inicial, "mY", "-1 month");//mes da proposta
		$filtro_marco_final = $this->newDate($marco_final, "mY", "+0 month");//mes do reajustamento
		
		// l i
		$t1 = DB::select("select * from ist where compet = '$filtro_marco_final' and ativo is null limit 0,1"); //dd($indice_momento_reajuste);
		$indice_marco_final = 0;
		if ($t1) {
			$indice_marco_final =  $t1[0]->ist;
		}
		
		$t2 = DB::select("select * from ist where compet = '$filtro_marco_inicial' and ativo is null limit 0,1");
		$indice_marco_inicial = 0;
		if ($t2) {
			$indice_marco_inicial =  $t2[0]->ist;
		}


		$valorAluguel =  $valor;

		/*
             *        V (I – I°)
             *   R = ------------, onde:
             *          I°
             *   R = Valor do reajuste procurado;
             *   V = Valor constante da proposta;
             *   I = Índice relativo ao mês do reajustamento;
             *   I° = Índice relativo ao mês da proposta
             */
			
		
		$calculoManual = $valorAluguel + (($valorAluguel * ($indice_marco_final - $indice_marco_inicial)) / $indice_marco_inicial);
		$percReajuste  = number_format(((($valorAluguel + ($valorAluguel * ($indice_marco_final - $indice_marco_inicial) / $indice_marco_inicial)) * 100) / $valor) - 100, 2, ",", ".");
		$memoriaCalculo = $this->getMemoriaCalculo($valorAluguel, $calculoManual, $indice_marco_final, $indice_marco_inicial);
		
		$this->valor_reajustado['aux_reajustado'] 			= $calculoManual;
		$this->valor_reajustado['aux_percentual_reajuste'] 	= $percReajuste;
		$this->valor_reajustado['aux_wli'] 					= $indice_marco_final;
		$this->valor_reajustado['aux_wlo'] 					= $indice_marco_inicial;

		$valor_original 	= $valor;
		$valor_reajustado 	= $calculoManual;
		$percentual_reajuste 	= $percReajuste;
		$inicio 		= $marco_inicial;
		$fim 			= $marco_final;
		$this->result_html = view('reajuste.calculo.item_reajustado', compact(['valor_original', 'valor_reajustado', 'percentual_reajuste', 'inicio', 'fim','qtdeMeses','memoriaCalculo']))->render();
		
    }

	function getMemoriaCalculo(){
		$valor = $this->getValor();
		$valor_reajustado = $this->valorReajustado;
		$indice_mes_reajuste = $this->indiceMesReajuste;
		$indice_mes_proposta = $this->indiceMesProposta;		
		if($valor == '' || $valor_reajustado =='' || $indice_mes_proposta == '' || $indice_mes_proposta == ''):
			throw new \Exception('Informe os parametros: valor,valor_reajustado,indice_mes_reajuste e indice_mes_proposta');
		endif;
		$memoriaCalculo = '<li>Fórmula:<br><kbd style=\'display:block;text-align:center; margin-bottom:.5rem\'> R = V (I – I°) / I°</kbd></li>';
		$memoriaCalculo .= '<li><hr></li>';
		$memoriaCalculo .= '<li>(R) Valor do reajuste procurado: ?</li>';
		$memoriaCalculo .= '<li>(V) Valor da Proposta: <span class=\'badge badge-secondary\'>R$ '.number_format($valor,2,',','.').'</span></li>';
		$memoriaCalculo .= '<li>(I)  Índice mês reajuste: <span class=\'badge badge-secondary\'>'.$indice_mes_reajuste.'?</span></li>';
		$memoriaCalculo .= '<li>(I°) Índice mês proposta: <span class=\'badge badge-secondary\'>'.$indice_mes_proposta.'?</span></li>';		
		$memoriaCalculo .= '<li>R = <span class=\'badge badge-secondary\'>R$ '.number_format($valor_reajustado,2,',','.').'</span></li>';

		$memoriaCalculo = '<ul class=\'memoria-calculo\'>'.$memoriaCalculo.'</ul>';
		return $memoriaCalculo;
	}
}