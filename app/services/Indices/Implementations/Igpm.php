<?php

namespace App\Services\Indices\Implementations;

use App\Models\IndiceValores;
use App\services\Indices\Contracts\CalculoIndice;
use Carbon\Carbon;
use DB;


class Igpm extends CalculoIndice{
	protected $valorReajustado = 0;
	protected $indiceMesReajuste = 0;
	protected $indiceMesProposta = 0;
	
	
	private function indiceReajustePorData($competencia){
		dd($this->getTipoIndice());
		$result = IndiceValores::whereHas('TipoIndice', function($q){
			$q->where('codigo', $this->getTipoIndice());
		})->get();
		dd($result);
		//$result = DB::select("select * from igpm where compet = '$competencia' and ativo is null limit 0,1");
		if ($result) {
			return  $result->acumulado_mes;
		}
		return 0;
	}

    public function calcular(){
		
		$marco_inicial = $this->getDataInicio();
		$marco_final = $this->getDataFim();
		$valorAluguel = $this->getValor();
		
		$mInicCarbon 	= Carbon::createFromFormat('Y-m-d H:i:s', $marco_inicial);
		$mFimCarbon  	= Carbon::createFromFormat('Y-m-d H:i:s', $marco_final);
		

		$qtdeMeses = $mInicCarbon->diffInMonths($mFimCarbon);

        //reajuste pela fórmula do manual de engenharia igpm       
        $filtro_marco_inicial = $this->newDate($marco_inicial, "mY", "-1 month");
        $filtro_marco_final = $this->newDate($marco_final, "mY", "+0 month");

		$this->indiceMesReajuste = $this->indiceReajustePorData($filtro_marco_inicial);
		$this->indiceMesProposta = $this->indiceReajustePorData($filtro_marco_final);

		/*
             *              Vo (li – lo)
             *   Vr = Vo + ------------, onde:
             *                  lo
             *   Vr = Valor do aluguel reajustado
             *   Vo = Valor do aluguel vigente
             *   I  = IGP-M do mês anterior ao do início da vigência do contrato ou do reajuste anterior
             *   Io = IGP-M do mês anterior ao da vigência do reajuste atual
             */


		$this->valorReajustado = $valorAluguel + ($valorAluguel * ($this->indiceMesReajuste - $this->indiceMesProposta) / $this->indiceMesProposta);
		$percReajuste  = number_format(((($valorAluguel + ($valorAluguel * ($this->indiceMesReajuste - $this->indiceMesProposta) / $this->indiceMesProposta)) * 100) / $valorAluguel) - 100, 2, ",", ".");
		        
		$valor_original         = $valorAluguel;
		$valor_reajustado       = $this->valorReajustado;
		$percentual_reajuste    = $percReajuste;
		$inicio 				= $mInicCarbon->format('d/m/Y');
		$fim 					= $mFimCarbon->format('d/m/Y');

        $memoriaCalculo = $this->getMemoriaCalculo();
		$this->result_html = view('reajuste.calculo.item_reajustado',compact(['valor_original','valor_reajustado','percentual_reajuste','inicio','fim','qtdeMeses','memoriaCalculo']))->render();

    }

    function getMemoriaCalculo(){
		$valor = $this->getValor(); 
		$valor_reajustado = $this->valorReajustado; 
		$indice_mes_reajuste = $this->indiceMesReajuste; 
		$indice_mes_proposta = $this->indiceMesProposta;

		if($valor == '' || $valor_reajustado =='' || $indice_mes_proposta == '' || $indice_mes_proposta == ''):
			throw new \Exception('Informe os parametros: valor,valor_reajustado,indice_mes_reajuste e indice_mes_proposta');
		endif;
		$memoriaCalculo = '<li>Fórmula:<br><kbd style=\'display:block;text-align:center; margin-bottom:.5rem\'> R = V + V x ((I – I°) / I°)</kbd></li>';
		$memoriaCalculo .= '<li><hr></li>';
		$memoriaCalculo .= '<li>(R) Valor do aluguel reajustado: ?</li>';
		$memoriaCalculo .= '<li>(V) Valor do aluguel vigente: <span class=\'badge badge-secondary\'>R$ '.number_format($valor,2,',','.').'</span></li>';
		$memoriaCalculo .= '<li>(I) IGP-M do mês anterior ao do início da vigência do contrato ou do reajuste anterior: <span class=\'badge badge-secondary\'>'.$indice_mes_reajuste.'</span></li>';
		$memoriaCalculo .= '<li>(I°) IGP-M do mês anterior ao da vigência do reajuste atual: <span class=\'badge badge-secondary\'>'.$indice_mes_proposta.'</span></li>';		
		$memoriaCalculo .= '<li>R = <span class=\'badge badge-secondary\'>R$ '.number_format($valor_reajustado,2,',','.').'</span></li>';
		$memoriaCalculo = '<ul class=\'memoria-calculo\'>'.$memoriaCalculo.'</ul>';
	}
}