<?php

namespace App\services\Indices\Implementations;

use App\Models\IndiceValores;
use App\Models\TipoIndices;
use App\services\Indices\Implementations\BacenSoapConection;
use App\services\Indices\Contracts\CalculoIndice;

use Carbon\Carbon;


class Icti extends CalculoIndice  {
	protected $marcoInicial = null;
	protected $marcoFinal = null;

	//retorna coleção de objetos IndiceValor
	private function buscaIndicePorTipo($buscarWebService = false)
	{
		$tipo = $this->getTipoIndice();
		if($buscarWebService){			
			//IMPLEMENTAR SE NECESSÁRIO
		}

		return IndiceValores::where('tipo_indices_codigo', $tipo)->get();
		
	}

	//override
    public function calcular(){	
		//mensal
		$marco_inicial = $this->getDataInicio();
		$marco_final = $this->getDataFim();
		$valor = $this->getValor();
		$tipoindice = TipoIndices::where('codigo', '=', $this->getTipoIndice())->pluck('descricao')->first();

		//consulta webservice do BC passando o tipo do índice
		$mInicCarbon 	= Carbon::createFromFormat('Y-m-d H:i:s', $marco_inicial);
		$mFimCarbon  	= Carbon::createFromFormat('Y-m-d H:i:s', $marco_final);
							
		// qtde de meses do intervalo do reajuste			
		$qtdeMeses = $mInicCarbon->diffInMonths($mFimCarbon);
		$memoriaCalculo = $this->getMemoriaCalculo();
		$valor_atualizado = $this->getValor();
		

		$percentual_reajuste = ($valor_atualizado > 0)? number_format((($valor_atualizado * 100) / $valor) - 100, 4, ",", ".") : number_format(0, 6, ",", ".");
		$fator = ($valor_atualizado > 0) ?  number_format(($valor_atualizado / $valor), 6, ",",".") : number_format(0, 6, ",", ".");

		$valor_original 		= $valor;
		$valor_reajustado		= $valor_atualizado;
		$inicio 				= $mInicCarbon->format('d/m/Y');
		$fim 					= $mFimCarbon->format('d/m/Y');

		$valor_original = isset($valor_original) ? $valor_original : 0;
		$valor_reajustado = isset($valor_reajustado) ? $valor_reajustado : 0;
		$valor_diferenca_reajuste = $valor_reajustado - $valor_original;

		return view('vendor.backpack.crud.calculadora_indices.resultado_calculo',compact(['valor_diferenca_reajuste','valor_original','valor_reajustado','percentual_reajuste','inicio','fim', 'qtdeMeses', 'memoriaCalculo','tipoindice','fator']))->render();			
    }

	public function getMemoriaCalculo(){
		$marco_inicial = $this->marcoInicial;
		$marco_final = $this->marcoFinal;	
		$valor = $this->getValor();

		if(is_null($marco_final) || is_null($marco_inicial)):
			$marco_inicial = $this->getDataInicio();
			$marco_final = $this->getDataFim();
			if (is_null($marco_final) || is_null($marco_inicial)) :
				throw new \Exception('Necessário informar os parâmetros de marco final e marco inicial');
			endif;
			$marco_inicial 	= Carbon::createFromFormat('Y-m-d H:i:s', $marco_inicial);
			$marco_final  	= Carbon::createFromFormat('Y-m-d H:i:s', $marco_final);
		endif;
		
		$data_inicio = $marco_inicial->format('Y-m-d');
		$data_fim = $marco_final->format('Y-m-d');
		
		$bacenDataHistory = $this->buscaIndicePorTipo();

		// retorna intervalo no formato mm/aaaa
		$arrayMeses = $this->arrayMeses($data_inicio, $data_fim);
		
		// elimina o primeiro mês
		unset($arrayMeses[0]);
		
		$wIndice = '-';
		$memoriaCalculo = '';
		$ultimoMesValido = $marco_final->format('m/Y');

		foreach($bacenDataHistory as $IndiceValor){
			$mes_ano_search = str_pad($IndiceValor->mes, 2, '0', STR_PAD_LEFT) . '/' . $IndiceValor->ano;
			if (array_search($mes_ano_search, $arrayMeses)) {
				if ($mes_ano_search <> $ultimoMesValido) {
					$wMes    = (string) $mes_ano_search;
					//dd($IndiceValor);
					$wIndice = (float) $IndiceValor->acumulado_mes;
					$valor = $valor + (($wIndice / 100) * $valor);
					$memoriaCalculo .= "<li>Mês: " . $wMes . " - Índice: " . $wIndice . "% </li>";
				}
			}
		}
		
		$this->setValor($valor);
		$memoriaCalculo = '<ul>'.$memoriaCalculo.'</ul>';

		return $memoriaCalculo;
	}
}