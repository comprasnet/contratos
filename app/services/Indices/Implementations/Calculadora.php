<?php

namespace App\services\Indices\Implementations;

use App\Models\IndiceValores;
use App\Models\TipoIndices;
use App\services\Indices\Contracts\Interfaces\CalculadoraDeIndices;
use App\services\Indices\Contracts\Interfaces\IndiceCalculavel;
use App\services\Indices\Implementations\Bacen;
use App\Services\Indices\Implementations\Igpm;
use App\Services\Indices\Implementations\Ist;
use Carbon\Carbon;
use Exception;

class Calculadora implements CalculadoraDeIndices
{
	protected $IndiceCalculavel;

	function __construct($inicio, $fim, $valor, $tipo_indice)
	{
		$indice = new Bacen($inicio, $fim, $valor, $tipo_indice);
		$this->setIndiceCalculavel($indice);
	}
	function setIndiceCalculavel(IndiceCalculavel $indice)
	{
		if (($indice->formaCalculo() == 'auto') || ($indice->getTipoIndice() == '9998')) {
			$this->IndiceCalculavel = $indice;
		}
		return null;
	}
	function getIndiceCalculavel(): IndiceCalculavel
	{
		return $this->IndiceCalculavel;
	}
	function handle()
	{
		$indice = $this->getIndiceCalculavel();
		if (!is_null($indice)) {
			//check dates
			$this->checkDateRange($indice->getDataFim());
			return $indice->calcular();
		}		
	}

	function checkDateRange($fim)
	{
		$fim = Carbon::createFromFormat('Y-m-d H:i:s', $fim);		
		$prev_month = $fim->subMonth();

		/* $serie = IndiceValores::where('tipo_indices_codigo', $this->getIndiceCalculavel()->getTipoIndice())
			->where('mes', $prev_month->month)->where('ano', $prev_month->year)->count(); */
			$serie = IndiceValores::where('tipo_indices_codigo', $this->getIndiceCalculavel()->getTipoIndice())
			->where(function($query) use($prev_month){$query->where('mes', intval($prev_month->month))
			->orWhere('mes', \str_pad($prev_month->month, 2, "0", \STR_PAD_LEFT));})->where('ano', $prev_month->year)->count();
			//dd($serie->toSql(), $serie->getBindings());
		if ($serie == 0) {
			return false;
		}
		return true;
		// if($serie == 0){
		// 	throw new Exception('Não existe série registrada para o período selecionado. <br/> Último registro da série: '.$prev_month->month.'/'.$prev_month->year);
		// }
	}
}
