<?php

namespace App\services\Indices\Implementations;

use App\Models\IndiceValores;
use Illuminate\Support\Facades\DB;

use function GuzzleHttp\json_decode;

class ImportacaoIST
{

    protected  $client;

    public function client($url, $periodo)
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', $url, [
            'headers' => [
                'Content-Type' => 'application/json'
            ],
            'json' => [
                'DataInicio' => $periodo['inicio'],
                "DataFim" => $periodo['fim']
            ]
        ]);

        $result =  (string) $response->getBody()->getContents();
        return json_decode($result);
    }

    function getJson($inicio = '2000-01-01'){
        $json = $this->client("https://api.anatel.gov.br/fiscalizacaoregulatoria/ist/ObterDadosIST",
        [
            'inicio' => $inicio,
            'fim' => Date(('Y-m-d'))
        ]);
       return $json;
    }

    function handle(){
        DB::transaction(function () {
            $tipo_codigo = '9998';
            $serie  = $this->getJson();

            foreach($serie as $item)
            {
                $result = IndiceValores::firstOrCreate(
                    [
                        'mes' => explode('-', $item->dataReferencia)[1],
                        'ano' => explode('-', $item->dataReferencia)[0],
                        'variacao' =>  (float) (isset($item->percentual))? $item->percentual : 0,
                        'acumulado_mes' => (float) $item->valor,
                        'tipo_indices_codigo' => (int) $tipo_codigo
                    ]
                );
            }
        });
        
    }

}
