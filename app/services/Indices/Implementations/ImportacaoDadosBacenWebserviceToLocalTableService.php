<?php

namespace App\services\Indices\Implementations;

use App\Models\IndiceValores;
use App\Models\TipoIndices;
use App\services\Indices\Implementations\BacenSoapConection;
use App\services\Indices\Contracts\CalculoIndice;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class ImportacaoDadosBacenWebserviceToLocalTableService
{
	private $DataSource;
	private $tipos_de_indice;
	private $lista_series;

	function __construct()
	{
		$this->DataSource = new BacenSoapConection();
		$this->tipos_de_indice = TipoIndices::where('fonte', 'bacen')->where('forma_calculo', 'auto')->pluck('nome', 'codigo')->toArray();
	}

	function handle()
	{
		return DB::transaction(function () {
			$series = [];
			foreach ($this->tipos_de_indice as $tipo_codigo => $nome_indice) {
				$series[$tipo_codigo] = $this->DataSource->buscaPorTipoIndice($tipo_codigo);
				foreach ($series[$tipo_codigo] as $serie_item) {
					$result = IndiceValores::firstOrCreate(
						[
							'mes' => explode('/', $serie_item['DATA'])[0],
							'ano' => explode('/', $serie_item['DATA'])[1],
							'variacao' => (float) $serie_item['VALOR'],
							'tipo_indices_codigo' => (int) $tipo_codigo
						]
					);
				}
			}
			$this->lista_series = $series;
		});
	}
}
