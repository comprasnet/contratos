<?php

namespace App\services;

use App\Http\Traits\ConsultaCompra;
use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\CompraTrait;
use App\Models\AmparoLegal;
use App\Http\Traits\EnviaPncpTrait;
use App\Models\Autoridadesignataria;
use App\Models\Codigoitem;
use App\Models\Compra;
use App\Models\Contratocronograma;
use App\Models\Contratoempenho;
use App\Models\Contratohistorico;
use App\Models\Contratounidadedescentralizada;
use App\Models\Empenho;
use App\Models\EnviaDadosPncp;
use App\Models\MinutaEmpenho;
use App\Models\Unidade;
use App\Repositories\ContratoRepository;
use App\services\CompraService;
use Illuminate\Foundation\Http\FormRequest;

class ContratoService
{
    use BuscaCodigoItens;
    use CompraTrait;
    use EnviaPncpTrait;
    use ConsultaCompra;

    //TODO FAZER USO CORRETO DO REPOSITORY
    //TODO REFATORAR OS METODOS AQUI PRESENTES PARA FAZER O ACESSO AO BANCO PELO REPOSITORY
    protected $contratoRepository;
    protected $compraService;

    protected $amparoLegalService;
    protected $contratocronograma;

    public function __construct(
        ContratoRepository $contratoRepository,
        CompraService      $compraService,
        AmparoLegalService $amparoLegalService,
        Contratocronograma $contratocronograma
    )
    {
        $this->contratoRepository = $contratoRepository;
        $this->compraService = $compraService;
        $this->amparoLegalService = $amparoLegalService;
        $this->contratocronograma = $contratocronograma;
    }

    public function validarCompraSiasg(\Illuminate\Http\Request $request): bool
    {
        $dados = (object)$request->all()[0];

        if ($dados->contrata_mais_brasil == '1') {
            $retornoSiasg = $this->buscarCompra(
                '',
                '',
                '',
                '',
                '',
                '',
                $this->converterIdContratacaoPNCP($dados->numero_contratacao)
            );
        } else {

            list(
                $modalidade,
                $uasgCompra,
                $uasgUser,
                $uasgBeneficiaria,
                $numeCompra,
                $ano_compra
                ) = $this->setVarsBuscarCompras($request);
            $retornoSiasg = $this->buscarCompra(
                $modalidade,
                $uasgCompra,
                $uasgUser,
                $uasgBeneficiaria,
                $numeCompra,
                $ano_compra
            );
        }

        try {

            //verificacoes igual na minutg
            // caso a compra exista e não tenha problemas retorna true
            if (is_null($retornoSiasg->data)) {
                $this->verificaCompra($dados, $retornoSiasg);
                return true;
            }
            $this->validaRetornoSiasg($retornoSiasg);

            $tipo_compra_id = $this->buscaTipoCompra($retornoSiasg->data->compraSispp->tipoCompra);


            if ($dados->contrata_mais_brasil == '1') {

                $unidade = Unidade::where('codigo', $retornoSiasg->data->compraSispp->unidade)->first();
                $compra = Compra::where('unidade_origem_id', $unidade->id)
                    ->where('modalidade_id', $dados->modalidade_id)
                    ->where('numero_ano', $retornoSiasg->data->compraSispp->numeroAno)
                    ->where('tipo_compra_id', $tipo_compra_id)
                    ->first();
            } else {
                $compra = Compra::where('unidade_origem_id', (int)$dados->unidadecompra_id)
                    ->where('modalidade_id', (int)$dados->modalidade_id)
                    ->where('numero_ano', $dados->licitacao_numero)
                    ->where('tipo_compra_id', $tipo_compra_id)
                    ->first();
            }

            if (is_null($compra)) {
                //se compra for nula e $retornoSiasg Insere a compra
                $compra = $this->insereCompra($retornoSiasg, $dados, $tipo_compra_id);
            }

            $leiPertence14133Derivadas = $this->compraService->compraPertenceLei14133Derivadas($compra->lei);

            if ($leiPertence14133Derivadas && !($compra->verificaPermissaoSisrp(session('user_ug_id')))) {
                throw new \Exception('Você não tem permissão para esta compra.');
            }

            return true;
        } catch (\Exception $e) {
            \Alert::error($e->getMessage())->flash();
            return false;
        }
    }

    /**
     * Verificação de compra. Método retirado de ContratoCrudController
     * @param $dados
     * @param $retornoSiasg
     * @return void
     * @throws \Exception If the purchase does not exist or if the user does not have permission.
     */
    public function verificaCompra($dados, $retornoSiasg): void
    {
        $compra = $this->verificaCompraExiste([
            'unidade_origem_id' => $dados->unidadecompra_id,
            'modalidade_id' => $dados->modalidade_id,
            'numero_ano' => $dados->licitacao_numero
        ]);

        if (!$compra) {
            //se nao veio do buscarCompra e não encontrou no nosso banco
            throw new \Exception($retornoSiasg->messagem);
        }

        if ($compra->tipo_compra_desc === 'SISRP') {
            $leiPertence14133Derivadas = $this->compraService->compraPertenceLei14133Derivadas($compra->lei);
            if ($leiPertence14133Derivadas && !($compra->verificaPermissaoSisrp(session('user_ug_id')))) {
                throw new \Exception($retornoSiasg->messagem);
            }
        }
    }

    /**
     * @param $retornoSiasg
     * @return void
     * @throws \Exception If the return from Siasg is null or if the data property is null.
     * @throws \Exception If the return from Siasg has a messagem property.
     * @throws \Exception If there is an error in querying and updating the purchase.
     */
    public function validaRetornoSiasg($retornoSiasg): void
    {
        #637 - estava gerando erro pouco inteligente ao acessar $retornoSiasg->data,
        #quando o $retornoSiasg chegava em branco
        if (is_null($retornoSiasg)) {
            throw new \Exception('Erro ao consultar e atualizar compra - retorno da consulta chegou em branco.');
        }

        if (is_null($retornoSiasg->data)) {
            if (isset($retornoSiasg->messagem)) {
                throw new \Exception($retornoSiasg->messagem);
            }

            throw new \Exception('Erro ao consultar e atualizar compra');
        }
    }

    public function buscaTipoCompra($descres)
    {
        $tipocompra = Codigoitem::wherehas('codigo', function ($q) {
            $q->where('descricao', '=', 'Tipo Compra');
        })
            ->where('descres', '0' . $descres)
            ->first();
        return $tipocompra->id;
    }


    /**
     * Insere a compra na base de dados. Método retirado de ContratoCrudController
     * @param $retornoSiasg
     * @param $dados
     * @param $tipo_compra_id
     * @return Compra
     * @throws \Exception When the user does not have permission for the purchase.
     * @throws \Exception When the purchase does not have a registered Price Registration Ata.
     * @throws \Exception When the purchase is not found with the selected modality.
     */
    public function insereCompra($retornoSiasg, $dados, $tipo_compra_id): Compra
    {
        $verificacao = $this->verificaPermissaoUasgCompra($retornoSiasg, $dados->unidadecompra_id);
        if (is_null($verificacao)) {
            throw new \Exception('Você não tem permissão para esta compra.');
        }

        $unidade_subrogada = $retornoSiasg->data->compraSispp->subrogada;
        $unidade_subrrogada_id = ($unidade_subrogada !== '000000')
            ? (int)$this->buscaIdUnidade($unidade_subrogada)
            : null;

        $leiPertence14133Derivadas =
            $this->compraService->compraPertenceLei14133Derivadas($retornoSiasg->data->compraSispp->lei);

        if ($leiPertence14133Derivadas && $retornoSiasg->data->compraSispp->tipoCompra == '2') {
            throw new \Exception('Compra não possui Ata de Registro de Preços registrada.');
        }

        $api_origem = $this->compraService->getApiOrigem($retornoSiasg);

        if ($api_origem === 1) {
            $modalidade = $this->retornaDescresPorId($dados->modalidade_id);
            if ($modalidade != $retornoSiasg->data->compraSispp->codigoModalidade) {
                throw new \Exception('Compra não encontrada com a modalidade selecionada');
            }
        }

        $compra = $this->compraService->updateOrCreateCompra([
            'unidade_origem_id' => (int)$dados->unidadecompra_id,
            'modalidade_id' => (int)$dados->modalidade_id,
            'numero_ano' => $dados->licitacao_numero,
            'tipo_compra_id' => $tipo_compra_id,
            'unidade_subrrogada_id' => $unidade_subrrogada_id,
            'inciso' => $retornoSiasg->data->compraSispp->inciso,
            'lei' => $retornoSiasg->data->compraSispp->lei,
            'artigo' => $retornoSiasg->data->compraSispp->artigo,
            'id_unico' => $retornoSiasg->data->compraSispp->idUnico,
            'cnpjOrgao' => $retornoSiasg->data->compraSispp->cnpjOrgao,
            'origem' => $api_origem,
        ]);

        if ($retornoSiasg->data->compraSispp->tipoCompra == '1') {
            $this->gravaParametroItensdaCompraSISPP($retornoSiasg, $compra);
        } else {
            $this->gravaParametroItensdaCompraSISRP($retornoSiasg, $compra);
        }
        return $compra;
    }

    /**
     * @param bool $validaItemNaoSeAplica
     * @param bool $validaLei14133
     * @param $validaNaoSisg
     * @param $request
     * @return string[]|void|null
     */
    public function verificaValidacaoCompra(
        bool $validaItemNaoSeAplica,
        bool $validaLei14133,
             $validaNaoSisg,
             $request
    )
    {
        /*
         * Valida caso não seja 'Não se Aplica'
         * Caso seja 14133
         * ou caso seja 'SISG' e leis nao 14133
         * */
        if ($validaItemNaoSeAplica && ($validaLei14133 || (!$validaLei14133 && $validaNaoSisg))) {
            $objectRequest = new \Illuminate\Http\Request();
            $objectRequest->setMethod('POST');
            $objectRequest->request->add([$request->all()]);

            if (!$this->validarCompraSiasg($objectRequest)) {
                return [
                    "texto_exibicao.validarcompra" => 'Dados da compra inválidos!',
                    "unidadecompra_id.validarcompra" => 'Unidade Da Compra.',
                    "modalidade_id.validarcompra" => 'Modalidade Da Licitação.',
                    "licitacao_numero.validarcompra" => 'Número Da Licitação.',
                ];
            }

            return null;
        }
    }

    /**
     * Faz verificação de amparo legal informado vs lei retornada pelo siasg
     * Se amparo legal for 14.133/2021, lei retornada pelo siasg precisa ser LEI14133
     * Se amparo legal for 8.666, a lei retornada pelo siasg não pode ser LEI14133
     */
    public function verificarAmparoLegalVsLeiSiasg(
        \Illuminate\Http\Request $request,
        bool                     $amparoLegalLei14133
    )
    {
        $arrayIdsAmparosLegais = $request->all()[0]['amparoslegais'] ?? $request->all()[0]['amparolegal'];

        list(
            $modalidade,
            $uasgCompra,
            $uasgUser,
            $uasgBeneficiaria,
            $numeCompra,
            $ano_compra
            ) = $this->setVarsBuscarCompras($request);

        $retornoSiasg = $this->buscarCompra(
            $modalidade,
            $uasgCompra,
            $uasgUser,
            $uasgBeneficiaria,
            $numeCompra,
            $ano_compra
        );

        return $this->amparoLegalService->verificarAmparoLegal($retornoSiasg, $arrayIdsAmparosLegais);
    }

    /**
     * Valida se o amparo legal é da lei 14.133 ou da 8.666, de acordo com a lei passada por parâmetro
     */
    private function validarIdAmparoLegalVsLei($lei, array $listaAmparoLegal)
    {
        $atoNormativo8666 = '8.666';

        if ($lei == '8.666') {
            $arrayIdAmparoLegal =
                AmparoLegal::select('id')->where('ato_normativo', 'LIKE', "%{$atoNormativo8666}%")->pluck('id')->toArray();
        }

        foreach ($listaAmparoLegal as $amparoLegal) {

            if (in_array($amparoLegal, $arrayIdAmparoLegal)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Sets the value of the 'prorrogavel' parameter in the given FormRequest object.
     *
     * @param FormRequest $request The FormRequest object.
     * @return void
     * @throws None
     */
    public function setCampoProrrogavel(FormRequest $request): void
    {
        $request->input('prorrogavel') === 's'
            ? $request->request->set('prorrogavel', true)
            : $request->request->set('prorrogavel', false);
    }

    /**
     * Sets the contrato with indefinite term.
     *
     * @param FormRequest $request The request object containing the data.
     * @return void
     */
    public function setCamposContratoComPrazoIndefinido(FormRequest $request): void
    {
        if ($request->is_prazo_indefinido == "1" || $request->is_prazo_indefinido == true) {
            $request->request->set('vigencia_fim', null);
            $request->request->set('num_parcelas', 1);
        }
    }

    /**
     * Caso tenha empenho preenchido utilizar os campos de unidade, modalidade e numero da licitacao de acordo
     * com a compra da minuta de empenho descartando os valores inseridos pelo usuário
     * @param FormRequest $request The FormRequest object.
     * @return void
     */
    public function setCamposCompras(FormRequest $request): void
    {
        if ($request->get('contrata_mais_brasil') == '1') {
            $camposBaseadosCompraPNCP = $this->buscarCamposBaseadosCompraPNCP($request->get('numero_contratacao'));
        } elseif (!empty($request->get('minutasempenho'))) {
            $camposBaseadosCompraPNCP = $this->buscarCamposBaseadosEmpenho(current($request->get('minutasempenho')));
        }

        if (isset($camposBaseadosCompraPNCP)) {
            $request->request->set('unidadecompra_id', $camposBaseadosCompraPNCP['unidade_origem_id']);
            $request->request->set('modalidade_id', $camposBaseadosCompraPNCP['modalidade_id']);
            $request->request->set('licitacao_numero', $camposBaseadosCompraPNCP['compra_numero_ano']);
        }
    }

    /**
     * Caso tenha empenho preenchido utilizar os campos de unidade, modalidade e numero da licitacao de acordo
     * com a compra da minuta de empenho descartando os valores inseridos pelo usuário
     * @param FormRequest $request The FormRequest object.
     * @return void
     */
    public function setCamposComprasContrataMaisBr(FormRequest $request): void
    {
        if ($request->get('contrata_mais_brasil') == '1') {
            $camposBaseadosCompraPNCP = $this->buscarCamposBaseadosCompraPNCP($request->get('numero_contratacao'));
            if (isset($camposBaseadosCompraPNCP)) {
                $request->request->set('unidadecompra_id', $camposBaseadosCompraPNCP['unidade_origem_id']);
                $request->request->set('modalidade_id', $camposBaseadosCompraPNCP['modalidade_id']);
                $request->request->set('licitacao_numero', $camposBaseadosCompraPNCP['compra_numero_ano']);
            }
        }
    }

    private function buscarCamposBaseadosEmpenho($idEmpenho)
    {
        return MinutaEmpenho::select(
            "compras.modalidade_id",
            "compras.unidade_origem_id",
            "compras.numero_ano as compra_numero_ano"
        )
            ->join('compras', 'compras.id', '=', 'minutaempenhos.compra_id')
            ->where('minutaempenhos.id', $idEmpenho)->firstOrFail()->toArray();
    }

    private function buscarCamposBaseadosCompraPNCP($numeroContratacao)
    {
        return Compra::select(
            "compras.modalidade_id",
            "compras.unidade_origem_id",
            "compras.numero_ano as compra_numero_ano"
        )
            ->where('numero_contratacao', '=', $numeroContratacao)->firstOrFail()->toArray();
    }

    /**
     * @param FormRequest $request The request object.
     * @return void
     */
    public function setCamposValores(FormRequest $request): void
    {
        $valor_parcela = $request->input('valor_parcela');
        $request->request->set('valor_parcela', $valor_parcela);

        $valor_global = $request->input('valor_global');
        $request->request->set('valor_global', $valor_global);
        $request->request->set('valor_inicial', $valor_global);
    }

    /**
     * @param FormRequest $request The request object.
     * @return void
     */
    public function setCampoAplicavelDecreto(FormRequest $request): void
    {
        if (!in_array($request->input('aplicavel_decreto'), ["0", "1"])) {
            $request->request->set('aplicavel_decreto', false);
        }
    }

    /**
     * @param UpdateRequest $request
     * @param $contrato_id
     * @param $unidadeId
     * @param $amparoslegais
     * @return void
     */
    public function processarVinculacoesContrato(FormRequest $request, $contrato_id, $unidadeId, $amparoslegais): void
    {
        // se não for contrato em elaboração
        if ($request->elaboracao === '0') {
            if (!empty($request->get('minutasempenho'))) {
                $this->vincularMinutaContratoHistorico($request->all(), $contrato_id, $unidadeId);
            }
            if (!empty($request->get('fornecedoresSubcontratados'))) {
                $this->vincularFornecedoresContratoHistorico($request->all(), $contrato_id);
            }
            // se for contrato em elaboração e tiver minuta
        } elseif (!empty($request->get('minutasempenho'))) {
            foreach ($request['minutasempenho'] as $minutaEmpenhoId) {
                $this->vincularContratoEmpenho($unidadeId, $minutaEmpenhoId, $contrato_id);
            }
        }
    }

    /**
     *  Ao gravar o contrato gravar as minutas para contratoHistorico na tabela pivot
     *
     * @param $request
     * @param $contratoId
     * @param $unidadeId
     */
    public function vincularMinutaContratoHistorico($request, $contratoId, $unidadeId): void
    {
        $contratoHistorico = Contratohistorico::where('contratohistorico.elaboracao', false)
            ->where('contrato_id', '=', $contratoId)->first();

        foreach ($request['minutasempenho'] as $minutaEmpenhoId) {
            // vincula os empenhos ao contrato historico
            $contratoHistorico->minutasempenho()->attach($minutaEmpenhoId);

            $this->vincularContratoEmpenho($unidadeId, $minutaEmpenhoId, $contratoId);
        }
        if ($contratoHistorico->cronograma_automatico) {
            $this->contratocronograma->inserirCronogramaFromHistorico($contratoHistorico);
            $this->contratocronograma->removeDuplicateCronogramas($contratoId, $contratoHistorico->id);
        }
    }

    /**
     * @param $unidadeId
     * @param $minutaEmpenhoId
     * @param $contratoId
     * @return void
     */
    public function vincularContratoEmpenho($unidadeId, $minutaEmpenhoId, $contratoId): void
    {
        $minutaEmpenho = MinutaEmpenho::select([
            'minutaempenhos.contrato_id',
            'minutaempenhos.fornecedor_empenho_id as fornecedor_id',
            'empenhos.id as empenho_id',
            'empenhos.unidade_id as unidade_id',
        ])
            ->join('saldo_contabil', 'saldo_contabil.id', '=', 'minutaempenhos.saldo_contabil_id')
            ->join('empenhos', function ($join) {
                $join->on('empenhos.numero', '=', 'minutaempenhos.mensagem_siafi')
                    ->on('empenhos.unidade_id', '=', 'saldo_contabil.unidade_id');
            })
            ->join('codigoitens as situacao', 'situacao.id', '=', 'minutaempenhos.situacao_id')
            ->join('codigoitens as tipo_empenhopor', 'tipo_empenhopor.id', '=', 'minutaempenhos.tipo_empenhopor_id')
            ->join('compras', 'compras.id', '=', 'minutaempenhos.compra_id')
            ->where('situacao.descricao', '=', 'EMPENHO EMITIDO')
            ->where('tipo_empenhopor.descricao', '=', 'Compra')
//            ->where('empenhos.unidade_id', '=', $unidadeId)
            ->where('minutaempenhos.id', '=', $minutaEmpenhoId)
            ->first();

        if ($minutaEmpenho != null) {
            Contratoempenho::updateOrCreate([
                'contrato_id' => $contratoId,
                'empenho_id' => $minutaEmpenho->empenho_id,
            ], [
                'fornecedor_id' => $minutaEmpenho->fornecedor_id,
                'unidadeempenho_id' => $minutaEmpenho->unidade_id,
                'automatico' => true,
            ]);
        }
    }

    /**
     *  Ao gravar o contrato gravar os fornecedores subcontratados para contratoHistorico na tabela pivot
     *
     * @param $request
     * @param $contrato_id
     */
    public function vincularFornecedoresContratoHistorico($request, $contrato_id): void
    {
        $contratoHistorico = Contratohistorico::where('contratohistorico.elaboracao', false)
            ->where('contrato_id', '=', $contrato_id)->first();

        // vincula os fornecedores subcontratados ao contrato historico
        foreach ($request['fornecedoresSubcontratados'] as $fornecedor_id) {
            $contratoHistorico->fornecedoresSubcontratados()->attach($fornecedor_id);
        }
    }

    public function controlaRegistrosPncpEntreEmpenhoEContrato(int $contrato_id, $retSituacao = 'RETPEN'): void
    {
        $contratoHistorico = Contratohistorico::where("contrato_id", $contrato_id)->latest()->first();

        if ($this->isContratoTipoEmpenho($contratoHistorico->tipo->descricao)) {
            $encontrouSequencialPNCP = $this->atualizarInformacaoPNCPContratoMinuta($contratoHistorico);
            $dadosEnviaPNCP = EnviaDadosPncp::where("contrato_id", $contrato_id)->latest()->first();
            $situacao = $this->recuperarIDSituacao($retSituacao);

            if ($encontrouSequencialPNCP) {
                $dadosEnviaPNCP->situacao = $situacao;
                $dadosEnviaPNCP->save();
            }

            # Se o contrato do tipo empenho tiver sido publicado no PNCP
            if (!$encontrouSequencialPNCP && !empty($dadosEnviaPNCP->sequencialPNCP)) {
                $enviaDadosPncpMinutaEmpenho =
                    EnviaDadosPncp::where("pncpable_id", $contratoHistorico->minutasempenho[0]->id)
                        ->where('pncpable_type', MinutaEmpenho::class)
                        ->first();

                $situacaoEnviaDadosPncpMinutaEmpenho = $enviaDadosPncpMinutaEmpenho->status->descres;

                # Atualizar o envia dados pncp da minuta de empenho se a situação for INCPEN
                if (empty($enviaDadosPncpMinutaEmpenho->sequencialPNCP) &&
                    $situacaoEnviaDadosPncpMinutaEmpenho == 'INCPEN'
                ) {
                    $enviaDadosPncpMinutaEmpenho->situacao = $situacao;
                    $enviaDadosPncpMinutaEmpenho->link_pncp = $dadosEnviaPNCP->link_pncp;
                    $enviaDadosPncpMinutaEmpenho->sequencialPNCP = $dadosEnviaPNCP->sequencialPNCP;
                    $enviaDadosPncpMinutaEmpenho->save();
                }
            }
        }
    }

    public function atualizarArquivosNoAtivar($contrato): void
    {
        $arquivos = $contrato->arquivos;
        sleep(1);
        foreach ($arquivos as $arquivo) {
            if ($arquivo->restrito == false) {
                $arquivo->envio_pncp_pendente = 'INCARQ';
            }
            $arquivo->contratohistorico_id = $contrato->historico()->first()->id;
            $arquivo->save();
        }
    }

    public function saveAbaEmpenhosContrato($contrato_id, $request, $user_id, $ativarElaboracaoBotaoExterno = false): void
    {


        $contratoEmpenhoExists = Contratoempenho::where('contrato_id', $contrato_id)->get();

        // $ativarElaboracaoBotaoExterno será true somente quando o contrato em elaboração for ativado pelo botão externo
        if ($ativarElaboracaoBotaoExterno === false) {
            // Deleta todas as relações de empenho com o contrato para recriá-las
            // para prencher corretamente quando for deletado do instrumento inicial
            if ($contratoEmpenhoExists->isNotEmpty()) {
                Contratoempenho::where('contrato_id', $contrato_id)->delete();
            }

            if (is_array($request->input('empenho')['numero_unidade']) &&
                !empty($request->input('empenho')['numero_unidade'])) {
                $arrayEmpenhoUnidades = $request->input('empenho')['numero_unidade'];
                $arrayFornecedor = $request->input('empenho')['numero_fornecedor'];
                $arrayNumeroEmpenho = $request->input('empenho')['numero_empenho'];

                foreach ($arrayEmpenhoUnidades as $keyEmpenho => $valueUnidadeId) {

                    if (!isset($arrayFornecedor[$keyEmpenho])) {
                        continue; // Pula caso os dados sejam inconsistentes
                    }
                    // Só salva como unidade descentralizada se a unidade for diferente da logada
                    if (session()->get('user_ug_id') != $valueUnidadeId && $request->input('elaboracao') != 1) {
                        $unidadeDescentralizada = Contratounidadedescentralizada::updateOrCreate(
                            [
                                'contrato_id' => $contrato_id,
                                'unidade_id' => $valueUnidadeId
                            ]
                        );
                    }

                    $empenho = Empenho::where('unidade_id', $valueUnidadeId)
                        ->where('numero', $arrayNumeroEmpenho[$keyEmpenho])
                        ->where('fornecedor_id', $arrayFornecedor[$keyEmpenho])
                        ->first();

                    if ($empenho) {

                        $contratoEmpenho = Contratoempenho::updateOrCreate(
                            [
                                'contrato_id' => $contrato_id,
                                'empenho_id' => $empenho->id,
                                'fornecedor_id' => $empenho->fornecedor_id,
                                'unidadeempenho_id' => $empenho->unidade_id,

                            ],
                            [
                                'user_id' => $user_id,
                                'automatico' => true
                            ]
                        );
                    }

                }
            }
        } else {
            foreach ($contratoEmpenhoExists as $contratoEmpenho) {
                if (session()->get('user_ug_id') != $contratoEmpenho->unidadeempenho_id) {
                    $unidadeDescentralizada = Contratounidadedescentralizada::updateOrCreate(
                        [
                            'contrato_id' => $contrato_id,
                            'unidade_id' => $contratoEmpenho->unidadeempenho_id
                        ]
                    );
                }
            }
        }


    }

    public function getContratoEmpenhoByContrato($contrato)
    {
        $arrayEmpenhoFormatado = [];
        $empenhos = $contrato->first()->empenhos;
        $minutasEmpenho = $contrato->first()->minutasempenho;

        foreach ($empenhos as $empenhoModel) {
            $empenhoFormatado = [
                'numero' => $empenhoModel->empenho->numero,
                'fornecedor_id' => $empenhoModel->empenho->fornecedor_id,
                'unidade_id' => $empenhoModel->empenho->unidade_id,
            ];

            // Busca a minuta relacionada ao número do empenho já salvo no contrato, se existir
            $minutaRelacionada = $minutasEmpenho->where('mensagem_siafi', $empenhoModel->empenho->numero)
                ->where('fornecedor_empenho_id', $empenhoModel->empenho->fornecedor_id)
                ->first();

            if ($minutaRelacionada) {
                $empenhoFormatado['minuta_id'] = $minutaRelacionada->id;
            }

            $arrayEmpenhoFormatado[] = $empenhoFormatado;
        }

        return response()->json(['empenhos' => $arrayEmpenhoFormatado]);
    }

    public function retornaTipos()
    {
        return Codigoitem::whereHas('codigo', function ($query) {
            $query->where('descricao', '=', 'Tipo de Contrato');
        })
            ->whereNotIn('descricao', config('app.tipo_contrato_not_in'))
            ->orderBy('descricao')
            ->pluck('descricao', 'id')
            ->toArray();
    }

    public function retornaCategorias()
    {
        return Codigoitem::whereHas('codigo', function ($query) {
            $query->where('descricao', '=', 'Categoria Contrato');
        })
            ->where('descricao', '<>', 'A definir')
            ->orderBy('descricao')
            ->pluck('descricao', 'id')
            ->toArray();
    }

    public function retornaCampoFornecedor($tab): array
    {
        return [
            'label' => "Fornecedor",
            'type' => "select2_from_ajax_single",
            'name' => 'fornecedor_id',
            'entity' => 'fornecedor',
            'attribute' => "cpf_cnpj_idgener",
            'attribute2' => "nome",
            'process_results_template' => 'gescon.process_results_fornecedor',
            'model' => "App\Models\Fornecedor",
            'data_source' => url("api/fornecedor"),
            'placeholder' => "Selecione o fornecedor",
            'minimum_input_length' => 2,
            'tab' => $tab
        ];
    }

    public function retornaCampoMinutasEmpenho($tab): array
    {
        return [
            'label' => 'Minutas de Empenho',
            'name' => 'minutasempenho',
            'placeholder' => 'Selecione minutas de empenho',
            'type' => 'select2_from_ajax_multiple_alias',
            'entity' => 'minutaempenho',
            'attribute' => 'nome_minuta_empenho',
            'model' => 'App\Models\MinutaEmpenho',
            'data_source' => url('api/minutaempenhoparacontrato'),
            'dependencies' => ['fornecedor_id'],
            'pivot' => true,
            'minimum_input_length' => 0,
            'wrapperAttributes' => [
                'title' => '{uasg} {modalidade} {numeroAno} - Nº do(s) Empenho(s) - Data de Emissão'
            ],
            'tab' => $tab,
        ];
    }

    public function retornaCampoSubcontratados($tab): array
    {
        return [
            // n-n relationship
            'label' => "Fornecedor(es) subcontratado(s)",
            'type' => "select2_from_ajax_multiple_single",
            'name' => 'fornecedoresSubcontratados',
            'entity' => 'fornecedoresSubcontratados',
            'attribute' => "cpf_cnpj_idgener",
            'attribute2' => "nome",
            'process_results_template' => 'gescon.process_results_multiple_fornecedor',
            'model' => "App\Models\Fornecedor",
            'data_source' => url("api/subcontratados"),
            'placeholder' => "Selecione o(s) Fornecedor(s)",
            'minimum_input_length' => 2,
            'tab' => $tab,
            'pivot' => true,
        ];
    }

    public function retornaCampoAssinatura($tab): array
    {
        return [
            'name' => 'data_assinatura',
            'label' => 'Data da Assinatura',
            'type' => 'date',
            'tab' => $tab,
            'modal_elaboracao' => true,
        ];
    }

    public function retornaCampoDataPublicacao($tab): array
    {
        return [
            'name' => 'data_publicacao',
            'label' => 'Data de Publicação no DOU',
            'type' => 'date',
            'tab' => $tab,
            'ico_help' => 'Data  igual ou anterior à data atual não gera extrato para o Diário Oficial da União. O envio para o PNCP de contratos com fundamento legal na Lei 14.133/2021 é automático após o seu registro e não depende do preenchimento deste campo.',
            'modal_elaboracao' => true,
        ];
    }

    public function retornaCampoAutoridades($unidade_id, $tab, $instrumentoInicial = false): array
    {
        $return = [
            // Autoridades Signatárias
            'label' => "Autoridades Signatárias",
            'type' => 'select2_multiple',
            'name' => 'contrato_autoridade_signataria',
            'entity' => 'contrato_autoridade_signataria',
            'attribute3' => 'titular',
            'attribute2' => 'cargo_autoridade_signataria',
            'attribute' => 'autoridade_signataria',
            'attribute_separator' => ' - ',
            'model' => Autoridadesignataria::class,
            'pivot' => true,
            'select_all' => true,
            'tab' => $tab,

            // LISTAR APENAS OS ATIVOS.
            'options' => (function ($query) use ($unidade_id) {
                return $query
                    ->select([
                        'autoridadesignataria.id',
                        'autoridadesignataria.titular',
                        'autoridadesignataria.autoridade_signataria',
                        'autoridadesignataria.cargo_autoridade_signataria'
                    ])
                    ->where('autoridadesignataria.unidade_id', $unidade_id)
                    ->where('autoridadesignataria.ativo', true)
                    ->orderBy('autoridade_signataria', 'ASC')
                    ->get();
            }),
        ];
        if (!$instrumentoInicial) {
            return $return + [
                    'attributes' => [
                        'id' => 'autoridades_signatarias',
                    ]];
        }

        return $return + [
                'default' => 'selected_all',
                'attributes' => [
                    'disabled' => 'disabled',
                    'id' => 'autoridades_signatarias',
                ]
            ];

    }

    public function retornaCampoObjeto($tab): array
    {
        return [
            'name' => 'objeto',
            'label' => 'Objeto',
            'type' => 'textarea_custom',
            'attributes' => [
                'onkeyup' => "maiuscula(this)"
            ],
            'tab' => $tab
        ];
    }

    public function retornaCampoInfoComplementar($tab): array
    {
        return [
            'name' => 'info_complementar',
            'label' => 'Informações Complementares',
            'type' => 'textarea_custom',
            'attributes' => [
                'onkeyup' => "maiuscula(this)"
            ],
            'tab' => $tab
        ];
    }

    public function retornaCampoModalidade($modalidades, $tab): array
    {
        return [
            'name' => 'modalidade_id',
            'label' => "Modalidade Compra",
            'type' => 'select2_from_array',
            'options' => $modalidades,
            'allows_null' => true,
            'tab' => $tab
        ];
    }

    public function retornaCampoAmparos($tab, $instrumentoInicial = false): array
    {
        $return = [
            'label' => 'Amparo Legal',
            'type' => 'select2_from_ajax_multiple_alias',
            'entity' => 'amparoslegais',
            'placeholder' => 'Selecione o Amparo Legal',
            'minimum_input_length' => 0,
            'data_source' => url('api/amparolegal'),
            'model' => 'App\Models\AmparoLegal',
            'dependencies' => ['modalidade_id'],
            'attribute' => 'campo_api_amparo',
            'pivot' => true,
            'tab' => $tab
        ];
        if (!$instrumentoInicial) {
            return $return + ['name' => 'amparoslegais'];
        }

        return $return + ['name' => 'amparolegal'];
    }

    public function retornaCampoContrataMaisBrasil($tab): array
    {
        return [
            'name' => 'contrata_mais_brasil',
            'label' => "Contrata+Brasil",
            'type' => 'radio',
            'options' => [
                1 => "Sim",
                0 => "Não"
            ],
            'attributes' => [
                'class' => 'opc_compra_brasil',
                'onclick' => 'toogleCompraBrasil($(this).val())'
            ],
            'default' => 0,
            'inline' => true,
            'tab' => $tab
        ];
    }

    public function retornaCampoContratacaoPNCP($tab): array
    {
        return [
            'name' => 'numero_contratacao',
            'label' => 'Id contratação PNCP',
            'type' => 'numcontratacaominuta',
            'attributes' => [
                'class' => 'form-control opc_mercado_gov',
            ],
            'wrapperAttributes' => [
                'id' => 'wrapper_numero_contratacao',
                'style' => "display: none;"
            ],
            'tab' => $tab
        ];
    }

    public function retornaCampoNumeroLicitacao($tab): array
    {
        return [
            'name' => 'licitacao_numero',
            'label' => 'Número Compra',
            'type' => 'numlicitacao',
            'tab' => $tab
        ];
    }

    public function retornaCampoUnidadeCompra($tab, $instrumentoInicial = false): array
    {
        return [
            'label' => "Unidade Compra",
            'type' => "select2_from_ajax_single",
            'name' => 'unidadecompra_id',
            'entity' => 'unidadecompra',
            'attribute' => "codigo",
            'attribute2' => "nomeresumido",
            'process_results_template' => 'gescon.process_results_unidade',
            'model' => "App\Models\Unidade",
            'ico_help' => 'Unidade Gerenciadora da Compra!',
            'data_source' => url("api/unidade"),
            'placeholder' => "Selecione a Unidade",
            'minimum_input_length' => 2,
            'tab' => $tab,
            'wrapperAttributes' => [
                'id' => 'wrapper_unidade_compra'
            ]
        ];
    }

    public function retornaCampoUnidadeBeneficiaria($tab, $instrumentoInicial = false): array
    {
        return [
            'label' => "Unidade Beneficiária",
            'type' => "select2_from_ajax_single",
            'name' => 'unidadebeneficiaria_id',
            'entity' => 'unidadebeneficiaria',
            'attribute' => "codigo",
            'attribute2' => "nomeresumido",
            'process_results_template' => 'gescon.process_results_unidade',
            'model' => "App\Models\Unidade",
            'ico_help' => 'Esse campo deverá ser preenchido apenas quando o saldo a ser consumido pertencer a uma UASG polo ou subordinada à UASG do usuário.',
            'data_source' => url("api/unidade"),
            'placeholder' => "Selecione a Unidade",
            'minimum_input_length' => 2,
            'tab' => $tab,
            'wrapperAttributes' => [
                'id' => 'wrapper_unidade_beneficiaria'
            ]
        ];
    }

    public function retornaCampoReceitaDespesa($tab): array
    {
        return [
            'name' => 'receita_despesa',
            'label' => "Receita / Despesa",
            'type' => 'select_from_array',
            'options' => [
                'D' => 'Despesa',
                'R' => 'Receita',
                'S' => 'Sem ônus',
                //'X'    => 'Receita e Despesa',
            ],
            'default' => 'D',
            'allows_null' => false,
            'tab' => $tab
        ];
    }

    public function retornaCampoTipo($tab): array
    {
        return [
            'name' => 'tipo_id',
            'label' => "Tipo",
            'type' => 'select2_from_array',
            'options' => $this->retornaTipos(),
            'allows_null' => true,
            'tab' => $tab,
            'attributes' => [
                'id' => 'tipo_contrato',
                'onchange' => "toogleAplicavelDecreto(this)",
            ],
        ];
    }

    public function retornaCampoCronograma($tab): array
    {
        return [
            'name' => 'cronograma_automatico',
            'label' => "Deseja gerar cronograma automático para o Contrato?",
            'type' => 'radio',
            'options' => [
                1 => "Sim",
                0 => "Não"
            ],
            'default' => 1,
            'inline' => true,
            'tab' => $tab
        ];
    }

    public function retornaCampoSubTipo($tab): array
    {
        return [
            'name' => 'subtipo',
            'label' => 'Subtipo',
            'type' => 'textarea',
            'attributes' => [
                'onkeyup' => "maiuscula(this)"
            ],
            'tab' => $tab
        ];
    }

    public function retornaCampoCategoria($tab): array
    {
        return [
            'name' => 'categoria_id',
            'label' => "Categoria",
            'type' => 'select2_from_array',
            'options' => $this->retornaCategorias(),
            'allows_null' => true,
            'tab' => $tab,
            'attributes' => [
                'onchange' => "toogleAplicavelDecreto(this)",
            ],
        ];
    }

    public function retornaCampoAplicavelDecreto($tab): array
    {
        return [
            'name' => 'aplicavel_decreto',
            'label' => "Aplicável o Decreto 11.430/2023 à contratação?",
            'type' => 'radio',
            'options' => [
                1 => "Sim",
                0 => "Não"
            ],
            'default' => 2,
            'inline' => true,
            'tab' => $tab,
            'ico_help' => 'Decreto aplicável para contratação de serviços contínuos com regime de dedicação exclusiva de '
                . 'mão de obra, com quantitativos mínimos de vinte e cinco colaboradores, que tenha havido previsão em '
                . 'edital de licitação ou aviso de contratação direta e cujo Estado possua acordo de cooperação técnica '
                . 'firmado com o Ministério da Gestão e da Inovação em Serviços Públicos e o Ministério das Mulheres.'
        ];
    }

    public function retornaCampoSubCategoria($tab): array
    {
        return [
            'name' => 'subcategoria_id',
            'label' => "Subcategoria",
            'type' => 'select2_from_ajax_single',
            'model' => 'App\Models\OrgaoSubcategoria',
            'entity' => 'orgaosubcategoria',
            'attribute' => 'descricao',
            'data_source' => url('api/orgaosubcategoria'),
            'placeholder' => 'Selecione...',
            'minimum_input_length' => 0,
            'dependencies' => ['categoria_id'],
            'method' => 'GET',
            'tab' => $tab
        ];
    }

    public function retornaCampoNumeroContrato($tab): array
    {
        return [
            'name' => 'numero',
            'label' => 'Contrato',
            'type' => 'numcontrato',
            'tab' => $tab,
            'modal_elaboracao' => true,
        ];
    }

    public function retornaCampoCodigoSistemaExterno($tab): array
    {
        return [
            'name' => 'codigo_sistema_externo',
            'label' => 'Código Sistema Externo',
            'type' => 'codsistemaexterno',
            'tab' => $tab,
            'ico_help' => 'Preencher com código do contrato em sistema externo integrado'
        ];
    }

    public function retornaCampoProcesso($tab): array
    {
        return [
            'name' => 'processo',
            'label' => 'Número Processo',
            'type' => 'numprocesso',
            'tab' => $tab
        ];
    }
}
