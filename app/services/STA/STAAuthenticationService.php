<?php

namespace App\services\STA;

use GuzzleHttp\Client;

class STAAuthenticationService
{
    public function getToken(): string
    {
        $dadosAutenticacao = [
            'username' => env('API_STA_USER'),
            'password' => env('API_STA_PASS')
        ];

        $urlAutenticacao = env('API_STA_HOST') . config('rotas-sta.login');

        $client = new Client();

        $resposta = $client->post($urlAutenticacao, [
            'json' => $dadosAutenticacao,
        ]);

        $corpoResposta = json_decode($resposta->getBody(), true);

        if (isset($corpoResposta['access_token'])) {
            return $corpoResposta['access_token'];
        }

        return '';
    }
}
