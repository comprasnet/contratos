<?php

namespace App\services\STA;

use App\Http\Resources\OrdemBancaria\OrdemBancariaApropriacaoCollection;
use App\Http\Traits\Formatador;
use App\Models\ApropriacaoContratoFaturas;
use App\Models\DomicilioBancario;
use App\Models\Contrato;
use App\Models\Empenho;
use App\Models\Fornecedor;
use App\Models\OrdemBancaria;
use App\Models\Unidade;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;

class STAOrdemBancariaService
{
    use Formatador;

    const CONSULTA_API = 'Consulta_API';

    public function recuperaCriaOrdensBancariasPorDocumento($documento, $url, $sfpadrao_id = null, $consulta_api = null, $instrumento_cobranca_id = null)
    {
        $tempoCriacao = $this->verificaCriacaoOrdemBancaria($documento, $sfpadrao_id, $instrumento_cobranca_id);
        $ordensBancarias = $this->recuperaOrdemBancaria($documento, $sfpadrao_id, $consulta_api, $instrumento_cobranca_id);

        $verificaCadastro = (sizeof($ordensBancarias) == 0 && ($tempoCriacao >= 1 || $tempoCriacao === null)) ||
            (sizeof($ordensBancarias) != 0 && $tempoCriacao >= 1);

        if ($verificaCadastro) {
            $ordensBancarias = $this->cadastraOrdemBancaria($documento, $url, $sfpadrao_id, $consulta_api, $instrumento_cobranca_id);
        } else {
            $ordensBancarias = $this->recuperaOrdemBancaria($documento, $sfpadrao_id, $consulta_api, $instrumento_cobranca_id);
        }

        if (sizeof($ordensBancarias) == 0) {
            throw new \Exception('Não foi localizada nenhuma ordem bancária para esta apropriação.', Response::HTTP_NOT_FOUND);
        }

        return $ordensBancarias;
    }

    public function retornaUnidadeId($documento)
    {
        $ug = substr(trim($documento), 0, 6);
        $gestao = substr(trim($documento), 6, 5);

        $unidade = Unidade::where('gestao', $gestao)
        ->where('codigo', str_pad($ug, 6, "0", STR_PAD_LEFT))
        ->first();

        if (!$unidade) {
            throw new \Exception("Unidade ug {$ug} e gestão {$gestao} não encontrada");
        }

        return $unidade->id;
    }

    public function verificaCriacaoOrdemBancaria($documento, $sfpadrao_id = null, $instrumento_cobranca_id = null)
    {

        $documentoOrigem = substr(trim($documento), 11, 12);
        $unidadeId = $this->retornaUnidadeId($documento);

        $ordemBancariaAtualizar = $this->retornaOrdemBancariaAtualizar($documentoOrigem, $unidadeId, $sfpadrao_id, $instrumento_cobranca_id);

        if ($ordemBancariaAtualizar) {
            // Calcula a diferença em dias
            $diasCriacao = Carbon::parse($ordemBancariaAtualizar->updated_at)->diffInMinutes(Carbon::now());
        }

        return $diasCriacao ?? null;
    }

    public function recuperaOrdemBancaria($documento, $sfpadrao_id = null, $consulta_api = null, $instrumento_cobranca_id = null)
    {
        $documentoOrigem = substr(trim($documento), 11, 12);
        $unidadeId = $this->retornaUnidadeId($documento);

        $ordemBancaria = OrdemBancaria::where([
            ['documentoorigem', $documentoOrigem],
            ['unidade_id', $unidadeId],
            ['sfpadrao_id', $sfpadrao_id],
            ['contratofaturas_id', $instrumento_cobranca_id],
        ])
            ->with('domicilioBancario:id,banco,conta,agencia')
            ->select([
                'cancelamentoob',
                'documentoorigem',
                'emissao',
                'numero',
                'numeroobcancelamento',
                'observacao',
                'processo',
                'valor',
                'numero',
                'domicilio_bancario_id'
            ])
            ->get();

        if ($consulta_api == self::CONSULTA_API) {
            return new OrdemBancariaApropriacaoCollection($ordemBancaria);
        }

        $agrupados = [];
        foreach ($ordemBancaria as $ob) {
            $key = $ob->domicilioBancario->conta . '-' . $ob->domicilioBancario->agencia . '-' . $ob->domicilioBancario->banco;

            if (!isset($agrupados[$key])) {
                $agrupados[$key] = [
                    'contadestino' => $ob->domicilioBancario->conta,
                    'agenciadestino' => $ob->domicilioBancario->agencia,
                    'bancodestino' => $ob->domicilioBancario->banco,
                    'ordensBancarias' => []
                ];
            }
            $agrupados[$key]['ordensBancarias'][] = $ob;
        }

        return $agrupados;
    }

    public function cadastraOrdemBancaria($documento, $url, $sfpadrao_id = null, $consulta_api = null, $instrumento_cobranca_id = null)
    {
        $host = env('API_STA_HOST');
        $url = $host . $this->replaceRouteParam($url, ['documento' => $documento]);

        $urlFetchService = new STAUrlFetcherService($url);
        $dados = $urlFetchService->fetchDataJson();

        $tipoFornecedor = ['1' => 'FISICA', '2' => 'JURIDICA'];
        if (sizeof($dados) > 0) {
            foreach ($dados as $dado) {

                $unidade = $this->retornaUnidade($dado);
                $fornecedor = $this->retornaCriaFornecedor($dado, $tipoFornecedor);

                $ordemBancaria = $this->criaOrdemBancaria($dado, $sfpadrao_id, $unidade, $fornecedor, $instrumento_cobranca_id);
                $this->criarEmpenhoOBxNE($ordemBancaria, $dado, $unidade);
            }
        }

        return $this->recuperaOrdemBancaria($documento, $sfpadrao_id, $consulta_api, $instrumento_cobranca_id);
    }

    private function criaOrdemBancaria($dado, $sfpadrao_id = null, $unidade, $fornecedor, $instrumento_cobranca_id = null)
    {
        $domicio_bancario = DomicilioBancario::firstOrCreate(
            [
                'banco' => $dado['banco'],
                'conta' => $dado['conta'],
                'agencia' => $dado['agencia'],
            ]
        );

        return OrdemBancaria::updateOrCreate([
            'sfpadrao_id' => $sfpadrao_id,
            'contratofaturas_id' => $instrumento_cobranca_id,
            'unidade_id' => $unidade->id,
            'numero' => $dado['numero'],
            'emissao' => $dado['emissao'],
            'documentoorigem' => $dado['documentoorigem']
        ],
            [
                'fornecedor_id' => $fornecedor->id,
                'observacao' => $dado['observacao'],
                'tipo_ob' => $dado['tipoob'],
                'processo' => $dado['processo'],
                'valor' => $dado['valor'],
                'cancelamentoob' => $dado['cancelamentoob'],
                'numeroobcancelamento' => $dado['numeroobcancelamento'],
                'domicilio_bancario_id' => $domicio_bancario->id,
                'updated_at' => now()
            ]
        );
    }

    private function criarEmpenhoOBxNE($ordemBancaria, $dado, $unidade)
    {
        if (isset($dado['empenhos']) && is_array($dado['empenhos'])) {
            foreach ($dado['empenhos'] as $numeroEmpenho) {

                $empenhoId = Empenho::buscaNumeroIdEmpenhoUg($unidade->id, substr($numeroEmpenho, 11));

                $ordemBancaria->obxne()->firstOrCreate([
                    'numeroempenho' => $numeroEmpenho,
                    'empenho_id' => $empenhoId
                ]);
            }
        }
    }

    private function retornaUnidade($dado)
    {
        return Unidade::where('gestao', trim($dado['gestao']))
            ->where('codigo', trim($dado['ug']))
            ->first();
    }

    private function retornaCriaFornecedor($dado, $tipoFornecedor)
    {
        $fornecedor = Fornecedor::where('cpf_cnpj_idgener', $dado['favorecidocodigo'])
            ->first();

        if (!$fornecedor) {
            $fornecedor = Fornecedor::create([
                'nome' => $dado['favorecidonome'],
                'cpf_cnpj_idgener' => trim($dado['favorecidocodigo']),
                'tipo_fornecedor' => $tipoFornecedor[$dado['tipofavorecido']]
            ]);
        }

        return $fornecedor;
    }

    public function retornaTipoConsulta(): string
    {
        return self::CONSULTA_API;
    }

    public function retornaNumeroFaturaOB(int $id_apropriacao, $permissaoFatura = true)
    {
        $sfPadrao = ApropriacaoContratoFaturas::where('apropriacoes_faturas_id', $id_apropriacao)
            ->first();

        if (!$sfPadrao) {
            throw new \Exception("Apropriação não encontrada.", Response::HTTP_NOT_FOUND);
        }

        $sfPadrao = $sfPadrao->sfpadrao()->first();

        $codigosSecundarias = backpack_user()->unidadesSecundarias();
        $unidadesUsuario = array_merge($codigosSecundarias, [backpack_user()->ugPrimariaRelation()->first()->codigo]);

        if ($permissaoFatura) {
            $possuiPermissao = in_array(str_pad($sfPadrao->codugemit, 6, "0", STR_PAD_LEFT), $unidadesUsuario);
            if (!$possuiPermissao) {
                throw new \Exception('Usuário sem permissão no sistema', Response::HTTP_FORBIDDEN);
            }
        }

        if (!$sfPadrao->numdh) {
            throw new \Exception("A apropriação não possui número dh.", Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $unidade = Unidade::where('codigo', str_pad($sfPadrao->codugemit, 6, "0", STR_PAD_LEFT))->first();

        if (!$unidade) {
            throw new \Exception("Unidade {$sfPadrao->codugemit} não encontrada", Response::HTTP_NOT_FOUND);
        }

        $documento = $unidade->codigo .
            $unidade->gestao .
            $sfPadrao->anodh .
            $sfPadrao->codtipodh .
            str_pad($sfPadrao->numdh, 6, "0", STR_PAD_LEFT);

        return $this->recuperaCriaOrdensBancariasPorDocumento($documento,
            config('rotas-sta.ordembancaria-por-documento'),
            $sfPadrao->id,
            $this->retornaTipoConsulta());
    }

    public function retornaNumeroFaturaOBIntrumentoCobranca(array $instrumento_cobranca_doc)
    {
        $documento = $instrumento_cobranca_doc['codigo'] .
            $instrumento_cobranca_doc['gestao'] .
            date('Y', strtotime($instrumento_cobranca_doc['emissao'])) .
            'IC' .
            str_pad($instrumento_cobranca_doc['id_inst_cobranca'], 6, "0", STR_PAD_LEFT);

        return $this->recuperaCriaOrdensBancariasPorDocumento($documento,
            config('rotas-sta.ordembancaria-por-documento'),
            null,
            $this->retornaTipoConsulta(),
            $instrumento_cobranca_doc['id_inst_cobranca']);
    }

    public function retornaContratoIds($request, $unidade_id = null)
    {
        $contratoIds = Contrato::whereHas('faturas')
            ->with([
                'faturas:id,contrato_id,situacao,numero,sfpadrao_id,emissao',
                'faturas.apropriacaoContratoFaturasApropriacao.apropriacao:id,fase_id',
                'faturas.apropriacaoContratoFaturasApropriacao.sfpadrao:id,anodh,codtipodh,numdh,situacao'

            ])
            ->when($request->antecipa_gov, function ($query) {
                return $query->whereHas('antecipagovs', function ($subQuery) {
                    $subQuery->whereNotNull('contrato_antecipagov.contrato_id');
                });
            })
            ->when($unidade_id, function ($query, $unidade_id) {
                return $query->where('contratos.unidade_id', (int)$unidade_id);
            })
            ->when($request->id_contrato, function ($query, $id_contrato) {
                return $query->where('contratos.id', (int)$id_contrato);
            })
            ->when($request->numero_contrato, function ($query, $numero_contrato) {
                $numero_contrato_sem_ano = explode('/', trim($numero_contrato))[0];
                return $query->whereRaw("SPLIT_PART(contratos.numero, '/', 1) = ?", [$numero_contrato_sem_ano]);
            })
            ->when($request->ano_contrato, function ($query, $ano_contrato) {
                return $query->whereRaw("SPLIT_PART(contratos.numero, '/', 2) = ?", [trim($ano_contrato)]);
            })
            ->select(['contratos.id', 'contratos.numero', 'contratos.unidade_id'])
            ->get();

        if (empty($contratoIds)) {
            return response()->json([
                "status" => "error",
                "message" => "Nenhum contrato encontrado para a unidade fornecida.",
            ], Response::HTTP_NOT_FOUND);
        }

        return $contratoIds;
    }

#------------------------------------------- FIM ORDEM BANCÁRIA INDIVIDUAL --------------------------------------------

#------------------------------------------- ORDEM BANCÁRIA POR LOTE --------------------------------------------------

    public function cadastraOrdemBancariaPorLote(array $documentos)
    {
        $host = env('API_STA_HOST');
        $url = $host . config('rotas-sta.ordembancaria-por-lote-documentos');

        $body = [
            'documentos' => $documentos
        ];

        //$documentosFiltrados = $this->verificaBuscaOrdemBancariaLote($documentos);
        $retornaSfPadraoInstCobranca = [];

        if (!empty($documentos)) {

            $urlFetchService = new STAUrlFetcherService($url, $body);
            $dados = $urlFetchService->fetchDataJson();

            $tipoFornecedor = ['1' => 'FISICA', '2' => 'JURIDICA'];

            if (isset($dados) && sizeof($dados) > 0) {
                foreach ($dados as $dadoOB) {

                    foreach ($dadoOB as $dado) {
                        $unidade = $this->retornaUnidade($dado);

                        $fornecedor = $this->retornaCriaFornecedor($dado, $tipoFornecedor);

                        $ordemBancaria = $this->criaOrdemBancaria($dado, $dado['sfpadrao'] ?? null, $unidade, $fornecedor, $dado['id_inst_cobranca'] ?? null);
                        $this->criarEmpenhoOBxNE($ordemBancaria, $dado, $unidade);
                        if (!empty($ordemBancaria->id)) {
                            $retornaSfPadraoInstCobranca[] = $dado['id_apropriacao'] ?? $dado['id_inst_cobranca'];
                        }
                    }
                }
                return array_unique($retornaSfPadraoInstCobranca);
            }
        }
        return $retornaSfPadraoInstCobranca;
    }

    public function retornaNumeroFaturaOBLote(array $apropriacoes)
    {

        $documentoLotes = [];

        foreach ($apropriacoes as $apropriacao) {

            $sfPadrao = ApropriacaoContratoFaturas::where('apropriacoes_faturas_id', $apropriacao['id_apropriacao'])
                ->first();

            $unidade = Unidade::where('codigo', str_pad($sfPadrao->sfpadrao()->first()->codugemit, 6, "0", STR_PAD_LEFT))->first();

            if (!$unidade) {
                continue;
            }

            $documentoLotes[] = [
                'documentoOrigem' => $unidade->codigo .
                    $unidade->gestao .
                    $sfPadrao->sfpadrao()->first()->anodh .
                    $sfPadrao->sfpadrao()->first()->codtipodh .
                    str_pad($sfPadrao->sfpadrao()->first()->numdh, 6, "0", STR_PAD_LEFT),
                'id_sfpadrao' => $apropriacao['id_sfpadrao'],
                'id_apropriacao' => $apropriacao['id_apropriacao']
            ];
        }

        return  $this->cadastraOrdemBancariaPorLote($documentoLotes);
    }

    /**
     * @param $documentoOrigem
     * @param $unidadeId
     * @param $sfpadrao_id
     * @param $instrumento_cobranca_id
     * @return OrdemBancaria
     */
    public function retornaOrdemBancariaAtualizar($documentoOrigem, $unidadeId, $sfpadrao_id, $instrumento_cobranca_id)
    {
        $ordemBancariaAtualizar = OrdemBancaria::where([
            ['documentoorigem', $documentoOrigem],
            ['unidade_id', $unidadeId],
            ['sfpadrao_id', $sfpadrao_id],
            ['contratofaturas_id', $instrumento_cobranca_id],
        ])
            ->select([
                'created_at',
                'updated_at'
            ])
            ->oldest()
            ->first();
        return $ordemBancariaAtualizar;
    }

    public function verificaBuscaOrdemBancariaLote(array $documentos)
    {
        return array_filter($documentos, function ($documento) {
            $documentoOrigem = substr(trim($documento['documentoOrigem']), 11, 12);
            $unidadeId = $this->retornaUnidadeId($documento['documentoOrigem']);

            $ordemBancariaAtualizar = $this->retornaOrdemBancariaAtualizar(
                $documentoOrigem,
                $unidadeId,
                $documento['id_sfpadrao'] ?? null,
                $documento['id_inst_cobranca'] ?? null
            );

            if ($ordemBancariaAtualizar) {
                // Calcula a diferença em dias
                $diasCriacao = Carbon::parse($ordemBancariaAtualizar->updated_at)->diffInDays(Carbon::now());
                return $diasCriacao !== 0;
            }
            return true;
        });
    }

}
