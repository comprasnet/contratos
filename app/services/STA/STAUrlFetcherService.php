<?php

namespace App\services\STA;

use App\services\STA\STAAuthenticationService;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\JsonResponse;

class STAUrlFetcherService
{
    protected $client;
    protected $authenticationService;
    protected $url;
    protected $body;

    public function __construct(string $url, array $body = [])
    {
        $this->url = $url;
        $this->body = $body;

        $this->client = new Client([
            'verify' => false,
            'timeout' => 99999,
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.9) Gecko/20071025 Firefox/3.0.0.1'
            ]
        ]);

        $this->authenticationService = new STAAuthenticationService();
    }

    /**
     * Retorna os dados brutos da resposta da requisição.
     *
     * @return string Os dados brutos da resposta da requisição.
     */
    public function fetchDataRaw()
    {
        $responseData = $this->fetchData();

        return $responseData->getBody()->getContents();
    }

    /**
     * Retorna os dados da resposta da requisição como um array JSON.
     * Se houver um erro na resposta, o array JSON conterá informações sobre o erro.
     *
     * @return array|string Os dados da resposta da requisição como um array JSON ou uma string com informações de erro.
     */
    public function fetchDataJson()
    {
        $responseData = $this->fetchData();
        if ($this->isError($responseData)) {
            return $responseData;
        }

        $content = $responseData->getBody()->getContents();

        return json_decode($content, true);

    }

    /**
     * Realiza a requisição HTTP e retorna a resposta.
     * Se ocorrer um erro na requisição, uma resposta de erro é retornada.
     *
     * @return mixed Retorna a resposta da requisição ou uma resposta de erro em formato JSON
     */
    private function fetchData()
    {
        try {
            $token = $this->authenticationService->getToken();

            $options = [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                    'Content-Type' => 'application/json',
                ]
            ];

            if (empty($this->body)) {
                $response = $this->client->get($this->url, $options);
            } else {

                $options['json'] = $this->body;
                $response = $this->client->post($this->url, $options);

            }

            return $response;
        } catch (RequestException $e) {
            // Iniciando como 401, pois se não houver retorno, é erro de certificado
            $statusCode = 401;

            if($e->getResponse() === null){
                return response()->json([
                    'error_code' => $statusCode,
                    'error_message' => $e->getMessage()
                ], $statusCode);
            }

            $statusCode = $e->getResponse()->getStatusCode();

            # Verifica se ocorreu um erro de autenticação (código 401)
            if ($statusCode === 401) {
                $content = $e->getResponse()->getBody()->getContents();

                $decodedContent = json_decode($content, true);

                # Se o conteúdo decodificado não for nulo e tiver a chave "status",
                # então é um erro de autenticação que precisa ser tratado
                if ($decodedContent !== null && isset($decodedContent['status'])) {
                    return response()->json([
                        'error_code' => $statusCode,
                        'error_message' => $decodedContent['status']
                    ], $statusCode);
                }
            }

            $error_message = ($statusCode === 500) ? 'Erro no servidor externo.' : $e->getMessage();

            # Se não for um erro de autenticação, retorna um erro genérico
            return response()->json([
                'error_code' => $statusCode,
                'error_message' => 'Erro ao fazer a requisição: ' . $error_message
            ], $statusCode);
        }
    }

    /**
     * Verifica se a resposta da requisição é um erro.
     * Os erros são colocados em um formato JSON com padrão: error_code e error_message.
     *
     * @param mixed $responseData A resposta da requisição.
     * @return bool True se a resposta é um erro, caso contrário False.
     */
    public function isError($responseData): bool
    {
        if ($responseData instanceof JsonResponse) {
            $data = $responseData->getData();
            # Se tem o índice error_code
            return isset($data->error_code);
        }

        return false;
    }

    /**
     * Retorna os detalhes do erro contidos na resposta.
     *
     * @param mixed $responseData A resposta da requisição.
     * @return mixed Os detalhes do erro.
     */
    public function getError($responseData)
    {
        $error = $responseData->getData();

        // Verifica se o atributo 'error_message' existe no array $error
        if (isset($error->error_message)) {
            // Aplica json_encode apenas ao atributo 'error_message'
            $error->error_message = json_encode($error->error_message);
        }

        return $error;
    }

}
