<?php

namespace App\services\Fornecedor;

use App\Http\Controllers\RFB;
use App\Http\Controllers\SICAF;
use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\Formatador;
use App\Models\Codigo;
use App\Models\Codigoitem;
use App\Models\Fornecedor;
use App\Models\FornecedorContratoHistorico;
use App\Models\FornecedorSicaf;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class FornecedorSicafService
{
    use Formatador;
    use BuscaCodigoItens;

    protected $sicafApi;

    public function __construct(SICAF $sicafApi)
    {
        $this->sicafApi = $sicafApi;
    }

    /**
     * @throws GuzzleException
     */
    public function consultaSicaf(Fornecedor $fornecedor)
    {

        $cnpj = $fornecedor->cpf_cnpj_idgener;

        $sicafApi = new SICAF();
        $sicaf = $sicafApi->dadosSituacaoNivel($this->retornaSomenteNumeros($cnpj));
        $sicafDadosBasicos = $sicafApi->dadosBasicos($this->retornaSomenteNumeros($cnpj));


        /** @var $fornecedorSicaf FornecedorSicaf */
        $fornecedorSicaf = FornecedorSicaf::where('fornecedor_id', $fornecedor->id)->first();

        $today = date('Y-m-d');


        $data = [
            'fornecedor_id' => $fornecedor->id,
            'date_receita_federal_pgfn' => $sicaf->situacaoNivel3->dadosReceitaFederalRFB->dataValidade ?? null,
            'validade_receita_federal_pgfn' => (
                    !$sicaf->situacaoNivel3->possuiPendenciaCadastral &&
                    (
                        $sicaf->situacaoNivel3->dadosReceitaFederalRFB->dataValidade === null ||
                        $sicaf->situacaoNivel3->dadosReceitaFederalRFB->dataValidade > $today
                    )
                ) ?? false,
            'date_certidao_fgts' => $sicaf->situacaoNivel3->dadosFGTS->dataValidade ?? null,
            'validade_certidao_fgts' => (
                    !$sicaf->situacaoNivel3->possuiPendenciaCadastral &&
                    (
                        $sicaf->situacaoNivel3->dadosFGTS->dataValidade === null ||
                        $sicaf->situacaoNivel3->dadosFGTS->dataValidade > $today
                    )
                ) ?? false,
            'date_certidao_trabalhista' => $sicaf->situacaoNivel3->dadosTrabalhista->dataValidade ?? null,
            'validade_certidao_trabalhista' => (
                    !$sicaf->situacaoNivel3->possuiPendenciaCadastral &&
                    (
                        $sicaf->situacaoNivel3->dadosTrabalhista->dataValidade === null ||
                        $sicaf->situacaoNivel3->dadosTrabalhista->dataValidade > $today
                    )
                ) ?? false,
            'date_receita_estadual_distrital' => $sicaf->situacaoNivel4->dadosCertidaoEstadual->dataValidade ?? null,
            'validate_receita_estadual_distrital' => (
                    !$sicaf->situacaoNivel4->possuiPendenciaCadastral &&
                    (
                        $sicaf->situacaoNivel4->dadosCertidaoEstadual->dataValidade === null ||
                        $sicaf->situacaoNivel4->dadosCertidaoEstadual->dataValidade > $today
                    )
                ) ?? false,
            'date_receita_municipal' => $sicaf->situacaoNivel4->dadosCertidaoMunicipal->dataValidade ?? null,
            'validade_receita_municipal' => (
                    !$sicaf->situacaoNivel4->possuiPendenciaCadastral &&
                    (
                        $sicaf->situacaoNivel4->dadosCertidaoMunicipal->dataValidade === null ||
                        $sicaf->situacaoNivel4->dadosCertidaoMunicipal->dataValidade > $today
                    )
                ) ?? false,
            'mei' => $sicafDadosBasicos->mei ?? null,
            'porte' => $this->retornaIdCodigoItem($sicafDadosBasicos->porte->codigo) ?? null,
            'nome_fantasia' => $sicafDadosBasicos->nomeFantasia ?? null,
            'data_vencimento_credenciamento' => $sicafDadosBasicos->dataVencimentoCredenciamento ?? null,
            'situacao_fornecedor' => $sicafDadosBasicos->situacaoFornecedor->codigo ?? null,
            'natureza_juridica' => $this->getNaturezaJurifica($sicafDadosBasicos->naturezaJuridica->descricao) ?? null,
            'origem_sistema' => $this->retornaIdCodigoItemPorDescricaoEDescres('Tipo Sistema Origem', 'ORIGEM1') ?? $this->retornaIdCodigoItemPorDescricaoEDescres('Tipo Sistema Origem', 'ORIGEM3')
        ];

        if ($fornecedorSicaf) {
            $fornecedorSicaf->fill($data);
            $fornecedorSicaf->save();
        } else {
            $fornecedorSicaf = FornecedorSicaf::create($data);
        }
        if ($sicaf->nome != $fornecedor->nome) {
            Fornecedor::where('id', $fornecedor->id)->first()
                ->update(['nome' => $sicaf->nome]);
        }
        return $fornecedorSicaf;
    }

    /**
     * @param $descresCodItem
     * @return \Illuminate\Database\Eloquent\HigherOrderBuilderProxy|int|mixed
     */
    public function retornaIdCodigoItem($descresCodItem)
    {
        $porteEmpresa = [1 => 'PORT01', 3 => 'PORT03', 5 => 'PORT05'];
        return $this->retornaIdCodigoItemPorDescricaoEDescres('Tipo Porte Empresa', $porteEmpresa[(int)$descresCodItem]);
    }

    /**
     * @param $descricao
     * @return int|mixed
     */
    public function getNaturezaJurifica($descricao)
    {
        $codigoId = Codigo::where('descricao', 'Tipo Administração')->select('id')->first();

        $codigoItem = Codigoitem::firstOrCreate([
            'codigo_id' => $codigoId->id,
            'descres' => Str::slug(Str::limit($descricao, 20, '')),
            'descricao' => $descricao,
            'visivel' => true
        ]);
        return $codigoItem->id;
    }

    public function consultaRFB(Fornecedor $fornecedor)
    {
        $cnpj = $fornecedor->cpf_cnpj_idgener;

        $rfb = new RFB(isset(backpack_user()->cpf) ? $this->retornaSomenteNumeros(backpack_user()->cpf) : null);
        $consultaCnpjReceita = $rfb->dadosCompletosEmpresa($this->retornaSomenteNumeros($cnpj));

        $fornecedorSicaf = FornecedorSicaf::where('fornecedor_id', $fornecedor->id)->first();

        $mei = $consultaCnpjReceita->informacoesAdicionais->optanteMei == 'Sim' ? true : false;
        $data = [
            'fornecedor_id' => $fornecedor->id,
            'mei' => $mei ?? null,
            'porte' => $this->retornaIdCodigoItem($consultaCnpjReceita->porte) ?? null,
            'nome_fantasia' => $consultaCnpjReceita->nomeFantasia,
            'data_vencimento_credenciamento' => $consultaCnpjReceita->situacaoCadastral->data,
            'situacao_fornecedor' => $consultaCnpjReceita->situacaoCadastral->codigo,
            'natureza_juridica' => $this->getNaturezaJurifica($consultaCnpjReceita->naturezaJuridica->descricao) ?? null,
            'origem_sistema' => $this->retornaIdCodigoItemPorDescricaoEDescres('Tipo Sistema Origem', 'ORIGEM2') ?? $this->retornaIdCodigoItemPorDescricaoEDescres('Tipo Sistema Origem', 'ORIGEM3')
        ];

        if ($fornecedorSicaf) {
            $fornecedorSicaf->fill($data);
            $fornecedorSicaf->save();
        } else {
            $fornecedorSicaf = FornecedorSicaf::create($data);
        }
        return $fornecedorSicaf;

    }

    public function retornaConsultaSicaf(Fornecedor $fornecedorSicaf)
    {
        try {
            $fornecedor = $this->consultaSicaf($fornecedorSicaf);
            return $fornecedor;
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            return $e->getCode();
        }
    }

    public function retornaConsultaRFB(Fornecedor $fornecedorSicaf)
    {
        try {
            $fornecedor = $this->consultaRFB($fornecedorSicaf);
            return $fornecedor;
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            return $e->getCode();
        }
    }

    public function retornaConsultaSicafeRFB($fornecedor_id, $contrato_id)
    {
        $fornecedorHistorico = FornecedorContratoHistorico::where('fornecedor_id', $fornecedor_id)
            ->where('contrato_id', $contrato_id)
            ->first();
        //Não avança caso existir no histórico
        if ($fornecedorHistorico) {
            return;
        }
        $fornecedor = Fornecedor::find($fornecedor_id);
        // Não avança caso o fornecedor não foi encontrado.
        if (!$fornecedor) {
            return;
        }
        $fornecedorService = new FornecedorSicafService(new SICAF());
        $consultaFornecedor = $fornecedorService->retornaConsultaSicaf($fornecedor);

        if (!is_object($consultaFornecedor)) {
            $consultaFornecedor = $fornecedorService->retornaConsultaRFB($fornecedor);
        }

        if (!$consultaFornecedor) {
            return;
        }

        $mei = $consultaFornecedor->mei == 'Sim' ? true : false;
        FornecedorContratoHistorico::firstOrCreate(
            [
                'contrato_id' => $contrato_id,
                'fornecedor_id' => $consultaFornecedor->fornecedor_id
            ],
            [
                'contrato_id' => $contrato_id,
                'fornecedor_id' => $consultaFornecedor->fornecedor_id,
                'porte' => $this->retornaIdCodigoItem($consultaFornecedor->porte) ?? null,
                'mei' => $mei ?? null
            ]
        );
    }
}
