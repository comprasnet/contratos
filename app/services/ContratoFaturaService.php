<?php

namespace App\services;

use App\Http\Controllers\SICAF;
use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\Formatador;
use App\Models\Codigoitem;
use App\Models\Contrato;
use App\Models\Contratofatura;
use App\Models\ContratoFaturaEmpenho;
use App\Models\ContratoFaturaMesAno;
use App\Models\ContratoFaturasItens;
use App\Models\Contratohistorico;
use App\Models\Empenho;
use App\Models\Feriado;
use App\Models\Fornecedor;
use App\Models\Paises;
use App\Models\Saldohistoricoitem;
use App\Models\Tipolistafatura;
use App\services\AmparoLegalService;
use App\services\Fornecedor\FornecedorSicafService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class ContratoFaturaService
{
    use Formatador;
    use BuscaCodigoItens;

    protected $amparoLegalService;
    protected $fornecedorSicafService;

    public function __construct(AmparoLegalService $amparoLegalService, FornecedorSicafService $fornecedorSicafService)
    {
        $this->amparoLegalService = $amparoLegalService;
        $this->fornecedorSicafService = $fornecedorSicafService;
    }

    public function getTipoListaFatura(int $contratoId)
    {
        $tipolistafaturaQuery = Tipolistafatura::where('situacao', true)
            ->orderBy('nome', 'ASC');

        $lei14133 = $this->amparoLegalService->existeAmparoLegal14133PorContrato($contratoId);

        if ($lei14133) {
            $tipolistafaturaQuery->whereIn('id', [1, 2, 3, 4]);
        }

        return $tipolistafaturaQuery->pluck('nome', 'id')->toArray();
    }

    public function getBotaoVoltar($crud)
    {
        $deOndeUsuarioVem = session()->get('de_onde_usuario_vem_faturas');
        if (empty($deOndeUsuarioVem)) {
            $paginaAnterior = \URL::previous();
            $verificacao = strpos($paginaAnterior, '/contrato');    //aqui quer dizer que vem de contratos

            if (!$verificacao) {
                \Session::put('de_onde_usuario_vem_faturas', 'contrato');
                $crud->addButtonFromView('top', 'voltar', 'voltarmeucontrato', 'end');
            } else {
                \Session::put('de_onde_usuario_vem_faturas', 'meuscontratos');
                $crud->addButtonFromView('top', 'voltar', 'voltarcontrato', 'end');
            }
        } else {
            if ($deOndeUsuarioVem == 'contrato') {
                $crud->addButtonFromView('top', 'voltar', 'voltarmeucontrato', 'end');
            } elseif ($deOndeUsuarioVem == 'meuscontratos') {
                $crud->addButtonFromView('top', 'voltar', 'voltarcontrato', 'end');
            }
        }
        return true;
    }

    public function colunas()
    {
        $colunas = [
            [
                'name' => 'getContrato',
                'label' => 'Contrato',
                'type' => 'model_function',
                'function_name' => 'getContrato',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'getTipoListaFatura',
                'label' => 'Tipo Lista',
                'type' => 'model_function',
                'function_name' => 'getTipoListaFatura',
                'orderable' => true,
                'limit' => 1000,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere('tipolistafatura.nome', 'like', "%" . strtoupper($searchTerm) . "%");
                },
            ],
            [
                'name' => 'tipoDeInstrumentoDeCobranca',
                'label' => 'Tipo de Instrumento',
                'type' => 'text',
                'orderable' => true,
                'limit' => 1000,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'geDownloadArquivo',
                'label' => 'Arquivo',
                'type' => 'model_function',
                'function_name' => 'geDownloadArquivo',
                'orderable' => true,
                'limit' => 1000,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'getJustificativaFatura',
                'label' => 'Justificativa',
                'type' => 'model_function',
                'function_name' => 'getJustificativaFatura',
                'limit' => 1000,
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'getSfpadrao',
                'label' => 'Doc. Origem Siafi',
                'type' => 'model_function',
                'function_name' => 'getSfpadrao',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'numero',
                'label' => 'Número',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'serie',
                'label' => 'Série',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'chave_nfe',
                'label' => 'Chave NFe',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'emissao',
                'label' => 'Dt. Emissão',
                'type' => 'date',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'vencimento',
                'label' => 'Dt. Limite de Pagamento',
                'type' => 'date',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'retornarValorTotal',
                'label' => 'Valor',
                'type' => 'model_function',
                'function_name' => 'retornarValorTotal',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'formatJuros',
                'label' => 'Juros',
                'type' => 'model_function',
                'function_name' => 'formatJuros',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'formatMulta',
                'label' => 'Multa',
                'type' => 'model_function',
                'function_name' => 'formatMulta',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'formatGlosa',
                'label' => 'Glosa',
                'type' => 'model_function',
                'function_name' => 'formatGlosa',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'formatValorFaturado',
                'label' => 'Valor Faturado',
                'type' => 'model_function',
                'function_name' => 'formatValorFaturado',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'processo',
                'label' => 'Processo',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'protocolo',
                'label' => 'Dt. Recebimento',
                'type' => 'date',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'ateste',
                'label' => 'Dt. Liquidação de Despesa',
                'type' => 'date',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'prazo',
                'label' => 'Dt. Prazo Pagto.',
                'type' => 'date',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'empenhos',
                'label' => 'Empenhos',
                'type' => 'select_multiple',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'entity' => 'empenhos',
                'attribute' => 'numero',
                'model' => Empenho::class,
                'pivot' => true,
            ],
            [
                'name' => 'repactuacao',
                'label' => 'Repactuação',
                'type' => 'boolean',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'options' => [0 => 'Não', 1 => 'Sim']
            ],
            [
                'name' => 'infcomplementar',
                'label' => 'Informações Complementares',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'mes_ano_valor_fatura',
                'label' => 'Ano / Mês Referência',
                'type' => 'mes_ano_valor_fatura',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'situacao',
                'label' => 'Situação',
                'type' => 'select_from_array',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'options' => config('app.situacao_fatura')
            ],
        ];

        return $colunas;
    }

    public function campos($contrato, $tipolistafatura, $contrato_id, $crud, $ondeVeio)
    {
        $con = Contrato::find($contrato_id);

        //guarda a acao do form (create ou edit)
        $actionMethod = $crud->getActionMethod();
        $deveConsultarSicaf = in_array($actionMethod, ['create', 'edit']);
        $sicafStatus = -1;

        $resultadoSicaf = false;

        if ($deveConsultarSicaf) {
            $sicafApi = new SICAF();

            $consultaDadosBasicosSicaf = $this->consultaSicaf($con->fornecedor);

            if (is_object($consultaDadosBasicosSicaf)) {
                $resultadoSicaf = true;

                if (
                    $consultaDadosBasicosSicaf->validade_receita_federal_pgfn &&
                    $consultaDadosBasicosSicaf->validade_certidao_fgts &&
                    $consultaDadosBasicosSicaf->validade_certidao_trabalhista &&
                    $consultaDadosBasicosSicaf->validate_receita_estadual_distrital &&
                    $consultaDadosBasicosSicaf->validade_receita_municipal
                ) {

                    if (
                        !$consultaDadosBasicosSicaf->date_receita_federal_pgfn ||
                        !$consultaDadosBasicosSicaf->date_certidao_fgts ||
                        !$consultaDadosBasicosSicaf->date_certidao_trabalhista ||
                        !$consultaDadosBasicosSicaf->date_receita_estadual_distrital ||
                        !$consultaDadosBasicosSicaf->date_receita_municipal
                    ) {
                        $sicafStatus = 2; //Valida com data sem informação
                    } else {
                        $sicafStatus = 1; //Válida com todas informações
                    }
                } else {
                    $sicafStatus = 3; //Invalida
                }
            } else {
                $sicafStatus = $consultaDadosBasicosSicaf; //Erro da api
            }

            if ($resultadoSicaf) {
                $existeCertidao = false;
                if ($this->retornaSomenteNumeros($con->fornecedor->cpf_cnpj_idgener)) {
                    $regularidadeFiscal = $sicafApi->regularidadeFiscalEconomicaFinanceira($this->retornaSomenteNumeros($con->fornecedor->cpf_cnpj_idgener));
                    if (is_object($regularidadeFiscal)) {
                        $existeCertidao = $regularidadeFiscal;
                    }
                }
            } else {
                $existeCertidao = false;
            }
        }

        //query que retornar as opções de contrato historico pelo contrato_id
        $queryContratoHistorico = Contratohistorico::select(
            [
                'contratohistorico.id as ch_id',
                DB::raw("CONCAT(codigoitens.descricao, ' - ', contratohistorico.numero, ' - Data Assinatura: ', to_char(contratohistorico.data_assinatura, 'DD/MM/YYYY')) as descricaonumero")
            ]
        )
            ->join('codigoitens', 'codigoitens.id', '=', 'contratohistorico.tipo_id')
            ->where('contratohistorico.contrato_id', $contrato_id)
            ->where('contratohistorico.elaboracao', false)
            ->when($ondeVeio === 'contratos', function ($query) {
                $query->where('codigoitens.descricao', '<>', 'Termo de Rescisão');
            })
            ->orderBy('contratohistorico.id', 'contratohistorico.updated_at', 'DESC');


        //caso seja edit pega o fatura_id pela rota
        $fatura_id = \Route::current()->parameter('fatura') ?? null;

        //option para o select de Historico
        $optionsContratoHistorico = clone $queryContratoHistorico; //clone necessario pois a mesma query é usada para outro fim

        //query que lista os itens na grid necessario para pegar o somatorio de valortotal_faturado
        $queryContratoFaturaItens = $this->getQueryContratofaturasItem(
            $fatura_id,
            $fatura_id ? Contratofatura::find($fatura_id)->contratohistorico_id : [],
            $actionMethod,
            $ondeVeio
        );

        //soma o campo valototal_faturado para preencher o input de Valor Total Faturado
        $sumValorTotalFaturado = empty($queryContratoFaturaItens) ? 0 : number_format(
            $queryContratoFaturaItens->sum('valortotal_faturado'),
            2,
            ',',
            '.'
        );

        //style para o campo readonly
        $styleQuandoEditAction = "background: #eee; pointer-events: none; touch-action: none";

        $arrayInstrumentoDeCobrancas = Contratofatura::getArrayComInstrumentoDeCobranca();

        $dataLimiteDias = $con->modalidade_id == Codigoitem::where('descricao', 'Dispensa')->first()->id ? 5 : 10;

        $campos = [
            [
                'name' => 'arquivo_do_instrumento_de_cobranca_aux',
                'type' => 'hidden',
                'value' => '',
            ],
            [
                'name' => 'toggle_sicaf',
                'label' => "Toggle SICAF",
                'type' => 'toggle_sicaf',
                'tab' => 'Dados Instrumentos de Cobrança',
            ],
            [
                'name' => 'status_sicaf',
                'type' => 'hidden',
                'value' => $sicafStatus,
                'tab' => 'Dados Instrumentos de Cobrança',
                'attributes' => [
                    'id' => 'status_sicaf',
                ],
            ],
            [
                'name' => 'date_receita_federal_pgfn',
                'label' => "Receita Federal e PGFN",
                'type' => $deveConsultarSicaf && $resultadoSicaf && $consultaDadosBasicosSicaf->date_receita_federal_pgfn ? 'date' : 'text',
                'tab' => 'Dados Instrumentos de Cobrança',
                'value' => $deveConsultarSicaf && $resultadoSicaf && $consultaDadosBasicosSicaf->date_receita_federal_pgfn ? $consultaDadosBasicosSicaf->date_receita_federal_pgfn : 'Sem Informação',
                'attributes' => [
                    'disabled' => true,
                ],
                'wrapperAttributes' => [
                    'class' => 'campo-sifac form-group col-sm-2 ' . ($deveConsultarSicaf && $resultadoSicaf && !$consultaDadosBasicosSicaf->validade_receita_federal_pgfn ? 'has-error' : ''),
                ]
            ],
            [
                'name' => 'date_certidao_fgts',
                'label' => "Certidão FGTS",
                'type' => $deveConsultarSicaf && $resultadoSicaf && $consultaDadosBasicosSicaf->date_certidao_fgts ? 'date' : 'text',
                'tab' => 'Dados Instrumentos de Cobrança',
                'value' => $deveConsultarSicaf && $resultadoSicaf && $consultaDadosBasicosSicaf->date_certidao_fgts ? $consultaDadosBasicosSicaf->date_certidao_fgts : 'Sem Informação',
                'attributes' => [
                    'disabled' => true,
                ],
                'wrapperAttributes' => [
                    'class' => 'campo-sifac form-group col-sm-2 ' . ($deveConsultarSicaf && $resultadoSicaf && !$consultaDadosBasicosSicaf->validade_certidao_fgts ? 'has-error' : ''),
                ]
            ],
            [
                'name' => 'date_certidao_trabalhista',
                'label' => "Certidão Trabalhista",
                'type' => $deveConsultarSicaf && $resultadoSicaf && $consultaDadosBasicosSicaf->date_certidao_trabalhista ? 'date' : 'text',
                'tab' => 'Dados Instrumentos de Cobrança',
                'value' => $deveConsultarSicaf && $resultadoSicaf && $consultaDadosBasicosSicaf->date_certidao_trabalhista ? $consultaDadosBasicosSicaf->date_certidao_trabalhista : 'Sem Informação',
                'attributes' => [
                    'disabled' => true,
                ],
                'wrapperAttributes' => [
                    'class' => 'campo-sifac form-group col-sm-2 ' . ($deveConsultarSicaf && $resultadoSicaf && !$consultaDadosBasicosSicaf->validade_certidao_trabalhista ? 'has-error' : ''),
                ]
            ],
            [
                'name' => 'date_receita_estadual_distrital',
                'label' => "Receita Estadual/Distrital",
                'tab' => 'Dados Instrumentos de Cobrança',
                'type' => 'download_sicaf_receita_estadual_municipal',
                'suffix' => ($deveConsultarSicaf && $existeCertidao && $existeCertidao != null && $existeCertidao->codigoArquivoRegFiscalEst)
                    ? '<a class="btn btn-default" href="'
                    . route('meus-contratos.certidaoestadual', [
                        'contrato_id' => $contrato_id
                    ]) . '" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a>'
                    : null,
                'value' => $deveConsultarSicaf && $resultadoSicaf && $consultaDadosBasicosSicaf->date_receita_estadual_distrital ? $this->retornaDataAPartirDeCampo($consultaDadosBasicosSicaf->date_receita_estadual_distrital) : 'Sem Informação',
                'attributes' => [
                    'disabled' => true,
                ],
                'wrapperAttributes' => [
                    'class' => 'campo-sifac form-group col-sm-2 ' . ($deveConsultarSicaf && $resultadoSicaf && !$consultaDadosBasicosSicaf->validate_receita_estadual_distrital ? 'has-error' : ''),
                ]
            ],
            [
                'name' => 'date_receita_municipal',
                'label' => "Receita Municipal",
                'tab' => 'Dados Instrumentos de Cobrança',
                'type' => 'download_sicaf_receita_estadual_municipal',
                'suffix' => ($deveConsultarSicaf && $existeCertidao && $existeCertidao != null && $existeCertidao->codigoArquivoRegFiscalMun) ?
                    '<a class="btn btn-default" href="'
                    . route('meus-contratos.certidaomunicipal', [
                        'contrato_id' => $contrato_id
                    ]) . '" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a>'
                    : null,
                'value' => $deveConsultarSicaf && $resultadoSicaf && $consultaDadosBasicosSicaf->date_receita_municipal ? $this->retornaDataAPartirDeCampo($consultaDadosBasicosSicaf->date_receita_municipal) : 'Sem Informação',
                'attributes' => [
                    'disabled' => true,
                ],
                'wrapperAttributes' => [
                    'class' => 'campo-sifac form-group col-sm-2 ' . ($deveConsultarSicaf && $resultadoSicaf && !$consultaDadosBasicosSicaf->validade_receita_municipal ? 'has-error' : ''),
                ]
            ],
            [
                'name' => 'contrato_id',
                'label' => "Número do instrumento",
                'type' => 'select_from_array',
                'options' => $contrato,
                'allows_null' => false,
                'attributes' => [
                    'readonly' => 'readonly',
                    'style' => 'pointer-events: none;touch-action: none;'
                ],
                'tab' => 'Dados Instrumentos de Cobrança',
            ],
            [
                'name' => 'tipolistafatura_id',
                'label' => "Tipo Lista",
                'type' => 'select2_from_array',
                'options' => $tipolistafatura,
                'allows_null' => true,
                'tab' => 'Dados Instrumentos de Cobrança',
            ],
            [
                'name' => 'tipo_de_instrumento_de_cobranca_id',
                'label' => "Tipo de Instrumento de Cobrança",
                'attribute' => [
                    'id' => 'tipo_de_instrumento_de_cobranca_id'
                ],
                'type' => 'select_from_array',
                'options' => $arrayInstrumentoDeCobrancas,
                'allows_null' => false,
                'tab' => 'Dados Instrumentos de Cobrança',
            ],
            [
                'name' => 'numero',
                'label' => "Número",
                'type' => 'text',
                'attributes' => [
                    'maxlength' => '17',
                    'onkeyup' => "maiuscula(this)",
                ],
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-6'
                ],
                'tab' => 'Dados Instrumentos de Cobrança',
            ],
            [
                'name' => 'serie',
                'label' => "Série",
                'type' => 'text',
                'attributes' => [
                    'id' => 'input_serie',
                    'maxlength' => '25',
                    'onkeyup' => "serieNotaFiscal(this)",
                    'readonly' => 'readonly'
                ],
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-6 serieNumero'
                ],
                'tab' => 'Dados Instrumentos de Cobrança',
            ],
            [
                'name' => 'chave_nfe',
                'label' => "Chave NFe",
                'type' => 'text',
                'suffix' => '<span id="chave_nfe_icon_check"></span>',
                'attributes' => [
                    'id' => 'input_chafe_nfe',
                    'maxlength' => '44',
                    'onkeyup' => "maiuscula(this);valivarChaveNFe(this, 'chave_nfe_icon_check')",
                    'onblur' => "estilizarComDefault(this, 'chave_nfe_icon_check')",
                    'style' => 'border-right:none'
                ],
                'tab' => 'Dados Instrumentos de Cobrança',
            ],
            [   // Upload
                'name' => 'arquivo_do_instrumento_de_cobranca',
                'label' => 'Arquivo do Instrumento de Cobrança',
                'attribute' => [
                    'id' => 'arquivo_do_instrumento_de_cobranca_file_input',
                ],

                'type' => 'upload',
                'upload' => true,
                'disk' => 'public', // if you store files in the /public folder, please omit this; if you store them in /storage or S3, please specify it;
                'tab' => 'Dados Instrumentos de Cobrança',
            ],
            [
                'name' => 'emissao',
                'label' => "Dt. Emissão",
                'type' => 'date',
                'tab' => 'Dados Instrumentos de Cobrança',
            ],
            //tab Itens Faturados
            [
                'name' => 'juros',
                'label' => 'Juros',
                'type' => 'money_fatura',
                'attributes' => [
                    'id' => 'juros',
                    'onkeyup' => "calcularValorTotalLiquido()",
                ],
                'wrapperAttributes' => [
                    'class' => 'form-group col-sm-4'
                ],
                'prefix' => "R$",
                'tab' => 'Itens Faturados',
            ],
            [
                'name' => 'multa',
                'label' => 'Multa',
                'type' => 'money_fatura',
                'attributes' => [
                    'id' => 'multa',
                    'onkeyup' => "calcularValorTotalLiquido()",
                ],
                'wrapperAttributes' => [
                    'class' => 'form-group col-sm-4'
                ],
                'prefix' => "R$",
                'tab' => 'Itens Faturados',
            ],
            [
                'name' => 'glosa',
                'label' => 'Glosa',
                'type' => 'money_fatura',
                'attributes' => [
                    'id' => 'glosa',
                    'onkeyup' => "calcularValorTotalLiquido();validarGlosaMaiorQueValorTotalFaturado(this)",
                ],
                'wrapperAttributes' => [
                    'class' => 'form-group col-sm-4'
                ],
                'prefix' => "R$",
                'tab' => 'Itens Faturados',
            ],
            [
                'name' => 'valorliquido',
                'label' => "Valor Líquido",
                'type' => 'text',
                'value' => $this->calculaAcrescimoEDecrescimo($sumValorTotalFaturado, $fatura_id),
                'prefix' => 'R$ ',
                'attributes' => [
                    'id' => 'valorliquido',
                    'readonly' => 'readonly',
                    'onchange' => "atualizarMaskMoney2Decimals(this)",
                    'class' => 'form-control'
                ],
                'wrapperAttributes' => [
                    'class' => 'form-group col-sm-6'
                ],
                'tab' => 'Itens Faturados',
            ],
            [
                'name' => 'valortotalfaturado_calc',
                'label' => "Valor Total Faturado",
                'type' => 'text',
                'value' => $sumValorTotalFaturado,
                'prefix' => 'R$ ',
                'attributes' => [
                    'id' => 'valortotalfaturado_calc',
                    'readonly' => 'readonly',
                    'onchange' => "atualizarMaskMoney2Decimals(this);setaInfoValorTotalFaturado();calcularSaldo();setaInfoValorTotalFaturadoEmpenho();calcularSaldoEmpenho()",
                    'class' => 'form-control'
                ],
                'wrapperAttributes' => [
                    'class' => 'form-group col-sm-6'
                ],
                'tab' => 'Itens Faturados',
            ],
            [
                'name' => 'table_itens_faturados',
                'type' => 'table_itens_faturados',
                'attributes' => [
                    'onkeyup' => "maiuscula(this)",
                    'class' => 'linha-divisoria'
                ],
                'options' => $optionsContratoHistorico->pluck('descricaonumero', 'ch_id'),
                'tab' => 'Itens Faturados',
            ],
            //fim tab Itens Faturados
            [
                'name' => 'processo',
                'label' => "Processo",
                'type' => 'numprocesso',
                'value' => $crud->actionIs('create') ? $con->processo : null,
                'tab' => 'Outras Informações',
            ],
            [
                'name' => 'protocolo',
                'label' => "Dt. Recebimento",
                'type' => 'date',
                'tab' => 'Outras Informações',
                'ico_help' => 'Data de protocolo do instrumento de cobrança.',
            ],
            [
                'name' => 'ateste',
                'label' => "Dt. Liquidação de Despesa",
                'type' => 'date',
                'tab' => 'Outras Informações',
                'attributes' => [
                    'onchange' => "updateDataLimite(this, $dataLimiteDias)",
                ],
                'ico_help' => 'Data do ateste do instrumento de cobrança.',
            ],
            [
                'name' => 'vencimento',
                'label' => "Dt. Limite Pagamento",
                'type' => 'date',
                'tab' => 'Outras Informações',
                'ico_help' => 'Conforme IN nº 77/2022',
            ],
            [
                'name' => 'repactuacao',
                'label' => "Repactuação?",
                'type' => 'radio',
                'options' => [0 => 'Não', 1 => 'Sim'],
                'default' => 0,
                'inline' => true,
                'tab' => 'Outras Informações',
            ],
            [
                'name' => 'infcomplementar',
                'label' => "Informações Complementares",
                'type' => 'text',
                'attributes' => [
                    'onkeyup' => "maiuscula(this)",
                ],
                'tab' => 'Outras Informações',
            ],
            [
                'name' => 'situacao',
                'label' => "Situação",
                'type' => 'select_from_array',
                'options' => config('app.situacao_fatura'),
                'default' => 'PEN',
                'attributes' => [
                    'readonly' => 'readonly',
                    'style' => 'pointer-events: none;touch-action: none;'
                ],
                'allows_null' => false,
                'tab' => 'Outras Informações',
            ],
            [ // Table
                'name' => 'mes_ano_json',
                'label' => 'Referência',
                'type' => 'fatura_mes_ano_table',
                'columns' => [
                    [
                        'name' => 'mesref',
                        'label' => 'Mês',
                        'type' => 'select2_from_array_fatura_mes_ano',
                        'options' => config('app.meses_referencia_fatura')
                    ],
                    [
                        'name' => 'anoref',
                        'label' => 'Ano',
                        'type' => 'select2_from_array_fatura_mes_ano',
                        'options' => config('app.anos_referencia_fatura')
                    ],
                    [
                        'name' => 'valorref',
                        'label' => 'Valor',
                        'type' => 'text',
                        'options' => null
                    ],
                ],
                'max' => 99, // maximum rows allowed in the table
                'min' => 1, // minimum rows allowed in the table
                'tab' => 'Outras Informações',
            ],
            [ // Table
                'name' => 'empenho_json',
                'label' => 'Empenhos',
                'type' => 'contratofaturas.empenho_table',
                'columns' => [
                    [
                        'label' => 'Empenho',
                        'type' => "select2_from_ajax",
                        'name' => 'empenho_id',
                        'entity' => ContratoFaturaEmpenho::class,
                        'attribute' => 'empenho_id',
                        'attribute2' => 'numero',
                        'model' => Empenho::class,
                        'data_source' => route('api.faturas.listaEmpenhosParaFaturas', ['contrato_id' => $contrato_id]),
                        'placeholder' => 'Selecione',
                        'minimum_input_length' => 0,
                    ],
                    [
                        'name' => 'empenhosubelemento',
                        'label' => 'Subelemento',
                        'type' => 'select_from_array',
                        'placeholder' => 'Selecione',
                        'options' => []
                    ],
                    [
                        'name' => 'empenhovalorref',
                        'label' => 'Valor',
                        'type' => 'text',
                        'options' => null
                    ],
                ],
                'max' => 99, // maximum rows allowed in the table
                'min' => 0, // minimum rows allowed in the table
                'sisg' => $con->unidade->sisg == true ? 'Sim' : 'Nao',
                'tab' => 'Outras Informações',
            ]
        ];

        return $campos;
    }

    public function preparaDados(Request $request)
    {
        if (!$this->verificarSeEmpenhoFoiVinculado($request)) {
            \Alert::error('É necessário vincular um empenho no instrumento de cobrança.')->flash();
            return ['error' => true];
        }

        if ($request->input('situacao') != 'PEN') {
            \Alert::error('Esse instrumento de cobrança não pode ser alterado!')->flash();
            return ['errorUpdate' => true];
        }

        $this->removerProvisoriamenteInputsMesAnoValor($request);

        $contrato_id = $request->input('contrato_id');

        $contrato = Contrato::find($contrato_id)->first();

        $v = number_format(floatval(str_replace(',', '.', str_replace('.', '', $request->input('valor')))), 2, '.', '');
        $j = number_format(floatval(str_replace(',', '.', str_replace('.', '', $request->input('juros')))), 2, '.', '');
        $m = number_format(floatval(str_replace(',', '.', str_replace('.', '', $request->input('multa')))), 2, '.', '');
        $g = number_format(floatval(str_replace(',', '.', str_replace('.', '', $request->input('glosa')))), 2, '.', '');
        $valorliquido = number_format(floatval(str_replace(',', '.', str_replace('.', '', $request->input('valorliquido')))), 2, '.', '');

        $valorFaturado = self::getValorFaturado($valorliquido, $g, $j, $m);

        $ateste = $request->input('ateste');

        $dataLimiteDias = $contrato->modalidade_id == Codigoitem::where('descricao', 'Dispensa')->first()->id ? 5 : 10;

        if ($ateste) {
            $dias = Feriado::calculaDiasUteis($ateste, $dataLimiteDias);
            $request->request->set('prazo', $dias[count($dias) - 1]);
        }

        $request->request->set('valor', $valorFaturado);    #415 valor passa a receber o valor faturado
        $request->request->set('juros', $j);
        $request->request->set('multa', $m);
        $request->request->set('glosa', $g);
        $request->request->set('valorliquido', $valorliquido);
        $request->request->set('valorfaturado', $valorFaturado);

        $request->request->set('situacao', 'PEN');

        $request->request->set('user_id', backpack_user()->id);

        return $request;
    }

    public function getValorFaturado($valorliquido, $glosa, $juros, $multa)
    {
        return ($valorliquido + $glosa - $juros - $multa);
    }

    private function formataValorref($valorref)
    {
        return number_format(
            floatval(
                str_replace(
                    ',',
                    '.',
                    str_replace(
                        '.',
                        '',
                        $valorref ?? 0
                    )
                )
            ),
            2,
            '.',
            ''
        );
    }

    /**
     * Atualiza ou Cria ContratoFaturasMesAno
     *
     * @param int $id_fatura
     */
    public function updateOrCreateContratoFaturasMesAno(array $mes_ano_valor, int $contratofaturas_id, $ids_contratofaturasmesano = null): void
    {
        $array_ids_contratofaturasmesano = json_decode($ids_contratofaturasmesano);

        $this->softDeleteContratoFaturasMesAno($mes_ano_valor, $array_ids_contratofaturasmesano);

        foreach ($mes_ano_valor as $key => $current) {
            $current['valorref'] = $this->formataValorref($current['valorref']);
            $current['mesref_anoref_valor_json'] = json_encode($current);
            $current['contratofaturas_id'] = $contratofaturas_id;

            ContratoFaturaMesAno::updateOrCreate(
                [
                    'id' => $array_ids_contratofaturasmesano[$key] ?? null,
                    'contratofaturas_id' => $current['contratofaturas_id'],
                ],
                [
                    'anoref' => $current['anoref'],
                    'mesref' => $current['mesref'],
                    'valorref' => $current['valorref'],
                    'mesref_anoref_valor_json' => $current['mesref_anoref_valor_json']
                ]
            );
        }
    }

    /**
     * Atualiza ou Cria ContratoFaturasMesAno
     *
     * @param UpdateRequest $request
     * @param int $id_fatura
     */
    public function updateOrCreateContratoFaturasEmpenho($empenhos, int $contratofaturas_id, $ids_contratofaturasempenho = null): void
    {
        $array_ids_contratofaturasempenho = json_decode($ids_contratofaturasempenho);

        $this->softDeleteContratoFaturasEmpenho($empenhos, $array_ids_contratofaturasempenho);

        if ($empenhos != null) {
            foreach ($empenhos as $key => $current) {

                $current['valorref'] = $this->formataValorref($current['empenhovalorref']);
                $current['contratofaturas_id'] = $contratofaturas_id;
                $current['empenhosubelemento'] = $current['empenhosubelemento'] ?? null;

                ContratoFaturaEmpenho::updateOrCreate(
                    [
                        'id' => $array_ids_contratofaturasempenho[$key] ?? null,
                        'contratofatura_id' => $current['contratofaturas_id'],
                    ],
                    [
                        'valorref' => $current['valorref'],
                        'empenho_id' => $current['empenho_id'],
                        'subelemento_id' => $current['empenhosubelemento'],
                    ]
                );
            }
        }
    }

    /**
     * Atualiza ou Cria ContratoItensFaturados
     *
     * @param $request
     * @param int $contratofaturas_id
     */
    public function updateOrCreateContratoItensFaturados($request, int $contratofaturas_id)
    {
        $contratohistorico_id = $request->descricaonumero;

        $itemable_type = $contratohistorico_id ? 'App\Models\Saldohistoricoitem' : 'App\Models\Contratoitem';

        if (!is_array($request['paisfabricacao_id'])) {
            $tamanhoPaisFabricacao_verificacao = 0;
        } else {
            $tamanhoPaisFabricacao_verificacao = sizeof($request['paisfabricacao_id']);
        }

        foreach ($request['valortotal_faturado'] as $key => $current) {

            if (
                $request['valortotal_faturado'][$key] === null &&
                $request['quantidade_faturado'][$key] === null &&
                $request['valorunitario_faturado'][$key] === null
            ) {
                continue;
            }

            if ($key < $tamanhoPaisFabricacao_verificacao) {
                $paisfabricacao_id = $request['paisfabricacao_id'][$key];
            } else {
                $paisfabricacao_id = null;
            }    #605

            ContratoFaturasItens::updateOrCreate(
                [
                    'id' => $request['id_contratofatura_itens'][$key] ?? null,
                    'saldohistoricoitens_id' => $request['saldohistoricoitens_id'][$key],
                    'contratofaturas_id' => $contratofaturas_id,
                ],
                [
                    'itemable_type' => $itemable_type,
                    'quantidade_faturado' => $request['quantidade_faturado'][$key] ?? 0,
                    'valorunitario_faturado' => str_replace(',', '.', str_replace('.', '', $request['valorunitario_faturado'][$key] ?? 0)),
                    'valortotal_faturado' => $this->formataValorref($request['valortotal_faturado'][$key]),
                    'paisfabricacao_id' => $paisfabricacao_id, #605
                ]
            );
        }
    }

    /**
     * Remove os registros da tabela ContratoFaturasEmpenho
     * @param array $empenhos
     * @param null $ids_contratofaturasempenho
     */
    private function softDeleteContratoFaturasEmpenho($empenhos, $ids_contratofaturasempenho = null): void
    {
        $empenhos = $empenhos != null ? $empenhos : [];

        //se for edição o array terá valor
        if ($ids_contratofaturasempenho !== null) {
            //cria array com ids nao removidos
            $array_ids_nao_removidos = array_map(function ($current) {
                return isset($current['ids_contratofaturasempenho']) ? $current['ids_contratofaturasempenho'] : null;
            }, $empenhos);

            //remove ids que foram removidos da grid
            foreach ($ids_contratofaturasempenho as $key => $value) {
                if (!in_array($ids_contratofaturasempenho[$key], $array_ids_nao_removidos)) {
                    ContratoFaturaEmpenho::find($ids_contratofaturasempenho[$key])->delete();
                }
            }
        }
    }

    /**
     * Remove os registros da tabela ContratoFaturasMesAno
     * @param array $mes_ano_valor
     * @param null $ids_contratofaturasmesano
     */
    private function softDeleteContratoFaturasMesAno(array $mes_ano_valor, $array_ids_contratofaturasmesano = null): void
    {
        //se for edição o array terá valor
        if ($array_ids_contratofaturasmesano !== null) {

            //cria array com ids nao removidos
            $array_ids_nao_removidos = array_map(function ($current) {
                return isset($current['idcontratofaturasmesano']) ? $current['idcontratofaturasmesano'] : null;
            }, $mes_ano_valor);

            //remove campos indesejados
            $array_ids_nao_removido_filter = array_filter($array_ids_nao_removidos, function ($item) {
                return (
                    $item !== null
                );
            });
            //remove ids que foram removidos da grid
            foreach ($array_ids_contratofaturasmesano as $key => $value) {
                if (!in_array($array_ids_contratofaturasmesano[$key], $array_ids_nao_removido_filter)) {
                    ContratoFaturaMesAno::find($array_ids_contratofaturasmesano[$key])->delete();
                }
            }
        }
    }
    /**
     * @param Request $request
     * Remove os campos do objeto request provisoriamente até que a migração de dados desses campos seja feita
     */
    private function removerProvisoriamenteInputsMesAnoValor(Request $request): void
    {
        $request->request->remove('mesref');
        $request->request->remove('anoref');
        $request->request->remove('valor');
    }

    public function verificarSeEmpenhoFoiVinculado($request)
    {
        $empenhosInformados = $request->get('empenho_id');
        if ($empenhosInformados[0] == null) {
            return false;
        }
        return true;
    }

    /**
     * Calcula acrescimos e decrescimos do valor total da fatura
     *
     *
     * @param $sumValorTotalFaturado valorTotal de cada item da fatura
     * @param null $fatura_id
     * @return string
     */
    private function calculaAcrescimoEDecrescimo($sumValorTotalFaturado, $fatura_id = null)
    {
        if (!$fatura_id) {
            return $sumValorTotalFaturado;
        }

        $fatura = Contratofatura::find($fatura_id);

        $sumValorTotalFaturado = number_format(floatval(str_replace(',', '.', str_replace('.', '', $sumValorTotalFaturado))), 2, '.', '');

        $totalComAcrescimoEDecrescimo = $sumValorTotalFaturado + $fatura->juros + $fatura->multa - $fatura->glosa;

        $totalComAcrescimoEDecrescimo = number_format(
            $totalComAcrescimoEDecrescimo,
            2,
            ',',
            '.'
        );
        return $totalComAcrescimoEDecrescimo;
    }
    public function consultaSicaf(Fornecedor $fornecedorSicaf)
    {
        $fornecedor = $this->fornecedorSicafService->retornaConsultaSicaf($fornecedorSicaf);

        if (!is_object($fornecedor)) {
            $fornecedor = $this->fornecedorSicafService->retornaConsultaRFB($fornecedorSicaf);
        }

        return $fornecedor;
    }
    /**
     * Monta a query que lista os itens da aba de itens faturados se $contratohistorico tiver valor monta a query com
     * SaldoHistoricoItem se não monta com Contratoitem
     *
     * @param null $contratohistorico_id
     * @param string $actionMethod
     * @return Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Query\Builder[]|\Illuminate\Support\Collection
     */
    private function getQueryContratofaturasItem($fatura_id, $contratoItems = [], $actionMethod = 'create', $ondeVeio)
    {
        //caso seja edicao faz join com a tabela contratofaturas_itens e add os campos no select
        if ($actionMethod === 'edit' || $fatura_id) {
            $query = ContratoFaturasItens::select([
                "contratofaturas_itens.*"
            ]);

            $query = $query->addSelect(
                "saldohistoricoitens.saldoable_type",
                "saldohistoricoitens.saldoable_id",
                "saldohistoricoitens.contratoitem_id",
                "saldohistoricoitens.numero_item_compra"
            )->leftJoin('saldohistoricoitens', function ($join) use ($fatura_id, $ondeVeio) {
                $join->on(
                    'contratofaturas_itens.saldohistoricoitens_id',
                    '=',
                    "saldohistoricoitens.id"
                );
                if ($ondeVeio == "meus-contratos") {
                    $join->leftJoin('contratoitens', 'contratoitens.id', '=', 'saldohistoricoitens.contratoitem_id');
                }
            });

            $query->where('contratofaturas_itens.contratofaturas_id', '=', $fatura_id);
            $query->whereNull('saldohistoricoitens.deleted_at');
            if ($ondeVeio == "meus-contratos") {
                $query->whereNull('contratoitens.deleted_at');
                $query->orderBy('contratoitens.tipo_id');
            }
        } elseif ($actionMethod === 'create') {
            if (empty($contratoItems)) {
                return [];
            }

            $query = Saldohistoricoitem::select('saldohistoricoitens.*');

            $first = array_shift($contratoItems);
            $query = $query->where('saldohistoricoitens.id', $first);

            if ($contratoItems && count($contratoItems)) {
                foreach ($contratoItems as $itemRepetido) {
                    $queryItensRepetidos = $this->queryParaItensRepetidos($itemRepetido);

                    $query->unionAll($queryItensRepetidos);
                }
            }
        } else {
            return [];
        }

        return $query->get();
    }

    public function queryParaItensRepetidos($itemRepetido)
    {
        $query = Saldohistoricoitem::select('saldohistoricoitens.*');

        $query = $query->where('saldohistoricoitens.id', $itemRepetido);

        return $query;
    }

    public function deletar($id)
    {
        $contratoFaturas = Contratofatura::where('id', $id)->first();

        if ($contratoFaturas->situacao !== 'PEN') {
            return ['error' => true];
        }

        //tem que deletar o relacionamento também, não só em contratofatura
        ContratoFaturaEmpenho::where('contratofatura_id', $id)->delete();
    }

    public function contratoItensList(Request $request)
    {
        $contratohistoricoId = $request->get('contratohistorico_id');

        $saldoHistoricoitems = Saldohistoricoitem::select("saldohistoricoitens.*")
            ->leftJoin('contratoitens', 'contratoitens.id', '=', 'saldohistoricoitens.contratoitem_id') #605
            ->where('saldohistoricoitens.saldoable_id', $contratohistoricoId)
            ->where('saldohistoricoitens.elaboracao', false)
            ->whereNull('contratoitens.deleted_at')
            ->orderBy('contratoitens.tipo_id')
            ->get();

        $items = [];

        foreach ($saldoHistoricoitems as $saldoHistoricoitem) {
            $items[] = [
                'value' => $saldoHistoricoitem->id,
                'text' => "{$saldoHistoricoitem->getTipoItem()} | {$saldoHistoricoitem->numero_item_compra} | {$saldoHistoricoitem->getDescricaoComplementar()}",
            ];
        }

        return response()->json($items);
    }

    public function contratoItensAjax(Request $request)
    {
        $contratoItems = $request->contratoItems;

        $actionMethod = $request->actionMethod;

        $fatura_id = $request->fatura_id;

        $queryItem = $this->getQueryContratofaturasItem($fatura_id, $contratoItems, $actionMethod, 'meus-contratos');

        $index = -1;

        return DataTables::of($queryItem)
            ->addIndexColumn()
            ->addColumn('tipo_id', function ($item) use ($actionMethod, &$index) {
                $index++;

                if ($actionMethod == 'create') {
                    $value = $item->getTipoItem();
                } else {
                    $value = $item->saldohistoricoitem->getTipoItem() ?? ' - ';
                }

                return $value;
            })->addColumn('catmatseritem_id', function ($item) use ($actionMethod, &$index) {
                if ($actionMethod == 'create') {
                    $value = $item->contratoitem ? $item->contratoitem->getCatmatseritem() : ' - ';
                } else {
                    $value = $item->saldohistoricoitem->contratoitem ? $item->saldohistoricoitem->contratoitem->getCatmatseritem() : ' - ';
                }

                return $value;
            })->addColumn('quantidade', function ($item) use ($actionMethod, &$index) {
                if ($actionMethod == 'create') {
                    $value = $item->quantidade;
                } else {
                    $value = intval($item->saldohistoricoitem->quantidade);
                }

                return $value;
            })->addColumn('valorunitario', function ($item) use ($actionMethod, &$index) {
                if ($actionMethod == 'create') {
                    $value = $item->valorunitario;
                } else {
                    $value = $item->saldohistoricoitem->valorunitario;
                }

                return $value;
            })->addColumn('quantidade_faturado', function ($item) use ($actionMethod, $request, &$index) {
                if ($actionMethod == 'create') {
                    $value = $this->getValueItensFaturadosSaldoHistoricoItens($request, $item, 'quantidade_faturado', $index);
                } else {
                    $value = $this->getValueItensFaturadosContratoFaturaItens($request, $item, 'quantidade_faturado', $index);
                }

                return view(
                    'crud::fields.contratofaturas.text',
                    [
                        'field' => [
                            'name' => "quantidade_faturado[]",
                            'label' => '',
                            'attributes' => [
                                'id' => "quantidade_faturado{$item->id}",
                                'class' => 'form-control',
                                'onkeyup' => "atualizarValorTotalLinha(this);atualizarMask17Decimals(this);",
                                'onchange' => "pushParaArrayGlobal(this, $index)",
                            ],
                            'value' => $value,
                            'wrapperAttributes' => [
                                'class' => 'quantidade_faturado'
                            ]
                        ],
                        'crud' => $item
                    ]
                );
            })->addColumn('valorunitario_faturado', function ($item) use ($actionMethod, $request, &$index) {
                if ($actionMethod == 'create') {
                    $value = $this->getValueItensFaturadosSaldoHistoricoItens($request, $item, 'valorunitario_faturado', $index);
                } else {
                    $value = $this->getValueItensFaturadosContratoFaturaItens($request, $item, 'valorunitario_faturado', $index);
                }

                return view(
                    'crud::fields.contratofaturas.text',
                    [
                        'field' => [
                            'name' => "valorunitario_faturado[]",
                            'label' => '',
                            'attributes' => [
                                'id' => "valorunitario_faturado{$item->id}",
                                'class' => 'form-control',
                                'onkeyup' => "atualizarValorTotalLinha(this);atualizarMaskMoney17Decimals(this)",
                                'onchange' => "pushParaArrayGlobal(this, $index)",
                            ],
                            'value' => $value,
                            'wrapperAttributes' => [
                                'class' => ''
                            ]
                        ],
                        'crud' => $item
                    ]
                );
            })->addColumn('valortotal_faturado', function ($item) use ($actionMethod, $request, &$index) {
                if ($actionMethod == 'create') {
                    $value = $this->getValueItensFaturadosSaldoHistoricoItens($request, $item, 'valortotal_faturado', $index);
                } else {
                    $value = $this->getValueItensFaturadosContratoFaturaItens($request, $item, 'valortotal_faturado', $index);
                }

                return view(
                    'crud::fields.contratofaturas.text',
                    [
                        'field' => [
                            'name' => "valortotal_faturado[]",
                            'label' => '',
                            'prefix' => "R$",
                            'attributes' => [
                                'id' => "valortotal_faturado{$item->id}",
                                'class' => 'form-control valor-total-faturado-row',
                                'readonly' => 'readonly',
                                'onchange' => "atualizarMaskMoney2Decimals(this);calcularValorTotalFaturado();pushParaArrayGlobal(this, $index)",
                            ],
                            'value' => $value,
                            'wrapperAttributes' => [
                                'class' => ''
                            ]
                        ],
                        'fields_hiddens' => [
                            'id_contratofatura_itens' => [
                                'name' => 'id_contratofatura_itens[]',
                                'value' => $actionMethod == 'edit' ? $item->id : null
                            ],
                            'saldohistoricoitens_id' => [
                                'name' => 'saldohistoricoitens_id[]',
                                'value' => $actionMethod == 'create' ? $item->id : $item->saldohistoricoitem->id
                            ],
                        ],
                        'crud' => $item
                    ]
                );
            })->addColumn('paisfabricacao_id', function ($item) use ($actionMethod, $request, &$index) {
                $brasil = Paises::BRASIL;
                $desconhecido = Paises::DESCONHECIDO;

                $idMaterial = $this->retornaIdCodigoItem('Tipo CATMAT e CATSER', 'Material');

                if ($actionMethod == 'create') {
                    $value = $this->getValueItensFaturadosSaldoHistoricoItens($request, $item, 'paisfabricacao_id', $index);
                } else {
                    $value = $this->getValueItensFaturadosContratoFaturaItens($request, $item, 'paisfabricacao_id', $index);
                }

                $view = view(
                    'crud::fields.contratofaturas.select2_from_ajax_paisfabricacao',
                    [
                        'field' => [
                            'name' => "paisfabricacao_id[]",
                            'options' => Paises::orderBy(DB::raw("id != {$brasil}, id != {$desconhecido}, nome"))->pluck('nome', 'id')->toArray(),
                            'allows_null' => true,
                            'attributes' => [
                                'class' => 'form-control select2-ajax-paisfabricacao',
                                'onchange' => "pushParaArrayGlobal(this, $index)",
                            ],
                            'wrapperAttributes' => [
                                'class' => ''
                            ],
                            'value' => $value,
                        ],
                        'crud' => $item
                    ]
                );

                if ($actionMethod == 'create') {
                    if ($item->contratoItem->tipo_id == $idMaterial) {
                        return $view;
                    }
                } else {
                    if ($item->saldohistoricoitem->contratoItem->tipo_id == $idMaterial) {
                        return $view;
                    }
                }

                return 'Não se aplica';
            })->rawColumns(
                ['catmatseritem_id', 'quantidade_faturado', 'valorunitario_faturado', 'valortotal_faturado', 'paisfabricacao_id']
            )->setRowClass(function ($item) {
                return 'row-grid-itens-faturados';
            })
            ->make(true);
    }

    private function getValueItensFaturadosSaldoHistoricoItens($request, Saldohistoricoitem $item, $nomeCampo, $index)
    {
        //caso haja dados inseridos na grid pelo usuario
        if (!empty($request->arrCamposFaturados)) {
            if (isset($request->arrCamposFaturados[$index]) && isset($request->arrCamposFaturados[$index][$nomeCampo])) {
                return $request->arrCamposFaturados[$index][$nomeCampo];
            }
        }

        //caso caso haja old_values e caso o contratohistorico_id seja igual ao old_contratohistorico_id seta old value
        $arr_old_values_itens_faturados = json_decode($request->old_values_itens_faturados, true);
        if (isset($arr_old_values_itens_faturados['old_' . $nomeCampo][$index])) {
            return $arr_old_values_itens_faturados['old_' . $nomeCampo][$index];
        }

        //caso o campo seja valortotal_faturado inicia com o valor total contratado
        if ($nomeCampo === 'valorunitario_faturado') {
            $broken_number = explode('.', $item->valorunitario);

            if (isset($broken_number[1])) {
                return number_format($broken_number[0], 0, ',', '.') . ',' . $broken_number[1];
            }

            return '';
        }

        //caso seja um create retorna default value 0
        return 0;
    }

    /**
     * Retorna o value dos campos da aba de itens faturados
     * o index do $arr_old_values_itens_faturados é contratoitens_id
     * Ordem de prioridade:
     * 1 - Dados inseridos na grid pelo usuario
     * 2 - Old value
     * 3 - Dado do banco
     * @param $request
     * @param $item
     * @param $nomeCampo
     * @return mixed|string
     */
    private function getValueItensFaturadosContratoFaturaItens($request, ContratoFaturasItens $item, $nomeCampo, $index)
    {
        //caso caso haja old_values e caso o contratohistorico_id seja igual ao old_contratohistorico_id seta old value
        $arr_old_values_itens_faturados = json_decode($request->old_values_itens_faturados, true);
        if (isset($arr_old_values_itens_faturados['old_' . $nomeCampo][$index])) {
            return $arr_old_values_itens_faturados['old_' . $nomeCampo][$index];
        }

        //caso haja resultado do banco
        if ($item->$nomeCampo !== null) {
            //caso for valortotal_faturado adiciona mascara
            if ($nomeCampo === 'valortotal_faturado') {
                return number_format(
                    $item->valortotal_faturado,
                    2,
                    ',',
                    '.'
                );
            }
            //caso for valorunitario_faturado adiciona mascara
            if ($nomeCampo === 'valorunitario_faturado') {
                $broken_number = explode('.', $item->valorunitario_faturado);
                return number_format($broken_number[0], 0, ',', '.') . ',' . $broken_number[1];
            }

            return $item->$nomeCampo;
        }

        //caso o campo seja valortotal_faturado inicia com o valor total contratado
        if ($nomeCampo === 'valorunitario_faturado') {
            $broken_number = explode('.', $item->saldohistoricoitem->valorunitario);
            return number_format($broken_number[0], 0, ',', '.') . ',' . $broken_number[1];
        }

        //caso seja um create retorna default value 0
        return 0;
    }

    public function certidaoEstadual(Request $request)
    {
        $con = Contrato::find($request->contrato_id);

        $cnpj = $this->retornaSomenteNumeros($con->fornecedor->cpf_cnpj_idgener);

        $sicaf = new SICAF();

        $dados = $sicaf->regularidadeFiscalEconomicaFinanceira($cnpj);

        $codigo = $dados->codigoArquivoRegFiscalEst;
        $hash = $dados->hashArquivoRegFiscalEst;

        $filename = 'Certidão estadual ' . $con->fornecedor->cpf_cnpj_idgener;
        $arquivo = $sicaf->arquivo($codigo, $hash);

        return new Response($arquivo, 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="' . $filename . '"'
        ]);
    }

    public function certidaoMunicipal(Request $request)
    {
        $con = Contrato::find($request->contrato_id);

        $cnpj = $this->retornaSomenteNumeros($con->fornecedor->cpf_cnpj_idgener);

        $sicaf = new SICAF();

        $dados = $sicaf->regularidadeFiscalEconomicaFinanceira($cnpj);

        $codigo = $dados->codigoArquivoRegFiscalMun;
        $hash = $dados->hashArquivoRegFiscalMun;

        $filename = 'Certidão municipal ' . $con->fornecedor->cpf_cnpj_idgener;
        $arquivo = $sicaf->arquivo($codigo, $hash);

        return new Response($arquivo, 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="' . $filename . '"'
        ]);
    }
}
