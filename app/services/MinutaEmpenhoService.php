<?php

namespace App\services;

use App\Models\ContratoMinutaEmpenhoPivot;
use App\Models\EnviaDadosPncp;
use App\Models\MinutaEmpenho;
use App\Repositories\MinutaEmpenhoRepository;
use App\services\PNCP\PncpService;

class MinutaEmpenhoService
{
    protected $minutaEmpenhoRepository;
    protected $pncpService;

    public function __construct(MinutaEmpenhoRepository $minutaEmpenhoRepository, PncpService $pncpService)
    {
        $this->minutaEmpenhoRepository = $minutaEmpenhoRepository;
        $this->pncpService = $pncpService;
    }

    /**
     * Responsável em recuperar as minutas de empenho substitutiva de contrato
     *
     * issue 741
     * @return mixed
     */
    public function getMinutasModalCriarContratoTipoEmpenho()
    {
        $arrMinutaEmpenho = $this->minutaEmpenhoRepository->getMinutasModalCriarContratoTipoEmpenho();
        $arrResult = [];

        //pegar apenas as minutas que não estão associados a um contrato
        foreach ($arrMinutaEmpenho as $m) {
            if (ContratoMinutaEmpenhoPivot::where('minuta_empenho_id', $m['id'])->first()) {
                continue;
            }

            # Se existir registro na tabela responsável pelo envio para o PNCP e
            # arquivo foi assinado no SIAFI
            if (! ($this->pncpService->verificaExisteRegistroEnviaPncpMinutaEmpenho($m['id']))) {
                continue;
            }

            //741 - verificar se é 14133
            if ( $this->pncpService->podeEnviarMinutaEmpenhoPNCP($m)) {
                $dados_empenho = [
                    'ugemitente' => $m['codigo'],
                    'anoempenho' => substr($m['numero_empenho'], 0, 4),
                    'numempenho' => (int)substr($m['numero_empenho'], 6, 6),
                ];
                // verificar se tem registro na envia_dados_pncp e satatus diferente de ASNPEN(assinatura pendente)
                if(! ($this-> verificaArquivoMinutaEmpenho($dados_empenho))) {
                    continue;
                }
            }
            $arrResult[] = $m->toArray();
        }

        return $arrResult;
    }

    /**
     *
     * Verifica se o arquivo da minuta de empenho está salva na pasta storage
     *
     * @param array $dados_empenho
     * @param $retornaCaminho
     * @return bool|string
     */
    public function verificaArquivoMinutaEmpenho(Array $dados_empenho, $retornaCaminho = null)
    {
        $nome_arquivo = $dados_empenho['ugemitente'] . '_' . $dados_empenho['anoempenho'] . 'NE'
            .  str_pad($dados_empenho['numempenho'], 6, "0", STR_PAD_LEFT);
        $path = 'empenhos_pdf/'.$dados_empenho['ugemitente'] . '_' . $dados_empenho['anoempenho'] . 'NE'
            .  str_pad($dados_empenho['numempenho'], 6, "0", STR_PAD_LEFT).'/';

        if ($retornaCaminho) {
            return $path.$nome_arquivo.".pdf";
        }

        return file_exists(config('app.app_path')."storage/app/" . $path.$nome_arquivo . ".pdf");
    }
}
