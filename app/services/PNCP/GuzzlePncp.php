<?php

namespace App\services\PNCP;

use App\Models\PncpUsuario;
use App\Repositories\PncpUsuarioRepository;
use GuzzleHttp\Client;

class GuzzlePncp
{
    private static $client;


    public function getToken()
    {
        $pncpUsuario = PncpUsuario::query()
            ->where('expires', '>=', date('Y-m-d H:i:s'))
            ->first();

        if (is_null($pncpUsuario) or empty($pncpUsuario)) {
            return $this->login();
        }

        return $pncpUsuario->token;
    }

    public function getClient()
    {
        if (!isset($this->client)) {
            self::$client = new Client([
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => $this->getToken()
                ],
                'base_uri'=>env('API_PNCP_URL')
            ]);
        }
        return self::$client;
    }


    private function getResponseLogin()
    {
        $pncpUsuario = PncpUsuario::query()
            ->first();

        $body = [
            'login' => $pncpUsuario->login,
            'senha' => $pncpUsuario->senha
        ];
        $client = new Client();
        return $client->request('POST', env('API_PNCP_URL').'v1/usuarios/login', [
            'headers' => [
                'Content-Type' => 'application/json',
            ],
            'json' => $body,
        ]);
    }

    private function login()
    {
        $response = $this->getResponseLogin();
        (new PncpUsuarioRepository())->save(
            $response->getHeader('authorization')[0],
            $response->getHeader('expires')[0]
        );
        return $response->getHeader('authorization')[0];
    }
}
