<?php

namespace App\services\PNCP;

use App\Http\Controllers\Api\PNCP\DocumentoContratoController;
use App\Http\Controllers\Api\PNCP\DocumentoTermoContratoController;
use App\Http\Controllers\Api\PNCP\UsuarioController;
use App\Http\Traits\Formatador;
use App\Models\Codigoitem;
use App\Models\Contrato;
use App\Models\Contratoarquivo;
use App\Models\Contratohistorico;
use App\Models\EnviaDadosPncp;
use App\Models\MinutaEmpenho;
use App\services\AmparoLegalService;
use Illuminate\Support\Facades\Log;
use App\Http\Traits\EnviaPncpTrait;

class PncpService
{
    use Formatador;
    use EnviaPncpTrait;

    protected $amparoLegalService;
    protected $tiposNegadosPNCP = ['Credenciamento', 'Outros'];
    protected $tiposEnviadosAoPNCP = [
        'Arrendamento',
        'Carta Contrato',
        'Comodato',
        'Concessão',
        'Contrato',
        'Empenho',
        'Outros',
        'Termo Aditivo',
        'Termo de Adesão',
        'Termo de Apostilamento',
        'Termo de Rescisão'
    ];

    public function __construct(AmparoLegalService $amparoLegalService = null)
    {
        $this->amparoLegalService = $amparoLegalService;
    }

    public function podeEnviarContratoPNCP(
        array             $idAmparoLegal,
        Contratohistorico $contratohistorico,
        Contratohistorico $instrumentoInicial
    )
    {
        $tiposContratoAceitos = config('api-pncp.tipos_contrato_aceitos_pncp');
        $tiposTermoAceitos = config('api-pncp.tipos_termo_contrato_aceitos_pncp');
        # Obtém a descrição do tipo do cadastro que está sendo feito (pode ser contrato ou termo)
        $tipoDescricao = $contratohistorico->tipo->descricao;

        # Se o tipo for um TERMO aceito no PNCP
        if (in_array($tipoDescricao, $tiposTermoAceitos)) {
            # Verifica se o tipo do contrato pai não é aceito no PNCP
            if (!in_array($contratohistorico->contrato->tipo->descricao, $tiposContratoAceitos)) {
                return false;
            }
        }
        # Se o tipo do cadastro (independente se é contrato ou termo) não é aceito no PNCP
        if (!in_array($tipoDescricao, $tiposContratoAceitos) && !in_array($tipoDescricao, $tiposTermoAceitos)) {
            return false;
        }
        $amparoLegal14133 = $this->amparoLegalService->amparoLegalLei14133($idAmparoLegal);
        if (!$amparoLegal14133) {
            return false;
        }
        $unidadeSigilo = $instrumentoInicial->unidade->sigilo;
        if ($unidadeSigilo) {
            return false;
        }
        $anoContrato = explode('/', $instrumentoInicial->numero);
        //caso o contrato seja de uma minuta, pega os 4 primeiros digitos do ano
        $anoContrato = $anoContrato[1] ?? substr($instrumentoInicial->numero, 0, 4);
        if ($anoContrato < 2021) {
            return false;
        }
        return true;
    }

    public function podeEnviarMinutaEmpenhoPNCP(MinutaEmpenho $minuta)
    {
        $minutaLei14133 = $this->minutaEmpenhoLei14133($minuta);
        $minutaForcaContrato = $minuta->minutaempenho_forcacontrato;

        if ($minutaLei14133 && $minutaForcaContrato) {
            return true;
        }

        return false;
    }

    /**
     * @param int $contratoId
     * @return mixed
     */
    public function getInstrumentoInicial(int $contratoId)
    {
        return Contratohistorico::where('contrato_id', $contratoId)
            ->where('contratohistorico.elaboracao', false)->oldest()->first();
    }

    public function isTermoContrato(Contratohistorico $contratohistorico): bool
    {
        if (in_array($contratohistorico->tipo->descricao, Contrato::getDescricoesTermosDeContrato())) {
            return true;
        }
        return false;
    }

    /**
     * Verifica se a minuta tem registro na tabela envia_dados_pncp para
     * usar na busca no metodo recuperaMinutasModalCriarContratoTipoEmpenho
     * @return bool
     */
    public static function verificaExisteRegistroEnviaPncpMinutaEmpenho($minuta_id): bool
    {
        //confere se existe registro para a Minuta
        $registro_envia_pncp = EnviaDadosPncp::join(
            'codigoitens as cdi', function ($status) {
            $status->on('envia_dados_pncp.situacao', '=', 'cdi.id')
                ->where('descres', '!=', 'ASNPEN');
        })
            ->where([
                ['pncpable_type', MinutaEmpenho::class],
                ['pncpable_id', '=', $minuta_id],
            ])
            ->orderBy('envia_dados_pncp.created_at', 'ASC')
            ->first();

        if ($registro_envia_pncp) {
            return true;
        }

        return false;
    }

    public function verificarEnviarArquivoPNCP(array $idAmparoLegal, Contratohistorico $instrumentoInicial)
    {
        $amparoLegal14133 = $this->amparoLegalService->amparoLegalLei14133($idAmparoLegal);
        $unidadeSigilo = $instrumentoInicial->unidade->sigilo;
        
        if (!$amparoLegal14133) {
            return false;
        }
        
        # Obtem o ano do contrato
        $anoContrato = explode('/', $instrumentoInicial->numero);
        //caso o contrato seja de uma minuta, pega os 4 primeiros digitos do ano
        $anoContrato = $anoContrato[1] ?? substr($instrumentoInicial->numero, 0, 4);

        if ($amparoLegal14133 &&
            in_array($instrumentoInicial->tipo->descricao, $this->tiposEnviadosAoPNCP) &&
            !$unidadeSigilo && $anoContrato >= 2021
        ) {
            return true;
        }

        return false;
    }

    public function minutaEmpenhoLei14133(MinutaEmpenho $minuta)
    {
        $idAmparoLegal = $minuta->amparo_legal()->pluck('id')->toArray();

        $amparoLegal14133 = $this->amparoLegalService->amparoLegalLei14133($idAmparoLegal);

        $tipoEmpenhoDescres = $minuta->tipo_empenhopor->descres;
        $anoCompra = intval(explode('/', $minuta->compra->numero_ano)[1]);
        $unidadeSigilo = $minuta->unidade_id()->first()->sigilo;

        if ($amparoLegal14133 && $tipoEmpenhoDescres == 'COM' && $anoCompra >= 2021 &&
            !$unidadeSigilo) {
            return true;
        }

        return false;
    }

    public function preparaEnvioArquivoParaPncp($contratoArquivo, $tipo)
    {
        $tipoArquivo = Contrato::padronizaDescricaoTermoContrato($contratoArquivo->getTipo());
        # Se for um dos termos que vai para o PNCP, busca o registro específico do termo na tabela envia_dados_pncp
        if (Contrato::isTermoDeContrato($tipoArquivo)) {
            $pncp = EnviaDadosPncp::where([
                ['pncpable_id', '=', $contratoArquivo->contratohistorico_id],
                ['pncpable_type', '=', Contratohistorico::class],
                ['tipo_contrato', $tipoArquivo]
            ])->oldest()->first();
        } else { # Se for um contrato de qualquer tipo, busca o registro do contrato na tabela envia_dados_pncp
            $pncp = EnviaDadosPncp::where([
                ['contrato_id', '=', $contratoArquivo->contrato_id],
                ['pncpable_type', '=', Contratohistorico::class],
            ])->oldest()->first();
        }
        # Se não encontrou, tenta buscar um registro que seja do contrato pai, porém sem tipo
        if (!isset($pncp)) {
            $pncp = EnviaDadosPncp::where('pncpable_id', $contratoArquivo->contratohistorico_id)
                ->where('pncpable_type', Contratohistorico::class)->first();
        }
        # Se realmente não encontrou nenhum registro, ou é bug antigo ou o contrato não pertence ao amparo legal 14133
        # Neste caso, retorna sucesso para permitir que o sistema exclua o arquivo 
        if (!isset($pncp)) {
            $response = new \stdClass();
            $response->code = 200;
            $response->message = 'Arquivo excluído com sucesso.';
            return $response;
        }
        if (isset($pncp)) {
            # Se a situação está como SUCESSO ou Exclusão de arquivo, marca como ARQPEN
            if ($pncp->status->descres == 'SUCESSO' || $tipo == 'DELARQ') {
                $pncp->situacao = Codigoitem::whereHas('codigo', function ($q) {
                    $q->where('descricao', 'Situação Envia Dados PNCP');
                })->where('descres', 'ARQPEN')->first()->id;
                $pncp->save();
            }
            $contratoArquivo = Contratoarquivo::withTrashed()->find($contratoArquivo->id);
            $contratoArquivo->envio_pncp_pendente = $tipo;
            $contratoArquivo->saveWithoutEvents();

            $tipoArquivoValidoSuperSEI = Contratoarquivo::getTiposArquivoValidosSuperSEI();
            if (isset($contratoArquivo->link_sei) &&
                in_array($contratoArquivo->getTipo(), $tipoArquivoValidoSuperSEI) &&
                $tipo !== 'DELARQ') {
                try {
                    $contratoArquivo->salvaTempDocumentoSEI();
                } catch (Exception $e) {
                    Log::error($e);
                }
            }

            $uc = new UsuarioController();
            $authPncp = $this->realizarLoginPNCP($uc);
            if (!empty($authPncp->code) && $authPncp->code >= 400) {
                return $authPncp;
            }

            $dcc = new DocumentoContratoController();
            $dtcc = new DocumentoTermoContratoController();
            $response = $this->incluirArquivoPNCPPorContrato(
                $contratoArquivo->contrato_historico,
                $pncp, $contratoArquivo, $dcc, $dtcc);
            return $response;
        }
    }

    public function realizarLoginPNCP(UsuarioController $uc)
    {
        return $uc->login();
    }

    public function getEnviaDadosPncpPeloIdDoContratoArquivo($idContratoArquivo)
    {
        $contratoArquivo = Contratoarquivo::select('contratohistorico_id')
            ->withTrashed()
            ->find($idContratoArquivo);
        return EnviaDadosPncp::where('pncpable_id', $contratoArquivo->contratohistorico_id)->first();
    }

    public function gerarResponse($response, $code = null, $message = null)
    {
        return response()->json([
            'message' => $message ?? $response->message,
            'code' => $response->code
        ], $code ?? $response->code);
    }
}
