<?php

namespace App\services\PNCP;

use App\Models\Compra;
use App\Models\Contrato;
use App\Repositories\ContratoRepository;
use Illuminate\Support\Facades\Log;

class DataPncpContrato
{
    private $currentPage = 0;

    private $contrato;
    private $numeroContrato;

    //Dados da compra
    private $anoCompra;
    private $sequencialCompra;
    private $cnpjOrgaoCompra;
    public $dataContrato = null;


    public function __construct(Contrato $contrato)
    {
        $compra = (new ContratoRepository())->getContratoCompra($contrato->id);

        if (empty($compra)) {
            return;
        }

        $this->setCompra($compra);
        $this->contrato = $contrato;
        $this->setNumeroContrato();
    }

    public function search()
    {
        if (!isset($this->dataContrato)) {
            $this->currentPage++;
            $guzzlePncp = new GuzzlePncp();
            $client = $guzzlePncp->getClient();
            $response = $client->get(
                'v1/orgaos/' . $this->cnpjOrgaoCompra . '/contratos/contratacao/'
                    . $this->anoCompra . '/' . $this->sequencialCompra . '?pagina=' . $this->currentPage
            );
            $dataResponse = json_decode($response->getBody()->getContents());

            if (isset($dataResponse->data) and count($dataResponse->data) > 0) {
                foreach ($dataResponse->data as $item) {
                    if ($item->numeroContratoEmpenho == $this->numeroContrato &&
                        $item->unidadeOrgao->codigoUnidade == $this->contrato->unidadeorigem->codigo) {
                        $this->dataContrato = $item;
                        return $item;
                    }
                }
                if ($dataResponse->paginasRestantes > 0) {
                    $this->search();
                }
            }
            #$this->dataContrato = null;
        }
        return $this->dataContrato;
    }

    private function setNumeroContrato()
    {
        $numero = explode('/', $this->contrato->numero);
        $this->numeroContrato = $numero[0];
    }

    private function setCompra(Compra $compra)
    {
        $numeroAno = explode('/', $compra->numero_ano);
        $this->cnpjOrgaoCompra = $compra->cnpjOrgao;
        $this->sequencialCompra = $compra->id_unico;
        $this->anoCompra = (int)trim($numeroAno[1]);
    }

    /**
     * @return Contrato
     */
    public function getContrato(): Contrato
    {
        return $this->contrato;
    }

    /**
     * @return mixed
     */
    public function getNumeroContrato()
    {
        return $this->numeroContrato;
    }

    /**
     * @return mixed
     */
    public function getAnoCompra()
    {
        return $this->anoCompra;
    }

    /**
     * @return mixed
     */
    public function getSequencialCompra()
    {
        return $this->sequencialCompra;
    }

    /**
     * @return mixed
     */
    public function getCnpjOrgaoCompra()
    {
        return $this->cnpjOrgaoCompra;
    }

    public function getLinkCreator()
    {
        return new SequencialLinkCreator($this);
    }

}
