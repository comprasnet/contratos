<?php

namespace App\services\PNCP;
/**
 * Cria um link pncp a partir do sequencial
 */
class SequencialLinkCreator
{
    private $sequencial;

    public function __construct(DataPncpContrato $sequencial)
    {
        $this->sequencial = $sequencial;
    }

    public function create()
    {
        $dataContrato = $this->sequencial->search();
        if(!is_null($dataContrato)) {
            $parsedUrl = parse_url(env('API_PNCP_URL'));

            $baseDomain = $parsedUrl['scheme'] . '://' . $parsedUrl['host'];

            return $baseDomain . '/pncp-api/v1/orgaos/' . $this->sequencial->getCnpjOrgaoCompra()
                . '/contratos/' . $this->sequencial->getAnoCompra() . '/'
                . str_pad($dataContrato->sequencialContrato, 6, "0", STR_PAD_LEFT);
        }
        return null;
    }
}
