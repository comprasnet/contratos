<?php

namespace App\Observers;

use App\Models\ContratoAutoridadeSignataria;

class ContratoAutoridadeSignatariaObserver
{
    /**
     * Handle the contrato autoridade signataria "created" event.
     *
     * @param  \App\ContratoAutoridadeSignataria  $contratoAutoridadeSignataria
     * @return void
     */
    public function created(ContratoAutoridadeSignataria $contratoAutoridadeSignataria)
    {
        //
    }

    /**
     * Handle the contrato autoridade signataria "updated" event.
     *
     * @param  \App\ContratoAutoridadeSignataria  $contratoAutoridadeSignataria
     * @return void
     */
    public function updated(ContratoAutoridadeSignataria $contratoAutoridadeSignataria)
    {
        //
    }

    public function creating(ContratoAutoridadeSignataria $contratoAutoridadeSignataria)
    {


    }

    public function deleting(ContratoAutoridadeSignataria $contratoAutoridadeSignataria)
    {


    }


    /**
     * Handle the contrato autoridade signataria "deleted" event.
     *
     * @param  \App\ContratoAutoridadeSignataria  $contratoAutoridadeSignataria
     * @return void
     */
    public function deleted(ContratoAutoridadeSignataria $contratoAutoridadeSignataria)
    {
        //
        // echo 'deletou... estou no observe..';
        // exit;
    }

    /**
     * Handle the contrato autoridade signataria "restored" event.
     *
     * @param  \App\ContratoAutoridadeSignataria  $contratoAutoridadeSignataria
     * @return void
     */
    public function restored(ContratoAutoridadeSignataria $contratoAutoridadeSignataria)
    {
        //
    }

    /**
     * Handle the contrato autoridade signataria "force deleted" event.
     *
     * @param  \App\ContratoAutoridadeSignataria  $contratoAutoridadeSignataria
     * @return void
     */
    public function forceDeleted(ContratoAutoridadeSignataria $contratoAutoridadeSignataria)
    {
        //
    }
}
