<?php

namespace App\Observers;

use Alert;
use App\DTO\Termo14133PNCDTO;
use App\Http\Controllers\Api\PNCP\ContratoControllerPNCP;
use App\Http\Controllers\Api\PNCP\DocumentoContratoController;
use App\Http\Controllers\Api\PNCP\DocumentoTermoContratoController;
use App\Http\Controllers\Api\PNCP\TermoContratoController;
use App\Http\Controllers\Api\PNCP\UsuarioController;
use App\Http\Controllers\Empenho\CompraSiasgCrudController;
use App\Models\MinutaEmpenho;
use App\Repositories\Termos14133PncpRepository;
use App\services\PNCP\PncpService;
use Backpack\CRUD\app\Http\Controllers\CrudController;

use App\Http\Controllers\Publicacao\DiarioOficialClass;
use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\DiarioOficial;
use App\Http\Traits\EnviaPncpTrait;
use App\Http\Traits\Formatador;
use App\Http\Traits\ValidarTermoPNCP;
use App\Models\BackpackUser;
use App\Models\CalendarEvent;
use App\Models\Codigoitem;
use App\Models\Compra;
use App\Models\Contrato;
use App\Models\Contratoarquivo;
use App\Models\Contratocronograma;
use App\Models\Contratoempenho;
use App\Models\Contratohistorico;
use App\Models\ContratoHistoricoMinutaEmpenho;
use App\Models\Contratoitem;
use App\Models\ContratoMinuta;
use App\Models\ContratoPublicacoes;
use App\Models\EnviaDadosPncp;
use App\Models\Saldohistoricoitem;
use App\User;
use DateTime;
use Doctrine\DBAL\Schema\AbstractAsset;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Redirect;
use Route;


class ContratohistoricoObserve
{

    use BuscaCodigoItens;
    use Formatador;
    use DiarioOficial;
    use ValidarTermoPNCP;
    use EnviaPncpTrait;

    private $request;
    private $pncpService;

    public function __construct(Contratocronograma $contratocronograma, Request $request, PncpService $pncpService)
    {
        $this->contratocronograma = $contratocronograma;
        $this->request = $request;
        $this->pncpService = $pncpService;
    }

    /**
     * Handle the contratohistorico "created" event.
     *
     * @param Contratohistorico $contratohistorico
     * @return void
     */
    public function created(Contratohistorico $contratohistorico)
    {
        // ContratoHistorico Ativo
        if ($contratohistorico->elaboracao != true) {
            $this->vincular($contratohistorico);
        }
    }

    /**
     * Handle the contratohistorico "updated" event.
     *
     * @param Contratohistorico $contratohistorico
     * @return void
     */
    public function updated(Contratohistorico $contratohistorico)
    {
        $podeEnviarPNCP = null;
        // Edição no ContratoHistorico Rascunho (elaboracao true)
        // aqui está ativando o termo
        if ($contratohistorico->elaboracao == false
            && $contratohistorico->wasChanged('elaboracao')
            && $contratohistorico->getOriginal('elaboracao') !== false) {
                $this->vincular($contratohistorico);
            }

        // Edição no ContratoHistorico Ativo (elaboracao false)
        if ($contratohistorico->getOriginal('elaboracao') != true) {
            $historico = Contratohistorico::where('contrato_id', $contratohistorico->contrato_id)
                ->orderBy('data_assinatura', 'ASC')
                ->get();

            if($historico[0]->aplicavel_decreto == null){
                $historico[0]->aplicavel_decreto = false;
            }

            $cronograma = Contratocronograma::where('contrato_id', $contratohistorico->contrato_id)
                ->delete();
            $this->contratocronograma->atualizaCronogramaFromHistorico($historico);

            $this->atualizaContrato($historico, $contratohistorico);
            $this->atualizaMinutasContrato($contratohistorico);
            $this->atualizaFornecedoresSubcontratadosContrato($contratohistorico);
            $this->createEventCalendar($contratohistorico);
            $contratohistoricoBase = $this->pncpService->getInstrumentoInicial($contratohistorico->contrato_id);
            $amparoLegalId = $contratohistoricoBase->amparolegal->pluck('id')->toArray();

            $isTermoContrato = $this->pncpService->isTermoContrato($contratohistorico);
            if (!$isTermoContrato) {
                foreach ($contratohistorico->amparolegal as $amparoLegal) {
                    if ($amparoLegal->getAmparoLegalEnviarPncp() === "Sim") {
                        $podeEnviarPNCP =
                            $this->pncpService->podeEnviarContratoPNCP($amparoLegalId, $contratohistorico, $contratohistoricoBase);
                        break;
                    }
                }
            } else {
                $podeEnviarPNCP = false;

                if ($contratohistorico->contrato->modalidade->descres != 'NAOSEAPLIC') {
                    $podeEnviarPNCP = $this->pncpService->podeEnviarContratoPNCP(
                        $amparoLegalId,
                        $contratohistorico,
                        $contratohistoricoBase
                    );
                }
            }

            if ($podeEnviarPNCP) {
                $this->preparaEnvioPNCP(
                    $contratohistorico->id,
                    $contratohistorico->contrato_id,
                    $contratohistorico->tipo->descricao,
                    $this->request->minutasempenho
                );
                $this->enviarSuperSEIArquivoPNCP($contratohistorico);
            } else {
                $this->verificaPNCP($contratohistorico->contrato_id, $contratohistorico->id);
            }

            //-------------------------------------------JOB-----------------------------------------------------------
            if ($contratohistorico->publicado && ($contratohistorico->publicacao->count() == 0)) {
                $this->executaAtualizacaoViaJob($contratohistorico);
                return true;
            }
            //-------------------------------------------------------------------------------------------------------------
            if (!is_null($contratohistorico->data_publicacao) && !is_null($contratohistorico->publicacao) && ($contratohistorico->publicado != true)) {
                if ($this->booCampoPublicacaoAlterado($contratohistorico)) {
                    $this->trataAtualizacaoPublicacoes($contratohistorico);
                }
            }
        }

    }

    private function trataAtualizacaoPublicacoes($contratohistorico)
    {
        $sisg = (isset($contratohistorico->unidade->sisg)) ? $contratohistorico->unidade->sisg : '';
        $cpf = $this->removeMascaraCPF(backpack_user()->cpf);
        if(($contratohistorico->publicacao->count() == 0)){
            $this->criaNovaPublicacao($contratohistorico,$cpf,true);
            return true;
        }

        if(($contratohistorico->publicacao->count() == 1)){
            $publicacao = ContratoPublicacoes::where('contratohistorico_id',$contratohistorico->id)->first();
            $this->verificaStatusPublicacao($publicacao,$contratohistorico,$sisg,$cpf);
            return true;
        }

        if(($contratohistorico->publicacao->count() > 1)){
            $publicacao = ContratoPublicacoes::where('contratohistorico_id',$contratohistorico->id)->latest()->first();
            $this->verificaStatusPublicacao($publicacao,$contratohistorico,$sisg,$cpf);
            return true;
        }

    }

    public function verificaStatusPublicacao($publicacao,$contratohistorico,$sisg,$cpf)
    {
        $importado = $this->verificaPublicacaoImportada($publicacao,$contratohistorico,$sisg,$cpf);

        if(!$importado) {

            switch ($publicacao->status_publicacao_id) {
                case $this->retornaIdCodigoItem('Situacao Publicacao', 'PUBLICADO'):
                    $this->criaRetificacao($contratohistorico, $sisg,$cpf);
                    break;
                case $this->retornaIdCodigoItem('Situacao Publicacao', 'A PUBLICAR'):
                case $this->retornaIdCodigoItem('Situacao Publicacao', 'TRANSFERIDO PARA IMPRENSA'):
                    $this->verificaStatusOnline($publicacao, $contratohistorico, $sisg,$cpf);
                    break;
                default;
                    //todo verficar porque não redireciona.
                    return redirect('/gescon/contrato/' . $contratohistorico->contrato_id . '/publicacao');
            }
        }
    }

    public function verificaPublicacaoImportada($publicacao,$contratohistorico,$sisg,$cpf)
    {
        $retorno = false;

        $publicado = $this->retornaIdCodigoItem('Situacao Publicacao', 'PUBLICADO');

        if (($publicacao->status == "Importado") && ($publicacao->status_publicacao_id == $publicado)) {

            $this->criaRetificacao($contratohistorico,$sisg,$cpf);

            $retorno = true;
        }
        return $retorno;
    }

    private function executaAtualizacaoViaJob($contratohistorico)
    {
        $publicado_id = $this->retornaIdTipoPublicado();

        $publicacao = ContratoPublicacoes::Create(
            [
                'contratohistorico_id' => $contratohistorico->id,
                'status_publicacao_id' => $publicado_id,
                'data_publicacao' => $contratohistorico->data_publicacao,
                'texto_dou' => '',
                'status' => 'Importado',
                'tipo_pagamento_id' => $this->retornaIdCodigoItem('Forma Pagamento', 'Isento'),
                'motivo_isencao' => 0
            ]
        );
        return $publicacao;
    }

    public function deleting(Contratohistorico $contratohistorico)
    {
        try{

            // se o histórico deletado for um termo de encerramento, precisaremos tratar os dados do contrato
            @$tipoHistorico = $contratohistorico->tipo->descricao;

            if(@$tipoHistorico == 'Termo de Encerramento'){
                $contratohistorico->atualizarContratoDelecaoTermoDeEncerramento();
            } elseif(@$tipoHistorico == 'Termo de Rescisão'){
                $contratohistorico->atualizarContratoDelecaoTermoDeRescisao();
            }

            $contratohistoricoBase = $this->pncpService->getInstrumentoInicial($contratohistorico->contrato_id);
            $amparoLegalId = $contratohistoricoBase->amparolegal->pluck('id')->toArray();

            $podeEnviarPNCP =
                $this->pncpService->podeEnviarContratoPNCP($amparoLegalId, $contratohistorico, $contratohistoricoBase);

            if($podeEnviarPNCP){
                $this->preparaExclusaoPNCP($contratohistorico->contrato_id, $contratohistorico->id);
            }

        }catch(Exception $q){
            Log::error($q);
        }
    }

    /**
     * Handle the contratohistorico "deleted" event.
     *
     * @param Contratohistorico $contratohistorico
     * @return void
     */
    public function deleted(Contratohistorico $contratohistorico)
    {
        $historico = Contratohistorico::where('contrato_id', '=', $contratohistorico->contrato_id)
            ->orderBy('data_assinatura', 'ASC')
            ->get();

        $cronograma = Contratocronograma::where('contrato_id', $contratohistorico->contrato_id)
            ->delete();

        $this->contratocronograma->atualizaCronogramaFromHistorico($historico);
        $this->atualizaContrato($historico, $contratohistorico);

        //percorre o histórico
        foreach ($historico as $h) {
            //se o id for diferente do que está sendo apagado
            if ($h->id != $contratohistorico->id ) {
                //busca o Saldohistorico itens
                $shi = Saldohistoricoitem::where('saldoable_id', $h->id)->get();
                //verificar se tem valores no array
                if (sizeof($shi) > 0) {
                    foreach ($shi as $s) {
                        //encontrar o item para atualizar valores
                        $item = Contratoitem::findOrFail($s->contratoitem_id);
                        if ($item) {
                            $item->quantidade  = $s->quantidade;
                            $item->valorunitario = $s->valorunitario;
                            $item->valortotal = $s->valortotal;
                            $item->periodicidade = $s->periodicidade;
                            $item->data_inicio = $s->data_inicio;
                            $item->numero_item_compra = $s->numero_item_compra;
                            $item->update();
                        }
                    }
                }
            }
        }

        @$tipoHistorico = $contratohistorico->tipo->descricao;
        if(@$tipoHistorico == 'Termo de Encerramento'){
            // tentativa de redirecionar para a listagem de contratos, mas não está indo.
            $linkLocation = '/gescon/contrato/';

            return Redirect::to($linkLocation);
        }
    }

    /**
     * Handle the contratohistorico "restored" event.
     *
     * @param Contratohistorico $contratohistorico
     * @return void
     */
    public function restored(Contratohistorico $contratohistorico)
    {
        //
    }

    /**
     * Handle the contratohistorico "force deleted" event.
     *
     * @param Contratohistorico $contratohistorico
     * @return void
     */
    public function forceDeleted(Contratohistorico $contratohistorico)
    {
        //
    }

    private function atualizaContrato($contratohistorico, $contratohistoricoatual = null)
    {
        foreach ($contratohistorico as $h) {
            $contrato_id = $h->contrato_id;
            $arrayhistorico = $h->toArray();

            $tipo = Codigoitem::find($arrayhistorico['tipo_id']);

            if ($tipo instanceof Codigoitem) {
                $array = $this->retornaArrayContratoHistorico($tipo, $arrayhistorico, $contrato_id, $contratohistoricoatual);

                $contrato = new Contrato();
                $contrato->atualizaContratoFromHistorico($contrato_id, $array);
            }
        }
    }

    public function retornaArrayContratoHistorico(Codigoitem $tipo, array $arrayhistorico, $contrato_id, $contratohistoricoatual = null)
    {
        switch ($tipo->descricao) {
            case 'Termo de Encerramento':
                return $this->retornaArrayEncerramento($arrayhistorico);
                break;
            case 'Termo de Rescisão':
                return $this->retornaArrayRescisao($arrayhistorico);
                break;
            case 'Termo Aditivo':
                return $this->retornaArrayAditivo($arrayhistorico, $contrato_id, $contratohistoricoatual);
                break;
            case 'Termo de Apostilamento':
                return $this->retornaArrayApostilamento($arrayhistorico);
                break;
            default:
                return $this->retornaArrayDefault($arrayhistorico);
        }
    }

    public function retornaArrayAditivo(array $arrayhistorico, $contrato_id, $contratohistoricoatual = null)
    {
        $novo_valor = $arrayhistorico['valor_global'];

        if ($arrayhistorico['supressao'] == 'S') {
            $contrato = Contrato::find($contrato_id);
            $novo_valor = $contrato->valor_global - $novo_valor;
        }

        $historico = Contratohistorico::find($arrayhistorico['id']);

        $arrayAditivo = [
            'fornecedor_id' => $arrayhistorico['fornecedor_id'],
            'unidade_id' => $arrayhistorico['unidade_id'],
            'info_complementar' => $arrayhistorico['info_complementar'],
            'valor_global' => $novo_valor,
            'num_parcelas' => $arrayhistorico['num_parcelas'],
            'valor_parcela' => $arrayhistorico['valor_parcela'],
            'publicado' =>  $arrayhistorico['publicado'],
        ];
        (isset($arrayhistorico['situacao'])) ? $arrayAditivo['situacao'] = $arrayhistorico['situacao'] : "";

        if(isset($historico->qualificacoes) && $historico->id != $contratohistoricoatual->id){

            foreach($historico->qualificacoes as $qualificacao) {
                if($qualificacao->descricao == "VIGÊNCIA") {
                    //coloque a nova data de vigência fim no aditivo
                    $arrayAditivo['vigencia_fim'] = $arrayhistorico['vigencia_fim'];

                }
            }
        }elseif(sizeof($this->request->qualificacoes) > 0) {
            $qualificacoes = $this->request->qualificacoes;

            foreach($qualificacoes as $qualificacao) {
                if(Codigoitem::find($qualificacao)->descricao == "VIGÊNCIA") {
                    //coloque a nova data de vigência fim no aditivo
                    $arrayAditivo['vigencia_fim'] = $this->request->vigencia_fim;

                }
            }
        }

        return $arrayAditivo;
    }

    public function retornaArrayApostilamento(array $arrayhistorico)
    {
        $arrayApostilamento = [
            'unidade_id' => $arrayhistorico['unidade_id'],
            'valor_global' => $arrayhistorico['valor_global'],
            'num_parcelas' => $arrayhistorico['num_parcelas'],
            'valor_parcela' => $arrayhistorico['valor_parcela'],
            'publicado' =>  $arrayhistorico['publicado'],
        ];
        (isset($arrayhistorico['situacao'])) ? $arrayApostilamento['situacao'] = $arrayhistorico['situacao'] : "";
        return $arrayApostilamento;
    }

    public function retornaArrayRescisao(array $arrayhistorico)
    {
        $arrayRescisao = [
            'vigencia_fim' => $arrayhistorico['vigencia_fim'],
            'situacao' => $arrayhistorico['situacao'],
            'publicado' =>  $arrayhistorico['publicado'],
        ];
        return $arrayRescisao;
    }

    // é aqui que vai fazer salvar em contrato, os dados do termo de encerramento
    public function retornaArrayEncerramento(array $arrayhistorico)
    {
        $contrato = $arrayhistorico['contrato']??(new Contrato())->toArray();
        $countPost = count($_POST);
        if($countPost > 0){
            // para os dois casos abaixo, é possível ser true, false ou null, que é não aplicável
            $isSaldoContaVinculadaLiberado = $_POST['is_saldo_conta_vinculada_liberado']??$contrato['is_saldo_conta_vinculada_liberado'];
            if($isSaldoContaVinculadaLiberado == 2 || $isSaldoContaVinculadaLiberado == ''){$isSaldoContaVinculadaLiberado = null;}     // 2 quer dizer que é não aplicável
            $isGarantiaContratualDevolvida = $_POST['is_garantia_contratual_devolvida']??$contrato['is_garantia_contratual_devolvida'];
            if($isGarantiaContratualDevolvida == 2){$isGarantiaContratualDevolvida = null;}     // 2 quer dizer que é não aplicável
            $dataEncerramento = $_POST['data_encerramento']??$contrato['data_encerramento'];
            $isObjetoContratualEntregue = $_POST['is_objeto_contratual_entregue']??$contrato['is_objeto_contratual_entregue'];
            $isCumpridasObrigacoesFinanceiras = $_POST['is_cumpridas_obrigacoes_financeiras']??$contrato['is_cumpridas_obrigacoes_financeiras'];
            $consecucaoObjetivosContratacao = $_POST['consecucao_objetivos_contratacao']??$contrato['consecucao_objetivos_contratacao'];
            // buscar dados do responsável signatário encerramento
            $userIdResponsavelSignatarioEncerramento = $_POST['user_id_responsavel_signatario_encerramento']??$contrato['user_id_responsavel_signatario_encerramento']??null;
            if( !is_null( $userIdResponsavelSignatarioEncerramento ) ){
                $objUser = User::find($userIdResponsavelSignatarioEncerramento);
                $user_id_responsavel_signatario_encerramento = $objUser->id;
                $nome_responsavel_signatario_encerramento = $objUser->name;
                $cpf_responsavel_signatario_encerramento = $objUser->cpf;
            } else {
                $user_id_responsavel_signatario_encerramento = null;
                $nome_responsavel_signatario_encerramento = null;
                $cpf_responsavel_signatario_encerramento = null;
            }
        } else {
            // aqui quer dizer que o Termo de Encerramento está sendo excluído - vamos tornar nulos os valores inseridos no ato do cadastro
            $isSaldoContaVinculadaLiberado = null;
            $isGarantiaContratualDevolvida = null;
            $dataEncerramento = null;
            $isObjetoContratualEntregue = false;    // esse é not null
            $isCumpridasObrigacoesFinanceiras = false;  // esse é not null
        }
        $arrayEncerramento = [
            'vigencia_fim' => $arrayhistorico['vigencia_fim'],
            'situacao' => $arrayhistorico['situacao'],
            'publicado' =>  $arrayhistorico['publicado'],
            'data_encerramento' => $dataEncerramento,
            'is_objeto_contratual_entregue' => $isObjetoContratualEntregue,
            'is_cumpridas_obrigacoes_financeiras' => $isCumpridasObrigacoesFinanceiras,
            'is_garantia_contratual_devolvida' => $isGarantiaContratualDevolvida,
            'is_saldo_conta_vinculada_liberado' => $isSaldoContaVinculadaLiberado,
            'consecucao_objetivos_contratacao' => @$consecucaoObjetivosContratacao,
            'user_id_responsavel_signatario_encerramento' => @$user_id_responsavel_signatario_encerramento,
            'nome_responsavel_signatario_encerramento' => @$nome_responsavel_signatario_encerramento,
            'cpf_responsavel_signatario_encerramento' => @$cpf_responsavel_signatario_encerramento,
        ];

        return $arrayEncerramento;
    }

    public function retornaArrayDefault(array $arrayhistorico)
    {
        unset($arrayhistorico['id']);
        unset($arrayhistorico['contrato_id']);
        unset($arrayhistorico['observacao']);
        unset($arrayhistorico['created_at']);
        unset($arrayhistorico['updated_at']);
        unset($arrayhistorico['retroativo']);
        unset($arrayhistorico['retroativo_mesref_de']);
        unset($arrayhistorico['retroativo_anoref_de']);
        unset($arrayhistorico['retroativo_mesref_ate']);
        unset($arrayhistorico['retroativo_anoref_ate']);
        unset($arrayhistorico['retroativo_vencimento']);
        unset($arrayhistorico['retroativo_valor']);
        unset($arrayhistorico['retroativo_soma_subtrai']);
        unset($arrayhistorico['is_com_valor']);
        unset($arrayhistorico['contrato']);

        $arrayDefault = array_filter($arrayhistorico, function ($a) {
            return trim($a) !== "";
        });

        if($arrayhistorico['is_prazo_indefinido'] == '1' or $arrayhistorico['is_prazo_indefinido'] == true)
        {
            $arrayDefault['vigencia_fim'] = null;
            $arrayDefault['num_parcelas'] = 1;
        }

        if($arrayhistorico['is_prazo_indefinido'] == '0' or $arrayhistorico['is_prazo_indefinido'] == false)
        {
            $arrayDefault['is_prazo_indefinido'] = false;
        }

        if (isset($arrayhistorico['situacao'])) {
            $arrayDefault['situacao'] = $arrayhistorico['situacao'];
        }
        if (isset($arrayhistorico['aplicavel_decreto'])) {
            $arrayDefault['aplicavel_decreto'] = $arrayhistorico['aplicavel_decreto'];
        }
        return $arrayDefault;
    }

    public function createEventCalendar(Contratohistorico $contratohistorico)
    {
        $contrato = Contrato::find($contratohistorico->contrato_id);

        $fornecedor = $contrato->fornecedor->cpf_cnpj_idgener . ' - ' . $contrato->fornecedor->nome;
        $ug = $contrato->unidade->codigo . ' - ' . $contrato->unidade->nomeresumido;

        $tituloinicio = 'Início Vigência Contrato: ' . $contrato->numero . ' Fornecedor: ' . $fornecedor . ' da UG: ' . $ug;
        $titulofim = 'Fim Vigência Contrato: ' . $contrato->numero . ' Fornecedor: ' . $fornecedor . ' da UG: ' . $ug;

        $events = [
            [
                'title' => $tituloinicio,
                'start_date' => new DateTime($contrato->vigencia_inicio),
                'end_date' => new DateTime($contrato->vigencia_inicio),
                'unidade_id' => $contrato->unidade_id
            ],
            [
                'title' => $titulofim,
                'start_date' => new DateTime($contrato->vigencia_fim),
                'end_date' => new DateTime($contrato->vigencia_fim),
                'unidade_id' => $contrato->unidade_id
            ]

        ];

        foreach ($events as $e) {
            $calendario = new CalendarEvent();
            $calendario->insertEvents($e);
        }

        return $calendario;
    }

    /**
     * Criar as vinculações do contrato histórico ativo
     *
     * @param Contratohistorico $contratohistorico
     * @return true|void
     */
    public function vincular(Contratohistorico $contratohistorico)
    {
        $podeEnviarPNCP = null;
        // vincula o amparo_legal ao contratohistorico
        $contratohistorico->vincularAmparoLegalContratoHistorico($this->request);

        $dataPublicacao = $contratohistorico->data_publicacao;
        $dataAtual = date('Y-m-d');

        $historico = Contratohistorico::where('contrato_id', $contratohistorico->contrato_id)
            ->where('contratohistorico.elaboracao', false)
            ->orderBy('data_assinatura', 'ASC')
            ->get();

        // Caso o usuario selecione para incluir o cronograma de forma automatica
        if ($contratohistorico->cronograma_automatico) {
            $this->contratocronograma->inserirCronogramaFromHistorico($contratohistorico);
        }

        $this->atualizaContrato($historico, $contratohistorico);
        $this->createEventCalendar($contratohistorico);

        $tipoOutros = $this->retornaIdCodigoItem('Tipo de Contrato', 'Outros');
        $contratohistoricoBase = $this->pncpService->getInstrumentoInicial($contratohistorico->contrato_id);
        $amparoLegalId = $contratohistoricoBase->amparolegal->pluck('id')->toArray();
        $isTermoContrato = $this->pncpService->isTermoContrato($contratohistorico);

        // Verifica se tem pelo menos um amparo legal que deve ser enviado pro PNCP selecionado no contrato
        if (!$isTermoContrato) {
            foreach ($contratohistorico->amparolegal as $amparoLegal) {
                if ($amparoLegal->getAmparoLegalEnviarPncp() === "Sim") {
                    $podeEnviarPNCP =
                        $this->pncpService->podeEnviarContratoPNCP($amparoLegalId, $contratohistorico, $contratohistoricoBase);
                    break;
                }
            }
        } else {
            $podeEnviarPNCP = false;
            if ($contratohistorico->contrato->modalidade->descres != 'NAOSEAPLIC') {
                $podeEnviarPNCP = $this->pncpService->podeEnviarContratoPNCP(
                    $amparoLegalId,
                    $contratohistorico,
                    $contratohistoricoBase
                );
            }
        }
        if ($podeEnviarPNCP) {
            $minutaId = null;
            # Dependendo de onde vem a requisição, a minuta vinculada vem em variáveis diferentes
            # [Adicionar Contrato] $this->request->minutasempenho
            # [Criar Contratos do tipo Empenho] $this->request->minuta
            # Ação de Ativar: não vem minuta, é preciso buscar as minutas através do contrato
            if (isset($this->request->minutasempenho) && isset($this->request->minutasempenho[0])) {
                $minutaId = $this->request->minutasempenho[0];
            } elseif (isset($this->request->minuta) && isset($this->request->minuta[0])) {
                $minutaId = $this->request->minuta[0];
            } else {
                $contrato = Contrato::find($contratohistorico->contrato_id);
                if ($contrato->minutasempenho->isNotEmpty() && isset($contrato->minutasempenho[0])) {
                    $minutaId = $contrato->minutasempenho[0]->id;
                }
            }
            $envioRealizado = $this->preparaEnvioPNCP(
                $contratohistorico->id,
                $contratohistorico->contrato_id,
                $contratohistorico->tipo->descricao,
                $minutaId
            );
            $this->enviarSuperSEIArquivoPNCP($contratohistorico);
            try {
                $uc = new UsuarioController();
                $this->realizarLoginPNCP($uc);

                $enviaDadosPNCP = EnviaDadosPncp::where('pncpable_type', ContratoHistorico::class)
                    ->where('pncpable_id', $contratohistorico->id)->oldest()->first();

                if ($isTermoContrato) {
                    (new Termos14133PncpRepository(
                        new Termo14133PNCDTO(
                            $contratohistorico->id,
                            $contratohistorico->tipo_id,
                            $contratohistorico->contrato_id
                        )
                    ))->enviarTermo14133Pncp();
                } else if (!$envioRealizado) { # Se for contrato e ainda não foi enviado para o PNCP
                    $cc = new ContratoControllerPNCP();
                    $cscc = new CompraSiasgCrudController();
                    $dcc = new DocumentoContratoController();
                    $dtcc = new DocumentoTermoContratoController();
                    $this->inserirContratoPNCP($contratohistorico, $enviaDadosPNCP, $cc, $dcc, $dtcc, $cscc);
                }

            } catch (Exception $ex) {
                $enviaDadosPNCP->retorno_pncp = $ex->getMessage();
                $enviaDadosPNCP->save();
            }
        }
        if ($contratohistorico->tipo_id != $tipoOutros) {
            if ($contratohistorico->publicado) {
                $this->executaAtualizacaoViaJob($contratohistorico);
                return true;
            }
            if ($contratohistorico->publicado != true) {
                // Quando a Data de Publicação for anterior à data atual, não deve ser gerada Minuta de Publicação
                if (strtotime($dataAtual) < strtotime($dataPublicacao)) {
                    // aqui quer dizer que a data de publicação é posterior à data atual - podemos gerar a minuta de publicação
                    $this->criaNovaPublicacao($contratohistorico, $this->removeMascaraCPF(backpack_user()->cpf), true);
                }
            }
        }
    }

    private function getSituacao($sisg, $data = null,$create = false, $lei14 = false)
    {
        $situacao = Codigoitem::whereHas('codigo', function ($query) {
            $query->where('descricao', 'Situacao Publicacao');
        })
            ->select('codigoitens.id');

        if ($data === null && $lei14 === true) {
            return $situacao->where('descricao', 'A PUBLICAR')->first();
        }

        if($create) {
            $data = Carbon::createFromFormat('Y-m-d', $data);
            if ($data->lte(Carbon::now())) {
                return $situacao->where('descricao', 'PUBLICADO')->first();
            }
        }
        if ($sisg) {
            return $situacao->where('descricao', 'A PUBLICAR')->first();
        }
        return $situacao->where('descricao', 'INFORMADO')->first();
    }

    private function retornaIdTipoPublicado()
    {
        return Codigoitem::whereHas('codigo', function ($query) {
            $query->where('descricao', 'Situacao Publicacao');
        })->where('descricao', 'PUBLICADO')->first()->id;
    }

    private function verificaStatusOnline($publicacao,$contratohistorico,$sisg,$cpf)
    {
        $diarioOficial = new DiarioOficialClass();
        $diarioOficial->atualizaStatusPublicacao($publicacao);

        (!is_null($publicacao->materia_id))
            ? $this->statusTransferidoParaImprensa($publicacao, $cpf, $contratohistorico, $sisg)
            : $this->statusAPublicar($publicacao,$cpf,$contratohistorico,$sisg);
    }

    private function statusTransferidoParaImprensa($publicacao,$cpf,$contratohistorico,$sisg)
    {
        $devolvido = $this->retornaIdCodigoItem('Situacao Publicacao', 'DEVOLVIDO PELA IMPRENSA');
        $sustada = $this->retornaIdCodigoItem('Situacao Publicacao', 'MATERIA SUSTADA');
        $publicado = $this->retornaIdCodigoItem('Situacao Publicacao', 'PUBLICADO');
        $transferido = $this->retornaIdCodigoItem('Situacao Publicacao', 'TRANSFERIDO PARA IMPRENSA');

        if ($publicacao->status_publicacao_id == $transferido) {
            $diarioOficial = new DiarioOficialClass();
            $retorno = $diarioOficial->sustaMateriaPublicacao($publicacao);

            if ($retorno->out->validaSustacao == "OK") {

                $publicacao->status = 'MATERIA SUSTADA';
                $publicacao->status_publicacao_id = $sustada;
                $publicacao->save();

                $statusPublicacao = ContratoPublicacoes::where('contratohistorico_id', $contratohistorico->id)
                    ->where('status_publicacao_id', $publicado)->first();

                //se houver alguma publicação com status publicado para esse instrumento CRIA RETIFICAÇÃO senão CRIANOVAPUBLICACAO
                (!is_null($statusPublicacao)) ? $this->criaRetificacao($contratohistorico, $sisg,$cpf)
                    : $this->criaNovaPublicacao($contratohistorico,$cpf);

            } else {
                $publicacao->status = 'ERRO AO TENTAR SUSTAR MATERIA';
                $publicacao->log = $retorno->out->validaSustacao;
                $publicacao->status_publicacao_id = $devolvido;
                $publicacao->save();
            }
        }
    }

    private function statusAPublicar($publicacao,$cpf,$contratohistorico,$sisg)
    {
        $publicado = $this->retornaIdCodigoItem('Situacao Publicacao', 'PUBLICADO');
        $statusPublicacao = ContratoPublicacoes::where('contratohistorico_id',$contratohistorico->id)
            ->where('status_publicacao_id',$publicado)->first();

        (!is_null($statusPublicacao)) ? $this->criaRetificacao($contratohistorico,$sisg,$cpf) : $this->criaNovaPublicacao($contratohistorico,$cpf);

    }

    private function atualizaMinutasContrato($contratohistorico)
    {
        // tipos que são permitidos manipular as minutas de empenho do contrato
        $tiposPermitidos = Codigoitem::whereHas('codigo', function ($query) {
            $query->where('descricao', '=', 'Tipo de Contrato');
        })
            ->where('descricao', '<>', 'Termo Aditivo')
            ->where('descricao', '<>', 'Termo de Apostilamento')
            ->where('descricao', '<>', 'Termo de Rescisão')
            ->where('descricao', '<>', 'Termo de Encerramento')
            ->orderBy('descricao')
            ->pluck('id')
            ->toArray();

        if (in_array($contratohistorico->tipo_id, $tiposPermitidos)) {
            $contrato = Contrato::find($contratohistorico->contrato_id);
            $contrato->minutasempenho()->detach();

            //todas minutas que serão vinculadas
            $arrContratoHistoricoMinutaEmpenho = ContratoHistoricoMinutaEmpenho::where('contrato_historico_id','=', $contratohistorico->id)->get();

            // vincula os empenhos ao contrato
            foreach ($arrContratoHistoricoMinutaEmpenho as $contratoHistoricoMinutaEmpenho) {
                $contrato->minutasempenho()->attach($contratoHistoricoMinutaEmpenho->minuta_empenho_id);

                $minutaEmpenho = MinutaEmpenho::select([
                    'minutaempenhos.contrato_id',
                    'minutaempenhos.fornecedor_empenho_id as fornecedor_id',
                    'empenhos.id as empenho_id',
                    'empenhos.unidade_id as unidade_id',
                ])
                    ->join('saldo_contabil', 'saldo_contabil.id', '=', 'minutaempenhos.saldo_contabil_id')
                    ->join('empenhos', function ($join) {
                        $join->on('empenhos.numero', '=', 'minutaempenhos.mensagem_siafi')
                            ->on('empenhos.unidade_id', '=', 'saldo_contabil.unidade_id');
                    })
                    ->join('codigoitens as situacao', 'situacao.id', '=', 'minutaempenhos.situacao_id')
                    ->join('codigoitens as tipo_empenhopor', 'tipo_empenhopor.id', '=', 'minutaempenhos.tipo_empenhopor_id')
                    ->where('situacao.descricao', '=', 'EMPENHO EMITIDO')
                    ->where('tipo_empenhopor.descricao', '=', 'Compra')
                    ->where('empenhos.unidade_id', '=', $contrato->unidade_id)
                    ->where('minutaempenhos.id', '=', $contratoHistoricoMinutaEmpenho->minuta_empenho_id)
                    ->first();

                if($minutaEmpenho != null) {
                    Contratoempenho::updateOrCreate([
                        'contrato_id' => $contrato->id,
                        'empenho_id' => $minutaEmpenho->empenho_id,
                    ], [
                        'fornecedor_id' => $minutaEmpenho->fornecedor_id,
                        'unidadeempenho_id' => $minutaEmpenho->unidade_id
                    ]);
                }
            }
        }
    }

    private function atualizaFornecedoresSubcontratadosContrato($contratohistorico)
    {
        // tipos que são permitidos manipular as minutas de empenho do contrato
        $tiposPermitidos = Codigoitem::whereHas('codigo', function ($query) {
            $query->where('descricao', '=', 'Tipo de Contrato');
        })
            ->where('descricao', '<>', 'Termo Aditivo')
            ->where('descricao', '<>', 'Termo de Apostilamento')
            ->where('descricao', '<>', 'Termo de Encerramento')
            ->where('descricao', '<>', 'Termo de Rescisão')
            ->orderBy('descricao')
            ->pluck('id')
            ->toArray();
        if (in_array($contratohistorico->tipo_id, $tiposPermitidos)) {
            $contrato = Contrato::find($contratohistorico->contrato_id);
            $contrato->fornecedoresSubcontratados()->detach();
            //todas minutas que serão vinculadas
            $arrContratoHistoricoFornecedoresSubcontratados = $contratohistorico->fornecedoresSubcontratados()->get();
            // vincula os empenhos ao contrato
            foreach ($arrContratoHistoricoFornecedoresSubcontratados as $contratoHistoricoFornecedoresSubcontratados) {
                $contrato->fornecedoresSubcontratados()->attach($contratoHistoricoFornecedoresSubcontratados->id);
            }
        }
    }
}
