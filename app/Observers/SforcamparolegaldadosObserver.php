<?php

namespace App\Observers;

use App\Http\Controllers\Admin\AmparoLegalCrudController;
use App\Models\AmparoLegal;
use App\Models\Sforcamparolegaldados;
use App\XML\Execsiafi;

class SforcamparolegaldadosObserver
{

    public function created(Sforcamparolegaldados $sforcamparolegaldados)
    {
        $amparoLegal = AmparoLegal::find($sforcamparolegaldados->amparo_legal_id);

        if($sforcamparolegaldados->tipo != 'D'){
            $amparoLegalRestricao = $amparoLegal->amparoLegalRestricao()->get();
            if ($amparoLegalRestricao) {
                $restricao = new AmparoLegalCrudController();
                $restricao->salvarSfRestricaoAmparoLegal($amparoLegalRestricao, $sforcamparolegaldados->id, false);
            }
        }


        $ws_siafi = new Execsiafi;
        $resposta = $ws_siafi->enviarAmparoLegal(
            session('user_ug'),
            config('app.ambiente_siafi'),
            date('Y'),
            $sforcamparolegaldados
        );

        if($sforcamparolegaldados->tipo == 'I'){
            $sforcamparolegaldados->codamparolegal = $resposta['codamparolegal'];
        }
        $sforcamparolegaldados->nonce = $resposta['nonce'];
        $sforcamparolegaldados->retornosiafi = $resposta['retornosiafi'];
        $sforcamparolegaldados->save();
    }


    public function updated(Sforcamparolegaldados $sforcamparolegaldados)
    {
        if ($sforcamparolegaldados->retornosiafi != '') {
            $this->atualizaAmparoLegal($sforcamparolegaldados);
        }

    }


    public function deleted(Sforcamparolegaldados $sforcamparolegaldados)
    {
        //
    }

    private function atualizaAmparoLegal(Sforcamparolegaldados $sforcamparolegaldados)
    {
        $amparolegal = AmparoLegal::find($sforcamparolegaldados->amparo_legal_id);
        $amparolegal->codigo = @$sforcamparolegaldados->codamparolegal;
        $amparolegal->retorno_siafi = $sforcamparolegaldados->retornosiafi;
        $amparolegal->save();
    }

}
