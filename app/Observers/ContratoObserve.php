<?php

namespace App\Observers;

use App\Models\Codigoitem;
use App\Models\Contrato;
use App\Models\Contratohistorico;
use App\Models\ContratoParametros;
use App\Models\Contratopublicacoes;
use Exception;
use Illuminate\Support\Facades\Log;

class ContratoObserve
{


    public function created(Contrato $contrato)
    {
        if ($contrato->verificaEmpenhoOrCartaContrato()) {
            ContratoParametros::create([
                'registro' => false,
                'contrato_id' => $contrato->id
            ]);
        }

        //se nao for um contrato em elaboracao
        if ($contrato->elaboracao != true) {
            $this->criarContratoHistorico($contrato);
        }
    }

    public function updated(Contrato $contrato)
    {

        // Edição no Contrato Rascunho (elaboracao true)
        // aqui está ativando o termo
        if ($contrato->elaboracao == false
            && $contrato->wasChanged('elaboracao')
            && $contrato->getOriginal('elaboracao') !== false) {
            $this->criarContratoHistorico($contrato);
            return true;
        }

        if ($contrato->elaboracao == true) {
            return true;
        }

        $tipos = Codigoitem::whereHas('codigo', function ($query) {
            $query->where('descricao', '=', 'Tipo de Contrato');
        })
            ->where('descricao', '<>', 'Termo Aditivo')
            ->where('descricao', '<>', 'Termo de Apostilamento')
            ->where('descricao', '<>', 'Termo de Rescisão')
            ->where('descricao', '<>', 'Termo de Encerramento')
            ->orderBy('descricao')
            ->pluck('id')
            ->toArray();

        $contrato_array = $contrato->toArray();

        unset($contrato_array['id']);
        unset($contrato_array['unidade_id']);
        unset($contrato_array['total_despesas_acessorias']);
        unset($contrato_array['numero']);
        unset($contrato_array['tipo_id']);

        $contratohistorico = $contrato->historico->where('unidade_id', $contrato->unidade_id)
            ->where('contrato_id', $contrato->id)
            ->where('numero', $contrato->numero)
            ->whereIn('tipo_id', $tipos)
            ->first();
        $contratohistorico->update($contrato_array);
    }

    public function deleted(Contrato $contrato)
    {
        $contrato->historico()->delete();
        $contrato->cronograma()->delete();
        $contrato->responsaveis()->delete();
        $contrato->garantias()->delete();
        $contrato->arquivos()->delete();
        $contrato->empenhos()->delete();
        $contrato->faturas()->delete();
        $contrato->ocorrencias()->delete();
        $contrato->terceirizados()->delete();
        $contrato->itens()->delete();
    }

    public function deleting(Contrato $contrato)
    {
        try {
            //Excluindo na ordem reversa para que as informações perdidas no primeiro historico não seja perdido
            $contrato = Contratohistorico::where('contrato_id', '=', $contrato->id)->orderBy('created_at', 'desc');
            $contrato->each(function ($historico) {
                $historico->delete();
            });
        } catch (Exception $e) {
            Log::error($e);
        }
    }

    /**
     * @param Contrato $contrato
     * @return void
     */
    public function criarContratoHistorico(Contrato $contrato): void
    {
        $con = $contrato;
        $contrato_array = $contrato->toArray();
        unset($contrato_array['id']);

        Contratohistorico::create($contrato_array + [
                'contrato_id' => $contrato->id,
                'observacao' => 'CELEBRAÇÃO DO CONTRATO: ' . $con->numero
                    . ' DE ACORDO COM PROCESSO NÚMERO: ' . $con->processo,
            ]);
    }


}
