<?php

namespace App\Observers;

// use App\Movimentacaocontratoconta;

use App\Models\Movimentacaocontratoconta;


class MovimentacaocontratocontaObserver
{

    public function deleting()
    {

        // \//Log::channel('schedule')->info('Passou por aqui!!! - MovimentacaocontratocontaObserver.php - deleting');
        // $post->comments()->delete();
    }


    /**
     * Handle the movimentacaocontratoconta "created" event.
     *
     * @param  \App\Movimentacaocontratoconta  $movimentacaocontratoconta
     * @return void
     */
    public function created()
    {
        //
        // \//Log::channel('schedule')->info('Passou por aqui!!! - MovimentacaocontratocontaObserver.php - created');

    }

    /**
     * Handle the movimentacaocontratoconta "updated" event.
     *
     * @param  \App\Movimentacaocontratoconta  $movimentacaocontratoconta
     * @return void
     */
    public function updated()
    {
        //
        // \//Log::channel('schedule')->info('Passou por aqui!!! - MovimentacaocontratocontaObserver.php - updated');


    }

    /**
     * Handle the movimentacaocontratoconta "deleted" event.
     *
     * @param  \App\Movimentacaocontratoconta  $movimentacaocontratoconta
     * @return void
     */
    public function deleted()
    {
        // \//Log::channel('schedule')->info('Passou por aqui!!! - MovimentacaocontratocontaObserver.php - deleted');

        //
    }

    /**
     * Handle the movimentacaocontratoconta "restored" event.
     *
     * @param  \App\Movimentacaocontratoconta  $movimentacaocontratoconta
     * @return void
     */
    public function restored()
    {
        //
        // \//Log::channel('schedule')->info('Passou por aqui!!! - MovimentacaocontratocontaObserver.php - restored');

    }

    /**
     * Handle the movimentacaocontratoconta "force deleted" event.
     *
     * @param  \App\Movimentacaocontratoconta  $movimentacaocontratoconta
     * @return void
     */
    public function forceDeleted()
    {
        //
        // \//Log::channel('schedule')->info('Passou por aqui!!! - MovimentacaocontratocontaObserver.php - forceDeleted');

    }
}
