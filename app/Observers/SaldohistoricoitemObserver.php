<?php

namespace App\Observers;

use App\Models\Contratohistorico;
use App\Models\Contratoitem;
use App\Models\Saldohistoricoitem;

class SaldohistoricoitemObserver
{
    public function __construct(Contratoitem $contratoitem)
    {
        $this->contratoitem = $contratoitem;
    }

    public function created(Saldohistoricoitem $saldohistoricoitem)
    {
        if ($saldohistoricoitem->elaboracao == false
            || $saldohistoricoitem->elaboracao == '0') {
            $this->contratoitem->atualizaSaldoContratoItem($saldohistoricoitem);

        }
    }


    public function updated(Saldohistoricoitem $saldohistoricoitem)
    {
        //se não é rascunho (elaboracao)
        if ($saldohistoricoitem->elaboracao == false
            || $saldohistoricoitem->elaboracao == '0') {
            $this->contratoitem->atualizaSaldoContratoItem($saldohistoricoitem);
        }
    }


    public function deleted(Saldohistoricoitem $saldohistoricoitem)
    {
        $this->contratoitem->deletaContratoItem($saldohistoricoitem);
    }
}
