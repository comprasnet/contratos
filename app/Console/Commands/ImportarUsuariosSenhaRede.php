<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\BackpackUser;
use Spatie\Permission\Models\Role;
use App\Http\Traits\Users;
use App\Http\Traits\Formatador;
use App\Models\Unidade;

class ImportarUsuariosSenhaRede extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importacao:usuarios-senha-rede';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importa usuários base Senha Rede.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    use Users;
    use Formatador;

    public function handle()
    {
        //$this->info('Processando Usuários com Perfil Gestor de Atas...');
        //$this->importarUsuarios($file = "lista_usuarios_gestor_atas.csv", $perfil = "GA");
        $this->info('Processando Usuários com Perfil Administrador Unidade...');
        $this->importarUsuarios($file = "lista_usuarios_cadastrador_local.csv", $perfil = "CL");
        $this->info('Processando Usuários com Perfil Administrador Órgão...');
        $this->importarUsuarios($file = "lista_usuarios_cadastrador_parcial.csv", $perfil = "CP");
    }

    private function importarUsuarios($file, $perfil) {

        $csv_file = fopen('database/data/'.$file,'r');
        $file = file('database/data/'.$file);

        $bar = $this->output->createProgressBar(count($file));
        $bar->start();

        //Define o perfil a ser atribuído aos usuários.
        $role = $this->getRole($perfil);

        $cont_new = 0;
        $cont_update = 0;
        for ($i = 0; $row = fgetcsv($csv_file,null,";"); ++$i) {

            $cpf = trim($row[0]);

            if ($this->validaCPF($cpf)) {

                $cpf = $this->formataCpf($cpf);
                $nome = trim($row[1]);

                $unidade = trim($row[2]);
                $unidade = str_pad($unidade, 6, "0", STR_PAD_LEFT);

                $unidade_obj = Unidade::where('codigo',$unidade)->first();
                $email = empty(trim($row[5])) ? trim($row[0]).'_trocar@' : strtolower(trim($row[5])); //Se campo vier "vazio", definir email = cpf + "_trocar@".

                $user = BackpackUser::where('cpf', $cpf)
                                        ->orWhere('email', $email)
                                        ->first();

                if (!isset($user) && isset($unidade_obj)) {  //Novo usuário com Unidade válida.
                    $user_new = BackpackUser::create([
                                'cpf' => $cpf,
                                'email' => $email,
                                'name' => strtoupper($nome),
                                'ugprimaria' => $unidade_obj->id,
                                'password' => bcrypt($this->geraSenhaAleatoria())
                            ]);

                    $user_new->assignRole($role->name);
                    ++$cont_new;

                } elseif (isset($user)) {  // Usuário existente
                    $unidade_user = $user->unidade()->get()->first(); //Unidade primária
                    if (isset($unidade_user)) {
                        if ($unidade_user->codigo == $unidade) { //Se as unidades são iguais, define o perfil correspondente para o usuário.
                            $user->assignRole($role->name);
                            $user->updated_at = now();
                            $user->save();
                            ++$cont_update;
                        }
                    }
                }

           }
           $bar->advance();
        }

        $bar->finish();
        fclose($csv_file);

        $this->line('');
        $this->line('Total de usuários lidos: '.$i);
        $this->line('Total de usuários inseridos: '.$cont_new);
        $this->line('Total de usuários atualizados: '.$cont_update);
    }


    private function getRole($perfil) {

        switch ($perfil) {
            case "CP": //Cadastrador Parcial
                $role_name = "Administrador Órgão";
                break;
            case "CL": //Cadastrador Local
                $role_name = "Administrador Unidade";
                break;
            case "GA": //Gestor de Atas
                $role_name = "Gestor de Atas";
                break;
            default:
                $role_name = "Consulta";
        }

        $role = Role::where("name", $role_name)->first();
        return $role;
    }
}
