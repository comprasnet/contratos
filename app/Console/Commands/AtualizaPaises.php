<?php

namespace App\Console\Commands;

use App\Http\Controllers\IBGE;
use App\Models\Paises;
use Illuminate\Console\Command;

class AtualizaPaises extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'atualizacao:paises';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Atualiza paises com a base de dados do IBG';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Paises::atualiza();
    }
}
