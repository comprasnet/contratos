<?php

namespace App\Console\Commands;

use App\Models\Contratoempenho;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CorrigirUnidadeEmpenhoDaTabelaContatoEmpenhos extends Command
{
    protected $signature = 'command:corrigirunidadeempenhodecontratoempenhos ' .
    '{--dry-run : Executa um dry run sem aplicar alterações} ' .
    '{--chunkValue= : Valor do chunkValue => 10000} ' .
    '{--limit= : Limit da query => 400000}';

    protected $description = 'Corrige os dados do campo unidadeempenho_id da tabela contratoempenhos.';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $chunkValue = (int) $this->option('chunkValue') ?: 10000;
        $limit      = (int) $this->option('limit') ?: 400000;

        $totalRegistros = $limit ?: $this->totalRegistrosASeremAtualizados();

        $this->info('Iniciando atualização de unidadeempenho_id da tabela contratoempenhos...');

        $progressBar = $this->output->createProgressBar($totalRegistros);
        $progressBar->start();

        DB::transaction(function () use($progressBar, $chunkValue, $limit) {

            $totalProcessado = 0;

            Contratoempenho::whereExists(function ($query) use($progressBar){

                $query->select(DB::raw(1))
                    ->from('empenhos')
                    ->whereColumn('empenhos.id', 'contratoempenhos.empenho_id')
                    ->whereNull("empenhos.deleted_at")
                    ->where(function ($subQuery) {
                        $subQuery->where('contratoempenhos.unidadeempenho_id', '<>', DB::raw('empenhos.unidade_id'))
                            ->orWhereNull('contratoempenhos.unidadeempenho_id');
                    });
            })
                ->limit($limit)
                ->chunk($chunkValue, function ($contratoEmpenhosParaCorrigirUnidade) use($progressBar, &$totalProcessado, $limit) {

                    foreach ($contratoEmpenhosParaCorrigirUnidade as $contratoEmpenho) {

                        $contratoEmpenho->update([
                            'unidadeempenho_id' => $contratoEmpenho->empenho->unidade_id,
                        ]);

                        $totalProcessado++;

                        $progressBar->advance();

                        if($totalProcessado >= $limit){
                            return false;
                        }
                    }
                });

            $progressBar->finish();
        });

        $this->info('');
        $this->info('Comando executado com sucesso!');
    }

    public function totalRegistrosASeremAtualizados()
    {
        return Contratoempenho::whereExists(function ($query) {
            $query->select(DB::raw(1))
                ->from('empenhos')
                ->whereColumn('empenhos.id', 'contratoempenhos.empenho_id')
                ->where(function ($subQuery) {
                    $subQuery->where('contratoempenhos.unidadeempenho_id', '<>', DB::raw('empenhos.unidade_id'))
                        ->orWhereNull('contratoempenhos.unidadeempenho_id');
                });
        })->count();
    }
}
