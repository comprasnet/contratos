<?php

namespace App\Console\Commands;

use App\Http\Controllers\Execfin\EmpenhoCrudController;
use Illuminate\Console\Command;

class AtualizaEmpenhos extends Command
{
    protected $signature = 'atualizacao:empenhos
                            {--ano= : Anos atualização =>2021}
                            {--rp= : Se atualizará RP ou não: sim}
                            {--rponly= : Se atualizará somente RP ou não: sim}
                            {--unidade= : Unidade específica para atualização.}
                            ';

    protected $description = 'Cria josb para Atualização de Empenhos por Ano ou Unidade específica.';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        //verifica parametros e monta variáveis
        if (!$this->option('ano')) {
            $anos = [date('Y')];
        }else{
            $anos = explode(';',$this->option('ano'));
        }

        if (!$this->option('unidade')) {
            $unidade = null;
        }else{
            $unidade = $this->option('unidade');
        }

        $rp = ($this->option('rp') == 'sim') ? true : false;

        $rpoly = ($this->option('rponly') == 'sim') ? true : false;

        $objeto = new EmpenhoCrudController();


        foreach ($anos as $ano){
            if($ano >= '2021' and $ano <= date('Y')){
                $retorno = $objeto->executaMigracaoEmpenho($ano, $unidade, $ano.'-01-01', $rp, $rpoly);

                if($retorno === true){
                    $this->line($ano.' - Deu bom!');
                }else{
                    $this->line($ano.' - Deu ruim!');
                }

            }else{
                $this->line('A Opção "--ano" deve ser entre >=2021 e <='.date('Y'));
                $this->line('Ex: "php artisan atualizacao:empenhos --ano=2021"');
            }
        }
    }
}
