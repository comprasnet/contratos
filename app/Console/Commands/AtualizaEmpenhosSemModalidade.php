<?php

namespace App\Console\Commands;

use App\Jobs\AtualizanedadosimcompletosJob;
use App\Models\Empenho;
use App\Models\Unidade;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class AtualizaEmpenhosSemModalidade extends Command
{
    protected $signature = 'atualizacao:nedadosincompletos';

    protected $description = 'Cria josb para Atualização de Empenhos sem modalidade.';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $unidades = Unidade::whereHas('empenhos', function ($e) {
            $e->whereNull('modalidade_licitacao_siafi')
                ->where(DB::raw('left(numero,4)'), '>=', '2021');
        })->get();

        foreach ($unidades as $unidade) {
            AtualizanedadosimcompletosJob::dispatch($unidade)->onQueue('migracaoempenho');
        }

        $this->line('Deu bom!');
    }
}
