<?php

namespace App\Console\Commands;

use App\Http\Controllers\Execfin\EmpenhoCrudController;
use App\Models\Unidade;
use Illuminate\Console\Command;

class AtualizaSaldoEmpenhos extends Command
{
    protected $signature = 'atualizacao:saldosempenhos
                            {--ano= : Anos atualização =>2021}
                            {--unidade= : Unidade a ser atualizada}
                            ';

    protected $description = 'Cria josb para Atualização de Saldos de Empenhos por Ano.';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        //verifica parametros e monta variáveis
        if (!$this->option('ano')) {
            $ano = [date('Y')];
        }else{
            $ano = $this->option('ano');
        }

        if (!$this->option('unidade')) {
            $unidade = null;
        }else{
            $unidade = Unidade::where('codigo',$this->option('unidade'))->first();
        }

        if ($ano >= '2021' and $ano <= date('Y')) {
            $objeto = new EmpenhoCrudController();
            $retorno = $objeto->rotinaAtualizaSaldoAgendamento($ano, $unidade);

            if ($retorno === true) {
                $this->line($ano . ' - Deu bom!');
            } else {
                $this->line($ano . ' - Deu ruim!');
            }

        } else {
            $this->line('A Opção "--ano" deve ser entre >=2021 e <=' . date('Y'));
            $this->line('Ex: "php artisan atualizacao:saldosempenhos --ano=2021"');
        }
    }
}
