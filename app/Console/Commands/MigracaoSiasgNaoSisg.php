<?php

namespace App\Console\Commands;

use App\Models\OrgaoSuperior;
use App\Models\Orgao;
use App\Models\Unidade;
use App\Models\Municipio;
use File;
use Illuminate\Console\Command;

class MigracaoSiasgNaoSisg extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importacao:siasg-nao-sisg';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importa órgãos superiores, órgãos e unidades.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->importacaoOrgaosSuperiores();
        $this->importacaoOrgaos();
        $this->importacaoUnidades();
    }

    private function importacaoOrgaosSuperiores() {
        $this->info('Processando Órgãos Superiores...');
        $json = File::get('database/data/orgaos_superiores_nao_sisg.json');
        $data = json_decode($json);

        $bar = $this->output->createProgressBar(count($data));
        $bar->start();

        $cont = 0;
        foreach ($data as $item) {

            $orgao_superior = OrgaoSuperior::where('codigo', str_pad(trim($item->IT_CO_ORGAO_VINCULADO), 5, "0", STR_PAD_LEFT))->first();

            if (!isset($orgao_superior)) {
                OrgaoSuperior::create([
                    'codigo' => str_pad(trim($item->IT_CO_ORGAO_VINCULADO), 5, "0", STR_PAD_LEFT),
                    'nome' => trim($item->IT_NO_ORGAO_VINCULADO),
                    'situacao' => true
                ]);
                ++$cont;
            }

            $bar->advance();
        }

        $bar->finish();
        $this->line('');
        $this->line('Total de órgãos superiores inseridos: '.$cont);
    }

    private function importacaoOrgaos() {
        $this->info('Processando Órgãos...');
        $json = File::get('database/data/orgaos_nao_sisg.json');
        $data = json_decode($json);

        $bar = $this->output->createProgressBar(count($data));
        $bar->start();

        $cont = 0;
        foreach ($data as $item) {

            $orgao = Orgao::where('codigo', str_pad(trim($item->IT_CO_ORGAO), 5, "0", STR_PAD_LEFT))->first();

            if (!isset($orgao)) {
                $orgao_superior_id = OrgaoSuperior::where('codigo',str_pad(trim($item->IT_CO_ORGAO_VINCULADO), 5, "0", STR_PAD_LEFT))->first()->id;

                Orgao::create([
                    'codigo' => str_pad(trim($item->IT_CO_ORGAO), 5, "0", STR_PAD_LEFT),
                    'orgaosuperior_id' => $orgao_superior_id,
                    'nome' => trim($item->IT_NO_ORGAO),
                    'codigosiasg' => str_pad(trim($item->IT_CO_ORGAO), 5, "0", STR_PAD_LEFT),
                    'cnpj' => trim($item->IT_NU_CGC_CPF) ?: null,
                    'situacao' => true
                ]);
                ++$cont;
            }

            $bar->advance();
        }

        $bar->finish();
        $this->line('');
        $this->line('Total de órgãos inseridos: '.$cont);
    }

    private function importacaoUnidades() {
        $this->info('Processando Unidades...');
        $json = File::get('database/data/unidades_nao_sisg.json');
        $data = json_decode($json);

        $bar = $this->output->createProgressBar(count($data));
        $bar->start();

        $cont = 0;
        foreach ($data as $item) {

            $unidade = Unidade::where('codigo',str_pad(trim($item->IT_CO_UNIDADE_GESTORA), 6, "0", STR_PAD_LEFT))->first();

            if (!isset($unidade)) {

                $orgao_id = Orgao::where('codigo',str_pad(trim($item->IT_CO_ORGAO), 5, "0", STR_PAD_LEFT))->first()->id;

                $cod_municipio_ibge = trim($item->COD_MUN_IBGE) ?: 0; //Define COD_MUN_IBGE = 0 se campo for NULL
                $municipio = Municipio::where('codigo_ibge',$cod_municipio_ibge)->first();
                $municipio_id = (isset($municipio) ? $municipio->id : null);

                $esfera = $this->getEsfera(trim($item->IT_IN_ESFERA));
                $poder = $this->getPoder(trim($item->IT_IN_TIPO_PODER));
                $tipoAdmin = $this->getTipoAdmin(trim($item->IT_IN_TIPO_ADMINISTRACAO));

                Unidade::create([
                    'codigo' => str_pad(trim($item->IT_CO_UNIDADE_GESTORA), 6, "0", STR_PAD_LEFT),
                    'orgao_id' => $orgao_id,
                    'nome' => trim($item->IT_NO_UNIDADE_GESTORA),
                    'nomeresumido' => trim($item->IT_NO_UNIDADE_GESTORA),
                    'tipo' => 'E',
                    'situacao' => true,
                    'aderiu_siasg' => trim($item->IT_IN_ADESAO_SIASG) == 'S' ? true : false,
                    'utiliza_siafi' => false,
                    'codigosiasg' => str_pad(trim($item->IT_CO_UNIDADE_GESTORA), 6, "0", STR_PAD_LEFT),
                    'municipio_id' => $municipio_id,
                    'esfera' => $esfera,
                    'poder' => $poder,
                    'tipo_adm' => $tipoAdmin,
                    'cnpj' => trim($item->IT_NU_CGC_CPF) ?: null,
                    'codigosiafi' => str_pad(trim($item->IT_CO_UNIDADE_GESTORA), 6, "0", STR_PAD_LEFT)
                ]);
                ++$cont;
            }

            $bar->advance();
        }

        $bar->finish();
        $this->line('');
        $this->line('Total de unidades inseridas: '.$cont);
    }

    private function getEsfera($esferaSiasg) {

        $esfera = '';

        switch ($esferaSiasg) {
            case 'F':
                $esfera = 'Federal';
                break;
            case 'E':
                $esfera = 'Estadual';
                break;
            case 'M':
                $esfera = 'Municipal';
                break;
        }
        return $esfera;

    }

    private function getPoder($poderSiasg) {

        $poder = '';

        switch ($poderSiasg) {
            case 0:
                $poder = 'Executivo';
                break;
            case 1:
                $poder = 'Legislativo';
                break;
            case 2:
                $poder = 'Judiciário';
                break;
        }
        return $poder;

    }

    private function getTipoAdmin($tipoAdminSiasg) {

        $tipoAdmin = '';

        switch ($tipoAdminSiasg) {
            case 1:
                $tipoAdmin = 'ADMINISTRAÇÃO DIRETA';
                break;
            case 2:
                $tipoAdmin = 'ESTATAL';
                break;
            case 3:
                $tipoAdmin = 'AUTARQUIA';
                break;
            case 4:
                $tipoAdmin = 'FUNDAÇÃO';
                break;
            case 5:
                $tipoAdmin = 'EMPRESA PÚBLICA COM. E FIN.';
                break;
            case 6:
                $tipoAdmin = 'ECONOMIA MISTA';
                break;
            case 7:
                $tipoAdmin = 'FUNDOS';
                break;
            case 8:
                $tipoAdmin = 'EMPRESA PUBLICA INDUSTRIAL E AGRICOLA';
                break;
            case 11:
                $tipoAdmin = 'ADMINISTRACAO DIRETA ESTADUAL';
                break;
            case 12:
                $tipoAdmin = 'ADMINISTRACAO DIRETA MUNICIPAL';
                break;
            case 13:
                $tipoAdmin = 'ADMINISTRACAO INDIRETA ESTADUAL';
                break;
            case 14:
                $tipoAdmin = 'ADMINISTRACAO INDIRETA MUNICIPAL';
                break;
            case 15:
                $tipoAdmin = 'EMPRESA PRIVADA';
                break;
        }
        return $tipoAdmin;

    }

}
