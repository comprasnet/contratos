<?php

namespace App\Console\Commands;

use App\Http\Controllers\Api\PNCP\PncpController;
use App\Models\Codigoitem;
use App\Models\EnviaDadosPncp;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use GuzzleHttp\Client;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class FixTermoComTipoDiferenteNumeroIgual extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:termo-mesmo-sequencial';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    #private $pncpController;
    private $idIncPen;
    private $idExcPen;
    private $idExcluido;
    private $log;

    public function __construct()
    {
        parent::__construct();
        $this->pncpController = new PncpController();
        $this->idIncPen = Codigoitem::where('descres', 'INCPEN')->first()->id;
        $this->idExcPen = Codigoitem::where('descres', 'EXCPEN')->first()->id;
        $this->idExcluido = Codigoitem::where('descres', 'EXCLUIDO')->first()->id;
        $this->tiposContratoAceitosPncp = config('api-pncp.tipos_contrato_aceitos_pncp');
        $this->tiposTermoAceitosPncp = config('api-pncp.tipos_termo_contrato_aceitos_pncp');
        $this->inicializaLog('1249-termos-corrigidos');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $debug = [
            'debug' => false,
            'limit' => false,
            'contrato_id' =>
            false
            #    66703
        ];
        $termosParaAnalisar = $this->getLinhasPncpDosTermosDeContrato($debug);
        $termosPorContrato = $this->organizaTermosPorContrato($termosParaAnalisar);
        $contratosPais = $this->getLinhaPncpDosContratosPais($termosPorContrato);
        foreach ($contratosPais as $contratoPai) {
            #dd($termosPorContrato[$contratoPai->contrato_id][0]);
            $termosDoContratoNoPncp = $this->buscarTodosOsTermosDeUmContratoNoPncp($contratoPai);
            if (!$termosDoContratoNoPncp) {
                continue;
            }
            #dd($termosDoContratoNoPncp[0]);
            $exibeContratoPai = true; # Apenas um controle para melhorar a exibição do LOG
            foreach ($termosPorContrato[$contratoPai->contrato_id] as $termoNaBaseLocal) {
                #dump($termoNaBaseLocal->id . ' - '. $termoNaBaseLocal->numero. ' - '.$termoNaBaseLocal->tipo_contrato);
                if ($termoNoPncp = $this->termoExisteNoPncp($termosDoContratoNoPncp, $termoNaBaseLocal)) {
                    /*$this->log->info(
                        'Termo encontrado no PNCP: ' .
                        'Contrato ID = ' . $contratoPai->contrato_id .
                        ' Termo = ' . $termoNaBaseLocal->numero . ' - ' . $termoNaBaseLocal->tipo_contrato
                    .' - ' . $termoNaBaseLocal->link_pncp);*/
                    if ($this->tipoEstaDiferente($termoNoPncp, $termoNaBaseLocal)) {
                        if ($exibeContratoPai) {
                            $this->exibeContratoPaiNoLog($contratoPai->contrato_id);
                            $exibeContratoPai = false;
                        }
                        $this->log->info(
                            '### DIFERENTE### - ID do termo: ' . $termoNaBaseLocal->id . ' - ' .
                            'LOCAL: ' . $termoNaBaseLocal->numero . ' - ' . $termoNaBaseLocal->tipo_contrato . ' - ' .
                            'PNCP: ' . $termoNoPncp['numeroTermoContrato'] . ' - ' .
                            $termoNoPncp['tipoTermoContratoNome']);
                        /*dump("PNCP: " . $termoNoPncp['tipoTermoContratoNome'] . ' - ' . $termoNoPncp['numeroTermoContrato']);
                        dump("Local: " . $termoNaBaseLocal->tipo_contrato . ' - ' . $termoNaBaseLocal->numero);
                        dump('-----------------');*/
                    }
                } else {
                    /*if ($exibeContratoPai) {
                        $this->exibeContratoPaiNoLog($contratoPai->contrato_id);
                        $exibeContratoPai = false;
                    }
                    $this->log->error('NÃO existe: ' . $termoNaBaseLocal->numero . ' - ' . $termoNaBaseLocal->tipo_contrato);*/
                }
            }
        }
    }

    private function exibeContratoPaiNoLog($id)
    {
        $this->log->info('---------------------------------------------------------------');
        $this->log->info('Contrato Pai: ' . $id);
    }

    private function getLinhasPncpDosTermosDeContrato($debug = [])
    {
        $termos = EnviaDadosPncp::select(
            'contratohistorico.id',
            'envia_dados_pncp.contrato_id',
            'contratohistorico.numero',
            'envia_dados_pncp.tipo_contrato',
            'envia_dados_pncp.sequencialPNCP',
            'envia_dados_pncp.link_pncp'
        )
            ->join('contratohistorico', 'contratohistorico.id', 'envia_dados_pncp.pncpable_id')
            ->where('pncpable_type', 'App\Models\Contratohistorico')
            ->whereIn('tipo_contrato', $this->tiposTermoAceitosPncp)
            ->where('envia_dados_pncp.updated_at', '>=', '2024-06-04');

        ############################################
        ### Área utilizada apenas para os testes ###
        ############################################
        if (isset($debug['limit']) && $debug['limit'] > 0) {
            $termos = $termos->limit($debug['limit']);
        }
        if (isset($debug['contrato_id']) && $debug['contrato_id']) {
            $termos = $termos->where('envia_dados_pncp.contrato_id', $debug['contrato_id']);
        }
        if (isset($debug['debug']) && $debug['debug']) {
            $sql = $termos->toSql();
            $bindings = $termos->getBindings();
            foreach ($bindings as $binding) {
                $value = is_numeric($binding) ? $binding : "'" . addslashes($binding) . "'";
                $sql = preg_replace('/\?/', $value, $sql, 1);
            }
            dd($sql);
        }
        ### Fim da área de testes ####################

        $termos = $termos->get();
        return $termos;
    }

    private function organizaTermosPorContrato($termosParaAnalisar)
    {
        $termosPorContrato = [];
        foreach ($termosParaAnalisar as $termo) {
            $termosPorContrato[$termo->contrato_id][] = $termo;
        }
        return $termosPorContrato;
    }

    private function getLinhaPncpDosContratosPais($termosPorContrato)
    {
        $contratoIds = array_keys($termosPorContrato);
        return EnviaDadosPncp::select('sequencialPNCP', 'link_pncp', 'contrato_id')
            ->whereIn('contrato_id', $contratoIds)
            ->whereIn('tipo_contrato', $this->tiposContratoAceitosPncp)
            ->oldest()
            ->get()
            ->keyBy('contrato_id');
    }

    private function verificaDadosSequencial($dadosContratoNaTabelaEnviaDadosPncp)
    {
        $sequencial = $this->divideSequencial($dadosContratoNaTabelaEnviaDadosPncp['sequencialPNCP']);
        if (!$sequencial) {
            Log::error(__FILE__ . ' - Não foi possível identificar o sequencial do contrato: ' .
                $dadosContratoNaTabelaEnviaDadosPncp['sequencialPNCP']);
            return false;
        }
        return $sequencial;
    }

    private function buscarTodosOsTermosDeUmContratoNoPncp($contratoPai)
    {
        $sequencialDoContratoPaiPncp = $this->verificaDadosSequencial($contratoPai);
        if (!$sequencialDoContratoPaiPncp) {
            return false;
        }
        $cnpj = $sequencialDoContratoPaiPncp['cnpj'];
        $ano = $sequencialDoContratoPaiPncp['ano'];
        $sequencial = $sequencialDoContratoPaiPncp['sequencialContrato'];
        $url = $this->pncpController->montaUrl($this->urlConsultaContrato($cnpj, $ano, $sequencial), null);
        #$this->log->info($url);
        try {
            $client = new Client(['verify' => false]);
            $response = $client->request('GET', $url);
            return json_decode($response->getBody()->getContents(), true);
        } catch (Exception $e) {
            Log::error("Erro ao consultar termos de contrato: " . $e->getMessage());
            return false;
        }
    }

    private function termoExisteNoPncp($termosDoContratoNoPncp, $termoNaBaseLocal)
    {
        foreach ($termosDoContratoNoPncp as $termoNoPncp) {
            if ($termoNoPncp['sequencialTermoContrato'] == $termoNaBaseLocal->sequencialPNCP) {
                return $termoNoPncp;
            }
        }
        return false;
    }

    private function tipoEstaDiferente($termoNoPncp, $termoNaBaseLocal)
    {
        if ($termoNoPncp['sequencialTermoContrato'] == $termoNaBaseLocal->sequencialPNCP &&
            $termoNoPncp['numeroTermoContrato'] == $termoNaBaseLocal->numero &&
            $termoNoPncp['tipoTermoContratoNome'] !== $termoNaBaseLocal->tipo_contrato) {
            return true;
        }
        return false;
    }


    private function divideSequencial($sequencialPncp)
    {
        if (!$sequencialPncp) return false;
        $hyphenParts = explode('-', $sequencialPncp);
        if (!count($hyphenParts) == 3) {
            return null;
        }
        try {
            [$cnpj, $codigo, $sequencialAno] = $hyphenParts;
        } catch (Exception $e) {
            $this->log->error($sequencialPncp . ' - ' . $e->getMessage());
            return null;
        }

        $slashParts = explode('/', $sequencialAno);
        if (!count($slashParts) == 2) {
            return null;
        }
        [$sequencialContrato, $ano] = $slashParts;
        $array = [
            'cnpj' => $cnpj,
            'codigo' => $codigo,
            'sequencialContrato' => $sequencialContrato,
            'ano' => $ano
        ];
        return $array;
    }

    private function urlConsultaContrato(string $cnpj, string $ano, string $sequencial)
    {
        return "v1/orgaos/" . $cnpj . "/contratos/" . $ano . "/" . $sequencial . "/termos";
    }

    private function getDadosLink($link_pncp)
    {
        $dados = explode('/', $link_pncp);
        $indice = 0;
        for ($i = count($dados) - 1; $indice <= 3; $i--) {
            switch ($indice) {
                case 0 :
                    $retorno['sequencialContrato'] = str_pad($dados[$i], 6, '0', STR_PAD_LEFT);
                    break;
                case 1 :
                    $retorno['ano'] = $dados[$i];
                    break;
                case 3 :
                    $retorno['cnpj'] = $dados[$i];
                    break;
            }
            $indice++;
        }

        return $retorno;
    }

    private function confereDadosLinkSequencial($sequencial, $link_pncp): bool
    {
        if ($sequencial['cnpj'] != $link_pncp['cnpj'] ||
            $sequencial['ano'] != $link_pncp['ano'] ||
            $sequencial['sequencialContrato'] != $link_pncp['sequencialContrato']) {

            Log::error(
                'Os dados do sequencial do termo não correspondem aos dados do link pncp. ' .
                ' sequencial cnpj: ' . $sequencial['cnpj'] . ' != link cnpj: ' . $link_pncp['cnpj'] .
                ' sequencial ano: ' . $sequencial['ano'] . ' != link ano: ' . $link_pncp['ano']);
            return false;
        }
        return true;
    }

    private function loginPncp()
    {
        try {
            $uc = new UsuarioController();
            $uc->login();
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
        return true;
    }

    private function logTermoSequencialContratoInvalido($termo, $msg)
    {
        Log::error(
            $msg . ' ' .
            "ID envia_dados_pncp: {$termo->id}; " .
            "ID do termo: {$termo->pncpable_id}; " .
            "Situação: {$termo->situacao}; " .
            "Tipo contrato: {$termo->tipo_contrato}; " .
            "Sequencial: {$termo->sequencialPNCP}");
    }

    private function inicializaLog($logName)
    {
        $handler = new StreamHandler(storage_path("logs/{$logName}.log"), Logger::INFO);
        $formatter = new LineFormatter("%datetime% %channel%.%level_name%: %message%\n");
        $handler->setFormatter($formatter);
        $log = new \Monolog\Logger('custom');
        $log->pushHandler($handler);
        $this->log = $log;
    }
}
