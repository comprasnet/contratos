<?php

namespace App\Console\Commands;

use App\Models\BackpackUser;
use Illuminate\Console\Command;
use App\Models\Contratoarquivo;
use Illuminate\Support\Facades\Auth;
use DB;


class SanitizarContratoArquivos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:SanitizarContratoArquivos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sanitiza a tabela contrato_arquivos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {

            $UserModel = BackpackUser::where('cpf','700.744.021-53')->first();

            Auth::login($UserModel, true);

            $this->buscaLinhasComMaisDeUmArquivo();
            $this->alteraArquivosUmaLinha();


        } catch (Exception $e) {

            throw new Exception("Error ao Processar a Sanitização da tabela contrato_arquivos", $e->getMessage());

        }


    }

    public function buscaLinhasComMaisDeUmArquivo() {
        //todos os registros com array
        $dados = Contratoarquivo::select('id')->where('arquivos', 'ILIKE', '{%}')->orderBy('id', 'ASC')->get()->toArray();
        //para testar com apenas um registro descomente a linha abaixo
        //$dados = Contratoarquivo::select('id')->where('arquivos', 'ILIKE', '{%}')->limit(1)->orderBy('id', 'ASC')->get()->toArray();


        //print_r($dados);
        $this->info('Sanitizando registros com vários arquivos');

        $bar = $this->output->createProgressBar(count($dados));

        foreach($dados as $dado) {

            //print_r($dado['id']);
            //ache um registro de cada vez
            $contrato_arquivo = Contratoarquivo::findOrFail($dado['id']);


             foreach($contrato_arquivo->arquivos as $arquivo) {
                 //print_r(gettype($arquivo));
                 //print_r($arquivo);
                 $bar->start();

                 DB::transaction(function () use ($bar, $arquivo, $contrato_arquivo) {
                     //crie os novos objeto a salvar
                     $contrato_arquivo_nova_linha = new Contratoarquivo;

                     $contrato_arquivo_nova_linha->contrato_id = $contrato_arquivo->contrato_id;
                     $contrato_arquivo_nova_linha->tipo = $contrato_arquivo->tipo;
                     $contrato_arquivo_nova_linha->descricao = $contrato_arquivo->descricao;
                     $contrato_arquivo_nova_linha->arquivos = $arquivo;
                     $contrato_arquivo_nova_linha->created_at = $contrato_arquivo->created_at;
                     $contrato_arquivo_nova_linha->processo = $contrato_arquivo->processo;
                     $contrato_arquivo_nova_linha->sequencial_documento = $contrato_arquivo->sequencial_documento;
                     $contrato_arquivo_nova_linha->envio_pncp_pendente = $contrato_arquivo->envio_pncp_pendente;
                     $contrato_arquivo_nova_linha->link_pncp = $contrato_arquivo->link_pncp;
                     $contrato_arquivo_nova_linha->sequencial_pncp = $contrato_arquivo->sequencial_pncp;
                     $contrato_arquivo_nova_linha->retorno_pncp = $contrato_arquivo->retorno_pncp;
                     $contrato_arquivo_nova_linha->contratohistorico_id = $contrato_arquivo->contratohistorico_id;
                     $contrato_arquivo_nova_linha->origem = $contrato_arquivo->origem;
                     $contrato_arquivo_nova_linha->link_sei = $contrato_arquivo->link_sei;

                     $contrato_arquivo_nova_linha->save();

                   $bar->advance();
                 });

                 $bar->finish();
             }

            //desative o objeto antigo colocando
            $contrato_arquivo->delete();


        }

    }
    public function alteraArquivosUmaLinha() {
        //para rodar todos os registros descomente a linha abaixo
        $dados = Contratoarquivo::select('id')->where('arquivos', 'ILIKE', '[%]')->orderBy('id', 'ASC')->get()->toArray();
        //para testar com apenas dois registros descomente a linha abaixo
        //$dados = Contratoarquivo::select('id')->where('arquivos', 'ILIKE', '[%]')->limit(2)->orderBy('id', 'ASC')->get()->toArray();

        $this->info('Sanitizando registros com um arquivo só dentro de um array. Retirando [ e ]');

        $bar = $this->output->createProgressBar(count($dados));

        foreach($dados as $dado) {
            $contrato_arquivo = Contratoarquivo::findOrFail($dado['id']);

            $bar->start();
            DB::transaction(function () use ($bar, $contrato_arquivo) {

                $contrato_arquivo->arquivos = $contrato_arquivo->arquivos[0];

                $contrato_arquivo->save();

                $bar->advance();
            });
          $bar->finish();
        }

    }

}
