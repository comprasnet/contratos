<?php

namespace App\Console\Commands;

use App\Models\Contrato;
use Carbon\Carbon;
use Illuminate\Console\Command;

class InativarContratoPorAno extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:inativacontrato-por-ano 
                                {--anoinicial=: Anos para inativar }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Este comando serve para inativar os contratos para a exibicao do grafico por ano no modulo do tranparencia';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // $this->line

        $anoInicial = $this->option('anoinicial');

        $contratoRecuperado = Contrato::whereNull("deleted_at")->whereRaw("extract(year from data_assinatura) >= {$anoInicial}")->get();

        $this->line("******************************************************************");
        $registrosAfetados = 0;
        foreach($contratoRecuperado as $key => $contrato) {
            $key++;
            $saidaUsuario = "{$key} - Contrato {$contrato->numero} com a data da assinatura {$contrato->data_assinatura} e Id {$contrato->id} será inativado";
            $this->line($saidaUsuario);

            $contrato->deleted_at = Carbon::now()->format('Y-m-d H:i:s');
            $contrato->save();
            $registrosAfetados = $key;
        }

        $this->line("Foram inativados {$registrosAfetados} registro(s)");

        $this->line("******************************************************************");
        
    }
}
