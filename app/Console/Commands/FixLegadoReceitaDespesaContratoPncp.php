<?php

namespace App\Console\Commands;

use App\Models\Contratohistorico;
use App\Models\EnviaDadosPncp;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use App\Http\Traits\EnviaPncpTrait;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Formatter\LineFormatter;

class FixLegadoReceitaDespesaContratoPncp extends Command
{
    use EnviaPncpTrait;

    private $urlPncp;
    private $clientHttp;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pncp:fix-receita-contrato {--debug=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->urlPncpConsultaContrato = env('API_PNCP_URL');
        $this->clientHttp = new Client();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /**
         * Controle DEBUG para exibir a query SQL a ser executada na base
         * Apenas para fins de teste
         */
        $debug = $this->option('debug') ?? false;
        $debug = filter_var($debug, FILTER_VALIDATE_BOOLEAN);

        /**
         * Bloco para controle de log
         * Serve para criar um arquivo específico para LOG
         */
        $handler = new StreamHandler(storage_path("logs/1271-2-resultado-processamento.log"),Logger::INFO);
        $formatter = new LineFormatter("%datetime% %channel%.%level_name%: %message%\n");
        $handler->setFormatter($formatter);
        $log = new \Monolog\Logger('custom');
        $log->pushHandler($handler);

        /**
         * Início do processamento
         */
        $log->info('Início: ' . date('d/m/Y H:i:s'));
        $idSucesso = $this->recuperarIDSituacao('SUCESSO');
        $idRetPen = $this->recuperarIDSituacao('RETPEN');

        /* Busca os contratos com estes critérios:
            - São de despesa
            - Criados antes de 22/07/2024 (depois desta data, os contratos já contêm a correção)
            - Que podem ser enviados para o PNCP
                - lei 14133 e derivadas
                - tipos aceitos pelo PNCP
            - Linha PNCP = SUCESSO (pois se está pendente, a rotina vai fazer o envio)
         */
        $contratos = ContratoHistorico::distinct()
            ->select([
                'envia_dados_pncp.id as envia_dados_pncp_id',
                'envia_dados_pncp.contrato_id',
                'contratohistorico.id as contratohistorico_id',
                'envia_dados_pncp.link_pncp'
            ])
            ->join('contratos','contratos.id', '=', 'contratohistorico.contrato_id')
            ->join('amparo_legal_contrato',
                'contratos.id', '=', 'amparo_legal_contrato.contrato_id')
            ->join('amparo_legal', function ($join) {
                $join->on('amparo_legal.id', '=', 'amparo_legal_contrato.amparo_legal_id')
                    ->where('amparo_legal.complemento_14133', '=', true);
            })
            ->join('envia_dados_pncp',
                'contratohistorico.id', '=', 'envia_dados_pncp.pncpable_id')
            ->join('unidades', 'contratohistorico.unidade_id', '=', 'unidades.id')
            ->where('unidades.sigilo', '=', false)
            ->where('contratos.receita_despesa', 'D')
            ->where('envia_dados_pncp.situacao', $idSucesso)
            ->whereNotNull('envia_dados_pncp.sequencialPNCP')
            ->whereIn('envia_dados_pncp.tipo_contrato', config('api-pncp.tipos_contrato_aceitos_pncp'))
            ->where('contratohistorico.created_at', '<', '2024-07-22') # issue 1239
        ;

        if ($debug) { # Exibe a query SQL que será executada para buscar os contratos
            $this->debug($contratos);
        } else {
            $contratos = $contratos->get();
        }

        foreach ($contratos as $contrato) {
            try {
                # Busca os dados do contrato no PNCP
                $response = $this->clientHttp->get($contrato['link_pncp']);
                usleep(500000); # Para não ocorrer too many requests
            } catch (\Exception $e) {
                try {
                    $responseBody = json_decode($e->getResponse()->getBody()->getContents(), true);
                } catch (\Exception $jsonException) {
                    $log->error('Erro ao processar o contrato (erro no JSON): ' .
                        "Contrato: {$contrato['contrato_id']} - " .
                        "Erro: " . $jsonException->getMessage());
                    continue;
                }
                if (isset($responseBody['status']) && !$responseBody['status']) {
                    $log->info('Contrato não encontrado no PNCP: ' .
                        "Contrato: {$contrato['contrato_id']} - " .
                        "Contrato histórico: {$contrato['contratohistorico_id']} - " .
                        "Link PNCP: {$contrato['link_pncp']}");
                } else {
                    $log->error('Erro ao acessar o PNCP para o contrato: ' .
                        "Contrato: {$contrato['contrato_id']} - " .
                        "Contrato histórico: {$contrato['contratohistorico_id']} - " .
                        "Link PNCP: {$contrato['link_pncp']} - " .
                        "Erro: " . $e->getMessage());
                }
                continue;
            }
            $contratoPncpContents = $response->getBody()->getContents();
            $contratoPncp = json_decode($contratoPncpContents);
            # Se o contrato no PNCP for de RECEITA, marcar a linha como RETPEN
            if ($contratoPncp->receita) {
                # Lista o ID dos contratos de Despesa que estão como Receita no PNCP
                $log->info("Contrato: {$contrato['contrato_id']} - Linha PNCP: {$contrato['envia_dados_pncp_id']}");
                EnviaDadosPncp::where('id', $contrato['envia_dados_pncp_id'])
                    ->update([
                    'situacao' => $idRetPen
                ]);
            }
        }
        $log->info('Fim: ' . date('d/m/Y H:i:s'));
    }

    private function debug($contratos)
    {
        $sql = $contratos->toSql();
        $bindings = $contratos->getBindings();
        foreach ($bindings as $binding) {
            if (is_bool($binding)) {
                $value = $binding ? 'true' : 'false';
            } else {
                $value = is_numeric($binding) ? $binding : "'" . addslashes($binding) . "'";
            }
            $sql = preg_replace('/\?/', $value, $sql, 1);
        }
        dd($sql);
    }

}
