<?php

namespace App\Console\Commands;

use App\Http\Traits\BuscaCodigoItens;
use App\Models\Contratoempenho;
use App\Models\ContratoMinutaEmpenhoPivot;
use App\Models\MinutaEmpenho;
use App\Models\Unidade;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;


class PreencheContratoEmpenhoCommand extends Command
{

    use BuscaCodigoItens;

    protected $signature = 'contratoempenho:create';

    protected $description = 'Comando para criar contrato empenho para os antigos registros da minuta empenho';

    public function __construct()
    {
        parent::__construct();
    }


    public function handle()
    {
        $unidades = Unidade::all();

        foreach ($unidades as $unidade) {
            $query = MinutaEmpenho::select([
                'minutaempenhos.contrato_id',
                'empenhos.fornecedor_id as fornecedor_id',
                'empenhos.id as empenho_id',
                'empenhos.unidade_id as unidade_id',
            ])
                ->join('saldo_contabil', 'saldo_contabil.id', '=', 'minutaempenhos.saldo_contabil_id')
                ->join('empenhos', function ($join) {
                    $join->on('empenhos.numero', '=', 'minutaempenhos.mensagem_siafi')
                        ->on('empenhos.unidade_id', '=', 'saldo_contabil.unidade_id');
                })
                ->join('codigoitens as situacao', 'situacao.id', '=', 'minutaempenhos.situacao_id')
                ->join('codigoitens as tipo_empenhopor', 'tipo_empenhopor.id', '=', 'minutaempenhos.tipo_empenhopor_id')
                ->join('contratos', 'contratos.id', '=', 'minutaempenhos.contrato_id')
                ->where('situacao.descricao', '=', 'EMPENHO EMITIDO')
                ->where('tipo_empenhopor.descricao', '=', 'Contrato')
                ->where('empenhos.unidade_id', '=', $unidade->id);

            $minutaEmpenhos = $query->get();

            foreach ($minutaEmpenhos as $minutaEmpenho) {
                //$bar->advance();

                Contratoempenho::updateOrCreate([
                    'contrato_id' => $minutaEmpenho->contrato_id,
                    'empenho_id' => $minutaEmpenho->empenho_id,
                ], [
                    'fornecedor_id' => $minutaEmpenho->fornecedor_id,
                    'unidadeempenho_id' => $minutaEmpenho->unidade_id
                ]);
            }
        }


        $contratoMinutaEmpenhoPivots = ContratoMinutaEmpenhoPivot::all();

        foreach ($contratoMinutaEmpenhoPivots as $contratoMinutaEmpenhoPivot) {
            $contrato = $contratoMinutaEmpenhoPivot->contrato;

            $minutaEmpenho = MinutaEmpenho::select([
                'minutaempenhos.contrato_id',
                'empenhos.fornecedor_id as fornecedor_id',
                'empenhos.id as empenho_id',
                'empenhos.unidade_id as unidade_id',
            ])
                ->join('saldo_contabil', 'saldo_contabil.id', '=', 'minutaempenhos.saldo_contabil_id')
                ->join('empenhos', function ($join) {
                    $join->on('empenhos.numero', '=', 'minutaempenhos.mensagem_siafi')
                        ->on('empenhos.unidade_id', '=', 'saldo_contabil.unidade_id');
                })
                ->join('codigoitens as situacao', 'situacao.id', '=', 'minutaempenhos.situacao_id')
                ->join('codigoitens as tipo_empenhopor', 'tipo_empenhopor.id', '=', 'minutaempenhos.tipo_empenhopor_id')
                ->join('contratos', 'contratos.id', '=', 'minutaempenhos.contrato_id')
                ->where('situacao.descricao', '=', 'EMPENHO EMITIDO')
                ->where('tipo_empenhopor.descricao', '=', 'Compra')
//                ->where('empenhos.unidade_id', '=', $contrato->unidade_id)
                ->where('minutaempenhos.id', '=', $contratoMinutaEmpenhoPivot->minuta_empenho_id)
                ->first();

            if(isset($minutaEmpenho->empenho_id)) {
                Contratoempenho::updateOrCreate([
                    'contrato_id' => $contrato->id,
                    'empenho_id' => $minutaEmpenho->empenho_id,
                ], [
                    'fornecedor_id' => $minutaEmpenho->fornecedor_id,
                    'unidadeempenho_id' => $minutaEmpenho->unidade_id
                ]);
            }
        }
    }

}
