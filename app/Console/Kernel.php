<?php

namespace App\Console;

use App\Jobs\JobTest;
use App\Models\Feriado;
use App\Models\Sfhorarioexcecao;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use PDOException;

class Kernel extends ConsoleKernel
{
    protected $schedule = null;

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];
    protected $path;

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $this->schedule = $schedule;
        $this->path = env('APP_PATH');

        $this->criarJobs();

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }

    protected function criarJobs()
    {
        $ambiente = config('app.app_amb');
        //Log::channel('schedule')->info($ambiente);

        switch ($ambiente) {
            case 'Ambiente Produção':
                //minutos
                $this->criarJobAtualizarSfPadrao();
                $this->criarJobAtualizaStatusPublicacao();
                $this->criarJobMinutasEmProcessamento();
                $this->enviaPublicaoesProximoDiaUtil();

                //agendamentos
                $this->alertarTelegram();
                $this->criarJobAtualizarND();
                $this->criarJobMigrarEmpenhos();
                $this->criarJobRotinaAtualizaIndicesBacen();
                $this->criarJobAtualizarSaldosEmpenhosPorunidades();
                // envio de e-mail de alerta (diário e mensal) só deve ocorrer em Produção
                $this->criarJobEnviarEmailsAlertas();
                $this->criarJobLimparActivityLogs();
                $this->criarJobContratoEmpenho();
                $this->criarJobLoginPNCP();
                $this->rotinasPNCP();
                $this->criarJobGeraArquivoEmpenhosDiferente14133();
                $this->criarJobEnviaMinutaAguardandoAnulacao();
                $this->criaJobAtualizaPaises();
                $this->criarArquivosDeLog();
                $this->verificaVencimentoCertificadoSiafi();
                break;

            case 'Ambiente Homologação':
                //minutos
                $this->criarJobAtualizarSfPadrao();
                $this->criarJobAtualizaStatusPublicacao();
                $this->criarJobMinutasEmProcessamento();
                $this->enviaPublicaoesProximoDiaUtil();

                //agendamentos
                $this->alertarTelegram();
                $this->criarJobAtualizarND();
                $this->criarJobMigrarEmpenhos();
                $this->criarJobRotinaAtualizaIndicesBacen();
                $this->criarJobAtualizarSaldosEmpenhosPorunidades();
                $this->criarJobLimparActivityLogs();
                $this->criarJobContratoEmpenho();
                $this->criarJobLoginPNCP();
                $this->rotinasPNCP();
                $this->criarJobGeraArquivoEmpenhosDiferente14133();
                $this->criarJobEnviaMinutaAguardandoAnulacao();
                $this->criaJobAtualizaPaises();
                $this->criarArquivosDeLog();
                $this->verificaVencimentoCertificadoSiafi();
                break;

            case 'Ambiente Treinamento':
                $this->alertarTelegram();
                $this->criarJobAtualizarND();
                $this->criarJobMigrarEmpenhos();
                $this->criarJobRotinaAtualizaIndicesBacen();
                $this->criarJobAtualizarSaldosEmpenhosPorunidades();
                $this->criaJobAtualizaPaises();
                $this->criarArquivosDeLog();
                $this->verificaVencimentoCertificadoSiafi();
                break;

            default:
                $this->criarWsdlSiafi();
                //Log::channel('schedule')->info('Ambiente não mapeado. Nenhum job iniciado.');
                break;
        }
    }

    protected function criarJobAtualizarSfPadrao()
    {
        $this->schedule->call(
            'App\Http\Controllers\Gescon\ContratosfpadraoCrudController@executaJobAtualizacaoSfPadrao'
        )
            ->timezone('America/Sao_Paulo')
            ->weekdays()
            ->everyMinute();
    }

    protected function executaConsumoWsSiafiEmpenho()
    {
        $this->schedule->call(
            'App\Http\Controllers\Execfin\EmpenhoCrudController@incluirEmpenhoSiafi'
        )
            ->timezone('America/Sao_Paulo')
            ->weekdays()
            ->everyMinute()
            ->between('9:00', '19:30');
    }

    protected function criarJobEnviarEmailsAlertas()
    {
        $this->schedule->call(
            'App\Http\Controllers\Admin\AlertaContratoController@enviaEmails'
        )
            ->timezone('America/Sao_Paulo')
            ->at('08:00');
    }

    protected function criarJobAtualizarND()
    {
        $this->schedule->call(
            'App\Http\Controllers\Execfin\EmpenhoCrudController@executaAtualizacaoNd'
        )
            ->timezone('America/Sao_Paulo')
            ->at('01:00');
    }

    protected function criarJobMigrarEmpenhos()
    {
        $this->schedule->call(
            'App\Http\Controllers\Execfin\EmpenhoCrudController@executaMigracaoEmpenho'
        )
            ->timezone('America/Sao_Paulo')
            ->at('02:00');
    }

    protected function criarJobRotinaAtualizaIndicesBacen()
    {
        $this->schedule->call(
            'App\Http\Controllers\Admin\IndiceValoresCrudController@executaJobsRotinaAtualizaIndicesBacen'
        )
            ->timezone('America/Sao_Paulo')
            ->at('23:00');
    }

    protected function criarJobAtualizaStatusPublicacao()
    {
        $this->schedule->call(
            'App\Http\Controllers\Publicacao\DiarioOficialClass@executaJobAtualizaSituacaoPublicacao'
        )
            ->timezone('America/Sao_Paulo')
            ->weekdays()
            ->at('08:20');
    }

    protected function enviaPublicaoesProximoDiaUtil()
    {
        $this->schedule->call(
            'App\Http\Controllers\Publicacao\DiarioOficialClass@enviaPublicacoesViaKernel'
        )
            ->timezone('America/Sao_Paulo')
            ->weekdays()
            ->at('18:15');
    }

    protected function criarJobEnviarContratoTermosPncpKernel()
    {
        $this->schedule->call(
            'App\Http\Controllers\Admin\EnviaPNCPController@enviarContratoTermosPncpKernel'
        )
            ->timezone('America/Sao_Paulo')
            ->name('job-enviar-contrato-termos-pncp')
            ->withoutOverlapping()
            ->weekdays()
            ->everyTenMinutes();
    }

    protected function criarJobRetificarContratoPncpKernel()
    {
        $this->schedule->call(
            'App\Http\Controllers\Admin\EnviaPNCPController@retificarContratoPncpKernel'
        )
            ->timezone('America/Sao_Paulo')
            ->name('job-retificar-contrato-pncp-kernel')
            ->withoutOverlapping()
            ->weekdays()
            ->everyTenMinutes();
    }

    protected function criarJobRemoverContratoPncpKernel()
    {
        $this->schedule->call(
            'App\Http\Controllers\Admin\EnviaPNCPController@removerContratoPncpKernel'
        )
            ->timezone('America/Sao_Paulo')
            ->name('job-remover-contrato-pncp-kernel')
            ->withoutOverlapping()
            ->weekdays()
            ->everyTenMinutes();
    }

    protected function criarJobEnviarArquivoContratoPncpKernel()
    {
        $this->schedule->call(
            'App\Http\Controllers\Admin\EnviaPNCPController@enviarArquivoContratoPncpKernel'
        )
            ->timezone('America/Sao_Paulo')
            ->name('job-enviar-arquivo-contrato-pncp-kernel')
            ->withoutOverlapping()
            ->weekdays()
            ->everyTenMinutes();
    }

    private function rotinasPNCP()
    {
        $this->criarJobEnviarContratoTermosPncpKernel();
        $this->criarJobRetificarContratoPncpKernel();
        $this->criarJobRemoverContratoPncpKernel();
        $this->criarJobEnviarArquivoContratoPncpKernel();
    }

    private function alertarTelegram()
    {
        $this->verificarJobsTravados();
        $this->verificarQueriesTravadas();
    }

    protected function criarJobGeraArquivoEmpenhosDiferente14133()
    {
        $this->schedule->call(
            'App\Http\Controllers\Admin\EnviaPNCPController@buscaArquivoEmpenhoAssinadoDiferente14133'
        )
            ->timezone('America/Sao_Paulo')
            ->name('job-gera-arquivo-empenhos-diferente-lei-14133')
            ->withoutOverlapping()
            ->weekdays()
            // só procura o arquivo no SIAFI no horário de funcionamento
            ->between(config('app.horario_siafi.abertura'), config('app.horario_siafi.fechamento'))
            ->cron('0 9,12,18 * * *')
//            ->everyFiveMinutes()
            ->skip(function () {
                $date = date('Y-m-d');
                $feriados = Feriado::all()->pluck('data')->toArray();
                if (in_array($date, $feriados)) {
                    return true;
                }
                return false;
            });
    }

    protected function criarJobAtualizarSaldoDeEmpenhos() // job travando
    {
        // $this->schedule->call(
        //     'App\Http\Controllers\Execfin\EmpenhoCrudController@executaAtualizaSaldosEmpenhos'
        // )
        //     ->timezone('America/Sao_Paulo')
        //     ->at('03:30');

    }

    protected function criarJobAtualizarSaldosEmpenhosPorunidades()
    {
        $this->schedule->call(
            'App\Http\Controllers\Execfin\EmpenhoCrudController@rotinaAtualizaSaldoAgendamento'
        )
            ->timezone('America/Sao_Paulo')
            ->at('03:30');
    }

    protected function criarJobAtualizarSaldoDeRPs()
    {
        $this->schedule->call(
            'App\Http\Controllers\Execfin\EmpenhoCrudController@executaAtualizaSaldosRPs'
        )
            ->timezone('America/Sao_Paulo')
            ->weekdays()
            ->at('04:00');
    }

    protected function criarJobEnvioEmpenhoSiasg()
    {
        $this->schedule->call(
            'App\Http\Controllers\Execfin\EmpenhoCrudController@enviaEmpenhoSiasg'
        )
            ->timezone('America/Sao_Paulo')
            ->weekdays()
            ->everyFifteenMinutes()
            ->between('09:15', '19:15')
            ->skip(function () {
                $date = date('Y-m-d');
                $feriados = Feriado::all()->pluck('data')->toArray();
                if (in_array($date, $feriados)) {
                    return true;
                }
                return false;
            });
    }

    protected function criarJobMinutasEmProcessamento()
    {
        $tabela = 'sfhorarioexcecao';
        try {
            if (Schema::hasTable($tabela)) {
                //Log::channel('schedule')->info("Executando criarJobMinutasEmProcessamento.");
                $hoje = Carbon::now()->toDateString();
                $busca = Sfhorarioexcecao::where('data', $hoje)->exists();
                if ($busca) {
                    $hora_inicio = date('H:i', strtotime("+15 minute", strtotime(Sfhorarioexcecao::where('data', $hoje)->value('hora_inicio'))));
                    $hora_fim = date('H:i', strtotime("-15 minute", strtotime(Sfhorarioexcecao::where('data', $hoje)->value('hora_fim'))));

                    $this->schedule->call(
                        'App\Http\Controllers\Empenho\Minuta\ProcessaNovamenteController@index'
                    )
                        ->timezone('America/Sao_Paulo')
                        ->everyFifteenMinutes()
                        ->between($hora_inicio, $hora_fim);

                } else {
                    $this->schedule->call(
                        'App\Http\Controllers\Empenho\Minuta\ProcessaNovamenteController@index'
                    )
                        ->timezone('America/Sao_Paulo')
                        ->weekdays()
                        ->everyFifteenMinutes()
                        ->between(config('app.horario_siafi.abertura'), config('app.horario_siafi.fechamento'))
                        ->skip(function () {
                            $date = date('Y-m-d');
                            $feriados = Feriado::all()->pluck('data')->toArray();
                            if (in_array($date, $feriados)) {
                                return true;
                            }
                            return false;
                        });
                }
            } else {
                //Log::channel('schedule')->info("Tabela $tabela não encontrada. Função criarJobMinutasEmProcessamento não executada.");
            }
        } catch (PDOException $e) {
            //Log::channel('schedule')->info('Falha no driver do BD: ' . $e->getMessage());
        }
    }

    protected function criarJobLoginPNCP()
    {
        //Log::channel('schedule')->info("JOB login PNCP");
        $this->schedule->call('App\Jobs\LoginPNCP@handle')
            ->timezone('America/Sao_Paulo')
            ->weekdays()
            ->everyThirtyMinutes();
    }

    protected function criarJobLimparActivityLogs()
    {
        $this->schedule->call(
            'App\Jobs\LimpaActivityLogJob@handle'
        )
            ->timezone('America/Sao_Paulo')
            ->dailyAt('23:30');
    }

    protected function criarJobContratoEmpenho()
    {
        $this->schedule->call(
            'App\Jobs\ContratoEmpenhoJob@handle'
        )
            ->timezone('America/Sao_Paulo')
            ->at('23:30');
    }

    protected function criarJobAgendamentoComunica()
    {
        Log::info("JOB enviar Comunica");
        $this->schedule->call(
            'App\Http\Controllers\Admin\ComunicaCrudController@disparaNotificacao'
        )
            ->timezone('America/Sao_Paulo')
            ->everyMinute();
    }

    private function executaCommand($fila, $horario = '09:00', $quantidadeExecucoes = 1, $timeout = 600, $tries = 1)
    {
        for ($i = 1; $i <= $quantidadeExecucoes; $i++) {
            $this->schedule->exec(
                "php $this->path" . "artisan queue:work --queue=$fila --stop-when-empty --timeout=$timeout --tries=$tries"
            )
                ->timezone('America/Sao_Paulo')
                // ->weekdays() // Pode ser diário. Se não houver fila, nada será executado!
                ->at($horario)
                ->runInBackground();
        }
    }

    private function executaCommandCron($fila, $quantidadeExecucoes = 1, $timeout = 600, $tries = 1, $minuto = '*', $hora = '*', $diasmes = '*', $meses = '*', $diassemana = '*')
    {
        for ($i = 1; $i <= $quantidadeExecucoes; $i++) {
            $this->schedule->exec(
                "php $this->path" . "artisan queue:work --queue=$fila --stop-when-empty --timeout=$timeout --tries=$tries"
            )
                ->timezone('America/Sao_Paulo')
                // ->weekdays() // Pode ser diário. Se não houver fila, nada será executado!
                ->cron("$minuto $hora $diasmes $meses $diassemana")
                ->runInBackground();
        }
    }

    /**
     * Criar job para enviar minutas aguardando anulação cujas remessas já foram enviadas
     * @return void
     */
    protected function criarJobEnviaMinutaAguardandoAnulacao()
    {
        $this->schedule->call(
            'App\Http\Controllers\Api\MinutaEmpenhoController@populaTabelasSiafiAnulacaoKernel'
        )
            ->timezone('America/Sao_Paulo')
            ->name('enviar-minutas-aguardando-anulacao')
            ->withoutOverlapping()
            ->weekdays()
            ->hourly()
            ->between('9:00', '19:30');
    }

    protected function criaJobAtualizaPaises()
    {
        $this->schedule->call(
            'App\Http\Controllers\PaisesController@atualizaPaises'
        )
            ->timezone('America/Sao_Paulo')
            ->name('atualiza-paises')
            ->withoutOverlapping()
            ->monthly()
            ->between('9:00', '19:30');
    }

    /**
     * Cria determinados arquivos de log para evitar problemas de permissão de gravação.
     *
     * @return void
     */
    protected function criarArquivosDeLog()
    {
        $this->schedule->call('App\Http\Controllers\LogController@criarArquivosDeLog')
            ->timezone('America/Sao_Paulo')
            ->at('00:10');
    }

    protected function verificaVencimentoCertificadoSiafi()
    {
        $this->schedule->call(
            'App\Http\Controllers\Admin\SfcertificadoController@verificaVencimentoCertificadoSiafi'
        )
            ->timezone('America/Sao_Paulo')
            ->at('01:00');

    }

    //protected function verificarJobsEQueriesTravadas()
    protected function verificarJobsTravados(): void
    {
        if (env('TELEGRAM', false)) {
            $this->schedule->call(
                'App\Http\Controllers\TelegramBotController@verificarJobsTravados'
            )
                ->timezone('America/Sao_Paulo')
                ->name('verificar_jobs_travados')
                ->withoutOverlapping()
                ->everyFiveMinutes();
        }

    }

    protected function verificarQueriesTravadas()
    {
        if (env('TELEGRAM', false)) {
            $this->schedule->call(
                'App\Http\Controllers\TelegramBotController@verificarQueriesTravadas'
            )
                ->timezone('America/Sao_Paulo')
                ->name('verificar_queries_travadas')
                ->withoutOverlapping()
                ->hourly();
        }
    }

    private function criarWsdlSiafi(): void
    {
            $this->schedule->call(
                'App\Http\Controllers\DownloadsController@setWsdl'
            )
                ->timezone('America/Sao_Paulo')
                ->at('10:00');
    }
}
