<?php

namespace App\Exceptions;

use Carbon\Carbon;
use Exception;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Migrations\MigrationCreator;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use PDOException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    private function salvarRegistro(Exception $exception, string $fileMigration) {
        $nomeBranch = config('app.app_version');

        $propierts = [  'codeErro' => $exception->getcode(),
                        'fileMigration' => $fileMigration,
                        'branch' => $nomeBranch,
                        'mensagemResumida' =>$exception->getMessage()];

        $mensagem =  substr($exception, 0, 2700);

        activity()
        ->useLog("ERRO_MIGRATION")
        ->withProperties($propierts)
        ->log($mensagem);
    }

    private function validarUsoMigration(Exception $exception, int $numeroArray) {

        if(isset($exception->gettrace()[$numeroArray])) {

            $funcaoExecutadaComErro = $exception->gettrace()[$numeroArray];

            if(isset($funcaoExecutadaComErro['function'])) {

                if($funcaoExecutadaComErro['function'] == 'runMigration') {

                    if(isset($funcaoExecutadaComErro['args'])) {

                        if($funcaoExecutadaComErro['args'][1] == 'up') {
                            $this->inserirLogMigration($exception, 11);
                        }

                    }

                }

                if($funcaoExecutadaComErro['function'] == 'runUp') {
                    $this->inserirLogMigration($exception, $numeroArray);
                }
            }

        }

    }

    private function registrarErroMigrateSintaxeArquivo(Exception $exception) {

        $arrayFile = explode('/',$exception->getFile());
        $arquivoMigrator = end($arrayFile);

        if ($arquivoMigrator == 'Migrator.php') {
            $this->salvarRegistro($exception, 'Erro ao carregar a migration');
        }

    }

    private function inserirLogMigration(Exception $exception, int $numeroArray) {
        if(isset($exception->gettrace()[$numeroArray])) {

            $informacoes = $exception->gettrace()[$numeroArray];
            $arrayFileMigration = explode("/",$informacoes["args"][0]);
            $dadosArquivoMigration = end($arrayFileMigration);

            $this->salvarRegistro($exception, $dadosArquivoMigration);
        }
    }
    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {

        $errosBD = config('error-postgress');
        if (in_array($exception->getcode(),$errosBD)) {
            $this->validarUsoMigration($exception, 10);
        }

        $this->validarUsoMigration($exception, 17);

        $this->registrarErroMigrateSintaxeArquivo($exception);

        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        $debug = env('APP_DEBUG');
        
        if($debug === true){
            return parent::render($request, $exception);
        }

        $errorsCode = [400, 401, 403, 404, 405, 408, 429, 503];

        if ($exception instanceof Exception) {
            $statusCode = method_exists($exception, 'getStatusCode')
                ? $exception->getStatusCode()
                : $exception->getCode();

            if (in_array($statusCode, $errorsCode) || $exception instanceof ValidationException) {
                return parent::render($request, $exception);
            }

            return $this->customiseError500($exception);
        }
    }

    private function customiseError500(Exception $exception)
    {
        $node = config('app.server_node');
        $date = Carbon::now()->format('Y-m-d H:i:s.u');
        $errorCode = preg_replace("/[^0-9]/", "", $date);
        $errorTitleMsg = 'Um erro inesperado aconteceu.';
        $errorMsg = "Essa situação pode ser temporária, então pedimos que tente realizar a ação novamente.<br>
                    Caso o problema persista
                    <a href='https://portaldeservicos.economia.gov.br/'>informe o erro</a>
                    indicando os passos que executou antes de ocorrer o problema, juntamente com a
                    URL e o código do erro para que a equipe possa rastrear e corrigir.
                    <br> Código: $errorCode";
        Log::error("Nó: $node ; Código: $errorCode ". $exception->getMessage());
        return response()->view('errors.500', compact('errorMsg', 'errorTitleMsg'), 500);
    }
}
