<?php

namespace App\Exceptions;

use Exception;

class EmptyCollectionException extends Exception
{
    protected $message = 'Collection is empty';
}