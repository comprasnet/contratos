<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use App\Models\TipoIndices;
class IndiceValores extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'indice_valores';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['tipo_indices_codigo','mes','ano', 'valor','variacao', 'acumulado_mes','acumulado_12_meses'];
    protected $appends = ['competencia'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getCompetenciaAttribute(){
        return $this->mes.$this->ano;
    }
    public function getFonte()
    {
        $fonte = TipoIndices::where('codigo',$this->tipo_indices_codigo)->pluck('fonte')->first();
        if ($fonte == "bacen"){
            $fonte = "BCB";
        } elseif ($fonte == "outras"){
            switch ($this->tipo_indices_codigo){
            case '9998':
               $fonte = "ANATEL";
               break;
            case '9997':
                $fonte = "IPEA";
                break;
            }
        }
        return $fonte;
    }

    public function getForma_calculo()
    {
        $forma_calculo = TipoIndices::where('codigo',$this->tipo_indices_codigo)->pluck('forma_calculo')->first();
        if($forma_calculo == "auto"){
            $forma_calculo = "automático";
        }
        return $forma_calculo;
    }

    public function getIndice_Nome()
    {
        //$indice_nome = TipoIndices::where('codigo',$this->tipo_indices_codigo)->pluck('nome')->first();  
        $indice_nome = $this->TipoIndice->nome;     
        return $indice_nome;        
        //return $this->TipoIndices->nome;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    function TipoIndice(){
        return $this->belongsTo(TipoIndices::class,'tipo_indices_codigo', 'codigo');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
