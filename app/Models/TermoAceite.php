<?php

namespace App\Models;

use App\User;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class TermoAceite extends Model
{
    use CrudTrait;
    use LogsActivity;

    protected static $logFillable = true;
    protected static $logName = 'termo_aceites';
    protected $table = 'termo_aceites';

    protected $fillable = [
        'user_id',
        'tipo_id',
        'status_id',
        'info'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    public function getUser()
    {
        return $this->user->name;
    }

    public function getTipo()
    {
        return $this->tipo->descricao;
    }

    public function getStatus()
    {
        return $this->status->descricao ?? null;
    }

    public function tipo()
    {
        return $this->belongsTo(Codigoitem::class, 'tipo_id');
    }

    public function status()
    {
        return $this->belongsTo(Codigoitem::class, 'status_id');
    }
}
