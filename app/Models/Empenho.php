<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Http\Traits\Formatador;

class Empenho extends Model
{
    use CrudTrait;
    use LogsActivity;
    protected static $logFillable = true;
    protected static $logName = 'empenho';
    use SoftDeletes;
    use Formatador;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'empenhos';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = [
        'numero',
        'unidade_id',
        'fornecedor_id',
        'planointerno_id',
        'naturezadespesa_id',
        'empenhado',
        'aliquidar',
        'liquidado',
        'pago',
        'rpinscrito',
        'rpaliquidar',
        'rpliquidado',
        'rppago',
        'rp',
        'fonte',
        'informacao_complementar',
        'sistema_origem',
        'data_emissao',
        'modalidade_licitacao_siafi',
        'ptres'
    ];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function inserirEmpenhoMigracaoConta(array $dados)
    {
        $this->fill($dados);
        $this->save();

        return $this;
    }

    public function buscaEmpenhosPorAnoUg(int $ano, int $unidade)
    {
        $empenhos = Empenho::whereHas('unidade', function ($q) use ($unidade){
            $q->where('codigo',$unidade);
        })
            ->where('numero', 'LIKE', $ano . 'NE%')
            ->get();

        return $empenhos;
    }

    public function buscaEmpenhosPorUg(int $unidade)
    {
        $empenhos = Empenho::whereHas('unidade', function ($q) use ($unidade){
            $q->where('codigo',$unidade);
        })
            ->get();

        return $empenhos;
    }

    public static function buscaNumeroIdEmpenhoUg(int $unidade, string $numero )
    {
        return Empenho::where('numero', $numero)
            ->where('unidade_id', $unidade)
            ->first()->id ?? null;
    }

    public function getFornecedor()
    {
        $fornecedor = Fornecedor::find($this->fornecedor_id);
        return $fornecedor->cpf_cnpj_idgener . ' - ' . $fornecedor->nome;

    }

    public function getUnidade()
    {
        $unidade = Unidade::find($this->unidade_id);
        return $unidade->codigo . ' - ' . $unidade->nomeresumido;

    }

    public function getNatureza()
    {
        if ($this->naturezadespesa_id) {

            $naturezadespesa = Naturezadespesa::find($this->naturezadespesa_id);
            return $naturezadespesa->codigo . ' - ' . $naturezadespesa->descricao;

        } else {
            return '';
        }
    }

    public function getPi()
    {
        if ($this->planointerno_id) {
            $planointerno = Planointerno::find($this->planointerno_id);
            return $planointerno->codigo . ' - ' . $planointerno->descricao;
        } else {
            return '-';
        }
    }

    public function formatVlrEmpenhado()
    {
        return 'R$ ' . number_format($this->empenhado, 2, ',', '.');
    }

    public function formatVlraLiquidar()
    {
        return 'R$ ' . number_format($this->aliquidar, 2, ',', '.');
    }

    public function formatVlrLiquidado()
    {
        return 'R$ ' . number_format($this->liquidado, 2, ',', '.');
    }

    public function formatVlrPago()
    {
        return 'R$ ' . number_format($this->pago, 2, ',', '.');
    }

    public function formatVlrRpInscrito()
    {
        return 'R$ ' . number_format($this->rpinscrito, 2, ',', '.');
    }

    public function formatVlrRpaLiquidar()
    {
        return 'R$ ' . number_format($this->rpaliquidar, 2, ',', '.');
    }

    public function formatVlrRpLiquidado()
    {
        return 'R$ ' . number_format($this->rpliquidado, 2, ',', '.');
    }

    public function formatVlrRpPago()
    {
        return 'R$ ' . number_format($this->rppago, 2, ',', '.');
    }

    /**
     * Retorna Empenhos e Fontes conforme $conta informada
     *
     * @param string $conta
     * @return array
     */
    public function retornaEmpenhoFontePorConta($conta)
    {
        // Dados de todos os empenhos - em memória
        $empenhos = session('empenho.fonte.conta');
        $pkCount = (is_array($empenhos) ? count($empenhos) : 0);
        if ($pkCount == 0) {
            // Se não houver dados na session, busca os dados no banco
            $empenhos = $this->retornaEmpenhosFonteConta($conta);
            session(['empenho.fonte.conta' => $empenhos]);
        }


        $registrosEncontrados = array_filter($empenhos, function ($empenho) use ($conta) {

            return ($empenho->nd == $conta);
        });


        return $registrosEncontrados;
    }

    /**
     * Retorna conjunto de Empenhos, fonte e conta (nd + subitem) por $ug
     *
     * @return array
     */
    public function retornaEmpenhosFonteConta()
    {
        $ug = session('user_ug_id');

        $sql = '';
        $sql .= 'SELECT ';
        $sql .= '	E.numero AS ne, ';
        $sql .= "	E.fonte AS fonte, ";
        $sql .= '	N.codigo || I.codigo AS nd ';
        $sql .= 'FROM';
        $sql .= '	empenhos AS E ';
        $sql .= 'LEFT JOIN ';
        $sql .= '	empenhodetalhado AS D on ';
        $sql .= '	D.empenho_id = E.id ';
        $sql .= 'LEFT JOIN ';
        $sql .= '	naturezasubitem AS I on ';
        $sql .= '	I.id = D.naturezasubitem_id ';
        $sql .= 'LEFT JOIN ';
        $sql .= '	naturezadespesa AS N on ';
        $sql .= '	N.id = I.naturezadespesa_id ';
        $sql .= 'WHERE ';
        $sql .= '	E.unidade_id = ?';
        $sql .= 'ORDER BY ';
        $sql .= '    nd, ';
        $sql .= '    ne ';

        $dados = DB::select($sql, [$ug]);

        return $dados;
    }

    public function retornaDadosEmpenhosGroupUgArray()
    {

        $unidade = Unidade::find(session()->get('user_ug_id'));

        $valores_empenhos = Empenho::whereHas('unidade', function ($q) use ($unidade) {
            $q->whereHas('orgao', function ($o) use ($unidade){
               $o->where('id',$unidade->orgao_id);
            });
            $q->where('situacao', '=', true);
        });
        $valores_empenhos->whereHas('naturezadespesa', function ($q) {
            $q->where('codigo', 'LIKE', '33%');
        });
        $valores_empenhos->leftjoin('unidades', 'empenhos.unidade_id', '=', 'unidades.id');
        $valores_empenhos->orderBy('nome');
        $valores_empenhos->groupBy('unidades.codigo');
        $valores_empenhos->groupBy('unidades.nomeresumido');
        $valores_empenhos->select([
            DB::raw("unidades.codigo ||' - '||unidades.nomeresumido as nome"),
            DB::raw('sum(empenhos.empenhado) as empenhado'),
            DB::raw("sum(empenhos.aliquidar) as aliquidar"),
            DB::raw("sum(empenhos.liquidado) as liquidado"),
            DB::raw("sum(empenhos.pago) as pago")
        ]);

        return $valores_empenhos->get()->toArray();

    }

    public function retornaDadosEmpenhosSumArray()
    {

        $valores_empenhos = Empenho::whereHas('unidade', function ($q) {
            $q->where('situacao', '=', true);
        });
        $valores_empenhos->whereHas('naturezadespesa', function ($q) {
            $q->where('codigo', 'LIKE', '33%');
        });
        $valores_empenhos->leftjoin('unidades', 'empenhos.unidade_id', '=', 'unidades.id');
        $valores_empenhos->select([
//            DB::raw("unidades.codigo ||' - '||unidades.nomeresumido as nome"),
            DB::raw('sum(empenhos.empenhado) as empenhado'),
            DB::raw("sum(empenhos.aliquidar) as aliquidar"),
            DB::raw("sum(empenhos.liquidado) as liquidado"),
            DB::raw("sum(empenhos.pago) as pago")
        ]);

        return $valores_empenhos->get()->toArray();

    }

    public function buscaEmpenhosAll($range, $rp = false)
    {
        $empenhos = Empenho::whereHas('unidade', function ($u) {
            $u->where('sigilo', false);} // Não retorna empenhos de unidades sigilosas.
        )->where('sistema_origem', 'COMPRASNET CONTRATOS'
        )->when($range != null, function ($r) use ($range) {
            $r->whereBetween('empenhos.updated_at', [$range[0], $range[1]]);}
        );

        if($rp){
            $empenhos->where('rp',true);
        }else{
            $empenhos->where('numero','like',$range[0]->year.'NE%');
        }

        return $empenhos->get();

    }

    public function buscaEmpenhosAllCipi($range, $rp = false)
    {

        $empenhos = DB::select( DB::raw("SELECT
                                            e.id AS id_empenho,
                                            u.codigo AS codigo_unidade_empenho,
                                            u.nome AS nome_unidade_empenho,
                                            u.gestao AS gestao_unidade_empenho,
                                            e.numero AS numero_empenho,
                                            e.data_emissao AS data_emissao_empenho,
                                            (SELECT f.cpf_cnpj_idgener || ' | ' || f.nome
                                             FROM fornecedores f
                                             WHERE f.id = e.fornecedor_id) AS doc_id_nome_fornecedor,
                                            e.fonte AS fonte_empenho,
	                                        e.ptres AS ptres_empenho,
                                            SUBSTRING(sc.conta_corrente,1,1) AS esfera,
                                            SUBSTRING(sc.conta_corrente,24,8) AS ugr,
	                                        e.modalidade_licitacao_siafi,
                                            (SELECT n.codigo || ' | ' || n.descricao
	                                         FROM naturezadespesa n
	                                         WHERE n.id = e.naturezadespesa_id) AS codigo_descricao_nd,
                                            (SELECT p.codigo || ' | ' || p.descricao
                                             FROM planointerno p
	                                         WHERE p.id = e.planointerno_id) AS codigo_descricao_planointerno,
                                            e.empenhado AS valor_empenhado,
	                                        e.aliquidar AS valor_aliquidar,
                                            e.liquidado AS valor_liquidado,
                                            e.pago AS valor_pago,
                                            e.rpinscrito AS valor_rpinscrito,
                                            e.rpaliquidar AS valor_rpaliquidar,
                                            e.rpliquidado AS valor_rpliquidado,
                                            e.rppago AS valor_rppago,
                                            e.informacao_complementar,
                                            e.sistema_origem,
                                            m.contrato_id,
                                            m.compra_id,
                                            m.numero_cipi AS id_cipi,
                                            c.descricao AS tipo_minuta,
                                            CASE
                                                WHEN c.descricao = 'Compra' THEN (SELECT u2.codigo || c2.descres || REPLACE (c.numero_ano, '/', '')
                                                                                  FROM compras c
                                                                                  INNER JOIN unidades u2 ON c.unidade_origem_id = u2.id
                                                                                  INNER JOIN codigoitens c2 ON c.modalidade_id = c2.id
                                                                                  WHERE c.id = m.compra_id)
                                                WHEN c.descricao = 'Contrato' THEN (SELECT u3.codigo || c4.descres || REPLACE(c3.numero, '/', '')
                                                                                    FROM contratos c3
                                                                                    INNER JOIN unidades u3 ON c3.unidade_id = u3.id
                                                                                    INNER JOIN codigoitens c4 ON c3.tipo_id = c4.id
                                                                                    WHERE c3.id = m.contrato_id)
                                                ELSE ''
                                            END AS origem_tipo_minuta,
                                            c2.descricao AS tipo_empenho,
                                            m.processo,
                                            m.descricao,
                                            m.local_entrega,
                                            al.codigo as amparo_legal,
                                            e.created_at,
	                                        e.updated_at
                                        FROM empenhos as e
                                        INNER JOIN unidades u ON e.unidade_id = u.id
                                        INNER JOIN minutaempenhos m ON e.numero = m.mensagem_siafi
                                        INNER JOIN saldo_contabil sc ON (m.saldo_contabil_id = sc.id AND e.unidade_id = sc.unidade_id)
                                        INNER JOIN codigoitens c ON m.tipo_empenhopor_id = c.id
                                        LEFT JOIN codigoitens c2 ON m.tipo_empenho_id = c2.id
                                        LEFT JOIN amparo_legal al ON m.amparo_legal_id = al.id
                                        WHERE u.sigilo IS FALSE
                                        AND u.deleted_at IS NULL
                                        AND e.deleted_at IS NULL
                                        AND m.deleted_at IS NULL
                                        AND e.sistema_origem = 'COMPRASNET CONTRATOS'
                                        AND m.numero_cipi ~ '^[0-9]'
                                        AND e.updated_at BETWEEN :date_begin AND :date_end")
                                , array('date_begin' => $range[0], 'date_end' => $range[1])
                            );

        return $empenhos;

    }

    // public function getTodosEmpenhosByUasgByNumeroEmpenho($uasgEmitente, $numeroEmpenho){

    //     /*

    //     Retorno:

    //     "id":
    //     "contrato_id":
    //     "compra_id":
    //     "numero": "2021NE000024",
    //     "unidade": "170195 - GRA/GO",
    //     "fornecedor": "04.795.101/0001-57 - FENIX ASSESSORIA & GESTAO EMPRESARIAL LTDA",

    //     PROBLEMAS:
    //     "naturezadespesa": "339039 - OUTROS SERVICOS DE TERCEIROS - PESSOA JURIDICA", => minutaempenho não faz relacionamento com naturezadespesa
    //     "empenhado": "54.491,21", => só em em empenhos
    //     "aliquidar": "0,00", => só em em empenhos
    //     "liquidado": "0,00", => só em em empenhos
    //     "pago": "54.491,21", => só em em empenhos
    //     "rpinscrito": "0,00", => só em em empenhos
    //     "rpaliquidar": "0,00", => só em em empenhos
    //     "rpaliquidado": "0,00", => só em em empenhos
    //     "rppago": "0,00" => só em em empenhos




    //     select
    //         m.id as minutaempenho_id, m.contrato_id, m.compra_id, m.mensagem_siafi as minutaempenho_numero,
    //         u.id as unidade_id, u.codigo as unidade_codigo, u.nomeresumido as unidade_nomeresumido,
    //         fc.nome as fornecedor_compra_nome, fc.cpf_cnpj_idgener as fornecedor_compra_cpf_cnpj_idgener,
    //         fe.nome as fornecedor_empenho_nome, fe.cpf_cnpj_idgener as fornecedor_empenho_cpf_cnpj_idgener

    //     from	minutaempenhos m
    //     inner	join unidades u on u.id = m.unidade_id
    //     inner	join fornecedores fc on fc.id = m.fornecedor_compra_id
    //     inner	join fornecedores fe on fe.id = m.fornecedor_empenho_id
    //     where	m.mensagem_siafi = '0001NE002021'
    //     and		u.codigo = '070001'
    //     */


    //     $empenhos = Empenho::where('empenhos.numero', $numeroEmpenho)
    //         ->select(

    //                     'minutaempenhos.id as minutaempenho_id')

    //         ->where('empenhos.numero', $numeroEmpenho)
    //         ->join('unidades', 'unidades.id', 'empenhos.unidade_id')
    //         ->join('contratoempenhos', 'contratoempenhos.empenho_id', 'empenhos.id')
    //         ->join('contratos', 'contratos.id', 'contratoempenhos.contrato_id')
    //         ->join('fornecedores', 'fornecedores.id', 'contratos.fornecedor_id')
    //         ->join('naturezadespesa', 'naturezadespesa.id', 'empenhos.naturezadespesa_id')

    //         // minuta empenho do contrato
    //         ->leftjoin('minutaempenhos', 'minutaempenhos.contrato_id', 'contratos.id')

    //         ->where('unidades.codigo', $uasgEmitente)->get();
    //     return $empenhos;




    //     $empenhos = Empenho::where('empenhos.numero', $numeroEmpenho)
    //         ->select(

    //                     'empenhos.id as empenho_id', 'empenhos.numero as empenho_numero', 'empenhos.empenhado as empenho_valor_empenhado',
    //                     'empenhos.aliquidar as empenho_valor_a_liquidar', 'empenhos.pago as empenho_valor_pago', 'empenhos.rpinscrito as empenho_valor_rpinscrito',
    //                     'empenhos.rpaliquidar as empenho_valor_rpaliquidar', 'empenhos.rpliquidado as empenho_valor_rpliquidado', 'empenhos.rppago as empenho_valor_rppago',
    //                     'contratoempenhos.contrato_id as contrato_id',
    //                     'unidades.codigo as unidade_codigo', 'unidades.nomeresumido as unidade_nomeresumido',
    //                     'fornecedores.cpf_cnpj_idgener as fornecedor_cpf_cnpj_idgener', 'fornecedores.nome as fornecedor_nome',
    //                     'naturezadespesa.codigo as naturezadespesa_codigo', 'naturezadespesa.descricao as naturezadespesa_descricao',
    //                     'minutaempenhos.compra_id as minutaempenhos_compra_id', 'minutaempenhos.empenhocontrato as minutaempenho_empenhocontrato')

    //         ->where('empenhos.numero', $numeroEmpenho)
    //         ->join('unidades', 'unidades.id', 'empenhos.unidade_id')
    //         ->join('contratoempenhos', 'contratoempenhos.empenho_id', 'empenhos.id')
    //         ->join('contratos', 'contratos.id', 'contratoempenhos.contrato_id')
    //         ->join('fornecedores', 'fornecedores.id', 'contratos.fornecedor_id')
    //         ->join('naturezadespesa', 'naturezadespesa.id', 'empenhos.naturezadespesa_id')

    //         // minuta empenho do contrato
    //         ->leftjoin('minutaempenhos', 'minutaempenhos.contrato_id', 'contratos.id')

    //         ->where('unidades.codigo', $uasgEmitente)->get();
    //     return $empenhos;
    // }




    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function unidade()
    {
        return $this->belongsTo(Unidade::class, 'unidade_id');
    }

    public function fornecedor()
    {
        return $this->belongsTo(Fornecedor::class, 'fornecedor_id');
    }

    public function naturezadespesa()
    {
        return $this->belongsTo(Naturezadespesa::class, 'naturezadespesa_id');
    }

    public function planointerno()
    {
        return $this->belongsTo(Planointerno::class, 'planointerno_id');
    }

    public function empenhodetalhado()
    {
        return $this->hasMany(Empenhodetalhado::class, 'empenho_id');
    }

    public function contratoempenho()
    {
        return $this->hasOne(Contratoempenho::class, 'empenho_id');
    }

    public function contratoEmpenhos()
    {
        return $this->belongsToMany(
            'App\Models\Contrato',
            'contratoempenhos',
            'empenho_id',
            'contrato_id'
        );
    }
    public function contratofaturaempenho()
    {
        return $this->hasOne(ContratoFaturaEmpenho::class, 'empenho_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function getNumeroAliquidarAttribute()
    {
        return $this->numero . ' - ' . $this->aliquidar;
    }

    public function descricao()
    {
        $dataEmissao = $this->data_emissao ? $this->retornaDataAPartirDeCampo($this->data_emissao) : $this->retornaDataAPartirDeCampo($this->created_at, 'Y-m-d H:i:s', 'd/m/Y');
        return "{$this->numero} - {$this->unidade->codigo} - {$this->fornecedor->nome} - {$dataEmissao}";
    }


}
