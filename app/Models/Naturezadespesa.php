<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Naturezadespesa extends Model
{
    use CrudTrait;
    use LogsActivity;
    use SoftDeletes;

    protected static $logFillable = true;
    protected static $logName = 'naturezadespesa';
    protected $fillable = [
        'codigo',
        'descricao',
        'situacao',
        'sistema_origem'
    ];
    protected $table = 'naturezadespesa';

    public function buscaNaturezadespesa(array $dado)
    {
        $nd = $this->where('codigo',$dado['codigo_nd'])
            ->where('sistema_origem',$dado['sistema_origem'])
            ->first();

        if(!isset($nd->id)){
            $nd = new Naturezadespesa();
            $nd->codigo = $dado['codigo_nd'];
            $nd->descricao = $dado['descricao_nd'];
            $nd->sistema_origem = $dado['sistema_origem'];
            $nd->situacao = true;
            $nd->save();
        }else{
            $nd->descricao = $dado['descricao_nd'];
            $nd->save();
        }
        return $nd;
    }

    public function naturezasubitem()
    {
        return $this->hasMany(Naturezasubitem::class, 'naturezasubitem_id');
    }

    public function empenhos()
    {
        return $this->hasMany(Empenho::class,'naturezadespesa_id');
    }


}
