<?php

namespace App\Models;

use App\Models\ContratoBase as Model;
use Backpack\CRUD\CrudTrait;
use Dompdf\Dompdf;
use Exception;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Spatie\Activitylog\Traits\LogsActivity;

class Contratoarquivo extends Model
{
    use CrudTrait;
    use LogsActivity;
    use SoftDeletes;

    protected static $logFillable = true;
    protected static $logName = 'contrato_arquivos';

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'contrato_arquivos';
    protected $fillable = [
        'contrato_id',
        'tipo',
        'processo',
        'sequencial_documento',
        'descricao',
        'arquivos',
        'origem',
        'link_sei',
        'envio_pncp_pendente',
        'link_pncp',
        'sequencial_pncp',
        'contratohistorico_id',
        'justificativa_exclusao',
        'retorno_pncp',
        'assinaturas_documento',
        'restrito',
        'user_id',
        'envio_v2',
        'contrato_opm_id',
        'rascunho'
    ];

    protected $casts = [
        'arquivos' => 'array'
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function getContrato()
    {
        return $this->getContratoNumero();
    }

    public function getTipo()
    {
        return $this->codigoItem()->first()->descricao;
    }

    /*
     * @Imprime a origem do anexo
     */
    public function getOrigemAnexo(){
        if ($this->origem == 0){
            $origem = 'Arquivo';
        } elseif ($this->origem == 1){
            $origem = 'SEI';
          } else {
            $origem = 'Minuta';
          }
        return  $origem;
    }

    public function formatarTextoRestricao() {
        $textoExibicao = 'Não';

        if($this->restrito) {
            $textoExibicao = 'Sim';
        }
        return $textoExibicao;
    }

    /*
     * @Imprime links da grid principal de anexos
     */
    public function getLinkAnexo(){

        if ($this->origem==0) {

            $retorno = $this->montarUrlDownload($this->arquivos, $this->descricao, $this->id);

        } elseif ($this->origem==1) {
           $retorno = "<a href='".$this->link_sei."' target='_blank'>".$this->descricao."</a>";

        } elseif ($this->origem==2) {
            $retorno = "<a href='".$this->link_sei."' target='_blank'>".$this->descricao."</a>";
        }

        return $retorno;
    }

    private function montarUrlDownload(?string $campoArquivo, ?string $texto, ?int $id) {
        if(filter_var($campoArquivo, FILTER_VALIDATE_URL)) {
            return '<a href="'.$campoArquivo.'" target="_blank">'.$texto.'</a>';
        } else {
            return '<a href="'. url("gescon/consulta/download-arquivo-contrato/{$id}") .'" target="_blank">'.$texto.'</a>';
        }
    }

    /**
     * Método temporário para o download do arquivo na grid, devido o ambiente de desenvolvimento não está funcionando o download.
     */
     public function getLinkAnexoTemporario() {
        return $this->montarUrlDownload($this->arquivos, $this->descricao, $this->id);
     }

    public function getListaArquivosComPath()
    {
        $arquivos_array = [];
        $i = 1;
        foreach ($this->arquivos as $arquivo) {

            $arquivos_array[] = [
                'arquivo_' . $i => env('APP_URL') . '/storage/' . $arquivo,
            ];
            $i++;
        }
        return $arquivos_array;
    }

    public function arquivoAPI()
    {
        return [
                'id' => $this->id,
                'contrato_id' => $this->contrato_id,
                'tipo' => $this->getTipo(),
                'processo' => $this->processo,
                'sequencial_documento' => $this->sequencial_documento,
                'descricao' => $this->descricao,
                'path_arquivo' => env('APP_URL') . '/storage/' . $this->arquivos, //Issue #137
                'origem' => ($this->arquivo=='0') ? 'Sistema' : 'SEI',
                'link_sei' => $this->link_sei,
        ];
    }

    public function serializaArquivoPNCP()
    {
        if(isset($this->link_sei)){
            return Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix()."sei_temp_pncp/".$this->contrato->unidade->orgao->codigo. '/' . $this->contrato_id. '/' .$this->id.'.pdf';
        }else{
            $filepath = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
            return $filepath . "/" . ((is_array($this->arquivos)) ? $this->arquivos[0] : $this->arquivos);
        }
    }

    public function serializaJustificativaMinutaDocumetoPNCP()
    {
        return [
            'justificativa' => 'Inserção automática pelo Padrão de Minuta',
        ];
    }

    public function serializaExclusaoPNCP()
    {
        return [
            'justificativa' => isset($this->justificativa_exclusao)? $this->justificativa_exclusao : ' ',
        ];
    }

    private function parametrizarURLSEI($link) {
        $ch = curl_init($link);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_ENCODING ,'');

        return $ch;
    }

    private function recuperarConteudoDocumentoSEI(string $link) {
        $ch = $this->parametrizarURLSEI($link);
        $informacoesUrlSEI = array();

        $informacoesUrlSEI['arquivo'] = curl_exec($ch);
        $informacoesUrlSEI['ch'] = $ch;

        return $informacoesUrlSEI;
    }

    private function montarArquivoSEI(string $link) {

        $informacoesUrlSEI = $this->recuperarConteudoDocumentoSEI($link);
        if(strpos($informacoesUrlSEI['arquivo'], "301 Moved Permanently") !== false){
            $link = str_replace("http","https",$link);
            $this->link_sei = $link;
            return $this->recuperarConteudoDocumentoSEI($link);
        }

        return $informacoesUrlSEI;

    }

    public function salvaTempDocumentoSEI(){
        try{
            $path = "sei_temp_pncp/".$this->contrato->unidade->orgao->codigo. '/' . $this->contrato_id.'/';
            $nomeArquivo = $this->id;

            $informacaoArquivo = $this->montarArquivoSEI($this->link_sei);

            $ext = config('api-pncp.tipos_sei')[curl_getinfo($informacaoArquivo['ch'], CURLINFO_CONTENT_TYPE)];

            curl_close($informacaoArquivo['ch']);

            if (!Storage::exists($path)) {
                Storage::makeDirectory($path);
            }

            Storage::put($path . '/' . $nomeArquivo . '.' . $ext, $informacaoArquivo['arquivo']);
            if($ext == "html"){
                $this->converteHTMLparaPDF($path, $nomeArquivo);

                $caminhoHtml = Storage::path($path . '/' . $nomeArquivo . '.html');

                if (file_exists($caminhoHtml)) {
                    unlink($caminhoHtml);
                }
                $ext = "pdf";
            };
            return;
        }catch(Exception $e){
            //Não colocar no Log erro de dados provindos de Web Services devido a vulnerabilidades de XSS
            Log::error("Erro ao salvar arquivo do SEI (Contrato: ".$this->contrato_id.", Arquivo: ".$this->id.", Link SEI: ".$this->link_sei.")");
            Log::error("Erro ao salvar arquivo do SEI". $e->getMessage());
            throw $e;
        }
    }

    public function mudarStatusRestricao()
    {
        if($this->restrito) {
            return "mudarstatusrestricao/$this->id/0";
        }

        return "mudarstatusrestricao/$this->id/1";

    }

    public function converteHTMLparaPDF($path, $nomeArquivo){
        try{

        //Implementação DOMPDF/Dompdf https://github.com/dompdf/dompdf
        $dompdf = new Dompdf();
        $dompdf->loadHtml(Storage::get($path.$nomeArquivo.'.html'), 'iso-8859-1');
        $dompdf->render();
        Storage::put($path.$nomeArquivo.'.pdf', $dompdf->output());
        }catch(Exception $e){
            dd($e->getMessage());
            //Não colocar no Log erro de dados provindos de Web Services devido a vulnerabilidades de XSS
            Log::error("Erro ao converter arquivo HTML do SEI (Contrato: ".$this->contrato_id.", Arquivo: ".$this->id.", Link SEI: ".$this->link_sei.")");
        }
    }

    public function excluiTempDocumentoSEI(){
        try{
            $path = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix()."sei_temp_pncp/".$this->contrato->unidade->orgao->codigo. '/' . $this->contrato_id. '/' .$this->id.'.pdf';
            if (file_exists($path))
                unlink($path);
        }catch(Exception $e){
            Log::error($e);
        }
    }

    public function buscaArquivosPorContratoId(int $contrato_id, $range)
    {
        $arquivos = $this::whereHas('contrato', function ($c) {
            $c->whereHas('unidade', function ($u) {
                $u->where('sigilo', "=", false);
            });
        })
            ->where('contrato_id', $contrato_id)
            ->when($range != null, function ($d) use ($range) {
                $d->whereBetween('contrato_arquivos.updated_at', [$range[0], $range[1]]);
            })
            ->get();

        return $arquivos;
    }

    public function buscaArquivos($range)
    {
        $arquivos = $this::whereHas('contrato', function ($c) {
            $c->whereHas('unidade', function ($u) {
                $u->where('sigilo', "=", false);
            });
        })
            ->when($range != null, function ($d) use ($range) {
                $d->whereBetween('contrato_arquivos.updated_at', [$range[0], $range[1]]);
            })
            ->get();

        return $arquivos;
    }

    public static function getTiposAceitosPncp() {
         return [
             12 => 'Contrato',
             13 => 'Termo de Rescisão',
             14 => 'Termo Aditivo',
             15 => 'Termo de Apostilamento',
             16 => 'Outros',
             17 => 'Nota de Empenho',
             18 => 'Relatório Final de Contrato',
         ];
    }

    public function isTipoAceitoPncp()
    {
        return in_array($this->getTipo, self::getTiposAceitosPncp());
    }

    public static function getTiposArquivoValidosSuperSEI()
    {
        return [
            'Contrato',
            'Relatório Final do Contrato',
            'Termo Aditivo',
            'Termo de Apostilamento',
            'Termo Apostilamento'
        ];
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function envia_dados_pncp()
    {
        return $this->morphToMany(EnviaDadosPncp::class, 'pncpable');
    }

    public function contrato()
    {
        return $this->belongsTo(Contrato::class, 'contrato_id');
    }

    public function codigoItem()
    {
        return $this->belongsTo(Codigoitem::class, 'tipo');
    }

    public function contrato_historico()
    {
        return $this->belongsTo(Contratohistorico::class, 'contratohistorico_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function saveWithoutEvents(array $options=[]){
        return static::withoutEvents(function() use ($options) {
            return $this->save($options);
        });
    }

    public function getViewButtonByUserId($crud)
    {
        if ($this->user_id == backpack_user()->id)
        {
            $crud->allowAccess('delete');

            if($this->restrito) {
                return '<a href="mudarstatusrestricao/'.$this->id.'/0" class="btn btn-xs btn-default" title="Alterar o status do documento para público"><i class="fa fa-unlock"></i></a>';
            }

            return '<a href="mudarstatusrestricao/'.$this->id.'/1" class="btn btn-xs btn-default" title="Alterar o status do documento para restrito"><i class="fa fa-lock"></i></a>';
        }

        $crud->denyAccess('delete');
        return null;
    }

    public function getTipoTermoPncp()
    {
        return ['Termo Rescisão', 'Termo Aditivo', 'Termo Apostilamento', 'Termo de Encerramento'];
    }

    /**
     * Obtém uma lista das situações pendentes de envio para o PNCP.
     *
     * @return array Lista de códigos de situação pendentes de envio para o PNCP.
     */
    public static function getSituacoesPendentesPncp()
    {
        return ['INCARQ', 'ATUARQ', 'DELARQ'];
    }

    /**
     * Verifica se uma situação específica está na lista de situações pendentes de envio para o PNCP.
     *
     * @param string $situacao Código da situação a ser verificada.
     * @return bool Retorna true se a situação estiver pendente de envio para o PNCP, caso contrário, retorna false.
     */
    public static function isSituacaoPendente(string $situacao){
        return in_array($situacao, self::getSituacoesPendentesPncp());
    }

}
