<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Spatie\Activitylog\Traits\LogsActivity;
use function foo\func;
use Illuminate\Support\Facades\DB;

class Contratoempenho extends Model
{
    use CrudTrait;
    use LogsActivity;
    protected static $logFillable = true;
    protected static $logName = 'contratoempenhos';

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'contratoempenhos';
    // protected $primaryKey = 'id';
    public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = [
        'contrato_id',
        'fornecedor_id',
        'empenho_id',
        'unidadeempenho_id',
        'user_id',
        'automatico',
    ];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function inserirContratoEmpenhoMigracaoConta(array $dados)
    {
        $this->fill($dados);
        $this->save();

        return $this;
    }


    public function buscaTodosEmpenhosContratosAtivos($range)
    {
        $empenhos = $this->whereHas('contrato', function ($c) {
            $c->whereHas('unidade', function ($s){
                $s->where('sigilo',false);
            })->where('situacao', true);
        })->whereHas('empenho', function ($x) use ($range){
            $x->when($range != null, function ($d) use ($range) {
                $d->whereBetween('empenhos.updated_at', [$range[0], $range[1]]);
            });
        })->get();

        return $empenhos;

    }

    public function getContrato()
    {
        if ($this->contrato_id) {
            $contrato = Contrato::find($this->contrato_id);
            return $contrato->numero;
        }

        return '';

    }

    public function getFornecedor()
    {
        $fornecedor = Fornecedor::find($this->fornecedor_id);
        return $fornecedor->cpf_cnpj_idgener . ' - ' . $fornecedor->nome;

    }

    public function getUnidadeEmpenho() //Unidade Emitente do Empenho
    {
        if ($this->unidadeempenho_id) {
            $unidade = Unidade::find($this->unidadeempenho_id);
            return @$unidade->codigosiafi . ' - ' . @$unidade->nomeresumido;
        }else{
            $unidade = Unidade::find(session()->get('user_ug_id'));
            return @$unidade->codigosiafi . ' - ' . @$unidade->nomeresumido;
        }

    }

    public function getUnidadeMinutaEmpenho() //Unidade da Minuta do Empenho
    {
        $empenho = MinutaEmpenho::select(['unidades.codigo', 'unidades.nomeresumido'])
        ->join('saldo_contabil', 'saldo_contabil.id', '=', 'minutaempenhos.saldo_contabil_id')
        ->join('empenhos', function ($join) {
            $join->on('empenhos.numero', '=', 'minutaempenhos.mensagem_siafi')
                ->on('empenhos.unidade_id', '=', 'saldo_contabil.unidade_id')
                ->on('empenhos.fornecedor_id', '=', 'minutaempenhos.fornecedor_empenho_id');
        })
        ->join('unidades', 'unidades.id', '=', 'minutaempenhos.unidade_id')
        ->where('empenhos.id', '=', $this->empenho_id)
        ->get()
        ->first();

        if ($empenho) {
            return $empenho->codigo . ' - ' . $empenho->nomeresumido;
        }

        return '';
    }

    public function getFornecedorEmpenho()
    {

        $empenho = Empenho::find($this->empenho_id);

        if ($empenho->fornecedor_id) {
            $fornecedor = $empenho->fornecedor()->first();
            return $fornecedor->cpf_cnpj_idgener . ' - ' . $fornecedor->nome;
        }

        return '';

    }

    public function getFornecedorContrato()
    {

        $contrato = Contrato::find($this->contrato_id);

        if ($contrato->fornecedor_id) {
            $fornecedor = $contrato->fornecedor()->first();
            return $fornecedor->cpf_cnpj_idgener . ' - ' . $fornecedor->nome;
        }

        return '';

    }

    public function getEmpenho()
    {
        $empenho = Empenho::find($this->empenho_id);
        return $empenho->numero;
    }

    public function getEmpenhoOuMinutaEmpenho()
    {
        if($this->tipo === 'minutaempenho') {
            $empenho = MinutaEmpenho::find($this->empenho_id);

            if($empenho) {
                return $empenho->mensagem_siafi;
            }
        } else {//MiniutaEmpenho
            $empenho = Empenho::find($this->empenho_id);
            return $empenho->numero;
        }

        return '';
    }

    /**
     * Retrona o código e a descrição do Plano Interno
     *
     * @return string
     */
    public function getPi()
    {
        $empenho = Empenho::find($this->empenho_id);
        if ($empenho->planointerno_id) {
            $planointerno = $empenho->planointerno()->first();
            return $planointerno->codigo . ' - ' . $planointerno->descricao;
        }

        return '-';

    }

    public function getNatureza()
    {
        $empenho = Empenho::find($this->empenho_id);

        if ($empenho->naturezadespesa_id) {

            $naturezadespesa = $empenho->naturezadespesa()->first();
            return $naturezadespesa->codigo . ' - ' . $naturezadespesa->descricao;

        }

        return '';

    }

    /**
     * Retorna a Data de Início da Vigência
     *
     * @return string
     */
    public function getVigenciaInicio()
    {
        return $this->retornaDataAPartirDeCampo($this->contrato->vigencia_inicio);
    }

    /**
     * Retorna a Data de Término da Vigência
     *
     * @return string
     */
    public function getVigenciaFim()
    {
        return $this->retornaDataAPartirDeCampo($this->contrato->vigencia_fim);
    }

    /**
     * Retorna o valor global, formatado como moeda em pt-Br
     *
     * @return string
     */
    public function getValorGlobal()
    {
        return $this->retornaCampoFormatadoComoNumero($this->contrato->valor_global);
    }

    /**
     * Retorna o valor da parcela, formatado como moeda em pt-Br
     *
     * @return string
     */
    public function getValorParcela()
    {
        return $this->retornaCampoFormatadoComoNumero($this->contrato->valor_parcela);
    }

    public function formatVlrEmpenhado()
    {
        if ($this->empenho_id) {
            $empenho = Empenhos::find($this->empenho_id);
            return 'R$ ' . number_format($empenho->empenhado, 2, ',', '.');
        }

        return '';
    }

    public function formatVlraLiquidar()
    {
        if ($this->empenho_id) {
            $empenho = Empenhos::find($this->empenho_id);
            return 'R$ ' . number_format($empenho->aliquidar, 2, ',', '.');
        }

        return '';
    }

    public function formatVlrLiquidado()
    {
        if ($this->empenho_id) {
            $empenho = Empenhos::find($this->empenho_id);
            return 'R$ ' . number_format($empenho->liquidado, 2, ',', '.');
        }

        return '';

    }

    public function formatVlrPago()
    {
        if ($this->empenho_id) {
            $empenho = Empenhos::find($this->empenho_id);
            return 'R$ ' . number_format($empenho->pago, 2, ',', '.');
        }

        return '';

    }

    public function formatVlrRpInscrito()
    {
        if ($this->empenho_id) {
            $empenho = Empenhos::find($this->empenho_id);
            return 'R$ ' . number_format($empenho->rpinscrito, 2, ',', '.');
        }

        return '';

    }

    public function formatVlrRpaLiquidar()
    {
        if ($this->empenho_id) {
            $empenho = Empenhos::find($this->empenho_id);
            return 'R$ ' . number_format($empenho->rpaliquidar, 2, ',', '.');
        }

        return '';

    }

    public function formatVlrRpLiquidado()
    {
        if ($this->empenho_id) {
            $empenho = Empenhos::find($this->empenho_id);
            return 'R$ ' . number_format($empenho->rpliquidado, 2, ',', '.');
        }

        return '';

    }

    public function formatVlrRpPago()
    {
        if ($this->empenho_id) {
            $empenho = Empenhos::find($this->empenho_id);
            return 'R$ ' . number_format($empenho->rppago, 2, ',', '.');
        }

        return '';

    }

    /**
     * Retorna uma coleção de Empenhos com a identificação única (UG + GESTAO + NUMERO) e o respectivo contrato (id) vinculado.
     *
     * @param int $ano Ano de emissão do empenho.
     * @return Collection<Contratoempenho>
     */
    public static function contratosEmpenhosPorAno(int $ano)
    {
        return ContratoEmpenho::select(DB::raw('(u.codigo || u.gestao || e.numero) AS id_documento_empenho'), 'contratoempenhos.contrato_id')
                                ->join('empenhos as e', 'e.id', '=', 'contratoempenhos.empenho_id')
                                ->join('unidades as u', 'u.id', '=', 'e.unidade_id')
                                ->whereRaw("LEFT(e.numero, 4)::integer = ?", [$ano])
                                ->get();
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function contrato()
    {
        return $this->belongsTo(Contrato::class, 'contrato_id');
    }

    public function empenho()
    {
        return $this->belongsTo(Empenho::class, 'empenho_id');
    }

    public function fornecedor()
    {
        return $this->belongsTo(Fornecedor::class, 'fornecedor_id');
    }

    public function unidadeEmpenhos()
    {
        return $this->belongsTo(Unidade::class, 'unidadeempenho_id');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    /**
     * Retorna $campo data formatado no padrão pt-Br: dd/mm/yyyy
     *
     * @param $campo
     * @return string
     */
    private function retornaDataAPartirDeCampo($campo)
    {
        try {
            if(strtotime(($campo)) === false)
                throw new \Exception();

            $data = \DateTime::createFromFormat('Y-m-d', $campo);
            $retorno = $data->format('d/m/Y');
        } catch (\Exception $e) {
            $retorno = '';
        }

        return $retorno;
    }

    /**
     * Retorna $campo numérico formatado no padrão pt-Br: 0.000,00
     *
     * @param $campo
     * @return string
     */
    private function retornaCampoFormatadoComoNumero($campo)
    {
        try {
            $retorno = number_format($campo, 2, ',', '.');
        } catch (\Exception $e) {
            $retorno = '';
        }

        return $retorno;
    }
}
