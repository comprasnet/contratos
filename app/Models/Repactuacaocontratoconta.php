<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Backpack\CRUD\CrudTrait;

class Repactuacaocontratoconta extends Model
{
    use CrudTrait;
    use LogsActivity;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'repactuacoes';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = [
        'contratoconta_id',
        'funcao_id',
        'salario_novo',
        'mes_inicio',
        'ano_inicio',
        'mes_fim',
        'ano_fim',
        'jornada',
        'salarios_atuais',
        'descricoes_complementares'
    ];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function getArrayJornadasByContratocontaidByFuncaoid($idContrato, $funcao_id){
        $array = \DB::table('contratocontas as cc')
            ->select('ct.jornada')
            ->distinct()
            ->join('contratoterceirizados as ct', 'ct.contrato_id', 'cc.contrato_id')
            ->where('cc.id', $idContrato)
            ->where('ct.funcao_id', $funcao_id)
            ->pluck('ct.jornada', 'ct.jornada')
            ->toArray();
            return $array;
    }
    // public function descricao_complementar(){
    //     return true;
    // }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
