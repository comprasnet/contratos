<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class SfOrcAmparoLegalDados extends Model
{
    use CrudTrait;
    use LogsActivity;

//    use SoftDeletes;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected static $logFillable = true;
    protected static $logName = 'sforcamparolegaldados';

    protected $table = 'sforcamparolegaldados';

    protected $guarded = [
        'id'
    ];

    protected $fillable = [
        'minutaempenho_id',
        'ugemitente',
        'anoempenho',
        'tipoempenho',
        'numempenho',
        'dtemis',
        'txtprocesso',
        'vlrtaxacambio',
        'vlrempenho',
        'codfavorecido',
        'codamparolegal',
        'txtinfocompl',
        'codtipotransf',
        'txtlocalentrega',
        'txtdescricao',
        'numro',
        'mensagemretorno',
        'situacao',
        'alteracao',
        'sfnonce_id'
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function amparoLegal()
    {
        return $this->belongsTo(AmparoLegal::class, 'amparo_legal_id');
    }

    public function sfAmparoLegalRestricao()
    {
        return $this->hasMany(SfRestricaoAmparoLegal::class, 'sforcamparolegaldados_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
