<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Http\Traits\NumeroAnoTrait;

class Compra extends Model
{
    use CrudTrait;
    use LogsActivity;
    use SoftDeletes;
    use NumeroAnoTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected static $logFillable = true;
    protected static $logName = 'compras';

    protected $table = 'compras';
    protected $guarded = [
        'id'
    ];

    protected $fillable = [
        'unidade_origem_id',
        'unidade_subrrogada_id',
        'modalidade_id',
        'tipo_compra_id',
        'numero_ano',
        'inciso',
        'lei',
        'uasg_beneficiaria_id',
        'artigo',
        'id_unico',
        'cnpjOrgao',
        'origem',
        'numero_contratacao'
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function gravaCompra($params)
    {

        $this->unidade_origem_id = $params['unidade_origem_id'];
        $this->unidade_subrrogada_id = $params['unidade_subrrogada_id'];
        $this->modalidade_id = $params['modalidade_id'];
        $this->tipo_compra_id = $params['tipo_compra_id'];
        $this->numero_ano = $params['numero_ano'];
        $this->inciso = $params['inciso'];
        $this->lei = $params['lei'];
        $this->id_unico = $params['id_unico'];
        $this->cnpjOrgao = $params['cnpjOrgao'];
        $this->save($params);

        return $this->id;
    }

    public function getUnidadeOrigem(): string
    {
        return $this->unidade_origem->codigo . ' - ' . $this->unidade_origem->nomeresumido;
    }

    public function getUnidadeSubrrogada(): string
    {
        if (!$this->unidade_subrrogada) {
            return '';
        }

        return $this->unidade_subrrogada->codigosiasg . ' - ' . $this->unidade_subrrogada->nomeresumido;
    }

    public function getUnidadeBeneficiaria(): string
    {
        if (!$this->unidade_beneficiaria) {
            return '';
        }

        return $this->unidade_beneficiaria->codigosiasg . ' - ' . $this->unidade_beneficiaria->nomeresumido;
    }

    public function getModalidade()
    {
        return $this->modalidade->descres . ' - ' . $this->modalidade->descricao;
    }

    public function verificaPermissaoSisrp($unidade_id, $contrato = null): bool
    {
        //recupera a Gerenciadora, os Participantes e os Caronas
        $unidadesIntegrantes = $this
            ->compra_item()
            ->join('compra_item_unidade as ciu', 'compra_items.id', '=', 'ciu.compra_item_id')
            ->groupBy('ciu.unidade_id')
            ->pluck('ciu.unidade_id')
            ->toArray();

        if($contrato){
            $unidade_id = $contrato->unidadeorigem_id;
        }

        return in_array($unidade_id, $unidadesIntegrantes);
    }

    public function getItensMaterialSemCatmatpdm()
    {
        return $this->join('compra_items as ci', 'ci.compra_id', '=', 'compras.id')
            ->join('catmatseritens as csri', 'ci.catmatseritem_id', '=', 'csri.id')
            ->leftJoin('catmatpdms as catpdm', 'csri.catmatpdm_id', '=', 'catpdm.id')
            ->where('compra_id', $this->id)
            ->where('tipo_item_id', 149)
            ->whereNull('csri.catmatpdm_id')
            ->select('ci.*', 'csri.codigo_siasg')
            ->get();
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function compra_item()
    {
        return $this->hasMany(CompraItem::class);
    }

    public function minuta_empenhos()
    {
        return $this->hasMany(MinutaEmpenho::class);
    }

    public function modalidade()
    {
        return $this->belongsTo(Codigoitem::class, 'modalidade_id');
    }

    public function tipo_compra()
    {
        return $this->belongsTo(Codigoitem::class, 'tipo_compra_id');
    }

    public function unidade_origem()
    {
        return $this->belongsTo(Unidade::class, 'unidade_origem_id');
    }

    public function unidade_subrrogada()
    {
        return $this->belongsTo(Unidade::class, 'unidade_subrrogada_id');
    }

    public function unidade_beneficiaria()
    {
        return $this->belongsTo(Unidade::class, 'uasg_beneficiaria_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    public function getTipoCompraAttribute()
    {
        return $this->tipo_compra()->first()->descricao;
    }
    public function getParametrosGerenciadoraAttribute(): array
    {
        $uasg = $this->unidade_origem->codigo;
        return [
            'modalidade'       => $this->modalidade->descres,
            'numeroAno'        => str_replace("/", "", $this->numero_ano),
            'uasgCompra'       => $uasg,
            'uasgUsuario'      => $uasg
        ];
    }

    public function getTipoCompraDescAttribute()
    {
        return $this->tipo_compra;
    }

    public function getCodigoUnidadeBeneficiariaAttribute(): ?string
    {
        if (!$this->unidade_beneficiaria) {
            return null;
        }

        return $this->unidade_beneficiaria->codigosiasg;
    }

    public function getCompraOrigemDescAttribute(): ?string
    {
        if ($this->origem == 1) {
            return 'NDC';
        }
        return 'SIASG';
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
