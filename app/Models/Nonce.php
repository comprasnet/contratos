<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Nonce extends Model
{
    /**
     * @var string
     */
    protected $table = 'nonce';

    /**
     * @var string[]
     */
    protected $fillable = [
        'user_id',
        'nonce',
        'ip',
        'status',
        'funcionalidade',
        'json_request',
        'json_response'
    ];

    /**
     * Os atributos que devem ser convertidos para tipos nativos.
     *
     * @var array
     */
    protected $casts = [
        'json_request' => 'array',
        'json_response' => 'array',
        'request' => 'array',
        'response' => 'array',
    ];

    /**
     * @return MorphTo
     */
    public function nonceable()
    {
        return $this->morphTo();
    }
}
