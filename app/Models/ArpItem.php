<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArpItem extends Model
{
    //
    public $primaryKey = 'id';
    protected $table = 'arp_item';

    public function arp()
    {
        return $this->belongsTo(Arp::class, 'arp_id');
    }

}
