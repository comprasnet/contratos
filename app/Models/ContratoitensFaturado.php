<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class ContratoitensFaturado extends Model
{
    use CrudTrait;
    use LogsActivity;
    use SoftDeletes;

    protected static $logFillable = true;
    protected static $logName = 'contratoitens_faturados';

    protected $fillable = [
        'contratoitens_id',
        'quantidade_faturado',
        'valorunitario_faturado',
        'valortotal_faturado',
    ];

    /*
   |--------------------------------------------------------------------------
   | RELATIONS
   |--------------------------------------------------------------------------
   */

    public function contratoItens()
    {
        return $this->belongsTo(Contratoitem::class, 'contratoitens_id');
    }
}
