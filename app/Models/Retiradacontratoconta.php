<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Spatie\Activitylog\Traits\LogsActivity;

class Retiradacontratoconta extends Model
{
    use CrudTrait;
    use LogsActivity;
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'movimentacaocontratocontas';
    protected $fillable = [
        'contratoconta_id',
        'tipo_id',
        'mes_competencia',
        'ano_competencia',
        'valor_total_mes_ano',
        'situacao_movimentacao',
        'user_id'
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getTipoMovimentacao(){
        $objCodigoItem = Codigoitem::find($this->tipo_id);
        return $descricao= $objCodigoItem->descricao;
    }

    public function formatValor(){
        return number_format($this->valor, 2, ',', '.');
    }

    public function getContratosTerceirizadosParaCombo($contrato_id){
        return $arrayContratosTerceirizados = Contratoterceirizado::where('contrato_id','=',$contrato_id)->pluck('nome', 'id')->toArray();
    }
    
    public function getEncargosParaCombo(){
        // Os dados da combo serão fixos - grupo A não entra.
        return $arrayObjetosEncargoParaCombo = array(
            'Férias' => 'Férias',
            'Décimo Terceiro' => 'Décimo Terceiro',
            'Férias e Décimo Terceiro' => 'Férias e Décimo Terceiro',
            'Demissão' => 'Rescisão',
            'Realocado' => 'Realocação'
        );
    }

    public function getIdContratoContaByIdContratoTerceirizado($idContratoTerceirizado){
        $objContratoConta = new Contratoconta();
        return $idContratoConta = $objContratoConta->getIdContratoContaByIdContratoTerceirizado($idContratoTerceirizado);
    }

    public function criarMovimentacao($request){
        $objMovimentacaocontratoconta = new Movimentacaocontratoconta();
        if($id = $objMovimentacaocontratoconta->criarMovimentacao($request)){
            return $id;
        }
        return false;
    }

    // verificar se no ano/mês de competência, o funcionário já tinha iniciado.
    public function verificarSeCompetenciaECompativelComDataInicio($request, $objContratoTerceirizado){
        $mesCompetencia = $request->input('mes_competencia');
        $anoCompetencia = $request->input('ano_competencia');
        $dataInicio = $objContratoTerceirizado->data_inicio;
        $dataFim = $objContratoTerceirizado->data_fim;
        $mesDataInicio = substr($dataInicio, 5, 2);
        $anoDataInicio = substr($dataInicio, 0, 4);
        $diaDataInicio = substr($dataInicio, 8, 2);
        $mesDataFim = substr($dataFim, 5, 2);
        $anoDataFim = substr($dataFim, 0, 4);
        $diaDataFim = substr($dataFim, 8, 2);
        if( $anoCompetencia < $anoDataInicio ){
            return false;
        }
        if( $anoCompetencia == $anoDataInicio  && $mesCompetencia < $mesDataInicio){
            return false;
        }
        return true;
    }
   
    public function excluirMovimentacao($idMovimentacao){
        $objMovimentacaocontratoconta = new Movimentacaocontratoconta();
        if($id = $objMovimentacaocontratoconta->excluirMovimentacao($idMovimentacao)){
            return true;
        }
        return false;
    }

    public function alterarStatusMovimentacao($idMovimentacao, $statusMovimentacao){
        $objMovimentacao = new Movimentacaocontratoconta();
        if($objMovimentacao->alterarStatusMovimentacao($idMovimentacao, $statusMovimentacao)){
            return true;
        }
        return false;
    }
}
