<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AmparoLegalRestricaoCodigo extends Model
{
   
  
   

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */


    protected $table = 'amparo_legal_restricao_codigos';


    

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
   

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function amparoLegalRestricao()
    {
        return $this->belongsTo(AmparoLegalRestricao::class, 'amparo_legal_restricao_id');
    }
}
