<?php

namespace App\Models;

use App\Http\Traits\BuscaCodigoItens;
use Illuminate\Database\Eloquent\Model;

class FornecedorSicaf extends Model
{
    use BuscaCodigoItens;

    protected $table = 'fornecedorsicaf';

    public $timestamps = true;

    protected $fillable = [
        'fornecedor_id',
        'date_receita_federal_pgfn',
        'validade_receita_federal_pgfn',
        'date_certidao_fgts',
        'validade_certidao_fgts',
        'date_certidao_trabalhista',
        'validade_certidao_trabalhista',
        'date_receita_estadual_distrital',
        'validate_receita_estadual_distrital',
        'date_receita_municipal',
        'validade_receita_municipal',
        'mei',
        'porte',
        'nome_fantasia',
        'natureza_juridica',
        'data_vencimento_credenciamento',
        'situacao_fornecedor',
        'origem_sistema'
    ];

    public function fornecedor()
    {
        return $this->belongsTo(Fornecedor::class, 'fornecedor_id');
    }

    public function getPorteAttribute($value)
    {
        if($value === null){
            return '';
        }

        $descricao = $this->retornaDescricaoCodigoItemPorId($value);

        return mb_convert_case($descricao, MB_CASE_TITLE, 'UTF-8');
    }

    public function getOrigemSistemaAttribute($value)
    {
        if($value === null){
            return '';
        }

        $descricao = $this->retornaDescricaoCodigoItemPorId($value);

        return mb_convert_case($descricao, MB_CASE_UPPER, 'UTF-8');
    }

    public function getMeiAttribute($value)
    {
        if($value === null){
            return '';
        }

        switch ($value){
            case true:
                return 'Sim';
            case false:
                return 'Não';
            default:
                return 'Não Especificado';
        }
    }

}
