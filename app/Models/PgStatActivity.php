<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PgStatActivity extends Model
{
    protected $table = 'pg_stat_activity';
}
