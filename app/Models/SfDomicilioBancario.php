<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class SfDomicilioBancario extends Model
{
    use LogsActivity;
    use CrudTrait;

    /**
     * Informa que não utilizará os campos create_at e update_at do Laravel
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Nome da tabela
     *
     * @var string
     */
    protected $table = 'sfdomiciliobancario';

    /**
     * Campos da tabela
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'sfpredoc_id',
        'banco',
        'agencia',
        'conta',
        'tipo'
    ];

    public function sfpredoc()
    {
        return $this->belongsTo(SfPredoc::class, 'sfpredoc_id');
    }


    /**
     * Retorna o numero do contrato.
     *
     */
    public function getContrato()
    {
        $contrato = $this->join('sfpredoc', 'sfdomiciliobancario.sfpredoc_id', '=', 'sfpredoc.id')
            ->join('sfdadospgto', 'sfpredoc.sfdadospgto_id', '=', 'sfdadospgto.id')
            ->join('sfpadrao', 'sfdadospgto.sfpadrao_id', '=', 'sfpadrao.id')
            ->join('apropriacoes_faturas_contratofaturas as aprf', 'sfpadrao.id', '=', 'aprf.sfpadrao_id')
            ->join('contratofaturas', 'aprf.contratofaturas_id', '=', 'contratofaturas.id')
            ->join('contratos', 'contratofaturas.contrato_id', '=', 'contratos.id')
            ->where('sfdomiciliobancario.id', $this->id)
            ->select('contratos.numero')
            ->get(); 

        return $contrato->isNotEmpty() ? $contrato->first()->numero : null;
    }
}
