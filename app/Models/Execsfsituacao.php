<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Execsfsituacao extends Model
{
    use CrudTrait;
    use LogsActivity;
    use SoftDeletes;

    protected static $logFillable = true;
    protected static $logName = 'execsfsituacao';

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'execsfsituacao';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = [
        'codigo',
        'descricao',
        'execsfsituacao_id',
        'aba',
        'status',
        'categoria_ddp',
        'afeta_custo',
        'tipo_situacao',
        'permite_contrato',
        'darf',
        'darf_numerado',
        'darf_numerado_decomposto'
    ];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function camposVariaveis($crud = false)
    {
        $button = '<div class="btn-group">
                        <button type="button" title="Mais" class="btn btn-xs btn-default dropdown-toggle dropdown-toggle-split"
                            data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false"><i class="fa fa-gears"></i>
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                            <ul class="dropdown-menu" >
                                <li><a href="/admin/situacaosiafi/' . $this->id . '/camposvariaveis">Campos</a></li>
                            </ul>
                    </div>';

        return $button;
    }

    public function getExecsfsituacao()
    {
        if ($this->execsfsituacao_id) {
            $execsfsituacao = Execsfsituacao::find($this->execsfsituacao_id);
            return $execsfsituacao->codigo . ' - ' . $execsfsituacao->descricao;
        } else {
            return '-';
        }
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function tipoSituacao()
    {
        return $this->belongsTo(Codigoitem::class, 'tipo_situacao');
    }

    public function tipodocs()
    {
        return $this->belongsToMany(Codigoitem::class, 'execsfsituacao_tipodoc', 'execsfsituacao_id', 'tipodoc_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
