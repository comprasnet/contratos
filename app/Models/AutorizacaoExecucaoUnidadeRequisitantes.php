<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AutorizacaoExecucaoUnidadeRequisitantes extends Model
{
    
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'autorizacaoexecucao_unidade_requisitantes';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function unidadeRequisitante()
    {
        return $this->belongsTo(Unidade::class, 'unidade_requisitante_id');
    }    

}
