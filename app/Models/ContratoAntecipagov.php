<?php

namespace App\Models;

use App\Models\AntecipaGov;
use App\Models\Contrato;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ContratoAntecipagov extends Model
{
    use LogsActivity;

    protected $table = 'contrato_antecipagov';

    public $timestamps = false;

    protected $fillable = [
        'contrato_id',
        'antecipagov_id'
    ];

    public function contratos()
    {
        return $this->belongsTo(Contrato::class, 'contrato_id');
    }

    public function antecipagov()
    {
        return $this->belongsTo(AntecipaGov::class, 'antecipagov_id');
    }
}
