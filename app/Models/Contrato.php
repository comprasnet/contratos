<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Models\Codigoitem;

// use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Traits\NumeroAnoTrait;


use function foo\func;

class Contrato extends Model
{
    use CrudTrait;
    use LogsActivity;

    // use SoftDeletes;
    use NumeroAnoTrait;

    protected static $logFillable = true;
    protected static $logName = 'contrato';

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'contratos';
    protected $fillable = [
        'numero',
        'fornecedor_id',
        'unidade_id',
        'unidadeorigem_id',
        'tipo_id',
        'subtipo',
        'categoria_id',
        'subcategoria_id',
        'processo',
        'objeto',
        'info_complementar',
        'receita_despesa',
        'fundamento_legal',
        'modalidade_id',
        'licitacao_numero',
        'data_assinatura',
        'data_publicacao',
        'data_proposta_comercial',
        'vigencia_inicio',
        'vigencia_fim',
        'valor_inicial',
        'valor_global',
        'num_parcelas',
        'valor_parcela',
        'valor_acumulado',
        'situacao_siasg',
        'situacao',
        'unidades_requisitantes',
        'unidadecompra_id',
        'numero_compra',
        'publicado',
        'prorrogavel',
        'unidadebeneficiaria_id',
        'is_prazo_indefinido',
        'justificativa_contrato_inativo_id',

        'data_encerramento',
        'is_objeto_contratual_entregue',
        'is_cumpridas_obrigacoes_financeiras',
        'is_saldo_conta_vinculada_liberado',
        'is_garantia_contratual_devolvida',

        'hora_encerramento',
        'nome_responsavel_encerramento',
        'cpf_responsavel_encerramento',
        'user_id_responsavel_encerramento',
        'consecucao_objetivos_contratacao',

        'user_id_responsavel_signatario_encerramento',
        'nome_responsavel_signatario_encerramento',
        'cpf_responsavel_signatario_encerramento',

        'te_valor_total_executado',
        'te_valor_total_pago',
        'te_saldo_disponivel_ou_bloqueado',
        'te_justificativa_nao_cumprimento',
        'te_saldo_bloqueado_garantia_contratual',
        'te_justificativa_bloqueio_garantia_contratual',
        'te_saldo_bloqueado_conta_deposito',
        'te_justificativa_bloqueio_conta_deposito',

        'cronograma_automatico',

        'grau_satisfacao_desempenho_contrato',
        'planejamento_contratacao_atendida',
        'sugestao_licao_aprendida',
        'codigo_sistema_externo',
        'elaboracao',
        'aplicavel_decreto',
        'contrata_mais_brasil',
        'numero_contratacao',
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function verificarSePossuiDomicilioBancario()
    {
        return $this->antecipagovs()->count() > 0 ? true : false;
    }

    public function verificarSeExisteDomicilioBancario()
    {
        return $this->antecipagovs()->exists();
    }

    public function getInstrumentoInicial(){
        $idContrato = $this->id;
        if ($idContrato) {
            return $objContratoHistorico = Contratohistorico::where('contrato_id', $idContrato)
                ->join('codigoitens as ci', 'ci.id', 'contratohistorico.tipo_id')
                ->join('codigos as c', 'c.id', 'ci.codigo_id')
                ->whereNotIn('ci.descricao', config('app.tipo_contrato_not_in'))
                ->where('c.descricao', 'Tipo de Contrato')
                ->first();
        } else {
            return false;
        }
    }

    // método que retorna as justificativas que estão salvas em codigoitens
    public static function getArrayComJustificativas()
    {
        $array = Codigoitem::whereHas('codigo', function ($query) {
            $query->where('descricao', '=', 'Justificativa Contrato Inativo');
        })
            ->orderBy('descricao')
            ->pluck('descricao', 'id')
            ->toArray();
        return $array;
    }

    public function getSituacao()
    {
        if ($this->situacao) {
            return 'Ativo';
        }
        // aqui a situação do contrato é inativo. vamos buscar a justificativa em codigo itens
        $objJustificativa = Codigoitem::where('id', $this->justificativa_contrato_inativo_id)->first();
        if (is_object($objJustificativa)) {
            $descricao = $objJustificativa->descricao;
            return $descricao;
        }
        return 'Inativo';
    }

    public function inserirContratoMigracaoConta(array $dados)
    {
        $this->fill($dados);
        $this->save();

        return $this;
    }

    // Alterado para contemplar Autoridades Signatárias
    public function buscaListaContratosUg($filtro)
    {
        $unidade_user = Unidade::find(session()->get('user_ug_id'));
        $sql = '    select                                                                                                                  ';
        $sql .= "           CONCAT(O.codigo, ' - ', O.nome) as orgao,                                                                       ";
        $sql .= "           CONCAT(U.codigo, ' - ', U.nomeresumido) as unidade,                                                             ";
        $sql .= '           CASE                                                                                                            ';
        $sql .= "               WHEN    contratos.receita_despesa = 'D'                                                                     ";
        $sql .= "               THEN    'Despesa'                                                                                           ";
        $sql .= "               ELSE    'Receita'                                                                                           ";
        $sql .= '           END as receita_despesa,                                                                                         ';
        $sql .= '           contratos.unidades_requisitantes, contratos.numero,                                                             ';
        $sql .= '           F.cpf_cnpj_idgener as fornecedor_codigo, F.nome as fornecedor_nome,                                             ';
        $sql .= '           T.descricao as tipo,                                                                                            ';
        $sql .= '           C.descricao as categoria,                                                                                       ';
        $sql .= '           S.descricao as subcategoria,                                                                                    ';
        $sql .= '           contratos.processo, contratos.objeto, contratos.info_complementar,                                              ';
        $sql .= '           M.descricao as modalidade,                                                                                      ';
        $sql .= '           contratos.licitacao_numero, contratos.data_assinatura, contratos.data_publicacao,                               ';
        $sql .= '           contratos.vigencia_inicio, contratos.vigencia_fim, contratos.valor_inicial,                                     ';
        $sql .= '           contratos.valor_global, contratos.num_parcelas, contratos.valor_parcela, contratos.valor_acumulado,             ';
        $sql .= '           contratos.data_proposta_comercial,                                                                              ';
        $sql .= '           CASE                                                                                                            ';
        $sql .= "               WHEN    contratos.situacao = 't'                                                                            ";
        $sql .= "               THEN    'Ativo'                                                                                             ";
        $sql .= "               ELSE    'Inativo'                                                                                           ";
        $sql .= '           END as situacao,                                                                                                ';
        $sql .= '           contratos.unidades_requisitantes,                                                                               ';
        $sql .= '           CASE                                                                                                            ';
        $sql .= "               WHEN    contratos.prorrogavel = 't'     THEN    'Sim'                                                       ";
        $sql .= "               WHEN    contratos.prorrogavel = 'f'     THEN    'Não'                                                       ";
        $sql .= '           END as prorrogavel,                                                                                             ';
        $sql .= '           (                                                                                                               ';
        $sql .= '                   select   aut3.autoridade_signataria                                                                     ';
        $sql .= '                   from     autoridadesignataria aut3                                                                      ';
        $sql .= '                   where    aut3.ativo = true                                                                              ';
        $sql .= '                   and      aut3.id in (                                                                                   ';
        $sql .= '                               select  cas.autoridadesignataria_id                                                         ';
        $sql .= '                               from    contrato_autoridade_signataria cas                                                  ';
        $sql .= '                               where   cas.contratohistorico_id = H.id                                                     ';
        $sql .= '                   )                                                                                                       ';
        $sql .= '           ) as autoridade_signataria,                                                                                     ';
        $sql .= '           (                                                                                                               ';
        $sql .= '                   select                                                                                                  ';
        $sql .= '                       CASE                                                                                                ';
        $sql .= '                           WHEN    aut4.titular = true                                                                     ';
        $sql .= "                           THEN    'Titular'                                                                               ";
        $sql .= "                           ELSE    'Substituta'                                                                            ";
        $sql .= "                        END as qualificacao_da_autoridade                                                                  ";
        $sql .= '                   from     autoridadesignataria aut4                                                                      ';
        $sql .= '                   where    aut4.ativo = true                                                                              ';
        $sql .= '                   and      aut4.id in (                                                                                   ';
        $sql .= '                               select  cas.autoridadesignataria_id                                                         ';
        $sql .= '                               from    contrato_autoridade_signataria cas                                                  ';
        $sql .= '                               where   cas.contratohistorico_id = H.id                                                     ';
        $sql .= '                   )                                                                                                       ';
        $sql .= '           ) as qualificacao_da_autoridade                                                                                 ';
        $sql .= '   from    contratos                                                                                                       ';
        $sql .= '   left    join orgaosubcategorias as S on S.id = contratos.subcategoria_id                                                ';
        $sql .= '   left    join codigoitens as M on M.id = contratos.modalidade_id                                                         ';
        $sql .= '   left    join codigoitens as C on C.id = contratos.categoria_id                                                          ';
        $sql .= '   left    join codigoitens as T on T.id = contratos.tipo_id                                                               ';
        $sql .= '   left    join fornecedores as F on F.id = contratos.fornecedor_id                                                        ';
        $sql .= '   left    join unidades as U on U.id = contratos.unidade_id                                                               ';
        $sql .= '   left    join orgaos as O on O.id = U.orgao_id                                                                           ';
        $sql .= '   left    join contratohistorico as H on H.contrato_id = contratos.id and H.tipo_id = (                                   ';
        $sql .= '           select  id from codigoitens where descricao =                                                                   ';
        $sql .= "           'Contrato'                                                                                                      ";
        $sql .= '           and     codigo_id = ( select id from codigos where descricao =                                                  ';
        $sql .= "           'Tipo de Contrato'";
        $sql .= '    ) )                                                                                                                    ';
        $sql .= "   where U.id = $unidade_user->id                                                                                          ";
        $sql .= "        and contratos.elaboracao = false                                                                                   ";
        $sql .= '   order   by contratos.numero, fornecedor_codigo                                                                          ';

        $resultado2 = DB::select($sql);
        return $resultado2;
    }

    public function buscaListaContratosOrgao($filtro)
    {
        $unidade_user = Unidade::find(session()->get('user_ug_id'));
        $idOrgaoUnidade = $unidade_user->orgao->id;

        $sql = '    select                                                                                                                  ';
        $sql .= "           CONCAT(O.codigo, ' - ', O.nome) as orgao,                                                                       ";
        $sql .= "           CONCAT(U.codigo, ' - ', U.nomeresumido) as unidade,                                                             ";
        $sql .= '           CASE                                                                                                            ';
        $sql .= "               WHEN    contratos.receita_despesa = 'D'                                                                     ";
        $sql .= "               THEN    'Despesa'                                                                                           ";
        $sql .= "               ELSE    'Receita'                                                                                           ";
        $sql .= '           END as receita_despesa,                                                                                         ';
        $sql .= '           contratos.unidades_requisitantes, contratos.numero,                                                             ';
        $sql .= '           F.cpf_cnpj_idgener as fornecedor_codigo, F.nome as fornecedor_nome,                                             ';
        $sql .= '           T.descricao as tipo,                                                                                            ';
        $sql .= '           C.descricao as categoria,                                                                                       ';
        $sql .= '           S.descricao as subcategoria,                                                                                    ';
        $sql .= '           contratos.processo, contratos.objeto, contratos.info_complementar,                                              ';
        $sql .= '           M.descricao as modalidade,                                                                                      ';
        $sql .= '           contratos.licitacao_numero, contratos.data_assinatura, contratos.data_publicacao,                               ';
        $sql .= '           contratos.vigencia_inicio, contratos.vigencia_fim, contratos.valor_inicial,                                     ';
        $sql .= '           contratos.valor_global, contratos.num_parcelas, contratos.valor_parcela, contratos.valor_acumulado,             ';
        $sql .= '           contratos.data_proposta_comercial,                                                                              ';
        $sql .= '           CASE                                                                                                            ';
        $sql .= "               WHEN    contratos.situacao = 't'                                                                            ";
        $sql .= "               THEN    'Ativo'                                                                                             ";
        $sql .= "               ELSE    'Inativo'                                                                                           ";
        $sql .= '           END as situacao,                                                                                                ';
        $sql .= '           contratos.unidades_requisitantes,                                                                               ';
        $sql .= '           CASE                                                                                                            ';
        $sql .= "               WHEN    contratos.prorrogavel = 't'     THEN    'Sim'                                                       ";
        $sql .= "               WHEN    contratos.prorrogavel = 'f'     THEN    'Não'                                                       ";
        $sql .= '           END as prorrogavel,                                                                                             ';
        $sql .= '           (                                                                                                               ';
        $sql .= '                   select   aut3.autoridade_signataria                                                                     ';
        $sql .= '                   from     autoridadesignataria aut3                                                                      ';
        $sql .= '                   where    aut3.ativo = true                                                                              ';
        $sql .= '                   and      aut3.id in (                                                                                   ';
        $sql .= '                               select  cas.autoridadesignataria_id                                                         ';
        $sql .= '                               from    contrato_autoridade_signataria cas                                                  ';
        $sql .= '                               where   cas.contratohistorico_id = H.id                                                     ';
        $sql .= '                   )                                                                                                       ';
        $sql .= '           ) as autoridade_signataria,                                                                                     ';
        $sql .= '           (                                                                                                               ';
        $sql .= '                   select                                                                                                  ';
        $sql .= '                       CASE                                                                                                ';
        $sql .= '                           WHEN    aut4.titular = true                                                                     ';
        $sql .= "                           THEN    'Titular'                                                                               ";
        $sql .= "                           ELSE    'Substituta'                                                                            ";
        $sql .= "                        END as qualificacao_da_autoridade                                                                  ";
        $sql .= '                   from     autoridadesignataria aut4                                                                      ';
        $sql .= '                   where    aut4.ativo = true                                                                              ';
        $sql .= '                   and      aut4.id in (                                                                                   ';
        $sql .= '                               select  cas.autoridadesignataria_id                                                         ';
        $sql .= '                               from    contrato_autoridade_signataria cas                                                  ';
        $sql .= '                               where   cas.contratohistorico_id = H.id                                                     ';
        $sql .= '                   )                                                                                                       ';
        $sql .= '           ) as qualificacao_da_autoridade                                                                                 ';
        $sql .= '   from    contratos                                                                                                       ';
        $sql .= '   left    join orgaosubcategorias as S on S.id = contratos.subcategoria_id                                                ';
        $sql .= '   left    join codigoitens as M on M.id = contratos.modalidade_id                                                         ';
        $sql .= '   left    join codigoitens as C on C.id = contratos.categoria_id                                                          ';
        $sql .= '   left    join codigoitens as T on T.id = contratos.tipo_id                                                               ';
        $sql .= '   left    join fornecedores as F on F.id = contratos.fornecedor_id                                                        ';
        $sql .= '   left    join unidades as U on U.id = contratos.unidade_id                                                               ';
        $sql .= '   left    join orgaos as O on O.id = U.orgao_id                                                                           ';

        $sql .= '   left    join contratohistorico as H on H.contrato_id = contratos.id and H.tipo_id = (                                   ';
        $sql .= '           select  id from codigoitens where descricao =                                                                   ';
        $sql .= "           'Contrato'                                                                                                      ";
        $sql .= '           and     codigo_id = ( select id from codigos where descricao =                                                  ';
        $sql .= "           'Tipo de Contrato'";
        $sql .= '    ) )                                                                                                                    ';

        $sql .= "   where O.id = $idOrgaoUnidade                                                                                            ";
        $sql .= "   and contratos.elaboracao = false                                                                                        ";
        $sql .= '   order   by contratos.numero, fornecedor_codigo                                                                          ';
        $resultado2 = DB::select($sql);
        return $resultado2;
    }

    public function buscaListaTodosContratos($filtro)
    {
        $lista = $this->select([
            DB::raw('CONCAT("O"."codigo", \' - \', "O"."nome")  as orgao'),
            DB::raw('CONCAT("U"."codigo",\' - \',"U"."nomeresumido")  as unidade'),
            DB::raw('CASE
                                WHEN receita_despesa = \'D\'
                                THEN \'Despesa\'
                                ELSE \'Receita\'
                           END as receita_despesa'),
            'unidades_requisitantes',
            'numero',
            DB::raw('"F"."cpf_cnpj_idgener" as fornecedor_codigo'),
            DB::raw('"F"."nome" as fornecedor_nome'),
            'T.descricao as tipo',
            'C.descricao as categoria',
            'S.descricao as subcategoria',
            'processo',
            'objeto',
            'info_complementar',
            'M.descricao as modalidade',
            'licitacao_numero',
            'data_assinatura',
            'data_publicacao',
            'data_proposta_comercial',
            'vigencia_inicio',
            'vigencia_fim',
            'valor_inicial',
            'valor_global',
            'num_parcelas',
            'valor_parcela',
            'valor_acumulado',
            DB::raw('CASE
                                WHEN contratos.situacao = \'t\'
                                THEN \'Ativo\'
                                ELSE \'Inativo\'
                           END as situacao'),
            'unidades_requisitantes',
            DB::raw('CASE
            WHEN contratos.prorrogavel = \'t\' THEN \'Sim\'
            WHEN contratos.prorrogavel = \'f\' THEN \'Não\'
            END as prorrogavel'),
        ]);

        $lista->leftjoin('orgaosubcategorias as S', 'S.id', '=', 'contratos.subcategoria_id');
        $lista->leftjoin('codigoitens as M', 'M.id', '=', 'contratos.modalidade_id');
        $lista->leftjoin('codigoitens as C', 'C.id', '=', 'contratos.categoria_id');
        $lista->leftjoin('codigoitens as T', 'T.id', '=', 'contratos.tipo_id');
        $lista->leftjoin('fornecedores as F', 'F.id', '=', 'contratos.fornecedor_id');
        $lista->leftjoin('unidades as U', 'U.id', '=', 'contratos.unidade_id');
        $lista->leftjoin('orgaos as O', 'O.id', '=', 'U.orgao_id');
        $lista->where('contratos.elaboracao', false);

        return $lista->get();
    }

    public function getContratosPorUnidade(int $unidade_id)
    {
        // não pode ter o get().
        return $contratos = Contrato::where('unidade_id', $unidade_id);
    }

    public function buscaNovosContratosPorUg(int $unidade_id)
    {
        $dataHoje = date('Y-m-d H:i:s');
        $cincoDiasAtras = date('Y-m-d H:i:s', strtotime('-5 days'));
        $contratosDaUnidade = self::getContratosPorUnidade($unidade_id);
        return $quantidadeDeContratosNovos = $contratosDaUnidade->join('fornecedores', 'fornecedores.id', '=', 'contratos.fornecedor_id')
            ->join('unidades', 'unidades.id', '=', 'contratos.unidade_id')
            ->where('contratos.vigencia_fim', '>=', $dataHoje)
            ->where('contratos.vigencia_inicio', '<=', $dataHoje)
            ->where('contratos.vigencia_inicio', '>=', $cincoDiasAtras)
            ->where('contratos.elaboracao', '=', false)
            ->count();
    }

    public function buscaContratosVigentesPorUg(int $unidade_id)
    {
        $dataHoje = date('Y-m-d H:i:s');
        $contratosDaUnidade = self::getContratosPorUnidade($unidade_id);
        return $quantidadeDeContratosNovos = $contratosDaUnidade->join('fornecedores', 'fornecedores.id', '=', 'contratos.fornecedor_id')
            ->join('unidades', 'unidades.id', '=', 'contratos.unidade_id')
            ->where('contratos.situacao', '=', true)
            ->where(function ($query) use ($dataHoje) {
                $query->where('contratos.vigencia_fim', '>=', $dataHoje)
                      ->where('contratos.vigencia_inicio', '<=', $dataHoje)
                      ->orWhere('contratos.is_prazo_indefinido', true);
            })
            ->where('contratos.elaboracao', '=', false)
            ->count();
    }

    public function buscaContratosVencidosPorUg(int $unidade_id)
    {
        $dataHoje = date('Y-m-d H:i:s');
        $cincoDiasAtras = date('Y-m-d H:i:s', strtotime('-5 days'));
        $contratosDaUnidade = self::getContratosPorUnidade($unidade_id);
        return $quantidadeDeContratosNovos = $contratosDaUnidade->join('fornecedores', 'fornecedores.id', '=', 'contratos.fornecedor_id')
            ->join('unidades', 'unidades.id', '=', 'contratos.unidade_id')
            ->where('contratos.vigencia_fim', '<', $dataHoje)
            ->where('contratos.elaboracao', '=', false)
            ->count();
    }

    public function atualizaContratoFromHistorico(string $contrato_id, array $array)
    {
        $this->where('id', '=', $contrato_id)
            ->update($array);

        return $this;
    }

    public function atualizaValorAcumuladoFromCronograma(Contratocronograma $contratocronograma)
    {
        $contrato_id = $contratocronograma->contrato_id;

        $valor_acumulado = $contratocronograma->where('contrato_id', '=', $contrato_id)
            ->sum('valor');

        $this->where('id', '=', $contrato_id)->update(['valor_acumulado' => $valor_acumulado]);

    }

    public function getFornecedor()
    {
        $fornecedor = Fornecedor::find($this->fornecedor_id);
        return $fornecedor->cpf_cnpj_idgener . ' - ' . $fornecedor->nome;
    }

    public function getReceitaDespesa()
    {
        switch ($this->receita_despesa) {
            case 'D':
                return 'Despesa';
            case 'R':
                return 'Receita';
            case 'S':
                return 'Sem ônus';
            case 'X':
                return 'Receita e Despesa';
            default:
                return '';
        }
    }


    public function getUnidade()
    {
        $unidade = Unidade::find($this->unidade_id);
        return $unidade->codigo . ' - ' . $unidade->nomeresumido;
    }

    public function getUnidadeOrigem()
    {
        if (!isset($this->unidadeorigem_id)) {
            return '';
        }

        return $this->unidadeorigem->codigo . ' - ' . $this->unidadeorigem->nomeresumido;
    }

    public function getUnidadeCompra()
    {
        if (!isset($this->unidadecompra_id)) {
            return '';
        }

        return $this->unidadecompra->codigo . ' - ' . $this->unidadecompra->nomeresumido;
    }

    public function getNumeroCompra()
    {
        if ($this->numero_compra) {
            $siasg_compra = Siasgcompra::find($this->numero_compra);
            if (!isset($siasg_compra->numero)) {
                return '';
            }
            return $siasg_compra->numero . '/' . $siasg_compra->ano;
        } else {
            return $this->licitacao_numero;
        }
    }

    public function getModalidade()
    {
        return $this->modalidade->descricao;
    }

    public function getOrgao()
    {
        $orgao = Orgao::whereHas('unidades', function ($query) {
            $query->where('id', '=', $this->unidade_id);
        })->first();

        return $orgao->codigo . ' - ' . $orgao->nome;
    }

    public function getTipo()
    {
        if ($this->tipo_id) {
            $tipo = Codigoitem::find($this->tipo_id);

            return $tipo->descricao;
        } else {
            return '';
        }
    }

    public function getCategoria()
    {
        return $this->categoria->descricao;
    }

    public function getSubCategoria()
    {
        if ($this->subcategoria_id) {
            $subcategoria = OrgaoSubcategoria::find($this->subcategoria_id);
            return $subcategoria->descricao;
        } else {
            return '';
        }
    }

    public function getVigenciaFimPrazoIndeterminado()
    {
        if ($this->is_prazo_indefinido == '1' || $this->is_prazo_indefinido == true) {
            return 'Indeterminado';
        }
        $vigenciafim = date_create($this->vigencia_fim);
        return date_formaT($vigenciafim, 'd/m/Y');

    }

    public function getNumeroParcelasPrazoIndeterminado()
    {
        if ($this->is_prazo_indefinido == '1' || $this->is_prazo_indefinido == true) {
            return 'Indeterminado';
        }
        return $this->num_parcelas;

    }

    public function getLinkPNCP($linkFormatado = false)
    {
        try {
            $codigoExcluido = Codigoitem::whereHas('codigo', function ($q) {
                $q->where('descricao', 'Situação Envia Dados PNCP');
            })->where('descres', 'EXCLUIDO')->first()->id;

            $pncp = @EnviaDadosPncp::where([
                ['pncpable_type', Contratohistorico::class],
                ['contrato_id', '=', $this->id],
                ['situacao', '<>', $codigoExcluido],
                ['sequencialPNCP', '<>', null]
            ])->oldest()->first();
            if (isset($pncp->sequencialPNCP) && isset($pncp->link_pncp)) {
                /*Mapa:
                de: [0]https://[2]pncp.gov.br/pncp-api/v1/orgaos/[6]10626896000172/contratos/[8]2022/[9]42
                de: [0]00489828000155-[1]2-[2]000032/[1]2022
                para: https://pncp.gov.br/app/contratos/10626896000172/2022/42
                */
                $host = explode("/", $pncp->link_pncp);
                $arr = explode("-", str_replace('/', '-', $pncp->sequencialPNCP));
                $link_pncp = $host[0] . '//' . $host[2] . '/app/contratos/' . $arr[0] . '/' . $arr[3] . '/' . $arr[2];
                if ($linkFormatado == true) {
                    $link_pncp = "<a href='$host[0]//$host[2]/app/contratos/$arr[0]/$arr[3]/$arr[2]' target='_blank'> Clique aqui para acessar</a>";
                }
                return $link_pncp;
            } else {
                return 'disabled';
            }
        } catch (\Exception $exception) {
            return 'disabled';
        }
    }

    public function formatVlrParcela()
    {
        return 'R$ ' . number_format($this->valor_parcela, 2, ',', '.');
    }

    public function retornaAmparo()
    {
        $amparo = "";
        foreach ($this->amparoslegais as $key => $value) {
            if ($key == 0) {
                $amparo .= "";
            } elseif ($key == (count($this->amparoslegais) - 1)) {
                $amparo .= " e ";
            } else {
                $amparo .= ", ";
            }

            $amparo .= $value->ato_normativo;
            $amparo .= (!is_null($value->artigo)) ? " - Artigo: " . $value->artigo : "";
            $amparo .= (!is_null($value->paragrafo)) ? " - Parágrafo: " . $value->paragrafo : "";
            $amparo .= (!is_null($value->inciso)) ? " - Inciso: " . $value->inciso : "";
            $amparo .= (!is_null($value->alinea)) ? " - Alinea: " . $value->alinea : "";
        }

        return $amparo;
    }

    public function formatVlrGlobal()
    {
        return 'R$ ' . number_format($this->valor_global, 2, ',', '.');
    }

    public function formatVlrAcumulado()
    {
        return 'R$ ' . number_format($this->valor_acumulado, 2, ',', '.');
    }

    public function formatTotalDespesasAcessorias()
    {
        return 'R$ ' . number_format($this->total_despesas_acessorias, 2, ',', '.');
    }

    public function contratoAPI(string $prefixoAPI)
    {

        return [
            'id' => $this->id,
            'receita_despesa' => ($this->receita_despesa) == 'D' ? 'Despesa' : 'Receita',
            'numero' => $this->numero,
            'contratante' => [
                'orgao_origem' => [
                    'codigo' => @$this->unidadeorigem->orgao->codigo,
                    'nome' => @$this->unidadeorigem->orgao->nome,
                    'unidade_gestora_origem' => [
                        'codigo' => @$this->unidadeorigem->codigo,
                        'nome_resumido' => @$this->unidadeorigem->nomeresumido,
                        'nome' => @$this->unidadeorigem->nome,
                        'sisg' => @$this->unidadeorigem->sisg == true ? 'Sim' : 'Não',
                        'utiliza_siafi' => @$this->unidadeorigem->utiliza_siafi == true ? 'Sim' : 'Não',
                        'utiliza_antecipagov' => @$this->unidadeorigem->utiliza_antecipagov == true ? 'Sim' : 'Não'
                    ],
                ],
                'orgao' => [
                    'codigo' => $this->unidade->orgao->codigo,
                    'nome' => $this->unidade->orgao->nome,
                    'unidade_gestora' => [
                        'codigo' => $this->unidade->codigo,
                        'nome_resumido' => $this->unidade->nomeresumido,
                        'nome' => $this->unidade->nome,
                        'sisg' => $this->unidade->sisg == true ? 'Sim' : 'Não',
                        'utiliza_siafi' => $this->unidade->utiliza_siafi == true ? 'Sim' : 'Não',
                        'utiliza_antecipagov' => $this->unidade->utiliza_antecipagov == true ? 'Sim' : 'Não'
                    ],
                ],
            ],
            'fornecedor' => [
                'tipo' => $this->fornecedor->tipo_fornecedor,
                'cnpj_cpf_idgener' => $this->fornecedor->cpf_cnpj_idgener,
                'nome' => $this->fornecedor->nome,
            ],
            'codigo_tipo' => @$this->tipo->descres,
            'tipo' => $this->tipo->descricao,
            'subtipo' => @$this->subtipo,
            'prorrogavel' => $this->getProrrogavelColunaAttribute() == '' ? null : $this->getProrrogavelColunaAttribute(),
            'situacao' => $this->situacao == true ? 'Ativo' : 'Inativo',
            'justificativa_inativo' => @$this->justificativa_inativo->descricao,
            'categoria' => $this->categoria->descricao,
            'subcategoria' => @$this->orgaosubcategoria->descricao,
            'unidades_requisitantes' => $this->unidades_requisitantes,
            'processo' => $this->processo,
            'objeto' => $this->objeto,
            'amparo_legal' => @$this->retornaAmparo(),
            'informacao_complementar' => $this->info_complementar,
            'codigo_modalidade' => @$this->modalidade->descres,
            'modalidade' => $this->modalidade->descricao,
            'unidade_compra' => @$this->unidadecompra->codigo,
            'licitacao_numero' => $this->licitacao_numero,
            'sistema_origem_licitacao' => @$this->sistema_origem_licitacao,
            'data_assinatura' => $this->data_assinatura,
            'data_publicacao' => $this->data_publicacao,
            'data_proposta_comercial' => $this->data_proposta_comercial,
            'vigencia_inicio' => $this->vigencia_inicio,
            'vigencia_fim' => $this->vigencia_fim,
            'valor_inicial' => number_format($this->valor_inicial, 2, ',', '.'),
            'valor_global' => number_format($this->valor_global, 2, ',', '.'),
            'num_parcelas' => $this->num_parcelas,
            'valor_parcela' => number_format($this->valor_parcela, 2, ',', '.'),
            'valor_acumulado' => number_format($this->valor_acumulado, 2, ',', '.'),
            'links' => [
                'historico' => url($prefixoAPI . '/' . $this->id . '/historico/'),
                'empenhos' => url($prefixoAPI . '/' . $this->id . '/empenhos/'),
                'cronograma' => url($prefixoAPI . '/' . $this->id . '/cronograma/'),
                'garantias' => url($prefixoAPI . '/' . $this->id . '/garantias/'),
                'itens' => url($prefixoAPI . '/' . $this->id . '/itens/'),
                'prepostos' => url($prefixoAPI . '/' . $this->id . '/prepostos/'),
                'responsaveis' => url($prefixoAPI . '/' . $this->id . '/responsaveis/'),
                'despesas_acessorias' => url($prefixoAPI . '/' . $this->id . '/despesas_acessorias/'),
                'faturas' => url($prefixoAPI . '/' . $this->id . '/faturas/'),
                'ocorrencias' => url($prefixoAPI . '/' . $this->id . '/ocorrencias/'),
                'terceirizados' => url($prefixoAPI . '/' . $this->id . '/terceirizados/'),
                'arquivos' => url($prefixoAPI . '/' . $this->id . '/arquivos/'),
            ]
        ];
    }

    public function buscaContratoPorId(int $contrato_id)
    {

        $contrato = Contrato::whereHas('unidade', function ($q) {
            $q->whereHas('orgao', function ($o) {
                $o->where('situacao', true);
            })->where('sigilo', false);
        })
            ->where('id', $contrato_id)
            ->where('elaboracao', false)
            ->get();

        return $contrato;
    }

    public function getAmparoLegal()
    {
        $obj = $this->belongsToMany(
            AmparoLegal::class,
            'amparo_legal_contrato',
            'contrato_id',
            'amparo_legal_id'
        )->first();
        if ($obj) {
            return $obj->ato_normativo;
        } else {
            return null;
        }
    }

    /**
     * Retorna um array de Contratos vinculados a minutas CIPI (Cadastro Integrado de Projetos de Investimento).
     *
     * @param array $range Carbon.
     * @return Contratos[]|null Retorna um array de Contratos
     */
    public static function buscaContratosComEmpenhosCipi($range)
    {
        $contratos = DB::select(DB::raw("SELECT DISTINCT
                                            o.codigo AS codigo_orgao,
                                            o.nome AS orgao_nome,
                                            u.codigo AS codigo_unidade,
                                            u.nome AS nome_unidade,
                                            u2.codigo AS codigo_unidade_origem,
                                            u2.nome AS nome_unidade_origem,
                                            c2.descricao AS tipo_contrato,
                                            c.id AS contrato_id,
                                            c.numero AS numero_contrato,
                                            c3.descricao AS categoria,
                                            m.numero_cipi AS id_cipi,
                                            c.licitacao_numero,
                                            u3.codigo || c4.descres || REPLACE (c.licitacao_numero, '/', '') AS chave_compra,
                                            regexp_replace(c.objeto, '\r\n', ' ', 'g') AS objeto,
                                            CASE WHEN c.receita_despesa = 'R' THEN 'Receita' ELSE 'Despesa'
                                                END AS receita_despesa,
                                            c.data_assinatura,
                                            c.data_publicacao,
                                            c.processo AS processo,
                                            c.vigencia_inicio,
                                            c.vigencia_fim,
                                            f.cpf_cnpj_idgener AS fornecedor_cnpj_cpf_idgener,
                                            f.nome AS fornecedor_nome,
                                            c.valor_global,
                                            c.valor_acumulado,
                                            'https://contratos.comprasnet.gov.br/transparencia/contratos/' || c.id AS link_transparencia,
                                            edp.link_pncp AS link_json_pncp,
                                            c.updated_at
                                        FROM contratos c
                                        JOIN unidades u ON u.id = c.unidade_id
                                        JOIN orgaos o ON o.id = u.orgao_id
                                        JOIN unidades u2 ON u2.id = c.unidadeorigem_id
                                        JOIN fornecedores f ON f.id = c.fornecedor_id
                                        JOIN minutaempenhos m ON m.contrato_id = c.id
                                        LEFT JOIN codigoitens c2 ON c2.id = c.tipo_id
                                        LEFT JOIN codigoitens c3 ON c3.id = c.categoria_id
                                        LEFT JOIN unidades u3 ON u3.id = c.unidadecompra_id
                                        LEFT JOIN codigoitens c4 ON c4.id = c.modalidade_id
                                        LEFT JOIN (SELECT sub_select.link_pncp, sub_select.contrato_id
                                                   FROM envia_dados_pncp sub_select
                                                   JOIN codigoitens c5 ON c5.id = sub_select.situacao
                                                   WHERE c5.descres = 'SUCESSO'
                                                   AND sub_select.contrato_id IS NOT NULL
                                                   AND sub_select.\"sequencialPNCP\" IS NOT NULL
                                                   AND sub_select.link_pncp IS NOT NULL
                                                   AND sub_select.pncpable_type = 'App\Models\Contratohistorico'
                                                   AND sub_select.tipo_contrato = 'Contrato'
                                                  ) AS edp ON edp.contrato_id = c.id
                                        WHERE c.deleted_at IS NULL
                                        and c.elaboracao = false
                                        AND m.deleted_at IS NULL
                                        AND m.numero_cipi ~ '^[0-9]'
                                        AND c.updated_at BETWEEN :date_begin AND :date_end")
            , array('date_begin' => $range[0], 'date_end' => $range[1]));

        return $contratos;
    }

    public function verificaSeContratoJaExiste($numero, $tipoContrato, $unidadeOrigem)
    {
        $contrato = $this->where('numero', $numero)
            ->where('tipo_id', $tipoContrato)
            ->where('unidadeorigem_id', $unidadeOrigem)
            ->first();

        if ($contrato) {
            return true;
        }

        return false;
    }

     /**
     * Verifica se o contrato é do tipo Empenho ou Carta de Contrato
     *
     * @return boolean
     */
    public function verificaEmpenhoOrCartaContrato()
    {
        $tipos = Codigoitem::whereIn('descricao', ['Empenho', 'Carta Contrato'])
        ->whereHas('codigo', function ($query) {
            $query->where('descricao', 'Tipo de Contrato');
        })->pluck('id')->toArray();

        return in_array($this->tipo_id, $tipos) ? true : false;
    }

    /**
     * Retorna um array com a descrição dos cadastros que são considerados Termos de Contrato
     *
     * @return string[]
     */
    public static function getDescricoesTermosDeContrato(): array
    {
        return [
            'Termo Aditivo',

            'Termo de Apostilamento',
            'Termo Apostilamento',

            'Termo Rescisão',
            'Termo de Rescisão',

            'Termo de Encerramento' # Para ser compatível com o legado
        ];
    }

    /**
     * @param $descricaoTermo
     * @return string
     *
     * Há descrições escritas de formas diferentes na tabela codigoitens para termos e arquivos de termos
     * Neste caso, padroniza para ser transparente ao enviar para o PNCP
     *
     */
    public static function padronizaDescricaoTermoContrato($descricaoTermo): string
    {
        $termos = [
            'Termo Apostilamento' => 'Termo de Apostilamento',
            'Termo Rescisão' => 'Termo de Rescisão'
        ];

        if (array_key_exists($descricaoTermo, $termos)) {
            return $termos[$descricaoTermo];
        }
        return $descricaoTermo;
    }

    /**
     * Retorna as descrições dos tipos de Contrato e Termos de contrato que são (os tipos) enviados ao PNCP
     *
     * @return array
     */
    public static function getDescricoesTiposArquivosVinculadosPncp(): array
    {
        return array_merge(['Contrato'], self::getDescricoesTermosDeContrato());
    }

    public static function isTermoDeContrato($tipo): bool
    {
        $descricoes = Contrato::getDescricoesTermosDeContrato();
        return in_array($tipo, $descricoes);
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function contrato_autoridade_signataria()
    {
        return $this->belongsToMany(
            'App\Models\Autoridadesignataria',
            'contrato_autoridade_signataria',           // from esse
            'contratohistorico_id', // where esse - 3  = contrato_id
            'autoridadesignataria_id',          // select esse - 1
        );
    }

    public function autoridades_signatarias()
    {
        return $this->belongsToMany(
            Contratohistorico::class,
            'contrato_autoridade_signataria',           // from esse
            'contratohistorico_id', // where esse - 3  = contrato_id
            'id',          // select esse - 1
        );
    }

    public function historicocontrato()
    {
        return $this->belongsToMany(
            Contratohistorico::class,
            Contratohistorico::class,        // from esse - 2
            'contrato_id', // where esse - 3  = contrato_id
            'id',          // select esse - 1
        );
    }

    public function arquivos()
    {
        return $this->hasMany(Contratoarquivo::class, 'contrato_id');
    }

    public function categoria()
    {
        return $this->belongsTo(Codigoitem::class, 'categoria_id');
    }

    public function contratominutas(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(ContratoMinuta::class, 'contrato_id');
    }

    public function cronograma()
    {
        return $this->hasMany(Contratocronograma::class, 'contrato_id');
    }

    public function despesasacessorias()
    {
        return $this->hasMany(Contratodespesaacessoria::class, 'contrato_id');
    }

    public function empenhos()
    {
        return $this->hasMany(Contratoempenho::class, 'contrato_id');
    }

    public function faturas()
    {
        return $this->hasMany(Contratofatura::class, 'contrato_id');
    }

    public function fornecedor()
    {
        return $this->belongsTo(Fornecedor::class, 'fornecedor_id');
    }

    public function garantias()
    {
        return $this->hasMany(Contratogarantia::class, 'contrato_id');
    }

    public function historico()
    {
        return $this->hasMany(Contratohistorico::class, 'contrato_id');
    }

    public function itens()
    {
        return $this->hasMany(Contratoitem::class, 'contrato_id');
    }

    public function modalidade()
    {
        return $this->belongsTo(Codigoitem::class, 'modalidade_id');
    }

    public function ocorrencias()
    {
        return $this->hasMany(Contratoocorrencia::class, 'contrato_id');
    }

    public function unidadesdescentralizadas()
    {
        return $this->hasMany(Contratounidadedescentralizada::class, 'contrato_id');
    }

    public function orgaosubcategoria()
    {
        return $this->belongsTo(OrgaoSubcategoria::class, 'subcategoria_id');
    }

    public function prepostos()
    {
        return $this->hasMany(Contratopreposto::class, 'contrato_id')
            ->where('situacao', true);
    }

    /**
     * retirado where situacao,
     * para que todos os responsáveis fossem trazidos para o extrato pdf e não apenas os ativos
     */
    public function responsaveis()
    {
        return $this->hasMany(Contratoresponsavel::class, 'contrato_id');
    }

    public function terceirizados()
    {
        return $this->hasMany(Contratoterceirizado::class, 'contrato_id');
    }

    public function tipo()
    {
        return $this->belongsTo(Codigoitem::class, 'tipo_id');
    }

    public function unidade()
    {
        return $this->belongsTo(Unidade::class, 'unidade_id');
    }

    public function unidadeorigem()
    {
        return $this->belongsTo(Unidade::class, 'unidadeorigem_id');
    }

    public function unidadecompra()
    {
        return $this->belongsTo(Unidade::class, 'unidadecompra_id');
    }

    public function unidadebeneficiaria()
    {
        return $this->belongsTo(Unidade::class, 'unidadebeneficiaria_id');
    }

    public function publicacao()
    {
        return $this->hasOne(Contratopublicacoes::class, 'contrato_id');
    }

    public function amparoslegais()
    {
        return $this->belongsToMany(
            AmparoLegal::class,
            'amparo_legal_contrato',
            'contrato_id',
            'amparo_legal_id'
        );
    }

    public function minutasempenho()
    {
        return $this->belongsToMany(
            'App\Models\MinutaEmpenho',
            'contrato_minuta_empenho_pivot',
            'contrato_id',
            'minuta_empenho_id'
        );
    }

    public function contratoEmpenhos()
    {
        return $this->belongsToMany(
            'App\Models\Empenho',
            'contratoempenhos',
            'contrato_id',
            'empenho_id'
        );
    }

    public function fornecedoresSubcontratados()
    {
        return $this->belongsToMany(
            Fornecedor::class,
            'contratos_fornecedores_pivot',
            'contrato_id',
            'fornecedor_id'
        );
    }

    public function antecipagovs()
    {
        return $this->belongsToMany(
            AntecipaGov::class,
            'contrato_antecipagov',
            'contrato_id',
            'antecipagov_id'
        );
    }

    public function justificativa_inativo()
    {
        return $this->belongsTo(Codigoitem::class, 'justificativa_contrato_inativo_id');
    }

    public function subrogacoes()
    {
        return $this->hasMany(Subrogacao::class, 'contrato_id');
    }

    public function fornecedorContratosHistorico()
    {
        return $this->belongsTo(FornecedorContratoHistorico::class, 'id','contrato_id')
            ->where('fornecedor_id', $this->fornecedor_id);
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    public function getProrrogavelColunaAttribute()
    {
        if ($this->prorrogavel === null) {
            return '';
        }
        return $this->prorrogavel == 1 ? 'Sim' : 'Não';
    }

    public function getLicitacaoNumeroLimpaAttribute()
    {
        return str_replace('/', '', $this->licitacao_numero);
    }

    public function getAmparosAttribute()
    {
        return $this->amparoslegais->toArray();
    }

    public function getPrimeiraSubrogacaoAttribute()
    {
        return $this->subrogacoes()->oldest()->first();
    }

    public function getLinkTransparencia($rota)
    {

        $codigoUnidade = $this->unidade->codigo;

        $link = "/transparencia/$rota?unidade=$codigoUnidade&numerocontrato=$this->numero";

        return "<a href='$link' target='__blank'>Clique aqui para acessar</a>";
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
