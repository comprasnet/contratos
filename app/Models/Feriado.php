<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use DateTime;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Feriado extends Model
{
    use CrudTrait;
    use LogsActivity;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'feriados';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = [
        'data',
        'descricao',
        'tipo_id'
    ];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */


    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function getTipoIdAttribute($value)
    {
        $retorno = $this->formatarAtributoTipoId($value);
        return $retorno;
    }

    // Métodos que auxiliam os mutators
    public function formatarAtributoTipoId($id)
    {
        $retorno = Codigoitem::find($id);
        if ($retorno) {
            return $retorno->descricao;
        } else {
            return null;
        }
    }

    public static function isFeriado($data) {
        $feriado = self::where('data', $data)->get();

        if (!$feriado->isEmpty()) {
            return true;
        }

        return false;
    }

    public static function calculaDiasUteis($data, $dias)
    {
        $dateTime = DateTime::createFromFormat('Y-m-d', $data);

        $listaDiasUteis = [];
        $contador = 0;

        while ($contador < $dias) {
            $dateTime->modify('+1 weekday'); // adiciona um dia pulando finais de semana
            $data = $dateTime->format('Y-m-d');

            if (!self::isFeriado($data)) {
                $listaDiasUteis[] = $data;
                $contador++;
            }
        }

        return $listaDiasUteis;
    }

}
