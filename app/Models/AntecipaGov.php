<?php

namespace App\Models;

use App\Models\Codigoitem;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class AntecipaGov extends Model
{
    use LogsActivity;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected static $logFillable = true;
    protected static $logName = 'antecipagov';

    protected $table = 'antecipagov';

    protected $fillable = [
//        'conta_bancaria',
//        'num_agencia',
//        'num_banco',
        'status_operacao',
        'num_operacao',
        'num_cotacao',
        'identificador_unico',
        'data_acao',
        'valor_operacao',
        'valor_parcela',
        'situacao_id',
        'domicilio_bancario_id'
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function contratos()
    {
        return $this->belongsToMany(
            Contrato::class,
            'contrato_antecipagov',
            'antecipagov_id',
            'contrato_id'
        );
    }

    public function statusSituacao()
    {
        return $this->hasOne(Codigoitem::class, 'id', 'situacao_id');
    }

    public function domicilioBancario()
    {
        return $this->belongsTo(DomicilioBancario::class, 'domicilio_bancario_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */


}
