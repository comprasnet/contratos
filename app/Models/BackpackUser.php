<?php

namespace App\Models;

use App\User;
use Backpack\Base\app\Notifications\ResetPasswordNotification as ResetPasswordNotification;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Permission\Traits\HasRoles;
use Tightenco\Parental\HasParentModel;

class BackpackUser extends User
{
    use HasParentModel;
    use Notifiable;
    use CrudTrait;
    use HasRoles;
    use LogsActivity;
    use SoftDeletes;

    protected static $logFillable = true;
    protected static $logName = 'usuario';

    protected $fillable = [
        'cpf',
        'name',
        'email',
        'ugprimaria',
        'password',
        'senhasiafi',
        'situacao',
        'acessogov',
        'id_sistema_origem',
        'last_login_at',
        'last_login_ip',
        'last_login_browser',
        'telegram_id'
    ];

    protected $table = 'users';

    /**
     * Send the password reset notification.
     *
     * @param string $token
     *
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    /**
     * Get the e-mail address where password reset links are sent.
     *
     * @return string
     */
    public function getEmailForPasswordReset()
    {
        return $this->email;
    }

    public function havePermissionUg($id)
    {

        $ugprimaria = $this->where('ugprimaria', '=', $id)->where('id', '=', $this->id)->first();
        $ugsecundaria = $this->whereHas('unidades', function ($query) use ($id) {
            $query->where('unidade_id', '=', $id);
        })->where('id', '=', $this->id)->first();

        if ($ugprimaria or $ugsecundaria) {
            return true;
        }
        return false;
    }

    public function unidadeprimaria($id)
    {
        $ug = Unidade::find($id);
        return $ug;

    }

    public function unidadesSecundarias()
    {
        $ugsecundaria = Unidade::select('codigo', 'id')
            ->whereHas('users', function ($query) {
                $query->where('user_id', '=', $this->id);
            })
            ->where('tipo', '=', 'E')
            ->orderBy('codigo', 'asc')
            ->pluck('codigo')
            ->toArray();

        return $ugsecundaria;
    }

    public function getUGPrimaria()
    {
        $retorno = '-';

        if ($this->ugprimaria) {
            $unidade = Unidade::find($this->ugprimaria);
            $retorno = $unidade->codigo . ' - ' . $unidade->nomeresumido;
        }

        return $retorno;
    }

    public function getOrgaoByUserId($idUser){
        $sql = "
                -- Busca o órgão vinculado a UG/UASG primária do usuário.
                SELECT
                    o.id,
                    o.codigo
                FROM
                    users u
                INNER JOIN unidades u2 ON
                    u.ugprimaria = u2.id
                INNER JOIN orgaos o ON
                    u2.orgao_id = o.id
                WHERE
                    u.id = $idUser
                UNION
                SELECT -- Busca o(s) órgão(s) vinculado(s) a(s) UG/UASG(s) secundária(s) do usuário.
                    o2.id,
                    o2.codigo
                FROM
                    users u3
                INNER JOIN unidadesusers u4 ON
                    u3.id = u4.user_id
                INNER JOIN unidades u5 ON
                    u4.unidade_id = u5.id
                INNER JOIN orgaos o2 ON
                    u5.orgao_id = o2.id
                WHERE
                    u3.id = $idUser;

        ";

        $res = DB::select($sql);
        return $res;

    }

    /**
     * Função que retorna todas as Unidades (A primária e as secundárias) vinculadas ao usuário.
     *
     * @return \Illuminate\Database\Eloquent\Collection|\App\Models\Unidade
     */
    public function getAllUnidadesUser() {

        $unidades = Unidade::select('unidades.*')
            ->join('users as u', 'unidades.id', '=', 'u.ugprimaria')
            ->where('u.id', $this->id)
            ->whereNull('unidades.deleted_at');

        $unidades = $unidades->union(
            Unidade::select('unidades.*')
                ->join('unidadesusers as u4', 'unidades.id', '=', 'u4.unidade_id')
                ->join('users as u3', 'u4.user_id', '=', 'u3.id')
                ->where('u3.id', $this->id)
                ->whereNull('unidades.deleted_at')
        );

        return $unidades->get();
    }

    public function getAdmins(){
        $usuariosAdministradores = $this->with('roles')
            ->whereHas('roles', function ($query) {
                $query->whereIn('name', ['Administrador']);
            })
            ->where('situacao', true)
            ->get();

        return $usuariosAdministradores;
    }

    public static function getPermissionAdmin($id){

    return Role::select('roles.id', 'roles.name')
        ->join('model_has_roles', 'roles.id', '=', 'model_has_roles.role_id')
        ->where('model_has_roles.model_id', $id)
        ->where('model_has_roles.model_type', 'App\Models\BackpackUser')
        ->pluck('roles.name','roles.id')
        ->toArray();

    }

    public static function getAdminSuporteRoles($idUser)
    {
        $admSuporte = "";

        if(backpack_user()->id == $idUser){
            $admSuporte = 'Administrador Suporte';
        }
            
        return array_merge(
            $admSuporte != '' ? ['Administrador Suporte'] : [],
            [
                'Administrador Órgão',
                'Administrador Unidade',
                'Setor Contratos',
                'Responsável por Contrato',
                'Execução Financeira',
                'Consulta',
                'Gestor de Atas',
                'Apoio Administrativo',
                'AntecipaGov',
                'Almoxarifado',
                'Consulta Global',
                // 'Fornecedor',
                // 'Preposto'
            ]
        );
    }

    public static function getExclusivoGestaoAtasRoles($idUser)
    {

        $admUnidade = "";

        if(backpack_user()->id == $idUser){
            $admUnidade = 'Administrador Unidade';
        }
        return array_merge(
            $admUnidade != '' ? ['Administrador Unidade'] : [],
            [
                'Gestor de Atas'
            ]
        );
    }

    public static function getAdminUnidadeRoles()
    {
        // Busca as permissões do usuario logado
        $rolesUsuarioLogado = BackpackUser::getPermissionAdmin(backpack_user()->id);
        $rolesExclude = BackpackUser::getExcludedRoles();
        
        // verificar se o usuario logado é Adm de orgão ou suporte para editar usuarios de unidade
        $filteredRoles = array_diff($rolesExclude, ['Administrador Unidade']);
        $RolesAdministrador = array_intersect($filteredRoles,  $rolesUsuarioLogado);

        return array_merge(
            !empty($RolesAdministrador) ? $RolesAdministrador : [],
            [
                'Apoio Administrativo',
                'AntecipaGov',
                'Administrador Unidade',
                'Setor Contratos',
                'Responsável por Contrato',
                'Execução Financeira',
                'Consulta',
                'Gestor de Atas',
                // 'Fornecedor',
                // 'Preposto',
                'Almoxarifado'
            ]
        );
        
    }

    public static function getAdminOrgaoRoles($idUser)
    {
        return array_merge(
                [
                    'Administrador Órgão',
                    'Administrador Unidade',
                    'Setor Contratos',
                    'Responsável por Contrato',
                    'Execução Financeira',
                    'Consulta',
                    'Gestor de Atas',
                    'Apoio Administrativo',
                    'AntecipaGov',
                    // 'Fornecedor',
                    // 'Preposto',
                    'Almoxarifado'
            ]
        );
    }

    public static function getExcludedRoles()
    {
        return [
            'Administrador',
            'Administrador Órgão',
            'Administrador Unidade',
            'Administrador Suporte',
            'Consulta Global',
            'Acesso API'
        ];
    }
    

    /**
     * Retorna, segundo relacionamento, único registro da tabela Unidades
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @author Anderson Sathler <asathler@gmail.com>
     */
    public function unidade()
    {
        return $this->belongsTo(Unidade::class, 'ugprimaria');
    }

    /**
     * Retorna, segundo relacionamento, todos os registros em UnidadesUsers
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function unidades()
    {
        return $this->belongsToMany(Unidade::class, 'unidadesusers', 'user_id', 'unidade_id');
    }
    public function unidades2()
    {
        return $this->belongsToMany(Unidade::class, 'unidadesusers', 'user_id', 'unidade_id');
    }
    /**
     * Retorna, segundo relacionamento, único registro da tabela Unidades
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @todo Rever nomenclatura deste método. Se for o caso, prefira o método $this->unidade()
     */
    public function ugPrimariaRelation()
    {
        return $this->belongsTo(Unidade::class, 'ugprimaria');
    }

    public function contratoSfPadrao()
    {
        return $this->hasMany(Contratosfpadrao::class, 'user_id');
    }

    public function sistemaOrigem()
    {
        return $this->belongsTo(SistemaOrigem::class, 'id_sistema_origem');
    }

}
