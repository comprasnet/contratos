<?php


namespace App\Models;

use App\Http\Traits\Formatador;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Contratofatura extends Model
{
    use CrudTrait;
    use LogsActivity;
    use SoftDeletes;
    use Formatador;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'contratofaturas';
    protected static $logFillable = true;
    protected static $logName = 'contratofaturas';

    protected $fillable = [
        'contrato_id',
        'tipolistafatura_id',
        'tipo_de_instrumento_de_cobranca_id',
        'justificativafatura_id',
        'sfadrao_id',
        'arquivo_do_instrumento_de_cobranca',
        'numero',
        'emissao',
        'prazo',
        'vencimento',
        'valor',
        'juros',
        'multa',
        'glosa',
        'valorliquido',
        'processo',
        'protocolo',
        'ateste',
        'repactuacao',
        'infcomplementar',
        'mesref',
        'anoref',
        'situacao',
        'chave_nfe',
        'contratohistorico_id',
        'valorfaturado',
        'user_id',
        'isvalorfaturadocalculado',
        'valor_antigo',
        'serie'
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function getArrayComInstrumentoDeCobranca()
    {
        $array = Codigoitem::whereHas('codigo', function ($query) {
            $query->where('descricao', '=', 'Tipo de Instrumento de Cobrança');
        })
            ->orderByRaw("
                    CASE
                        WHEN descricao ILIKE 'Nota Fiscal' THEN 1
                        WHEN descricao ILIKE 'Nota Fiscal Eletrônica' THEN 2
                        WHEN descricao ILIKE 'Fatura' THEN 3
                        ELSE 4
                    END
            ")
            ->orderBy('descricao')
            ->pluck('descricao', 'id')
            ->toArray();
        return $array;
    }

    public function inserirContratoFaturaMigracaoConta(array $dados)
    {
        $this->fill($dados);
        $this->save();

        return $this;
    }

    /**
     * Retorna o órgão da fatura, exibindo código e nome do mesmo
     *
     * @return string
     */
    public function getOrgao()
    {
        $orgao = Orgao::whereHas('unidades', function ($query) {
            $query->where('id', '=', $this->contrato->unidade_id);
        })->first();

        return $orgao->codigo . ' - ' . $orgao->nome;
    }

    /**
     * Retorna a unidade da fatura, exibindo código e nome resumido do mesmo
     *
     * @return string
     */
    public function getUnidade()
    {
        $unidade = Unidade::find($this->contrato->unidade_id);

        return $unidade->codigo . ' - ' . $unidade->nomeresumido;
    }

    /**
     * Retorna o tipo da lista
     *
     * @return string
     */
    public function getTipoLista()
    {
        $tipolista = Tipolistafatura::find($this->tipolistafatura_id);

        return $tipolista->nome;
    }

    /**
     * Retorna o número do processo da fatura
     *
     * @return string
     */
    public function getProcesso()
    {
        return $this->processo;
    }

    /**
     * Retorna o número da fatura
     *
     * @return int
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Retorna a justificativa do processo
     *
     * @return string
     */
    public function getJustificativa()
    {
        if ($this->justificativafatura_id) {
            $justificativa = Justificativafatura::find($this->justificativafatura_id);
            return $justificativa->nome;
        }

        return '';
    }

    /**
     * Retorna o contrato da fatura
     *
     * @return string
     */
    public function getContrato()
    {
        return $this->contrato->numero ?? '';
    }

    /**
     * Retorna o Fornecedor do contrato, exibindo código e nome do mesmo
     *
     * @return string
     */
    public function getFornecedor()
    {
        $fornecedor = Fornecedor::find($this->contrato->fornecedor_id);

        return $fornecedor->cpf_cnpj_idgener . ' - ' . $fornecedor->nome;
    }


    /**
     * Retorna o Fornecedor do contrato, exibindo código
     *
     * @return string
     */
    public function getCodigoFornecedor()
    {
        $fornecedor = Fornecedor::find($this->contrato->fornecedor_id);

        return $fornecedor->cpf_cnpj_idgener;
    }

    /**
     * Retorna o Fornecedor do contrato, exibindo código
     *
     * @return string
     */
    public function getRazaoSocialComCodigoFornecedor()
    {
        $fornecedor = Fornecedor::find($this->contrato->fornecedor_id);

        return $fornecedor->cpf_cnpj_idgener . ' - ' . $fornecedor->nome;
    }

    /**
     * Retorna o Tipo de Lista
     *
     * @return string
     */
    public function getTipoListaFatura()
    {
        return $this->tipolista->nome;
    }

    /**
     * Retorna o órgão da fatura, exibindo código e nome do mesmo
     *
     * @return string
     */
    public function getJustificativaFatura()
    {
        if ($this->justificativafatura_id) {
            $justificativafatura = Justificativafatura::find($this->justificativafatura_id);
            return $justificativafatura->nome . ": " . $justificativafatura->descricao;
        } else {
            return '';
        }
    }

    public function getSfpadrao()
    {
        if ($this->sfpadrao_id) {
            $sfpadrao = SfPadrao::find($this->sfpadrao_id);
            return $sfpadrao->anodh . $sfpadrao->codtipodh . str_pad($sfpadrao->numdh, 6, "0", STR_PAD_LEFT);
        } else {
            return '';
        }
    }

    /**
     * Retorna o valor da fatura, formatado como moeda em pt-Br
     *
     * @return string
     */
    public function formatValor()
    {
        return $this->retornaCampoFormatadoComoNumero($this->valor, true);
    }

    /**
     * Retorna o valor total faturado da fatura
     *
     * @return string
     */
    public function retornarValorTotalFaturadoSemAcrescimoEDecrecimo()
    {
        return $this->contratofaturasitem->sum('valortotal_faturado');
    }

    /**
     * Retorna o valor dos itens, se for 0, retorna o valor da fatura
     *
     * @return string
     */
    public function retornarValorTotal()
    {
        $total = $this->retornarValorTotalFaturadoSemAcrescimoEDecrecimo();

        if($total != 0) {
            return $this->retornaCampoFormatadoComoNumero($total, true);
        }

        return $this->formatValor();
    }

    /**
     * Retorna o valor dos juros, formatado como moeda em pt-Br
     *
     * @return string
     */
    public function formatJuros()
    {
        return $this->retornaCampoFormatadoComoNumero($this->juros, true);
    }

    /**
     * Retorna o valor da multa, formatado como moeda em pt-Br
     *
     * @return string
     */
    public function formatMulta()
    {
        return $this->retornaCampoFormatadoComoNumero($this->multa, true);
    }

    /**
     * Retorna o valor da glosa, formatado como moeda em pt-Br
     *
     * @return string
     */
    public function formatGlosa()
    {
        $numeroFormatado = $this->retornaCampoFormatadoComoNumero($this->glosa);

        return "(R$ $numeroFormatado)";
    }

    public function formatValorFaturado()
    {
        return $this->retornaCampoFormatadoComoNumero($this->valorfaturado, true);
    }

    /**
     * Retorna o valor líquido, formatado como moeda em pt-Br
     *
     * @return string
     */
    public function formatValorLiquido()
    {
        return $this->retornaCampoFormatadoComoNumero($this->valorliquido, true);
    }

    /**
     * Retorna a situação, conforme array de situações
     *
     * @return string
     */
    public function retornaSituacao()
    {
        $situacoes = config('app.situacao_fatura');
        $situacao = isset($situacoes[$this->situacao]) ? $situacoes[$this->situacao] : '';

        return $situacao;
    }
    /**
     * Retorna a Data de Início da Vigência
     *
     * @return string
     */
    public function getVigenciaInicio()
    {
        return $this->retornaDataAPartirDeCampo($this->contrato->vigencia_inicio);
    }

    /**
     * Retorna a Data de Término da Vigência
     *
     * @return string
     */
    public function getVigenciaFim()
    {
        return $this->retornaDataAPartirDeCampo($this->contrato->vigencia_fim);
    }

    /**
     * Retorna o valor global, formatado como moeda em pt-Br
     *
     * @return string
     */
    public function getValorGlobal()
    {
        return $this->retornaCampoFormatadoComoNumero($this->contrato->valor_global);
    }

    /**
     * Retorna o valor da parcela, formatado como moeda em pt-Br
     *
     * @return string
     */
    public function getValorParcela()
    {
        return $this->retornaCampoFormatadoComoNumero($this->contrato->valor_parcela);
    }

    private function recuperarInformacoesEmpenhoFaturaAPI() {

        $retornoEmpenho = array(); //Previsto o retorno no formato Array
        $valor_empenho = null;

        if(count($this->empenhos) == 0) {
            $retornoEmpenho = null;
        } else {
            for($i=0; $i<count($this->empenhos); $i++) {

                #675
                $valor_empenho = ContratoFaturaEmpenho::where('contratofatura_id', $this->empenhos[$i]->pivot->contratofatura_id)
                    ->where('empenho_id', $this->empenhos[$i]->id)
                    ->select('valorref')
                    ->first();
                $subelemento = $this->buscarSubelementoDoEmpenho($this->empenhos[$i]->numero, $this->empenhos[$i]->unidade->codigo);

                $retornoEmpenho [$i]['id_empenho'] = $this->empenhos[$i]->id;
                $retornoEmpenho [$i]['numero_empenho'] = $this->empenhos[$i]->numero;
                $retornoEmpenho[$i]['valor_empenho'] = number_format($valor_empenho['valorref'], 2, ',', '.');
                $retornoEmpenho[$i]['subelemento'] = $subelemento;
            }
        }
        return $retornoEmpenho;
    }

    private function recuperarInformacoesRefernciaFaturaAPI() {

        $dadosReferencia_array = [];
        $dadosReferencia = ContratoFaturaMesAno::where("contratofaturas_id",$this->id)->select("mesref","anoref","valorref")->get();

        foreach ($dadosReferencia as $dados_model) {
            $dadosReferencia_array[] = $dados_model->contratoFaturaMesAnoAPI();
        }
        return empty($dadosReferencia_array) ? null : $dadosReferencia_array;

    }

    private function recuperarInformacoesItemFaturadoFaturaAPI() {

        $itemFaturado_array = [];
        $itemFaturado = ContratoFaturasItens::join("saldohistoricoitens","saldohistoricoitens.id","=","contratofaturas_itens.saldohistoricoitens_id")
                                             ->where("contratofaturas_id",$this->id)
                                             ->select(  "saldohistoricoitens.contratoitem_id AS id_item_contrato","quantidade_faturado",
                                                        "valorunitario_faturado","valortotal_faturado")
                                             ->get();

        foreach ($itemFaturado as $dados_model) {
            $itemFaturado_array[] = $dados_model->contratoFaturaItensAPI();
        }
        return empty($itemFaturado_array) ? null : $itemFaturado_array;

    }

    public function faturaAPI()
    {

        return [
                'id' => $this->id,
                'contrato_id' => $this->contrato_id,
                'tipolistafatura_id' => $this->tipolista->nome,
                'justificativafatura_id' => $this->getJustificativaFatura(),
                'sfadrao_id' => $this->getSfpadrao(),
                'numero' => $this->numero,
                'emissao' => $this->emissao,
                'prazo' => $this->prazo,
                'vencimento' => $this->vencimento,
                'valor' => number_format($this->valor, 2, ',', '.'),
                'juros' => number_format($this->juros, 2, ',', '.'),
                'multa' => number_format($this->multa, 2, ',', '.'),
                'glosa' => number_format($this->glosa, 2, ',', '.'),
                'valorliquido' => number_format($this->valorliquido, 2, ',', '.'),
                'processo' => $this->processo,
                'protocolo' => $this->protocolo,
                'ateste' => $this->ateste,
                'repactuacao' => $this->repactuacao == true ? 'Sim' : 'Não',
                'infcomplementar' => $this->infcomplementar,
                'mesref' => $this->mesref,
                'anoref' => $this->anoref,
                'situacao' => $this->retornaSituacao(),
        ];
    }

    public function faturaPorContratoAPI()
    {

        return [
                'id' => $this->id,
                'contrato_id' => $this->contrato_id,
                'tipolistafatura_id' => $this->tipolista->nome,
                'justificativafatura_id' => $this->getJustificativaFatura(),
                'sfadrao_id' => $this->getSfpadrao(),
                'numero' => $this->numero,
                'emissao' => $this->emissao,
                'prazo' => $this->prazo,
                'vencimento' => $this->vencimento,
                'valor' => number_format($this->valor, 2, ',', '.'),
                'juros' => number_format($this->juros, 2, ',', '.'),
                'multa' => number_format($this->multa, 2, ',', '.'),
                'glosa' => number_format($this->glosa, 2, ',', '.'),
                'valorliquido' => number_format($this->valorliquido, 2, ',', '.'),
                'processo' => $this->processo,
                'protocolo' => $this->protocolo,
                'ateste' => $this->ateste,
                'repactuacao' => $this->repactuacao == true ? 'Sim' : 'Não',
                'infcomplementar' => $this->infcomplementar,
                'mesref' => $this->mesref,
                'anoref' => $this->anoref,
                'situacao' => $this->retornaSituacao(),
                'chave_nfe' => $this->chave_nfe,
                'dados_empenho' => $this->recuperarInformacoesEmpenhoFaturaAPI(),
                'dados_referencia' => $this->recuperarInformacoesRefernciaFaturaAPI(),
                'dados_item_faturado' => $this->recuperarInformacoesItemFaturadoFaturaAPI()
        ];
    }

    private function retornarFiltroContratofaturasitem($d, $request) {
        if (isset($request['id_item_fatura'])) {
            $d->whereHas('contratofaturasitem', function ($e) use ($request) {
                $e->where('contratofaturas_id', "=", $request['id_item_fatura']);
            });
        }

        if (isset($request['quantidade_min_faturada_item_fatura']) && isset($request['quantidade_max_faturada_item_fatura'])) {
            $d->whereHas('contratofaturasitem', function ($e) use ($request) {
                $e->whereBetween('quantidade_faturado',[$request['quantidade_min_faturada_item_fatura'],$request['quantidade_max_faturada_item_fatura']]);
            });
        }

        if (isset($request['valor_unitario_min_faturada_item_fatura']) && isset($request['valor_unitario_max_faturada_item_fatura'])) {
            $d->whereHas('contratofaturasitem', function ($e) use ($request) {
                $e->whereBetween('valorunitario_faturado',[$request['valor_unitario_min_faturada_item_fatura'],$request['valor_unitario_max_faturada_item_fatura']]);
            });
        }

        if (isset($request['valor_total_min_faturada_item_fatura']) && isset($request['valor_total_max_faturada_item_fatura'])) {
            $d->whereHas('contratofaturasitem', function ($e) use ($request) {
                $e->whereBetween('valortotal_faturado',[$request['valor_total_min_faturada_item_fatura'],$request['valor_total_max_faturada_item_fatura']]);
            });
        }

        return $d;
    }

    private function retornarFiltroContratofaturasmesano($d, $request) {
        if (isset($request['mes_referencia_inicial']) && isset($request['mes_referencia_final']) && isset($request['ano_referencia'])) {
            $d->whereHas('contratofaturasmesano', function ($e) use ($request) {
                $e->whereBetween('mesref', [$request['mes_referencia_inicial'], $request['mes_referencia_final']])
                  ->where("anoref",$request['ano_referencia']);
            });
        }

        if (!isset($request['mes_referencia_inicial']) && !isset($request['mes_referencia_final']) && isset($request['ano_referencia'])) {
            $d->whereHas('contratofaturasmesano', function ($e) use ($request) {
                $e->where("anoref",$request['ano_referencia']);
            });
        }

        if (isset($request['valor_referencia_min']) && isset($request['valor_referencia_max'])) {

            $d->whereHas('contratofaturasmesano', function ($e) use ($request) {
                $e->whereBetween("valorref",[$request['valor_referencia_min'], $request['valor_referencia_max']]);
            });
        }

        return $d;
    }

    private function aplicarFiltrobuscaFaturasPorContratoId($d, array $request) {

        if (isset($request['chave_nfe'])) {
            $d->where("chave_nfe",$request['chave_nfe']);
        }

        if (isset($request['id_item_item_faturado'])) {
            $d->where("tipolistafatura_id",$request['id_item_item_faturado']);
        }

        if (isset($request['id_empenho'])) {
            $d->whereHas('empenhos', function ($e) use ($request) {
                $e->where('empenho_id', "=", $request['id_empenho']);
            });
        }

        if (isset($request['dt_alteracao_min']) && isset($request['dt_alteracao_max'])) {
            $d->whereBetween('contratofaturas.updated_at', [$request['dt_alteracao_min'], $request['dt_alteracao_max']]);
        }

        $d = $this->retornarFiltroContratofaturasitem($d,$request);
        $d = $this->retornarFiltroContratofaturasmesano($d,$request);

        return $d;
    }

    public function buscaFaturasPorContratoId(int $contrato_id, array $request)
    {

        $faturas = $this::whereHas('contrato', function ($c){
            $c->whereHas('unidade', function ($u){
                $u->where('sigilo', "=", false);
            });
        })
            ->where('contrato_id', $contrato_id)
            ->when($request != null, function ($d) use ($request) {
                $this->aplicarFiltrobuscaFaturasPorContratoId($d, $request);
                // $d->whereBetween('contratofaturas.updated_at', [$request[0], $request[1]]);
            })
            ->orderBy("vencimento", "DESC")
            ->get();

        return $faturas;
    }

    public function buscaFaturas($range)
    {
        $faturas = $this::whereHas('contrato', function ($c){
            $c->whereHas('unidade', function ($u){
                $u->where('sigilo', "=", false);
            });
        })
            ->when($range != null, function ($d) use ($range) {
                $d->whereBetween('contratofaturas.updated_at', [$range[0], $range[1]]);
            })
            ->get();

        return $faturas;
    }

    public function getAmparoLegal()
    {
        return ($this->contrato->amparoslegais->first()->ato_normativo) ?? '-';
    }

    #310
    public function geDownloadArquivo()
    {
        if(!empty($this->arquivo_do_instrumento_de_cobranca)) {

            return ('<a href="'  .url('storage/' . ($this->arquivo_do_instrumento_de_cobranca)) . '" target="_blank">Baixar</a>');
        }
        return '-';

    }

    public function countApropriacao()
    {
        return ApropriacaoContratoFaturas::withCount('contratofaturas_id')
            ->orderBy('contratofaturas_count', 'desc')->get();
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function apropriacoes()
    {
        return $this->belongsToMany(
            'App\Models\ApropriacaoFaturas',
            'apropriacoes_faturas_contratofaturas',
            'contratofaturas_id',
            'apropriacoes_faturas_id'
        );
    }

    public function apropriacaoContratoFaturasApropriacaoComId()
    {
        return $this->hasMany(ApropriacoesFaturasContratofaturas::class, 'contratofaturas_id', 'id');
    }

    public function apropriacaoContratoFaturasApropriacao()
    {
        return $this->apropriacaoContratoFaturasApropriacaoComId()
            ->select('apropriacoes_faturas_contratofaturas.*', 'apropriacoes_faturas_contratofaturas.apropriacoes_faturas_id');
    }

    public function contrato()
    {
        return $this->belongsTo(Contrato::class, 'contrato_id');
    }

    public function empenhos()
    {
        return $this->belongsToMany(Empenho::class, 'contratofatura_empenhos', 'contratofatura_id', 'empenho_id') ->distinct();
    }

    public function justificativa()
    {
        return $this->belongsTo(Justificativafatura::class, 'justificativafatura_id');
    }

    public function tipolista()
    {
        return $this->belongsTo(Tipolistafatura::class, 'tipolistafatura_id');
    }

    public function contratofaturasitem()
    {
        return $this->hasMany(ContratoFaturasItens::class, 'contratofaturas_id');
    }

    public function contratofaturasmesano()
    {
        return $this->hasMany(ContratoFaturaMesAno::class, 'contratofaturas_id');
    }

    public function codigoitem()
    {
        return $this->belongsTo(Codigoitem::class, 'tipo_de_instrumento_de_cobranca_id');
    }

    public function sfPadrao()
    {
        return $this->belongsTo(SfPadrao::class, 'sfpadrao_id');
    }

    public function contratoFaturaEmpenhos()
    {
        return $this->hasMany(ContratoFaturaEmpenho::class, 'contratofatura_id');
    }

    public function contratofaturaItens()
    {
        return $this->hasMany(ContratoFaturasItens::class, 'contratofaturas_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    public function getLeiAttribute()
    {
        $historico = $this->contrato->historico()->where('tipo_id',$this->contrato->tipo_id)->where('numero',$this->contrato->numero)->first();

        return ($historico->amparolegal->first()->ato_normativo) ?? '-';
    }

    /*Pela unidade do contrato verifica se é um contrato de uma unidade Gerenciadora ou Descentralizada */
    public function getContratoGerenciadoraAttribute()
    {

        $contratoGerenciadora = Contrato::where('id', $this->attributes['contrato_gerenciadora'])->first();
        $unidadeGerenciadora = Unidade::where('id', $contratoGerenciadora->unidade_id)->first();

        if($contratoGerenciadora->unidade_id === session('user_ug_id'))
        {
            return session('user_ug');
        }

        return $unidadeGerenciadora->codigo;
    }

    /*Pela unidade do contrato verifica se é um contrato de uma unidade Gerenciadora ou Descentralizada */
    public function getContratoDescAttribute()
    {
        if($this->attributes['contrato_desc'] === null)
        {
            return ' - ';
        }

        $contratoDesc = Contrato::where('id', $this->attributes['contrato_desc'])->first();

        if($contratoDesc->unidade_id !== session('user_ug_id'))
        {
            return session('user_ug');
        }
        return ' - ';
    }

    public function getIdParaEmpenhoAttribute()
    {
        $contratofaturas_id = $this->attributes['id_para_empenho'];

        $codigoNumeroEmpenho = ContratoFaturaEmpenho::selectRaw("CONCAT(unidades.codigo, ' - ', empenhos.numero) as codigo_numero_empenho")
            ->join('empenhos', 'empenhos.id', '=', 'contratofatura_empenhos.empenho_id')
            ->join('unidades', 'unidades.id', '=', 'empenhos.unidade_id')
            ->where('contratofatura_id', $contratofaturas_id)
            ->distinct()
            ->pluck('codigo_numero_empenho')
            ->implode(', ');

        return $codigoNumeroEmpenho ? $codigoNumeroEmpenho : ' - ';
    }

    public function getTipoDeInstrumentoDeCobrancaAttribute()
    {
         return ($this->codigoitem->descricao) ?? '-';
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function setArquivoDoInstrumentoDeCobrancaAttribute($value)
    {
        $date = date('Y/m');

        $attribute_name = "arquivo_do_instrumento_de_cobranca";
        $disk = "public";
        $destination_path = "contratofatura/" . $date;

        $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);

        // return $this->attributes[{$attribute_name}]; // uncomment if this is a translatable field
    }

    public function getButtonByUserId ($crud)
    {
        if (empty($this->user_id)) {
            $crud->allowAccess('update');
            $crud->allowAccess('delete');
            return null;
        } else {
            if ($this->user_id == backpack_user()->id) {
                $crud->allowAccess('update');
                $crud->allowAccess('delete');
                return null;
            }
            else {
                $crud->denyAccess('update');
                $crud->denyAccess('delete');
                return null;
            }
        }
    }

    public function buscarSubelementoDoEmpenho(string $numeroEmpenho, string $codUg)
    {
        $unidadeSfPadrao = Unidade::where('codigo', str_pad($codUg, 6, "0", STR_PAD_LEFT))
            ->orWhere('codigosiafi', str_pad($codUg, 6, "0", STR_PAD_LEFT))
            ->first();
        if(!$unidadeSfPadrao){
            return null;
        };

        $empenho = Empenho::where('numero', $numeroEmpenho)->where('unidade_id', $unidadeSfPadrao->id)->first();
        if(!$empenho->empenhodetalhado->toArray()){
            return null;
        }
        $naturezaSubItem_id = $empenho->empenhodetalhado->first()->naturezasubitem_id;

        return Naturezasubitem::find($naturezaSubItem_id)->codigo;
    }

}
