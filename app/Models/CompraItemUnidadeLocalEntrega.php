<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Spatie\Activitylog\Traits\LogsActivity;

class CompraItemUnidadeLocalEntrega extends Model
{
    use CrudTrait;
    use LogsActivity;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected static $logFillable = true;
    protected static $logName = 'compra_item_unidade_local_entrega';
    protected $table = 'compra_item_unidade_local_entrega';

    protected $fillable = [
        'compra_item_unidade_id',
        'endereco_id_novo_divulgacao',
        'endereco_id',
        'quantidade',
        'unidade_entrega_id'
    ];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function endereco(): BelongsTo
    {
        return $this->belongsTo(Enderecos::class, 'endereco_id');
    }
    public function compraItemUnidade(): BelongsTo
    {
        return $this->belongsTo(CompraItemUnidade::class, 'compra_item_unidade_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
