<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Orgao extends Model
{
    use CrudTrait;
    use LogsActivity;

    protected static $logFillable = true;
    protected static $logName = 'orgao';
    use SoftDeletes;

    protected $table = 'orgaos';

    protected $fillable = [
        'codigo',
        'orgaosuperior_id',
        'nome',
        'codigosiasg',
        'codigo_siorg',
        'situacao',
        'cnpj'
    ];

    public function getOrgaoSuperior()
    {
        $orgaosuperior = OrgaoSuperior::find($this->orgaosuperior_id);
        return $orgaosuperior->codigo . " - " . $orgaosuperior->nome;

    }
    public function serializaPNCP()
    {
        return [
            'id' => $this->id,
            'cnpj' => $this->cnpj,
            'razaoSocial' => $this->razaoSocial,
            'cnpjEnteResponsavel' => $this->cnpjEnteResponsavel,
            'poderId' => $this->poderId,
            'esferaId' => $this->esferaId,
            'validado' => $this->validado,
            'dataValidacao' => $this->dataValidacao,
            'dataInclusao' => $this->dataInclusao,
            'dataAtualizacao' => $this->dataAtualizacao,
        ];
    }
    public function orgaosuperior()
    {

        return $this->belongsTo(OrgaoSuperior::class, 'orgaosuperior_id');

    }

    public function unidades()
    {
        return $this->hasMany(Unidade::class, 'orgao_id');
    }

    public function configuracao()
    {
        return $this->hasOne(Orgaoconfiguracao::class, 'orgao_id');
    }

    public static function permissaoSei()
    {
        $unidade = Unidade::where('codigo', session()->get('user_ug'))->first();

        $orgaosPermitidos = [
            37000,//CGU
            37202,//INSS
        ];

        return in_array($unidade->orgao->codigo, $orgaosPermitidos);
    }

}
