<?php

namespace App\Models;

use App\Http\Traits\Formatador;
use Backpack\CRUD\CrudTrait;
use Spatie\Activitylog\Traits\LogsActivity;

class ContratoPublicacoes extends ContratoBase
{
    use CrudTrait;
    use LogsActivity;
    use Formatador;

    protected static $logFillable = true;
    protected static $logName = 'contratopublicacoes';

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'contratopublicacoes';
    protected $fillable = [
        'contratohistorico_id',
        'data_publicacao',
        'empenho',
        'hash',
        'link_publicacao',
        'log',
        'materia_id',
        'motivo_devolucao',
        'motivo_isencao_id',
        'oficio_id',
        'pagina_publicacao',
        'secao_jornal',
        'situacao',
        'status',
        'status_publicacao_id',
        'texto_dou',
        'texto_rtf',
        'tipo_pagamento_id',
        'transacao_id',
        'cpf'
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function retornaPublicacoesEnviadas()
    {

        $status_id = Codigoitem::whereHas('codigo', function ($query) {
            $query->where('descricao', '=', 'Situacao Publicacao');
        })
            ->where('descres', '=', '01')
            ->first()->id;

        return $this->whereNotNull('oficio_id')->where('status_publicacao_id', $status_id)->get();
    }

    public function getLinkPublicacao(){
        $linkPublicacao = $this->link_publicacao;
        return '<a href="'.$linkPublicacao.'" target="_blank">'.$linkPublicacao.'</a>';
    }

    public function getCodUnidade()
    {
        return $this->contratohistorico->unidade->codigo ?? '';
    }

    public function getCodSiorg()
    {
        return $this->contratohistorico->unidade->codigo_siorg ?? '';
    }

    public function publicacaoAPI()
    {
        return [
            'id' => $this->id,
            'contratohistorico_id' => $this->contratohistorico_id,
            'data_publicacao' => $this->data_publicacao,
            'status_publicacao_id' => @$this->getStatusPublicacaoDescresAttribute(),
            'status' => $this->status,
            'texto_dou' => $this->texto_dou,
            'link_publicacao' => $this->link_publicacao,
        ];
    }

    public function buscaPublicacoes($range)
    {
        $publicacoes = ContratoPublicacoes::whereHas('contratohistorico', function ($q) {
            $q->whereHas('unidade', function ($o) {
                $o->where('sigilo', false);
            });
        })
            ->whereBetween('updated_at', [$range[0], $range[1]])
            ->orderBy('id')
            ->get();

        return $publicacoes;
    }

    public function buscaPublicacoesPorContratoIdAPI(int $contrato_id, $range)
    {
        $publicacoes = ContratoPublicacoes::whereHas('contratohistorico', function ($q) use ($contrato_id) {
            $q->whereHas('unidade', function ($o) {
                $o->where('sigilo', false);
            })
            ->where('contrato_id', $contrato_id);
        })
            ->when($range != null, function ($d) use ($range) {
               $d->whereBetween('updated_at', [$range[0], $range[1]]);
             })
             ->get();

        return $publicacoes;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function contratohistorico()
    {
        return $this->belongsTo(Contratohistorico::class, 'contratohistorico_id');
    }

    public function status_publicacao()
    {
        return $this->belongsTo(Codigoitem::class,'status_publicacao_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    public function getStatusPublicacaoAttribute()
    {
        return $this->status_publicacao()->first()->descricao;
    }

    public function getStatusPublicacaoDescresAttribute()
    {
        return $this->status_publicacao()->first()->descres;
    }

    public function getTipoPublicacaoAttribute()
    {
        return $this->contratohistorico->tipo->descricao;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

}
