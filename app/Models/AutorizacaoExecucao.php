<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AutorizacaoExecucao extends Model
{
    use SoftDeletes;
    
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'autorizacaoexecucoes';
    protected $appends = ['valor_total'];

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    
    public function contrato()
    {
        return $this->belongsTo(Contrato::class, 'contrato_id');
    }
    
    public function tipo()
    {
        return $this->belongsTo(Codigoitem::class, 'tipo_id');
    }
    
    public function situacao()
    {
        return $this->belongsTo(Codigoitem::class, 'situacao_id');
    }
    
    public function autorizacaoExecucaoUnidadeRequisitantes()
    {
        return $this->hasMany(AutorizacaoExecucaoUnidadeRequisitantes::class, "autorizacaoexecucoes_id");
    }

    public function autorizacaoExecucaoItens()
    {
        return $this->hasMany(AutorizacaoExecucaoItens::class, "autorizacaoexecucoes_id");
    }

    public function autorizacaoExecucaoEmpenhos()
    {
        return $this->hasMany(AutorizacaoExecucaoEmpenhos::class, "autorizacaoexecucoes_id");
    }    

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    public function getValorTotalAttribute()
    {
        $total = AutorizacaoexecucaoItens::where('autorizacaoexecucoes_id', $this->id)
            ->selectRaw('SUM(quantidade * parcela * valor_unitario) as total')
            ->value('total');

        return $total;
    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */    

    public function getUnidadesRequisitantesByAttribute(array $attributesToFetch)
    {
         // Obtém a coleção de unidades requisitantes
        $unidadesRequisitantes = $this->autorizacaoExecucaoUnidadeRequisitantes;

        // Mapeia a coleção para um array de acordo com os parâmetros da função
        $unidadesRequisitantesArray = $unidadesRequisitantes->map(function ($item) use($attributesToFetch) {
            $result = [];
            foreach ($attributesToFetch as $chave => $valor) {
                $result[$valor] = $item->unidadeRequisitante->$chave;
            }
            return $result;
        });

        return $unidadesRequisitantesArray->all();
    }

    public function getEmpenhosByAttribute(array $attributesToFetch)
    {
        // Obtém a coleção de empenhos
        $empenhos = $this->autorizacaoExecucaoEmpenhos;

        // Mapeia a coleção para um array de acordo com os parâmetros da função
        $empenhosArray = $empenhos->map(function ($item) use($attributesToFetch) {
            $result = [];
            foreach ($attributesToFetch as $chave => $valor) {
                //Tratamento para atributos aninhados
                if (strpos($chave, '.') !== false) {
                    list($relation, $attribute) = explode('.', $chave);
                    $result[$valor] = $item->empenho->$relation->$attribute;
                } else {
                    $result[$valor] = $item->empenho->$chave;
                }
                //Formata campos monetários
                if (in_array($chave, ['empenhado', 'aliquidar', 'liquidado', 'pago'])) {
                    $result[$valor] = number_format($item->empenho->$chave, 2, ',', '.');
                }
            }
            return $result;
        });
        
        return $empenhosArray->all();
    }

    public function getItens()
    {
        // Obtém a coleção de itens
        $itens = $this->autorizacaoExecucaoItens;

        // Mapeia a coleção para um array
        $itensArray = $itens->map(function ($item) {
            return [
                'id' => $item->id,
                'periodo_vigencia_inicio' => $item->periodo_vigencia_inicio,
                'periodo_vigencia_fim' => $item->periodo_vigencia_fim,
                'tipo_item' => $item->saldoHistoricoItem->contratoItem->tipo->descres ?? null,
                'numero_item_compra' => $item->saldoHistoricoItem->numero_item_compra ?? null,
                'item' => $item->saldoHistoricoItem->contratoItem->item->descricao ?? null,
                'quantidade' => number_format($item->quantidade, 12, ',', '.'),
                'parcela' => $item->parcela,
                'valor_unitario' => number_format($item->valor_unitario, 2, ',', '.'),
                'subcontratacao' => ($item->subcontratacao ?? false) ? 'Sim' : 'Não',
            ];
        });

        return $itensArray->all();
    }

}
