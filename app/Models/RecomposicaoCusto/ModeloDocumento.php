<?php

namespace App\Models\RecomposicaoCusto;

use App\Models\Codigoitem;
use App\Models\ContratoMinuta;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class ModeloDocumento extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'modelo_documentos';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['descricao','codigoitens_id','template','ativo'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    function getTipoDocumento(){        
        return $this->CodigoItem()->first()->descricao;
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    function CodigoItem(){
        return $this->belongsTo(Codigoitem::class,'codigoitens_id');
    }

    function ContratoMinuta(){
        return $this->hasMany(ContratoMinuta::class,'tipo_modelo_documento_id');
    }

    
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    
}
