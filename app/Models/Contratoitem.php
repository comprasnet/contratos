<?php

namespace App\Models;

use App\Models\ContratoBase as Model;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Contratoitem extends Model
{
    use CrudTrait;
    use LogsActivity;
    use SoftDeletes;

    protected static $logFillable = true;
    protected static $logName = 'contratoitens';
    use SoftDeletes;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'contratoitens';
    protected $fillable = [
        'contrato_id',
        'tipo_id',
        'grupo_id',
        'catmatseritem_id',
        'descricao_complementar',
        'quantidade',
        'periodicidade',
        'data_inicio',
        'valorunitario',
        'valortotal',
        'numero_item_compra',
        'contratohistorico_id',
        'tipo_material'
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function atualizaSaldoContratoItem(Saldohistoricoitem $saldohistoricoitem)
    {
        $saldoitens = Saldohistoricoitem::where('contratoitem_id', $saldohistoricoitem->contratoitem_id)
            ->where('contratohistorico.elaboracao', false)
            ->where('saldohistoricoitens.elaboracao', false)
            ->orderBy('contratohistorico.data_assinatura', 'ASC')
            ->join('contratohistorico', 'contratohistorico.id', '=', 'saldohistoricoitens.saldoable_id')
            ->get();

        foreach ($saldoitens as $saldoitem) {
            $contratoitem = Contratoitem::find($saldoitem->contratoitem_id);
            $contratoitem->quantidade = $saldoitem->quantidade;
            $contratoitem->valorunitario = $saldoitem->valorunitario;
            $contratoitem->valortotal = $saldoitem->valortotal;
            $contratoitem->periodicidade = $saldoitem->periodicidade;
            $contratoitem->data_inicio = $saldoitem->data_inicio;
            $contratoitem->numero_item_compra = $saldoitem->numero_item_compra;
            $contratoitem->tipo_material = $saldoitem->tipo_material;
            $contratoitem->save();
        }
    }

    public function deletaContratoItem(Saldohistoricoitem $saldohistoricoitem)
    {
        $contratoitem = Contratoitem::find($saldohistoricoitem->contratoitem_id);
        if ($contratoitem) {
            $contratoitem->delete();
        }
    }

    public function getContrato()
    {
        if ($this->contrato_id) {
            $contrato = Contrato::find($this->contrato_id);
            return $contrato->numero;
        } else {
            return '';
        }
    }

    public function getTipo()
    {
        if ($this->tipo_id) {
            $tipo = Codigoitem::find($this->tipo_id);

            return $tipo->descricao;
        } else {
            return '';
        }
    }

    public function getCatmatseritem(bool $formatacaoSomenteTexto = false)
    {
        if ($this->catmatseritem_id) {
            if ($formatacaoSomenteTexto) {
                $catmatseritem_descricao = '';
                if ($this->catmatseritem_id) {
                    $catmatseritem = Catmatseritem::find($this->catmatseritem_id);
                    $catmatseritem_descricao = isset($catmatseritem) ? $catmatseritem->descricao : ' - ';
                }
                $textoCatmatseritem = $catmatseritem_descricao;
            } else {
                $idBotao = "botaoDescricaoCompleta_{$this->id}";
                $idTexto = "textoDescricaoCompleta_{$this->id}";

                $descricao = $this->descricao_complementar;
                $descricaoResumida = $this->item->descricao;

                if (empty($descricao) || $descricao == $descricaoResumida) {
                    $compra = Contrato::join('contratoitens', 'contratoitens.contrato_id', '=', 'contratos.id')
                        ->join('compras as comp', function ($join) {
                            $join->on('comp.numero_ano', '=', 'contratos.licitacao_numero')
                                ->on('comp.modalidade_id', '=', 'contratos.modalidade_id')
                                ->on('comp.unidade_origem_id', '=', 'contratos.unidadecompra_id');
                        })
                        ->join('compra_items as ci', function ($join) {
                            $join->on('ci.compra_id', '=', 'comp.id')
                                ->on('ci.numero', '=', 'contratoitens.numero_item_compra');
                        })
                        ->where('contratos.id', $this->contrato_id)
                        ->where('contratoitens.id', $this->id)
                        ->selectRaw('upper(descricaodetalhada) as descricaodetalhada')
                        ->distinct()
                        ->get()
                        ->first();

                    $descricao = !empty($compra->descricaodetalhada) ? $compra->descricaodetalhada : '' ;
                   // dump(str_replace_array('?', $compra->getBindings(), $compra->toSql()));

                }

                $descricaoResumida = strtoupper($descricaoResumida);
                $descricao = strtoupper($descricao);

                if(empty($descricao)){
                    $descricao = $this->descricao_complementar;
                    $descricaoResumida = mb_substr($this->item->descricao, 0,50);
                }
                $botaoResumido = "<button type='button' class='btn btn-default btn-xs' onclick='exibirDescricaoCompleta(`{$descricao}`,`{$descricaoResumida}`,`{$this->id}`)' >
                                    <span id='{$idBotao}' data-tipo='resumido' >
                                        <i class='fa fa-fw fa-caret-square-o-down'></i>
                                    </span>
                                </button>";
                $textoCatmatseritem = "{$this->item->codigo_siasg} - <span id='{$idTexto}'  style='white-space: normal' >{$descricaoResumida}</span> {$botaoResumido}";
            }

            return $textoCatmatseritem;
        } else {
            return '';
        }
    }

    public function getCatmatsergrupo()
    {
        if ($this->grupo_id) {
            $grupo = Catmatsergrupo::find($this->grupo_id);

            return $grupo->descricao;
        } else {
            return '';
        }
    }

    public function formatValorUnitarioItem()
    {
        return 'R$ ' . number_format($this->valorunitario, 2, ',', '.');
    }

    public function formatValorTotalItem()
    {
        return 'R$ ' . number_format($this->valortotal, 2, ',', '.');
    }

    public function itemAPI()
    {
        return [
                'id' => $this->id,
                'contrato_id' => $this->contrato_id,
                'tipo_id' => $this->getTipo(),
                'tipo_material' => $this->getTipo() == 'Serviço' ? "Não se aplica" : $this->tipo_material,
                'grupo_id' => $this->getCatmatsergrupo(),
                'catmatseritem_id' => $this->getCatmatseritem(true),
                'descricao_complementar' => $this->descricao_complementar,
                'quantidade' => $this->quantidade,
                'valorunitario' => number_format($this->valorunitario, 2, ',', '.'),
                'valortotal' => number_format($this->valortotal, 2, ',', '.'),
                'numero_item_compra' => $this->numero_item_compra,
                'data_inicio_item' => $this->created_at
        ];
    }

    private function aplicarFiltroItensPorContrato($d, array $queryString) {

        if ( isset($queryString['dt_alteracao_min']) && isset($queryString['dt_alteracao_max'])) {
            $d->whereBetween('contratoitens.updated_at', [$queryString['dt_alteracao_min'], $queryString['dt_alteracao_max']]);
        }

        if ( isset($queryString['dt_criacao_inicio_item']) && isset($queryString['dt_criacao_fim_item'])) {
            $d->whereBetween('contratoitens.created_at', [$queryString['dt_criacao_inicio_item'], $queryString['dt_criacao_fim_item']]);
        }

        if ( isset($queryString['numero_item_compra_inicial']) && isset($queryString['numero_item_compra_final'])) {

            if ( !strcasecmp($queryString['numero_item_compra_inicial'], 'null') && !strcasecmp($queryString['numero_item_compra_inicial'], 'null') ) {
                $d->whereNull('contratoitens.numero_item_compra');
            } else {
                $queryString['numero_item_compra_inicial'] = $this->montarNumeroItem($queryString['numero_item_compra_inicial']);
                $queryString['numero_item_compra_final'] = $this->montarNumeroItem($queryString['numero_item_compra_final']);

                $d->whereBetween('contratoitens.numero_item_compra', [$queryString['numero_item_compra_inicial'], $queryString['numero_item_compra_final']]);
            }

        }


        return $d;
    }

    private function montarMensagemSaidaAPI(int $codigoErro = 200, $mensagem = 'Sucesso') {
        return ['code' => $codigoErro, 'message' => $mensagem];
    }

    private function validarCampo($key,$tipo, array $queryString) {
        $saida = $this->montarMensagemSaidaAPI();
        switch($tipo) {
            case 'data':
                if ( isset($queryString[$key]) ) {
                    if(!$this->validarData($queryString[$key]) ) {
                        $saida =  $this->montarMensagemSaidaAPI(400,"O campo {$key} aceita somente valor tipo data, no padrão Y-m-d H:i:s");
                    }
                }
            break;
            case 'numero':
                if ( isset($queryString[$key]) ) {

                    if( intval($queryString[$key]) == 0  && !!strcasecmp($queryString[$key], 'null') ) {
                        $saida =  $this->montarMensagemSaidaAPI(400,"O campo {$key} aceita somente valor numérico");
                    }
                }
            break;
        }
        return $saida;
    }

    private function validarCampoBuscaItensPorContratoId (array $queryString) {


            $validacaoDtAlteracaoMin = $this->validarCampo('dt_alteracao_min','data', $queryString);
            $validacaoDtAlteracaoMax = $this->validarCampo('dt_alteracao_max','data', $queryString);

            $validacaoDtCriacaoInicioItem   = $this->validarCampo('dt_criacao_inicio_item','data', $queryString);
            $validacaoDtCriacaoFimItem      = $this->validarCampo('dt_criacao_fim_item','data', $queryString);

            $validacaoNumeroItemCompraInicial   = $this->validarCampo('numero_item_compra_inicial','numero', $queryString);
            $validacaoNumeroItemCompraFinal     = $this->validarCampo('numero_item_compra_final','numero', $queryString);

            if($validacaoDtAlteracaoMin['code'] != 200) {
                return $validacaoDtAlteracaoMin;
            }
            if($validacaoDtAlteracaoMax['code'] != 200) {
                return $validacaoDtAlteracaoMax;
            }

            if($validacaoDtCriacaoInicioItem['code'] != 200) {
                return $validacaoDtCriacaoInicioItem;
            }
            if($validacaoDtCriacaoFimItem['code'] != 200) {
                return $validacaoDtCriacaoFimItem;
            }

            if($validacaoNumeroItemCompraInicial['code'] != 200) {
                return $validacaoNumeroItemCompraInicial;
            }

            if($validacaoNumeroItemCompraFinal['code'] != 200) {
                return $validacaoNumeroItemCompraFinal;
            }

            return true;

    }

    public function buscaItensPorContratoId(int $contrato_id, array $queryString)
    {
        $validacao = $this->validarCampoBuscaItensPorContratoId($queryString);

        if(is_array($validacao)) {
            return $validacao;
        }

        $itens = $this::whereHas('contrato', function ($c){
            $c->whereHas('unidade', function ($u){
                $u->where('sigilo', "=", false);
            });
        })
            ->where('contrato_id', $contrato_id)
            ->when($queryString != null, function ($d) use ($queryString) {
                $this->aplicarFiltroItensPorContrato($d, $queryString);
                // $d->whereBetween('contratoitens.updated_at', [$queryString['dt_alteracao_min'], $queryString['dt_alteracao_max']]);

                // if(!empty($queryString['dt_inicio_item'])) {
                //     $d->where()
                // }
            })
            ->orderBy("contratoitens.numero_item_compra","DESC")
            ->get();

        return $itens;
    }

    public function buscaItens($range)
    {
        $itens = $this::whereHas('contrato', function ($c){
            $c->whereHas('unidade', function ($u){
                $u->where('sigilo', "=", false);
            });
        })
            ->when($range != null, function ($d) use ($range) {
                $d->whereBetween('contratoitens.updated_at', [$range[0], $range[1]]);
            })
            ->get();

        return $itens;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function contrato()
    {
        return $this->belongsTo(Contrato::class, 'contrato_id');
    }

    public function grupo()
    {
        return $this->belongsTo(Catmatsergrupo::class, 'grupo_id');
    }

    public function item()
    {
        return $this->belongsTo(Catmatseritem::class, 'catmatseritem_id');
    }

    public function servicos()
    {
        return $this->belongsToMany(Servico::class, 'contratoitem_servico', 'contratoitem_id', 'servico_id');
    }

    public function tipo()
    {
        return $this->belongsTo(Codigoitem::class, 'tipo_id');
    }

    public function saldoHistoricoItens()
    {
        return $this->morphOne(Saldohistoricoitem::class, 'saldoable');
    }

    public function contratofaturasItens()
    {
        return $this->morphOne(ContratoFaturasItens::class, 'itemable');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    public function getDescricaoGrupoAttribute($value)
    {
        return $this->grupo()->first()->descricao;
    }

    public function getDescricaoItemAttribute($value)
    {
        return $this->item()->first()->descricao;
    }

    public function getDescricaoTipoAttribute($value)
    {
        return $this->tipo()->first()->descricao;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
