<?php

namespace App\Models;

use App\Http\Traits\Formatador;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Spatie\Activitylog\Traits\LogsActivity;

class Contratoresponsavel extends ContratoBase
{
    use CrudTrait;
    use LogsActivity;
    use SoftDeletes;
    use Formatador;

    protected static $logFillable = true;
    protected static $logName = 'responsavel';

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'contratoresponsaveis';
    protected $fillable = [
        'contrato_id',
        'user_id',
        'funcao_id',
        'instalacao_id',
        'portaria',
        'situacao',
        'data_inicio',
        'data_fim',
        'telefone_fixo',
        'telefone_celular',
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function inserirContratoresponsavelMigracaoConta(array $dados)
    {
        $this->fill($dados);
        $this->save();

        return $this;
    }

    public function getContrato()
    {
        return $this->getContratoNumero();
    }

    public function getUser()
    {
        $usuarioCpf = $this->user->cpf;
        $usuarioNome = $this->user->name;

        return $this->retornaMascaraCpf($usuarioCpf). ' - ' . $usuarioNome;
    }

    public function getFuncao()
    {
        return $this->funcao->descricao;
    }

    public function getInstalacao()
    {
        return ($this->instalacao) ? $instalacao = $this->instalacao->nome : '';
    }

    /**
     * Retorna a Data de Início
     *
     * @return string
     * @author Anderson Sathler <asathler@gmail.com>
     */
    public function getDataInicio()
    {
        return $this->retornaDataAPartirDeCampo($this->data_inicio);
    }

    /**
     * Retorna a Data de Início
     *
     * @return string
     * @author Anderson Sathler <asathler@gmail.com>
     */
    public function getDataFim()
    {
        return $this->retornaDataAPartirDeCampo($this->data_fim);
    }

    public function responsavelAPI($usuarioTransparencia)
    {
        return [
            'id' => $this->id,
            'contrato_id' => $this->contrato_id,
            'usuario' => $usuarioTransparencia,
            'funcao_id' => $this->funcao->descricao,
            'instalacao_id' => $this->getInstalacao(),
            'portaria' => $this->portaria,
            'situacao' => $this->situacao == true ? 'Ativo' : 'Inativo',
            'data_inicio' => $this->data_inicio,
            'data_fim' => $this->data_fim
        ];
    }

    public function buscaResponsaveisPorContratoId(int $contrato_id, $range)
    {
        $responsaveis = $this::whereHas('contrato', function ($c){
            $c->whereHas('unidade', function ($u){
                $u->where('sigilo', "=", false);
            });
        })
            ->where('contrato_id', $contrato_id)
            ->when($range != null, function ($d) use ($range) {
                $d->whereBetween('contratoresponsaveis.updated_at', [$range[0], $range[1]]);
            })
            ->get();


        return $responsaveis;
    }

    public function buscaResponsaveis($range)
    {
        $responsaveis = $this::whereHas('contrato', function ($c){
            $c->whereHas('unidade', function ($u){
                $u->where('sigilo', "=", false);
            });
        })
            ->when($range != null, function ($d) use ($range) {
                $d->whereBetween('contratoresponsaveis.updated_at', [$range[0], $range[1]]);
            })
            ->get();

        return $responsaveis;
    }

    public function buscaContratoResponsaveisOrgao($idContrato, $idOrgao){
        $sql = "
                SELECT
                    c.id,
                    cr.id AS responsavel_id,
                    u.name,
                    u.cpf,
                    ci.descricao AS funcao,
                    i.nome AS instalacoes,
                    cr.portaria,
                    cr.situacao,
                    cr.data_inicio,
                    cr.data_fim
                FROM
                    contratos c
                INNER JOIN contratoresponsaveis cr ON
                    c.id = cr.contrato_id
                INNER JOIN users u ON
                    u.id = cr.user_id
                INNER JOIN codigoitens ci ON
                    ci.id = cr.funcao_id
                LEFT JOIN instalacoes i ON
                    i.id = cr.instalacao_id
                INNER JOIN unidades un ON
                    un.id = c.unidade_id --Unidade Gestora atual do Contrato.
                INNER JOIN orgaos o ON
                    o.id = un.orgao_id
                WHERE
                    c.id = $idContrato
                AND o.id IN (".implode(',', $idOrgao).")
                AND cr.deleted_at IS NULL
                ORDER BY
                    u.name ASC;
        ";

        $res = DB::select($sql);
        return $res;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function contrato()
    {
        return $this->belongsTo(Contrato::class, 'contrato_id');
    }

    public function funcao()
    {
        return $this->belongsTo(Codigoitem::class, 'funcao_id');
    }

    public function instalacao()
    {
        return $this->belongsTo(Instalacao::class, 'instalacao_id');
    }

    public function user()
    {
        return $this->belongsTo(BackpackUser::class, 'user_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    public function getMaskedCpfAttribute($value)
    {
        $retorno = '';
        if (isset($this->user()->first()->cpf)) {
            $retorno = $this->retornaMascaraCpf($this->user()->first()->cpf);
        }
        return $retorno;
    }

    public function getUsuarioNomeAttribute($value)
    {
        return $this->user()->first()->name ?? '';
    }

    public function getUsuarioEmailAttribute($value)
    {
        return $this->user()->first()->email ?? '';
    }

    public function getDescricaoTipoAttribute($value)
    {
        return $this->funcao()->first()->descricao ?? '';
    }

    public function getCpfUnmasked(){
        $cpf = $this->user()->first()->cpf;
        $output = preg_replace( '/[^0-9]/', '', $cpf );

        return $output;

    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

}
