<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Spatie\Activitylog\Traits\LogsActivity;

class Unidade extends Model
{
    use CrudTrait;
    use LogsActivity;
    use SoftDeletes;

    protected static $logFillable = true;
    protected static $logName = 'unidade';

    protected $table = 'unidades';

    protected $fillable = [
        'orgao_id',
        'codigo',
        'gestao',
        'codigosiasg',
        'nome',
        'nomeresumido',
        'telefone',
        'tipo',
        'situacao',
        'sisg',
        'municipio_id',
        'esfera',
        'poder',
        'tipo_adm',
        'aderiu_siasg',
        'utiliza_siafi',
        'codigo_siorg',
        'sigilo',
        'cnpj',
        'utiliza_custos',
        'codigosiafi',
        'exclusivo_gestao_atas',
        'utiliza_antecipagov',
        'despacho_autorizatorio',
    ];

    public function buscaUnidadeExecutoraPorCodigo($codigo)
    {
        $unidade = $this->where('codigo', $codigo)
            ->where('tipo', 'E')
            ->first();
        return $unidade->id;
    }

    public function getOrgao()
    {
        if ($this->orgao_id) {
            $orgao = Orgao::find($this->orgao_id);
            return $orgao->codigo . " - " . $orgao->nome;
        }

        return '';
    }

    public function getTipo()
    {

        if ($this->tipo == 'E') {
            $tipo = "Executora";
        }

        if ($this->tipo == 'C') {
            $tipo = "Controle";
        }

        if ($this->tipo == 'S') {
            $tipo = "Setorial Contábil";
        }

        return $tipo;
    }

    public function getMunicipio()
    {
        if (!$this->municipio_id)
            return '';
        return $this->municipio->nome;
    }

    public function getUF()
    {
        if (!$this->municipio_id)
            return '';
        return $this->municipio->estado->sigla;
    }

    public function serializaPNCP()
    {
        return [
            'codigoIBGE' => @$this->municipio->codigo_ibge,
            'codigoUnidade' => $this->codigo,
            'nomeUnidade' => $this->nome
        ];
    }

    /**
     * Método responsável em montar o body  para cadastrar a unidade no PNCP
     * @return array|string
     */
    public function montarBodyPNCP()
    {
        if (empty($this->municipio)) {
            return 'Unidade sem vínculo com o município';
        }

        return [
            'codigoIBGE' => $this->municipio->codigo_ibge,
            'codigoUnidade' => $this->codigo,
            'nomeUnidade' => $this->nome
        ];
    }

    public function buscaUnidadeGestoraMaeDaExecutora($id_unidade) {
        $sql = DB::table("unidadegestora_unidadesexecutorasfinanceiras")
            ->where("unidade_executora_financeira_id", "=", $id_unidade)
            ->join("unidades", "unidades.id", "=", "unidadegestora_unidadesexecutorasfinanceiras.unidade_gestora_id")
            ->first();

            return $sql;
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function compras()
    {
        return $this->hasMany(Siasgcompra::class, 'unidade_id');
    }

    public function configuracao()
    {
        return $this->hasOne(Unidadeconfiguracao::class, 'unidade_id');
    }

    public function contratos()
    {
        return $this->hasMany(Contrato::class, 'unidade_id');
    }

    public function empenhos()
    {
        return $this->hasMany(Empenho::class, 'unidade_id');
    }

    public function minuta_empenhos()
    {
        return $this->hasMany(MinutaEmpenho::class);
    }

    public function municipio()
    {
        return $this->belongsTo(Municipio::class, 'municipio_id');
    }

    public function orgao()
    {
        return $this->belongsTo(Orgao::class, 'orgao_id');
    }

    public function users()
    {
        return $this->belongsToMany(BackpackUser::class, 'unidadesusers', 'unidade_id', 'user_id');
    }

    public function user()
    {
        return $this->hasOne(BackpackUser::class,  'ugprimaria');
    }

    public function compraItem()
    {
        return $this->belongsToMany(
            'App\Models\Unidade',
            'compra_item_unidade',
            'compra_item_id',
            'unidade_id'
        );
    }

    //issue 499
    public function unidadesExecutorasFinanceiras()
    {
        return $this->belongstoMany(
            'App\Models\Unidade',
            'unidadegestora_unidadesexecutorasfinanceiras',
            'unidade_gestora_id',
            'unidade_executora_financeira_id'
        );
    }
    public function codigo_siafi()
    {
        return $this->belongsTo(
            Unidade::class,
            'codigosiafi',
            'codigo'
        );
    }

    //Busca Id da unidade do código siafi diferente de código siasg issues -578
    public function getIdUnidadeSiafiAttribute()
    {
        return $this->codigo_siafi->id;

        // $siafi = $this->codigo_siafi;

        // if(isset($siafi->id)){

        //     return $siafi->id;
        // }

        // return $this->id;
    }


}
