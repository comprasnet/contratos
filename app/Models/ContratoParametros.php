<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class ContratoParametros extends Model
{
    use CrudTrait;

    protected $table = 'contrato_parametros';

    protected $fillable = [
        'pz_recebimento_provisorio', //Prazo para recebimento provisório
        'pz_recebimento_definitivo', //Prazo para recebimento definitivo
        'pz_pagamento', //Prazo para pagamento
        'data_pagamento_recorrente', //Data de pagamento recorrente
        'registro', //Registro de OS/F
        'contrato_id',
        'tipo_recebimento_provisorio',
        'tipo_recebimento_definitivo',
        'tipo_pagamento',
        'tipo_pagamento_recorrente'
    ];

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function contrato()
    {
        return $this->belongsTo(Contrato::class, 'contrato_id', 'id');
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    public function getPzRecebimentoProvisorio()
    {
        return $this->attributes['pz_recebimento_provisorio'] ?
            $this->attributes['pz_recebimento_provisorio'] . ' dias ' . $this->attributes['tipo_recebimento_provisorio'] :
            'Não informado';
    }

    public function getPzRecebimentoDefinitivo()
    {
        return $this->attributes['pz_recebimento_definitivo'] ?
            $this->attributes['pz_recebimento_definitivo'] . ' dias ' . $this->attributes['tipo_recebimento_definitivo'] :
            'Não informado';
    }

    public function getPzPagamento()
    {
        return $this->attributes['pz_pagamento'] ?
            $this->attributes['pz_pagamento'] . ' dias ' . $this->attributes['tipo_pagamento'] :
            'Não informado';
    }

    public function getDataPagamentoRecorrente()
    {
        return $this->attributes['data_pagamento_recorrente'] ?
            $this->attributes['data_pagamento_recorrente'] . 
            ($this->attributes['tipo_pagamento_recorrente'] == 'Útil' ? ' ° dia Útil' : ' dia ' . $this->attributes['tipo_pagamento_recorrente']) :
            'Não informado';
    }
    
    public function getTipoPagamentoRecorrente()
    {
        return ($this->attributes['tipo_pagamento_recorrente'] == 'Útil') ? 'Úteis' : 'Corridos';
    }
}
