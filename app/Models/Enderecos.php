<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Spatie\Activitylog\Traits\LogsActivity;

class Enderecos extends Model
{
    use CrudTrait;
    use LogsActivity;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected static $logFillable = true;
    protected static $logName = 'enderecos';
    protected $table = 'enderecos';
    public $timestamps = true;

    protected $fillable = [
        'municipios_id',
        'cep',
        'logradouro',
        'numero',
        'complemento',
        'bairro',
        'codigo_municipio_ndc'
    ];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function municipio(): BelongsTo
    {
        return $this->belongsTo(Municipio::class, 'municipios_id');
    }

    public function localExecucao(): HasOne
    {
        return $this->hasOne(ContratoLocalExecucao::class, 'endereco_id', 'id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    public function getLocalidadeUfAttribute()
    {
        return $this->municipio->nome . '/' . $this->municipio->estado->sigla;
    }

    public function getDescricaoAttribute()
    {
        return $this->localExecucao->descricao;
    }
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
