<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Crypt;
use Spatie\Activitylog\Traits\LogsActivity;

class PncpUsuario extends Model
{
    use CrudTrait;
    use LogsActivity;
    use SoftDeletes;

    protected static $logFillable = true;
    protected static $logName = 'pncp_usuarios';
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'pncp_usuarios';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = [
        'id',
        'id_pncp',
        'cpfCnpj',
        'nome',
        'email',
        'administrador',
        'token',
        'senha',
        'ativo'
    ];

    public function serializaPNCP()
    {

        return [
            'id' => $this->id_pncp,
            'cpfCnpj' => $this->cpfCnpj,
            'nome' => $this->nome,
            'email' => $this->email,
            'administrador' => $this->administrador,
            'token' => $this->token,
            'senha' => $this->senha,
            'entesAutorizados' => []
        ];
    }


}
