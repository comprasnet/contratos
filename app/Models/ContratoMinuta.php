<?php

namespace App\Models;

use App\Models\RecomposicaoCusto\ModeloDocumento;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class ContratoMinuta extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'contrato_minutas';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['data','contrato_id','tipo_modelo_documento_id','contrato_minutas_status_id',
        'texto_minuta','codigoitens_id','documento_formatado','qtd_min_assinaturas','numero_termo_contrato',
        'pdf_gerado', 'numero_super_sei'];
    // protected $hidden = [];
    // protected $dates = [];
    protected $contrato_model;

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    function getNumeroContrato(){
        return $this->Contrato->numero;
    }

    function getNumeroProcesso()
    {
        return $this->Contrato->processo;
    }

    function getTipoDeDocumento()
    {

        if(empty($this->ModeloDocumento)) {
            return $this->TipoDocumento->descricao;
            // return 'Padrão de Documento Não Selecionado';
            return $this->CodigoItem->descricao;
        } else {
            return $this->ModeloDocumento->CodigoItem->descricao;
        }

        //
    }

    function getDescricaoModeloDocumento(){

        if(empty($this->ModeloDocumento)) {
            return 'Padrão de Documento Não Selecionado';
        } else {
            return $this->ModeloDocumento->descricao;
        }

    }

    function getStatus()
    {
        return $this->Status->descricao;
    }

    public function converteHTMLparaPDF(){
        try {

            $mpdf = new \Mpdf\Mpdf();

            $mpdf->WriteHTML($this->texto_minuta);
            $minuta = ContratoMinuta::find($this->id)->update(['pdf_gerado' => 1]);
            $mpdf->Output();
        } catch (\Mpdf\MpdfException $e) {
            echo $e->getMessage();
        }
    }

    public function getPdfGerado(){
        return $this->pdf_gerado;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    function ModeloDocumento(){
        return $this->belongsTo(ModeloDocumento::class,'tipo_modelo_documento_id');
    }
    function Contrato()
    {
        return $this->belongsTo(Contrato::class, 'contrato_id');
    }
    function Status()
    {
        return $this->belongsTo(Codigoitem::class, 'contrato_minutas_status_id');
    }

    function TipoDocumento() {
        return $this->belongsTo(Codigoitem::class,'codigoitens_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
