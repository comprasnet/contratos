<?php

namespace App\Models;

use App\Repositories\MinutaRepository;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class ContratoFaturaEmpenhosApropriacao extends Model
{
    use CrudTrait;

    protected $table = 'contrato_fatura_empenhos_apropriacao';
    protected $fillable = [
        'contratofatura_id',
        'empenho_id',
        'valorref',
        'apropriado_siafi',
        'sfpcoitem_id',
        'subelemento_id',
    ];

    public $timestamps = false;

    public function contratoFaturas()
    {
        return $this->belongsTo(Contratofatura::class, 'contratofatura_id');
    }

    public function empenho()
    {
        return $this->belongsTo(Empenho::class, 'empenho_id');
    }

    public function getCelulaOrcamentaria($empenho, $fornecedor_id, $unidade_id)
    {
        $fornecedor = Fornecedor::findOrFail($fornecedor_id);
        $unidade = Unidade::findOrFail($unidade_id);

        if ($fornecedor && $unidade) {
            $conta_corrente = new MinutaRepository();
            $contaCorrenteEmpenho = substr($conta_corrente->getMinutaByEmpenho($empenho, $fornecedor, $unidade)->conta_corrente ?? '', 0, 1);

            if ($contaCorrenteEmpenho) {
                switch ($contaCorrenteEmpenho) {
                    case 1:
                        $celulaOrcamento = 'Orçamento Fiscal';
                        break;
                    case 2:
                        $celulaOrcamento = 'Orçamento de Seguridade Social';
                        break;
                    case 3:
                        $celulaOrcamento = 'Orçamento de Investimento das Empresas não Dependentes';
                        break;
                    case 4:
                        $celulaOrcamento = 'Orçamento Próprio das Empresas não Dependentes';
                        break;
                }
            }
        }

        return $celulaOrcamento ?? '';
    }
}
