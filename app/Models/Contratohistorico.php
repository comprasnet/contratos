<?php

namespace App\Models;


// use App\Models\Autoridadesignataria;
// use App\Models\ContratoAutoridadeSignataria;

use App\Http\Traits\EnceramentoContratoTrait;
use App\Http\Traits\Formatador;
use Backpack\CRUD\CrudTrait;
use App\Http\Traits\LogTrait;
use Exception;
use Illuminate\Database\Eloquent\Collection;
// use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Http\Traits\NumeroAnoTrait;

class Contratohistorico extends ContratoBase
{
    use CrudTrait;
    use LogsActivity;
    // use SoftDeletes;
    use Formatador;
    use EnceramentoContratoTrait;
    use NumeroAnoTrait;
    use LogTrait;

    protected static $logFillable = true;
    protected static $logName = 'contrato_historico';

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'contratohistorico';
    protected $fillable = [
        'numero',
        'contrato_id',
        'fornecedor_id',
        'unidade_id',
        'unidadeorigem_id',
        'tipo_id',
        'categoria_id',
        'subcategoria_id',
        'receita_despesa',
        'processo',
        'objeto',
        'info_complementar',
        'fundamento_legal',
        'amparo_legal_id',
        'modalidade_id',
        'licitacao_numero',
        'data_assinatura',
        'data_publicacao',
        'data_proposta_comercial',
        'vigencia_inicio',
        'vigencia_fim',
        'valor_inicial',
        'valor_global',
        'num_parcelas',
        'valor_parcela',
        'novo_valor_global',
        'novo_num_parcelas',
        'novo_valor_parcela',
        'data_inicio_novo_valor',
        'observacao',
        'retroativo',
        'retroativo_mesref_de',
        'retroativo_anoref_de',
        'retroativo_mesref_ate',
        'retroativo_anoref_ate',
        'retroativo_vencimento',
        'retroativo_valor',
        'retroativo_soma_subtrai',
        'unidades_requisitantes',
        'situacao',
        'supressao',
        'unidadecompra_id',
        'publicado',
        'subtipo',
        'data_fim_novo_valor',
        'prorrogavel',
        'is_com_valor',
        'unidadebeneficiaria_id',
        'is_prazo_indefinido',
        'cronograma_automatico',
        'elaboracao',
        'codigo_sistema_externo',
        'aplicavel_decreto'
    ];
    protected $casts = [
        'prorrogavel' => 'integer',
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function formataDataParaPadraoBrasileiro($dataPadraoAmericano)
    {
        /**
         * Vamos pegar a data recebida, quebrá-la em aaaa, mm, dd
         * e fazer a nova formatação
         */
        $dia = substr($dataPadraoAmericano, 8, 2);
        $mes = substr($dataPadraoAmericano, 5, 2);
        $ano = substr($dataPadraoAmericano, 0, 4);
        return $dia . "/" . $mes . "/" . $ano;
    }

    public function getDataEncerramento()
    {
        $contrato_id = $this->contrato_id;
        $dataEncerramento = self::formataDataParaPadraoBrasileiro(Contrato::find($contrato_id)->data_encerramento);
        return $dataEncerramento;
    }

    public function getConsecucaoObjetivosContratacao()
    {
        return $this->contrato->consecucao_objetivos_contratacao;
    }

    public function atualizarContratoDelecaoTermoDeEncerramento()
    {
        // aqui quer dizer que um termo de encerramento foi deletado. Vamos atualizar o contrato
        $idContrato = $this->contrato_id;
        $objContrato = Contrato::find($idContrato);
        $objContrato->unsetEventDispatcher();// Retirando o evento automatico da observer de contrato
        $objContrato->justificativa_contrato_inativo_id = null;
        $objContrato->situacao = true;
        $objContrato->data_encerramento = null;
        $objContrato->hora_encerramento = null;
        $objContrato->user_id_responsavel_encerramento = null;
        $objContrato->nome_responsavel_encerramento = null;
        $objContrato->cpf_responsavel_encerramento = null;
        $objContrato->consecucao_objetivos_contratacao = null;
        $objContrato->user_id_responsavel_signatario_encerramento = null;
        $objContrato->nome_responsavel_signatario_encerramento = null;
        $objContrato->cpf_responsavel_signatario_encerramento = null;
        $objContrato->grau_satisfacao_desempenho_contrato = null;
        $objContrato->planejamento_contratacao_atendida = false;
        $objContrato->sugestao_licao_aprendida = null;
        $objContrato->is_saldo_conta_vinculada_liberado = null;
        $objContrato->is_garantia_contratual_devolvida = null;
        $objContrato->is_objeto_contratual_entregue = false;
        $objContrato->is_cumpridas_obrigacoes_financeiras = false;
        $objContrato->te_valor_total_executado = null;
        $objContrato->te_valor_total_pago = null;
        $objContrato->te_saldo_disponivel_ou_bloqueado = null;
        $objContrato->te_justificativa_nao_cumprimento = null;
        $objContrato->te_saldo_bloqueado_garantia_contratual = null;
        $objContrato->te_justificativa_bloqueio_garantia_contratual = null;
        $objContrato->te_saldo_bloqueado_conta_deposito = null;
        $objContrato->te_justificativa_bloqueio_conta_deposito = null;
        if ($objContrato->save()) {
            $objContrato->setEventDispatcher(app('events')); // Voltando o evento automatico da observer de contrato
            return true;
        }
        return false;
    }

    #907
    public function atualizarContratoDelecaoTermoDeRescisao()
    {
        // aqui quer dizer que um termo de rescisão foi deletado. Vamos atualizar o contrato
        $idContrato = $this->contrato_id;
        $objContrato = Contrato::find($idContrato);
        $objContrato->unsetEventDispatcher();// Retirando o evento automatico da observer de contrato
        $objContrato->justificativa_contrato_inativo_id = null;
        if ($objContrato->save()) {
            $objContrato->setEventDispatcher(app('events')); // Voltando o evento automatico da observer de contrato
            return true;
        }
        return false;
    }

    public function inserirContratohistoricoMigracaoConta(array $dados)
    {
        $this->fill($dados);
        $this->save();

        return $this;
    }

    public function createNewHistorico(array $dado)
    {
        $contratohistorico = $this->where('numero', '=', $dado['numero'])
            ->where('contrato_id', '=', $dado['contrato_id'])
            ->where('tipo_id', '=', $dado['tipo_id'])
            ->first();

        if (!$contratohistorico) {
            $this->fill($dado);
            $this->save();
            return $this;
        }

        $contratohistorico->fill($dado);
        $contratohistorico->save();

        return $contratohistorico;
    }

    public function getReceitaDespesaHistorico()
    {
        $retorno['D'] = 'Despesa';
        $retorno['R'] = 'Receita';
        $retorno['S'] = 'Sem ônus';
        $retorno['X'] = 'Receita e Despesa';
        $retorno[''] = '';

        return $retorno[$this->receita_despesa];
    }

    public function getTipo()
    {
        return $this->tipo->descricao;
    }

    public function getCategoria()
    {
        return isset($this->categoria->descricao) ? $this->categoria->descricao : '';
    }

    public function getSubCategoria()
    {
        return isset($this->orgaosubcategoria) ? $this->orgaosubcategoria->descricao : '';
    }

    public function getFornecedorHistorico()
    {
        $fornecedorCpfCnpj = $this->fornecedor->cpf_cnpj_idgener;
        $fornecedorNome = $this->fornecedor->nome;

        return $fornecedorCpfCnpj . ' - ' . $fornecedorNome;
    }

    public function getModalidade()
    {
        return isset($this->modalidade) ? $this->modalidade->descricao : '';
    }

    public function formatVlrGlobalHistorico()
    {
        return $this->retornaCampoFormatadoComoNumero($this->valor_global, true);
    }

    public function formatVlrParcelaHistorico()
    {
        return $this->retornaCampoFormatadoComoNumero($this->valor_parcela, true);
    }

    // NOTA: Demais formatadores não estavam presentes, numa revisão preliminar, na ContratohistoricoCrudController,
    //       contudo, foram mantidas pela eventual manutenção de retrocompatibilidade.
    //
    // Métodos com alterações
    public function formatNovoVlrGlobalHistorico()
    {
        return $this->retornaCampoFormatadoComoNumero($this->novo_valor_global, true);
    }

    public function formatNovoVlrParcelaHistorico()
    {
        return $this->retornaCampoFormatadoComoNumero($this->novo_valor_parcela, true);
    }

    public function formatVlrRetroativoValor()
    {
        return $this->retornaCampoFormatadoComoNumero($this->retroativo_valor, true);

        /*
        if ($this->retroativo_valor) {
            return 'R$ ' . number_format($this->retroativo_valor, 2, ',', '.');
        }
        return '';
        */
    }

    public function formatVlrGlobal()
    {
        return $this->retornaCampoFormatadoComoNumero($this->valor_global, true);
    }

    public function formatVlrParcela()
    {
        return $this->retornaCampoFormatadoComoNumero($this->valor_parcela, true);
    }

    //
    // Métodos sem NENHUMA alteração!
    public function getUnidadeHistorico()
    {
        $unidade = Unidade::find($this->unidade_id);

        return $unidade->codigo . ' - ' . $unidade->nomeresumido;
    }

    public function getUnidadeOrigemHistorico()
    {
        if (!isset($this->unidadeorigem_id)) {
            return '';
        }

        return $this->unidadeorigem->codigo . ' - ' . $this->unidadeorigem->nomeresumido;
    }

    public function getOrgaoHistorico()
    {
        $orgao = Orgao::whereHas('unidades', function ($query) {
            $query->where('id', '=', $this->unidade_id);
        })->first();

        return $orgao->codigo . ' - ' . $orgao->nome;
    }

    public function getTipoHistorico()
    {
        if ($this->tipo_id) {
            $tipo = Codigoitem::find($this->tipo_id);

            return $tipo->descricao;
        } else {
            return '';
        }
    }

    public function getCategoriaHistorico()
    {
        if (!$this->categoria_id) {
            return '';
        }

        $categoria = Codigoitem::find($this->categoria_id);

        return $categoria->descricao;
    }

    public function getSubCategoriaHistorico()
    {
        if ($this->subcategoria_id) {
            $subcategoria = OrgaoSubcategoria::find($this->subcategoria_id);
            return $subcategoria->descricao;
        } else {
            return '';
        }
    }

    public function getRetroativoMesAnoReferenciaDe()
    {
        if ($this->retroativo_mesref_de and $this->retroativo_anoref_de) {
            return $this->retroativo_mesref_de . '/' . $this->retroativo_anoref_de;
        }

        return '';
    }

    public function getRetroativoMesAnoReferenciaAte()
    {
        if ($this->retroativo_mesref_ate and $this->retroativo_anoref_ate) {
            return $this->retroativo_mesref_ate . '/' . $this->retroativo_anoref_ate;
        }

        return '';
    }

    public function getFornecedor()
    {
        $fornecedor = Fornecedor::find($this->fornecedor_id);

        return $fornecedor->cpf_cnpj_idgener . ' - ' . $fornecedor->nome;
    }

    public function getReceitaDespesa()
    {
        switch ($this->receita_despesa) {
            case 'D':
                return 'Despesa';
            case 'R':
                return 'Receita';
            case 'S':
                return 'Sem ônus';
            case 'X':
                return 'Receita e Despesa';
            default:
                return '';
        }
    }

    public function getUnidade()
    {
        $unidade = Unidade::find($this->unidade_id);

        return @$unidade->codigo;
    }

    public function getUnidadeOrigem()
    {
        if (!isset($this->unidadeorigem_id)) {
            return '';
        }

        return $this->unidadeorigem->codigo;
    }

    public function getUnidadeCodigo()
    {
        $unidade = Unidade::find($this->unidade_id);

        return @$unidade->codigo;
    }

    public function getUnidadeOrigemCodigo()
    {
        if (!isset($this->unidadeorigem_id)) {
            return '';
        }

        return $this->unidadeorigem->codigo;
    }

    public function getOrgao()
    {
        $orgao = Orgao::whereHas('unidades', function ($query) {
            $query->where('id', '=', $this->unidade_id);
        })->first();

        return $orgao->codigo . ' - ' . $orgao->nome;
    }

    #140 retorna string 'indeterminado se o contrato for prazo indeterminado
    public function getVigenciaFimPrazoIndeterminado()
    {
        if ($this->is_prazo_indefinido == '1' || $this->is_prazo_indefinido == true) {
            return 'Indeterminado';
        }
        $vigenciafim = date_create($this->vigencia_fim);
        return date_formaT($vigenciafim, 'd/m/Y');

    }

    #140 retorna string 'indeterminado se o contrato for prazo indeterminado'
    public function getNumeroParcelasPrazoIndeterminado()
    {
        if ($this->is_prazo_indefinido == '1' || $this->is_prazo_indefinido == true) {
            return 'Indeterminado';
        }
        return $this->num_parcelas;

    }

    public function retornaAmparo()
    {
        $amparo = "";

        $cont = (is_array($this->amparolegal)) ? count($this->amparolegal) : 0;

        foreach ($this->amparolegal as $key => $value) {

            if ($cont < 2) {
                $amparo .= $value->ato_normativo;
                $amparo .= (!is_null($value->artigo)) ? " - Artigo: " . $value->artigo : "";
                $amparo .= (!is_null($value->paragrafo)) ? " - Parágrafo: " . $value->paragrafo : "";
                $amparo .= (!is_null($value->inciso)) ? " - Inciso: " . $value->inciso : "";
                $amparo .= (!is_null($value->alinea)) ? " - Alinea: " . $value->alinea : "";
            }
            if ($key == 0 && $cont > 1) {
                $amparo .= $value->ato_normativo;
                $amparo .= (!is_null($value->artigo)) ? " - Artigo: " . $value->artigo : "";
                $amparo .= (!is_null($value->paragrafo)) ? " - Parágrafo: " . $value->paragrafo : "";
                $amparo .= (!is_null($value->inciso)) ? " - Inciso: " . $value->inciso : "";
                $amparo .= (!is_null($value->alinea)) ? " - Alinea: " . $value->alinea : "";
            }
            if ($key > 0 && $key < ($cont - 1)) {
                $amparo .= ", " . $value->ato_normativo;
                $amparo .= (!is_null($value->artigo)) ? " - Artigo: " . $value->artigo : "";
                $amparo .= (!is_null($value->paragrafo)) ? " - Parágrafo: " . $value->paragrafo : "";
                $amparo .= (!is_null($value->inciso)) ? " - Inciso: " . $value->inciso : "";
                $amparo .= (!is_null($value->alinea)) ? " - Alinea: " . $value->alinea : "";
            }
            if ($key == ($cont - 1)) {
                $amparo .= " e " . $value->ato_normativo;
                $amparo .= (!is_null($value->artigo)) ? " - Artigo: " . $value->artigo : "";
                $amparo .= (!is_null($value->paragrafo)) ? " - Parágrafo: " . $value->paragrafo : "";
                $amparo .= (!is_null($value->inciso)) ? " - Inciso: " . $value->inciso : "";
                $amparo .= (!is_null($value->alinea)) ? " - Alinea: " . $value->alinea : "";
            }
        }

        return $amparo;
    }

    /**
     * Ao gravar o contrato gravar o amparo legal para contratoHistorico na tabela pivot
     *
     * @param $request
     * @param $contrato_id
     */

    public function vincularAmparoLegalContratoHistorico($request)
    {
        if (isset($request['amparoslegais'])) {
            // vincula os empenhos ao contrato historico
            foreach ($request['amparoslegais'] as $amparoLegalId) {
                $this->amparolegal()->attach($amparoLegalId);
            }
            return;
        }

        //caso não tenha amparos legais no request, buscar no contrato
        $amparos = $this->contrato->amparoslegais;
        if (!($amparos->isEmpty())) {
            $this->amparolegal()->attach($amparos);
        }
    }

    public function possuiAmparo($amparo)
    {

        if (@$this->amparolegal instanceof Collection) {
            foreach ($this->amparolegal as $key => $value) {
                if ($value->ato_normativo == $amparo) {
                    return true;
                }
            }
        }
        return false;
    }

    public function historicoAPI()
    {

        return [
            'id' => $this->id,
            'contrato_id' => $this->contrato_id,
            'receita_despesa' => ($this->receita_despesa) == 'D' ? 'Despesa' : 'Receita',
            'numero' => $this->numero,
            'observacao' => $this->observacao,
            'ug' => @$this->unidade->codigo,
            'gestao' => @$this->unidade->gestao,
            'fornecedor' => [
                'tipo' => @$this->fornecedor->tipo_fornecedor,
                'cnpj_cpf_idgener' => @$this->fornecedor->cpf_cnpj_idgener,
                'nome' => @$this->fornecedor->nome,
            ],
            'codigo_tipo' => $this->tipo->descres ?? '',
            'tipo' => $this->tipo->descricao ?? '',
            'categoria' => $this->categoria->descricao ?? '',
            'qualificacao_termo' => $this->getQualificacaoHistoricoContrato(),
            'processo' => $this->processo,
            'objeto' => $this->objeto,
            'fundamento_legal_aditivo' => @$this->fundamento_legal,
            'informacao_complementar' => $this->info_complementar,
            'modalidade' => $this->modalidade->descricao ?? '',
            'licitacao_numero' => $this->licitacao_numero,
            'codigo_unidade_origem' => @$this->unidadeorigem->codigo,
            'nome_unidade_origem' => @$this->unidadeorigem->nome,
            'data_assinatura' => $this->data_assinatura,
            'data_publicacao' => $this->data_publicacao,
            'data_proposta_comercial' => $this->data_proposta_comercial,
            'vigencia_inicio' => $this->vigencia_inicio,
            'vigencia_fim' => $this->vigencia_fim,
            'valor_inicial' => number_format($this->valor_inicial, 2, ',', '.'),
            'valor_global' => number_format($this->valor_global, 2, ',', '.'),
            'num_parcelas' => $this->num_parcelas,
            'valor_parcela' => number_format($this->valor_parcela, 2, ',', '.'),
            'novo_valor_global' => number_format($this->novo_valor_global, 2, ',', '.'),
            'novo_num_parcelas' => $this->novo_num_parcelas,
            'novo_valor_parcela' => number_format($this->novo_valor_parcela, 2, ',', '.'),
            'data_inicio_novo_valor' => $this->data_inicio_novo_valor,
            'retroativo' => ($this->retroativo) == true ? 'Sim' : 'Não',
            'retroativo_mesref_de' => $this->retroativo_mesref_de,
            'retroativo_anoref_de' => $this->retroativo_anoref_de,
            'retroativo_mesref_ate' => $this->retroativo_mesref_ate,
            'retroativo_anoref_ate' => $this->retroativo_anoref_ate,
            'retroativo_vencimento' => $this->retroativo_vencimento,
            'retroativo_valor' => number_format($this->retroativo_valor, 2, ',', '.'),
        ];
    }

    private function getQualificacaoHistoricoContrato()
    {

        //Para um mesmo Termo, pode haver uma ou mais qualificações.
        $array = $this->qualificacoes->all();

        if (sizeof($array) == 0) {
            return null;
        } else {
            $array = $this->qualificacoes->map(function ($item) {
                return [
                    'codigo' => (int)$item->descres,
                    'descricao' => $item->descricao
                ];
            });
            $array = $array->unique()->values()->all();
            return $array;
        }
    }

    public function buscaHistoricoPorContratoId(int $contrato_id, $range)
    {
        $historico = $this::whereHas('contrato', function ($c) {
            $c->whereHas('unidade', function ($u) {
                $u->where('sigilo', "=", false);
            });
        })
            ->where('contrato_id', $contrato_id)
            ->when($range != null, function ($d) use ($range) {
                $d->whereBetween('contratohistorico.updated_at', [$range[0], $range[1]]);
            })
            ->where('contratohistorico.elaboracao', false)
            ->orderBy('contratohistorico.data_assinatura')
            ->get();

        return $historico;
    }

    public function buscaHistoricos($range)
    {
        $historico = $this::whereHas('contrato', function ($c) {
            $c->whereHas('unidade', function ($u) {
                $u->where('sigilo', "=", false);
            });
        })
            ->when($range != null, function ($d) use ($range) {
                $d->whereBetween('updated_at', [$range[0], $range[1]]);
            })
            ->where('contratohistorico.elaboracao', false)
            ->orderBy('data_assinatura')
            ->get();

        return $historico;
    }

    public function serializaPNCP($cnpjCompra, $sequencialCompra, $alteracao, Contratohistorico $contratoHistoricoMinutaEmpenho = null)
    {
        $receita = false;
        $idContrato = $this->contrato_id;
        $objContrato = Contrato::find($idContrato);

        if (is_null($objContrato->minutasempenho->get('numero_cipi'))) {
            $numero_cipi = null;
        } else {
            $numero_cipi = $objContrato->minutasempenho->get('numero_cipi');
        }

        if (
            $this->getReceitaDespesa() == 'Receita e Despesa' ||
            $this->getReceitaDespesa() == 'Receita'
        ) {
            $receita = true;
        }

        if ($this->tipo->descricao == 'Empenho') {
            $anoContrato = explode('NE', $this->numero)[0];
            $numeroContratoEmpenho = $this->numero;
            $this->info_complementar = empty($contratoHistoricoMinutaEmpenho) ? null : $contratoHistoricoMinutaEmpenho->informacaoComplementar;
            @$this->unidade->cnpj = null;
        } else {
            $numeroContratoEmpenho = explode('/', $this->numero)[0];
            $anoContrato = explode('/', $this->numero)[1];
        }


        $unidadeSubRogada = $this->unidade->codigo != $this->unidadeorigem->codigo ? $this->unidade->codigo : null;
        $cnpjSubRogado = null;
        if (!is_null($unidadeSubRogada)) {
            $cnpjSubRogado = $this->unidade->orgao->cnpj;
        }

        $campos = [
            'cnpjCompra' => $cnpjCompra,
            'anoCompra' => explode('/', $this->licitacao_numero)[1],
            'sequencialCompra' => intval($sequencialCompra),
            'tipoContratoId' => config('api-pncp.tipo_contrato')[$this->tipo->descres],
            'numeroContratoEmpenho' => $numeroContratoEmpenho,
            'anoContrato' => $anoContrato,
            'processo' => $this->processo,
            'categoriaProcessoId' => config('api-pncp.categoria_contrato')[$this->getContratoCodigoDescricao()],
            'niFornecedor' => $this->trataFornecedor(),
            'tipoPessoaFornecedor' => config('api-pncp.tipo_fornecedor')[$this->fornecedor->tipo_fornecedor],
            'nomeRazaoSocialFornecedor' => $this->fornecedor->nome,
            'receita' => $receita,
            'codigoUnidade' => $this->unidadeorigem->codigo,
            'objetoContrato' => $this->objeto,
            'valorInicial' => $this->valor_inicial,
            'numeroParcelas' => $this->num_parcelas,
            'valorParcela' => $this->valor_parcela,
            'valorGlobal' => $this->valor_global,
            'dataAssinatura' => $this->data_assinatura,
            'dataVigenciaInicio' => $this->vigencia_inicio,
            'dataVigenciaFim' => $this->vigencia_fim,
            'valorAcumulado' => $this->valor_acumulado,
            'cnpjOrgaoSubRogado' => $cnpjSubRogado,
            'codigoUnidadeSubRogada' => $unidadeSubRogada,
            'niFornecedorSubContratado' => null,
            'tipoPessoaFornecedorSubContratado' => null,
            'nomeRazaoSocialFornecedorSubContratado' => null,
            'informacaoComplementar' => $this->info_complementar,
            'identificadorCipi' => $numero_cipi,
        ];
        if ($alteracao) {
            $campos = array_merge($campos, ['justificativa' => $this->justificativaExclusao]);
        }
        return $campos;
    }

    public function trataFornecedor()
    {
        $retorno = null;
        switch ($this->fornecedor->tipo_fornecedor) {
            case 'UG':
                $ug = Unidade::where('codigo', $this->fornecedor->cpf_cnpj_idgener)->first();
                $retorno = @$ug->cnpj;
                break;
            case 'IDGENERICO':
                $retorno = $this->fornecedor->cpf_cnpj_idgener;
                break;
            default:
                $retorno = preg_replace("/[^0-9]/", "", $this->fornecedor->cpf_cnpj_idgener);
        }
        return $retorno;
    }

    public function serializaExclusaoPNCP()
    {
        return [
            'justificativa' => isset($this->justificativaExclusao) ? $this->justificativaExclusao : '',
        ];
    }

    public function serializaTermoPNCP($alteracao)
    {
        try {
            $tipoTermo = config('api-pncp.tipo_termo_contrato')[$this->tipo->descres];
            $dataVigenciaInicio = $this->data_assinatura;
            $dataVigenciaFim = $this->vigencia_fim;

            switch (intval($tipoTermo)) {
                case 1: # Termo de Rescisão
                    $dataVigenciaInicio = null;
                    break;
                case 2: # Termo Aditivo
                    if ($this->termoAditivoTemEstaQualificacao('REAJUSTE')) {
                        $dataVigenciaInicio = $this->data_inicio_novo_valor;
                    }
                    if ($this->termoAditivoTemEstaQualificacao('VIGÊNCIA')) {
                        $dataVigenciaInicio = $this->vigencia_inicio;
                    }
                    break;
                case 3: # Termo de Apostilamento
                    $dataVigenciaInicio = $this->data_inicio_novo_valor;
                    $dataVigenciaFim = $this->data_fim_novo_valor ?? $this->vigencia_fim;
                    break;
                default:
                    break;
            }
            $campos = [
                'tipoTermoContratoId' => $tipoTermo,
                'numeroTermoContrato' => $this->numero,
                //'objetoTermoContrato' => $this->objeto, -> aguardar solução
                'objetoTermoContrato' => $this->observacao,
                'qualificacaoAcrescimoSupressao' => $this->termoAditivoTemEstaQualificacao('ACRÉSCIMO / SUPRESSÃO'),
                'qualificacaoVigencia' => $this->termoAditivoTemEstaQualificacao('VIGÊNCIA'),
                'qualificacaoFornecedor' => $this->termoAditivoTemEstaQualificacao('FORNECEDOR'),
                'qualificacaoInformativo' => $this->termoAditivoTemEstaQualificacao('INFORMATIVO'),
                'qualificacaoReajuste' => $this->termoAditivoTemEstaQualificacao('REAJUSTE'),
                'dataAssinatura' => $this->data_assinatura,
                'niFornecedor' => $this->trataFornecedor(),
                'tipoPessoaFornecedor' => config('api-pncp.tipo_fornecedor')[$this->fornecedor->tipo_fornecedor],
                'nomeRazaoSocialFornecedor' => $this->fornecedor->nome,
                'niFornecedorSubContratado' => $this->niFornecedorSubContratado ?? null,
                'tipoPessoaFornecedorSubContratado' => $this->tipoPessoaFornecedorSubContratado ?? null,
                'nomeRazaoSocialFornecedorSubContratado' => $this->nomeRazaoSocialFornecedorSubContratado ?? null,
                'informativoObservacao' => $this->observacao,
                'fundamentolegal' => $this->fundamento_legal ?? null,
                'valorAcrescido' => $this->valor_acumulado,
                'numeroParcelas' => $this->num_parcelas,
                'valorParcela' => $this->valor_parcela,
                'valorGlobal' => $this->valor_global,
                'prazoAditadoDias' => 0,
                'dataVigenciaInicio' => $dataVigenciaInicio,
                'dataVigenciaFim' => $dataVigenciaFim
            ];
            if ($alteracao) {
                $campos = array_merge($campos, ['justificativa' => $this->justificativaExclusao]);
            }
            return $campos;
        } catch (Exception $e) {
            $this->inserirLogPncp($e);
        }
    }

    protected function termoAditivoTemEstaQualificacao($descricao)
    {
        if (isset($this->idQualificacoes) && !empty($this->idQualificacoes)) {
            $descricoes = Codigoitem::whereIn('id', $this->idQualificacoes)
                ->pluck('descricao')
                ->toArray();
            return in_array($descricao, $descricoes);
        }
        return $this->qualificacoes->contains('descricao', $descricao);
    }

    public function getContratoCodigoDescricao()
    {
        return (Contrato::find($this->contrato_id))->categoria->descricao;
    }

    public function getIsSaldoContaVinculadaLiberado()
    {
        return $this->recuperarIsSaldoContaVinculadaLiberadoRelatorio($this->contrato->is_saldo_conta_vinculada_liberado);
    }

    public function getIsGarantiaContratualDevolvida()
    {
        return $this->recuperarIsGarantiaContratualDevolvidaRelatorio($this->contrato->is_garantia_contratual_devolvida);
    }

    public function getGrauSatisfacaoDesempenhoContratoRelatorio()
    {
        return $this->recuperarGrauSatisfacaoDesempenhoContratoRelatorio($this->contrato->grau_satisfacao_desempenho_contrato);
    }

    public function getPlanejamentoContratacaoAtendida()
    {
        return $this->recuperarPlanejamentoContratacaoAtendidaRelatorio($this->contrato->grau_satisfacao_desempenho_contrato);
    }

    public function getIdByContratoIdItens($contrato_id_itens)
    {
        $contratosId = $this::select("id")->where('contrato_id', '=', $contrato_id_itens)->pluck('id')->toArray();
        $array_id = [];
        foreach ($contratosId as $id)
            $array_id[] = $id;
        return $array_id;
    }

    public function isTermoDeContrato(): bool
    {
        $descricoes = Contrato::getDescricoesTermosDeContrato();
        return in_array($this->tipo->descricao, $descricoes);
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    // Método contrato() passa a ser herdado de ContratoBase
    public function contrato()
    {
        return $this->belongsTo(Contrato::class, 'contrato_id');
    }
    */
    public function envia_dados_pncp()
    {
        return $this->morphToMany(EnviaDadosPncp::class, 'pncpable');
    }

    public function fornecedor()
    {
        return $this->belongsTo(Fornecedor::class, 'fornecedor_id');
    }

    public function cronograma()
    {
        return $this->hasMany(Contratocronograma::class, 'contratohistorico_id');
    }

    public function unidadeorigem()
    {
        return $this->belongsTo(Unidade::class, 'unidadeorigem_id');
    }

    public function tipo()
    {
        return $this->belongsTo(Codigoitem::class, 'tipo_id');
    }

    public function modalidade()
    {
        return $this->belongsTo(Codigoitem::class, 'modalidade_id');
    }

    public function orgaosubcategoria()
    {
        return $this->belongsTo(OrgaoSubcategoria::class, 'subcategoria_id');
    }

    public function saldosItens()
    {
        return $this->morphMany(Saldohistoricoitem::class, 'saldoable');
    }

    public function unidade()
    {
        return $this->belongsTo(Unidade::class, 'unidade_id');
    }

    public function unidadecompra()
    {
        return $this->belongsTo(Unidade::class, 'unidadecompra_id');
    }

    public function unidadebeneficiaria()
    {
        return $this->belongsTo(Unidade::class, 'unidadebeneficiaria_id');
    }

    public function arquivos()
    {
        return $this->hasMany(Contratoarquivo::class, 'contratohistorico_id');
    }

    public function publicacao()
    {
        return $this->hasMany(Contratopublicacoes::class, 'contratohistorico_id');
    }

    public function amparolegal()
    {
        return $this->belongsToMany(
            'App\Models\AmparoLegal',
            'amparo_legal_contratohistorico',
            'contratohistorico_id',
            'amparo_legal_id'
        );
    }

    public function qualificacoes()
    {
        return $this->belongsToMany(
            'App\Models\Codigoitem',
            'contratohistoricoqualificacao',
            'contratohistorico_id',
            'tipo_id'
        );
    }

    public function minutasempenho()
    {
        return $this->belongsToMany(
            'App\Models\MinutaEmpenho',
            'contrato_historico_minuta_empenho',
            'contrato_historico_id',
            'minuta_empenho_id'
        );
    }

    // assim funciona, mas acho que está limpando na base por conta disso.
    // testando assim, mas continua limpando a base
    public function contrato_autoridade_signataria()
    {
        return $this->belongsToMany(
        // AutoridadeSignataria::class,
            'App\Models\AutoridadeSignataria',
            'contrato_autoridade_signataria',           // from esse
            'contratohistorico_id', // where esse - 3  = contrato_id
            'autoridadesignataria_id',          // select esse - 1
        // 'contrato_id',
        );
    }


    public function categoria()
    {
        return $this->belongsTo(Codigoitem::class, 'categoria_id');
    }

    public function fornecedoresSubcontratados()
    {
        return $this->belongsToMany(
            Fornecedor::class,
            'contratohistorico_fornecedores_pivot',
            'contratohistorico_id',
            'fornecedor_id'
        );
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    public function getProrrogavelColunaAttribute()
    {
        if ($this->prorrogavel === null) {
            return '';
        }
        return $this->prorrogavel == 1 ? 'Sim' : 'Não';
    }

    public function getComboPublicacaoAttribute()
    {
        return $this->numero . ' - ' . $this->tipo->descricao;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

}
