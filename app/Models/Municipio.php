<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Municipio extends Model
{
    use SoftDeletes;
    use LogsActivity;
    use CrudTrait;
    public $primaryKey = 'id';
    protected $table = 'municipios';
    protected $fillable = ['nome'];

    public function getMunicipioPorCodigo(string $codigo)
    {
        if (!empty($codigo)) {
            return $this->join('estados', 'estados.id', '=', 'municipios.estado_id')
                ->whereRaw("codigo_ibge::text LIKE '$codigo%'")
                ->select(['municipios.*',  'estados.sigla'])
                ->first();
        }
        return null;
    }

    public function getMunicipioPorUF(string $uf, string $municipio)
    {
        return $this->whereHas('estado', function ($query) use ($uf) {
            $query->where("sigla", $uf);
        })
            ->where("nome", 'ILIKE', "%{$municipio}%")
            ->first();
    }

    public function getEstadoSiglaAttribute()
    {
        return $this->estado->sigla;
    }

    public function getMunicipio(string $codigo, string $uf = null, string $municipio = null)
    {
        return $this->getMunicipioPorCodigo($codigo) ?? $this->getMunicipioPorUF($uf, $municipio);
    }

    public function estado()
    {
        return $this->belongsTo(Estado::class, 'estado_id', 'id');
    }
}
