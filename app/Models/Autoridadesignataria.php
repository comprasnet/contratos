<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Autoridadesignataria extends Model
{
    use CrudTrait;
    use SoftDeletes;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'autoridadesignataria';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['unidade_id', 'autoridade_signataria', 'cargo_autoridade_signataria', 'titular', 'ativo'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getAtivo()
    {
        if ($this->ativo) {
            return 'Ativa';
        }

        return 'Inativa';
    }

    public function getTitular()
    {
        if ($this->titular) {
            return 'Titular';
        }

        return 'Substituta';
    }

    public function vinculoAtaContrato($idSignatario)
    {
        $possuiVinculo = AutoridadeSignataria::whereNotExists(function ($query) {
            $query->select('autoridade_signataria_id')
                ->from('arp_autoridade_signataria')
                ->whereColumn('arp_autoridade_signataria.autoridade_signataria_id', 'autoridadesignataria.id');
        })
            ->whereNotExists(function ($query) {
                $query->select('autoridadesignataria_id')
                    ->from('contrato_autoridade_signataria')
                    ->whereColumn('contrato_autoridade_signataria.autoridadesignataria_id', 'autoridadesignataria.id');
            })
            ->where('id', $idSignatario)
            ->select('id')
            ->get()
            ->toArray();

        return $possuiVinculo;

    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    // public function setTitular($value)
    // {
    //     if($value){return 'Titular';}
    //     else{return 'Substituto';}
    // }
}
