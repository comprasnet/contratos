<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class Arp extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'arp';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = [    ];

    // protected $hidden = [];
    public static $datesGovBr = [
        'data_assinatura',
        'vigencia_inicial',
        'vigencia_final'
    ];

    protected $casts = [
        'compra_centralizada' => 'boolean'
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function getCompraCentralizada()
    {
        if ($this->compra_centralizada) {
            return "Sim";
        }

        return "Não";
    }

    public function getNumeroCompra()
    {
        return $this->compras->numero_ano;
    }

    public function getNumeroAno()
    {
        return "{$this->numero}/{$this->ano}";
    }

    public function getCatMatSerItem($id, $numero_item)
    {
        $result=ArpItem::join(
            "compra_item_fornecedor",
            "arp_item.compra_item_fornecedor_id",
            "=",
            "compra_item_fornecedor.id"
        )
        ->join("fornecedores", "fornecedores.id", "=", "compra_item_fornecedor.fornecedor_id")
        ->join("compra_items", "compra_items.id", "=", "compra_item_fornecedor.compra_item_id")
        ->join('catmatseritens', 'catmatseritens.id', '=', 'compra_items.catmatseritem_id')
        ->join('catmatsergrupos', 'catmatseritens.grupo_id', '=', 'catmatsergrupos.id')
        ->join('codigoitens', 'codigoitens.id', '=', 'catmatsergrupos.tipo_id')
        ->where("arp_item.arp_id", $id)
        ->where("compra_items.numero", $numero_item)
        ->selectRaw("CASE
                        WHEN compra_items.permite_carona = true THEN 'Sim'
                        WHEN compra_items.permite_carona = false THEN 'Não'
                        END AS aceita_adesao,
                        codigoitens.descricao , codigoitens.descres,
                        compra_items.numero, compra_items.descricaodetalhada, compra_items.maximo_adesao")
        ->distinct("compra_items.numero")
        ->orderBy("compra_items.numero")
        ->first();

        return $result->tipo_item;
    }

    public function getValorTotal()
    {
        return $this->valor_total;
    }

    /**
     * Método responsável em retornar a unidade de origem formatada
     */
    public function getUnidadeOrigem()
    {
        return $this->unidades->exibicaoCompletaUASG();
    }

    /**
     * Método responsável em retornar a unidade de origem da compra formatada
     */
    public function getUnidadeOrigemCompra()
    {
        $unidadeOrigemCompra = $this->compras->unidadeOrigem;

        return $unidadeOrigemCompra->codigo."-".$unidadeOrigemCompra->nomeresumido;
    }

    public function getModalidadeCompra()
    {
        return $this->compras->getModalidade();
    }

    public static function getGestorAta(int $idAta)
    {
        $arp = Arp::find($idAta);
        $gestores = array();
        foreach ($arp->gestor as $key => $gestor) {
            $gestores['Nome'][$key] = $gestor->user->name;
            $gestores['Email'][$key] = $gestor->user->email;
        }

        return $gestores;
    }

    public static function getAutoridadeSignataria(int $idAta)
    {
        $arp = Arp::find($idAta);
        $autoridades = array();
        foreach ($arp->autoridade_signataria as $key => $autoridade_signataria) {
            $autoridades['Nome'][$key] = $autoridade_signataria->user->autoridade_signataria;
            $autoridades['Cargo'][$key] = $autoridade_signataria->user->cargo_autoridade_signataria;
        }

        return $autoridades;
    }

    public static function getUnidadeParticipante(int $idAta)
    {
        $unidades = ArpItem::getUnidadeParticipantePorAta($idAta, false);

        $unidadeParticipante = array();
        foreach ($unidades as $key => $unidade) {
            $unidadeParticipante['Código'][$key] = $unidade->codigo;
            $unidadeParticipante['UASG'][$key] = $unidade->nomeresumido;
            $textoTipoUasg = '';
            switch ($unidade->tipo_uasg) {
                case 'G':
                    $textoTipoUasg = 'Gerenciadora';
                    break;
                case 'P':
                    $textoTipoUasg = 'Participante';
                    break;
                case 'C':
                    $textoTipoUasg = 'Carona';
                    break;
            }

            $unidadeParticipante['Tipo'][$key] = $textoTipoUasg;
        }
        return $unidadeParticipante;
    }

    public static function getUnidadeParticipanteItens(int $idAta, $numero_item)
    {
        $unidadesItens = ArpItem::getUnidadeParticipantePorAtaItem($idAta, $numero_item);

        $unidadeParticipanteItem = array();

        foreach ($unidadesItens as $key => $unidadeItem) {
            $unidadeParticipanteItem['Código'][$key] = $unidadeItem->codigo;
            $unidadeParticipanteItem['Unidade'][$key] = $unidadeItem->nomeresumido;
            $unidadeParticipanteItem['Tipo da unidade'][$key] = $unidadeItem->tipo_uasg;
            $unidadeParticipanteItem['Quantidade_autorizada'][$key] = $unidadeItem->quantidade_autorizada;
            //$unidadeParticipanteItem['Quantidade_Homologada'][$key] = $unidadeItem->quantidade_homologada_total;
        }

        return $unidadeParticipanteItem;
    }



    /*
        public static function getPorAtaMinutaEmpenhoItens(int $situacao,$numero_item )
        {
            $empenhosItens = ArpItem::getItemPorAtaMinutaEmpenho($situacao,  $numero_item) ;

            $empenhoMinutaItens = array();

            foreach ($empenhosItens as $key => $empenhoItens) {
                $empenhoMinutaItens['mensagem_siafi'][$key] = $empenhoItens->mensagem_siafi;
                //$empenhoMinutaItens['created_at'][$key] = $empenhoItens->created_at;
                //$empenhoMinutaItens['Codigo'][$key] = $empenhoItens->codigo;

            }

            return $empenhoMinutaItens;
        }
    */
    public static function getPorAtaMinutaEmpenhoItens(int $situacao, string $numero_item, int $idAta)
    {
        $empenhosItens = ArpItem::getItemPorAtaMinutaEmpenho($situacao, $numero_item, $idAta);

        $listaEmpenhoMinutaItens = array();

        foreach ($empenhosItens as $key => $empenhoItens) {
            $dataFormat = '';
            if ($empenhoItens->created_at) {
                $dataFormat=Carbon::createFromFormat('Y-m-d H:i:s', $empenhoItens->created_at)->format('d/m/Y');
            }
            $listaEmpenhoMinutaItens['Número do Empenho'][$key] = $empenhoItens->mensagem_siafi;
            $listaEmpenhoMinutaItens['Data do Empenho'][$key] = $dataFormat;
            $listaEmpenhoMinutaItens['Quantidade Empenhada'][$key] = $empenhoItens->quantidade_minuta_empenhada;
         //  $listaEmpenhoMinutaItens['Soma Autorizada'][$key] = $empenhoItens->soma_quantidade_autorizada;
            //$listaEmpenhoMinutaItens['Quantidade Registrada'][$key] = $empenhoItens->quantidade_registrada;
            //$listaEmpenhoMinutaItens['Saldo para Empenho'][$key] = $empenhoItens->saldo_minuta_empenho;
            // adicionando a coluna "fornecedor"
            $listaEmpenhoMinutaItens['Fornecedor do Empenho'][$key] = $empenhoItens->fornecedor_empenho;
        }

        return $listaEmpenhoMinutaItens;
    }

    public static function getPorAtaMinutaEmpenhoItensSimpli(int $idAta, string $numero_item, int $situacao)
    {
        $empenhosItemSimplificada = ArpItem::getItemPorAtaMinutaEmpenhoSimplificada($idAta, $numero_item, $situacao);


        $listaEmpenhoMinutaItemSimplificada = array();
        foreach ($empenhosItemSimplificada as $key => $empenhoItensSimp) {
            $listaEmpenhoMinutaItemSimplificada['Quantidade Autorizada'][$key] =
            $empenhoItensSimp->quantidade_autorizada;
            $listaEmpenhoMinutaItemSimplificada['Saldo para Empenho'][$key] = $empenhoItensSimp->saldo_para_empenho;
        }

        return $listaEmpenhoMinutaItemSimplificada;
    }
    public static function getDetalhamentoAdesaoItens(int $idAta, string $numero_item)
    {
        $adesaoItens   = ArpItem::getSaldoAdesao($idAta, $numero_item);
        $listAdesaoItens  = array();
        foreach ($adesaoItens as $key => $adesaoItem) {
            $listAdesaoItens['Quantidade máxima para Adesão'][$key] = $adesaoItem->maximo_adesao;
            $listAdesaoItens['Quantidade aprovada da Adesão'][$key] = $adesaoItem->quantidade_aprovada;
            $listAdesaoItens['Unidade'][$key] = $adesaoItem->unidade_solicitacao;
            $listAdesaoItens['Quantidade disponivel para adesão'][$key] = $adesaoItem->saldo_adesao;
        }
        return $listAdesaoItens;
    }

    public static function getDetalhamentoContratosItens(int $idAta, string $numero_item)
    {
        $contratoItens   = ArpItem::getContratosItem($idAta, $numero_item);
        $listContratoItens  = array();
        foreach ($contratoItens as $key => $contratosItem) {
            $listContratoItens['Número do instrumento'][$key] = $contratosItem->numero;
            $listContratoItens['Fornecedor'][$key] = $contratosItem->fornecedor_contrato;
           // $listContratoItens[''][$key] = '<a href="'.str_replace('appredirect/','transparencia/contratos/', env('URL_CONTRATO_VERSAO_UM')).$contratosItem->id.'" target="_blank">'.trans('Acessar').'</a>';
            $listContratoItens[''][$key] = '<a href="'.str_replace('appredirect/','transparencia/contratos/', env('URL_CONTRATO_VERSAO_UM')).$contratosItem->id.'" target="_blank" style="font-size: 27px;" title="Clique aqui para acessar o link">'.trans('').'<i class="la la-external-link" aria-hidden="true" style="margin-right: -25px;"></i></a>';
        }
        return $listContratoItens;
    }

    public static function getItemAtaSimplificada(int $idAta)
    {
        $itens = ArpItem::getItemPorAtaSimplificada($idAta);
        $listaItem = array();

        foreach ($itens as $key => $itemSelecionado) {
            $listaItem['Número'][$key] = $itemSelecionado->numero;
            $listaItem['Item'][$key] = $itemSelecionado->descricaodetalhada;
            $listaItem['Quantidade_máxima_adesão'][$key] = $itemSelecionado->maximo_adesao;
            $listaItem['Aceita_adesão'][$key] = $itemSelecionado->aceita_adesao;
        }

        return $listaItem;
    }




    public static function getFornecedorCompra(int $idAta)
    {
        $fornecedores = ArpItem::getFornecedorPorAta($idAta);

        $listaFornecedor = array();
        foreach ($fornecedores as $key => $fornecedor) {
            $listaFornecedor['CNPJ'][$key] = $fornecedor->cpf_cnpj_idgener;
            $listaFornecedor['Fornecedor'][$key] = $fornecedor->nome;
        }

        return $listaFornecedor;
    }

    public function getFornecedorPorAta()
    {
        $fornecedores = ArpItem::join(
            "compra_item_fornecedor",
            "arp_item.compra_item_fornecedor_id",
            "=",
            "compra_item_fornecedor.id"
        )
        ->join("fornecedores", "fornecedores.id", "=", "compra_item_fornecedor.fornecedor_id")
        ->where("arp_item.arp_id", $this->id)
        ->selectRaw("CONCAT(cpf_cnpj_idgener, ' - ' ,nome) AS nome_fornecedor, cpf_cnpj_idgener, nome")
        ->groupBy("cpf_cnpj_idgener", "nome")
        ->get();

        foreach ($fornecedores as $fornecedor) {
            return $fornecedor['nome_fornecedor'];
        }
    }
    public function getFornecedorPorAtaTransparencia()
    {
        $fornecedores = ArpItem::join(
            "compra_item_fornecedor",
            "arp_item.compra_item_fornecedor_id",
            "=",
            "compra_item_fornecedor.id"
        )
            ->join("fornecedores", "fornecedores.id", "=", "compra_item_fornecedor.fornecedor_id")
            ->where("arp_item.arp_id", $this->id)
            ->selectRaw("CONCAT(cpf_cnpj_idgener, ' - ' ,nome) AS nome_fornecedor, cpf_cnpj_idgener, nome")
            ->groupBy("cpf_cnpj_idgener", "nome")
            ->get();

        $fornecedoresArray = []; // Array para armazenar os fornecedores

        foreach ($fornecedores as $fornecedor) {
            $fornecedoresArray[] = $fornecedor['nome_fornecedor']; // Adicionar o fornecedor ao array
        }


        return $fornecedoresArray;
    }
    public function getFornecedorPorAtaItem(int $idAta, string $numero_item)
    {
        $fornecedores = ArpItem::join(
            "compra_item_fornecedor",
            "arp_item.compra_item_fornecedor_id",
            "=",
            "compra_item_fornecedor.id"
        )
            ->join("fornecedores", "fornecedores.id", "=", "compra_item_fornecedor.fornecedor_id")
            ->join("compra_items", "compra_items.id", "=", "compra_item_fornecedor.compra_item_id")
            ->where("compra_items.numero", $numero_item)
            ->where('arp_id', '=', $idAta)
            ->select(DB::raw("compra_item_fornecedor.quantidade_homologada_vencedor, compra_item_fornecedor.valor_unitario, compra_item_fornecedor.classificacao"))
            ->selectRaw("CONCAT(cpf_cnpj_idgener, ' - ' ,nome) AS nome_fornecedor, cpf_cnpj_idgener, nome")
            ->groupBy("cpf_cnpj_idgener", "nome", "compra_item_fornecedor.quantidade_homologada_vencedor", "compra_item_fornecedor.valor_unitario", "compra_item_fornecedor.classificacao")
            ->orderby("compra_item_fornecedor.classificacao")
           ->get();

        $listaFornecedor = array();
        foreach ($fornecedores as $key => $fornecedor) {
            $listaFornecedor['Classificação'][$key] = $fornecedor->classificacao;
            $listaFornecedor['CNPJ'][$key] = $fornecedor->cpf_cnpj_idgener;
            $listaFornecedor['Fornecedor'][$key] = $fornecedor->nome;
            $listaFornecedor['Quantidade_total'][$key] = number_format($fornecedor->quantidade_homologada_vencedor, 5, ',', '.');
            $listaFornecedor['Valor_unitário'][$key] = 'R$ ' . number_format($fornecedor->valor_unitario, 5, ',', '.');
        }
        return $listaFornecedor;
    }


    # Método responsável em montar um array com as informações do itens da ata
    public static function getItemAta(int $idAta)
    {
        $itens = ArpItem::getItemPorAta($idAta);
        $listaItem = array();

        foreach ($itens as $key => $itemSelecionado) {
            $listaItem['CNPJ'][$key] = $itemSelecionado->cpf_cnpj_idgener;
            $listaItem['Fornecedor_(Classificação)'][$key] =
            $itemSelecionado->nomefornecedor." ({$itemSelecionado->classificacao})";
            $listaItem['Número'][$key] = $itemSelecionado->numero;
            $listaItem['Item'][$key] = $itemSelecionado->descricaodetalhada;

            $listaItem['Quantidade_Registrada'][$key] =
            number_format($itemSelecionado->quantidade_homologada_vencedor, 5, ',', '.');
            $listaItem['Valor_unitário'][$key] = number_format($itemSelecionado->valor_unitario, 4, ',', '.');

            $valorTotal = $itemSelecionado->valor_negociado;
            // $itemSelecionado->valor_unitario * $itemSelecionado->maximo_adesao;

            $listaItem['Valor_total'][$key] = number_format($valorTotal, 4, ',', '.');
            $listaItem['Quantidade_máxima_adesão'][$key] = (int)$itemSelecionado->maximo_adesao;
            $listaItem['Aceita_adesão'][$key] = $itemSelecionado->aceita_adesao;
        }

        return $listaItem;
    }

    # Método responsável em traduzir o status da Ata
    public function getStatus()
    {
        switch ($this->rascunho) {
            case true:
                return 'Em elaboração';
            break;
            case false:
                return 'Ativa';
            break;
        }
    }

    # Método responsável em exibir os botões de ações para o usuário conforme o status da ata
    public function getViewButtonArp($crud)
    {
        if ($this->rascunho) {
            $crud->allowAccess('update');
            $crud->allowAccess('delete');

            return null;
        }

        $crud->denyAccess('update');
        $crud->denyAccess('delete');

        if (empty($this->getLinkPNCP())) {
            return null;
        }

        $botaoRetificar =  '<a class="btn btn-sm btn-link" href="'.url('arp/retificar/'.$this->id).'"
                            data-toggle="tooltip" title="Retificar"><i class="fas fa-redo"></i></a>';
        return $botaoRetificar;
    }

    /**
     * Método responsável em exibir o status para o usuário na listagem de atas
     */
    public function getStatusArp()
    {
        # Se a ata estiver em rascunho, exibir o status de Em Elaboração
        if ($this->rascunho) {
            return 'Em Elaboração';
        }

        # Se a ata estiver em concluída, exibir o status de Ativa
        if ($this->tipo->descricao == 'Ata de Registro de Preços') {
            return 'Ativa';
        }

        return $this->tipo->descricao;
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function compras()
    {
        return $this->belongsTo(Compras::class, 'compra_id');
    }
    public function arp()
    {
        return $this->belongsTo(Arp::class, 'arp_id');
    }

    public function unidades()
    {
        return $this->belongsTo(Unidade::class, 'unidade_origem_id');
    }

    public function unidadeCompraOrigem()
    {
        return $this->belongsTo(Unidade::class, "unidade_origem_compra_id");
    }

    public function gestor()
    {
        return $this->hasMany(ArpGestor::class, 'arp_id');
    }

    public function autoridade_signataria()
    {
        return $this->hasMany(ArpAutoridadeSignataria::class, 'arp_id');
    }

    public function tipo()
    {
        return $this->belongsTo(CodigoItem::class, 'tipo_id');
    }

    public function item()
    {
        return $this->hasMany(ArpItem::class, 'arp_id');
    }

    public function arquivos(): HasMany
    {
        return $this->hasMany(ArpArquivo::class);
    }

    public function envia_dados_pncp()
    {
        return $this->morphOne(EnviaDadosPncp::class, 'pncpable');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    | SCOPES
    |--------------------------------------------------------------------------
    */

    public function scopeVigenciaInicial(Builder $query, string $date): Builder
    {
        $date = Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d') . ' 23:59:59';
        return $query->where('vigencia_inicial', '<=', $date);
    }

    public function scopeVigenciaFinal(Builder $query, string $date): Builder
    {
        return $query->where('vigencia_final', '>=', $date);
    }

    public function scopeAnoCompra(Builder $query, string $ano_compra): Builder
    {
        return $query->with('compras')->where('numero_ano', 'like', '%'.$ano_compra.'%');
    }

    public function scopeNumeroCompra(Builder $query, string $numero_compra): Builder
    {
        return $query->with('compras')->where('numero_ano', 'like', '%'.$numero_compra.'%');
    }

    public function scopeAnoAta(Builder $query, string $ano_ata): Builder
    {
        return $query->with('arp')
            ->where("arp.ano", "like", "%".$ano_ata."%");
    }
    public function scopeNumeroAta(Builder $query, string $numero_ata): Builder
    {
        return $query->with('arp')
            ->where("arp.numero", "like", "%".$numero_ata."%");
    }

    public function scopeDescricaoItem(Builder $query, string $descricao_item): Builder
    {
        return $query->with('compra_items')
            ->where("compra_items.descricaodetalhada", "ilike", "%".$descricao_item."%");
    }

    public function getAtaArpAdesaoAttribute()
    {
        return "{$this->numero}/{$this->ano}";
    }

    /**
     * Método responsável em exibir o botão para o usuário ir para a ata
     * no PNCP
     */
    public function getLinkPNCP()
    {
        # Recupera os dados da ata na tabela responsável pelo envio para o PNCP
        $enviaDadosPNCP = EnviaDadosPncp::where('pncpable_id', $this->id)
                            ->where('pncpable_type', self::class)
                            ->first();

        # Se não existir registro, bloqueia o botão
        if (empty($enviaDadosPNCP)) {
            return null;
        }
        // dd($enviaDadosPNCP, $enviaDadosPNCP->link_pncp);
        // $linkPncp = EnviaDadosPncpHistorico::select('link_pncp')
        //     ->where('pncpable_id', $this->id)
        //     ->where('pncpable_type', self::class)
        //     ->where('log_name', 'inserir_ata')
        //     ->whereIn('status_code', [200, 201])
        //     ->first();

        // $sequencialPNCP = EnviaDadosPncpHistorico::select('sequencialPNCP')
        //     ->where('pncpable_id', $this->id)
        //     ->where('pncpable_type', self::class)
        //     ->where('log_name', 'consultar_sequencialPNCP')
        //     ->whereIn('status_code', [200, 201])
        //     ->first();

        # Se não existir o link ou o sequencial, bloqueia o botão
        if (is_null($enviaDadosPNCP->link_pncp) || is_null($enviaDadosPNCP->sequencialPNCP)) {
            return null;
        }

        $host = explode("/", $enviaDadosPNCP->link_pncp);
        $arr = explode("-", str_replace('/', '-', $enviaDadosPNCP->sequencialPNCP));

        return $host[0] .
            '//' .
            $host[2] .
            '/app/atas/' .
            $arr[0] .
            '/' .
            $arr[3] .
            '/' .
            intval($arr[2]) .
            '/' .
            intval($arr[4]);
    }





    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
