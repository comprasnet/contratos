<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class ContratoFaturasItens extends Model
{
    use CrudTrait;
    use LogsActivity;
    use SoftDeletes;

    protected static $logFillable = true;
    protected static $logName = 'contratofaturas_itens';
    protected $table = 'contratofaturas_itens';

    protected $fillable = [
        'contratofaturas_id',
        'saldohistoricoitens_id',
        'quantidade_faturado',
        'valorunitario_faturado',
        'valortotal_faturado',
        'paisfabricacao_id',
    ];

    /*
   |--------------------------------------------------------------------------
   | FUNCTIONS
   |--------------------------------------------------------------------------
   */

   public function contratoFaturaItensAPI()
   {
       return [
           'id_item_contrato' => $this->id_item_contrato,
           'quantidade_faturado' => number_format($this->quantidade_faturado, 5, ',', '.'),
           'valorunitario_faturado' => number_format($this->valorunitario_faturado, 4, ',', '.'),
           'valortotal_faturado' => number_format($this->valortotal_faturado, 2, ',', '.'),
       ];
   }

    /*
   |--------------------------------------------------------------------------
   | RELATIONS
   |--------------------------------------------------------------------------
   */
    public function saldohistoricoitem()
    {
        return $this->belongsTo(Saldohistoricoitem::class, 'saldohistoricoitens_id');
    }

    public function contratofaturas()
    {
        return $this->belongsTo(Contratofatura::class, 'contratofaturas_id');
    }

    public function paisfabricacao()
    {
        return $this->belongsTo(Paises::class, 'paisfabricacao_id');
    }

}
