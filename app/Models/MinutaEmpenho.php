<?php

namespace App\Models;

use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\Formatador;
use App\services\AmparoLegalService;
use Backpack\CRUD\CrudTrait;

use Eduardokum\LaravelMailAutoEmbed\Models\EmbeddableEntity;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Boolean;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Models\CompraItemMinutaEmpenho;
use Illuminate\Database\Eloquent\Collection;

class MinutaEmpenho extends Model
{
    use CrudTrait;
    use LogsActivity;
    use SoftDeletes;
    use BuscaCodigoItens;
    use Formatador;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected static $logFillable = true;
    protected static $logName = 'minuta_empenhos';

    protected $table = 'minutaempenhos';

    protected $guarded = [
        'id'
    ];

    protected $fillable = [
        'amparo_legal_id',
        'compra_id',
        'conta_contabil_passivo_anterior',
        'data_emissao',
        'descricao',
        'etapa',
        'fornecedor_compra_id',
        'fornecedor_empenho_id',
        'informacao_complementar',
        'local_entrega',
        'numero_empenho_sequencial',
        'passivo_anterior',
        'processo',
        'saldo_contabil_id',
        'situacao_id',
        'taxa_cambio',
        'tipo_empenho_id',
        'tipo_minuta_empenho',
        'unidade_id',
        'valor_total',
        'tipo_empenhopor_id',
        'contrato_id',
        'numero_cipi',
        'minutaempenho_forcacontrato'
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function formatarTextoParaAguardandoAnulacao(): string
    {

        #recupera id da minuta anulada
        $idMinutaAnulada = $this->getMinutaAnuladaIdAttribute();
        #recupera numero do empenho pelo $idMinutaAnulada
        $numeroEmpenho = $this::where('id', $idMinutaAnulada)->first()->mensagem_siafi ?? ' - ';

        return "AGUARDANDO EMISSÃO OU EXCLUSÃO DA MINUTA DE ALTERAÇÃO DE FONTE DO EMPENHO $numeroEmpenho EM ANDAMENTO";
    }

    public function getMensagemSiafiTratada()
    {
        $mensagem = str_replace("\"", "''", $this->mensagem_siafi);
        return $mensagem;
    }

    public function getCpfEmitente()
    {
        // verificar se a minuta já foi emitida
        $situacao = $this->situacao->descricao;
        if ($situacao != 'EMPENHO EMITIDO') {
            // para minutas não emitidas, não buscaremos o cpf_user nem o nonce
            return null;
        }
        // aqui a situação da minuta é EMPENHO EMITIDO - buscar o último sforcempenhodados
        $objSforcempenhodados = $this->empenho_dados()->get()->first(); // alterei para first, pois com last estava trazendo o menor id de sforcempenho
        if ($objSforcempenhodados) {
            $cpfUser = $objSforcempenhodados->cpf_user;
        } else {
            $cpfUser = null;
        }
        return $cpfUser;
    }

    public function getNonce()
    {
        // verificar se a minuta já foi emitida
        $situacao = $this->situacao->descricao;
        if ($situacao != 'EMPENHO EMITIDO') {
            // para minutas não emitidas, não buscaremos o cpf_user nem o nonce
            return null;
        }
        // aqui a situação da minuta é EMPENHO EMITIDO - buscar o último sforcempenhodados
        $objSforcempenhodados = $this->empenho_dados()->get()->last();
        if ($objSforcempenhodados) {
            $sfnonce = $objSforcempenhodados->sfnonce;
        } else {
            $sfnonce = null;
        }
        return $sfnonce;
    }    // retorna minutas de empenho, que tenham força de contrato

    public function getTodosEmpenhosComForcaDeContratoByUasgByNumeroEmpenho($codigoUnidadeEmitente, $numeroEmpenho)
    {
        $minutas = MinutaEmpenho::where('minutaempenhos.mensagem_siafi', $numeroEmpenho)
            ->select(
                'minutaempenhos.updated_at as minutaempenho_data_criacao',
                'minutaempenhos.id as minutaempenho_id',
                'minutaempenhos.contrato_id as contrato_id',
                'minutaempenhos.compra_id as compra_id',
                'minutaempenhos.mensagem_siafi as numero_empenho',
                'minutaempenhos.unidade_id as minutaempenho_unidade_id',
                'saldo_contabil.unidade_id as saldocontabil_unidade_id',
                'unidades.codigo as unidade_codigo',
                'unidades.nomeresumido as unidade_nomeresumido',
                'unidades.id as unidade_id',
                'unidades.nome as unidade_nomecompleto',
                'fornecedor_compra.nome as fornecedor_compra_nome',
                'fornecedor_compra.cpf_cnpj_idgener as fornecedor_compra_cpf_cnpj_idgener',
                'fornecedor_empenho.nome as fornecedor_empenho_nome',
                'fornecedor_empenho.cpf_cnpj_idgener as fornecedor_empenho_cpf_cnpj_idgener',
                'empenhos.empenhado as empenho_valor_empenhado',
                'empenhos.aliquidar as empenho_valor_a_liquidar',
                'empenhos.liquidado as empenho_valor_liquidado',
                'empenhos.pago as empenho_valor_pago',
                'empenhos.rpinscrito as empenho_valor_rpinscrito',
                'empenhos.rpaliquidar as empenho_valor_rpaliquidar',
                'empenhos.rpliquidado as empenho_valor_rpliquidado',
                'empenhos.rppago as empenho_valor_rppago',
                'empenhos.unidade_id as empenho_unidade_id',
                'empenhos.id as empenho_id',
                'naturezadespesa.codigo as naturezadespesa_codigo',
                'naturezadespesa.descricao as naturezadespesa_descricao',
                'tipo_empenhopor.descricao as minutaempenho_tipo_empenhopor',
                'orgaos.nome as orgao_nome',
                'orgaos.codigo as orgao_codigo',
                'contratos.numero as contrato_numero'
            )
            ->join('saldo_contabil', 'saldo_contabil.id', 'minutaempenhos.saldo_contabil_id')
            ->join('unidades', 'unidades.id', 'saldo_contabil.unidade_id')
            ->join('fornecedores as fornecedor_compra', 'fornecedor_compra.id', 'minutaempenhos.fornecedor_compra_id')
            ->join('fornecedores as fornecedor_empenho', 'fornecedor_empenho.id', 'minutaempenhos.fornecedor_empenho_id')
            ->leftjoin('empenhos', 'empenhos.numero', 'minutaempenhos.mensagem_siafi')              // tem que ser left
            ->leftjoin('naturezadespesa', 'naturezadespesa.id', 'empenhos.naturezadespesa_id')
            ->leftjoin('codigoitens as tipo_empenho', 'minutaempenhos.tipo_empenho_id', 'tipo_empenho.id')
            ->leftjoin('codigoitens as tipo_empenhopor', 'minutaempenhos.tipo_empenhopor_id', 'tipo_empenhopor.id')
            ->leftjoin('orgaos', 'orgaos.id', 'unidades.orgao_id')
            ->leftjoin('contrato_minuta_empenho_pivot', 'contrato_minuta_empenho_pivot.minuta_empenho_id', 'minutaempenhos.id')
            ->leftjoin('contratos', 'contratos.id', 'contrato_minuta_empenho_pivot.contrato_id')
            ->where('unidades.codigo', $codigoUnidadeEmitente)
            ->where('minutaempenhos.contrato_id', null)
            ->where('minutaempenhos.empenhocontrato', false)
            ->get();

        // vamos varrer o array e verificar a unidade, por conta do leftjoin que não podemos usar com o where
        $arrayFinal = array();
        foreach ($minutas as $minutaVerificarUnidade) {
            $unidadeDaMinuta = $minutaVerificarUnidade['saldocontabil_unidade_id'];
            $unidadeDoEmpenho = $minutaVerificarUnidade['empenho_unidade_id'];
            if ($unidadeDoEmpenho == $unidadeDaMinuta) {
                array_push($arrayFinal, $minutaVerificarUnidade);
            }
        }

        return $arrayFinal;
    }

    // verifica se a minuta é de contrato - retorn true se for.
    public function verificarSeMinutaEDeContrato($codigoUnidadeEmitente, $numeroEmpenho)
    {
        $minutas = MinutaEmpenho::where('minutaempenhos.mensagem_siafi', $numeroEmpenho)
            ->select(
                'saldo_contabil.unidade_id as saldocontabil_unidade_id',
                'empenhos.unidade_id as empenho_unidade_id'
            )
            ->join('contratos', 'contratos.id', 'minutaempenhos.contrato_id')   // aqui verificamos se o empenho é do tipo contrato
            ->join('saldo_contabil', 'saldo_contabil.id', 'minutaempenhos.saldo_contabil_id')
            ->join('unidades', 'unidades.id', 'saldo_contabil.unidade_id')
            ->join('fornecedores as fornecedor_compra', 'fornecedor_compra.id', 'minutaempenhos.fornecedor_compra_id')
            ->join('fornecedores as fornecedor_empenho', 'fornecedor_empenho.id', 'minutaempenhos.fornecedor_empenho_id')
            ->leftjoin('empenhos', 'empenhos.numero', 'minutaempenhos.mensagem_siafi')              // tem que ser left
            ->leftjoin('naturezadespesa', 'naturezadespesa.id', 'empenhos.naturezadespesa_id')
            ->where('unidades.codigo', $codigoUnidadeEmitente)
            ->where('minutaempenhos.contrato_id', '<>', null)   // aqui verificamos se o empenho é do tipo contrato
            ->where('minutaempenhos.empenhocontrato', false)
            ->get();
        // vamos varrer o array e verificar a unidade, por conta do leftjoin que não podemos usar com o where
        $arrayFinal = array();
        foreach ($minutas as $minutaVerificarUnidade) {
            $unidadeDaMinuta = $minutaVerificarUnidade['saldocontabil_unidade_id'];
            $unidadeDoEmpenho = $minutaVerificarUnidade['empenho_unidade_id'];
            if ($unidadeDoEmpenho == $unidadeDaMinuta) {
                array_push($arrayFinal, $minutaVerificarUnidade);
            }
        }
        if (count($arrayFinal) > 0) {
            return true;
        }
        return false;
    }

    /**
     * Retorna dados da Minuta de Empenho para apresentação
     *
     * @return array
     */
    public function retornaListagem()
    {
        $ug = session('user_ug');
        $listagem = MinutaEmpenho::where('id', 1)->get();

        return $listagem;
    }

#TODO: REFATORAR ESTE MÉTODO PARA REDUÇÃO DE IFS QUASE IDÊNTICOS
    public function retornaAmparoPorMinuta()
    {
        if ($this->tipo_empenhopor->descricao === 'Contrato') {
            return $this->retornaAmparoPorMinutadeContrato();
        }

        $query = AmparoLegal::select([
            'amparo_legal.id',
            DB::raw("ato_normativo ||
            case when (amparo_legal.artigo is not null) then ' - Artigo: ' || amparo_legal.artigo else '' end ||
            case when (paragrafo is not null) then ' - Parágrafo: ' || paragrafo else '' end ||
            case when (amparo_legal.inciso is not null) then ' - Inciso: ' || amparo_legal.inciso else '' end ||
            case when (alinea is not null) then ' - Alinea: ' || alinea else '' end
            as campo_api_amparo")
        ])
            ->where('amparo_legal.codigo', '<>', '');

        if ($this->tipo_empenhopor->descricao == "Compra") {

            $lei = $this->getLeiAttribute();
            $incisoRomano = $this->numberToRomanRepresentation($this->inciso);

            $this->verificaAtributosMinutaCompraQuery($lei, $query);

            $cloneQuery = clone $query;

            if (!empty($incisoRomano) || $incisoRomano === "0") {
                $query->where('amparo_legal.inciso', $incisoRomano);
            }

            if (!empty($this->compra->artigo)) {
                $query->where("amparo_legal.artigo", $this->compra->artigo);
            }
        }

        $query->join('compras', 'compras.modalidade_id', '=', 'amparo_legal.modalidade_id')
            ->join('minutaempenhos', 'minutaempenhos.compra_id', '=', 'compras.id')
            ->where('minutaempenhos.id', $this->id)
            ->groupBy('amparo_legal.id');

        // Quando não encontra na nossa base, ele consultar sem o inciso
        if ($query->count() == 0) {
            $cloneQuerySemArtigo = clone $cloneQuery;

            if ($this->tipo_empenhopor->descricao == "Compra") {

                if (!empty($this->compra->artigo)) {
                    $cloneQuery->where("amparo_legal.artigo", $this->compra->artigo);
                }

                $cloneQuery->join('compras', 'compras.modalidade_id', '=', 'amparo_legal.modalidade_id')
                    ->join('minutaempenhos', 'minutaempenhos.compra_id', '=', 'compras.id')
                    ->where('minutaempenhos.id', $this->id)
                    ->groupBy('amparo_legal.id');
                //Não encontrando novamente, ele busca sem inciso e sem artigo
                if ($cloneQuery->count() > 0) {
                    return $cloneQuery->pluck('campo_api_amparo', 'amparolegal.id')->toArray();
                }

                $cloneQuerySemArtigo->join('compras', 'compras.modalidade_id', '=', 'amparo_legal.modalidade_id')
                    ->join('minutaempenhos', 'minutaempenhos.compra_id', '=', 'compras.id')
                    ->where('minutaempenhos.id', $this->id)
                    ->groupBy('amparo_legal.id');

                if ($cloneQuerySemArtigo->count() > 0) {
                    return $cloneQuerySemArtigo->pluck('campo_api_amparo', 'amparolegal.id')->toArray();
                }

                // Quando não é encontrada a LEI, inciso e artigo na nossa base
                // refaz a query sem os parâmetros de lei e inciso.
                $querySemLei = AmparoLegal::select([
                    'amparo_legal.id',
                    DB::raw("ato_normativo ||
                            case when (amparo_legal.artigo is not null) then ' - Artigo: ' || amparo_legal.artigo else '' end ||
                            case when (paragrafo is not null) then ' - Parágrafo: ' || paragrafo else '' end ||
                            case when (amparo_legal.inciso is not null) then ' - Inciso: ' || amparo_legal.inciso else '' end ||
                            case when (alinea is not null) then ' - Alinea: ' || alinea else '' end
                            as campo_api_amparo")
                ])
                    ->where('amparo_legal.codigo', '<>', '');

                $this->verificaAtributosMinutaCompraQuery('', $querySemLei, '');

                $querySemLei->join('compras', 'compras.modalidade_id', '=', 'amparo_legal.modalidade_id')
                    ->join('minutaempenhos', 'minutaempenhos.compra_id', '=', 'compras.id')
                    ->where('minutaempenhos.id', $this->id)
                    ->groupBy('amparo_legal.id');

                if ($querySemLei->count() > 0) {
                    return $querySemLei->pluck('campo_api_amparo', 'amparolegal.id')->toArray();
                }
            }
        }

        return $query->pluck('campo_api_amparo', 'amparolegal.id')->toArray();
    }


    public function retornaAmparoPorMinutadeContrato()
    {
        return AmparoLegal::select(['amparo_legal.id', DB::raw("ato_normativo ||
                    case when (artigo is not null)  then ' - Artigo: ' || artigo else '' end ||
                    case when (paragrafo is not null)  then ' - Parágrafo: ' || paragrafo else '' end ||
                    case when (amparo_legal.inciso is not null)  then ' - Inciso: ' || amparo_legal.inciso else '' end ||
                    case when (alinea is not null)  then ' - Alinea: ' || alinea else '' end
                    as campo_api_amparo")
        ])->join('contratos', 'contratos.modalidade_id', '=', 'amparo_legal.modalidade_id')
            ->join('minutaempenhos', 'minutaempenhos.contrato_id', '=', 'contratos.id')
            ->where('minutaempenhos.id', $this->id)
            ->where('amparo_legal.codigo', '<>', '')
            ->pluck('campo_api_amparo', 'amparolegal.id')->toArray();
    }

    /**
     * Método Necessário para mostrar valor escolhido do campo multiselect após submeter
     * quando o attribute o campo estiver referenciando um alias na consulta da API
     * obrigatório quando utilizar campo select2_from_ajax_multiple_alias
     * @return  string nome_minuta_empenho
     */
    public function retornaConsultaMultiSelect($item)
    {
        $minuta = $this
            ->select(['minutaempenhos.id',
                DB::raw("CONCAT(minutaempenhos.mensagem_siafi, ' - ', to_char(data_emissao, 'DD/MM/YYYY')  )
                                 as nome_minuta_empenho")])
            ->distinct('minutaempenhos.id')
            ->join('compras', 'minutaempenhos.compra_id', '=', 'compras.id')
            ->join('codigoitens', 'codigoitens.id', '=', 'compras.modalidade_id')
            ->join('unidades', 'minutaempenhos.unidade_id', '=', 'unidades.id')
            ->leftJoin('contrato_minuta_empenho_pivot', 'minutaempenhos.id', '=', 'contrato_minuta_empenho_pivot.minuta_empenho_id')
            ->where('minutaempenhos.id', $item->id)
            ->first();

        return $minuta->nome_minuta_empenho;
    }

    public function atualizaFornecedorCompra($fornecedor_id)
    {
        $this->fornecedor_compra_id = $fornecedor_id;
        $this->fornecedor_empenho_id = $fornecedor_id;
        $this->etapa = 3;
        $this->update();
    }

    public function getUnidade() //Unidade Gestora => 'Unidade da Minuta do Empenho'
    {

        $unidade = $this->unidade_id()->first();
        return $unidade->codigo . ' - ' . $unidade->nomeresumido;
    }

    public function getUgEmitente() //UG Emitente => 'Unidade Emitente do Empenho'
    {
        if (isset($this->saldo_contabil)) {
            $ug = $this->saldo_contabil->unidade_id()->first();
            return $ug->codigo . ' - ' . $ug->nomeresumido;
        }
        return '';
    }

    public function getUnidadeCompra()
    {
        $unidade = $this->compra->unidade_origem()->first();
        return $unidade->codigo . ' - ' . $unidade->nomeresumido;
    }

    public function getSituacao()
    {
        switch ($this->situacao->descricao) {

            case 'AGUARDANDO ANULAÇÃO':

                return $this->formatarTextoParaAguardandoAnulacao();

            default:
                return $this->situacao->descricao;
        }

        return $this->situacao->descricao;
    }

    /**
     * Retorna a situação da remessa recebida
     * @param string $remessa_id
     * @return String
     */
    public function getSituacaoRemessa(string $remessa_id): string
    {
        return $this->remessa()->where('id', $remessa_id)->select('situacao_id')->first()->situacao->descricao;
    }

    public function getFornecedorEmpenho()
    {
        $fornecedor = $this->fornecedor_empenho()->first();
        if ($fornecedor) {
            return $fornecedor->cpf_cnpj_idgener . ' - ' . $fornecedor->nome;
        }
        return '';
    }

    /**
     * Retorna descrição do Tipo do Empenho
     *
     * @return string
     */
    public function getTipoEmpenho()
    {
        return $this->tipo_empenho->descricao ?? '';
    }

    public function getTipoEmpenhoPor()
    {
        return $this->tipo_empenhopor->descricao ?? '';
    }

    /**
     * Retorna descrição do Amparo Legal
     *
     * @return string
     */
    public function getAmparoLegal()
    {

        if (isset($this->amparo_legal)) {
            $artigo = isset($this->amparo_legal->artigo) ? ' - Artigo: ' . $this->amparo_legal->artigo : '';
            $paragrafo = isset($this->amparo_legal->paragrafo) ? ' - Parágrafo: ' . $this->amparo_legal->paragrafo : '';
            $inciso = isset($this->amparo_legal->inciso) ? ' - Inciso: ' . $this->amparo_legal->inciso : '';
            $alinea = isset($this->amparo_legal->alinea) ? ' - Alínea: ' . $this->amparo_legal->alinea : '';

            return $this->amparo_legal->ato_normativo . $artigo . $paragrafo . $inciso . $alinea;
        }
        return '';
    }

    public function getItens($minutaempenhos_remessa_id)
    {
        $tipo_contrato_id = $this->retornaIdCodigoItem('Tipo Empenho Por', 'Contrato');

        //SE FOR CONTRATO
        if ($this->tipo_empenhopor_id == $tipo_contrato_id) {
            return $this->contratoItemMinutaEmpenho()
                ->join(
                    'naturezasubitem',
                    'naturezasubitem.id',
                    '=',
                    'contrato_item_minuta_empenho.subelemento_id'
                )
                ->join(
                    'codigoitens',
                    'codigoitens.id',
                    '=',
                    'contrato_item_minuta_empenho.operacao_id'
                )
                ->join(
                    'contratoitens',
                    'contratoitens.id',
                    '=',
                    'contrato_item_minuta_empenho.contrato_item_id'
                )
                ->where('contrato_item_minuta_empenho.minutaempenhos_remessa_id', $minutaempenhos_remessa_id)
                ->select(
                    DB::raw('contratoitens.numero_item_compra              AS "numeroItemCompra"'),
                    DB::raw('contrato_item_minuta_empenho.numseq                 AS "numeroItemEmpenho"'),
                    DB::raw('CEIL(contrato_item_minuta_empenho.quantidade)             AS "quantidadeEmpenhada"'),
                    DB::raw('naturezasubitem.codigo AS subelemento'),
                    DB::raw('LEFT(codigoitens.descres, 1)     AS "tipoEmpenhoOperacao"'),
                    DB::raw('contratoitens.valorunitario AS "valorUnitarioItem"'),
                    DB::raw('NULL AS "tipoUASG"'),
                    DB::raw('(SELECT SUM(valor)
                        FROM contrato_item_minuta_empenho cime
                        WHERE cime.minutaempenho_id = contrato_item_minuta_empenho.minutaempenho_id
                          AND cime.minutaempenhos_remessa_id =
                              contrato_item_minuta_empenho.minutaempenhos_remessa_id) AS "valorTotalEmpenho"
                    ')
                )
                ->get();
        }

        return
            $this->compraItemMinutaEmpenho()
                ->join(
                    'naturezasubitem',
                    'naturezasubitem.id',
                    '=',
                    'compra_item_minuta_empenho.subelemento_id'
                )
                ->join(
                    'codigoitens',
                    'codigoitens.id',
                    '=',
                    'compra_item_minuta_empenho.operacao_id'
                )
                ->join(
                    'compra_items',
                    'compra_items.id',
                    '=',
                    'compra_item_minuta_empenho.compra_item_id'
                )
                ->join(
                    'compra_item_fornecedor',
                    'compra_item_fornecedor.compra_item_id',
                    '=',
                    'compra_items.id'
                )
                ->join(
                    'compra_item_unidade',
                    'compra_item_unidade.compra_item_id',
                    '=',
                    'compra_items.id'
                )
                ->where('compra_item_minuta_empenho.minutaempenhos_remessa_id', $minutaempenhos_remessa_id)
                ->select(
                    DB::raw('compra_items.numero              AS "numeroItemCompra"'),
                    DB::raw('compra_item_minuta_empenho.numseq                 AS "numeroItemEmpenho"'),
                    DB::raw('CEIL(compra_item_minuta_empenho.quantidade)             AS "quantidadeEmpenhada"'),
                    DB::raw('naturezasubitem.codigo AS subelemento'),
                    DB::raw('LEFT(codigoitens.descres, 1)     AS "tipoEmpenhoOperacao"'),
                    DB::raw('compra_item_fornecedor.valor_unitario AS "valorUnitarioItem"'),
                    DB::raw('compra_item_unidade.tipo_uasg AS "tipoUASG"'),
                    DB::raw('(SELECT SUM(valor)
                        FROM compra_item_minuta_empenho cime
                        WHERE cime.minutaempenho_id = compra_item_minuta_empenho.minutaempenho_id
                          AND cime.minutaempenhos_remessa_id =
                              compra_item_minuta_empenho.minutaempenhos_remessa_id) AS "valorTotalEmpenho"
                    ')
                )
                ->get();
    }

    public function getLinkPNCP()
    {
        $codigoExcluido = Codigoitem::whereHas('codigo', function ($q) {
            $q->where('descricao', 'Situação Envia Dados PNCP');
        })->where('descres', 'EXCLUIDO')->first()->id;

        $pncp = @EnviaDadosPncp::where([
            ['pncpable_type', MinutaEmpenho::class],
            ['pncpable_id', '=', $this->id],
            ['situacao', '<>', $codigoExcluido],
            ['sequencialPNCP', '<>', null]
        ])->oldest()->first();

        if (isset($pncp->sequencialPNCP) && isset($pncp->link_pncp)) {
            /*Mapa:
            de: [0]https://[2]pncp.gov.br/pncp-api/v1/orgaos/[6]10626896000172/contratos/[8]2022/[9]42
            de: [0]00489828000155-[1]2-[2]000032/[1]2022
            para: https://pncp.gov.br/app/contratos/10626896000172/2022/42
            */
            $host = explode("/", $pncp->link_pncp);
            $arr = explode("-", str_replace('/', '-', $pncp->sequencialPNCP));
            $link_pncp = $host[0] . '//' . $host[2] . '/app/contratos/' . $arr[0] . '/' . $arr[3] . '/' . $arr[2];
            return $link_pncp;
        } else {
            return 'disabled';
        }
    }

    public function serializaPNCP($cnpjCompra, $sequencialCompra)
    {
        // verificar numero_cipi antes de colocar no array
        if (is_null($this->numero_cipi)) {
            $numero_cipi = null;
        } else {
            $numero_cipi = $this->numero_cipi;
        }
        return [
            'cnpjCompra' => $cnpjCompra,
            'anoCompra' => explode('/', $this->compra->numero_ano)[1],
            'sequencialCompra' => intval($sequencialCompra),
            'tipoContratoId' => config('api-pncp.tipo_contrato')["99"],//Constante
            'numeroContratoEmpenho' => $this->mensagem_siafi,
            'anoContrato' => explode('NE', $this->mensagem_siafi)[0],
            'processo' => $this->processo,
            'categoriaProcessoId' => config('api-pncp.categoria_contrato')["Serviços"],//["Empenho Força de Contrato"],//Constante
            'niFornecedor' => $this->trataFornecedor(),
            'tipoPessoaFornecedor' => config('api-pncp.tipo_fornecedor')[$this->fornecedor_empenho->tipo_fornecedor],
            'nomeRazaoSocialFornecedor' => $this->fornecedor_empenho->nome,
            'receita' => false,
            'codigoUnidade' => $this->saldo_contabil->unidade_id()->first()->codigo,
            'objetoContrato' => $this->descricao,
            'valorInicial' => $this->valor_total,
            'numeroParcelas' => 1,
            'valorParcela' => $this->valor_total,
            'valorGlobal' => $this->valor_total,
            'dataAssinatura' => $this->data_emissao,
            'dataVigenciaInicio' => $this->data_emissao,
            'dataVigenciaFim' => explode('NE', $this->mensagem_siafi)[0] . '-12-31',
            'valorAcumulado' => $this->valor_total,
            'cnpjOrgaoSubRogado' => null,
            'codigoUnidadeSubRogada' => null,
            'niFornecedorSubContratado' => null,
            'tipoPessoaFornecedorSubContratado' => null,
            'nomeRazaoSocialFornecedorSubContratado' => null,
            'informacaoComplementar' => $this->informacao_complementar,
            'identificadorCipi' => @$numero_cipi
        ];
    }

    public function trataFornecedor()
    {
        $retorno = null;
        switch ($this->fornecedor_empenho->tipo_fornecedor) {
            case 'UG':
                $ug = Unidade::where($this->fornecedor_empenho->cpf_cnpj_idgener, 'codigo')->first();
                $retorno = @$ug->cnpj;
                break;
            case 'IDGENERICO':
                $retorno = $this->fornecedor_empenho->cpf_cnpj_idgener;
                break;
            default:
                $retorno = preg_replace("/[^0-9]/", "", $this->fornecedor_empenho->cpf_cnpj_idgener);
        }
        return $retorno;
    }

    public function possuiAmparo($amparo)
    {

        if (isset($this->amparo_legal)) {
            if ($this->amparo_legal->ato_normativo == $amparo) {
                return true;
            }
        }
        return false;
    }

    public function serializaArquivoPNCP()
    {
        $dados_empenho = [
            'ugemitente' => $this->saldo_contabil->unidade_id()->first()->codigo,
            'anoempenho' => substr($this->mensagem_siafi, 0, 4),
            'numempenho' => (int)substr($this->mensagem_siafi, 6, 6),
        ];

        $nome_arquivo = $dados_empenho['ugemitente'] . '_' . $dados_empenho['anoempenho'] . 'NE' . str_pad($dados_empenho['numempenho'], 6, "0", STR_PAD_LEFT);
        $path = 'empenhos_pdf/' . $dados_empenho['ugemitente'] . '_' . $dados_empenho['anoempenho'] . 'NE' . str_pad($dados_empenho['numempenho'], 6, "0", STR_PAD_LEFT) . '/';

        $filepath = config('app.app_path') . "storage/app";
        return $filepath . "/" . $path . $nome_arquivo . '.pdf';
    }

    public function buscaMinutasEmpenhosPorUasgEAno($uasg, $ano, $dt_alteracao_min, $dt_alteracao_max)
    {

        $minutas = self::from('minutaempenhos')
            ->select(
                'minutaempenhos.id as id_minuta',
                'codigoitens_tipo_empenho_minuta.descricao as tipo_minuta',
                'unidade_uasg_minuta.codigo as uasg_minuta',
                'minutaempenhos.data_emissao AS data_emissao_minuta',
                'unidade_ug_emitente.codigo AS ug_emitente',
                'minutaempenhos.mensagem_siafi AS numero_empenho',
                'fornecedores.cpf_cnpj_idgener AS cpf_cnpj_fornecedor_empenho',
                'fornecedores.nome AS nome_fornecedor_empenho',
                'codigoitens_tipo_empenho.descricao AS tipo_empenho',
                'unidade_uasg_compra.codigo AS uasg_compra',
                'codigoitens_modalidade_compra.descricao AS modalidade_compra',
                DB::raw('CASE
                            WHEN codigoitens_tipo_empenho_minuta.descricao = \'Suprimento\' THEN NULL
                            WHEN codigoitens_tipo_empenho_minuta.descricao = \'Contrato\' THEN contratos.licitacao_numero
                            ELSE compras.numero_ano
                    END AS numero_compra'),
                'unidade_uasg_beneficiaria.codigo AS uasg_beneficiaria',
                DB::raw('TRIM(minutaempenhos.numero_contrato) AS numero_contrato'),
                DB::raw('CASE
                            WHEN compras.numero_ano = \'99999/9999\' THEN NULL ELSE minutaempenhos.informacao_complementar
                    END AS chave'),
                DB::raw('TRIM(TO_CHAR(minutaempenhos.valor_total,\'999G999G990D99\')) AS valor_total_minuta')
            )
            ->join('unidades as unidade_uasg_minuta', 'unidade_uasg_minuta.id', 'minutaempenhos.unidade_id')
            ->join('saldo_contabil', 'saldo_contabil.id', 'minutaempenhos.saldo_contabil_id')
            ->join('unidades as unidade_ug_emitente', 'unidade_ug_emitente.id', 'saldo_contabil.unidade_id')
            ->join('fornecedores', 'fornecedores.id', 'minutaempenhos.fornecedor_empenho_id')
            ->join('codigoitens as codigoitens_tipo_empenho', 'codigoitens_tipo_empenho.id', 'minutaempenhos.tipo_empenho_id')
            ->join('compras', 'compras.id', 'minutaempenhos.compra_id')
            ->join('unidades as unidade_uasg_compra', 'unidade_uasg_compra.id', 'compras.unidade_origem_id')
            ->join('codigoitens as codigoitens_modalidade_compra', 'codigoitens_modalidade_compra.id', 'compras.modalidade_id')
            ->join('codigoitens as codigoitens_situacao_minuta', 'codigoitens_situacao_minuta.id', 'minutaempenhos.situacao_id')
            ->join('codigoitens as codigoitens_tipo_empenho_minuta', 'codigoitens_tipo_empenho_minuta.id', 'minutaempenhos.tipo_empenhopor_id')
            ->leftJoin('unidades as unidade_uasg_beneficiaria', 'unidade_uasg_beneficiaria.id', 'compras.uasg_beneficiaria_id')
            ->leftJoin('contratos', 'contratos.id', 'minutaempenhos.contrato_id')
            ->where('unidade_uasg_minuta.codigo', $uasg)
            ->whereRaw("to_char(minutaempenhos.data_emissao,'YYYY') = '$ano'")
            ->where('codigoitens_situacao_minuta.descricao', 'EMPENHO EMITIDO')
            ->when(($dt_alteracao_min != null and $dt_alteracao_max != null), function ($d) use ($dt_alteracao_min, $dt_alteracao_max) {
                $d->whereBetween('minutaempenhos.updated_at', [$dt_alteracao_min, $dt_alteracao_max]);
            })
            ->get();

        return $minutas;
    }

    public function duplicarMinuta()
    {
        //copy attributes
        $new = $this->replicate();

        //save model before you recreate relations (so it has an id)
        $new->push();

        //reset relations on EXISTING MODEL (this way you can control which ones will be loaded
        $this->relations = [];

        //load relations on EXISTING MODEL
        $this->load('compraItemMinutaEmpenho'/*,'relation2'*/);

        //re-sync everything
        foreach ($this->relations as $relationName => $values) {
            $new->{$relationName}()->saveMany($values);
        }
    }

    public function vinculadaContratoEmpenhoPivot()
    {
        return (bool)ContratoMinutaEmpenhoPivot::where('minuta_empenho_id', $this->id)->exists();
    }

    public function temContratoempenhoRelacionado()
    {
        $empenhoVinculado = Empenho::where('unidade_id', $this->saldo_contabil->unidade_id)
            ->where('numero', $this->mensagem_siafi)
            ->where('fornecedor_id', $this->fornecedor_empenho_id)
            ->first();

        //não tem empenho vinculado
        if (!$empenhoVinculado) {
            return false;
        }

        $temContratoEmpenho = Contratoempenho::where('empenho_id', $empenhoVinculado->id)->first();

        return (bool)$temContratoEmpenho;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    //148
    public function envia_dados_pncp()
    {
        return $this->morphMany(EnviaDadosPncp::class, 'pncpable');
    }

    public function amparo_legal()
    {
        return $this->belongsTo(AmparoLegal::class, 'amparo_legal_id');
    }

    public function compra()
    {
        return $this->belongsTo(Compra::class, 'compra_id');
    }

    public function empenho_dados()
    {
        return $this->hasMany(SfOrcEmpenhoDados::class, 'minutaempenho_id');
    }

    public function fornecedor_compra()
    {
        return $this->belongsTo(Fornecedor::class, 'fornecedor_compra_id');
    }

    public function fornecedor_empenho()
    {
        return $this->belongsTo(Fornecedor::class, 'fornecedor_empenho_id');
    }

    public function saldo_contabil()
    {
        return $this->belongsTo(SaldoContabil::class, 'saldo_contabil_id');
    }

    public function tipo_empenho()
    {
        return $this->belongsTo(Codigoitem::class, 'tipo_empenho_id');
    }

    public function tipo_empenhopor()
    {
        return $this->belongsTo(Codigoitem::class, 'tipo_empenhopor_id');
    }

    public function unidade_id()
    {
        return $this->belongsTo(Unidade::class, 'unidade_id');
    }

    public function passivo_anterior()
    {
        return $this->hasMany(ContaCorrentePassivoAnterior::class, 'minutaempenho_id');
    }

    public function remessa()
    {
        return $this->hasMany(MinutaEmpenhoRemessa::class, 'minutaempenho_id');
    }

    public function remessa_alteracao_fonte()
    {
        return $this->hasOne(MinutaEmpenhoRemessa::class, 'alteracao_fonte_minutaempenho_id');
    }

    public function situacao()
    {
        return $this->belongsTo(Codigoitem::class, 'situacao_id');
    }

    public function contratoMinutaEmpenhoPivot()
    {
        return $this->hasMany(ContratoMinutaEmpenhoPivot::class, 'minuta_empenho_id');
    }

    public function contrato()
    {
        return $this->belongsToMany(
            'App\Models\Contrato',
            'contrato_minuta_empenho_pivot',
            'minuta_empenho_id',
            'contrato_id'
        );
    }

    public function compraItemMinutaEmpenho()
    {
        return $this->hasMany(CompraItemMinutaEmpenho::class, 'minutaempenho_id');
    }

    public function contratoItemMinutaEmpenho()
    {
        return $this->hasMany(ContratoItemMinutaEmpenho::class, 'minutaempenho_id');
    }

    public function contrato_vinculado()
    {
        return $this->belongsTo(Contrato::class, 'contrato_id');
    }

    public static function existeArquivoMinutaEmpenho(array $dados_empenho, $retornaCaminho = null)
    {
        $nome_arquivo = $dados_empenho['ugemitente'] . '_' . $dados_empenho['anoempenho'] . 'NE'
            . str_pad($dados_empenho['numempenho'], 6, "0", STR_PAD_LEFT);
        $path = 'empenhos_pdf/' . $dados_empenho['ugemitente'] . '_' . $dados_empenho['anoempenho'] . 'NE'
            . str_pad($dados_empenho['numempenho'], 6, "0", STR_PAD_LEFT) . '/';

        if ($retornaCaminho) {
            return $path . $nome_arquivo . ".pdf";
        }

        return file_exists(config('app.app_path') . "storage/app/" . $path . $nome_arquivo . ".pdf");
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    public function getCompraModalidadeAttribute()
    {
        $compra = $this->compra()->first()->modalidade()->first();
        return $compra->descres . ' - ' . $compra->descricao;
    }

    public function getTipoCompraAttribute()
    {
        return $this->compra()->first()->tipo_compra()->first()->descricao;
    }

    public function getNumeroAnoAttribute()
    {
        return $this->compra()->first()->numero_ano;
    }

    public function getIncisoAttribute()
    {
        return $this->compra()->first()->inciso;
    }

    public function getLeiAttribute()
    {
        return $this->compra()->first()->lei;
    }

    public function getMaxRemessaAttribute()
    {
        return $this->remessa()->max('id');
    }

    public function getMaxNumeroRemessaAttribute()
    {
        return $this->remessa()->max('remessa');
    }

    public function getMaxEtapaRemessaAttribute()
    {
        return $this->remessa()->max('etapa');
    }

    public function getSituacaoDescricaoAttribute()
    {
        return $this->situacao->descricao;
    }

    public function getEmpenhoPorAttribute()
    {
        return $this->tipo_empenhopor->descricao ?? '';
    }

    public function getFornecedorEmpenhoCpfcnpjidgenerSessaoAttribute()
    {
        $fornecedor = $this->fornecedor_empenho()->first();
        return $fornecedor->cpf_cnpj_idgener ?? '';
    }

    public function textoMinutaForcaContrato()
    {
        return $this->minutaempenho_forcacontrato == true ? 'Sim' : 'Não';
    }

    public function getAnoExercicioAttribute()
    {
        return substr($this->mensagem_siafi, 0, 4);
    }

    public function getExercicioAtualAttribute()
    {
        return substr($this->mensagem_siafi, 0, 4) == date('Y');
    }

    /**
     * Retorna true caso ano da minuta seja igual ao ano atual
     * Ou caso a mensagem_siafi seja nula
     *
     * @return bool
     */
    public function getExercicioAtualOuMsgNullAttribute()
    {
        return is_null($this->mensagem_siafi) || substr($this->mensagem_siafi, 0, 4) == date('Y');
    }

    public function getMinutaAssinadaPncpAttribute()
    {
        $dados_empenho = [
            'ugemitente' => $this->saldo_contabil->unidade_id()->first()->codigo,
            'anoempenho' => substr($this->mensagem_siafi, 0, 4),
            'numempenho' => (int)substr($this->mensagem_siafi, 6, 6),
        ];

        return self::existeArquivoMinutaEmpenho($dados_empenho);
    }

    /**
     * Retorna Se a Minuta de Empenho foi enviada ao PNCP ou Não
     * @return bool
     */
    public function getMinutaEnviadaPncpAttribute()
    {
        $pncp = $this->envia_dados_pncp()->first();

        if ($pncp !== null) {
            if (($pncp->sequencialPNCP !== null) && ($pncp->status->descres != 'EXCLUIDO')) {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * Retorna Se a Fonte foi alterada ou Não
     * @return bool
     */
    public function getFonteAlteradaAttribute(): bool
    {
        return strpos($this->informacao_complementar, 'Anulada') !== false;
    }

    /**
     * Retorna o ID da minuta Anulada ou Null caso não tenha a fonte alterada
     *
     * @return int|null
     */
    public function getMinutaAnuladaIdAttribute(): ?int
    {
        $minutaAnulada = MinutaEmpenhoRemessa::select(['minutaempenho_id'])
            ->where('alteracao_fonte_minutaempenho_id', $this->id)
            ->first();
        if ($minutaAnulada === null) {
            return null;
        }
        return $minutaAnulada->minutaempenho_id;
    }

    /**
     * Verifica se a minuta tem registro na tabela envia_dados_pncp para usar na busca no metodo recuperaMinutasModalCriarContratoTipoEmpenho
     * @return bool
     */
    public static function verificaExisteRegistroEnviaPncp($minuta_id): bool
    {
        $minuta_id = $minuta_id;
        //confere se existe registro para a Minuta
        $registro_envia_pncp = EnviaDadosPncp::join(
            'codigoitens as cdi', function ($status) {
            $status->on('envia_dados_pncp.situacao', '=', 'cdi.id')
                ->where('descres', '!=', 'ASNPEN');
        })
            ->where([
                ['pncpable_type', MinutaEmpenho::class],
                ['pncpable_id', '=', $minuta_id],
            ])
            ->orderBy('envia_dados_pncp.created_at', 'ASC')
            ->first();

        if ($registro_envia_pncp) {
            return true;
        }
        return false;
    }

    /**
     * Verifica se a instância atual da minuta tem registro na tabela envia_dados_pncp para usar na ContatoCrudController
     * @return bool
     */
    public function verificaExisteRegistroEnviaPncpObj(): bool
    {
        //confere se existe registro para a Minuta
        $registro_envia_pncp = EnviaDadosPncp::
        join(
            'codigoitens as cdi', function ($status) {
            $status->on('envia_dados_pncp.situacao', '=', 'cdi.id')
                ->where('descres', '!=', 'ASNPEN');
        })
            ->where([
                ['pncpable_type', MinutaEmpenho::class],
                ['pncpable_id', '=', $this->id],
            ])
            ->orderBy('envia_dados_pncp.created_at', 'ASC')
            ->first();

        if ($registro_envia_pncp) {
            return true;
        }
        return false;
    }

    public function getSisppServicoAttribute(): bool
    {
        $result = $this->join('compra_item_minuta_empenho as cime', 'minutaempenhos.id', '=', 'cime.minutaempenho_id')
            ->join('compra_items as ci', 'cime.compra_item_id', '=', 'ci.id')
            ->join('compras as c', 'ci.compra_id', '=', 'c.id')
            ->join('codigoitens as tipo_compra', 'c.tipo_compra_id', '=', 'tipo_compra.id')
            ->join('codigoitens as tipo_itens', 'ci.tipo_item_id', '=', 'tipo_itens.id')
            ->where('minutaempenhos.id', $this->id)
            ->selectRaw("CASE WHEN tipo_compra.descricao = 'SISPP' AND tipo_itens.descricao = 'Servico' THEN true ELSE false END AS sispp_servico")
            ->first();

        return $result ? $result->sispp_servico : false;
    }

    public function verificaAtributosMinutaCompraQuery($lei, $query): void
    {
        $existeAmparoLegalEmLeiCompra = resolve(AmparoLegalService::class)
            ->existeAmparoLegalEmLeiCompra($lei);

        $query->where("complemento_14133", $existeAmparoLegalEmLeiCompra);
    }

    public static function getMinutaEmpenhoSubstitutivoContratoDiferenteLeiQuatorzeUmTresTres()
    {
        return Minutaempenho::join('compra_item_minuta_empenho', function ($join) {
            $join->on('minutaempenhos.id', 'compra_item_minuta_empenho.minutaempenho_id')
                ->whereNull('minutaempenhos.deleted_at');
        })
            ->join('minutaempenhos_remessa', 'compra_item_minuta_empenho.minutaempenhos_remessa_id', 'minutaempenhos_remessa.id')
            ->join('codigoitens', 'minutaempenhos.situacao_id', 'codigoitens.id')
            ->join('codigoitens AS ci1', 'minutaempenhos_remessa.situacao_id', 'ci1.id')
            ->join('amparo_legal', 'minutaempenhos.amparo_legal_id', 'amparo_legal.id')
            ->leftjoin('arquivo_generico', function ($query) {
                $query->on('arquivo_generico.arquivoable_id', 'minutaempenhos.id')
                    ->where('arquivoable_type', self::class);
            })
            ->where('codigoitens.descricao', 'EMPENHO EMITIDO')
            ->where('minutaempenhos.minutaempenho_forcacontrato', '1')
            ->whereNotNull('minutaempenhos.mensagem_siafi')
            ->whereRaw("minutaempenhos.updated_at >= DATE_TRUNC('month', CURRENT_DATE - INTERVAL '1 month')")
            ->whereNull('arquivo_generico.nome')
            ->distinct()
            ->select('minutaempenhos.*')
            ->get();
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
