<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RequisicoesNonce extends Model
{
    /**
     * @var string
     */
    protected $table = 'requisicoes_nonce';

    /**
     * @var string[]
     */
    protected $fillable = [
        'nonce_id',
        'requisicao_enviada',
        'resposta_api'
    ];

    protected $casts = [
        'requisicao_enviada' => 'array',
        'resposta_api' => 'array'
    ];

    protected $hidden = [
        'updated_at',
    ];
}
