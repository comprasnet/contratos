<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Migration extends Model
{
    use CrudTrait;

    protected $table = 'migrations';

}
