<?php
namespace App\Models;

use App\Models\Codigo;
use App\Models\Codigoitem;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Contratoconta extends Model
{
    protected $primaryKey = 'id';
    use CrudTrait;
    use LogsActivity;
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'contratocontas';
    protected $fillable = [
        'contrato_id', 'banco', 'conta', 'agencia', 'conta_corrente', 'fat_empresa',
        'percentual_grupo_a_13_ferias', 'percentual_submodulo22', 'percentual_grupo_a_13_ferias_codigoitens_id', 'percentual_submodulo_22_id',
        'is_conta_vinculada_pela_resolucao169_cnj', 'percentual_ferias', 'percentual_abono_ferias', 'percentual_multa_sobre_fgts', 'percentual_13',
        'saldo_encerramento','nome_responsavel_encerramento', 'cpf_responsavel_encerramento'
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getFatEmpresa(){
        if($this->is_conta_vinculada_pela_resolucao169_cnj){return 'Não se aplica.';}
        else{return $this->fat_empresa.'%';}
    }

    // verificar se a conta vinculada foi cadastrada pelo caderno de conta vinculada ou pela resolução 169 CNJ
    public function getTipoContaVinculada(){
        if($this->is_conta_vinculada_pela_resolucao169_cnj){
            return 'Resolução 169 CNJ';
        } else{
            return 'IN 05/2017';
        }      
    }

    // método que retorna os códigos bancários que estão salvos em codigoitens
    public static function getArrayComCodigosBancarios(){
        $array = Codigoitem::whereHas('codigo', function ($query) {
                $query->where('descricao', '=', 'Códigos Bancários');
            })
                ->where('visivel', true)
                ->orderBy('descricao')
                ->pluck('descricao', 'descres')
                ->toArray();
        return $array;
    }

    // método que recebe o banco, agência e conta e verifica se ainda não foram utilizados
    public static function verificarSeDadosAindaNaoExistemNaBase($request, $acao){
        $banco = $request->banco;
        $agencia = $request->agencia;
        $conta = $request->conta_corrente;

        if($acao == 'insert'){
            $quantidade = Contratoconta::where('banco', $banco)
                ->where('agencia', $agencia)
                ->where('conta_corrente', $conta)
                ->count();
        } elseif($acao == 'update'){
            $id = $request->id;

            $quantidade = Contratoconta::where('banco', $banco)
                ->where('agencia', $agencia)
                ->where('conta_corrente', $conta)
                ->where('id', '<>', $id)
                ->count();
        }
        if($quantidade > 0){return true;}
        return false;
    }

    // método que pega o valor informado do encargo (1, 2 ou 3%) e busca os percentuais do grupo A e do submódulo 2.2, referentes ao encargo
    public static function getDadosPercentuaisGrupoAESubmodulo22ByEncargoResolucao169Cnj($request){
        $arrayDados = array();
        $percentualSubmodulo22 = $request->get('percentual_submodulo22');

        // os percentuais para resolução, serão informados manualmente
        $percentual13 = $request->get('percentual_13');
        $percentualFerias = $request->get('percentual_ferias');
        $percentualAbonoDeFerias = $request->get('percentual_abono_ferias');
        $percentualMultaSobreFgts = $request->get('percentual_multa_sobre_fgts');

        // vamos calcular o percentual grupo A
        // (13º salário + Férias + Abono de Férias) * Submódulo 2.2 / 100 
        $percentualGrupoA = (( $percentual13 + $percentualFerias + $percentualAbonoDeFerias ) * $percentualSubmodulo22) / 100;

        $fatEmpresa = null;
        // vamos organizar o array que retornaremos
        $arrayDados['fat_empresa'] = $fatEmpresa;
        $arrayDados['percentual_grupo_a'] = $percentualGrupoA;
        $arrayDados['percentual_grupo_a_id'] = null;
        $arrayDados['percentual_submodulo_22'] = $percentualSubmodulo22;
        $arrayDados['percentual_submodulo_22_id'] = null;
        $arrayDados['percentual_ferias'] = $percentualFerias;
        $arrayDados['percentual_abono_ferias'] = $percentualAbonoDeFerias;
        $arrayDados['percentual_multa_sobre_fgts'] = $percentualMultaSobreFgts;
        $arrayDados['percentual_13'] = $percentual13;
        return $arrayDados;
    }

    // Este método retona um array com objetos de encargos desta conta vinculada (contratoconta) - para resolução 169 CNJ
    public static function getArrayEncargosByIdContratocontaParaResolucao169Cnj($idContratoconta){
        $objContratoConta = Contratoconta::where('id', $idContratoconta)->first();

        $obj13 = new \stdClass;
        $obj13->id = null;
        $obj13->descricao = '13º (décimo terceiro) salário';
        $obj13->percentual = $objContratoConta->percentual_13;

        $objMulta = new \stdClass;
        $objMulta->id = null;
        $objMulta->descricao = 'Multa sobre o FGTS para as rescisões sem justa causa';
        $objMulta->percentual = $objContratoConta->percentual_multa_sobre_fgts;

        $objFerias = new \stdClass;
        $objFerias->id = null;
        $objFerias->descricao = 'Férias';
        $objFerias->percentual = $objContratoConta->percentual_ferias;

        $objAbonoFerias = new \stdClass;
        $objAbonoFerias->id = null;
        $objAbonoFerias->descricao = 'Abono de Férias';
        $objAbonoFerias->percentual = $objContratoConta->percentual_abono_ferias;

        return $arrayObjetosEncargos = [
            $obj13, $objMulta, $objFerias, $objAbonoFerias
        ];

    }

    // método para Conta Vinculada pelo Caderno, que pega o valor informado do encargo (1, 2 ou 3%)
    // e busca os percentuais do grupo A e do submódulo 2.2, referentes ao encargo
    public static function getDadosPercentuaisGrupoAESubmodulo22ByEncargo($request){
        $arrayDados = array();
        $fatEmpresa = $request->get('fat_empresa');
        // vamos parametrizar o que iremos buscar em codigoitens
        if($fatEmpresa == '1'){$percentualGrupoA = '7.39'; $percentualSubmodulo22 = '34.8';}
        elseif($fatEmpresa == '2'){$percentualGrupoA = '7.60'; $percentualSubmodulo22 = '35.8';}
        elseif($fatEmpresa == '3'){$percentualGrupoA = '7.82'; $percentualSubmodulo22 = '36.8';}
        // buscar dados do grupo A
        $objDadosGrupoA = \DB::table('codigos as c')
            ->select('ci.*')
            ->where('c.descricao','=','Percentual Grupo A')
            ->join('codigoitens as ci', 'ci.codigo_id', '=', 'c.id')
            ->where('ci.descricao', '=', $percentualGrupoA)
            ->first();
        // buscar dados do submódulo 2.2
        $objDadosSubmodulo22 = \DB::table('codigos as c')
            ->select('ci.*')
            ->where('c.descricao','=','Percentual do Submódulo 2.2')
            ->join('codigoitens as ci', 'ci.codigo_id', '=', 'c.id')
            ->where('ci.descricao', '=', $percentualSubmodulo22)
            ->first();
        // vamos organizar o array que retornaremos
        $arrayDados['fat_empresa'] = $fatEmpresa;
        $arrayDados['percentual_grupo_a'] = $percentualGrupoA;
        $arrayDados['percentual_grupo_a_id'] = $objDadosGrupoA->id;
        $arrayDados['percentual_submodulo_22'] = $percentualSubmodulo22;
        $arrayDados['percentual_submodulo_22_id'] = $objDadosSubmodulo22->id;
        return $arrayDados;
    }

    public static function getIdContratocontaByidLancamento($idLancamento){
        $obj = \DB::table('lancamentos')
            ->select('contratocontas.id')
            ->where('lancamentos.id','=',$idLancamento)
            ->join('movimentacaocontratocontas', 'movimentacaocontratocontas.id', '=', 'lancamentos.movimentacao_id')
            ->join('contratocontas', 'contratocontas.id', '=', 'movimentacaocontratocontas.contratoconta_id')
            ->first();
        $idContratoConta = $obj->id;
        return $idContratoConta;
    }

    public function getIdContratoContaByIdContratoTerceirizado($idContratoTerceirizado){
        $obj = \DB::table('contratoterceirizados')
            ->select('contratocontas.id')
            ->where('contratoterceirizados.id','=',$idContratoTerceirizado)
            ->join('contratos', 'contratos.id', '=', 'contratoterceirizados.contrato_id')
            ->join('contratocontas', 'contratocontas.contrato_id', '=', 'contratos.id')
            ->first();
        $idContratoConta = $obj->id;
        return $idContratoConta;
    }

    public function alterarSituacaoFuncionárioParaDemitido($idContratoTerceirizado, $dataDemissao){
        $objContratoTerceirizado = Contratoterceirizado::where('id', '=', $idContratoTerceirizado)->first();
        $objContratoTerceirizado->situacao = 'f';
        $objContratoTerceirizado->data_fim = $dataDemissao;
        if( $objContratoTerceirizado->save() ){
            return true;
        } else {
            return false;
        }
    }

    public function alterarSituacaoFuncionárioParaRealocado($idContratoTerceirizado){
        $objContratoTerceirizado = Contratoterceirizado::where('id', '=', $idContratoTerceirizado)->first();
        $objContratoTerceirizado->realocado = 't';
        if( $objContratoTerceirizado->save() ){
            return true;
        } else {
            return false;
        }
    }

    // retorna ativa ou encerrada, de acordo com a data de encerramento.
    public function getStatusDaConta(){
        if($this->data_encerramento == null){return 'Ativa';}
        else{
            $dataEncerramento = $this->data_encerramento;
            return 'Encerrada';
        }
    }

    public function getStatusDaContaParaMostrarNaColuna(){
        if($this->data_encerramento == null){return 'Ativa';}
        else{
            $dataEncerramento = $this->data_encerramento;
            return '<font color="red">Encerrada em '.$this->data_encerramento.'</font>';
        }
    }

    public function getSaldoContratoContaPorIdEncargoPorContratoTerceirizado($idContratoTerceirizado, $idEncargo){
        $tipoIdEncargo = self::getTipoIdEncargoByIdEncargo($idEncargo);
        return $saldo = self::getSaldoContratoContaPorTipoEncargoPorContratoTerceirizado($idContratoTerceirizado, $tipoIdEncargo);
    }

    /**
     * O saldo para grupo A será pego de forma diferente
     * grupo A será armazenado em contrato conta e não mais em encargo,
     * pois irá variar de conta para conta.
     */
    public function getSaldoContratoContaGrupoAPorContratoTerceirizado($idContratoTerceirizado){
        $saldoDeposito = self::getSaldoDepositoGrupoAPorContratoTerceirizado($idContratoTerceirizado);
        $saldoRetirada = self::getSaldoRetiradaGrupoAPorContratoTerceirizado($idContratoTerceirizado);
        return $saldo = ($saldoDeposito - $saldoRetirada);
    }

    public function getSaldoDepositoGrupoAPorContratoTerceirizado($idContratoTerceirizado){
        $saldoDeposito = \DB::table('lancamentos')
            ->join('movimentacaocontratocontas', 'lancamentos.movimentacao_id', '=', 'movimentacaocontratocontas.id')
            ->join('codigoitens', 'codigoitens.id', '=', 'movimentacaocontratocontas.tipo_id')
            ->where('codigoitens.descricao','=','Provisão')
            ->join('codigos', 'codigos.id', '=', 'codigoitens.codigo_id')
            ->where('codigos.descricao','=','Tipo Movimentação')
            ->where('lancamentos.encargo_id', null)
            ->where('lancamentos.contratoterceirizado_id','=',$idContratoTerceirizado)
            ->sum('lancamentos.valor');
        return $saldoDeposito = number_format(floatval($saldoDeposito), 2, '.', '');
    }

    public function getSaldoRetiradaGrupoAPorContratoTerceirizado($idContratoTerceirizado){
        $saldoRetirada = \DB::table('lancamentos')
            ->join('movimentacaocontratocontas', 'lancamentos.movimentacao_id', '=', 'movimentacaocontratocontas.id')
            ->join('codigoitens', 'codigoitens.id', '=', 'movimentacaocontratocontas.tipo_id')
            ->where('codigoitens.descricao','=','Liberação')
            ->join('codigos', 'codigos.id', '=', 'codigoitens.codigo_id')
            ->where('codigos.descricao','=','Tipo Movimentação')
            ->where('lancamentos.encargo_id', null)
            ->where('lancamentos.contratoterceirizado_id','=',$idContratoTerceirizado)
            ->sum('lancamentos.valor');
        return $saldoRetirada = number_format(floatval($saldoRetirada), 2, '.', '');
    }

    /**
     * caso a conta vinculada seja pela resolução 169 cnj, o valor do grupo A estará salvo
     * diretamente na tabela lançamento com o nome Incidência....
     * Não será possível buscar o grupo A, apenas pelo encargo_id = null, pois todos os encargo_id serão null para este tipo de conta vinculada
     */
    public function getSaldoContratoContaGrupoAPorContratoTerceirizadoParaResolucao169Cnj($idContratoTerceirizado){
        $saldoDeposito = self::getSaldoDepositoGrupoAPorContratoTerceirizadoParaResolucao169Cnj($idContratoTerceirizado);
        $saldoRetirada = self::getSaldoRetiradaGrupoAPorContratoTerceirizadoParaResolucao169Cnj($idContratoTerceirizado);
        return $saldo = ($saldoDeposito - $saldoRetirada);
    }

    public function getSaldoDepositoGrupoAPorContratoTerceirizadoParaResolucao169Cnj($idContratoTerceirizado){
        $saldoDeposito = \DB::table('lancamentos')
            ->join('movimentacaocontratocontas', 'lancamentos.movimentacao_id', '=', 'movimentacaocontratocontas.id')
            ->join('codigoitens', 'codigoitens.id', '=', 'movimentacaocontratocontas.tipo_id')
            ->where('codigoitens.descricao','=','Provisão')
            ->join('codigos', 'codigos.id', '=', 'codigoitens.codigo_id')
            ->where('codigos.descricao','=','Tipo Movimentação')
            ->where('lancamentos.encargo_id', null)
            ->where('lancamentos.encargo_nome', 'Incidência do Grupo A sobre férias, abono e 13o. salário')
            ->where('lancamentos.contratoterceirizado_id','=',$idContratoTerceirizado)
            ->sum('lancamentos.valor');
        return $saldoDeposito = number_format(floatval($saldoDeposito), 2, '.', '');
    }

    public function getSaldoRetiradaGrupoAPorContratoTerceirizadoParaResolucao169Cnj($idContratoTerceirizado){
        $saldoRetirada = \DB::table('lancamentos')
            ->join('movimentacaocontratocontas', 'lancamentos.movimentacao_id', '=', 'movimentacaocontratocontas.id')
            ->join('codigoitens', 'codigoitens.id', '=', 'movimentacaocontratocontas.tipo_id')
            ->where('codigoitens.descricao','=','Liberação')
            ->join('codigos', 'codigos.id', '=', 'codigoitens.codigo_id')
            ->where('codigos.descricao','=','Tipo Movimentação')
            ->where('lancamentos.encargo_id', null)
            ->where('lancamentos.encargo_nome', 'Incidência do Grupo A sobre férias, abono e 13o. salário')
            ->where('lancamentos.contratoterceirizado_id','=',$idContratoTerceirizado)
            ->sum('lancamentos.valor');
        return $saldoRetirada = number_format(floatval($saldoRetirada), 2, '.', '');
    }

    public function getTipoIdEncargoByIdEncargo($idEncargoInformado){
        return $id = Encargo::where('id', '=', $idEncargoInformado)->first()->tipo_id;
    }

    public static function getSaldoDepositoPorContratoTerceirizado($idContratoTerceirizado){
        $saldoDeposito = \DB::table('lancamentos')
            ->join('movimentacaocontratocontas', 'lancamentos.movimentacao_id', '=', 'movimentacaocontratocontas.id')
            ->join('codigoitens', 'codigoitens.id', '=', 'movimentacaocontratocontas.tipo_id')
            ->where('codigoitens.descricao','=','Provisão')
            ->join('codigos', 'codigos.id', '=', 'codigoitens.codigo_id')
            ->where('codigos.descricao','=','Tipo Movimentação')
            ->where('lancamentos.contratoterceirizado_id','=',$idContratoTerceirizado)
            ->sum('lancamentos.valor');
        return $saldoDeposito = number_format(floatval($saldoDeposito), 2, '.', '');
    }

    public static function getSaldoRetiradaPorContratoTerceirizado($idContratoTerceirizado){
        $saldoRetirada = \DB::table('lancamentos')
            ->join('movimentacaocontratocontas', 'lancamentos.movimentacao_id', '=', 'movimentacaocontratocontas.id')
            ->join('codigoitens', 'codigoitens.id', '=', 'movimentacaocontratocontas.tipo_id')
            ->join('codigos', 'codigos.id', '=', 'codigoitens.codigo_id')
            ->where('codigos.descricao','=','Tipo Movimentação')
            ->where('codigoitens.descricao','=','Liberação')
            ->where('lancamentos.contratoterceirizado_id','=',$idContratoTerceirizado)
            ->sum('lancamentos.valor');
        return $saldoRetirada = number_format(floatval($saldoRetirada), 2, '.', '');
    }

    public static function getSaldoContratoContaPorContratoTerceirizado($idContratoTerceirizado){
        $saldoDeposito = self::getSaldoDepositoPorContratoTerceirizado($idContratoTerceirizado);
        $saldoRetirada = self::getSaldoRetiradaPorContratoTerceirizado($idContratoTerceirizado);
        return $saldo = ($saldoDeposito - $saldoRetirada);
    }

    public function getSaldoContratoContaPorNomeEncargoPorContratoTerceirizadoResolucao169Cnj($idContratoTerceirizado, $nomeEncargo){
        $saldoDeposito = self::getSaldoDepositoPorNomeEncargoPorContratoTerceirizadoResolucao169Cnj($idContratoTerceirizado, $nomeEncargo);
        $saldoRetirada = self::getSaldoRetiradaPorNomeEncargoPorContratoTerceirizadoResolucao169Cnj($idContratoTerceirizado, $nomeEncargo);
        return $saldo = ($saldoDeposito - $saldoRetirada);
    }

    public function getSaldoDepositoPorNomeEncargoPorContratoTerceirizadoResolucao169Cnj($idContratoTerceirizado, $nomeEncargo){
        $saldoDeposito = \DB::table('lancamentos')
            ->join('movimentacaocontratocontas', 'lancamentos.movimentacao_id', '=', 'movimentacaocontratocontas.id')
            ->join('codigoitens', 'codigoitens.id', '=', 'movimentacaocontratocontas.tipo_id')
            ->where('codigoitens.descricao','=','Provisão')
            ->join('codigos', 'codigos.id', '=', 'codigoitens.codigo_id')
            ->where('codigos.descricao','=','Tipo Movimentação')
            ->where('lancamentos.encargo_nome', $nomeEncargo)
            ->where('lancamentos.contratoterceirizado_id','=',$idContratoTerceirizado)
            ->sum('lancamentos.valor');
        return $saldoDeposito = number_format(floatval($saldoDeposito), 2, '.', '');
    }

    public function getSaldoRetiradaPorNomeEncargoPorContratoTerceirizadoResolucao169Cnj($idContratoTerceirizado, $nomeEncargo){
        $saldoRetirada = \DB::table('lancamentos')
            ->join('movimentacaocontratocontas', 'lancamentos.movimentacao_id', '=', 'movimentacaocontratocontas.id')
            ->join('codigoitens', 'codigoitens.id', '=', 'movimentacaocontratocontas.tipo_id')
            ->where('codigoitens.descricao','=','Liberação')
            ->join('codigos', 'codigos.id', '=', 'codigoitens.codigo_id')
            ->where('codigos.descricao','=','Tipo Movimentação')
            ->where('lancamentos.encargo_nome', $nomeEncargo)
            ->where('lancamentos.contratoterceirizado_id','=',$idContratoTerceirizado)
            ->sum('lancamentos.valor');
        return $saldoRetirada = number_format(floatval($saldoRetirada), 2, '.', '');
    }

    public function getSaldoContratoContaPorTipoEncargoPorContratoTerceirizado($idContratoTerceirizado, $tipo_id){
        $saldoDeposito = self::getSaldoDepositoPorTipoEncargoPorContratoTerceirizado($idContratoTerceirizado, $tipo_id);
        $saldoRetirada = self::getSaldoRetiradaPorTipoEncargoPorContratoTerceirizado($idContratoTerceirizado, $tipo_id);
        return $saldo = ($saldoDeposito - $saldoRetirada);
    }

    public function getSaldoDepositoPorTipoEncargoPorContratoTerceirizado($idContratoTerceirizado, $tipo_id){
        $saldoDeposito = \DB::table('lancamentos')
            ->join('movimentacaocontratocontas', 'lancamentos.movimentacao_id', '=', 'movimentacaocontratocontas.id')
            ->join('codigoitens', 'codigoitens.id', '=', 'movimentacaocontratocontas.tipo_id')
            ->where('codigoitens.descricao','=','Provisão')
            ->join('codigos', 'codigos.id', '=', 'codigoitens.codigo_id')
            ->where('codigos.descricao','=','Tipo Movimentação')
            ->join('encargos', 'encargos.id', '=', 'lancamentos.encargo_id')
            ->where('encargos.tipo_id', '=', $tipo_id)
            ->where('lancamentos.contratoterceirizado_id','=',$idContratoTerceirizado)
            ->sum('lancamentos.valor');
        return $saldoDeposito = number_format(floatval($saldoDeposito), 2, '.', '');
    }

    public function getSaldoRetiradaPorTipoEncargoPorContratoTerceirizado($idContratoTerceirizado, $tipo_id){
        $saldoRetirada = \DB::table('lancamentos')
            ->join('movimentacaocontratocontas', 'lancamentos.movimentacao_id', '=', 'movimentacaocontratocontas.id')
            ->join('codigoitens', 'codigoitens.id', '=', 'movimentacaocontratocontas.tipo_id')
            ->where('codigoitens.descricao','=','Liberação')
            ->join('codigos', 'codigos.id', '=', 'codigoitens.codigo_id')
            ->where('codigos.descricao','=','Tipo Movimentação')
            ->join('encargos', 'encargos.id', '=', 'lancamentos.encargo_id')
            ->where('encargos.tipo_id', '=', $tipo_id)
            ->where('lancamentos.contratoterceirizado_id','=',$idContratoTerceirizado)
            ->sum('lancamentos.valor');
        return $saldoRetirada = number_format(floatval($saldoRetirada), 2, '.', '');
    }

    public static function getSaldoContaVinculadaByIdContrato($idContrato){
        $arrayTodosContratosTerceirizadosByIdContrato = Contratoterceirizado::where('contrato_id', $idContrato)->get();
        $saldoTotal = 0;
        foreach($arrayTodosContratosTerceirizadosByIdContrato as $objContratoTerceirizado){
            $idContratoTerceirizado = $objContratoTerceirizado->id;
            $saldoContratoTerceirizado = self::getSaldoContratoContaPorContratoTerceirizado($idContratoTerceirizado);
            $saldoTotal = ( $saldoTotal + $saldoContratoTerceirizado );
        }
        return $saldoTotal;

        return number_format($saldoTotal, 2, ',', '.');
    }

    public function getSaldoContratoContaParaColunas(){
        $idContrato = $this->contrato_id;
        $arrayTodosContratosTerceirizadosByIdContrato = Contratoterceirizado::where('contrato_id', $idContrato)->get();
        $saldoTotal = 0;
        foreach($arrayTodosContratosTerceirizadosByIdContrato as $objContratoTerceirizado){
            $idContratoTerceirizado = $objContratoTerceirizado->id;
            $saldoContratoTerceirizado = self::getSaldoContratoContaPorContratoTerceirizado($idContratoTerceirizado);
            $saldoTotal = ( $saldoTotal + $saldoContratoTerceirizado );
        }
        return number_format($saldoTotal, 2, ',', '.');
    }
    
}
