<?php

namespace App\Models;

use App\Models\RecomposicaoCusto\ModeloDocumento;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Codigoitem extends Model
{
    use CrudTrait;
    use LogsActivity;
    use SoftDeletes;

    protected static $logFillable = true;
    protected static $logName = 'codigoitens';


    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'codigoitens';
    protected $fillable = ['codigo_id', 'descres', 'descricao'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */


    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function codigo()
    {
        return $this->belongsTo(Codigo::class, 'codigo_id');
    }

    public function contratohistoricos()
    {
        return $this->belongsToMany(
            'App\Models\Contratohistorico',
            'contratohistoricoqualificacao',
            'tipo_id',
            'contratohistorico_id'
        );
    }

    function ModelosDeDocumento()
    {
        return $this->hasMany(ModeloDocumento::class, 'codigoitens_id');
    }

    public function execsfsituacoes()
    {
        return $this->belongsToMany(Execsfsituacao::class, 'execsfsituacao_tipodoc', 'tipodoc_id', 'execsfsituacao_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function getFullDescAttribute()
    {
        return "{$this->descres} - {$this->descricao}";
    }

    public static function obterCodigoItemPorDescricaoEId($id)
    {
        $codigoItemByDescriptionAndId = self::where('descricao', 'like', '%Termo%')
            ->where('id', $id)
            ->first();

        if ($codigoItemByDescriptionAndId) {

            return true;
        }

        return false;
    }
}
