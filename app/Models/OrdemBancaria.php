<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class OrdemBancaria extends Model
{
    use LogsActivity;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'ordens_bancarias';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = [
        'sfpadrao_id',
        'unidade_id',
        'numero',
        'emissao',
        'fornecedor_id',
        'observacao',
        'tipo_ob',
        'processo',
        'cancelamentoob',
        'numeroobcancelamento',
        'valor',
        'documentoorigem',
        'bancodestino',
        'agenciadestino',
        'contadestino',
        'contratofaturas_id',
        'domicilio_bancario_id',
        'updated_at'
//        'bancodestino',
//        'agenciadestino',
//        'contadestino',

    ];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function obxne()
    {
        return $this->hasMany(ObxNe::class, 'ordembancaria_id');
    }

    public function domicilioBancario()
    {
        return $this->belongsTo(DomicilioBancario::class, 'domicilio_bancario_id');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    public function getValorAttribute($value)
    {
        if (empty($value)) {
            return 'R$ 0,00';
        }

        return 'R$ ' . number_format($value, 2, ',', '.');
    }

    public function getValorNaoFormatadoAttribute()
    {
        return $this->attributes['valor'];
    }

    public function getEmissaoAttribute($value)
    {
        if (empty($value)) {
            return '00/00/0000';
        }

        return date('d/m/Y', strtotime($value));
    }
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
