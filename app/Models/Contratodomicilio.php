<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Contratodomicilio extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'antecipagov';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = [
        'contrato_id',
        'num_operacao',
        'domicilio_bancario_id'
    ];

    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getSituacao(){
        return $descricao = Contratodomicilio::where('antecipagov.id', $this->id)
            ->join('codigoitens as ci', 'ci.id', 'antecipagov.situacao_id')
            ->select('antecipagov.*', 'ci.descricao')
            ->first()->descricao;
    }
    public function getContrato()
    {
        if($this->contrato_id){
            $contrato = Contrato::find($this->contrato_id);
            return $contrato->numero;
        }else{
            if($this->id && $this->conta_bancaria){
                // aqui o $this não se refere ao contrato_antecipagov e sim ao antecipagov. (não deu certo fazer o join)
                $dadosContratoId = \DB::select( ' select contrato_antecipagov.contrato_id from contrato_antecipagov where contrato_antecipagov.antecipagov_id =  ' . $this->id );
                $contrato_id = $dadosContratoId[0]->contrato_id;
                $contrato = Contrato::find($contrato_id);
                return $contrato->numero;
            } else {
                return null;
            }

        }
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function domicilioBancario()
    {
        return $this->hasOne(DomicilioBancario::class, 'id', 'domicilio_bancario_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
