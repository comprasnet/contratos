<?php

namespace App\Models;

use App\Http\Traits\BuscaCodigoItens;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class FornecedorContratoHistorico extends Model
{
    use CrudTrait;
    use LogsActivity;
    use BuscaCodigoItens;
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected static $logFillable = true;
    protected static $logName = 'fornecedor_contratos_historico';

    protected $table = 'fornecedor_contratos_historico';

    protected $guarded = [
        'id'
    ];

    protected $fillable = [
        'contrato_id',
        'fornecedor_id',
        'porte',
        'mei'
    ];



    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function getMeiAttribute($value)
    {
        if($value === null){
            return '';
        }

        switch ($value){
            case true:
                return 'Sim';
            case false:
                return 'Não';
            default:
                return 'Não Especificado';
        }
    }

    public function getPorteAttribute($value)
    {
        if($value === null){
            return '';
        }

        $descricao = $this->retornaDescricaoCodigoItemPorId($value);

        return mb_convert_case($descricao, MB_CASE_TITLE, 'UTF-8');
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
