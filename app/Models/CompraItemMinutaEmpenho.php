<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Http\Traits\BuscaCodigoItens;

class CompraItemMinutaEmpenho extends Model
{
    use CrudTrait;
    use LogsActivity;
    use BuscaCodigoItens;

//    use SoftDeletes;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected static $logFillable = true;
    public $timestamps = true;
    protected static $logName = 'compra_item_minuta_empenho';

    protected $table = 'compra_item_minuta_empenho';
    protected $guarded = [
        //
    ];

    protected $fillable = [
        'compra_item_id',   // Chave composta: 1/3
        'minutaempenho_id', // Chave composta: 2/3
        'subelemento_id',
        'operacao_id',
        'remessa', // Chave composta: 3/3
        'quantidade',
        'valor',
        'minutaempenhos_remessa_id',
        'numseq',
        'compra_item_unidade_id',
        'compra_item_fornecedor_id'
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function retornaItensMinuta($id_minuta) {

        $itens = $this->select(
            'compra_item_minuta_empenho.compra_item_id',
            'compra_item_minuta_empenho.quantidade',
            'compra_item_minuta_empenho.valor',
            'compra_item_minuta_empenho.numseq as sequencial_siafi',
            'codigoitens.descres as tipo_operacao',
            'compra_items.numero as numero_item_compra',
            'catmatseritens.codigo_siasg as codigo_item',
            DB::raw("CONCAT(natdesp.codigo, ' - ', natdesp.descricao) AS nd_empenho"),
            'natsub.codigo as subelemento_item',
            'catmatseritens.descricao',
            'compra_item_fornecedor.valor_unitario as valorunitario',
            'compra_items.descricaodetalhada as descricao_detalhada')
            ->join('compra_items', 'compra_items.id', 'compra_item_minuta_empenho.compra_item_id')
            ->join('catmatseritens', 'catmatseritens.id', 'compra_items.catmatseritem_id')
            ->join('codigoitens', 'codigoitens.id', 'compra_item_minuta_empenho.operacao_id')
            ->join('compra_item_fornecedor', 'compra_item_fornecedor.compra_item_id', 'compra_item_minuta_empenho.compra_item_id')
            ->join('naturezasubitem as natsub', 'natsub.id', 'compra_item_minuta_empenho.subelemento_id')
            ->join('naturezadespesa as natdesp', 'natdesp.id', 'natsub.naturezadespesa_id')
            ->where('compra_item_minuta_empenho.minutaempenho_id', $id_minuta)
            ->whereIn('codigoitens.descres', ['INCLUSAO'])
            ->distinct()->get();

        return $itens;

    }

    public function retornaAlteracaoDeItensMinuta($id_minuta) {

        $itens = Minutaempenho::select(
            'compra_item_minuta_empenho.compra_item_id',
            'codigoitens.descricao as operacao_alteracao',
            'compra_item_minuta_empenho.quantidade as quantidade_alteracao',
            'compra_item_minuta_empenho.valor as valor_total_alteracao',
            'compra_item_fornecedor.valor_unitario as valor_unitario_alteracao',
        )
        ->join('compra_item_minuta_empenho', 'compra_item_minuta_empenho.minutaempenho_id', '=', 'minutaempenhos.id')
        ->join('compra_items', 'compra_items.id', 'compra_item_minuta_empenho.compra_item_id')
        ->join('compra_item_fornecedor', 'compra_item_fornecedor.compra_item_id', 'compra_item_minuta_empenho.compra_item_id')
        ->join('minutaempenhos_remessa', function ($join) {
            $join->on('minutaempenhos_remessa.id', '=', 'compra_item_minuta_empenho.minutaempenhos_remessa_id')
                 ->orWhere(function ($query) {
                     $query->whereNotNull('minutaempenhos_remessa.alteracao_fonte_minutaempenho_id')
                           ->where('minutaempenhos.id', '=', DB::raw('minutaempenhos_remessa.minutaempenho_id'));
                 });
        })
        ->join('codigoitens', 'codigoitens.id', '=', 'minutaempenhos_remessa.situacao_id')
        ->leftJoin('conta_corrente_passivo_anterior', 'conta_corrente_passivo_anterior.minutaempenhos_remessa_id', '=', 'minutaempenhos_remessa.id')
        ->where('minutaempenhos.id', '=', $id_minuta)
        ->where('minutaempenhos_remessa.remessa', '<>', 0)
        ->where('minutaempenhos.situacao_id', 217)
        ->whereNotIn('codigoitens.descres', ['INCLUSAO'])
        ->whereNull('minutaempenhos.deleted_at')
        ->distinct()
        ->get();

        return $itens;

    }

    public function itemAPI($saldo_quantidade_item, $saldo_valor_total_item, Collection $alteracaoCompraItemMinutaEmpenho = null, bool $trazerAlteracaoItem = false){

        $arrayAlteracaoItemMinuta = [];

        if ($alteracaoCompraItemMinutaEmpenho->isNotEmpty()) {
            $arrayAlteracaoItemMinuta = $alteracaoCompraItemMinutaEmpenho
                ->where('compra_item_id', $this->compra_item_id)
                ->makeHidden('compra_item_id')
                ->toArray();
        }

        $arrayAlteracaoItemMinuta = array_values($arrayAlteracaoItemMinuta);

        $result = [
            'sequencial_siafi' => $this->sequencial_siafi,
            'numero_item_compra' => $this->numero_item_compra,
            'codigo_item' => $this->codigo_item,
            'subelemento_item' => $this->subelemento_item,
            'descricao' => $this->descricao,
            'descricao_detalhada' => $this->descricao_detalhada,
            'quantidade' => number_format($saldo_quantidade_item, 5, ',', '.'),
            'valor_unitario' => number_format($this->valorunitario, 4, ',', '.'),
            'valor_total' => number_format($saldo_valor_total_item, 2, ',', '.'),
        ];

        if ($trazerAlteracaoItem) {
            $result['alteracao'] = $arrayAlteracaoItemMinuta;
        }

        return $result;
    }

    /**
     * Retorna o nome da Tabela
     * @return string
     */
    public static function getTableName(): string
    {
        return (new self())->getTable();
    }

    public function retornaValorUnitario(){

        return $this->select('compra_item_fornecedor.valor_unitario')
            ->join('minutaempenhos', 'minutaempenhos.id','compra_item_minuta_empenho.minutaempenho_id')
            ->join('compra_item_fornecedor', function($join)
            {
                $join->on('compra_item_fornecedor.fornecedor_id', 'minutaempenhos.fornecedor_compra_id');
                $join->on('compra_item_fornecedor.compra_item_id','compra_item_minuta_empenho.compra_item_id');
            })
            ->where('compra_item_minuta_empenho.id', $this->id)
            ->first()->valor_unitario;

    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function compra_item()
    {
        return $this->belongsTo(CompraItem::class, 'compra_item_id');
    }

    public function minutaempenho()
    {
        return $this->belongsTo(MinutaEmpenho::class, 'minutaempenho_id');
    }

    public function subelemento()
    {
        return $this->belongsTo(Naturezasubitem::class, 'subelemento_id');
    }

    public function minutaempenhos_remessa()
    {
        return $this->belongsTo(MinutaEmpenhoRemessa::class, 'minutaempenhos_remessa_id');
    }

    public function operacao()
    {
        return $this->belongsTo(Codigoitem::class, 'operacao_id');
    }

    public function compra_item_unidade()
    {
        return $this->belongsTo(CompraItemUnidade::class, 'compra_item_unidade_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    public function getSituacaoRemessaAttribute(): string
    {
        return $this->minutaempenhos_remessa->situacao->descricao;
    }

    public function getOperacaoAttribute(): string
    {
        return $this->operacao()->first()->descricao;
    }

    public function getOperacaoDescresAttribute(): string
    {
        return $this->operacao()->first()->descres;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
