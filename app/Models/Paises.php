<?php

namespace App\Models;

use App\Http\Controllers\IBGE;
use Illuminate\Database\Eloquent\Model;

class Paises extends Model
{

    const BRASIL = 76;

    const DESCONHECIDO = 1;

    protected $table = 'paises';

    public $timestamps = true;

    protected $fillable = [
        'id',
        'nome',
    ];

    public static function atualiza()
    {
        $ibge = new IBGE();
        $paises = $ibge->paises();

        Paises::updateOrCreate(['id' => Paises::DESCONHECIDO], [
            'nome' => 'Desconhecido',
        ]);

        foreach ($paises as $pais) {
            Paises::updateOrCreate(['id' => $pais->id->M49], [
                'nome' => $pais->nome->abreviado,
            ]);
        }
    }

}
