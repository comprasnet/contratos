<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ContratoFaturaEmpenho extends Model
{

    use CrudTrait;
    use LogsActivity;

    public $timestamps = false;

    public $incrementing = false;

    protected static $logFillable = true;
    protected static $logName = 'contratofatura_empenhos';

    protected $table = 'contratofatura_empenhos';

    protected $fillable = [
        'contratofatura_id',
        'empenho_id',
        'valorref',
        'subelemento_id',
    ];


    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function contratoFaturas()
    {
        return $this->belongsTo(Contratofatura::class, 'contratofatura_id');
    }

    public function empenho()
    {
        return $this->belongsTo(Empenho::class, 'empenho_id');
    }

    public function subelemento()
    {
        return $this->belongsTo(Naturezasubitem::class, 'subelemento_id');
    }

}
