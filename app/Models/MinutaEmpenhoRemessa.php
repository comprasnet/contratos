<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class MinutaEmpenhoRemessa extends Model
{
    use CrudTrait;
    use LogsActivity;
    use SoftDeletes;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected static $logFillable = true;
    protected static $logName = 'minutaempenhos_remessa';

    protected $table = 'minutaempenhos_remessa';

    protected $fillable = [
        'minutaempenho_id',
        'situacao_id',
        'remessa',
        'etapa',
        'sfnonce',
        'alteracao_fonte_minutaempenho_id'
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function retornaCompraItemMinutaEmpenho()
    {
        return CompraItemMinutaEmpenho::where('minutaempenho_id', $this->minutaempenho_id)
            ->where('minutaempenhos_remessa_id', $this->id);
    }

    public function getSituacao()
    {
        return $this->situacao->descricao;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function minutaempenho()
    {
        return $this->belongsTo(MinutaEmpenho::class, 'minutaempenho_id');
    }

    public function situacao()
    {
        return $this->belongsTo(Codigoitem::class, 'situacao_id');
    }

    public function contacorrente()
    {
        return $this->hasMany(ContaCorrentePassivoAnterior::class, 'minutaempenhos_remessa_id');
    }

    public function itens()
    {
        if ($this->minutaempenho->empenho_por === 'Contrato') {
            return $this->hasMany(ContratoItemMinutaEmpenho::class, 'minutaempenhos_remessa_id');
        }

        return $this->hasMany(CompraItemMinutaEmpenho::class, 'minutaempenhos_remessa_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    public function getSituacaoDescricaoAttribute()
    {
        return $this->situacao->descricao;
    }

    public function getAlteracaoFonteAttribute(): bool
    {
        return $this->alteracao_fonte_minutaempenho_id !== null ;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
