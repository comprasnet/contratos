<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        // Using class based composers...
        View::composer(
            'backpack::inc.sidebar_content', 'App\Http\View\Composers\SidebarComposer'
        );

        View::composer(
            'crud::fields.fatura_mes_ano_table', 'App\Http\View\Composers\FaturaMesAnoTableComposer'
        );

        View::composer(
            'crud::fields.contratofaturas.empenho_table', 'App\Http\View\Composers\EmpenhoTableComposer'
        );

        View::composer(
            'crud::fields.table_itens_faturados', 'App\Http\View\Composers\TableItensFaturadosComposer'
        );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
