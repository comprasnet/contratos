<?php

namespace App\Providers;

use App\Models\AmparoLegal;
use App\Models\BackpackUser;
use App\Models\Catmatseratualizacao;
use App\Models\Comunica;
use App\Models\Contrato;
use App\Models\Contratoarquivo;
use App\Models\Contratocronograma;
use App\Models\Contratodespesaacessoria;
use App\Models\Contratohistorico;
use App\Models\ContratoHistoricoQualificacao;
use App\Models\Contratoitem;
use App\Models\Contratosfpadrao;
use App\Models\ContratoPublicacoes;
use App\Models\MinutaEmpenho;
use App\Models\Saldohistoricoitem;
use App\Models\SfOrcEmpenhoDados;
use App\Models\Siasgcompra;
use App\Models\Siasgcontrato;
use App\Models\Subrogacao;
use App\Models\Movimentacaocontratoconta;
use App\Models\Lancamento;
use App\Models\SfOrcAmparoLegalDados;
use App\Models\ContratoAutoridadeSignataria;
use App\Models\IndiceValores;
use App\Observers\CatmatseratualizacaoObserver;
use App\Observers\ComunicaObserver;
use App\Observers\ContratoarquivoObserver;
use App\Observers\ContratocronogramaObserve;
use App\Observers\ContratodespesaacessoriaObserver;
use App\Observers\ContratohistoricoObserve;
use App\Observers\ContratohistoricoqualificacaoObserver;
use App\Observers\ContratopublicacaoObserver;
use App\Observers\ContratoitemObserver;
use App\Observers\ContratoObserve;
use App\Observers\ContratosfpadraoObserver;
use App\Observers\MinutaEmpenhoObserver;
use App\Observers\SaldohistoricoitemObserver;
use App\Observers\SforcempenhodadosObserver;
use App\Observers\SiasgcompraObserver;
use App\Observers\SiasgcontratoObserver;
use App\Observers\SubrogacaoObserver;
use App\Observers\UsuarioObserver;
use App\Observers\MovimentacaocontratocontaObserver;
use App\Observers\LancamentoObserver;
use App\Observers\SforcamparolegaldadosObserver;
use App\Observers\ContratoAutoridadeSignatariaObserver;
use App\Services\Indices\Implementations\BacenSoapConection;
use App\services\Indices\Implementations\ImportacaoDadosBacenWebserviceToLocalTableService;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;
use Spatie\Activitylog\Models\Activity;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        if (!app()->runningInConsole()) {
            Activity::saving(function (Activity $activity) {
                $activity->ip = \Request::ip();
                if (backpack_user()) {
                    $activity->causer_id = backpack_user()->id;
                }
            });
        }

        BackpackUser::observe                 (UsuarioObserver::class);
        Catmatseratualizacao::observe         (CatmatseratualizacaoObserver::class);
        Comunica::observe                     (ComunicaObserver::class);
        Contrato::observe                     (ContratoObserve::class);
        #Contratoarquivo::observe              (ContratoarquivoObserver::class);
        ContratoAutoridadeSignataria::observe (ContratoAutoridadeSignatariaObserver::class);
        Contratocronograma::observe           (ContratocronogramaObserve::class);
        Contratodespesaacessoria::observe     (ContratodespesaacessoriaObserver::class);
        Contratohistorico::observe            (ContratohistoricoObserve::class);
        Contratoitem::observe                 (ContratoitemObserver::class);
        ContratoPublicacoes::observe          (ContratopublicacaoObserver::class);
        Contratosfpadrao::observe             (ContratosfpadraoObserver::class);
        Lancamento::observe                   (LancamentoObserver::class);
        Movimentacaocontratoconta::observe    (MovimentacaocontratocontaObserver::class);
        Saldohistoricoitem::observe           (SaldohistoricoitemObserver::class);
        SfOrcAmparoLegalDados::observe        (SforcamparolegaldadosObserver::class);
        SfOrcEmpenhoDados::observe            (SforcempenhodadosObserver::class);
        Siasgcompra::observe                  (SiasgcompraObserver::class);
        Siasgcontrato::observe                (SiasgcontratoObserver::class);
        Subrogacao::observe                   (SubrogacaoObserver::class);
        MinutaEmpenho::observe                (MinutaEmpenhoObserver::class);

        if(config('app.protocolo_http') === 'https') {
            \URL::forceScheme('https');
        }

        if( Config::get("app.app_amb") != 'Ambiente Produção' &&
            Config::get("app.app_amb") != 'Ambiente Homologação' &&
            Config::get("app.app_amb") != 'Ambiente Treinamento' ){
            $appPath = ENV('APP_PATH');
            $stringfromfile = file("{$appPath}.git/HEAD", FILE_USE_INCLUDE_PATH);

            $firstLine = $stringfromfile[0]; //get the string from the array

            $explodedstring = explode("/", $firstLine, 3); //seperate out by the "/" in the string

            $branchname = $explodedstring[2]; //get the one that is always the branch name
            Config::set('app.app_version', $branchname);
        }

        JsonResource::withoutWrapping();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
