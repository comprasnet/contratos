<?php

namespace App\Validators;

use App\Rules\ValidarEmpenho;
use Illuminate\Support\Facades\Route;

class EmpenhoValidation
{
    public static function rules($request)
    {
        $actionMethod = Route::current()->getActionMethod();

        return [
            'numero' => [
                'required',
                new ValidarEmpenho($request, $actionMethod),
            ],
            'data_emissao' => 'required',
            'unidade_id' => 'required',
            'fornecedor_id' => 'required',
            'naturezadespesa_id' => 'required',
            ];
    }

    public static function messages()
    {
        return [
            'numero.required' => 'O campo "Número Empenho" é obrigatório!',
            'numero.unique' => 'Este Número de Empenho já está cadastrado!',
            'unidade_id.required' => 'O campo "Unidade Gestora" é obrigatório!',
            'fornecedor_id.required' => 'O campo "Credor / Fornecedor" é obrigatório!',
            'naturezadespesa_id.required' => 'O campo "Natureza Despesa (ND)" é obrigatório!',
        ];
    }
}
