<?php
/**
 * Created by PhpStorm.
 * User: Junior
 * Date: 05/06/2019
 * Time: 13:48
 */

namespace App\PDF;

use App\Models\Municipio;
use App\Models\Estado;
use App\Models\Unidade;
use App\Models\Orgao;
use App\Models\Contrato;
use Codedge\Fpdf\Fpdf\Fpdf;

class PdfEncerramentoContrato extends Fpdf
{
    function Header()
    {

        $contrato_id = \Route::current()->parameter('contrato_id');
        $contrato = Contrato::find($contrato_id);
        $numeroContrato = $contrato->numero;
        $codigoUasg = $contrato->unidade->codigo;

        $codigoUnidadeGestora = session()->get('user_ug');
        $unidade = Unidade::where('codigo', $codigoUnidadeGestora)->first();
        $nomeUnidade = $unidade->nome;
        $idOrgao = $unidade->orgao_id;
        $orgao = Orgao::find($idOrgao);
        $nomeOrgao = $orgao->nome;
        $nomeFonte = 'Arial';
        // $nomeFonte = 'Calibri';

        // brasão
        // $this->SetFont($nomeFonte,'B',16);
        $this->SetFont($nomeFonte,'B',16);
        $this->Cell(0,0, $this->Image(public_path()."/img/brasao_de_armas_da_republica_2.png", 94, 5,'PNG')  ,0,0,'C');

        $this->SetY("28");
        $this->Cell(50,5,"",0,0,'L');
        $this->SetFont($nomeFonte,'',8);
        $this->Cell(90,5, utf8_decode($nomeOrgao),0,0,'C');

        $this->SetY("32");
        $this->Cell(50,5,"",0,0,'L');
        $this->SetFont($nomeFonte,'',8);
        $this->Cell(90,5, utf8_decode($nomeUnidade),0,0,'C');

        // título
        $this->SetY("44");
        $this->Cell(50,5,"",0,0,'L');
        $this->SetFont($nomeFonte,'B',16);
        $this->Cell(90,5, utf8_decode("RELATÓRIO FINAL DO CONTRATO nº ".$numeroContrato." - UASG ".$codigoUasg),0,0,'C');
        // data de hoje no título
        // $this->SetFont($nomeFonte,'I',8);
        // $this->Cell(0,5,date('d/m/Y'),0,1,'R');

        // linha do cabeçalho
        $this->SetY("54");
        $this->Cell(0,0,'',1,1,'L');
        $this->Ln(8);
    }

    function Footer()
    {
        $contrato_id = \Route::current()->parameter('contrato_id');
        $contrato = Contrato::find($contrato_id);
        $numeroContrato = $contrato->numero;
        $codigoUasg = $contrato->unidade->codigo;

        $nomeFonte = 'Arial';
        // $nomeFonte = 'Calibri';

        $this->SetY(-15);
        $this->SetFont($nomeFonte,'B',7);
        // linha do rodapé
        $this->Cell(0,0,'',1,1,'L');
        // dados pra mostrar no rodapé, à esquerda
        // $this->Cell(50,10,utf8_decode($nomeMunicipio.'/'.$siglaEstado.', '.$dataHoje.' - Unidade Gestora: '.$codigoUnidadeGestora.' - '.backpack_auth()->user()->name),0,0,'L');

        // mostrar a página no rodapé
        $this->SetFont($nomeFonte,'I',8);

        $this->Cell(0,10,utf8_decode('Página '.$this->PageNo() ),0,0,'R');

        $this->Cell(0,0,'',1,1,'L');
        $this->SetFont($nomeFonte,'I',8);
        $this->Cell(0,10,utf8_decode( "Relatório Final do Contrato nº ".$numeroContrato." - UASG ".$codigoUasg ),0,0,'L');

    }

    function NbLines($w, $txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw =& $this->CurrentFont['cw'];
        if ($w == 0)
            $w = $this->w - $this->rMargin - $this->x;
        $wmax = ($w - 2 * $this->cMargin) * 1000 / $this->FontSize;
        $s = str_replace("\r", '', $txt);
        $nb = strlen($s);
        if ($nb > 0 and $s[$nb - 1] == "\n")
            $nb--;
        $sep = -1;
        $i = 0;
        $j = 0;
        $l = 0;
        $nl = 1;
        while ($i < $nb) {
            $c = $s[$i];
            if ($c == "\n") {
                $i++;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
                continue;
            }
            if ($c == ' ')
                $sep = $i;
            $l += $cw[$c];
            if ($l > $wmax) {
                if ($sep == -1) {
                    if ($i == $j)
                        $i++;
                } else
                    $i = $sep + 1;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
            } else
                $i++;
        }
        return $nl;
    }
    // public function formataDataParaPadraoBrasileiro($dataPadraoAmericano){
    //     /**
    //      * Vamos pegar a data recebida, quebrá-la em aaaa, mm, dd
    //      * e fazer a nova formatação
    //      */
    //     $dia = substr($dataPadraoAmericano, 8, 2);
    //     $mes = substr($dataPadraoAmericano, 5, 2);
    //     $ano = substr($dataPadraoAmericano, 0, 4);
    //     return $dia."/".$mes."/".$ano;
    // }

}
