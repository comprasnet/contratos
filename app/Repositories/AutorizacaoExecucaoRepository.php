<?php

namespace App\Repositories;

use App\Models\AutorizacaoExecucao;

class AutorizacaoExecucaoRepository
{
    
    protected $autorizacaoExecucao;

    public function __construct(AutorizacaoExecucao $autorizacaoExecucao)
    {
        $this->autorizacaoExecucao = $autorizacaoExecucao;
    }

     /**
     * Retorna uma lista de ordens de serviço de fornecimento associadas a um contrato específico.
     * 
     * @param int $contrato_id do contrato para filtrar as ordens de serviço.
     * @param int|null $autorizacao_execucao_id (Opcional) para filtrar por uma ordem de serviço específica.
     * 
     * @return \Illuminate\Database\Eloquent\Collection Lista de ordens de serviço de fornecimento correspondentes.
     * @throws \Exception Se ocorrer algum erro durante a consulta.
     */
    public function getListaOrdemServicoFornecimentoPorContrato ($contrato_id, $autorizacao_execucao_id = null)
    {
        try {
            return $this->autorizacaoExecucao::query()
                    ->where('contrato_id', $contrato_id)
                    ->when($autorizacao_execucao_id, function ($query, $autorizacao_execucao_id) {
                        return $query->where('id', $autorizacao_execucao_id);
                    })
                    ->get();
        } catch (\Exception $e) {
            throw $e;
        }
    }

}
