<?php

namespace App\Repositories;

use App\Models\Codigo;
use App\Models\Codigoitem;
use App\Models\Contrato;
use App\Models\Contratoarquivo;
use Illuminate\Http\Request;

class ContratoArquivoRepository
{
    protected $contratoarquivo;
    public function __construct(Contratoarquivo $contratoarquivo)
    {
        $this->contratoarquivo = $contratoarquivo;
    }

    public function atualizarSituacaoRestrito(int $idContratoArquivo, array $dadosAtualizacao)
    {
        return $this->contratoarquivo->where("id",$idContratoArquivo)->update($dadosAtualizacao);
    }

    public function getContratoArquivoUnico(int $idContratoArquivo)
    {
        return $this->contratoarquivo->find($idContratoArquivo);
    }

    public function save(Request $request, $local)
    {
        $contrato = Contrato::find($request->contrato_id);
        $idContratoHistorico = null;
        if (!$contrato->elaboracao) {
            $idContratoHistorico = $contrato->historico()->oldest()->first()->id;
        }

        $anexos = [];

        // get variaveis sei
        if (isset($request->nomeSei)) {
            for ($i = 0; $i < count($request->linkAnexoSei); $i++) {
                $anexo = new Contratoarquivo();
                $anexo->tipo = $request->tipoSei[$i];
                $anexo->contrato_id = $request->contrato_id;
                $anexo->descricao = "Descrição SEI: {$request->nomeSei[$i]} <br>Descrição Comprasgovbr: {$request->descricaoSei[$i]}";
                $anexo->link_sei = $request->linkAnexoSei[$i];
                $anexo->processo = $request->processo;
                $anexo->arquivos = $request->linkAnexoSei[$i];
                $anexo->origem = "1";
                $anexo->contratohistorico_id =  ($request->contratohistoricoSEI_id[$i]) ? str_replace("/", "", $request->contratohistoricoSEI_id[$i]) * 1 : $idContratoHistorico;
                $anexo->restrito = $request->restrito[$i];
                $anexo->user_id = backpack_user()->id;

                $anexo->save();
                $anexos[] = $anexo;
            }
        }

        // get variáveis para upload de arquivos
        if (isset($request->arquivos)) {
            $disk = "local";
            $destination_path = "contrato/" . $contrato->id . "_" .
                str_replace('/', '_', $contrato->numero);

            if (!empty($request->arquivos)) {
                for ($i = 0; $i < count($request->arquivos); $i++) {
                    if ($request->arquivos[$i]->extension() == 'PDF' || $request->arquivos[$i]->extension() == 'pdf') {
                        //cria o nome
                        $new_file_name = $request->arquivos[$i]->hashName();
                        //faz upload
                        $file_path = $request->arquivos[$i]->storeAs($destination_path, $new_file_name, $disk);

                        //seta descrição igual tipo do arquivo quando nenhum valor for informado pelo usuário
                        $descricao = '';
                        if (is_null($request->descricao[$i]) || trim($request->descricao[$i]) == '') {
                            $id_tipo_arquivo = $request->tipo[$i];

                            $descricao = $local == "meus-contratos" ? $this->listaTipoMeuContrato($id_tipo_arquivo)[$id_tipo_arquivo] : $this->listaTipo($id_tipo_arquivo)[$id_tipo_arquivo];
                        } else {
                            $descricao = $request->descricao[$i];
                        }
                        //grava no banco
                        $anexo = new Contratoarquivo();
                        $anexo->contrato_id = $request->contrato_id;
                        $anexo->tipo = $request->tipo[$i];
                        $anexo->descricao = $descricao;
                        $anexo->sequencial_documento = $request->processoSapiens[$i];
                        $anexo->arquivos = $file_path;  //$request->descricao[$i];
                        $anexo->origem = "0";
                        $anexo->contratohistorico_id = ($request->contratohistorico_id[$i]) ? str_replace("/", "", $request->contratohistorico_id[$i]) * 1 : $idContratoHistorico;
                        $anexo->restrito = $request->restrito[$i];
                        $anexo->user_id = backpack_user()->id;

                        $anexo->save();
                        $file_path = '';
                        $anexos[] = $anexo;
                    }
                }
            }
        }
        return $anexos;
    }

    public function listaTipoMeuContrato()
    {
        return $this->retornaArrayCodigosItensNotInDescres(
            array('CONTRATO', 'ADITIVO', 'APOSTILA', 'RESCISAO'),
            'Tipo Arquivos Contrato'
        );
    }

    public function listaTipo($id = '')
    {
        return Codigoitem::select('descricao', 'id')
            ->where('codigo_id', Codigo::TIPO_ARQUIVOS_CONTRATO)
            ->where(function ($codigo) use ($id) {
                if ($id != '') {
                    $codigo->where('id', $id);
                }
            })
            ->where('descricao', '<>', 'Termo de Encerramento')
            ->orderBy('descricao')
            ->pluck('descricao', 'id')
            ->toArray();
    }

    public function retornaArrayCodigosItensNotInDescres($arrayDescres, $descricao)
    {
        return Codigoitem::whereHas('codigo', function ($query) use ($descricao) {
            $query->where('descricao', '=', $descricao)
                ->whereNull('deleted_at');
        })->whereNull('deleted_at')
            ->whereNotIn('descres', $arrayDescres)
            ->where('descricao', '<>', 'Termo de Encerramento')
            ->orderBy('descricao')
            ->pluck('descricao', 'id')
            ->toArray();
    }
}
