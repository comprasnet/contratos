<?php

namespace App\Repositories;

use App\Models\Compra;
use App\Models\Contratohistorico;

class ContratoRepository
{
    public function getContratoCompra(int $contratoId)
    {
        return Compra::join('contratohistorico', function ($join) {
            $join->on('contratohistorico.licitacao_numero', '=', 'compras.numero_ano')
                ->on('contratohistorico.unidadecompra_id', '=', 'compras.unidade_origem_id')
                ->on('contratohistorico.modalidade_id', '=', 'compras.modalidade_id');
        })
            ->where('contratohistorico.contrato_id', '=', $contratoId)
            ->select('compras.*')
            ->first();
    }

    public function getInstrumentoInicial($contratoId){
        $instrumento = Contratohistorico::query()->where('contrato_id', $contratoId)
            ->join('codigoitens as ci', 'ci.id', 'contratohistorico.tipo_id')
            ->join('codigos as c', 'c.id', 'ci.codigo_id')
            ->whereNotIn('ci.descricao',config('app.tipo_contrato_not_in'))
            ->where('c.descricao', 'Tipo de Contrato')
            ->select('contratohistorico.*')
            ->first();
        return $instrumento;
    }
}
