<?php

namespace App\Repositories;

use App\Models\AmparoLegal;

class AmparoLegalRepository
{
    protected $amparoLegal;
    public function __construct(AmparoLegal $amparoLegal)
    {
        $this->amparoLegal = $amparoLegal;
    }

    /**
     *
     * Método base para todas as consultas que necessitam ser do amparo legal 14.133 e seus complementos
     *
     * @return mixed
     */
    public function getAmparoLegal14133Base()
    {
        return $this->amparoLegal->where('complemento_14133', true);
    }

    /**
     * Retornar todos os amparos
     * @return mixed
     */
    public function getTodosAmparosPodeEnviarPNCP()
    {
        return $this->getAmparoLegal14133Base()->get();
    }

    public function getAmparoPodeEnviarPNCPPorId(array $idAmparoLegal)
    {
        return $this->getAmparoLegal14133Base()->whereIn('id', $idAmparoLegal)->get();
    }

    public function existeAmparoLegal14133PorContrato(int $idContrato)
    {
        return $this->getAmparoLegal14133Base()
            ->join('amparo_legal_contrato', 'amparo_legal_contrato.amparo_legal_id', '=', 'amparo_legal.id')
            ->where('amparo_legal_contrato.contrato_id', '=', $idContrato)
            ->exists();
    }

    public function getPorFiltros(
        $modalidade = null,
        $ato_normativo = null,
        $artigo = null,
        $paragrafo = null,
        $inciso = null,
        $alinea = null
    ) {
        $query = AmparoLegal::select('amparo_legal.*', 'codigoitens.descricao')
            ->leftJoin('codigoitens', 'amparo_legal.modalidade_id', '=', 'codigoitens.id');

        if (!is_null($modalidade)) {
            $query->where('codigoitens.descricao', $modalidade);
        }

        if (!is_null($ato_normativo)) {
            $query->where('amparo_legal.ato_normativo', 'like', "%$ato_normativo%");
        }

        if (!is_null($artigo)) {
            $query->where('amparo_legal.artigo', $artigo);
        }

        if (!is_null($paragrafo)) {
            $query->where('amparo_legal.paragrafo', $paragrafo);
        }

        if (!is_null($inciso)) {
            $query->where('amparo_legal.inciso', $inciso);
        }

        if (!is_null($alinea)) {
            $query->where('amparo_legal.alinea', $alinea);
        }

        return $query->get();
    }

    public function getAmparoLegalUnico(int $id)
    {
        return $this->amparoLegal->find($id);
    }
}
