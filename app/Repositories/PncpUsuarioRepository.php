<?php

namespace App\Repositories;

use App\Models\PncpUsuario;

class PncpUsuarioRepository
{
    public function save($token, $expires)
    {
        $expiresInSeconds = $expires / 1000;
        $pncpUsuario = PncpUsuario::query()->first();
        $pncpUsuario->expires_in = $expiresInSeconds;
        $pncpUsuario->expires = now()->addSeconds($expiresInSeconds);
        $pncpUsuario->token = $token;
        $pncpUsuario->save();
    }
}
