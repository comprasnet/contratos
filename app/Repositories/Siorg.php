<?php

namespace App\Repositories;

use App\Http\Traits\Formatador;
use App\Jobs\UpdateUnidadeOrganizacionalEstruturaJob;
use App\Models\JobUser;
use Illuminate\Foundation\Bus\DispatchesJobs;

class Siorg extends Base
{
    use Formatador;
    use DispatchesJobs;

    public function updateUnidadeOrganizacionalEstrutura(array $estrutura, string $codigo_siorg)
    {

        # Cria o queue passando como parâmetro os dados da compra e as unidades participantes
        $job = (new UpdateUnidadeOrganizacionalEstruturaJob($estrutura))
            ->onQueue('update-unidade-organizacional-estrutura');

        # Recupera o ID do Job para popular a tabela intermediária responsável
        # em verificar se o item ainda está processado
        $jobId = $this->dispatch($job);

        # Monta os dados para inserir na tabela intermediária
        $insertJobUser = [
            'user_id' => backpack_user()->id,
            'job_id' => $jobId,
            'job_user_type' => \App\Models\Siorg::class,
            'job_user_id' => $codigo_siorg,
        ];

        JobUser::create($insertJobUser);

        return false;
    }

}
