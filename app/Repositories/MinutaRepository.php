<?php

namespace App\Repositories;

use App\Http\Traits\MinutaEmpenhoTrait;
use App\Models\CompraItemMinutaEmpenho;
use App\Models\ContratoItemMinutaEmpenho;
use App\Models\MinutaEmpenho;
use Illuminate\Support\Facades\DB;

class MinutaRepository
{
    use MinutaEmpenhoTrait;

    public function getItensContrato($minutaEmpenho)
    {
        $itens = MinutaEmpenho::join(
            'contrato_item_minuta_empenho',
            'contrato_item_minuta_empenho.minutaempenho_id',
            '=',
            'minutaempenhos.id'
        )
            ->join(
                'contratoitens',
                'contratoitens.id',
                '=',
                'contrato_item_minuta_empenho.contrato_item_id'
            )
            ->join(
                'compras',
                'compras.id',
                '=',
                'minutaempenhos.compra_id'
            )
            ->join(
                'codigoitens as tipo_compra',
                'tipo_compra.id',
                '=',
                'compras.tipo_compra_id'
            )
            ->join(
                'codigoitens',
                'codigoitens.id',
                '=',
                'contratoitens.tipo_id'
            )
            ->join(
                'saldo_contabil',
                'saldo_contabil.id',
                '=',
                'minutaempenhos.saldo_contabil_id'
            )
            ->join(
                'naturezadespesa',
                'naturezadespesa.codigo',
                '=',
                DB::raw("SUBSTRING(saldo_contabil.conta_corrente,18,6)")
            )
            ->join(
                'catmatseritens',
                'catmatseritens.id',
                '=',
                'contratoitens.catmatseritem_id'
            )
            ->join(
                'minutaempenhos_remessa',
                'minutaempenhos_remessa.id',
                '=',
                'contrato_item_minuta_empenho.minutaempenhos_remessa_id'
            )
            ->where('naturezadespesa.sistema_origem', '=', 'SIAFI')
            ->where('minutaempenhos.id', $minutaEmpenho->id)
            ->where('minutaempenhos.unidade_id', session('user_ug_id'))
            ->distinct()
            ->select(
                [
                    'contrato_item_minuta_empenho.contrato_item_id',
                    'contrato_item_minuta_empenho.operacao_id',
                    'tipo_compra.descricao as tipo_compra_descricao',
                    'codigoitens.descricao',
                    'saldo_contabil.id as saldo_id',
                    'catmatseritens.codigo_siasg',
                    'contratoitens.numero_item_compra as numero_item',
                    'catmatseritens.descricao as catmatser_desc',
                    DB::raw("SUBSTRING(catmatseritens.descricao for 50) AS catmatser_desc_simplificado"),
                    'contratoitens.descricao_complementar as descricaodetalhada',
                    DB::raw("SUBSTRING(contratoitens.descricao_complementar for 50) AS descricaosimplificada"),

                    'contratoitens.quantidade as qtd_item',
                    'contratoitens.valorunitario as valorunitario',
                    'naturezadespesa.codigo as natureza_despesa',
                    'naturezadespesa.id as natureza_despesa_id',
                    'contratoitens.valortotal',
                    'saldo_contabil.saldo',
                    'contrato_item_minuta_empenho.subelemento_id',
                    DB::raw('left(minutaempenhos.mensagem_siafi, 4) as exercicio'),
                    'contrato_item_minuta_empenho.numseq',
                    DB::raw('contrato_item_minuta_empenho.id AS "numeroLinha"')
                ]
            )
            ->orderBy('contrato_item_minuta_empenho.numseq', 'asc');
        $soma = ContratoItemMinutaEmpenho::select([
            'contrato_item_id',
            DB::raw("sum(contrato_item_minuta_empenho.quantidade) as qtd_total_item"),
            DB::raw("sum(contrato_item_minuta_empenho.valor) as vlr_total_item"),
        ])
            ->where('minutaempenho_id', $minutaEmpenho->id)
            ->groupBy('contrato_item_id');

        //CREATE
        if (is_null(session('remessa_id'))) {
            $itens->where('minutaempenhos_remessa.remessa', 0);

            $itens->addSelect([
                DB::raw("0 AS quantidade"),
                DB::raw("0 AS valor"),
            ]);

            return $this->retornarArray($itens->get()->toArray(), $soma->get()->toArray(), 'contrato_item_id');
        }

        $itens->where('contrato_item_minuta_empenho.minutaempenhos_remessa_id', session('remessa_id'));

        $itens->addSelect([
            'contrato_item_minuta_empenho.quantidade',
            'contrato_item_minuta_empenho.valor',
        ]);

        $soma->where('minutaempenhos_remessa_id', '<>', session('remessa_id'));

        return $this->retornarArray($itens->get()->toArray(), $soma->get()->toArray(), 'contrato_item_id');
    }

    public function getItensCompra($minutaEmpenho, $fornecedor_id, $fornecedor_compra_id)
    {

        $itens = MinutaEmpenho::join(
            'compra_item_minuta_empenho',
            'compra_item_minuta_empenho.minutaempenho_id',
            '=',
            'minutaempenhos.id'
        )
            ->join(
                'compra_items',
                'compra_items.id',
                '=',
                'compra_item_minuta_empenho.compra_item_id'
            )
            ->join(
                'compras',
                'compras.id',
                '=',
                'compra_items.compra_id'
            )
            ->join(
                'codigoitens as tipo_compra',
                'tipo_compra.id',
                '=',
                'compras.tipo_compra_id'
            )
            ->join(
                'codigoitens',
                'codigoitens.id',
                '=',
                'compra_items.tipo_item_id'
            )
            ->join(
                'saldo_contabil',
                'saldo_contabil.id',
                '=',
                'minutaempenhos.saldo_contabil_id'
            )
            ->join(
                'naturezadespesa',
                'naturezadespesa.codigo',
                '=',
                DB::raw("SUBSTRING(saldo_contabil.conta_corrente,18,6)")
            )
            ->join(
                'compra_item_fornecedor',
                'compra_item_fornecedor.compra_item_id',
                '=',
                'compra_items.id'
            )
            ->join(
                'compra_item_unidade',
                'compra_item_unidade.compra_item_id',
                '=',
                'compra_items.id'
            )
            ->join(
                'catmatseritens',
                'catmatseritens.id',
                '=',
                'compra_items.catmatseritem_id'
            )
            ->join(
                'minutaempenhos_remessa',
                'minutaempenhos_remessa.id',
                '=',
                'compra_item_minuta_empenho.minutaempenhos_remessa_id'
            )
            ->join('codigoitens as mod', 'mod.id', '=', 'compras.modalidade_id')
            ->where('minutaempenhos.id', $minutaEmpenho->id)
            ->where('naturezadespesa.sistema_origem', '=', 'SIAFI')
            ->where('compra_item_unidade.unidade_id', session('user_ug_id'))
            ->where('compra_item_unidade.situacao', true)
            ->where('compra_item_fornecedor.situacao', true)
            ->where('compra_items.situacao', true)
            ->distinct()
            ->select(
                [
                    DB::raw('compra_item_minuta_empenho.id as cime_id'),
                    'compra_item_minuta_empenho.compra_item_id',
                    'compra_item_minuta_empenho.operacao_id',
                    'compra_item_fornecedor.fornecedor_id',
                    'compra_item_fornecedor.id as cif_id',
                    'compra_items.numero as numero_item',
                    'tipo_compra.descricao as tipo_compra_descricao',
                    'codigoitens.descricao',
                    'catmatseritens.descricao AS catmatser_desc',
                    DB::raw('SUBSTRING(catmatseritens.descricao FOR 50) AS catmatser_desc_simplificado'),
                    'compra_items.catmatseritem_id',
                    'compra_items.descricaodetalhada',
                    'catmatseritens.codigo_siasg',
                    DB::raw("SUBSTRING(compra_items.descricaodetalhada for 50) AS descricaosimplificada"),
                    'compra_item_unidade.quantidade_saldo as qtd_item',
                    'compra_item_unidade.id as ciu_id',
                    DB::raw("CASE
                                WHEN (
                                    compra_items.criterio_julgamento = 'V'
                                    OR compra_items.criterio_julgamento is null
                                    OR mod.descres = '99'
                                    OR mod.descres = '06'
                                ) THEN compra_item_fornecedor.valor_unitario
                                ELSE compra_item_fornecedor.valor_unitario *
                                    ((100 - compra_item_fornecedor.percentual_maior_desconto) / 100)
                                END                                           AS valorunitario"),
                    'naturezadespesa.codigo as natureza_despesa',
                    'naturezadespesa.id as natureza_despesa_id',
                    DB::raw("CASE
                                WHEN (
                                    compra_items.criterio_julgamento = 'V'
                                    OR compra_items.criterio_julgamento is null
                                    OR mod.descres = '99'
                                    OR mod.descres = '06'
                                ) THEN compra_item_fornecedor.valor_negociado
                                ELSE compra_item_fornecedor.valor_negociado *
                                     ((100 - compra_item_fornecedor.percentual_maior_desconto) / 100)
                                END                                           AS valortotal"),
                    'saldo_contabil.saldo',
                    'saldo_contabil.id as saldo_id',
                    'compra_item_minuta_empenho.subelemento_id',
                    DB::raw('left(minutaempenhos.mensagem_siafi, 4) as exercicio'),
                    'compra_item_minuta_empenho.numseq',
                    DB::raw('compra_item_minuta_empenho.id AS "numeroLinha"'),
                    'compra_items.criterio_julgamento',
                    'compra_item_fornecedor.percentual_maior_desconto',
                    DB::raw("compra_item_fornecedor.valor_unitario * ((100-compra_item_fornecedor.percentual_maior_desconto)/100) as valor_unitario_desconto"),
                    DB::raw("compra_item_fornecedor.valor_negociado * ((100-compra_item_fornecedor.percentual_maior_desconto)/100) as valor_negociado_desconto"),
//                            DB::raw("CASE
//                                WHEN (
//                                    compra_items.situacao = false
//                                    OR compra_item_fornecedor.situacao = false
//                                    OR compra_item_unidade.situacao = false
//                                ) THEN false
//                                ELSE true
//                                END AS verificacao_situacao"), #removendo bloco de código pela issue 796

                ]
            )
            ->orderBy('compra_item_minuta_empenho.numseq', 'asc');

        $itens = $this->setCondicaoFornecedor(
            $minutaEmpenho,
            $itens,
            $minutaEmpenho->empenho_por,
            $fornecedor_id,
            $fornecedor_compra_id
        );

        $soma = CompraItemMinutaEmpenho::select([
            'compra_item_id',
            DB::raw("sum(compra_item_minuta_empenho.quantidade) as qtd_total_item"),
            DB::raw("sum(compra_item_minuta_empenho.valor) as vlr_total_item"),
        ])
            ->where('minutaempenho_id', $minutaEmpenho->id)
            ->groupBy('compra_item_id');

        //CREATE
        if (is_null(session('remessa_id'))) {
            $itens->where('minutaempenhos_remessa.remessa', 0);
            $itens->addSelect([
                DB::raw("0 AS quantidade"),
                DB::raw("0 AS valor"),
            ]);

            return $this->retornarArray($itens->get()->toArray(), $soma->get()->toArray(), 'compra_item_id');
        }

        //UPDATE
        $itens->addSelect([
            'compra_item_minuta_empenho.quantidade',
            'compra_item_minuta_empenho.valor',
        ]);
        $itens->where('compra_item_minuta_empenho.minutaempenhos_remessa_id', session('remessa_id'));

        $soma->where('minutaempenhos_remessa_id', '<>', session('remessa_id'));

        return $this->retornarArray($itens->get()->toArray(), $soma->get()->toArray(), 'compra_item_id');

    }

    /**
     * Responsável em recuperar as minutas de empenho substitutiva de contrato
     * @return mixed
     */
    public function getMinutasModalCriarContratoTipoEmpenho()
    {
        return MinutaEmpenho::select(
            'minutaempenhos.id as id',
            'minutaempenhos.mensagem_siafi as numero_empenho',
            // 'minutaempenhos.valor_total as valor',
            DB::raw("to_char(minutaempenhos.valor_total, 'L9G999G990D99') as valor"),
            DB::raw("to_char(minutaempenhos.data_emissao, 'DD/MM/YYYY') as data_emissao"),
            'fornecedores.cpf_cnpj_idgener as cnpj_fornecedor',
            'fornecedores.nome as nome_fornecedor',
            'codigoitens.descricao',
            'ugemitente.codigo',
            'amparo_legal.ato_normativo',
            'amparo_legal.complemento_14133',
            'amparo_legal_id',
            'minutaempenhos.tipo_empenhopor_id',
            'minutaempenhos.compra_id',
            'minutaempenhos.unidade_id',
            'minutaempenhos.minutaempenho_forcacontrato'
        )
            ->join('fornecedores', 'fornecedores.id', 'minutaempenhos.fornecedor_empenho_id')
            ->join('codigoitens', 'codigoitens.id', 'minutaempenhos.situacao_id')
            ->leftJoin('unidades', 'unidades.id', 'minutaempenhos.unidade_id')
            ->join('saldo_contabil', 'saldo_contabil.id', 'minutaempenhos.saldo_contabil_id')
            ->join(DB::raw("unidades as ugemitente"), 'ugemitente.id', 'saldo_contabil.unidade_id')
            ->join('amparo_legal', 'amparo_legal.id', 'minutaempenhos.amparo_legal_id')
            ->where('minutaempenhos.minutaempenho_forcacontrato', true)
            ->where('minutaempenhos.mensagem_siafi', '<>', null)
            ->where('codigoitens.descricao', 'EMPENHO EMITIDO')
            ->where('minutaempenhos.unidade_id', session('user_ug_id'))
            ->whereNotExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('contrato_minuta_empenho_pivot')
                    ->whereRaw('contrato_minuta_empenho_pivot.minuta_empenho_id = minutaempenhos.id');
            })
            ->orderby('minutaempenhos.data_emissao', 'ASC')
            ->get()
            //pegar apenas as minutas que não estão associados a um contrato
            ->filter(function ($m) {
                // Não precisa verificar a existência do arquivo para leis antigas
                if (!$m->complemento_14133) {
                    return true;
                }
                $dados_empenho = [
                    'ugemitente' => $m->codigo,
                    'anoempenho' => substr($m->numero_empenho, 0, 4),
                    'numempenho' => (int)substr($m->numero_empenho, 6, 6),
                ];
                return MinutaEmpenho::existeArquivoMinutaEmpenho($dados_empenho);
            });
    }

    public function getMinutaByEmpenho($empenho, $fornecedor, $unidade){
         $minuta = MinutaEmpenho::where('minutaempenhos.mensagem_siafi', $empenho)
            ->where('minutaempenhos.fornecedor_empenho_id', $fornecedor->id)
            ->where('ugemitente.id', $unidade->id)
            ->join('saldo_contabil', 'saldo_contabil.id', 'minutaempenhos.saldo_contabil_id')
            ->join('unidades', 'unidades.id', 'minutaempenhos.unidade_id')
            ->join('unidades as ugemitente', 'ugemitente.id', 'saldo_contabil.unidade_id')
            ->select(['unidades.codigo', 'saldo_contabil.unidade_id','ugemitente.codigo as ugemitente' ,'minutaempenhos.mensagem_siafi', 'saldo_contabil.conta_corrente'])
            ->first();

        return $minuta;
    }
}
