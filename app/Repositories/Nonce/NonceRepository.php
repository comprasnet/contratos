<?php

namespace App\Repositories\Nonce;

use App\Http\Traits\NonceTrait;
use App\Models\Nonce;
use App\Models\RequisicoesNonce;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class NonceRepository
{

    use NonceTrait;

    /**
     * @param array $data
     * @param string $ip
     * @param bool $status
     * @param string $response
     */
    public function createNonce(
        array  $data,
        string $ip,
        bool   $status,
        string $response,
        ?int   $createId = null
    )
    {
        # Obtém dados necessário para o nonce, através da rota
        $action = $this->getActionInfo();

        return DB::table('nonce')->insertGetId([
            'user_id' => Auth::user()->id,
            'nonce' => $data['nonce'],
            'ip' => $ip,
            #'nonceable_type' =>  $createId ? $this->model() : null,
            'nonceable_type' => $action['nonceable_type'],
            'nonceable_id' => $createId,
            'status' => $status,
            'funcionalidade' => $action['funcionalidade'],
            'json_request' => json_encode($data),
            'json_response' => json_encode($response),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }

    /**
     * Método responsável por retornar a descrição do model nonce
     *
     * @return string
     */


    /**
     * @param $nonce
     * @return mixed
     * @throws Exception
     */
    public function show($nonce)
    {
        // Verificar se o usuário é um administrador
        if (backpack_user()->hasRole('Administrador')) {
            $data = Nonce::select(
                'nonce',
                'status',
                'funcionalidade',
                'json_request',
                'json_response'
            )->where('nonce', $nonce)->first();
        } else {
            // O usuário não é um administrador, execute a consulta com restrição
            $data = Nonce::select(
                'nonce',
                'status',
                'funcionalidade',
                'json_request',
                'json_response'
            )->where('nonce', $nonce)->where('user_id', backpack_user()->id)->first();
        }

        if (!isset($data)) {
            throw new Exception('Nonce não encontrado.', Response::HTTP_NOT_FOUND);
        }

        return [
            'nonce' => $data->nonce,
            'status' => $data->status,
            'funcionalidade' => $data->funcionalidade,
            'json_request' => gettype($data->json_request) != 'array' ? json_decode($data->json_request) : $data->json_request,
            'json_response' => gettype($data->json_request) != 'array' ? json_decode($data->json_response) : $data->json_response,
        ];
    }

    /**
     * @param $data
     * @param $nonce
     * @return array
     * @throws \Exception
     */
    public function requisicoesNonce($data, $nonce)
    {
        try {

            $user = Auth::user();

            // $numero = array_key_exists('numero', $data) ? $data['numero'] : null;
            // $favorecido = array_key_exists('favorecido', $data) ? $data['favorecido'] : null;
            $dataInformada = array_key_exists('data', $data) ? $this->transformarData($data['data']) : null;

            if ($user->profile == 'S') {
                throw new Exception('Usuário não possui permissão para acessar esta funcionalidade', 401);
            }

            $nonceCadastrado = Nonce::where('nonce', $nonce)->first();

            if (!$nonceCadastrado) {
                throw new Exception('Nonce não encontrado', 404);
            }

            $requisicoesNonce = RequisicoesNonce::where('nonce_id', $nonceCadastrado->id)
                ->when($dataInformada, function ($query, $dataInformada) {
                    $query->whereBetween('created_at', $dataInformada);
                });

            return [
                'nonce' => $nonceCadastrado->nonce,
                'data_pesquisa' => $dataInformada,
                'quantidade_vezes_enviada' => $requisicoesNonce->count(),
                'requisicoes_nonce' => $requisicoesNonce
                    // ->when($numero, function ($query, $numero) {
                    //     $query->whereJsonContains('requisicao_enviada->numero', $numero);
                    // })
                    // ->when($favorecido, function ($query, $favorecido) {
                    //     $query->whereJsonContains('requisicao_enviada->favorecido', $favorecido);
                    // })
                    ->get(),
            ];
        } catch (\Exception $e) {

            Log::info(
                'Não foi possível atender a requisição.',
                [
                    'message' => $e->getMessage(),
                    'file' => $e->getFile(),
                    'code' => $e->getCode(),
                    'line' => $e->getLine(),
                ]
            );

            throw new Exception('Não foi possível atender a requisição.', 409);
        }
    }

    /**
     * @param $data
     * @return mixed
     * @throws Exception
     */
    public function todasRequisicoesNonce($data)
    {
        try {
            $user = Auth::user();

            $dataInformada = array_key_exists('data', $data) ? $this->transformarData($data['data']) : null;

            if ($user->profile == 'S') {
                throw new Exception('Usuário não possui permissão para acessar esta funcionalidade', 401);
            }

            return RequisicoesNonce::when($dataInformada, function ($query, $dataInformada) {
                $query->whereBetween('created_at', $dataInformada);
            })->get();
        } catch (\Exception $e) {

            Log::info(
                'Não foi possível atender a requisição.',
                [
                    'message' => $e->getMessage(),
                    'file' => $e->getFile(),
                    'code' => $e->getCode(),
                    'line' => $e->getLine(),
                ]
            );

            throw new Exception('Não foi possível atender a requisição.', 409);
        }
    }

    /**
     * @param $dataInformada
     * @return array
     */
    private function transformarData($dataInformada): array
    {
        return [Carbon::parse($dataInformada)->startOfDay(), Carbon::parse($dataInformada)->endOfDay()];
    }

    /**
     * Através de um número nonce, verifica se já existe um registro na base com este número
     *
     * @param int $nonce Número único nonce
     * @return Object Collection com o nonce encontrado ou null
     */
    public function nonceExists($nonce)
    {
        # Se o nonce for encontrado, já retorna uma mensagem padrão para todas as rotinas
        $errorMessage = 'Este NONCE já está registrado.';
        return DB::table('nonce')
            ->select('*')
            ->addSelect(DB::raw("'$errorMessage' as error_message"))
            ->where('nonce', $nonce)
            ->where('user_id', Auth::user()->id)
            ->first();
    }
}
