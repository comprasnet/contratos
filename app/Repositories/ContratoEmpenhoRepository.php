<?php

namespace App\Repositories;

use App\Models\Empenho;
use App\Models\Empenhodetalhado;
use App\Models\Fornecedor;
use App\Models\Naturezadespesa;
use App\Models\Naturezasubitem;
use App\Models\Planointerno;
use App\Models\Unidade;
use App\services\STA\STAUrlFetcherService;
use App\STA\ConsultaApiSta;
use App\XML\Execsiafi;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ContratoEmpenhoRepository
{

    public function criarEmpenho(Request $request)
    {
        $empenho = null;
        $padrao = '/empenho_nao_encontrado/i';
        $palavraEncontrada = null;

        // Se o empenho for encontrado pelo datatable no nosso banco, ele vem com o id normalmente
        // Se não, ele vem com essa flag 'empenho_nao_encontrado' como id
        // EX: 2023NE999991empenho_nao_encontrado
        if (preg_match($padrao, $request->input('empenho_id'), $matches)) {
            $palavraEncontrada = $matches[0];
        }

        if ($palavraEncontrada !== 'empenho_nao_encontrado') {
            $empenho = Empenho::where('id', $request->input('empenho_id'))
                ->where('fornecedor_id', $request->input('fornecedor_id'))
                ->where('unidade_id', $request->input('unidadeempenho_id'))
                ->first();
        }

        $codigoSiafi = Unidade::where('id', $request->input('unidadeempenho_id'))->first()->codigosiafi;
        $ug = Unidade::where('codigosiafi', $codigoSiafi)->first();

        if ($empenho != null) {
            $request->request->set('empenho_id', $empenho->id);
        } else {
            $unidade_logada_sessao = session()->get('user_ug_id');
            $unidade_logada = Unidade::where('id', $unidade_logada_sessao)->first();
            $numero_empenho_nao_encontrado = $request->input('empenho_id');
            $ug = Unidade::where('codigosiafi', $codigoSiafi)->first();
            $fornecedor = $request->input('fornecedor_id');
            $fornecedor_cnpj_cpf = Fornecedor::where(['id' => $fornecedor])->first()->cpf_cnpj_idgener;

            // Aqui eu removo o 'empenho_nao_encontrado'
            // para passar apenas o número do empenho pro SIAFI ou STA
            $numero_empenho = trim($numero_empenho_nao_encontrado, $palavraEncontrada);

            $objEmpenhoID = $this->buscaInsereEmpenhoSTA(
                $codigoSiafi,
                $ug->gestao,
                $numero_empenho,
                $fornecedor_cnpj_cpf,
                $ug->id,
                $fornecedor
            );

            if (isset($objEmpenhoID['error'])) {
                \Alert::error($objEmpenhoID['error'])->flash();
                return ['error' => true];
            }
            if (!$objEmpenhoID) {
                $SiafiEmpenhoID = $this->buscaInsereEmpenhoSIAFI(
                    $codigoSiafi,
                    $ug->gestao,
                    $numero_empenho,
                    $fornecedor_cnpj_cpf,
                    $ug->id,
                    $fornecedor
                );

                if (!$SiafiEmpenhoID) {
                    \Alert::error('Empenho não encontrado.')->flash();
                    return ['error' => true];
                }
                $objEmpenhoID = $SiafiEmpenhoID;
            }
            $request->request->set('empenho_id', $objEmpenhoID);
        }

        if (!$this->atualizaSaldoEmpenho(
            $request->all(),
            $ug->codigosiafi,
            $ug->gestao,
            config('app.contas_contabeis_empenhodetalhado_exercicioatual')
        )) {
            Log::error('ocorreu um erro ao atualizar o empenho detalhado - id-empenho: ' . $objEmpenhoID);
        }
    }

    protected function buscaInsereEmpenhoSTA($codigo_ug, $unidade_gestao, $empenho, $fornecedor_cnpj_cpf, $unidade_id, $fornecedor_id)
    {
        $sta_host = env('API_STA_HOST');
        // $empenho = '2023NE000264';
        $uri = $sta_host . config('rotas-sta.empenho-por-numero') . $codigo_ug . $unidade_gestao . $empenho;

        // Verifica se os 4 primeiros números do empenho são integer
        $firstFourCharsEmpenho = substr($empenho, 0, 4);
        $isIntEmpenho = ctype_digit($firstFourCharsEmpenho);

        if (empty($isIntEmpenho)) {
            return ['error' => 'Empenho informado inválido.'];
        }

        $urlFetchService = new STAUrlFetcherService($uri);
        $retorno = $urlFetchService->fetchDataJson();

        if ($urlFetchService->isError($retorno)) {
            $error = $urlFetchService->getError($retorno);
            logger()->info(__METHOD__ . ' - Erro ao buscar o empenho no STA ' .
                '[' . $error->error_code . '] ' . $error->error_message);

            return false;
        }

        if (!$retorno) {
            return false;
        }

        $cpf_cnpj_api = preg_replace('/\D/', '', $retorno['cpfcnpjugidgener']);
        $fornecedor = preg_replace('/\D/', '', $fornecedor_cnpj_cpf);

        if ($fornecedor == $cpf_cnpj_api) {
            DB::beginTransaction();
            try {
                $planoInterno = Planointerno::where('codigo', $retorno['picodigo'])
                    ->get()
                    ->first();
                if (!$planoInterno) {
                    // Cria o plano interno caso haja a descrição no retorno
                    if($retorno['pidescricao']){
                        $planoInterno = Planointerno::create([
                            'codigo' => trim($retorno['picodigo']),
                            'descricao' => trim($retorno['pidescricao']),
                            'situacao' => true,
                            'sistema_origem' => $retorno['sistemaorigem']
                        ]);
                    }else{
                        // Se não encontrar o plano interno, ele passa o id do -8
                        $planoInterno = Planointerno::where(['descricao' => 'SEM INFORMACAO'])
                            ->get()
                            ->first();
                    }
                }

                $naturezaDespesa = Naturezadespesa::where('codigo', $retorno['naturezadespesa'])
                    ->where('sistema_origem', $retorno['sistemaorigem'])
                    ->get()
                    ->first();
                // Caso não tenha a ND na nossa base, ele cadastra
                if (!$naturezaDespesa) {
                    unset($naturezaDespesa);

                    $naturezaDespesa = Naturezadespesa::create([
                        'codigo' => $retorno['naturezadespesa'],
                        'descricao' => $retorno['naturezadespesadescricao'],
                        'situacao' => true,
                        'sistema_origem' => $retorno['sistemaorigem']
                    ]);
                }
                $objEmpenho = Empenho::updateOrCreate(
                    [
                        'numero' => $empenho,
                        'unidade_id' => $unidade_id,
                    ],
                    [
                        'fornecedor_id' => $fornecedor_id,
                        'planointerno_id' => $planoInterno->id,
                        'naturezadespesa_id' => $naturezaDespesa->id,
                        'fonte' => $retorno['fonte'],
                        'informacao_complementar' => $retorno['informacaocomplementar'],
                        'sistema_origem' => $retorno['sistemaorigem'],
                        'data_emissao' => $retorno['emissao'],
                        'ptres' => $retorno['ptres'],
                        'created_at' => now(),
                        'updated_at' => now()
                    ]
                );

                if (!empty($retorno['itens'])) {
                    foreach ($retorno['itens'] as $item) {
                        $naturezaSubitem = Naturezasubitem::where('naturezadespesa_id', $naturezaDespesa->id)
                            ->where(
                                'codigo',
                                str_pad($item['subitem'], 2, "0", STR_PAD_LEFT)
                            )
                            ->get()
                            ->first();

                        // Caso não encontre o item na nossa base, ele cadastra
                        if (!$naturezaSubitem) {
                            $naturezaSubitem = Naturezasubitem::create([
                                'naturezadespesa_id' => $naturezaDespesa->id,
                                'codigo' => str_pad($item['subitem'], 2, "0", STR_PAD_LEFT),
                                'descricao' => $item['subitemdescricao'],
                                'situacao' => true,
                            ]);
                        } else {
                            Naturezasubitem::updateOrCreate(
                                [
                                    'naturezadespesa_id' => $naturezaDespesa->id,
                                    'codigo' => str_pad($item['subitem'], 2, "0", STR_PAD_LEFT),
                                ],
                                [
                                    'descricao' => trim($item['subitemdescricao'])
                                ]
                            );
                        }

                        Empenhodetalhado::updateOrCreate(
                            [
                                'empenho_id' => $objEmpenho->id,
                                'naturezasubitem_id' => $naturezaSubitem->id
                            ],
                            [
                                'created_at' => now(),
                                'updated_at' => now()
                            ]
                        );
                    }
                }
                DB::commit();
                return $objEmpenho->id;
            } catch (Exception $e) {
                Log::error('erro em buscaInsereEmpenhoSTA: ' . $e->getMessage());
                DB::rollBack();
            }
        }
    }

    protected function buscaInsereEmpenhoSIAFI($codigo_ug, $unidade_gestao, $empenho, $fornecedor_cnpj_cpf, $unidade_id, $fornecedor_id)
    {
        try {
            DB::beginTransaction();
            $execsiafi = new Execsiafi();
            $user = backpack_user();
            $user->cpf = env('USUARIO_SIAFI');
            $user->senhasiafi = env('SENHA_SIAFI');
            $amb = config('app.ambiente_siafi');
            $ano = substr($empenho, 0, 4);

            if ($execsiafi->validarAnoSiafi($ano) === false) {
                return false;
            }
            $retorno = $execsiafi->empenhoDetalhado($user, $codigo_ug, $amb, $ano, $empenho);

            if ($retorno['situacao'] == 'ERRO') {
                return false;
            } else {
                $cpf_cnpj_api = preg_replace('/\D/', '', $retorno['codFavorecido']);
                $fornecedor = preg_replace('/\D/', '', $fornecedor_cnpj_cpf);

                if ($fornecedor == $cpf_cnpj_api) {
                    $planoInterno = Planointerno::where(['codigo' => trim($retorno['codPlanoInterno'])])
                        ->get()
                        ->first();

                    if (!$planoInterno) {
                        if($retorno['codPlanoInterno'] == ''){
                            $planoInterno = Planointerno::create([
                                'codigo' => '-8',
                                'descricao' => 'SEM INFORMACAO',
                                'situacao' => true,
                                'sistema_origem' => $retorno['sistemaOrigem']
                            ]);
                        }elseif ($retorno['codPlanoInterno']) {
                            $planoInterno = Planointerno::create([
                                'codigo' => $retorno['codPlanoInterno'],
                                'descricao' => 'SEM INFORMACAO',
                                'situacao' => true,
                                'sistema_origem' => $retorno['sistemaOrigem']
                            ]);
                        }else{
                            // Se não encontrar o plano interno, ele passa o id do -8
                            $planoInterno = Planointerno::where(['descricao' => 'SEM INFORMACAO'])
                                ->get()
                                ->first();
                        }
                    }
                    $naturezaDespesa = Naturezadespesa::where(['codigo' => $retorno['naturezaDespesa']])
                        ->get()
                        ->first();

                    // Caso não tenha a ND na nossa base, ele cadastra
                    if (!$naturezaDespesa) {
                        unset($naturezaDespesa);

                        $naturezaDespesa = Naturezadespesa::create([
                            'codigo' => $retorno['naturezaDespesa'],
                            'descricao' => $retorno['descricao'],
                            'situacao' => true,
                            'sistema_origem' => $retorno['sistemaOrigem']
                        ]);
                    }

                    $objEmpenho = Empenho::updateOrCreate(
                        [
                            'numero' => $empenho,
                            'unidade_id' => $unidade_id
                        ],
                        [
                            'fornecedor_id' => $fornecedor_id,
                            'planointerno_id' => $planoInterno->id,
                            'naturezadespesa_id' => $naturezaDespesa->id,
                            'fonte' => $retorno['fonte'],
                            'informacao_complementar' => $retorno['infoComplementar'],
                            'sistema_origem' => $retorno['sistemaOrigem'],
                            'data_emissao' => $retorno['dataEmissao'],
                            'ptres' => $retorno['codPTRES'],
                            'created_at' => now(),
                            'updated_at' => now()
                        ]
                    );

                    if (!empty($retorno['natureza_subitem_desc'])) {
                        foreach ($retorno['natureza_subitem_desc'] as $subitem) {

                            $naturezaSubItem = Naturezasubitem::where(['codigo' => str_pad($subitem['codSubElemento'], 2, "0", STR_PAD_LEFT)])
                                ->where(['naturezadespesa_id' => $naturezaDespesa->id])
                                ->get()
                                ->first();

                            // Caso não encontre o item na nossa base, ele cadastra
                            if (!$naturezaSubItem) {
                                $naturezaSubItem = Naturezasubitem::create([
                                    'naturezadespesa_id' => $naturezaDespesa->id,
                                    'codigo' => str_pad($subitem['codSubElemento'], 2, "0", STR_PAD_LEFT),
                                    'descricao' => $subitem['descricao'],
                                    'situacao' => true,

                                ]);
                            }
                            Empenhodetalhado::updateOrCreate(
                                [
                                    'empenho_id' => $objEmpenho->id
                                ],
                                [
                                    'naturezasubitem_id' => $naturezaSubItem->id,
                                    'created_at' => now(),
                                    'updated_at' => now()
                                ]
                            );
                        }
                    }
                    DB::commit();
                    return $objEmpenho->id;
                }
            }
        } catch (Exception $e) {
            Log::info('erro em buscaInsereEmpenhoSIAFI: ' . $e->getMessage());
            DB::rollBack();
        }
    }

    private function atualizaSaldoEmpenho($request, $codigoSiafi, $gestao, $contasContabeis)
    {
        $empenhoID = $request['empenho_id'];
        $empenho = Empenho::where('id', $empenhoID)->with('empenhodetalhado.naturezasubitem')->first();
        $naturezaSubItem = [];
        $consulta = new ConsultaApiSta();
        $empenho->ano_emissao = ($empenho->ano_emissao) ?: substr($empenho->numero, 0, 4);
        $listaConfigContasContabeis = config('app.contas_contabeis_empenhodetalhado_exercicioatual');

        foreach ($empenho->empenhodetalhado as $empenhoDetalhado) {
            $naturezaSubItem[$empenhoDetalhado->id] = $empenhoDetalhado->naturezasubitem->codigo;
        }

        try {
            DB::beginTransaction();
            foreach ($naturezaSubItem as $id => $subitem) {
                $objEmpenhoDetalhado = $empenho->empenhodetalhado->where('id', $id)->first();

                $empenhoNumero = $empenho->numero . $subitem;
                $retorno = $consulta->saldocontabilAnoUgGestaoVariasContacontabeisContacorrente(
                    $empenho->ano_emissao,
                    $codigoSiafi,
                    $gestao,
                    $contasContabeis,
                    $empenhoNumero
                );


                foreach ($listaConfigContasContabeis as $colunas => $contas) {
                    if (isset($retorno[$contas])) {
                        $objEmpenhoDetalhado->update([
                            $colunas => $retorno[$contas]
                        ]);
                    }
                }
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::error("atualizaSaldoEmpenho: " . $e->getMessage());
            return false;
        }

        return true;
    }
}
