<?php

namespace App\Repositories;

use App\Models\Codigoitem;

class CodigoItemRepository
{
    public function getByDescres($descres){
        return Codigoitem::query()
            ->where('descres','=',$descres)
            ->first();
    }

    public function getByDescresAndDescription($descres,$description){
        return Codigoitem::query()
            ->where('descres','=',$descres)
            ->where('descricao','=',$description)
            ->first();
    }


    public function getByDescresAndLikeDescription($descres,$description){
        return Codigoitem::query()
            ->where('descres','=',$descres)
            ->where('descricao','ilike',$description)
            ->first();
    }
}
