<?php

namespace App\Repositories;

use App\DTO\Termo14133PNCDTO;
use App\Http\Controllers\Admin\EnviaPNCPController;
use App\Models\Codigoitem;
use App\Models\EnviaDadosPncp;
use Illuminate\Support\Facades\DB;

/**
 *
 */
class Termos14133PncpRepository
{
    /**
     * @var Termo14133PNCDTO
     */
    private $termo14133PNCPDTO;

    /**
     * @param Termo14133PNCDTO $termo14133PNCPDTO
     */
    public function __construct(Termo14133PNCDTO $termo14133PNCPDTO)
    {
        $this->termo14133PNCPDTO = $termo14133PNCPDTO;
    }

    /**
     * @return void
     */
    public function enviarTermo14133Pncp()
    {
        $codigoItemByDescriptionAndId = Codigoitem::where('descricao', 'like', '%Termo%')
            ->where('id', intval($this->termo14133PNCPDTO->contratoHistoricoTipoId))
            ->first();

        if ($codigoItemByDescriptionAndId) {

            $amparoLegal = DB::table('amparo_legal_contrato')
                ->join('amparo_legal', 'amparo_legal.id','amparo_legal_contrato.amparo_legal_id')
                ->select('amparo_legal.ato_normativo')
                ->where('amparo_legal_contrato.contrato_id', intval($this->termo14133PNCPDTO->contratoId))
                ->where('amparo_legal.complemento_14133', true)
                ->first();

            if ($amparoLegal) {
                $contratohistoricoId = intval($this->termo14133PNCPDTO->contratohistoricoId);
                $enviaDadosPncp = EnviaDadosPncp::where('pncpable_id', $contratohistoricoId)->first();

                if ($enviaDadosPncp) {
                    (new EnviaPNCPController())->enviaUnidade($enviaDadosPncp->id);
                }
            }
        }
    }
}
