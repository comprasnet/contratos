<?php
namespace App\Repositories;

use App\Models\Empenho;
class EmpenhoRepository
{
    protected $empenho;
    public function __construct(Empenho $empenho)
    {
        $this->empenho = $empenho;
    }

    public function getEmpenho(string $numeroEmpenho, int $fornecedorId, string $unidadeId): ?Empenho
    {

        return $this
            ->empenho
            ->where('numero', $numeroEmpenho)
            ->where('unidade_id', $unidadeId)
            ->where('fornecedor_id', $fornecedorId)
            ->first();
    }
}
