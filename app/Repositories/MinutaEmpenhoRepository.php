<?php

namespace App\Repositories;

use App\Models\MinutaEmpenho;
use Illuminate\Support\Facades\DB;
use App\Models\Empenho;

class MinutaEmpenhoRepository
{
    protected $minutaEmpenho;
    public function __construct(MinutaEmpenho $minutaEmpenho)
    {
        $this->minutaEmpenho = $minutaEmpenho;
    }

    /**
     * Responsável em recuperar as minutas de empenho substitutiva de contrato
     * @return mixed
     */
    public function getMinutasModalCriarContratoTipoEmpenho()
    {
        return $this->minutaEmpenho->select(
            'minutaempenhos.id as id',
            'minutaempenhos.mensagem_siafi as numero_empenho',
            // 'minutaempenhos.valor_total as valor',
            DB::raw("to_char(minutaempenhos.valor_total, 'L9G999G990D99') as valor"),
            DB::raw("to_char(minutaempenhos.data_emissao, 'DD/MM/YYYY') as data_emissao"),
            'fornecedores.cpf_cnpj_idgener as cnpj_fornecedor',
            'fornecedores.nome as nome_fornecedor',
            'codigoitens.descricao',
            'ugemitente.codigo',
            'amparo_legal.ato_normativo',
            'amparo_legal_id',
            'tipo_empenhopor_id',
            'compra_id'
        )
        ->join('fornecedores', 'fornecedores.id', 'minutaempenhos.fornecedor_empenho_id')
        ->join('codigoitens', 'codigoitens.id', 'minutaempenhos.situacao_id')
        ->leftJoin('unidades', 'unidades.id', 'minutaempenhos.unidade_id')
        ->join('saldo_contabil', 'saldo_contabil.id', 'minutaempenhos.saldo_contabil_id')
        ->join(DB::raw("unidades as ugemitente"), 'ugemitente.id', 'saldo_contabil.unidade_id')
        ->join('amparo_legal', 'amparo_legal.id', 'minutaempenhos.amparo_legal_id')
        ->where('minutaempenhos.minutaempenho_forcacontrato', true)
        ->where('minutaempenhos.mensagem_siafi', '<>', null)
        ->where('codigoitens.descricao', 'EMPENHO EMITIDO')
        ->where('minutaempenhos.unidade_id', session('user_ug_id'))
        ->orderby('minutaempenhos.data_emissao', 'ASC')
        ->get();
    }

    /**
     * query para consulta do endpoint /api/v1/contrato/empenho/consultar/{empenho_id}
     * @param $empenhoId
     * @param array $arrayParams
     * @return mixed
     */
    public function recuperarEmpenhoParaConsultaEmpenhoAPI($empenhoId)
    {
        $empenho = Empenho::leftjoin('minutaempenhos', function ($join) {
            $join->on('empenhos.numero', '=', 'minutaempenhos.mensagem_siafi')
                ->whereRaw('minutaempenhos.fornecedor_empenho_id = empenhos.fornecedor_id');
        })
            ->leftJoin('saldo_contabil', function ($join) {
                $join->on('minutaempenhos.saldo_contabil_id', '=', 'saldo_contabil.id')
                    ->whereRaw('empenhos.unidade_id = saldo_contabil.unidade_id');
            })
            ->leftJoin('fornecedores', 'fornecedores.id', '=', 'empenhos.fornecedor_id')
            ->leftJoin('compra_item_minuta_empenho', 'minutaempenhos.id', '=', 'compra_item_minuta_empenho.minutaempenho_id')
            ->leftJoin('compras', 'compras.id', '=', 'minutaempenhos.compra_id')
            ->leftJoin('codigoitens', 'codigoitens.id', '=', 'minutaempenhos.tipo_empenhopor_id')
            ->leftJoin('codigoitens as codigoitens_tipo_empenho', 'codigoitens_tipo_empenho.id', 'minutaempenhos.tipo_empenho_id')
            ->leftJoin('codigoitens as codigoitens_modalidade_compra', 'codigoitens_modalidade_compra.id', 'compras.modalidade_id')
            ->leftJoin('unidades as unidade_uasg_compra', 'unidade_uasg_compra.id', 'compras.unidade_origem_id')
            ->leftJoin('contratos', 'contratos.id', 'minutaempenhos.contrato_id')
            ->join('unidades as ug_emitente_empenho', 'ug_emitente_empenho.id', 'empenhos.unidade_id')
            ->select(
                'empenhos.id as id_empenho',
                'empenhos.data_emissao as data_emissao_empenho',
                'ug_emitente_empenho.codigo as ug_emitente_empenho',
                'empenhos.numero as numero_empenho',
                'fornecedores.cpf_cnpj_idgener as cpf_cnpj_fornecedor_empenho',
                'fornecedores.nome as nome_fornecedor_empenho',
                'codigoitens_tipo_empenho.descricao as tipo_empenho',
                'unidade_uasg_compra.codigo as uasg_compra',
                'codigoitens_modalidade_compra.descricao as modalidade_compra',
                'compras.numero_ano as numero_compra',
                'unidade_uasg_compra.codigo as uasg_beneficiaria',
                DB::raw('(
                    SELECT json_agg(
                        json_build_object(
                            \'numero_contrato\', c.numero, 
                            \'unidade_gestora_contrato\', unidades.codigo,
                            \'contrato_id\', c.id
                        )
                    )
                    FROM contratos c 
                    INNER JOIN contratoempenhos ce ON c.id = ce.contrato_id
                    INNER JOIN unidades ON unidades.id = c.unidade_id
                    WHERE ce.empenho_id = empenhos.id
                ) AS contratos'),
                DB::raw('CASE
                            WHEN compras.numero_ano = \'99999/9999\' THEN NULL ELSE minutaempenhos.informacao_complementar
                    END AS chave'),
                'empenhos.empenhado',
                'empenhos.aliquidar',
                'empenhos.liquidado',
                'empenhos.pago',
                'empenhos.rpinscrito',
                'empenhos.rpaliquidar',
                'empenhos.rpliquidado',
                'empenhos.rppago',
                'empenhos.id_sistema_origem as sistema_origem',
            )
            ->groupBy(
                'empenhos.id',
                'empenhos.data_emissao',
                'ug_emitente_empenho.codigo',
                'empenhos.numero',
                'fornecedores.cpf_cnpj_idgener',
                'fornecedores.nome',
                'codigoitens_tipo_empenho.descricao',
                'unidade_uasg_compra.codigo',
                'codigoitens_modalidade_compra.descricao',
                'compras.numero_ano',
                'unidade_uasg_compra.codigo',
                'contratos.numero',
                'minutaempenhos.id',
                'minutaempenhos.informacao_complementar',
                'empenhos.empenhado',
                'empenhos.aliquidar',
                'empenhos.liquidado',
                'empenhos.pago',
                'empenhos.rpinscrito',
                'empenhos.rpaliquidar',
                'empenhos.rpliquidado',
                'empenhos.rppago',
                'empenhos.id_sistema_origem'
            )
        ->where('empenhos.id', $empenhoId)
        ->first();
        
        $empenho->contratos = json_decode($empenho->contratos);

        return $empenho;

    }

}
