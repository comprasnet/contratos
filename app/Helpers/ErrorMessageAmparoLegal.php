<?php

namespace App\Helpers;

use Illuminate\Http\JsonResponse;
use Illuminate\Database\Eloquent\Collection;

class ErrorMessageAmparoLegal
{

    public static function getErrorMessage($errorCode)
    {
        $errorMessages = [
            'token_expirado' => 'Token expirado',
            'sem_permissao_sistema' => 'Usuário sem permissão no sistema',
            'amparo_legal_não_encontrado' =>  'Amparo Legal não encontrado'

        ];

        return $errorMessages[$errorCode] ?? 'Erro desconhecido';
    }

    public static function getHttpErrorCode($errorCode)
    {
        $errorCodesMap = [
            'token_expirado' => 401,
            'sem_permissao_sistema' => 403,
            'amparo_legal_não_encontrado' => 404
        ];

        return $errorCodesMap[$errorCode] ?? 500; // Usando o código de status HTTP 500 (Internal Server Error) como padrão
    }

    public static function verificaPermissaoSistema()
    {
        $isAdmin = backpack_user()->hasRole(['Administrador']);
        $hasPermissionRole = backpack_user()->hasRole('Acesso API');        

        if (!($isAdmin || $hasPermissionRole)) {
            return response()->json([
                'status' => 'error',
                'message' => self::getErrorMessage('sem_permissao_sistema')
            ], self::getHttpErrorCode('sem_permissao_sistema'));
        }

        return null;
    }

    public static function validarResultado(Collection $result)
    {
        if ($result->isEmpty()) {
            return response()->json([
                'status' => 'error',
                'message' => self::getErrorMessage('amparo_legal_não_encontrado')
            ], self::getHttpErrorCode('amparo_legal_não_encontrado'));
        }

        return null;
    }
}
