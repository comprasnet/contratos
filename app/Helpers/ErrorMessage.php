<?php

namespace App\Helpers;

class ErrorMessage{

    public static function getErrorMessage($statusCode)
    {
        $errorMessages = [
            403 => 'Usuário sem permissão no sistema',
            401 => 'Token expirado',
            404 => 'Registro não encontrado',
            400 => 'Campo {campo} é obrigatório',
            500 => 'Erro interno do servidor',
        ];

        return $errorMessages[$statusCode] ?? 'Erro desconhecido';
    }
}
