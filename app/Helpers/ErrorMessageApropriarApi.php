<?php

namespace App\Helpers;

class ErrorMessageApropriarApi
{
  //  use ContratoFaturaTrait;

    public static function getErrorMessage($errorCode)
    {
        $errorMessages = [
            'token_expirado' => 'Token expirado',
            'nonce_existente' => 'O código nonce já existe',
            'sem_permissao_contrato' => 'Usuário sem permissão no contrato para apropriação',
            'sem_permissao_sistema' => 'Usuário sem permissão no sistema',
            'failed_job_apropriar' => 'Falha no job EnviarApropriacaoParaSiafiJob'
        ];

        return $errorMessages[$errorCode] ?? 'Erro desconhecido';
    }

    public static function getHttpErrorCode($errorCode)
    {
        $errorCodesMap = [
            'token_expirado' => 401,
            'nonce_existente' => 403,
            'sem_permissao_contrato' => 403,
            'sem_permissao_sistema' => 403,
            'failed_job_apropriar' => 503
        ];

        return $errorCodesMap[$errorCode] ?? 422; // Código 422 Unprocessable Entity como padrão
    }
   
}
