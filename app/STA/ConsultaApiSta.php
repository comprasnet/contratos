<?php


namespace App\STA;


use Alert;
use App\Http\Controllers\AdminController;
use App\services\STA\STAUrlFetcherService;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use App\Http\Traits\Formatador;

class ConsultaApiSta
{
    use Formatador;

    protected $host_sta;
    protected const LOG_CHANNEL = 'atualizasaldone';

    public function __construct()
    {
        $this->host_sta = config('migracao.api_sta');
        backpack_auth()->check();
    }

    public function saldocontabilAnoUgGestaoVariasContacontabeisContacorrente(
        string $ano,
        string $ug,
        string $gestao,
        array  $contacontabil,
        string $contacorrente
    )
    {
        $idDoProcesso = Str::uuid();
        $prefix = __METHOD__ . ' | ' . $idDoProcesso . ' | ';

        $body = [
            'ano' => $ano,
            'unidade' => $ug,
            'gestao' => $gestao,
            'contascontabeis' => implode(',', $contacontabil),
            'contacorrente' => $contacorrente
        ];

        $url = $this->host_sta .
            config('rotas-sta.saldocontabil-post-ano-ug-gestao-contascontabeis-contacorrente');

        $urlFetchService = new STAUrlFetcherService($url, $body);
        $dados = $urlFetchService->fetchDataJson();

        if ($urlFetchService->isError($dados)) {

            $error = $urlFetchService->getError($dados);

            Log::channel(self::LOG_CHANNEL)->error($prefix .
                'Erro ' . $error->error_code . ' ao executar a url: ' . $error->error_message);

            if (backpack_user()) {
                Alert::error($error->error_message)->flash();
                return redirect('/execfin/empenho');
            }

            return false;
        }

        $retorno = [];

        if (count($dados) > 0) {
            foreach ($dados as $dado) {
                $retorno[$dado['conta_contabil']] = number_format(
                    $dado['saldo'], 2, '.', '');
            }
        }

        return $retorno;
    }

    public function saldocontabilAnoUgGestaoContacontabilContacorrente(
        string $ano,
        string $ug,
        string $gestao,
        string $contacontabil,
        string $contacorrente
    )
    {
        $idDoProcesso = Str::uuid();
        $prefix = __METHOD__ . ' | ' . $idDoProcesso . ' | ';

        $param = [
            'ano' => $ano,
            'ug' => $ug,
            'gestao' => $gestao,
            'contacontabil' => $contacontabil,
            'contacorrente' => $contacorrente
        ];

        $url = $this->host_sta .
            $this->replaceRouteParam(
                config('rotas-sta.saldocontabil-get-ano-ug-gestao-contascontabeis-contacorrente'),
                $param);

        Log::channel(self::LOG_CHANNEL)->info($prefix . 'Requisição à URL: ' . $url);

        $urlFetchService = new STAUrlFetcherService($url);
        $dados = $urlFetchService->fetchDataJson();

        if ($urlFetchService->isError($dados)) {

            $error = $urlFetchService->getError($dados);

            Log::channel(self::LOG_CHANNEL)->error($prefix .
                'Erro ' . $error->error_code . ' ao executar a url: ' . $error->error_message);

            if (backpack_user()) {
                Alert::error($error->error_message)->flash();
                return redirect('/execfin/empenho');
            }

            return false;
        }

        $retorno = null;

        if (!empty($dados)) {
            #Log::channel(self::LOG_CHANNEL)->info($prefix.'Houve retorno de saldo');
            #Log::channel(self::LOG_CHANNEL)->info($prefix. 'Retorno: '.json_encode($dados));
            $retorno = [
                'tiposaldo' => $dados['tiposaldo'],
                'saldo' => number_format($dados['saldo'], 2, '.', ''),
            ];
        } else {
            Log::channel(self::LOG_CHANNEL)->info($prefix . 'Não houve retorno de saldo.');
        }

        return $retorno;
    }

    public function saldocontabilAnoUgGestaoContacontabil(string $ano, string $ug, string $gestao, string $contacontabil)
    {
        $idDoProcesso = Str::uuid();
        $prefix = __METHOD__ . ' | ' . $idDoProcesso . ' | ';

        $param = [
            'ano' => $ano,
            'ug' => $ug,
            'gestao' => $gestao,
            'contacontabil' => $contacontabil
        ];

        $url = $this->host_sta .
            $this->replaceRouteParam(config('rotas-sta.saldocontabil-por-ano-ug-gestao-contacontabil'), $param);

        $urlFetchService = new STAUrlFetcherService($url);
        $dados = $urlFetchService->fetchDataJson();

        if ($urlFetchService->isError($dados)) {
            $error = $urlFetchService->getError($dados);

            Log::channel(self::LOG_CHANNEL)
                ->info($prefix . ' - Erro ao buscar o empenho no STA ' .
                    '[' . $error->error_code . '] ' . $error->error_message);

            return false;
        }

        $retorno = [];
        $pkCount = (is_array($dados) ? count($dados) : 0);

        if ($pkCount > 0) {
            foreach ($dados as $dado) {
                $retorno[] = [
                    'contacorrente' => $dado['conta_corrente'],
                    'tiposaldo' => $dado['tiposaldo'],
                    'saldo' => number_format($dado['saldo'], 2, '.', ''),
                    'updated_at' => $dado['updated_at']
                ];
            }

            return $retorno;
        }

        return $dados;
    }

    public function saldocontabilAnoUgGestao(string $ano, string $ug, string $gestao)
    {
        $idDoProcesso = Str::uuid();
        $prefix = __METHOD__ . ' | ' . $idDoProcesso . ' | ';

        $param = [
            'ano' => $ano,
            'ug' => $ug,
            'gestao' => $gestao
        ];

        $url = $this->host_sta .
            $this->replaceRouteParam(config('rotas-sta.saldocontabil-por-ano-ug-gestao'), $param);

        $urlFetchService = new STAUrlFetcherService($url);
        $dados = $urlFetchService->fetchDataJson();

        if ($urlFetchService->isError($dados)) {
            $error = $urlFetchService->getError($dados);

            Log::channel(self::LOG_CHANNEL)
                ->info($prefix . ' - Erro ao buscar o empenho no STA ' .
                    '[' . $error->error_code . '] ' . $error->error_message);

            return false;
        }

        $retorno = [];
        foreach ($dados as $dado) {

            $retorno[] = [
                'contacontabil' => $dado['conta_contabil'],
                'contacorrente' => $dado['conta_corrente'],
                'tiposaldo' => $dado['tiposaldo'],
                'saldo' => number_format($dado['saldo'], 2, '.', '')
            ];
        }

        return $retorno;
    }

}
