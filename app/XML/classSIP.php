<?php



namespace App\Http\Controllers;
use SoapClient;


class classSIP extends Controller {
    private $urlWsdl;
    private $soapClient;
    private $SiglaSistema;
    private $IdentificacaoServico;
    public  $url;

    public function __construct($url = 'http://homo-sei.inss.gov.br/sip/controlador_ws.php?servico=wsdl'){
        //definição no homecontroller
        $this->urlWsdl = $this->url; 
        try {
            $this->soapClient = new \SoapClient($urlWsdl, ['exceptions' => true]);

            $this->SiglaSistema = config('app.siglaSistema');
            $this->IdentificacaoServico = config('app.identificacaoServico');
            
        } catch ( Exception $e ) { 
            if ($e->getMessage() == 'Service Unavailable') {
                echo 'Não foi possível conectar ao WEBService do SIP... Tente novamente em alguns minutos!';
                return;
            }
        }
    }

    /* listar permissões no SEI (ok)
    @ IdUnidade : 110003626
    @ Retorna:
	Array
	(
		[0] => stdClass Object
			(
				[IdSistema] => 100000100
				[IdOrgaoUsuario] => 0
				[IdUsuario] => 100001034
				[IdOrigemUsuario] => 36856479115
				[IdOrgaoUnidade] => 0
				[IdUnidade] => 110000000
				[IdOrigemUnidade] => 
				[IdPerfil] => 100000939
				[DataInicial] => 25/05/2020
				[DataFinal] => 
				[SinSubunidades] => N
			)

		[1] => stdClass Object
			(
				[IdSistema] => 100000100
				[IdOrgaoUsuario] => 0
				[IdUsuario] => 100001034
				[IdOrigemUsuario] => 36856479115
				[IdOrgaoUnidade] => 0
				[IdUnidade] => 110000380
				[IdOrigemUnidade] => 06001
				[IdPerfil] => 100000938
				[DataInicial] => 11/11/2019
				[DataFinal] => 
				[SinSubunidades] => N
			)

		[2] => stdClass Object
			(
				[IdSistema] => 100000100
				[IdOrgaoUsuario] => 0
				[IdUsuario] => 100001034
				[IdOrigemUsuario] => 36856479115
				[IdOrgaoUnidade] => 0
				[IdUnidade] => 110000000
				[IdOrigemUnidade] => 
				[IdPerfil] => 100000938
				[DataInicial] => 25/05/2020
				[DataFinal] => 
				[SinSubunidades] => N
			)

		[3] => stdClass Object
			(
				[IdSistema] => 100000100
				[IdOrgaoUsuario] => 0
				[IdUsuario] => 100001034
				[IdOrigemUsuario] => 36856479115
				[IdOrgaoUnidade] => 0
				[IdUnidade] => 110000000
				[IdOrigemUnidade] => 
				[IdPerfil] => 100000940
				[DataInicial] => 25/05/2020
				[DataFinal] => 
				[SinSubunidades] => N
			)

	)
    */ 
    public function listaPermissao($CPF){
        $IdSistema = '100000100';
        $IdOrgaoUsuario = '0';
        $response = $this->soapClient->listarPermissao($IdSistema, $IdOrgaoUsuario, NULL, $CPF, NULL, NULL,NULL, NULL);
        return $response;
    }

    public function listaPermissaoPrincipal($CPF){
        $IdSistema = '100000100';
        $IdOrgaoUsuario = '0';
        $response = $this->soapClient->listarPermissao($IdSistema, $IdOrgaoUsuario, NULL, $CPF, NULL, NULL,NULL, NULL);
        foreach($response as $resp){
            if ($resp->IdPerfil == '100000938' && $resp->IdUnidade != '110000000' && strlen($resp->IdOrigemUnidade)<=8){
                if(is_null($resp->DataFinal)){
                    $responsa["IdSistema"] = $resp->IdSistema;
                    $responsa["IdOrgaoUsuario"] = $resp->IdOrgaoUsuario;
                    $responsa["IdUsuario"] = $resp->IdUsuario;
                    $responsa["IdOrigemUsuario"] = $resp->IdOrigemUsuario;
                    $responsa["IdOrgaoUnidade"] = $resp->IdOrgaoUnidade;
                    $responsa["IdUnidade"] = $resp->IdUnidade;
                    $responsa["IdOrigemUnidade"] = $resp->IdOrigemUnidade;
                    $responsa["IdPerfil"] = $resp->IdPerfil;
                    $responsa["DataInicial"] = $resp->DataInicial;
                    $responsa["DataFinal"] = $resp->DataFinal;
                    $responsa["SinSubunidades"] = $resp->SinSubunidades;
                    $responsa["ativo"] = "S";
                } else {
                    $responsa["ativo"] = "N";
                }
            }
        }
        return (object)$responsa;
    }
    
    /* Carregar Unidades (ok)
    @ IdSistema : 100000100
    @ permite pegar a OL da Tb0700 (posição 7 do array)
    @ Retorna:
    [110000403] => Array
                    (
                            [0] => 110000403
                            [1] => 0
                            [2] => APSAN - GEXCGD
                            [3] => Agência da Previdência Social Anastácio
                            [4] => S
                            [5] => Array
                                    (
                                    )

                            [6] => Array
                                    (
                                            [0] => 110003426
                                            [1] => 110002590
                                            [2] => 110000380
                                    )

                            [7] => 06001220
                    )
    */ 
    public function listaUnidades($IdSistema, $IdUnidade){
        $response = $this->soapClient->carregarUnidades($IdSistema, NULL, $IdUnidade);
        return $response;
    }

}

?>