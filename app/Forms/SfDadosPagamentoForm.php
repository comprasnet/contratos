<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class SfDadosPagamentoForm extends Form
{

    public function buildForm()
    {
        $this->add(
            'dtpgtoreceb',
            'date', [
            'label' => 'Data de Pagamento',
            'rules' => 'required',
            'label_show' => false,
            'attr' => [
                'id' => 'dtpgtoreceb',
                'data-nome-aba-principal' => 'dados-pagamento-tab'
            ]
        ])->add(
            'codcredordevedor',
            'text', [
            'label' => 'Favorecido',
            'rules' => 'required',
            'label_show' => false,
            'attr' => [
                'id' => 'codcredordevedor',
                'data-nome-aba-principal' => 'dados-pagamento-tab'
            ]
        ])->add('btnSubmitFormSfDadosPagamento', 'submit', [
            'label' => '<i class="fa fa-save"></i> Confirmar Dados de Pagamento',
            'attr' => [
                'class' => 'btn btn-success esconderQuandoApropriacaoEmitidaSiafi'
            ]
        ]);
    }
}
