<?php
namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class SfDadosBasicosForm extends Form
{

    public function buildForm()
    {
        $ugPagamento = $this->getFormOptions();

        $opcoes = [];

        if(isset($ugPagamento['sfDadosBasicos'])){
            foreach ($ugPagamento['sfDadosBasicos'] as $item) {
                $value = $item['value'];
                $description = $item['description'];
                $opcoes[$value] = $value . ' - ' . $description;
            }
        }


        $this->add('txtobser', 'textarea', [
            'label' => 'Observação',
            'rules' => 'required|min:1|max:468',
            'required ' => true,
            'attr' => [
                'id' => 'txtobser',
                'data-nome-aba-principal' => 'dados-basicos-tab',
                'spellcheck' => 'false',
            ]
        ])->add('txtinfoadic', 'textarea', [
            'label' => 'Informações Adicionais',
            'required ' => true,
            'attr' => [
                'id' => 'txtinfoadic',
                'data-nome-aba-principal' => 'dados-basicos-tab',
                'spellcheck' => 'false',
            ]
        ])->add('codugpgto', 'select', [
            'label_show' => false,
            'required ' => true,
            'choices' => $opcoes,
            'attr' => [
                'id' => 'codugpgto',
                'data-nome-aba-principal' => 'dados-basicos-tab',
                'spellcheck' => 'false',
                'class' => 'form-control',
                'onchange' => "atualizaUgPagamento(this)"
            ]
        ])->add('vlrtaxacambio', 'text', [
            'label_show' => false,
            'required ' => true,
            'attr' => [
                'id' => 'vlrtaxacambio',
                'data-nome-aba-principal' => 'dados-basicos-tab',
                'onkeyup' => "atualizarMaskMoney(this)",
            ]
        ])->add('dtemis', 'date', [
            'label_show' => false,
            'required ' => true,
            'attr' => [
                'id' => 'dtemis',
                'data-nome-aba-principal' => 'dados-basicos-tab'
            ]
        ])->add('dtateste', 'date', [
            'label_show' => false,
            'required ' => true,
            'attr' => [
                'id' => 'dtateste',
                'data-nome-aba-principal' => 'dados-basicos-tab'
            ]
        ])->add('dtvenc', 'date', [
            'label_show' => false,
            'required ' => true,
            'attr' => [
                'id' => 'dtvenc',
                'data-nome-aba-principal' => 'dados-basicos-tab'
            ]
        ])->add('codcredordevedor', 'text', [
            'label_show' => false,
            'required ' => true,
            'attr' => [
                'id' => 'codcredordevedor-dados-basicos',
                'onchange' => "atualizaCredorApropriacao(this)",
                'data-nome-aba-principal' => 'dados-basicos-tab'
            ]
        ])->add('txtprocesso', 'text', [
            'label_show' => false,
            'required ' => true,
            'attr' => [
                'id' => 'txtprocesso',
                'data-nome-aba-principal' => 'dados-basicos-tab'
            ]
        ])->add('btnSubmitFormSfDadosBasicos', 'submit', [
            'label' => '<i class="custom-shadow fa fa-save"></i> Confirmar Dados Básicos',
            'attr' => [
                'id' => 'btnSubmitFormSfDadosBasicos',
                'class' => 'btn btn-success esconderQuandoApropriacaoEmitidaSiafi custom-shadow'
            ]
        ]);
    }
}
