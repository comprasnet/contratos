<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class MudarUgForm extends Form
{
    public function buildForm()
    {
        // caso seja adm vamos abrir o campo texto para entrar com o código da UG - mvascs@gmail.com //171 adicionando permissão adm suporte
        if (backpack_user()->hasRole('Administrador')
            or backpack_user()->hasRole('Administrador Suporte')
            or backpack_user()->hasRole('Consulta Global')
            or backpack_user()->hasRole('Desenvolvedor')) {
            $ugs = $this->getData('ugs') ?? "NULL";
            $this
            ->add('ug', 'text', [
                'label' => 'Informe o código da UG/UASG',
                'rules' => 'required|max:6',
                'attr' => [
                ],
            ]);
        } else {
            $ugs = $this->getData('ugs') ?? "NULL";
            $this
                ->add('ug', 'select', [
                    'label' => 'UG/UASG',
                    'attr' => [
                    ],
                    'choices' => $ugs,
                    'empty_value' => 'Selecione',
                ]);
        }
    }
}
