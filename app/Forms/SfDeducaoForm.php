<?php
namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class SfDeducaoForm extends Form
{

    public function buildForm()
    {
        $this->add('txtobser', 'textarea', [
            'label' => 'Observação',
            'rules' => 'required|min:1',
            'required ' => true,
            'attr' => [
                'id' => 'txtobser',
                'disabled' => true
            ]
        ]);
    }
}
