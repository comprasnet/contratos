<?php

namespace App\Http\Controllers\Auth;

use App\Http\Traits\Formatador;
use App\Http\Traits\UsuarioAcessoGovBrTrait;
use Backpack\Base\app\Http\Controllers\Controller;
use App\Foundation\Auth\AuthenticatesUsers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Spatie\Activitylog\Traits\LogsActivity;
use Jenssegers\Agent\Agent;

class LoginController extends Controller
{
    use LogsActivity;
    use Formatador;
    use UsuarioAcessoGovBrTrait;

    protected $data = []; // the information we send to the view

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers {
        logout as defaultLogout;
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $guard = backpack_guard_name();

        $this->middleware("guest:$guard", ['except' => 'logout']);

        // ----------------------------------
        // Use the admin prefix in all routes
        // ----------------------------------

        // If not logged in redirect here.
        $this->loginPath = property_exists($this, 'loginPath') ? $this->loginPath
            : backpack_url('login');

        // Redirect here after successful login.
        $this->redirectTo = property_exists($this, 'redirectTo') ? $this->redirectTo
            : backpack_url('dashboard');

        // Redirect here after logout.
        $this->redirectAfterLogout = property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout
            : backpack_url();
    }

    /**
     * Return custom username for authentication.
     *
     * @return string
     */
    public function username()
    {
        return backpack_authentication_column();
    }

    /**
     * Log the user out and redirect him to specific location.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $idUsuario = auth()->id();
    
        // Adicionar verificação se $idUsuario não é nulo
        if (!is_null($idUsuario)) {
            $this->removerDadosUsuarioRedis($idUsuario);
        }
    
        $this->guard()->logout();
    
        // Adicionar verificação para evitar problemas com sessão
        if (session()->has('logged_in') && session('logged_in') === 'gov') {
            $url = config('acessogov.host') . '/logout'
                . '?post_logout_redirect_uri=' . config('app.url') . '/';
            
            // Certificar-se de que os valores das configurações são válidos
            if ($url) {
                Session::flush();
                return Redirect::away($url);
            }
        }
    
        Session::flush();
        return redirect($this->redirectAfterLogout);
    }
    

    /**
     * Get the guard to be used during logout.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return backpack_auth();
    }

    // -------------------------------------------------------
    // Laravel overwrites for loading backpack views
    // -------------------------------------------------------

    /**
     * Show the application login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        $this->data['title'] = trans('backpack::base.login'); // set the page title
        $this->data['username'] = $this->username();

        $blade = 'auth.login-nativo';

        if(config('app.app_amb') == 'Ambiente Treinamento') {
            $blade = 'auth.login-treinamento';
        }

        $acessoSomenteLoginGov = $this->acessarSomenteGov();

        if ($acessoSomenteLoginGov) {
            $blade = 'auth.login-govbr';
        }

        return view("backpack::{$blade}", $this->data);
    }

    /**
     * The user has been authenticated.
     *
     * @param Request $request
     * @param  mixed  $user
     * @return void
     */
    protected function authenticated(Request $request, $user)
    {
        session(['logged_in' => 'nativo']);
        activity()->disableLogging();
        $agent = new Agent();
        $user->last_login_at = Carbon::now();
        $user->last_login_ip = $request->ip();
        $user->last_login_browser = $agent->browser();
        $user->save();
        activity()->enableLogging();
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginFormBpUsr()
    {
        $this->data['title'] = trans('backpack::base.login'); // set the page title
        $this->data['username'] = $this->username();

        $blade = 'backpack::auth.login';

        return view($blade, $this->data);
    }
}
