<?php


namespace App\Http\Controllers;

use App\Models\Contratoarquivo;
use App\Models\Unidade;
use App\Models\Unidadeconfiguracao;
use App\Models\Orgaoconfiguracao;
use Exception;
use Illuminate\Support\Facades\Session;

use SoapClient;

class classSEI extends Controller {
    private $urlWsdl;
    private $soapClient;
    private $SiglaSistema;
    private $IdentificacaoServico;
    private $url;

    public function __construct(){
        //pega a uasg e descobre link SEI
        $urlWsdl = $this->PegaUrlSei();
        $chaveAcesso = $this->PegaChaveAcesso();

        if ($urlWsdl) {
            $this->urlWsdl = $urlWsdl;

            try {
                $this->soapClient = new \SoapClient($this->urlWsdl, ['exceptions' => true]);

                $this->SiglaSistema = config('app.siglaSistema');
                if ($chaveAcesso) {
                    $this->IdentificacaoServico = $chaveAcesso;
                }else{
                    $this->IdentificacaoServico = config('app.identificacaoServico');
                }

            } catch (\Exception $e) { 
                if ($this->soapClient == null) {
                    throw new \Exception($e->getMessage());
                }
                throw new \Exception($e->getMessage());
            }
        } else {
            throw new Exception('Não existe URL do SEI associada a esta UASG! Entre em contato com o Administrador!');
        }
    }

    /*
     * @ SiglaSistema: ComprasNet
     * Pega a URL do SEI conforme configurado na tabela de
     * Unidades / Órgão.
     * Primeiro tenta na Unidade, se não encontrar, pesquisa
     * em Órgãos.
     */
    public function PegaUrlSei(){

        $unidade = Unidade::find(session()->get('user_ug_id'));

        $url = null;

        if (isset($unidade->configuracao->url_sei) and $unidade->configuracao->url_sei !=null){
            $url = $unidade->configuracao->url_sei;
        } else {
            if (isset($unidade->orgao->configuracao->url_sei) and $unidade->orgao->configuracao->url_sei !=null){
                $url = $unidade->orgao->configuracao->url_sei;
            }
        }

        return $url;
    }

    public function PegaChaveAcesso(){

        $unidade = Unidade::find(session()->get('user_ug_id'));

        $chave_acesso_sei = null;
        if (isset($unidade->configuracao->chave_acesso_sei) and $unidade->configuracao->chave_acesso_sei !=null){
            $chave_acesso_sei = $unidade->configuracao->chave_acesso_sei;
        } else {
            if (isset($unidade->orgao->configuracao->chave_acesso_sei) and $unidade->orgao->configuracao->chave_acesso_sei !=null){
                $chave_acesso_sei = $unidade->orgao->configuracao->chave_acesso_sei;
            }
        }

        return $chave_acesso_sei;
    }

    /* Somente para testar conexão com o webservice do órgão. Se resposta for diferente de 0, conectou
      @ SiglaSistema: ComprasNet
      @ IdentificacaoServico: ComprasNetServices
    */
    public function listarSeries(){
        $response = $this->soapClient->listarSeries($this->SiglaSistema, $this->IdentificacaoServico);
        return $response;
    }

    /* listar paises (ok)
    @ IdUnidade : 110003626
    */
    public function listaPaises($IdUnidade){
        $response = $this->soapClient->listarPaises($this->SiglaSistema, $this->IdentificacaoServico, $IdUnidade);
        return $response;
    }

    /* Para pegar UF (ok)
    @ IdUnidade : 110003626
    */
    public function listaUF($IdUnidade){
        $response = $this->soapClient->listarEstados($this->SiglaSistema,$this->IdentificacaoServico, $IdUnidade);
        return $response;
   }

    /* listar cidades (ok)
    @ IdUnidade : 110003626
    */
    public function listaCidades($IdUnidade){
        $response = $this->soapClient->listarCidades($this->SiglaSistema, $this->IdentificacaoServico, $IdUnidade);
        return $response;
    }

    /* para pegar usuário (ok)
    @ IdUnidade : 110003626
    */
    public function listaUsuarios($IdUnidade){
        $response = $this->soapClient->listarUsuarios($this->SiglaSistema, $this->IdentificacaoServico, $IdUnidade);
        return $response;
    }

    /* para pegar lista de unidades (ok)
    */
    public function listaUnidades(){
        $response = $this->soapClient->listarUnidades($this->SiglaSistema, $this->IdentificacaoServico);
        return $response;
    }

    /* listar unidades (ok)
    @ IdUnidade : 110003626
    */
    public function listaMarcadoresUnidade($IdUnidade){
        $IdUnidade = '110000380'; // opcional
        $response = $this->soapClient->listarMarcadoresUnidade($this->SiglaSistema, $this->IdentificacaoServico, $IdUnidade = NULL);
        return $response;
    }

    /* listar hipóteses legais (ok)
    @ IdUnidade : 110003626
    */
    public function listaHipotesesLegais($IdUnidade){
        $response = $this->soapClient->listarHipotesesLegais($this->SiglaSistema, $this->IdentificacaoServico, $IdUnidade);
        return $response;
    }

    /* listar feriados (ok)
    @ IdUnidade : 110003626
    @ Data formato dd-mm-aaaa
    */
    public function listaFeriados($IdUnidade, $DataInicial, $DataFinal){
        $IdOrgao = '0';
        $response = $this->soapClient->listarFeriados($this->SiglaSistema, $this->IdentificacaoServico, $IdUnidade, $IdOrgao, $DataInicial, $DataFinal);
        return $response;
    }

    /* listar extensoes permitidas (ok)
    @ IdUnidade : 110003626
    */
    public function listaExtensoesPermitidas($IdUnidade){
        $response = $this->soapClient->listarExtensoesPermitidas($this->SiglaSistema, $this->IdentificacaoServico, $IdUnidade);
        return $response;
    }

    /* listar extensoes permitidas (ok)
    @ IdUnidade : 110003626
    */
    public function listaContatos($IdUnidade){
        $response = $this->soapClient->listarContatos($this->SiglaSistema, $this->IdentificacaoServico, $IdUnidade);
        return $response;
    }

    /* listar cargos (ok)
    @ IdUnidade : 110003626
    */
    public function listaCargos($IdUnidade){
        $response = $this->soapClient->listarCargos($this->SiglaSistema, $this->IdentificacaoServico, $IdUnidade);
        return $response;
    }

   /* listar marcadores (ok)
      @ IdUnidade : id do registro disponível no link (110003626)
      @ ProtocoloProcedimento:  35014.000184/2020-95
    */
    public function listarAndamentosMarcadores($IdUnidade, $ProtocoloProcedimento){
        $response = $this->soapClient->listarAndamentosMarcadores($this->SiglaSistema, $this->IdentificacaoServico, $IdUnidade, $ProtocoloProcedimento);
        return $response;
    }

    /* listar documentos (ok)
    @ IdUnidade : 110003626
    @ ProtocoloDocumento: 0089816
    */
    public function consultarDocumento($IdUnidade, $ProtocoloDocumento){
        $SinRetornarAndamentoGeracao = 'S';
        $SinRetornarAssinaturas  = 'S';
        $SinRetornarPublicacao  = 'N';
        $SinRetornarCampos = 'S';
        $response = $this->soapClient->consultarDocumento($this->SiglaSistema, $this->IdentificacaoServico, $IdUnidade, $ProtocoloDocumento, $SinRetornarAndamentoGeracao, $SinRetornarAssinaturas, $SinRetornarPublicacao, $SinRetornarCampos);
        return $response;
    }

    /* consultar documentos (ok)

       MÉTODO PRINCIPAL PARA CONSULTA

    @ IdUnidade : 110003626
    @ ProtocoloDocumento: 35014.000001/2020-31
    */
    public function consultarProcedimento($IdUnidade = null, $ProtocoloDocumento){
        $SinRetornarAssuntos = 'S';
        $SinRetornarInteressados = 'S';
        $SinRetornarObservacoes = 'S';
        $SinRetornarAndamentoGeracao = 'S';
        $SinRetornarAndamentoConclusao = 'S';
        $SinRetornarUltimoAndamento = 'S';
        $SinRetornarUnidadesProcedimentoAberto = 'S';
        $SinRetonarProcedimentosRelacionados = 'S';
        $SinRetornarProcedimentosAnexados = 'S';
        $response = $this->soapClient->consultarProcedimento($this->SiglaSistema, $this->IdentificacaoServico, $IdUnidade, $ProtocoloDocumento, $SinRetornarAssuntos, $SinRetornarInteressados, $SinRetornarObservacoes, $SinRetornarAndamentoGeracao, $SinRetornarAndamentoConclusao, $SinRetornarUltimoAndamento, $SinRetornarUnidadesProcedimentoAberto, $SinRetonarProcedimentosRelacionados, $SinRetornarProcedimentosAnexados);

        return $response;
    }

    /* incluir documentos em processo existente (ok)

       MÉTODO PRINCIPAL PARA INCLUSÃO

    @ IdUnidade : 110003626
    @ IdProcedimento: 103274 // pegar na url do processo
    @ Descricao: descrição do despacho
    @ Observacao: observação do despacho
    @ Texto: texto do despacho
    */
    //ADICIONAR PARAMETRO ID SERIE
    public function incluirDocumento($IdUnidade, $IdProcedimento, $Descricao, $arrayInteressados, $arrayDestinatarios, $Observacao, $Texto,$tipo_documento = '5'){
        //Documento Gerado
        $DocumentoGerado = array();
        $DocumentoGerado['Tipo'] = 'G';

        //se incluindo em um processo existente informar o id neste campo
        //se incluindo o documento no momento da geracao do processo passar null
        $DocumentoGerado['IdProcedimento'] = $IdProcedimento; // pegar na url do processo
        $DocumentoGerado['IdSerie'] = $tipo_documento; // 5 = Despacho
        //Portaria
        $DocumentoGerado['Numero'] = null;
        $DocumentoGerado['Data'] = null;
        $DocumentoGerado['Descricao'] = $Descricao;
        $DocumentoGerado['Remetente'] = null;
        $arrInteressados = array();
        $arrInteressados[] = array('Sigla'=>'GEXCGD', 'Nome' => 'Raimundo');
        $DocumentoGerado['Interessados'] = $arrInteressados;

        $arrDestinatarios = array();
        $arrDestinatarios[] = array('Sigla'=>'udv', 'Nome' => 'Álvaro');
        $DocumentoGerado['Destinatarios'] = $arrDestinatarios;
        $DocumentoGerado['Observacao'] = 'observacao teste documento portaria 2';
        $DocumentoGerado['NomeArquivo'] = null;
        $textoGrande = $Texto;
        $DocumentoGerado['Conteudo'] = base64_encode($textoGrande);
        $DocumentoGerado['NivelAcesso'] = 0; //0 - publico 1 - restrito 2 - sigiloso
        $response = $this->soapClient->incluirDocumento($this->SiglaSistema, $this->IdentificacaoServico, $IdUnidade, $DocumentoGerado);
        return $response;
    }

    /* Método para enviar um processo para um determinado setor

    @ ProtocoloDocumento : 35092.000365/2015-21
    @ ArrayOfIdUnidade: 110000428 (ver tabela de unidades x ol)
    */
    public function enviarProcesso($protocolo = '35092.000365/2015-21', $minhaUnidade='110000428', $destino = ['Sigla'=>'110000380', 'Nome' => 110000380]){
        $ProtocoloDocumento = $protocolo; //'35092.000365/2015-21';
        $IdUnidade = $minhaUnidade; //['Sigla'=>'110000380', 'Nome' => 110000380]; // id da unidade destino
        $ArrayOfIdUnidade = $destino; //['Sigla'=>'110000380', 'Nome' => 110000380];
        $response = $this->soapClient->enviarProcesso($this->SiglaSistema, $this->IdentificacaoServico, $IdUnidade, $ProtocoloDocumento, $ArrayOfIdUnidade,null,null,null,null,null,null,null);

	return $response;

    }

    /* Método para enviar um processo para um determinado setor

    @ ProtocoloDocumento : 35092.000365/2015-21
    @ ArrayOfIdUnidade: 110000428 (ver tabela de unidades x ol)
    */
    public function listarTipoDocumentos(){
        $response = [
                        ['item'=>'3', 'descricao'=>'Ato'],
                        ['item'=>'310', 'descricao'=>'Carta'],
                        ['item'=>'5', 'descricao'=>'Despacho'],
                        ['item'=>'11', 'descricao'=>'Ofício'],
                        ['item'=>'309', 'descricao'=>'Relatório'],
                        ['item'=>'242', 'descricao'=>'Termo Aditivo'],
                        ['item'=>'244', 'descricao'=>'Termo de Apostilamento']
                    ];
        return $response;
    }

    /* Método para pegar a extensão do arquivo pela URL
     *
     @ Link: https://seihom.economia.gov.br/sei/processo_acesso_externo_consulta.php?id_acesso_externo=228630&infra_hash=511860f78584128c5635704db1349ea9
     @ Retorna: pdf, xml....
     */
    public function pegaExtensaoArquivoURL($link = "https://seihom.economia.gov.br/sei/documento_consulta_externa.php?id_acesso_externo=1830&id_documento=105814&infra_hash=e7d546023451570aba117d7c010fe7f4"){
        $url      = $link;
        $buffer   = file_get_contents($url);
        $finfo    = new finfo(FILEINFO_MIME_TYPE); //var_dump($finfo->buffer($buffer));
        $extensao = explode("/",$finfo->buffer($buffer)); //string(8) "text/xml"
        return $extensao[1];
    }

    /*
     * Método para limpar o anexo scrapp do SEI
     * @Link: https://seihom.economia.gov.br/sei/processo_acesso_externo_consulta.php?id_acesso_externo=228630&infra_hash=511860f78584128c5635704db1349ea9
     * @Returno: link puro
     */
    public function limpaLinkAnexo($link){
        $linkSei = explode('/',$this->urlWsdl);
        $linkSistemaSeiAbsoluto = $linkSei[0].'//'.$linkSei[2].'/'.$linkSei[3].'/';
        if($link[1]=='javascript:void(0);'){
            $aux1 = str_replace("infraLimparFormatarTrAcessada(this.parentNode.parentNode);window.open('","",$link[0]);
        } else{
            $aux1 = $link[1];
        }
        $aux2 = str_replace("');","",$linkSistemaSeiAbsoluto.$aux1);
        return $aux2;
    }

    /* Método para pesquisar os anexos de um processo
    @ Link : https://homo-sei.inss.gov.br/sei/processo_acesso_externo_consulta.php?id_acesso_externo=1830&infra_hash=98073da000e7cea9cf7a89a8bb1f82cb
    @ Retorno: lista de anexos
    */
    public function listarAnexosProcesso($link = "https://seihom.economia.gov.br/sei/processo_acesso_externo_consulta.php?id_acesso_externo=228630&infra_hash=511860f78584128c5635704db1349ea9",$idProcesso){
        $html_content = file_get_contents($link);
        $dom = @\DOMDocument::loadHTML($html_content);
        $xpath = new \DOMXPath($dom);
        $nodes = $xpath->query('//div[@class="infraAreaTabela"]');
        $auxTable = "<link rel='stylesheet' type='text/css' href='https://cdn.datatables.net/1.11.0/css/jquery.dataTables.min.css'>";
        $auxTable .= "<h4>Anexos disponíveis no SEI para o Processo selecionado acima </h4><hr>";
        $auxTable .= "<table width='100%' id='example' class='table dataTable no-footer dtr-inline table-responsive-md'><thead><tr>";
        foreach ($nodes as $node) {
            $cabecalho = $xpath->query('table/tr/th[@class="infraTh"]', $node);
            foreach ($cabecalho as $cabeca){
                $aux[] = $cabeca->nodeValue;
                if (!empty($cabeca->nodeValue))
                   $auxTable .= "<td align='center'><strong>".$cabeca->nodeValue."</strong></td>";
            }

            $auxTable .= "</tr></thead><tbody>";
            $trs = $xpath->query('table/tr[@class="infraTrClara"]', $node);
            $i=0;
            foreach ($trs as $tr){
                if (strlen($tr->nodeValue)>0){

                    $linkSujoSei = $xpath->query('//a[@class="ancoraPadraoAzul" or @class="ancoraPadraoPreta"]', $node)->item($i)->getAttribute('onclick');
                    $linkSujoSuper = $xpath->query('//a[@class="ancoraPadraoAzul" or @class="ancoraPadraoPreta"]', $node)->item($i)->getAttribute('href');
                    $linkSujo = array($linkSujoSei,$linkSujoSuper);
                }
                $linkLimpo = $this->limpaLinkAnexo($linkSujo);
                $encontrouExcluido = strpos($linkLimpo, "Documento cancelado.");
                if ($encontrouExcluido===false)
                    $extensaoArquivo = "pdf";
                else
                    $extensaoArquivo = "html"; 

                if ($extensaoArquivo=="pdf") {
                    $checkVisivel = "";
                    $semLink = "";
                    $imagemIcon = "<i class='fa fa-search'  style='padding-left:15px;cursor:pointer' title='Visualizar o anexo'></i>";
                } elseif ($extensaoArquivo=="html") {
                    $checkVisivel = "disabled";
                    $semLink = "no";
                    $imagemIcon = "<img src='../Templates/images/botao_visualizar.fw.png' style='opacity: 0.5;' width='60' border='0' title='Documento cancelado no SEI'>";
                } elseif ($extensaoArquivo=="xml") {
                    $imagemIcon = "<img src='../Templates/images/444.png' width='60' border='0'>";
                } else {
                    $imagemIcon = "<i class='fa fa-search'  style='padding-left:15px;cursor:pointer' title='Visualizar o anexo'></i>";
                }

                if ($extensaoArquivo=="pdf" || $extensaoArquivo=="html"){

                        $validacaoAnexoSEI =  Contratoarquivo::select('contrato_id')->whereNull("deleted_at")->where("link_sei",$linkLimpo)->pluck('contrato_id')->toArray();;

                        $auxTexto = preg_replace("/\n/","<td><center>",$tr->nodeValue,3);
                        $auxTexto = explode("<td><center>", $auxTexto);

                        if (!in_array($idProcesso, $validacaoAnexoSEI)) {
                            $auxTable .= "<tr>";
                            $auxTable .= "<td style='border:1pt solid #f4f4f4'><input $checkVisivel style='display:block;float:left;margin-left:30px' type='checkbox' name='checkAnexo' value='".$linkLimpo."|".ltrim($auxTexto[1])."|".ltrim($auxTexto[0])."' ><a ".$semLink."href='".$linkLimpo."' target='_blank' title='Vizualizar o anexo do SEI'>$imagemIcon</a></td><td style='border:1pt solid #f4f4f4'><center>".preg_replace("/\n/","<td style='border:1pt solid #f4f4f4'><center>",$tr->nodeValue,3)."</center></td>";
                            $auxTable .= "</tr>";
                        }

                }
                $i++;
            }
        }

        $auxTable .= "</tbody></table>"
                . "<br><button id='btnAssocia' type='button'  onclick='escolheSeiAnexos()'  class='btn btn-info' style='float:right'><i class='fa fa-link'></i> Selecionar Registros</button><br><br>"
                . "<!--<script type='text/javascript' src='../Templates/js/dataTables/jquery-3.5.1.js'></script>-->"
                . "<script type='text/javascript' src='https://cdn.datatables.net/1.11.0/js/jquery.dataTables.min.js'></script>
                <script>
                    $(document).ready(function() {
                            $('#example').DataTable({
                                lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, 'Tudo']],
                                language:{
                                \"sEmptyTable\": \"Nenhum registro encontrado\",
                                \"sInfo\": \"Mostrando de _START_ até _END_ de _TOTAL_ registros\",
                                \"sInfoEmpty\": \"Mostrando 0 até 0 de 0 registros\",
                                \"sInfoFiltered\": \"(Filtrados de _MAX_ registros)\",
                                \"sInfoPostFix\": \"\",
                                \"sInfoThousands\": \".\",
                                \"sLengthMenu\": \"_MENU_ resultados por página\",
                                \"sLoadingRecords\": \"Carregando...\",
                                \"sProcessing\": \"Processando...\",
                                \"sZeroRecords\": \"Nenhum registro encontrado\",
                                \"sSearch\": \"Pesquisar\",
                                \"oPaginate\": {
                                    \"sNext\": \"Próximo\",
                                    \"sPrevious\": \"Anterior\",
                                    \"sFirst\": \"Primeiro\",
                                    \"sLast\": \"Último\"
                                },
                                \"oAria\": {
                                    \"sSortAscending\": \": Ordenar colunas de forma ascendente\",
                                    \"sSortDescending\": \": Ordenar colunas de forma descendente\"
                                }
                            }
                            });
                    });
                </script>";


        return $auxTable;

    }

    public function lerXMLSei($arquivo = "notafiscalxml.xml"){
        $link = $arquivo;
        $xml = simplexml_load_file($link);
        foreach($xml as $item){
            echo "<strong>Número:</strong> "
            .utf8_decode($xml->NFe->infNFe->ide->nNF)."<br />";
            echo "<strong>Natureza:</strong> "
            .utf8_decode($xml->NFe->infNFe->ide->natOp)."<br />";
            echo "<strong>D.Emissao:</strong> "
            .utf8_decode($xml->NFe->infNFe->ide->dhEmi)."<br />";
            echo "<strong>CNPJ:</strong> "
            .utf8_decode($xml->NFe->infNFe->emit->CNPJ)."<br />";
            echo "<strong>Razão Social:</strong> "
            .utf8_decode($xml->NFe->infNFe->emit->xNome)."<br />";
            echo "<br />";
            echo "<strong>Valor R$:</strong> "
            .utf8_decode($xml->NFe->infNFe->total->ICMSTot->vNF)."<br />";
            echo "<br />";

        }
    }

    public function ListarProcessosFilho($processo){
        $result = $this->consultarProcedimento(null, $processo);
        return $result;
    }

}

/*
  Exemplos
*/
//$objSei = new SEI();
//$listaUF = $objSei->listaUF('110005398'); // lista UF
//var_dump($listaUF);exit;
/* Para incluir um documento (despacho)

   1 - Consulto pelo número do processo
   2 - Obtenho o ID do processo
   3 - Incluo o Despcaho
*/
/*$listaProcedimentos = $objSei->consultarProcedimento('110003626', '35014.000001/2020-31'); // listar dados do processo para pegar ID, por exemplo
var_dump($listaProcedimentos->UltimoAndamento);
$IdProcedimento = $listaProcedimentos->IdProcedimento;
$arrayInteressados[] = null;
$arrayDestinatarios[] = null;
$Texto = 'Texto do despacho que será gerado aqui';*/
/*$incluirDocumento = $objSei->incluirDocumento(
                                    '110003626',
                                    $IdProcedimento,
                                    'Descricao aqui',
                                    $arrayInteressados,
                                    $arrayDestinatarios,
                                    'Observacao aqui',
                                    $Texto);         */

?>
