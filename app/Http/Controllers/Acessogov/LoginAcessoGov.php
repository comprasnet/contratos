<?php

namespace App\Http\Controllers\Acessogov;

use App\Http\Controllers\Controller;
use App\Models\BackpackUser;
use Carbon\Carbon;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use Jenssegers\Agent\Agent;

class LoginAcessoGov extends Controller
{
    private $api_acessogov;
    private $host_acessogov;
    private $response_type;
    private $redirect_uri;
    private $client_id;
    private $scope;
    private $nonce;
    private $state;
    private $secret;

    const MSG_ERRO = 'Ocorreu um erro ao se comunicar com o acesso gov, tente novamente mais tarde';
    const MSG_CHECK_EMAIL = "Seu e-mail não foi validado no cadastro Gov.br. Acesse o site acesso.gov para realizar a validação.";
    const MSG_ERRO_NIVEL = "Acesso Negado: Para continuar é necessário nível Prata ou Superior.";

    public function __construct()
    {
        $this->api_acessogov  = config('acessogov.api_host');
        $this->host_acessogov = config('acessogov.host');
        $this->response_type  = config('acessogov.response_type');
        $this->client_id	  = config('acessogov.client_id');
        $this->scope          = config('acessogov.scope');
        $this->redirect_uri   = config('app.url') . '/acessogov';
        $this->nonce          = Str::random(12);
        $this->state          = Str::random(13);
        $this->secret         = config('acessogov.secret');
    }

    public function autorizacao()
    {
        $url = $this->host_acessogov
            . '/authorize?response_type=' . $this->response_type
            . '&client_id=' . $this->client_id
            . '&scope=' . $this->scope
            . '&redirect_uri=' . urlencode($this->redirect_uri . '/tokenacesso')
            . '&nonce=' . $this->nonce
            . '&state=' . $this->state;

        return Redirect::away($url);
    }

    public function tokenAcesso(Request $request)
    {
        try {
            $fields_string = '';

            $headers = array(
                'Content-Type:application/x-www-form-urlencoded',
                'Authorization: Basic '. base64_encode($this->client_id . ":" . $this->secret)
            );

            $campos = array(
                'grant_type' => 'authorization_code',
                'code' => $request->get('code'),
                'redirect_uri' => urlencode($this->redirect_uri.'/tokenacesso')
            );

            foreach($campos as $key=>$value) {
                $fields_string .= $key.'='.$value.'&';
            }

            rtrim($fields_string, '&');

            $urlProvider = $this->host_acessogov . '/token';
            $urlJwk = $this->host_acessogov. '/jwk';

            $ch_token = curl_init();
            curl_setopt($ch_token, CURLOPT_URL, $urlProvider);
            curl_setopt($ch_token, CURLOPT_POSTFIELDS, $fields_string);
            curl_setopt($ch_token, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch_token, CURLOPT_SSL_VERIFYPEER, true);
            curl_setopt($ch_token, CURLOPT_POST, true);
            curl_setopt($ch_token, CURLOPT_HTTPHEADER, $headers);
            $json_output_tokens = json_decode(curl_exec($ch_token), true);
            curl_close($ch_token);

            $ch_jwk = curl_init();
            curl_setopt($ch_jwk,CURLOPT_SSL_VERIFYPEER, true);
            curl_setopt($ch_jwk,CURLOPT_URL, $urlJwk);
            curl_setopt($ch_jwk, CURLOPT_RETURNTRANSFER, TRUE);
            $json_output_jwk = json_decode(curl_exec($ch_jwk), true);
            curl_close($ch_jwk);

            $access_token = $json_output_tokens['access_token'];

            try{
                $json_output_payload_access_token = $this->processToClaims($access_token, $json_output_jwk);
            } catch (Exception $e) {
                session(['url.intended' => url()->previous()]);
                return redirect()->route('login')->withError($e->getMessage());
            }

            $id_token = $json_output_tokens['id_token'];

            try{
                $json_output_payload_id_token = $this->processToClaims($id_token, $json_output_jwk);
            } catch (Exception $e) {
                session(['url.intended' => url()->previous()]);
                return redirect()->route('login')->withError($e->getMessage());
            }

            $retorno = ['access_token' => $access_token, 'id_token' => $id_token];

            $rota = $this->retornaDados($retorno);

            if($rota == 'login')
                return redirect('login')->withWarning(self::MSG_CHECK_EMAIL);

            if ($rota == 'nivel') {
                $url = $this->host_acessogov . '/logout'
                    . '?post_logout_redirect_uri=' . config('app.url') . '/';
                return Redirect::away($url);
            }

            return redirect()->intended(route($rota));


        } catch (Exception $e) {
            session(['url.intended' => url()->previous()]);
            return redirect()->route('login')->withError(self::MSG_ERRO);
        }
    }

    public function retornaDados(array $token)
    {
        try {
            $headers = array(
                'Authorization: Bearer '. $token['access_token']
            );

            $url = $this->host_acessogov. "/jwk";

            $ch_jwk = curl_init();
            curl_setopt($ch_jwk, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch_jwk,CURLOPT_SSL_VERIFYPEER, true);
            curl_setopt($ch_jwk,CURLOPT_URL, $url);
            curl_setopt($ch_jwk, CURLOPT_RETURNTRANSFER, TRUE);
            $json_output_jwk = json_decode(curl_exec($ch_jwk), true);
            curl_close($ch_jwk);

            $dados = $this->processToClaims($token['id_token'], $json_output_jwk);

            $verificaNivel = $this->verificaNivel($token, $dados['sub']); //Verifica nivel conta gov

            $rota = $dados['email_verified']
            ? ($verificaNivel ? $this->login($dados) : 'nivel')
            : 'login';
            //($dados['email_verified']) ? $rota = $this->login($dados) : $rota = 'login';

            return $rota;

        } catch (Exception $e) {
            session(['url.intended' => url()->previous()]);
            return redirect()->route('login')->withError(self::MSG_ERRO);
        }
    }

    public function verificaNivel(array $token, $cpf)
    {
        try {
            $headers = array(
                'Authorization: Bearer '. $token['access_token']
            );

            $url = $this->api_acessogov. "/confiabilidades/v3/contas/$cpf/niveis?response-type=ids";
    
            $ch_nivel = curl_init();
            curl_setopt($ch_nivel, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch_nivel, CURLOPT_SSL_VERIFYPEER, true);
            curl_setopt($ch_nivel, CURLOPT_URL, $url);
            curl_setopt($ch_nivel, CURLOPT_RETURNTRANSFER, TRUE);
            $json_output_nivel = json_decode(curl_exec($ch_nivel), true);
            curl_close($ch_nivel);
        
            $id = end($json_output_nivel)['id'];

            if ($id != 1) {
                return true;
            }

            $linkConfiabilidades = config('app.app_amb') == 'Ambiente Produção' 
            ? 'https://confiabilidades.acesso.gov.br' 
            : 'https://confiabilidades.staging.acesso.gov.br';
    
            $msg_erro_nivel = 'Sua conta gov.br não possui nível necessário (prata ou ouro) 
            para utilizar este serviço, acesse o link
            <a href="' . $linkConfiabilidades . '" target="_blank">Confiabilidades.gov.br</a> 
            para adquirir o nível necessário.';

            session()->put('error_nivel', true);
            session()->put('error_msg', $msg_erro_nivel);

            return false;

        } catch (Exception $e) {
            session(['url.intended' => url()->previous()]);
            return redirect()->route('login')->withError(self::MSG_ERRO);
        }
    }

    public function login($dados)
    {
        $cpf = $this->mask($dados['sub'],'###.###.###-##');
        $user = BackpackUser::where('cpf',$cpf)->first();

        if(is_null($user)){
            $rota = $this->cadastraUsuarioAcessoGov($dados);
        }

        $this->verificaEmailGovBR($dados, $user);

        $rota = $this->loginUsuarioAcessoGov($user);


        return $rota;
    }

    public function cadastraUsuarioAcessoGov($dados)
    {
        $params = [
            'cpf' => $this->mask($dados['sub'],'###.###.###-##'),
            'name' => Str::upper($dados['name']),
            'password' => Hash::make($dados['amr'][0] . $this->generateRandomString(5)),
            'email' => $dados['email'],
            'acessogov' => 1
            ];

            $backpackuser = new BackpackUser($params);
            $backpackuser->save();
            $user = BackpackUser::where('cpf',$params['cpf'])->first();

            $rota = $this->loginUsuarioAcessoGov($user);
            return $rota;
    }

    public function loginUsuarioAcessoGov(BackpackUser $user)
    {
        (is_null($user->ugprimaria))?$rota = 'transparencia.index' : $rota = 'backpack.inicio';
        Auth::login($user, true);

        $this->authenticated(request(), $user);
        
        return $rota;
    }

    function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    public function processToClaims($token, $jwk)
    {
        $modulus = JWT::urlsafeB64Decode($jwk['keys'][0]['n']);
        $publicExponent = JWT::urlsafeB64Decode($jwk['keys'][0]['e']);

        $components = array(
            'modulus' => pack('Ca*a*', 2, $this->encodeLength(strlen($modulus)), $modulus),
            'publicExponent' => pack('Ca*a*', 2, $this->encodeLength(strlen($publicExponent)), $publicExponent)
        );
        $RSAPublicKey = pack(
            'Ca*a*a*',
            48,
            $this->encodeLength(strlen($components['modulus']) + strlen($components['publicExponent'])),
            $components['modulus'],
            $components['publicExponent']
        );

        $rsaOID = pack('H*', '300d06092a864886f70d0101010500'); // hex version of MA0GCSqGSIb3DQEBAQUA
        $RSAPublicKey = chr(0) . $RSAPublicKey;
        $RSAPublicKey = chr(3) . $this->encodeLength(strlen($RSAPublicKey)) . $RSAPublicKey;
        $RSAPublicKey = pack(
            'Ca*a*',
            48,
            $this->encodeLength(strlen($rsaOID . $RSAPublicKey)),
            $rsaOID . $RSAPublicKey
        );
        $RSAPublicKey = "-----BEGIN PUBLIC KEY-----\r\n"
            . chunk_split(base64_encode($RSAPublicKey), 64)
            . '-----END PUBLIC KEY-----';

        JWT::$leeway = 3 * 60; //em segundos

        $decoded = JWT::decode($token, $RSAPublicKey, array('RS256'));

        return (array)$decoded;
    }

    public function encodeLength($length)
    {
        if ($length <= 0x7F) {
            return chr($length);
        }

        $temp = ltrim(pack('N', $length), chr(0));
        return pack('Ca*', 0x80 | strlen($temp), $temp);
    }

    function mask($val, $mask){
        $maskared = '';
        $k = 0;

        for($i = 0; $i<=strlen($mask)-1; $i++) {
            if($mask[$i] == '#') {
                if(isset($val[$k]))
                    $maskared .= $val[$k++];
            } else {
                if(isset($mask[$i]))
                    $maskared .= $mask[$i];
            }
        }

        return $maskared;
    }

    /**
     * Método para verificar o email do usuario quando o login é feito pelo govbr
     * se o email for 'cpf_trocar', faz um update para o email vindo do govbr
     */
    public function verificaEmailGovBR($dados, $user){
        //$cpfUnmasked = $dados['sub'];

        if(strpos($user->email, '_trocar') !== false){

            $cpf =$this->mask($dados['sub'],'###.###.###-##');
            try {
                $user = BackpackUser::where('cpf',$cpf)->first()->update(['email' => $dados['email']]);
            } catch (\Exception $e) {
                Log::info("falha VerificaEmailGOVBR: ". $dados['email']. " - userEmail: " . $user->email);
                Log::info($e->getMessage());
            }

        }
    }

    /**
     * The user has been authenticated.
     *
     * @param Request $request
     * @param  mixed  $user
     * @return void
     */
    protected function authenticated(Request $request, $user)
    {
        session(['logged_in' => 'gov']);
        activity()->disableLogging();
        $agent = new Agent();
        $user->last_login_at = Carbon::now();
        $user->last_login_ip = $request->ip();
        $user->last_login_browser = $agent->browser();
        $user->save();
        activity()->enableLogging();
    }

}
