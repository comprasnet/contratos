<?php

namespace App\Http\Controllers;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Facades\Log;

abstract class RestAPIController extends Controller
{
    protected $client;
    protected $token;
    protected $baseUrl;
    protected $headerPadrao = ['Content-Type' => 'application/json'];

    public function __construct()
    {
        $this->client = new Client(['verify' => false]);
    }

    abstract function buscaToken();

    abstract function buscaBaseUrl();

    public function post($url, $header, $body)
    {
        $response = null;
        // verificar qual o identificador cipi que está sendo enviado
        $teste = json_decode($body);
        @$testeIdentificadorCipi = $teste->identificadorCipi;
        try {
            $response = $this->client->request('POST', $url, [
                'body' => $body,
                'headers' => $header
            ]);

            return $response;

        } catch (RequestException $e) {
            if (strlen($e->getResponse()->getBody()->getContents()) > 1) {
                $e->getResponse()->getBody()->rewind();
                $erro = new Exception($e->getResponse()->getBody()->getContents() . ", URL: " . $url);
            } else {
                $erro = new Exception($e->getMessage() . ", URL: " . $url);
            }
            $erro->status = $e->getResponse()->getStatusCode();
            $erro->body = $body;
            throw $erro;
        } catch (Exception $e) {
            $erro = new Exception($e->getMessage() . ", URL: " . $url);
            $erro->body = $body;
            throw $erro;
        } finally {
            if ($response !== null) {
                $response->getBody()->close();
            }
        }
    }

    public function enviarArquivoParaPncp($url, $header, $paramConteudo, $arquivo)
    {
        Log::debug('Fechando corpo da resposta do PNCP');
        $response = null;
        try {
            $response = $this->client->request('POST', $url, [
                'headers' => $header,
                'multipart' => [
                    [
                        'name' => $paramConteudo,
                        'contents' => fopen($arquivo, 'r'),
                    ]
                ]
            ]);
            $statusCode = $response->getStatusCode();
            $responseBody = $response->getBody()->getContents();
            $locationHeader = $response->getHeader('location');
            $location = $locationHeader[0] ?? null;
            $message = 'Envio do arquivo realizado com sucesso.';

            # Verifica se o código está na faixa de erros (4xx ou 5xx)
            if ($statusCode >= 400 && $statusCode < 600) {
                $responseJson = json_decode($responseBody, true);
                $message = $responseJson['message'] ?? 'Ocorreu um erro no envio do arquivo.';
            }
            return response()->json([
                'code' => $statusCode,
                'message' => $message,
                'details' => null, // Pode adicionar detalhes se necessário
                'urlRequisitada' => $url,
                'linkPncp' => $location
            ], 200, ['Content-Type' => 'application/json']);

        } catch (Exception $e) {
            // Tratamento de exceções
            $statusCode = 500;
            $message = $e->getMessage();
            $responseDetails = null;

            # Verifica se é uma RequestException com resposta
            if ($e instanceof RequestException && $e->hasResponse()) {
                $statusCode = $e->getResponse()->getStatusCode();
                $responseBody = $e->getResponse()->getBody()->getContents();
                $responseJson = json_decode($responseBody, true);

                $message = $responseJson['message'] ?? $message;
                $responseDetails = $responseJson['path'] ?? null;
            }

            if (strpos($message, 'Este arquivo já foi enviado para o PNCP') !== false) {
                $message = 'O mesmo arquivo já foi selecionado anteriormente, ' .
                    'não sendo possível enviar arquivo duplicado.';
            } else if (strpos($message, '400 BAD_REQUEST') !== false) {
                # Se a mensagem de retorno iniciar com "400 BAD_REQUEST",
                # retira essa parte e formata a mensagem para ficar mais amigável para o usuário
                $message = str_replace('400 BAD_REQUEST', '', $message);
                $message = trim($message, " \t\n\r\0\x0B'\"") . '.'; # Remove espaços, aspas e apóstrofos
            }

            return response()->json([
                'code' => $statusCode,
                'message' => $message,
                'details' => $responseDetails, // Adiciona detalhes se necessário
                'url' => $url
            ], 200, ['Content-Type' => 'application/json']);
        } finally {
            if ($response) {
                $response->getBody()->close();
            }
        }
    }

    public function get($url, $header)
    {
        try {
            $response = $this->client->request('GET', $url, [
                'headers' => $header
            ]);
            return $response;
        } catch (RequestException $e) {
            if ($e->getResponse() !== null) {

                if (strlen($e->getResponse()->getBody()->getContents()) > 1) {
                    $e->getResponse()->getBody()->rewind();
                    $erro = new Exception($e->getResponse()->getBody()->getContents() . ", URL: " . $url);
                } else {
                    $erro = new Exception($e->getMessage() . ", URL: " . $url);
                }

                $erro->status = $e->getResponse()->getStatusCode();
                throw $erro;
            }
            throw new Exception($e->getMessage() . ", URL: " . $url);;
        } catch (Exception $e) {
            $erro = new Exception($e->getMessage() . ", URL: " . $url);
            throw $erro;
        }
    }

    public function put($url, $header, $body)
    {
        $response = null;
        try {
            $response = $this->client->request('PUT', $url, [
                'body' => $body,
                'headers' => $header
            ]);
            return $response;
        } catch (RequestException $e) {
            if (strlen($e->getResponse()->getBody()->getContents()) > 1) {
                $e->getResponse()->getBody()->rewind();
                $erro = new Exception($e->getResponse()->getBody()->getContents() . ", URL: " . $url);
            } else {
                $erro = new Exception($e->getMessage() . ", URL: " . $url);
            }
            $erro->status = $e->getResponse()->getStatusCode();
            $erro->body = $body;
            throw $erro;
        } catch (Exception $e) {
            $erro = new Exception($e->getMessage() . ", URL: " . $url);
            $erro->body = $body;
            throw $erro;
        } finally {
            if ($response !== null) {
                $response->getBody()->close();
            }
        }
    }

    public function delete($url, $header, $body)
    {
        $response = null;
        try {
            $response = $this->client->request('DELETE', $url, [
                'body' => $body,
                'headers' => $header
            ]);
            return $response;

        } catch (RequestException $e) {
            if (strlen($e->getResponse()->getBody()->getContents()) > 1) {
                $e->getResponse()->getBody()->rewind();
                $erro = new Exception($e->getResponse()->getBody()->getContents() . ", URL: " . $url);
            } else {
                $erro = new Exception($e->getMessage() . ", URL: " . $url);
            }
            $erro->status = $e->getResponse()->getStatusCode();
            throw $erro;
        } catch (Exception $e) {
            $erro = new Exception($e->getMessage() . ", URL: " . $url);
            throw $erro;
        } finally {
            if ($response !== null) {
                $response->getBody()->close();
            }
        }
    }

    public function deletePncpFile($url, $header, $body)
    {
        $response = null;
        try {
            $response = $this->client->request('DELETE', $url, [
                'body' => $body,
                'headers' => $header
            ]);
            $statusCode = $response->getStatusCode();
            $responseBody = $response->getBody()->getContents();
            $message = 'Arquivo excluído com sucesso.';
            # Verifica se o código está na faixa de erros (4xx ou 5xx)
            if ($statusCode >= 400 && $statusCode < 600) {
                $responseJson = json_decode($responseBody, true);
                $message = $responseJson['message'] ?? 'Um erro ocorreu ao tentar excluir o arquivo.';
            }
            return response()->json([
                'code' => $statusCode,
                'message' => $message,
                'details' => null, // Pode adicionar detalhes se necessário
                'url' => $url
            ], 200, ['Content-Type' => 'application/json']);
        } catch (Exception $e) {
            $statusCode = 500;
            $message = $e->getMessage();
            if ($e instanceof RequestException && $e->hasResponse()) {
                $statusCode = $e->getResponse()->getStatusCode();
                $responseBody = $e->getResponse()->getBody()->getContents();
                $responseJson = json_decode($responseBody, true);
                $message = $responseJson['message'] ?? $message;
            }
            return response()->json([
                'code' => $statusCode,
                'message' => $message,
                'details' => null,
                'url' => $url
            ], 200, ['Content-Type' => 'application/json']);
        } finally {
            if ($response !== null) {
                $response->getBody()->close();
            }
        }
    }

    public function montaHeader($autentica, $params)
    {
        if ($autentica) {
            $params = array_merge($params, ["Authorization" => $this->token]);
        }
        return $params;

    }

    public function montaBody($body)
    {
        return json_encode($body);
    }

    public function montaParametros($url, $params)
    {
        return $url . "?" . http_build_query($params);
    }

    public function montaUrl($url, $params)
    {
        $url = $this->baseUrl . $url;
        if ($params) {
            $url = $this->montaParametros($url, $params);
        }
        return $url;
    }

    public function requestTeste($codigo, $header, $body)
    {

        return new Response($codigo, $header, $body);

    }

}
