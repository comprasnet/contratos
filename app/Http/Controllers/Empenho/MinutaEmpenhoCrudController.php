<?php

namespace App\Http\Controllers\Empenho;

use Alert;
use App\Forms\InserirFornecedorForm;
use App\Http\Controllers\Api\PNCP\ContratoControllerPNCP;
use App\Http\Controllers\DownloadsController;
use App\Http\Requests\MinutaEmpenhoRequest as StoreRequest;
use App\Http\Requests\MinutaEmpenhoRequest as UpdateRequest;
use App\Http\Traits\ArquivoBase64ToPdf;
use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\CompraTrait;
use App\Http\Traits\Formatador;
use App\Http\Traits\MinutaEmpenhoTrait;
use App\Jobs\AtualizarSaldoEmpenhoJob;
use App\Jobs\ProcessarLoteEmpenhoPNCPJob;
use App\Models\AmparoLegal;
use App\Models\Codigoitem;
use App\Models\Compra;
use App\Models\CompraItemMinutaEmpenho;
use App\Models\CompraItemUnidade;
use App\Models\ContratoItemMinutaEmpenho;
use App\Models\EnviaDadosPncp;
use App\Models\Fornecedor;
use App\Models\MinutaEmpenho;
use App\Models\MinutaEmpenhoRemessa;
use App\Models\SaldoContabil;
use App\Models\SfOrcEmpenhoDados;
use App\Repositories\Base;
use App\services\PNCP\PncpService;
use App\XML\Execsiafi;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\CrudPanel;
use Carbon\Carbon;
use Exception;
use FormBuilder;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Arr;
use Redirect;
use Route;
use URL;
use App\Http\Traits\PermissaoTrait;
use App\Http\Traits\HelperTrait;

/**
 * Class MinutaEmpenhoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class MinutaEmpenhoCrudController extends CrudController
{
    use Formatador, CompraTrait, ArquivoBase64ToPdf, MinutaEmpenhoTrait, BuscaCodigoItens, PermissaoTrait, HelperTrait;

    /**
     * guarda o ano da minuta passado pela rota
     * @var string
     */
    private $anoMinuta;
    protected $pncpService;

    public function __construct(PncpService $pncpService)
    {
        parent::__construct();
        $this->pncpService = $pncpService;
    }

    public function setup()
    {
        #pega acao da pagina
        $actionMethod = $this->crud->getActionMethod();

        #atribui id da minuta caso seja acao especifica de uma minuta
        $this->minuta_id = ($actionMethod !== 'index' && $actionMethod !== 'search') ?
            $this->crud->getCurrentEntryId() :
            false;

        #pega ano da rota
        $anoRota = \Route::current()->parameter('ano');

        #pega ano da minuta
        $this->anoMinuta = now()->year === (int) $anoRota ? $anoRota : 'anos anteriores';

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\MinutaEmpenho');
        $this->crud->setRoute(config('backpack.base.route_prefix') . 'empenho/minuta');
        $this->crud->setEntityNameStrings('Minuta de empenho', "Minutas de empenho $this->anoMinuta");
        $this->crud->setEditView('vendor.backpack.crud.empenho.edit');
        $this->crud->setShowView('vendor.backpack.crud.empenho.show');
        $this->crud->setListView('vendor.backpack.crud.empenho.list');

        $this->crud->addButtonFromView('top', 'create', 'createbuscacompra');
        $this->crud->addButtonFromView('line', 'update', 'etapaempenho', 'end');
        $this->crud->addButtonFromView('line', 'atualizarsituacaominuta', 'atualizarsituacaominuta', 'beginning');
        $this->crud->addButtonFromView('line', 'deletarminuta', 'deletarminuta', 'end');
        $this->crud->addButtonFromView('line', 'moreminuta', 'moreminuta', 'end');

        $this->crud->urlVoltar = route(
            'empenho.minuta.etapa.subelemento',
            ['minuta_id' => $this->minuta_id]
        );

        $this->crud->allowAccess('update');
        $this->crud->allowAccess('show');
        $this->crud->denyAccess('delete');

        #remove botoes de acao para minutas de anos anteriores
        if (now()->year !== (int) $anoRota) {
            $this->crud->removeButton('create', 'top');
            $this->crud->removeButton('deletarminuta', 'line');
            $this->crud->removeButton('update', 'line');
        }

        #TODO: Remover esse tipo de verificação e fazer pelo ACL
        if ($this->verificaPermissaoConsulta('minuta') === false){
            $this->crud->removeButton('create', 'top');
            $this->crud->denyAccess('delete');
            $this->crud->denyAccess('update');
            $this->crud->removeButton('deletarminuta', 'line');
            $this->crud->removeButton('atualizarsituacaominuta', 'line');

        }

        $this->crud->addClause('where', 'minutaempenhos.unidade_id', '=', session()->get('user_ug_id'));
        $this->crud->addClause(
            'leftJoin',
            'fornecedores',
            'fornecedores.id',
            '=',
            'minutaempenhos.fornecedor_empenho_id'
        );
        $this->crud->addClause('leftJoin', 'codigoitens', 'codigoitens.id', '=', 'minutaempenhos.tipo_empenhopor_id');
        $this->crud->addClause('leftJoin', 'compras', 'compras.id', '=', 'minutaempenhos.compra_id');
        $this->crud->addClause('leftjoin', 'saldo_contabil as sc', 'sc.id', 'minutaempenhos.saldo_contabil_id');
        $this->crud->addClause('leftJoin', 'unidades as u', 'u.id', '=', 'minutaempenhos.unidade_id');
        $this->crud->addClause('leftJoin', 'unidades as uni_compra', 'compras.unidade_origem_id', '=', 'uni_compra.id');
        $this->crud->addClause('leftJoin', 'unidades as unidade_ug_emitente', 'unidade_ug_emitente.id', 'sc.unidade_id');
        $this->crud->addClause('leftJoin', 'fornecedores as f', 'f.id', '=', 'minutaempenhos.fornecedor_empenho_id');
        $this->crud->addClause('leftJoin', 'sforcempenhodados as sf', 'sf.minutaempenho_id', '=', 'minutaempenhos.id');
        $this->crud->addClause('leftJoin', 'codigoitens as situacao', 'situacao.id', '=', 'minutaempenhos.situacao_id');
        $this->crud->addClause('leftJoin', 'codigoitens as modalidade', 'modalidade.id', '=', 'compras.modalidade_id');
        $this->crud->addClause('leftJoin', 'codigoitens as tipo_compra', 'tipo_compra.id', '=', 'compras.tipo_compra_id');
        $this->crud->addClause('leftjoin', 'codigoitens as codigoitens_tipo_empenho_minuta', 'codigoitens_tipo_empenho_minuta.id', '=' ,'minutaempenhos.tipo_empenhopor_id');
        $this->crud->addClause('leftjoin', 'codigoitens as codigoitens_tipo_empenho', 'codigoitens_tipo_empenho.id', '=' ,'minutaempenhos.tipo_empenho_id');
        $this->crud->addClause('leftjoin', 'amparo_legal as amp', 'amp.id', '=' ,'minutaempenhos.amparo_legal_id');


        $this->addClauseAnoMinuta($this->anoMinuta);

        $this->crud->orderBy('minutaempenhos.updated_at', 'desc');
        $this->crud->addClause(
            'select', [
                DB::raw('DISTINCT minutaempenhos.*'),
                'compras.numero_ano as compra_numero_ano',
                'compras.inciso as inciso_compra',
                'compras.lei as lei_compra',
                'situacao.descricao as situacao_minuta_descricao',
                'sf.cpf_user',
                'sf.sfnonce',
                'modalidade.descricao as modalidade_descricao',
                'tipo_compra.descricao as tipo_compra_descricao',
                'codigoitens_tipo_empenho_minuta.descricao as tipo_empenho_minuta_descricao',
                'codigoitens_tipo_empenho.descricao as tipo_empenho_descricao',
                DB::raw(
                    "CASE
                               WHEN minutaempenhos.minutaempenho_forcacontrato = 1
                                THEN 'sim'
                                ELSE 'não'
                           END AS minutaempenho_forcacontrato"
                ),
                DB::raw("CONCAT(u.codigo, ' - ', u.nomeresumido) as unidade_descricao"),
                DB::raw("CONCAT(f.cpf_cnpj_idgener, ' - ', f.nome) as fornec_cpf_cnpj"),
                DB::raw("CONCAT(uni_compra.codigo, ' - ', uni_compra.nomeresumido) as unidade_compra"),
                DB::raw("CONCAT(unidade_ug_emitente.codigo, ' - ', unidade_ug_emitente.nomeresumido) as unidade_ug_emitente_empenho"),
                'sc.unidade_id as id_unidade_saldo_contabil',
                DB::raw(
                    "CASE
                                WHEN
                                    amp.artigo IS NOT NULL
                                    OR amp.paragrafo IS NOT NULL
                                    OR amp.inciso IS NOT NULL
                                    OR amp.alinea IS NOT NULL
                                    THEN CONCAT(
                                        amp.ato_normativo,
                                        COALESCE(CONCAT(' - Artigo: ', amp.artigo), ''),
                                        COALESCE(CONCAT(' - Parágrafo: ', amp.paragrafo), ''),
                                        COALESCE(CONCAT(' - Inciso: ', amp.inciso), ''),
                                        COALESCE(CONCAT(' - Alínea: ', amp.alinea), '')
                                    )
                                    ELSE NULL
                                END as amparo_legal_descricao"
                )
            ]
        );
        $this->crud->addClause('where', function ($query) {
            $query->whereNull('sf.alteracao')
                ->orWhere('sf.alteracao', '=', false);
        });

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */
        $this->crud->enableExportButtons();

        $this->adicionaCampos($this->minuta_id);
        $this->adicionaColunas($this->minuta_id);
        $this->aplicaFiltros();

        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        if ($this->verificaPermissaoConsulta('minuta') === false){
            return redirect()->to('/empenho/minuta/ano/'.date('Y'));
        }

        $redirect_location = parent::storeCrud($request);

        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        $rota = 'empenho/passivo-anterior/' . $this->minuta_id;
        $conta_id = session('conta_id') ?? '';
        if ($conta_id) {
            $rota = route('empenho.crud.passivo-anterior.edit', ['minuta_id' => $conta_id]);
        }
        $request->request->set('taxa_cambio', $this->retornaFormatoAmericano($request->taxa_cambio));
        $request->request->set('etapa', 7);

        $redirect_location = parent::updateCrud($request);

        return Redirect::to($rota);
    }

    public function show($id)
    {
        $content = parent::show($id);

        $removeColumns = [
            'tipo_empenhopor_id',
            'situacao_id',
            'unidade_id',
            'compra_id',
            'fornecedor_compra_id',
            'fornecedor_empenho_id',
            'saldo_contabil_id',
            'tipo_empenho_id',
            'amparo_legal_id',
            'numero_empenho_sequencial',
            'passivo_anterior',
            'conta_contabil_passivo_anterior',
            'credor',
            'modalidade_descricao',
            'tipo_compra_descricao',
            'compra_numero_ano',
            'inciso_compra',
            'lei_compra',
        ];
        $addColumnsShowOnly = [
            'colunas' => [
                [
                    'box' => 'compra',
                    'name' => 'tipo_compra',
                    'label' => 'Tipo da Compra', // Table column heading
                    'type' => 'text',
                    'function_name' => null,
                ],
                [
                    'box' => 'compra',
                    'name' => 'compra_modalidade',
                    'label' => 'Modalidade', // Table column heading
                    'type' => 'text',
                    'function_name' => null,
                ],
                [
                    'box' => 'compra',
                    'name' => 'numero_ano',
                    'label' => 'Número/Ano da Compra', // Table column heading
                    'type' => 'text',
                    'function_name' => null,
                ],
                [
                    'box' => 'compra',
                    'name' => 'inciso',
                    'label' => 'Inciso', // Table column heading
                    'type' => 'text',
                    'function_name' => null,
                ],
                [
                    'box' => 'compra',
                    'name' => 'lei',
                    'label' => 'Lei', // Table column heading
                    'type' => 'text',
                    'function_name' => null,
                ],
                [
                    'box' => 'resumo',
                    'name' => 'situacao_minuta_descricao',
                    'label' => 'Situação',
                    'type' => 'model_function',
                    'function_name' => 'getSituacao'
                ],
                [
                    'box' => 'resumo',
                    'name' => 'minutaempenho_forcacontrato',
                    'label' => 'Empenho Substitutivo de Contrato',
                    'type' => 'model_function',
                    'function_name' => 'textoMinutaForcaContrato',
                    'priority' => 1,
                ],
                [
                    'box' => 'resumo',
                    'name' => 'unidade_descricao',
                    'label' => 'Unidade da Minuta do Empenho',
                    'type' => 'model_function',
                    'function_name' => 'getUnidade',
                ],
                [
                    'box' => 'resumo',
                    'name' => 'unidade_ug_emitente_empenho',
                    'label' => 'Unidade Emitente do Empenho',//UG Emitente
                    'type' => 'model_function',
                    'function_name' => 'getUgEmitente',
                ],
                [
                    'box' => 'resumo',
                    'name' => 'unidade_compra',
                    'label' => 'UASG Compra',
                    'type' => 'model_function',
                    'function_name' => 'getUnidadeCompra',
                ],
                [
                    'box' => 'resumo',
                    'name' => 'fornec_cpf_cnpj',
                    'label' => 'Credor',
                    'type' => 'model_function',
                    'function_name' => 'getFornecedorEmpenho',
                ],
                [
                    'box' => 'resumo',
                    'name' => 'tipo_empenho_minuta_descricao',
                    'label' => 'Tipo de Minuta', // Table column heading
                    'type' => 'model_function',
                    'function_name' => 'getTipoEmpenhoPor',
                ],
                [
                    'box' => 'resumo',
                    'name' => 'tipo_empenho_descricao',
                    'label' => 'Tipo de Empenho', // Table column heading
                    'type' => 'model_function',
                    'function_name' => 'getTipoEmpenho',
                ],
                [
                    'box' => 'resumo',
                    'name' => 'amparo_legal_descricao',
                    'label' => 'Amparo Legal', // Table column heading
                    'type' => 'model_function',
                    'function_name' => 'getAmparoLegal',
                ]
            ]
        ];
        $modMinuta = MinutaEmpenho::find($id);
        $this->adicionaBoxItens($id, $modMinuta);
        $this->adicionaBoxSaldo($id);
        if ($modMinuta->fonte_alterada) {
            $this->adicionaColunaLinkMinutaAnulada($modMinuta);
        }
        $this->crud->removeColumns($removeColumns);
        $this->addColumnsShowOnly($addColumnsShowOnly['colunas']);
        $this->crud->removeColumn('tipo_empenhopor_id');
        $this->crud->removeColumn('situacao_id');
        $this->crud->removeColumn('unidade_id');
        $this->crud->removeColumn('compra_id');

        $this->crud->removeColumn('fornecedor_compra_id');
        $this->crud->removeColumn('fornecedor_empenho_id');
        $this->crud->removeColumn('saldo_contabil_id');

        $this->crud->removeColumn('tipo_empenho_id');
        $this->crud->removeColumn('amparo_legal_id');

        $this->crud->removeColumn('numero_empenho_sequencial');
        $this->crud->removeColumn('passivo_anterior');
        $this->crud->removeColumn('conta_contabil_passivo_anterior');
        $this->crud->removeColumn('tipo_minuta_empenho');

        return $content;
    }

    protected function adicionaCampos($minuta_id)
    {
        $modelo = MinutaEmpenho::find($minuta_id);

        $this->adicionaCampoNumeroEmpenho();
        $this->adicionaCampoCipi();
        $this->adicionaCampoDataEmissao($modelo);
        $this->adicionaCampoTipoEmpenho();
        $this->adicionaCampoFornecedor();
        $this->adicionaCampoProcesso();
        $this->adicionaCampoAmparoLegal($modelo);
        $this->adicionaCampoEmpenhoSubstitutivoDeContrato($modelo);
        $this->adicionaCampoTaxaCambio();
        $this->adicionaCampoLocalEntrega();
        $this->adicionaCampoDescricao();
    }

    protected function adicionaCampoNumeroEmpenho()
    {
        $this->crud->addField([
            'name' => 'numero_empenho_sequencial',
            'label' => 'Número Empenho',
            'type' => 'text',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6',
                'title' => 'Esse campo é opcional. Preencha caso sua unidade deseje' .
                    ' controlar a numeração do empenho. Ao deixar o campo em branco,' .
                    ' o sistema irá realizar o controle da numeração dos empenhos automaticamente.',
            ]
        ]);
    }

    protected function adicionaCampoCipi()
    {
        $this->crud->addField([
            'name' => 'numero_cipi',
            'label' => 'ID CIPI',
            'type' => 'text_cipi',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6',
                'title' => 'Formato padrão: 99.99-99',
            ]
        ]);
    }

    protected function adicionaCampoDataEmissao($minuta): void
    {
        $attributes = [];
        if (isset($minuta->fonte_alterada) && $minuta->fonte_alterada) {
            $attributes['readonly'] = 'readonly';
        }

        $this->crud->addField([
            'name'              => 'data_emissao',
            'label'             => 'Data Emissão',
            'type'              => 'date',
            'attributes'        => $attributes,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6',
                'title' => 'Somente data atual ou retroativa',
            ],
            'searchLogic' => function (Builder $query, $column, $searchTerm) {
                $formats = ['Y-m-d', 'd-m-Y', 'd/m/Y'];
                $parsedDate = null;
                foreach ($formats as $format) {
                    try {
                        $date = \Carbon\Carbon::createFromFormat($format, $searchTerm);
                        $parsedDate = $date->format('Y-m-d');
                        break;
                    } catch (\Exception $e) {
                        continue;
                    }
                }
                if ($parsedDate) {
                    $query->orWhereRaw("to_char(empenhos.data_emissao, 'YYYY-MM-DD') = ?", [$parsedDate]);
                } else {
                    $query->orWhereRaw("empenhos.data_emissao::text ilike ?", ["%{$searchTerm}%"]);
                }
            },
        ]);
    }

    protected function adicionaCampoTipoEmpenho()
    {
        $tipo_empenhos = Codigoitem::whereHas('codigo', function ($query) {
            $query->where('descricao', '=', 'Tipo Minuta Empenho');
        })->where('visivel', false)->orderBy('descricao')->pluck('descricao', 'id')->toArray();

        $this->crud->addField([
            'name' => 'tipo_empenho_id',
            'label' => "Tipo Empenho",
            'type' => 'select2_from_array',
            'options' => $tipo_empenhos,
            'allows_null' => true,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ]
        ]);
    }

    private function montarOptionFornecedorPassoSeis(int $idMinuta)
    {
        $contrato = MinutaEmpenho::join("contratos", "contratos.id", "=", "minutaempenhos.contrato_id")
                    ->join("fornecedores", "fornecedores.id", "=", "contratos.fornecedor_id")
                    ->where("minutaempenhos.id", $idMinuta)
                    ->where("fornecedores.cpf_cnpj_idgener", "not like", "%ESTRANGEIRO%")
                    ->select("fornecedores.id", "contratos.created_at")
                    ->selectRaw("CONCAT(fornecedores.cpf_cnpj_idgener,' - ',fornecedores.nome) as descricao, 0 AS fornecedorSequencial");

        $subContratadas = MinutaEmpenho::join("contratos", "contratos.id", "=", "minutaempenhos.contrato_id")
                          ->join("contratos_fornecedores_pivot", "contratos_fornecedores_pivot.contrato_id", "=", "contratos.id")
                          ->join("fornecedores", "fornecedores.id", "=", "contratos_fornecedores_pivot.fornecedor_id")
                          ->where("minutaempenhos.id", $idMinuta)
                          ->where("fornecedores.cpf_cnpj_idgener", "not like", "%ESTRANGEIRO%")
                          ->select("fornecedores.id", "contratos.created_at")
                          ->selectRaw("CONCAT(fornecedores.cpf_cnpj_idgener,' - SUBCONTRATADO - ',fornecedores.nome) as descricao, 1 AS fornecedorSequencial")
                          ->union($contrato)
                          ->orderBy('fornecedorsequencial')
                          ->pluck("descricao", "id")
                          ->toArray();
        return $subContratadas;
    }

    protected function adicionaCampoFornecedor()
    {

        $tipo_empenhos = $this->montarOptionFornecedorPassoSeis($this->minuta_id);

        if (empty($tipo_empenhos)) {
            $this->crud->addField([
                'label' => "Credor",
                'type' => "select2_from_ajax_credor",
                'name' => 'fornecedor_empenho_id',
                'entity' => 'fornecedor_empenho',
                'attribute' => "cpf_cnpj_idgener",
                'attribute2' => "nome",
                'process_results_template' => 'gescon.process_results_fornecedor',
                'model' => "App\Models\Fornecedor",
                'data_source' => url("api/fornecedor"),
                'placeholder' => "Selecione o fornecedor",
                'minimum_input_length' => 2,
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-6'
                ],
                'form' => $this->retonaFormModal()
            ]);
        } else {
            $this->crud->addField([
                'name' => 'fornecedor_empenho_id',
                'label' => "Credor",
                'type' => 'select2_from_array',
                'options' => $tipo_empenhos,
                'allows_null' => true,
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-6'
                ]
            ]);
        }
    }

    protected function adicionaCampoProcesso()
    {
        $this->crud->addField([
            'name' => 'processo',
            'label' => 'Número Processo',
            'type' => 'numprocminuta',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6',
            ]
        ]);
    }

    protected function adicionaCampoAmparoLegal(MinutaEmpenho $modelo = null)
    {
        $options = $modelo ? $modelo->retornaAmparoPorMinuta($modelo) : [];
        $keyDefault = null;

        if (count($options) == 1) {
            $keyDefault =  array_keys($options)[0];
        }

        $this->crud->addField([
            'name' => 'amparo_legal_id',
            'label' => "Amparo Legal",
            'type' => 'select2_from_array',
            'options' => $options,
            'allows_null' => true,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],
            'default' => $keyDefault,
        ]);
    }

    protected function adicionaCampoEmpenhoSubstitutivoDeContrato(MinutaEmpenho $modelo = null)
    {

        $campoMinutaEmpenhoForcaContrato = [
            'label' => "Empenho Substitutivo de Contrato",
            'name' => 'minutaempenho_forcacontrato',
            'type' => 'radio',
            'options' => [1 => 'Sim', 0 => 'Não'],
            'inline' => true,
            'default' => 0,
            'ico_help' => 'Os empenhos substitutivos de contratos assinados da Lei 14.133 serão enviados
                            automaticamente para o PNCP.',
            'attributes'   => [
                'class'      => 'opc_compra'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ]

            ];
        if ($modelo != null) {
            if ($modelo->getTipoEmpenhoPor() =="Compra") {

               $this->crud->addField( $campoMinutaEmpenhoForcaContrato );

            } else {
                $campoMinutaEmpenhoForcaContrato['attributes'] = [ 'class' => 'opc_compra', 'disabled' => 'disabled'];
                $this->crud->addField( $campoMinutaEmpenhoForcaContrato );

            }
        } else {
            $campoMinutaEmpenhoForcaContrato['attributes'] = [ 'class' => 'opc_compra', 'disabled' => 'disabled'];
            $this->crud->addField( $campoMinutaEmpenhoForcaContrato );

        }
    }

    protected function adicionaCampoTaxaCambio()
    {
        $this->crud->addField([
            'name' => 'taxa_cambio',
            'label' => 'Taxa de Cambio',
            'type' => 'taxa_cambio',
            'attributes' => [
                'id' => 'taxa_cambio'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ]
        ]);
    }

    protected function adicionaCampoLocalEntrega(): void
    {
        $this->crud->addField([
            'name' => 'local_entrega',
            'label' => 'Local de Entrega',
            'attributes' => [
                'onblur' => "maiuscula(this)"
            ]
        ]);
    }

    protected function adicionaCampoDescricao(): void
    {
        $this->crud->addField([
            'name' => 'descricao',
            'label' => 'Descrição / Observação',
            'type' => 'textarea',
            'attributes' => [
                'onblur' => "maiuscula(this)"
            ]
        ]);
    }

    /**
     * Configura a grid de visualização
     */
    protected function adicionaColunas($minuta_id): void
    {
        $this->adicionaColunaUnidade();

        $this->adicionaColunaUGEmitente();
        $this->adicionaColunaFornecedorEmpenho();

        $this->adicionaColunaTipoCompra();
        $this->adicionaColunaUnidadeCompra();
        $this->adicionaColunaModalidade();
        $this->adicionaColunaTipoEmpenhoPor();
        $this->adicionaColunaNumeroAnoCompra();

        $this->adicionaColunaTipoEmpenho();
        $this->adicionaColunaAmparoLegal();

        $this->adicionaColunaIncisoCompra();
        $this->adicionaColunaLeiCompra();
        $this->adicionaColunaMinutaEmpenhoForcaContrato();
        $this->adicionaColunaValorTotal();

        $this->adicionaColunaMensagemSiafi();
        $this->adicionaColunaSituacao();
        $this->adicionaColunaCreatedAt();
        $this->adicionaColunaUpdatedAt();

        $this->adicionaColunaNumeroEmpenho();
        $this->adicionaColunaCipi();
        $this->adicionaColunaDataEmissao();
        $this->adicionaColunaProcesso();
        $this->adicionaColunaTaxaCambio();
        $this->adicionaColunaLocalEntrega();
        $this->adicionaColunaDescricao();

        if (backpack_user()->hasRole('Administrador')) {
            $this->adicionaColunaCpfEmitente();
            $this->adicionaColunaNONCE();
        }

        if($minuta_id){
            $this->adicionaColunaUnidadeBoxCompra($minuta_id);
            $this->adicionaColunaNumeroAnoContrato($minuta_id);
        }
    }

    public function adicionaColunaMinutaEmpenhoForcaContrato() : void
    {
        $this->crud->addColumn([
            'box' => 'resumo',
            'name' => 'minutaempenho_forcacontrato',
            'label' => 'Empenho Substitutivo de Contrato',
            'type' => 'text',
            'priority' => 1,
            'orderable' => true,
            'visibleInTable' => false,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true
        ]);
    }

    public function adicionaColunaSituacao(): void
    {
        $this->crud->addColumn([
            'box' => 'resumo',
            'name' => 'situacao_minuta_descricao',
            'label' => 'Situação',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true
        ]);
    }

    public function adicionaColunaMensagemSiafi(): void
    {
        $this->crud->addColumn([
            'box' => 'resumo',
            'name' => 'getMensagemSiafiTratada',
            'label' => 'Mensagem SIAFI',
            'type' => 'model_function',
            'function_name' => 'getMensagemSiafiTratada',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
            'searchLogic' => function ($query, $column, $searchTerm) {
                $query->orWhere('mensagem_siafi', 'ilike', '%' . $searchTerm . '%');
            },
        ]);
    }

    /**
     * Configura a coluna Unidade
     */
    public function adicionaColunaUnidade(): void
    {

            $this->crud->addColumn([
                'box' => 'resumo',
                'name' => 'unidade_descricao',
                'label' => 'Unidade da Minuta do Empenho',//Unidade Gestora
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true
            ]);


    }

    public function adicionaColunaUGEmitente(): void
    {
        $this->crud->addColumn([
            'box' => 'resumo',
            'name' => 'unidade_ug_emitente_empenho',
            'label' => 'Unidade Emitente do Empenho',//UG Emitente
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true
        ]);

    }

    public function adicionaColunaCpfEmitente()
    {
        $this->crud->addColumn([
            'box' => 'resumo',
            'name' => 'cpf_user',
            'label' => 'CPF Emitente', // Table column heading
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => false, // no point, since it's a large text
            'visibleInModal' => true, // would make the modal too big
            'visibleInExport' => true, // not important enough
            'visibleInShow' => false, // sure, why not
            'searchLogic' => function ($query, $column, $searchTerm) {
                $query->orWhereHas('empenho_dados', function ($q) use ($column, $searchTerm) {
                    $q->where('sforcempenhodados.cpf_user', 'like', '%' . $searchTerm . '%');
                    $q->whereRaw('sforcempenhodados.id IN (select max(id) from sforcempenhodados where minutaempenho_id = minutaempenhos.id)');
                    $q->whereRaw("situacao.descricao = 'EMPENHO EMITIDO'"); // esta situação está sendo trazida na addClause
                });
            },
        ]);
    }

    public function adicionaColunaNONCE()
    {
        $this->crud->addColumn([
            'box' => 'resumo',
            'name' => 'sfnonce',
            'label' => 'NONCE', // Table column heading
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => false, // no point, since it's a large text
            'visibleInModal' => true, // would make the modal too big
            'visibleInExport' => true, // not important enough
            'visibleInShow' => false, // sure, why not
            'searchLogic' => function ($query, $column, $searchTerm) {
                $query->orWhereHas('empenho_dados', function ($q) use ($column, $searchTerm) {
                    $q->where('sforcempenhodados.sfnonce', 'like', '%' . $searchTerm . '%');
                });
            },
        ]);
    }

    public function adicionaColunaCreatedAt(): void
    {
        $this->crud->addColumn([
            'box' => 'resumo',
            'name' => 'created_at',
            'label' => 'Criação em',
            'type' => 'datetime',
            'orderable' => true,
            'visibleInTable' => false,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
            'searchLogic' => function (Builder $query, $column, $searchTerm) {
                $formats = ['Y-m-d', 'd-m-Y', 'd/m/Y'];
                $parsedDate = null;
                foreach ($formats as $format) {
                    try {
                        $date = \Carbon\Carbon::createFromFormat($format, $searchTerm);
                        $parsedDate = $date->format('Y-m-d');
                        break;
                    } catch (\Exception $e) {
                        continue;
                    }
                }
                if ($parsedDate) {
                    $query->orWhereRaw("to_char(minutaempenhos.created_at, 'YYYY-MM-DD') = ?", [$parsedDate]);
                } else {
                    $query->orWhereRaw("minutaempenhos.created_at::text ilike ?", ["%{$searchTerm}%"]);
                }
            },
        ]);
    }

    public function adicionaColunaUpdatedAt(): void
    {
        $this->crud->addColumn([
            'box' => 'resumo',
            'name' => 'updated_at',
            'label' => 'Atualizado em',
            'type' => 'datetime',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
            'searchLogic' => $this->retornaSearchData('minutaempenhos', 'updated_at')
        ]);
    }

    public function adicionaColunaUnidadeCompra(): void
    {
        $this->crud->addColumn([
            'box' => 'resumo',
            'name' => 'unidade_compra',
            'label' => 'UASG Compra',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true
        ]);
    }

    public function adicionaColunaFornecedorEmpenho(): void
    {

        $this->crud->addColumn([
            'box' => 'resumo',
            'name' => 'fornec_cpf_cnpj',
            'label' => 'Credor', // Table column heading
            'orderable' => true,
            'limit' => 100,
            'visibleInTable' => false, // no point, since it's a large text
            'visibleInModal' => true, // would make the modal too big
            'visibleInExport' => true, // not important enough
            'visibleInShow' => true, // sure, why not
            'searchLogic' => function (Builder $query, $column, $searchTerm) {
                $query->orWhere('f.cpf_cnpj_idgener', 'ilike', "%$searchTerm%");
                $query->orWhere('f.nome', 'ilike', "%" . ($searchTerm) . "%");
            },
        ]);

    }

    public function adicionaColunaTipoEmpenhoPor()
    {
        $this->crud->addColumn([
            'box' => 'resumo',
            'name' => 'tipo_empenho_minuta_descricao',
            'label' => 'Tipo de Minuta', // Table column heading
            'orderable' => true,
            'visibleInTable' => true, // no point, since it's a large text
            'visibleInModal' => true, // would make the modal too big
            'visibleInExport' => true, // not important enough
            'visibleInShow' => true, // sure, why not
            'searchLogic' => function ($query, $column, $searchTerm) {
                $query->orWhere('codigoitens_tipo_empenho_minuta.descricao', 'ilike', '%' . $searchTerm . '%');
            },

        ]);
    }

    public function adicionaColunaTipoEmpenho()
    {
        $this->crud->addColumn([
            'box' => 'resumo',
            'name' => 'tipo_empenho_descricao',
            'label' => 'Tipo de Empenho', // Table column heading
            'orderable' => true,
            'visibleInTable' => false, // no point, since it's a large text
            'visibleInModal' => true, // would make the modal too big
            'visibleInExport' => true, // not important enough
            'visibleInShow' => true, // sure, why not
        ]);
    }

    public function adicionaColunaAmparoLegal()
    {
        $this->crud->addColumn([
            'box' => 'resumo',
            'name' => 'amparo_legal_descricao',
            'label' => 'Amparo Legal', // Table column heading
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => false, // no point, since it's a large text
            'visibleInModal' => true, // would make the modal too big
            'visibleInExport' => true, // not important enough
            'visibleInShow' => true, // sure, why not
        ]);

    }

    public function adicionaColunaModalidade()
    {
            $this->crud->addColumn([
                'box' => 'compra',
                'name' => 'modalidade_descricao',
                'label' => 'Modalidade', // Table column heading
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ]);
    }

    public function adicionaColunaValorTotal()
    {
        $this->crud->addColumn([
            'box' => 'resumo',
            'name' => 'valor_total',
            'label' => 'Valor Total', // Table column heading
            'type' => 'number',
            'prefix' => 'R$ ',
            'decimals' => 2,
            'orderable' => true,
            'visibleInTable' => false, // no point, since it's a large text
            'visibleInModal' => true, // would make the modal too big
            'visibleInExport' => true, // not important enough
            'visibleInShow' => true, // sure, why not
        ]);
    }

    public function adicionaColunaTipoCompra()
    {
            $this->crud->addColumn([
                'box' => 'compra',
                'name' => 'tipo_compra_descricao',
                'label' => 'Tipo da Compra', // Table column heading
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ]);
    }

    public function adicionaColunaNumeroAnoCompra()
    {
            $this->crud->addColumn([
                'box' => 'compra',
                'name' => 'compra_numero_ano',
                'label' => 'Número/Ano da Compra', // Table column heading
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
                'searchLogic' => function ($query, $column, $searchTerm) {
                    $query->orWhere('compras.numero_ano', 'ilike', '%' . $searchTerm . '%');
                },
            ]);
    }

    public function adicionaColunaIncisoCompra()
    {
            $this->crud->addColumn([
                'box' => 'compra',
                'name' => 'inciso_compra',
                'label' => 'Inciso', // Table column heading
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false, // no point, since it's a large text
                'visibleInModal' => false, // would make the modal too big
                'visibleInExport' => false, // not important enough
                'visibleInShow' => true, // sure, why not
            ]);
    }

    public function adicionaColunaLeiCompra()
    {
            $this->crud->addColumn([
                'box' => 'compra',
                'name' => 'lei_compra',
                'label' => 'Lei',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => false,
                'visibleInExport' => false,
                'visibleInShow' => true,
            ]);
    }

    public function adicionaColunaNumeroAnoContrato($minuta_id){

        $modMinuta = MinutaEmpenho::find($minuta_id);
        $codigoitem = Codigoitem::find($modMinuta->tipo_empenhopor_id);

        if($codigoitem->descricao === 'Contrato'){
            $this->crud->addColumn([
                'box' => 'compra',
                'name' => 'numero_contrato',
                'label' => 'Número/Ano do Contrato', // Table column heading
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false, // no point, since it's a large text
                'visibleInModal' => false, // would make the modal too big
                'visibleInExport' => false, // not important enough
                'visibleInShow' => true, // sure, why not,
                'limit' => '1000'
            ]);
        }else{
            $this->crud->addColumn([
                'box' => 'compra',
                'name' => 'numero_contrato_custom_html',
                'label' => 'Número/Ano do Contrato', // Table column heading
                'type' => 'custom_html',
                'orderable' => true,
                'visibleInTable' => false, // no point, since it's a large text
                'visibleInModal' => false, // would make the modal too big
                'visibleInExport' => false, // not important enough
                'visibleInShow' => true, // sure, why not
                'value' => '<span></span>'
            ]);
        }
    }

    public function adicionaColunaUnidadeBoxCompra($minuta_id): void
    {
        $modMinuta = MinutaEmpenho::find($minuta_id);
        $codigoitem = Codigoitem::find($modMinuta->tipo_empenhopor_id);

        if($codigoitem->descricao === 'Contrato'){
            $this->crud->addColumn([
                'box' => 'compra',
                'name' => 'unidade_gestoral_atual_contrato',
                'label' => 'Unidade Gestora Atual do Contrato',
                'type' => 'model_function',
                'function_name' => 'getUnidade',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true
            ]);
        }else{
            $this->crud->addColumn([
                'box' => 'compra',
                'name' => 'unidade_gestora_atual_custom_html',
                'label' => 'Unidade Gestora Atual do Contrato', // Table column heading
                'type' => 'custom_html',
                'orderable' => true,
                'visibleInTable' => false, // no point, since it's a large text
                'visibleInModal' => false, // would make the modal too big
                'visibleInExport' => false, // not important enough
                'visibleInShow' => true, // sure, why not
                'value' => '<span></span>'
            ]);
        }

    }

    public function adicionaBoxItens($minuta_id, $modMinuta)
    {
        $codigoitem = Codigoitem::find($modMinuta->tipo_empenhopor_id);
        $fornecedor_id = $modMinuta->fornecedor_empenho_id;
        $fornecedor_compra_id = $modMinuta->fornecedor_compra_id;

        if ($codigoitem->descricao === 'Contrato') {
            $itens = ContratoItemMinutaEmpenho::join(
                'contratoitens',
                'contratoitens.id',
                '=',
                'contrato_item_minuta_empenho.contrato_item_id'
            )
                ->join('minutaempenhos', 'minutaempenhos.id', '=', 'contrato_item_minuta_empenho.minutaempenho_id')
                ->join('contratos', 'contratos.id', '=', 'minutaempenhos.contrato_id')
                ->join('codigoitens', 'codigoitens.id', '=', 'contratoitens.tipo_id')
                ->join('catmatseritens', 'catmatseritens.id', '=', 'contratoitens.catmatseritem_id')
                ->join('fornecedores', 'fornecedores.id', '=', 'contratos.fornecedor_id')
                ->join(
                    'minutaempenhos_remessa',
                    'minutaempenhos_remessa.id',
                    '=',
                    'contrato_item_minuta_empenho.minutaempenhos_remessa_id'
                )
                ->where('contrato_item_minuta_empenho.minutaempenho_id', $minuta_id)
                ->where('minutaempenhos_remessa.remessa', 0)
                ->distinct()
                ->select([
                    DB::raw('fornecedores.cpf_cnpj_idgener AS "CPF/CNPJ/IDGENER do Fornecedor"'),
                    DB::raw('fornecedores.nome AS "Fornecedor"'),
                    DB::raw('codigoitens.descricao AS "Tipo do Item"'),
                    DB::raw('catmatseritens.codigo_siasg AS "Código do Item"'),
                    DB::raw('contratoitens.numero_item_compra AS "Número do Item"'),
                    DB::raw('catmatseritens.descricao AS "Descrição"'),
                    DB::raw("CASE
                                        WHEN contratoitens.descricao_complementar != 'undefined'
                                            THEN contratoitens.descricao_complementar
                                        ELSE ''
                                    END  AS \"Descrição Detalhada\""),
                    DB::raw('contrato_item_minuta_empenho.quantidade AS "Quantidade"'),
                    DB::raw('contrato_item_minuta_empenho.valor AS "Valor Total do Item"'),
                    'contrato_item_minuta_empenho.numseq'
                ])
                ->orderBy('contrato_item_minuta_empenho.numseq', 'asc')
                ->get()->toArray();
        }

        if ($codigoitem->descricao === 'Compra' || $codigoitem->descricao === 'Suprimento') {
            $this->crud->removeColumn('contrato_id');
            $itens = CompraItemMinutaEmpenho::join(
                'compra_items',
                'compra_items.id',
                '=',
                'compra_item_minuta_empenho.compra_item_id'
            );

                if ($modMinuta->exercicio_atual) {
                    $itens = $itens->join(
                        'compra_item_fornecedor',
                        function($join) {
                            $join->on('compra_item_fornecedor.compra_item_id', '=', 'compra_items.id')
                                ->on('compra_item_minuta_empenho.compra_item_fornecedor_id', '=', 'compra_item_fornecedor.id');
                        }
                    )
                        ->join(
                            'compra_item_unidade',
                            function($join) {
                                $join->on('compra_item_unidade.compra_item_id', '=', 'compra_items.id')
                                    ->on('compra_item_minuta_empenho.compra_item_unidade_id', '=', 'compra_item_unidade.id');
                            }
                        );

                } else {

                    $itens = $itens
                        ->join(
                            'compra_item_fornecedor',
                            'compra_item_fornecedor.compra_item_id',
                            '=',
                            'compra_items.id'
                        )
                        ->join('compra_item_unidade', 'compra_item_unidade.compra_item_id', '=', 'compra_items.id')
                        ->where('compra_items.situacao', '=', true)
                        ->where('compra_item_fornecedor.situacao', '=', true)
                        ->where('compra_item_unidade.situacao', '=', true);
                }

            $itens = $itens->join('naturezasubitem', 'naturezasubitem.id', '=', 'compra_item_minuta_empenho.subelemento_id')
                ->join('codigoitens', 'codigoitens.id', '=', 'compra_items.tipo_item_id')
                ->join('catmatseritens', 'catmatseritens.id', '=', 'compra_items.catmatseritem_id')

                ->join('fornecedores', 'fornecedores.id', '=', 'compra_item_fornecedor.fornecedor_id')
                ->join(
                    'minutaempenhos_remessa',
                    'minutaempenhos_remessa.id',
                    '=',
                    'compra_item_minuta_empenho.minutaempenhos_remessa_id'
                )
                ->join(
                    'compras',
                    'compras.id',
                    '=',
                    'compra_items.compra_id'
                )
                ->join('codigoitens as mod', 'mod.id', '=', 'compras.modalidade_id')
                ->where('compra_item_minuta_empenho.minutaempenho_id', $minuta_id)
                ->where('compra_item_unidade.unidade_id', $modMinuta->unidade_id)
                ->where('minutaempenhos_remessa.remessa', 0)
                ->distinct();

            $itens = $itens->select([
                    DB::raw('fornecedores.cpf_cnpj_idgener AS "CPF/CNPJ/IDGENER do Fornecedor"'),
                    DB::raw('fornecedores.nome AS "Fornecedor"'),
                    DB::raw('codigoitens.descricao AS "Tipo do Item"'),
                    DB::raw('catmatseritens.codigo_siasg AS "Código do Item"'),
                    DB::raw('compra_items.codigo_ncmnbs AS "Código NCM/NBS"'),
                    DB::raw('compra_items.numero AS "Número do Item"'),
                    DB::raw('catmatseritens.descricao AS "Descrição"'),
                    DB::raw('compra_items.descricaodetalhada AS "Descrição Detalhada"'),
                    DB::raw('naturezasubitem.codigo || \' - \' || naturezasubitem.descricao AS "ND Detalhada"'),
                    DB::raw("CASE
                            WHEN (
                                compra_items.criterio_julgamento = 'V'
                                OR compra_items.criterio_julgamento is null
                                OR mod.descres = '99'
                                OR mod.descres = '06'
                            ) THEN compra_item_fornecedor.valor_unitario
                            ELSE compra_item_fornecedor.valor_unitario *
                                 ((100 - compra_item_fornecedor.percentual_maior_desconto) / 100)
                            END                                           AS \"Valor unitário\""),
                    DB::raw('compra_item_minuta_empenho.quantidade AS "Quantidade"'),
                    DB::raw('compra_item_minuta_empenho.valor AS "Valor Total do Item"'),
                    'compra_item_minuta_empenho.numseq'
                ])
                ->orderBy('compra_item_minuta_empenho.numseq', 'asc');
            $itens = $this->setCondicaoFornecedor(
                $modMinuta,
                $itens,
                $codigoitem->descricao,
                $fornecedor_id,
                $fornecedor_compra_id
            );

            $itens = $itens->get()->toArray();

            $itens = array_map(function ($itens) {
                if ($itens['Valor unitário']) {
                    $itens['Valor unitário'] = number_format($itens['Valor unitário'], 4, '.', '');
                }
                return $itens;
            }, $itens);
        }

        $this->crud->addColumn([
            'box' => 'itens',
            'name' => 'itens',
            'label' => 'itens', // Table column heading
            'orderable' => true,
            'visibleInTable' => false, // no point, since it's a large text
            'visibleInModal' => false, // would make the modal too big
            'visibleInExport' => false, // not important enough
            'visibleInShow' => true, // sure, why not
            'values' => $itens
        ]);
    }

    public function adicionaBoxSaldo($minuta_id)
    {

        $saldo = SaldoContabil::join('minutaempenhos', 'minutaempenhos.saldo_contabil_id', '=', 'saldo_contabil.id')
            ->select([
                DB::raw('SUBSTRING(saldo_contabil.conta_corrente,1,1) AS "Esfera"'),
                DB::raw('SUBSTRING(saldo_contabil.conta_corrente,2,6) AS "PTRS"'),
                DB::raw('SUBSTRING(saldo_contabil.conta_corrente,8,10) AS "Fonte"'),
                DB::raw('SUBSTRING(saldo_contabil.conta_corrente,18,6) AS "ND"'),
                DB::raw('SUBSTRING(saldo_contabil.conta_corrente,24,8) AS "UGR"'),
                DB::raw('SUBSTRING(saldo_contabil.conta_corrente,32,11) AS "Plano Interno"'),
                DB::raw('TO_CHAR(saldo_contabil.saldo,\'999G999G000D99\') AS "Crédito orçamentário"'),
                DB::raw('TO_CHAR(minutaempenhos.valor_total,\'999G999G000D99\') AS "Utilizado"'),
                DB::raw('TO_CHAR(saldo_contabil.saldo - minutaempenhos.valor_total, \'999G999G000D99\')  AS "Saldo"'),

            ])
            ->where('minutaempenhos.id', $minuta_id)
            ->get()
            ->toArray();

        $this->crud->addColumn([
            'box' => 'saldo',
            'name' => 'saldo',
            'label' => 'saldo', // Table column heading
            'orderable' => true,
            'visibleInTable' => false, // no point, since it's a large text
            'visibleInModal' => false, // would make the modal too big
            'visibleInExport' => false, // not important enough
            'visibleInShow' => true, // sure, why not
            'values' => $saldo
        ]);
    }

    public function adicionaColunaNumeroEmpenho()
    {
    }

    public function adicionaColunaCipi()
    {
    }

    public function adicionaColunaDataEmissao()
    {
    }


    public function adicionaColunaProcesso()
    {
    }


    public function adicionaColunaTaxaCambio()
    {
    }

    public function adicionaColunaLocalEntrega()
    {
    }

    public function adicionaColunaDescricao()
    {
    }

    public function adicionaColunaLinkMinutaAnulada($modMinuta): void
    {
        $minuta_anulada_id = $modMinuta->minuta_anulada_id;
        $emp = substr($modMinuta->informacao_complementar,
            strrpos($modMinuta->informacao_complementar, ':') + 2);
        $this->crud->addColumn([
            'name' => 'minuta_anulada',
            'label' => 'Minuta Anulada',
            'type' => 'closure',
            'function' => function ($entry) use ($minuta_anulada_id, $emp) {
                return '<a href="' .
                    route(
                        'empenho.crud./minuta.show',
                        ['minutum' => $minuta_anulada_id]
                    )
                    . '"> ' . $emp . ' </a>';
            }
        ]);
    }

    protected function aplicaFiltros()
    {
        $this->aplicaFiltroSituacao();
        $this->aplicaFiltroModalidade();
    }

    protected function aplicaFiltroSituacao()
    {
        $arrayOptionsSituacao = [
                217 => 'EMPENHO EMITIDO',
                215 => 'EM PROCESSAMENTO',
                214 => 'EM ANDAMENTO',
                218 => 'EMPENHO CANCELADO',
                216 => 'ERRO',
            ];

        #exibe filtro de situacao apenas para minutas do ano atual pois minutas de anos anteriores sempre terão
        #a situacao = EMPENHO EMITIDO
        if (now()->year === (int) $this->anoMinuta) {
            $this->crud->addFilter([
                'name' => 'getSituacao',
                'label' => 'Situação',
                'type' => 'select2_multiple',

            ], $arrayOptionsSituacao, function ($value) {
                $this->addClauseAnoMinuta($this->anoMinuta);
            });
        }
    }

    protected function aplicaFiltroModalidade()
    {
        $this->crud->addFilter([
            'name' => 'compra_modalidade',
            'label' => 'Modalidade',
            'type' => 'select2_multiple',

        ], [
            76 => '05 - Pregão',
            73 => '01 - Convite',
            77 => '02 - Tomada de Preços',
            75 => '07 - Inexigibilidade',
            72 => '20 - Concurso',
            74 => '06 - Dispensa',
            71 => '03 - Concorrência',
            184 => '22 - Tomada de Preços por Técnica e Preço',
            185 => '33 - Concorrência por Técnica e Preço',
            186 => '44 - Concorrência Internacional por Técnica e Preço',
            187 => '04 - Concorrência Internacional',
            160 => '99 - Regime Diferenciado de Contratação',

        ], function ($value) {
            $this->addClauseAnoMinuta($this->anoMinuta);
        });
    }

    public function retonaFormModal()
    {
        return FormBuilder::create(InserirFornecedorForm::class, [
            'id' => 'form_modal'
        ]);
    }

    public function inserirFornecedorModal(Request $request)
    {

        DB::beginTransaction();
        try {
            $fornecedor = Fornecedor::firstOrCreate(
                ['cpf_cnpj_idgener' => $request->cpf_cnpj_idgener],
                [
                    'tipo_fornecedor' => $request->fornecedor,
                    'nome' => $request->nome
                ]
            );
            DB::commit();
        } catch (Exception $exc) {
            DB::rollback();
        }
        return $fornecedor;
    }

    public function executarAtualizacaoSituacaoMinuta($id)
    {
        $minuta = MinutaEmpenho::find($id);
        $date_time = \DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s'));

        if (!isset($minuta)) {
            Alert::warning('Minuta não encontrada!')->flash();
            return redirect(URL::previous());
        }

        if ($minuta->situacao->descricao == 'ERRO') {
            DB::beginTransaction();
            try {
                $situacao = Codigoitem::wherehas('codigo', function ($q) {
                    $q->where('descricao', '=', 'Situações Minuta Empenho');
                })
                    ->where('descricao', 'EM PROCESSAMENTO')
                    ->first();
                $minuta->situacao_id = $situacao->id;
                $minuta->save();

                $modSfOrcEmpenhoDados = SfOrcEmpenhoDados::where('minutaempenho_id', $id)
                    ->where('alteracao', false)
                    ->latest()
                    ->first();

                $remessa = MinutaEmpenhoRemessa::find($modSfOrcEmpenhoDados->minutaempenhos_remessa_id);
                if (!$remessa->sfnonce) {
                    $base = new Base();
                    $remessa->sfnonce = $base->geraNonceSiafiEmpenho($remessa->minutaempenho_id, $remessa->id);
                    $remessa->save();
                }

                if ($modSfOrcEmpenhoDados->sfnonce != $remessa->sfnonce) {
                    $modSfOrcEmpenhoDados->sfnonce = $remessa->sfnonce;
                }
                $modSfOrcEmpenhoDados->situacao = 'EM PROCESSAMENTO';
                $modSfOrcEmpenhoDados->save();

                DB::commit();
                Alert::success('Situação da minuta alterada com sucesso!')->flash();
                return redirect(URL::previous());
            } catch (Exception $exc) {
                DB::rollback();
            }
        }

        if ($minuta->situacao->descricao == 'EM PROCESSAMENTO') {
            $modSfOrcEmpenhoDados = SfOrcEmpenhoDados::where('minutaempenho_id', $id)
                ->where('alteracao', false)
                ->latest()
                ->first();

            $updated_at = \DateTime::createFromFormat('Y-m-d H:i:s', $modSfOrcEmpenhoDados->updated_at)->modify('+15 minutes');

            if ($date_time < $updated_at) {
                Alert::warning('Situação da minuta não pode ser alterada, tente novamente em 15 minutos!')->flash();
                return redirect(URL::previous());
            }

            $remessa = MinutaEmpenhoRemessa::find($modSfOrcEmpenhoDados->minutaempenhos_remessa_id);

            if (!$remessa->sfnonce) {
                if (!$modSfOrcEmpenhoDados->sfnonce_id) {
                    Alert::warning('Minuta com problema! Por favor crie uma nova minuta!')->flash();
                    return redirect(URL::previous());
                }
                $modSfOrcEmpenhoDados->sfnonce = $modSfOrcEmpenhoDados->sfnonce_id;
            }

            $modSfOrcEmpenhoDados->txtdescricao .= ' ';
            $modSfOrcEmpenhoDados->situacao = 'EM PROCESSAMENTO';
            $modSfOrcEmpenhoDados->save();

            Alert::success('Minuta será processada novamente, por favor aguarde!')->flash();
            return redirect(URL::previous());
        }

        Alert::warning('Situação da minuta não pode ser alterada!')->flash();
        return redirect(URL::previous());
    }

    public function deletarMinuta($id)
    {
        $minuta = MinutaEmpenho::find($id);

        if (($minuta->situacao->descricao === 'ERRO' || $minuta->situacao->descricao === 'EM ANDAMENTO')
            && $minuta->remessa_alteracao_fonte === null
        ) {
            DB::beginTransaction();
            try {
                $msgRetorno = $this->apagarMinuta($minuta);
                DB::commit();

                Alert::success($msgRetorno)->flash();
                return redirect(URL::previous());
            } catch (Exception $exc) {
                DB::rollback();
                Alert::error('Erro! Tente novamente mais tarde!')->flash();
                return redirect(URL::previous());
            }
        }

        Alert::warning('Operação não permitida!')->flash();
        return redirect(URL::previous());
    }

    public function pdfEmpenho($id)
    {
        try {
            $idTipoArquivoMinutaEmpenho =  Codigoitem::where([
                'descres' => 'ARQ_MINUTA_EMPENHO',
                'descricao' => 'Arquivo minuta de empenho'
            ])->first()->id;
            $minuta = MinutaEmpenho::find($id);

            $arquivoDownload = $this->gerarPdfNovo($minuta, $idTipoArquivoMinutaEmpenho);

            if ($arquivoDownload->sucesso) {
                return Storage::download($arquivoDownload->pathFileEmpenho);
            }

            //caso o usuario logado não tenha senha siafi
            if (
                isset($arquivoDownload->mensagem)
                && strpos($arquivoDownload->mensagem, 'USUARIO INEXISTENTE') !== false
            ) {

                //tente novamente com o cpf padrão
                $arquivoDownload = $this->gerarPdfNovo($minuta, $idTipoArquivoMinutaEmpenho, true);

                if ($arquivoDownload->sucesso) {
                    return Storage::download($arquivoDownload->pathFileEmpenho);
                }
            }

            Alert::error('Erro! '.$arquivoDownload->mensagem)->flash();

        } catch (Exception $ex) {
            $this->inserirLogCustomizado('minuta-empenho', 'error', $ex);
        }

        return redirect(URL::previous());
    }

    public function toogleForcaContrato($id)
    {

        if (!backpack_user()->hasRole('Administrador') &&
            !backpack_user()->hasRole('Administrador Órgão') &&
            !backpack_user()->hasRole('Execução Financeira') &&
            !backpack_user()->hasRole('Setor Contratos')
        ) {
            Alert::error('Você não tem permissão para executar esta funcionalidade!')->flash();
            return redirect(URL::previous());
        }

        $minutaempenho = MinutaEmpenho::find($id);

        if ($minutaempenho->getEmpenhoPorAttribute() != "Compra") {
            Alert::error('Apenas Tipo de Minutas = Compra pode ter força de contrato!')->flash();
            return redirect(URL::previous());
        }

        $arquivoPdfMinutaEmpenho = null;

        DB::beginTransaction();
        try {
            $minutaempenho->minutaempenho_forcacontrato = $minutaempenho->minutaempenho_forcacontrato == 0 ? 1 : 0;
            $idTipoArquivoMinutaEmpenho =  Codigoitem::where([
                'descres' => 'ARQ_MINUTA_EMPENHO',
                'descricao' => 'Arquivo minuta de empenho'
            ])->first()->id;

            # Salvar o arquivo do empenho se estiver assinado
            $arquivoPdfMinutaEmpenho = $this->gerarPdfNovo($minutaempenho, $idTipoArquivoMinutaEmpenho);

            $mensagem_sucesso =
                $minutaempenho->minutaempenho_forcacontrato == 1 ?
                    'Empenho definido como substitutivo de contrato' : 'Empenho não é mais substitutivo de contrato';

            $minutaempenho->save();

            DB::commit();

            # Se apresentar alerta em relação ao buscar o empenho no SIAFI
            if (!empty($arquivoPdfMinutaEmpenho) && !$arquivoPdfMinutaEmpenho->sucesso) {
                $mensagem_sucesso .= "<br> {$arquivoPdfMinutaEmpenho->mensagem}";
                Alert::warning($mensagem_sucesso)->flash();
                return redirect(URL::previous());
            }

            Alert::success($mensagem_sucesso)->flash();
            return redirect(URL::previous());
        } catch (Exception $exc) {
            DB::rollback();
            $mensagemErro = "Erro ao alterar o toogle da minuta de empenho para força de contrato {$exc}";
            Log::error($mensagemErro);
            Alert::error('Erro! Tente novamente mais tarde!')->flash();
            return redirect(URL::previous());
        }

        return redirect(URL::previous());
    }

    public function geraArquivoEmpenhoPNCP()
    {
        $arquivo_pendente = EnviaDadosPncp::whereHas('status', function ($ci) {
            $ci->where('descres', 'ASNPEN');
        })->where([
            ['pncpable_type', MinutaEmpenho::class],
            ['linkArquivoEmpenho', null]
        ])
            ->get();
        foreach ($arquivo_pendente as $empenho) {
            try {
                $minuta = MinutaEmpenho::find($empenho->pncpable_id);

                if ($minuta->situacao->descricao == 'EMPENHO EMITIDO' && !file_exists($minuta->serializaArquivoPNCP())) {
                    $this->pdfEmpenho($minuta->id);
                }

            } catch (Exception $e) {
                Log::error($e->getMessage());
            } finally {

            }
        }
    }

    private function preparaEnvioPNCP($minutaId)
    {
        $pncp = EnviaDadosPncp::where('pncpable_id', $minutaId)->where('pncpable_type', MinutaEmpenho::class)->first();

        if (isset($pncp)) {
            //ARQPEN so entra quando o status é sucesso
            if (
                ($pncp->status->descres == 'ASNPEN' || $pncp->status->descres == 'ASNPEN_PNCP') &&
                empty($pncp->linkArquivoEmpenho)) {
                $pncp->situacao = Codigoitem::whereHas('codigo', function ($q) {
                    $q->where('descricao', 'Situação Envia Dados PNCP');
                })->where('descres', 'INCPEN')->first()->id;
                $pncp->save();
            }
        }
    }

    private function addClauseAnoMinuta(string $anoMinuta)
    {
        $anoMinuta = (int) $anoMinuta;
        $situacao_id = $this->retornaIdCodigoItem('Situações Minuta Empenho', 'EMPENHO EMITIDO');

        #pega querystring do filtro de modalidade
        $arrModalidadeFiltro = json_decode(request()->get('compra_modalidade'));

        if ($arrModalidadeFiltro !== null) {
            $this->addClauseFiltroModalidade($arrModalidadeFiltro);
        }

        #anos anteriores
        if ($anoMinuta !== now()->year) {
            $this->crud->addClause('where', 'minutaempenhos.situacao_id', '=', $situacao_id);
            $this->crud->addClause(
                'whereRaw',
                'left (minutaempenhos.mensagem_siafi, 4) < date_part(\'year\', current_date)::text'
            );

            return;
        }

        #pega querystring do filtro de situacao
        $arrSituacao = json_decode(request()->get('getSituacao'));

        if ($arrSituacao !== null) {
            #adiciona filtro de situacao
            $this->addClauseFiltroSituacao($situacao_id, $arrSituacao);
        }

        #caso seja ano atual
        $this->crud->addClause(
            'whereRaw',
            DB::raw(
                '(
                        "minutaempenhos"."mensagem_siafi" is null
                        or
                        "minutaempenhos"."situacao_id" <> 217
                        or ("minutaempenhos"."situacao_id" = 217 and
                            left(minutaempenhos.mensagem_siafi, 4) = date_part(\'year\', current_date)::text)
                        )'
            )
        );
    }

    private function addClauseFiltroSituacao(int $situacao_id, array $arrSituacao)
    {
        #caso situacao do filtro tenha a situacao EMPENHO EMITIDO
        $whereEmpenhoEmitido = in_array($situacao_id, $arrSituacao) ?
            "minutaempenhos.situacao_id = $situacao_id and
                    left(minutaempenhos.mensagem_siafi, 4) =  date_part('year', current_date)::text " :
            '';

        #remove o id da situacao EMPENHO EMITIDO pois a condicao para trazer essa situacao esta separada
        $arrSituacao = array_diff($arrSituacao, [$situacao_id]);

        #prefix or
        $prefixOr = $whereEmpenhoEmitido !== '' ? 'or' : '';

        #condicao para trazer as situações
        $orClause = count($arrSituacao) > 0 ?
            $prefixOr . ' minutaempenhos.situacao_id IN (' . implode($arrSituacao, ',') . ')' :
            '';

        $this->crud->addClause(
            'whereRaw',
            DB::raw(
                $whereEmpenhoEmitido . $orClause
            )
        );
    }

    private function addClauseFiltroModalidade(array $arrModalidadeFiltro)
    {
        $this->crud->addClause(
            'whereIn',
            'compras.modalidade_id',
            $arrModalidadeFiltro
        );
    }
}
