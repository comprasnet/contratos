<?php

namespace App\Http\Controllers\Empenho;

use Alert;
use App\Http\Controllers\Empenho\Minuta\BaseControllerEmpenho;
use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\CompraTrait;
use App\Http\Traits\Formatador;
use App\Http\Traits\PermissaoTrait;
use App\Models\CompraItemFornecedor;
use App\Models\CompraItem;
use App\Models\CompraItemMinutaEmpenho;
use App\Models\ContratoItemMinutaEmpenho;
use App\Models\CompraItemUnidade;
use App\Models\Codigoitem;
use App\Models\MinutaEmpenho;
use App\Models\Naturezasubitem;
use App\Models\Fornecedor;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Route;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;

class SubelementoController extends BaseControllerEmpenho
{
    use Formatador;
    use CompraTrait;
    use BuscaCodigoItens;
    use PermissaoTrait;

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($this->verificaPermissaoConsulta('minuta') === false){
            return redirect()->to('/empenho/minuta/ano/'.date('Y'));
        }
        $minuta_id = Route::current()->parameter('minuta_id');
        $modMinutaEmpenho = MinutaEmpenho::find($minuta_id);
        $fornecedor_id = $modMinutaEmpenho->fornecedor_compra_id;

        $codigoitem = Codigoitem::find($modMinutaEmpenho->tipo_empenhopor_id);
        if ($codigoitem->descricao === 'Contrato') {
            $tipo = 'contrato_item_id';
            $itens = MinutaEmpenho::join(
                'contrato_item_minuta_empenho',
                'contrato_item_minuta_empenho.minutaempenho_id',
                '=',
                'minutaempenhos.id'
            )
                ->join(
                    'contratoitens',
                    'contratoitens.id',
                    '=',
                    'contrato_item_minuta_empenho.contrato_item_id'
                )
                ->join(
                    'compras',
                    'compras.id',
                    '=',
                    'minutaempenhos.compra_id'
                )
                ->join(
                    'codigoitens as tipo_compra',
                    'tipo_compra.id',
                    '=',
                    'compras.tipo_compra_id'
                )
                ->join(
                    'codigoitens',
                    'codigoitens.id',
                    '=',
                    'contratoitens.tipo_id'
                )
                ->join(
                    'saldo_contabil',
                    'saldo_contabil.id',
                    '=',
                    'minutaempenhos.saldo_contabil_id'
                )
                ->join(
                    'naturezadespesa',
                    'naturezadespesa.codigo',
                    '=',
                    DB::raw("SUBSTRING(saldo_contabil.conta_corrente,18,6)")
                )
                ->join(
                    'catmatseritens',
                    'catmatseritens.id',
                    '=',
                    'contratoitens.catmatseritem_id'
                )
                ->where('minutaempenhos.id', $minuta_id)
                ->where('minutaempenhos.unidade_id', session('user_ug_id'))
                ->where('naturezadespesa.sistema_origem', '=', 'SIAFI')
                ->select(
                    [
                        'contrato_item_minuta_empenho.contrato_item_id',
                        'tipo_compra.descricao as tipo_compra_descricao',
                        'codigoitens.descricao',
                        'catmatseritens.codigo_siasg',
                        'contratoitens.numero_item_compra as numero_item',
                        'catmatseritens.descricao as catmatser_desc',
                        DB::raw("SUBSTRING(catmatseritens.descricao for 50) AS catmatser_desc_simplificado"),
                        'contratoitens.descricao_complementar as descricaodetalhada',
                        DB::raw("SUBSTRING(contratoitens.descricao_complementar for 50) AS descricaosimplificada"),
                        'contratoitens.quantidade as qtd_item',
                        'contratoitens.valorunitario as valorunitario',
                        'naturezadespesa.codigo as natureza_despesa',
                        'naturezadespesa.id as natureza_despesa_id',
                        'contratoitens.valortotal',
                        'saldo_contabil.saldo',
                        'contrato_item_minuta_empenho.subelemento_id',
                        'contrato_item_minuta_empenho.quantidade',
                        'contrato_item_minuta_empenho.valor',
                        DB::raw("SUBSTRING(saldo_contabil.conta_corrente,18,6) AS natureza_despesa"),
                        DB::raw('contrato_item_minuta_empenho.id AS "numeroLinha"')
                    ]
                )
                ->distinct()
                ->get()
                ->toArray();

            $valor_utilizado = ContratoItemMinutaEmpenho::where('contrato_item_minuta_empenho.minutaempenho_id', $minuta_id)
                ->select(DB::raw('coalesce(sum(valor),0) as sum'))
                ->first()->toArray();
        }

        if ($codigoitem->descricao === 'Compra') {
            $tipo = 'compra_item_id';
            $itens = MinutaEmpenho::join(
                'compra_item_minuta_empenho',
                'compra_item_minuta_empenho.minutaempenho_id',
                '=',
                'minutaempenhos.id'
            )
                ->join(
                    'compra_items',
                    'compra_items.id',
                    '=',
                    'compra_item_minuta_empenho.compra_item_id'
                )
                ->join(
                    'compras',
                    'compras.id',
                    '=',
                    'compra_items.compra_id'
                )
                ->join(
                    'codigoitens as tipo_compra',
                    'tipo_compra.id',
                    '=',
                    'compras.tipo_compra_id'
                )
                ->join(
                    'codigoitens',
                    'codigoitens.id',
                    '=',
                    'compra_items.tipo_item_id'
                )
                ->join(
                    'saldo_contabil',
                    'saldo_contabil.id',
                    '=',
                    'minutaempenhos.saldo_contabil_id'
                )
                ->join(
                    'naturezadespesa',
                    'naturezadespesa.codigo',
                    '=',
                    DB::raw("SUBSTRING(saldo_contabil.conta_corrente,18,6)")
                )
                ->join(
                    'compra_item_fornecedor',
                    'compra_item_fornecedor.compra_item_id',
                    '=',
                    'compra_items.id'
                )
                ->join(
                    'catmatseritens',
                    'catmatseritens.id',
                    '=',
                    'compra_items.catmatseritem_id'
                )
                ->join(
                    'compra_item_unidade',
                    'compra_item_unidade.compra_item_id',
                    '=',
                    'compra_items.id'
                )
                ->join('codigoitens as mod', 'mod.id', '=', 'compras.modalidade_id')
                ->where('minutaempenhos.id', $minuta_id)
                ->where('compra_item_unidade.unidade_id', session('user_ug_id'))
                ->where('compra_items.situacao', '=', true)
                ->where('compra_item_fornecedor.situacao', '=', true)
                ->where('compra_item_unidade.situacao', '=', true)
                ->where('naturezadespesa.sistema_origem', '=', 'SIAFI')
                ->whereRaw("
                    CASE WHEN compra_item_unidade.tipo_uasg = 'C'
                            or tipo_compra.descricao = 'SISPP'
                        THEN compra_item_unidade.fornecedor_id = '$fornecedor_id'
                        ELSE compra_item_unidade.fornecedor_id IS NULL
                    END")
                ->select(
                    [
                        'compra_item_minuta_empenho.compra_item_id',
                        'compra_item_fornecedor.fornecedor_id',
                        'tipo_compra.descricao as tipo_compra_descricao',
                        'codigoitens.descricao',
                        'catmatseritens.codigo_siasg',
                        'compra_items.numero as numero_item',
                        'catmatseritens.descricao as catmatser_desc',
                        DB::raw("SUBSTRING(catmatseritens.descricao for 50) AS catmatser_desc_simplificado"),
                        'compra_items.descricaodetalhada',
                        DB::raw("SUBSTRING(compra_items.descricaodetalhada for 50) AS descricaosimplificada"),
                        'compra_item_unidade.quantidade_saldo',
                        DB::raw("CASE
                                    WHEN (tipo_compra.descricao = 'SISPP' OR compra_item_unidade.tipo_uasg = 'C') THEN
                                        CASE
                                            WHEN compra_item_unidade.quantidade_saldo < 0 THEN 0
                                            ELSE compra_item_unidade.quantidade_saldo + compra_item_minuta_empenho.quantidade END
                                    ELSE
                                        CASE
                                            WHEN (compra_item_unidade.quantidade_saldo < 0 ) THEN 0
                                            ELSE compra_item_unidade.quantidade_saldo + compra_item_minuta_empenho.quantidade
                                        END
                                 END AS qtd_item"),
                        DB::raw("CASE
                            WHEN (
                                compra_items.criterio_julgamento = 'V'
                                OR compra_items.criterio_julgamento is null
                                OR mod.descres = '99'
                                OR mod.descres = '06'
                            ) THEN compra_item_fornecedor.valor_unitario
                            ELSE compra_item_fornecedor.valor_unitario *
                                 ((100 - compra_item_fornecedor.percentual_maior_desconto) / 100)
                            END                                           AS valorunitario"),
                        'naturezadespesa.codigo as natureza_despesa',
                        'naturezadespesa.id as natureza_despesa_id',
                        DB::raw("CASE
                            WHEN (
                                compra_items.criterio_julgamento = 'V'
                                OR compra_items.criterio_julgamento is null
                                OR mod.descres = '99'
                                OR mod.descres = '06'
                            ) THEN compra_item_fornecedor.valor_negociado
                            ELSE compra_item_fornecedor.valor_negociado *
                                 ((100 - compra_item_fornecedor.percentual_maior_desconto) / 100)
                            END                                           AS valortotal"),
                        'saldo_contabil.saldo',
                        'compra_item_minuta_empenho.subelemento_id',
                        'compra_item_minuta_empenho.quantidade',
                        'compra_item_minuta_empenho.valor',
                        DB::raw("SUBSTRING(saldo_contabil.conta_corrente,18,6) AS natureza_despesa"),
                        DB::raw('compra_item_minuta_empenho.id AS "numeroLinha"')
                    ]
                )
                ->distinct();

            $itens = $this->setCondicaoFornecedor(
                $modMinutaEmpenho,
                $itens,
                $codigoitem->descricao,
                $modMinutaEmpenho->fornecedor_empenho_id,
                $fornecedor_id
            );

            $itens = $itens->get()
                ->toArray();

            $itens = array_map(static function ($itens) {
                $itens['valorunitario'] = round((float)$itens['valorunitario'], 4);
                $itens['valorunitario'] =
                    number_format($itens['valorunitario'], 4, '.', '');

                $itens['valortotal']    = round((float)$itens['valortotal'], 4);
                $itens['valortotal'] =
                    number_format($itens['valortotal'], 4, '.', '');
                return $itens;
            }, $itens);

            $valor_utilizado = CompraItemMinutaEmpenho::where('compra_item_minuta_empenho.minutaempenho_id', $minuta_id)
                ->select(DB::raw('coalesce(sum(valor),0) as sum'))
                ->first()->toArray();
        }

        if ($codigoitem->descricao === 'Suprimento') {
            $tipo = 'compra_item_id';

            $itens = MinutaEmpenho::join(
                'compra_item_minuta_empenho',
                'compra_item_minuta_empenho.minutaempenho_id',
                '=',
                'minutaempenhos.id'
            )
                ->join(
                    'compra_items',
                    'compra_items.id',
                    '=',
                    'compra_item_minuta_empenho.compra_item_id'
                )
                ->join(
                    'compras',
                    'compras.id',
                    '=',
                    'compra_items.compra_id'
                )
                ->join(
                    'codigoitens as tipo_compra',
                    'tipo_compra.id',
                    '=',
                    'compras.tipo_compra_id'
                )
                ->join(
                    'codigoitens',
                    'codigoitens.id',
                    '=',
                    'compra_items.tipo_item_id'
                )
                ->join(
                    'saldo_contabil',
                    'saldo_contabil.id',
                    '=',
                    'minutaempenhos.saldo_contabil_id'
                )
                ->join(
                    'naturezadespesa',
                    'naturezadespesa.codigo',
                    '=',
                    DB::raw("SUBSTRING(saldo_contabil.conta_corrente,18,6)")
                )
                ->join(
                    'compra_item_fornecedor',
                    'compra_item_fornecedor.compra_item_id',
                    '=',
                    'compra_items.id'
                )
                ->join(
                    'catmatseritens',
                    'catmatseritens.id',
                    '=',
                    'compra_items.catmatseritem_id'
                )
                ->join(
                    'compra_item_unidade',
                    'compra_item_unidade.compra_item_id',
                    '=',
                    'compra_items.id'
                )
                ->where('minutaempenhos.id', $minuta_id)
                ->where('compra_item_fornecedor.fornecedor_id', $modMinutaEmpenho->fornecedor_empenho_id)
                ->where('compra_item_unidade.unidade_id', session('user_ug_id'))
                ->where('naturezadespesa.sistema_origem', '=', 'SIAFI')
                ->select(
                    [
                        'compra_item_minuta_empenho.compra_item_id',
                        'compra_item_fornecedor.fornecedor_id',
                        'tipo_compra.descricao as tipo_compra_descricao',
                        'codigoitens.descricao',
                        'catmatseritens.codigo_siasg',
                        'compra_items.numero as numero_item',
                        'catmatseritens.descricao as catmatser_desc',
                        DB::raw("SUBSTRING(catmatseritens.descricao for 50) AS catmatser_desc_simplificado"),
                        'compra_items.descricaodetalhada',
                        DB::raw("SUBSTRING(compra_items.descricaodetalhada for 50) AS descricaosimplificada"),
                        'compra_item_unidade.quantidade_saldo as qtd_item',
                        'compra_item_fornecedor.valor_unitario as valorunitario',
                        'naturezadespesa.codigo as natureza_despesa',
                        'naturezadespesa.id as natureza_despesa_id',
                        'compra_item_fornecedor.valor_negociado as valortotal',
                        'saldo_contabil.saldo',
                        'compra_item_minuta_empenho.subelemento_id',
                        'compra_item_minuta_empenho.quantidade',
                        'compra_item_minuta_empenho.valor',
                        DB::raw("SUBSTRING(saldo_contabil.conta_corrente,18,6) AS natureza_despesa"),
                        DB::raw('compra_item_minuta_empenho.id AS "numeroLinha"')
                    ]
                )
                ->get()
                ->toArray();
            $valor_utilizado = CompraItemMinutaEmpenho::where('compra_item_minuta_empenho.minutaempenho_id', $minuta_id)
                ->select(DB::raw('coalesce(sum(valor),0) as sum'))
                ->first()->toArray();
        }

        if ($request->ajax()) {
            $dados = DataTables::of($itens)
                ->addColumn(
                    'ci_id',
                    function ($item) use ($modMinutaEmpenho) {
                        return $this->addColunaCompraItemId($item);
                    }
                )
                ->addColumn(
                    'subitem',
                    function ($item) use ($modMinutaEmpenho) {
                        return $this->addColunaSubItem($item);
                    }
                )
                ->addColumn(
                    'quantidade',
                    function ($item) use ($tipo) {
                        return $this->addColunaQuantidade($item, $tipo);
                    }
                )
                ->addColumn(
                    'valor_total',
                    function ($item) use ($tipo) {
                        return $this->addColunaValorTotal($item, $tipo);
                    }
                )
                ->addColumn(
                    'valor_total_item',
                    function ($item) use ($tipo) {
                        return $this->addColunaValorTotalItem($item, $tipo);
                    }
                )
                ->addColumn('descricaosimplificada', function ($itens) use ($modMinutaEmpenho) {
                    if ($itens['descricaosimplificada'] != null && $itens['descricaosimplificada'] !== 'undefined') {

                        return $this->montarBotaoTextoDetalhado($itens['descricaosimplificada'], $itens['descricaodetalhada'], $itens['numeroLinha']);

                    }

                    return $this->montarBotaoTextoDetalhado($itens['catmatser_desc_simplificado'], $itens['catmatser_desc'], $itens['numeroLinha']);

                })
                ->rawColumns(['subitem', 'quantidade', 'valor_total', 'valor_total_item', 'descricaosimplificada'])
                ->make(true);

            return $dados;
        }

        $html = $this->retornaGridItens();

        $tipo = ($codigoitem->descricao === 'Compra' || $codigoitem->descricao === 'Suprimento') ? 'compra_item_id' : 'contrato_item_id';
        return view(
            'backpack::mod.empenho.Etapa5SubElemento',
            compact('html')
        )->with([
            'credito'         => $itens[0]['saldo'],
            'valor_utilizado' => $valor_utilizado['sum'],
            'saldo'           => $itens[0]['saldo'] - $valor_utilizado['sum'],
            'update'          => false,
            'tipo'            => $tipo,
            'tipo_item'       => $itens[0]['descricao'],
            'update'          => $valor_utilizado['sum'] > 0,
            'fonte_alterada'  => $modMinutaEmpenho->fonte_alterada
        ]);
    }

    /**
     * Monta $html com definições do Grid
     *
     * @return Builder
     */
    private function retornaGridItens(): Builder
    {
        $html = $this->htmlBuilder
            ->addColumn(
                [
                    'data' => 'ci_id',
                    'name' => 'ci_id',
                    'title' => '',
                    'orderable' => false,
                    'searchable' => false,
                    'visible' => false
                ]
            )
            ->addColumn(
                [
                    'data' => 'descricao',
                    'name' => 'descricao',
                    'title' => 'Tipo',
                    'orderable' => false,
                    'searchable' => false
                ]
            )
            ->addColumn(
                [
                    'data' => 'codigo_siasg',
                    'name' => 'codigo_siasg',
                    'title' => 'Codigo',
                ]
            )
            ->addColumn(
                [
                    'data' => 'numero_item',
                    'name' => 'numero_item',
                    'title' => 'Número',
                    'orderable' => false,
                    'searchable' => false
                ]
            )
            ->addColumn(
                [
                    'data' => 'descricaosimplificada',
                    'name' => 'descricaosimplificada',
                    'title' => 'Descrição',
                ]
            )
            ->addColumn(
                [
                    'data' => 'qtd_item',
                    'name' => 'qtd_item',
                    'title' => 'Qtd. de Item',
                ]
            )
            ->addColumn(
                [
                    'data' => 'valorunitario',
                    'name' => 'valorunitario',
                    'title' => 'Valor Unit.',
                ]
            )
            ->addColumn(
                [
                    'data' => 'valor_total_item',
                    'name' => 'valor_total_item',
                    'title' => 'Valor Total do Item',
                ]
            )
            ->addColumn(
                [
                    'data' => 'natureza_despesa',
                    'name' => 'natureza_despesa',
                    'title' => 'Natureza da Despesa',
                ]
            )
            ->addColumn(
                [
                    'data' => 'subitem',
                    'name' => 'subitem',
                    'title' => 'Subelemento',
                    'orderable' => false,
                    'searchable' => false
                ]
            )
            ->addColumn(
                [
                    'data' => 'quantidade',
                    'name' => 'quantidade',
                    'title' => 'Qtd',
                    'orderable' => false,
                    'searchable' => false
                ]
            )
            ->addColumn(
                [
                    'data' => 'valor_total',
                    'name' => 'valor_total',
                    'title' => 'Valor Total',
                    'orderable' => false,
                    'searchable' => false
                ]
            )
            ->parameters(
                [
                    'processing' => true,
                    'serverSide' => true,
                    'info' => true,
                    'order' => [
                        0,
                        'desc'
                    ],
                    'autoWidth' => false,
                    'bAutoWidth' => false,
                    'paging' => true,
                    'lengthChange' => true,
                    'lengthMenu' => [
                        [10, 25, 50, 100, -1],
                        ['10', '25', '50', '100', 'Todos']
                    ],
                    'language' => [
                        'url' => asset('/json/pt_br.json')
                    ],
                    'initComplete' => 'function() { $(\'.subitem\').select2(); atualizaMascara() }',
                    'responsive' => [true,
                        'details' => [
                            'renderer' => '$.fn.dataTable.Responsive.renderer.listHiddenNodes()'
                        ]
                    ]
                ]
            );

        return $html;
    }

    private function addColunaSubItem($item)
    {
        $subItens = Naturezasubitem::where('naturezadespesa_id', $item['natureza_despesa_id'])
            ->orderBy('codigo', 'asc')
            ->get()->pluck('codigo_descricao', 'id');

        $retorno = '<select name="subitem[]" id="subitem" class="subitem" style="width:200px;">';
        foreach ($subItens as $key => $subItem) {
            $selected = ($key == $item['subelemento_id']) ? 'selected' : '';
            $retorno .= "<option value='$key' $selected>$subItem</option>";
        }
        $retorno .= '</select>';
        return $this->addColunaCompraItemId($item) . $retorno;
    }

    private function addColunaQuantidade($item, $tipo)
    {
        //CASO SEJA CONTRATO
        if ($tipo === 'contrato_item_id') {
            return $this->setColunaContratoQuantidade($item);
        }

        //CASO SEJA SUPRIMENTO
        if (strpos($item['catmatser_desc'], 'SUPRIMENTO') !== false) {
            return $this->setColunaSuprimentoQuantidade($item);
        }

        //CASO SEJA COMPRA E SISRP
        if ($item['tipo_compra_descricao'] === 'SISRP') {
            return $this->setColunaCompraSisrpQuantidade($item);
        }

        //CASO SEJA COMPRA SISPP MATERIAL
        if ($item['descricao'] === 'Material') {
            return $this->setColunaCompraSisppMaterialQuantidade($item);
        }

        return $this->setColunaCompraSisppServicoQuantidade($item);

    }

    private function addColunaValorTotal($item, $tipo)
    {
        //CASO SEJA CONTRATO
        if ($tipo === 'contrato_item_id') {
            return $this->setColunaContratoValorTotal($item);
        }

        //CASO SEJA SUPRIMENTO
        if (strpos($item['catmatser_desc'], 'SUPRIMENTO') !== false) {
            return $this->setColunaSuprimentoValorTotal($item);
        }

        //CASO SEJA COMPRA E SISRP
        if ($item['tipo_compra_descricao'] === 'SISRP') {
            return $this->setColunaCompraSisrpValorTotal($item);
        }

        //CASO SEJA COMPRA SISPP MATERIAL
        if ($item['descricao'] === 'Material') {
            return $this->setColunaCompraSisppMaterialValorTotal($item);
        }

        return $this->setColunaCompraSisppServicoValorTotal($item);

    }

    private function addColunaValorTotalItem($item, $tipo)
    {
        return "<td>" . $item['qtd_item'] * $item['valorunitario'] . "</td>"
            . " <input  type='hidden' id='valor_total_item" . $item[$tipo] . "'"
            . " name='valor_total_item[]"
            . "' value='" . $item['qtd_item'] * $item['valorunitario'] . "'> ";
    }

    private function addColunaCompraItemId($item)
    {
        if (isset($item['compra_item_id'])) {
            return " <input  type='hidden' id='" . ''
                . "' data-tipo='' name='compra_item_id[]' value='" . $item['compra_item_id'] . "'   > ";
        }
        return " <input  type='hidden' id='" . ''
            . "' data-tipo='' name='contrato_item_id[]' value='" . $item['contrato_item_id'] . "'   > ";
    }
    /**
     * Método criado para receber um array de valores e fazer o devido arredondamento dos valores recebidos.
     */
    public function arredondarValores($arrayArredondar){
        /**
         * O array abaixo foi criado para testar o arredondamento, baseado nos valores passados para teste.
         * Para fazer o teste, descomentar o arrayArredondar e descomentar a linha do dd() que está antes do return.
         */
        // $arrayArredondar = array(
        //     '0' => 3200.00,
        //     '1' => 2700.00,
        //     '2' => 2109.99,
        //     '3' => 1699.995,
        //     '4' => 2959.995,
        //     '5' => 2930.00,
        //     '6' => 900.00,
        //     '7' => 49.899,
        // );
        $index = 0;
        $arrayFinal = array();
        $valorTotal = 0;
        foreach($arrayArredondar as $valorArredondar){
            $valorArredondado = round($valorArredondar, 2);
            $arrayFinal[$index] = $valorArredondado;
            $index++;
            $valorTotal = ($valorTotal + $valorArredondado);
        }
        // descomentar a linha abaixo e o arrayArredondar, no início do método, para teste.
        return $arrayFinal;
    }
    // método que recebe os dados, calcula o valor máximo permitido para a etapa 5 e verifica se é maior do que o valor informado
    public function verificarValorFinalParaEtapa5($request, $item, $valorInformado, $index, $objMinuta){

        $valorTotalDoItem = $request->valor_total_item[$index];
        // dados do item
        $objCompraItemFornecedor = CompraItemFornecedor::where('compra_item_id', $item)
            ->where('fornecedor_id', $objMinuta->fornecedor_compra_id)
            ->first();
        // dados da compra
        $compraItemUnidade = CompraItemUnidade::where('compra_item_id', $item)
            ->where('unidade_id', session('user_ug_id'))
            ->first();

        $quantidadeAutorizadaCompraItemUnidade = $compraItemUnidade->quantidade_autorizada;
        $quantidadeSaldoCompraItemUnidade = $compraItemUnidade->quantidade_saldo;
        $valorUnitarioCompraItemFornecedor = floatval($objCompraItemFornecedor->valor_unitario);
        // valor total da compra é a quantidade autorizada do item * valor unitário
        $valorTotalDaCompra = ( $quantidadeAutorizadaCompraItemUnidade * $valorUnitarioCompraItemFornecedor );

        // Esse valor total do item mais 25% seria o valor total da compra * 25%
        // $valorTotalCompra25PorCento = $valorTotalDaCompra * 1.25;

        $valorTotalCompra25PorCento = $valorTotalDaCompra;

        // Para calcularmos o valor final para verificação, usaremos a fórmula
        $valorFinalParaVerificacao = (
             $valorTotalCompra25PorCento -
             ( $quantidadeAutorizadaCompraItemUnidade - $quantidadeSaldoCompraItemUnidade ) *
             $valorUnitarioCompraItemFornecedor
        );
        $valorFinalParaVerificacaoArredondado = round($valorFinalParaVerificacao, 2);

        if($valorInformado > $valorFinalParaVerificacaoArredondado){
            return $arrayRetorno = array('valorFinalParaVerificacaoArredondado' => $valorFinalParaVerificacaoArredondado, 'result' => false);
        } else {
            return $arrayRetorno = array('valorFinalParaVerificacaoArredondado' => $valorFinalParaVerificacaoArredondado, 'result' => true);
        }
    }
    public function store(Request $request)
    {
        list($dados, $credito, $valor_utilizado, $minuta_id) = $this->extrairDadosDoRequest($request);

        if ($credito - $valor_utilizado < 0) {
            Alert::error('O saldo não pode ser negativo.')->flash();
            return redirect()->route('empenho.minuta.etapa.subelemento', ['minuta_id' => $minuta_id]);
        }

        list($valores, $modMinuta, $tipo) = $this->tratarValores($request, $minuta_id);

        if ($tipo === 'Compra') {
            if (in_array(0, $valores) || in_array('', $valores)) {
                Alert::error('O item não pode estar com valor zero.')->flash();
                return redirect()->route('empenho.minuta.etapa.subelemento', ['minuta_id' => $minuta_id]);
            }

            foreach ($dados['qtd'] as $indice => $qtd) {
                $quantidadeTotal = $dados['quantidade_total'][$indice];

                if ($qtd > $quantidadeTotal) {
                    $compra_item_id = $dados['compra_item_id'][$indice];
                    $compraItem = CompraItem::find($compra_item_id);

                    Alert::error('A quantidade selecionada ('.$qtd.') do item ['.($compraItem->numero).'] é maior' .
                        ' que a Qtd. de Item ('.$quantidadeTotal.') permitida.')->flash();
                    return redirect()->route('empenho.minuta.etapa.subelemento', ['minuta_id' => $minuta_id]);
                }
            }
        }

        DB::beginTransaction();
        try {
            //Caso seja do Tipo Contrato, atualizar ContratoItemMinutaEmpenho
            $this->tipoContratoAtualizarCIME($tipo, $request, $valores);

            if ($tipo == 'Compra') {
                $tipoCompraId = $this->retornaIdCodigoItem('Tipo Compra','SISRP');

                $compra_item_ids = $request->compra_item_id;
                foreach ($compra_item_ids as $index => $item) {

                    if($tipoCompraId == $modMinuta->compra->tipo_compra_id){
                        if (floor($request->qtd[$index]) != $request->qtd[$index]) {
                            Alert::error('A quantidade selecionada deve ser inteira.')->flash();
                            return redirect()->route('empenho.minuta.etapa.subelemento', ['minuta_id' => $minuta_id]);
                        }
                    }

                    if ($request->qtd[$index] == 0) {
                        Alert::error('A quantidade selecionada não pode ser zero.')->flash();
                        return redirect()->route('empenho.minuta.etapa.subelemento', ['minuta_id' => $minuta_id]);
                    }

                    //todo verificar se isso aqui vai dar problema com duas compra items unidades
                    CompraItemMinutaEmpenho::where('compra_item_id', $item)
                        ->where('minutaempenho_id', $request->minuta_id)
                        ->update([
                            'subelemento_id' => $request->subitem[$index],
                            'quantidade' => ($request->qtd[$index]),
                            'valor' => $valores[$index]
                        ]);

                    $compraItemUnidade = CompraItemUnidade::where('compra_item_unidade.compra_item_id', $item)
                        ->where('compra_item_unidade.unidade_id', session('user_ug_id'))
                        ->where('compra_item_unidade.situacao', '=', true)
                        ->whereRaw("
                            CASE WHEN compra_item_unidade.tipo_uasg = 'C'
                                    or tipo_compra.descricao = 'SISPP'
                                THEN compra_item_unidade.fornecedor_id = '".$modMinuta->fornecedor_compra_id."'
                                ELSE compra_item_unidade.fornecedor_id IS NULL
                            END")
                        ->join('compra_items', 'compra_items.id', '=', 'compra_item_unidade.compra_item_id')
                        ->join('compras', 'compras.id', '=', 'compra_items.compra_id')
                        ->join('codigoitens as tipo_compra', 'tipo_compra.id', '=', 'compras.tipo_compra_id')
                        ->select('compra_item_unidade.*')
                        ->first();

                    $saldo = $this->retornaSaldoAtualizado(
                        $item,
                        null,
                        $compraItemUnidade->id
                    );
                    $compraItemUnidade->quantidade_saldo = $saldo->saldo;
                    $compraItemUnidade->save();
                }
            }

            if ($tipo == 'Suprimento') {
                $compra_item_ids = $request->compra_item_id;
                foreach ($compra_item_ids as $index => $item) {

                    if ($request->qtd[$index] == 0) {
                        Alert::error('A quantidade selecionada não pode ser zero.')->flash();
                        return redirect()->route('empenho.minuta.etapa.subelemento', ['minuta_id' => $minuta_id]);
                    }

                    CompraItemMinutaEmpenho::where('compra_item_id', $item)
                        ->where('minutaempenho_id', $request->minuta_id)
                        ->update([
                            'subelemento_id' => $request->subitem[$index],
                            'quantidade' => ($request->qtd[$index]),
                            'valor' => $valores[$index]
                        ]);
                }
            }

            $modMinuta = MinutaEmpenho::find($minuta_id);
            $modMinuta->etapa = 6;
            $modMinuta->valor_total = $request->valor_utilizado;
            $modMinuta->save();

            DB::commit();
        } catch (Exception $exc) {
            DB::rollback();
        }

        return redirect()->route('empenho.crud./minuta.edit', ['minutum' => $modMinuta->id]);
    }

    public function update(Request $request)
    {
        list($dados, $credito, $valor_utilizado, $minuta_id) = $this->extrairDadosDoRequest($request);

        if ($credito - $valor_utilizado < 0) {
            Alert::error('O saldo não pode ser negativo.')->flash();
            return redirect()->route('empenho.minuta.etapa.subelemento', ['minuta_id' => $minuta_id]);
        }

        list($valores, $modMinuta, $tipo) = $this->tratarValores($request, $minuta_id);

        if ($tipo === 'Compra') {
            if (in_array(0, $valores) || in_array('', $valores)) {
                Alert::error('O item não pode estar com valor zero.')->flash();
                return redirect()->route('empenho.minuta.etapa.subelemento', ['minuta_id' => $minuta_id]);
            }

            foreach ($dados['qtd'] as $indice => $qtd) {
                $quantidadeTotal = $dados['quantidade_total'][$indice];

                if ($qtd > $quantidadeTotal) {
                    $compra_item_id = $dados['compra_item_id'][$indice];
                    $compraItem = CompraItem::find($compra_item_id);

                    Alert::error('A quantidade selecionada ('.$qtd.') do item ['.($compraItem->numero).'] é maior' .
                        ' que a Qtd. de Item ('.$quantidadeTotal.') permitida.')->flash();
                    return redirect()->route('empenho.minuta.etapa.subelemento', ['minuta_id' => $minuta_id]);
                }
            }
        }

        DB::beginTransaction();
        try {
            //Caso seja do Tipo Contrato, atualizar ContratoItemMinutaEmpenho
            $this->tipoContratoAtualizarCIME($tipo, $request, $valores);

            if ($tipo === 'Compra') {
                $tipoCompraId = $this->retornaIdCodigoItem('Tipo Compra','SISRP');

                $compra_item_ids = $request->compra_item_id;
                foreach ($compra_item_ids as $index => $item) {

                    if($tipoCompraId == $modMinuta->compra->tipo_compra_id){
                        if (floor($request->qtd[$index]) != $request->qtd[$index]) {
                            Alert::error('A quantidade selecionada deve ser inteira.')->flash();
                            return redirect()->route('empenho.minuta.etapa.subelemento', ['minuta_id' => $minuta_id]);
                        }
                    }

                    //todo verificar se isso aqui vai dar problema com duas compra items unidades
                    $cime = CompraItemMinutaEmpenho::where('compra_item_id', $item)
                        ->where('minutaempenho_id', $request->minuta_id);
                    $cime->update([
                            'subelemento_id' => $request->subitem[$index],
                            'quantidade' => ($request->qtd[$index]),
                            'valor' => $valores[$index]
                        ]);

                    $compraItemUnidade = CompraItemUnidade::where('compra_item_unidade.compra_item_id', $item)
                        ->where('compra_item_unidade.unidade_id', session('user_ug_id'))
                        ->where('compra_item_unidade.situacao', '=', true)
                        ->whereRaw("
                            CASE WHEN compra_item_unidade.tipo_uasg = 'C'
                                    or tipo_compra.descricao = 'SISPP'
                                THEN compra_item_unidade.fornecedor_id = '".$modMinuta->fornecedor_compra_id."'
                                ELSE compra_item_unidade.fornecedor_id IS NULL
                            END")
                        ->join('compra_items', 'compra_items.id', '=', 'compra_item_unidade.compra_item_id')
                        ->join('compras', 'compras.id', '=', 'compra_items.compra_id')
                        ->join('codigoitens as tipo_compra', 'tipo_compra.id', '=', 'compras.tipo_compra_id')
                        ->select('compra_item_unidade.*')
                        ->first();
//                    dd($compraItemUnidade);

                    $saldo = $this->retornaSaldoAtualizado(
                        $item,
                        null,
                        $compraItemUnidade->id
                    );
                    $compraItemUnidade->quantidade_saldo = $saldo->saldo;
                    $compraItemUnidade->save();
                }
            }
            if ($tipo === 'Suprimento') {
                $compra_item_ids = $request->compra_item_id;
                foreach ($compra_item_ids as $index => $item) {

                    if ($request->qtd[$index] == 0) {
                        Alert::error('A quantidade selecionada não pode ser zero.')->flash();
                        return redirect()->route('empenho.minuta.etapa.subelemento', ['minuta_id' => $minuta_id]);
                    }

                    CompraItemMinutaEmpenho::where('compra_item_id', $item)
                        ->where('minutaempenho_id', $request->minuta_id)
                        ->update([
                            'subelemento_id' => $request->subitem[$index],
                            'quantidade' => ($request->qtd[$index]),
                            'valor' => $valores[$index]
                        ]);
                }
            }

            $modMinuta = MinutaEmpenho::find($minuta_id);
            $modMinuta->etapa = 6;
            $modMinuta->valor_total = $request->valor_utilizado;
            $modMinuta->save();

            DB::commit();
        } catch (Exception $exc) {
            DB::rollback();
        }

        return redirect()->route('empenho.crud./minuta.edit', ['minutum' => $modMinuta->id]);
    }

    /**
     * @param Request $request
     * @param $minuta_id
     * @return array
     */
    public function tratarValores(Request $request, $minuta_id): array
    {
        $valores = $request->valor_total;

        $valores = array_map(
            function ($valores) {
                return $this->retornaFormatoAmericano($valores);
            },
            $valores
        );

        $valores = $this->arredondarValores($valores);

        $modMinuta = MinutaEmpenho::find($minuta_id);
        $tipo = $modMinuta->tipo_empenhopor->descricao;
        return array($valores, $modMinuta, $tipo);
    }

    /**
     * @param $tipo
     * @param Request $request
     * @param $valores
     */
    public function tipoContratoAtualizarCIME($tipo, Request $request, $valores): void
    {
        if ($tipo === 'Contrato') {
            $contrato_item_ids = $request->contrato_item_id;

            foreach ($contrato_item_ids as $index => $item) {
                ContratoItemMinutaEmpenho::where('contrato_item_id', $item)
                    ->where('minutaempenho_id', $request->minuta_id)
                    ->update([
                        'subelemento_id' => $request->subitem[$index],
                        'quantidade' => ($request->qtd[$index]),
                        'valor' => $valores[$index]
                    ]);
            }
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    public function extrairDadosDoRequest(Request $request): array
    {
        $dados = $request->all();
        $credito = (number_format($request->credito, 2, '.', ''));
        $valor_utilizado = (number_format($request->valor_utilizado, 2, '.', ''));
        $minuta_id = $request->get('minuta_id');
        return array($dados, $credito, $valor_utilizado, $minuta_id);
    }

}
