<?php

namespace App\Http\Controllers\Empenho\Pdm;


use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Traits\CatmatpdmsTrait;
use Illuminate\Support\Facades\DB;

class ConsultaFracionamentoDespesaController extends CrudController
{
    use CatmatpdmsTrait;

    public function setup()
    {
        $unidadeLogada = session()->get('user_ug_id');

        $this->crud->setModel('App\Models\Catmatpdms');
        $this->crud->setListView('backpack::crud.fracionamentodespesa.list');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/empenho/consulta/fracionamento-despesa-pdm');
        $this->crud->setEntityNameStrings('Relatório de material por PDM', 'Relatório de material por PDMs');
        $this->crud->setDetailsRowView('backpack::crud.details.fracionamentodespesa.details_row');

        $this->crud->addClause('from', DB::raw("
                        ((
                            select
                                catmatpdms.id,
                                catmatpdms.codigo,
                                catmatpdms.descricao,
                                extract('Year' from minutaempenhos.data_emissao) as ano_minuta_emissao,
                                SUM(compra_item_minuta_empenho.valor) as valor_empenhado,
                                codigoitens.descricao AS tipo_item
                            from
                                catmatpdms
                            inner join catmatseritens on
                                catmatseritens.catmatpdm_id = catmatpdms.id
                            inner join compra_items on
                                compra_items.catmatseritem_id = catmatseritens.id
                            inner join codigoitens on
                                codigoitens.id = compra_items.tipo_item_id
                            inner join compra_item_minuta_empenho on
                                compra_item_minuta_empenho.compra_item_id = compra_items.id
                            inner join minutaempenhos on
                                minutaempenhos.id = compra_item_minuta_empenho.minutaempenho_id
                            inner join saldo_contabil on
                                saldo_contabil.id = minutaempenhos.saldo_contabil_id
                            inner join unidades on
                                unidades.id = saldo_contabil.unidade_id
                            inner join minutaempenhos_remessa on
                                compra_item_minuta_empenho.minutaempenhos_remessa_id = minutaempenhos_remessa.id
                            where
                                minutaempenhos.situacao_id = 217
                                and minutaempenhos.unidade_id = $unidadeLogada
                                and ((minutaempenhos_remessa.situacao_id = 215
                                    and minutaempenhos_remessa.remessa = 0)
                                or (minutaempenhos_remessa.remessa > 0
                                    and minutaempenhos_remessa.situacao_id = 217))
                            group by
                                catmatpdms.id,
                                catmatpdms.codigo,
                                catmatpdms.descricao,
                                codigoitens.descricao,
                                extract('Year' from minutaempenhos.data_emissao)
                            having
                                SUM(compra_item_minuta_empenho.valor) != 0
                            )
                            union
                            (
                            select
                                catmatpdms.id,
                                catmatpdms.codigo,
                                catmatpdms.descricao,
                                extract('Year' from minutaempenhos.data_emissao) as ano_minuta_emissao,
                                SUM(cime.valor) as valor_empenhado,
                                codigoitens.descricao AS tipo_item
                            from
                                catmatpdms
                            inner join catmatseritens on
                                catmatseritens.catmatpdm_id = catmatpdms.id
                            inner join contratoitens as c on
                                c.catmatseritem_id = catmatseritens.id
                            inner join contrato_item_minuta_empenho as cime on
                                cime.contrato_item_id = c.id
                            inner join minutaempenhos on
                                minutaempenhos.id = cime.minutaempenho_id
                            inner join saldo_contabil on
                                saldo_contabil.id = minutaempenhos.saldo_contabil_id
                            inner join unidades on
                                unidades.id = saldo_contabil.unidade_id
                            inner join minutaempenhos_remessa on
                                cime.minutaempenhos_remessa_id = minutaempenhos_remessa.id
                            inner join codigoitens on
                                codigoitens.id = c.tipo_id
                            where
                                minutaempenhos.situacao_id = 217
                            and minutaempenhos.unidade_id = $unidadeLogada
                            and ((minutaempenhos_remessa.situacao_id = 215
                                and minutaempenhos_remessa.remessa = 0)
                            or (minutaempenhos_remessa.remessa > 0
                                and minutaempenhos_remessa.situacao_id = 217))
                            group by
                                catmatpdms.id,
                                catmatpdms.codigo,
                                catmatpdms.descricao,
                                codigoitens.descricao,
                                extract('Year' from minutaempenhos.data_emissao)
                            having
                                SUM(cime.valor) != 0
                            order by
                                extract('Year' from minutaempenhos.data_emissao) desc
                        ) )as catmatpdms"
        )
        );

        $this->crud->addClause('select', [
            "catmatpdms.id",
            "catmatpdms.codigo",
            "catmatpdms.descricao",
            'catmatpdms.tipo_item',
            "catmatpdms.ano_minuta_emissao",
            DB::raw("CASE
                  WHEN SUM(catmatpdms.valor_empenhado) >= 1
                  THEN LTRIM(TO_CHAR(SUM(catmatpdms.valor_empenhado), 'FM999G999G000D0099'), '0')
                  ELSE TO_CHAR(SUM(catmatpdms.valor_empenhado), 'FM999G999G0D0099')
                  END AS valor_empenhado")
        ]);
        $this->crud->addClause('groupBY', ['catmatpdms.id', 'catmatpdms.codigo', 'catmatpdms.descricao', 'catmatpdms.tipo_item', 'ano_minuta_emissao']);
        $this->crud->addClause('orderBy', 'ano_minuta_emissao', 'desc');
        $this->crud->enableExportButtons();
        $this->crud->enableDetailsRow();
        //Removendo todos os botões para tirar a coluna "ações"
        $this->crud->removeAllButtons();
        $this->crud->allowAccess('details_row');

        $this->crud->addColumns($this->adicionaColunas());

        $this->adicionaFiltroAno();
    }

    private function adicionaColunas()
    {
        $colunas[] = [
            'name' => 'ano_minuta_emissao',
            'label' => 'Ano',
            'type' => 'string',
            'priority' => 1,
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
        ];
        $colunas[] = [
            'name' => 'codigo',
            'label' => 'Código PDM',
            'type' => 'string',
            'priority' => 1,
            'searchLogic' => function ($query, $column, $searchTerm) {
                $query->orWhere('catmatpdms.codigo', 'ilike', '%' . $searchTerm . '%');
            },
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
        ];
        $colunas[] = [
            'name' => 'tipo_item',
            'label' => 'Tipo',
            'type' => 'string',
            'priority' => 1,
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
        ];


        $colunas[] = [
            'name' => 'descricao',
            'label' => 'Descrição',
            'type' => 'string',
            'priority' => 1,
            'searchLogic' => function ($query, $column, $searchTerm) {
                $query->orWhere('catmatpdms.descricao', 'ilike', '%' . $searchTerm . '%');
            },
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
        ];
        $colunas[] = [
            'name' => 'valor_empenhado',
            'label' => 'Valor Empenhado',
            'type' => 'string',
            'priority' => 1,
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
            'prefix'        => 'R$ ',
        ];

        return $colunas;
    }

    private function adicionaFiltroAno()
    {
        $anos = [];

        $campo = [
            'name' => 'ano',
            'type' => 'select2',
            'label' => 'Ano'
        ];

        for($ano = 2021; $ano <= date('Y'); $ano++){
            $anos[$ano] = $ano;
        }

        return $this->crud->addFilter(
            $campo, $anos, function ($value) {
            $this->crud->query->where('ano_minuta_emissao', '=', "$value");
        });
    }

    public function show($id)
    {
        $content = parent::show($id);

        return $content;
    }

    public function showDetailsRow($id)
    {
        $thead = ['Item', 'Empenho', 'Unidade Emitente do Empenho', 'Valor', 'Tipo Empenho'];
        $pdmItens = $this->getPDMItens($id);
        return view('backpack::crud.details.fracionamentodespesa.details_row', ['thead' => $thead, 'itens' => $pdmItens]);
    }

    // Método para consertar o bug da paginação do datatable quando tem orderby e groupby
    public function search()
    {
        $this->crud->hasAccessOrFail('list');
        $this->crud->setOperation('list');

        $totalRows = $this->crud->model->count();
        $filteredRows = $this->crud->query->toBase()->getCountForPagination();
        $startIndex = $this->request->input('start') ?: 0;
        // if a search term was present
        if ($this->request->input('search') && $this->request->input('search')['value']) {
            // filter the results accordingly
            $this->crud->applySearchTerm($this->request->input('search')['value']);
            // recalculate the number of filtered rows
            $filteredRows = $this->crud->count();
        }
        // start the results according to the datatables pagination
        if ($this->request->input('start')) {
            $this->crud->skip($this->request->input('start'));
        }
        // limit the number of results according to the datatables pagination
        if ($this->request->input('length')) {
            $this->crud->take($this->request->input('length'));
        }
        // overwrite any order set in the setup() method with the datatables order
        if ($this->request->input('order')) {
            $column_number = $this->request->input('order')[0]['column'];
            $column_direction = $this->request->input('order')[0]['dir'];
            $column = $this->crud->findColumnById($column_number);
            if ($column['tableColumn']) {
                // clear any past orderBy rules
                $this->crud->query->getQuery()->orders = null;
                // apply the current orderBy rules
                $this->crud->query->orderBy($column['name'], $column_direction);
            }

            // check for custom order logic in the column definition
            if (isset($column['orderLogic'])) {
                $this->crud->customOrderBy($column, $column_direction);
            }
        }
        $entries = $this->crud->getEntries();

        return $this->crud->getEntriesAsJsonForDatatables($entries, $totalRows, $filteredRows, $startIndex);
    }
}
