<?php

namespace App\Http\Controllers\Empenho\Pdm;


use App\Models\Catmatseritem;
use App\Models\ModelGenerica;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Illuminate\Support\Facades\DB;

class ConsultaFracionamentoDespesaServicoController extends CrudController
{

    public function setup()
    {
        $unidadeLogada = session()->get('user_ug_id');

        $this->crud->setModel('App\Models\Catmatseritem');
        $this->crud->setListView('backpack::crud.fracionamentodespesa.servico.list');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/empenho/consulta/fracionamento-despesa-servico');
        $this->crud->setEntityNameStrings('Relatório por código de serviço', 'Relatório por códigos de serviço');
        $this->crud->setDetailsRowView('backpack::crud.details.fracionamentodespesa.details_row');


        $this->crud->addClause('from', DB::raw("
                        ((
                            select
                                catmatseritens.id,
                                catmatseritens.codigo_siasg,
                                catmatseritens.descricao,
                                extract('Year' from minutaempenhos.data_emissao) as ano_minuta_emissao,
                                SUM(compra_item_minuta_empenho.valor) as valor_empenhado,
                                catmatseritens.deleted_at
                            from
                                catmatseritens
                            inner join catmatsergrupos on
                                catmatseritens.grupo_id = catmatsergrupos.id
                            inner join compra_items on
                                compra_items.catmatseritem_id = catmatseritens.id
                            inner join codigoitens on
                                codigoitens.id = compra_items.tipo_item_id
                            inner join codigos on
                                codigos.id = codigoitens.codigo_id
                            inner join compra_item_minuta_empenho on
                                compra_item_minuta_empenho.compra_item_id = compra_items.id
                            inner join minutaempenhos on
                                minutaempenhos.id = compra_item_minuta_empenho.minutaempenho_id
                            inner join saldo_contabil on
                                saldo_contabil.id = minutaempenhos.saldo_contabil_id
                            inner join unidades on
                                unidades.id = saldo_contabil.unidade_id
                            inner join minutaempenhos_remessa on
                                compra_item_minuta_empenho.minutaempenhos_remessa_id = minutaempenhos_remessa.id
                            where
                                minutaempenhos.situacao_id = 217
                                and minutaempenhos.unidade_id = $unidadeLogada
                                and ((minutaempenhos_remessa.situacao_id = 215
                                    and minutaempenhos_remessa.remessa = 0)
                                or (minutaempenhos_remessa.remessa > 0
                                    and minutaempenhos_remessa.situacao_id = 217))
                                and codigoitens.descricao = 'Serviço'
                                and codigos.descricao = 'Tipo CATMAT e CATSER'
                                and catmatseritens.deleted_at is null
                            group by
                                catmatseritens.id,
                                catmatseritens.codigo_siasg,
                                catmatseritens.descricao,
                                extract('Year' from minutaempenhos.data_emissao)
                            having
                                SUM(compra_item_minuta_empenho.valor) != 0
                            )
                            union
                            (
                            select
                                catmatseritens.id,
                                catmatseritens.codigo_siasg,
                                catmatseritens.descricao,
                                extract('Year' from minutaempenhos.data_emissao) as ano_minuta_emissao,
                                SUM(cime.valor) as valor_empenhado,
                                catmatseritens.deleted_at
                            from
                                catmatseritens
                            inner join catmatsergrupos on
                                catmatseritens.grupo_id = catmatsergrupos.id
                            inner join contratoitens as c on
                                c.catmatseritem_id = catmatseritens.id
                            inner join contrato_item_minuta_empenho as cime on
                                cime.contrato_item_id = c.id
                            inner join minutaempenhos on
                                minutaempenhos.id = cime.minutaempenho_id
                            inner join saldo_contabil on
                                saldo_contabil.id = minutaempenhos.saldo_contabil_id
                            inner join unidades on
                                unidades.id = saldo_contabil.unidade_id
                            inner join minutaempenhos_remessa on
                                cime.minutaempenhos_remessa_id = minutaempenhos_remessa.id
                            inner join codigoitens on
                                codigoitens.id = c.tipo_id
                            inner join codigos on
                                codigos.id = codigoitens.codigo_id
                            where
                                minutaempenhos.situacao_id = 217
                            and minutaempenhos.unidade_id = $unidadeLogada
                            and ((minutaempenhos_remessa.situacao_id = 215
                                and minutaempenhos_remessa.remessa = 0)
                            or (minutaempenhos_remessa.remessa > 0
                                and minutaempenhos_remessa.situacao_id = 217))
                            and codigoitens.descricao = 'Serviço'
                            and codigos.descricao = 'Tipo CATMAT e CATSER'
                            and catmatseritens.deleted_at is null
                            group by
                                catmatseritens.id,
                                catmatseritens.codigo_siasg,
                                catmatseritens.descricao,
                                extract('Year' from minutaempenhos.data_emissao)
                            having
                                SUM(cime.valor) != 0
                            order by
                                extract('Year' from minutaempenhos.data_emissao) desc
                        ) )as catmatseritens"
                    )
        );


        $this->crud->addClause('select', [
            "catmatseritens.id",
            "catmatseritens.codigo_siasg",
            "catmatseritens.descricao",
            "catmatseritens.ano_minuta_emissao",
            DB::raw("LTRIM(TO_CHAR(SUM(catmatseritens.valor_empenhado::DECIMAL), 'FM999G999G000D0099'), '0') as valor_empenhado")
        ]);
        $this->crud->addClause('groupBY', ['catmatseritens.id', 'catmatseritens.codigo_siasg', 'catmatseritens.descricao', 'ano_minuta_emissao']);
        $this->crud->addClause('orderBy', 'ano_minuta_emissao', 'desc');


        $this->crud->enableExportButtons();
        $this->crud->enableDetailsRow();
        //Removendo todos os botões para tirar a coluna "ações"
        $this->crud->removeAllButtons();
        $this->crud->allowAccess('details_row');

        $this->crud->addColumns($this->adicionaColunas());

        $this->adicionaFiltroAno();
    }


    private function adicionaColunas()
    {
        $colunas[] = [
            'name' => 'ano_minuta_emissao',
            'label' => 'Ano',
            'type' => 'string',
            'priority' => 1,
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
        ];

        $colunas[] = [
            'name' => 'codigo_siasg',
            'label' => 'Código',
            'type' => 'string',
            'searchLogic' => function ($qr, $col, $value) {
                $qr->orWhere('catmatseritens.codigo_siasg', 'like', "%$value%");
            },
            'priority' => 1,
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
        ];

        $colunas[] = [
            'name' => 'descricao',
            'label' => 'Descrição',
            'type' => 'string',
            'priority' => 1,
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
        ];

        $colunas[] = [
            'name' => 'valor_empenhado',
            'label' => 'Valor Empenhado',
            'type' => 'string',
            'priority' => 1,
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
            'prefix'        => 'R$ ',
        ];


        return $colunas;
    }

    private function adicionaFiltroAno()
    {
        $anos = [];

        $campo = [
            'name' => 'ano',
            'type' =>  'select2',
            'label' => 'Ano'
        ];

        for ($ano = 2021; $ano <= date('Y'); $ano++) {
            $anos[$ano] = $ano;

        }
        return $this->crud->addFilter(
            $campo, $anos, function ($value) {
                $this->crud->query->where('ano_minuta_emissao', '=', "$value");
            });
    }

    public function show($id)
    {
        $content = parent::show($id);

        return $content;
    }

    public function showDetailsRow($id)
    {
        $thead = ['Item', 'Empenho', 'Unidade Emitente do Empenho', 'Valor', 'Tipo Empenho'];
        $pdmItens = $this->getItens($id);
      //  dd($pdmItens);
        return view('backpack::crud.details.fracionamentodespesa.servico.details_row', ['thead' => $thead, 'itens' => $pdmItens]);
    }

    private function getItens($id, $ano = null)
    {
        $unidadeLogada = session()->get('user_ug_id');

        // Ano aqui deve vir direto da rota concatenado com o id
        if ($ano == null) {
            $idAno = explode("-", $id);
            $id = $idAno[0];
            $ano = $idAno[1];
        }

        $sqlItem = Catmatseritem::select([
            'compra_items.descricaodetalhada',
            \DB::raw("EXTRACT('Year' FROM minutaempenhos.data_emissao) as ano_minuta_emissao"),
            'catmatseritens.codigo_siasg',
            'minutaempenhos.mensagem_siafi',
            'unidades.nomeresumido as unidadenome',
            'unidades.codigo',
            \DB::raw("case
		            when SUM(compra_item_minuta_empenho.valor) >= 1
                    then LTRIM(TO_CHAR(SUM(compra_item_minuta_empenho.valor), 'FM999G999G000D0099'), '0')
		            else TO_CHAR(SUM(compra_item_minuta_empenho.valor), 'FM99G999G0D0099')
	                end as valor"),
            DB::raw("'Compra' as tipo_minuta")
            ])
            ->distinct()
            ->join('catmatsergrupos', 'catmatseritens.grupo_id', '=', 'catmatsergrupos.id')
            ->join('compra_items', 'compra_items.catmatseritem_id', '=', 'catmatseritens.id')
            ->join('compra_item_minuta_empenho', 'compra_item_minuta_empenho.compra_item_id', '=', 'compra_items.id')
            ->join('minutaempenhos', 'minutaempenhos.id', '=', 'compra_item_minuta_empenho.minutaempenho_id')
            ->join('saldo_contabil', 'saldo_contabil.id', '=', 'minutaempenhos.saldo_contabil_id')
            ->join('unidades', 'unidades.id', '=', 'saldo_contabil.unidade_id')
            ->join('minutaempenhos_remessa', 'compra_item_minuta_empenho.minutaempenhos_remessa_id', '=', 'minutaempenhos_remessa.id')
            ->where('catmatseritens.id', $id)
            ->where('minutaempenhos.unidade_id', '=', $unidadeLogada)
            ->where('minutaempenhos.mensagem_siafi', 'like', "%$ano%")
            ->whereRaw("minutaempenhos.situacao_id = 217
                        AND (
                        (minutaempenhos_remessa.situacao_id = 215 AND minutaempenhos_remessa.remessa = 0)
                        OR  (minutaempenhos_remessa.remessa > 0 AND minutaempenhos_remessa.situacao_id = 217)
                    )")
            ->groupBy('compra_items.descricaodetalhada', \DB::raw("EXTRACT('Year' FROM minutaempenhos.data_emissao)"), 'catmatseritens.codigo_siasg', 'minutaempenhos.mensagem_siafi', 'unidades.nomeresumido', 'unidades.codigo')
            ->havingRaw("SUM(compra_item_minuta_empenho.valor) != 0");
           // ->get()->toArray();


        $sqlItemUnion = Catmatseritem::select([
            'c.descricao_complementar',
            \DB::raw("EXTRACT('Year' FROM minutaempenhos.data_emissao) as ano_minuta_emissao"),
            'catmatseritens.codigo_siasg',
            'minutaempenhos.mensagem_siafi',
            'unidades.nomeresumido as unidadenome',
            'unidades.codigo',
            \DB::raw("case
		            when SUM(cime.valor) >= 1
                    then LTRIM(TO_CHAR(SUM(cime.valor), 'FM999G999G000D0099'), '0')
		            else TO_CHAR(SUM(cime.valor), 'FM99G999G0D0099')
	                end as valor"),
            DB::raw("'Contrato' as tipo_minuta")
        ])
            ->distinct()
            ->join('catmatsergrupos', 'catmatseritens.grupo_id', '=', 'catmatsergrupos.id')
            ->join('contratoitens as c', 'c.catmatseritem_id', '=', 'catmatseritens.id')
            ->join('contrato_item_minuta_empenho as cime', 'cime.contrato_item_id', '=', 'c.id')
            ->join('minutaempenhos', 'minutaempenhos.id', '=', 'cime.minutaempenho_id')
            ->join('saldo_contabil', 'saldo_contabil.id', '=', 'minutaempenhos.saldo_contabil_id')
            ->join('unidades', 'unidades.id', '=', 'saldo_contabil.unidade_id')
            ->join('minutaempenhos_remessa', 'cime.minutaempenhos_remessa_id', '=', 'minutaempenhos_remessa.id')
            ->where('catmatseritens.id', $id)
            ->where('minutaempenhos.unidade_id', '=', $unidadeLogada)
            ->where('minutaempenhos.mensagem_siafi', 'like', "%$ano%")
            ->whereRaw("minutaempenhos.situacao_id = 217
                        AND (
                        (minutaempenhos_remessa.situacao_id = 215 AND minutaempenhos_remessa.remessa = 0)
                        OR  (minutaempenhos_remessa.remessa > 0 AND minutaempenhos_remessa.situacao_id = 217)
                    )")
            ->groupBy('c.descricao_complementar', \DB::raw("EXTRACT('Year' FROM minutaempenhos.data_emissao)"), 'catmatseritens.codigo_siasg', 'minutaempenhos.mensagem_siafi', 'unidades.nomeresumido', 'unidades.codigo')
            ->havingRaw("SUM(cime.valor) != 0");

        $sql = $sqlItem->union($sqlItemUnion)->get()->toArray();
       // dd(str_replace_array('?', $sql->getBindings(), $sql->toSql()));
//        dd(preg_replace('/\s+/', ' ', str_replace_array(
//            '?',
//            $sql->getBindings(),
//            $sql->toSql()
//        )));

        return $sql;
    }


// Método para consertar o bug da paginação do datatable quando tem orderby e groupby
    public function search()
    {
        $this->crud->hasAccessOrFail('list');
        $this->crud->setOperation('list');

        $totalRows = $this->crud->model->count();
        $filteredRows = $this->crud->query->toBase()->getCountForPagination();
        $startIndex = $this->request->input('start') ?: 0;
        // if a search term was present
        if ($this->request->input('search') && $this->request->input('search')['value']) {
            // filter the results accordingly
            $this->crud->applySearchTerm($this->request->input('search')['value']);
            // recalculate the number of filtered rows
            $filteredRows = $this->crud->count();
        }
        // start the results according to the datatables pagination
        if ($this->request->input('start')) {
            $this->crud->skip($this->request->input('start'));
        }
        // limit the number of results according to the datatables pagination
        if ($this->request->input('length')) {
            $this->crud->take($this->request->input('length'));
        }
        // overwrite any order set in the setup() method with the datatables order
        if ($this->request->input('order')) {
            $column_number = $this->request->input('order')[0]['column'];
            $column_direction = $this->request->input('order')[0]['dir'];
            $column = $this->crud->findColumnById($column_number);
            if ($column['tableColumn']) {
                // clear any past orderBy rules
                $this->crud->query->getQuery()->orders = null;
                // apply the current orderBy rules
                $this->crud->query->orderBy($column['name'], $column_direction);
            }

            // check for custom order logic in the column definition
            if (isset($column['orderLogic'])) {
                $this->crud->customOrderBy($column, $column_direction);
            }
        }
        $entries = $this->crud->getEntries();

        return $this->crud->getEntriesAsJsonForDatatables($entries, $totalRows, $filteredRows, $startIndex);
    }
}
