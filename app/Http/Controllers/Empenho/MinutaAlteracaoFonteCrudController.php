<?php
/**
 * Controller com métodos para Saldo Contábil da Minuta de Empenho
 */

namespace App\Http\Controllers\Empenho;

use Alert;
use App\Forms\InserirCelulaOrcamentariaForm;
use App\Http\Controllers\Empenho\Minuta\BaseControllerEmpenho;
use App\Http\Requests\MinutaAlteracaoRequest as StoreRequest;
use App\Http\Requests\MinutaAlteracaoRequest as UpdateRequest;
use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\CompraTrait;
use App\Http\Traits\PermissaoTrait;
use App\Models\Codigoitem;
use App\Models\CompraItemMinutaEmpenho;
use App\Models\CompraItemUnidade;
use App\Models\ContratoItemMinutaEmpenho;
use App\Models\MinutaEmpenhoRemessa;
use App\Models\SaldoContabil;
use App\Models\Unidade;
use App\Models\MinutaEmpenho;
use App\Repositories\Base;
use Exception;
use FormBuilder;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Redirect;
use Route;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use App\STA\ConsultaApiSta;
use App\Http\Traits\Users;
use App\Http\Traits\Formatador;

class MinutaAlteracaoFonteCrudController extends BaseControllerEmpenho
{
    use Users;
    use Formatador;
    use BuscaCodigoItens;
    use CompraTrait;
    use PermissaoTrait;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     * @throws Exception
     */
    public function index(Request $request)
    {
        if ($this->verificaPermissaoConsulta('minuta') === false){
            return redirect()->to('/empenho/minuta/ano/'.date('Y'));
        }

        $minuta_id      = Route::current()->parameter('minuta_id');
        $modMinuta      = MinutaEmpenho::find($minuta_id);
        $unidade_id     = session('user_ug_id');

        if (!empty(session('unidade_ajax_id'))) {
            $unidade_id = session('unidade_ajax_id');
        }

        $modUnidade = Unidade::find($unidade_id);

        $html = $this->retornaGrid($unidade_id);
        $form = $this->retonaFormModal($modUnidade->id, $minuta_id);

        return view('backpack::mod.empenho.AlteracaoSaldoContabil', compact(['html', 'form']))
            ->with('minuta_id', $modMinuta->id)
            ->with('nova_minuta_id', null)
            ->with('fornecedor_id', $modUnidade->fornecedor_compra_id)
            ->with('unidades', $this->buscaUg())
            ->with('modUnidade', $modUnidade)
            ->with('url', route('empenho.crud.alteracaoFonte.store', ['minuta_id'=> $minuta_id]))
            ->with('update', false);
    }
    public function edit(Request $request)
    {
        if ($this->verificaPermissaoConsulta('minuta') === false){
            return redirect()->to('/empenho/minuta/ano/'.date('Y'));
        }

        $minuta_id      = Route::current()->parameter('minuta_id');
        $nova_minuta_id = Route::current()->parameter('nova_minuta_id');
        $modMinuta      = MinutaEmpenho::find($minuta_id);
        $unidade_id     = session('user_ug_id');

        DB::beginTransaction();
        try {
            $cime = CompraItemMinutaEmpenho::where('minutaempenho_id', $minuta_id)->first();
            CompraItemMinutaEmpenho::where('minutaempenho_id', $nova_minuta_id)
                ->update([
                    'compra_item_fornecedor_id' => $cime->compra_item_fornecedor_id,
                    'compra_item_unidade_id' => $cime->compra_item_unidade_id
                ]);
            DB::commit();
        }catch (Exception $e) {
            DB::rollBack();
        }


        if (!empty(session('unidade_ajax_id'))) {
            $unidade_id = session('unidade_ajax_id');
        }

        $modUnidade = Unidade::find($unidade_id);

        $html = $this->retornaGrid($unidade_id);
        $form = $this->retonaFormModal($modUnidade->id, $minuta_id);

        return view('backpack::mod.empenho.AlteracaoSaldoContabil', compact(['html', 'form']))
            ->with('minuta_id', $modMinuta->id)
            ->with('nova_minuta_id', $nova_minuta_id)
            ->with('fornecedor_id', $modUnidade->fornecedor_compra_id)
            ->with('unidades', $this->buscaUg())
            ->with('modUnidade', $modUnidade)
            ->with('url', route('empenho.crud.alteracaoFonte.update', [
                'minuta_id'=> $minuta_id,
                'nova_minuta_id'=> $nova_minuta_id
            ]))
            ->with('update', true);
    }

    public function retornaSaldosComMascara($saldos)
    {
        return array_map(function ($saldos) {
            if ($saldos['saldo']) {
                $saldos['saldo'] = str_replace(',', '.', $saldos['saldo']);
                $saldos['saldo'] = str_replace_last('.', ',', $saldos['saldo']);
            }
            return $saldos;
        }, $saldos);
    }


    public function consultaApiSta($ano, $ug, $gestao, $contacontabil)
    {
        $apiSta = new ConsultaApiSta();
        return $apiSta->saldocontabilAnoUgGestaoContacontabil($ano, $ug, $gestao, $contacontabil);
    }

    public function store(Request $request)
    {
        if ($this->verificaPermissaoConsulta('minuta') === false){
            return redirect()->to('/empenho/minuta/ano/'.date('Y'));
        }

        $minuta_id = Route::current()->parameter('minuta_id');
        $saldo_contabil_id = $request->get('saldo');

        $minuta = MinutaEmpenho::find($minuta_id);
        DB::beginTransaction();
        try {

            //Criação da Nova Minuta
            $novaMinuta = $minuta->replicate();
            $novaMinuta->saldo_contabil_id = $saldo_contabil_id;

            $novaMinuta->situacao_id = $this->retornaIdCodigoItem('Situações Minuta Empenho', 'AGUARDANDO ANULAÇÃO');
            $novaMinuta->informacao_complementar .= " - Minuta Anulada: $minuta->mensagem_siafi";
            $novaMinuta->mensagem_siafi = null;
            $novaMinuta->save();
            //Colocando aqui a situacao em processamento para ficar igual acontece a minuta original
            //A última situação da remessa 0 é em processamento
            $situacao_processamento = $this->retornaIdCodigoItem(
                'Situações Minuta Empenho',
                'EM PROCESSAMENTO'
            );

            //Criação da nova remessa
            $novaRemessa = MinutaEmpenhoRemessa::create([
                'minutaempenho_id' => $novaMinuta->id,
                'situacao_id' => $situacao_processamento,
                'remessa' => 0,
            ]);

            $base = new Base();
            $novaRemessa->sfnonce = $base->geraNonceSiafiEmpenho($novaMinuta->id, $novaRemessa->id);
            $novaRemessa->save();

            $situacao_andamento = $this->retornaIdCodigoItem(
                'Situações Minuta Empenho',
                'EM ANDAMENTO'
            );

            //Criação da nova remessa de Alteração
            $novaRemessaAlteracao = MinutaEmpenhoRemessa::create([
                'minutaempenho_id' => $minuta_id,
                'situacao_id' => $situacao_andamento,
                'etapa' => 2,
                'alteracao_fonte_minutaempenho_id' => $novaMinuta->id,
                'remessa' => $minuta->max_numero_remessa + 1
            ]);

            $novaRemessaAlteracao->sfnonce = $base->geraNonceSiafiEmpenho($minuta_id, $novaRemessaAlteracao->id);
            $novaRemessaAlteracao->save();
            DB::commit();
        } catch (Exception $exc) {
            DB::rollback();
        }
        return redirect()->route('empenho.crud.alteracaoFonte.subelemento', [
            'minuta_id' => $minuta_id
            , 'nova_minuta_id' => $novaMinuta->id
        ]);
    }

    public function update(Request $request)
    {
        if ($this->verificaPermissaoConsulta('minuta') === false){
            return redirect()->to('/empenho/minuta/ano/'.date('Y'));
        }

        $minuta_id         = Route::current()->parameter('minuta_id');
        $nova_minuta_id    = Route::current()->parameter('alteracaoFonte');
        $saldo_contabil_id = $request->get('saldo');

        DB::beginTransaction();
        try {
            $novaMinuta = MinutaEmpenho::find($nova_minuta_id);
            $novaMinuta->saldo_contabil_id = $saldo_contabil_id;
            $novaMinuta->save();

            $minuta = MinutaEmpenho::find($minuta_id);

            $rota = 'empenho.crud.alteracaoFonte.subelemento';
            if ($minuta->max_etapa_remessa !== 2) {
                $rota = 'empenho.crud.alteracaoFonte.subelemento.edit';
            }

            DB::commit();
        } catch (Exception $exc) {
            DB::rollback();
        }

        return redirect()->route($rota, [
            'minuta_id' => $minuta_id
            , 'nova_minuta_id' => $novaMinuta->id
        ]);
    }

    public function atualizaMinuta(Request $request)
    {
        if (!$request->get('saldo')) {
            Alert::error('Selecione o Saldo Contábil.')->flash();
            return redirect()->back();
        }

        $minuta_id = $request->get('minuta_id');
        $saldo_contabil_id = $request->get('saldo');

        $modMinuta = MinutaEmpenho::find($minuta_id);
        $modMinuta->etapa = 5;
        $modMinuta->saldo_contabil_id = $saldo_contabil_id;
        $modMinuta->save();

        return redirect()->route('empenho.minuta.etapa.subelemento', ['minuta_id' => $minuta_id]);
    }

    public function inserirCelulaOrcamentaria(Request $request)
    {

        $conta_corrente = $this->retornaContaCorrente($request);
        $saldo = $request->get('valor');
        $unidade_id = $request->get('unidade_id');
        $ano = config('app.ano_minuta_empenho');
        $contacontabil = config('app.conta_contabil_credito_disponivel');
        $modSaldo = new SaldoContabil();
        $modSaldo->unidade_id = $unidade_id;
        $modSaldo->ano = $ano;
        $modSaldo->conta_contabil = $contacontabil;
        $modSaldo->conta_corrente = $conta_corrente;
        $modSaldo->saldo = $this->retornaFormatoAmericano($saldo);
        $modSaldo->save();

        return redirect()->route(
            'empenho.minuta.etapa.saldocontabil',
            [
                'minuta_id' => $request->get('minuta_id')
            ]
        );
    }

    public function retornaContaCorrente(Request $request)
    {
        $conta_corrente = '';
        $conta_corrente .= $request->get('esfera');
        $conta_corrente .= $request->get('ptrs');
        $conta_corrente .= $request->get('fonte');
        $conta_corrente .= $request->get('natureza_despesa');
        $conta_corrente .= (!empty($request->get('ugr'))) ? $request->get('ugr') : '        ';
        $conta_corrente .= $request->get('plano_interno');

        return $conta_corrente;
    }

    /**
     * Monta $html com definições do Grid
     *
     * @return Builder
     */
    private function retornaGrid($unidade_id)
    {

        $html = $this->htmlBuilder
            ->addColumn([
                'data' => 'btn_selecionar',
                'name' => 'btn_selecionar',
                'title' => 'Selecione'
            ])
            ->addColumn([
                'data' => 'esfera',
                'name' => 'esfera',
                'title' => 'Esfera',
                'class' => 'text-center'
            ])
            ->addColumn([
                'data' => 'ptrs',
                'name' => 'ptrs',
                'title' => 'PTRS'
            ])
            ->addColumn([
                'data' => 'fonte',
                'name' => 'fonte',
                'title' => 'Fonte'
            ])
            ->addColumn([
                'data' => 'nd',
                'name' => 'nd',
                'title' => 'Natureza da Despesa'
            ])
            ->addColumn([
                'data' => 'ugr',
                'name' => 'ugr',
                'title' => 'UGR'
            ])
            ->addColumn([
                'data' => 'plano_interno',
                'name' => 'plano_interno',
                'title' => 'Plano Interno'
            ])
            ->addColumn([
                'data' => 'saldo',
                'name' => 'saldo',
                'title' => 'Valor'
            ])
            ->addColumn([
                'data' => 'action',
                'name' => 'action',
                'title' => 'Ações',
                'orderable' => false,
                'searchable' => false
            ])
            ->ajax([
                'url' => route('empenho.crud.alteracao.fonte'),
                'type' => 'GET',
                /*'data' => 'function(d) { d.key = "value"; }',*/
            ])
            ->parameters([
                'processing' => true,
                'serverSide' => true,
                'responsive' => true,
                'info' => true,
                'order' => [
                    7,
                    'desc'
                ],
                'autoWidth' => false,
                'bAutoWidth' => false,
                'paging' => true,
                'lengthChange' => true,
                'language' => [
                    'url' => asset('/json/pt_br.json')
                ]
            ]);

        return $html;
    }


    private function retornaBtnAtualizar($id)
    {
        $btnUpdate = '';
        $btnUpdate .= '<button type="button" class="btn btn-primary btn-sm" id=' . $id
            . ' name=atualiza_saldo_acao_' . $id . ">";
        $btnUpdate .= '<i class="fa fa-refresh"></i></button>';

        return $btnUpdate;
    }


    private function retornaBtSelecionar($id)
    {
        $btn = '';
        $btn .= "<input type='radio' class='custom-control-input' id=saldo_" . $id . " name='saldo' value=" . $id . ">";
        return $btn;
    }


    private function retornaAcoes($id, $minuta_id)
    {
        $selecionar = $this->retornaBtnAtualizar($id, $minuta_id);
        $botoes = $selecionar;

        $acoes = '';
        $acoes .= '<div class="btn-group">';
        $acoes .= $botoes;
        $acoes .= '</div>';

        return $acoes;
    }

    public function retonaFormModal($unidade_id, $minuta_id)
    {
        return FormBuilder::create(InserirCelulaOrcamentariaForm::class, [

            'id' => 'form_modal'

        ])->add('unidade_id', 'hidden', [
            'value' => $unidade_id,
            'attr' => [
                'id' => 'unidade_id'
            ]
        ])->add('minuta_id', 'hidden', [
            'value' => $minuta_id,
            'attr' => [
                'id' => 'minuta_id'
            ]
        ]);
    }

    public function retornaAjaxGrid()
    {
        $unidade_id = session('user_ug_id');

        if (!empty(session('unidade_ajax_id'))) {
            $unidade_id = session('unidade_ajax_id');
        }

        $saldosContabeis = SaldoContabil::retornaSaldos($unidade_id);
        $saldos = $this->retornaSaldosComMascara($saldosContabeis);

        return DataTables::of($saldos)
            ->addColumn(
                'action',
                function ($saldos) {
                    return $this->retornaBtnAtualizar($saldos['id']);
                }
            )
            ->addColumn(
                'btn_selecionar',
                function ($saldos) {
                    return $this->retornaBtSelecionar($saldos['id']);
                }
            )
            ->rawColumns(['action', 'btn_selecionar'])
            ->make(true);
    }

    private function retornaSomaItens($tipo, $operacao_id, $minutaempenho_id, $minutaempenhos_remessa_id, $class)
    {
        $class = $tipo.ItemMinutaEmpenho::class;
        $item = strtolower($tipo) . '_item_id';

        return $class::select(
            $item,
            'minutaempenho_id',
            'subelemento_id',
            DB::raw("SUM({$class::getTableName()}.quantidade) AS quantidade"),
            DB::raw("SUM({$class::getTableName()}.valor)      AS valor"),
            DB::raw($operacao_id . ' as operacao_id'),
            DB::raw($minutaempenhos_remessa_id . ' as minutaempenhos_remessa_id'),
            'numseq'
        )
            ->where('minutaempenho_id', $minutaempenho_id)
            ->groupBy($item, 'subelemento_id', 'minutaempenho_id', 'numseq')
            ->orderBy('numseq', 'asc')
            ->get();
    }

    private function setAnulacao(String $empenho_por, $anulacao, $minuta_id, $remessa_id)
    {
        $functions = [
            'Compra' => 'setAnulacaoMinutaCompra',
            'Contrato' => 'setAnulacaoMinutaContrato',
            'Suprimento' => 'setAnulacaoMinutaCompra'
        ];

        $arrTipos = [
            'Compra'     => 'Compra',
            'Contrato'   => 'Contrato',
            'Suprimento' => 'Compra'
        ];

        $tipo = $arrTipos[$empenho_por];

        $class = $tipo.ItemMinutaEmpenho::class;

        $itens = $this->retornaSomaItens('Compra', $anulacao, $minuta_id, $remessa_id, $class);

        $class::insert($itens->toArray());

        //TODO SALVAR CompraItemUnidade QUANDO FOR COMPRA

        //TODO VERIFICAR SE É COMPRA OU CONTRATO

        //TODO SALVAR NONCE
    }

    public function setAnulacaoMinutaCompra($empenho_por, $anulacao, $minuta_id, $remessa_id)
    {
        $itens = $this->retornaSomaItens($empenho_por, $anulacao, $minuta_id, $remessa_id);
        CompraItemMinutaEmpenho::insert($itens->toArray());
    }

    public function setAnulacaoMinutaContrato()
    {
    }

    /**
     * Método para gravar a tela 2 da Alteracao de fonte
     *
     * @param UpdateRequest $request
     * @return RedirectResponse|void
     */
    public function gravarSubelemento(StoreRequest $request)
    {

        foreach ($request->valor_total as $key => $value) {
            if ((int)$request->qtd[$key] > "0.00000" || $request->valor_total[$key] > "0,00") {
                if (substr($request->tipo_alteracao[$key], stripos($request->tipo_alteracao[$key], '|') + 1) == "NENHUMA") {
                    \Alert::error('Deve ser escolhido um Tipo de Operação de Reforço ou Anulação nos itens alterados.')->flash();
                    return redirect()->back()->withInput();
                }
            }
        }

        $minuta_id       = $request->get('minuta_id');
        $modMinuta       = MinutaEmpenho::find($minuta_id);
        $tipo            = $modMinuta->empenho_por;
        $valores         = $request->valor_total;
        $nova_minuta_id  = $request->get('nova_minuta_id');
        $nova_remessa_id = $nova_minuta_id != null ? MinutaEmpenhoRemessa::where('minutaempenho_id', $nova_minuta_id)
            ->select('id')->first()->id : null;
        $anulacao_remessa_id = $modMinuta->max_remessa;
        $anulacaoRemessa = MinutaEmpenhoRemessa::find($anulacao_remessa_id);
        $etapa           = 3;
        $tipoAlteracao   = 'alteracao';

        DB::beginTransaction();
        try {
            if ($tipo === 'Compra' || $tipo === 'Suprimento') {
                $atualizarFilhos = $modMinuta->compraItemMinutaEmpenho[0]->compra_item_fornecedor_id;
                array_walk($valores, function (&$value, $key) use ($request, $anulacao_remessa_id) {

                    $operacao = explode('|', $request->tipo_alteracao[$key]);
                    $quantidade = $request->qtd[$key];
                    $valor = $this->retornaFormatoAmericano($request->valor_total[$key]);

                    $valor = self::arredondarValor($valor);

                    if ($operacao[1] === 'ANULAÇÃO') {
                        $quantidade = 0 - $quantidade;
                        $valor = 0 - $valor;
                    }

                    $value = [
                        'compra_item_id' => $request->compra_item_id[$key],
                        'minutaempenho_id' => $request->minuta_id,
                        'subelemento_id' => $request->subitem[$key],
                        'operacao_id' => $operacao[0],
                        'minutaempenhos_remessa_id' => $anulacao_remessa_id,
                        'quantidade' => $quantidade,
                        'valor' => $valor,
                        'numseq' => $request->numseq[$key],
                        'compra_item_fornecedor_id' => $request->cif_id[$key],
                        'compra_item_unidade_id' => $request->ciu_id[$key]
                    ];
                });

                CompraItemMinutaEmpenho::insert($valores);
                $this->setItensAlteracaoFonte($valores, $request, $nova_remessa_id, 'Compra');
                $tipoAlteracao = 'alteracaoFonte';

                if ($tipo === 'Compra') {
                    foreach ($valores as $index => $valor) {
                        $compraItemUnidade = CompraItemUnidade::where(
                            'compra_item_unidade.compra_item_id',
                            $valor['compra_item_id']
                        )
                            ->where('compra_item_unidade.unidade_id', session('user_ug_id'))
                            ->whereRaw("
                            CASE WHEN compra_item_unidade.tipo_uasg = 'C'
                                    or tipo_compra.descricao = 'SISPP'
                                THEN compra_item_unidade.fornecedor_id = '". $modMinuta->fornecedor_compra_id ."'
                                ELSE compra_item_unidade.fornecedor_id IS NULL
                            END")
                            ->join('compra_items', 'compra_items.id', '=', 'compra_item_unidade.compra_item_id')
                            ->join('compras', 'compras.id', '=', 'compra_items.compra_id')
                            ->join('codigoitens as tipo_compra', 'tipo_compra.id', '=', 'compras.tipo_compra_id')
                            ->select('compra_item_unidade.*')
                            ->first();
                        $compraItemUnidade->quantidade_saldo =
                            $this->retornaSaldoAtualizado(
                                $valor['compra_item_id'],
                                null,
                                $compraItemUnidade->id
                            )->saldo;
                        $compraItemUnidade->save();
                    }
                }

                if($atualizarFilhos === null){
                    CompraItemMinutaEmpenho::where('compra_item_id', $request->compra_item_id[$key])
                        ->where('minutaempenho_id', $request->minuta_id)
                        ->update([
                            'compra_item_fornecedor_id' => $request->cif_id[$key],
                            'compra_item_unidade_id' => $request->ciu_id[$key]
                        ]);
                }
            }
            if ($tipo === 'Contrato') {
                array_walk($valores, function (&$value, $key) use ($request, $anulacao_remessa_id) {

                    $operacao   = explode('|', $request->tipo_alteracao[$key]);
                    $quantidade = $request->qtd[$key];
                    $valor      = $this->retornaFormatoAmericano($request->valor_total[$key]);

                    $valor = self::arredondarValor($valor);

                    if ($operacao[1] === 'ANULAÇÃO') {
                        $quantidade = 0 - $quantidade;
                        $valor = 0 - $valor;
                    }

                    $value = [
                        'contrato_item_id'          => $request->contrato_item_id[$key],
                        'minutaempenho_id'          => $request->minuta_id,
                        'subelemento_id'            => $request->subitem[$key],
                        'operacao_id'               => $operacao[0],
                        'minutaempenhos_remessa_id' => $anulacao_remessa_id,
                        'quantidade'                => $quantidade,
                        'valor'                     => $valor,
                        'numseq'                    => $request->numseq[$key],
                    ];
                });

                ContratoItemMinutaEmpenho::insert($valores);

                $this->setItensAlteracaoFonte($valores, $request, $nova_remessa_id, 'Contrato');
                $tipoAlteracao = 'alteracaoFonte';
            }

            $anulacaoRemessa->etapa = $etapa;
            $anulacaoRemessa->save();

            DB::commit();
            return Redirect::to("empenho/minuta/{$minuta_id}/{$tipoAlteracao}/{$anulacao_remessa_id}/show/{$minuta_id}");
        } catch (Exception $e) {
            Log::error(__METHOD__ . ' (' . $e->getLine() . ') - ' . $e->getMessage());
            DB::rollback();
            \Alert::error('Ocorreu um erro ao tentar salvar a alteração.')->flash();
            return redirect()->back()->withInput();
        }
    }

    /**
     * @param UpdateRequest $request
     * @return mixed
     */
    public function atualizarSubelemento(UpdateRequest $request)
    {
        foreach ($request->valor_total as $key => $value) {
            if ((int)$request->qtd[$key] > "0.00000" || $request->valor_total[$key] > "0,00") {
                if (substr($request->tipo_alteracao[$key], stripos($request->tipo_alteracao[$key], '|') + 1) == "NENHUMA") {
                    \Alert::error('Deve ser escolhido um Tipo de Operação de Reforço ou Anulação nos itens alterados.')->flash();
                    return redirect()->back()->withInput();
                }
            }
        }

        $remessa_id      = Route::current()->parameter('alteracao');

        $minuta_id       = $request->get('minuta_id');
        $modMinuta       = MinutaEmpenho::find($minuta_id);
        $tipo            = $modMinuta->empenho_por;
        $valores         = $request->valor_total;
        $nova_minuta_id  = $request->get('nova_minuta_id');
        $nova_remessa_id = MinutaEmpenhoRemessa::where('minutaempenho_id', $nova_minuta_id)->select('id')->first()->id;
        $anulacao_remessa_id = $modMinuta->max_remessa;
        $anulacaoRemessa = MinutaEmpenhoRemessa::find($anulacao_remessa_id);

        $etapa           = 3;
        $tipoAlteracao   = 'alteracao';

        DB::beginTransaction();
        try {
            $inclusao_id = $this->retornaIdCodigoItem(
                'Operação item empenho',
                'INCLUSAO'
            );
            $valor_total = 0;

            if ($tipo === 'Compra' || $tipo === 'Suprimento') {
                $atualizarFilhos = $modMinuta->compraItemMinutaEmpenho[0]->compra_item_fornecedor_id;
                foreach ($valores as $key => $value) {
                    $operacao = explode('|', $request->tipo_alteracao[$key]);
                    $quantidade = $request->qtd[$key];
                    $valor = $this->retornaFormatoAmericano($request->valor_total[$key]);
                    $valor_total += $valor;

                    $valor = self::arredondarValor($valor);

                    switch ($operacao[1]) {
                        case 'NENHUMA':
                            $quantidade = 0;
                            $valor = 0;
                            break;
                        case 'ANULAÇÃO':
                            $quantidade = 0 - $quantidade;
                            $valor = 0 - $valor;
                            break;
                    }

                    CompraItemMinutaEmpenho::where('compra_item_id', $request->compra_item_id[$key])
                        ->where('minutaempenho_id', $request->minuta_id)
                        ->where('minutaempenhos_remessa_id', $anulacao_remessa_id)
                        ->update([
                            'subelemento_id' => $request->subitem[$key],
                            'operacao_id'    => $operacao[0],
                            'quantidade'     => $quantidade,
                            'valor'          => $valor,
                        ]);

                    if ($nova_minuta_id !== null) {
                        $tipoAlteracao = 'alteracaoFonte';

                        if ($operacao[1] === 'ANULAÇÃO') {
                            $quantidade = 0 - $quantidade;
                            $valor = 0 - $valor;
                        }

                        CompraItemMinutaEmpenho::where('compra_item_id', $request->compra_item_id[$key])
                            ->where('minutaempenho_id', $nova_minuta_id)
                            ->where('minutaempenhos_remessa_id', $nova_remessa_id)
                            ->update([
                                'subelemento_id' => $request->subitem[$key],
                                'operacao_id'    => $inclusao_id,
                                'quantidade'    => $quantidade,
                                'valor'         => $valor,
                            ]);

                        $novaMinuta = MinutaEmpenho::find($nova_minuta_id);
                        $novaMinuta->valor_total = abs($valor_total);
                        $novaMinuta->save();
                    }
                    if ($tipo === 'Compra') {
                        $compraItemUnidade = CompraItemUnidade::where(
                            'compra_item_unidade.compra_item_id',
                            $request->compra_item_id[$key]
                        )
                            ->where('compra_item_unidade.unidade_id', session('user_ug_id'))
                            ->whereRaw("
                            CASE WHEN compra_item_unidade.tipo_uasg = 'C'
                                    or tipo_compra.descricao = 'SISPP'
                                THEN compra_item_unidade.fornecedor_id = '". $modMinuta->fornecedor_compra_id ."'
                                ELSE compra_item_unidade.fornecedor_id IS NULL
                            END")
                            ->join('compra_items', 'compra_items.id', '=', 'compra_item_unidade.compra_item_id')
                            ->join('compras', 'compras.id', '=', 'compra_items.compra_id')
                            ->join('codigoitens as tipo_compra', 'tipo_compra.id', '=', 'compras.tipo_compra_id')
                            ->select('compra_item_unidade.*')
                            ->first();
                        $saldo = $this->retornaSaldoAtualizado(
                            $request->compra_item_id[$key],
                            null,
                            $compraItemUnidade->id
                        );
                        $compraItemUnidade->quantidade_saldo = $saldo->saldo;
                        $compraItemUnidade->save();
                    }

                    if($atualizarFilhos === null){
                        CompraItemMinutaEmpenho::where('compra_item_id', $request->compra_item_id[$key])
                            ->where('minutaempenho_id', $request->minuta_id)
                            ->update([
                                'compra_item_fornecedor_id' => $request->cif_id[$key],
                                'compra_item_unidade_id' => $request->ciu_id[$key]
                            ]);
                    }
                }
            }

            if ($tipo === 'Contrato') {
                foreach ($valores as $key => $value) {
                    $operacao = explode('|', $request->tipo_alteracao[$key]);
                    $quantidade = $request->qtd[$key];
                    $valor = $this->retornaFormatoAmericano($request->valor_total[$key]);
                    $valor_total   += $valor;

                    $valor = self::arredondarValor($valor);

                    switch ($operacao[1]) {
                        case 'NENHUMA':
                            $quantidade = 0;
                            $valor      = 0;
                            break;
                        case 'ANULAÇÃO':
                            $quantidade = 0 - $quantidade;
                            $valor      = 0 - $valor;
                            break;
                    }

                    ContratoItemMinutaEmpenho::where('contrato_item_id', $request->contrato_item_id[$key])
                        ->where('minutaempenho_id', $request->minuta_id)
                        ->where('minutaempenhos_remessa_id', $anulacao_remessa_id)
                        ->update([
                            'subelemento_id' => $request->subitem[$key],
                            'operacao_id'    => $operacao[0],
                            'quantidade'     => $quantidade,
                            'valor'          => $valor,

                        ]);

                    if ($nova_minuta_id !== null) {
                        $tipoAlteracao = 'alteracaoFonte';
                        if ($operacao[1] === 'ANULAÇÃO') {
                            $quantidade = 0 - $quantidade;
                            $valor      = 0 - $valor;
                        }

                        ContratoItemMinutaEmpenho::where('contrato_item_id', $request->contrato_item_id[$key])
                            ->where('minutaempenho_id', $nova_minuta_id)
                            ->where('minutaempenhos_remessa_id', $nova_remessa_id)
                            ->update([
                                'subelemento_id' => $request->subitem[$key],
                                'operacao_id'    => $inclusao_id,
                                'quantidade'     => $quantidade,
                                'valor'          => $valor,
                            ]);

                        $novaMinuta = MinutaEmpenho::find($nova_minuta_id);
                        $novaMinuta->valor_total = abs($valor_total);
                        $novaMinuta->save();
                    }
                }
            }

            $anulacaoRemessa->etapa = $etapa;
            $anulacaoRemessa->save();

            DB::commit();
            return Redirect::to("empenho/minuta/{$minuta_id}/{$tipoAlteracao}/{$anulacao_remessa_id}/show/{$minuta_id}");
        } catch (Exception $exc) {
            DB::rollback();
        }
    }

    /**
     * @param $valores
     * @param $request
     * @param $nova_remessa_id
     * @return mixed
     */
    public function setItensAlteracaoFonte($valores, $request, $nova_remessa_id, string $tipo)
    {
        $class = "App\Models\\"."{$tipo}ItemMinutaEmpenho";
        $item  = strtolower($tipo) . '_item_id';

        $inclusao_id = $this->retornaIdCodigoItem(
            'Operação item empenho',
            'INCLUSAO'
        );

        $valor_total = 0;

        array_walk(
            $valores,
            function (&$value, $key) use ($request, $nova_remessa_id, $item, $inclusao_id, &$valor_total) {

                $operacao      = explode('|', $request->tipo_alteracao[$key]);
                $quantidade    = $value['quantidade'];
                $valor         = $value['valor'];
                $valor_total   += $value['valor'];

                if ($operacao[1] === 'ANULAÇÃO') {
                    $quantidade = 0 - $quantidade;
                    $valor      = 0 - $valor;
                }

                $value = [
                    $item                       => $request->{$item}[$key],
                    'minutaempenho_id'          => $request->nova_minuta_id,
                    'subelemento_id'            => $request->subitemNovo[$key],
                    'operacao_id'               => $inclusao_id,
                    'minutaempenhos_remessa_id' => $nova_remessa_id,
                    'quantidade'                => $quantidade,
                    'valor'                     => $valor,
                    'numseq'                    => $request->numseq[$key],
                    'compra_item_fornecedor_id' => $request->cif_id[$key],
                    'compra_item_unidade_id'    => $request->ciu_id[$key]
                ];
            }
        );

        $class::insert($valores);
        $novaMinuta = MinutaEmpenho::find($request->get('nova_minuta_id'));
        $novaMinuta->valor_total = abs($valor_total);
        $novaMinuta->save();
    }

    /**
     * Método criado para receber um valor e fazer o devido arredondamento do valor recebido.
     */
    public function arredondarValor($valorArredondar)
    {
        return $valorArredondado = round($valorArredondar, 2);
    }
}
