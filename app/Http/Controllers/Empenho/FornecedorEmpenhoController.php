<?php

namespace App\Http\Controllers\Empenho;

use Alert;
use App\Http\Controllers\Empenho\Minuta\BaseControllerEmpenho;
use App\Http\Controllers\CNBS;
use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\CompraTrait;
use App\Http\Traits\PermissaoTrait;
use App\Models\Catmatseritem;
use App\Models\Codigoitem;
use App\Models\Compra;
use App\Models\CompraItem;
use App\Models\CompraItemFornecedor;
use App\Models\CompraItemMinutaEmpenho;
use App\Models\CompraItemUnidade;
use App\Models\Contrato;
use App\Models\ContratoItemMinutaEmpenho;
use App\Models\Fornecedor;
use App\Models\MinutaEmpenho;
use App\Models\MinutaEmpenhoRemessa;
use App\Repositories\Base;
use App\services\AmparoLegalService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Route;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

/**
 * Controller com métodos e funções da Tela 2 e 3
 */
class FornecedorEmpenhoController extends BaseControllerEmpenho
{
    use CompraTrait;
    use BuscaCodigoItens;
    use PermissaoTrait;

    /**
     * Tela 2
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     * @throws Exception
     */
    public function index(Request $request)
    {
        if ($this->verificaPermissaoConsulta('minuta') === false){
            return redirect()->to('/empenho/minuta/ano/'.date('Y'));
        }

        $minuta_id = Route::current()->parameter('minuta_id');
        $uasg_inativa = Route::current()->parameter('uasg_inativa');

        $modMinutaEmpenho = MinutaEmpenho::find($minuta_id);
        $codigoitem = Codigoitem::find($modMinutaEmpenho->tipo_empenhopor_id);


        if ($codigoitem->descricao === 'Contrato') {
            //$fornecedores = $modMinutaEmpenho->contrato()->first()->fornecedor;
            $fornecedores = MinutaEmpenho::join(
                'contratos',
                'contratos.id',
                '=',
                'minutaempenhos.contrato_id'
            )
                ->join('fornecedores', 'fornecedores.id', '=', 'contratos.fornecedor_id')
                ->where('minutaempenhos.id', $minuta_id)
                ->select([
                    'fornecedores.id',
                    'fornecedores.nome',
                    'fornecedores.cpf_cnpj_idgener',
                    DB::raw('1 AS situacao_sicaf')
                ])
                ->get()
                ->toArray();
//            ;dd($fornecedores->getBindings(),$fornecedores->toSql());
        }
        if ($codigoitem->descricao == 'Compra') {
            $sisrpId = $this->retornaIdCodigoItem('Tipo Compra', 'SISRP');
            $sisppId = $this->retornaIdCodigoItem('Tipo Compra', 'SISPP');
            $fornecedores = MinutaEmpenho::join('compras', 'compras.id', '=', 'minutaempenhos.compra_id')
                ->join('compra_items', 'compra_items.compra_id', '=', 'compras.id')
                ->join('compra_item_unidade', 'compra_item_unidade.compra_item_id', '=', 'compra_items.id')
                ->join('unidades', 'unidades.id', '=', 'compra_item_unidade.unidade_id')
                ->join('compra_item_fornecedor', 'compra_item_fornecedor.compra_item_id', '=', 'compra_items.id')
                ->join('fornecedores', 'fornecedores.id', '=', 'compra_item_fornecedor.fornecedor_id')
                ->distinct()
                ->where('minutaempenhos.id', $minuta_id)
                /**
                 * Regras das vigências:
                 * caso o compra_items.ata_vigencia_fim seja null,
                 *      permitir compras SISPP
                 *      OU permitir compras SISRP
                 *          E LEI 14133 COM a vigencia sendo observada na tabela compra_item_fornecedor
                 *
                 * caso o compra_items.ata_vigencia_fim não seja nulo e não seja compras SISRP E LEI 14133,
                 * a vigência deve ser observada  na tabela  compra_items
                 */
                ->where(function ($query) use ($sisrpId, $sisppId) {
                    $query->where(function ($query) use ($sisrpId, $sisppId) {
                        $query->whereNull('compra_items.ata_vigencia_fim')
                            ->where('tipo_compra_id', $sisppId)
                            ->orWhere(function ($query) use ($sisrpId) {
                                $query->where('tipo_compra_id', $sisrpId)
                                    ->where('lei', 'LEI14133')
                                    ->where(
                                        'compra_item_fornecedor.ata_vigencia_inicio',
                                        '<=',
                                        Carbon::now()->toDateString()
                                    )
                                    ->where(
                                        'compra_item_fornecedor.ata_vigencia_fim',
                                        '>=',
                                        Carbon::now()->toDateString()
                                    );
                            });
                    })
                        ->orWhere(function ($query) use ($sisrpId) {
                            $query->where('compra_items.ata_vigencia_fim', '>=', Carbon::now()->toDateString())
                                ->where(function ($query) use ($sisrpId) {
                                    $query->where('tipo_compra_id', '<>', $sisrpId)
                                        ->orWhere('lei', '<>', 'LEI14133');
                                });
                        });
                })
                ->where('compra_item_unidade.quantidade_saldo', '>', 0)
//                ->where('compra_items.situacao','=',true)
//                ->where('compra_item_fornecedor.situacao','=',true)
//                ->where('compra_item_unidade.situacao','=',true)
                ->where('compra_items.situacao', '=', true)
                ->where('compra_item_fornecedor.situacao', '=', true)
                ->where('compra_item_unidade.situacao', '=', true)
                ->orderBy('fornecedores.nome', 'desc')
                ->select([
                    'fornecedores.id', 'fornecedores.nome',
                    'fornecedores.cpf_cnpj_idgener',
                    'compra_item_fornecedor.situacao_sicaf'
                ])
                ->get()
                ->toArray();
        }
        if ($codigoitem->descricao == 'Suprimento') {
            $fornecedores = Fornecedor::whereIn('tipo_fornecedor', ['FISICA', 'UG'])
                ->select([
                    'fornecedores.id',
                    'fornecedores.nome',
                    'fornecedores.cpf_cnpj_idgener',
                    DB::raw('1 AS situacao_sicaf')
                ])
                ->get()
                ->toArray();
        }


        if ($request->ajax()) {
            return DataTables::of($fornecedores)->addColumn('action', function ($fornecedores) use ($minuta_id) {
                return $this->retornaAcoes($fornecedores['id'], $minuta_id, $fornecedores['situacao_sicaf']);
            })->addColumn('icone', function ($fornecedores) use ($minuta_id) {
                return '<i class="fa fa-' . ($fornecedores['situacao_sicaf'] != 1 ? 'times' : 'check') . '"></i>';
            })->rawColumns(['icone', 'action'])
                ->make(true);
        }

        $html = $this->retornaGrid();

        if (isset($uasg_inativa)) {
            return view('backpack::mod.empenho.Etapa2Fornecedor', compact('html'))->with('uasg_inativa', $uasg_inativa)->with('sem_fornecedores', empty($fornecedores));
        }
        return view('backpack::mod.empenho.Etapa2Fornecedor', compact('html'))->with('sem_fornecedores', empty($fornecedores));
    }

    /**
     * Retorna html das ações disponíveis
     *
     * @param number $id
     * @return string
     */
    private function retornaAcoes($id, $minuta_id, $situacao_sicaf)
    {
        $acoes = '';
        $acoes .= '<a href="' . route('empenho.minuta.etapa.item', ['minuta_id' => $minuta_id, 'fornecedor_id' => $id]);
        $acoes .= '"Selecionar ';
        $acoes .= "class='btn btn-default btn-sm' ";
        $acoes .= 'title="Selecionar este fornecedor">';
        $acoes .= '<i class="fa fa-check-circle"></i></a>';
        $sem_acao = '<i class="glyphicon glyphicon-ban-circle"></i>';

        //$acoes = ($situacao_sicaf != 1) ? $sem_acao : $acoes;

        return $acoes;
    }

    /**
     * Monta $html com definições do Grid
     *
     * @return Builder
     */
    private function retornaGrid()
    {

        $html = $this->htmlBuilder
            ->addColumn([
                'data' => 'action',
                'name' => 'action',
                'title' => 'Ações',
                'orderable' => false,
                'searchable' => false
            ])
            ->addColumn([
                'data' => 'cpf_cnpj_idgener',
                'name' => 'cpf_cnpj_idgener',
                'title' => 'CNPJ - CPF - Número',
            ])
            ->addColumn([
                'data' => 'nome',
                'name' => 'nome',
                'title' => 'Fornecedor'
            ])
            ->addColumn([
                'data' => 'icone',
                'name' => 'icone',
                'title' => 'Situação SICAF',
                'orderable' => false,
                'searchable' => false
            ])
            ->parameters([
                'processing' => true,
                'serverSide' => true,
                'responsive' => true,
                'info' => true,
                'order' => [
                    0,
                    'desc'
                ],
                'searchDelay' => 3000,
                'autoWidth' => false,
                'bAutoWidth' => false,
                'paging' => true,
                'lengthChange' => true,
                'language' => [
                    'url' => asset('/json/pt_br.json')
                ]
            ]);

        return $html;
    }

    //TELA 3
    public function item(Request $request)
    {
        if ($this->verificaPermissaoConsulta('minuta') === false){
            return redirect()->to('/empenho/minuta/ano/'.date('Y'));
        }

        $minuta_id = Route::current()->parameter('minuta_id');
        $modMinutaEmpenho = MinutaEmpenho::find($minuta_id);
        $acoes = '<input type="checkbox" name="selectAll" id="selectAll" > Ações';
        $classificacao = null;
        $fornecedor_id = Route::current()->parameter('fornecedor_id');
        if (!is_null($modMinutaEmpenho)) {
            $modMinutaEmpenho->atualizaFornecedorCompra($fornecedor_id);
            session(['fornecedor_compra' => $fornecedor_id]);
        }
        $codigoitem = Codigoitem::find($modMinutaEmpenho->tipo_empenhopor_id);

        if ($codigoitem->descricao === 'Contrato') {
            $tipo = 'contrato_item_id';
            $update = ContratoItemMinutaEmpenho::where('minutaempenho_id', $minuta_id)->get()->isNotEmpty();
            $itens = Contrato::where('fornecedor_id', '=', $fornecedor_id)
                ->where('contratos.id', '=', $modMinutaEmpenho->contrato_id)
                ->whereNull('contratoitens.deleted_at')
                ->join('contratoitens', 'contratoitens.contrato_id', '=', 'contratos.id')
                ->join('codigoitens', 'codigoitens.id', '=', 'contratoitens.tipo_id')
                ->join('catmatseritens', 'catmatseritens.id', '=', 'contratoitens.catmatseritem_id')
                ->select([
                    'contratoitens.id',
                    'codigoitens.descricao',
                    'contratoitens.numero_item_compra as numero',
                    'catmatseritens.codigo_siasg',
                    'catmatseritens.descricao as catmatser_desc',
                    'catmatseritens.catmatpdm_id',
                    'catmatseritens.id as catmatseritens_id',
                    DB::raw("SUBSTRING(catmatseritens.descricao for 50) AS catmatser_desc_simplificado"),
                    'contratoitens.descricao_complementar as descricaodetalhada',
                    DB::raw("SUBSTRING(contratoitens.descricao_complementar for 50) AS descricaosimplificada"),
                    'contratoitens.quantidade as quantidade_saldo',
                    'contratoitens.valorunitario as valor_unitario',
                    'contratoitens.valortotal as valor_negociado'
//                    'minutaempenhos.numero_contrato'
                ])
                ->get();
            $itensCollection = $itens;
            $itens = $itens->toArray();

            //verificar e vincular itens materiais que nao tenha catmatpdm
            $itensCollection = $itensCollection->where('descricao', 'Material')->where('catmatpdm_id', null);
            foreach ($itensCollection as $value) {
                $pdm = CNBS::getPdmMaterialByCode($value->codigo_siasg);
                $obj = new \stdClass;
                $obj->tipo = 'M';
                $obj->pdm = str_pad($pdm->codigoPdm, 5, 0, STR_PAD_LEFT);
                $obj->nomePdm = $pdm->nomePdm;
                $catmatpdm_id = $this->getCatmatpdm_id($obj);
                Catmatseritem::find($value->catmatseritens_id)->update(['catmatpdm_id' => $catmatpdm_id]);
            }

            //#239
            if (count(array_unique(Arr::pluck($itens, 'descricao')))  > 1) {
                $acoes = 'Ações';
            }
        }

        if ($codigoitem->descricao === 'Compra') {
            $tipo = 'compra_item_id';
            $sisrpId = $this->retornaIdCodigoItem('Tipo Compra', 'SISRP');
            $sisppId = $this->retornaIdCodigoItem('Tipo Compra', 'SISPP');
            $update = CompraItemMinutaEmpenho::where('minutaempenho_id', $minuta_id)->get()->isNotEmpty();

            $leiFormatada14133Derivadas = resolve(AmparoLegalService::class)
                ->recuperarAmparoLegalFormatadoPorLeiCompra($modMinutaEmpenho->compra->lei);

            $itens = CompraItem::join('compras', 'compras.id', '=', 'compra_items.compra_id')
                ->join('compra_item_fornecedor', 'compra_item_fornecedor.compra_item_id', '=', 'compra_items.id')
                ->join('fornecedores', 'fornecedores.id', '=', 'compra_item_fornecedor.fornecedor_id')
                ->join('compra_item_unidade', 'compra_item_unidade.compra_item_id', '=', 'compra_items.id')
                ->join('unidades', 'unidades.id', '=', 'compra_item_unidade.unidade_id')
                ->join('codigoitens', 'codigoitens.id', '=', 'compra_items.tipo_item_id')
                ->join(
                    'catmatseritens',
                    'catmatseritens.id',
                    '=',
                    'compra_items.catmatseritem_id'
                )
                ->join('codigoitens as mod', 'mod.id', '=', 'compras.modalidade_id')
                ->join('codigoitens as tipo_compra', 'tipo_compra.id', '=', 'compras.tipo_compra_id')


                /**
                 * Regras das vigências:
                 * caso o compra_items.ata_vigencia_fim seja null,
                 *      permitir compras SISPP
                 *      OU permitir compras SISRP
                 *          E LEI 14133 COM a vigencia sendo observada na tabela compra_item_fornecedor
                 *
                 * caso o compra_items.ata_vigencia_fim não seja nulo e não seja compras SISRP E LEI 14133,
                 * a vigência deve ser observada  na tabela  compra_items
                 */
                ->where(function ($query) use ($sisrpId, $sisppId, $leiFormatada14133Derivadas) {
                    $query->where(function ($query) use ($sisrpId, $sisppId, $leiFormatada14133Derivadas) {
                        $query->whereNull('compra_items.ata_vigencia_fim')
                            ->where('tipo_compra_id', $sisppId)
                            ->orWhere(function ($query) use ($sisrpId, $leiFormatada14133Derivadas) {
                                $query->where('tipo_compra_id', $sisrpId)
                                    ->whereIn('lei', $leiFormatada14133Derivadas)
                                    ->where(
                                        'compra_item_fornecedor.ata_vigencia_inicio',
                                        '<=',
                                        Carbon::now()->toDateString()
                                    )
                                    ->where(
                                        'compra_item_fornecedor.ata_vigencia_fim',
                                        '>=',
                                        Carbon::now()->toDateString()
                                    );
                            });
                    })
                        ->orWhere(function ($query) use ($sisrpId, $leiFormatada14133Derivadas) {
                            $query->where('compra_items.ata_vigencia_fim', '>=', Carbon::now()->toDateString())
                                ->where(function ($query) use ($sisrpId, $leiFormatada14133Derivadas) {
                                    $query->where('tipo_compra_id', '<>', $sisrpId)
                                        ->orWhere(function ($query) use ($leiFormatada14133Derivadas) {
                                            $query->whereNotIn('compras.lei', $leiFormatada14133Derivadas);
                                        });
                                });
                        });
                })
                ->where('compra_item_unidade.quantidade_saldo', '>', 0)
                ->where('compra_item_unidade.unidade_id', session('user_ug_id'))
                ->where('compras.id', $modMinutaEmpenho->compra_id)
                ->where('compra_items.situacao', '=', true)
                ->where('compra_item_fornecedor.situacao', '=', true)
                ->where('compra_item_unidade.situacao', '=', true)
//                ->where('compra_item_unidade.fornecedor_id', $fornecedor_id)
//                ->where('compra_item_fornecedor.fornecedor_id', $fornecedor_id)
                ->whereRaw("
                    CASE WHEN compra_item_unidade.tipo_uasg = 'C'
                            or tipo_compra.descricao = 'SISPP'
                        THEN compra_item_unidade.fornecedor_id = '".$modMinutaEmpenho->fornecedor_compra_id."'
                        ELSE compra_item_unidade.fornecedor_id IS NULL
                    END")
                ->select([
                    'compra_items.id',
                    'codigoitens.descricao',
                    'catmatseritens.codigo_siasg',
                    'catmatseritens.descricao as catmatser_desc',
                    DB::raw("SUBSTRING(catmatseritens.descricao for 50) AS catmatser_desc_simplificado"),
                    'compra_items.descricaodetalhada',
                    DB::raw("SUBSTRING(compra_items.descricaodetalhada for 50) AS descricaosimplificada"),
                    'compra_item_unidade.quantidade_saldo',
                    'compra_item_fornecedor.valor_unitario',
                    'compra_item_fornecedor.valor_negociado',
                    'compra_items.numero',
                    'compra_items.criterio_julgamento',
                    'compra_item_fornecedor.percentual_maior_desconto',
                    DB::raw("compra_item_fornecedor.valor_unitario * " .
                        "((100-compra_item_fornecedor.percentual_maior_desconto)/100) as valor_unitario_desconto"),
                    DB::raw("compra_item_fornecedor.valor_negociado * " .
                        "((100-compra_item_fornecedor.percentual_maior_desconto)/100) as valor_negociado_desconto"),
                    'mod.descres',
                    'compra_item_fornecedor.classificacao',
                    'compra_item_fornecedor.fornecedor_id',
                    'codigo_ncmnbs'
                ])
                ->distinct();

            $itens = $this->setCondicaoFornecedor(
                $modMinutaEmpenho,
                $itens,
                $codigoitem->descricao,
                $modMinutaEmpenho->fornecedor_empenho_id,
                $modMinutaEmpenho->fornecedor_compra_id
            );

            $itens = $itens->get()
                ->toArray();

            $itens = array_map(static function ($itens) {
                $itens['valor_unitario_desconto'] = round((float)$itens['valor_unitario_desconto'], 4);
                $itens['valor_unitario_desconto'] =
                    number_format($itens['valor_unitario_desconto'], 4, '.', '');

                $itens['valor_negociado_desconto'] = round((float)$itens['valor_negociado_desconto'], 5);
                $itens['valor_negociado_desconto'] =
                    number_format($itens['valor_negociado_desconto'], 4, '.', '');

                return $itens;
            }, $itens);

            //#239
            if (count(array_unique(Arr::pluck($itens, 'descricao')))  > 1) {
                $acoes = 'Ações';
            }

           $classificacao = $this->verificaClassificacaoItens($modMinutaEmpenho);
        }

        if ($codigoitem->descricao === 'Suprimento') {
            $acoes = 'Ações';
            DB::beginTransaction();
            try {
                $this->gravaCompraItemFornecedorSuprimento($modMinutaEmpenho, $fornecedor_id);
                DB::commit();
            } catch (Exception $exc) {
                DB::rollback();
            }

            $tipo = 'compra_item_id';
            $update = CompraItemMinutaEmpenho::where('minutaempenho_id', $minuta_id)->get()->isNotEmpty();

            $itens = CompraItem::join('compras', 'compras.id', '=', 'compra_items.compra_id')
                ->join('compra_item_fornecedor', 'compra_item_fornecedor.compra_item_id', '=', 'compra_items.id')
                ->join('fornecedores', 'fornecedores.id', '=', 'compra_item_fornecedor.fornecedor_id')
                ->join('compra_item_unidade', 'compra_item_unidade.compra_item_id', '=', 'compra_items.id')
                ->join('unidades', 'unidades.id', '=', 'compra_item_unidade.unidade_id')
                ->join('codigoitens', 'codigoitens.id', '=', 'compra_items.tipo_item_id')
                ->join(
                    'catmatseritens',
                    'catmatseritens.id',
                    '=',
                    'compra_items.catmatseritem_id'
                )
                ->where('compra_item_unidade.quantidade_saldo', '>', 0)
                ->where('compra_item_unidade.unidade_id', session('user_ug_id'))
                ->where('compras.id', $modMinutaEmpenho->compra_id)
                ->where(function ($query) use ($fornecedor_id) {
                    $query->where('compra_item_unidade.fornecedor_id', $fornecedor_id)
                        ->orWhere('compra_item_fornecedor.fornecedor_id', $fornecedor_id);
                })
                ->select([
                    'compra_items.id',
                    'codigoitens.descricao',
                    'catmatseritens.codigo_siasg',
                    'catmatseritens.descricao as catmatser_desc',
                    DB::raw("SUBSTRING(catmatseritens.descricao for 50) AS catmatser_desc_simplificado"),
                    'compra_items.descricaodetalhada',
                    DB::raw("SUBSTRING(compra_items.descricaodetalhada for 50) AS descricaosimplificada"),
                    'compra_item_unidade.quantidade_saldo',
                    'compra_item_fornecedor.valor_unitario',
                    'compra_item_fornecedor.valor_negociado',
                    'compra_items.numero'
                ])
                ->get()
                ->toArray();
        }

        if ($request->ajax()) {
            return DataTables::of($itens)
                ->addColumn('action', function ($itens) use ($modMinutaEmpenho, $tipo) {
                    return $this->retornaRadioItens($itens['id'], $modMinutaEmpenho->id, $itens['descricao'], $tipo);
                })
                ->addColumn('descricaosimplificada', function ($itens) use ($modMinutaEmpenho) {
                    if ($itens['descricaosimplificada'] != null && $itens['descricaosimplificada'] !== 'undefined') {
                        return $this->retornaDescricaoDetalhada(
                            $itens['descricaosimplificada'],
                            $itens['descricaodetalhada']
                        );
                    }
                    return $this->retornaDescricaoDetalhada(
                        $itens['catmatser_desc_simplificado'],
                        $itens['catmatser_desc']
                    );
                })
                ->rawColumns(['descricaosimplificada', 'action'])
                ->make(true);
        }

        $desconto = in_array('D', Arr::pluck($itens, 'criterio_julgamento'), true)
            && !in_array('99', Arr::pluck($itens, 'descres'), true)
            && !in_array('06', Arr::pluck($itens, 'descres'), true);
        $html = $this->retornaGridItens($acoes, $desconto, $codigoitem->descricao);

        return view(
            'backpack::mod.empenho.Etapa3Itensdacompra',
            compact('html')
        )->with(['update' => $update, 'classificacaoFornecedores' => $classificacao]);
    }

    private function retornaRadioItens($id, $minuta_id, $descricao, $tipo): string
    {
        return " <input  type='checkbox' id='item_$id' data-tipo='$descricao' " .
            " name='itens[][$tipo]' value='$id'  data-tipo_item='$tipo'" .
            " onclick=\"bloqueia('$descricao');toggleItensASubmeter(this.value, $(this).is(':checked'))\" > ";
    }

    private function retornaDescricaoDetalhada($descricao, $descricaocompleta): string
    {
        return $descricao . ' <i class="fa fa-info-circle" title="' . $descricaocompleta . '"></i>';
    }

    /**
     * Monta $html com definições do Grid
     *
     * @return Builder
     */
    private function retornaGridItens(string $acoes, bool $desconto, string $tipo = ''): Builder
    {

        $html = $this->htmlBuilder
            ->addColumn([
                'data' => 'action',
                'name' => 'action',
                'title' => $acoes,
                'orderable' => false,
                'searchable' => false
            ])
            ->addColumn([
                'data' => 'numero',
                'name' => 'numero',
                'title' => 'N. Item',
            ])
            ->addColumn([
                'data' => 'descricao',
                'name' => 'descricao',
                'title' => 'Tipo',
            ])
            ->addColumn([
                'data' => 'codigo_siasg',
                'name' => 'codigo_siasg',
                'title' => 'Código CATMATSER',
            ]);
        if ($tipo != 'Suprimento' && $tipo != 'Contrato'){
            $html = $this->htmlBuilder->addColumn([
                'data' => 'codigo_ncmnbs',
                'name' => 'codigo_ncmnbs',
                'title' => 'Código NCM/NBS',
            ]);
        }

        $html = $this->htmlBuilder->addColumn([
                'data' => 'descricaosimplificada',
                'name' => 'descricaosimplificada',
                'title' => 'Descrição',
            ])
            ->addColumn([
                'data' => 'quantidade_saldo',
                'name' => 'quantidade_saldo',
                'title' => 'Qtd./Saldo',
            ])
            ->addColumn([
                'data' => 'valor_unitario',
                'name' => 'valor_unitario',
                'title' => 'Valor Unit.',
            ])
            ->addColumn([
                'data' => 'valor_negociado',
                'name' => 'valor_negociado',
                'title' => 'Valor Total <i class="fa fa-info-circle" title="O valor total refere-se ao ' .
                    'valor negociado ou homologado para o item."></i>',
            ]);
        if ($desconto) {
            $html = $this->htmlBuilder->addColumn([
                'data' => 'percentual_maior_desconto',
                'name' => 'percentual_maior_desconto',
                'title' => '% Desconto',
            ])->addColumn([
                'data' => 'valor_unitario_desconto',
                'name' => 'valor_unitario_desconto',
                'title' => 'Valor Unit. Desconto',
            ])->addColumn([
                'data' => 'valor_negociado_desconto',
                'name' => 'valor_negociado_desconto',
                'title' => 'Valor Total Com Desconto',
            ]);
        }

        $html = $this->htmlBuilder->parameters([
            'processing' => true,
            'serverSide' => true,
            'responsive' => true,
            'info' => true,
            'order' => [
                0,
                'desc'
            ],
            'autoWidth' => false,
            'bAutoWidth' => false,
            'paging' => true,
            'lengthChange' => true,
            'lengthMenu' => [
                [10, 25, 50, 100, -1],
                ['10', '25', '50', '100', 'Todos']
            ],
            'language' => [
                'url' => asset('/json/pt_br.json')
            ]
        ]);

        return $html;
    }

    public function store(Request $request)
    {
        [$minuta, $minuta_id, $fornecedor_id, $itens] = $this->setVariaveis($request);

        try {
            $this->verificaQtdItens($itens);
            $this->verificaTiposItens($itens);
        } catch (Exception $e) {
            return $this->retornaPaginaItens($e, $minuta_id, $fornecedor_id);
        }

        DB::beginTransaction();
        try {
            $situacao_andamento = Codigoitem::wherehas('codigo', function ($q) {
                $q->where('descricao', '=', 'Situações Minuta Empenho');
            })
                ->where('descricao', 'EM ANDAMENTO')
                ->first();


            $remessa = MinutaEmpenhoRemessa::create([
                'minutaempenho_id' => $minuta_id,
                'situacao_id' => $situacao_andamento->id,
                'remessa' => 0,
            ]);
            $base = new Base();
            $remessa->sfnonce = $base->geraNonceSiafiEmpenho($minuta_id, $remessa->id);
            $remessa->save();

            $codigoitem = Codigoitem::find($minuta->tipo_empenhopor_id);

            foreach ($itens as $index => $item) {

                if($codigoitem->descricao == 'Compra' || $codigoitem->descricao == 'Suprimento'){
                    [$compraItemFornecedor, $compraItemUnidade] = $this->getCifCiu(
                        $itens[$index]['compra_item_id'],
                        $fornecedor_id
                    );

                    $itens[$index]['compra_item_fornecedor_id'] = $compraItemFornecedor->id;
                    $itens[$index]['compra_item_unidade_id'] = $compraItemUnidade->id;
                }

                $itens[$index]['minutaempenho_id'] = $minuta_id;
                $itens[$index]['minutaempenhos_remessa_id'] = $remessa->id;
                $itens[$index]['numseq'] = $index + 1;
            }

            if ($codigoitem->descricao == 'Contrato') {
                ContratoItemMinutaEmpenho::insert($itens);
            } else {
                CompraItemMinutaEmpenho::insert($itens);
            }


            $minuta->etapa = 4;
            $minuta->save();

            DB::commit();

            return redirect()->route('empenho.minuta.gravar.saldocontabil', ['minuta_id' => $minuta_id]);
        } catch (Exception $exc) {
            DB::rollback();
            throw $exc;
        }
    }

    public function update(Request $request)
    {
        [$minuta, $minuta_id, $fornecedor_id, $itens] = $this->setVariaveis($request);

        try {
            $this->verificaQtdItens($itens);
            $this->verificaTiposItens($itens);
        } catch (Exception $e) {
            return $this->retornaPaginaItens($e, $minuta_id, $fornecedor_id);
        }

        DB::beginTransaction();
        try {
            $codigoitem = Codigoitem::find($minuta->tipo_empenhopor_id);

            if ($codigoitem->descricao == 'Contrato') {
                $cime = ContratoItemMinutaEmpenho::where('minutaempenho_id', $minuta_id);
                $cime_deletar = $cime->get();
                $cime->delete();
                $remessa_id = $minuta->remessa[0]->id;

                foreach ($itens as $index => $item) {
                    $itens[$index]['minutaempenho_id'] = $minuta_id;
                    $itens[$index]['minutaempenhos_remessa_id'] = $remessa_id;
                    $itens[$index]['numseq'] = $index + 1;
                }

                ContratoItemMinutaEmpenho::insert($itens);
            } else {
                $cime = CompraItemMinutaEmpenho::where('minutaempenho_id', $minuta_id);
                $cime_deletar = $cime->get();
                $cime->delete();
                $remessa_id = $minuta->remessa[0]->id;

                foreach ($cime_deletar as $item) {
                    $compraItemUnidade = CompraItemUnidade::where('compra_item_unidade.compra_item_id', $item->compra_item_id)
                        ->where('compra_item_unidade.unidade_id', session('user_ug_id'))
                        ->where('compra_item_unidade.id', $item->compra_item_unidade_id)
                        ->join('compra_items', 'compra_items.id', '=', 'compra_item_unidade.compra_item_id')
                        ->join('compras', 'compras.id', '=', 'compra_items.compra_id')
                        ->join('codigoitens as tipo_compra', 'tipo_compra.id', '=', 'compras.tipo_compra_id')
                        ->select('compra_item_unidade.*')
                        ->first();

                    $compraItemUnidade->quantidade_saldo =
                        $this->retornaSaldoAtualizado(
                            $item->compra_item_id,
                            null,
                            $compraItemUnidade->id
                    )->saldo;
                    $compraItemUnidade->save();
                }

                foreach ($itens as $index => $item) {
                    [$compraItemFornecedor, $compraItemUnidade] = $this->getCifCiu(
                        $itens[$index]['compra_item_id'],
                        $fornecedor_id
                    );

                    $itens[$index]['minutaempenho_id'] = $minuta_id;
                    $itens[$index]['minutaempenhos_remessa_id'] = $remessa_id;
                    $itens[$index]['numseq'] = $index + 1;
                    $itens[$index]['compra_item_fornecedor_id'] = $compraItemFornecedor->id;
                    $itens[$index]['compra_item_unidade_id'] = $compraItemUnidade->id;
                }
                CompraItemMinutaEmpenho::insert($itens);
            }

            $base = new Base();
            $remessa = MinutaEmpenhoRemessa::find($remessa_id);
            $remessa->sfnonce = $base->geraNonceSiafiEmpenho($minuta_id, $remessa->id);
            $remessa->save();

            $minuta->etapa = 4;
            $minuta->save();
            DB::commit();

            return redirect()->route('empenho.minuta.gravar.saldocontabil', ['minuta_id' => $minuta_id]);
        } catch (Exception $exc) {
            DB::rollback();
            throw $exc;
            return redirect()->back();
        }
    }

    /**
     * Verifica os tipos de itens
     * Caso tenha Material e Serviço retorna erro
     * @param $itens
     * @return void
     * @throws Exception
     */
    public function verificaTiposItens($itens): void
    {
        $objItens = collect($itens)->pluck('compra_item_id');

        $count = CompraItem::whereIn('id', $objItens)
            ->selectRaw('COUNT(DISTINCT tipo_item_id)')
            ->pluck('count')
            ->first();

        if ($count > 1) {
            throw new Exception('Não é possível escolher itens do tipo Material e Serviço na mesma minuta.');
        }
    }

    /**
     * É obrigatório pelo menos 1 item
     * @param $itens
     * @return void
     * @throws Exception
     */
    public function verificaQtdItens($itens): void
    {
        if (!isset($itens)) {
            throw new Exception('Escolha pelo menos 1 item.');
        }
    }

    /**
     * @param Exception $e
     * @param $minuta_id
     * @param $fornecedor_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function retornaPaginaItens(Exception $e, $minuta_id, $fornecedor_id): \Illuminate\Http\RedirectResponse
    {
        Alert::error($e->getMessage())->flash();
        return redirect()->route(
            'empenho.minuta.etapa.item',
            ['minuta_id' => $minuta_id, 'fornecedor_id' => $fornecedor_id]
        )->withInput();
    }

    /**
     * Seta algumas variaveis no Store e Update
     * @param Request $request
     * @return array
     */
    public function setVariaveis(Request $request): array
    {
        $minuta = MinutaEmpenho::find($request->minuta_id);
        $minuta_id = $request->minuta_id;
        $fornecedor_id = $request->fornecedor_id;
        $itens = $request->itens;
        return array($minuta, $minuta_id, $fornecedor_id, $itens);
    }

    private function verificaClassificacaoItens($modMinutaEmpenho){

       $fornecedores = Compra::select([
            'compras.id as compra_id','compra_items.numero',
            'fornecedores.id as fornecedor_id', 'fornecedores.nome',
            'fornecedores.cpf_cnpj_idgener', 'compra_item_fornecedor.classificacao',
            'unidade_origem_id', 'tipo_compra_id', 'compra_item_id',
            'compra_items.descricaodetalhada'
            ])
            ->join('compra_items', 'compra_items.compra_id', '=', 'compras.id')
            ->join('compra_item_fornecedor', 'compra_item_fornecedor.compra_item_id', '=', 'compra_items.id')
            ->join('fornecedores', 'fornecedores.id', '=', 'compra_item_fornecedor.fornecedor_id')
            ->where(['compras.id' => $modMinutaEmpenho->compra_id])
            ->orderBy('compra_items.numero')
            ->get()->toArray();

        return $fornecedores;
    }

    /**
     * Retorna compraItemFornecedor e compraItemUnidade
     *
     * @param $compra_item_id
     * @param $fornecedor_id
     * @return array
     */
    public function getCifCiu($compra_item_id, $fornecedor_id): array
    {
        $compraItemFornecedor = CompraItemFornecedor::where('compra_item_id', $compra_item_id)
            ->where('fornecedor_id', $fornecedor_id)
            ->first();

        $compraItemUnidade = CompraItemUnidade::where('compra_item_id', $compra_item_id)
            ->where('unidade_id', session('user_ug_id'));

        //adaptação necessária porcausa dos Caronas com mais de um fornecedor para mesma unidade
        if ($compraItemUnidade->count() !== 1) {
            $compraItemUnidade = $compraItemUnidade->where('fornecedor_id', $fornecedor_id);
        }
        $compraItemUnidade = $compraItemUnidade->first();
        return array($compraItemFornecedor, $compraItemUnidade);
    }

}
