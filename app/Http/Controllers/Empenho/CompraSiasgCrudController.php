<?php

namespace App\Http\Controllers\Empenho;

use Alert;

use App\Http\Controllers\CNBS;
use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\ConsultaCompra;
use App\Http\Traits\Formatador;
use App\Http\Traits\PermissaoTrait;
use App\Models\Catmatseritem;
use App\Models\Codigo;
use App\Models\Codigoitem;
use App\Models\Compra;
use App\Models\Contrato;
use App\Models\CompraItem;
use App\Models\CompraItemFornecedor;
use App\Models\CompraItemMinutaEmpenho;
use App\Models\CompraItemUnidade;
use App\Models\ContratoMinutaEmpenhoPivot;
use App\Models\Fornecedor;
use App\Models\MinutaEmpenho;
use App\Models\Unidade;
use App\services\AmparoLegalService;
use App\XML\ApiSiasg;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CompraSiasgRequest as StoreRequest;
use App\Http\Requests\CompraSiasgRequest as UpdateRequest;
use http\Params;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Route;
use App\Http\Traits\CompraTrait;
use App\Http\Traits\LogTrait;
use Exception;
use GuzzleHttp\Exception\RequestException;
use App\Http\Controllers\NDC;
use stdClass;

class CompraSiasgCrudController extends CrudController
{
    use Formatador;
    use CompraTrait;
    use BuscaCodigoItens;
    use LogTrait;
    use ConsultaCompra;
    use PermissaoTrait;

    public const MATERIAL = [149, 194];
    public const SERVICO  = [150, 195];
    public const SISPP    = 1;
    public const SISRP    = 2;

    public function setup()
    {

        $modalidades = Codigoitem::whereHas('codigo', function ($query) {
            $query->where('descricao', '=', 'Modalidade Licitação');
        })
            ->whereRaw('LENGTH(descres) <= 2')
            ->orderBy('descres')
            ->select(DB::raw("CONCAT(descres,' - ',descricao) AS descres_descricao"), 'id')
            ->pluck('descres_descricao', 'id');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel(Compra::class);
        $this->crud->setRoute(
            config('backpack.base.route_prefix')
            . '/empenho/buscacompra'
        );
        $this->crud->setEntityNameStrings('Buscar Compra', 'Buscar Compras');
        $this->crud->setCreateView('vendor.backpack.crud.empenho.create');
        $this->crud->urlVoltar = route('empenho.crud./minuta.index', ['ano' => now()->year]);

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $this->fields($modalidades);

        // add asterisk for fields that are required in GlosaRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    private function fields(Collection $modalidades): void
    {
        $this->setFielContrato();
        $this->setFieldModalidade($modalidades);
        $this->setFieldNumeroAno();
        $this->setFieldUnidadeCompra();
        $this->setFieldUasgBeneficiaria();
        $this->setFieldFornecedor();
        $this->setFieldContratacao();
    }

    private function setFieldModalidade($modalidade): void
    {
        $this->crud->addField([
            'name' => 'modalidade_id',
            'label' => "Modalidade Compra",
            'type' => 'select2_from_array',
            'options' => $modalidade,
            'allows_null' => true,
            'attributes' => [
                'disabled' => 'disabled',
                'class' => 'form-control select2_from_array opc_compra',
                'id' => 'opc_compra_modalidade'
            ],
        ]);
    }

    private function setFieldNumeroAno(): void
    {
        $this->crud->addField([
            'name' => 'numero_ano',
            'label' => 'Número / Ano',
            'type' => 'numcontrato',
            'attributes' => [
                'disabled' => 'disabled',
                'class' => 'form-control opc_compra',
            ],
        ]);
    }

    private function setFieldUasgBeneficiaria(): void
    {
        $this->crud->addField([
            'label' => "Unidade Beneficiária",
            'type' => "select2_from_ajax_single",   // alterado para esse tipo, para possibilitar que a imagem do hint apareça, pois só aparece nesse tipo.
            'name' => 'uasg_beneficiaria_id',
            'entity' => 'unidades',
            'attribute' => "codigo",
            'attributes' => [
                'disabled' => 'disabled',
                'class' => 'form-control opc_compra opc_beneficiaria col-md-12',
                'id' => 'opc_compra_uasg_beneficiaria',
                'title' => 'Este campo deverá ser preenchido apenas quando o saldo a ser consumido pertence a uma UASG polo ou subordinada à UASG do usuário.'
            ],
            'attribute2' => "nomeresumido",
            'process_results_template' => 'gescon.process_results_unidade',
            'model' => "App\Models\Unidade",
            'data_source' => url("api/unidade"),
            'placeholder' => "Selecione a UASG Beneficiária",
            'minimum_input_length' => 2,
            'ico_help' => 'Este campo deverá ser preenchido apenas quando o saldo a ser consumido pertence a uma UASG polo ou subordinada à UASG do usuário.',
        ]);
    }

    private function setFieldUnidadeCompra(): void
    {
        $this->crud->addField([
            'label' => "Unidade Compra",
            'type' => "select2_from_ajax",
            'name' => 'unidade_origem_id',
            'entity' => 'unidade_origem',
            'attribute' => "codigo",
            'attributes' => [
                'disabled' => 'disabled',
                'class' => 'form-control opc_compra',
                'id' => 'opc_compra_unidade'
            ],
            'attribute2' => "nomeresumido",
            'process_results_template' => 'gescon.process_results_unidade',
            'model' => "App\Models\Unidade",
            'data_source' => url("api/unidade"),
            'placeholder' => "Selecione a Unidade",
            'minimum_input_length' => 2,

        ]);
    }

    private function setFielContrato(): void
    {
        $this->crud->addField([
            'label' => "Contrato",
            'type' => "select2_from_ajax",
            'name' => 'id',
            'entity' => 'contratos',
            'attribute' => "numero",
            'model' => "App\Models\Contrato",
            'data_source' => url("api/contrato/numero"),
            'placeholder' => "Selecione um Contrato",
            'minimum_input_length' => 6,
            'attributes' => [
                'class' => 'form-control opc_contrato',
                'id' => 'opc_contrato_numero'
            ],
        ]);
    }

    private function setFieldFornecedor()
    {
        $this->crud->addField([
            'label' => "Suprido",
            'type' => "select2_from_ajax_suprido",
            'name' => 'fornecedor_empenho_id',
            'entity' => 'fornecedor',
            'attribute' => "cpf_cnpj_idgener",
            'model' => Fornecedor::class,
            'data_source' => url("api/suprido"),
            'placeholder' => "Selecione o suprido",
            'minimum_input_length' => 2,
            'attributes' => [
                'disabled' => 'disabled',
                'class' => 'form-control opc_suprimento',
            ],
        ]);
    }

    private function setFieldContratacao(){
        $this->crud->addField([
            'name' => 'numero_contratacao',
            'label' => 'Id contratação PNCP',
            'type' => 'numcontratacaominuta',
            'attributes' => [
                'disabled' => 'disabled',
                'class' => 'form-control opc_mercado_gov',
            ],
        ]);
    }

    public function show($id)
    {
        $content = parent::show($id);

        return $content;
    }

    public function store(StoreRequest $request)
    {
        if ($request->tipoEmpenho != 3) {
            [$contrato, $retornoSiasg] = $this->getCompraContrato($request);
        }

        if ($this->verificaPermissaoConsulta('minuta') === false) {
            return redirect()->to('/');
        }
        //CONTRATO
        if ($request->tipoEmpenho == 1) {
            if (is_null($retornoSiasg->data)) {
                $compra = $this->verificaCompraExiste([
                    'unidade_origem_id' => $contrato->unidadecompra_id,
                    'modalidade_id' => $contrato->modalidade_id,
                    'numero_ano' => $contrato->licitacao_numero
                ]);
                if (!$compra) {
                    if($this->getErroMensagemSiasgNDC($retornoSiasg) !== false){
                        return redirect('/empenho/buscacompra')->with(
                            'alert-warning',
                            $this->getErroMensagemSiasgNDC($retornoSiasg)
                        );
                    }

                    return redirect('/empenho/buscacompra')->with('alert-warning', $retornoSiasg->messagem);
                }

                $msg_original = $retornoSiasg->messagem;

                $retornoSiasg = $compra;

                if ($compra->tipo_compra_desc === 'SISRP') {
                    $compraPertenceLei14133Derivadas = resolve(AmparoLegalService::class)
                        ->existeAmparoLegalEmLeiCompra($compra->lei);

                    if ($compraPertenceLei14133Derivadas &&
                        !($compra->verificaPermissaoSisrp(session('user_ug_id'), $contrato))) {
                        return redirect('/empenho/buscacompra')->with('alert-warning', $msg_original);
                    }

                    $retornoSiasg = $this->getRetornoSiasg($compra);
                }
            }

            $unidade_autorizada = $this->verificaPermissaoUasgCompraParamContrato($contrato);

            if (is_null($unidade_autorizada)) {
                return redirect('/empenho/buscacompra')
                    ->with('alert-warning', 'Você não tem permissão para realizar empenho para esta compra.');
            }

            $unidade_origem_id = $request->get('unidadeorigem_contrato_id') ?? $contrato->unidadeorigem_id;
            $obj = new stdClass();
            $obj->unidade_origem_id = $contrato->unidadecompra_id;

            $verificaPermissaoUasg = $this->verificaPermissaoUasgCompra($retornoSiasg, $obj, $unidade_origem_id);

            if (is_null($verificaPermissaoUasg)) {
                return redirect('/empenho/buscacompra')
                    ->with('alert-warning', 'Você não tem permissão para realizar empenho com
                    a compra informada no contrato.');
            }

            $request->request->set('numero_ano', $contrato->licitacao_numero);
            $request->request->set('unidade_origem_id', $contrato->unidadecompra_id);
            $request->request->set('modalidade_id', $contrato->modalidade->id);

            $request->request->set('unidadeorigem_contrato_id', $unidade_origem_id);

            $this->montaParametrosCompra($retornoSiasg, $request);

            $situacao_id = $this->retornaIdCodigoItem('Situações Minuta Empenho', 'EM ANDAMENTO');

            DB::beginTransaction();

            try {
                $compra = $this->atualizarCompra($request, $retornoSiasg);

                if ($compra === 'LEI14133SISRP') {
                    return redirect('/empenho/buscacompra')->with(
                        'alert-warning',
                        'Compra não possui Ata de Registro de Preços registrada.'
                    );
                }

                $tipo_empenhopor = $this->getEmpenhopor('Contrato');

                $minutaEmpenho = $this->gravaMinutaEmpenho([
                    'situacao_id'          => $situacao_id,
                    'compra_id'            => $compra->id,
                    'contrato_id'          => $contrato->id,
                    'unidade_origem_id'    => $contrato->unidadecompra_id,
                    'unidade_id'           => $unidade_autorizada,
                    'modalidade_id'        => $contrato->modalidade_id,
                    'numero_ano'           => $contrato->numero,
                    'tipo_empenhopor'      => $tipo_empenhopor->id,
                    'fornecedor_compra_id' => $contrato->fornecedor_id,
                    'numero_contrato'      => $contrato->numero,
                ]);

                DB::commit();

                if ($retornoSiasg->messagem === "A UASG da compra está inativa") {
                    return redirect('/empenho/fornecedor/' . $minutaEmpenho->id . '/' . $retornoSiasg->messagem);
                }

                return redirect('/empenho/fornecedor/' . $minutaEmpenho->id);
            } catch (Exception $exc) {
                DB::rollback();
                $this->inserirLogCustomizado('minuta-empenho', 'error', $exc);
                return redirect('/empenho/buscacompra')->with('alert-danger', 'Erro ao buscar a compra!');
            }
        }

        //COMPRA
        if ($request->tipoEmpenho == 2 || $request->tipoEmpenho == 4) {
            if (is_null($retornoSiasg->data)) {
                $compra = $this->verificaCompraExiste([
                    'unidade_origem_id' => $request->get('unidade_origem_id'),
                    'modalidade_id'     => $request->get('modalidade_id'),
                    'numero_ano'        => $request->get('numero_ano')
                ]);

                if (!$compra) {
                    if($this->getErroMensagemSiasgNDC($retornoSiasg) !== false){
                        return redirect('/empenho/buscacompra')->with(
                            'alert-warning',
                            $this->getErroMensagemSiasgNDC($retornoSiasg)
                        );
                    }
                    return redirect('/empenho/buscacompra')->with('alert-warning', $retornoSiasg->messagem);
                }
                if ($compra->tipo_compra_desc === 'SISRP') {
                    $compraPertenceLei14133Derivadas = resolve(AmparoLegalService::class)
                        ->existeAmparoLegalEmLeiCompra($compra->lei);

                    if ($compraPertenceLei14133Derivadas && !($compra->verificaPermissaoSisrp(session('user_ug_id')))) {
                        return redirect('/empenho/buscacompra')->with('alert-warning', $retornoSiasg->messagem);
                    }

                    if ($compraPertenceLei14133Derivadas) {
                        $retornoSiasg = $this->getRetornoSiasg($compra);
                    }
                }
            }

            if ($retornoSiasg->messagem !== "Sucesso") {
                return redirect('/empenho/buscacompra')->with('alert-warning', $retornoSiasg->messagem);
            }

            if (isset($retornoSiasg->data->api_origem)
                && $retornoSiasg->data->api_origem === "novo_divulgacao_compra") {
                $modalidade = $this->retornaDescresPorId($request->input('modalidade_id'));
                if ($modalidade != $retornoSiasg->data->compraSispp->codigoModalidade) {
                    return redirect('/empenho/buscacompra')->with(
                        'alert-warning',
                        'Compra não encontrada com a modalidade selecionada'
                    );
                }
            }

            if (isset($retornoSiasg->data->api_origem) && $retornoSiasg->data->api_origem === "pncp") {
                $request->unidade_origem_id = Unidade::where
                ('codigo', $retornoSiasg->data->compraSispp->unidade)->first()->id;
                $request->request->set('numero_ano', $retornoSiasg->data->compraSispp->numeroAno);
                $request->request->set('unidade_origem_id', $request->unidade_origem_id);
                $request->request->set('modalidade_id', 75);
            }

            $unidade_autorizada_id = $this->verificaPermissaoUasgCompra($retornoSiasg, $request);

            if (is_null($unidade_autorizada_id)) {
                return redirect('/empenho/buscacompra')
                    ->with('alert-warning', 'Você não tem permissão para realizar empenho para esta compra.');
            }

            $this->montaParametrosCompra($retornoSiasg, $request);

            $situacao_id = $this->retornaIdCodigoItem('Situações Minuta Empenho', 'EM ANDAMENTO');

            DB::beginTransaction();
            try {
                $compra = $this->atualizarCompra($request, $retornoSiasg);
                if ($compra === 'LEI14133SISRP') {
                    return redirect('/empenho/buscacompra')->with(
                        'alert-warning',
                        'Compra não possui Ata de Registro de Preços registrada.'
                    );
                }

                $compraPertenceLei14133Derivadas = resolve(AmparoLegalService::class)
                    ->existeAmparoLegalEmLeiCompra($compra->lei);

                if ($compraPertenceLei14133Derivadas &&
                    !($compra->verificaPermissaoSisrp(session('user_ug_id')))) {
                    return redirect('/empenho/buscacompra')
                        ->with('alert-warning', 'Você não tem permissão para realizar empenho para esta compra.');
                }

                $tipo_empenhopor = $this->getEmpenhopor('Compra');

                $minutaEmpenho = $this->gravaMinutaEmpenho([
                    'situacao_id'       => $situacao_id,
                    'compra_id'         => $compra->id,
                    'contrato_id'       => null,
                    'unidade_origem_id' => $compra->unidade_origem_id,
                    'unidade_id'        => $unidade_autorizada_id,
                    'modalidade_id'     => $compra->modalidade_id,
                    'numero_ano'        => $compra->numero_ano,
                    'tipo_empenhopor'   => $tipo_empenhopor->id
                ]);

                DB::commit();

                return redirect('/empenho/fornecedor/' . $minutaEmpenho->id);
            } catch (Exception $exc) {
                DB::rollback();
                $this->inserirLogCustomizado('minuta-empenho', 'error', $exc);

                if (strpos($exc->getMessage(), 'local de entrega inválido') !== false) {
                    return redirect('/empenho/buscacompra')
                        ->with(
                            'alert-warning',
                            $exc->getMessage()
                        );
                }

                return redirect('/empenho/buscacompra')->with('alert-danger', 'Erro ao buscar a compra!');
            }
        }

        //SUPRIMENTO
        if ($request->tipoEmpenho == 3) {
            DB::beginTransaction();
            try {
                $modadalidade_id = $this->retornaIdCodigoItem('Modalidade Licitação', 'Suprimento de Fundos');
                $tipo_compra_id = $this->retornaIdCodigoItem('Tipo Compra', 'SISPP');
                $request->request->set('unidade_origem_id', session('user_ug_id'));
                $request->request->set('modalidade_id', $modadalidade_id);
                $request->request->set('numero_ano', '99999/9999');
                $request->request->set('tipo_compra_id', $tipo_compra_id);
                $request->request->set('unidade_subrrogada_id', null);
                $request->request->set('inciso', null);
                $request->request->set('lei', null);

                $compra = $this->updateOrCreateCompra($request);
                $this->gravaParametrosSuprimento($compra, $request->fornecedor_empenho_id);

                $tipo_empenhopor_id = $this->retornaIdCodigoItem('Tipo Empenho Por', 'Suprimento');

                $situacao_id = $this->retornaIdCodigoItem('Situações Minuta Empenho', 'EM ANDAMENTO');
                $minutaEmpenho = $this->gravaMinutaEmpenho([
                    'situacao_id' => $situacao_id,
                    'compra_id' => $compra->id,
                    'contrato_id' => null,
                    'unidade_origem_id' => $compra->unidade_origem_id,
                    'unidade_id' => session('user_ug_id'),
                    'modalidade_id' => $compra->modalidade_id,
                    'numero_ano' => $compra->numero_ano,
                    'tipo_empenhopor' => $tipo_empenhopor_id,
                    'fornecedor_empenho_id' => $request->fornecedor_empenho_id,
                ]);

                DB::commit();

                return redirect(route(
                    'empenho.minuta.etapa.item',
                    ['minuta_id' => $minutaEmpenho->id, 'fornecedor_id' => $request->fornecedor_empenho_id]
                ));
            } catch (Exception $exc) {
                DB::rollback();
                $this->inserirLogCustomizado('minuta-empenho', 'error', $exc);
                return redirect('/empenho/buscacompra')->with('alert-danger', 'Erro ao buscar a compra!');
            }
        }
    }

    public function updateOrCreateCompra($request)
    {
        if ($request->lei != null){
            $compraPertenceLei14133Derivadas = resolve(AmparoLegalService::class)
                ->existeAmparoLegalEmLeiCompra($request->lei);

            if ($compraPertenceLei14133Derivadas) {
                $sisrpId = $this->retornaIdCodigoItem('Tipo Compra', 'SISRP');

                //Não atualiza compra caso SISRP e LEI14133. A compra é atualizada no gestão de atas
                if ($request->tipo_compra_id == $sisrpId) {
                    return Compra::where('unidade_origem_id', (int)$request->get('unidade_origem_id'))
                        ->where('modalidade_id', (int)$request->get('modalidade_id'))
                        ->where('numero_ano', $request->get('numero_ano'))
                        ->where('tipo_compra_id', $request->get('tipo_compra_id'))
                        ->first();
                }
            }
        }


        return Compra::updateOrCreate(
            [
                'unidade_origem_id' => (int)$request->get('unidade_origem_id'),
                'modalidade_id' => (int)$request->get('modalidade_id'),
                'numero_ano' => $request->get('numero_ano'),
                'tipo_compra_id' => $request->get('tipo_compra_id')
            ],
            [
                'unidade_subrrogada_id' => $request->get('unidade_subrrogada_id'),
                'tipo_compra_id' => $request->get('tipo_compra_id'),
                'inciso' => $request->get('inciso'),
                'lei' => $request->get('lei'),
                'artigo' => $request->get('artigo'),
                'id_unico' => $request->get('id_unico'),
                'cnpjOrgao' => $request->get('cnpjOrgao'),
                'origem' => $request->get('origem'),
                'numero_contratacao' => $request->get('numero_contratacao_original') ?? null
            ]
        );
    }

    /**
     * Buscar a compra no SIASG pelos parâmetros informados na tela de busca de compra
     * @param Request $request
     * @return mixed
     */
    public function consultaCompraSiasgAntigo(Request $request)
    {
        $modalidade             = Codigoitem::find($request->modalidade_id);
        $uasgCompra             = Unidade::find($request->unidade_origem_id);
        $codigoUasgBeneficiaria = $this->getUasgBeneficiaria($request);
        $numero_ano             = explode('/', $request->get('numero_ano'));
        $apiSiasg               = new ApiSiasg();

        $params = [
            'modalidade'       => $modalidade->descres,
            'numeroAno'        => $numero_ano[0] . $numero_ano[1],
            'uasgCompra'       => $uasgCompra->codigo,
            'uasgBeneficiaria' => $codigoUasgBeneficiaria,
            'uasgUsuario'      => session('user_ug')
        ];

        return json_decode($apiSiasg->executaConsulta('COMPRASISPP', $params));
    }

    public function consultaCompraSiasgPNCP($modalidade, $uasgCompra, $uasgUsuario, $numeroAno)
    {
        try {
            $apiSiasg = new ApiSiasg();

            $params = [
                'modalidade' => $modalidade,
                'uasgCompra' => $uasgCompra,
                'uasgUsuario' => $uasgUsuario,
                'numeroAno' => $numeroAno
            ];

            return json_decode($apiSiasg->executaConsulta('COMPRASISPP', $params));
        } catch (RequestException $e) {
            throw new Exception($e->getResponse()->getBody()->getContents());
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Método criado para Recuperar a Compra e o Contrato (qdo for minuta de Contrato)
     * @param UpdateRequest $request
     * @return array
     */
    public function getCompraContrato(UpdateRequest $request): array
    {
        $tipoEmpenho          = $request->input('tipoEmpenho');
        $numero_ano           = $request->input('numero_ano');
        $numero_compra        = $request->input('numero_compra');
        $ano_compra           = $request->input('ano_compra');
        $modalidade_id        = $request->input('modalidade_id');
        $unidade_origem_id = $unidade_compra_id    = $request->input('unidade_origem_id');
        $uasg_beneficiaria_id = $request->input('uasg_beneficiaria_id');
        $contrato_id          = $request->input('contrato_id');
        $contrato             = null;
        $uasgBeneficiaria     = !empty($uasg_beneficiaria_id)
            ? Unidade::find($uasg_beneficiaria_id)->codigo
            : null;
        $uasgUser             = session()->get('user_ug');

        if ($tipoEmpenho == 1) {
            $contrato                     = Contrato::find($contrato_id);
            $numero_ano_contrato          = explode('/', $contrato->numero);

            //caso o contrato seja de uma minuta, pega os 4 primeiros digitos do ano
            $numero_ano_contrato[1]       = $numero_ano_contrato[1] ?? substr($contrato->numero, 0, 4);

            $numero_ano                   = $contrato->licitacao_numero;
            [$numero_compra, $ano_compra] = explode('/', $contrato->licitacao_numero);
            $unidade_origem_id            = $contrato->unidadeorigem_id;
            $unidade_compra_id            = $contrato->unidadecompra_id;
            $modalidade_id                = $contrato->modalidade->id;
            $uasgUser                     = Unidade::find($unidade_origem_id)->codigo;
            // unidadebeneficiaria_id está sendo atualizado em tempo real na tabela contratos
            // mas não salva quando envia o campo null
            // em contratohistorico mantém a unidade original e salva corretamente
            if (!empty($contrato->getInstrumentoInicial()->unidadebeneficiaria_id)) {
                $uasgBeneficiaria = Unidade::find($contrato->unidadebeneficiaria_id)->codigo;
            }
        }

        $numeroAno  = str_replace('/', '', $numero_ano);

        // MERCADOGOV
        if($tipoEmpenho == 4){
            // INEX
            $modalidade_id = 75;
            $uasgCompra = null;
        }else{
            $uasgCompra = Unidade::find($unidade_compra_id)->codigo;
        }

        $modalidade = $this->retornaDescresPorId($modalidade_id);

        $num_contratacao = $request->input('numero_contratacao');
        $retornoSiasg = $this->buscarCompra(
            $modalidade,
            $uasgCompra,
            $uasgUser,
            $uasgBeneficiaria,
            $numero_compra,
            $ano_compra,
            $num_contratacao
        );

        //se for minuta de contrato,
        //se for contrato for igual ou inferior ao ano 2023
        // e não encontrou a compra
        if (isset($contrato, $retornoSiasg)
            && $tipoEmpenho == 1
            && $numero_ano_contrato[1] < 2024
            && $contrato->created_at < '2024-01-01'
            && is_null($retornoSiasg->data)
        ) {
            $uasgUser             = session()->get('user_ug');
            $request->request->set('unidadeorigem_contrato_id', session()->get('user_ug_id'));
            $retornoSiasg = $this->buscarCompra(
                $modalidade,
                $uasgCompra,
                $uasgUser,
                $uasgBeneficiaria,
                $numero_compra,
                $ano_compra
            );
        }
        return array($contrato, $retornoSiasg);
    }

    /**
     * Buscar a compra no SIASG pelo contrato informado na tela de busca de compra
     * @param $request
     * @param $contrato
     * @return mixed
     */
    private function consultaContratoSiasg($request, $contrato)
    {
        $codigoUasgBeneficiaria = $this->getUasgBeneficiaria($request);

        $params = [
            'modalidade'       => $contrato->modalidade->descres,
            'numeroAno'        => $contrato->licitacao_numero_limpa,
            'uasgCompra'       => $contrato->unidadecompra->codigo,
            'uasgUsuario'      => $contrato->unidadeorigem->codigo,
            'uasgBeneficiaria' => $codigoUasgBeneficiaria,
        ];

        $apiSiasg = new ApiSiasg();
        return json_decode($apiSiasg->executaConsulta('COMPRASISPP', $params));
    }

    public function verificaPermissaoUasgCompra($compraSiasg, $request, $unidade_origem_id = null)
    {
        $unidade_autorizada_id = null;
        $tipoCompra = $compraSiasg->data->compraSispp->tipoCompra;
        $subrrogada = $compraSiasg->data->compraSispp->subrogada;
        $unidade_id = session('user_ug_id');
        $codigo_unidade = session('user_ug');

        // caso esteja na verificação de compra da minuta de contrato
        if ($unidade_origem_id) {
            $unidade_id = $unidade_origem_id;
            $codigo_unidade = Unidade::where('id', $unidade_origem_id)->first()->codigo;

            if ($tipoCompra == $this::SISPP) {
                if ($subrrogada != '000000') {
                    //A unidade origem do contrato é igual à unidade sub-rogada da compra, ou
                    if ($subrrogada == $codigo_unidade) {
                        return $unidade_id;
                    }

                    //A unidade atual do contrato (unidade logada) é igual à unidade sub-rogada da compra.
                    if ($subrrogada == session('user_ug')) {
                        return session('user_ug_id');
                    }
                }

                // A unidade origem do contrato ($unidade_origem_id)
                // é igual à unidade origem da compra ($request->unidade_origem_id)
                if ($request->unidade_origem_id == $unidade_origem_id) {
                    return $unidade_id;
                }
                // A unidade atual  do contrato (unidade logada) (session('user_ug_id'))
                // é igual à unidade origem da compra ($request->unidade_origem_id)
                if ($request->unidade_origem_id == session('user_ug_id')) {
                    return $unidade_id;
                }
            }
        }

        if ($tipoCompra == $this::SISPP) {
            if ($subrrogada != '000000') {
                ($subrrogada == $codigo_unidade) ? $unidade_autorizada_id = $unidade_id : '';
            } else {
                ($request->unidade_origem_id == $unidade_id)
                    ? $unidade_autorizada_id = $request->unidade_origem_id : '';
            }
        } else {
            $unidade_autorizada_id = $unidade_id;
        }

        return $unidade_autorizada_id;
    }

    public function verificaPermissaoUasgCompraParamContrato($contrato)
    {
        $unidade_autorizada = null;

        $uasgdescentralizada = $contrato->unidadesdescentralizadas()
            ->where('unidade_id', session('user_ug_id'))
            ->where('contrato_id', $contrato->id)
            ->first();

        if ($uasgdescentralizada) {
            $unidade_autorizada = $uasgdescentralizada->unidade_id;
        } else {
            $uasgContrato = Unidade::find($contrato->unidade_id);

            ($uasgContrato->id == session('user_ug_id')) ? $unidade_autorizada = session('user_ug_id') : '';
        }

        return $unidade_autorizada;
    }

    public function montaParametrosCompra($compraSiasg, $request): void
    {
        $unidade_subrogada = $compraSiasg->data->compraSispp->subrogada;
        $request->request->set(
            'unidade_subrrogada_id',
            ($unidade_subrogada <> '000000') ? (int)$this->buscaIdUnidade($unidade_subrogada) : null
        );

        $origem = 2; // siasg

        if (isset($compraSiasg->data->api_origem)) {
            switch ($compraSiasg->data->api_origem) {
                case 'novo_divulgacao_compra':
                    $origem = 1;
                    break;
                case 'pncp':
                    $origem = 3;
                    break;
                default:
                    $origem = 2;
                    break;
            }
        }

        $request->request->set('origem', $origem);
        $request->request->set('tipo_compra_id', $this->buscaTipoCompra($compraSiasg->data->compraSispp->tipoCompra));
        $request->request->set('inciso', $compraSiasg->data->compraSispp->inciso);
        $request->request->set('lei', $compraSiasg->data->compraSispp->lei);
        $request->request->set('id_unico', @$compraSiasg->data->compraSispp->idUnico);
        $request->request->set('cnpjOrgao', @$compraSiasg->data->compraSispp->cnpjOrgao);

        $artigoCompra = empty($compraSiasg->data->compraSispp->artigo) ? null : $compraSiasg->data->compraSispp->artigo;
        $request->request->set('artigo', $artigoCompra);
    }

    public function buscaTipoCompra($descres)
    {
        $tipocompra = Codigoitem::wherehas('codigo', function ($q) {
            $q->where('descricao', '=', 'Tipo Compra');
        })
            ->where('descres', '0' . $descres)
            ->first();
        return $tipocompra->id;
    }

    public function verificaCompraExisteParamContrato($contrato)
    {
        $uasgCompra = Unidade::find($contrato->unidadeorigem_id);

        $compra = Compra::where('unidade_origem_id', $uasgCompra->id)
            ->where('modalidade_id', $contrato->modalidade_id)
            ->where('numero_ano', $contrato->numero)
            ->first();

        return $compra;
    }

    public function gravaMinutaEmpenho($params)
    {
        $minutaEmpenho = new MinutaEmpenho();
        $minutaEmpenho->unidade_id = $params['unidade_id'];
        $minutaEmpenho->compra_id = $params['compra_id'];
        $minutaEmpenho->contrato_id = $params['contrato_id'];
        $minutaEmpenho->situacao_id = $params['situacao_id'];
        $minutaEmpenho->informacao_complementar = $this->retornaInfoComplementar($params);
        $minutaEmpenho->tipo_empenhopor_id = $params['tipo_empenhopor'];
        $etapa = 2;

        if (isset($params['fornecedor_compra_id'])) {
            $minutaEmpenho->fornecedor_empenho_id = $params['fornecedor_compra_id'];
            $minutaEmpenho->numero_contrato = $params['numero_contrato'];
            $etapa = 3;
        }

        $minutaEmpenho->etapa = $etapa;

        $minutaEmpenho->save();
        return $minutaEmpenho;
    }

    public function retornaInfoComplementar($params)
    {
        $compra = Compra::find($params['compra_id']);

        $numeroAno = str_replace("/", "", $params['numero_ano']);
        $modalide = Codigoitem::find($params['modalidade_id']);
        $unidade_compra = Unidade::find($compra->unidade_origem_id);
        $unidade = Unidade::find($params['unidade_id']);

        return @$unidade_compra->codigo . $modalide->descres . $numeroAno . " - UASG Minuta: " . $unidade->codigo;
    }

    public function update(UpdateRequest $request)
    {
        $this->setRequestFaixa($request);
        $redirect_location = parent::updateCrud($request);
        return $redirect_location;
    }

    private function getUasgBeneficiaria($request)
    {
        $codigoUasgBeneficiaria = null;
        $idUasgBeneficiaria = $request->uasg_beneficiaria_id ?: null;

        if ($idUasgBeneficiaria) {
            $uasgBeneficiaria = Unidade::find($idUasgBeneficiaria);
            $codigoUasgBeneficiaria = $uasgBeneficiaria->codigo;
            if ($codigoUasgBeneficiaria == '') {
                $codigoUasgBeneficiaria = null;
            }
        }
        return $codigoUasgBeneficiaria;
    }

    /**
     * Atualizar Compra
     * Método criado para unificar neste arquivo as chamadas de atualização de compra
     * @param $request
     * @param $retornoSiasg
     * @return mixed
     */
    public function atualizarCompra($request, $retornoSiasg)
    {
        $compra = $this->updateOrCreateCompra($request);

        //COMPRA SISPP
        if ($retornoSiasg->data->compraSispp->tipoCompra == '1') {
            $this->gravaParametroItensdaCompraSISPP($retornoSiasg, $compra, null, $request->get('unidadeorigem_contrato_id'));
            return $compra;
        }

        //COMPRA SISRP: tipoCompra == '2'

        //Caso seja lei 14133 e a compra já exista, não grava os itens.
        //Ela é atualizada no gestão de Atas

        $compraPertenceLei14133Derivadas = resolve(AmparoLegalService::class)
            ->existeAmparoLegalEmLeiCompra($retornoSiasg->data->compraSispp->lei);

        //Caso seja lei 14133.
        if ($compraPertenceLei14133Derivadas) {
            //Caso a compra não exista, retorna erro.
            if ($compra === null) {
                return 'LEI14133SISRP';
            }
            $this->atribuirCatmatpdmToItems($compra, $retornoSiasg);
        }

        if (!$compraPertenceLei14133Derivadas) {
            $this->gravaParametroItensdaCompraSISRP($retornoSiasg, $compra, $request->get('unidadeorigem_contrato_id'));
        }

        return $compra;
    }

    /**
     * Buscar o tipo de compra ou contrato
     * @return mixed
     */
    public function getEmpenhopor($tipo)
    {
        return Codigoitem::where('descricao', '=', $tipo)
            ->where('descres', strtoupper(substr($tipo, 0, 3)))
            ->first();
    }

    /**
     * verifica itens da SISRP DA 14133 QUE NÃO POSSUEM ITEMS COM CATMATSERITENS VINCULADO A CATMATPDM
     * @param $compra
     * @param $retornoSiasg
     * @return void
     */
    public function atribuirCatmatpdmToItems($compra, $retornoSiasg): void
    {
        $itensSemPdm = $compra->getItensMaterialSemCatmatpdm();
        if (isset($retornoSiasg->data->linkSisrpCompleto)) {
            $consultaCompra = new ApiSiasg();
            $linkSisrpCompleto = $retornoSiasg->data->linkSisrpCompleto;
            foreach ($itensSemPdm as $index => $item) {
                foreach ($linkSisrpCompleto as $urlItem) {
                    if (strpos($urlItem->linkSisrpCompleto, "&numeroItem=" . $item->numero) !== false) {
                        $dadosItemCompra = ($consultaCompra->consultaCompraByUrl($urlItem->linkSisrpCompleto));
                        $catmatpdm_id = $this->getCatmatpdm_id((object)$dadosItemCompra['data']['dadosAta']);
                        Catmatseritem::where('id', $item->catmatseritem_id)->update(['catmatpdm_id' => $catmatpdm_id]);
                        break;
                    }
                }
            }
            return;
        }

        //caso os itens não venham do SIASG
        foreach ($itensSemPdm as $index => $item) {
            $pdm = CNBS::getPdmMaterialByCode($item->codigo_siasg);
            if (isset($pdm)) {
                $obj = new stdClass;
                $obj->tipo = 'M';
                $obj->pdm = str_pad($pdm->codigoPdm, 5, 0, STR_PAD_LEFT);
                $obj->nomePdm = $pdm->nomePdm;
                $catmatpdm_id = $this->getCatmatpdm_id($obj);
                Catmatseritem::where('id', $item->catmatseritem_id)->update(['catmatpdm_id' => $catmatpdm_id]);
            }
        }
    }

    /**
     * Seta retorno siasg fake para compras da sisrp da 14133
     * @param $compra
     * @return object
     */
    public function getRetornoSiasg($compra): object
    {
        return (object)[
            'data' => (object)[
                'compraSispp' => (object)[
                    'tipoCompra' => '2',
                    'inciso' => $compra['inciso'],
                    'lei' => $compra['lei'],
                    'artigo' => $compra['artigo'],
                    'subrogada' => '000000',
                    'idUnico' => $compra['id_unico'],
                    'cnpjOrgao' => $compra['cnpjOrgao'],
                    'ano' => substr($compra['numero_ano'], 6, 6),
                ]
            ],
            'messagem' => 'Sucesso',
            'codigoRetorno' => 200
        ];
    }

    private function verificaPermissaoSisrp14133($request, $compra): bool
    {
        $unidadesIntegrantes =
            CompraItem::join('compra_item_unidade as ciu', 'compra_items.id', '=', 'ciu.compra_item_id')
                ->where('compra_items.compra_id', '=', $compra->id)
                ->groupBy('ciu.unidade_id')
                ->pluck('ciu.unidade_id');


        $verificacao = $unidadesIntegrantes->where('unidade_id', '=', session('user_ug_id'));

        return $verificacao->isEmpty();
    }
}
