<?php

namespace App\Http\Controllers\Apropriacao;

use App\Models\AntecipaGov;
use App\Models\ApropriacaoContratoFaturas;
use App\Models\ApropriacaoFaturas;
use App\Models\Contrato;
use App\Models\Contratofatura;
use App\Models\ContratoFaturaEmpenhosApropriacao;
use App\Models\ContratoFaturaMesAno;
use App\Models\Empenho;
use App\Models\Execsfsituacao;
use App\Models\Naturezasubitem;
use App\Models\SfAcrescimo;
use App\Models\SfCentroCusto;
use App\Models\SfDadosPgto;
use App\Models\SfDeducao;
use App\Models\SfDocOrigem;
use App\Models\SfDomicilioBancario;
use App\Models\SfItemRecolhimento;
use App\Models\SfPadrao;
use App\Models\SfParcela;
use App\Models\SfPco;
use App\Models\SfPcoItem;
use App\Models\SfPredoc;
use App\Models\SfRelItemDeducao;
use App\Models\Unidade;
use App\Rules\NaoAceitarFeriado;
use App\Rules\NaoAceitarFimDeSemana;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\Formatador;
use App\Forms\SfDadosBasicosForm;
use App\Forms\SfDadosPagamentoForm;
use App\Models\SfDadosBasicos;
use App\Models\Sfrelitemvlrcc;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Route;
use Exception;
use Yajra\DataTables\Html\Builder;
use App\Http\Traits\BuscaCodigoItens;
use App\Models\ContratoFaturaEmpenho;
use App\Models\ExecsfsituacaoCamposVariaveis;
use App\Models\Fornecedor;
use Illuminate\Support\Facades\Validator;

class AlteracaoApropriacaoController extends Controller
{
    use Formatador, FormBuilderTrait, BuscaCodigoItens;

    /**
     * @var DadosAbasAlteracaoApropriacao
     */
    private $dadosAbasAlteracaoApropriacao;

    private $htmlBuilder = '';

    private $apropriacao = '';

    private $contrato_id = '';

    /**
     * @var FaturaController
     */
    private $faturaController;

    public function __construct(DadosAbasAlteracaoApropriacao $dadosAbasAlteracaoApropriacao, Builder $htmlBuilder)
    {
        $apropriacao_id = request('apropriacao_id') ?: Route::current()->parameter('apropriacao_id');

        if ($apropriacao_id) {
            $apropriacao = ApropriacaoFaturas::where('id', $apropriacao_id)->first();
            $this->apropriacao = $apropriacao;

            if (!empty($apropriacao->faturas) && isset($apropriacao->faturas[0])) {
                $this->contrato_id = $apropriacao->faturas[0]->contrato_id;
            } else {
                $this->contrato_id = null;
            }
        }

        $this->dadosAbasAlteracaoApropriacao = $dadosAbasAlteracaoApropriacao;
        $this->htmlBuilder = $htmlBuilder;
        $this->faturaController = new FaturaController($this->htmlBuilder);
    }

    public function index(Request $request)
    {
        if (!backpack_user()->can('apropriar_fatura')) {
            abort('403', config('app.erro_permissao'));
        }

        $sf_padrao_id = Route::current()->parameter('sf_padrao_id');

        if (!session()->has('arrPadraoAlteracaoApropriacao')) {
            $this->faturaController->adicionaPadraoNaSessao($sf_padrao_id, false);
        }
        $sfPadrao = SfPadrao::find($sf_padrao_id);

        $numaba = Route::current()->parameter('numaba') ?? '';

        $situacaoApropriacao = $this->recuperarSituacaoApropriacao($this->apropriacao->id);

        //Altera data emissão quando a situação e andamento.
        if ($situacaoApropriacao == 'AND' || $situacaoApropriacao == 'ERR') {
            $sfPadrao->atualizaDtEmissaoApropriacao($sf_padrao_id);
            SfDadosBasicos::where('sfpadrao_id', $sf_padrao_id)
                ->where('confirma_dados', false)
                ->update(['dtemis' => now()]);
        }

        /***************************************************************************************
         ********************************** CAMPOS DH PADRAO (TIPO DH, ANO, UG) ****************/

        $arrayTiposDHPadrao = DB::table('execsfsituacao_tipodoc as etc')
            ->join('execsfsituacao as es', 'etc.execsfsituacao_id', '=', 'es.id')
            ->join('codigoitens as ci', 'etc.tipodoc_id', '=', 'ci.id')
            ->where('es.status', true)
            ->select('ci.descres', 'ci.descricao')
            ->orderByRaw("
                    CASE
                        WHEN ci.descres ILIKE 'NP%' THEN 1
                        WHEN ci.descres ILIKE 'RP%' THEN 2
                        ELSE 3
                    END
            ")
            ->orderBy('ci.descres')
            ->pluck('descricao', 'descres')
            ->toArray();

        $unidadeDhPadrao = Unidade::where('codigo', str_pad($sfPadrao->codugemit, 6, "0", STR_PAD_LEFT))->first();

        $arrayCamposDHPadrao = [
            'arrayTiposDHPadrao' => $arrayTiposDHPadrao,
            'anodh' => $sfPadrao->anodh,
            'nomeugemit' => $unidadeDhPadrao->nome,
            'codugemit' => $sfPadrao->codugemit,
            'codtipodh' => $sfPadrao->codtipodh,
            'utiliza_custos' => $unidadeDhPadrao->utiliza_custos
        ];

        /***************************************************************************************
         ********************************** ABA DADOS BASICOS **********************************/

        //$sfDadosBasicos = $this->dadosAbasAlteracaoApropriacao->criarDadosAbaDadosBasicos();

//        $sfDadosBasicosForm = \FormBuilder::create(SfDadosBasicosForm::class, [
//            'url' => route('fatura.form.dados-basicos', [$sfDadosBasicos->sf_padrao_id, $this->apropriacao->id]),
//
//            'method' => 'POST',
//            'model' => $sfDadosBasicos->modelSfDadosBasicos,
//            'novalidate' => 'novalidate',
//            'sfDadosBasicos' => $this->carregaUnidadeDescentralizadaContrato(),
//            'attr' => ['id' => 'sfDadosBasicosForm']
//        ]);

        /***************************************************************************************
         ****************************** ABA PRINCIPAL COM ORÇAMENTO ****************************/

//        $sfPco = SfPco::where('sfpadrao_id', $sf_padrao_id)
//            ->orderBy('id', 'asc')
//            ->get();
//        $valorTotalAbaPco = 0;

//        $sfpcoForm = $this->dadosAbasAlteracaoApropriacao->criarDadosAbaPco(
//            $sfPco,
//            $valorTotalAbaPco,
//            $sf_padrao_id,
//            $sfPadrao->codtipodh,
//            $this->apropriacao->id
//        );

        /***************************************************************************************
         ****************************** ABA DADOS DE PAGAMENTO *********************************/

        /*$sfPreDoc = SfPredoc::with('sfdomiciliobancarioFavo', 'sfdomiciliobancarioPgto')
        ->whereHas('sfdadospgto', function ($query) use ($sf_padrao_id) {
            $query->where('sfpadrao_id', $sf_padrao_id);
        })->first();

        $arr_favorecidos = SfDadosPgto::where('sfpadrao_id', $sf_padrao_id)->get();


        $formDadosPagamento =  $this->dadosAbasAlteracaoApropriacao->criarDadosAbaDadosPagamento(
            $arr_favorecidos,
            $this->contrato_id,
            $this->apropriacao->id,
            $sf_padrao_id
        );*/

        $existsSits = false;
        if ($situacaoApropriacao == 'APR') {
            $searchSits = SfPco::where('sfpadrao_id', $sf_padrao_id)
                ->whereIn('codsit',['DSP101','DSP201'])
                ->get();

            $existsSits = $searchSits->isEmpty() ? false : true;
        }

        return view(
            'backpack::mod.apropriacao.fatura-form',
            [
                //'sfDadosBasicosForm'            =>  $sfDadosBasicosForm,                //Form da aba Dados Basicos
                //'sfpcoForm'                     =>  $sfpcoForm,                         //Form da aba Pco
                //'sfPco'                         =>  $sfPco,                             //Dados da aba Pco
                //'valorTotalAbaPco'              =>  $valorTotalAbaPco,                  //Valor total da Aba da aba Pco
                //'valorTotalCentroCustoAInformar' => $valorTotalCentroCustoAInformar,     //Valor total de Centro de Custo a Informar
                'numaba'                        =>  (int) $numaba,                      //numero da aba,
                //'sfDadosPagamentoForm'          =>  $formDadosPagamento,                //Form da aba Dados de Pagamento
                //'arrFavorecidos'                =>  $arr_favorecidos,                   //Dados dos favorecidds da aba Dados de Pagamento,
                'sf_padrao_id'                  =>  $sf_padrao_id,                      //id do padrao passado pela rota
                //'totalValorDeducao' =>  $totalValorDeducao,                             //total da Aba de Deducao
                //'sfDeducaoForm' =>      $sfDeducaoForm,                                 //Form da aba Deducao
                //'sfDeducao' =>          $sfDeducao,                                     //Dados da aba Deducao
                //'sfCentroCusto'                 =>  $sfCentroCusto,                     //Dados da aba Centro de Custo
                //'sfCentroCustoForm'             =>  $sfCentroCustoForm,                 //Form da aba Centro de Custo
                'arrayCamposDHPadrao'           =>  $arrayCamposDHPadrao,               //Dados com campos de SfPadrao
                'situacaoApropriacao'           =>  $situacaoApropriacao,               //Situação da apropriação das faturas
                'idFaturasToRedirectBack'       =>  $this->apropriacao->faturas[0]->id, //id da primeira fatura da Apr
                //'situacaoUtilizaCentroCusto'    =>  $afetaCusto,
                //'sfPreDoc'                      =>  $sfPreDoc,                          //Retorna pré-doc da apropriação
                'existsSits'                    =>  $existsSits                         //Verificação se existe DSP101, DSP201 na sfPco
            ]
        );
    }

    public function carregarTelaApropriacao($sf_padrao_id, $apropriacao_id, $nomeAba)
    {
        switch ($nomeAba) {
            case 'pco-tab':
                return $this->geraTelaPcoTab($sf_padrao_id);
            case 'dados-basicos-tab':
                return $this->geraTelaDadosBasicosTab();
            case 'deducao-tab':
                return $this->geraTelaDeducaoTab($sf_padrao_id);
            case 'centro-custo-tab':
                return $this->geraTelaCentroCustoTab($sf_padrao_id);
            case 'dados-pagamento-tab':
                return $this->geraTelaDadosPagamentoTab($sf_padrao_id, $apropriacao_id);
        }
    }

    public function alterarAbaDadosBasicos(Request $request)
    {
        $sf_padrao_id = $request->sf_padrao_id;

        $formValidation = $this->form(SfDadosBasicosForm::class);

        if (!$formValidation->isValid()) {

            return response()->json([
                'status' => 'error',
                'data' => $formValidation->getErrors()
            ]);
        }

        //GRAVA ALTERAÇÃO DOS CAMPOS DA ABA 1 - DADOS BASICOS
        $sfDadosBasicos = SfDadosBasicos::where('sfpadrao_id', $sf_padrao_id)->get()->first();
        $sfDadosBasicos->codugpgto = $request->codugpgto;
        $sfDadosBasicos->txtobser = $request->txtobser;
        $sfDadosBasicos->txtinfoadic = $request->txtinfoadic;
        $sfDadosBasicos->dtemis = $request->dtemis;
        $sfDadosBasicos->dtateste = $request->dtateste;
        $sfDadosBasicos->dtvenc = $request->dtvenc;
        $sfDadosBasicos->txtprocesso = $request->txtprocesso;
        $sfDadosBasicos->vlrtaxacambio = $this->dadosAbasAlteracaoApropriacao->formatarVlrParaBanco(
            $request->vlrtaxacambio
        );
        $sfDadosBasicos->confirma_dados = true;

        $sfDadosBasicos->save();

        \Alert::success('Dados da aba alterados com sucesso')->flash();

        return response()->json([
            'status' => 'success',
            'data' => []
        ]);
    }

    public function alterarAbaPco(Request $request)
    {
        $origin_request = clone $request;
        $sf_padrao_id = $request->sf_padrao_id;
        $arrResult = $this->validarFormAbaPco($request, $sf_padrao_id);

        if (!$arrResult['isValid']) {
            return response()->json([
                'status' => 'error',
                'data' => $arrResult['form']->getErrors(),
                'mensagemError' => 'Dados obrigatórios não preenchidos! ' . ($arrResult['has_situations'] ? $arrResult['arr_situations'] : '') . ' Favor verificar.'
            ]);
        }

        if ($arrResult['isValid']) {
            DB::beginTransaction();

            foreach ($request->get('sfpco') as $key => $sfpco_current) {
                try {

                    $sfpco = SfPco::find($sfpco_current);
                    #salva os campos variaveis pertencentes a tabela sfpco
                    $sfpco->numclassd = (int)str_replace(".", '', $origin_request->get('numclassd')[$sfpco->id] ?? '');
                    $sfpco->numclasse = (int)str_replace(".", '', $origin_request->get('numclasse')[$sfpco->id] ?? '');
                    $sfpco->txtinscrd = str_replace(".", '', $origin_request->get('txtinscrd')[$sfpco->id] ?? '');
                    $sfpco->txtinscre = str_replace(".", '', $origin_request->get('txtinscre')[$sfpco->id] ?? '');
                    $sfpco->indrtemcontrato = (bool)$origin_request->get('indrtemcontrato')[$key];
                    $sfpco->confirma_dados = true;
                    $sfpco->repetir_conta_empenho = (bool)$origin_request->get('repetircontaempenho')[$key];
                    $sfpco->despesaantecipada = (bool)$origin_request->get('despesaantecipada')[$key];

                    $sfpco->save();

                    DB::commit();
                } catch (\Exception $exc) {
                    DB::rollBack();
                    \Alert::error('Erro na alteração dos dados da aba')->flash();
                    redirect()->back()->withErrors($arrResult['form']->getErrors())->withInput();
                }
            }

            $this->salvarDadosSfPcoItem(
                $request->get('sfpcoitem'),
                $origin_request,
                $sf_padrao_id,
                $arrResult['form']
            );

            #salvando dados de sfParcela
            if ($request->get('sfparcela')) {
                $this->salvarDadosSfParcela($request, $sf_padrao_id, $arrResult['form']);
            }

            #altera numAbaLiberada para a proxima aba na sessao
            \Alert::success('Dados da aba alterados com sucesso')->flash();
            return response()->json(['status' => 'success']);
        } else {

            if ($origin_request->get('confirma-dados-pco') !== null) {

                $this->salvarDadosSfPcoItem(
                    $request->get('sfpcoitem'),
                    $origin_request,
                    $sf_padrao_id,
                    $arrResult['form']
                );

                #salvando dados de sfParcela
                if ($request->get('sfparcela')) {
                    $this->salvarDadosSfParcela($request, $sf_padrao_id, $arrResult['form']);
                }
            }

            \Alert::error(
                'Dados obrigatórios não preenchidos! ' .
                ($arrResult['has_situations'] ? $arrResult['arr_situations'] : '') .
                ' \n Favor verificar.'
            )->flash();

            redirect()->back()->withErrors($arrResult['form']->getErrors())->withInput();
        }

        //----------------------------------------------------

        return redirect()->route(
            'alterar.antes.apropriar.fatura',
            [
                'sf_padrao_id' => $sf_padrao_id,
                'apropriacao_id' => $this->apropriacao->id,
                'numaba' => 2
            ]
        );
    }

    private function updateSfPcoIndrtemcontrato($request)
    {
        $sfpco = SfPco::find($request->idSfPco);
        $sfpco->repetir_conta_empenho = $request->repetirContasEmpenhos;
        $sfpco->save();

        $dataToUpdate['indrtemcontrato'] = $request->indrtemcontrato;

        if ($request->indrtemcontrato === '0') {
            $dataToUpdate = array_merge($dataToUpdate, [
                'numclassd' => 0,
                'numclasse' => 0,
                'txtinscre' => null,
                'txtinscrd' => null,
            ]);
        }

        SfPco::where('sfpadrao_id', $request->sf_padrao_id)->update($dataToUpdate);
    }

    public function alterarAbaDeducao(Request $request)
    {
        $origin_request = clone $request;
        $sf_padrao_id = $request->sf_padrao_id;

        $arrResult = $this->validarFormAbaDeducao($request, $sf_padrao_id);

        if (!$arrResult['isValid']) {
            return response()->json([
                'status' => 'error',
                'data' => $arrResult['form']->getErrors(),
                'mensagemError' => 'Dados obrigatórios não preenchidos! Favor verificar.'
            ]);
        }

        if ($arrResult['isValid']) {
            DB::beginTransaction();
            foreach ($request->get('sfdeducao') as $key => $sfdeducao_current) {

                try {
                    $sfdeducao_item = SfDeducao::find($sfdeducao_current);
                    $sfdeducao_item->dtvenc = $origin_request->get('sfdeducaodtvenc')[$key];
                    $sfdeducao_item->codsit = $origin_request->get('sfdeducaocodsit')[$key];
                    $sfdeducao_item->codugpgto = (int)$origin_request->get('sfdeducaocodugpgto')[$key];
                    $sfdeducao_item->vlr = $this->dadosAbasAlteracaoApropriacao->formatarVlrParaBanco($origin_request->get('sfdeducaovlr')[$key]);
                    $sfdeducao_item->possui_acrescimo = $origin_request->get('sfdeducaopossui_acrescimo')[$key] ?? false;
                    $sfdeducao_item->confirma_dados = true;
                    $sfdeducao_item->dtpgtoreceb = $origin_request->get('sfdeducaodtpgtoreceb')[$key];
                    $sfdeducao_item->txtinscra = str_replace(".", '', $origin_request->get('txtinscra')[$sfdeducao_item->id] ?? '');
                    $sfdeducao_item->txtinscrb = str_replace(".", '', $origin_request->get('txtinscrb')[$sfdeducao_item->id] ?? '');
                    $sfdeducao_item->save();

                    $this->salvaPredocDeducao($sfdeducao_item, $origin_request, $key);

                    DB::commit();
                } catch (\Exception $exc) {
                    DB::rollBack();
                    \Alert::error('Erro na alteração dos dados da aba')->flash();
                    redirect()->back()->withErrors($arrResult['form']->getErrors())->withInput();
                    //----------------------------------------------------
                }
            }

            $sf_deducao_vlr = SfDeducao::where('sfpadrao_id', $sf_padrao_id)->get()->sum('vlr');
            $sfDadosBasicos = SfDadosBasicos::where('sfpadrao_id', $sf_padrao_id)->first();
            if ($sfDadosBasicos) {
                $sfdados_pgto = SfDadosPgto::where('sfpadrao_id', $sf_padrao_id)->first();
                $sfdados_pgto->vlr = $sfDadosBasicos->vlr - $sf_deducao_vlr;
                $sfdados_pgto->save();
            }

            if (!empty($request->get('sfitemrecolhedor'))) {
                $this->salvarDadosSfItemRecolhimento(
                    $request->get('sfitemrecolhedor'),
                    $origin_request,
                    $sf_padrao_id,
                    $arrResult['form']
                );
            }

            if (!empty($request->get('sfitemacrescimo'))) {
                $this->salvarDadosSfItemAcrescimo(
                    $request->get('sfitemacrescimo'),
                    $request,
                    $sf_padrao_id,
                    $arrResult['form']
                );
            }
            \Alert::success('Dados da aba alterados com sucesso')->flash();

            return response()->json(['status' => 'success']);
        } else {

            if ($request->get('confirma-dados-deducao') !== null) {
                $this->confirmaDadosSituacaoDeducao($request, $sf_padrao_id);
            }

            \Alert::error('Dados obrigatórios não preenchidos! Favor verificar.')->flash();
            redirect()->back()->withErrors($arrResult['form']->getErrors())->withInput();
        }

        return redirect()->route(
            'alterar.antes.apropriar.fatura',
            [
                'sf_padrao_id' => $sf_padrao_id,
                'apropriacao_id' => $this->apropriacao->id,
                'numaba' => 3
            ]
        );
    }

    public function salvarDadosSfParcela($request, $sf_padrao_id, $sfpcoForm = null)
    {
        $arrSfParcela = $request->get('sfparcela');
        if ($arrSfParcela && is_array($arrSfParcela) && count($arrSfParcela) > 0) {
            foreach ($arrSfParcela as $key_parcela => $parcela) {
                try {
                    $sfParcela = SfParcela::find($parcela);

                    if (!$sfParcela) {
                        continue;
                    }

                    //$sfParcela->numparcela = $request->get('sfparcelanumparcela')[$key_parcela];
                    $sfParcela->dtprevista = $request->get('sfparceladtprevista')[$key_parcela];
                    $sfParcela->vlr = $this->dadosAbasAlteracaoApropriacao->formatarVlrParaBanco(
                        $request->get('sfparcelavlr')[$key_parcela]
                    );

                    $sfParcela->save();

                    DB::commit();
                } catch (\Exception $exc) {
                    $this->atualizarNumAbaLiberadaNaSessao(4, (int)$sf_padrao_id);

                    DB::rollBack();
                    \Alert::error('Erro ao salvar parcelas')->flash();

                    #retorna com erros se nao vier de uma requisição ajax
                    if ($sfpcoForm !== null) {
                        redirect()->back()->withErrors($sfpcoForm->getErrors())->withInput();
                    }
                }
            }
        }
    }

    public function salvarRemoverDadosVinculoEmpenhoDeducao($request, $sf_padrao_id, $sfdeducaoForm = null)
    {

        if($request->delete_sfrelacionamentodeducao){
            try {
                $sfitem_deducao = SfRelItemDeducao::find($request->idRelacionamento);

                if ($sfitem_deducao) {
                    $sfitem_deducao->delete();
                    DB::commit();
                }

            } catch (\Exception $exc) {
                $this->atualizarNumAbaLiberadaNaSessao(3, (int)$sf_padrao_id);

                DB::rollBack();
                \Alert::error('Erro na alteração dos dados da aba')->flash();
                #retorna com erros se nao vier de uma requisição ajax
                if ($sfdeducaoForm !== null) {
                    redirect()->back()->withErrors($sfdeducaoForm->getErrors())->withInput();
                }
                //----------------------------------------------------
            }
        }

        if (is_array($request->empenhosSelecionados) && count($request->empenhosSelecionados) > 0) {
            foreach ($request->empenhosSelecionados as $empenho) {

                try {
                    $deletadoItem = false;

                    $sfitem_deducao = SfRelItemDeducao::where('sfded_id', $empenho['idDeducao'])
                        ->where('numseqpai', $empenho['pco_numseqitem'])
                        ->where('numseqitem', $empenho['pcoitem_numseqitem'])
                        ->first();

                    if (isset($sfitem_deducao) && $sfitem_deducao->sfpcoitem_id == null) {
                        $sfitem_deducao->delete();
                        $deletadoItem = true;
                    }

                    if (!$sfitem_deducao || $deletadoItem) {
                        $sfitem_deducao = new SfRelItemDeducao();
                        $sfitem_deducao->sfded_id = $empenho['idDeducao'];
                        $sfitem_deducao->numseqpai = $empenho['pco_numseqitem'];
                        $sfitem_deducao->numseqitem = $empenho['pcoitem_numseqitem'];
                        $sfitem_deducao->sfpcoitem_id = $empenho['sfpcoitem_id'];
                        $sfitem_deducao->tipo = 'RELPCOITEM';
                        $sfitem_deducao->save();
                        DB::commit();
                    }

                } catch (\Exception $exc) {
                    $this->atualizarNumAbaLiberadaNaSessao(3, (int)$sf_padrao_id);

                    DB::rollBack();
                    \Alert::error('Erro na alteração dos dados da aba')->flash();
                    #retorna com erros se nao vier de uma requisição ajax
                    if ($sfdeducaoForm !== null) {
                        redirect()->back()->withErrors($sfdeducaoForm->getErrors())->withInput();
                    }
                    //----------------------------------------------------
                }
            }
        }
    }

    public function salvarDadosSfItemRecolhimento($arrSfitemrecolhimento, $request, $sf_padrao_id, $sfdeducaoForm = null)
    {
        if ($arrSfitemrecolhimento && is_array($arrSfitemrecolhimento) && count($arrSfitemrecolhimento) > 0) {
            foreach ($arrSfitemrecolhimento as $key => $sfitemrecolhimento_current) {
                try {
                    $sfrecolhimento_item = SfItemRecolhimento::find($sfitemrecolhimento_current);
                    if (!$sfrecolhimento_item) {
                        continue;
                    }

                    $sfrecolhimento_item->codrecolhedor = $request->get('recolhedor')[$sfitemrecolhimento_current];
                    $sfrecolhimento_item->vlr = $this->dadosAbasAlteracaoApropriacao->formatarVlrParaBanco($request->get('vlrPrincipal')[$sfitemrecolhimento_current]);
                    $sfrecolhimento_item->vlrbasecalculo = $this->dadosAbasAlteracaoApropriacao->formatarVlrParaBanco($request->get('vlrbasecalculo')[$sfitemrecolhimento_current]);
                    $sfrecolhimento_item->vlrmulta = $this->dadosAbasAlteracaoApropriacao->formatarVlrParaBanco($request->get('vlrmulta')[$sfitemrecolhimento_current]);
                    $sfrecolhimento_item->vlrjuros = $this->dadosAbasAlteracaoApropriacao->formatarVlrParaBanco($request->get('vlrjuros')[$sfitemrecolhimento_current]);
                    $sfrecolhimento_item->save();

                    DB::commit();
                } catch (\Exception $exc) {

                    $this->atualizarNumAbaLiberadaNaSessao(3, (int)$sf_padrao_id);

                    DB::rollBack();
                    \Alert::error('Erro na alteração dos dados da aba')->flash();
                    #retorna com erros se nao vier de uma requisição ajax
                    if ($sfdeducaoForm !== null) {
                        redirect()->back()->withErrors($sfdeducaoForm->getErrors())->withInput();
                    }
                    //----------------------------------------------------
                }
            }
        }
    }

    public function salvarDadosSfItemAcrescimo($arrSfitemacrescimo, $request, $sf_padrao_id, $sfdeducaoForm = null)
    {
        if ($arrSfitemacrescimo && is_array($arrSfitemacrescimo) && count($arrSfitemacrescimo) > 0) {
            foreach ($arrSfitemacrescimo as $key => $sfitemacrescimo_current) {
                try {
                    $sfacrescimo_item = SfAcrescimo::find($sfitemacrescimo_current);
                    if (!$sfacrescimo_item) {
                        continue;
                    }

                    $sfacrescimo_item->tpacrescimo = $request->get('sfacrescimotpacrescimo')[$sfitemacrescimo_current];
                    $sfacrescimo_item->numempe = $request->get('sfacrescimonumemp')[$sfitemacrescimo_current];
                    $sfacrescimo_item->codsubitemempe = (int)$request->get('sfacrescimocodsubitemempe')[$sfitemacrescimo_current];
                    $sfacrescimo_item->vlr = $this->dadosAbasAlteracaoApropriacao->formatarVlrParaBanco($request->get('sfacrescimovlr')[$sfitemacrescimo_current]);
                    $sfacrescimo_item->numclassa = (int)str_replace(".", '', $request->get('numclassa')[$sfitemacrescimo_current] ?? '');
                    $sfacrescimo_item->numclassb = (int)str_replace(".", '', $request->get('numclassb')[$sfitemacrescimo_current] ?? '');
                    $sfacrescimo_item->txtinscra = str_replace(".", '', $request->get('txtinscra')[$sfitemacrescimo_current] ?? '');
                    $sfacrescimo_item->txtinscrb = str_replace(".", '', $request->get('txtinscrb')[$sfitemacrescimo_current] ?? '');

                    $sfacrescimo_item->save();

                    DB::commit();
                } catch (\Exception $exc) {

                    $this->atualizarNumAbaLiberadaNaSessao(3, (int)$sf_padrao_id);

                    DB::rollBack();
                    \Alert::error('Erro na alteração dos dados da aba')->flash();
                    #retorna com erros se nao vier de uma requisição ajax
                    if ($sfdeducaoForm !== null) {
                        redirect()->back()->withErrors($sfdeducaoForm->getErrors())->withInput();
                    }
                    //----------------------------------------------------
                }
            }
        }
    }

    public function salvarDadosSfPcoItem($arrSfpcoitem, $request, $sf_padrao_id, $sfpcoForm = null)
    {
        if ($arrSfpcoitem && is_array($arrSfpcoitem) && count($arrSfpcoitem) > 0) {
            foreach ($arrSfpcoitem as $key => $sfpcoitem_current) {
                try {
                    $sfpc_item = SfPcoItem::find($sfpcoitem_current);
                    if (!$sfpc_item) {
                        continue;
                    }

                    $idSubelemento = $request->get('codsubitemempe')[$key] ?? null;
                    $subelemento = Naturezasubitem::where('id', $idSubelemento)->first();
                    $sfpc_item->numempe = $request->get('numempe')[$key];
                    $sfpc_item->codsubitemempe = $subelemento ? $subelemento->codigo : null;
                    $sfpc_item->subelemento_id = $subelemento ? $subelemento->id : null;

                    if ($request->repetirContasEmpenhos) {
                        $sfpc_item->numclassa = (int)str_replace(".", '', $request->repetirNumClassA ?? '');
                        $sfpc_item->numclassb = (int)str_replace(".", '', $request->repetirNumClassB ?? '');
                    } else {
                        $sfpc_item->numclassa = (int)str_replace(".", '', $request->get('numclassa')[$sfpc_item->id] ?? '');
                        $sfpc_item->numclassb = (int)str_replace(".", '', $request->get('numclassb')[$sfpc_item->id] ?? '');
                    }

                    $sfpc_item->numclassc = (int)str_replace(".", '', $request->get('numclassc')[$sfpc_item->id] ?? '');
                    $sfpc_item->txtinscra = str_replace(".", '', $request->get('txtinscra')[$sfpc_item->id] ?? '');
                    $sfpc_item->txtinscrb = str_replace(".", '', $request->get('txtinscrb')[$sfpc_item->id] ?? '');
                    $sfpc_item->txtinscrc = str_replace(".", '', $request->get('txtinscrc')[$sfpc_item->id] ?? '');

                    $sfpc_item->vlr = $this->dadosAbasAlteracaoApropriacao->formatarVlrParaBanco($request->get('vlr')[$key]);

                    $sfpc_item->save();

                    DB::commit();
                } catch (\Exception $exc) {

                    $this->atualizarNumAbaLiberadaNaSessao(2, (int)$sf_padrao_id);

                    DB::rollBack();
                    \Alert::error('Erro na alteração dos dados da aba')->flash();
                    #retorna com erros se nao vier de uma requisição ajax
                    if ($sfpcoForm !== null) {
                        redirect()->back()->withErrors($sfpcoForm->getErrors())->withInput();
                    }
                    //----------------------------------------------------
                }
            }
        }
    }

    /**
     *Valida se quando a situacao tiver o campo despesa antecipada marcada como 'SIM' se o valor total das
     *parcelas é igual ao valor total da situacao
     * @param $request
     * @return void
     */
    /*public function validarParcelasQuandoDespesaAntecipada($request){

    }*/

    public function formatFieldsRequest($request)
    {
        foreach ($request->get('sfpcoitem') as $key => $sfpcoitem_current) {
            $sfpcoitem = SfPcoItem::find($sfpcoitem_current);
            $request->request->set('numempe' . $sfpcoitem->id . $sfpcoitem->sfpco_id, $request->get('numempe')[$key]);
            $request->request->set('vlr' . $sfpcoitem->id . $sfpcoitem->sfpco_id, $request->get('vlr')[$key]);

            foreach (['numclassa', 'numclassb', 'numclassc'] as $field) {
                if ($request->has($field)) {
                    $fieldValues = $request->get($field);

                    if (isset($fieldValues[$sfpcoitem_current])) {
                        $request->request->set(
                            $field . $sfpcoitem->id . $sfpcoitem->sfpco_id,
                            $fieldValues[$sfpcoitem_current]
                        );
                    }
                }
            }
        }

        foreach ($request->get('sfpco') as $key => $sfpco_current) {
            $request->request->set('codsit' . $sfpco_current, $request->get('codsit')[$key]);
        }

        foreach ($request->get('sfpco') as $key => $sfpco_current) {
            $request->request->set('indrtemcontrato' . $key, $request->get('indrtemcontrato')[$key]);
            foreach (['numclassd', 'numclasse'] as $field) {
                if ($request->has($field)) {
                    $fieldValues = $request->get($field);

                    if (isset($fieldValues[$sfpco_current])) {
                        $request->request->set(
                            $field . $sfpco_current,
                            $fieldValues[$sfpco_current]
                        );
                    }
                }
            }
        }
    }

    protected function numeroEmpenhoPertenceAUnidade($numeroEmpenho, $sfpcoitem)
    {
        try {
            $unidade = Unidade::where('codigo', $sfpcoitem->pco->codugempe)->first();
            $empenho = Empenho::where('numero', $numeroEmpenho)->where('unidade_id', $unidade->id)->first();
            if ($empenho) {
                return true;
            }
            return false;
        } catch (\Exception $e) {
            Log::info('fun numeroEmpenhoPertenceAUnidade: ' . $e->getMessage());
            return true;
        }
    }

    public function validarFormAbaPco($request, $sf_padrao_id)
    {
        $this->formatFieldsRequest($request);
        unset($request['numempe']);
        unset($request['vlr']);
        unset($request['codsit']);

        $arr_field_pco_numempe = [];
        $arr_field_pco_codsit = [];
        $arr_field_pco_numclass = [];
        $arr_situations  = [];
        $arr_field_pco_contas = [];
        $has_situations = false;

        foreach ($request->get('sfpcoitem') as $key => $sfpcoitem_current) {
            $sfpcoitem = SfPcoItem::find($sfpcoitem_current);
            $arr_field_pco_numempe['numempe' . $sfpcoitem->id . $sfpcoitem->sfpco_id] = [
                'name' => 'numempe' . $sfpcoitem->id . $sfpcoitem->sfpco_id,
                'value' => null,
                'type' => 'text',
                'rules' => [
                    'required',
                    function ($attribute, $value, $fail) use ($sfpcoitem) {
                        if (! $this->numeroEmpenhoPertenceAUnidade($value, $sfpcoitem)) {
                            $fail('O empenho não pertence a esta unidade.');
                        }
                    },
                ],
                'label' => 'Nº do Empenho',
                'label_show' => false,
                'attr' => [
                    'id' => 'numempe' . $sfpcoitem->id . $sfpcoitem->sfpco_id,
                    'name' => 'numempe[]',
                ]
            ];
        }
        foreach ($request->get('sfpco') as $key => $sfpco_current) {
            $arr_field_pco_codsit['codsit' . $sfpco_current] = [
                'name' => 'codsit' . $sfpco_current,
                'value' => null,
                'type' => 'select',
                'rules' => 'required',
                'error_messages' => ["codsit$sfpco_current.required" => 'Campo obrigatório'],
            ];
        }

        foreach ($request->get('sfpco') as $key => $sfpco_current) {

            if ($request->get('indrtemcontrato')[$key] == "1") {
                foreach (['numclassd', 'numclasse'] as $field) {
                    if ($request->has($field) && array_key_exists($sfpco_current, $request->$field)) {
                        $arr_field_pco_numclass[$field . $sfpco_current] = [
                            'name' => $field . $sfpco_current,
                            'value' => null,
                            'type' => 'text',
                            'rules' => [
                                'required',
                            ],
                            'attr' => [
                                'id' => $field . $sfpco_current,
                                'name' => $field,
                            ],
                            'error_messages' => [
                                $field . $sfpco_current . '.required' => 'Campo obrigatório',
                            ],
                        ];

                        if (!$request->get($field . $sfpco_current)) {
                            $arr_situations[] = $request->get('codsit' . $sfpco_current);
                            $has_situations = true;
                        }
                    }
                }
            }
            foreach ($request->get('sfpcoitem') as $key => $sfpcoitem_current) {
                $sfpcoitem = SfPcoItem::find($sfpcoitem_current);

                // Verifica se o sfpcoitem está associado ao sfpco_current
                if ($sfpcoitem && $sfpcoitem->sfpco_id == $sfpco_current) {

                    foreach (['numclassa', 'numclassb', 'numclassc'] as $field) {
                        if ($request->has($field) && array_key_exists($sfpcoitem_current, $request->$field)) {
                            $arr_field_pco_contas[$field . $sfpcoitem->id . $sfpcoitem->sfpco_id] = [
                                'name' => $field . $sfpcoitem->id . $sfpcoitem->sfpco_id,
                                'value' => null,
                                'type' => 'text',
                                'rules' => [
                                    'required',
                                ],
                                'attr' => [
                                    'id' => $field . $sfpcoitem->id . $sfpcoitem->sfpco_id,
                                    'name' => $field,
                                ],
                                'error_messages' => [
                                    $field . $sfpcoitem->id . $sfpcoitem->sfpco_id . '.required' => 'Campo obrigatório'
                                ],
                            ];

                            if (!$request->get($field . $sfpcoitem->id . $sfpcoitem->sfpco_id)) {
                                $arr_situations[] = $request->get('codsit' . $sfpco_current);
                                $has_situations = true;
                            }
                        }
                    }
                }
            }
        }

        if ($has_situations) {
            $arr_situations = ' Em: ' . implode(', ', array_unique($arr_situations));
        }
        $sfpcoForm = \FormBuilder::createByArray(
            array_merge($arr_field_pco_numempe, $arr_field_pco_codsit, $arr_field_pco_numclass, $arr_field_pco_contas),
            [
                'url' => route('fatura.form.alterar-aba-pco', ['sf_padrao_id' => $sf_padrao_id, 'numaba' => 2]),
                'method' => 'POST',
                'novalidate' => 'novalidate'
            ]
        );

        if (!$sfpcoForm->isValid()) {
            $this->atualizarNumAbaLiberadaNaSessao(2, (int) $sf_padrao_id);

            return [
                'form' => $sfpcoForm,
                'isValid' => false,
                'has_situations' => $has_situations,
                'arr_situations' => $arr_situations,
            ];
        }
        return [
            'form' => $sfpcoForm,
            'isValid' => true
        ];
    }

    public function formatFieldsDeducaoRequest($request)
    {
        if ($request->get('sfitemrecolhedor') != null) {

            foreach ($request->get('sfitemrecolhedor') as $key => $sfdeducaoitem_current) {
                $sfitemrecolhedor = SfItemRecolhimento::find($sfdeducaoitem_current);
                if (!$sfitemrecolhedor) {
                    continue;
                }
                $request->request->set('recolhedor' . $sfitemrecolhedor->sfded_id . $sfitemrecolhedor->id, $request->get('recolhedor')[$sfdeducaoitem_current]);
                $request->request->set('vlrbasecalculo' . $sfitemrecolhedor->sfded_id . $sfitemrecolhedor->id, $request->get('vlrbasecalculo')[$sfdeducaoitem_current]);
                $request->request->set('vlrPrincipal' . $sfitemrecolhedor->sfded_id . $sfitemrecolhedor->id, $request->get('vlrPrincipal')[$sfdeducaoitem_current]);
            }

        }

        if ($request->get('sfitemacrescimo') != null) {
            foreach ($request->get('sfitemacrescimo') as $key => $sfacrescimoitem_current) {
                $sfitemacrescimo = SfAcrescimo::find($sfacrescimoitem_current);
                if (!$sfitemacrescimo) {
                    continue;
                }
                $request->request->set('numclassa' . $sfitemacrescimo->sfded_id . $sfitemacrescimo->id, $request->get('numclassa')[$sfacrescimoitem_current]);
            }
        }

        if ($request->get('sfdeducao') != null) {
            foreach ($request->get('sfdeducao') as $key => $sfdeducaoitem_current) {
                $sfDeducao = SfDeducao::find($sfdeducaoitem_current);

                if (!$sfDeducao) {
                    continue;
                }

                if (isset($request->get('txtinscra')[$key])) {
                    $request->request->set('txtinscra' . $sfDeducao->id, $request->get('txtinscra')[$key]);
                }

                if (isset($request->get('txtinscrb')[$key])) {
                    $request->request->set('txtinscrb' . $sfDeducao->id, $request->get('txtinscrb')[$key]);
                }

                $request->request->set('sfdeducaodtpgtoreceb' . $sfDeducao->id, $request->get('sfdeducaodtpgtoreceb')[$key]);
                $request->request->set('sfdeducaodtvenc' . $sfDeducao->id, $request->get('sfdeducaodtvenc')[$key]);

                if (isset($request->get('sfpredoctipo')[$sfDeducao->id]) && $request->get('sfpredoctipo')[$sfDeducao->id] != null) {

                    switch ($request->get('sfpredoctipo')[$sfDeducao->id]) {
                        case 'predocdar':
                            $request->request->set('sfpredocdtprdoapuracao' . $sfDeducao->id, $request->get('sfpredocdtprdoapuracao')[$sfDeducao->id]);
                            $request->request->set('sfpredocreferencia' . $sfDeducao->id, $request->get('sfpredocreferencia')[$sfDeducao->id]);
                            $request->request->set('sfpredoctxtobser' . $sfDeducao->id, $request->get('sfpredoctxtobser')[$sfDeducao->id]);
                            $request->request->set('sfpredoccodugtmdrserv' . $sfDeducao->id, $request->get('sfpredoccodugtmdrserv')[$sfDeducao->id]);
                            $request->request->set('sfpredocnumnf' . $sfDeducao->id, $request->get('sfpredocnumnf')[$sfDeducao->id]);
                            $request->request->set('sfpredocnumsubserienf' . $sfDeducao->id, $request->get('sfpredocnumsubserienf')[$sfDeducao->id]);
                            $request->request->set('sfpredocnumaliqnf' . $sfDeducao->id, $request->get('sfpredocnumaliqnf')[$sfDeducao->id]);
                            $request->request->set('sfpredocnumsubserienf' . $sfDeducao->id, $request->get('sfpredocnumsubserienf')[$sfDeducao->id]);
                            $request->request->set('sfpredoctxtserienf' . $sfDeducao->id, $request->get('sfpredoctxtserienf')[$sfDeducao->id]);
                            $request->request->set('sfpredocdtemisnf' . $sfDeducao->id, $request->get('sfpredocdtemisnf')[$sfDeducao->id]);
                            $request->request->set('sfpredoccodmuninf' . $sfDeducao->id, $request->get('sfpredoccodmuninf')[$sfDeducao->id]);
                            break;
                        case 'predocdarf':
                            $request->request->set('sfpredocdtprdoapuracao' . $sfDeducao->id, $request->get('sfpredocdtprdoapuracao')[$sfDeducao->id]);
                            $request->request->set('sfpredoctxtobser' . $sfDeducao->id, $request->get('sfpredoctxtobser')[$sfDeducao->id]);
                            $request->request->set('sfpredoccodtipodarf' . $sfDeducao->id, $request->get('sfpredoccodtipodarf')[$sfDeducao->id]);
                            break;
                        default:
                            $request->request->set('sfpredocdtprdoapuracao' . $sfDeducao->id, $request->get('sfpredocdtprdoapuracao')[$sfDeducao->id]);
                            break;
                    }
                }
            }
        }
    }

    public function confirmaDadosSituacaoDeducao($request, $sf_padrao_id)
    {
        parse_str($request->input('formData'), $dadosFormSfDeducaoArray);
        $request->merge($dadosFormSfDeducaoArray);
        $this->salvaDadosRelacionadosDeducao($request, $sf_padrao_id);
    }

    public function validarFormAbaDeducao($request, $sf_padrao_id)
    {
        $this->formatFieldsDeducaoRequest($request);
        $arr_field_deducao_recolhedor = [];
        $arr_field_deducao_codsit = [];
        $arr_field_deducao = [];
        $arr_field_predoc = [];

        foreach ($request->get('sfdeducao') as $key => $sfdeducaoitem_current) {

            $sfitemdeducao = SfDeducao::find($sfdeducaoitem_current);
            $sfpredoc = SfPredoc::where('sfded_id', $sfitemdeducao->id)->first();

            if ($sfpredoc && isset($request->get('sfpredoccodtipodarf')[$sfitemdeducao->id])) {
                switch ($request->get('sfpredoccodtipodarf')[$sfitemdeducao->id]) {
//                    case 'DFS':
//                    case 'DFD':
//                        $arr_field_predoc['sfpredocnumref' . $key] = [
//                            'name' => 'sfpredocnumref' . $sfitemdeducao->id,
//                            'value' => null,
//                            'type' => 'text',
//                            'rules' => 'required',
//                            'label' => '',
//                            'error_messages' => ["numref.required" => 'Campo obrigatório'],
//                            'label_show' => true,
//                            'attr' => [
//                                'id' => 'sfpredocnumref' . $sfitemdeducao->id,
//                                'name' => "sfpredocnumref[$sfitemdeducao->id]",
//                            ]
//                        ];
//                        break;
                    case 'DFN':
                        $arr_field_predoc['sfpredocnumcodbarras' . $key] = [
                            'name' => 'sfpredocnumcodbarras' . $sfitemdeducao->id,
                            'value' => null,
                            'type' => 'text',
                            'rules' => 'required',
                            'label' => '',
                            'error_messages' => ["sfpredocnumcodbarras.required" => 'Campo obrigatório'],
                            'label_show' => true,
                            'attr' => [
                                'id' => 'sfpredocnumcodbarras' . $sfitemdeducao->id,
                                'name' => "sfpredocnumcodbarras[$sfitemdeducao->id]",
                            ]
                        ];
                        break;
                }

                $arr_field_predoc['sfpredocdtprdoapuracao' . $key] = [
                    'name' => 'sfpredocdtprdoapuracao' . $sfitemdeducao->id,
                    'value' => null,
                    'type' => 'date',
                    'rules' => 'required',
                    'label' => '',
                    'error_messages' => ["sfpredocdtprdoapuracao.required" => 'Campo obrigatório'],
                    'label_show' => true,
                    'attr' => [
                        'id' => 'sfpredocdtprdoapuracao' . $sfitemdeducao->id,
                        'name' => "sfpredocdtprdoapuracao[$sfitemdeducao->id]",
                    ]
                ];

//                $arr_field_predoc['sfpredoccodrecurso' . $key] = [
//                    'name' => 'sfpredoccodrecurso' . $sfitemdeducao->id,
//                    'value' => null,
//                    'type' => 'text',
//                    'rules' => 'required',
//                    'label' => '',
//                    'error_messages' => ["sfpredoccodrecurso.required" => 'Campo obrigatório'],
//                    'label_show' => true,
//                    'attr' => [
//                        'id' => 'sfpredoccodrecurso' . $sfitemdeducao->id,
//                        'name' => "sfpredoccodrecurso[$sfitemdeducao->id]",
//                    ]
//                ];

                $arr_field_predoc['sfpredoctxtobser' . $key] = [
                    'name' => 'sfpredoctxtobser' . $sfitemdeducao->id,
                    'value' => null,
                    'type' => 'text',
                    'rules' => 'required',
                    'label' => '',
                    'error_messages' => ["sfpredoctxtobser.required" => 'Campo obrigatório'],
                    'label_show' => true,
                    'attr' => [
                        'id' => 'sfpredoctxtobser' . $sfitemdeducao->id,
                        'name' => "sfpredoctxtobser[$sfitemdeducao->id]",
                    ]
                ];
            }

            //Para situações DDR
            if ($sfpredoc && isset($request->get('sfpredoctipo')[$sfitemdeducao->id]) && $request->get('sfpredoctipo')[$sfitemdeducao->id] == 'predocdar') {
                $arr_field_predoc['sfpredocreferencia' . $key] = [
                    'name' => 'sfpredocreferencia' . $sfitemdeducao->id,
                    'value' => null,
                    'type' => 'text',
                    'rules' => 'required',
                    'label' => '',
                    'error_messages' => ["sfpredocreferencia.required" => 'Campo obrigatório'],
                    'label_show' => true,
                    'attr' => [
                        'id' => 'sfpredocreferencia' . $sfitemdeducao->id,
                        'name' => "sfpredocreferencia[$sfitemdeducao->id]",
                    ]
                ];

                $arr_field_predoc['sfpredoctxtobser' . $key] = [
                    'name' => 'sfpredoctxtobser' . $sfitemdeducao->id,
                    'value' => null,
                    'type' => 'text',
                    'rules' => 'required',
                    'label' => '',
                    'error_messages' => ["sfpredoctxtobser.required" => 'Campo obrigatório'],
                    'label_show' => true,
                    'attr' => [
                        'id' => 'sfpredoctxtobser' . $sfitemdeducao->id,
                        'name' => "sfpredoctxtobser[$sfitemdeducao->id]",
                    ]
                ];
            }
        }

        if ($request->get('sfitemacrescimo') != null) {
            foreach ($request->get('sfitemacrescimo') as $key => $sfacrescimoitem_current) {
                $sfitemacrescimo = SfAcrescimo::find($sfacrescimoitem_current);

                if (!$sfitemacrescimo) {
                    continue;
                }

                $arr_field_deducao_recolhedor['numclassa' . $sfitemacrescimo->sfded_id . $sfitemacrescimo->id] = [
                    'name' => 'numclassa' . $sfitemacrescimo->sfded_id . $sfitemacrescimo->id,
                    'value' => null,
                    'type' => 'text',
                    'rules' => 'required',
                    'label' => '',
                    'error_messages' => ["recolhedor.required" => 'Campo obrigatório'],
                    'label_show' => true,
                    'attr' => [
                        'id' => 'numclassa' . $sfitemacrescimo->sfded_id . $sfitemacrescimo->id,
                        'name' => "numclassa[$sfitemacrescimo->id]",
                    ]
                ];
            }
        }

        if ($request->get('sfdeducao') != null) {
            foreach ($request->get('sfdeducao') as $key => $sfdeducaoitem_current) {

                $sfitemdeducao = SfDeducao::find($sfdeducaoitem_current);

                if (!$sfitemdeducao) {
                    continue;
                }

                if ($request->get('txtinscra') !== null && key_exists($key, $request->get('txtinscra'))) {
                    $arr_field_deducao['txtinscra' . $sfitemdeducao->id] = [
                        'name' => 'txtinscra' . $sfitemdeducao->id,
                        'value' => null,
                        'type' => 'text',
                        'rules' => [
                            'required',
                            function ($attribute, $value, $fail) {
                                if (empty($value)) {
                                    $fail('Campo obrigatório não pode ser vazio');
                                }
                            },
                        ],
                        'label' => '',
                        'error_messages' => ["txtinscra.required" => 'Campo obrigatório'],
                        'label_show' => true,
                        'attr' => [
                            'id' => 'txtinscra' . $sfitemdeducao->id,
                            'name' => "txtinscra[$sfitemdeducao->id]",
                        ]
                    ];
                }

                if ($request->get('txtinscrb') !== null && key_exists($key, $request->get('txtinscrb'))) {
                    $arr_field_deducao['txtinscrb' . $sfitemdeducao->id] = [
                        'name' => 'txtinscrb' . $sfitemdeducao->id,
                        'value' => null,
                        'type' => 'text',
                        'rules' => [
                            'required',
                            function ($attribute, $value, $fail) {
                                if (empty($value)) {
                                    $fail('Campo obrigatório não pode ser vazio');
                                }
                            },
                        ],
                        'label' => '',
                        'error_messages' => ["txtinscrb.required" => 'Campo obrigatório'],
                        'label_show' => true,
                        'attr' => [
                            'id' => 'txtinscrb' . $sfitemdeducao->id,
                            'name' => "txtinscrb[$sfitemdeducao->id]",
                        ]
                    ];
                }

                $arr_field_deducao['sfdeducaodtvenc' . $sfitemdeducao->id] = [
                    'name' => 'sfdeducaodtvenc' . $sfitemdeducao->id,
                    'value' => null,
                    'type' => 'date',
                    'rules' => [
                        'required',
                        function ($attribute, $value, $fail) {
                            if (empty($value)) {
                                $fail('Campo obrigatório não pode ser vazio');
                            }

                            $dataAtual = Carbon::today();
                            $dataInput = Carbon::parse($value);

                            if ($dataInput->lt($dataAtual)) {
                                $fail('A data de vencimento deve ser maior ou igual à data de hoje');
                            }
                        },
                    ],
                    'label' => '',
                    'error_messages' => ["sfdeducaodtvenc.required" => 'Campo obrigatório'],
                    'label_show' => true,
                    'attr' => [
                        'id' => 'sfdeducaodtvenc' . $sfitemdeducao->id,
                        'name' => "sfdeducaodtvenc[]",
                    ]
                ];

                $arr_field_deducao['sfdeducaodtpgtoreceb' . $sfitemdeducao->id] = [
                    'name' => 'sfdeducaodtpgtoreceb' . $sfitemdeducao->id,
                    'value' => null,
                    'type' => 'date',
                    'rules' => [
                        'required',
                        function ($attribute, $value, $fail) {
                            if (empty($value)) {
                                $fail('Campo obrigatório não pode ser vazio');
                            }

                            $dataAtual = Carbon::today();
                            $dataInput = Carbon::parse($value);

                            if ($dataInput->lt($dataAtual)) {
                                $fail('A data de pagamento deve ser maior ou igual à data de hoje');
                            }

                            $feriado = new NaoAceitarFeriado();
                            $fimSemana = new NaoAceitarFimDeSemana();

                            if (!$feriado->passes('sfdeducaodtpgtoreceb', $value)) {
                                $fail('A data de pagamento escolhida não pode ser feriado.');
                            }

                            if (!$fimSemana->passes('sfdeducaodtpgtoreceb', $value)) {
                                $fail('A data de pagamento escolhida não pode ser final de semana.');
                            }
                        },
                    ],
                    'label' => '',
                    'error_messages' => ["sfdeducaodtpgtoreceb$sfitemdeducao->id.required" => 'Campo obrigatório'],
                    'label_show' => true,
                    'attr' => [
                        'id' => 'sfdeducaodtpgtoreceb' . $sfitemdeducao->id,
                        'name' => "sfdeducaodtpgtoreceb[]",
                    ]
                ];
            }
        }

        if ($request->get('sfitemrecolhedor') != null) {
            foreach ($request->get('sfitemrecolhedor') as $key => $sfitemrecolhedor_current) {
                $sfitemrecolhedor = SfItemRecolhimento::find($sfitemrecolhedor_current);
                if (!$sfitemrecolhedor) {
                    continue;
                }
                $arr_field_deducao_recolhedor['recolhedor' . $sfitemrecolhedor->sfded_id . $sfitemrecolhedor->id] = [
                    'name' => 'recolhedor' . $sfitemrecolhedor->sfded_id . $sfitemrecolhedor->id,
                    'value' => null,
                    'type' => 'text',
                    'rules' => 'required',
                    'label' => 'Recolhedor',
                    'error_messages' => ["recolhedor.required" => 'Campo obrigatório'],
                    'label_show' => true,
                    'attr' => [
                        'id' => 'recolhedor' . $sfitemrecolhedor->sfded_id . $sfitemrecolhedor->id,
                        'name' => "recolhedor[$sfitemrecolhedor->id]",
                    ]
                ];

                $arr_field_deducao_recolhedor['vlrbasecalculo' . $sfitemrecolhedor->sfded_id . $sfitemrecolhedor->id] = [
                    'name' => 'vlrbasecalculo' . $sfitemrecolhedor->sfded_id . $sfitemrecolhedor->id,
                    'value' => '0,00',
                    'type' => 'text',
                    'rules' => [
                        'required',
                        function ($attribute, $value, $fail) {
                            if ($value == '0,00' || $value == '0.00') {
                                $fail('Campo obrigatório não pode ser zero ou vazio');
                            }
                        },
                    ],
                    'label' => 'Base de Cálculo',
                    'error_messages' => ["vlrbasecalculo.required" => 'Campo obrigatório'],
                    'label_show' => true,
                    'attr' => [
                        'id' => 'vlrbasecalculo' . $sfitemrecolhedor->sfded_id . $sfitemrecolhedor->id,
                        'name' => "vlrbasecalculo[$sfitemrecolhedor->id]",
                    ]
                ];

                $arr_field_deducao_recolhedor['vlrPrincipal' . $sfitemrecolhedor->sfded_id . $sfitemrecolhedor->id] = [
                    'name' => 'vlrPrincipal' . $sfitemrecolhedor->sfded_id . $sfitemrecolhedor->id,
                    'value' => '0,00',
                    'type' => 'text',
                    'rules' => [
                        'required',
                        function ($attribute, $value, $fail) {
                            if ($value == '0,00' || $value == '0.00') {
                                $fail('Campo obrigatório não pode ser zero ou vazio');
                            }
                        },
                    ],
                    'label' => 'Valor da Receita',
                    'error_messages' => ["vlrPrincipal.required" => 'Campo obrigatório'],
                    'label_show' => true,
                    'attr' => [
                        'id' => 'vlrPrincipal' . $sfitemrecolhedor->sfded_id . $sfitemrecolhedor->id,
                        'name' => "vlrPrincipal[$sfitemrecolhedor->id]",
                    ]
                ];
            }
        }

        $sfdeducaoForm = \FormBuilder::createByArray(
            array_merge($arr_field_deducao_recolhedor, $arr_field_deducao_codsit, $arr_field_deducao, $arr_field_predoc),
            [
                'url' => route('fatura.form.alterar-aba-deducao', ['sf_padrao_id' => $sf_padrao_id, 'numaba' => 3]),
                'method' => 'POST',
                'novalidate' => 'novalidate',
            ]
        );

        if (!$sfdeducaoForm->isValid()) {
            $this->atualizarNumAbaLiberadaNaSessao(3, (int)$sf_padrao_id);

            return [
                'form' => $sfdeducaoForm,
                'isValid' => false
            ];
        }
        return [
            'form' => $sfdeducaoForm,
            'isValid' => true
        ];
    }

    public function alterarAbaDadosPgto(Request $request)
    {
        $sf_padrao_id = $request->sf_padrao_id;

        $formValidation = $this->form(SfDadosPagamentoForm::class);

        if (!$formValidation->isValid()) {
            return response()->json([
                'status' => 'error',
                'data' => $formValidation->getErrors()
            ]);

        }

        $favorecido = is_array($request->codcredordevedor)
            ? $request->codcredordevedor[0]
            : $request->codcredordevedor;

        $sfDadosBasicos = SfDadosBasicos::where('sfpadrao_id', $sf_padrao_id)->get()->first();
        $sfDadosBasicos->dtpgtoreceb = $request->dtpgtoreceb;
        $sfDadosBasicos->save();

        $sfdadospgto = SfDadosPgto::where('sfpadrao_id', $sf_padrao_id)->first();
        $sfdadospgto->codcredordevedor = $this->limpaDocumentos($favorecido);
        $sfdadospgto->confirma_dados = true;
        $sfdadospgto->save();

        $preDoc = SfPredoc::where('sfdadospgto_id', $sfdadospgto->id)->first();
        if ($preDoc) {
            $preDoc->update(['codcredordevedor' => $this->limpaDocumentos($favorecido)]);
        }
        #caso exista dados de domicilio bancario salva nas tabelas sfpredoc e depois na sfdomiciliobancario
        if (isset($request->antecipagov_id)) {
            $domicilioBancario = AntecipaGov::where('id', $request->antecipagov_id)->first();

            $processoFatura = Contratofatura::where('sfpadrao_id', $sf_padrao_id)->first()->processo;

            foreach ($request->get('codcredordevedor') as $favorecido) {

                $dom = $domicilioBancario->domicilioBancario;
                $sfPredoc = SfPredoc::updateOrCreate(
                    [
                        'codtipoob' => 'OBC',
                        'codcredordevedor' => $this->limpaDocumentos($favorecido),
                        'txtprocesso' => $processoFatura,
                        'tipo' => 'predocob',
                        'txtobser' => substr("AntecipaGov Nº Op. $domicilioBancario->num_operacao, Bco: $dom->banco, Ag: $dom->agencia, Conta: $dom->conta, " . $sfDadosBasicos->txtobser, 0, 468),
                        'sfdadospgto_id' => $sfdadospgto->id
                    ]
                );

                #cria ou atualiza domicilio bancario do favorecido
                SfDomicilioBancario::updateOrCreate(
                    [
                        'sfpredoc_id' => $sfPredoc->id,
                        'banco' => $dom->banco,
                        'agencia' => $dom->agencia,
                        'conta' => $dom->conta,
                        'tipo' => 'numDomiBancFavo',
                    ]
                );

                #cria ou atualiza domicilio bancario do pgto
                SfDomicilioBancario::updateOrCreate(
                    [
                        'sfpredoc_id' => $sfPredoc->id,
                        'banco' => null,
                        'agencia' => null,
                        'conta' => 'UNICA',
                        'tipo' => 'numDomiBancPgto',
                    ]
                );
            }
        }

        $arrMesAnoRef = ContratoFaturaMesAno::where('contratofaturas_id', $this->apropriacao->faturas[0]->id)->get();
        $centro_custo = SfCentroCusto::where('sfpadrao_id', $sf_padrao_id)->count();

        if (count($arrMesAnoRef) == 1 && $centro_custo == 0) {
            $sfpco = $this->dadosAbasAlteracaoApropriacao->sfpcoAfetaCustoNaoId($sf_padrao_id);

            if (is_object($sfpco)) {
                $sfpcoitens = SfPcoItem::whereIn('sfpco_id', $sfpco)->get();

                $sfpcoParcelas = SfPco::whereIn('id', $sfpco)->get();

                $temDespesa = array_filter($sfpcoParcelas->toArray(), function ($valor) {
                    return $valor['despesaantecipada'] === true;
                });

                if ($temDespesa) {
                    foreach ($sfpcoParcelas as $key => $item) {
                        $parcelas = $item->cronBaixaPatrimonial->first()->parcela()->get();

                        foreach ($parcelas as $parcela) {
                            $novoSfCentrocusto = new SfCentroCusto();
                            $novoSfCentrocusto->sfpadrao_id = $sf_padrao_id;
                            $novoSfCentrocusto->mesreferencia = Carbon::parse($parcela->dtprevista)->format('m');
                            $novoSfCentrocusto->anoreferencia = Carbon::parse($parcela->dtprevista)->format('Y');
                            $novoSfCentrocusto->numseqitem = $key + 1;
                            $novoSfCentrocusto->push();

                            $this->faturaController->criarSfrelitemvlrcc($novoSfCentrocusto->id, $parcela->vlr, null, null, $request);
                        }
                    }

                } else {
                    if (count($sfpcoitens) >= 1) {
                        foreach ($sfpcoitens as $key => $item) {
                            $novoSfCentrocusto = new SfCentroCusto();
                            $novoSfCentrocusto->sfpadrao_id = $sf_padrao_id;
                            $novoSfCentrocusto->mesreferencia = $arrMesAnoRef[0]->mesref;
                            $novoSfCentrocusto->anoreferencia = $arrMesAnoRef[0]->anoref;
                            $novoSfCentrocusto->numseqitem = $key + 1;
                            $novoSfCentrocusto->push();

                            $this->faturaController->criarSfrelitemvlrcc($novoSfCentrocusto->id, $item->vlr, null, null, $request);
                        }
                    }
                }
            }
        }

        $sfpco = SfPco::where('sfpadrao_id', $sf_padrao_id)->get();
        $count_afeta_custo = $sfpco->filter(function ($value, $key) {
            return $this->dadosAbasAlteracaoApropriacao->retornarAfetaCustoSituacao($value->codsit) == true;
        });

        $telaPagamento = session()->get('arrPadraoAlteracaoApropriacao')[$sf_padrao_id]['numAbaLiberada'] == 4;

        // Se as situações forem todas sem afeta custo, exclui os centros de custo
        if (count($count_afeta_custo) == 0 && $telaPagamento) {
            SfCentroCusto::where('sfpadrao_id', $sf_padrao_id)->delete();
        }

        return response()->json([
            'status' => 'success',
            'data' => []
        ]);
    }

    public function salvaDadosPreDoc(Request $request)
    {
        try {
            DB::beginTransaction();

            $sf_padrao_id = $request->sf_padrao_id;

            $sfdadospgto = SfDadosPgto::where('sfpadrao_id', $sf_padrao_id)->first();
            $sfdadospgto->update(['codcredordevedor'=>$this->limpaDocumentos($request->codcredor)]);

            $sfPredoc = SfPredoc::updateOrCreate(
                ['sfdadospgto_id' => $sfdadospgto->id],
                [
                    'codtipoob' => $request->codtipoob,
                    'codcredordevedor' => $this->limpaDocumentos($request->codcredor),
                    'txtprocesso' => $request->txtprocesso,
                    'vlrTaxaCambio' => $this->dadosAbasAlteracaoApropriacao->formatarVlrParaBanco(
                        $request->taxacambio
                    ),
                    'codnumlista' => $request->codnumlista,
                    'txtcit' => $request->txtcit,
                    'tipo' => 'predocob',
                    'txtobser' => $request->obser,
                    'sfdadospgto_id' => $sfdadospgto->id
                ]
            );

            SfDomicilioBancario::where('sfpredoc_id',$sfPredoc->id)
            ->where( 'tipo','numDomiBancFavo')->delete();
            #cria ou atualiza domicilio bancario do favorecido
            SfDomicilioBancario::updateOrCreate(
                [
                    'sfpredoc_id' => $sfPredoc->id,
                    'banco' => $request->bancoFavorecido,
                    'agencia' => $request->agenciaFavorecido,
                    'conta' => $request->contaFavorecido,
                    'tipo' => 'numDomiBancFavo',
                ]
            );

            SfDomicilioBancario::where('sfpredoc_id',$sfPredoc->id)
                        ->where('tipo','numDomiBancPgto')->delete();
            #cria ou atualiza domicilio bancario do pgto
            SfDomicilioBancario::updateOrCreate(
                [
                    'sfpredoc_id' => $sfPredoc->id,
                    'banco' => $request->bancoPagador,
                    'agencia' => $request->agenciaPagador,
                    'conta' => $request->contaPagador,
                    'tipo' => 'numDomiBancPgto',
                ]
            );

            DB::commit();
            \Alert::success('Dados do Pré-Doc cadastrados com sucesso.')->flash();
            return response()->json(['success' => true], 201);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info(__METHOD__ . ' Linha ' . __LINE__ . ' - Erro ao salvar' .
            '[' . $e->getMessage() . ']');
            return response()->json(['success' => false, 'message' => $e->getMessage()], 500);
        }
    }

    public function removerPreDoc(Request $request)
    {

        $sf_padrao_id = $request->sf_padrao_id;

        try {
            DB::beginTransaction();
            $sfPreDoc = SfPredoc::whereHas('sfdadospgto', function ($query) use ($sf_padrao_id) {
                $query->where('sfpadrao_id', $sf_padrao_id);
            });
            $sfDomicilioBamcario = SfDomicilioBancario::whereHas('sfpredoc.sfdadospgto', function ($query) use ($sf_padrao_id) {
                $query->where('sfpadrao_id', $sf_padrao_id);
            });
            $sfPreDoc->forceDelete();
            $sfDomicilioBamcario->forceDelete();

            \Alert::success('Pré-Doc removido com sucesso!')->flash();

            DB::commit();

            return response()->json([
                'status' => 'success',
                'data' => []
            ]);

        } catch (\Exception $e) {

            DB::rollBack();

            return response()->json([
                'status' => 'error',
                'data' => $e->getMessage()
            ]);
        }
    }

    public function consultaBancos(Request $request)
    {
        if ($request->tipo == "Favorecido") {

            $domicilioBancario = SfDomicilioBancario::join('sfpredoc', 'sfdomiciliobancario.sfpredoc_id', '=', 'sfpredoc.id')
                ->join('sfdadospgto', 'sfpredoc.sfdadospgto_id', '=', 'sfdadospgto.id')
                ->join('sfpadrao', 'sfdadospgto.sfpadrao_id', '=', 'sfpadrao.id')
                ->join('apropriacoes_faturas_contratofaturas as aprf', 'sfpadrao.id', '=', 'aprf.sfpadrao_id')
                ->join('contratofaturas', 'aprf.contratofaturas_id', '=', 'contratofaturas.id')
                ->join('contratos', 'contratofaturas.contrato_id', '=', 'contratos.id')
                ->join('fornecedores', 'contratos.fornecedor_id', '=', 'fornecedores.id')
                ->join('apropriacoes_faturas as apr', 'aprf.apropriacoes_faturas_id', '=', 'apr.id')
                ->join('codigoitens', 'codigoitens.id', '=', 'apr.fase_id')
                ->whereIn('codigoitens.descres', ['APR', 'CAN'])
                ->where('fornecedores.cpf_cnpj_idgener', '=', $request->cnpjUg)
                ->where('sfdomiciliobancario.tipo', 'numDomiBancFavo')
                ->whereIn('sfdomiciliobancario.id', function ($query) {
                    $query->selectRaw('MAX(sfdomiciliobancario.id)')
                    ->from('sfdomiciliobancario')
                    ->groupBy('sfdomiciliobancario.conta', 'sfdomiciliobancario.banco', 'sfdomiciliobancario.agencia');
                })
                ->select('sfdomiciliobancario.conta', 'sfdomiciliobancario.banco', 'sfdomiciliobancario.agencia', 'sfdomiciliobancario.id')
                ->orderBy('sfdomiciliobancario.id', 'desc')
                ->get();

            return $domicilioBancario;
        }

        if ($request->tipo == "Pagador") {

            $domicilioBancario = SfDomicilioBancario::join('sfpredoc', 'sfdomiciliobancario.sfpredoc_id', '=', 'sfpredoc.id')
                ->join('sfdadospgto', 'sfpredoc.sfdadospgto_id', '=', 'sfdadospgto.id')
                ->join('sfpadrao', 'sfdadospgto.sfpadrao_id', '=', 'sfpadrao.id')
                ->join('sfdadosbasicos', 'sfpadrao.id', '=', 'sfdadosbasicos.sfpadrao_id')
                ->join('apropriacoes_faturas_contratofaturas as aprf', 'sfpadrao.id', '=', 'aprf.sfpadrao_id')
                ->join('apropriacoes_faturas as apr', 'aprf.apropriacoes_faturas_id', '=', 'apr.id')
                ->join('codigoitens', 'codigoitens.id', '=', 'apr.fase_id')
                ->whereIn('codigoitens.descres', ['APR', 'CAN'])
                ->where('sfdadosbasicos.codugpgto', '=', $request->cnpjUg)
                ->where('sfdomiciliobancario.tipo', 'numDomiBancPgto')
                ->where(DB::raw("LOWER(TRIM(sfdomiciliobancario.conta))"), '!=', 'unica')
                ->whereIn('sfdomiciliobancario.id', function ($query) {
                    $query->selectRaw('MAX(sfdomiciliobancario.id)')
                    ->from('sfdomiciliobancario')
                    ->groupBy('sfdomiciliobancario.conta', 'sfdomiciliobancario.banco', 'sfdomiciliobancario.agencia');
                })
                ->select('sfdomiciliobancario.conta', 'sfdomiciliobancario.banco', 'sfdomiciliobancario.agencia', 'sfdomiciliobancario.id')
                ->orderBy('sfdomiciliobancario.id', 'desc')
                ->get();

            return $domicilioBancario;
        }
    }

    function limpaDocumentos($valor)
    {
        $valor = trim($valor);
        $valor = str_replace(".", "", $valor);
        $valor = str_replace(",", "", $valor);
        $valor = str_replace("-", "", $valor);
        $valor = str_replace("/", "", $valor);
        return $valor;
    }

    public function alterarAbaDadosCentroCusto(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'codcentrocusto.*' => 'required_with:codsiorg.*,codugbenef.*',
        ], [
            'codcentrocusto.*.required_with' => 'O campo Centro de Custo é obrigatório.',
        ]);

        if ($validator->fails()) {
            $errors = array_unique($validator->errors()->all()); // Remove duplicatas
            $errorMessage = implode('<br>', $errors); // Junta as mensagens com quebras de linha

            return response()->json([
                'status' => 'error',
                'mensagemError' => $errorMessage
            ]);
        }
        // $isValidFormCentroCusto = $this->validarCamposAbaCentroCusto($request);

        $sumValorTotalPco = $this->retornarValorTotalAbaPco($request->sf_padrao_id);

        if ($sumValorTotalPco !== 0) {
            $isValidQtCentroCusto = $this->validarPeloMenosUmCentroCusto($request);
            $isValidValorTotalAbaPco = $this->validarValorTotalAbaPco($request, $sumValorTotalPco);

            if ($isValidQtCentroCusto !== '' || $isValidValorTotalAbaPco !== '') {
                return response()->json([
                    'status' => 'error',
                    'mensagemError' => $isValidValorTotalAbaPco ?: $isValidQtCentroCusto
                ]);
            }
        }

        $sfCentroCusto = SfCentroCusto::where('sfpadrao_id', $request->sf_padrao_id)->orderBy('id', 'asc')->get();

        try {
            $this->salvarDadosSfCentroCusto($sfCentroCusto, $request);

            $this->deleteIdsRemovidosCheckbox($request->sf_padrao_id, $request);

            //if ($request->numseqpai) {
            if ($request->numseqpai_checkbox) {
                # salva dados na tabela sfrelitemvlrcc
                //$this->salvarDadosSfRelItemVlrCC($request->numseqpai, $request);
                $this->salvarDadosSfRelItemVlrCC($request->numseqpai_checkbox, $request);
            }
            //--------------------------------------------------------------

            //altera numAbaLiberada para a proxima aba na sessao
            //$this->atualizarNumAbaLiberadaNaSessao(5, (int) $request->sf_padrao_id);

            \Alert::success('Dados da aba alterados com sucesso')->flash();

            //----------------------------------------------------
            //----------------------------------------------------
            return response()->json(['status' => 'success', 'apropriacaoSiafi' => true]);
        } catch (\Exception $exc) {
            DB::rollBack();
            \Alert::error('Erro na alteração dos dados da aba')->flash();
            //----------------------------------------------------

            return redirect()->route(
                'alterar.antes.apropriar.fatura',
                [
                    'sf_padrao_id' => $request->sf_padrao_id,
                    'apropriacao_id' => $this->apropriacao->id,
                    'numaba' => 5
                ]
            );
        }
    }

    public function salvarDadosSfCentroCusto($sfCentroCusto, $request)
    {

        foreach ($sfCentroCusto as $key => $current_centro_custo) {
            if ($request->has('codugbenef.' . $key)) {
                try {
                    $current_centro_custo->codugbenef = $request->codugbenef[$key] ?: null;
                    $current_centro_custo->codcentrocusto = $request->codcentrocusto[$key] ?: null;
                    $current_centro_custo->codsiorg = $request->codsiorg[$key] ?: null;
                    $current_centro_custo->mesreferencia = $request->mesreferencia[$key] ?: null;
                    $current_centro_custo->anoreferencia = $request->anoreferencia[$key] ?: null;
                    $current_centro_custo->confirma_dados = true;

                    $current_centro_custo->save();
                } catch (Exception $e) {
                    dd($e->getMessage());
                }
            }
        }
    }

    public function salvarDadosSfRelItemVlrCC($arrNumseqpai, $request)
    {
        foreach ($arrNumseqpai as $key => $numseqpai_current) {

            $numseqitem = $request->has('numseqitem.' . $key) ? $request->numseqitem[$key] : null;
            $centroCustoId = $request->has('centro_custo_id.' . $key) ? $request->centro_custo_id[$key] : null;
            $temDespAntecipada = $request->has('despesaantecipada.' . $key) ? $request->despesaantecipada[$key] : null;
            $tipo = $temDespAntecipada ? 'RELDESPESAANTECIPADA' : 'RELPCOITEM';
            $sfCentroCusto = SfCentroCusto::find($centroCustoId);
            $vlr = $request->has('vlr.' . $key) ? $this->dadosAbasAlteracaoApropriacao->formatarVlrParaBanco(
                $request->vlr[$key]
            ) : null;

            if (!$sfCentroCusto) {
                continue;
            }

                try {
                    $sfrelitemvlrcc = Sfrelitemvlrcc::where('sfcc_id', $centroCustoId)
                        ->where('numseqpai', $numseqpai_current)
                        ->where('numseqitem', $numseqitem)
                        ->get()->first();

                    Sfrelitemvlrcc::updateOrCreate(
                        [
                            'id'            =>  $sfrelitemvlrcc->id ?? null,
                            'sfcc_id'       =>  $centroCustoId,
                            'numseqpai'     =>  $numseqpai_current,
                            'numseqitem'    =>  $numseqitem,
                        ],
                        [
                            'vlr'           =>  $vlr,
                            'tipo'          =>  $tipo
                        ]
                    );
                } catch (\Exception $e) {
                    Log::info($e->getMessage());
                }
        }
    }

    public function validarCamposAbaCentroCusto($request)
    {
        $sumValorTotalPco = $this->retornarValorTotalAbaPco($request->sf_padrao_id);

        #caso o valor total seja 0 então não precisa validar centro de custo pois significa que não consta nenhum
        #item que afeta custo

        if ($sumValorTotalPco !== 0) {
            $isValidQtCentroCusto = $this->validarPeloMenosUmCentroCusto($request);
            $isValidValorTotalAbaPco = $this->validarValorTotalAbaPco($request, $sumValorTotalPco);

            if (
                !$isValidQtCentroCusto ||
                !$isValidValorTotalAbaPco
            ) {
                return false;
            }

            return true;
        }

        return true;
    }

    public function validarPeloMenosUmCentroCusto(Request $request)
    {
        if (!isset($request->codugbenef)) {
            return 'É necessário adicionar pelo o menos um Centro de Custo';
        }

        return '';
    }

    public function validarValorTotalAbaPco($request, $sumValorTotalPco)
    {

        #recupera valor total da aba centro de custo
        $arrValorTotalCentroCusto = [];
        $itensCheck = $request->numseqpai_checkbox;

        if (!is_null($request->numseqpai) && !is_null($itensCheck) > 0) {
            foreach ($request->numseqpai as $key => $value) {
                if (key_exists($key, $itensCheck)) {
                    $arrValorTotalCentroCusto[$key] = $this->dadosAbasAlteracaoApropriacao->formatarVlrParaBanco(
                        $request->vlr[$key]
                    );
                }
            }
        } else {
            $arrValorTotalCentroCusto[] = 0;
        }

        $sumValorTotalCentroCusto = array_reduce($arrValorTotalCentroCusto, function ($previous, $current) {
            return $previous + $current;
        }, 0);

        #caso não tenha valor vindo de sfPco então passa na validação
//        if ($sumValorTotalPco === 0) {
//            return true;
//        }

        if (round((float)$sumValorTotalPco, 2) < round((float)$sumValorTotalCentroCusto, 2)) {
            return 'O valor total dos itens selecionados devem corresponder ao valor total da aba Pco';
        }

        return '';
    }

    public function createOrDeleteSfPcoViaAjax(Request $request)
    {

        $sf_padrao_id = $request->sf_padrao_id;

        $txtobser_pco = $request->txtobser_pco;

        //$result  = novo id sfpco criado ou caso tenha sido delete entao retorna array com erro
        $result = $this->createOrDeleteSfPco($request);

        if (isset($result['status']) && $result['status'] === 'error') {
            return $result;
        }

        $sfPco = SfPco::where('sfpadrao_id', $sf_padrao_id)
            ->orderBy('id', 'asc')
            ->get();

        $valorTotalAbaPco = 0;

        if (!$request['delete_sfpco']) {
            $valorTotalAbaPco = $this->buscaEmpenhosParaCriarAbaPCO($request, $sfPco->last()->id);
        }

        $sfPadrao = SfPadrao::find($sf_padrao_id);

        $sfpcoForm = $this->dadosAbasAlteracaoApropriacao->criarDadosAbaPco(
            $sfPco,
            $valorTotalAbaPco,
            $sf_padrao_id,
            $sfPadrao->codtipodh,
            $this->apropriacao->id
        );

        $idPcoActive = $request->delete_sfpco ? $request->idPcoActive : $result;

        //altera numAbaLiberada
        $this->atualizarNumAbaLiberadaNaSessao(2, (int) $sf_padrao_id);

        return view(
            'backpack::base.mod.apropriacao.aba-principal-com-orcamento',
            compact(
                'sfPco',
                'txtobser_pco',
                'sfpcoForm',
                'valorTotalAbaPco',
                'idPcoActive'
            )
        );
    }

    public function buscaEmpenhosParaCriarAbaPCO($request, $sfPcoId)
    {
        $empenhos = ContratoFaturaEmpenho::join(
            'contratofaturas',
            'contratofaturas.id',
            '=',
            'contratofatura_empenhos.contratofatura_id'
        )
            ->join(
                'apropriacoes_faturas_contratofaturas',
                'contratofaturas.id',
                '=',
                'apropriacoes_faturas_contratofaturas.contratofaturas_id'
            )
            ->join('empenhos', 'contratofatura_empenhos.empenho_id', '=', 'empenhos.id')
            ->leftJoin('naturezasubitem', 'naturezasubitem.id', '=', 'contratofatura_empenhos.subelemento_id')
            ->select(
                'contratofatura_empenhos.contratofatura_id as contratofatura_id',
                'empenhos.numero',
                'empenhos.id as empenho_id',
                'contratofatura_empenhos.subelemento_id',
                'naturezasubitem.codigo',
                DB::raw('sum(contratofatura_empenhos.valorref) as valorreftotal')
            )
            ->where('apropriacoes_faturas_contratofaturas.apropriacoes_faturas_id', $request['apropriacao_id'])
            ->whereNotIn('empenhos.id', $request['empenhos_ids'])
            ->groupBy(
                'empenhos.numero',
                'empenhos.id',
                'contratofatura_empenhos.subelemento_id',
                'contratofatura_empenhos.contratofatura_id',
                'naturezasubitem.codigo'
            )
            ->get();

        if ($empenhos->count() > 0) {
            foreach ($empenhos as $key => $empenhosFatura) {
                $sfPcoItem = SfPcoItem::where('sfpco_id', $sfPcoId)
                    ->where('numempe', $empenhosFatura->numero)
                    ->where('subelemento_id', $empenhosFatura->subelemento_id)
                    ->first();

                if ($sfPcoItem) {
                    $sfPcoItem->vlr += $empenhosFatura->valorreftotal ?? 0;
                    $sfPcoItem->save();
                } else {
                    $novoSfPcoItem = new SfPcoItem();
                    $novoSfPcoItem->sfpco_id = $sfPcoId;
                    $novoSfPcoItem->numempe = $empenhosFatura->numero ?? '';
                    $novoSfPcoItem->vlr = $empenhosFatura->valorreftotal ?? 0;
                    $novoSfPcoItem->indrliquidado = true;
                    $novoSfPcoItem->codsubitemempe = $empenhosFatura->subelemento ? $empenhosFatura->subelemento->codigo : null;
                    $novoSfPcoItem->subelemento_id = $empenhosFatura->subelemento_id ?? null;
                    $novoSfPcoItem->numseqitem = $key + 1;
                    $novoSfPcoItem->push();
                }
                $this->faturaController->criarPopularContratoFaturaEmpenhosApropriacao($empenhosFatura, $novoSfPcoItem->id);
            }
            return $empenhos->sum('valorreftotal');
        } else {
            SfPcoItem::create([
                'sfpco_id' => $sfPcoId,
                'indrliquidado' => true
            ]);
            return 0;
        }
    }

    public function criaNovaDeducaoViaAjax(Request $request)
    {
        $sf_padrao_id = $request->sf_padrao_id;

        $result = $this->createOrDeleteSfDeducao($request);

        $sfDeducao = SfDeducao::where('sfpadrao_id', $sf_padrao_id)->orderBy('id', 'ASC')->get();

        $sfDadosBasicos = SfDadosBasicos::where('sfpadrao_id', $sf_padrao_id)->first();
        if ($sfDadosBasicos) {
            $sfDadosPgto = SfDadosPgto::where('sfpadrao_id', $sf_padrao_id)->first();

            $sfDadosPgto->vlr = $sfDadosBasicos->vlr;
            $sfDadosPgto->save();
        }

        parse_str($request->input('formData'), $dadosFormSfpcoItemArray);
        $request->merge($dadosFormSfpcoItemArray);

        $this->salvaDadosRelacionadosDeducao($request, $sf_padrao_id);

        $totalValorDeducao = 0;

        //$arrayOptions = $this->carregaUnidadeDescentralizadaContrato();
        $opcoes = [];

        //$opcoes = $this->organizaParaSelect2Input($arrayOptions, $opcoes);

        $sfDeducaoForm = $this->dadosAbasAlteracaoApropriacao->criarDadosAbaDeducao(
            $sfDeducao,
            $totalValorDeducao,
            $opcoes,
            $sf_padrao_id,
            $this->apropriacao->id
        );

        $idDeducaoActive = $request->delete_sfDeducao ? $request->idDeducaoActive : $result;

        //altera numAbaLiberada
        $this->atualizarNumAbaLiberadaNaSessao(3, (int)$sf_padrao_id);

        return view(
            'backpack::base.mod.apropriacao.aba-deducao',
            compact(
                'sfDeducao',
                'totalValorDeducao',
                'sfDeducaoForm',
                'idDeducaoActive'
            )
        );
    }

    public function carregaUnidadeDescentralizadaContrato()
    {
        $apropriacao_id = request('apropriacao_id') ?: Route::current()->parameter('apropriacao_id');
        if ($apropriacao_id) {
            $apropriacaos = ApropriacaoFaturas::where('id', $apropriacao_id)->first();
            $contrato_ids = array_column($apropriacaos->faturas->toArray(), 'contrato_id');
            $unidadesPermitidas = [];

            foreach ($contrato_ids as $contrato_id) {
                $contrato = Contrato::findOrFail($contrato_id)
                    ->unidade
                    ->toArray();

                $unidadePrimariaContrato[] = [
                    'value' => $contrato['codigo'],
                    'description' => $contrato['nome']
                ];

                $unidadesDescentralizadas = Contrato::findOrFail($contrato_id)
                    ->unidadesdescentralizadas
                    ->pluck('unidade')
                    ->map(function ($unidade) {
                        return ['value' => $unidade->codigo, 'description' => $unidade->nome];
                    })
                    ->toArray();

                $unidadesPermitidas = array_merge($unidadePrimariaContrato, $unidadesDescentralizadas, $unidadesPermitidas);
            }
            $arrayUnidadePrimariaDescentralizada = array_values(array_unique($unidadesPermitidas, SORT_REGULAR));
        }

        return $arrayUnidadePrimariaDescentralizada;
    }

    public function atualizaCodUgEmitEmpenhoViaAjax(Request $request)
    {
        $sf_padrao_id = $request->sf_padrao_id;
        $codUgEmpenho = $request->codUgEmpenho;
        $pco_id = $request->pcoId;

        DB::beginTransaction();
        try {
            $sfPco = SfPco::where('sfpadrao_id', $sf_padrao_id)
                ->where('id', $pco_id)
                ->first();
            $sfPco->codugempe = $codUgEmpenho;
            $sfPco->save();

            $descricaoUnidade = Unidade::where('codigo', $codUgEmpenho)->first()->nome;

            DB::commit();
            return response()->json(['mensagem' => 'Unidade do empenho atualizada com sucesso.', 'nomeUnidade' => $descricaoUnidade]);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error' => 'Erro ao atualizar unidade do empenho.']);
        }
    }

    public function atualizaCodUgPagadoraDeducaoViaAjax(Request $request)
    {
        $sf_padrao_id = $request->sf_padrao_id;
        $codUgEmpenho = $request->codUgEmpenho;
        $deducao_id = $request->idDeducao;

        DB::beginTransaction();
        try {
            $sfDeducao = SfDeducao::where('sfpadrao_id', $sf_padrao_id)
                ->where('id', $deducao_id)
                ->first();
            $sfDeducao->codugpgto = $codUgEmpenho;
            $sfDeducao->save();

            $descricaoUnidade = Unidade::where('codigo',$codUgEmpenho)->first()->nome;

            DB::commit();
            return response()->json(['mensagem' => 'Unidade da Ug Pagadora atualizada com sucesso.','nomeUnidade' => $descricaoUnidade]);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error' => 'Erro ao atualizar unidade pagadora.']);
        }

    }

    public function retornaDescricaoCodigoDeducaoViaAjax(Request $request)
    {
        $sf_padrao_id = $request->sf_padrao_id;

        $this->createOrUpdateSfDeducaoSituacao($request);

        $sfDeducao = SfDeducao::where('sfpadrao_id', $sf_padrao_id)->orderBy('id', 'ASC')->get();

        $this->salvaDadosRelacionadosDeducao($request, $sf_padrao_id);

        $totalValorDeducao = 0;

        //$arrayOptions = $this->carregaUnidadeDescentralizadaContrato();
        $opcoes = [];

        //$opcoes = $this->organizaParaSelect2Input($arrayOptions, $opcoes);

        $sfDeducaoForm = $this->dadosAbasAlteracaoApropriacao->criarDadosAbaDeducao(
            $sfDeducao,
            $totalValorDeducao,
            $opcoes,
            $sf_padrao_id,
            $this->apropriacao->id
        );

        $idDeducaoActive = $request->idDeducao;

        //altera numAbaLiberada
        $this->atualizarNumAbaLiberadaNaSessao(3, (int)$sf_padrao_id);

        return view(
            'backpack::base.mod.apropriacao.aba-deducao',
            compact(
                'sfDeducao',
                'totalValorDeducao',
                'sfDeducaoForm',
                'idDeducaoActive'
            )
        );

    }

    public function createOrUpdateSfPcoSituacaoViaAjax(Request $request)
    {
        $sf_padrao_id = $request->sf_padrao_id;

        $txtobser_pco = $request->txtobser_pco;

        $this->createOrUpdateSfPcoSituacao($request);

        $sfPco = SfPco::where('sfpadrao_id', $sf_padrao_id)
            ->orderBy('id', 'asc')
            ->get();

        $sfPadrao = SfPadrao::find($sf_padrao_id);

        $valorTotalAbaPco = 0;

        $sfpcoForm = $this->dadosAbasAlteracaoApropriacao->criarDadosAbaPco(
            $sfPco,
            $valorTotalAbaPco,
            $sf_padrao_id,
            $sfPadrao->codtipodh,
            $this->apropriacao->id
        );

        $idPcoActive = $request->idSfPco;

        return view(
            'backpack::base.mod.apropriacao.aba-principal-com-orcamento',
            compact(
                'sfPco',
                'txtobser_pco',
                'sfpcoForm',
                'valorTotalAbaPco',
                'idPcoActive'
            )
        );
    }

    public function updateSfPcoIndrtemcontratoViaAjax(Request $request)
    {
        $sf_padrao_id = $request->sf_padrao_id;
        $txtobser_pco = $request->txtobser_pco;

        $this->updateSfPcoIndrtemcontrato($request);

        $sfPco = SfPco::where('sfpadrao_id', $sf_padrao_id)
            ->orderBy('id', 'asc')
            ->get();

        $sfPadrao = SfPadrao::find($sf_padrao_id);

        $valorTotalAbaPco = 0;

        $sfpcoForm = $this->dadosAbasAlteracaoApropriacao->criarDadosAbaPco(
            $sfPco,
            $valorTotalAbaPco,
            $sf_padrao_id,
            $sfPadrao->codtipodh,
            $this->apropriacao->id
        );

        $idPcoActive = $request->idSfPco;

        return view(
            'backpack::base.mod.apropriacao.aba-principal-com-orcamento',
            compact(
                'sfPco',
                'txtobser_pco',
                'sfpcoForm',
                'valorTotalAbaPco',
                'idPcoActive'
            )
        );
    }

    public function updateSfPcoNumClassViaAjax(Request $request)
    {
        $sf_padrao_id = $request->sf_padrao_id;
        $txtobser_pco = $request->txtobser_pco;

        $this->updateSfPcoNumClass($request);

        $sfPco = SfPco::where('sfpadrao_id', $sf_padrao_id)
            ->orderBy('id', 'asc')
            ->get();

        $sfPadrao = SfPadrao::find($sf_padrao_id);

        $valorTotalAbaPco = 0;

        $sfpcoForm = $this->dadosAbasAlteracaoApropriacao->criarDadosAbaPco(
            $sfPco,
            $valorTotalAbaPco,
            $sf_padrao_id,
            $sfPadrao->codtipodh,
            $this->apropriacao->id
        );

        $idPcoActive = $request->idSfPco;

        return view(
            'backpack::base.mod.apropriacao.aba-principal-com-orcamento',
            compact(
                'sfPco',
                'txtobser_pco',
                'sfpcoForm',
                'valorTotalAbaPco',
                'idPcoActive'
            )
        );
    }

    private function createOrDeleteSfPco($request)
    {
        if ($request->delete_sfpco) {
            $sfRelItemVlrCC = $this->countSfRelItemVlrCCPorSfPco($request);

            $existeSfRelItemVlrCC = $this->verificarSfPcoItemExisteEmSfRelItemVlrCC($sfRelItemVlrCC);

            if ($existeSfRelItemVlrCC) {
                return ['status' => 'error', 'mensagem' => 'Empenho(s) vinculado ao Centro de Custo'];
            }

            $sfPco = SfPco::find($request->idPco);
            $sfPco->delete();

            return null;
        }

        return $this->faturaController->criarSfPco($request->sf_padrao_id, $request->numseqitem, $this->contrato_id);
    }

    private function updateSfPcoNumClass($request)
    {
        $sfpco = SfPco::find($request->idSfPco);

        $campoVariavel = $this->retornaCampoVariavel($sfpco->codsit);
        $sfpco->$campoVariavel = $request->numclass;

        $sfpco->save();

        $sfpcoUpdates = SfPco::where('sfpadrao_id', $request->sf_padrao_id)
        ->where('numseqitem', '!=', '1')
        ->get();
        foreach ($sfpcoUpdates as $item) {
            $campoVariavel = $this->retornaCampoVariavel($item->codsit);
            $item->$campoVariavel = $request->numclass;
            $item->indrtemcontrato = $sfpco->indrtemcontrato;
            $item->save();
        }

        #salva os dados já inseridos no form para quando retornar a view os dados já virem do banco
        parse_str($request->input('formData'), $dadosFormSfpcoItemArray);
        #inserir dados do form de sfpcoitem no objeto request
        $request->merge($dadosFormSfpcoItemArray);

        $this->salvarDadosSfPcoItem(
            $request->get('sfpcoitem'),
            $request,
            $request->sf_padrao_id
        );
    }

    private function retornaCampoVariavel($codsit)
    {
        $campo = ExecsfsituacaoCamposVariaveis::join('execsfsituacao', 'execsfsituacao.id', '=', 'execsfsituacao_campos_variaveis.execsfsituacao_id')
            ->where('execsfsituacao.codigo', $codsit)
            ->whereIn('execsfsituacao_campos_variaveis.rotulo', ['Conta de Contratos', 'Conta de Contrato'])
            ->first();
        return $campo->campo ?? null;
    }

    private function createOrDeleteSfDeducao($request)
    {
        if ($request->delete_sfDeducao) {

            $sfDeducao = SfDeducao::find($request->idDeducao);
            $sfDeducao->delete();

            return null;
        }

        return $this->faturaController->criarSfDeducao($request->sf_padrao_id, $request->numseqitem);
    }

    private function createOrUpdateSfPcoSituacao($request)
    {

        $sfpco_id = $request->idSfPco;
        $codsit = $request->codsit;

        $sfpco = SfPco::find($sfpco_id);

        if ($codsit) {
            $campoVariavel = $this->retornaCampoVariavel($codsit);

            if ($campoVariavel) {

                $valorNumclass = SfPco::where('sfpadrao_id', $sfpco->sfpadrao_id)
                    ->where('numseqitem', '1')->first();
                $campoVariavelSeq1 = $this->retornaCampoVariavel($valorNumclass->codsit);

                $sfpco->numclassd = ($campoVariavel === 'numclassd') ? $valorNumclass->$campoVariavelSeq1 : 0;
                $sfpco->numclasse = ($campoVariavel === 'numclasse') ? $valorNumclass->$campoVariavelSeq1 : 0;
            } else {
                $sfpco->numclassd = 0;
                $sfpco->numclasse = 0;
            }
        }

        $sfpco->confirma_dados = false;
        $sfpco->codsit = $codsit;
        $sfpco->save();
    }

    private function createOrUpdateSfDeducaoSituacao($request)
    {

        $sfdeducao_id = $request->idDeducao;
        $codsit = $request->codsit;

        $sfdeducao = SfDeducao::find($sfdeducao_id);
        $sfdeducao->codsit = $codsit;
        $sfdeducao->dtpgtoreceb = null;
        $sfdeducao->vlr = 0;
        $sfdeducao->txtinscra = '';
        $sfdeducao->txtinscrb = '';
        $sfdeducao->possui_acrescimo = false;
        $sfdeducao->dtvenc = null;
        $sfdeducao->confirma_dados = false;
        $sfdeducao->save();

        //Limpa a dedução ao alterar a situacao
        SfItemRecolhimento::where('sfded_id', $sfdeducao->id)->delete();
        SfAcrescimo::where('sfded_id', $sfdeducao->id)->delete();
        SfPredoc::where('sfded_id', $sfdeducao->id)->delete();
        SfRelItemDeducao::where('sfded_id', $sfdeducao->id)->delete();
    }

    public function createOrDeleteSfPcoItemViaAjax(Request $request)
    {
        $sf_padrao_id = $request->sf_padrao_id;
        $txtobser_pco = $request->txtobser_pco;
        $sfPadrao = SfPadrao::find($sf_padrao_id);
        $valorTotalAbaPco = 0;

        $arrayResposta = $this->createOrDeleteSfPcoItem($request);

        #salva os dados já inseridos no form para quando retornar a view os dados já virem do banco
        parse_str($request->input('formData'), $dadosFormSfpcoItemArray);
        #inserir dados do form de sfpcoitem no objeto request
        $request->merge($dadosFormSfpcoItemArray);

        $this->salvarDadosSfPcoItem(
            $request->get('sfpcoitem'),
            $request,
            $sf_padrao_id
        );

        foreach ($request->get('sfpco') as $key => $sfpco) {
            $sfPcoModel = SfPco::where('id', $sfpco)->first();
            $sfPcoModel->numclassd = (int)str_replace(".", '', $request->get('numclassd')[$sfpco] ?? '');
            $sfPcoModel->numclasse = (int)str_replace(".", '', $request->get('numclasse')[$sfpco] ?? '');
            $sfPcoModel->txtinscrd = str_replace(".", '', $request->get('txtinscrd')[$sfpco] ?? '');
            $sfPcoModel->txtinscre = str_replace(".", '', $request->get('txtinscre')[$sfpco] ?? '');
            $sfPcoModel->indrtemcontrato = (bool)$request->get('indrtemcontrato')[$key];
            $sfPcoModel->repetir_conta_empenho = (bool) $request->get('repetircontaempenho')[$key];

            $sfPcoModel->save();
        }

        $sfPco = SfPco::where('sfpadrao_id', $sf_padrao_id)
            ->orderBy('id', 'asc')
            ->get();

        $sfpcoForm = $this->dadosAbasAlteracaoApropriacao->criarDadosAbaPco(
            $sfPco,
            $valorTotalAbaPco,
            $sf_padrao_id,
            $sfPadrao->codtipodh,
            $this->apropriacao->id
        );

        if (isset($arrayResposta['status']) && $arrayResposta['status'] === 'error') {
            return $arrayResposta;
        }

        $idPcoActive = (int) $request->idPco;

        //altera numAbaLiberada
        $this->atualizarNumAbaLiberadaNaSessao(3, (int) $sf_padrao_id);

        return view(
            'backpack::base.mod.apropriacao.aba-principal-com-orcamento',
            compact(
                'sfPco',
                'txtobser_pco',
                'sfpcoForm',
                'valorTotalAbaPco',
                'idPcoActive' //usado para capturar a aba do pco que está ativa
            )
        );
    }

    public function salvarValorSfpcoItem($idSfpcoItem, $arrayValorNotasEmpenho, $instrumentoCobranca, $subelemento, $empenhoNumero, $subelemento_id, $arrayCheckbox)
    {
        $sfPcoitem = SfPcoItem::where('id', $idSfpcoItem)->first();

        //Realiza a soma total dos itens marcados o ckeckbox para salvar.
        $total = 0;
        foreach ($arrayValorNotasEmpenho as $index => $valor) {
            if (isset($arrayCheckbox[$index]) && $arrayCheckbox[$index] === true && $valor !== null) {
                // Remove pontos e substitui vírgulas por pontos para formatar como número
                $valorNumerico = floatval(str_replace(['.', ','], ['', '.'], $valor));
                // Adiciona o valor à soma
                $total += $valorNumerico;
            }
        }

        try {
            $sfPcoitem->vlr = $total;
            $sfPcoitem->codsubitemempe = $subelemento;
            $sfPcoitem->subelemento_id = $subelemento_id;
            $sfPcoitem->numempe = $empenhoNumero;
            $sfPcoitem->save();
        } catch (Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());
        }
    }

    public function localizaEmpenho($codigoUnidade, $numeroEmpenho, $instCobrancaId, $sfpcoitem_id)
    {
        # Recupera a ug pagamento ou emitente do empenho
        $unidade = Unidade::where('codigo', $codigoUnidade)->first();

        if (!$unidade) {
            return null;
        }

        # Verifica se o empenho existe na apropriação existindo independente da unidade retorna para atualização.
        $contratoFaturaEmpenhoApropriacao = ContratoFaturaEmpenhosApropriacao::where('contratofatura_id', $instCobrancaId)
            ->where('sfpcoitem_id', $sfpcoitem_id)
            ->first();

        if ($contratoFaturaEmpenhoApropriacao) {
            $empenhoApropriacaoNumero = $contratoFaturaEmpenhoApropriacao->empenho()->first() ?? null;;
        }

        $empenhoQuery = Empenho::query();

        if (!isset($empenhoApropriacaoNumero)) {
            $empenhoQuery->where('unidade_id', $unidade->id)
                ->where('numero',$numeroEmpenho);
        }

        if (isset($empenhoApropriacaoNumero)) {
            // Se não houver número de empenho, adiciona o filtro por unidade_id
            $empenhoQuery->where('id', $empenhoApropriacaoNumero->id)
                ->where('numero',$empenhoApropriacaoNumero->numero);
        }

        $empenho = $empenhoQuery->first();

        if (!$empenho) {
            return null;
        }

        return $empenho;
    }

    public function updateOrCreateInstrumentoCobrancaEmpenhoAjax(Request $request)
    {
        $arrayInstrumentoCobrancaId = $request->get('instcobranca_ids');
        $arrayEmpenhosId            = $request->get('empenhos_ids');
        $arrayValorRef              = $request->get('valorref');
        $arrayCheckbox              = $request->get('checkboxFaturaEmpenho');
        $numeroEmpenho              = $request->get('numeroEmpenho');
        $idSubelemento              = $request->get('idSubelemento');
        $idSfPcoItem                = $request->get('idSfPcoItem');
        $apropriacao_id             = $request->get('apropriacao_id');
        $eventFromOnkeyupEmpenho    = $request->get('eventFromOnkeyupEmpenho');
        $subelemento                = $request->get('subelemento');

        #Fazer validacao se array existe e é maior que 0
        #caso a ação do ajax tenha vindo do onkeyup de empenho apenas consulta os instrumentos de cobrança do empenho
        #se a ação não veio do onkeyup então veio do botão salvar, então atualiza ou cria dados em contratofatura_empenhos
        if (!$eventFromOnkeyupEmpenho) {

            $apropriacao = ApropriacaoContratoFaturas::where('apropriacoes_faturas_id', $apropriacao_id)->first();
            $codigoUnidade = $apropriacao->sfpadrao->codugemit;
            $sfPcoUnidade = SfPcoItem::where('id', $idSfPcoItem)->first()->pco()->first()->codugempe;
            $unidadeEmitenteApropriacao = $codigoUnidade === $sfPcoUnidade ? $codigoUnidade : $sfPcoUnidade;
            $subelemento = Naturezasubitem::where('id', $idSubelemento)->first()->codigo;

            foreach ($arrayInstrumentoCobrancaId as $index => $instCobrancaId) {

                #virá null pq não houve seleção de uma nova nota por tanto n tem id
                if ($instCobrancaId === null || $instCobrancaId === '') {
                    continue;
                }
                $empenho = $this->localizaEmpenho($unidadeEmitenteApropriacao, $numeroEmpenho, $instCobrancaId, $idSfPcoItem);
                $sfRelItemVlrCC = $this->countSfRelItemVlrCCPorNumseqPaiNumseqItem($request);

                $existeSfRelItemVlrCC = $this->verificarSfPcoItemExisteEmSfRelItemVlrCC($sfRelItemVlrCC);

                if ($existeSfRelItemVlrCC) {
                    return [
                        'response' => [
                            'status' => 'error', 'message' => 'Empenho vinculado ao Centro de Custo'
                        ]
                    ];
                }

                try {
                    DB::beginTransaction();

                    $this->salvarValorSfpcoItem($idSfPcoItem, $arrayValorRef, $instCobrancaId, $subelemento, $empenho->numero, $idSubelemento, $arrayCheckbox);

                    $this->updateOrCreateContratoFaturaEmpenhosApropriacao(
                        $instCobrancaId,
                        $idSubelemento,
                        $empenho->id,
                        $arrayValorRef[$index],
                        $arrayCheckbox[$index],
                        $idSfPcoItem
                    );
                    DB::commit();
                } catch (Exception $e) {
                    DB::rollBack();
                    Log::info($e->getMessage());
                    if(!isset($empenho)){
                        return [
                            'response' => [
                                'status' => 'error', 'message' => 'O empenho não pertence a esta unidade.'
                            ]
                        ];
                    }
                    return [
                        'response' => [
                            'status' => 'error', 'message' => 'Não foi possível realizar a alteração'
                        ]
                    ];
                }
            }
        }

        $faturaEmpenho = $this->dadosAbasAlteracaoApropriacao->findContratoFaturaEmpenhosByNumEmpenho(
            $numeroEmpenho,
            $apropriacao_id,
            $idSubelemento,
            $unidadeEmitenteApropriacao ?? null,
            $idSfPcoItem
        );

        $arrayContratoFaturas = $faturaEmpenho ? $faturaEmpenho->pluck('contratofatura_id')->toArray() : [];

        #atribui apropriacaocontratofaturas ao obj sfpcoitem
        $apropriacaoFaturas = ApropriacaoContratoFaturas::join(
            'contratofaturas',
            'apropriacoes_faturas_contratofaturas.contratofaturas_id',
            '=',
            'contratofaturas.id'
        )
            ->where(
                'apropriacoes_faturas_id',
                $apropriacao_id
            )
            ->whereNotIn(
                'apropriacoes_faturas_contratofaturas.contratofaturas_id',
                $arrayContratoFaturas
            )
            ->get();

        $sfpcoitem_current = SfPcoItem::where('id', $idSfPcoItem)->first();
        $sfpcoitem_current->faturaEmpenho = $faturaEmpenho ? $faturaEmpenho->get() : null;
        $sfpcoitem_current->apropriacaoFaturas = $apropriacaoFaturas;

        return [
            'view' => view(
                'backpack::base.mod.apropriacao.partials.instrumentos-cobranca-do-empenho',
                compact(
                    'sfpcoitem_current'

                )
            )->render(),
            'response' => [
                'status' => 'success',
                'message' => 'Dados atualizados com sucesso'
            ]

        ];

    }

    public function updateOrCreateContratoFaturaEmpenhosApropriacao(
        $instrumentoCobrancaId,
        $idSubelemento,
        $empenhoId,
        $valorRef,
        $isChecked,
        $idSfPcoItem
    )
    {
        #caso checkbox esteja marcado então atualize campo valor se não exclui vinculo do instrumento de cobranca
        #com o empenho
        if ($isChecked) {
            $valorRef = $this->dadosAbasAlteracaoApropriacao->formatarVlrParaBanco(
                $valorRef
            );

            $apropriacao = ContratoFaturaEmpenhosApropriacao::where('contratofatura_id', $instrumentoCobrancaId)
                ->where('sfpcoitem_id', $idSfPcoItem)
                ->where(function ($query) use ($idSubelemento) {
                    if (!empty($idSubelemento)) {
                        // Se a variável $idSubelemento não estiver vazia, aplica o filtro
                        $query->where('subelemento_id', $idSubelemento)
                            ->orWhereNull('subelemento_id');
                    } else {
                        // Se a variável $idSubelemento estiver vazia, retorna registros com subelemento_id não nulo ou nulo
                        $query->whereNotNull('subelemento_id')
                            ->orWhereNull('subelemento_id');
                    }
                })
                ->first();

            if ($apropriacao) {
               return $apropriacao->update([
                    'empenho_id' => $empenhoId,
                    'valorref' => $valorRef,
                    'sfpcoitem_id' => $idSfPcoItem,
                    'subelemento_id' => $idSubelemento,
                ]);
            } else {
                return ContratoFaturaEmpenhosApropriacao::create([
                    'contratofatura_id' => $instrumentoCobrancaId,
                    'sfpcoitem_id' => $idSfPcoItem,
                    'subelemento_id' => $idSubelemento,
                    'empenho_id' => $empenhoId,
                    'valorref' => $valorRef,
                ]);
            }
        }

        ContratoFaturaEmpenhosApropriacao::where('contratofatura_id', $instrumentoCobrancaId)
            ->where('empenho_id', $empenhoId)
            ->where('subelemento_id', $idSubelemento)
            ->where('sfpcoitem_id', $idSfPcoItem)
            ->delete();

        return true;
    }

    public function createOrDeleteSfParcelaAjax(Request $request)
    {
        $sf_padrao_id = $request->sf_padrao_id;

        $txtobser_pco = $request->txtobser_pco;

        $sfPcoByPadraoId = SfPco::where('sfpadrao_id', $sf_padrao_id)->get();

        $sfPcoCurrent = SfPco::where('id', $request->idPco)->first();
        if($sfPcoCurrent->despesaantecipada == false){
            $sfPcoCurrent->despesaantecipada = true;
            $sfPcoCurrent->save();
        }

        $sfPcoCurrent->sfpcoitem = $sfPcoCurrent->pcoItens();

        $this->createOrDeleteSfParcela($request, $sfPcoCurrent);

        #salva os dados já inseridos no form para quando retornar a view os dados já virem do banco
        parse_str($request->input('formData'), $dadosFormSfpcoItemArray);
        #inserir dados do form de sfpcoitem no objeto request
        $request->merge($dadosFormSfpcoItemArray);

        $this->salvarDadosSfParcela($request, $sf_padrao_id);

        $sfPadrao = SfPadrao::find($sf_padrao_id);

        $sfPco = SfPco::where('sfpadrao_id', $sf_padrao_id)
            ->orderBy('id', 'asc')
            ->get();

        $valorTotalAbaPco = 0;

//        $sfpcoForm = $this->dadosAbasAlteracaoApropriacao->criarDadosAbaPco(
//            $sfPcoByPadraoId,
//            $valorTotalAbaPco,
//            $sf_padrao_id,
//            $sfPadrao->codtipodh,
//            $this->apropriacao->id
//        );

        $sfpcoForm = $this->dadosAbasAlteracaoApropriacao->criarDadosAbaPco(
            $sfPco,
            $valorTotalAbaPco,
            $sf_padrao_id,
            $sfPadrao->codtipodh,
            $this->apropriacao->id
        );

        $idPcoActive = $request->idSfPco;

        return view(
            'backpack::base.mod.apropriacao.aba-principal-com-orcamento',
            compact(
                'sfPco',
                'txtobser_pco',
                'sfpcoForm',
                'valorTotalAbaPco',
                'idPcoActive'
            )
        );

//        return view(
//            'backpack::base.mod.apropriacao.partials.cronograma-baixa-patrimonial',
//            [
//                'sfpcoForm' => $sfpcoForm,
//                'sfpco_current' => $sfPcoCurrent,
//                'txtobser_pco' => $txtobser_pco,
//            ]
//        );
    }

    private function salvaDadosRelacionadosDeducao($request, $sf_padrao_id)
    {
        $this->salvaDeducao($request);

        if (!empty($request->get('sfitemrecolhedor'))) {
            $this->salvarDadosSfItemRecolhimento(
                $request->get('sfitemrecolhedor'),
                $request,
                $sf_padrao_id
            );
        }

        if (!empty($request->get('sfitemacrescimo'))) {
            $this->salvarDadosSfItemAcrescimo(
                $request->get('sfitemacrescimo'),
                $request,
                $sf_padrao_id
            );
        }
    }

    public function createOrDeleteSfRelItemDeducaoViaAjax(Request $request)
    {

        $sf_padrao_id = $request->sf_padrao_id;

        $this->salvarRemoverDadosVinculoEmpenhoDeducao(
            $request,
            $sf_padrao_id
        );

        parse_str($request->input('formData'), $dadosFormSfDeducaoArray);
        $request->merge($dadosFormSfDeducaoArray);

        $this->salvaDadosRelacionadosDeducao($request, $sf_padrao_id);

        $sfDeducao = SfDeducao::where('sfpadrao_id', $sf_padrao_id)->orderBy('id', 'ASC')->get();
        $totalValorDeducao = 0;

        //$arrayOptions = $this->carregaUnidadeDescentralizadaContrato();
        $opcoes = [];

        //$opcoes = $this->organizaParaSelect2Input($arrayOptions, $opcoes);

        $sfDeducaoForm = $this->dadosAbasAlteracaoApropriacao->criarDadosAbaDeducao(
            $sfDeducao,
            $totalValorDeducao,
            $opcoes,
            $sf_padrao_id,
            $this->apropriacao->id
        );

        $idDeducaoActive = (int)$request->idDeducao;

        return view(
            'backpack::base.mod.apropriacao.aba-deducao',
            compact(
                'sfDeducao',
                'totalValorDeducao',
                'sfDeducaoForm',
                'idDeducaoActive'
            )
        );
    }

    public function createOrDeleteSfItemRecolhimentoAjax(Request $request)
    {

        $sf_padrao_id = $request->sf_padrao_id;

        $this->createOrDeleteSfItemRecolhimento($request, $sf_padrao_id);


        #salva os dados já inseridos no form para quando retornar a view os dados já virem do banco
        parse_str($request->input('formData'), $dadosFormSfDeducaoArray);
        #inserir dados do form de sfpcoitem no objeto request
        $request->merge($dadosFormSfDeducaoArray);

        $this->salvaDadosRelacionadosDeducao($request, $sf_padrao_id);

        //Situação/situações
        $sfDeducao = SfDeducao::where('sfpadrao_id', $sf_padrao_id)->orderBy('id', 'ASC')->get();

        $totalValorDeducao = 0;

        //$arrayOptions = $this->carregaUnidadeDescentralizadaContrato();
        $opcoes = [];

        //$opcoes = $this->organizaParaSelect2Input($arrayOptions, $opcoes);

        $sfDeducaoForm = $this->dadosAbasAlteracaoApropriacao->criarDadosAbaDeducao(
            $sfDeducao,
            $totalValorDeducao,
            $opcoes,
            $sf_padrao_id,
            $this->apropriacao->id
        );

        $idDeducaoActive = (int)$request->idDeducao;

        return view(
            'backpack::base.mod.apropriacao.aba-deducao',
            compact(
                'sfDeducao',
                'totalValorDeducao',
                'sfDeducaoForm',
                'idDeducaoActive'
            )
        );
    }

    public function createOrDeleteSfPreDocDeducaoAjax(Request $request)
    {

        $sf_padrao_id = $request->sf_padrao_id;

        $this->createOrDeleteSfPredocDeducao($request);


        #salva os dados já inseridos no form para quando retornar a view os dados já virem do banco
        parse_str($request->input('formData'), $dadosFormSfDeducaoArray);
        #inserir dados do form de sfpcoitem no objeto request
        $request->merge($dadosFormSfDeducaoArray);

        $this->salvaDadosRelacionadosDeducao($request, $sf_padrao_id);

        //Situação/situações
        $sfDeducao = SfDeducao::where('sfpadrao_id', $sf_padrao_id)->orderBy('id', 'ASC')->get();

        $totalValorDeducao = 0;

        //$arrayOptions = $this->carregaUnidadeDescentralizadaContrato();
        $opcoes = [];

        //$opcoes = $this->organizaParaSelect2Input($arrayOptions, $opcoes);

        $sfDeducaoForm = $this->dadosAbasAlteracaoApropriacao->criarDadosAbaDeducao(
            $sfDeducao,
            $totalValorDeducao,
            $opcoes,
            $sf_padrao_id,
            $this->apropriacao->id
        );

        $idDeducaoActive = (int)$request->idDeducao;

        return view(
            'backpack::base.mod.apropriacao.aba-deducao',
            compact(
                'sfDeducao',
                'totalValorDeducao',
                'sfDeducaoForm',
                'idDeducaoActive'
            )
        );
    }

    public function createOrDeleteSfAcrescimoAjax(Request $request)
    {

        $sf_padrao_id = $request->sf_padrao_id;

        $this->createOrDeleteSfItemAcrescimo($request);


        #salva os dados já inseridos no form para quando retornar a view os dados já virem do banco
        parse_str($request->input('formData'), $dadosFormSfDeducaoArray);
        #inserir dados do form de sfpcoitem no objeto request
        $request->merge($dadosFormSfDeducaoArray);

        $this->salvaDadosRelacionadosDeducao($request, $sf_padrao_id);

        //Situação/situações
        $sfDeducao = SfDeducao::where('sfpadrao_id', $sf_padrao_id)->orderBy('id', 'ASC')->get();

        $totalValorDeducao = 0;

        //$arrayOptions = $this->carregaUnidadeDescentralizadaContrato();
        $opcoes = [];

        //$opcoes = $this->organizaParaSelect2Input($arrayOptions, $opcoes);

        $sfDeducaoForm = $this->dadosAbasAlteracaoApropriacao->criarDadosAbaDeducao(
            $sfDeducao,
            $totalValorDeducao,
            $opcoes,
            $sf_padrao_id,
            $this->apropriacao->id
        );

        $idDeducaoActive = (int)$request->idDeducao;

        return view(
            'backpack::base.mod.apropriacao.aba-deducao',
            compact(
                'sfDeducao',
                'totalValorDeducao',
                'sfDeducaoForm',
                'idDeducaoActive'
            )
        );
    }

    public function createOrDeleteSfPredocDeducao($request)
    {
        #caso tem idParcela entao deleta essa parcela
        if (!is_null($request->idPreDoc)) {
            $sfPreDocDeducao = SfPredoc::where('id', $request->idPreDoc)
                ->where('sfded_id', $request->idDeducao)
                ->first();

            if ($sfPreDocDeducao) {
                $sfPreDocDeducao->delete();
            }
        }

        if (is_null($request->idPreDoc)) {
            try {
                $sfDeducao = SfDeducao::where('id', $request->idDeducao)->first();

                $sfPreDocDeducao = new SfPredoc();
                $sfPreDocDeducao->sfded_id = $sfDeducao->id;
                $sfPreDocDeducao->codmuninf = $sfDeducao->txtinscra ?? '';
                $sfPreDocDeducao->save();

            } catch (\Exception $e) {
                return $e->getMessage();
            }
        }

        return true;
    }

    public function removerParcelasCronBaixaPatrimonial(Request $request)
    {
        $sfPco_current = SfPco::where('id', $request->idPco)->first();

        $sfronBaixaPatrimonial = $sfPco_current->cronBaixaPatrimonial[0] ?? null;

        $sf_padrao_id = $request->sf_padrao_id;

        $sfPadrao = SfPadrao::find($sf_padrao_id);

        $txtobser_pco = $request->txtobser_pco;

        if ($sfronBaixaPatrimonial) {
            try {
                $sfronBaixaPatrimonial->parcela()->delete();
                $sfPco_current->despesaantecipada = false;
                $sfPco_current->save();
            } catch (\Exception $e) {
                \Alert::error('Erro na alteração dos dados da aba')->flash();
            }
        }

        $sfPco = SfPco::where('sfpadrao_id', $sf_padrao_id)
            ->orderBy('id', 'asc')
            ->get();

        $valorTotalAbaPco = 0;

        $sfpcoForm = $this->dadosAbasAlteracaoApropriacao->criarDadosAbaPco(
            $sfPco,
            $valorTotalAbaPco,
            $sf_padrao_id,
            $sfPadrao->codtipodh,
            $this->apropriacao->id
        );

        $idPcoActive = $request->idPco;

        return view(
            'backpack::base.mod.apropriacao.aba-principal-com-orcamento',
            compact(
                'sfPco',
                'txtobser_pco',
                'sfpcoForm',
                'valorTotalAbaPco',
                'idPcoActive'
            )
        );

//        return view(
//            'backpack::base.mod.apropriacao.partials.cronograma-baixa-patrimonial',
//            [
//                'sfpcoForm' => $sfpcoForm,
//                'sfpco_current' => $sfPco,
//                'txtobser_pco' => $txtobser_pco,
//            ]
//        );
    }

    public function createOrDeleteSfCentroCustoViaAjax(Request $request)
    {

        $sf_padrao_id = $request->sf_padrao_id;

        $this->createOrDeleteSfCentroCusto($request, $sf_padrao_id);

        $sfCentroCusto = SfCentroCusto::where('sfpadrao_id', $sf_padrao_id)->orderBy('id', 'asc')->get();

        #atribui dados do form serializado para a variavel $dadosFormSfCentroCustoArray
        parse_str($request->input('formData'), $dadosFormSfCentroCustoArray);
        #insere os dados do form no obj $request
        $request->merge($dadosFormSfCentroCustoArray);
        #salva os dados já inseridos no form
        $this->salvarDadosSfCentroCusto($sfCentroCusto, $request);

        if ($request->numseqpai_checkbox) {
            $this->salvarDadosSfRelItemVlrCC($request->numseqpai_checkbox, $request);
        }

        $sfCentroCustoForm = $this->dadosAbasAlteracaoApropriacao->criarDadosFormAbaCentroCusto(
            $sfCentroCusto,
            $sf_padrao_id,
            $this->apropriacao->id
        );

        //necessário recuperar o total da aba pco para exibir nas labels informativas
        $valorTotalCentroCustoAInformar = $this->recuperarValorTotalCentroCustoAInformar($sf_padrao_id);

        return view(
            'backpack::base.mod.apropriacao.aba-centro-de-custo',
            compact(
                'sfCentroCusto',        //Dados da aba Centro de Custo
                'sfCentroCustoForm',    //Form da aba Centro de Custo
                'valorTotalCentroCustoAInformar' //pega o valor total da aba pco para exibir em centro de custo
            )
        );
    }

    public function reloadingCentroCustoViaAjax(Request $request)
    {
        $sf_padrao_id = $request->sf_padrao_id;

        $sfCentroCusto = SfCentroCusto::where('sfpadrao_id', $sf_padrao_id)->orderBy('id', 'asc')->get();

        $sfCentroCustoForm = $this->dadosAbasAlteracaoApropriacao->criarDadosFormAbaCentroCusto(
            $sfCentroCusto,
            $sf_padrao_id,
            $this->apropriacao->id
        );

        //necessário recuperar o total da aba pco para exibir nas labels informativas
        $valorTotalCentroCustoAInformar = $this->recuperarValorTotalCentroCustoAInformar($sf_padrao_id);

        return view(
            'backpack::base.mod.apropriacao.aba-centro-de-custo',
            compact(
                'sfCentroCusto',        //Dados da aba Centro de Custo
                'sfCentroCustoForm',    //Form da aba Centro de Custo
                'valorTotalCentroCustoAInformar' //pega o valor total da aba pco para exibir em centro de custo
            )
        );
    }

    private function createOrDeleteSfPcoItem($request)
    {
        #nesse caso deleta o sfpcoitem pelo id
        if (!is_null($request->idPcoItem)) {
            $sfRelItemVlrCC = $this->countSfRelItemVlrCCPorNumseqPaiNumseqItem($request);

            $existeSfRelItemVlrCC = $this->verificarSfPcoItemExisteEmSfRelItemVlrCC($sfRelItemVlrCC);

            if ($existeSfRelItemVlrCC) {
                return ['status' => 'error', 'mensagem' => 'Empenho vinculado ao Centro de Custo'];
            }

            $this->deleteSfPcoItem($request);

            return ['status' => 'success', 'mensagem' => 'Empenho removido'];
        }

        if ($request->repetirContasEmpenhos) {
            $arrPcoItem = [
                'sfpco_id' => $request->idPco,
                'numempe' => $request->numempe,
                'codsubitemempe' => $request->codsubitemempe,
                'vlr' => $request->vlr,
                'numclassa' => $request->repetirNumClassA,
                'numclassb' => $request->repetirNumClassB,
                'numseqitem' => $request->numseqitem,
                'contratofatura_id' => $request->contratofatura_id
            ];
        } else {

            #nesse caso insere um novo pco item
            $arrPcoItem = [
                'sfpco_id' => $request->input('idPco'),
                'numempe' => $request->numempe,
                'codsubitemempe' => $request->codsubitemempe,
                'vlr' => $request->vlr,
                'numclassa' => $request->numclassa,
                'numclassb' => $request->numclassb,
                'numseqitem' => $request->numseqitem,
                'contratofatura_id' => $request->contratofatura_id
            ];
        }

        $this->faturaController->criarSfPcoItemViaArray($arrPcoItem);

        return '';
    }

    private function createOrDeleteSfParcela($request, SfPco $sfPco)
    {
        #caso tem idParcela entao deleta essa parcela
        if (!is_null($request->idParcela)) {
            $this->deleteSfParcela($request->idParcela);
        }

        #caso nao tenha idParcela entao insere nova parcela
        if (is_null($request->idParcela)) {
            try {
                $novoSfParcela = new SfParcela();
                $novoSfParcela->sfcronbaixapatrimonial_id = $sfPco->cronBaixaPatrimonial[0]->id;
                $novoSfParcela->dtprevista = now();
                $novoSfParcela->vlr = 0;
                $novoSfParcela->save();

            } catch (\Exception $e) {
                return $e->getMessage();
            }
        }

        #reordena as parcelas
        $this->dadosAbasAlteracaoApropriacao->reorderNumParcela($sfPco->cronBaixaPatrimonial[0]->id);

        return true;
    }

    private function createOrDeleteSfItemRecolhimento($request, $sf_padrao_id = null)
    {
        #caso tem idParcela entao deleta essa parcela
        if (!is_null($request->idRecolhedor)) {
            $sfItemRecolhimento = SfItemRecolhimento::find($request->idRecolhedor);
            $sfItemRecolhimento->delete();
        }

        #caso nao tenha idParcela entao insere nova parcela
        if (is_null($request->idRecolhedor)) {
            try {
                $sfDeducao = SfDeducao::where('id', $request->idDeducao)->first();

                if ($sf_padrao_id) {
                    $sfDadosBasicos = SfDadosBasicos::where('sfpadrao_id', $sf_padrao_id)->first();
                }

                $novoSfItemRecolhimento = new SfItemRecolhimento();
                $novoSfItemRecolhimento->sfded_id = $sfDeducao->id;
                $novoSfItemRecolhimento->codrecolhedor = $sfDadosBasicos->codcredordevedor ?? '';
                $novoSfItemRecolhimento->vlrbasecalculo = $sfDadosBasicos->vlr ?? 0;
                $novoSfItemRecolhimento->numseqitem = $sfDeducao->getMaxSeqItemRecolhimento();
                $novoSfItemRecolhimento->vlr = 0;
                $novoSfItemRecolhimento->save();

            } catch (\Exception $e) {
                return $e->getMessage();
            }
        }

        return true;
    }

    private function createOrDeleteSfItemAcrescimo($request)
    {
        #caso tem idParcela entao deleta essa parcela
        if (!is_null($request->idAcrescimo)) {
            $sfItemAcrescimo = SfAcrescimo::find($request->idAcrescimo);
            $sfItemAcrescimo->delete();
        }

        if (is_null($request->idAcrescimo)) {
            try {
                $sfDeducao = SfDeducao::where('id', $request->idDeducao)->first();

                $sfItemAcrescimo = new SfAcrescimo();
                $sfItemAcrescimo->sfded_id = $sfDeducao->id;
                $sfItemAcrescimo->vlr = 0;
                $sfItemAcrescimo->save();

            } catch (\Exception $e) {
                return $e->getMessage();
            }
        }

        return true;
    }

    private function deleteSfPcoItem($request)
    {
        $sfPcoItem = SfPcoItem::find($request->idPcoItem);

        if ($sfPcoItem !== null) {
            $sfPcoItem->delete();
        }
    }

    private function deleteSfParcela($idParcela)
    {
        $sfParcela = SfParcela::find($idParcela);
        $sfParcela->delete();
    }

    private function countSfRelItemVlrCCPorNumseqPaiNumseqItem($request)
    {

        $centroCustos = SfCentroCusto::where('sfpadrao_id', $request->sf_padrao_id)->get()->pluck('id')->toArray();

        return Sfrelitemvlrcc::where('numseqpai', $request->numseqpai)
            ->where('numseqitem', $request->numseqitem)
            ->whereIn('sfcc_id', $centroCustos)
            ->count();
    }

    /**
     * Recupera centro de custo pelo sf_padrao_id
     * Verifica se existe registro em Sfrelitemvlrcc por centro de custo e numseqpai(sfpco) e numseqitem(sfpcoitem)
     *
     * @param $request
     * @return mixed
     */
    private function countSfRelItemVlrCCPorSfPco($request)
    {

        $centroCustos = SfCentroCusto::where('sfpadrao_id', $request->sf_padrao_id)->get()->pluck('id')->toArray();

        $sfpcoItem = SfPcoItem::where('sfpco_id', $request->idPco)->get();

        foreach ($sfpcoItem as $sfpcoitem_current) {
            $sfrelitemvlrcc = Sfrelitemvlrcc::where('numseqpai', $sfpcoitem_current->pco->numseqitem)
                ->where('numseqitem', $sfpcoitem_current->numseqitem)
                ->whereIn('sfcc_id', $centroCustos)
                ->count();

            if ($sfrelitemvlrcc > 0) {
                return $sfrelitemvlrcc;
            }
        }

        return 0;
    }

    private function verificarSfPcoItemExisteEmSfRelItemVlrCC($sfRelItemVlrCC)
    {
        if ($sfRelItemVlrCC > 0) {
            return true;
        }

        return false;
    }

    private function createOrDeleteSfCentroCusto($request, $sf_padrao_id)
    {
        if (!is_null($request->idCentroCusto)) {
            $sfCentroCusto = SfCentroCusto::find($request->idCentroCusto);
            $sfCentroCusto->delete();
        }

        if (is_null($request->idCentroCusto)) {
            $arrNovoCentroCusto = [
                'sfpadrao_id' =>          $sf_padrao_id,
                'codcentrocusto' =>     $request->codcentrocusto,
                'mesreferencia' =>      $request->mesreferencia,
                'anoreferencia' =>      $request->anoreferencia,
                'codugbenef' =>         $request->codugbenef,
                'codsiorg' =>         $request->codsiorg,
                'numseqitem' =>         $request->numseqitem
            ];

            $this->faturaController->criarSfCentroCustoViaAjax($arrNovoCentroCusto);
        }
    }

    /**
     * @param array $arrayOptions
     * @param array $opcoes
     * @return array
     */
    public function organizaParaSelect2Input(array $arrayOptions, array $opcoes): array
    {
        if (!empty($arrayOptions)) {
            foreach ($arrayOptions as $item) {
                $value = $item['value'];
                $description = $item['description'];
                $opcoes[$value] = $value . ' - ' . $description;
            }
        }
        return $opcoes;
    }

    /**
     * @param Request $request
     * @return void
     */
    public function salvaDeducao(Request $request): void
    {
        if ($request->get('sfdeducao') !== null) {
            foreach ($request->get('sfdeducao') as $key => $sfdeducao_current) {
                $sfdeducao = SfDeducao::find($sfdeducao_current);

                if ($sfdeducao) {

                    $sfdeducao->codsit = $request->get('sfdeducaocodsit')[$key];
                    $sfdeducao->codugpgto = (int)$request->get('sfdeducaocodugpgto')[$key];
                    $sfdeducao->vlr = $this->dadosAbasAlteracaoApropriacao->formatarVlrParaBanco($request->get('sfdeducaovlr')[$key]);
                    $sfdeducao->possui_acrescimo = $request->get('sfdeducaopossui_acrescimo')[$key] ?? false;
                    $sfdeducao->txtinscra = str_replace(".", '', $request->get('txtinscra')[$sfdeducao->id] ?? '');
                    $sfdeducao->txtinscrb = str_replace(".", '', $request->get('txtinscrb')[$sfdeducao->id] ?? '');
                    if (!empty($request->get('sfdeducaodtpgtoreceb')[$key]) && isset($request->get('sfdeducaodtpgtoreceb')[$key])) {
                        $sfdeducao->dtpgtoreceb = $request->get('sfdeducaodtpgtoreceb')[$key];
                    }
                    if (!empty($request->get('sfdeducaodtvenc')[$key]) && isset($request->get('sfdeducaodtvenc')[$key])) {
                        $sfdeducao->dtvenc = $request->get('sfdeducaodtvenc')[$key];
                    }
                    $sfdeducao->save();

                    $this->salvaPredocDeducao($sfdeducao, $request, $key);
                }
            }
        }
    }

    /**
     * @param $sfdeducao
     * @param Request $request
     * @param $key
     * @return void
     */
    public function salvaPredocDeducao($sfdeducao, Request $request, $key): void
    {
        $sfdeducao_predoc_item = SfPredoc::where('sfded_id', $sfdeducao->id)->first();

        if (!$sfdeducao_predoc_item || !isset($request->get('sfpredoctipo')[$sfdeducao_predoc_item->sfded_id])) {
            return;
        }

        $tipoPredoc = $request->get('sfpredoctipo')[$sfdeducao_predoc_item->sfded_id];
        $codTipoDarf = $request->get('sfpredoccodtipodarf')[$sfdeducao_predoc_item->sfded_id] ?? null;

        if ($tipoPredoc === 'predocdarf') {
            $this->processarPredocDarf($sfdeducao_predoc_item, $codTipoDarf, $request, $sfdeducao_predoc_item->sfded_id);
        }

        if ($tipoPredoc === 'predocdar') {
            $this->processarPredocDar($sfdeducao_predoc_item, $request, $sfdeducao_predoc_item->sfded_id);
        }

        $sfdeducao_predoc_item->codtipoob = $codTipoDarf;
        $sfdeducao_predoc_item->dtprdoapuracao = $this->dataEValida($request->get('sfpredocdtprdoapuracao')[$sfdeducao_predoc_item->sfded_id]) ? $request->get('sfpredocdtprdoapuracao')[$sfdeducao_predoc_item->sfded_id] : null;
        $sfdeducao_predoc_item->txtprocesso = $request->get('sfpredoctxtprocesso')[$sfdeducao_predoc_item->sfded_id];
        $sfdeducao_predoc_item->codrecurso = $request->get('sfpredoccodrecurso')[$sfdeducao_predoc_item->sfded_id];
        $sfdeducao_predoc_item->txtobser = $request->get('sfpredoctxtobser')[$sfdeducao_predoc_item->sfded_id];
        $sfdeducao_predoc_item->tipo = $tipoPredoc;

        $sfdeducao_predoc_item->save();
    }

    private function processarPredocDarf($sfdeducao_predoc_item, $codTipoDarf, Request $request, $key): void
    {
        switch ($codTipoDarf) {
            case 'DFS':
                $sfdeducao_predoc_item->numref = $request->get('sfpredocnumref')[$key] ?? null;
                $sfdeducao_predoc_item->vlrrctabrutaacum = $this->formatarValor($request->get('sfpredocvlrrctabrutaacum')[$key]);
                $sfdeducao_predoc_item->vlrpercentual = $this->formatarValor($request->get('sfpredocvlrpercentual')[$key]);
                break;

            case 'DFN':
                $sfdeducao_predoc_item->numcodbarras = $request->get('sfpredocnumcodbarras')[$key] ?? null;
                break;

            case 'DFD':
                $sfdeducao_predoc_item->numref = $request->get('sfpredocnumref')[$key] ?? null;
                break;
        }
    }

    private function processarPredocDar($sfdeducao_predoc_item, Request $request, $key): void
    {
        $sfdeducao_predoc_item->codugtmdrserv = (int)$request->get('sfpredoccodugtmdrserv')[$key];
        $sfdeducao_predoc_item->codmuninf = $request->get('sfpredoccodmuninf')[$key] ?: null;
        $sfdeducao_predoc_item->numnf = (int)$request->get('sfpredocnumnf')[$key];
        $sfdeducao_predoc_item->vlrnf = $this->formatarValor($request->get('sfpredocvlrnf')[$key]);
        $sfdeducao_predoc_item->mesreferencia = substr($request->get('sfpredocreferencia')[$key] ?? '', 0, 2);
        $sfdeducao_predoc_item->anoreferencia = substr($request->get('sfpredocreferencia')[$key] ?? '', 3, 6);
        $sfdeducao_predoc_item->dtemisnf = $this->dataEValida($request->get('sfpredocdtemisnf')[$key]) ? $request->get('sfpredocdtemisnf')[$key] : null;
        $sfdeducao_predoc_item->txtserienf = $request->get('sfpredoctxtserienf')[$key];
        $sfdeducao_predoc_item->numsubserienf = (int)$request->get('sfpredocnumsubserienf')[$key];
        $sfdeducao_predoc_item->numaliqnf = $this->formatarValor($request->get('sfpredocnumaliqnf')[$key]);
    }

    private function dataEValida($data, $formato = 'Y-m-d'): bool
    {
        $d = DateTime::createFromFormat($formato, $data);
        return $d && $d->format($formato) === $data;
    }

    private function formatarValor($valor): float
    {
        return $this->dadosAbasAlteracaoApropriacao->formatarVlrParaBanco($valor) ?? 0;
    }

    private function redirectToRoute(string $routeName, $numAba, $sf_padrao_id)
    {
        return redirect()->route(
            $routeName,
            [
                'sf_padrao_id' =>  $sf_padrao_id,
                'numaba' => $numAba
            ]
        );
    }

    /**
     * Atualiza o valor da aba liberada na sessao
     *
     * @param int $numAbaLiberada
     * @param int $padraoId
     */
    private function atualizarNumAbaLiberadaNaSessao(int $numAbaLiberada, int $padraoId): void
    {

        $arrPadraoAlteracaoApropriacao = session()->get('arrPadraoAlteracaoApropriacao');

        $arrPadraoAlteracaoApropriacao[$padraoId]['numAbaLiberada'] = $numAbaLiberada;

        session(['arrPadraoAlteracaoApropriacao' => $arrPadraoAlteracaoApropriacao]);
    }


    private function deleteIdsRemovidosCheckbox($sfpadrao_id, $request)
    {
        $arrIdsChecked = [];

        $arrIdsTotal = SfCentroCusto::join('sfrelitemvlrcc', 'sfrelitemvlrcc.sfcc_id', '=', 'sfcentrocusto.id')
            ->where('sfpadrao_id', $sfpadrao_id)
            ->pluck('sfrelitemvlrcc.id')
            ->toArray();

        if ($request->numseqpai_checkbox) {
            foreach ($request->numseqpai_checkbox as $key => $value) {
                $arrIdsChecked = Sfrelitemvlrcc::where('sfcc_id', $request->centro_custo_id[$key])
                    ->where('numseqpai', $request->numseqpai_checkbox[$key])
                    ->where('numseqitem', $request->numseqpai_checkbox[$key])
                    ->pluck('id')->toArray();
            }
        }

        if (count($arrIdsTotal) > 0) {
            //remove ids que foram removidos da grid
            foreach ($arrIdsTotal as $key => $value) {
                if (!in_array($arrIdsTotal[$key], $arrIdsChecked)) {
                    Sfrelitemvlrcc::find($arrIdsTotal[$key])->delete();
                }
            }
        }
    }

    public function atualizarCodTipoDhPadraoViaAjax(Request $request)
    {

        $sf_padrao_id = $request->sf_padrao_id;
        $codtipodh = $request->codtipodh;

        DB::beginTransaction();
        try {
            SfPadrao::find($sf_padrao_id)->update(['codtipodh' => $codtipodh]);

            DB::commit();
        } catch (\Exception $exc) {
            DB::rollback();
        }
    }

    public function atualizarCodTipoCodugEmitViaAjax(Request $request)
    {

        $sf_padrao_id = $request->sf_padrao_id;
        $codugemit = $request->codugemit;

        preg_match('/\(\s*(\d+)\s*\)/', $codugemit, $matches);
        if (isset($matches[1])) {
            $valorNumerico = $matches[1];
        } else {
            return;
        }

        DB::beginTransaction();
        try {
            SfPadrao::find($sf_padrao_id)->update(['codugemit' => $valorNumerico]);
            SfPco::where('sfpadrao_id',$sf_padrao_id)->update(['codugempe' => $valorNumerico]);

            DB::commit();
        } catch (\Exception $exc) {
            DB::rollback();
        }
    }

    public function recuperarSituacaoPorTipoDhViaAjax(Request $request)
    {
        $codtipodh = $request->codtipodh;

        $idTipoSituacaoDHSiafi = $this->retornaIdCodigoItemPorDescricaoEDescres('Tipos Situações DH SIAFI', 'SIT0001');
        $idCodTipoDH = $this->retornaIdCodigoItemPorDescricaoEDescres('Tipos DH SIAFI', $codtipodh);

        $arrayTipoSituacaoDHSiafi = Execsfsituacao::join('execsfsituacao_tipodoc', 'execsfsituacao_tipodoc.execsfsituacao_id', '=', 'execsfsituacao.id')
            ->where('tipo_situacao', $idTipoSituacaoDHSiafi)
            ->where('tipodoc_id', $idCodTipoDH)
            ->orderBy('codigo')
            ->pluck('codigo')
            ->toArray();

        return $arrayTipoSituacaoDHSiafi;
    }

    /**
     * Retorna Centro de Custo a Informar
     *
     * soma o valor de sfpcoitem
     *
     * soma apenas o valor dos itens onde situacao possui afeta_custo true
     *
     * @param int $sf_padrao_id
     * @return string
     */
    public function recuperarValorTotalCentroCustoAInformar(int $sf_padrao_id): string
    {
        $sfPco = SfPco::where('sfpadrao_id', $sf_padrao_id)
            ->orderBy('id', 'asc')
            ->get();

        $valorTotalCentroCustoAInformar = 0;

        foreach ($sfPco as $sfpco_current) {
            $execsfsituacao = Execsfsituacao::where('codigo', $sfpco_current->codsit)->first();

            if (!$execsfsituacao) {
                continue;
            }

            if ($execsfsituacao->afeta_custo) {
                $valorTotalCentroCustoAInformar += $sfpco_current->pcoItens->sum('vlr');
            }
        }

        return number_format($valorTotalCentroCustoAInformar, 2, ',', '.');
    }


    /*
     * @description Parametros da função são para os casos que é chamada como API por um sistema externo.
     * @param int|null $sf_padrao_id
     * @param int|null $apropriacao_id
     * @param int|null $contratofaturasId
     * @param boolean|null $isApi
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function removerApropriacaoFatura(
        $sf_padrao_id = null,
        $apropriacao_id = null,
        $contratofaturasId = null,
        $isApi = null
    ) {
        try {
            DB::beginTransaction();
            $sf_padrao_id = $sf_padrao_id ? $sf_padrao_id : Route::current()->parameter('sf_padrao_id');
            $apropriacao_id = $apropriacao_id ? $apropriacao_id : Route::current()->parameter('apropriacao_id');
            $apropriacaoFaturas = ApropriacaoFaturas::where('id', $apropriacao_id)->first();


            $idFirstContratoFatura = $apropriacaoFaturas->faturas()->first()->id;

            #seta situacao das faturas para pendente e sfpadrao_id das faturas para null
            foreach ($apropriacaoFaturas->faturas()->get() as $contratoFaturas) {
                $contratoFaturas->situacao = 'PEN';
                $contratoFaturas->sfpadrao_id = null;
                $contratoFaturas->save();

                ContratoFaturaEmpenhosApropriacao::where('contratofatura_id',$contratoFaturas->id)->delete();
            }

            $apropriacaoFaturas->faturas()->detach(); //faturas é a referencia para apropriacoes_faturas_contratofaturas
            $apropriacaoFaturas->forceDelete();

            #recupera models das tabelas envolvidas na apropriacao de faturas
            $sfPadrao           = SfPadrao::find($sf_padrao_id);
            $sfDadosBasicos     = SfDadosBasicos::where('sfpadrao_id', $sf_padrao_id)->first();
            $sfDocOrigem        = SfDocOrigem::where('sfdadosbasicos_id', $sfDadosBasicos->id);
            $sfPco              = SfPco::where('sfpadrao_id', $sf_padrao_id);
            $sfDeducao          = SfDeducao::where('sfpadrao_id', $sf_padrao_id);
            $sfDadosPagamento   = SfDadosPgto::where('sfpadrao_id', $sf_padrao_id);
            $sfPreDoc           = SfPredoc::whereHas('sfdadospgto', function ($query) use ($sf_padrao_id) {
                $query->where('sfpadrao_id', $sf_padrao_id);
            });
            $sfDomicilioBamcario = SfDomicilioBancario::whereHas('sfpredoc.sfdadospgto', function ($query) use ($sf_padrao_id) {
                $query->where('sfpadrao_id', $sf_padrao_id);
            });

            #delete nas tabelas envolvidas na apropriacao de faturas
            $sfCentroCusto = SfCentroCusto::where('sfpadrao_id', $sf_padrao_id)->get();

            foreach ($sfCentroCusto as $centro_custo_current) {
                $arrSfrelitemvlrcc = Sfrelitemvlrcc::where('sfcc_id', $centro_custo_current->id)->pluck('id')->toArray();

                Sfrelitemvlrcc::whereIn('id', $arrSfrelitemvlrcc)->forceDelete();

                $centro_custo_current->forceDelete();
            }

            $sfDocOrigem->forceDelete();
            $sfDadosBasicos->forceDelete();
            $sfPco->forceDelete();
            $sfDeducao->forceDelete();
            $sfDadosPagamento->forceDelete();
            $sfPreDoc->forceDelete();
            $sfDomicilioBamcario->forceDelete();
            $sfPadrao->delete();

            DB::commit();

            //deletado com sucesso para chamada externa via API
            if ($isApi) {
                return $contratofaturasId;
            }

            \Alert::success('Apropriação de fatura(s) removida!')->flash();

            $contratofaturasId = $contratofaturasId ? $contratofaturasId : Route::current()->parameter('contratofaturas_id');

            #nesse caso a rota está na listagem de apropriacao de um instrumento de cobrança
            if (!is_null($contratofaturasId)) {
                return redirect()->route('crud.apropriacao.index', $contratofaturasId);
            }

            #nesse caso a rota está dentro da apropriação dos instrumentos de cobrança
            return redirect()->route('crud.fatura.index');
        } catch (\Exception $e) {
            DB::rollback();

            if ($isApi) {
                return $e;
            }

            \Alert::error('Ocorreu um erro ao tentar remover a apropriação de fatura(s)!')->flash();

            if (!is_null($contratofaturasId)) {
                return redirect()->route('crud.apropriacao.index', $contratofaturasId);
            }

            return redirect()->route(
                'alterar.antes.apropriar.fatura',
                [
                    'sf_padrao_id' => $sf_padrao_id,
                    'apropriacao_id' => $this->apropriacao->id,
                ]
            );
        }
    }

    public function recuperarSituacaoApropriacao($apropriacao_id)
    {
        $apropriacaoFatura = ApropriacaoFaturas::where('id', $apropriacao_id)->first();

        $descricaoCodigoItem = $this->retornaDescCodigoItem($apropriacaoFatura->fase_id);

        return $this->retornaDescresCodigoItem(
            'Status Apropriação Instrumento de Cobrança SIAFI',
            $descricaoCodigoItem
        );
    }

    public function retornarValorTotalAbaPco($sf_padrao_id)
    {

        $sfPco = SfPco::where('sfpadrao_id', $sf_padrao_id)
            ->orderBy('id', 'asc')
            ->get();

        #recupera valor total da aba pco
        $arrValorTotalPco = [];

        foreach ($sfPco as $key => $sfpco_current) {
            $execsfsituacao = Execsfsituacao::where('codigo', $sfpco_current->codsit)->first();

            if ($execsfsituacao) {
                if ($execsfsituacao->afeta_custo) {
                    $arrValorTotalPco[$key] = SfPcoItem::where('sfpco_id', $sfpco_current->id)->sum('vlr');
                }
            }
        }

        return $sumValorTotalPco = array_reduce($arrValorTotalPco, function ($previous, $current) {
            return $previous + $current;
        }, 0);
    }

    public function buscarFornecedorViaAjax(Request $request)
    {
        $cpf_cnpj_idgener = $request->codcredordevedor;

        $fornecedor = Fornecedor::where('cpf_cnpj_idgener', $cpf_cnpj_idgener)->first();
        return response()->json(['fornecedor' => $fornecedor]);
    }

    public function buscarSubelementoDoEmpenhoViaAjax(Request $request)
    {
        $numeroEmpenho = $request->empenho;
        $codUg = $request->codUg;
        $from = $request->from ?? '';

        return $this->dadosAbasAlteracaoApropriacao->buscarSubelementoDoEmpenho($numeroEmpenho, $codUg, $from);
    }

    public function buscarSubelementoCadastradoSfPcoItemDoEmpenhoViaAjax(Request $request)
    {
        $sfpco = $request->sfpco;
        $sfpcoitem = $request->sfpcoitem;

        return $this->dadosAbasAlteracaoApropriacao->buscarSubelementoCadastradoSfPcoItemDoEmpenho($sfpco, $sfpcoitem);
    }

    public function atualizaCredoApropriacao(Request $request)
    {

        $sf_padrao_id = $request->sf_padrao_id;
        $credor_devedor = $request->credor_devedor;

        if (strlen($this->limpa_cpf_cnpj($credor_devedor)) == 6 || strlen($this->limpa_cpf_cnpj($credor_devedor)) == 14 || preg_match('/^EX/i', $credor_devedor)) {

            $contrato = Contrato::where('id', $this->contrato_id)->first();
            $fornecedor = Fornecedor::where('cpf_cnpj_idgener', $credor_devedor)->first();
            $sfdadosbasicos = SfDadosBasicos::where('sfpadrao_id', $sf_padrao_id)->first();

            if (!$fornecedor) {
                return response()->json(['error' => 'Fornecedor não localizado.', 'cnpj' => $this->formataCnpjCpf($sfdadosbasicos->codcredordevedor) ?? '']);
            }

            if ($sfdadosbasicos) {

                //verifica tipo do fornecedor se for ug troca para cnpj e vice versa.
                $tipo_credor_devedor = $this->retornaTipoFornecedor($this->limpa_cpf_cnpj($credor_devedor));
                $tipo_fornecedor_contrato = $this->retornaTipoFornecedor($this->limpa_cpf_cnpj($contrato->fornecedor->cpf_cnpj_idgener));

                if ($tipo_credor_devedor == 'UG' && $tipo_fornecedor_contrato == 'UG' && $this->limpa_cpf_cnpj($contrato->fornecedor->cpf_cnpj_idgener) !== $this->limpa_cpf_cnpj($credor_devedor)) {
                    return response()->json(['error' => 'Não é permitido alterar de UG para UG.', 'cnpj' => $contrato->fornecedor->cpf_cnpj_idgener ?? '']);
                }

                if ($tipo_credor_devedor == 'JURIDICA' && $tipo_fornecedor_contrato == 'JURIDICA' && $this->limpa_cpf_cnpj($contrato->fornecedor->cpf_cnpj_idgener) !== $this->limpa_cpf_cnpj($credor_devedor)) {
                    return response()->json(['error' => 'Não é permitido alterar de um CNPJ para outro CNPJ.', 'cnpj' => $contrato->fornecedor->cpf_cnpj_idgener ?? '']);
                }

                if ($tipo_fornecedor_contrato != 'IDGENERICO' && $tipo_credor_devedor == 'IDGENERICO') {
                    return response()->json(['error' => 'Não é permitido alterar para fornecedor estrangeiro', 'cnpj' => $contrato->fornecedor->cpf_cnpj_idgener ?? '']);
                }

                //Altera credor dados basicos e pagamento
                $sfdadosbasicos->codcredordevedor = $this->limpa_cpf_cnpj($credor_devedor);
                $sfdadosbasicos->save();

                SfDadosPgto::where('sfpadrao_id', $sf_padrao_id)->update(['codcredordevedor' => $this->limpa_cpf_cnpj($credor_devedor)]);

                SfDocOrigem::where('sfdadosbasicos_id', $sfdadosbasicos->id)->update(['codidentemit' => $this->limpa_cpf_cnpj($credor_devedor)]);
            }
        }

        return response()->json(['fornecedor_nome' => $fornecedor->nome ?? '', 'cnpj' => $fornecedor->cpf_cnpj_idgener ?? '']);
    }

    public function geraTelaPcoTab($sf_padrao_id)
    {
        $txtobser = SfDadosBasicos::where('sfpadrao_id', $sf_padrao_id)->first()->txtobser;
        $sf_padrao_id = Route::current()->parameter('sf_padrao_id');

        $sfPadrao = SfPadrao::find($sf_padrao_id);

        $sfPco = SfPco::where('sfpadrao_id', $sf_padrao_id)
            ->orderBy('id', 'asc')
            ->get();
        $valorTotalAbaPco = 0;

        $sfpcoForm = $this->dadosAbasAlteracaoApropriacao->criarDadosAbaPco(
            $sfPco,
            $valorTotalAbaPco,
            $sf_padrao_id,
            $sfPadrao->codtipodh,
            $this->apropriacao->id
        );

        return view('vendor.backpack.base.mod.apropriacao.aba-principal-com-orcamento', [
            'sfPco' => $sfPco,
            'txtobser_pco' => $txtobser,
            'sfpcoForm' => $sfpcoForm,
            'valorTotalAbaPco' => $valorTotalAbaPco,
            'idPcoActive' => null
        ]);
    }

    public function geraTelaDadosBasicosTab()
    {
        $sfDadosBasicos = $this->dadosAbasAlteracaoApropriacao->criarDadosAbaDadosBasicos();

        $sfDadosBasicosForm = \FormBuilder::create(SfDadosBasicosForm::class, [
            'url' => route('fatura.form.dados-basicos', [$sfDadosBasicos->sf_padrao_id, $this->apropriacao->id]),

            'method' => 'POST',
            'model' => $sfDadosBasicos->modelSfDadosBasicos,
            'novalidate' => 'novalidate',
            'sfDadosBasicos' => $this->carregaUnidadeDescentralizadaContrato(),
            'attr' => ['id' => 'sfDadosBasicosForm']
        ]);

        return view('vendor.backpack.base.mod.apropriacao.aba-dados-basicos', [
            'sfDadosBasicos' => $sfDadosBasicos,
            'sfDadosBasicosForm' => $sfDadosBasicosForm,
        ]);
    }

    public function geraTelaDadosPagamentoTab($sf_padrao_id, $apropriacao_id)
    {
        $sfPreDoc = SfPredoc::with('sfdomiciliobancarioFavo', 'sfdomiciliobancarioPgto')
            ->whereHas('sfdadospgto', function ($query) use ($sf_padrao_id) {
                $query->where('sfpadrao_id', $sf_padrao_id);
            })->first();

        $arr_favorecidos = SfDadosPgto::where('sfpadrao_id', $sf_padrao_id)->get();

        $arrDadosPagamento =  $this->dadosAbasAlteracaoApropriacao->criarDadosAbaDadosPagamento(
            $arr_favorecidos,
            $this->contrato_id,
            $this->apropriacao->id,
            $sf_padrao_id
        );

        $situacaoApropriacao = $this->recuperarSituacaoApropriacao($apropriacao_id);


        return view('vendor.backpack.base.mod.apropriacao.aba-dados-pagamento', [
            'sfDadosBasicos' => $arrDadosPagamento['sfDadosBasicos'],
            'sfDadosPagamentoForm' => $arrDadosPagamento['formDadosPagamento'],
            'arrFavorecidos' => $arr_favorecidos,
            'sfPreDoc' => $sfPreDoc,
            'situacaoApropriacao' => $situacaoApropriacao,
        ]);
    }

    public function geraTelaDeducaoTab($sf_padrao_id)
    {
        /***************************************************************************************
         ****************************** ABA DEDUÇÃO ********************************************/

        //Situação/situações
        $sfDeducao = SfDeducao::where('sfpadrao_id', $sf_padrao_id)->orderBy('id', 'ASC')->get();

        $totalValorDeducao = 0;

        $opcoes = [];

        $sfDeducaoForm = $this->dadosAbasAlteracaoApropriacao->criarDadosAbaDeducao(
            $sfDeducao,
            $totalValorDeducao,
            $opcoes,
            $sf_padrao_id,
            $this->apropriacao->id
        );

        return view('vendor.backpack.base.mod.apropriacao.aba-deducao',
            [
                'sfDeducaoForm' => $sfDeducaoForm,
                'sfDeducao' => $sfDeducao,
                'idDeducaoActive' => null,
                'totalValorDeducao' => $totalValorDeducao
            ]);
    }

    public function geraTelaCentroCustoTab($sf_padrao_id)
    {
        /***************************************************************************************
         ****************************** ABA CENTRO DE CUSTO ************************************/

        $sfPadrao = SfPadrao::find($sf_padrao_id);

        $unidadeDhPadrao = Unidade::where('codigo', str_pad($sfPadrao->codugemit, 6, "0", STR_PAD_LEFT))->first();

        $sfCentroCusto = SfCentroCusto::where('sfpadrao_id', $sf_padrao_id)->orderBy('id', 'asc')->get();

        $sfCentroCustoForm = $this->dadosAbasAlteracaoApropriacao->criarDadosFormAbaCentroCusto(
            $sfCentroCusto,
            $sf_padrao_id,
            $this->apropriacao->id
        );

        $sfPco = SfPco::where('sfpadrao_id', $sf_padrao_id)
            ->orderBy('id', 'asc')
            ->get();

        $afetaCusto = $unidadeDhPadrao->utiliza_custos;
        foreach ($sfPco as $sfpco_current) {
            $execsfsituacao = Execsfsituacao::where('codigo', $sfpco_current->codsit)->first();
            if ($execsfsituacao) {
                $afetaCusto = $execsfsituacao->afeta_custo;
            }
        }

        $valorTotalCentroCustoAInformar = $this->recuperarValorTotalCentroCustoAInformar($sf_padrao_id);


        return view('vendor.backpack.base.mod.apropriacao.aba-centro-de-custo',
            [
                'sfCentroCustoForm' => $sfCentroCustoForm,
                'sfCentroCusto' => $sfCentroCusto,
                'valorTotalCentroCustoAInformar' => $valorTotalCentroCustoAInformar, //pega o valor total da aba pco para exibir em centro de custo
                'situacaoUtilizaCentroCusto' => $afetaCusto,
            ]);
    }
}
