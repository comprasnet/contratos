<?php

namespace App\Http\Controllers\Apropriacao;

use App\Http\Requests\ApropriacaoInstrumentoCobrancaRequest as UpdateRequest;
use App\Models\Contratofatura;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\CrudPanel;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Traits\BuscaCodigoItens;
use function config;

/**
 * Class ApropriacaoInstrumentoCobrancaCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ApropriacaoInstrumentoCobrancaCrudController extends CrudController
{
    use BuscaCodigoItens;

    public function setup()
    {
        if (!backpack_user()->can('apropriar_fatura')) {
            abort('403', config('app.erro_permissao'));
        }

        $contratofaturas_id = \Route::current()->parameter('contratofaturas_id');

        #pega id do contrato
        $contrato_id = Contratofatura::where('id', $contratofaturas_id)->first()->contrato_id ?? null;

        if (!$contrato_id) {
            abort('403', config('app.erro_url_param'));
        }

        #seta no objeto crud
        $this->crud->contrato_id = $contrato_id;

        $this->crud->setModel('App\Models\ApropriacaoContratoFaturas');
        $this->crud->setRoute(
            config('backpack.base.route_prefix') . '/instrumentocobranca/' . $contratofaturas_id . '/apropriacao'
        );
        $this->crud->setEntityNameStrings(
            'Pagamento de Instrumento de Cobrança',
            'Pagamento de Instrumento de Cobrança'
        );

        $this->crud->setRequiredFields(UpdateRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        $this->crud->addClause(
            'select',
            [
                'apropriacoes_faturas_contratofaturas.*',
                'contratos.numero as numero_contrato',
                'contratofaturas.numero as numero_contratofaturas',
                'contratofaturas.situacao as situacao_contratofaturas',
                'sfpadrao.*'
            ]
        );

        #inicio joins
        $this->crud->addClause(
            'join',
            'contratofaturas',
            'contratofaturas.id',
            '=',
            'apropriacoes_faturas_contratofaturas.contratofaturas_id'
        );

        $this->crud->addClause(
            'join',
            'apropriacoes_faturas',
            'apropriacoes_faturas.id',
            '=',
            'apropriacoes_faturas_contratofaturas.apropriacoes_faturas_id'
        );

        $this->crud->addClause(
            'join',
            'contratos',
            'contratos.id',
            '=',
            'contratofaturas.contrato_id'
        );

        $this->crud->addClause(
            'join',
            'fornecedores',
            'fornecedores.id',
            '=',
            'contratos.fornecedor_id'
        );


        $this->crud->addClause(
            'join',
            'sfpadrao',
            'sfpadrao.id',
            '=',
            'apropriacoes_faturas_contratofaturas.sfpadrao_id'
        );
        #fim joins

        #busca as apropriações do instrumento de cobrança da url
        $this->crud->addClause('where', 'contratofaturas_id', '=', $contratofaturas_id);

        #adiciona colunas na grid
        $gridColuns = $this->addColumns();
        $this->crud->addColumns($gridColuns);

        $this->crud->denyAccess('create');
        $this->crud->denyAccess('show');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');

        if (backpack_user()->can('apropriar_fatura')) {
            $this->crud->addButton(
                'line',
                'apropriacao_fatura_andamento',
                'view',
                'crud::buttons.apropriacao_fatura_andamento',
                'beginning'
            );

            $this->crud->addButton(
                'line',
                'remover_apropriacao_fatura',
                'view',
                'crud::buttons.remover_apropriacao_fatura',
                'end'
            );

            $this->crud->addButton(
                'line',
                'busca_ordem_bancaria_fatura',
                'view',
                'crud::buttons.busca_ordem_bancaria_fatura',
                'end'
            );

            $this->crud->addButton(
                'top',
                'apropriacao_fatura',
                'view',
                'crud::buttons.nova_apropriacao_fatura'
            );
        }

        $this->crud->addButton(
            'top',
            'voltarparalistagemfaturasapropriacao',
            'view',
            'crud::buttons.voltarparalistagemfaturasapropriacao'
        );

        #se existir sessao de apropriacao de faturas então remove-las

        if(session()->has('arrPadraoAlteracaoApropriacao')){
            session()->forget('arrPadraoAlteracaoApropriacao');
        }
    }

    public function addColumns()
    {
        return [
            [
                'name' => 'numero_contrato',
                'label' => 'Número do Contrato',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'numero_contratofaturas',
                'label' => 'Número do Instrumento',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'getFornecedor',
                'label' => 'Fornecedor',
                'type' => 'model_function',
                'function_name' => 'getFornecedor',
                'orderable' => true,
                'limit' => 50,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere('fornecedores.cpf_cnpj_idgener', 'like', "%" . strtoupper($searchTerm) . "%");
                    $query->orWhere('fornecedores.nome', 'like', "%" . strtoupper($searchTerm) . "%");
                },
            ],
            [
                'name' => 'anodh',
                'label' => 'Ano DH',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'codtipodh',
                'label' => 'Código Tipo DH',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'numdh',
                'label' => 'Número DH',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'dtemis',
                'label' => 'Data de Emissão',
                'type' => 'date',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'msgretorno',
                'label' => 'Mensagem SIAFI',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'getDescricaoSituacaoApropriacao',
                'label' => 'Situação',
                'type' => 'model_function',
                'function_name' => 'getDescricaoSituacaoApropriacao',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
        ];
    }

    public function store(UpdateRequest $request)
    {
        $redirect_location = parent::storeCrud($request);
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        $redirect_location = parent::updateCrud($request);
        return $redirect_location;
    }

    public function show($id)
    {
        $content = parent::show($id);

        return $content;
    }
}
