<?php

namespace App\Http\Controllers\Apropriacao;

use App\Models\ApropriacaoContratoFaturas;
use App\Models\Codigoitem;
use App\Models\ContratoFaturaEmpenho;
use App\Models\ContratoFaturaEmpenhosApropriacao;
use App\Models\Empenho;
use App\Models\SfPadrao;
use App\Models\SfPco;
use App\Models\Unidade;
use App\XML\Execsiafi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\BuscaCodigoItens;
use App\Models\BackpackUser;
use App\Models\TermoAceite;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ApropriacaoFaturaSiafiController extends Controller
{
    use BuscaCodigoItens;

    protected $xml_siafi;

    public function __construct()
    {
        $this->xml_siafi = new Execsiafi();
    }

    public function enviarApropriacaoFaturaSiafi(Request $request, BackpackUser $user = null)
    {
        $sfpadrao_id = $request->sfpadrao_id;
        $status = null;

        $sfpadrao = SfPadrao::find($sfpadrao_id);

        if (!isset($sfpadrao->id)) {
            return 'Erro';
        }

        $requiredFields = [
            'codcentrocusto' => 'Codigo do Centro de Custo deve ser preenchido e salvo.',
            'mesreferencia' => 'Mês deve ser preenchido e salvo.',
            'anoreferencia' => 'Ano deve ser preenchido e salvo.',
            'codugbenef' => 'Código da UG Beneficiada deve ser preenchido e salvo',
            'codsiorg' => 'Código SIORG deve ser peenchido e salvo',
        ];
//        foreach ($requiredFields as $field => $errorMessage) {
//            $emptyItems = $sfpadrao->centroCusto->filter(function ($item) use ($field) {
//                return trim($item->$field) === '' || trim($item->$field) === null;
//            });
//
//            if ($emptyItems->isNotEmpty()) {
//                abort(422, $errorMessage);
//            }
//        }

        foreach ($sfpadrao->centroCusto as $item) {
            $todosVazios = collect($requiredFields)->keys()->every(function ($field) use ($item) {
                return trim($item->$field) === '' || trim($item->$field) === null;
            });

            if ($todosVazios) {
                continue;
            }

            foreach ($requiredFields as $field => $errorMessage) {
                if (trim($item->$field) === '' || trim($item->$field) === null) {
                    abort(422, $errorMessage);
                }
            }
        }

        $retorno = $this->xml_siafi->apropriaNovoDh(
            $user ?? backpack_user(),
            str_pad($sfpadrao->codugemit, 6, "0", STR_PAD_LEFT),
            config('app.ambiente_siafi'),
            config('app.ano_minuta_empenho'),
            $sfpadrao
        );

        if ($retorno->resultado[0] == 'SUCESSO') {
            $sfpadrao->numdh = $retorno->resultado[1];
            $sfpadrao->msgretorno = $retorno->resultado[2];
            $sfpadrao->situacao = "S";
            $sfpadrao->save();

            $status = "S";
        }

        if ($retorno->resultado[0] == 'FALHA') {
            $sfpadrao->msgretorno = isset($retorno->resultado[2]) ? $retorno->resultado[1] . ' ' . $retorno->resultado[2] : $retorno->resultado[1];
            //$sfpadrao->msgretorno = $retorno->resultado[1];
            $sfpadrao->situacao = "F";
            $sfpadrao->save();

            $status = "F";

//            if (isset($retorno->resultado[2]) && stripos($retorno->resultado[2], 'Unmarshalling Error cvc-minLength-valid ') !== false) {
//                // A mensagem algo menor que zero.
//                $sfpadrao->msgretorno = $retorno->resultado[2];
//                $sfpadrao->save();
//            }
//
//            if (isset($retorno->resultado[2]) && stripos($retorno->resultado[2], 'Unmarshalling Error cvc-minExclusive-valid') !== false) {
//                // Fora horario utilização
//                $sfpadrao->msgretorno = $retorno->resultado[2];
//                $sfpadrao->save();
//            }
//
//            if (isset($retorno->resultado[2]) && stripos($retorno->resultado[2], '(0086) NAO ESTA AUTORIZADA A UTILIZACAO') !== false) {
//                // Fora horario utilização
//                $sfpadrao->msgretorno = $retorno->resultado[2];
//                $sfpadrao->save();
//            }
//
//            if (isset($retorno->resultado[2]) && stripos($retorno->resultado[2], 'USUARIO INEXISTENTE') !== false) {
//                // A mensagem contém "USUARIO INEXISTENTE"
//                $sfpadrao->msgretorno = $retorno->resultado[2];
//                $sfpadrao->save();
//            }
//
//            if (isset($retorno->resultado[2]) && stripos($retorno->resultado[2], 'USUARIO BLOQUEADO') !== false) {
//                // A mensagem contém "USUARIO BLOQUEADO"
//                $sfpadrao->msgretorno = $retorno->resultado[2];
//                $sfpadrao->save();
//            }


        }

        $this->atualizaStatusApropriacaoFaturas($sfpadrao, $status);

        return 'success';
    }

    public function cancelarApropriacaoFaturaSiafi(Request $request, $sfpadraoURLParam = null, BackpackUser $user = null)
    {
        $sfpadrao_id = $request->sfpadrao_id;
        $status = null;

        $sfpadrao = SfPadrao::find($sfpadrao_id);

        if (!isset($sfpadrao->id)) {
            return 'Erro';
        }

        $retorno = $this->xml_siafi->cancelaDh(
            $user ?? backpack_user(),
            $sfpadrao->codugemit,
            config('app.ambiente_siafi'),
            config('app.ano_minuta_empenho'),
            $sfpadrao
        );

        if ($retorno->resultado[0] == 'SUCESSO') {
            $sfpadrao->numdh = $retorno->resultado[1];
            $sfpadrao->msgretorno = $retorno->resultado[2];
            $sfpadrao->situacao = "S";
            $sfpadrao->save();

            $status = "C";

            if ($request->existsSits === 'true') {
                $tipo = Codigoitem::where('descres', 'APROPRIACAO')->first();
                $tipoStatus = Codigoitem::where('descricao', 'Siafi Cancelado')->first();

                TermoAceite::create([
                    'user_id' => backpack_auth()->user()->id,
                    'tipo_id' => $tipo->id,
                    'status_id' => $tipoStatus->id,
                    'info' => $sfpadrao->anodh . $sfpadrao->codtipodh . str_pad($sfpadrao->numdh, 6, "0", STR_PAD_LEFT)
                ]);
            }
        }

        if ($retorno->resultado[0] == 'FALHA') {
            $sfpadrao->msgretorno = $retorno->resultado[1];
            $sfpadrao->situacao = "F";
            $sfpadrao->save();

            $status = "F";

            if (isset($retorno->resultado[2]) && stripos($retorno->resultado[2], 'USUARIO INEXISTENTE') !== false) {
                // A mensagem contém "USUARIO INEXISTENTE"
                $sfpadrao->msgretorno = $retorno->resultado[2];
                $status = 'Z';
                $sfpadrao->save();
            }

            if (isset($retorno->resultado[2]) && stripos($retorno->resultado[2], 'USUARIO BLOQUEADO') !== false) {
                // A mensagem contém "USUARIO BLOQUEADO"
                $sfpadrao->msgretorno = $retorno->resultado[2];
                $status = 'Z';
                $sfpadrao->save();
            }

            if (stripos($retorno->resultado[1], 'Documento Hábil já cancelado') !== false) {
                // A mensagem contém "Documento Hábil já cancelado"
                $status = "C";
            }
        }

        $this->atualizaStatusApropriacaoFaturas($sfpadrao, $status);

        return 'success';
    }

    public function atualizaStatusApropriacaoFaturas(SfPadrao $sfpadrao, $status = null)
    {
        $codigoDescres = "Status Apropriação Instrumento de Cobrança SIAFI";

        if($status == "S"){
            $codigoItemDescres = "Siafi Apropriado";
            $situacao = $this->getCodigoItemByDescres($codigoDescres, $codigoItemDescres);
            $this->atualizaInstrumentoCobranca($sfpadrao->id);
        }

        if($status == "F"){
            $codigoItemDescres = "Siafi Erro";
            $situacao = $this->getCodigoItemByDescres($codigoDescres, $codigoItemDescres);
        }

        if($status == "C"){
            $codigoItemDescres = "Siafi Cancelado";
            $situacao = $this->getCodigoItemByDescres($codigoDescres, $codigoItemDescres);
        }

        if ($status != "Z") {
            $apropriacaofaturascontratofaturas = $sfpadrao->apropriacoes()->get();
            foreach ($apropriacaofaturascontratofaturas as $apropriacaofaturascontratofatura) {
                $apropriacaofaturascontratofatura->apropriacao->fase_id = $situacao->id;
                $apropriacaofaturascontratofatura->apropriacao->save();
                $apropriacaofaturascontratofatura->fatura->situacao = $this->atualizarSituacaoInstrumentoCobranca(
                    $status, $situacao, $codigoDescres
                );
                $apropriacaofaturascontratofatura->fatura->save();
            }
        }

        return true;

    }

    /**
     * Em caso de Cancelamento SIAFI deixa a situação do(s) instrumento(s) como Pendente
     * @param $status
     * @param $situacao
     * @param $codigoDescres
     * @return mixed
     */
    public function atualizarSituacaoInstrumentoCobranca($status, $situacao, $codigoDescres)
    {
        if($status !== 'C')
        {
            return $situacao->descres;
        }

        $situacaoPendente = $this->retornaDescresCodigoItem($codigoDescres, 'Pendente');

        return $situacaoPendente;
    }

    //Deleta os empenhos do instrumento do cobrança e atualiza com os empenhos do pco na tabela contrato_fatura_empenhos_apropriacao
    public function atualizaInstrumentoCobranca($sfPadrao_id)
    {
        DB::beginTransaction();
        try {
            // Encontrar os registros existentes
            $apropContratoFatura = ApropriacaoContratoFaturas::where('sfpadrao_id', $sfPadrao_id)->pluck('contratofaturas_id')->toArray();

            // Verificar se o número de itens é o mesmo
            $sfpcos = SfPco::where('sfpadrao_id', $sfPadrao_id)->with('pcoItens')->get();
            $numItems = count($sfpcos->pluck('pcoItens')->flatten());
            $numContratoFaturaEmpenhos = ContratoFaturaEmpenhosApropriacao::whereIn('contratofatura_id', $apropContratoFatura)->count();

            ContratoFaturaEmpenho::whereIn('contratofatura_id', $apropContratoFatura)->delete();

            if ($numItems > $numContratoFaturaEmpenhos) {
                // Continuar para salvar os itens na tabela ContratoFaturaEmpenho
                $this->salvarItensContratoFaturaEmpenhoApropriacao($sfpcos, $apropContratoFatura);
            }

            $contratoFaturaEmpenhoAprops = ContratoFaturaEmpenhosApropriacao::whereIn('contratofatura_id', $apropContratoFatura)->get();
            foreach ($contratoFaturaEmpenhoAprops as $contratoFaturaEmpenhoAprop) {

                #Localiza contrato_fatura_id
                $contFatEmpAprop = new ContratoFaturaEmpenho();
                $contFatEmpAprop->empenho_id = $contratoFaturaEmpenhoAprop->empenho_id;
                $contFatEmpAprop->subelemento_id = $contratoFaturaEmpenhoAprop->subelemento_id;
                $contFatEmpAprop->contratofatura_id = $contratoFaturaEmpenhoAprop->contratofatura_id;
                $contFatEmpAprop->valorref = $contratoFaturaEmpenhoAprop->valorref;
                $contFatEmpAprop->save();

                // Atualizar para true o registro caso o retorno do siafi tenha sido bem-sucedido
                $contratoFaturaEmpenhoAprop->update(['apropriado_siafi' => true]);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());
        }
    }

    private function salvarItensContratoFaturaEmpenhoApropriacao($sfpcos, $apropContratoFatura)
    {
        foreach ($sfpcos as $sfpco) {
            foreach ($sfpco['pcoItens'] as $sfItem) {
                $unidade = Unidade::where('codigo', $sfpco->codugempe)->select('id')->first();
                $empenho = Empenho::where('numero', $sfItem->numempe)->where('unidade_id', $unidade->id)->select('id')->first();
                // Criar novo registro para ContratoFaturaEmpenho
                $contratoFaturaEmpenho = new ContratoFaturaEmpenhosApropriacao();
                $contratoFaturaEmpenho->contratofatura_id = $sfItem->contratofatura_id ?? $apropContratoFatura[0];
                $contratoFaturaEmpenho->empenho_id = $empenho->id;
                $contratoFaturaEmpenho->subelemento_id = $sfItem->subelemento_id;
                $contratoFaturaEmpenho->valorref = $sfItem->vlr;
                $contratoFaturaEmpenho->sfpcoitem_id = $sfItem->id;
                $contratoFaturaEmpenho->save();
            }
        }
    }
}
