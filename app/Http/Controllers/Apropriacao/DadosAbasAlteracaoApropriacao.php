<?php

namespace App\Http\Controllers\Apropriacao;

use App\Http\Traits\BuscaCodigoItens;
use App\Models\ApropriacaoContratoFaturas;
use App\Models\ApropriacaoFaturas;
use App\Models\Contrato;
use App\Models\ContratoAntecipagov;
use App\Models\ContratoFaturaEmpenhosApropriacao;
use App\Models\ContratoFaturaMesAno;
use App\Models\Empenho;
use App\Models\Execsfsituacao;
use App\Models\ExecsfsituacaoCamposVariaveis;
use App\Models\Fornecedor;
use App\Models\Municipio;
use App\Models\SfAcrescimo;
use App\Models\SfCentroCusto;
use App\Models\SfCronBaixaPatrimonial;
use App\Models\SfDadosBasicos;
use App\Models\SfDeducao;
use App\Models\SfDocOrigem;
use App\Models\SfItemRecolhimento;
use App\Models\SfPadrao;
use App\Models\SfParcela;
use App\Models\SfPco;
use App\Models\SfPcoItem;
use App\Models\SfPredoc;
use App\Models\SfRelItemDeducao;
use App\Models\Sfrelitemvlrcc;
use App\Models\Unidade;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Route;

//traits
use App\Http\Traits\Formatador;

/**
 * Classe que auxilia na criação dos dados apresentados nas abas de Alteracao de Apropriacao
 *
 * Abas - Dados Basicos, Principal Com Orçamento, Dedução, Dados de pagamento, Centro de Custo
 *
 * Class DadosAbasAlteracaoApropriacao
 * @package App\Http\Controllers\Apropriacao
 */
class DadosAbasAlteracaoApropriacao
{
    use Formatador, BuscaCodigoItens;

    const RESTRICAO_DESPESA_ANTECIPADA = 'Despesa Antecipada';
    const RESTRICAO_CONTRATO = 'Contrato';
    const RESTRICAO_UG_PAIS = 'UG do País';
    const RESTRICAO_REALIZACAO_GRU = 'Realização GRU';
    const RESTRICAO_SEM_RESTRICAO = 'Sem Restrição';

    public function criarDadosAbaDadosBasicos()
    {

        $params = new \stdClass;

        $params->sf_padrao_id = Route::current()->parameter('sf_padrao_id');
        $params->modelSfDadosBasicos = SfDadosBasicos::where('sfpadrao_id', $params->sf_padrao_id)->get()->first();
        $params->txtprocesso = $params->modelSfDadosBasicos->txtprocesso;
        $params->txtobser = $params->modelSfDadosBasicos->txtobser;
        $params->vlr = $params->modelSfDadosBasicos->vlr;
        $params->dtemis = date('d-m-Y');
        $params->dtvenc = $this->retornaDataAPartirDeCampo($params->modelSfDadosBasicos->dtvenc);
        $params->nomeugpgto = Unidade::where('codigo', str_pad($params->modelSfDadosBasicos->codugpgto, 6, "0", STR_PAD_LEFT))->get()->first()->nome;
        $params->codugpgto = $params->modelSfDadosBasicos->codugpgto;
        $params->dtpgtoreceb = $this->retornaDataAPartirDeCampo($params->modelSfDadosBasicos->dtpgtoreceb);
        $params->vlrtaxacambio = $params->modelSfDadosBasicos->vlrtaxacambio;

        $fornecedor = new Fornecedor();

        $dados_fornecedor = $fornecedor->buscaFornecedorPorNumero(
            $params->modelSfDadosBasicos->codcredordevedor
        ) ?? null;

        if (!is_null($dados_fornecedor)) {
            $params->nomeFornecedor = array_get($dados_fornecedor->toArray(), 'nome') ?? null;
            $params->tipo_fornecedor = array_get($dados_fornecedor->toArray(), 'tipo_fornecedor') ?? null;
        } else {
            $params->nomeFornecedor = ' - ';
            $params->tipo_fornecedor = ' - ';
        }

        $params->codcredordevedor = $this->retornaMascaraCnpjCpf($params->modelSfDadosBasicos->codcredordevedor);
        switch ($this->retornaTipoFornecedor($params->modelSfDadosBasicos->codcredordevedor)) {
            case 'FISICA':
            case 'JURIDICA':
                $params->codcredordevedor = $this->retornaMascaraCnpjCpf($params->modelSfDadosBasicos->codcredordevedor);
                break;
            default:
                $params->codcredordevedor = $params->modelSfDadosBasicos->codcredordevedor;
                break;
        }
        $params->codcredordevedorsemmascara = $this->limpa_cpf_cnpj(
            $params->modelSfDadosBasicos->codcredordevedor
        );

        $params->docOrigem = SfDocOrigem::where('sfdadosbasicos_id', $params->modelSfDadosBasicos->id)->get();

        #coloca mascara no campo de cpf/cnpj
        foreach ($params->docOrigem as $doc) {
            switch ($this->retornaTipoFornecedor($doc->codidentemit)) {
                case 'FISICA':
                case 'JURIDICA':
                    $doc->codidentemit = $this->retornaMascaraCnpjCpf($doc->codidentemit);
                    break;
                default:
                    break;
            }
        }

        return $params;
    }

    public function setarValorIndrTemContrato($temContrato, $contratoTipoEmpenho)
    {

        if($temContrato === null && $contratoTipoEmpenho){
            return '0'; //NÃO
        }

        if($temContrato === null && $contratoTipoEmpenho === false){
            return '1'; //SIM
        }

        return $temContrato;

    }

    public function criarDadosAbaPco(
        $sfPco,
        &$totalVlrAba,
        $sf_padrao_id,
        $codtipodh,
        $apropriacao_id
    ) {
        $arr_campos_aba_pco = [];

        foreach ($sfPco as $key_sfpco => $sfpco_current) {
            $this->reorderNumSeqItem('App\Models\SfPco', $sfpco_current, $key_sfpco + 1);

            $sfpco_current->sfpcoitem = SfPcoItem::where('sfpco_id', $sfpco_current->id)
            ->orderBy('id','asc')
            ->get();

            $sfpco_current->nomeSituacaoPco = Execsfsituacao::where('codigo', $sfpco_current->codsit)
                    ->get()->first()->descricao ?? ' - ';

            $sfpco_current->nomeugempe = Unidade::where('codigo', $sfpco_current->codugempe)
                    ->get()->first()->nome ??' - ';

            $totalVlrAba += $sfpco_current->sfpcoitem->sum('vlr');

            $arrayOptions = [];

            $arrCamposVariaveis = $this->retornarCamposVariaveis($sfpco_current);


            $apropriacaoContratoFaturas = ApropriacaoContratoFaturas::where('apropriacoes_faturas_id',$apropriacao_id)->first();

            #contrato
            $contrato = $apropriacaoContratoFaturas->fatura->contrato;
            $idTipoEmpenho = $this->retornaIdCodigoItem('Tipo de Contrato', 'Empenho');

            $contratoTipoEmpenho = $contrato->tipo_id === $idTipoEmpenho;

            #seta indrtemcontrato verificando se o contrato é do tipo empenho
           // $sfpco_current->indrtemcontrato = $this->setarValorIndrTemContrato($sfpco_current, $contratoTipoEmpenho);

            if (!is_null($codtipodh) && !empty($codtipodh) && $codtipodh !== "  ") {
                $arrayOptions = $this->recuperarArrayOptionsSelectSituacao($codtipodh);
                $arrayOptionsCodUGEmpenho = $this->buscaArrayUgEmitenteEmpenhoSelect($sf_padrao_id);
            }

            $arr_campos_aba_pco['codsit' . $key_sfpco] = [
                'name' => 'codsit' . $sfpco_current->id,
                'value' => $sfpco_current->codsit ?? '',
                'type' => 'select',
                'choices' => $arrayOptions ?? [],
                'rules' => 'required',
                'label_show' => false,
                'selected' => $sfpco_current->codsit,
                'attr' => [
                    'id' => 'codsit' . $sfpco_current->id,
                    'name' => 'codsit[]',
                    'data-nome-aba' => 'aba-sfpco' . $sfpco_current->id,
                    'data-nome-aba-principal' => 'pco-tab',
                    'onchange' => "bindCampoSituacaoNaAba(this);
                                        apagarCamposVariaveisDeSfPcoItem();
                                            createOrUpdateSfPcoSituacaoViaAjax(this, $sfpco_current->id);"
                ]
            ];
            $arr_campos_aba_pco['codugempe' . $key_sfpco] = [
                'name' => 'codugempe' . $sfpco_current->id,
                'value' => $sfpco_current->codugempe ?? '',
                'type' => 'text',
                'choices' => $arrayOptionsCodUGEmpenho ?? [],
                'rules' => 'required',
                'label_show' => false,
                'selected' => $sfpco_current->codugempe,
                'attr' => [
                    'id' => 'codugempe' . $sfpco_current->id,
                    'name' => 'codugempe[]',
                    'data-nome-aba' => 'aba-sfpco' . $sfpco_current->id,
                    'data-nome-aba-principal' => 'pco-tab',
                    'onchange' => "alteraUnidadeEmitenteEmpenho(this, $sfpco_current->id);"
                ]
            ];
            $arr_campos_aba_pco['repetircontaempenho' . $key_sfpco] = [
                'name' => 'repetircontaempenho' . $sfpco_current->id,
                'value' => $sfpco_current->repetir_conta_empenho,
                'type' => 'select',
                'choices' => [0 => 'NÃO', 1 => 'SIM'],
                'rules' => 'required',
                'label_show' => false,
                'selected' => $sfpco_current->repetir_conta_empenho,
                'attr' => [
                    'id' => 'repetircontaempenho' . $sfpco_current->id,
                    'name' => 'repetircontaempenho[]',
                    'data-nome-aba' => 'aba-sfpco' . $sfpco_current->id,
                    'data-nome-aba-principal' => 'pco-tab'
                ]
            ];

            $arr_campos_aba_pco['indrtemcontrato' . $key_sfpco] = [
                'name' => 'indrtemcontrato' . $sfpco_current->id,
                'value' => $sfpco_current->indrtemcontrato,
                'type' => 'select',
                'choices' => (!empty($arrCamposVariaveis) && $arrCamposVariaveis[0]['permite_contrato']) ? [1 => 'SIM', 0 => 'NÃO'] : [0 => 'NÃO'],
                'rules' => 'required',
                'label_show' => false,
                'selected' => $sfpco_current->indrtemcontrato,
                'attr' => [
                    'id' => 'indrtemcontrato' . $sfpco_current->id,
                    'name' => 'indrtemcontrato[]',
                    'data-nome-aba' => 'aba-sfpco' . $sfpco_current->id,
                    'data-nome-aba-principal' => 'pco-tab',
                    'onchange' => "updateSfPcoIndrtemcontrato(this, $sfpco_current->id, $sfpco_current->numseqitem);
                        exibirOuEsconderCampoVariavel($sfpco_current->id, this, '" . self::RESTRICAO_CONTRATO . "');",

                ]
            ];

            $arr_campos_aba_pco['despesaantecipada'.$key_sfpco] = [
                'name' => 'despesaantecipada'.$sfpco_current->id,
                'value' => !!$sfpco_current->despesaantecipada,
                'type' => 'select',
                'choices' => [1 => 'SIM', 0 => 'NÃO'],
                'rules' => 'required',
                'label_show' => false,
                'selected' => $sfpco_current->despesaantecipada ? '1' : '0',
                'attr' => [
                    'id' => 'despesaantecipada'.$sfpco_current->id,
                    'name' => 'despesaantecipada[]',
                    'onchange' => "exibirOuEsconderCampoVariavel($sfpco_current->id, this, '" . self::RESTRICAO_DESPESA_ANTECIPADA . "');" .
                        "exibirOuEsconderCampoCronogramaBaixaPatrimonial(this, {$sfpco_current->id})"
                ]
            ];

            //campo hidden para guardar array com ids de sfpc
            $arr_campos_aba_pco['sfpco' . $key_sfpco] = [
                'name' => 'sfpco' . $sfpco_current->id,
                'value' => $sfpco_current->id,
                'type' => 'hidden',
                'label_show' => false,
                'attr' => [
                    'id' => 'sfpco' . $sfpco_current->id,
                    'name' => 'sfpco[]'
                ]
            ];

            $arr_campos_aba_pco['sfpcoconfirma_dados' . $key_sfpco] = [
                'name' => 'sfpcoconfirma_dados' . $sfpco_current->id,
                'value' => $sfpco_current->confirma_dados ? 'true' : 'false',
                'type' => 'hidden',
                'label_show' => false,
                'attr' => [
                    'id' => 'sfpcoconfirma_dados' . $sfpco_current->id,
                    'name' => 'sfpcoconfirma_dados[]',
                    'class' => 'sfpcoApropriacaoConfirmaDados'
                ]
            ];

            #######################################################
            ####criar campos variaveis de sfpco ###################
            #######################################################

            foreach ($arrCamposVariaveis as $camposVariaveis) {
                $morphSfPcoOuSfPcoItem = $this->verificarTabelaCampoVariavel(
                    $camposVariaveis['campo'],
                    $sfpco_current,
                    null
                );

                $booExibirCampoVariavel = $this->exibirOuEsconderCampoVariavel(
                    $sfpco_current,
                    $camposVariaveis['campo'],
                    $sfpco_current->despesaantecipada ?? false,
                    $sfpco_current->indrtemcontrato
                );

                $display = !$booExibirCampoVariavel ? 'display:none;' : '';

                $mascara = $this->inserirPontosNaMascara($camposVariaveis['mascara']);

                if ($morphSfPcoOuSfPcoItem && $morphSfPcoOuSfPcoItem->getTable() === 'sfpco') {
                    $arr_campos_aba_pco[$camposVariaveis['campo'] . $key_sfpco] = [
                        'name' => $camposVariaveis['campo'] . $sfpco_current->id,
                        'value' => $this->getValueCampoVariavelSfPco(
                            $sfpco_current,
                            $camposVariaveis['campo'],
                            $morphSfPcoOuSfPcoItem->{$camposVariaveis['campo']}
                        ),
                        'type' => 'text',
                        'required ' => true,
                        'label' => $camposVariaveis['rotulo'],
                        'label_show' => true,
                        'label_attr' => [
                            'class' => 'control-label'. ' '.
                                $camposVariaveis['campo']. $sfpco_current->id,
                            'for' => $camposVariaveis['campo'] . $sfpco_current->id,
                            'style' => "$display"
                        ],
                        'wrapper' => ['class' => 'form-group'. ' '. $camposVariaveis['campo'] ],
                        'attr' => [
                            'id' => $camposVariaveis['campo'] . $sfpco_current->id,
                            'class' => 'form-control camposVariaveisSfPcoItem camposVariaveisSfPcoItem'.$sfpco_current->id.  ' '. $camposVariaveis['campo'],
                            'name' => $camposVariaveis['campo'] . "[$sfpco_current->id]",
                            'data-mascara' => $mascara,
                            'data-restricao' => $this->getRestricaoCampoVariavel(
                                $sfpco_current->codsit,
                                $camposVariaveis['campo']
                            ),
                            'data-rotulo' => $this->getRotuloCampoVariavel(
                                $sfpco_current->codsit,
                                $camposVariaveis['campo']
                            ),
                            'style' => "text-align: center; $display",
                            'onchange' => "updateSfPcoNumClass(this, $sfpco_current->id, $sfpco_current->numseqitem);",
                        ]
                    ];
                }
            }

            foreach ($sfpco_current->sfpcoitem as $key => $sfpcoitem_current) {

                $this->reorderNumSeqItem('App\Models\SfPcoItem', $sfpcoitem_current, $key + 1);

                #recupera os dados do empenho na tabela contratofatura_empenhos
                $numeroEmpenho = $sfpcoitem_current->numempe ?? '';
                $codUgPco = $sfpcoitem_current->pco()->first()->codugempe ?? '';

                $faturaEmpenho = $this->findContratoFaturaEmpenhosByNumEmpenho($numeroEmpenho, $apropriacao_id, $sfpcoitem_current->subelemento_id, $codUgPco, $sfpcoitem_current->id);

                #atribui faturaEmpenhos ao obj sfpcoitem
                $sfpcoitem_current->faturaEmpenho = $faturaEmpenho ? $faturaEmpenho->get() : null;
                $arrayContratoFaturas =  $faturaEmpenho ? $faturaEmpenho->pluck('contratofatura_id')->toArray() : [];

                #atribui apropriacaocontratofaturas ao obj sfpcoitem
                $sfpcoitem_current->apropriacaoFaturas = ApropriacaoContratoFaturas::join(
                    'contratofaturas',
                    'apropriacoes_faturas_contratofaturas.contratofaturas_id',
                    '=',
                    'contratofaturas.id'
                )
                    ->where(
                        'apropriacoes_faturas_id',
                        $apropriacao_id
                    )
                    ->whereNotIn(
                        'apropriacoes_faturas_contratofaturas.contratofaturas_id',
                        $arrayContratoFaturas
                    )
                    ->get();

                $arr_campos_aba_pco['numempe'.$key_sfpco.$key] = [
                    'name' => 'numempe'.$sfpcoitem_current->id.$sfpco_current->id,
                    'value' => $sfpcoitem_current->numempe,
                    'type' => 'text',
                    'rules' => 'required',
                    'label_show' => false,
                    'wrapper' => false,
                    'attr' => [
                        'id' => 'numempe'.$sfpcoitem_current->id.$sfpco_current->id,
                        'name' => 'numempe[]',
                        'onkeyup' => "maiuscula(this);" .
                                     "buscarSubelementoDoEmpenho(" .
                                        "this, " .
                            "codsubitemempe".$sfpcoitem_current->id.$sfpco_current->id."," .
                            "{$sfpcoitem_current->id}, {$sfpco_current->id})",
                        'data-nome-aba' => 'aba-sfpco'.$sfpco_current->id,
                        'data-nome-aba-principal' => 'pco-tab'
                    ]
                ];

                $arr_campos_aba_pco['contratofatura_id'.$key_sfpco.$key] = [
                    'name' => 'contratofatura_id'.$sfpcoitem_current->id.$sfpco_current->id,
                    'value' => $sfpcoitem_current->contratofatura_id,
                    'type' => 'hidden',
                    'rules' => 'required',
                    'label_show' => false,
                    'wrapper' => false,
                    'attr' => [
                        'id' => 'contratofatura_id'.$sfpcoitem_current->id.$sfpco_current->id,
                        'name' => 'contratofatura_id[]',
                        'data-nome-aba' => 'aba-sfpco'.$sfpco_current->id,
                        'data-nome-aba-principal' => 'pco-tab'
                    ]
                ];

                $arrayOptionsSubElementoEmpenho = $this->buscarSubelementoDoEmpenho(
                    $sfpcoitem_current->numempe ?? '',
                    $sfpco_current->codugempe ?? ''
                );

                $codsubitemempe = $sfpcoitem_current->codsubitemempe ? $this->formatarSubelementoZeroEsquerda($sfpcoitem_current->codsubitemempe) : '';

                #aqui atribui o codsubitemempe depois de verificar se precisa colocar 0 a esquerda
                $sfpcoitem_current->codsubitemempe = $codsubitemempe;

                $arr_campos_aba_pco['codsubitemempe'.$key_sfpco.$key] = [
                    'name' => 'codsubitemempe'.$sfpcoitem_current->id.$sfpco_current->id,
                    'value' => $codsubitemempe,
                    'type' => 'select',
                    'choices' => count($arrayOptionsSubElementoEmpenho) > 0 ?
                        $arrayOptionsSubElementoEmpenho :
                        ['' => 'NÂO ENCONTRADO'],
                    'rules' => 'required',
                    'label_show' => false,
                    'selected' => $sfpcoitem_current->subelemento_id,
                    'attr' => [
                        'id' => 'codsubitemempe'.$sfpcoitem_current->id.$sfpco_current->id,
                        'name' => 'codsubitemempe[]',
                    ]
                ];

                $arr_campos_aba_pco['codsubitemempecadastrado'.$key_sfpco.$key] = [
                    'name' => 'codsubitemempecadastrado'.$sfpcoitem_current->id.$sfpco_current->id,
                    'value' => $sfpcoitem_current->subelemento_id,
                    'type' => 'hidden',
                    'label_show' => false,
                    'wrapper' => false,
                    'attr' => [
                        'id' => 'codsubitemempecadastrado'.$sfpcoitem_current->id.$sfpco_current->id,
                        'name' => 'codsubitemempecadastrado[]',
                        'onkeyup' => "buscarSubelementoDoEmpenho(" .
                            "numempe".$sfpcoitem_current->id.$sfpco_current->id."," .
                            "codsubitemempe".$sfpcoitem_current->id.$sfpco_current->id."," .
                            "{$sfpcoitem_current->id}, {$sfpco_current->id}, true)",
                        'data-nome-aba' => 'aba-sfpco'.$sfpco_current->id,
                        'data-nome-aba-principal' => 'pco-tab'
                    ]
                ];

                $arr_campos_aba_pco['vlr'.$key_sfpco.$key] = [
                    'name' => 'vlr'.$sfpcoitem_current->id.$sfpco_current->id,
                    'value' => number_format($sfpcoitem_current->vlr, 2, ',', '.'),
                    'type' => 'text',
                    'required ' => true,
                    'label_show' => false,
                    'attr' => [
                        'id' => 'vlr'.$sfpcoitem_current->id.$sfpco_current->id,
                        'name' => 'vlr[]',
                        'readonly' => 'readonly',
                        "data-vlr-situacao" => $sfpco_current->id,
                        number_format($sfpcoitem_current->vlr, 2, ',', '.'),
                        'onkeyup' => "atualizarMaskMoney(this);atualizarValoresParcela($sfpco_current->id)",
                    ]
                ];

                #######################################################
                ####criar campos variaveis de sfpcoitem ###############
                #######################################################

                foreach ($arrCamposVariaveis as $camposVariaveis) {
                    #verifica se o campo variavel pertence a sfpco ou sfpcoitem
                    $morphSfPcoOuSfPcoItem = $this->verificarTabelaCampoVariavel(
                        $camposVariaveis['campo'],
                        $sfpco_current,
                        $sfpcoitem_current
                    );

                    $booExibirCampoVariavel = $this->exibirOuEsconderCampoVariavel(
                        $sfpco_current,
                        $camposVariaveis['campo'],
                        $sfpco_current->despesaantecipada ?? false,
                        $sfpco_current->indrtemcontrato
                    );

                    $display = !$booExibirCampoVariavel ? 'display:none;' : '';

                    $mascara = $this->inserirPontosNaMascara($camposVariaveis['mascara']);

                    if ($morphSfPcoOuSfPcoItem->getTable() === 'sfpcoitem') {
                        if ($display != 'display:none;') {
                            $arr_campos_aba_pco[$camposVariaveis['campo'] . $key_sfpco . $key] = [
                                'name' => $camposVariaveis['campo'] . $sfpcoitem_current->id . $sfpco_current->id,
                                'value' => $morphSfPcoOuSfPcoItem->{$camposVariaveis['campo']} === 0 ? '' :
                                                                    $morphSfPcoOuSfPcoItem->{$camposVariaveis['campo']},
                                'type' => 'text',
                                'required ' => true,
                                'label' => $camposVariaveis['rotulo'],
                                'label_attr' => [
                                    'class' => 'control-label'. ' '.
                                                $camposVariaveis['campo']. $sfpcoitem_current->id . $sfpco_current->id,
                                    'for' => $camposVariaveis['campo'] . $sfpcoitem_current->id . $sfpco_current->id,
                                    'style' => "$display"
                                ],
                                'attr' => [
                                    'id' => $camposVariaveis['campo'] . $sfpcoitem_current->id . $sfpco_current->id,
                                    'class' => 'form-control camposVariaveisSfPcoItem camposVariaveisSfPcoItem' . $sfpco_current->id,
                                    'name' => $camposVariaveis['campo'] . "[$sfpcoitem_current->id]",
                                    'onchange' => $key === 0 && ($camposVariaveis['campo'] == 'numclassa' || $camposVariaveis['campo'] == 'numclassb') ? "repetirContasEmpenho($sfpco_current->id, '{$camposVariaveis['campo']}')" : '',
                                    'data-mascara' => $mascara,
                                    'data-restricao' => $this->getRestricaoCampoVariavel(
                                        $sfpco_current->codsit,
                                        $camposVariaveis['campo']
                                    ),
                                    'data-rotulo' => $this->getRotuloCampoVariavel(
                                        $sfpco_current->codsit,
                                        $camposVariaveis['campo']
                                    ),
                                    'style' => "text-align: center; $display"
                                ],
                            ];
                        }
                    }
                }

                //campo hidden para guardar array com ids de sfpcoitem
                $arr_campos_aba_pco['sfpcoitem'.$key_sfpco.$key] = [
                    'name' => 'sfpcoitem'.$sfpcoitem_current->id.$sfpco_current->id,
                    'value' => $sfpcoitem_current->id,
                    'type' => 'hidden',
                    'label_show' => false,
                    'attr' => [
                        'id' => 'sfpcoitem'.$sfpcoitem_current->id.$sfpco_current->id,
                        'name' => 'sfpcoitem[]'
                    ]
                ];
            }

            /***************************************************
             *Criação dos campos de Cronograma Baixa Patrimonial
             ***************************************************/
            $sfCronBaixaPatrimonial = SfCronBaixaPatrimonial::firstOrCreate(['sfpco_id' => $sfpco_current->id]);

            $sfParcela = SfParcela::where('sfcronbaixapatrimonial_id', $sfCronBaixaPatrimonial->id)->get();

            foreach ($sfParcela as $key_parcela => $parcela) {
                $arr_campos_aba_pco['sfparcelavlr' . $key_sfpco. $key_parcela] = [
                    'name' => 'sfparcelavlr' . $sfpco_current->id. $parcela->id,
                    'value' => number_format($parcela->vlr, 2, ',', '.'),
                    'type' => 'text',
                    'label' => 'Valor',
                    'attr' => [
                        'id' => 'sfparcelavlr' . $sfpco_current->id. $parcela->id,
                        'name' => "sfparcelavlr[]",
                        "data-vlr-parcela-situacao" => $sfpco_current->id,
                        'onkeyup' => "atualizarMaskMoney(this);atualizarValoresParcela($sfpco_current->id)",
                    ]
                ];

                $arr_campos_aba_pco['sfparceladtprevista' . $key_sfpco. $key_parcela] = [
                    'name' => 'sfparceladtprevista' . $sfpco_current->id. $parcela->id,
                    'value' => $parcela->dtprevista,
                    'type' => 'date',
                    'label' => 'Data do Agendamento',
                    'attr' => [
                        'id' => 'sfparceladtprevista' . $sfpco_current->id. $parcela->id,
                        'name' => "sfparceladtprevista[]",
                    ]
                ];

                $arr_campos_aba_pco['sfparcelanumparcela' . $key_sfpco. $key_parcela] = [
                    'name' => 'sfparcelanumparcela' . $sfpco_current->id. $parcela->id,
                    'value' => $parcela->numparcela,
                    'type' => 'number',
                    'label' => 'Parcela',
                    'attr' => [
                        'id' => 'sfparcelanumparcela' . $sfpco_current->id. $parcela->id,
                        'name' => "sfparcelanumparcela[]",
                        'onkeyup' => "atualizarMaskMoney(this)",
                        'readonly' => 'readonly',
                    ]
                ];

                //campo hidden para guardar array com ids de sfpcoitem
                $arr_campos_aba_pco['sfparcela'.$key_sfpco.$key_parcela] = [
                    'name' => 'sfparcela'.$parcela->id.$sfpco_current->id,
                    'value' => $parcela->id,
                    'type' => 'hidden',
                    'label_show' => false,
                    'attr' => [
                        'id' => 'sfpcoitem'.$parcela->id.$sfpco_current->id,
                        'name' => 'sfparcela[]'
                    ]
                ];
            }
        }

        //cria form para aba Pco
        $sfpcoForm = \FormBuilder::createByArray(
            $arr_campos_aba_pco,
            [
                'url' => route('fatura.form.alterar-aba-pco', [$sf_padrao_id, $apropriacao_id]),
                'method' => 'POST',
                'novalidate' => 'novalidate',
                'attr' => [
                    'id' => 'sfDadosPcoForm'
                ]
            ]
        );
//        $sfpcoForm->add(
//            'btnSubmitFormSfpco',
//            'submit',
//            [
//                'label' => '<i class="fa fa-save"></i> Confirmar Principal com Orçamento',
//                'attr' => [
//                    'class' => 'btn btn-success esconderQuandoApropriacaoEmitidaSiafi',
//                    'id' => 'btnSubmitAbaPco',
//                    'style' => isset($sfPco[0]->sfpcoitem) && count($sfPco[0]->sfpcoitem) > 0 ? '' : 'display: none',
//                ]
//            ]
//        );

        return $sfpcoForm;
    }

    public function criarDadosAbaDeducao(
        $sfDeducao,
        &$totalValorDeducao,
        $optionsUgPagamento = [],
        $sf_padrao_id,
        $apropriacao_id
    )
    {
        $arr_field_deducao_id = [];

        $sfCodTipoDh = SfPadrao::find($sf_padrao_id);

        if (!is_null($sfCodTipoDh->codtipodh) && !empty($sfCodTipoDh->codtipodh) && $sfCodTipoDh->codtipodh !== "  ") {
            $arrayOptionsSituacao = $this->recuperarArrayOptionsSelectSituacaoDeducao($sfCodTipoDh->codtipodh);
        }

        foreach ($sfDeducao as $key_sfdeducao => $sfdeducao_current) {
            $this->reorderNumSeqItem('App\Models\SfDeducao', $sfdeducao_current, $key_sfdeducao + 1);

            $arrCamposVariaveis = $this->retornarCamposVariaveis($sfdeducao_current);
            //acrescentando nomes das situações
            $nomeSituacao = Execsfsituacao::where('codigo', $sfdeducao_current->codsit)->get()->first();
            $sfdeducao_current->nomeSituacao = $nomeSituacao === null ? ' - ' : $nomeSituacao->descricao;

            $nomeUgPagora = Unidade::where('codigo', $sfdeducao_current->codugpgto)->get()->first();
            $sfdeducao_current->nomeUgPagadora = $nomeUgPagora === null ? ' - ' : $nomeUgPagora->nome;

            $sfpredoc = SfPredoc::where('sfded_id', $sfdeducao_current->id)->get()->first();
            $sfdeducao_current->idPreDoc = $sfpredoc->id ?? null;
            if ($sfpredoc) {

                $arr_field_deducao_id['sfpredoccodrecurso' . $sfdeducao_current->id] = [
                    'name' => 'sfpredoccodrecurso' . $sfdeducao_current->id,
                    'selected' => $sfpredoc->codrecurso ?? 0,
                    'type' => 'select',
                    'choices' => [
                        0 => '0 - Limite de Saque sem Controle de Empenho',
                        1 => '1 - Com cota do Orçamento do Exercício',
                        2 => '2 - Com Limite de Restos a Pagar',
                        3 => '3 - Com Vinculação de Pagamento',
                        8 => '8 - Limite de Pagamento com Títulos Públicos',
                    ],
                    'label_show' => false,
                    'attr' => [
                        'id' => 'sfpredoccodrecurso' . $sfdeducao_current->id,
                        'name' => "sfpredoccodrecurso[$sfdeducao_current->id]"
                    ]
                ];

                $arr_field_deducao_id['sfpredoctxtprocesso' . $sfdeducao_current->id] = [
                    'name' => 'sfpredoctxtprocesso' . $sfdeducao_current->id,
                    'value' => $sfpredoc->txtprocesso ?? '',
                    'type' => 'text',
                    'label_show' => false,
                    'attr' => [
                        'id' => 'sfpredoctxtprocesso' . $sfdeducao_current->id,
                        'name' => "sfpredoctxtprocesso[$sfdeducao_current->id]",
                        'maxlength' => 40
                    ]
                ];

                $arr_field_deducao_id['sfpredocdtprdoapuracao' . $sfdeducao_current->id] = [
                    'name' => 'sfpredocdtprdoapuracao' . $sfdeducao_current->id,
                    'value' => $sfpredoc->dtprdoapuracao ?? '',
                    'type' => 'date',
                    'label_show' => false,
                    'attr' => [
                        'id' => 'sfpredocdtprdoapuracao' . $sfdeducao_current->id,
                        'name' => "sfpredocdtprdoapuracao[$sfdeducao_current->id]",
                        'maxlength' => 40
                    ]
                ];

                $arr_field_deducao_id['sfpredocnumref' . $sfdeducao_current->id] = [
                    'name' => 'sfpredocnumref' . $sfdeducao_current->id,
                    'value' => $sfpredoc->numref,
                    'type' => 'text',
                    'label_show' => false,
                    'attr' => [
                        'id' => 'sfpredocnumref' . $sfdeducao_current->id,
                        'name' => "sfpredocnumref[$sfdeducao_current->id]",
                    ]
                ];

                $arr_field_deducao_id['sfpredocnumcodbarras' . $sfdeducao_current->id] = [
                    'name' => 'sfpredocnumcodbarras' . $sfdeducao_current->id,
                    'value' => $sfpredoc->numcodbarras,
                    'type' => 'text',
                    'label_show' => false,
                    'attr' => [
                        'id' => 'sfpredocnumcodbarras' . $sfdeducao_current->id,
                        'name' => "sfpredocnumcodbarras[$sfdeducao_current->id]",
                    ]
                ];

                $arr_field_deducao_id['sfpredocvlrrctabrutaacum' . $sfdeducao_current->id] = [
                    'name' => 'sfpredocvlrrctabrutaacum' . $sfdeducao_current->id,
                    'value' => $sfpredoc->vlrrctabrutaacum ? number_format($sfpredoc->vlrrctabrutaacum, 2, ',', '.') : '',
                    'type' => 'text',
                    'label_show' => false,
                    'attr' => [
                        'id' => 'sfpredocvlrrctabrutaacum' . $sfdeducao_current->id,
                        'name' => "sfpredocvlrrctabrutaacum[$sfdeducao_current->id]",
                        'onkeyup' => 'atualizarMaskMoney(this);'
                    ]
                ];

                $arr_field_deducao_id['sfpredocvlrpercentual' . $sfdeducao_current->id] = [
                    'name' => 'sfpredocvlrpercentual' . $sfdeducao_current->id,
                    'value' => $sfpredoc->vlrpercentual ? number_format($sfpredoc->vlrpercentual, 2, ',', '.') : '',
                    'type' => 'text',
                    'label_show' => false,
                    'attr' => [
                        'id' => 'sfpredocvlrpercentual' . $sfdeducao_current->id,
                        'name' => "sfpredocvlrpercentual[$sfdeducao_current->id]",
                        'onkeyup' => 'atualizarMaskMoney(this);'
                    ]
                ];

                $arr_field_deducao_id['sfpredoctxtobser' . $sfdeducao_current->id] = [
                    'name' => 'sfpredoctxtobser' . $sfdeducao_current->id,
                    'value' => $sfpredoc->txtobser,
                    'type' => 'textarea',
                    'label_show' => false,
                    'attr' => [
                        'id' => 'sfpredoctxtobser' . $sfdeducao_current->id,
                        'name' => "sfpredoctxtobser[$sfdeducao_current->id]",
                        'cols' => 29,
                        'rows' => 9,
                        'style' => 'margin-bottom: 15px',
                        'maxlength' => 230
                    ]
                ];

                $sfdeducao_current->predoctipo = $sfdeducao_current->codsit && str_starts_with($sfdeducao_current->codsit, 'DDR') ? 'predocdar' : 'predocdarf';

                if ($sfdeducao_current->predoctipo == 'predocdarf') {
                    $opcoes = array();

                    foreach ($arrCamposVariaveis as $keyV => $campoVariavel)
                    {
                        isset($arrCamposVariaveis[$keyV]['darf']) && $arrCamposVariaveis[$keyV]['darf'] == true ? $opcoes['DFS'] = 'DARF' : '';
                        isset($arrCamposVariaveis[$keyV]['darf_numerado']) && $arrCamposVariaveis[$keyV]['darf_numerado'] == true ? $opcoes['DFN'] = 'DARF Numerado' : '';
                        isset($arrCamposVariaveis[$keyV]['darf_numerado_decomposto']) && $arrCamposVariaveis[$keyV]['darf_numerado_decomposto'] == true ? $opcoes['DFD'] = 'DARF Numerado Decomposto' : '';
                    }

                    $arr_field_deducao_id['sfpredoccodtipodarf' . $sfdeducao_current->id] = [
                        'name' => 'sfpredoccodtipodarf' . $sfdeducao_current->id,
                        'selected' => $sfpredoc->codtipoob ?? '',
                        'type' => 'select',
                        'choices' => $opcoes,
                        'label_show' => false,
                        'attr' => [
                            'id' => 'sfpredoccodtipodarf' . $sfdeducao_current->id,
                            'name' => "sfpredoccodtipodarf[$sfdeducao_current->id]",
                            'onchange' => "atualizaSituacaoPreDoc();"
                        ]
                    ];
                }

                $arr_field_deducao_id['sfpredocreferencia' . $sfdeducao_current->id] = [
                    'name' => 'sfpredocreferencia' . $sfdeducao_current->id,
                    'value' => $sfpredoc->mesreferencia . $sfpredoc->anoreferencia,
                    'type' => 'text',
                    'label_show' => false,
                    'attr' => [
                        'id' => 'sfpredocreferencia' . $sfdeducao_current->id,
                        'name' => "sfpredocreferencia[$sfdeducao_current->id]",
                        'data-mascara' => 'XX/XXXX',
                        'class' => 'form-control camposVariaveisSfRecolhimento campoReferenciaPredoc',
                    ]
                ];

                $arr_field_deducao_id['sfpredoccodugtmdrserv' . $sfdeducao_current->id] = [
                    'name' => 'sfpredoccodugtmdrserv' . $sfdeducao_current->id,
                    'value' => $sfpredoc->codugtmdrserv !== 0 ? $sfpredoc->codugtmdrserv : '',
                    'type' => 'text',
                    'label_show' => false,
                    'attr' => [
                        'id' => 'sfpredoccodugtmdrserv' . $sfdeducao_current->id,
                        'name' => "sfpredoccodugtmdrserv[$sfdeducao_current->id]",
                    ]
                ];

                $arr_field_deducao_id['sfpredoccodmuninf' . $sfdeducao_current->id] = [
                    'name' => 'sfpredoccodmuninf' . $sfdeducao_current->id,
                    'value' => $sfpredoc->codmuninf !== 0 ? $sfpredoc->codmuninf : '',
                    'type' => 'text',
                    'label_show' => false,
                    'attr' => [
                        'id' => 'sfpredoccodmuninf' . $sfdeducao_current->id,
                        'name' => "sfpredoccodmuninf[$sfdeducao_current->id]",
                        'onkeyup' => "buscaMunicipioPorCod($sfdeducao_current->id, this, 'municipioDeducaoPreDoc');",
                        'class' => 'form-control buscaMunicipioCod camposVariaveisSfDeducao camposVariaveisSfDeducao' . $sfdeducao_current->id
                    ]
                ];

                $arr_field_deducao_id['sfpredocnumnf' . $sfdeducao_current->id] = [
                    'name' => 'sfpredocnumnf' . $sfdeducao_current->id,
                    'value' => $sfpredoc->numnf !== 0 ? $sfpredoc->numnf : '',
                    'type' => 'text',
                    'label_show' => false,
                    'attr' => [
                        'id' => 'sfpredocnumnf' . $sfdeducao_current->id,
                        'name' => "sfpredocnumnf[$sfdeducao_current->id]",
                    ]
                ];

                $arr_field_deducao_id['sfpredocdtemisnf' . $sfdeducao_current->id] = [
                    'name' => 'sfpredocdtemisnf' . $sfdeducao_current->id,
                    'value' => $sfpredoc->dtemisnf,
                    'type' => 'date',
                    'label_show' => false,
                    'attr' => [
                        'id' => 'sfpredocdtemisnf' . $sfdeducao_current->id,
                        'name' => "sfpredocdtemisnf[$sfdeducao_current->id]",
                    ]
                ];

                $arr_field_deducao_id['sfpredoctxtserienf' . $sfdeducao_current->id] = [
                    'name' => 'sfpredoctxtserienf' . $sfdeducao_current->id,
                    'value' => $sfpredoc->txtserienf,
                    'type' => 'text',
                    'label_show' => false,
                    'attr' => [
                        'id' => 'sfpredoctxtserienf' . $sfdeducao_current->id,
                        'name' => "sfpredoctxtserienf[$sfdeducao_current->id]",
                    ]
                ];

                $arr_field_deducao_id['sfpredocnumsubserienf' . $sfdeducao_current->id] = [
                    'name' => 'sfpredocnumsubserienf' . $sfdeducao_current->id,
                    'value' => $sfpredoc->numsubserienf !== 0 ? $sfpredoc->numsubserienf : '',
                    'type' => 'text',
                    'label_show' => false,
                    'attr' => [
                        'id' => 'sfpredocnumsubserienf' . $sfdeducao_current->id,
                        'name' => "sfpredocnumsubserienf[$sfdeducao_current->id]",
                    ]
                ];

                $arr_field_deducao_id['sfpredocnumaliqnf' . $sfdeducao_current->id] = [
                    'name' => 'sfpredocnumaliqnf' . $sfdeducao_current->id,
                    'value' => $sfpredoc->numaliqnf ? number_format($sfpredoc->numaliqnf, 2, ',', '.') : '',
                    'type' => 'text',
                    'label_show' => false,
                    'attr' => [
                        'id' => 'sfpredocnumaliqnf' . $sfdeducao_current->id,
                        'name' => "sfpredocnumaliqnf[$sfdeducao_current->id]",
                        'onkeyup' => "atualizarMaskMoney(this)",
                    ]
                ];

                $arr_field_deducao_id['sfpredocvlrnf' . $sfdeducao_current->id] = [
                    'name' => 'sfpredocvlrnf' . $sfdeducao_current->id,
                    'value' => $sfpredoc->vlrnf ? number_format($sfpredoc->vlrnf, 2, ',', '.') : '',
                    'type' => 'text',
                    'label_show' => false,
                    'attr' => [
                        'id' => 'sfpredocvlrnf' . $sfdeducao_current->id,
                        'name' => "sfpredocvlrnf[$sfdeducao_current->id]",
                        'onkeyup' => "atualizarMaskMoney(this)",
                    ]
                ];

                $arr_field_deducao_id['sfpredoctipo' . $sfdeducao_current->id] = [
                    'name' => 'sfpredoctipo' . $sfdeducao_current->id,
                    'value' => $sfdeducao_current->codsit && str_starts_with($sfdeducao_current->codsit, 'DDR') ? 'predocdar' : 'predocdarf',
                    'type' => 'hidden',
                    'label_show' => false,
                    'attr' => [
                        'id' => 'sfpredoctipo' . $sfdeducao_current->id,
                        'name' => "sfpredoctipo[$sfdeducao_current->id]",
                    ]
                ];

            }

            //total da aba dedução
            $totalValorDeducao += $sfdeducao_current->vlr;

            //campos data vencimento e pagamento
            $arr_field_deducao_id = $this->camposDeducao($sfdeducao_current, $arr_field_deducao_id, $optionsUgPagamento, $arrayOptionsSituacao);

            foreach ($arrCamposVariaveis as $camposVariaveis) {
                #verifica se o campo variavel pertence a sfdeducao ou sfacrescimo
                $morphSfDeducaoOuSfAcrescimo = $this->verificarTabelaCampoVariavelDeducao(
                    $camposVariaveis['campo'],
                    $sfdeducao_current,
                    null
                );

                $mascara = $this->inserirPontosNaMascara($camposVariaveis['mascara']);

                if (!is_null($morphSfDeducaoOuSfAcrescimo) && $morphSfDeducaoOuSfAcrescimo->getTable() === 'sfdeducao') {

                    $tipoCampo = $camposVariaveis['rotulo'] === 'Código do Município' ? "buscaMunicipioPorCod($sfdeducao_current->id, this, 'municipioDeducao');" : '';
                    $classe = $camposVariaveis['rotulo'] === 'Código do Município' ? "buscaMunicipioCod" : '';

                    $arr_field_deducao_id[$camposVariaveis['campo'] . $key_sfdeducao] = [
                        'name' => $camposVariaveis['campo'] . $sfdeducao_current->id,
                        'value' => $morphSfDeducaoOuSfAcrescimo->{$camposVariaveis['campo']} === 0 ? '' :
                            $morphSfDeducaoOuSfAcrescimo->{$camposVariaveis['campo']},
                        'type' => 'text',
                        'required ' => true,
                        'label' => $camposVariaveis['rotulo'],
                        'label_attr' => [
                            'class' => 'control-label' . ' ' .
                                $camposVariaveis['campo'] . $sfdeducao_current->id,
                            'for' => $camposVariaveis['campo'] . $sfdeducao_current->id
                        ],
                        'attr' => [
                            'id' => $camposVariaveis['campo'] . $sfdeducao_current->id,
                            'class' => 'form-control camposVariaveisSfDeducao camposVariaveisSfDeducao' . $sfdeducao_current->id . ' ' . $classe,
                            'name' => $camposVariaveis['campo'] . "[$sfdeducao_current->id]",
                            'data-mascara' => $mascara,
                            'data-restricao' => $this->getRestricaoCampoVariavel(
                                $sfdeducao_current->codsit,
                                $camposVariaveis['campo']
                            ),
                            'onkeyup' => $tipoCampo,
                            'data-rotulo-tabela' => $this->getRotuloCampoVariavel(
                                $sfdeducao_current->codsit,
                                $camposVariaveis['campo']
                            ),
                        ],
                    ];
                }
            }

            //lista recolhedores
            $sfdeducao_current->recolhedores = SfItemRecolhimento::where('sfded_id', $sfdeducao_current->id)->orderBy('numseqitem', 'ASC')->get();

            foreach ($sfdeducao_current->recolhedores as $keyri => $current_recolhedor) {
                $this->reorderNumSeqItem(
                    'App\Models\SfItemRecolhimento',
                    $current_recolhedor,
                    $keyri + 1
                );

                $arr_field_deducao_id = $this->camposRecolhedorDeducao($sfdeducao_current, $current_recolhedor, $arr_field_deducao_id);
            }

            //validando tamanho para mandar flag quando não há recolhedor(es)
            if (sizeof($sfdeducao_current->recolhedores) <= 0) {
                $sfdeducao_current->recolhedores = 'vazio';
            }
            //lista Relacionamentos
            $arrRelacionamentos = SfPco::where('sfpadrao_id', $sfdeducao_current->sfpadrao_id)->orderBy('id', 'ASC')->get();
            $sfpcoitemRelacionamento = SfPcoItem::whereIn('sfpco_id', $arrRelacionamentos->pluck('id'))
                ->groupBy(['id', 'numseqitem', 'numempe', 'vlr', 'codsubitemempe', 'sfpco_id', 'subelemento_id'])
                ->select(['id', 'numseqitem', 'numempe', 'vlr', 'codsubitemempe', 'sfpco_id', 'subelemento_id'])
                ->orderBy('sfpco_id', 'ASC')
                ->get();

            $sfdeducao_current->relacionamentos = $sfpcoitemRelacionamento;

            $sfdeducao_current->relItemDeducao = SfRelItemDeducao::where('sfded_id', $sfdeducao_current->id)
                ->orderBy('numseqpai', "ASC")
                ->orderBy('numseqitem', "ASC")
                ->get();

            foreach ($sfdeducao_current->relItemDeducao as $key => $relItemDed) {
                $sfpco = SfPco::where('sfpadrao_id', $sfdeducao_current->sfpadrao_id)
                    ->where('numseqitem', $relItemDed->numseqpai)
                    ->first();

                if (!$sfpco) {
                    continue;
                }

                $sfPcoItem = SfPcoItem::where('id', $relItemDed->sfpcoitem_id)
                    ->where('sfpco_id', $sfpco->id)
                    ->first();

                if ($sfPcoItem) {
                    $arr_field_deducao_id = $this->camposRelItemDeducao($sfdeducao_current, $relItemDed, $sfpco, $arr_field_deducao_id, $sfPcoItem);
                }
            }

            # LISTA DE ACRESCIMOS
            $sfdeducao_current->acrescimos = SfAcrescimo::where('sfded_id', $sfdeducao_current->id)
                ->orderBy('id', 'ASC')
                ->get();

            $arr_field_deducao_id['sfdeducaopossui_acrescimo' . $sfdeducao_current->id] = [
                'name' => 'sfdeducaopossui_acrescimo' . $sfdeducao_current->id,
                'value' => $sfdeducao_current->possui_acrescimo ?? '',
                'type' => 'select',
                'choices' => ($sfdeducao_current->acrescimos != null && count($sfdeducao_current->acrescimos)) == 0 ? [1 => 'SIM', 0 => 'NÃO'] : [1 => 'SIM'],
                'rules' => 'required',
                'label_show' => false,
                'selected' => count($sfdeducao_current->acrescimos) == 0 ? 0 : $sfdeducao_current->possui_acrescimo,
                'attr' => [
                    'id' => 'sfdeducaopossui_acrescimo' . $sfdeducao_current->id,
                    'name' => 'sfdeducaopossui_acrescimo[]',
                    'onchange' => "exibirOuEsconderCamposDeducao($sfdeducao_current->id, this, 'acrescimo-deducao-container');"
                ]
            ];

            if (sizeof($sfdeducao_current->acrescimos) <= 0) {
                $sfdeducao_current->acrescimos = 'vazio';
            } else {
                foreach ($sfdeducao_current->acrescimos as $key_acrescimo => $acrescimo_current) {

                    $arr_field_deducao_id['sfacrescimonumemp' . $sfdeducao_current->id . $acrescimo_current->id] = [
                        'name' => 'sfacrescimonumemp' . $sfdeducao_current->id . $acrescimo_current->id,
                        'value' => $acrescimo_current->numempe,
                        'type' => 'text',
                        'label_show' => false,
                        'attr' => [
                            'id' => 'sfacrescimonumemp' . $sfdeducao_current->id . $acrescimo_current->id,
                            'name' => "sfacrescimonumemp[$acrescimo_current->id]"
                        ]
                    ];

                    $arr_field_deducao_id['sfacrescimotpacrescimo' . $sfdeducao_current->id . $acrescimo_current->id] = [
                        'name' => 'sfacrescimotpacrescimo' . $sfdeducao_current->id . $acrescimo_current->id,
                        'selected' => $acrescimo_current->tpacrescimo ?? 'Selecione',
                        'type' => 'select',
                        'label_show' => false,
                        'choices' => [
                            'M' => 'Multa',
                            'J' => 'Juros de Mora',
                            'E' => 'Encargos',
                            'O' => 'Outros Acréscimos',
                        ],
                        'default' => 'Selecione',
                        'attr' => [
                            'id' => 'sfacrescimotpacrescimo' . $sfdeducao_current->id . $acrescimo_current->id,
                            'name' => "sfacrescimotpacrescimo[$acrescimo_current->id]"
                        ]
                    ];

                    $arr_field_deducao_id['sfacrescimocodsubitemempe' . $sfdeducao_current->id . $acrescimo_current->id] = [
                        'name' => 'sfacrescimocodsubitemempe' . $sfdeducao_current->id . $acrescimo_current->id,
                        'value' => $acrescimo_current->codsubitemempe,
                        'type' => 'text',
                        'label_show' => false,
                        'attr' => [
                            'id' => 'sfacrescimocodsubitemempe' . $sfdeducao_current->id . $acrescimo_current->id,
                            'name' => "sfacrescimocodsubitemempe[$acrescimo_current->id]"
                        ]
                    ];

                    $arr_field_deducao_id['sfacrescimovlr' . $sfdeducao_current->id . $acrescimo_current->id] = [
                        'name' => 'sfacrescimovlr' . $sfdeducao_current->id . $acrescimo_current->id,
                        'value' => $acrescimo_current->vlr ? number_format($acrescimo_current->vlr, 2, ',', '.') : '',
                        'type' => 'text',
                        'label_show' => false,
                        'attr' => [
                            'id' => 'sfacrescimovlr' . $sfdeducao_current->id . $acrescimo_current->id,
                            'name' => "sfacrescimovlr[$acrescimo_current->id]",
                            'onkeyup' => "atualizarMaskMoney(this)",
                            'data-valor-vlracrescimo' => number_format($acrescimo_current->vlr, 2, ',', '.')
                        ]
                    ];

                    $arr_field_deducao_id['sfitemacrescimo' . $sfdeducao_current->id . $acrescimo_current->id] = [
                        'name' => 'sfitemacrescimo' . $sfdeducao_current->id . $acrescimo_current->id,
                        'value' => $acrescimo_current->id,
                        'type' => 'hidden',
                        'label_show' => false,
                        'attr' => [
                            'id' => 'sfitemacrescimo' . $sfdeducao_current->id . $acrescimo_current->id,
                            'name' => "sfitemacrescimo[$acrescimo_current->id]"
                        ]
                    ];

                    #######################################################
                    ####criar campos variaveis de acrescimo ###############
                    #######################################################

                    foreach ($arrCamposVariaveis as $camposVariaveis) {
                        #verifica se o campo variavel pertence a sfdeducao ou sfacrescimo
                        $morphSfDeducaoOuSfAcrescimo = $this->verificarTabelaCampoVariavelDeducao(
                            $camposVariaveis['campo'],
                            null,
                            $acrescimo_current,
                            $sfdeducao_current->codsit
                        );

                        $mascara = $this->inserirPontosNaMascara($camposVariaveis['mascara']);
                        if (!is_null($morphSfDeducaoOuSfAcrescimo) && $morphSfDeducaoOuSfAcrescimo->getTable() === 'sfacrescimo') {

                            $arr_field_deducao_id[$camposVariaveis['campo'] . $key_sfdeducao . $key_acrescimo] = [
                                'name' => $camposVariaveis['campo'] . $sfdeducao_current->id . $acrescimo_current->id,
                                'value' => $morphSfDeducaoOuSfAcrescimo->{$camposVariaveis['campo']} === 0 ? '' :
                                    $morphSfDeducaoOuSfAcrescimo->{$camposVariaveis['campo']},
                                'type' => 'text',
                                'required ' => true,
                                'label_show' => false,
                                'attr' => [
                                    'id' => $camposVariaveis['campo'] . $sfdeducao_current->id . $acrescimo_current->id,
                                    'class' => 'form-control camposVariaveisSfDeducao camposVariaveisAcrescimoSfDeducao' . $sfdeducao_current->id,
                                    'name' => $camposVariaveis['campo'] . "[$acrescimo_current->id]",
                                    'data-mascara' => $mascara,
                                    'data-restricao' => $this->getRestricaoCampoVariavel(
                                        $sfdeducao_current->codsit,
                                        $camposVariaveis['campo']
                                    ),
                                    'data-rotulo-tabela' => $this->getRotuloCampoVariavel(
                                        $sfdeducao_current->codsit,
                                        $camposVariaveis['campo']
                                    ),
                                ],
                            ];
                        }
                    }
                }
            }
        }

        $sfDeducaoForm = \FormBuilder::createByArray(
            array_merge(
                $arr_field_deducao_id
            ),
            [
                'url' => route('fatura.form.alterar-aba-deducao', [$sf_padrao_id, $apropriacao_id]),
                'method' => 'POST',
                'novalidate' => 'novalidate',
                'attr' => [
                    'class' => '',
                    'id' => 'formAbaDeducao'
                ]
            ]);
//        $sfDeducaoForm->add(
//            'btnSubmitFormDeducao', 'submit', [
//                'label' => '<i class="fa fa-save"></i> Confirmar Aba Dedução',
//                'attr' => [
//                    'class' => 'btn btn-success esconderQuandoApropriacaoEmitidaSiafi'
//                ]
//            ]
//        );

        return $sfDeducaoForm;
    }

    public function criarDadosAbaDadosPagamento(&$arr_favorecidos, int $contrato_id, $apropriacao_id, $sf_padrao_id)
    {

        $sfDadosBasicos = SfDadosBasicos::where('sfpadrao_id', $sf_padrao_id)->get()->first();

        $arr_field_pgto = [];
        foreach ($arr_favorecidos as $key => $favorecido) {

            $date = \DateTime::createFromFormat('d/m/Y', $sfDadosBasicos->dtpgtoreceb);
            $arr_field_pgto['dtpgtoreceb'] = [
                'name' => 'dtpgtoreceb',
                'value' => $date ? $date->format('Y-m-d') : null, // Converte para YYYY-MM-DD
                'type' => 'date',
                'label_show' => false,
                'attr' => [
                    'id' => 'dtpgtoreceb',
                    'name' => "dtpgtoreceb",
                ],
            ];

            #verifica se há registro de operação de crédito ativa no AntecipaGov
            $contratoAntecipagov = ContratoAntecipagov::where('contrato_id', $contrato_id)
                    ->latest('id')
                    ->first();

            // caso situacao seja diferente de liquidar LIQUIDAR e diferente de CANCELAR
            if ($contratoAntecipagov !== null && $contratoAntecipagov->antecipagov->status_operacao !== 'LIQUIDAR' &&
                $contratoAntecipagov->antecipagov->status_operacao !== 'CANCELAR'
            ) {
                $favorecido->dadosAntecipaGov = $contratoAntecipagov->antecipagov;
            }else{
                $arr_field_pgto['codcredordevedor'] = [
                    'name' => 'codcredordevedor',
                    'value' => $favorecido->codcredordevedor,
                    'type' => 'text',
                    'rules' => 'required',
                    'label_show' => false,
                    'attr' => [
                        'id' => 'codcredordevedor',
                        'name' => "codcredordevedor",
                        'style '=> 'width: 100%; padding: 5px; margin-bottom: 0px; text-align: center;',
                        'onchange' => "retornaMascaraCnpjCpf(this); buscarFornecedor(this)"
                    ],
                ];
            }

            $formDadosPagamento = \FormBuilder::createByArray(
            array_merge(
                $arr_field_pgto
            ),[
                'url' => route('fatura.form.alterar-aba-dados-pgto', [$sf_padrao_id, $apropriacao_id]),
                'method' => 'POST',
                'model' => $sfDadosBasicos,
                'novalidate' => 'novalidate',
                'attr' => ['id' => 'sfDadosPagamentoForm']
            ]);

            $formDadosPagamento->add(
                'btnSubmitFormSfDadosPagamento', 'submit', [
                    'label' => '<i class="fa fa-save"></i> Confirmar Dados de Pagamento',
                    'attr' => [
                        'class' => 'btn btn-success esconderQuandoApropriacaoEmitidaSiafi'
                    ]
                ]
            );

            return ['formDadosPagamento' => $formDadosPagamento, 'sfDadosBasicos' => $sfDadosBasicos];
        }
    }

    public function criarDadosFormAbaCentroCusto($sfCentroCusto, $sf_padrao_id, $apropriacao_id)
    {

        $arr_field_codugbenef = [];
        $arr_field_codcentrocusto = [];
        $arr_field_codsiorg = [];
        $arr_field_mesreferencia = [];
        $arr_field_anoreferencia = [];
        $arr_field_numseqpai = [];
        $arr_field_numseqitem = [];
        $arr_field_vlr = [];
        $arr_field_centro_custo_id = [];
        $arrSfpcoSituacao = [];

        $currentKeyIndex = 0;

        #reordena os numseqitem de centro de custo
        $this->reorderNumSeqItemCentroCusto($sf_padrao_id);

        foreach ($sfCentroCusto as $keycc => $current_centro_custo) {

            #reordena os numseqitem de centro de custo
//            $this->reorderNumSeqItemCentroCusto(
//                'App\Models\SfCentroCusto',
//                $current_centro_custo,
//                $keycc + 1
//            );

            #######################################################
            #preenche array de campos referente ao centro de custo#
            #######################################################

            $arr_field_codugbenef['codugbenef' . $current_centro_custo->id] = [
                'name' => 'codugbenef' . $current_centro_custo->id,
                'value' => $current_centro_custo->codugbenef,
                'type' => 'text',
                'rules' => 'required',
                'label_show' => false,
                'attr' => [
                    'id' => 'codugbenef' . $current_centro_custo->id,
                    'name' => 'codugbenef[]',
                    'class' => "form-control input-sm centroCustoObrigatorio centroCustoObrigatorio{$current_centro_custo->id}",
                ],
            ];

            $arr_field_codcentrocusto['codcentrocusto' . $current_centro_custo->id] = [
                'name' => 'codcentrocusto' . $current_centro_custo->id,
                'value' => $current_centro_custo->codcentrocusto,
                'type' => 'text',
                'rules' => 'required',
                'label_show' => false,
                'attr' => [
                    'id' => 'codcentrocusto' . $current_centro_custo->id,
                    'name' => 'codcentrocusto[]',
                    'class' => "form-control input-sm centroCustoObrigatorio centroCustoObrigatorio{$current_centro_custo->id}",
                    'maxlength' => 11
                ],
            ];

            $arr_field_codsiorg['codsiorg' . $current_centro_custo->id] = [
                'name' => 'codsiorg' . $current_centro_custo->id,
                'value' => $current_centro_custo->codsiorg,
                'type' => 'number',
                'label_show' => false,
                'attr' => [
                    'id' => 'codsiorg' . $current_centro_custo->id,
                    'name' => 'codsiorg[]',
                    'class' => "form-control input-sm centroCustoObrigatorio centroCustoObrigatorio{$current_centro_custo->id}",
                ],
            ];

            $arr_field_mesreferencia['mesreferencia' . $current_centro_custo->id] = [
                'name' => 'mesreferencia' . $current_centro_custo->id,
                'value' => $current_centro_custo->mesreferencia,
                'type' => 'text',
                'rules' => 'required',
                'label_show' => false,
                'attr' => [
                    'id' => 'mesreferencia' . $current_centro_custo->id,
                    'name' => 'mesreferencia[]',
                    'class' => "form-control input-sm centroCustoObrigatorio centroCustoObrigatorio{$current_centro_custo->id}",
                ],
            ];

            $arr_field_anoreferencia['anoreferencia' . $current_centro_custo->id] = [
                'name' => 'anoreferencia' . $current_centro_custo->id,
                'value' => $current_centro_custo->anoreferencia,
                'type' => 'text',
                'rules' => 'required',
                'label_show' => false,
                'attr' => [
                    'id' => 'anoreferencia' . $current_centro_custo->id,
                    'name' => 'anoreferencia[]',
                    'class' => "form-control input-sm centroCustoObrigatorio centroCustoObrigatorio{$current_centro_custo->id}",
                ],
            ];

            ############################################################
            #preenche arrays com campos referente a situacao da aba pco#
            ############################################################

            $sfpco = SfPco::where('sfpadrao_id', $sf_padrao_id)->get();
            $sfpcoAfetaCustoNao = $this->sfpcoAfetaCustoNaoId($sf_padrao_id);

            if (!$sfpcoAfetaCustoNao) {
                continue;
            }

            $sfpcoitem = SfPcoItem::whereIn('sfpco_id', $sfpcoAfetaCustoNao)
                ->orderBy('id', 'asc')
                ->get();

            $contratosFaturasApropriacao = ApropriacaoContratoFaturas::where('sfpadrao_id', $sf_padrao_id)->get()->pluck('contratofaturas_id');

            $valor_mes_CC = ContratoFaturaMesAno::whereIn('contratofaturas_id', $contratosFaturasApropriacao)
                ->orderBy('mesref', 'ASC')
                ->orderBy('anoref', 'ASC')
                ->get();

            $temDespesa = array_filter($sfpco->toArray(), function ($valor) {
                return $valor['despesaantecipada'] === true;
            });

            if (count($valor_mes_CC) == 1 && !$temDespesa) {

                $keys = array_keys($sfpcoitem->toArray());

                if (empty($keys)) {
                    continue;
                }

                if ($currentKeyIndex >= count($keys)) {
                    $currentKeyIndex = 0;
                }

                $currentKey = $keys[$currentKeyIndex];
                $currentItem = $sfpcoitem[$currentKey];

                $sfrelitemvlrcc = Sfrelitemvlrcc::select('vlr')
                    ->where('sfcc_id', $current_centro_custo->id)
                    ->where('numseqpai', $currentItem->pco->numseqitem)
                    ->where('numseqitem', $currentItem->numseqitem)
                    ->first();

                $indexField = $keycc . $currentKey;

                $arrSfpcoSituacao[$current_centro_custo->id][$indexField] = [
                    'sfpcoitem_id' => $currentItem->id,
                    'codsit' => $currentItem->pco->codsit,
                    'despesaantecipada' => $currentItem->pco->despesaantecipada,
                    'numempe' => $currentItem->numempe,
                    'sfrelitemvlrcc' => $sfrelitemvlrcc,
                    'numseqpai' => $currentItem->pco->numseqitem,
                    'numseqitem' => $currentItem->numseqitem,
                    'idcc' => $current_centro_custo->id,
                    'indexField' => $indexField,
                    'pcoitem_vlr' => number_format(
                        $currentItem->vlr, 2, ',', '.'
                    )
                ];

                $this->criarDadosFormSituacaoAbaCentroCusto(
                    $currentItem->pco->despesaantecipada,
                    $current_centro_custo->id,
                    $indexField,
                    $currentItem->pco->numseqitem,
                    $currentItem->numseqitem,
                    $sfrelitemvlrcc,
                    $currentItem->vlr,
                    $arr_field_numseqpai,
                    $arr_field_numseqitem,
                    $arr_field_vlr,
                    $arr_field_centro_custo_id
                );

                // Incrementa o índice para a próxima iteração do loop principal
                $currentKeyIndex++;

            } else {

                foreach ($sfpco as $keysfpco => $sfpco_current) {
                    $situacaoAfetaCusto = $this->retornarAfetaCustoSituacao($sfpco_current->codsit);
                    #se a situacao tiver o campo afeta_custo false então não exibe na aba de centro de custo
                    if (!$situacaoAfetaCusto) {
                        continue;
                    }

                    $sfpcoitem = SfPcoItem::where('sfpco_id', $sfpco_current->id)
                        ->orderBy('id', 'asc')
                        ->get();

                    foreach ($sfpcoitem as $keysfpi => $sfpcoitem_current) {
                        $sfrelitemvlrcc = Sfrelitemvlrcc::select('vlr')
                            ->where('sfcc_id', $current_centro_custo->id)
                            ->where('numseqpai', $sfpco_current->numseqitem)
                            ->where('numseqitem', $sfpcoitem_current->numseqitem)
                            ->first();

                        $indexField = $keycc . $keysfpco . $keysfpi;

                        $arrSfpcoSituacao[$current_centro_custo->id][$indexField] = [
                            'sfpcoitem_id' => $sfpcoitem_current->id,
                            'codsit' => $sfpco_current->codsit,
                            'despesaantecipada' => $sfpco_current->despesaantecipada,
                            'numempe' => $sfpcoitem_current->numempe,
                            'sfrelitemvlrcc' => $sfrelitemvlrcc,
                            'numseqpai' => $sfpco_current->numseqitem,
                            'numseqitem' => $sfpcoitem_current->numseqitem,
                            'idcc' => $current_centro_custo->id,
                            'indexField' => $indexField,
                            'pcoitem_vlr' => number_format(
                                count($sfpcoitem) == 1 && count($valor_mes_CC) > 1 ? $valor_mes_CC[$keycc]->valorref ?? 0 : $sfpcoitem_current->vlr, 2, ',', '.'
                            )
                        ];

                        if ($sfpco_current->despesaantecipada) {
                            $valor_item = $sfpco_current->cronBaixaPatrimonial->first()->parcela()->get();

                            if (isset($valor_item[$keycc])) {
                                $valor_item = $valor_item[$keycc]->vlr;
                            } else {
                                $valor_item = 0;
                            }

                        } else {
                            $valor_item = count($sfpcoitem) == 1 && count($valor_mes_CC) > 1 ? $valor_mes_CC[$keycc]->valorref ?? 0 : $sfpcoitem_current->vlr;
                        }

                        $this->criarDadosFormSituacaoAbaCentroCusto(
                            $sfpco_current->despesaantecipada,
                            $current_centro_custo->id,
                            $indexField,
                            $sfpco_current->numseqitem,
                            $sfpcoitem_current->numseqitem,
                            $sfrelitemvlrcc,
                            $valor_item,
                            $arr_field_numseqpai,
                            $arr_field_numseqitem,
                            $arr_field_vlr,
                            $arr_field_centro_custo_id,
                            $sfpcoitem_current->vlr
                        );
                    }
                }
            }
            $current_centro_custo->arrSfpcoSituacao = $arrSfpcoSituacao;
        }

        $sfCentroCustoForm = \FormBuilder::createByArray(
            array_merge(
                $arr_field_codugbenef,
                $arr_field_codcentrocusto,
                $arr_field_codsiorg,
                $arr_field_mesreferencia,
                $arr_field_anoreferencia,
                $arr_field_numseqpai,
                $arr_field_numseqitem,
                $arr_field_vlr,
                $arr_field_centro_custo_id
            ),
            [
                'url' => route('fatura.form.alterar-aba-centro-custo', [$sf_padrao_id, $apropriacao_id]),
                'method' => 'POST',
                'novalidate' => 'novalidate',
                'attr' => [
                    'id' => 'sfCentroCustoForm'
                ]
            ]
        );
//        $sfCentroCustoForm->add(
//            'btnSubmitFormSfCentroCusto',
//            'submit',
//            [
//                'label' => '<i class="fa fa-save"></i> Confirmar Centro de Custo',
//                'attr' => [
//                    'class' => 'btn btn-success esconderQuandoApropriacaoEmitidaSiafi btnConfirmarCentroCusto'
//                ]
//            ]
//        );

        return $sfCentroCustoForm;
    }


    /**
     *
     * * Cria os campos referentes a form de situacões na aba centro de custo
     *
     * @param $centro_custo_id
     * @param $indexField
     * @param $numseqpai
     * @param $numseqitem
     * @param $vlr
     * @param array $arr_field_numseqpai
     * @param array $arr_field_numseqitem
     * @param array $arr_field_vlr
     * @param array $arr_field_centro_custo_id
     */
    public function criarDadosFormSituacaoAbaCentroCusto(
        $booSfpcoPossuiDespesaAntecipada,
        $centro_custo_id,
        $indexField,
        $numseqpai,
        $numseqitem,
        $sfrelitemvlrcc,
        $pcoitem_vlr,
        array &$arr_field_numseqpai,
        array &$arr_field_numseqitem,
        array &$arr_field_vlr,
        array &$arr_field_centro_custo_id,
        $valorEmpenho = null
    ): void {

        $arr_field_numseqpai['numseqpai_checkbox' . $indexField] = [
            'name' => 'numseqpai_checkbox' . $indexField,
            'value' => $numseqpai,
            'type' => 'checkbox',
            'checked' => !!$sfrelitemvlrcc,
            'label_show' => false,
            'attr' => [
                'id' => $indexField,
                'name' => "numseqpai_checkbox[$indexField]",
                'class' => "pointer-hand checkbox-numseqpai checkbox-numseqpai$numseqpai$numseqitem",
                'data-checkbox-numseqitem' => $numseqitem,
                'data-checkbox-centro-custo' => $centro_custo_id,
                'onchange' => "atualizarValoresLabelInformativa()"
            ],
        ];

        //campo hidden para guardar array com ids de numseqpai
        $arr_field_numseqitem['numseqpai' . $indexField] = [
            'name' => 'numseqpai' . $indexField,
            'value' => $numseqpai,
            'type' => 'hidden',
            'label_show' => false,
            'attr' => [
                'id' => 'numseqpai' . $indexField,
                'name' => "numseqpai[$indexField]"
            ]
        ];

        //campo hidden para guardar array com ids de numseqitem
        $arr_field_numseqitem['numseqitem' . $indexField] = [
            'name' => 'numseqitem' . $indexField,
            'value' => $numseqitem,
            'type' => 'hidden',
            'label_show' => false,
            'attr' => [
                'id' => 'numseqitem' . $indexField,
                'name' => "numseqitem[$indexField]"
            ]
        ];

        //campo hidden para guardar array com ids de centro_custo
        $arr_field_centro_custo_id['centro_custo_id' . $indexField] = [
            'name' => 'centro_custo_id' . $indexField,
            'value' => $centro_custo_id,
            'type' => 'hidden',
            'label_show' => false,
            'attr' => [
                'id' => 'centro_custo_id' . $indexField,
                'name' => "centro_custo_id[$indexField]"
            ]
        ];

        //campo hidden para guardar array que verifica se sfpco possui despesa antecipada
        $arr_field_centro_custo_id['despesaantecipada' . $indexField] = [
            'name' => 'despesaantecipada' . $indexField,
            'value' => $booSfpcoPossuiDespesaAntecipada,
            'type' => 'hidden',
            'label_show' => false,
            'attr' => [
                'id' => 'despesaantecipada' . $indexField,
                'name' => "despesaantecipada[$indexField]"
            ]
        ];

        $arr_field_vlr['vlr'. $indexField] = [
            'name' => 'vlr'. $indexField,
            'value' => $sfrelitemvlrcc  ? number_format($sfrelitemvlrcc->vlr, 2, ',', '.') : number_format($pcoitem_vlr, 2, ',', '.'),
            'type' => 'text',
            'rules' => 'required',
            'label_show' => false,
            'attr' => [
                'id'    => 'vlr'. $indexField,
                'name'  => "vlr[$indexField]",
                'class' => "form-control input-sm vlr$numseqpai$numseqitem",
                'onkeyup' => "atualizarValoresLabelInformativa();
                              atualizarMaskMoney(this);
                              validarValoresEmpenhosSelecionados(this, $numseqpai, $numseqitem)",
                'data-valor-pcoitem' => number_format($valorEmpenho !== null ? $valorEmpenho : $pcoitem_vlr, 2, ',', '.')
            ],
        ];
    }

    public function getTipoPredoc($tipoPredoc): string
    {
        switch ($tipoPredoc) {
            case 'predocob':
                return 'OB';
            case 'predocdarf':
                return 'DARF';
            default:
                return 'PREDOC';
        }
    }

    public function formatarVlrParaBanco($valorref)
    {
        return number_format(
            floatval(
                str_replace(
                    ',',
                    '.',
                    str_replace(
                        '.',
                        '',
                        $valorref ?? 0
                    )
                )
            ),
            2,
            '.',
            ''
        );
    }

    public function recuperarArrayOptionsSelectSituacao($codtipodh)
    {

//        $idTipoSituacaoDHSiafi = $this->retornaIdCodigoItemPorDescricaoEDescres(
//            'Tipos Situações DH SIAFI',
//            'SIT0001'
//        );

        $idTipoSituacaoDHSiafi = $this->retornaIdsCodigoItemPorDescricaoEDescresArray(
            'Tipos Situações DH SIAFI',
            ['SIT0001','SIT0004','SIT0005', 'SIT0015', 'SIT0016']
        );

        $idCodTipoDH = $this->retornaIdCodigoItemPorDescricaoEDescres('Tipos DH SIAFI', $codtipodh);

        $arrayTipoSituacaoDHSiafi = Execsfsituacao::join(
            'execsfsituacao_tipodoc',
            'execsfsituacao_tipodoc.execsfsituacao_id',
            '=',
            'execsfsituacao.id'
        )
            ->whereIn('tipo_situacao', $idTipoSituacaoDHSiafi)
            ->where('tipodoc_id', $idCodTipoDH)
            ->where('status', true)
            ->orderBy('codigo')
            ->pluck('codigo', 'codigo')
            ->toArray();

        $arrayOptions = Arr::collapse([['' => 'Selecione...'], $arrayTipoSituacaoDHSiafi]);

        return $arrayOptions;
    }

    public function recuperarArrayOptionsSelectSituacaoDeducao($codtipodh)
    {

        $idTipoSituacaoDHSiafi = $this->retornaIdCodigoItemPorDescricaoEDescres(
            'Tipos Situações DH SIAFI',
            'SIT0007'
        );

        $idCodTipoDH = $this->retornaIdCodigoItemPorDescricaoEDescres('Tipos DH SIAFI', $codtipodh);

        $arrayTipoSituacaoDHSiafi = Execsfsituacao::join(
            'execsfsituacao_tipodoc',
            'execsfsituacao_tipodoc.execsfsituacao_id',
            '=',
            'execsfsituacao.id'
        )
            ->where('tipo_situacao', $idTipoSituacaoDHSiafi)
            ->where('tipodoc_id', $idCodTipoDH)
            ->where('status', true)
            ->orderBy('codigo')
            ->pluck('codigo', 'codigo')
            ->toArray();

        $arrayOptions = Arr::collapse([['' => 'Selecione...'], $arrayTipoSituacaoDHSiafi]);

        return $arrayOptions;
    }

//    public function buscaArrayUgPagamentoSelect()
//    {
//        $arrayUnidades = backpack_user()->getAllUnidadesUser()
//            ->map(function ($unidade) {
//                return ['value' => $unidade->codigo, 'description' => $unidade->nome];
//            })
//            ->toArray();
//        // Ordena o array de unidades pelo valor
//        usort($arrayUnidades, function ($a, $b) {
//            return strcmp($a['value'], $b['value']);
//        });
//
//        return $arrayUnidades;
//    }

    public function buscaArrayUgEmitenteEmpenhoSelect($sfpadrao_id)
    {
        $dadosBasicos = SfDadosBasicos::where('sfpadrao_id', $sfpadrao_id)->first();

        if ($dadosBasicos) {
            $orgaoId = Unidade::where('codigo', $dadosBasicos->codugpgto)->value('orgao_id');

            if ($orgaoId) {
                $arrayUnidades = Unidade::where('orgao_id', $orgaoId)->get()
                    ->mapWithKeys(function ($unidade) {
                        $key = $unidade->codigo;
                        $value = $unidade->codigo . ' - ' . $unidade->nome;
                        return [$key => $value];
                    })
                    ->sort(function ($valor1, $valor2) {
                        // Extrair o nome e faz a ordenação por nome.
                        $nome1 = explode(' - ', $valor1)[1];
                        $nome2 = explode(' - ', $valor2)[1];
                        return strcmp($nome1, $nome2);
                    })
                    ->toArray();
            } else {
                $arrayUnidades = [];
            }
        } else {
            $arrayUnidades = [];
        }

        return $arrayUnidades;
    }

    public function retornarAfetaCustoSituacao($codsit)
    {

        if (is_null($codsit)) {
            return false;
        }

        $execsfsituacao = Execsfsituacao::where('codigo', $codsit)->first()->afeta_custo;

        return $execsfsituacao;
    }

    /**
     * Retorna os campos variaveis pela situacao de sfpco
     *
     * @param $sfpco_current
     * @return mixed
     */
    public function retornarCamposVariaveis($sfpco_current)
    {
        return ExecsfsituacaoCamposVariaveis::join(
            'execsfsituacao as esfs',
            'esfs.id',
            '=',
            'execsfsituacao_campos_variaveis.execsfsituacao_id'
        )
            ->where('esfs.codigo', $sfpco_current->codsit)
            ->get()
            ->toArray();
    }


    /**
     * Retorna a Classe que o campo pertence
     *
     * @param string $campo qualquer um entre ('numclassa', 'numclassb', 'numclassc', 'txtinscra', 'txtinscrb',
     *                      'txtinscrc', 'numclassd', 'numclasse', 'txtinscrd', 'txtinscre')
     * @param SfPco $sfpco_current
     * @param SfPcoItem $sfpcoitem_current
     * @return SfPco|SfPcoItem|null
     */
    public function verificarTabelaCampoVariavel(
        string $campo,
        Sfpco $sfpco_current = null,
        SfPcoItem $sfpcoitem_current = null
    ) {

        $arrCamposSfPco = ['numclassd', 'numclasse', 'txtinscrd', 'txtinscre'];
        $arrCamposSfPcoItem = ['numclassa', 'numclassb', 'numclassc', 'txtinscra', 'txtinscrb', 'txtinscrc'];

        if (in_array($campo, $arrCamposSfPco)) {
            return $sfpco_current;
        }

        if (in_array($campo, $arrCamposSfPcoItem)) {
            return $sfpcoitem_current;
        }

        return null;
    }

    public function verificarTabelaCampoVariavelDeducao(
        string      $campo,
        SfDeducao   $sfdeducao_current = null,
        SfAcrescimo $sfitemacrescimo_current = null,
                    $situacao = null
    )
    {

        $arrCamposSfDeducao = ['txtinscra', 'txtinscrb'];
        $arrCamposSfAcrescimoItem = ['numclassa'];

        if ($situacao && str_starts_with($situacao, 'DDR')) {
            $arrCamposSfAcrescimoItem = ['txtinscra', 'numclassa', 'txtinscrb'];
        }

        if (in_array($campo, $arrCamposSfDeducao) && $sfdeducao_current !== null) {
            return $sfdeducao_current;
        }

        if (in_array($campo, $arrCamposSfAcrescimoItem) && $sfitemacrescimo_current !== null) {
            return $sfitemacrescimo_current;
        }

        return null;
    }

    private function inserirPontosNaMascara($mascara)
    {

        if ($mascara === '' || $mascara === null) {
            return '';
        }

        $arrMascara = str_split($mascara);

        $result = '';
        foreach ($arrMascara as $index => $char) {
            $result .= $char;

            if (in_array($index, [0, 1, 2, 3, 4, 6])) {
                $result .= '.';
            }
        }

        return rtrim($result, '.');
    }

    public function getRestricaoCampoVariavel(string $codsit, string $campo)
    {
        return ExecsfsituacaoCamposVariaveis::select('restricao')
            ->join(
                'execsfsituacao as esfs',
                'esfs.id',
                '=',
                'execsfsituacao_campos_variaveis.execsfsituacao_id'
            )
            ->where('esfs.codigo', $codsit)
            ->where('campo', $campo)
            ->first()
            ->restricao;
    }

    public function getRotuloCampoVariavel(string $codsit, string $campo)
    {
        return ExecsfsituacaoCamposVariaveis::select('rotulo')
            ->join(
                'execsfsituacao as esfs',
                'esfs.id',
                '=',
                'execsfsituacao_campos_variaveis.execsfsituacao_id'
            )
            ->where('esfs.codigo', $codsit)
            ->where('campo', $campo)
            ->first()
            ->rotulo;
    }

    /**
     * Retorna booleando indicando se é para exibir ou nao o campo variável de acordo com a restricao dele
     * @param SfPco $sfpco
     * @param string $campo
     * @param bool $despesaantecipada
     * @param bool $temContrato
     * @return bool
     */
    private function exibirOuEsconderCampoVariavel(
        SfPco $sfpco,
        string $campo,
        bool $despesaantecipada,
        bool $temContrato = false
    ) {
        $tipoFornecedor = $this->recuperarTipoFornecedorDadosBasicos($sfpco->sfpadrao_id);

        $restricaoCampo = $this->getRestricaoCampoVariavel($sfpco->codsit, $campo);

        #caso desepesa antecipada seja verdadeiro verifica a restricao para exibir ou nao
        if ($despesaantecipada) {
            if ($restricaoCampo === self::RESTRICAO_DESPESA_ANTECIPADA) {
                return true;
            }
        }

        #caso tem contrato seja verdadeiro verifica a restricao para exibir ou nao
        if ($temContrato) {
            if ($restricaoCampo === self::RESTRICAO_CONTRATO) {
                return true;
            }
        }

        #caso favorecido seja UG verifica a restricao para exibir ou nao
        //if ($tipoFornecedor === 'UG') {
            if ($restricaoCampo === self::RESTRICAO_REALIZACAO_GRU || $restricaoCampo === self::RESTRICAO_UG_PAIS) {
                return true;
            }
        //}

        if ($restricaoCampo === self::RESTRICAO_SEM_RESTRICAO) {
            return true;
        }

        return false;
    }

    public function recuperarTipoFornecedorDadosBasicos($sfpadrao_id)
    {
        $dadosBasicos = SfDadosBasicos::where('sfpadrao_id', $sfpadrao_id)->get()->first();

        $fornecedor = new Fornecedor();

        return $fornecedor->buscaFornecedorPorNumero($dadosBasicos->codcredordevedor)->tipo_fornecedor;
    }

    /*
     * Ao percorrer as tabelas que possuem numseqitem salva na ordem.
     */
    public function reorderNumSeqItem(string $className, $objClass, $numseqitem)
    {
        $sfTableWithNumseqItem = $className::find($objClass->id);
        $sfTableWithNumseqItem->numseqitem = $numseqitem;
        $sfTableWithNumseqItem->save();
    }

    public function reorderNumSeqItemCentroCusto($sfpadrao_id)
    {
        $sf_centro_custo = SfCentroCusto::where('sfpadrao_id', $sfpadrao_id)
            ->whereNotNull('codcentrocusto')->get();

        foreach ($sf_centro_custo as $key => $sf_centro_custo_current) {
            $sf_centro_custo_current->numseqitem = $key + 1;
            $sf_centro_custo_current->save();
        }
    }

    /**
     * busca dados do empenho em (contratofatura_empenhos e contratofatura) pelo numero do empenho e subelemento de sfpcoitem
     * @param string $numEmpenho
     * @return string|array
     */
    public function findContratoFaturaEmpenhosByNumEmpenho(string $numEmpenho, $apropriacao_id, $idSubelemento, $codUgPco, $idSfPcoItem = null)
    {
        $apropriacao = ApropriacaoContratoFaturas::where('apropriacoes_faturas_id', $apropriacao_id)->first();

        $codigoUnidade[] = [str_pad($apropriacao->sfpadrao->codugemit, 6, "0", STR_PAD_LEFT)];

        if($codUgPco){
            $codigoUnidade[] = [str_pad($codUgPco, 6, "0", STR_PAD_LEFT)];
        }

        if ($apropriacao) {
            $apropriacaos = ApropriacaoFaturas::where('id', $apropriacao_id)->first();

            $contrato_ids = array_column($apropriacaos->faturas->toArray(), 'contrato_id');
            $unidadesGeral = [];
            foreach ($contrato_ids as $contrato_id) {

                $unidadesDescentralizadas = Contrato::findOrFail($contrato_id)
                    ->unidadesdescentralizadas
                    ->pluck('unidade')
                    ->map(function ($unidade) {
                        return [str_pad($unidade->codigo, 6, "0", STR_PAD_LEFT)];
                    })
                    ->toArray();

                $unidadesGeral = array_merge($unidadesDescentralizadas, $unidadesGeral, $codigoUnidade);
            }
            $codigoUnidade = array_values(array_unique($unidadesGeral, SORT_REGULAR));
        }

        $unidade = Unidade::whereIn('codigo', $codigoUnidade)->get();

        $unidades = [];

        foreach ($unidade as $uni) {
            $unidades[] = $uni->id;
        }

        $empenho = Empenho::where('numero', $numEmpenho)->whereIn('unidade_id', $unidades)->pluck('id')->toArray();

        #se n tiver empenho correspondente ao numero entao retorna vazio
        if (!$empenho) {
            return '';
        }

        $arrContratoFaturasId = ApropriacaoContratoFaturas::where('apropriacoes_faturas_id', $apropriacao_id)
            ->pluck('contratofaturas_id')
            ->toArray();

        $faturaEmpenho = ContratoFaturaEmpenhosApropriacao::whereIn('empenho_id', $empenho)
            ->where(function ($query) use ($idSubelemento) {
                $query->where('subelemento_id', $idSubelemento)
                    ->orWhereNull('subelemento_id');
            })
            ->whereIn('contratofatura_id', $arrContratoFaturasId)
            ->where('sfpcoitem_id', $idSfPcoItem);

        #se n tiver faturaEmpenho correspondente ao empenho entao retorna vazio
        if (!$faturaEmpenho) {
            return '';
        }

        return $faturaEmpenho;
    }

    /**
     * Retorna array com subelementos do empenho
     * @param string $empenho
     * @return array
     */
    public function buscarSubelementoDoEmpenho(string $numeroEmpenho = '', string $codUg = '', $from = '') : array
    {

        $unidadeSfPadrao = Unidade::where('codigo', str_pad($codUg, 6, "0", STR_PAD_LEFT))->first();

        if (!$numeroEmpenho) {
            return ['selecione' => 'Selecione'];
        }

        $arrEmpenhos = Empenho::where('numero', $numeroEmpenho)->where('unidade_id', $unidadeSfPadrao->id)->get();
        $arrSubelemento = [];

        foreach ($arrEmpenhos as $empenho) {
            foreach ($empenho->empenhodetalhado as $key => $empDetalhado) {

                    $index = $empDetalhado->naturezasubitem->id;

                $arrSubelemento[$index] = $empDetalhado->getSubitem();
            }
        }
        ksort($arrSubelemento);
        return $arrSubelemento;
    }

    /**
     * Retorna array com subelementos cadastrando do sfpcoitem
     * @param string $empenho
     * @return array
     */
    public function buscarSubelementoCadastradoSfPcoItemDoEmpenho(int $sfpco, int $sfpcoitem): ?int
    {

        $item = SfPcoItem::where('sfpco_id', $sfpco)
            ->where('id', $sfpcoitem)
            ->first();

        if ($item) {
            return $item->subelemento_id;
        } else {
            return null;
        }
    }

    /**
     * Retorna valor para o campo variavel de sfPco quando o campo for
     * @param SfPco $sfPco
     * @param string $nomeCampo
     * @param string $valueCampo
     * @return array|string|string[]
     */
    public function getValueCampoVariavelSfPco(SfPco $sfPco, string $nomeCampo = '', $valueCampo = '')
    {
        $temContrato = $sfPco->indrtemcontrato;
        $booRotuloFavContrato = $this->getRotuloCampoVariavel($sfPco->codsit, $nomeCampo) === 'Favorecido do Contrato';

        if (!is_null($valueCampo) || !$temContrato || !$booRotuloFavContrato) {
            return $valueCampo;
        }
        $sfDadosBasicos = SfDadosBasicos::where('sfpadrao_id', $sfPco->sfpadrao_id)->first();

        return $this->limpa_cpf_cnpj($sfDadosBasicos->codcredordevedor);
    }

    /**
     * Verifica se o codigo do subelemento está entre 1 e 9 caso sim adiciona um 0 a esquerda
     * @param int $codSubelemento
     * @return int|string
     */
    public function formatarSubelementoZeroEsquerda(int $codSubelemento)
    {

        $codSubelementoFormatado = $codSubelemento;

        if ($codSubelemento >= 1 && $codSubelemento <= 9) {
            // Acrescenta um zero à esquerda usando str_pad
            $codSubelementoFormatado = str_pad($codSubelemento, 2, '0', STR_PAD_LEFT);
        }

        return $codSubelementoFormatado;
    }

    /*
     * Procura os registros de SfParcela e ordena o campo numparcela
     */
    public function reorderNumParcela(int $cronbaixapatrimonialId) : void
    {
        $parcelas = SfParcela::where('sfcronbaixapatrimonial_id', $cronbaixapatrimonialId)->orderBy('numparcela')->get();

        if($parcelas->isNotEmpty())
        {
            foreach ($parcelas as $key_parcela => $parcela) {
                $parcela->numparcela = $key_parcela + 1;
                $parcela->save();

            }
        }
    }

    /**
     * @param $sfdeducao_current
     * @param $current_recolhedor
     * @param array $arr_field_deducao_id
     * @return array
     */
    public function camposRecolhedorDeducao($sfdeducao_current, $current_recolhedor, array $arr_field_deducao_id): array
    {
        $arr_field_deducao_id['numseqitem' . $sfdeducao_current->id . $current_recolhedor->id] = [
            'name' => 'numseqitem' . $sfdeducao_current->id . $current_recolhedor->id,
            'value' => $current_recolhedor->numseqitem,
            'type' => 'text',
            'label_show' => false,
            'attr' => [
                'id' => 'numseqitem' . $sfdeducao_current->id . $current_recolhedor->id,
                'name' => 'numseqitem[]',
                'readonly' => true
            ]
        ];

        $arr_field_deducao_id['recolhedor' . $sfdeducao_current->id . $current_recolhedor->id] = [
            'name' => 'recolhedor' . $sfdeducao_current->id . $current_recolhedor->id,
            'value' => $current_recolhedor->codrecolhedor,
            'type' => 'text',
            'rules' => 'required',
            'label_show' => false,
            'attr' => [
                'id' => 'recolhedor' . $sfdeducao_current->id . $current_recolhedor->id,
                'name' => "recolhedor[$current_recolhedor->id]",
                'class' => 'form-control camposVariaveisSfRecolhimento',
            ]
        ];

        $arr_field_deducao_id['vlrbasecalculo' . $sfdeducao_current->id . $current_recolhedor->id] = [
            'name' => 'vlrbasecalculo' . $sfdeducao_current->id . $current_recolhedor->id,
            'value' => $current_recolhedor->vlrbasecalculo ? number_format($current_recolhedor->vlrbasecalculo, 2, ',', '.') : '',
            'type' => 'text',
            'rules' => 'required',
            'label_show' => false,
            'attr' => [
                'id' => 'vlrbasecalculo' . $sfdeducao_current->id . $current_recolhedor->id,
                'name' => "vlrbasecalculo[$current_recolhedor->id]",
                'onchange' => "atualizarMaskMoney(this)",
                'onblur' => "atualizarMaskMoney(this)",
                'data-valor-vlrbasecalculo' => number_format($sfdeducao_current->vlrbasecalculo, 2, ',', '.')
            ],
        ];

        $arr_field_deducao_id['vlrPrincipal' . $sfdeducao_current->id . $current_recolhedor->id] = [
            'name' => 'vlrPrincipal' . $sfdeducao_current->id . $current_recolhedor->id,
            'value' => $current_recolhedor->vlr ? number_format($current_recolhedor->vlr, 2, ',', '.') : '',
            'type' => 'text',
            'rules' => 'required',
            'label_show' => false,
            'attr' => [
                'id' => 'vlrPrincipal' . $sfdeducao_current->id . $current_recolhedor->id,
                'name' => "vlrPrincipal[$current_recolhedor->id]",
                'onchange' => "atualizaTotaisRodape($sfdeducao_current->id);atualizarMaskMoney(this)",
                'onkeyup' => "atualizaTotaisRodape($sfdeducao_current->id);atualizarMaskMoney(this)",
                //'readonly' => true,
                'data-valor-vlrReceita' => number_format($sfdeducao_current->vlr, 2, ',', '.'),
                'class' => "form-control vlrPrincipalSituacao$sfdeducao_current->id"
            ],
        ];

        $arr_field_deducao_id['vlrmulta' . $sfdeducao_current->id . $current_recolhedor->id] = [
            'name' => 'vlrmulta' . $sfdeducao_current->id . $current_recolhedor->id,
            'value' => $current_recolhedor->vlrmulta ? number_format($current_recolhedor->vlrmulta, 2, ',', '.') : '',
            'type' => 'text',
            'rules' => 'required',
            'label_show' => false,
            'attr' => [
                'id' => 'vlrmulta' . $sfdeducao_current->id . $current_recolhedor->id,
                'name' => "vlrmulta[$current_recolhedor->id]",
                'onkeyup' => "atualizaTotaisRodape($sfdeducao_current->id);atualizarMaskMoney(this)",
                'onblur' => "atualizaTotaisRodape($sfdeducao_current->id);atualizarMaskMoney(this)",
                'data-valor-vlrmulta' => number_format($sfdeducao_current->vlrmulta, 2, ',', '.')
            ],
        ];

        $arr_field_deducao_id['vlrjuros' . $sfdeducao_current->id . $current_recolhedor->id] = [
            'name' => 'vlrjuros' . $sfdeducao_current->id . $current_recolhedor->id,
            'value' => $current_recolhedor->vlrjuros ? number_format($current_recolhedor->vlrjuros, 2, ',', '.') : '',
            'type' => 'text',
            'rules' => 'required',
            'label_show' => false,
            'attr' => [
                'id' => 'vlrjuros' . $sfdeducao_current->id . $current_recolhedor->id,
                'name' => "vlrjuros[$current_recolhedor->id]",
                'onkeyup' => "atualizaTotaisRodape($sfdeducao_current->id);atualizarMaskMoney(this)",
                'onblur' => "atualizaTotaisRodape($sfdeducao_current->id);atualizarMaskMoney(this)",
                'data-valor-vlrjuros' => number_format($sfdeducao_current->vlrjuros, 2, ',', '.')
            ],
        ];

        $arr_field_deducao_id['sfitemrecolhedor' . $sfdeducao_current->id . $current_recolhedor->id] = [
            'name' => 'sfitemrecolhedor' . $sfdeducao_current->id . $current_recolhedor->id,
            'value' => $current_recolhedor->id,
            'type' => 'hidden',
            'label_show' => false,
            'attr' => [
                'id' => 'sfitemrecolhedor' . $sfdeducao_current->id . $current_recolhedor->id,
                'name' => "sfitemrecolhedor[$current_recolhedor->id]"
            ]
        ];
        return $arr_field_deducao_id;
    }

    /**
     * @param $sfdeducao_current
     * @param $relItemDed
     * @param SfPco $sfpco
     * @param array $arr_field_deducao_id
     * @param SfPcoItem $sfPcoItem
     * @return array
     */
    public function camposRelItemDeducao($sfdeducao_current, $relItemDed, SfPco $sfpco, array $arr_field_deducao_id, SfPcoItem $sfPcoItem): array
    {
        $arr_field_deducao_id['sfitemrelacionamentocodsit' . $sfdeducao_current->id . $relItemDed->id] = [
            'name' => 'sfitemrelacionamentocodsit' . $sfdeducao_current->id . $relItemDed->id,
            'value' => $sfpco->codsit,
            'type' => 'text',
            'label_show' => false,
            'attr' => [
                'id' => 'sfitemrelacionamentocodsit' . $sfdeducao_current->id . $relItemDed->id,
                'name' => "sfitemrelacionamentocodsit[$relItemDed->id]",
                'readonly' => true
            ]
        ];

        $arr_field_deducao_id['sfitemrelacionamentonumempe' . $sfdeducao_current->id . $relItemDed->id] = [
            'name' => 'sfitemrelacionamentonumempe' . $sfdeducao_current->id . $relItemDed->id,
            'value' => $sfPcoItem->numempe,
            'type' => 'text',
            'label_show' => false,
            'attr' => [
                'id' => 'sfitemrelacionamentonumempe' . $sfdeducao_current->id . $relItemDed->id,
                'name' => "sfitemrelacionamentonumempe[$relItemDed->id]",
                'readonly' => true
            ]
        ];

        $arr_field_deducao_id['sfitemrelacionamentosubelemento' . $sfdeducao_current->id . $relItemDed->id] = [
            'name' => 'sfitemrelacionamentosubelemento' . $sfdeducao_current->id . $relItemDed->id,
            'value' => $sfPcoItem->subelemento->codigo,
            'type' => 'text',
            'label_show' => false,
            'attr' => [
                'id' => 'sfitemrelacionamentosubelemento' . $sfdeducao_current->id . $relItemDed->id,
                'name' => "sfitemrelacionamentosubelemento[$relItemDed->id]",
                'readonly' => true
            ]
        ];

        $arr_field_deducao_id['sfitemrelacionamentovlr' . $sfdeducao_current->id . $relItemDed->id] = [
            'name' => 'sfitemrelacionamentovlr' . $sfdeducao_current->id . $relItemDed->id,
            'value' => $sfPcoItem->vlr ? number_format($sfPcoItem->vlr, 2, ',', '.') : '',
            'type' => 'text',
            'label_show' => false,
            'attr' => [
                'id' => 'sfitemrelacionamentovlr' . $sfdeducao_current->id . $relItemDed->id,
                'name' => "sfitemrelacionamentovlr[$relItemDed->id]",
                'readonly' => true
            ]
        ];
        return $arr_field_deducao_id;
    }

    /**
     * @param $sfdeducao_current
     * @param array $arr_field_deducao_id
     * @param $optionsUgPagamento
     * @param array $arrayOptionsSituacao
     * @return array
     */
    public function camposDeducao($sfdeducao_current, array $arr_field_deducao_id, $optionsUgPagamento, array $arrayOptionsSituacao): array
    {
        $arr_field_deducao_id['sfdeducaodtpgtoreceb' . $sfdeducao_current->id] = [
            'name' => 'sfdeducaodtpgtoreceb' . $sfdeducao_current->id,
            'value' => $sfdeducao_current->dtpgtoreceb,
            'type' => 'date',
            'rules' => 'required',
            'label_show' => false,
            'attr' => [
                'id' => 'sfdeducaodtpgtoreceb' . $sfdeducao_current->id,
                'name' => 'sfdeducaodtpgtoreceb[]',
            ]
        ];

        $arr_field_deducao_id['sfdeducaodtvenc' . $sfdeducao_current->id] = [
            'name' => 'sfdeducaodtvenc' . $sfdeducao_current->id,
            'value' => $sfdeducao_current->dtvenc,
            'type' => 'date',
            'rules' => 'required',
            'label_show' => false,
            'attr' => [
                'id' => 'sfdeducaodtvenc' . $sfdeducao_current->id,
                'name' => 'sfdeducaodtvenc[]',
            ]
        ];

        $arr_field_deducao_id['sfdeducao' . $sfdeducao_current->id] = [
            'name' => 'sfdeducao' . $sfdeducao_current->id,
            'value' => $sfdeducao_current->id,
            'type' => 'hidden',
            'label_show' => false,
            'attr' => [
                'id' => 'sfdeducao' . $sfdeducao_current->id,
                'name' => 'sfdeducao[]',
                'class' => 'sfdeducaoApropriacao'
            ]
        ];

        $arr_field_deducao_id['sfdeducaoconfirma_dados' . $sfdeducao_current->id] = [
            'name' => 'sfdeducaoconfirma_dados' . $sfdeducao_current->id,
            'value' => $sfdeducao_current->confirma_dados ? 'true' : 'false',
            'type' => 'hidden',
            'label_show' => false,
            'attr' => [
                'id' => 'sfdeducaoconfirma_dados' . $sfdeducao_current->id,
                'name' => 'sfdeducaoconfirma_dados[]',
                'class' => 'sfdeducaoApropriacaoConfirmaDados'
            ]
        ];

        $arr_field_deducao_id['sfdeducaocodugpgto' . $sfdeducao_current->id] = [
            'name' => 'sfdeducaocodugpgto' . $sfdeducao_current->id,
            'value' => $sfdeducao_current->codugpgto !== 0 ? $sfdeducao_current->codugpgto : '',
            'type' => 'text',
            'label_show' => false,
            'required' => true,
            'attr' => [
                'id' => 'sfdeducaocodugpgto' . $sfdeducao_current->id,
                'name' => 'sfdeducaocodugpgto[]',
                'onchange' => "alteraUnidadePagamentoDeducao(this, $sfdeducao_current->id);"
            ]
        ];

        $arr_field_deducao_id['sfdeducaocodsit' . $sfdeducao_current->id] = [
            'name' => 'sfdeducaocodsit' . $sfdeducao_current->id,
            'selected' => $sfdeducao_current->codsit,
            'type' => 'select',
            'label_show' => false,
            'required' => true,
            'default' => 'Selecione',
            'choices' => $arrayOptionsSituacao ?? [],
            'attr' => [
                'id' => 'sfdeducaocodsit' . $sfdeducao_current->id,
                'name' => 'sfdeducaocodsit[]',
                'class' => 'codugpgtodeducao',
                'data-nome-aba' => 'aba-deducao' . $sfdeducao_current->id,
                'data-nome-aba-principal' => 'deducao-tab',
                'onchange' => "bindCampoSituacaoNaAba(this);
                                            alteraDescricaoSituacaoDeducao(this,$sfdeducao_current->id);"
            ]
        ];

        $arr_field_deducao_id['sfdeducaovlr' . $sfdeducao_current->id] = [
            'name' => 'sfdeducaovlr' . $sfdeducao_current->id,
            'value' => $sfdeducao_current->vlr ? number_format($sfdeducao_current->vlr, 2, ',', '.') : '',
            'type' => 'text',
            'label_show' => false,
            'required' => true,
            'attr' => [
                'id' => 'sfdeducaovlr' . $sfdeducao_current->id,
                'name' => 'sfdeducaovlr[]',
                'onchange' => "atualizarMaskMoney(this)",
                'data-valor-deducao' => number_format($sfdeducao_current->vlr, 2, ',', '.')
            ]
        ];
        return $arr_field_deducao_id;
    }

    public function sfpcoAfetaCustoNaoId($sf_padrao_id)
    {
        $afetaCustoNao = SfPco::join('execsfsituacao as es', 'sfpco.codsit', '=', 'es.codigo')
            ->where('sfpadrao_id', $sf_padrao_id)
            ->where('es.afeta_custo', true)
            ->select('sfpco.id')
            ->get()->pluck('id');

        if ($afetaCustoNao->count() > 0) {
            return $afetaCustoNao;
        }
        return false;
    }
}
