<?php
/**
 * Controller com métodos e funções da Apropriação da Fatura
 *
 * @author Anderson Sathler M. Ribeiro <asathler@gmail.com>
 */

namespace App\Http\Controllers\Apropriacao;

use App\Http\Controllers\Folha\Apropriacao\BaseController;
use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\Formatador;
use App\Models\ApropriacaoContratoFaturas;
use App\Models\ApropriacaoFaturas;
use App\Models\Contrato;
use App\Models\Contratofatura;
use App\Models\ContratoFaturaEmpenho;
use App\Models\ContratoFaturaEmpenhosApropriacao;
use App\Models\ContratoFaturaMesAno;
use App\Models\SfAcrescimo;
use App\Models\SfCentroCusto;
use App\Models\SfCompensacao;
use App\Models\SfCredito;
use App\Models\SfCronBaixaPatrimonial;
use App\Models\SfDadosBasicos;
use App\Models\SfDadosPgto;
use App\Models\SfDeducao;
use App\Models\SfDespesaAnular;
use App\Models\SfDespesaAnularItem;
use App\Models\SfDocContabilizacao;
use App\Models\SfDocOrigem;
use App\Models\SfDocRelacionado;
use App\Models\SfEncargos;
use App\Models\SfItemRecolhimento;
use App\Models\SfOutrosLanc;
use App\Models\SfPadrao;
use App\Models\SfParcela;
use App\Models\SfPco;
use App\Models\SfPcoItem;
use App\Models\SfPredoc;
use App\Models\SfPso;
use App\Models\SfPsoItem;
use App\Models\SfRelCredito;
use App\Models\SfRelDeducaoItem;
use App\Models\SfRelEncargoItem;
use App\Models\SfRelEncargos;
use App\Models\Sfrelitemvlrcc;
use App\Models\SfRelPcoItem;
use App\Models\SfRelPsoItem;
use App\Models\SfTramite;
use App\services\STA\STAOrdemBancariaService;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;

/**
 * Disponibiliza as funcionalidades básicas para controllers
 *
 * @category Conta
 * @package Conta_Folha_Apropriacao_Fatura
 * @author Anderson Sathler M. Ribeiro <asathler@gmail.com>
 * @copyright AGU - Advocacia Geral da União ©2018 <http://www.agu.gov.br>
 * @license GNU General Public License v2.0. <https://choosealicense.com/licenses/gpl-2.0/>
 */
class FaturaController extends BaseController
{
    use Formatador, BuscaCodigoItens;

    private $htmlBuilder = '';
    private $apropriacaoId = null;
    private $apropriacaoValor = 0;
    private $apropriacaoVencimento = '';
    private $apropriacaoAteste = '';
    private $apropriacaoProcesso = '';
    private $apropriacaoCodCredorDev = '';
    private $apropriacaoDataPgRec = '';
    private $apropriacaoNumDocOrigem = '';
    private $apropriacaoDesAnularTotal = 0;
    private $apropriacaoAcrescimo = 0;
    private $apropriacaoTotalDeducao = 0;
    private $apropriacaoTotalCentroCusto = 0;
    private $empenhosFaturas = null;
    private $contrato = null;
    private $fatura = null;
    private $pcoId = null;

    private $msgErroFaturaDoContrato = 'Fatura não pertence ao contrato informado.';
    private $msgErroFaturaEmApropriacao = 'Fatura já apropriada.';
    private $msgErroFaturasEmApropriacao = 'Fatura já apropriada.';
    private $msgErroFaturasEmAndamento = 'Fatura possui apropriação em andamento.';
    private $msgErroFaturaInexistente = 'Nenhuma fatura válida foi encontrada para apropriação.';
    private $msgErroAtesteAnteriorEmissao = 'Data de Ateste é anterior à data de Emissão.';
    private $msgErroEmpenho = 'Uma ou mais faturas não possui empenho vinculado.';
    private $msgErroUnidadeEmpenho = 'Uma ou mais faturas com unidade de empenho diferente.';
    private $msgErroFaturasMesmoContrato = 'As faturas selecionadas devem pertencer ao mesmo contrato.';
    private $msgErroContratoNaoTipoEmpenho = 'Selecione apenas faturas de um mesmo contrato ou contrato do tipo empenho de um mesmo fornecedor.';
    private $msgErroFaturasPossuiSiafiErro = 'Fatura possui apropriação com siafi erro. Exclua ou edite a apropriação existente para continuar.';

    /**
     * Método construtor
     *
     * @param Builder $htmlBuilder
     * @author Anderson Sathler M. Ribeiro <asathler@gmail.com>
     */
    public function __construct(Builder $htmlBuilder)
    {
        backpack_auth()->check();
        $this->htmlBuilder = $htmlBuilder;
    }

    /**
     * Apresenta o grid com a listagem das apropriações da fatura
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     * @throws \Exception
     * @author Anderson Sathler <asathler@gmail.com>
     */
    public function index(Request $request)
    {
        $dados = ApropriacaoFaturas::retornaDadosListagem()->get();

        if ($request->ajax()) {
            return DataTables::of($dados)->addColumn('action', function ($registro) {
                return $this->retornaAcoes($registro);
            })
                ->editColumn('ateste', '{!!
                    !is_null($ateste) ? date_format(date_create($ateste), "d/m/Y") : ""
                !!}')
                ->editColumn('vencimento', '{!!
                    !is_null($vencimento) ? date_format(date_create($vencimento), "d/m/Y") : ""
                !!}')
                ->editColumn('total', '{!! number_format(floatval($total), 2, ",", ".") !!}')
                ->editColumn('valor', '{!! number_format(floatval($valor), 2, ",", ".") !!}')
                ->setRowId('registro_id')
                ->make(true);
        }

        $html = $this->retornaHtmlGrid();
        return view('backpack::mod.apropriacao.fatura', compact('html'));
    }

    /**
     * Método para criação de registro de apropriação com única fatura
     *
     * @param Contrato $contrato
     * @param Contratofatura $fatura
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @author Anderson Sathler <asathler@gmail.com>
     */
    public function create(Contrato $contrato, Contratofatura $fatura)
    {

        $faturaIds = (array)$fatura->id;
        $padraoId = request()->idPadrao;

        $this->apropriacaoVencimento = Contratofatura::whereIn('id', $faturaIds)->max('vencimento');

        $this->setarEmpenhoDasFaturas($faturaIds);

        $arrayValidacao = $this->validarAntesDeIniciarApropriacao($faturaIds);

        if ($arrayValidacao['status'] === 'error') {
            return response()->json($arrayValidacao);
        }

        DB::beginTransaction();
        try {
            $idPadraoDuplicado = $this->executaApropriacaoFaturas($faturaIds, $padraoId);

            $this->atualizarSituacaoEPadraoFaturas($faturaIds, $idPadraoDuplicado);

            $this->adicionaPadraoNaSessao($idPadraoDuplicado, true);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
        }

        return response()->json(
            [
                'result' => 'success',
                'message' => 'Fatura(s) incluída(s) na apropriação',
                'idPadraoDuplicado' => $idPadraoDuplicado,
                'idApropriacao' => $this->apropriacaoId
            ]
        );
    }

    /**
     * Método para criação de registro de apropriação com uma ou mais faturas
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @author Anderson Sathler <asathler@gmail.com>
     */
    public function createMany()
    {

        $retorno['tipo'] = 'warning';
        $retorno['mensagem'] = '';

        $faturaIds = request()->entries;
        $padraoId = request()->idPadrao;

        $this->apropriacaoVencimento = Contratofatura::whereIn('id', $faturaIds)->max('vencimento');
        $this->setarEmpenhoDasFaturas($faturaIds);

        $arrayValidacao = $this->validarAntesDeIniciarApropriacao($faturaIds);

        if ($arrayValidacao['status'] === 'error') {
            return response()->json($arrayValidacao);
        }

        DB::beginTransaction();
        try {
            $idPadraoDuplicado = $this->executaApropriacaoFaturas($faturaIds, $padraoId);

            $this->atualizarSituacaoEPadraoFaturas($faturaIds, $idPadraoDuplicado);

            $this->adicionaPadraoNaSessao($idPadraoDuplicado, true);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
        }

        $retorno['tipo'] = 'success';

        return response()->json(
            [
                'result' => 'success',
                'message' => 'Fatura(s) incluída(s) na apropriação',
                'idPadraoDuplicado' => $idPadraoDuplicado,
                'idApropriacao' => $this->apropriacaoId
            ]
        );
    }

    /**
     * Método para criação de registro de apropriação com uma ou mais faturas
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @author Anderson Sathler <asathler@gmail.com>
     */
    public function createComNovoPadrao(Request $request)
    {

        $faturaIds = request()->is('api/*') ? (array) request()->id_inst_cobranca : (array) request()->entries;

        $this->apropriacaoVencimento = Contratofatura::whereIn('id', $faturaIds)->max('vencimento');

        $this->setarEmpenhoDasFaturas($faturaIds);

        $arrayValidacao = $this->validarAntesDeIniciarApropriacao($faturaIds);

        if ($arrayValidacao['status'] === 'error') {
            return response()->json($arrayValidacao);
        }

        DB::beginTransaction();
        try {

            $padraoId = $this->criarSfPadrao($request);

            $this->gerarApropriacaoFaturas($faturaIds, $padraoId);

            $this->criarRegistroTabelasSf($padraoId, $faturaIds, $request);

            $this->atualizarSituacaoEPadraoFaturas($faturaIds, $padraoId);

            $this->adicionaPadraoNaSessao($padraoId, false);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
        }

        return response()->json(
            [
                'result' => 'success',
                'message' => 'Fatura(s) incluída(s) na apropriação',
                'padraoId' => $padraoId,
                'idApropriacao' => $this->apropriacaoId
            ]
        );
    }

    public function validarAntesDeIniciarApropriacao($faturaIds)
    {
        if (!$this->validaContratoFaturas($faturaIds)) {

            $contratosTipoEmpenhos = $this->validaContratoEmpenhoMesmoFornecedorFaturas($faturaIds);

            if (!$contratosTipoEmpenhos) {
                return [
                    'status'    => 'error',
                    'message'   => $this->msgErroContratoNaoTipoEmpenho
                ];
            }
        }

        if (!$this->validaDataAteste($faturaIds)) {
            return [
                'status'    => 'error',
                'message'   => $this->msgErroAtesteAnteriorEmissao
            ];
        }

        if ($this->validarFaturaApropriada($faturaIds)) {
            return [
                'status'    => 'error',
                'message'   => $this->msgErroFaturasEmApropriacao
            ];
        }

        if ($this->validarFaturaPossuiSiafiErro($faturaIds)) {
            return [
                'status'    => 'error',
                'message'   => $this->msgErroFaturasPossuiSiafiErro
            ];
        }

        if ($this->validarFaturaEmAndamento($faturaIds)) {
            return [
                'status'    => 'error',
                'message'   => $this->msgErroFaturasEmAndamento
            ];
        }

        if ($this->validaExistenciaFaturas($faturaIds)) {
            return [
                'status'    => 'error',
                'message'   => $this->msgErroFaturaInexistente
            ];
        }

        return ['status' => 'success', 'message' => 'success'];
    }

    /**
     * Exclui a apropriação da fatura informada
     *
     * @param ApropriacaoFaturas $apropriacaoFatura
     * @return \Illuminate\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     * @author Anderson Sathler <asathler@gmail.com>
     */
    public function destroy(ApropriacaoFaturas $apropriacaoFatura)
    {
        $msg = config('mensagens.apropriacao-exclusao-alerta');
        $status = 'Alerta';

        if ($apropriacaoFatura->fase_id != $this->retornaIdCodigoItem('Status Processo', 'Processo finalizado')) {
            $apropriacaoFatura->delete();

            $msg = config('mensagens.apropriacao-exclusao');
            $status = 'Sucesso';
        }

        $this->exibeMensagem($msg, $status);

        return redirect('/apropriacao/fatura')->withInput();
    }

    /**
     * Exibe relatório da apropriação
     *
     * @param $id
     * @author Anderson Sathler M. Ribeiro <asathler@gmail.com>
     */
    public function show($id)
    {
        $modelApropriacao = new ApropriacaoFaturas();
        $identificacao = $modelApropriacao->retornaDadosIdentificacao($id)->first()->toArray();

        $modelPco = new SfPco();
        $pcos = $modelPco->retornaPcosProApropriacaoDaFatura($id)->get()->toArray();

        return view('backpack::mod.apropriacao.relatorio', compact('identificacao', 'pcos'));
    }

    /**
     * Valida se fatura pertence ao contrato informado
     *
     * @param integer $faturaContratoId
     * @return bool
     * @author Anderson Sathler M. Ribeiro <asathler@gmail.com>
     */
    protected function validaFaturaDeDiferentesContratos($faturaContratoId)
    {
        return Contratofatura::whereIn('id', $faturaContratoId)->pluck('contrato_id')->unique()->count() !== 1;
    }

    /**
     * Valida se alguma fatura já está apropriada ENV (Siafi Apropriado)
     *
     * @param array $faturaIds
     * @return bool
     */
    protected function validarFaturaApropriada($faturaIds)
    {
        $contratoFaturasApropriadas = Contratofatura::where('situacao', 'APR')
            ->whereIn('id', $faturaIds)
            ->get();
        return $contratoFaturasApropriadas->count() > 0;
    }
    /**
     * Valida se alguma fatura está com siafi erro.
     *
     * @param array $faturaIds
     * @return bool
     */
    protected function validarFaturaPossuiSiafiErro($faturaIds)
    {
        $contratoFaturasApropriadas = Contratofatura::where('situacao', 'ERR')
            ->whereIn('id', $faturaIds)
            ->get();
        return $contratoFaturasApropriadas->count() > 0;
    }

    /**
     * Valida se alguma fatura já está em Andamento
     *
     * @param array $faturaIds
     * @return bool
     */
    protected function validarFaturaEmAndamento($faturaIds)
    {
        $contratoFaturasApropriadas = Contratofatura::where('situacao', 'AND')
            ->whereIn('id', $faturaIds)
            ->get();
        return $contratoFaturasApropriadas->count() > 0;
    }

    /**
     * Valida se fatura existe
     *
     * @param array $faturaIds
     * @return bool
     * @author Anderson Sathler M. Ribeiro <asathler@gmail.com>
     */
    protected function validaExistenciaFaturas($faturaIds)
    {
        return Contratofatura::whereIn('id', $faturaIds)->doesntExist();
    }

    /**
     * Valida se data de ateste é anterior à data de emissão
     *
     * @param array $faturaIds
     * @return bool
     * @author Franklin Justiniano <franklin.linux@gmail.com>
     */
    protected function validaDataAteste($faturaIds)
    {
        $this->apropriacaoAteste = Contratofatura::whereIn('id', $faturaIds)->max('ateste');
        $hoje = date('Y-m-d');
        $data_ateste = Carbon::createFromFormat('Y-m-d', $this->apropriacaoAteste)->format('Y-m-d');
        if ($data_ateste > $hoje) {
            return false;
        }

        return true;
    }

    /**
     * seta o Empenho das faturas selecionadas para apropriação
     *
     * @param array $faturaIds
     */
    protected function setarEmpenhoDasFaturas($faturaIds)
    {

        $this->empenhosFaturas = [];

        try {
            $empenhos = ContratoFaturaEmpenho::join(
                'contratofaturas',
                'contratofaturas.id',
                '=',
                'contratofatura_empenhos.contratofatura_id'
            )
                ->join('empenhos', 'contratofatura_empenhos.empenho_id', '=', 'empenhos.id')
                ->leftJoin('naturezasubitem', 'naturezasubitem.id', '=', 'contratofatura_empenhos.subelemento_id')
                ->select(
                    'contratofatura_empenhos.contratofatura_id as contratofatura_id',
                    'empenhos.numero',
                    'empenhos.id as empenho_id',
                    'contratofatura_empenhos.subelemento_id',
                    'naturezasubitem.codigo',
                    DB::raw('sum(DISTINCT contratofatura_empenhos.valorref) as valorreftotal'))
                ->whereIn('contratofaturas.id', $faturaIds)
                ->groupBy('empenhos.numero', 'empenhos.id', 'contratofatura_empenhos.subelemento_id', 'contratofatura_empenhos.contratofatura_id', 'naturezasubitem.codigo')
                ->get();

            if ($empenhos->count() > 0) {
                $this->empenhosFaturas = $empenhos;
            }

        }catch (\Exception $e) {
            #@TODO inserir log de apropriacao aqui
        }
    }

    protected function validaUnidadeEmpenhos($faturaIds)
    {
    }

    /**
     * Valida se as faturas são do mesmo contrato.
     *
     * @param array $faturaIds
     * @return bool
     * @author Franklin Justiniano <franklin.linux@gmail.com>
     */
    protected function validaContratoFaturas($faturaIds)
    {
        $qtd_contratos = Contratofatura::whereIn('id', $faturaIds)
            ->distinct()
            ->count('contrato_id');
        if ($qtd_contratos == 1) {
            return true;
        }
        return false;
    }

    /**
     * Valida se as faturas são de contratos tipo empenho e do mesmo fornecedor.
     *
     * @param array $faturaIds
     * @return bool
     */
    protected function validaContratoEmpenhoMesmoFornecedorFaturas($faturaIds)
    {
        $qtd_contratos = Contratofatura::whereIn('id', $faturaIds)
            ->with(['contrato' => function ($query) {
                $query->select('id', 'tipo_id', 'fornecedor_id');
            }])
            ->get()
            ->map(function ($contratoFatura) {
                return [
                    'tipo' => $this->retornaDescCodigoItem($contratoFatura->contrato->tipo_id),
                    'fornecedor' => $contratoFatura->contrato->fornecedor_id
                ];
            })
            ->toArray();

        $todosEmpenho = array_reduce($qtd_contratos, function ($acc, $item) {
            return $acc && $item['tipo'] === 'Empenho';
        }, true);

        $fornecedores = array_unique(array_column($qtd_contratos, 'fornecedor'));

        if ($todosEmpenho && count($fornecedores) === 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Valida se as faturas são do mesmo contrato.
     *
     * @param array $faturaIds
     * @return bool
     * @author Franklin Justiniano <franklin.linux@gmail.com>
     */
    protected function montaTxtObser($faturaIds)
    {
        $texto = "APROPRIAÇÃO DA FATURA " . $this . $this->apropriacaoNumDocOrigem . " ";
        $texto .= "DO CONTRATO " . $this->contrato->numero . ", ";
        $texto .= "PROCESSO " . $this->contrato->processo . ", ";
        $texto .= "MÊS DE REFERÊNCIA " . $this->fatura->mesref . "/" . $this->fatura->anoref;

        return $texto;
    }

    /**
     * Cria registro da apropriação
     *
     * @param array $faturaIds
     * @return bool
     * @author Anderson Sathler M. Ribeiro <asathler@gmail.com>
     */
    protected function gerarApropriacaoFaturas($faturaIds, $sfPadraoId)
    {
        $this->fatura = Contratofatura::whereIn('id', $faturaIds)
            ->having('vencimento', '=', $this->apropriacaoVencimento)
            ->groupBy('id')
            ->first();

        #943 passando a pegar valorfaturado ao inves de valorliquido
        $valorTotal = Contratofatura::whereIn('id', $faturaIds)->sum('valorfaturado');

        $this->apropriacaoCodCredorDev = $this->fatura->getCodigoFornecedor();
        $this->apropriacaoNumDocOrigem = $this->fatura->numero;
        $this->contrato = $this->fatura->contrato;

        $apropriacaoId = ApropriacaoFaturas::create([
            'valor' => $valorTotal,
            'fase_id' => $this->retornaIdCodigoItem(
                'Status Apropriação Instrumento de Cobrança SIAFI',
                'Apropriação em Andamento'
            )
        ])->id;

        foreach ($faturaIds as $faturaId) {
            ApropriacaoContratoFaturas::create([
                'apropriacoes_faturas_id' => $apropriacaoId,
                'contratofaturas_id' => $faturaId,
                'sfpadrao_id' => $sfPadraoId
            ]);
        }

        $this->apropriacaoId = $apropriacaoId;
        $this->apropriacaoValor = $valorTotal;
    }

    /**
     * Se houver documento hábil padrão para fatura a apropriar, duplica dados SfPadrao (e dependências)
     *
     * @todo: Mudar os métodos para duplicação de Sf... para suas respectivas App\Models\Sf...
     * @author Anderson Sathler M. Ribeiro <asathler@gmail.com>
     */
    protected function executaApropriacaoFaturas($faturaIds, $padraoId)
    {

        $sfPadraoOriginal = SfPadrao::find($padraoId);

        if ($sfPadraoOriginal) {
            $sfPadraoIdDuplicado_id = $this->duplicaSfPadrao($sfPadraoOriginal);

            $this->gerarApropriacaoFaturas($faturaIds, $sfPadraoIdDuplicado_id);

            $this->duplicaSfDadosBasicos($sfPadraoOriginal->id, $sfPadraoIdDuplicado_id, $faturaIds);
            $this->duplicaSfPco($sfPadraoOriginal->id, $sfPadraoIdDuplicado_id, $faturaIds);
            $this->duplicaSfPso($sfPadraoOriginal->id, $sfPadraoIdDuplicado_id);
            $this->duplicaSfCredito($sfPadraoOriginal->id, $sfPadraoIdDuplicado_id);
            $this->duplicaSfOutrosLanc($sfPadraoOriginal->id, $sfPadraoIdDuplicado_id);
            $this->duplicaSfDeducao($sfPadraoOriginal->id, $sfPadraoIdDuplicado_id);
            $this->duplicaSfCentroCusto($sfPadraoOriginal->id, $sfPadraoIdDuplicado_id, $faturaIds);
            $this->duplicaSfDadosPgto($sfPadraoOriginal->id, $sfPadraoIdDuplicado_id);
            $this->duplicaSfDocContabilizacao($sfPadraoOriginal->id, $sfPadraoIdDuplicado_id);

            return $sfPadraoIdDuplicado_id;
        }
    }

    public function criarRegistroTabelasSf($padraoId, $faturaIds, $request)
    {

        $this->criarSfDadosBasicos($padraoId, $faturaIds, $request);

        $this->criarSfPco($padraoId, $request);

        $this->criarSfDadosPgto($padraoId, $request);

        $this->criarSfCentroCusto($padraoId, $request);
    }

    public function criarSfDadosBasicos($padraoId, $faturaIds, $request)
    {
        $dadosPreparados = $this->prepararDadosBasicos($padraoId, $faturaIds, $request);

        $sfDadosBasicos = new SfDadosBasicos();
        $sfDadosBasicos->fill((array) $dadosPreparados);
        $sfDadosBasicos->save();

        $this->criarSfDocOrigem($sfDadosBasicos->id, $faturaIds);
    }

    public function prepararDadosBasicos($padraoId, $faturaIds, $request)
    {
        $novoSfDadosBasicos = new \stdClass();
        $novoSfDadosBasicos->sfpadrao_id = $padraoId;

        if ($request->is('api/*')) {

            $novoSfDadosBasicos->dtemis = $request->data_emissao_contabil;
            $novoSfDadosBasicos->dtvenc = $request->data_vencimento;
            $novoSfDadosBasicos->vlrtaxacambio = $request->taxa_cambio;
            $novoSfDadosBasicos->codugpgto = $request->cod_ug_emitente;
            $novoSfDadosBasicos->txtprocesso = $request->processo;
            $novoSfDadosBasicos->txtobser = $request->observacao;
            $novoSfDadosBasicos->dtateste = $request->data_ateste;
            $novoSfDadosBasicos->txtinfoadic = $request->informacoes_adicionais;
            $novoSfDadosBasicos->vlr =  $this->apropriacaoValor;
            $novoSfDadosBasicos->codcredordevedor = $this->retornaSomenteNumeros($this->apropriacaoCodCredorDev);
            $novoSfDadosBasicos->dtpgtoreceb = $request->data_pagamento;
        } else {

            $novoSfDadosBasicos->dtemis = now();
            $novoSfDadosBasicos->dtvenc = $this->apropriacaoVencimento;
            $novoSfDadosBasicos->codugpgto = session()->get('user_ug');
            $novoSfDadosBasicos->vlr = $this->apropriacaoValor;
            $novoSfDadosBasicos->txtprocesso = $this->fatura->processo;
            $txtobser = $this->gerarTxtObserPadrao(
                $faturaIds,
                $this->fatura->getRazaoSocialComCodigoFornecedor()
            );
            $novoSfDadosBasicos->txtobser = $this->formatarStringTxtObserPeloTamanho($novoSfDadosBasicos, $txtobser);
            $novoSfDadosBasicos->dtateste = $this->apropriacaoAteste;
            switch ($this->retornaTipoFornecedor($this->apropriacaoCodCredorDev)) {
                case 'FISICA':
                case 'JURIDICA':
                    $novoSfDadosBasicos->codcredordevedor = $this->retornaSomenteNumeros($this->apropriacaoCodCredorDev);
                    break;
                default:
                    $novoSfDadosBasicos->codcredordevedor = $this->apropriacaoCodCredorDev;
                    break;
            }
            $novoSfDadosBasicos->dtpgtoreceb = $this->apropriacaoVencimento;
        }

        return $novoSfDadosBasicos;
    }

    public function formatarStringTxtObserPeloTamanho($novoSfDadosBasicos, $txtobser)
    {
        if (strlen($txtobser) > 468) {
            $novoSfDadosBasicos->txtinfoadic = $txtobser;
            // Se for maior que 468 caracteres, remova os 3 últimos caracteres e adicione reticências
            return mb_substr($txtobser, 0, 465, 'UTF-8') . '...'; // 468 - 3 = 465
        }
        return $txtobser;
    }

    public function criarSfPco($padraoId, $request, $contrato_id = null)
    {
        $numseqpco = 1;

        if (isset($request) && method_exists($request, 'is') && $request->is('api/*')) {
            foreach ($request['sf_pco'] as $pco) {

                $dadosPreparados = $this->prepararSfPco($padraoId, $numseqpco, $pco, $request);

                $novoSfPco = new SfPco();
                $novoSfPco->fill((array)$dadosPreparados);
                $novoSfPco->save();

                $this->criarSfPcoItem($novoSfPco->id, $pco['sf_pco_item'], $request);
                $numseqpco++;
            }
        } else {

            $indrtemcontrato = $this->setarValorIndrTemContrato($contrato_id, $padraoId);

            $novoSfPco = new SfPco();
            $novoSfPco->sfpadrao_id = $padraoId;
            $novoSfPco->codugempe = SfPadrao::where('id', $padraoId)->first()->codugemit;
            $novoSfPco->numseqitem = $numseqpco;
            $novoSfPco->despesaantecipada = false;
            $novoSfPco->indrtemcontrato = $indrtemcontrato;
            $novoSfPco->push();

            $this->criarSfPcoItem($novoSfPco->id, null, $request);
        }


        return $novoSfPco->id;
    }

    public function criarSfDeducao($padraoId, $numseqitem)
    {
        $novoSfDeducao = new SfDeducao();
        $novoSfDeducao->sfpadrao_id = $padraoId;
        $novoSfDeducao->codugpgto = SfPadrao::where('id', $padraoId)->first()->codugemit;
        $novoSfDeducao->numseqitem = $numseqitem != null ? $numseqitem : 1;
        $novoSfDeducao->indrliquidado = false;
        $novoSfDeducao->push();

        return $novoSfDeducao->id;
    }

    public function setarValorIndrTemContrato($contrato_id, $padraoId = null)
    {
        $sfPco = SfPco::where('sfpadrao_id', $padraoId)
            ->where('numseqitem', 1)->first();
        if ($padraoId && $sfPco) {
            return $sfPco->indrtemcontrato;
        }
        
        $contrato = $contrato_id ? Contrato::find($contrato_id) : $this->contrato;
        $idTipoEmpenho = $this->retornaIdCodigoItem('Tipo de Contrato', 'Empenho');

        return $contrato->tipo_id !== $idTipoEmpenho;
    }

    public function prepararSfPco($padraoId, $numseqitem, $pco, $request)
    {

        $prepararSfPco = new \stdClass();

        $prepararSfPco->sfpadrao_id = $padraoId;
        $prepararSfPco->numseqitem = $numseqitem;
        $prepararSfPco->codugempe =  $request->cod_ug_emitente;
        $prepararSfPco->codsit = $pco['cod_situacao'];
        $prepararSfPco->txtinscrd = $pco['txtinscrd'] ?? null;
        $prepararSfPco->numclassd = $pco['numclassd'] ?? null;
        $prepararSfPco->txtinscre = $pco['txtinscre'] ?? null;
        $prepararSfPco->numclasse = $pco['numclasse'] ?? null;
        $prepararSfPco->despesaantecipada = $pco['despesa_antecipada'];
        $prepararSfPco->indrtemcontrato = $pco['indicador_contrato'];

        return $prepararSfPco;
    }


    public function criarSfPcoItem($sfPcoId, $sfpcoItems, $request)
    {
        $numseqitem = 1;

        if (isset($request) && method_exists($request, 'is') && $request->is('api/*')) {
            foreach ($sfpcoItems as $item) {

                $dadosPreparados = $this->prepararSfPcoItem($sfPcoId, $numseqitem, $item, $request);

                $novoSfPcoItem = new SfPcoItem();
                $novoSfPcoItem->fill((array)$dadosPreparados);
                $novoSfPcoItem->save();

                $numseqitem++;
            }
        } else {
            if (isset($this->empenhosFaturas) && count($this->empenhosFaturas) && !$this->empenhosFaturas->isEmpty()) {
                foreach ($this->empenhosFaturas as $key => $empenhosFatura) {
                    $sfPcoItem = SfPcoItem::where('sfpco_id', $sfPcoId)
                        ->where('numempe', $empenhosFatura->numero)
                        ->where('subelemento_id', $empenhosFatura->subelemento_id)
                        ->first();

                    if ($sfPcoItem) {
                        $sfPcoItem->vlr += $empenhosFatura->valorreftotal ?? 0;
                        $sfPcoItem->save();
                    } else {
                        $novoSfPcoItem = new SfPcoItem();
                        $novoSfPcoItem->sfpco_id = $sfPcoId;
                        $novoSfPcoItem->numempe = $empenhosFatura->numero ?? '';
                        $novoSfPcoItem->vlr = $empenhosFatura->valorreftotal ?? 0;
                        $novoSfPcoItem->indrliquidado = true;
                        $novoSfPcoItem->codsubitemempe = $empenhosFatura->subelemento ? $empenhosFatura->subelemento->codigo : null;
                        $novoSfPcoItem->subelemento_id = $empenhosFatura->subelemento_id ?? null;
                        $novoSfPcoItem->numseqitem = $key + 1;
                        $novoSfPcoItem->contratofatura_id = $empenhosFatura->contratofatura_id ?? null;
                        $novoSfPcoItem->push();
                    }
                    $this->criarPopularContratoFaturaEmpenhosApropriacao($empenhosFatura, $novoSfPcoItem->id);
                }
            }
        }
    }

    public function criarPopularContratoFaturaEmpenhosApropriacao($empenhoFatura, $sfpcoitem_id)
    {
        $novoPopulaContratoEmpAprop = new ContratoFaturaEmpenhosApropriacao();
        $novoPopulaContratoEmpAprop->contratofatura_id = $empenhoFatura->contratofatura_id;
        $novoPopulaContratoEmpAprop->empenho_id = $empenhoFatura->empenho_id;
        $novoPopulaContratoEmpAprop->subelemento_id = $empenhoFatura->subelemento_id;
        $novoPopulaContratoEmpAprop->sfpcoitem_id = $sfpcoitem_id;
        $novoPopulaContratoEmpAprop->valorref = $empenhoFatura->valorreftotal;
        $novoPopulaContratoEmpAprop->push();
    }

    public function prepararSfPcoItem($sfPcoId, $numseqitem, $item, $request)
    {

        $prepararSfPcoItem = new \stdClass();
        $prepararSfPcoItem->sfpco_id = $sfPcoId;
        $prepararSfPcoItem->numseqitem = $numseqitem;
        $prepararSfPcoItem->codugempe = $request->cod_ug_emitente;
        $prepararSfPcoItem->numempe = $item['numero_empenho'];
        $prepararSfPcoItem->codsubitemempe = $item['subelemento'];
        $prepararSfPcoItem->indrliquidado = $item['indicador_liquidado'];
        $prepararSfPcoItem->vlr = $item['valor_item'];
        $prepararSfPcoItem->txtinscra =  $item['txtinscra'] ?? null;
        $prepararSfPcoItem->txtinscrb =  $item['txtinscrb'] ?? null;
        $prepararSfPcoItem->txtinscrc =  $item['txtinscrc'] ?? null;
        $prepararSfPcoItem->numclassa =  $item['numclassa'] ?? null;
        $prepararSfPcoItem->numclassb =  $item['numclassb'] ?? null;
        $prepararSfPcoItem->numclassc =  $item['numclassc'] ?? null;

        return  $prepararSfPcoItem;
    }

    public function criarSfPcoItemViaArray($arrPcoItem)
    {

        $novoSfPcoItem = new SfPcoItem();

        $novoSfPcoItem->sfpco_id =      $arrPcoItem['sfpco_id'];
        $novoSfPcoItem->numempe =       $arrPcoItem['numempe'];
        $novoSfPcoItem->vlr =           $arrPcoItem['vlr'];
        $novoSfPcoItem->numclassa =     (int) str_replace(".", '', $arrPcoItem['numclassa']);
        $novoSfPcoItem->numclassb =     (int) str_replace(".", '', $arrPcoItem['numclassb']);
        $novoSfPcoItem->indrliquidado = true;
        $novoSfPcoItem->numseqitem = $arrPcoItem['numseqitem'];
        //$novoSfPcoItem->contratofatura_id = $arrPcoItem['contratofatura_id'];
        $novoSfPcoItem->push();
    }


    public function criarSfDocOrigem($sfDadosBasicosId, $faturaIds)
    {
        foreach ($faturaIds as $key => $faturaId) {
            $faturaDoc = Contratofatura::where('id', $faturaIds[$key])->first();

            $novoSfDocOrigem = new SfDocOrigem();
            $novoSfDocOrigem->sfdadosbasicos_id = $sfDadosBasicosId;
            $novoSfDocOrigem->dtemis = $faturaDoc->emissao;
            $novoSfDocOrigem->vlr = $faturaDoc->valorfaturado;
            switch ($this->retornaTipoFornecedor($faturaDoc->getCodigoFornecedor())) {
                case 'FISICA':
                case 'JURIDICA':
                    $novoSfDocOrigem->codidentemit = $this->retornaSomenteNumeros($faturaDoc->getCodigoFornecedor());
                    break;
                default:
                    $novoSfDocOrigem->codidentemit = $faturaDoc->getCodigoFornecedor();
                    break;
            }
            $novoSfDocOrigem->numdocorigem = substr($faturaDoc->numero, 0, 1248);

            $novoSfDocOrigem->push();
        }
    }

    public function criarSfDadosPgto($padraoId)
    {
        if (!app()->runningInConsole()) {
            $novosfDadosPgto = new SfDadosPgto();
            $novosfDadosPgto->sfpadrao_id = $padraoId;
            switch ($this->retornaTipoFornecedor($this->fatura->getCodigoFornecedor())) {
                case 'FISICA':
                case 'JURIDICA':
                    $novosfDadosPgto->codcredordevedor = $this->retornaSomenteNumeros($this->fatura->getCodigoFornecedor());
                    break;
                default:
                    $novosfDadosPgto->codcredordevedor = $this->fatura->getCodigoFornecedor();
                    break;
            }
            $novosfDadosPgto->vlr = $this->apropriacaoValor;
            $novosfDadosPgto->push();

            return true;
        }
    }

    public function criarSfCentroCusto($padraoId, $request)
    {

        $numseqitem = 1;

        if ($request->is('api/*')) {
            if (isset($request['sf_centro_custo']) && is_array($request['sf_centro_custo'])) {
                foreach ($request['sf_centro_custo'] as $centrocusto) {

                    $dadosPreparados = $this->prepararSfCentroCusto($padraoId, $numseqitem, $centrocusto);

                    $novoSfCentrocusto = new SfCentroCusto();
                    $novoSfCentrocusto->fill((array) $dadosPreparados);
                    $novoSfCentrocusto->save();

                    $this->criarSfrelitemvlrcc($novoSfCentrocusto->id, null, $padraoId, $centrocusto['item_vlrcc'], $request);
                    $numseqitem++;
                }
            }
        } else {
            $arrMesAnoRef = ContratoFaturaMesAno::where('contratofaturas_id', $this->fatura->id)->get();
//
            if(count($arrMesAnoRef) == 1){
                $sfpco = SfPco::where('sfpadrao_id', $padraoId)->get()->pluck('id');
                $sfpcoitens = SfPcoItem::whereIn('sfpco_id', $sfpco)->get();
//
                if(count($sfpcoitens) >= 1){
//                    foreach ($sfpcoitens as $key => $item) {
//                        $novoSfCentrocusto = new SfCentroCusto();
//                        $novoSfCentrocusto->sfpadrao_id = $padraoId;
//                        $novoSfCentrocusto->mesreferencia = $arrMesAnoRef[0]->mesref;
//                        $novoSfCentrocusto->anoreferencia = $arrMesAnoRef[0]->anoref;
//                        $novoSfCentrocusto->numseqitem = $key + 1;
//                        $novoSfCentrocusto->push();
//
//                        $this->criarSfrelitemvlrcc($novoSfCentrocusto->id, $item->vlr, null, null, $request);
                    //}
                }
            }else{
                foreach ($arrMesAnoRef as $key => $mesAnoRef) {
                    $novoSfCentrocusto = new SfCentroCusto();
                    $novoSfCentrocusto->sfpadrao_id = $padraoId;
                    $novoSfCentrocusto->mesreferencia = $mesAnoRef->mesref;
                    $novoSfCentrocusto->anoreferencia = $mesAnoRef->anoref;
                    $novoSfCentrocusto->numseqitem = $key + 1;
                    $novoSfCentrocusto->push();

                    $this->criarSfrelitemvlrcc($novoSfCentrocusto->id, $mesAnoRef->valorref, null, null, $request);
                }
            }
        }
    }

    public function prepararSfCentroCusto($padraoId, $numseqitem, $centrocusto)
    {

        $prepararSfCc = new \stdClass();

        $prepararSfCc->sfpadrao_id = $padraoId;
        $prepararSfCc->numseqitem = $numseqitem;
        $prepararSfCc->codcentrocusto = $centrocusto['cod_centro_custo'] ?? null;
        $prepararSfCc->mesreferencia = $centrocusto['mes'] ?? null;
        $prepararSfCc->anoreferencia = $centrocusto['ano'] ?? null;
        $prepararSfCc->codugbenef = $centrocusto['ug_beneficiada'] ?? null;
        $prepararSfCc->codsiorg = $centrocusto['codigo_siorg'] ?? null;

        return $prepararSfCc;
    }

    /**
     * Metodo chamado pela funcao x para criar Centro de Custo para a tela de Apropriacao de Faturas
     *
     * @param $padraoId
     */
    public function criarSfCentroCustoViaAjax($arrNovoCentroCusto): void
    {

        $novosfCentroCusto = new SfCentroCusto();

        $novosfCentroCusto->sfpadrao_id = $arrNovoCentroCusto['sfpadrao_id'];
        $novosfCentroCusto->codcentrocusto = $arrNovoCentroCusto['codcentrocusto'];
        $novosfCentroCusto->codsiorg = $arrNovoCentroCusto['codsiorg'];
        $novosfCentroCusto->mesreferencia = $arrNovoCentroCusto['mesreferencia'];
        $novosfCentroCusto->anoreferencia = $arrNovoCentroCusto['anoreferencia'];
        $novosfCentroCusto->codugbenef = $arrNovoCentroCusto['codugbenef'];
        $novosfCentroCusto->numseqitem = $arrNovoCentroCusto['numseqitem'];

        $novosfCentroCusto->push();
    }

    public function criarSfrelitemvlrcc($idNovosfCentroCusto, $valor, $padraoId, $itemsVlrCC, $request)
    {

        $numseqitem = 1;

        if ($request->is('api/*')) {

            foreach ($itemsVlrCC as $itemVlrCC) {

                $modelPco = new SfPco();

                $numSeqPai = $modelPco->retornaNumSeqItemPorCodSitENumEmpe($itemVlrCC['cod_situacao'], $itemVlrCC['numero_empenho'], $padraoId);

                $dadosPreparados = $this->prepararSfRelItemVlrCC($idNovosfCentroCusto, $itemVlrCC, $numseqitem, $numSeqPai);

                $novoSfrelitemvlrcc = new Sfrelitemvlrcc();
                $novoSfrelitemvlrcc->fill((array) $dadosPreparados);
                $novoSfrelitemvlrcc->save();

                $numseqitem++;
            }
        } else {
            $sfRelitemvlrccDuplicado = new Sfrelitemvlrcc();
            $sfRelitemvlrccDuplicado->sfcc_id = $idNovosfCentroCusto;
            $sfRelitemvlrccDuplicado->vlr = $valor;

            $sfRelitemvlrccDuplicado->push();
        }
    }

    public function prepararSfRelItemVlrCC($idNovosfCentroCusto, $itemVlrCC, $numseqitem, $numseqpai)
    {

        $prepararSfItemCC = new \stdClass();
        $prepararSfItemCC->sfcc_id = $idNovosfCentroCusto;
        $prepararSfItemCC->numseqitem = $numseqitem;
        $prepararSfItemCC->numseqpai = $numseqpai;
        $prepararSfItemCC->vlr = $itemVlrCC['valor_item'] ?? null;
        $prepararSfItemCC->tipo = 'RELPCOITEM';

        return  $prepararSfItemCC;
    }

    /**
     * Duplica anterior registro sfpadrao
     *
     * @param $sfPadrao
     * @return int
     * @author Anderson Sathler M. Ribeiro <asathler@gmail.com>
     */
    protected function duplicaSfPadrao($sfPadrao)
    {
        $sfPadraoDuplicado = $sfPadrao->replicate();
        $sfPadraoDuplicado->fk = $this->apropriacaoId;
        $sfPadraoDuplicado->categoriapadrao = 'EXECFATURA';
        $sfPadraoDuplicado->decricaopadrao = '';
        $sfPadraoDuplicado->anodh = date('Y');
        $sfPadraoDuplicado->numdh = null;
        $sfPadraoDuplicado->msgretorno = '';
        $sfPadraoDuplicado->tipo = '';
        $sfPadraoDuplicado->situacao = 'P';
        $sfPadraoDuplicado->created_at = now();
        $sfPadraoDuplicado->updated_at = now();
        $sfPadraoDuplicado->user_id = backpack_user()->id;
        $sfPadraoDuplicado->push();

        return $sfPadraoDuplicado->id;
    }

    /**
     * Duplica anterior registro sfdadosbasicos
     *
     * @param $sfPadraoId
     * @param $sfPadraoIdDuplicado
     * @return bool
     * @author Anderson Sathler M. Ribeiro <asathler@gmail.com>
     */
    protected function duplicaSfDadosBasicos($sfPadraoId, $sfPadraoIdDuplicado, $faturaIds)
    {
        $sfDadosBasicos = SfDadosBasicos::where('sfpadrao_id', $sfPadraoId)->first();

        if (!$sfDadosBasicos) {
            return false;
        }

        $sfDadosBasicosDuplicado = $sfDadosBasicos->replicate();
        $sfDadosBasicosDuplicado->sfpadrao_id = $sfPadraoIdDuplicado;
        $sfDadosBasicosDuplicado->dtemis = now();
        $sfDadosBasicosDuplicado->dtvenc = $this->apropriacaoVencimento;
        $sfDadosBasicosDuplicado->vlr = $this->apropriacaoValor;
        $sfDadosBasicosDuplicado->txtprocesso = $this->fatura->processo;
        $sfDadosBasicosDuplicado->dtateste = $this->apropriacaoAteste;
        $sfDadosBasicosDuplicado->codcredordevedor = $this->limpa_cpf_cnpj($this->apropriacaoCodCredorDev);
        $sfDadosBasicosDuplicado->dtpgtoreceb = $this->apropriacaoVencimento;
        $txtobser = $this->gerarTxtObserPadrao(
            $faturaIds,
            $this->fatura->getRazaoSocialComCodigoFornecedor()
        );
        $sfDadosBasicosDuplicado->txtobser = $this->formatarStringTxtObserPeloTamanho(
            $sfDadosBasicosDuplicado,
            $txtobser
        );
        $sfDadosBasicosDuplicado->push();

        $this->duplicaSfDocOrigem($sfDadosBasicos->id, $sfDadosBasicosDuplicado, $faturaIds);
        $this->duplicaSfDocRelacionado($sfDadosBasicos->id, $sfDadosBasicosDuplicado);
        $this->duplicaSfTramite($sfDadosBasicos->id, $sfDadosBasicosDuplicado);

        return true;
    }

    /**
     * Retorna observação padrao para o campo txtobser da tabela sfdadosbasicos
     * @param array $faturaIds
     * @param string $fornecedor
     * @return string
     */
    public function gerarTxtObserPadrao(array $faturaIds, string $fornecedor): string
    {
        if (count($faturaIds) === 0) {
            return '';
        }
        $contratoFaturas = Contratofatura::whereIn('id', $faturaIds)->get();
        $numeroContrato = $contratoFaturas[0]->contrato->numero ?? ' - ';
        $stringNumerosContratofaturas = $contratoFaturas->pluck('numero')->implode(', ');

        $stringProcessosContratofaturas = $contratoFaturas->pluck('processo')->unique()->implode(', ');
        $stringMesAno = ContratoFaturaMesAno::selectRaw(
            "CONCAT(contratofaturas_mes_ano.mesref,'/',contratofaturas_mes_ano.anoref) as periodoref"
        )
            ->whereIn('contratofaturas_id', $faturaIds)
            ->pluck('periodoref')
            ->unique()
            ->sortBy(function ($item) {
                return Carbon::createFromFormat('m/Y', $item);
            })
            ->implode(', ');

        return "Pgto do(s) Instr.(s) de Cobrança(s) núm.(s):" .
            " $stringNumerosContratofaturas, Contrato nº: $numeroContrato " .
            " - $fornecedor, Proc.(s) núm.(s): $stringProcessosContratofaturas, do(s)" .
            " fornecimento(s)/serviço(s) prestado(s) ao(s) mês(es): $stringMesAno.";
    }



    /**
     * Duplica anterior registro sfdocorigem
     *
     * @param $sfDadosBasicosId
     * @param $sfDadosBasicosIdDuplicado
     * @return bool
     * @author Franklin Silva <franklin.linux@gmail.com>
     */
    protected function duplicaSfDocOrigem($sfDadosBasicosId, $sfDadosBasicosIdDuplicado, $faturaIds)
    {

        $sfDocOrigem = SfDocOrigem::where('sfdadosbasicos_id', $sfDadosBasicosId)->first();

        if (!$sfDocOrigem) {
            return false;
        }
        foreach ($faturaIds as $key => $faturaId) {
            $faturaDoc = Contratofatura::where('id', $faturaIds[$key])->first();

            $sfDocOrigemDuplicado = $sfDocOrigem->replicate();
            $sfDocOrigemDuplicado->sfdadosbasicos_id = $sfDadosBasicosIdDuplicado->id;
            $sfDocOrigemDuplicado->dtemis = $faturaDoc->emissao;
            $sfDocOrigemDuplicado->vlr = $faturaDoc->valorfaturado;
            $sfDocOrigemDuplicado->codidentemit = $this->limpa_cpf_cnpj($faturaDoc->getCodigoFornecedor());
            $sfDocOrigemDuplicado->numdocorigem = substr($faturaDoc->numero, 0, 1248);

            $sfDocOrigemDuplicado->push();
        }
        return true;
    }

    /**
     * Duplica anterior registro SfDocRelacionado
     *
     * @param $sfDadosBasicosId
     * @param $sfDadosBasicosIdDuplicado
     * @return bool
     * @author Franklin Silva <franklin.linux@gmail.com>
     */
    protected function duplicaSfDocRelacionado($sfDadosBasicosId, $sfDadosBasicosIdDuplicado)
    {

        $sfDocRelacionado = SfDocRelacionado::where('sfdadosbasicos_id', $sfDadosBasicosId)->get();
        if (!$sfDocRelacionado) {
            return false;
        }
        foreach ($sfDocRelacionado as $key => $docRelacionado) {
            $sfDocRelacionadoDuplicado = $docRelacionado->replicate();
            $sfDocRelacionadoDuplicado->sfdadosbasicos_id = $sfDadosBasicosIdDuplicado->id;
            $sfDocRelacionadoDuplicado->push();
        }
        return true;
    }

    /**
     * Duplica anterior registro SfTramite
     *
     * @param $sfDadosBasicosId
     * @param $sfDadosBasicosIdDuplicado
     * @return bool
     * @author Franklin Silva <franklin.linux@gmail.com>
     */
    protected function duplicaSfTramite($sfDadosBasicosId, $sfDadosBasicosIdDuplicado)
    {
        $sfTramite = SfTramite::where('sfdadosbasicos_id', $sfDadosBasicosId)->get();
        if (!$sfTramite) {
            return false;
        }
        foreach ($sfTramite as $key => $tramite) {
            $sfTramiteDuplicado = $tramite->replicate();
            $sfTramiteDuplicado->sfdadosbasicos_id = $sfDadosBasicosIdDuplicado->id;
            $sfTramiteDuplicado->push();
        }
        return true;
    }


    /**
     * Duplica anterior registro sfpco
     *
     * @param $sfPadraoId
     * @param $sfPadraoIdDuplicado_id
     * @return false
     * @author Anderson Sathler M. Ribeiro <asathler@gmail.com>
     */
    protected function duplicaSfPco($sfPadraoId, $sfPadraoIdDuplicado_id, $faturaIds)
    {
        $sfPco = SfPco::where('sfpadrao_id', $sfPadraoId)->get();

        if (!$sfPco) {
            return false;
        }
        foreach ($sfPco as $key => $pco) {
            $sfPcoDuplicado = $pco->replicate();
            $sfPcoDuplicado->sfpadrao_id = $sfPadraoIdDuplicado_id;
            $sfPcoDuplicado->push();
            $this->duplicaSfPcoItem($pco->id, $sfPcoDuplicado->id, $faturaIds);
            $this->duplicaSfCronBaixaPatrimonial($pco->id, $sfPcoDuplicado->id, 'sfpco_id');
        }

        return true;
    }

    /**
     * Duplica anterior registro sfpcoitem
     *
     * @param $sfPcoId
     * @param $sfPcoIdDuplicadoId
     * @return bool
     * @author Anderson Sathler M. Ribeiro <asathler@gmail.com>
     */
    protected function duplicaSfPcoItem($sfPcoId, $sfPcoIdDuplicadoId, $faturaIds)
    {

        $sfPcoItem = SfPcoItem::where('sfpco_id', $sfPcoId)
        ->orderBy('id', 'asc')
        ->get();

        if (!$sfPcoItem) {
            return false;
        }

        if (isset($this->empenhosFaturas) && count($this->empenhosFaturas) && !$this->empenhosFaturas->isEmpty()) {
            foreach ($this->empenhosFaturas as $key => $empenhosFatura) {

                #guarda numero do empenho do instrumento de cobranca
                $numeroEmpe = $empenhosFatura->numero ?? '';

                $bucaSfPcoItem = SfPcoItem::where('sfpco_id', $sfPcoIdDuplicadoId)
                    ->where('numempe', $numeroEmpe)
                    ->where('subelemento_id', $empenhosFatura->subelemento_id)
                    ->first();

                if ($bucaSfPcoItem) {
                    $bucaSfPcoItem->vlr += $empenhosFatura->valorreftotal ?? 0;
                    $bucaSfPcoItem->save();
                } else {
                    $novoSfPcoItem = new SfPcoItem();
                    $novoSfPcoItem->sfpco_id = $sfPcoIdDuplicadoId;
                    $novoSfPcoItem->numempe = $numeroEmpe;
                    $novoSfPcoItem->vlr = $empenhosFatura->valorreftotal ?? 0;
                    $novoSfPcoItem->indrliquidado = true;
                    $novoSfPcoItem->codsubitemempe = $empenhosFatura->subelemento ? $empenhosFatura->subelemento->codigo : null;
                    $novoSfPcoItem->subelemento_id = $empenhosFatura->subelemento_id ?? null;
                    $novoSfPcoItem->numseqitem = $key + 1;

                    #Verificar se $numeroEmpe do instrumento de cobrança está presente na collection de $sfPcoItem
                    #do DH Padrao e caso sim preenche com os dados.
                    $sfPcoItemEncontrado = $sfPcoItem->first();

                    if ($sfPcoItemEncontrado) {

                        $novoSfPcoItem->txtinscra = $sfPcoItemEncontrado->txtinscra;
                        $novoSfPcoItem->numclassa = $sfPcoItemEncontrado->numclassa;
                        $novoSfPcoItem->txtinscrb = $sfPcoItemEncontrado->txtinscrb;
                        $novoSfPcoItem->numclassb = $sfPcoItemEncontrado->numclassb;
                        $novoSfPcoItem->txtinscrc = $sfPcoItemEncontrado->txtinscrc;
                        $novoSfPcoItem->numclassc = $sfPcoItemEncontrado->numclassc;
                    }

                    $novoSfPcoItem->push();
                }
                $this->criarPopularContratoFaturaEmpenhosApropriacao($empenhosFatura, $novoSfPcoItem->id);
            }
        }
        return true;
    }

    /**
     * Duplica anterior registro sfpcoitem
     *
     * @param $sfPcoId
     * @param $sfPcoIdDuplicadoId
     * @return bool
     * @author Franklin Silva <franklin.linux@gmail.com>
     */
    protected function duplicaSfCronBaixaPatrimonial($idBuscaSf, $idSfDuplicado, string $idExterno)
    {

        $sfCronBaixaPatrimonial = SfCronBaixaPatrimonial::where($idExterno, $idBuscaSf)->get();

        if (!$sfCronBaixaPatrimonial) {
            return false;
        }
        foreach ($sfCronBaixaPatrimonial as $key => $cronBaixaPatrimonial) {
            $sfCronBaixaPatrimonialDuplicado = $cronBaixaPatrimonial->replicate();
            $sfCronBaixaPatrimonialDuplicado->$idExterno = $idSfDuplicado;
            $sfCronBaixaPatrimonialDuplicado->push();
        }
        return true;
    }


    /**
     * Duplica anterior registro SfParcela
     *
     * @param $sfCronBaixaPatrimonialId
     * @param $sfCronBaixaPatrimonialDuplicadoId
     * @return bool
     * @author Franklin Silva <franklin.linux@gmail.com>
     */
    protected function duplicaSfParcelas($sfCronBaixaPatrimonialId, $sfCronBaixaPatrimonialDuplicadoId)
    {
        $SfParcela = SfParcela::where('sfCronBaixaPatrimonial_id', $sfCronBaixaPatrimonialId)->get();

        if (!$SfParcela) {
            return false;
        }
        foreach ($SfParcela as $key => $parcela) {
            $sfParcelaDuplicado = $parcela->replicate();
            $sfParcelaDuplicado->sfCronBaixaPatrimonial_id = $sfCronBaixaPatrimonialDuplicadoId;
            $sfParcelaDuplicado->push();
        }
        return true;
    }

    /**
     * Duplica anterior registro sfpso
     *
     * @param $sfPadraoId
     * @param $sfPadraoIdDuplicado_id
     * @return bool
     * @author Franklin Silva <franklin.linux@gmail.com>
     */
    protected function duplicaSfPso($sfPadraoId, $sfPadraoIdDuplicado_id)
    {
        $sfPso = SfPso::where('sfpadrao_id', $sfPadraoId)->get();

        if (!$sfPso) {
            return false;
        }
        foreach ($sfPso as $key => $pso) {
            $sfPsoDuplicado = $pso->replicate();
            $sfPsoDuplicado->sfpadrao_id = $sfPadraoIdDuplicado_id;
            $sfPsoDuplicado->push();
            $this->duplicaSfPsoItem($pso->id, $sfPsoDuplicado->id);
        }

        return true;
    }

    /**
     * Duplica anterior registro sfpsoitem
     *
     * @param $sfPsoId
     * @param $sfPsoIdDuplicadoId
     * @return bool
     * @author Franklin Silva <franklin.linux@gmail.com>
     */
    protected function duplicaSfPsoItem($sfPsoId, $sfPsoIdDuplicadoId)
    {
        $sfPsoItem = SfPsoItem::where('sfpco_id', $sfPsoId)->get();

        if (!$sfPsoItem) {
            return false;
        }
        foreach ($sfPsoItem as $key => $psoItem) {
            $sfPsoItemDuplicado = $psoItem->replicate();
            $sfPsoItemDuplicado->sfpso_id = $sfPsoIdDuplicadoId;
            $sfPsoItemDuplicado->vlr = $this->apropriacaoValor;
            $sfPsoItemDuplicado->push();
        }
        return true;
    }


    /**
     * Duplica anterior registro sfcredito
     *
     * @param $sfPadraoId
     * @param $sfPadraoIdDuplicado_id
     * @return bool
     * @author Franklin Silva <franklin.linux@gmail.com>
     */
    protected function duplicaSfCredito($sfPadraoId, $sfPadraoIdDuplicado_id)
    {
        $sfCredito = SfCredito::where('sfpadrao_id', $sfPadraoId)->get();

        if (!$sfCredito) {
            return false;
        }
        foreach ($sfCredito as $key => $credito) {
            $sfCreditoDuplicado = $credito->replicate();
            $sfCreditoDuplicado->sfpadrao_id = $sfPadraoIdDuplicado_id;
            $sfCreditoDuplicado->push();
        }

        return true;
    }


    /**
     * Duplica anterior registro sfpco
     *
     * @param $sfPadraoId
     * @param $sfPadraoIdDuplicado_id
     * @return false
     * @author Anderson Sathler M. Ribeiro <asathler@gmail.com>
     */
    protected function duplicaSfOutrosLanc($sfPadraoId, $sfPadraoIdDuplicado_id)
    {
        $sfOutrosLanc = SfOutrosLanc::where('sfpadrao_id', $sfPadraoId)->get();

        if (!$sfOutrosLanc) {
            return false;
        }
        foreach ($sfOutrosLanc as $key => $outrosLanc) {
            $sfOutrosLancDuplicado = $outrosLanc->replicate();
            $sfOutrosLancDuplicado->sfpadrao_id = $sfPadraoIdDuplicado_id;
            $sfOutrosLancDuplicado->push();
            $this->duplicaSfCronBaixaPatrimonial($outrosLanc->id, $sfOutrosLancDuplicado->id, 'sfoutroslanc_id');
        }

        return true;
    }

    /**
     * Duplica anterior registro sfdeducao
     *
     * @param $sfPadraoId
     * @param $sfPadraoIdDuplicado_id
     * @return false
     * @author Franklin Silva <franklin.linux@gmail.com>
     */

    /**
     *@TODO Não duplica sfDeducao, porém futuramente será duplicado.
     */

    protected function duplicaSfDeducao($sfPadraoId, $sfPadraoIdDuplicado_id)
    {
        $sfDeducao = SfDeducao::where('sfpadrao_id', $sfPadraoId)->get();

        if (!$sfDeducao) {
            return false;
        }
        foreach ($sfDeducao as $key => $deducao) {
            $sfDeducaoDuplicado = $deducao->replicate();
            $sfDeducaoDuplicado->sfpadrao_id = $sfPadraoIdDuplicado_id;
            $percentual = ($deducao->vlr / $this->apropriacaoValor);
            $percentual = (number_format($percentual,5,'.',''));
            $this->apropriacaoTotalDeducao += ($percentual * $this->apropriacaoValor);
            $sfDeducaoDuplicado->vlr = $this->apropriacaoTotalDeducao;
            $sfDeducaoDuplicado->push();
            $this->duplicaSfItemRecolhimento($deducao->id,$sfDeducaoDuplicado->id,'sfded_id',$percentual);
            $this->duplicaSfPredoc($deducao->id,$sfDeducaoDuplicado->id,'sfded_id');
            $this->duplicaSfAcrescimo($deducao->id,$sfDeducaoDuplicado->id,'sfded_id');
            $this->duplicaSfRelPcoItem($deducao->id,$sfDeducaoDuplicado->id);
            $this->duplicaSfRelPsoItem($deducao->id,$sfDeducaoDuplicado->id);
            $this->duplicaSfRelCredito($deducao->id,$sfDeducaoDuplicado->id);

        }

        return true;
    }


    /**
     * Duplica anterior registro SfItemRecolhimento
     *
     * @param $idBuscaSf
     * @param $idSfDuplicado
     * @param $idExterno
     * @return bool
     * @author Franklin Silva <franklin.linux@gmail.com>
     */
    protected function duplicaSfItemRecolhimento($idBuscaSf, $idSfDuplicado, string $idExterno, $percentual = null)
    {
        $sfItemRecolhimento = SfItemRecolhimento::where($idExterno, $idBuscaSf)->get();

        if (!$sfItemRecolhimento) {
            return false;
        }
        foreach ($sfItemRecolhimento as $key => $itemRecolhimento) {
            $sfItemRecolhimentoDuplicado = $itemRecolhimento->replicate();
            $sfItemRecolhimentoDuplicado->$idExterno = $idSfDuplicado;
            $sfItemRecolhimentoDuplicado->vlr = $this->apropriacaoValor * $percentual;
            $sfItemRecolhimentoDuplicado->vlrbasecalculo = $this->apropriacaoValor;
            //            todo quando for encargo não subtrair da fatura ou dados pagamento.
            ($idExterno != 'sfencargos_id')
                ? $sfItemRecolhimentoDuplicado->vlr - ($this->apropriacaoValor * $percentual)
                : $sfItemRecolhimentoDuplicado->vlr - $this->apropriacaoValor;
            $sfItemRecolhimentoDuplicado->push();
        }

        return true;
    }


    /**
     * Duplica anterior registro SfPredoc
     *
     * @param $idBuscaSf
     * @param $idSfDuplicado
     * @param $idExterno
     * @return bool
     * @author Franklin Silva <franklin.linux@gmail.com>
     */
    protected function duplicaSfPredoc($idBuscaSf, $idSfDuplicado, string $idExterno)
    {
        $sfPredoc = SfPredoc::where($idExterno, $idBuscaSf)->get();

        if (!$sfPredoc) {
            return false;
        }
        foreach ($sfPredoc as $key => $predoc) {
            $sfPredocDuplicado = $predoc->replicate();
            $sfPredocDuplicado->$idExterno = $idSfDuplicado;
            $sfPredocDuplicado->txtobser = $predoc->txtobser;
            $sfPredocDuplicado->push();
        }

        return true;
    }

    /**
     * Duplica anterior registro SfAcrescimo
     *
     * @param $idBuscaSf
     * @param $idSfDuplicado
     * @param $idExterno
     * @return bool
     * @author Franklin Silva <franklin.linux@gmail.com>
     */
    protected function duplicaSfAcrescimo($idBuscaSf, $idSfDuplicado, string $idExterno)
    {
        $sfAcrescimo = SfAcrescimo::where($idExterno, $idBuscaSf)->get();

        if (!$sfAcrescimo) {
            return false;
        }
        foreach ($sfAcrescimo as $key => $acrescimo) {
            $sfAcrescimoDuplicado = $acrescimo->replicate();
            $sfAcrescimoDuplicado->$idExterno = $idSfDuplicado;
            $this->apropriacaoAcrescimo += $sfAcrescimoDuplicado->vlr;
            $sfAcrescimoDuplicado->push();
        }

        return true;
    }

    /**
     * Duplica anterior registro SfRelPcoItem
     *
     * @param $sfDeducaoId
     * @param $sfDeducaoDuplicado_id
     * @return bool
     * @author Franklin Silva <franklin.linux@gmail.com>
     */
    protected function duplicaSfRelPcoItem($sfDeducaoId, $sfDeducaoDuplicado_id)
    {
        $sfRelPcoItem = SfRelPcoItem::where('sfded_id', $sfDeducaoId)->get();

        if (!$sfRelPcoItem) {
            return false;
        }
        foreach ($sfRelPcoItem as $key => $relPcoItem) {
            $sfRelPcoItemDuplicado = $relPcoItem->replicate();
            $sfRelPcoItemDuplicado->sfded_id = $sfDeducaoDuplicado_id;
            $sfRelPcoItemDuplicado->push();
        }

        return true;
    }

    /**
     * Duplica anterior registro SfRelPsoItem
     *
     * @param $sfDeducaoId
     * @param $sfDeducaoDuplicado_id
     * @return bool
     * @author Franklin Silva <franklin.linux@gmail.com>
     */
    protected function duplicaSfRelPsoItem($sfDeducaoId, $sfDeducaoDuplicado_id)
    {
        $sfRelPsoItem = SfRelPsoItem::where('sfded_id', $sfDeducaoId)->get();

        if (!$sfRelPsoItem) {
            return false;
        }
        foreach ($sfRelPsoItem as $key => $relPsoItem) {
            $sfRelPsoItemDuplicado = $relPsoItem->replicate();
            $sfRelPsoItemDuplicado->sfded_id = $sfDeducaoDuplicado_id;
            $sfRelPsoItemDuplicado->push();
        }

        return true;
    }


    /**
     * Duplica anterior registro SfRelCredito
     *
     * @param $sfDeducaoId
     * @param $sfDeducaoDuplicado_id
     * @return bool
     * @author Franklin Silva <franklin.linux@gmail.com>
     */
    protected function duplicaSfRelCredito($sfDeducaoId, $sfDeducaoDuplicado_id)
    {
        $sfRelCredito = SfRelCredito::where('sfded_id', $sfDeducaoId)->get();

        if (!$sfRelCredito) {
            return false;
        }
        foreach ($sfRelCredito as $key => $credito) {
            $sfRelCreditoDuplicado = $credito->replicate();
            $sfRelCreditoDuplicado->sfded_id = $sfDeducaoDuplicado_id;
            $sfRelCreditoDuplicado->push();
        }

        return true;
    }


    /**
     * Duplica anterior registro SfEncargos
     *
     * @param $sfPadraoId
     * @param $sfPadraoIdDuplicado_id
     * @return false
     * @author Franklin Silva <franklin.linux@gmail.com>
     */
    protected function duplicaSfEncargos($sfPadraoId, $sfPadraoIdDuplicado_id)
    {
        $sfEncargos = SfEncargos::where('sfpadrao_id', $sfPadraoId)->get();

        if (!$sfEncargos) {
            return false;
        }
        foreach ($sfEncargos as $key => $encargos) {
            $sfEncargosDuplicado = $encargos->replicate();
            $sfEncargosDuplicado->sfpadrao_id = $sfPadraoIdDuplicado_id;
            $percentual = ($encargos->vlr / $this->apropriacaoValor);
            $percentual = (number_format($percentual, 5, '.', ''));
            $sfEncargosDuplicado->vlr = $percentual * $this->apropriacaoValor;
            $sfEncargosDuplicado->push();
            $this->duplicaSfItemRecolhimento($encargos->id, $sfEncargosDuplicado->id, 'sfencargos_id', $percentual);
            $this->duplicaSfPredoc($encargos->id, $sfEncargosDuplicado->id, 'sfencargos_id');
            $this->duplicaSfAcrescimo($encargos->id, $sfEncargosDuplicado->id, 'sfencargos_id');
        }

        return true;
    }

    /**
     * Duplica anterior registro SfDespesaAnular
     *
     * @param $sfPadraoId
     * @param $sfPadraoIdDuplicado_id
     * @return false
     * @author Franklin Silva <franklin.linux@gmail.com>
     */
    protected function duplicaSfDespesaAnular($sfPadraoId, $sfPadraoIdDuplicado_id)
    {
        $sfDespesaAnular = SfDespesaAnular::where('sfpadrao_id', $sfPadraoId)->get();

        if (!$sfDespesaAnular) {
            return false;
        }
        foreach ($sfDespesaAnular as $key => $despesaAnular) {
            $sfDespesaAnularDuplicado = $despesaAnular->replicate();
            $sfDespesaAnularDuplicado->sfpadrao_id = $sfPadraoIdDuplicado_id;
            $sfDespesaAnularDuplicado->push();
            $this->duplicaSfDespesaAnularItem($despesaAnular->id, $sfDespesaAnularDuplicado->id);
        }
        return true;
    }

    /**
     * Duplica anterior registro SfDespesaAnularItem
     *
     * @param $sfDespesaAnularId
     * @param $sfDespesaAnularDuplicada_id
     * @return false
     * @author Franklin Silva <franklin.linux@gmail.com>
     */
    protected function duplicaSfDespesaAnularItem($sfDespesaAnularId, $sfDespesaAnularDuplicada_id)
    {
        $sfDespesaAnularItem = SfDespesaAnularItem::where('sfdespesaanular_id', $sfDespesaAnularId)->get();

        if (!$sfDespesaAnularItem) {
            return false;
        }
        foreach ($sfDespesaAnularItem as $key => $despesaAnularItem) {
            $sfDespesaAnularItemDuplicado = $despesaAnularItem->replicate();
            $sfDespesaAnularItemDuplicado->sfdespesaanular_id = $sfDespesaAnularDuplicada_id;
            $this->apropriacaoDesAnularTotal += $sfDespesaAnularItemDuplicado->vlr;
            $sfDespesaAnularItemDuplicado->push();
            $this->duplicaSfRelEncargos($despesaAnularItem->id, $sfDespesaAnularItemDuplicado->id);
        }
        return true;
    }

    /**
     * Duplica anterior registro SfRelEncargos
     *
     * @param $sfDespesaAnularItemId
     * @param $sfDespesaAnularItemDuplicada_id
     * @return false
     * @author Franklin Silva <franklin.linux@gmail.com>
     */
    protected function duplicaSfRelEncargos($sfDespesaAnularItemId, $sfDespesaAnularItemDuplicada_id)
    {
        $sfRelEncargos = SfRelEncargos::where('sfdespesaanularitem_id', $sfDespesaAnularItemId)->get();

        if (!$sfRelEncargos) {
            return false;
        }
        foreach ($sfRelEncargos as $key => $relEncargo) {
            $sfRelencargoDuplicado = $relEncargo->replicate();
            $sfRelencargoDuplicado->sfdespesaanularitem_id = $sfDespesaAnularItemDuplicada_id;
            $sfRelencargoDuplicado->push();
        }
        return true;
    }

    /**
     * Duplica anterior registro SfCompensacao
     *
     * @param $sfPadraoId
     * @param $sfPadraoIdDuplicado_id
     * @return false
     * @author Franklin Silva <franklin.linux@gmail.com>
     */
    protected function duplicaSfCompensacao($sfPadraoId, $sfPadraoIdDuplicado_id)
    {
        $sfCompensacao = SfCompensacao::where('sfpadrao_id', $sfPadraoId)->get();

        if (!$sfCompensacao) {
            return false;
        }
        foreach ($sfCompensacao as $key => $compensacao) {
            $sfCompensacaoDuplicado = $compensacao->replicate();
            $sfCompensacaoDuplicado->sfpadrao_id = $sfPadraoIdDuplicado_id;
            $sfCompensacaoDuplicado->push();
            $this->duplicaSfRelDeducaoItem($compensacao->id, $sfCompensacaoDuplicado->id);
            $this->duplicaSfRelEncargoItem($compensacao->id, $sfCompensacaoDuplicado->id);
        }

        return true;
    }

    /**
     * Duplica anterior registro SfRelDeducaoItem
     *
     * @param $sfCompensacaoId
     * @param $sfCompensacaomDuplicada_id
     * @return false
     * @author Franklin Silva <franklin.linux@gmail.com>
     */
    protected function duplicaSfRelDeducaoItem($sfCompensacaoId, $sfCompensacaomDuplicada_id)
    {
        $sfRelDeducaoItem = SfRelDeducaoItem::where('sfcompensacao_id', $sfCompensacaoId)->get();

        if (!$sfRelDeducaoItem) {
            return false;
        }
        foreach ($sfRelDeducaoItem as $key => $relDeducaoItem) {
            $sfRelDeducaoItemDuplicado = $relDeducaoItem->replicate();
            $sfRelDeducaoItemDuplicado->sfcompensacao_id = $sfCompensacaomDuplicada_id;
            $sfRelDeducaoItemDuplicado->push();
        }
        return true;
    }

    /**
     * Duplica anterior registro SfRelEncargoItem
     *
     * @param $sfCompensacaoId
     * @param $sfCompensacaomDuplicada_id
     * @return false
     * @author Franklin Silva <franklin.linux@gmail.com>
     */
    protected function duplicaSfRelEncargoItem($sfCompensacaoId, $sfCompensacaomDuplicada_id)
    {
        $sfRelEncargoItem = SfRelEncargoItem::where('sfcompensacao_id', $sfCompensacaoId)->get();

        if (!$sfRelEncargoItem) {
            return false;
        }
        foreach ($sfRelEncargoItem as $key => $relEncargoItem) {
            $sfRelEncargoItemDuplicado = $relEncargoItem->replicate();
            $sfRelEncargoItemDuplicado->sfcompensacao_id = $sfCompensacaomDuplicada_id;
            $sfRelEncargoItemDuplicado->push();
        }
        return true;
    }


    /**
     * Duplica anterior registro SfCentroCusto
     *
     * @param $sfPadraoId
     * @param $sfPadraoIdDuplicado_id
     * @author Franklin Silva <franklin.linux@gmail.com>
     */
    protected function duplicaSfCentroCusto($sfPadraoId, $sfPadraoIdDuplicado_id, $faturaIds)
    {
        $sfCentroCusto = SfCentroCusto::where('sfpadrao_id', $sfPadraoId)->first();

        $arrMesAnoRef = ContratoFaturaMesAno::whereIn('contratofaturas_id', $faturaIds)
            ->selectRaw('anoref, mesref, SUM(valorref) as total_valor')
            ->groupBy('anoref', 'mesref')
            ->orderBy('anoref')
            ->orderBy('mesref')
            ->get();

        foreach ($arrMesAnoRef as $key => $mesAnoRef) {
            $sfcentroCustoDuplicado = new SfCentroCusto();
            $sfcentroCustoDuplicado->codcentrocusto = isset($sfCentroCusto->codcentrocusto) ? $sfCentroCusto->codcentrocusto : null;
            $sfcentroCustoDuplicado->codugbenef = isset($sfCentroCusto->codugbenef) ? $sfCentroCusto->codugbenef : null;
            $sfcentroCustoDuplicado->codsiorg = isset($sfCentroCusto->codsiorg) ? $sfCentroCusto->codsiorg : null;
            $sfcentroCustoDuplicado->sfpadrao_id = $sfPadraoIdDuplicado_id;
            $sfcentroCustoDuplicado->mesreferencia = $mesAnoRef->mesref;
            $sfcentroCustoDuplicado->anoreferencia = $mesAnoRef->anoref;
            $sfcentroCustoDuplicado->numseqitem = $key + 1;
            $sfcentroCustoDuplicado->push();

            if ($sfCentroCusto) $this->duplicaSfRelitemvlrcc($sfCentroCusto->id, $sfcentroCustoDuplicado->id, $mesAnoRef->total_valor);
        }
    }

    /**
     * Duplica anterior registro SfRelPcoItem
     *
     * @param $sfCentroCustoId
     * @param $sfCentroCustoDuplicada_id
     * @return false
     * @author Franklin Silva <franklin.linux@gmail.com>
     */
    protected function duplicaSfRelitemvlrcc($sfCentroCustoId, $sfCentroCustoDuplicada_id, $novoValor)
    {
        $SfRelitemvlrcc = Sfrelitemvlrcc::where('sfcc_id', $sfCentroCustoId)->get();

        if (!$SfRelitemvlrcc) {
            return false;
        }
        foreach ($SfRelitemvlrcc as $key => $relitemvlrcc) {
            $sfRelitemvlrccDuplicado = $relitemvlrcc->replicate();
            $sfRelitemvlrccDuplicado->sfcc_id = $sfCentroCustoDuplicada_id;
            $sfRelitemvlrccDuplicado->vlr = $novoValor;
            $sfRelitemvlrccDuplicado->push();
        }
        return true;
    }


    /**
     * Duplica anterior registro SfDadosPgto
     *
     * @param $sfPadraoId
     * @param $sfPadraoIdDuplicado_id
     * @author Franklin Silva <franklin.linux@gmail.com>
     */
    protected function duplicaSfDadosPgto($sfPadraoId, $sfPadraoIdDuplicado_id)
    {
        $sfDadosPgto = SfDadosPgto::where('sfpadrao_id', $sfPadraoId)->get();

        if (!$sfDadosPgto) {
            return false;
        }

        foreach ($sfDadosPgto as $key => $dadosPgto) {
            $sfDadosPgtoDuplicado = $dadosPgto->replicate();
            $sfDadosPgtoDuplicado->sfpadrao_id = $sfPadraoIdDuplicado_id;
            $sfDadosPgtoDuplicado->vlr = ($this->apropriacaoValor -  ($this->apropriacaoTotalDeducao + $this->apropriacaoDesAnularTotal)) + $this->apropriacaoAcrescimo;
            $sfDadosPgtoDuplicado->push();
        }

        return true;
    }

    /**
     * Duplica anterior registro SfDocContabilizacao
     *
     * @param $sfPadraoId
     * @param $sfPadraoIdDuplicado_id
     * @return false
     * @author Franklin Silva <franklin.linux@gmail.com>
     */
    protected function duplicaSfDocContabilizacao($sfPadraoId, $sfPadraoIdDuplicado_id)
    {
        $sfDocContabilizacao = SfDocContabilizacao::where('sfpadrao_id', $sfPadraoId)->get();

        if (!$sfDocContabilizacao) {
            return false;
        }
        foreach ($sfDocContabilizacao as $key => $docContabilizacao) {
            $sfDocContabilizacaoDuplicado = $docContabilizacao->replicate();
            $sfDocContabilizacaoDuplicado->sfpadrao_id = $sfPadraoIdDuplicado_id;
            $sfDocContabilizacaoDuplicado->push();
        }

        return true;
    }

    /**
     * Monta $html com definições para montagem do Grid
     *
     * @return \Yajra\DataTables\Html\Builder
     * @author Anderson Sathler M. Ribeiro <asathler@gmail.com>
     */
    protected function retornaHtmlGrid()
    {
        $html = $this->htmlBuilder;

        $html->addColumn([
            'data' => 'numero',
            'name' => 'numero',
            'title' => 'Contrato'
        ]);

        $html->addColumn([
            'data' => 'fornecedor',
            'name' => 'fornecedor',
            'title' => 'Fornecedor'
        ]);

        $html->addColumn([
            'data' => 'ateste',
            'name' => 'ateste',
            'title' => 'Dt. Ateste'
        ]);

        $html->addColumn([
            'data' => 'vencimento',
            'name' => 'vencimento',
            'title' => 'Dt. Vencimento'
        ]);

        $html->addColumn([
            'data' => 'faturas',
            'name' => 'faturas',
            'title' => 'Faturas'
        ]);

        $html->addColumn([
            'data' => 'total',
            'name' => 'total',
            'title' => 'Total',
            'class' => 'text-right'
        ]);

        $html->addColumn([
            'data' => 'valor',
            'name' => 'valor',
            'title' => 'Valor informado',
            'class' => 'text-right'
        ]);

        $html->addColumn([
            'data' => 'fase',
            'name' => 'fase',
            'title' => 'Fase'
        ]);

        $html->addColumn([
            'data' => 'action',
            'name' => 'action',
            'title' => 'Ações',
            'class' => 'text-nowrap',
            'orderable' => false,
            'searchable' => false
        ]);

        $html->parameters([
            'processing' => true,
            'serverSide' => true,
            'responsive' => true,
            'info' => true,
            'order' => [
                0,
                'desc'
            ],
            'autoWidth' => false,
            'bAutoWidth' => false,
            'paging' => true,
            'lengthChange' => true,
            'language' => [
                'url' => asset('/json/pt_br.json')
            ]
        ]);

        return $html;
    }

    /**
     * Monta $html com definições da coluna Ações
     *
     * @param $registro
     * @return string
     * @author Anderson Sathler M. Ribeiro <asathler@gmail.com>
     */
    protected function retornaAcoes($registro)
    {
        $apropriacaoId = $registro->id;
        $finalizada = $registro->fase_id == $this->retornaIdCodigoItem(
            'Status Apropriação Instrumento de Cobrança SIAFI',
            'Documento Emitido'
        ) ? true : false;

        $editar = $this->retornaBtnEditar($apropriacaoId);
        $excluir = $this->retornaBtnExcluir($apropriacaoId);
        $relatorio = $this->retornaBtnRelatorio($apropriacaoId);
        $dochabil = $this->retornaBtnDocHabil($apropriacaoId);

        $acaoFinalizada = $relatorio . $dochabil;
        $acaoEmAndamento = $editar . $excluir;

        if ($finalizada) {
            $acaoEmAndamento .= $relatorio;
        }

        $acoes = '';
        $acoes .= '<div class="btn-group">';
        $acoes .= ($finalizada == true) ? $acaoFinalizada : $acaoEmAndamento;
        $acoes .= '</div>';

        return $acoes;
    }

    /**
     * @param $faturaIds
     * Atualiza a situacao das faturas apropriadas para AND - 'Aproriacao em Andamento'
     */
    protected function atualizarSituacaoEPadraoFaturas($faturaIds, $padraoId)
    {
        DB::beginTransaction();
        try {
            Contratofatura::whereIn('id', $faturaIds)
                ->update(['situacao' => 'AND', 'sfpadrao_id' => $padraoId]);
            DB::commit();
        } catch (\Exception $exc) {
            DB::rollback();
        }
    }



    /**
     * Monta $html para exibição, ou não, do botão de excluir
     *
     * @param $apropriacaoId
     * @return string
     * @author Anderson Sathler M. Ribeiro <asathler@gmail.com>
     */
    protected function retornaBtnExcluir($apropriacaoId)
    {
        $excluir = '';
        $excluir .= '<a '; //href="" ';
        $excluir .= "class='btn btn-default btn-sm '";
        $excluir .= 'data-toggle="modal" ';
        $excluir .= 'data-target="#confirmaExclusaoApropriacaoFatura" ';
        $excluir .= 'data-id="' . $apropriacaoId . '"';
        $excluir .= 'name="delete_modal" ';
        $excluir .= 'title="Excluir apropriação da fatura">';
        $excluir .= '<i class="fa fa-trash"></i></a>';

        return $excluir;
    }

    /**
     * Monta $html para exibição, ou não, do botão de edição
     *
     * @param $apropriacaoId
     * @return string
     * @author Anderson Sathler M. Ribeiro <asathler@gmail.com>
     */
    protected function retornaBtnEditar($apropriacaoId)
    {
        $editar = '';
        // Exibir form com campos para geração da apropriação
        $editar .= '<a href="#" ';
        $editar .= "class='btn btn-default btn-sm '";
        $editar .= 'title="Informar dados para apropriação da fatura">';
        $editar .= '<i class="fa fa-play"></i></a>';

        return $editar;
    }

    protected function retornaBtnRelatorio($apropriacaoId)
    {
        $relatorio = '';
        $relatorio .= '<a href="/apropriacao/fatura/' . $apropriacaoId . '" ';
        $relatorio .= "class='btn btn-default btn-sm '";
        $relatorio .= 'title="Relatório da apropriação">';
        $relatorio .= '<i class="fa fa-list-alt"></i></a>';

        return $relatorio;
    }

    protected function retornaBtnDocHabil($apropriacaoId)
    {
        $docHabil = '';
        $docHabil .= '<a href="/apropriacao/fatura/' . $apropriacaoId . '/dochabil/" ';
        $docHabil .= "class='btn btn-default btn-sm '";
        $docHabil .= 'title="Documento Hábil Apropriado">';
        $docHabil .= '<i class="fa fa-file-o"></i></a>';

        return $docHabil;
    }

    private function criarSfPadrao($request): int
    {
        $codtipodh = request()->is('api/*') ? $request->tipo_dh : '';
        $codugemit = request()->is('api/*') ? $request->cod_ug_emitente : $this->retornarUgEmitenteDhPadrao();
        $arrNovoSfPadrao = array();

        $arrNovoSfPadrao['fk'] = $this->apropriacaoId;
        $arrNovoSfPadrao['categoriapadrao'] = 'EXECFATURA';
        $arrNovoSfPadrao['decricaopadrao'] = '';
        $arrNovoSfPadrao['codugemit'] =  $codugemit;
        $arrNovoSfPadrao['anodh'] = date('Y');
        $arrNovoSfPadrao['codtipodh'] = $codtipodh;
        $arrNovoSfPadrao['numdh'] = null;
        $arrNovoSfPadrao['dtemis'] = date('Y-m-d');
        $arrNovoSfPadrao['txtmotivo'] = null;
        $arrNovoSfPadrao['msgretorno'] = null;
        $arrNovoSfPadrao['tipo'] = '';
        $arrNovoSfPadrao['situacao'] = 'P';
        $arrNovoSfPadrao['user_id'] = backpack_user()->id;
        $novoSfPadrao = SfPadrao::create($arrNovoSfPadrao);

        return $novoSfPadrao->id;
    }

    public function adicionaPadraoNaSessao($idPadrao, $booExibir)
    {
        //caso o atributo 'arrPadraoAlteracaoApropriacao' não exista na sessao, cria.
        if (!session()->has('arrPadraoAlteracaoApropriacao')) {
            session(['arrPadraoAlteracaoApropriacao' => []]);
        }

        $arrPadraoAlteracaoApropriacao = session()->get('arrPadraoAlteracaoApropriacao');

        //caso o idPadrao nao esteja na sessao, adiciona.
        if (!array_key_exists($idPadrao, $arrPadraoAlteracaoApropriacao)) {
            $arrPadraoAlteracaoApropriacao[$idPadrao] = [
                'exibirAbaDeducao' => $booExibir,
                'numAbaLiberada' => 1
            ];
        }

        session(['arrPadraoAlteracaoApropriacao' => $arrPadraoAlteracaoApropriacao]);
    }

    /**
     * Retorna a unidade do empenho da fatura ou a unidade do usuário logado
     * @TODO aguardando model ContratofaturaEmpenho #298 para chamar a unidade de lá
     * @return mixed
     */
    public function retornarUgEmitenteDhPadrao()
    {
        /*if ($this->fatura->empenhos->first()->unidade->codigo) {
            return $this->fatura->empenhos->first()->unidade->codigo;
        }*/

        return session()->get('user_ug');
    }

    public function buscaOrdemBancariaAjax($numDocumento, $sfpadrao_id)
    {
        try {
            DB::beginTransaction();
            $staService = new STAOrdemBancariaService;
            $ordensBancariasAgrupadas = $staService->recuperaCriaOrdensBancariasPorDocumento($numDocumento, config('rotas-sta.ordembancaria-por-documento'), $sfpadrao_id);
            DB::commit();
            return response()->json([
                'html' => view('backpack::base.mod.apropriacao.modal-ordem-bancaria-accordion', compact('ordensBancariasAgrupadas'))->render()
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error(__METHOD__ . ' ' . $e->getMessage() . ' documento: ' . $numDocumento . ' sfpadrao_id: ' . $sfpadrao_id);
            return response()->json([
                'error' => 'Ordem Bancária não encontrada.'
            ]);
        }
    }
}
