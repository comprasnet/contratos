<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;

class CNBS
{
    public const API_SERVICE_DEFAULT = 'https://cnbs.estaleiro.serpro.gov.br/';
    /**
     * @var bool|mixed|string
     */
    private static $apiServico;

    public function __construct()
    {
        self::$apiServico = env('API_CATALOGO_COMPRAS', self::API_SERVICE_DEFAULT);
    }

    public static function getPdmMaterialByCode($code)
    {
        self::$apiServico = env('API_CATALOGO_COMPRAS', self::API_SERVICE_DEFAULT);
        $cacheKey = 'cnbs_get_pdm_material_by_code_' . $code;
        $pdmMaterial = Cache::get($cacheKey);
        if (is_null($pdmMaterial)) {
            $url = self::$apiServico . 'cnbs-api/material/v1/recuperaDadosItemMaterialPorCodigo';
            $client = new Client();
            $response = $client->get($url, ['query' => ['codigo_item_material' => $code]]);
            if ($response->getStatusCode() != 200) {
                throw new Exception('Erro ao acessar Catalogo');
            }
            $json = json_decode($response->getBody());
            if (!is_null($json)) {
                $pdmMaterial = $json;
                Cache::put($cacheKey, $pdmMaterial, 60 * 5); // cache for 5 minutes
            }
        }
        return $pdmMaterial;
    }
}
