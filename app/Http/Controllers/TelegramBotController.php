<?php

namespace App\Http\Controllers;

use App\Models\BackpackUser;
use App\Models\Jobs;
use App\Models\PgStatActivity;
use Illuminate\Encryption\Encrypter;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

class TelegramBotController extends Controller
{
    public function verificarJobsTravados(): void
    {
        // Verifica se há mais de 30 jobs em execução há mais de 15 minutos
        $jobsData = Jobs::selectRaw('COUNT(*) as job_count, CASE
            WHEN COUNT(*) > '.env('TELEGRAM_QTD_JOBS', 30).'
                AND MIN(EXTRACT(EPOCH FROM NOW()) - created_at) / 60 > '.env('TELEGRAM_TEMPO_JOBS', 15).'
                THEN TRUE
            ELSE FALSE
            END AS result')
            ->first();

        $jobsBlocked = $jobsData->result;
        $jobCount    = $jobsData->job_count;

        if ($jobsBlocked) {
            $ambiente = config('app.app_amb');
            $node = config('app.server_node');
            // Obtém os IDs do Telegram
            $telegramIds = $this->getTelegramIds();

            // Envia mensagem se houver jobs bloqueados
            $this->sendTelegramMessage($telegramIds, "<b>JOBS TRAVADOS:</b>\nExistem <b>$jobCount</b> ".
                ' jobs em execução cujo tempo é maior do que <b>'.env('TELEGRAM_TEMPO_JOBS', 15) .
                "</b> minutos.\nAmbiente: [" . $ambiente . "]\nVM: [" . $node . "]");
        }
    }
    public function verificarQueriesTravadas(): void
    {
        // Verifica se há queries em execução há mais de 15 minutos
        $intervaloMinutos = env('TELEGRAM_TEMPO_QUERIES', 15);
        $queriesBlocked = PgStatActivity::where('state', 'active')
                ->whereRaw("query_start < NOW() - INTERVAL '$intervaloMinutos minutes'")
                ->get();

        $numQueries = $queriesBlocked->count();
        $pidList = $queriesBlocked->pluck('pid')->implode(', ');

        // Envia mensagem se houver queries bloqueadas
        if ($numQueries > 0) {
            $ambiente = config('app.app_amb');
            $node = config('app.server_node');
            // Obtém os IDs do Telegram
            $telegramIds = $this->getTelegramIds();

            $message = "<b>CONSULTAS LENTAS:</b>\nExistem <b>$numQueries</b> " . 'consultas em execução cujo tempo ' .
                "é maior do que <b>$intervaloMinutos</b> minutos.\nAmbiente: [" . $ambiente .
                "]\nVM: [" . $node .
                "]\nPIDs: " . $pidList;

            $this->sendTelegramMessage($telegramIds, $message);
        }
    }

    //Método para enviar mensagens para o Telegram
    private function sendTelegramMessage($chatIds, $message)
    {
        $guzzleClient = new Client([
            'verify' => false,
        ]);
        foreach ($chatIds as $id) {
            try {
                $Encrypter = new Encrypter(base64_decode(substr(env('TELEGRAM_KEY'), 7)), 'AES-256-CBC');

                $guzzleClient->post('https://api.telegram.org/bot'.env('TELEGRAM_BOT_TOKEN').'/sendMessage', [
                    'json' => [
                        'chat_id' => $Encrypter->decryptString($id),
                        'parse_mode' => 'HTML',
                        'text' => $message,
                    ],
                ]);

            } catch (\Exception $e) {
                Log::error($e);
            }
        }
    }

    /**
     * @return array
     */
    public function getTelegramIds(): array
    {
        return BackpackUser::whereNotNull('telegram_id')
            ->groupBy(['telegram_id'])
            ->pluck('telegram_id')
            ->toArray();
    }

}
