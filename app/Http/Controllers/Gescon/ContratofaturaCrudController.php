<?php

namespace App\Http\Controllers\Gescon;

use App\Http\Traits\Formatador;
use App\Models\Contrato;
use App\services\Fornecedor\FornecedorSicafService;
use App\services\ContratoFaturaService;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ContratofaturaRequest as StoreRequest;
use App\Http\Requests\ContratofaturaRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use App\Http\Traits\BuscaCodigoItens;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * Class ContratofaturaCrudController
 *
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ContratofaturaCrudController extends CrudController
{
    use Formatador;
    use BuscaCodigoItens;

    protected $contratoFaturaService;
    protected $fornecedorSicafService;
    public function __construct(ContratoFaturaService $contratoFaturaService, FornecedorSicafService $fornecedorSicafService)
    {
        parent::__construct();
        $this->contratoFaturaService = $contratoFaturaService;
        $this->fornecedorSicafService = $fornecedorSicafService;
    }

    public function setup()
    {
        $contrato_id = \Route::current()->parameter('contrato_id');

        if (
            backpack_user()->hasRole('Setor Contratos') ||
            backpack_user()->hasRole('Execução Financeira') ||
            backpack_user()->hasRole('Administrador') ||
            backpack_user()->hasRole('Administrador Órgão') ||
            backpack_user()->hasRole('Administrador Unidade') ||
            backpack_user()->hasRole('Administrador Suporte') ||
            backpack_user()->hasRole('Almoxarifado')) {
            $contrato = Contrato::where('id', '=', $contrato_id)
                ->where('contratos.elaboracao', 'false')
                ->first();
        } else {
            $contrato = Contrato::where('id', '=', $contrato_id)
                ->whereHas('responsaveis', function ($query) {
                    $query->where('user_id', '=', backpack_user()->id);
                })
                ->where('contratos.elaboracao', 'false')
                ->first();
        }


        if (!$contrato) {
            abort('403', config('app.erro_permissao'));
        }

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        // */
        $this->crud->setModel('App\Models\Contratofatura');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/gescon/contrato/' . $contrato_id . '/faturas');
        $this->crud->setEntityNameStrings('Instrumento de Cobrança do Contrato', 'Instrumento de Cobrança do Contrato');
        $this->crud->setCreateContentClass('col-md-12');
        $this->crud->setEditContentClass('col-md-12');

        #238
        $this->contratoFaturaService->getBotaoVoltar($this->crud);

        $this->crud->addClause('join', 'tipolistafatura', 'tipolistafatura.id', '=', 'contratofaturas.tipolistafatura_id');
        $this->crud->addClause('select', 'contratofaturas.*');
        $this->crud->addClause('where', 'contrato_id', '=', $contrato_id);

        $this->crud->setCreateView('vendor.backpack.crud.contrato.contratofatura.create');
        $this->crud->setEditView('vendor.backpack.crud.contrato.contratofatura.edit');

        $this->crud->addButton('line', 'delete', 'view', 'crud::buttons.delete_inst_cobranca', 'end');
        $this->crud->enableExportButtons();

        $this->crud->denyAccess('create');
        $this->crud->allowAccess('show');

        if (backpack_user()->hasRole('Setor Contratos') ||
            backpack_user()->hasRole('Administrador') ||
            backpack_user()->hasRole('Administrador Órgão') ||
            backpack_user()->hasRole('Administrador Unidade') ||
            backpack_user()->hasRole('Administrador Suporte') ||
            backpack_user()->hasRole('Almoxarifado')) {
            $this->crud->AllowAccess('create');
            $this->crud->AllowAccess('update');
            $this->crud->AllowAccess('delete');
        }

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $colunas = $this->contratoFaturaService->colunas();
        $this->crud->addColumns($colunas);

        $con = $contrato->where('id', '=', $contrato_id)
            ->pluck('numero', 'id')
            ->toArray();

        $tipolistafatura = $this->contratoFaturaService->getTipoListaFatura($contrato_id);

        $campos = $this->contratoFaturaService->campos($con, $tipolistafatura, $contrato_id, $this->crud, "contrato");
        $this->crud->addFields($campos);

        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        try {
            DB::beginTransaction();

            $request = $this->contratoFaturaService->preparaDados($request);
            if (isset($request['error']) && $request['error']) {
                return redirect()->back()->withInput();
            }

            $redirect_location = parent::storeCrud($request);

            $id_fatura = $this->crud->entry->id;

            $this->contratoFaturaService->updateOrCreateContratoFaturasMesAno($request->mes_ano_json, $id_fatura);
            $this->contratoFaturaService->updateOrCreateContratoFaturasEmpenho($request->empenho_json, $id_fatura);
            $this->contratoFaturaService->updateOrCreateContratoItensFaturados($request, $id_fatura);
            DB::commit();

            return $redirect_location;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error(__METHOD__ .' line: '.__LINE__ .' Erro ao cadastrar o instrumento de Cobrança '.$e->getMessage());
            \Alert::error('Erro ao cadastrar o instrumento de Cobrança.')->flash();
            return redirect()->back()->withInput();
        }
    }

    public function update(UpdateRequest $request)
    {
        try {
            DB::beginTransaction();

            $request = $this->contratoFaturaService->preparaDados($request);

            if (isset($request['errorUpdate']) && $request['errorUpdate']) {
                return redirect('/gescon/contrato/' . \Route::current()->parameter('contrato_id') . '/faturas');
            }

            $redirect_location = parent::updateCrud($request);

            $id_fatura = $this->crud->entry->id;

            $this->contratoFaturaService->updateOrCreateContratoFaturasMesAno($request->mes_ano_json, $id_fatura, $request->ids_contratofaturasmesano);
            $this->contratoFaturaService->updateOrCreateContratoFaturasEmpenho($request->empenho_json, $id_fatura, $request->ids_contratofaturasempenho);
            $this->contratoFaturaService->updateOrCreateContratoItensFaturados($request, $id_fatura);
            DB::commit();

            return $redirect_location;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error(__METHOD__ .' line: '.__LINE__ .' Erro ao atualizar o instrumento de Cobrança '.$e->getMessage());
            \Alert::error('Erro ao atualizar o instrumento de Cobrança.')->flash();
            return redirect()->back()->withInput();
        }
    }

    public function show($id)
    {
        $content = parent::show($id);

        $this->crud->removeColumn('contrato_id');
        $this->crud->removeColumn('tipolistafatura_id');
        $this->crud->removeColumn('justificativafatura_id');
        $this->crud->removeColumn('sfpadrao_id');
        $this->crud->removeColumn('valor');
        $this->crud->removeColumn('juros');
        $this->crud->removeColumn('multa');
        $this->crud->removeColumn('glosa');
        $this->crud->removeColumn('valorliquido');
        $this->crud->removeColumn('user_id');

        return $content;
    }

    public function destroy($id)
    {
        $this->crud->hasAccessOrFail('delete');

        $id = $this->crud->getCurrentEntryId() ?? $id;

        $deletar = $this->contratoFaturaService->deletar($id);

        if (isset($deletar['error']) && $deletar['error'])  return response()->json(['status' => 'error']);

        return $this->crud->delete($id);
    }
}
