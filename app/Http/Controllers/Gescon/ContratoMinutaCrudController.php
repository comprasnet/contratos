<?php

namespace App\Http\Controllers\Gescon;

use App\Models\Codigoitem;
use App\Models\Contrato;
use App\Models\Contratoempenho;
use App\Models\Contratohistorico;
use App\Models\Contratoitem;
use App\Models\ContratoMinuta;
use App\Models\Contratoresponsavel;
use App\Models\Estado;
use App\Models\Municipio;
use App\Models\Orgao;
use App\Models\OrgaoSuperior;
use App\Models\Unidade;
use App\Http\Requests\Contrato_minutaRequest as StoreRequest;
use App\Http\Requests\Contrato_minutaRequest as UpdateRequest;
use App\Http\Traits\ValidarTermoPNCP;
use App\User;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\CrudPanel;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;


/**
 * Class Contrato_minutaCrudController
 * @package App\Http\Controllers\Gescon
 * @property-read CrudPanel $crud
 */
class ContratoMinutaCrudController extends CrudController
{
    use ValidarTermoPNCP;

    protected $tela_atual ='';

    private function montarInformacaoContrato() {
        $contrato_id = \Route::current()->parameter('contrato_id');
        $contrato = Contrato::with('tipo','unidadeorigem','unidade','itens.grupo','categoria','fornecedor','amparosLegais','responsaveis', 'minutasempenho.fornecedor_empenho')->where('id', '=', $contrato_id)->get();

        return $contrato;
    }

    public function setup()
    {
        //Proibir este controller para contrato do tipo Empenho(Gerado por uma Minuta de Empenho do Tipo contrato com força de empenho)
        if(Contrato::find(\Route::current()->parameter('contrato_id'))->tipo->descricao === "Empenho") {
            abort('403', config('app.erro_permissao'));
        }

        if(!backpack_user()->hasRole('Administrador') && !(backpack_user()->hasRole('Setor Contratos') && Orgao::permissaoSei())){
            abort('403', config('app.erro_permissao'));
        }

        $contrato_id = \Route::current()->parameter('contrato_id');
        $contrato = $this->montarInformacaoContrato();

        if (!$contrato) {
            abort('403', config('app.erro_permissao'));
        }
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\ContratoMinuta');
        $this->crud->setListView('backpack::crud.minutas.list'  );//nova list view
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/gescon/contrato/' . $contrato_id . '/minutas');
        $this->crud->setEntityNameStrings('Minuta de Documento', 'Minuta de Documento');
        $this->crud->addClause('where', 'contrato_id', '=', $contrato_id);
        $this->crud->addButtonFromView('top', 'voltar', 'voltarcontrato', 'end');

        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');
        if(backpack_user()->can('administrar_padroes_de_minutas') == true){
            $this->crud->allowAccess('delete');
            $this->crud->addButtonFromView('line', 'Alteração da minuta de documento', 'alterarminutadocumento', 'end'); //alteração buttons
            $this->crud->addButtonFromView('line', 'Apagar minuta de documento', 'deletarminutadocumento', 'end');//delete buttons
        }

        $this->crud->addButtonFromView('line', 'contratominutaenviosei', 'contratominutaenviosei', 'end'); //envio sei buttons
        $this->crud->addButtonFromView('line', 'Ver link SEI', 'verlinksei', 'end');//envio sei buttons
        $this->crud->addButtonFromView('line', 'Gerar PDF', 'gerarpdf', 'end');//gerar pdf
        $this->crud->enableExportButtons();
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration Global
        |--------------------------------------------------------------------------
        */

        $this->crud->denyAccess('create');
        $this->crud->allowAccess('show');

        if((backpack_user()->can('contrato_inserir')) || Orgao::permissaoSei()) {
            $this->crud->allowAccess('create');
        }

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */
        $this->montaJsonTokens($contrato);

        $campos = $this->Campos($contrato);
        $colunas = $this->Colunas($contrato);
        $this->crud->addColumns($colunas);
        $this->crud->addFields($campos);

        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    private function montaJsonTokens($contrato){

        $con = $contrato->first();
        $item = (is_null($con->itens)) ? 'vazio' : $con->itens->first();

        $numero_compra = (is_null($con->numero_compra)) ? 'vazio' : $con->numero_compra;
        $amparo_legal = (is_null($con->amparoslegais)) ? 'vazio' : $con->amparoslegais->first();

        $fornecedor = $con->fornecedor->first();
        if (is_null($fornecedor) == false) {
            $cpf_cnpj_idgener = $fornecedor->cpf_cnpj_idgener;
            $fornecedor_nome = $fornecedor->nome;
        }else{
            $fornecedor_nome = 'Indisponível';
            $cpf_cnpj_idgener = 'Indisponível';
        }
        $minutasempenho = $con->minutasempenho->first();

        if(is_null($minutasempenho) == false){
            $credorEmpenho = (is_null($minutasempenho->fornecedor_empenho) == false)? $minutasempenho->fornecedor_empenho->nome : 'Indisponível';
            $uasgCompraEmpenho = (is_null($minutasempenho->compra) == false) ? $minutasempenho->compra->unidade_origem_id : 'Indisponível';
            $tipoCompraEmpenho = (is_null($minutasempenho->compra) == false) ? $minutasempenho->compra->modalidade->descricao : 'Indisponível';
            $numeroAnoEmpenho = (is_null($minutasempenho->fornecedor_empenho) == false) ? $minutasempenho->compra->numero_ano : 'Indisponível';
            $valorTotalEmpenho = $minutasempenho->valor_total;
            $tipoEmpenho = (is_null($minutasempenho->tipo_empenho) == false) ? $minutasempenho->tipo_empenho->descricao : 'Indisponível';
        }else{
            $credorEmpenho = 'Indisponível';
            $uasgCompraEmpenho = 'Indisponível';
            $tipoCompraEmpenho = 'Indisponível';
            $numeroAnoEmpenho = 'Indisponível';
            $valorTotalEmpenho = 'Indisponível';
            $tipoEmpenho = 'Indisponível';
        }

        if(is_null($con->responsaveis->first()) == false){
            $usuarioResponsavel = $con->responsaveis->first()->user->name;
            $funcaoResponsavel = $con->responsaveis->first()->funcao->descricao;
        }else{
            $usuarioResponsavel = 'Indefinido';
            $funcaoResponsavel = 'Indefinido';
        }

        $tokens = [
            //TOKENS CONTRATO
            '##numeroInstrumentoContrato##' =>  $con->numero,
            '##unidadeGestoraOrigemContrato##' =>  $con->getUnidadeOrigem(),
            '##unidadeGestoraAtualContrato##' =>  $con->getUnidade(),
            '##unidadesRequisitantesContrato##' =>  $con->unidades_requisitantes,
            '##tipoContrato##' =>  $con->getTipo(),
            '##categoriaContrato##' =>  $con->getCategoria(),
            '##processoContrato##' =>  $con->processo,
            '##objetoContrato##' =>  $con->objeto,
            '##inicioVigenciaContrato##' =>  Carbon::createFromFormat('Y-m-d', $con->vigencia_inicio)->format('d/m/Y'),
            '##fimVigenciaContrato##' =>  $con->vigencia_fim ? Carbon::createFromFormat('Y-m-d', $con->vigencia_fim)->format('d/m/Y') : null,
            '##valorGlobalContrato##' =>  $con->formatVlrGlobal(),
            '##numeroParcelasContrato##' =>  "$con->num_parcelas",
            '##valorParcelasContrato##' =>  $con->formatVlrParcela(),
            '##prorrogavelContrato##' =>  'indisponivel',
            '##fornecedorContrato##' =>  $con->getFornecedor(),
            //ITENS
            '##itemGrupo##' =>  @$item->grupo->descricao,
            '##item##' =>  @$item->item->descricao,
            '##descricaoComplementar##' =>  @$item->descricao_complementar,
            '##quantidade##' =>  @$item->quantidade,
            '##dataInicio##' =>  @$item->data_inicio,
            '##valorUnitario##' =>  @$item->valorunitario,
            '##valorTotal##' =>  @$item->valortotal,
            //FORNECEDOR
            '##cnpj##' =>  $cpf_cnpj_idgener,
            '##razaoSocial##' =>  $fornecedor_nome,
            //COMPRA
            '##numeroCompraContrato##' =>  $numero_compra,
            '##uasgCompraContrato##' =>  'não encontrado',
            //AMPARO LEGAL
            '##atoNormativo##' =>  (is_null($amparo_legal) == false)? $amparo_legal->ato_normativo : '',
            '##artigo##' => (is_null($amparo_legal) == false) ? $amparo_legal->artigo : '',
            '##paragrafo##' => (is_null($amparo_legal) == false) ?  $amparo_legal->paragrafo : '',
            '##inciso##' => (is_null($amparo_legal) == false) ? $amparo_legal->inciso : '',
            '##alinea##' => (is_null($amparo_legal) == false) ? $amparo_legal->alinea : '',
            //RESPONSAVEIS CONTRATO
            '##usuarioResponsavel##' =>  $usuarioResponsavel,
            '##funcaoResponsavel##' =>  $funcaoResponsavel,
            //EMPENHO
            '##credorEmpenho##' =>  $credorEmpenho,
            '##uasgCompraEmpenho##' =>  $uasgCompraEmpenho,
            '##tipoCompraEmpenho##' =>  $tipoCompraEmpenho,
            '##numeroAnoEmpenho##' =>  $numeroAnoEmpenho,
            '##tipoMinutaEmpenho##' =>  'Indisponível(*)',//verificar onde encontrar isto
            '##tipoEmpenho##' =>  $tipoEmpenho,
            '##valorTotalEmpenho##' =>  $valorTotalEmpenho,

        ];

        $this->data['tokens'] = json_encode($tokens);
    }

    public function Colunas($contrato){
        $colunas = [
            [
                'name' => 'getNumeroContrato',
                'label' => 'Número do instrumento',
                'type' => 'model_function',
                'function_name' => 'getNumeroContrato',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'getNumeroProcesso',
                'label' => 'Número do processo',
                'type' => 'model_function',
                'function_name' => 'getNumeroProcesso',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'getTipoDeDocumento',
                'label' => 'Tipo de doc.',
                'type' => 'model_function',
                'function_name' => 'getTipoDeDocumento',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'getDescricaoModeloDocumento',
                'label' => 'Modelo de doc.',
                'type' => 'model_function',
                'function_name' => 'getDescricaoModeloDocumento',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'data',
                'type' => 'date',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'getStatus',
                'label' => 'Status',
                'type' => 'model_function',
                'function_name' => 'getStatus',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'numero_super_sei',
                'label' => 'Nº Super(SEI)',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ]

        ];
        return $colunas;
    }

    public function getNomeFuncaoResponsavelContrato($contrato_id, $user_id){
        return $nomeFuncao = Contratoresponsavel::where('contrato_id', $contrato_id)
            ->where('user_id', $user_id)
            ->join('codigoitens as ci', 'ci.id', 'contratoresponsaveis.funcao_id')
            ->first()
            ->descricao;
    }

    public function getTermoDeEncerramento($contrato_id){
        return $objTermoEncerramento = Contratohistorico::where('contrato_id', $contrato_id)
            ->join('codigoitens as ci', 'ci.id', 'contratohistorico.tipo_id')
            ->join('codigos as c', 'c.id', 'ci.codigo_id')
            ->where('ci.descricao', 'like', 'Termo de Encerramento')
            ->where('c.descricao', 'Tipo de Contrato')
            ->first();
    }

    public function getTextoPdfGeradoEncerramento($contrato_id){

        // dados buscados
        $contrato = Contrato::find($contrato_id);
        $numeroContrato = $contrato->numero;
        $dataEncerramentoContrato = self::formataDataParaPadraoBrasileiro($contrato->data_encerramento);

        // buscar dados do responsável signatário encerramento
        $objUser = User::find($contrato->user_id_responsavel_signatario_encerramento);
        $nomeSignatarioEncerramento = $objUser->name;
        $nomeFuncaoSignatarioEncerramento = self::getNomeFuncaoResponsavelContrato($contrato_id, $contrato->user_id_responsavel_signatario_encerramento);

        $isObjetoContratualEntregue = $contrato->is_objeto_contratual_entregue ? 'Sim' : 'Não';
        $isCumpridasObrigacoesFinanceiras = $contrato->is_cumpridas_obrigacoes_financeiras ? 'Sim' : 'Não';

        if($contrato->is_saldo_conta_vinculada_liberado === true){$is_saldo_conta_vinculada_liberado = 'Sim';}
        elseif($contrato->is_saldo_conta_vinculada_liberado === false){$is_saldo_conta_vinculada_liberado = 'Não';}
        else{$is_saldo_conta_vinculada_liberado = 'Não aplicável';}

        if($contrato->is_garantia_contratual_devolvida === true){$is_garantia_contratual_devolvida = 'Sim';}
        elseif($contrato->is_garantia_contratual_devolvida === false){$is_garantia_contratual_devolvida = 'Não';}
        else{$is_garantia_contratual_devolvida = 'Não aplicável';}

        if($contrato->planejamento_contratacao_atendida === true){$planejamento_contratacao_atendida = 'Sim';}
        elseif($contrato->planejamento_contratacao_atendida === false){$planejamento_contratacao_atendida = 'Não';}
        else{$planejamento_contratacao_atendida = 'Não aplicável';}

        if($contrato->grau_satisfacao_desempenho_contrato == 0){$grau_satisfacao_desempenho_contrato = 'Muito baixo';}
        elseif($contrato->grau_satisfacao_desempenho_contrato == 1){$grau_satisfacao_desempenho_contrato = 'Baixo';}
        elseif($contrato->grau_satisfacao_desempenho_contrato == 2){$grau_satisfacao_desempenho_contrato = 'Médio';}
        elseif($contrato->grau_satisfacao_desempenho_contrato == 3){$grau_satisfacao_desempenho_contrato = 'Alto';}
        elseif($contrato->grau_satisfacao_desempenho_contrato == 4){$grau_satisfacao_desempenho_contrato = 'Muito alto';}

        $objInstrumentoInicial = $contrato->getInstrumentoInicial();
        $vigenciaInicio = self::formataDataParaPadraoBrasileiro($objInstrumentoInicial->vigencia_inicio);
        $vigenciaFim = self::formataDataParaPadraoBrasileiro($objInstrumentoInicial->vigencia_fim);

        $objTermoEncerramento = self::getTermoDeEncerramento($contrato_id);
        $numeroProcessoTermoEncerramento = $objTermoEncerramento->processo;

        // tratar, pois chega aqui com várias linhas
        $sugestao_licao_aprendida = $contrato->sugestao_licao_aprendida;
        $sugestao_licao_aprendida = str_replace("<p>", "", $sugestao_licao_aprendida);
        $sugestao_licao_aprendida = str_replace("</p>", "", $sugestao_licao_aprendida);
        $sugestao_licao_aprendida = str_replace("–", "-", $sugestao_licao_aprendida);       //tratando hífen
        $sugestao_licao_aprendida = str_replace('“', '"', $sugestao_licao_aprendida);       //tratando aspas especiais iniciais
        $sugestao_licao_aprendida = str_replace('”', '"', $sugestao_licao_aprendida);       //tratando aspas especiais finais
        $sugestao_licao_aprendida = str_replace('—', '-', $sugestao_licao_aprendida);       //tratando travessão
        $sugestao_licao_aprendida = strip_tags($sugestao_licao_aprendida);
        $sugestao_licao_aprendida = nl2br($sugestao_licao_aprendida);
        $sugestao_licao_aprendida = str_replace("\r\n", "", $sugestao_licao_aprendida);
        $arraySugestaoLicaoAprendida = explode("<br />", $sugestao_licao_aprendida);

        // tratar, pois chega aqui com várias linhas
        $observacaoTermoEncerramento2 = $objTermoEncerramento->observacao;
        $observacaoTermoEncerramento = nl2br($objTermoEncerramento->observacao);
        $observacaoTermoEncerramento = str_replace("\r\n", "", $observacaoTermoEncerramento);
        $observacaoTermoEncerramento = str_replace("–", "-", $observacaoTermoEncerramento);     //tratando hífen
        $observacaoTermoEncerramento = str_replace('“', '"', $observacaoTermoEncerramento);     //tratando aspas especiais iniciais
        $observacaoTermoEncerramento = str_replace('”', '"', $observacaoTermoEncerramento);     //tratando aspas especiais finais
        $observacaoTermoEncerramento = str_replace('—', '-', $observacaoTermoEncerramento);       //tratando travessão
        $arrayObservacaoTermoEncerramento = explode("<br />", $observacaoTermoEncerramento);

        // tratar, pois chega aqui com várias linhas
        $isMostrarConsecucao = true;
        if( !is_null($contrato->consecucao_objetivos_contratacao) ){
            $consecucao_objetivos_contratacao = nl2br($contrato->consecucao_objetivos_contratacao);
            $consecucao_objetivos_contratacao = str_replace("\r\n", "", $consecucao_objetivos_contratacao);
            $consecucao_objetivos_contratacao = str_replace("–", "-", $consecucao_objetivos_contratacao);       //tratando hífen
            $consecucao_objetivos_contratacao = str_replace('“', '"', $consecucao_objetivos_contratacao);       //tratando aspas especiais iniciais
            $consecucao_objetivos_contratacao = str_replace('”', '"', $consecucao_objetivos_contratacao);       //tratando aspas especiais finais
            $consecucao_objetivos_contratacao = str_replace('—', '-', $consecucao_objetivos_contratacao);       //tratando travessão
            $arrayConsecucao = explode("<br />", $consecucao_objetivos_contratacao);
        } else {
            $isMostrarConsecucao = false;
        }

        $nomeUasg = $contrato->unidade->nome;
        $nomeResumidoUasg = $contrato->unidade->nomeresumido;
        $codigoUasg = $contrato->unidade->codigo;
        $codigoOrgao = $contrato->unidade->orgao->codigo;
        $nomeOrgao = $contrato->unidade->orgao->nome;

        $nomeFornecedorContrato = $contrato->fornecedor->nome;

        $codigoUnidadeGestora = session()->get('user_ug');
        $unidade = Unidade::where('codigo', $codigoUnidadeGestora)->first();
        $idMunicipio = $unidade->municipio_id;
        $municipio = Municipio::find($idMunicipio);
        $nomeMunicipio = $municipio->nome;

        $idEstado = $municipio->estado_id;
        $estado = Estado::find($idEstado);
        $siglaEstado = $estado->sigla;
        $nomeEstado = $estado->nome;

        $dataHoje = self::formataDataParaPadraoBrasileiro(date("Y-m-d"));

        $contratanteUasg = $codigoUasg . ' - ' . $nomeUasg;
        $contratanteOrgao = $codigoOrgao . ' - ' . $nomeOrgao;
        $contratada = $nomeFornecedorContrato;

        $assinaturaCidade =  $nomeMunicipio.'/'.$siglaEstado.', '.$dataEncerramentoContrato.' - Unidade Gestora: '.$codigoUnidadeGestora;
        $assinaturaNome =

        $textoPdfGeradoEncerramento = '
        RELATÓRIO FINAL DO CONTRATO No. '.$numeroContrato.' - UASG '.$codigoUnidadeGestora.'
        <br><br>Contrato No.: '.$numeroContrato.'
        <br>Contratante: '.$contratanteUasg.'
        <br>'.$contratanteOrgao.'
        <br>Contratada: '.$contratada.'
        <br>Número do Processo: '.$numeroProcessoTermoEncerramento.'
        <br>Vigência início: '.$vigenciaInicio.'
        <br>Vigência fim: '.$vigenciaFim.'
        <br><br>O objeto contratual foi entregue à Contratante em sua totalidade? <br>'.$isObjetoContratualEntregue.'
        <br><br>Foram cumpridads todas as obrigações financeiras junto à Contratada? <br>'.$isCumpridasObrigacoesFinanceiras.'
        <br><br>O saldo remanescente dos recursos depositados na Conta-Depósito Vinculada foi liberado para o Fornecedor até o encerrameno do contrato? <br>'.$is_saldo_conta_vinculada_liberado.'
        <br><br>A gatantia contratual foi integralmente devolvida para a Contratada? <br>'.$is_garantia_contratual_devolvida.'
        <br><br>Grau de satisfação com o desempenho do contrato: <br>'.$grau_satisfacao_desempenho_contrato.'
        <br><br>A necessidade formalizada no Planejamento da Contratação (DOD/DFD/ETP/TR/PB) foi plenamente atendida pelo contrato? <br>'.$planejamento_contratacao_atendida.'
        <br><br>Informações sobre a consecução dos objetivos que tenham justificado a contratação: <br>'.$consecucao_objetivos_contratacao.'
        <br><br>Eventuais condutas a serem adotadas para o aprimoramento das atividades da Administração: <br>'.$sugestao_licao_aprendida.'
        <br><br>Observação: <br>'.$observacaoTermoEncerramento.'
        <br><br>'.$assinaturaCidade.'
        <br><br><br>_____________________________________________________
        <br>Documento assinado eletronicamente
        <br>'.$nomeSignatarioEncerramento.'
        <br>'.$nomeFuncaoSignatarioEncerramento.'
        ';

        return $textoPdfGeradoEncerramento;

    }

    public function formataDataParaPadraoBrasileiro($dataPadraoAmericano){
        /**
         * Vamos pegar a data recebida, quebrá-la em aaaa, mm, dd
         * e fazer a nova formatação
         */
        $dia = substr($dataPadraoAmericano, 8, 2);
        $mes = substr($dataPadraoAmericano, 5, 2);
        $ano = substr($dataPadraoAmericano, 0, 4);
        return $dia."/".$mes."/".$ano;
    }

    public function getNumeroTermoEncerramento($contrato_id){
        return $numeroTermoEncerramento = Contratohistorico::where('contrato_id', $contrato_id)
            ->join('codigoitens as ci', 'ci.id', 'contratohistorico.tipo_id')
            ->join('codigos as c', 'c.id', 'ci.codigo_id')
            ->where('ci.descricao', 'like', 'Termo de Encerramento')
            ->where('c.descricao', 'Tipo de Contrato')
            ->first()->numero;
    }

    public function Campos($contrato)
    {
        $vemDoEncerramento = session()->get('vemDoEncerramento');
        $textoPdfGeradoEncerramento = null;

        if($vemDoEncerramento){
            $contrato_id = \Route::current()->parameter('contrato_id');
            $tipos_documento =  Codigoitem::whereHas('codigo', function ($q){
                $q->where('descricao', '=', 'Tipos Documentos SEI');
            })
            ->where('codigoitens.descricao', 'Relatório Final')
            ->get()->pluck('descricao','id')->toArray();
            $numeroTermoEncerramento = self::getNumeroTermoEncerramento($contrato_id);

            $status_minuta =  Codigoitem::whereHas('codigo', function ($q){ //inclusao para evitar da criação da coluna nova no Codigo Itens
                $q->where('descricao', '=', 'Status Contrato Minuta SEI');
            })
            ->where('descricao','=','Em elaboração')
            ->orWhere('descricao','=','Pronto para envio')
            ->get()->pluck('descricao','id')->toArray();

            $textoPdfGeradoEncerramento = self::getTextoPdfGeradoEncerramento($contrato_id);

        } else {
            session()->forget('vemDoEncerramento');

            // como era:
            $status_minuta =  Codigoitem::whereHas('codigo', function ($q){ //inclusao para evitar da criação da coluna nova no Codigo Itens
                $q->where('descricao', '=', 'Status Contrato Minuta SEI');
            })
            ->where('descricao','<>','Erro')
            ->where('descricao', '<>', 'Enviado')
            ->get()->pluck('descricao','id')->toArray();

            $tipos_documento =  Codigoitem::whereHas('codigo', function ($q){ //inclusao para evitar da criação da coluna nova no Codigo Itens
                $q->where('descricao', '=', 'Tipos Documentos SEI');
            })->get()->pluck('descricao','id')->toArray();
        }

        $campos_bloqueados_na_visao_edit = $this->camposBloqueadosNaVisaoEdit($tipos_documento,$contrato);

        $num_instrumento = $contrato->pluck('numero','id')->toArray();
        $num_processo = $contrato->first()->processo;

        $campos = [

            [ // select_from_array
                'name' => 'contrato_id',
                'label' => "Número do instrumento",
                'type' => 'select_from_array',
                'options' => $num_instrumento,
                'allows_null' => false,
                'wrapperAttributes' => [
                    'class' => 'form-group col-xs-12 col-md-4'
                ],
                'attributes' => [
                    'readonly' => "true"
                ]
            ],
            //numero processo
            [
                'name' => 'processo',
                'label' => 'Número Processo',
                'type' => 'numprocesso',
                'default' => $num_processo,
                'wrapperAttributes' => [
                    'class' => 'form-group col-xs-12 col-md-4'
                ],
                'attributes' => [
                    'readonly' => "true"
                ]
            ],
            [
                'name' => 'data',
                'label' => 'Data',
                'type' => 'date',
                'default' => Date('Y-m-d'),
                'attributes' => [
                    'readonly' => "true"
                ],
                'wrapperAttributes' => [
                    'class' => 'form-group col-xs-12 col-md-4'
                ]
            ],
            $campos_bloqueados_na_visao_edit[0],
            $campos_bloqueados_na_visao_edit[1],
            $campos_bloqueados_na_visao_edit[2],
            $campos_bloqueados_na_visao_edit[3],

            [
                'name' => 'numero_termo_contrato',
                'label' => 'Número do Termo',
                'type' => 'text',
                'default' => $vemDoEncerramento ? $numeroTermoEncerramento : null,
                'attributes' => [
                    'readonly' => "true",
                    'placeholder' => "Se o tipo de documento for Termo Aditivo, Termo de Apostilamento, Termo de Encerramento OU Termo de Rescisão. O campo será habilitado.",
                    'id' => 'numero_termo_contrato'
                ],
                'wrapperAttributes' => [
                    'class' => 'form-group col-xs-12 col-md-12'
                ],
            ],
            [   // CustomHTML
                'name' => 'botao_token',
                'type' => 'custom_html',
                'wrapperAttributes' => [
                    'class' => 'form-group col-xs-3'
                ],
                'value' => '<a href="#" class="btn btn-block btn-primary" id="buscaCampo" data-toggle="modal" data-target="#myModal"><span><span class="ico-badge-militar"></span> Relacionar dados do contrato</span></a>'
            ],
            [   // CustomHTML
                'name' => 'botao_empenho',
                'type' => 'custom_html',
                'wrapperAttributes' => [
                    'class' => 'form-group col-xs-3'
                ],
                'value' => '<a href="#" class="btn btn-block btn-primary" id="buscaCampo" data-toggle="modal" data-target="#myModalEmpenho"><span><span class="ico-badge-militar"></span> Relacionar empenho</span></a>'
            ],
            [   // CustomHTML
                'name' => 'botao_itens_contrato',
                'type' => 'custom_html',
                'wrapperAttributes' => [
                    'class' => 'form-group col-xs-3'
                ],
                'value' => '<a href="#" class="btn btn-block btn-primary" id="buscaCampo" data-toggle="modal" data-target="#myModalItensContrato"><span><span class="ico-badge-militar"></span> Relacionar itens</span></a>'
            ],
            [   // CustomHTML
                'name' => 'botao_responsavel_contrato',
                'type' => 'custom_html',
                'wrapperAttributes' => [
                    'class' => 'form-group col-xs-3'
                ],
                'value' => '<a href="#" class="btn btn-block btn-primary" id="buscaCampo" data-toggle="modal" data-target="#myModalResponsavelContrato"><span><span class="ico-badge-militar"></span> Relacionar responsáveis</span></a>'
            ],
            [   // CustomHTML
                'name' => 'criar_cabecalho',
                'type' => 'minuta_documento.criar_cabecalho',
                'wrapperAttributes' => [
                    'class' => 'form-group col-xs-12'
                ],
            ],
            [ // select_from_array
                'name' => 'copiar_minuta_documento',
                'label' => "Modelo de Documento",
                'type' => 'select_from_array',
                'options' => [],
                'attributes' => ['id' => 'copiar_minuta_documento',  'disabled' => 'disabled'],
                'wrapperAttributes' => [
                    'class' => 'form-group col-xs-12 col-md-12'
                ]
            ],
            [   // CKEditor
                'name' => 'texto_minuta',
                'label' => 'Texto',
                'default' => $vemDoEncerramento ? $textoPdfGeradoEncerramento : null,
                'type' => 'ckeditor',
                'extra_plugins' => ['oembed', 'widget'],
                'options' => [
                    'autoGrow_minHeight' => 600,
                    'autoGrow_bottomSpace' => 100,
                ]
            ],
            //status
            [
                'name' => 'contrato_minutas_status_id',
                'label' => "Status",
                'type' => 'select2_from_array',
                'options' => $status_minuta,
                'allows_null' => false,
                'wrapperAttributes' => [
                    'class' => 'form-group col-xs-12 col-md-4'
                ]
            ],
            [   //Quantidade mínima de assinaturas
                'name' => 'qtd_min_assinaturas',
                'label' => "Nº assinaturas previstas",
                'type' => 'select_from_array',
                'options' => ['Selecione',1,2,3,4,5,6,7,8,9,10],
                'allows_null' => false,
                'default' => 1,
                'wrapperAttributes' => [
                    'class' => 'form-group col-xs-12 col-md-4'
                ]
            ]
        ];

        return $campos;
    }

    public function camposBloqueadosNaVisaoEdit($tipos_documento){

        $vemDoEncerramento = session()->get('vemDoEncerramento');
        if($vemDoEncerramento){
            $idRelatorioFinal =  Codigoitem::whereHas('codigo', function ($q){
                $q->where('descricao', '=', 'Tipos Documentos SEI');
            })
            ->where('codigoitens.descricao', 'Relatório Final')->first()->id;
        } else {
            $idRelatorioFinal = null;
        }

        if(is_null(Request::segment(6)) == false){
            if(Request::segment(6) == 'edit'){
                $id = $this->crud->getCurrentEntryId();

                $reg = $this->crud->getEntry($id);

                $idModeloDocumento = null;
                $idCodigoItem = null;
                $idCodigoItemDescricao = null;
                $idModeloDocumentoDescricao = null;

                if (empty($reg->ModeloDocumento)) {
                    $idModeloDocumento = null;
                    $idCodigoItem = $reg->TipoDocumento->id;
                    $idCodigoItemDescricao = $reg->TipoDocumento->descricao;
                    $idModeloDocumentoDescricao = 'Padrão de Documento Não Selecionado';
                } else {
                    $idModeloDocumento = $reg->ModeloDocumento->id;
                    $idCodigoItem = $reg->ModeloDocumento->CodigoItem->id;
                    $idCodigoItemDescricao = $reg->ModeloDocumento->CodigoItem->descricao;
                    $idModeloDocumentoDescricao = $reg->ModeloDocumento->descricao;
                }

                return [
                    [   // Hidden
                        'name'  => 'tipo_modelo_documento_id',
                        'type'  => 'hidden',
                        'value' => $idModeloDocumento,
                    ],
                    [   // Hidden
                        'name'  => 'codigoitens_id',
                        'type'  => 'hidden',
                        'value' => $idCodigoItem,
                    ],
                    [
                        'name'  => 'tipo_selecionado',
                        'label' => "Tipo Documento",
                        'type'  => 'text',
                        'default' => $idCodigoItemDescricao,
                        'wrapperAttributes' => [
                            'class' => 'form-group col-xs-12 col-md-6'
                        ],
                        'attributes' => [
                            'readonly' => "true"
                        ],
                    ],
                    [
                        'name'  => 'padrao_selecionado',
                        'label' => 'Padrão de documento',
                        'type'  => 'text',
                        'default' => $idModeloDocumentoDescricao,
                        'wrapperAttributes' => [
                            'class' => 'form-group col-xs-12 col-md-6'
                        ],
                        'attributes' => [
                            'readonly' => "true"
                        ],
                    ],
                ];
            }
        }
        return [
            //Tipos de Documento
            [   // Hidden
                'name'  => 'fake',
                'type'  => 'hidden',
                'value' => 'fake',
            ],
            [   // Hidden
                'name'  => 'fake_2',
                'type'  => 'hidden',
                'value' => 'fake 2',
            ],
            [
                'label' => "Tipo Documento",
                'name' => 'codigoitens_id',
                'type' => 'select_from_array',
                'options' => $tipos_documento,
                'default' => $vemDoEncerramento ? $idRelatorioFinal : null,
                'attributes' => [
                    'id' => 'codigoitens_id'
                ],
                'wrapperAttributes' => [
                    'class' => 'form-group col-xs-12 col-md-6 select'
                ],
                'attributes' => [ 'onchange' => 'recuperarMinutaDocumentoPorTipoDocumento(this)' ],
                'allows_null' => true,
            ],
            //Padrões de Minuta (Modelos de Documento)
            [
                'name' => 'tipo_modelo_documento_id',
                'label' => "Padrões de Documento",
                'type' => 'select2_from_ajax_padrao_documento',
                'entity'=>'ModeloDocumento',
                'attribute'=>'descricao',
                'data_source'          => url('api/modelosdocumentos'),
                'placeholder'          => 'Selecione um padrão de minuta',
                'include_all_form_fields' => true,
                'minimum_input_length' => 0,
                'dependencies'         => ['codigoitens_id'],
                'wrapperAttributes' => [
                    'class' => 'form-group col-xs-12 col-md-6'
                ]
            ],
        ];
    }

    /**
     * Show the form for creating inserting a new row.
     *
     * @return Response
     */
    public function create()
    {
        $this->crud->hasAccessOrFail('create');
        $this->crud->setOperation('create');

        // prepare the fields you need to show
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getCreateFields();
        $this->data['title'] = $this->crud->getTitle() ?? trans('backpack::crud.add') . ' ' . $this->crud->entity_name;
        $this->data['dadosUnidadeCabecalho'] = $this->recuperarDadosUnidadeParaCabecalho();

        return view('vendor.backpack.crud.minutas.create', $this->data);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {

        if (backpack_user()->can('administrar_padroes_de_minutas') == true) {
            $this->crud->allowAccess('update');
            $this->crud->allowAccess('delete');
        }
        $this->crud->hasAccessOrFail('update');
        $this->crud->setOperation('update');

        // get entry ID from Request (makes sure its the last ID for nested resources)
        $id = $this->crud->getCurrentEntryId() ?? $id;

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = $this->crud->getTitle() ?? trans('backpack::crud.edit') . ' ' . $this->crud->entity_name;
        $this->data['dadosUnidadeCabecalho'] = $this->recuperarDadosUnidadeParaCabecalho();

        $this->data['id'] = $id;

        // verifica se já existe um pdf gerado para esta minuta
        // caso tenha, redireciona para lista
        if($this->data['entry']->pdf_gerado == true){
            return redirect()->to($this->data['crud']->route);
        }else{
            return view('vendor.backpack.crud.minutas.edit', $this->data);
        }
    }

    /**
     * Display all rows in the database for this entity.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $this->data['classes_controle_acesso_botoes'] = 'update delete';
        $this->crud->hasAccessOrFail('list');
        $this->crud->setOperation('list');

        $this->data['crud'] = $this->crud;
        $this->data['title'] = $this->crud->getTitle() ?? mb_ucfirst($this->crud->entity_name_plural);

        return view('vendor.backpack.crud.minutas.list', $this->data);
    }

    public function store(StoreRequest $request)
    {

        if($this->campoEmBranco($request->input('codigoitens_id'), $request->input('numero_termo_contrato'))) {
            return redirect()->back()->withInput(Input::all())->withErrors(
                [
                    "numero_termo_contrato"=>'O Número do Termo deve ser informado.',
                ]
            );
        }

        $redirect_location = parent::storeCrud($request);

        if( session()->get('vemDoEncerramento') == true ){
            $contrato_id = \Route::current()->parameter('contrato_id');
            session()->forget('vemDoEncerramento');
            return redirect('/gescon/contrato/'.$contrato_id.'/minutas');
        }

        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {

        if (backpack_user()->can('administrar_padroes_de_minutas') == true) {
            $this->crud->allowAccess('update');
        }

        $redirect_location = parent::updateCrud($request);

        return $redirect_location;
    }

    public function imprimir($contrato_id, $contrato_minuta_id)
    {
        $contratoMinuta = ContratoMinuta::find($contrato_minuta_id);

        return $contratoMinuta->converteHTMLparaPDF();
    }

    private function montarResponsaveisContrato(int $idContrato) {
        $responsaveis = Contratoresponsavel::join("users","contratoresponsaveis.user_id","=","users.id")
                                            ->join("codigoitens","contratoresponsaveis.funcao_id","=","codigoitens.id")
                                            ->where('contrato_id',$idContrato)
                                            ->selectRaw("UPPER( CONCAT(users.name,' - ',codigoitens.descricao) ) AS responsavelContrato")
                                            ->get();
        if($responsaveis) {
            return $responsaveis->toArray();
        }

        return [];
    }

    private function montaJsonTokensAjax($contrato, int $idItemContrato = 0){

        $con = $contrato->first();
        $item = (is_null($con->itens)) ? 'vazio' : $con->itens->where("id",$idItemContrato)->first();

        $numero_compra = (is_null($con->numero_compra)) ? 'vazio' : $con->numero_compra;
        $amparo_legal = (is_null($con->amparoslegais)) ? 'vazio' : $con->amparoslegais->first();

        $fornecedor = $con->fornecedor->first();
        if (is_null($fornecedor) == false) {
            $cpf_cnpj_idgener = $fornecedor->cpf_cnpj_idgener;
            $fornecedor_nome = $fornecedor->nome;
        }else{
            $fornecedor_nome = 'Indisponível';
            $cpf_cnpj_idgener = 'Indisponível';
        }
        $minutasempenho = $con->minutasempenho->first();

        if(is_null($minutasempenho) == false){
            $credorEmpenho = (is_null($minutasempenho->fornecedor_empenho) == false)? $minutasempenho->fornecedor_empenho->nome : 'Indisponível';
            $uasgCompraEmpenho = (is_null($minutasempenho->compra) == false) ? $minutasempenho->compra->unidade_origem_id : 'Indisponível';
            $tipoCompraEmpenho = (is_null($minutasempenho->compra) == false) ? $minutasempenho->compra->modalidade->descricao : 'Indisponível';
            $numeroAnoEmpenho = (is_null($minutasempenho->fornecedor_empenho) == false) ? $minutasempenho->compra->numero_ano : 'Indisponível';
            $valorTotalEmpenho = $minutasempenho->valor_total;
            $tipoEmpenho = (is_null($minutasempenho->tipo_empenho) == false) ? $minutasempenho->tipo_empenho->descricao : 'Indisponível';
        }else{
            $credorEmpenho = 'Indisponível';
            $uasgCompraEmpenho = 'Indisponível';
            $tipoCompraEmpenho = 'Indisponível';
            $numeroAnoEmpenho = 'Indisponível';
            $valorTotalEmpenho = 'Indisponível';
            $tipoEmpenho = 'Indisponível';
        }

        $tokens = [
            //TOKENS CONTRATO
            'numeroInstrumentoContrato' =>  $con->numero,
            'unidadeGestoraOrigemContrato' =>  $con->getUnidadeOrigem(),
            'unidadeGestoraAtualContrato' =>  $con->getUnidade(),
            'unidadesRequisitantesContrato' =>  $con->unidades_requisitantes,
            'tipoContrato' =>  $con->getTipo(),
            'categoriaContrato' =>  $con->getCategoria(),
            'processoContrato' =>  $con->processo,
            'objetoContrato' =>  $con->objeto,
            'inicioVigenciaContrato' =>  Carbon::createFromFormat('Y-m-d', $con->vigencia_inicio)->format('d/m/Y'),
            'fimVigenciaContrato' =>  $con->vigencia_fim ? Carbon::createFromFormat('Y-m-d', $con->vigencia_fim)->format('d/m/Y') : null,
            'valorGlobalContrato' =>  $con->formatVlrGlobal(),
            'numeroParcelasContrato' =>  "$con->num_parcelas",
            'valorParcelasContrato' =>  $con->formatVlrParcela(),
            'prorrogavelContrato' =>  'indisponível',
            'fornecedorContrato' =>  $con->getFornecedor(),
            //FORNECEDOR
            'cnpj' =>  $cpf_cnpj_idgener,
            'razaoSocial' =>  $fornecedor_nome,
            //COMPRA
            'numeroCompraContrato' =>  $numero_compra,
            'uasgCompraContrato' =>  'não encontrado',
            //AMPARO LEGAL
            'atoNormativo' =>  (is_null($amparo_legal) == false)? $amparo_legal->ato_normativo : '',
            'artigo' => (is_null($amparo_legal) == false) ? $amparo_legal->artigo : '',
            'paragrafo' => (is_null($amparo_legal) == false) ?  $amparo_legal->paragrafo : '',
            'inciso' => (is_null($amparo_legal) == false) ? $amparo_legal->inciso : '',
            'alinea' => (is_null($amparo_legal) == false) ? $amparo_legal->alinea : '',
            //EMPENHO
            'credorEmpenho' =>  $credorEmpenho,
            'uasgCompraEmpenho' =>  $uasgCompraEmpenho,
            'tipoCompraEmpenho' =>  $tipoCompraEmpenho,
            'numeroAnoEmpenho' =>  $numeroAnoEmpenho,
            'tipoMinutaEmpenho' =>  'Indisponível(*)',//verificar onde encontrar isto
            'tipoEmpenho' =>  $tipoEmpenho,
            'valorTotalEmpenho' =>  $valorTotalEmpenho,
        ];

        return $tokens;
    }
    private function montarItemContrato(int $idContrato, int $idItemSelecionado = 0) {
        $item = Contratoitem::join("catmatseritens","contratoitens.catmatseritem_id","=","catmatseritens.id")
                            ->join("catmatsergrupos","contratoitens.grupo_id","=","catmatsergrupos.id")
                            ->where("contratoitens.contrato_id",$idContrato)
                            ->whereNull("contratoitens.deleted_at")
                            ->select(   "contratoitens.numero_item_compra",
                                        "catmatsergrupos.descricao AS itemGrupo",
                                        "catmatseritens.descricao as item",
                                        "contratoitens.descricao_complementar AS descricaoComplementar",
                                        "catmatseritens.codigo_siasg",
                                        "contratoitens.id",
                                        )
                            ->selectRaw("to_char(contratoitens.quantidade, 'fm999G999D') AS quantidade")
                            ->selectRaw("TRIM(COALESCE(TO_CHAR(data_inicio, 'DD/MM/YYYY'), '')) AS dataInicio")
                            ->selectRaw("TRIM( to_char(valorunitario, '999G999G990D99') ) AS valorUnitario")
                            ->selectRaw("TRIM( to_char(valortotal, '999G999G990D99') ) AS valorTotal")
                            ->orderBy('numero_item_compra', 'ASC');

        if($idItemSelecionado > 0) {
            $item = $item->where("contratoitens.id",$idItemSelecionado)->first();
        } else {
            $item = $item->get();
        }

        if($item) {
            return $item->toArray();
        }

        return [];
    }

    private function montarEmpenho(int $idContrato, int $idItemSelecionado = 0) {

        $item = ContratoEmpenho::join("empenhos", "empenhos.id", "=", "contratoempenhos.empenho_id")
        ->join("unidades", "unidades.id", "=", "empenhos.unidade_id")
        ->join("fornecedores", "fornecedores.id", "=", "empenhos.fornecedor_id")
        ->where("contratoempenhos.contrato_id",$idContrato)
        ->whereNull("empenhos.deleted_at")
        ->select("empenhos.numero",
                 "unidades.codigo",
                 "contratoempenhos.id")
        ->selectRaw("CONCAT('R$ ', TRIM( to_char(empenhos.empenhado, '999G999G990D99') )) AS valorempenhado")
        ->selectRaw("CONCAT('R$ ', TRIM( to_char(empenhos.aliquidar, '999G999G990D99') )) AS saldoatual")
        ->selectRaw("CONCAT_WS(' - ', fornecedores.cpf_cnpj_idgener, fornecedores.nome) as credor")
        ->selectRaw("CONCAT_WS(' - ', unidades.codigo, unidades.nomeresumido) as ugemitente")
        ->selectRaw("TRIM( TO_CHAR(empenhos.data_emissao, 'DD/MM/YYYY') ) as dataemissao");

        if($idItemSelecionado > 0) {
            $item = $item->where("contratoempenhos.id",$idItemSelecionado)->first();
        } else {
            $item = $item->get();
        }

        if($item) {
            return $item->toArray();
        }

        return [];
    }

    public function montarTokenAjax() {

        $contrato = $this->montarInformacaoContrato();

        return $this->montaJsonTokensAjax($contrato);
    }

    public function montarTokenResponsavelAjax() {
        $contratoId = \Route::current()->parameter('contrato_id');
        return $this->montarResponsaveisContrato($contratoId);
    }

    public function montarTokenItemContratoAjax() {
        $contratoId = \Route::current()->parameter('contrato_id');
        return $this->montarItemContrato($contratoId);
    }

    public function montarTokenEmpenhoAjax() {
        $contratoId = \Route::current()->parameter('contrato_id');
        return $this->montarEmpenho($contratoId);
    }

    public function montarInformacaoTabelaEmpenho() {
        $contratoId = \Route::current()->parameter('contrato_id');
        $idEmpenho = \Route::current()->parameter('idEmpenho');

        return $this->montarEmpenho($contratoId, $idEmpenho);
    }

    public function montarInformacaoTabelaItemContrato() {
        $contratoId = \Route::current()->parameter('contrato_id');
        $idItemContrato = \Route::current()->parameter('idItemContrato');

        return $this->montarItemContrato($contratoId, $idItemContrato);
    }

    public function copiarMinuaExistente() {
        $idTipoDocumento = \Route::current()->parameter('idTipoDocumento');
        $userUgId = \Route::current()->parameter('userUgId');

        $documentosParaCopiar = ContratoMinuta::join("contratos","contrato_minutas.contrato_id","=","contratos.id")
                                                ->join("fornecedores","contratos.fornecedor_id","=","fornecedores.id")
                                                ->where("codigoitens_id",$idTipoDocumento)
                                                ->where("unidadeorigem_id",$userUgId)
                                                ->selectRAW("CONCAT(to_char(data, 'DD/MM/YYYY'),' - ', numero,' - ',fornecedores.nome) AS text, contrato_minutas.id")
                                                ->get();

        $saida [] = ['id'=>"","text"=>"Selecione o documento para copiar"];

        if($documentosParaCopiar) {
            foreach($documentosParaCopiar as $documento) {
                array_push($saida, ['id'=> $documento->id,"text"=>$documento->text])  ;
            }

        }

        return  json_encode($saida);
    }

    public function exibirMinuaExistente() {
        $idMinuta = \Route::current()->parameter('idMinuta');
        $contratoMinuta = ContratoMinuta::find($idMinuta);

        return $contratoMinuta->texto_minuta;
    }

    /**
     * Retora array com dados da unidade conforme cadastro da unidade em que o usuário está logado.
     *
     * @return array
     */
    private function recuperarDadosUnidadeParaCabecalho()
    {

        $idUnidade = session('user_ug_id');

        $unidadeUsuarioLogado = Unidade::where('id', $idUnidade)->first();

        $orgaoUnidade = Orgao::where('id', $unidadeUsuarioLogado->orgao_id)->first();

        $orgaoSuperior = OrgaoSuperior::where('id', $orgaoUnidade->orgaosuperior_id)->first();

        return [
            'nomeUnidadeUsuarioLogado'          => $unidadeUsuarioLogado->nome  ?? ' - ',
            'nomeOrgaoUsuarioLogado'            => $orgaoUnidade->nome          ?? ' - ',
            'nomeOrgaoSuperiorUsuarioLogado'    => $orgaoSuperior->nome         ?? ' - '
        ];
    }

}
