<?php

namespace App\Http\Controllers\Gescon;

use App\Models\Contrato;
use App\Models\Contratoarquivo;
use App\Models\EnviaDadosPncp;
use App\services\AmparoLegalService;
use App\services\ContratoArquivoService;
use App\services\PNCP\PncpService;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ContratoarquivoRequest as StoreRequest;
use App\Http\Requests\ContratoarquivoRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use App\Http\Traits\ValidarTermoPNCP;
use App\Http\Traits\EnviaPncpTrait;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

/**
 * Class ContratoarquivoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ContratoarquivoCrudController extends CrudController
{
    use ValidarTermoPNCP;
    use EnviaPncpTrait;

    protected $amparoLegalService;
    protected $contratoArquivoService;

    public function __construct(
        AmparoLegalService     $amparoLegalService,
        ContratoArquivoService $contratoArquivoService
    )
    {
        parent::__construct();
        $this->amparoLegalService = $amparoLegalService;
        $this->contratoArquivoService = $contratoArquivoService;
    }

    public function setup()
    {
        $contrato_id = \Route::current()->parameter('contrato_id');
        $contrato = Contrato::where('id', '=', $contrato_id)
            ->where('unidade_id', '=', session()->get('user_ug_id'))->first();
        if (!$contrato) {
            abort('403', config('app.erro_permissao'));
        }



        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Contratoarquivo');
        $this->crud->setEntityNameStrings('Arquivo do Contrato', 'Arquivos - Contrato');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/gescon/contrato/' . $contrato_id . '/arquivos');
        $this->crud->addClause('where', 'contrato_id', '=', $contrato_id);
        $this->crud->addClause('where', 'rascunho', '=', false);
        $this->crud->orderBy("created_at", "DESC");
        $this->crud->addButtonFromView('top', 'voltar', 'voltarcontrato', 'end');
        $this->crud->enableExportButtons();
        $this->crud->denyAccess('delete');
        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->allowAccess('show');

        $this->crud->addButtonFromView(
            'line',
            'Atualizar assinatura',
            'atualizarassinaturaminuta',
            'end'
        ); //envio sei buttons

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */
        $colunas = $this->contratoArquivoService->colunas();
        $this->crud->addColumns($colunas);

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */

        (backpack_user()->can('contratoarquivo_inserir')) ? $this->crud->allowAccess('create') : null;
        //(backpack_user()->can('contratoarquivo_editar')) ? $this->crud->allowAccess('update') : null;
        // (backpack_user()->can('contratoarquivo_deletar')) ? $this->crud->allowAccess('publicar_documento') : null;

        if (backpack_user()->can('contratoarquivo_deletar')) {
            $this->crud->allowAccess('delete');
            $this->crud->addButtonFromView(
                'line',
                'delete',
                'delete_contrato_arquivo',
                'end'
            );
        }

        $this->crud->addButtonFromView('line', 'mudarstatusrestricao', 'mudarstatusrestricao', 'beginning');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */
        $num_processo = $contrato->processo;

        $campos = $this->contratoArquivoService->campos(
            [$contrato->id => $contrato->numero],
            $contrato->amparoslegais,
            $num_processo,
            $contrato->elaboracao,
            'contrato'
        );
        $this->crud->addFields($campos);
        // add asterisk for fields that are required in ContratoarquivoRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    /*
     * Derivação do método padrão do Backpack para permitir a gravação
     * Sem passar pela MUTATORS
     * @autor: Álvaro - INSS
     * @ano:   2021
     */
    public function storeCrude(StoreRequest $request)
    {
        if (empty($request->arquivos) && !isset($request->nomeSei)) {
            \Alert::warning('Nenhum arquivo enviado')->flash();
            return \Redirect::to($this->crud->route . '/create');
        }

        $validaTamanhoArquivoSEI = $this->contratoArquivoService->validaTamanhoArquivoSEI($request);
        if ($validaTamanhoArquivoSEI['error']) {
            $arquivos = implode(', ', $validaTamanhoArquivoSEI['errors']);

            \Alert::error('O "' . $arquivos . '" excede o limite de 30MB.')->flash();
            return \Redirect::to($this->crud->route . '/create');
        }

        $arquivosProcessados = [];
        $contratoArquivosRemover = [];
        $arquivosSalvosComSucesso = 0;
        $errorMessages = [];
        try {
            DB::beginTransaction();
            $contratoArquivos = $this->contratoArquivoService->save($request, 'contrato');
            /************************************************************************************
             * ROTINA DE ENVIO DE ARQUIVOS PARA PNCP
             ************************************************************************************/
            foreach ($contratoArquivos as $contratoArquivo) {
                if (!$contratoArquivo->restrito) {
                    try {
                        $pncpService = new PncpService();
                        $response = $pncpService->preparaEnvioArquivoParaPncp($contratoArquivo, 'INCARQ');
                        # 403 - Se for erro de autenticação, grava os arquivos e mantém o envio pendente
                        if (!empty($response->code) && $response->code >= 400 && $response->code !== 403) {
                            throw new \Exception($response->message);
                        }
                        $arquivosSalvosComSucesso++;
                    } catch (\Exception $e) {
                        $contratoArquivosRemover[] = $contratoArquivo;
                        $errorMessages[] = "Erro no arquivo '{$contratoArquivo->descricao}': {$e->getMessage()}";
                        continue;
                    }
                }
                $arquivosProcessados[] = $contratoArquivo;
            }
            # Exclui os arquivos que deram erro. Isto é necessário, pois os arquivos já foram cadastrados no service
            if (!empty($contratoArquivosRemover)) {
                foreach ($contratoArquivosRemover as $contratoArquivoRemover) {
                    $contratoArquivoRemover->delete();
                }
            }
            # Percorre os arquivos processados para atualizar a situação dos registros 'pai' na tabela envia_dados_pncp
            foreach ($arquivosProcessados as $arquivoProcessado) {
                $this->atualizarSituacaoEnviaDadosPncpPeloIdContratoArquivo($arquivoProcessado->id);
            }
            /************************************************************************************/
            DB::commit();
            if ($arquivosSalvosComSucesso > 0) {
                \Alert::success("{$arquivosSalvosComSucesso} arquivo(s) cadastrados(s) com sucesso.")->flash();
            }
            if (!empty($errorMessages)) {
                \Alert::error(implode('<br><br>', $errorMessages))->flash();
            }
            return $this->performSaveAction($contratoArquivo->getKey());
        } catch (\Exception $e) {
            DB::rollback();
            Log::info(__METHOD__ . ' Linha ' . __LINE__ . ' - Erro ao salvar Arquivo' .
            '[' . $e->getMessage() . ']');
            \Alert::error($e->getMessage())->flash();
            return \Redirect::back()->withInput();
        }
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        //$redirect_location = parent::storeCrud($request);
        $redirect_location = $this->storeCrude($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        $idContratoArquivo = request()->id;
        $contratoArquivo = ContratoArquivo::findOrFail($idContratoArquivo);

        if (!isset($contratoArquivo->justificativa_exclusao)) {
            $contratoArquivo->justificativa_exclusao = 'Excluído por: ' . backpack_user()->name;
            $contratoArquivo->saveWithoutEvents();
        }

        $pncpService = new PncpService();

        if (empty($contratoArquivo->sequencial_pncp) && ($contratoArquivo->envio_pncp_pendente !== null)) {
            $contratoArquivo->excluiTempDocumentoSEI();
            #$contratoArquivo->envio_pncp_pendente = null;
            #$contratoArquivo->saveWithoutEvents();
        }
        $response = $pncpService->preparaEnvioArquivoParaPncp($contratoArquivo, 'DELARQ');
        if ($response->code >= 400) {
            DB::rollBack();
            $errorMessage = 'Erro ao tentar excluir arquivo do PNPC.';
            $mensagemCompletaParaLog = $errorMessage . "\n" .
                'ID do Arquivo: ' . $contratoArquivo->id . "\n" .
                'ID do Contrato: ' . $contratoArquivo->contrato_id . "\n" .
                'ID do ContratoHistorico: ' . $contratoArquivo->contratohistorico_id . "\n" .
                'Sequencial PNCP: ' . $contratoArquivo->sequencial_pncp . "\n" .
                'Mensagem de retorno: ' . $response->message;
            $this->inserirLogPncp($mensagemCompletaParaLog, __METHOD__, __LINE__);
            return $pncpService->gerarResponse($response, null, $errorMessage);
        }
        $contratoArquivo->delete();
        $this->atualizarSituacaoEnviaDadosPncpPeloIdContratoArquivo($idContratoArquivo);
        DB::commit();
        $response->message = 'Arquivo excluído com sucesso.';
        return $pncpService->gerarResponse($response);

    }

    public function atualizacaoSituacaoArquivoMinuta()
    {
        return $this->contratoArquivoService->atualizacaoSituacaoArquivoMinuta(\Route::current()->parameter('id'), 'contrato');
    }

    public function mudarStatusDocumento()
    {
        $idContratoArquivo = \Route::current()->parameter('idContratoArquivo');
        $restrito = \Route::current()->parameter('restrito');
        $nomeUsuario = backpack_user()->name;
        $this->contratoArquivoService->alterarSituacaoRestritoArquivoContrato(
            $restrito,
            $idContratoArquivo,
            $nomeUsuario
        );
        $this->atualizarSituacaoEnviaDadosPncpPeloIdContratoArquivo($idContratoArquivo);
        return redirect()->back();
    }

    public function show($id)
    {
        $content = parent::show($id);

        $this->crud->removeColumn('user_id');

        return $content;
    }

}
