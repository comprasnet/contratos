<?php

namespace App\Http\Controllers\Gescon;

use App\Repositories\ContratoRepository;
use App\User;
use App\Http\Traits\PermissaoTrait;
use App\Models\Contratoresponsavel;
use App\Models\Estado;
use App\Models\Municipio;
use App\Models\Codigoitem;
use App\Models\Contrato;
use App\Models\Fornecedor;
use App\Models\Unidade;
use App\Models\Contratoconta;
use App\Models\Contratohistorico;
use Backpack\CRUD\app\Http\Controllers\CrudController;

use App\PDF\PdfEncerramentoContrato;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\EncerramentoRequest as StoreRequest;
use App\Http\Requests\EncerramentoRequest as UpdateRequest;
use App\Http\Traits\EnceramentoContratoTrait;
use Backpack\CRUD\CrudPanel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class EncerramentoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class EncerramentoCrudController extends CrudController
{
    use EnceramentoContratoTrait;
    use PermissaoTrait;

    public function setup()
    {
        [$contrato_id, $contrato] = $this->verificarPermissaoContrato();

        $quatidadeDeTermosDeEncerramentoCadastrados = $this->getQuantidadeTermoDeEncerramento($contrato_id);

        $isUsuarioResponsavelPeloContrato = $this->verificarSeUsuarioEResponsavelPeloContrato($contrato_id); #795

        $contrato = Contrato::where('id', '=', $contrato_id)
            ->where('unidade_id', '=', session()->get('user_ug_id'))->first();

        if (!$contrato) {
            #795
            $this->getBotaoVoltar();
            $deOndeUsuarioVem = session()->get('de_onde_usuario_vem_encerramento');
            if($deOndeUsuarioVem == 'contrato'){
                // aqui o usuário vem de contratos e a unidade do contrato não é a unidade dele.
                abort('403', config('app.erro_permissao'));
            } else {
                // aqui usuário vem de meus contratos - verificar se é responsável pelo contrato, pois a unidade dele não é un. do contrato
                if( !self::verificarSeUsuarioLogadoEResponsavelPorContrato($contrato_id) ){
                    abort('403', config('app.erro_permissao'));
                }
            }
        }

        $tps = Codigoitem::whereHas('codigo', function ($query) {
            $query->where('descricao', '=', 'Tipo de Contrato');
        })
            ->Where('descricao', '=', 'Termo de Encerramento')
            ->pluck('id')
            ->toArray();

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Contratohistorico');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/gescon/contrato/' . $contrato_id . '/encerramento');
        $this->crud->setEntityNameStrings('Relatório Final', 'Relatório Final');

        $this->crud->addButtonFromView('line', 'gerarpdfencerramentocontrato', 'gerarpdfencerramentocontrato', 'beginning'); #536
        $this->crud->addButtonFromView('line', 'gerarminutarelatoriofinal', 'gerarminutarelatoriofinal', 'end'); #597
        $this->crud->addButtonFromView('line', 'delete', 'delete', 'end'); #597

        session()->forget('vemDoEncerramento'); #597

        $this->crud->addClause('join', 'fornecedores', 'fornecedores.id', '=', 'contratohistorico.fornecedor_id');
        $this->crud->addClause('join', 'unidades', 'unidades.id', '=', 'contratohistorico.unidade_id');

        if(!$isUsuarioResponsavelPeloContrato){
            $this->crud->addClause('where', 'unidade_id', '=', session()->get('user_ug_id'));
        }
        $this->crud->addClause('where', 'contratohistorico.elaboracao', '=', false);
        $this->crud->addClause('select', 'contratohistorico.*');
        $this->crud->addClause('where', 'contrato_id', '=', $contrato_id);
        foreach ($tps as $t) {
            $this->crud->addClause('where', 'tipo_id', '=', $t);
        }

        $this->getBotaoVoltar();

        $this->crud->enableExportButtons();
        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');
        $this->crud->allowAccess('show');

        // vamos verificar se já existe um termo de encerramento para este contrato e, caso negativo, habilitaremos o botão adicionar
        (backpack_user()->can('contratoencerramento_inserir')) && $quatidadeDeTermosDeEncerramentoCadastrados<1 ? $this->crud->allowAccess('create') : null;
        (backpack_user()->can('contratoencerramento_editar')) ? $this->crud->allowAccess('update') : null;
        (backpack_user()->can('contratoencerramento_deletar')) ? $this->crud->allowAccess('delete') : null;

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $colunas = $this->Colunas();
        $this->crud->addColumns($colunas);

        $tipo = Codigoitem::whereHas('codigo', function ($query) {
            $query->where('descricao', '=', 'Tipo de Contrato');
        })
            ->where('descricao', '=', 'Termo de Encerramento')
            ->first();

        $campos = $this->Campos($tipo, $contrato_id, $quatidadeDeTermosDeEncerramentoCadastrados);
        $this->crud->addFields($campos);

        // add asterisk for fields that are required in EncerramentoRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

    }

    public function verificarSeUsuarioLogadoEResponsavelPorContrato(int $contrato_id){
        $user_id = backpack_user()->id;
        $quantidade = Contrato::where('contratos.id', $contrato_id)
            ->join('contratoresponsaveis as cr', 'cr.contrato_id', 'contratos.id')
            ->join('users as u', 'u.id', 'cr.user_id')
            ->where('u.id', '=', $user_id)
            ->count();
        if($quantidade > 0 ){return true;} else {return false;}
    }

    public function getBotaoVoltar(){
        $deOndeUsuarioVem = session()->get('de_onde_usuario_vem_encerramento');
        if( empty( $deOndeUsuarioVem ) ){
            $paginaAnterior = \URL::previous();
            $verificacao = strpos($paginaAnterior, '/contrato');    //aqui quer dizer que vem de contratos
            if(!$verificacao){
                \Session::put('de_onde_usuario_vem_encerramento', 'meuscontratos');
                $this->crud->addButtonFromView('top', 'voltar', 'voltarmeucontrato', 'end');
            }
            else{
                \Session::put('de_onde_usuario_vem_encerramento', 'contrato');
                $this->crud->addButtonFromView('top', 'voltar', 'voltarcontrato', 'end');
            }
        } else {
            if($deOndeUsuarioVem == 'contrato'){
                $this->crud->addButtonFromView('top', 'voltar', 'voltarcontrato', 'end');
            }
            elseif($deOndeUsuarioVem == 'meuscontratos'){
                $this->crud->addButtonFromView('top', 'voltar', 'voltarmeucontrato', 'end');
            }
        }
        return true;
    }

    public function verificarSeUsuarioEResponsavelPeloContrato(int $contrato_id){
        $user_id = backpack_user()->id;
        $responsaveis = $this->getResponsaveis($contrato_id);
        foreach($responsaveis as $key => $value){if($key == $user_id){return true;}}
        return false;
    }

    public function getQuantidadeTermoDeEncerramento($contrato_id){
        $quantidadeDeTermo = Contratohistorico::where('contrato_id', $contrato_id)
            ->join('codigoitens as ci', 'ci.id', 'contratohistorico.tipo_id')
            ->join('codigos as c', 'c.id', 'ci.codigo_id')
            ->where('ci.descricao', 'like', 'Termo de Encerramento')
            ->where('contratohistorico.elaboracao', false)
            ->where('c.descricao', 'Tipo de Contrato')
            ->count();
        return $quantidadeDeTermo;
    }

    public function Colunas()
    {
        $colunas = [
            [
                'name' => 'numero',
                'label' => 'Núm. Encerramento',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false, // no point, since it's a large text
                'visibleInModal' => false, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => false, // sure, why not
            ],
            [
                'name' => 'numero',
                'label' => 'Contrato',
                'type' => 'text',
                'limit' => 1000,
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'processo',
                'label' => 'Processo',
                'type' => 'text',
                'limit' => 1000,
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'observacao',
                'label' => 'Observação',
                'type' => 'text',
                'limit' => 1000,
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'vigencia_fim',
                'label' => 'Vigência Fim',
                'type' => 'date',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                // data_encerramento
                'name' => 'getDataEncerramento',
                'label' => 'Data de Encerramento',
                'type' => 'model_function',
                'function_name' => 'getDataEncerramento', // the method in your Model
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'data_publicacao',
                'label' => 'Data de Publicação',
                'type' => 'date',
                'orderable' => true,
                'visibleInTable' => false, // no point, since it's a large text
                'visibleInModal' => false, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => false, // sure, why not
            ],
            [
                'name' => 'getIsSaldoContaVinculadaLiberado',
                'label' => 'O saldo remanescente dos recursos depositados na Conta-Depósito Vinculada foi liberado para o Fornecedor até o encerramento do contrato?',
                'type' => 'model_function',
                'function_name' => 'getIsSaldoContaVinculadaLiberado', // the method in your Model
                'orderable' => false,
                'visibleInTable' => false, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'getIsGarantiaContratualDevolvida',
                'label' => 'A garantia contratual foi integralmente devolvida para a Contratada?',
                'type' => 'model_function',
                'function_name' => 'getIsGarantiaContratualDevolvida', // the method in your Model
                'orderable' => false,
                'visibleInTable' => false, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'getGrauSatisfacaoDesempenhoContratoRelatorio',
                'label' => 'Grau de satisfação com o desempenho do contrato',
                'type' => 'model_function',
                'function_name' => 'getGrauSatisfacaoDesempenhoContratoRelatorio', // the method in your Model
                'orderable' => false,
                'visibleInTable' => false, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'getPlanejamentoContratacaoAtendida',
                'label' => 'A necessidade formalizada no Planejamento da Contratação (DOD/DFD/ETP/TR/PB) foi plenamente atendida pelo contrato?',
                'type' => 'model_function',
                'function_name' => 'getPlanejamentoContratacaoAtendida', // the method in your Model
                'orderable' => false,
                'visibleInTable' => false, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'getConsecucaoObjetivosContratacao',
                'label' => 'Consecução dos objetivos da contratação',
                'type' => 'model_function',
                'function_name' => 'getConsecucaoObjetivosContratacao', // the method in your Model
                // 'limit' => 1000,
                'orderable' => true,
                'visibleInTable' => false, // no point, since it's a large text
                'visibleInModal' => false, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => false, // sure, why not
            ],

        ];

        return $colunas;

    }
    public function getResponsaveis($contrato_id){
        return $responsaveis = Contrato::where('contratos.id', $contrato_id)
            ->join('contratoresponsaveis as cr', 'cr.contrato_id', 'contratos.id')
            ->join('users as u', 'u.id', 'cr.user_id')
            ->select('u.id as user_id', 'u.name as user_name', 'u.cpf as user_cpf')
            ->pluck('user_name', 'user_id')
            ->toArray();
    }

    public function Campos($tipo, $contrato_id, $quatidadeDeTermosDeEncerramentoCadastrados)
    {
        $dataHoje = date("Y-m-d");
        $contrato = Contrato::find($contrato_id);
        $tipo_contrato =   Codigoitem::find($contrato->tipo_id);
        $isSaldoContaVinculadaLiberado = $this->recuperarIsSaldoContaVinculadaLiberado($contrato->is_saldo_conta_vinculada_liberado);
        $isGarantiaContratualDevolvida = $this->recuperarIsGarantiaContratualDevolvida($contrato->is_garantia_contratual_devolvida);
        $dataEncerramento = $dataHoje;
        $grauSatisfacaoDesempenhoContrato = $this->recuperarGrauSatisfacaoDesempenhoContrato($contrato->grau_satisfacao_desempenho_contrato);

        $recuperarPlanejamentoContratacaoAtendida = $this->recuperarPlanejamentoContratacaoAtendida($contrato->planejamento_contratacao_atendida);

        $responsaveis = $this->getResponsaveis($contrato_id);

        $campos = [
            [   // Hidden
                'name' => 'tipo_contrato',
                'type' => 'hidden',
                'default' => $tipo_contrato->descricao,
                'attributes' => ['id' => 'tipo_contrato']
            ],
            [   // Hidden
                'name' => 'data_publicacao',
                'type' => 'hidden',
                'default' => $contrato->data_publicacao,
            ],
            [   // Hidden
                'name' => 'receita_despesa',
                'type' => 'hidden',
                'default' => $contrato->receita_despesa,
            ],
            [   // Hidden
                'name' => 'tipo_id',
                'type' => 'hidden',
                'default' => $contrato->tipo_id,
            ],
            [   // Hidden
                'name' => 'contrato_id',
                'type' => 'hidden',
                'default' => $contrato->id,
            ],
            [   // Hidden
                'name' => 'fornecedor_id',
                'type' => 'hidden',
                'default' => $contrato->fornecedor_id,
            ],
            [   // Hidden
                'name' => 'unidade_id',
                'type' => 'hidden',
                'default' => $contrato->unidade_id,
            ],
            [   // Hidden
                'name' => 'categoria_id',
                'type' => 'hidden',
                'default' => $contrato->categoria_id,
            ],
            [   // Date
                'name' => 'vigencia_inicio',
                'type' => 'hidden',
                'default' => $contrato->vigencia_inicio,
            ],
            [   // Hidden
                'name' => 'tipo_id',
                'type' => 'hidden',
                'default' => $tipo->id,
            ],
            [   // Hidden
                'name' => 'numero',
                'type' => 'hidden',
                'default' => $contrato->numero,

            ],
            [
                'name' => 'numero',
                'label' => 'Contrato',
                'type' => 'numcontratoencerramento',
                'default' => $contrato->numero,
                'attributes' => [
                    'readonly'=>'readonly',
                    'style' => 'pointer-events: none;touch-action: none;'
                ],
            ],
            [
                'name' => 'processo',
                'label' => 'Número Processo',
                'default' => $contrato->processo,
            ],
            [
                // Date
                'name' => 'vigencia_fim',
                'label' => 'Vigência Fim',
                'type' => 'date',
                'default' => $contrato->vigencia_fim,
                'attributes' => [
                    'readonly'=>'readonly',
                    'style' => 'pointer-events: none;touch-action: none;'
                ],
            ],
            [   // Date
                'name' => 'data_encerramento',
                'label' => 'Data Encerramento do Contrato',
                'type' => 'date',
                'default' => $contrato->data_encerramento,
                'ico_help' => 'Data efetiva do encerramento do contrato',
            ],
            [
                'name' => 'is_objeto_contratual_entregue',
                'label' => "O objeto contratual foi entregue à Contratante em sua totalidade?",
                'type' => 'radio',
                'options' => [0 => 'Não', 1 => 'Sim'],
                'default' => $contrato->is_objeto_contratual_entregue,
                'inline'  => true,

            ],
            [
                'name' => 'is_cumpridas_obrigacoes_financeiras',
                'label' => "Foram cumpridas todas as obrigações financeiras junto à Contratada?",
                'type' => 'radio',
                'options' => [0 => 'Não', 1 => 'Sim'],
                'default' => $contrato->is_cumpridas_obrigacoes_financeiras,
                'inline'  => true,
            ],
            [
                'name' => 'te_valor_total_executado',
                'label' => 'Valor total executado',
                'type' => 'money',
                'default' => $contrato->te_valor_total_executado,
                'attributes' => [
                    'id' => 'te_valor_total_executado',
                ],
                'allowZero' => "true",
                'prefix' => "R$",
            ],
            [
                'name' => 'te_valor_total_pago',
                'label' => 'Valor total pago',
                'type' => 'money',
                'default' => $contrato->te_valor_total_pago,
                'attributes' => [
                    'id' => 'te_valor_total_pago',
                ],
                'allowZero' => "true",
                'prefix' => "R$",
            ],
            [
                'name' => 'te_saldo_disponivel_ou_bloqueado',
                'label' => 'Saldo disponível ou bloqueado',
                'type' => 'money',
                'default' => $contrato->te_saldo_disponivel_ou_bloqueado,
                'attributes' => [
                    'id' => 'te_saldo_disponivel_ou_bloqueado',
                ],
                'allowZero' => "true",
                'prefix' => "R$",
            ],
            [
                'name' => 'te_justificativa_nao_cumprimento',
                'label' => 'Justificativa de não cumprimento das obrigações financeiras junto à Contratada',
                'type' => 'textarea',
                'default' => $contrato->te_justificativa_nao_cumprimento,
            ],
            [
                'name' => 'is_saldo_conta_vinculada_liberado',
                'label' => "O saldo remanescente dos recursos depositados na Conta-Depósito Vinculada foi liberado para o Fornecedor até o encerramento do contrato?",
                'type' => 'radio',
                'options' => [0 => 'Não', 1 => 'Sim', 2 => 'Não aplicável'],
                'default' => $isSaldoContaVinculadaLiberado,
                'inline'  => true,
            ],
            [
                'name' => 'te_saldo_bloqueado_conta_deposito',
                'label' => 'Saldo bloqueado',
                'type' => 'money',
                'default' => $contrato->te_saldo_bloqueado_conta_deposito,
                'attributes' => [
                    'id' => 'te_saldo_bloqueado_conta_deposito',
                ],
                'allowZero' => "true",
                'prefix' => "R$",
            ],
            [
                'name' => 'te_justificativa_bloqueio_conta_deposito',
                'label' => 'Justificativa de bloqueio da Conta-Depósito Vinculada',
                'type' => 'textarea',
                'default' => $contrato->te_justificativa_bloqueio_conta_deposito,
            ],
            [
                'name' => 'is_garantia_contratual_devolvida',
                'label' => " A garantia contratual foi integralmente devolvida para a Contratada?",
                'type' => 'radio',
                'options' => [0 => 'Não', 1 => 'Sim', 2 => 'Não aplicável'],
                'default' => $isGarantiaContratualDevolvida,
                'inline'  => true,
            ],
            [
                'name' => 'te_saldo_bloqueado_garantia_contratual',
                'label' => 'Saldo bloqueado',
                'type' => 'money',
                'default' => $contrato->te_saldo_bloqueado_garantia_contratual,
                'attributes' => [
                    'id' => 'te_saldo_bloqueado_garantia_contratual',
                ],
                'allowZero' => "true",
                'prefix' => "R$",
            ],
            [
                'name' => 'te_justificativa_bloqueio_garantia_contratual',
                'label' => 'Justificativa de bloqueio da garantia contratual',
                'type' => 'textarea',
                'default' => $contrato->te_justificativa_bloqueio_garantia_contratual,
            ],
            [
                'name' => 'grau_satisfacao_desempenho_contrato',
                'label' => " Grau de satisfação com o desempenho do contrato",
                'type' => 'radio',
                'options' => [0 => 'Muito baixo', 1 => 'Baixo', 2 => 'Médio', 3 => 'Alto', 4 => 'Muito Alto'],
                'default' => $grauSatisfacaoDesempenhoContrato,
                'inline'  => true,
            ],
            [
                'name' => 'planejamento_contratacao_atendida',
                'label' => "A necessidade formalizada no Planejamento da Contratação (DOD/DFD/ETP/TR/PB) foi plenamente atendida pelo contrato?",
                'type' => 'radio',
                'options' => [0 => 'Não', 1 => 'Sim'],
                'default' => $recuperarPlanejamentoContratacaoAtendida,
                'inline'  => true,
            ],
            [
                'name' => 'consecucao_objetivos_contratacao',
                'label' => 'Consecução dos objetivos da contratação',
                'type' => 'textarea',
                'default' => $contrato->consecucao_objetivos_contratacao,
                'attributes' => [
                ],
                'ico_help' => 'Inserir evidências e observações sobre a consecução dos objetivos que tenham justificado a contratação, inclusos nos documentos de Planejamento da Contratação.',
            ],
            [
                'name' => 'sugestao_licao_aprendida',
                'label' => " Sugestões / Lições aprendidas",
                'type' => 'textarea',
                'default' => $contrato->sugestao_licao_aprendida,
                'ico_help' => 'Inserir eventuais condutas a serem adotadas para o aprimoramento das atividades da Administração.',
            ],
            [
                'name' => 'observacao',
                'label' => 'Observação',
                'type' => 'textarea',
                'attributes' => [
                ],
            ],
            [ // select_from_array
                'name' => 'user_id_responsavel_signatario_encerramento',
                'label' => 'Signatário',
                'type' => 'select2_from_array',
                'options' => $responsaveis,
                'default' => $contrato->user_id_responsavel_signatario_encerramento,
                'allows_null' => true,
                'allows_multiple' => false, // OPTIONAL; needs you to cast this to array in your model;
            ],

        ];

        return $campos;
    }

    public function verificarSeContaVinculadaPossuiSaldo($idContrato){
        $saldoContaVinculada = Contratoconta::getSaldoContaVinculadaByIdContrato($idContrato);
        if($saldoContaVinculada > 0){return false;}
        return true;
    }

    public function verificarSeNãoExisteContaVinculadaOuSeEstaEncerrada($idContrato){
        /**
         * Caso não exista conta vinculada para o contrato: retorna true
         * Caso exista e esteja como encerrada: retorna true
         * Caso exista e não esteja como encerrada: retorna false
         */
        $objContaVinculada = Contratoconta::where('contrato_id', $idContrato)->first();
        if(!is_object($objContaVinculada)){
            return true;
        } elseif( $objContaVinculada->getStatusDaConta() == "Encerrada" ){
            return true;
        } else {
            return false;
        }
    }

    public function getIdCodigoItensJustificativaContratoInativo($descricao){
        // buscar o id em codigoitens para rescindido
        return $idRescindido = Codigoitem::where('codigoitens.descricao', $descricao)
            ->select('codigoitens.id')
            ->join('codigos as c', 'c.id', 'codigoitens.codigo_id')
            ->where('c.descricao', 'Justificativa Contrato Inativo')
            ->first()
            ->id;
    }

    public function darAndamentoAoEncerramento($request){
        try{
            /** @var ContratoRepository $contratoRepository */

            $contratoRepository = app(ContratoRepository::class);
            // quando um termo de encerramento for cadastrado, precisaremos tornar o contrato inativo e com o justificativa = encerrado.
            $dataHoje = date("Y-m-d");
            $horaAgora = date("H:i:s");
            $user_id = backpack_user()->id;
            $name_user_id = backpack_user()->name;
            $cpf_user_id = backpack_user()->cpf;
            $request->request->set('situacao', false);
            $idJustificativaEncerrado = self::getIdCodigoItensJustificativaContratoInativo('Encerrado');
            $request->request->set('justificativa_contrato_inativo_id', $idJustificativaEncerrado);
            // como a justificativa não está sendo salva apenas pelo request, vou salvar na mão.
            /** @var Contrato $objContrato **/
            $objContrato = Contrato::find($request->input('contrato_id'));
            $instrumentoInicial = $contratoRepository->getInstrumentoInicial($request->input('contrato_id'));


            $objContrato->unsetEventDispatcher(); // Retirando o evento automatico da observer de contrato
            $request->request->set('data_assinatura', $objContrato->data_assinatura);
            $objContrato->justificativa_contrato_inativo_id = $idJustificativaEncerrado;
            $objContrato->situacao = false;
            $objContrato->data_encerramento = $request->input('data_encerramento');
            $objContrato->hora_encerramento = $horaAgora;
            $objContrato->user_id_responsavel_encerramento = $user_id;
            $objContrato->nome_responsavel_encerramento = $name_user_id;
            $objContrato->cpf_responsavel_encerramento = $cpf_user_id;
            $objContrato->consecucao_objetivos_contratacao = $request->get('consecucao_objetivos_contratacao');

            // buscar dados do responsável signatário encerramento
            if( !is_null($request->get('user_id_responsavel_signatario_encerramento')) ){
                $objUser = User::find($request->get('user_id_responsavel_signatario_encerramento'));
                $objContrato->user_id_responsavel_signatario_encerramento = $objUser->id;
                $objContrato->nome_responsavel_signatario_encerramento = $objUser->name;
                $objContrato->cpf_responsavel_signatario_encerramento = $objUser->cpf;
            } else {
                $objContrato->user_id_responsavel_signatario_encerramento = null;
                $objContrato->nome_responsavel_signatario_encerramento = null;
                $objContrato->cpf_responsavel_signatario_encerramento = null;
                //request
                $request->user_id_responsavel_signatario_encerramento = null;
                $request->nome_responsavel_signatario_encerramento = null;
                $request->cpf_responsavel_signatario_encerramento = null;
            }

            $objContrato->grau_satisfacao_desempenho_contrato = $request->get('grau_satisfacao_desempenho_contrato');
            $objContrato->planejamento_contratacao_atendida = $request->get('planejamento_contratacao_atendida') ;
            $objContrato->sugestao_licao_aprendida = $request->get('sugestao_licao_aprendida');

            $this->tratarSimNaoNaoAplicavel($request, 'is_saldo_conta_vinculada_liberado');
            $this->tratarSimNaoNaoAplicavel($request, 'is_garantia_contratual_devolvida');

            $objContrato->is_saldo_conta_vinculada_liberado = $request->get('is_saldo_conta_vinculada_liberado') ;
            $objContrato->is_garantia_contratual_devolvida = $request->get('is_garantia_contratual_devolvida');
            $objContrato->is_objeto_contratual_entregue = $request->get('is_objeto_contratual_entregue');

            #1168
            $objContrato->te_valor_total_executado = $request->get('te_valor_total_executado');
            $objContrato->te_valor_total_pago = $request->get('te_valor_total_pago');
            $objContrato->te_saldo_disponivel_ou_bloqueado = $request->get('te_saldo_disponivel_ou_bloqueado');
            $objContrato->te_saldo_bloqueado_garantia_contratual = $request->get('te_saldo_bloqueado_garantia_contratual');
            $objContrato->te_saldo_bloqueado_conta_deposito = $request->get('te_saldo_bloqueado_conta_deposito');
            $objContrato->te_justificativa_nao_cumprimento = $request->get('te_justificativa_nao_cumprimento');
            $objContrato->te_justificativa_bloqueio_garantia_contratual = $request->get('te_justificativa_bloqueio_garantia_contratual');
            $objContrato->te_justificativa_bloqueio_conta_deposito = $request->get('te_justificativa_bloqueio_conta_deposito');

            $objContrato->save();


            if($instrumentoInicial){
                $instrumentoInicial->situacao = false;
                $instrumentoInicial->save();
            }

            $objContrato->setEventDispatcher(app('events')); // Voltando o evento automatico da observer de contrato


        } catch(Exception $e){
            throw new Exception("Error ao Processar a Requisição", $e->getMessage());
        }
        return true;
    }

    public function tratarSimNaoNaoAplicavel (Request $request, $paramRequest) : void
    {
        if($request->get($paramRequest) === '0')
        {
            $request->request->set($paramRequest, false);
            return;
        }

        if($request->get($paramRequest) === '1')
        {
            $request->request->set($paramRequest, true);
            return;
        }

        if($request->get($paramRequest) !== '0' && $request->get($paramRequest) !== '1')
        {
            $request->request->set($paramRequest, null);
            return;
        }

    }

public function store(StoreRequest $request)
    {
        $idContrato = $request->input('contrato_id');

        // A conta vinculada precisa estar como encerrada ou o contrato não ter conta vinculada.
        if( !$this->verificarSeNãoExisteContaVinculadaOuSeEstaEncerrada($idContrato) ){
            \Alert::error('Existe conta-depósito vinculada pendente de encerramento.')->flash();
            return redirect()->back()->withInput();
        } else {
            try {
                // agora podemos dar andamento ao encerramento.
                $request->request->set('situacao',false);
                $redirect_location = parent::storeCrud($request);
                self::darAndamentoAoEncerramento($request);

                return $redirect_location;

            }catch (Exception $e){
                \Alert::error('Ocorreu um erro ao encerrar o contrato.')->flash();
                DB::rollBack();

                return redirect()->back()->withInput();
            }

        }
    }
    public function update(UpdateRequest $request)
    {
        $idContrato = $request->input('contrato_id');

        // A conta vinculada precisa estar como encerrada ou o contrato não ter conta vinculada.
        if( !$this->verificarSeNãoExisteContaVinculadaOuSeEstaEncerrada($idContrato) ){
            \Alert::error('Existe conta-depósito vinculada pendente de encerramento.')->flash();
            return redirect()->back()->withInput();
        } else {
            $request->request->set('situacao',0);
            // agora podemos dar andamento ao encerramento.
            self::darAndamentoAoEncerramento($request);
            $redirect_location = parent::updateCrud($request);
            return $redirect_location;
        }

    }

    public function tirarAcentos($string){
        return preg_replace(
            array(
                "/(á|à|ã|â|ä)/",
                "/(Á|À|Ã|Â|Ä)/",
                "/(é|è|ê|ë)/",
                "/(É|È|Ê|Ë)/",
                "/(í|ì|î|ï)/",
                "/(Í|Ì|Î|Ï)/",
                "/(ó|ò|õ|ô|ö)/",
                "/(Ó|Ò|Õ|Ô|Ö)/",
                "/(ú|ù|û|ü)/",
                "/(Ú|Ù|Û|Ü)/",
                "/(ñ)/",
                "/(Ñ)/"
            ),
            explode(" ","a A e E i I o O u U n N"),
            $string
        );
    }

    public function formataDataParaPadraoBrasileiro($dataPadraoAmericano){
        /**
         * Vamos pegar a data recebida, quebrá-la em aaaa, mm, dd
         * e fazer a nova formatação
         */
        $dia = substr($dataPadraoAmericano, 8, 2);
        $mes = substr($dataPadraoAmericano, 5, 2);
        $ano = substr($dataPadraoAmericano, 0, 4);
        return $dia."/".$mes."/".$ano;
    }


	public function getTermoDeEncerramento($contrato_id){
        return $objTermoEncerramento = Contratohistorico::where('contrato_id', $contrato_id)
            ->join('codigoitens as ci', 'ci.id', 'contratohistorico.tipo_id')
            ->join('codigos as c', 'c.id', 'ci.codigo_id')
            ->where('ci.descricao', 'like', 'Termo de Encerramento')
            ->where('c.descricao', 'Tipo de Contrato')
            ->first();
    }

    public function getNomeFuncaoResponsavelContrato($contrato_id, $user_id){
        return $nomeFuncao = Contratoresponsavel::where('contrato_id', $contrato_id)
            ->where('user_id', $user_id)
            ->join('codigoitens as ci', 'ci.id', 'contratoresponsaveis.funcao_id')
            ->first()
            ->descricao;
    }

    public function gerarMinutaRelatorioFinal(){
        // Em ContratoMinutaCrudController, a variável $vemDoEncerramento será verificada
        $contrato_id = \Route::current()->parameter('contrato_id');
        session()->put('vemDoEncerramento', true);
        return redirect('/gescon/contrato/'.$contrato_id.'/minutas/create');
    }

    public function gerarpdf(int $contrato_id){
        // configuração de linhas do pdf
        $linhaInicialPadrao = 59;
        $linhaInicial = $linhaInicialPadrao;
        $quantidadeMaximaDeLinhas = 210;
        $quantidadeMaximaDeCaracteresPorLinha = 90;
        $tamanhoMaximoLinha = 50;
        $nomeFonte = 'Arial';
        $tamanhoFonteNomeCampo = 8;
        $tamanhoFonteConteudoCampo = 9;

        // dados buscados
        $contrato = Contrato::find($contrato_id);
        $numeroContrato = $contrato->numero;
        $dataEncerramentoContrato = self::formataDataParaPadraoBrasileiro($contrato->data_encerramento);

        // buscar dados do responsável signatário encerramento

        $objUser = User::find($contrato->user_id_responsavel_signatario_encerramento);
        $nomeSignatarioEncerramento = $objUser->name;
        $nomeFuncaoSignatarioEncerramento = self::getNomeFuncaoResponsavelContrato($contrato_id, $contrato->user_id_responsavel_signatario_encerramento);

        $isObjetoContratualEntregue = $contrato->is_objeto_contratual_entregue ? 'Sim' : 'Não';
        $isCumpridasObrigacoesFinanceiras = $contrato->is_cumpridas_obrigacoes_financeiras ? 'Sim' : 'Não';

        if($contrato->is_saldo_conta_vinculada_liberado === true){$is_saldo_conta_vinculada_liberado = 'Sim';}
        elseif($contrato->is_saldo_conta_vinculada_liberado === false){$is_saldo_conta_vinculada_liberado = 'Não';}
        else{$is_saldo_conta_vinculada_liberado = 'Não aplicável';}

        if($contrato->is_garantia_contratual_devolvida === true){$is_garantia_contratual_devolvida = 'Sim';}
        elseif($contrato->is_garantia_contratual_devolvida === false){$is_garantia_contratual_devolvida = 'Não';}
        else{$is_garantia_contratual_devolvida = 'Não aplicável';}

        if($contrato->planejamento_contratacao_atendida === true){$planejamento_contratacao_atendida = 'Sim';}
        elseif($contrato->planejamento_contratacao_atendida === false){$planejamento_contratacao_atendida = 'Não';}
        else{$planejamento_contratacao_atendida = 'Não aplicável';}

        if($contrato->grau_satisfacao_desempenho_contrato == 0){$grau_satisfacao_desempenho_contrato = 'Muito baixo';}
        elseif($contrato->grau_satisfacao_desempenho_contrato == 1){$grau_satisfacao_desempenho_contrato = 'Baixo';}
        elseif($contrato->grau_satisfacao_desempenho_contrato == 2){$grau_satisfacao_desempenho_contrato = 'Médio';}
        elseif($contrato->grau_satisfacao_desempenho_contrato == 3){$grau_satisfacao_desempenho_contrato = 'Alto';}
        elseif($contrato->grau_satisfacao_desempenho_contrato == 4){$grau_satisfacao_desempenho_contrato = 'Muito alto';}

        $objInstrumentoInicial = $contrato->getInstrumentoInicial();
        $vigenciaInicio = self::formataDataParaPadraoBrasileiro($objInstrumentoInicial->vigencia_inicio);
        $vigenciaFim = self::formataDataParaPadraoBrasileiro($objInstrumentoInicial->vigencia_fim);

        $objTermoEncerramento = self::getTermoDeEncerramento($contrato_id);
        $numeroProcessoTermoEncerramento = $objTermoEncerramento->processo;

        // tratar, pois chega aqui com várias linhas
        $sugestao_licao_aprendida = $contrato->sugestao_licao_aprendida;
        $sugestao_licao_aprendida = str_replace("<p>", "", $sugestao_licao_aprendida);
        $sugestao_licao_aprendida = str_replace("</p>", "", $sugestao_licao_aprendida);
        $sugestao_licao_aprendida = str_replace("–", "-", $sugestao_licao_aprendida);       //tratando hífen
        $sugestao_licao_aprendida = str_replace('“', '"', $sugestao_licao_aprendida);       //tratando aspas especiais iniciais
        $sugestao_licao_aprendida = str_replace('”', '"', $sugestao_licao_aprendida);       //tratando aspas especiais finais
        $sugestao_licao_aprendida = str_replace('—', '-', $sugestao_licao_aprendida);       //tratando travessão
        $sugestao_licao_aprendida = strip_tags($sugestao_licao_aprendida);
        $sugestao_licao_aprendida = nl2br($sugestao_licao_aprendida);
        $sugestao_licao_aprendida = str_replace("\r\n", "", $sugestao_licao_aprendida);
        $arraySugestaoLicaoAprendida = explode("<br />", $sugestao_licao_aprendida);

        // tratar, pois chega aqui com várias linhas
        $observacaoTermoEncerramento2 = $objTermoEncerramento->observacao;
        $observacaoTermoEncerramento = nl2br($objTermoEncerramento->observacao);
        $observacaoTermoEncerramento = str_replace("\r\n", "", $observacaoTermoEncerramento);
        $observacaoTermoEncerramento = str_replace("–", "-", $observacaoTermoEncerramento);     //tratando hífen
        $observacaoTermoEncerramento = str_replace('“', '"', $observacaoTermoEncerramento);     //tratando aspas especiais iniciais
        $observacaoTermoEncerramento = str_replace('”', '"', $observacaoTermoEncerramento);     //tratando aspas especiais finais
        $observacaoTermoEncerramento = str_replace('—', '-', $observacaoTermoEncerramento);       //tratando travessão
        $arrayObservacaoTermoEncerramento = explode("<br />", $observacaoTermoEncerramento);

        // tratar, pois chega aqui com várias linhas
        $isMostrarConsecucao = true;
        if( !is_null($contrato->consecucao_objetivos_contratacao) ){
            $consecucao_objetivos_contratacao = nl2br($contrato->consecucao_objetivos_contratacao);
            $consecucao_objetivos_contratacao = str_replace("\r\n", "", $consecucao_objetivos_contratacao);
            $consecucao_objetivos_contratacao = str_replace("–", "-", $consecucao_objetivos_contratacao);       //tratando hífen
            $consecucao_objetivos_contratacao = str_replace('“', '"', $consecucao_objetivos_contratacao);       //tratando aspas especiais iniciais
            $consecucao_objetivos_contratacao = str_replace('”', '"', $consecucao_objetivos_contratacao);       //tratando aspas especiais finais
            $consecucao_objetivos_contratacao = str_replace('—', '-', $consecucao_objetivos_contratacao);       //tratando travessão
            $arrayConsecucao = explode("<br />", $consecucao_objetivos_contratacao);
        } else {
            $isMostrarConsecucao = false;
        }

        $nomeUasg = $contrato->unidade->nome;
        $nomeResumidoUasg = $contrato->unidade->nomeresumido;
        $codigoUasg = $contrato->unidade->codigo;
        $codigoOrgao = $contrato->unidade->orgao->codigo;
        $nomeOrgao = $contrato->unidade->orgao->nome;

        $nomeFornecedorContrato = $contrato->fornecedor->nome;

        // INICIO DA GERAÇÃO DO PDF
        $pdf = new PdfEncerramentoContrato("P", "mm", "A4");
        $pdf->SetTitle("Relatório Final do Contrato", 1);
        $pdf->AliasNbPages();
        $pdf->AddPage();

        $pdf->SetY($linhaInicial);
        $pdf->SetFont($nomeFonte, 'B', $tamanhoFonteNomeCampo);
        $pdf->Cell(31, 5, utf8_decode("Contrato No.: "), 0, 0, 'L');
        $pdf->SetFont($nomeFonte, '', 10);
        $pdf->Cell(20, 5, utf8_decode($numeroContrato . ' - UG: ' . $codigoUasg . ' - ' . $nomeResumidoUasg), 0, 0, 'L');

        $pdf->SetY($linhaInicial = $linhaInicial + 5);
        $pdf->SetFont($nomeFonte, 'B', $tamanhoFonteNomeCampo);
        $pdf->Cell(31, 5, utf8_decode("Contratante: "), 0, 0, 'L');
        $pdf->SetFont($nomeFonte, '', 10);
        $pdf->Cell(20, 5, utf8_decode($codigoUasg . ' - ' . $nomeUasg), 0, 0, 'L');

        $pdf->SetY($linhaInicial = $linhaInicial + 5);
        $pdf->SetFont($nomeFonte, 'B', $tamanhoFonteNomeCampo);
        $pdf->Cell(31, 5, utf8_decode(""), 0, 0, 'L');
        $pdf->SetFont($nomeFonte, '', 10);
        $pdf->Cell(20, 5, utf8_decode($codigoOrgao . ' - ' . $nomeOrgao), 0, 0, 'L');

        $pdf->SetY($linhaInicial = $linhaInicial + 5);
        $pdf->SetFont($nomeFonte, 'B', $tamanhoFonteNomeCampo);
        $pdf->Cell(31, 5, utf8_decode("Contratada: "), 0, 0, 'L');
        $pdf->SetFont($nomeFonte, '', $tamanhoFonteConteudoCampo);
        $pdf->Cell(20, 5, utf8_decode(strlen($nomeFornecedorContrato) > 65 ? substr($nomeFornecedorContrato, 0, 65) . " [...]" : $nomeFornecedorContrato), 0, 0, 'L');

        $pdf->SetY($linhaInicial = $linhaInicial + 5);
        $pdf->SetFont($nomeFonte, 'B', $tamanhoFonteNomeCampo);
        $pdf->Cell(31, 5, utf8_decode("Número do Processo: "), 0, 0, 'L');
        $pdf->SetFont($nomeFonte, '', $tamanhoFonteConteudoCampo);
        $pdf->Cell(20, 5, utf8_decode($numeroProcessoTermoEncerramento), 0, 0, 'L');

        $pdf->SetY($linhaInicial = $linhaInicial + 5);
        $pdf->SetFont($nomeFonte, 'B', $tamanhoFonteNomeCampo);
        $pdf->Cell(31, 5, utf8_decode("Vigência início: "), 0, 0, 'L');
        $pdf->SetFont($nomeFonte, '', $tamanhoFonteConteudoCampo);
        $pdf->Cell(20, 5, utf8_decode($vigenciaInicio), 0, 0, 'L');

        $pdf->SetY($linhaInicial = $linhaInicial + 5);
        $pdf->SetFont($nomeFonte, 'B', $tamanhoFonteNomeCampo);
        $pdf->Cell(31, 5, utf8_decode("Vigência fim: "), 0, 0, 'L');
        $pdf->SetFont($nomeFonte, '', $tamanhoFonteConteudoCampo);
        $pdf->Cell(20, 5, utf8_decode($vigenciaFim), 0, 0, 'L');

        // verificar quebra de página
        $paginaNova = false;
        if($linhaInicial > $quantidadeMaximaDeLinhas){$pdf->AddPage();$linhaInicial = $linhaInicialPadrao;$paginaNova = true;}

        if($paginaNova){$pdf->SetY($linhaInicial);}else{$pdf->SetY($linhaInicial = $linhaInicial + 10);}
        $pdf->SetFont($nomeFonte, 'B', $tamanhoFonteNomeCampo);
        $pdf->Cell(31, 5, utf8_decode("O objeto contratual foi entregue à Contratante em sua totalidade? "), 0, 0, 'L');

        $pdf->SetY($linhaInicial = $linhaInicial + 5);
        $pdf->SetFont($nomeFonte, '', $tamanhoFonteConteudoCampo);
        $pdf->Cell(20, 5, utf8_decode($isObjetoContratualEntregue), 0, 0, 'L');

        $pdf->SetY($linhaInicial = $linhaInicial + 10);
        $pdf->SetFont($nomeFonte, 'B', $tamanhoFonteNomeCampo);
        $pdf->Cell(31, 5, utf8_decode("Foram cumpridas todas as obrigações financeiras junto à Contratada? "), 0, 0, 'L');

        $pdf->SetY($linhaInicial = $linhaInicial + 5);
        $pdf->SetFont($nomeFonte, '', $tamanhoFonteConteudoCampo);
        $pdf->Cell(20, 5, utf8_decode($isCumpridasObrigacoesFinanceiras), 0, 0, 'L');

        // verificar quebra de página
        $paginaNova = false;
        if($linhaInicial > $quantidadeMaximaDeLinhas){$pdf->AddPage();$linhaInicial = $linhaInicialPadrao;$paginaNova = true;}

        if($paginaNova){$pdf->SetY($linhaInicial);}else{$pdf->SetY($linhaInicial = $linhaInicial + 10);}
        $pdf->SetFont($nomeFonte, 'B', $tamanhoFonteNomeCampo);
        $pdf->Cell(31, 5, utf8_decode("O saldo remanescente dos recursos depositados na Conta-Depósito Vinculada foi liberado para o Fornecedor até o encerramento do contrato? "), 0, 0, 'L');

        $pdf->SetY($linhaInicial = $linhaInicial + 5);
        $pdf->SetFont($nomeFonte, '', $tamanhoFonteConteudoCampo);
        $pdf->Cell(20, 5, utf8_decode($is_saldo_conta_vinculada_liberado), 0, 0, 'L');

        $pdf->SetY($linhaInicial = $linhaInicial + 10);
        $pdf->SetFont($nomeFonte, 'B', $tamanhoFonteNomeCampo);
        $pdf->Cell(31, 5, utf8_decode("A garantia contratual foi integralmente devolvida para a Contratada? "), 0, 0, 'L');

        $pdf->SetY($linhaInicial = $linhaInicial + 5);
        $pdf->SetFont($nomeFonte, '', $tamanhoFonteConteudoCampo);
        $pdf->Cell(20, 5, utf8_decode($is_garantia_contratual_devolvida), 0, 0, 'L');

        $pdf->SetY($linhaInicial = $linhaInicial + 10);
        $pdf->SetFont($nomeFonte, 'B', $tamanhoFonteNomeCampo);
        $pdf->Cell(31, 5, utf8_decode("Grau de satisfação com o desempenho do contrato: "), 0, 0, 'L');

        $pdf->SetY($linhaInicial = $linhaInicial + 5);
        $pdf->SetFont($nomeFonte, '', $tamanhoFonteConteudoCampo);
        $pdf->Cell(20, 5, utf8_decode($grau_satisfacao_desempenho_contrato), 0, 0, 'L');

        // verificar quebra de página
        $paginaNova = false;
        if($linhaInicial > $quantidadeMaximaDeLinhas){$pdf->AddPage();$linhaInicial = $linhaInicialPadrao;$paginaNova = true;}

        if($paginaNova){$pdf->SetY($linhaInicial);}else{$pdf->SetY($linhaInicial = $linhaInicial + 10);}
        $pdf->SetFont($nomeFonte, 'B', $tamanhoFonteNomeCampo);
        $pdf->Cell(31, 5, utf8_decode("A necessidade formalizada no Planejamento da Contratação (DOD/DFD/ETP/TR/PB) foi plenamente atendida pelo contrato? "), 0, 0, 'L');

        // verificar quebra de página
        $paginaNova = false;
        if($linhaInicial > $quantidadeMaximaDeLinhas){$pdf->AddPage();$linhaInicial = $linhaInicialPadrao;$paginaNova = true;}

        $pdf->SetY($linhaInicial = $linhaInicial + 5);
        $pdf->SetFont($nomeFonte, '', $tamanhoFonteConteudoCampo);
        $pdf->Cell(20, 5, utf8_decode($planejamento_contratacao_atendida), 0, 0, 'L');


        // verificar quebra de página
        $paginaNova = false;
        if($linhaInicial > $quantidadeMaximaDeLinhas){$pdf->AddPage();$linhaInicial = $linhaInicialPadrao;$paginaNova = true;}

        if($isMostrarConsecucao){
            $pdf->SetY($linhaInicial = $linhaInicial + 10);
            $pdf->SetFont($nomeFonte, 'B', $tamanhoFonteNomeCampo);
            $pdf->Cell(31, 5, utf8_decode("Informações sobre a consecução dos objetivos que tenham justificado a contratação: "), 0, 0, 'L');

            // linhas
            foreach($arrayConsecucao as $consecucao_objetivos_contratacao){

                $pdf->SetY($linhaInicial = $linhaInicial + 5);
                $pdf->SetFont($nomeFonte, '', $tamanhoFonteConteudoCampo);
                // $pdf->Write(5, utf8_decode($consecucao_objetivos_contratacao));
                $pdf->MultiCell(0,5, utf8_decode($consecucao_objetivos_contratacao),0,'J');

                // verificação de quantidade de linhas necessárias.
                $quantidadeDeLinhasNecessarias = 0;
                $quantidadeDeCaracteresLinha = strlen($consecucao_objetivos_contratacao);
                if($quantidadeDeCaracteresLinha > $quantidadeMaximaDeCaracteresPorLinha){
                    $quantidadeDeLinhasNecessarias = round( $quantidadeDeCaracteresLinha / $quantidadeMaximaDeCaracteresPorLinha );
                    $pdf->SetY($linhaInicial = $linhaInicial + ( $quantidadeDeLinhasNecessarias * 5)  );
                    $linhaInicial = ($linhaInicial - 5);
                }

                // verificar quebra de página
                $paginaNova = false;
                if($linhaInicial > $quantidadeMaximaDeLinhas){$pdf->AddPage();$linhaInicial = $linhaInicialPadrao;$paginaNova = true;}

            }
        }

        // verificar quebra de página
        $paginaNova = false;
        if($linhaInicial > $quantidadeMaximaDeLinhas){$pdf->AddPage();$linhaInicial = $linhaInicialPadrao;$paginaNova = true;}

        if($paginaNova){$pdf->SetY($linhaInicial);}else{$pdf->SetY($linhaInicial = $linhaInicial + 10);}
        $pdf->SetFont($nomeFonte, 'B', $tamanhoFonteNomeCampo);
        $pdf->Cell(31, 5, utf8_decode("Eventuais condutas a serem adotadas para o aprimoramento das atividades da Administração: "), 0, 0, 'L');

        // linhas
        foreach($arraySugestaoLicaoAprendida as $sugestao_licao_aprendida){

            if($sugestao_licao_aprendida != ""){
                $pdf->SetY($linhaInicial = $linhaInicial + 5);
                $pdf->SetFont($nomeFonte, '', $tamanhoFonteConteudoCampo);
                // $pdf->Write(5, utf8_decode($sugestao_licao_aprendida));
                $pdf->MultiCell(0,5, utf8_decode($sugestao_licao_aprendida),0,'J');

                // verificação de quantidade de linhas necessárias.
                $quantidadeDeLinhasNecessarias = 0;
                $quantidadeDeCaracteresLinha = strlen($sugestao_licao_aprendida);
                if($quantidadeDeCaracteresLinha > $quantidadeMaximaDeCaracteresPorLinha){
                    $quantidadeDeLinhasNecessarias = round( $quantidadeDeCaracteresLinha / $quantidadeMaximaDeCaracteresPorLinha );
                    $pdf->SetY($linhaInicial = $linhaInicial + ( $quantidadeDeLinhasNecessarias * 5)  );
                    $linhaInicial = ($linhaInicial - 5);
                }

                // verificar quebra de página
                $paginaNova = false;
                if($linhaInicial > $quantidadeMaximaDeLinhas){$pdf->AddPage();$linhaInicial = $linhaInicialPadrao;$paginaNova = true;}
            }

        }

        // verificar quebra de página
        $paginaNova = false;
        if($linhaInicial > $quantidadeMaximaDeLinhas){$pdf->AddPage();$linhaInicial = $linhaInicialPadrao;$paginaNova = true;}

        $pdf->SetY($linhaInicial = $linhaInicial + 10);
        $pdf->SetFont($nomeFonte, 'B', $tamanhoFonteNomeCampo);
        $pdf->Cell(31, 5, utf8_decode("Observação: "), 0, 0, 'L');

        foreach( $arrayObservacaoTermoEncerramento as $observacaoTermoEncerramento ){

            $pdf->SetY($linhaInicial = $linhaInicial + 5);
            $pdf->SetFont($nomeFonte, '', $tamanhoFonteConteudoCampo);
            $pdf->MultiCell(0,5, utf8_decode($observacaoTermoEncerramento),0,'J');

            // verificação de quantidade de linhas necessárias.
            $quantidadeDeLinhasNecessarias = 0;
            $quantidadeDeCaracteresLinha = strlen($observacaoTermoEncerramento);
            if($quantidadeDeCaracteresLinha > $quantidadeMaximaDeCaracteresPorLinha){
                $quantidadeDeLinhasNecessarias = round( $quantidadeDeCaracteresLinha / $quantidadeMaximaDeCaracteresPorLinha );
                $pdf->SetY($linhaInicial = $linhaInicial + ( $quantidadeDeLinhasNecessarias * 5)  );
                $linhaInicial = ($linhaInicial - 5);
            }


            // verificar quebra de página
            $paginaNova = false;
            if($linhaInicial > $quantidadeMaximaDeLinhas){$pdf->AddPage();$linhaInicial = $linhaInicialPadrao;$paginaNova = true;}
        }


        // verificar quebra de página
        $paginaNova = false;
        if($linhaInicial > $quantidadeMaximaDeLinhas){$pdf->AddPage();$linhaInicial = $linhaInicialPadrao;$paginaNova = true;}

        $codigoUnidadeGestora = session()->get('user_ug');
        $unidade = Unidade::where('codigo', $codigoUnidadeGestora)->first();
        $idMunicipio = $unidade->municipio_id;
        $municipio = Municipio::find($idMunicipio);
        $nomeMunicipio = $municipio->nome;

        $idEstado = $municipio->estado_id;
        $estado = Estado::find($idEstado);
        $siglaEstado = $estado->sigla;
        $nomeEstado = $estado->nome;

        $dataHoje = self::formataDataParaPadraoBrasileiro(date("Y-m-d"));

        // assinatura
        $paginaNova = false;
        if($linhaInicial > $quantidadeMaximaDeLinhas){$pdf->AddPage();$linhaInicial = $linhaInicialPadrao;$paginaNova = true;}

        if($paginaNova){$pdf->SetY($linhaInicial);}else{$pdf->SetY($linhaInicial = $linhaInicial + 25);}
        $pdf->Cell(50,5,"",0,0,'L');
        $pdf->SetFont($nomeFonte,'',$tamanhoFonteNomeCampo);
        $pdf->Cell(90,5,utf8_decode($nomeMunicipio.'/'.$siglaEstado.', '.$dataEncerramentoContrato.' - Unidade Gestora: '.$codigoUnidadeGestora),0,0,'C');

        $paginaNova = false;
        if($linhaInicial > $quantidadeMaximaDeLinhas){$pdf->AddPage();$linhaInicial = $linhaInicialPadrao;$paginaNova = true;}

        if($paginaNova){$pdf->SetY($linhaInicial);}else{$pdf->SetY($linhaInicial = $linhaInicial + 20);}
        $pdf->SetFont($nomeFonte, '', $tamanhoFonteNomeCampo);
        $pdf->Cell(0, 0, utf8_decode("______________________________________________________"), 0, 0, 'C');

        $paginaNova = false;
        if($linhaInicial > $quantidadeMaximaDeLinhas){$pdf->AddPage();$linhaInicial = $linhaInicialPadrao;$paginaNova = true;}

        if($paginaNova){$pdf->SetY($linhaInicial);}else{$pdf->SetY($linhaInicial = $linhaInicial + 3);}
        $pdf->Cell(50,5,"",0,0,'L');
        $pdf->SetFont($nomeFonte,'I',$tamanhoFonteNomeCampo);
        $pdf->Cell(90,5,utf8_decode('Documento assinado eletronicamente'),0,0,'C');

        $paginaNova = false;
        if($linhaInicial > $quantidadeMaximaDeLinhas){$pdf->AddPage();$linhaInicial = $linhaInicialPadrao;$paginaNova = true;}

        if($paginaNova){$pdf->SetY($linhaInicial);}else{$pdf->SetY($linhaInicial = $linhaInicial + 5);}
        $pdf->Cell(50,5,"",0,0,'L');
        $pdf->SetFont($nomeFonte,'B',$tamanhoFonteNomeCampo);
        $pdf->Cell(90,5,utf8_decode($nomeSignatarioEncerramento),0,0,'C');

        if($paginaNova){$pdf->SetY($linhaInicial);}else{$pdf->SetY($linhaInicial = $linhaInicial + 5);}
        $pdf->Cell(50,5,"",0,0,'L');
        $pdf->SetFont($nomeFonte,'',$tamanhoFonteNomeCampo);
        $pdf->Cell(90,5,utf8_decode($nomeFuncaoSignatarioEncerramento),0,0,'C');

        $nome_arquivo = 'Termo de Encerramento contrato ' . $contrato->numero . '.pdf';
        $pdf->Output('D', $nome_arquivo);
    }


}
