<?php

namespace App\Http\Controllers\Gescon;

use App\Models\Contrato;
use App\Models\ContratoAntecipagov;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ContratodomicilioRequest as StoreRequest;
use App\Http\Requests\ContratodomicilioRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class ContratoOperacaoCreditoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ContratoOperacaoCreditoCrudController extends CrudController
{
    public function setup()
    {
        $contrato_id = \Route::current()->parameter('contrato_id');
        $contrato = Contrato::where('id','=',$contrato_id)->where('unidade_id','=',session()->get('user_ug_id'))->first();
        if(!$contrato) {abort('403', config('app.erro_permissao'));}

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Contratodomicilio');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/gescon/contrato/'.$contrato_id.'/operacaocredito');
        $this->crud->setEntityNameStrings('Operação de Crédito - AntecipaGov', 'Operação de Crédito - AntecipaGov');

        $this->crud->addClause('select', [
            'a.id',
            'a.num_operacao',
            'a.situacao_id',
            'ca.contrato_id',
            'ci.descricao',
            'c.numero',
            'a.created_at',
            'a.data_acao',
            'a.domicilio_bancario_id'
        ]);
        $this->crud->addClause('from', 'antecipagov as a');
        $this->crud->addClause('join', 'contrato_antecipagov as ca', function ($join) {
            $join->on('ca.antecipagov_id', '=', 'a.id');
        });
        $this->crud->addClause('join', 'contratos as c', function ($join) use ($contrato_id) {
            $join->on('c.id', '=', 'ca.contrato_id');
        });
        $this->crud->addClause('where', 'c.id', $contrato_id);
        $this->crud->addClause('join', 'codigoitens as ci', 'ci.id', '=', 'a.situacao_id');
        $subquery = \DB::table('antecipagov')
            ->select('num_operacao', \DB::raw('MAX(id) as id'))
            ->groupBy('num_operacao')
            ->toSql();
        $this->crud->addClause('join', \DB::raw("($subquery) as antecipagov"), function ($join) {
            $join->on('antecipagov.num_operacao', '=', 'a.num_operacao')
                ->on('antecipagov.id', '=', 'a.id');
        });
        $this->crud->addClause('orderBy', 'a.num_operacao');
        $this->crud->addButtonFromView('top', 'voltar', 'voltarcontrato', 'end');

        $this->crud->enableExportButtons();
        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');
        $this->crud->allowAccess('show');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        // $this->crud->setFromDb();

        $colunas = $this->Colunas();
        $this->crud->addColumns($colunas);

        // add asterisk for fields that are required in ContratodomicilioRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

        // colunas para listagem
        public function Colunas()
        {
            $colunas = [
                [
                    'name' => 'getContrato',
                    'label' => 'Número do instrumento', // Table column heading
                    'type' => 'model_function',
                    'function_name' => 'getContrato', // the method in your Model
                    'orderable' => true,
                    'visibleInTable' => true, // no point, since it's a large text
                    'visibleInModal' => true, // would make the modal too big
                    'visibleInExport' => true, // not important enough
                    'visibleInShow' => true, // sure, why not
                ],
                [
                    'name' => 'domicilioBancario.banco',
                    'label' => 'Instituição Financeira',
                    'type' => 'text',
                    'orderable' => true,
                    'visibleInTable' => true, // no point, since it's a large text
                    'visibleInModal' => true, // would make the modal too big
                    'visibleInExport' => true, // not important enough
                    'visibleInShow' => true, // sure, why not
                ],
                [
                    'name' => 'domicilioBancario.agencia',
                    'label' => 'Agência',
                    'type' => 'text',
                    'orderable' => true,
                    'visibleInTable' => true, // no point, since it's a large text
                    'visibleInModal' => true, // would make the modal too big
                    'visibleInExport' => true, // not important enough
                    'visibleInShow' => true, // sure, why not
                ],
                [
                    'name' => 'domicilioBancario.conta',
                    'label' => 'Conta Bancária',
                    'type' => 'text',
                    'orderable' => true,
                    'visibleInTable' => true, // no point, since it's a large text
                    'visibleInModal' => true, // would make the modal too big
                    'visibleInExport' => true, // not important enough
                    'visibleInShow' => true, // sure, why not
                ],
                [
                    'name' => 'num_operacao',
                    'label' => 'Número da operação de crédito',
                    'type' => 'text',
                    'orderable' => true,
                    'visibleInTable' => true, // no point, since it's a large text
                    'visibleInModal' => true, // would make the modal too big
                    'visibleInExport' => true, // not important enough
                    'visibleInShow' => true, // sure, why not
                ],
                [
                    'name' => 'getSituacao',
                    'label' => 'Situação', // Table column heading
                    'type' => 'model_function',
                    'function_name' => 'getSituacao', // the method in your Model
                    'orderable' => true,
                    'visibleInTable' => true, // no point, since it's a large text
                    'visibleInModal' => true, // would make the modal too big
                    'visibleInExport' => true, // not important enough
                    'visibleInShow' => true, // sure, why not
                ],
                [
                    'name' => 'data_acao',
                    'label' => 'Atualizado em',
                    'type' => 'date',
                    'orderable' => true,
                    'visibleInTable' => true, // no point, since it's a large text
                    'visibleInModal' => true, // would make the modal too big
                    'visibleInExport' => true, // not important enough
                    'visibleInShow' => true, // sure, why not
                ],
                [
                    'name' => 'created_at',
                    'label' => 'Criado em',
                    'type' => 'date',
                    'orderable' => true,
                    'visibleInTable' => true, // no point, since it's a large text
                    'visibleInModal' => true, // would make the modal too big
                    'visibleInExport' => true, // not important enough
                    'visibleInShow' => true, // sure, why not
                ],

            ];
            return $colunas;
        }

        /**
         * Adiciona todos os filtros desejados para esta funcionalidade
         *
         * @author Saulo Soares <saulosao@gmail.com>
         */
        public function adicionaFiltros()
        {

        }

        public function show($id)
        {
            $content = parent::show($id);

            $this->crud->removeColumns(['action', 'descricao', 'domicilio_bancario_id']);

            $this->crud->addColumn(
                [
                    'name' => 'getSituacao',
                    'label' => 'Situação', // Table column heading
                    'type' => 'model_function',
                    'function_name' => 'getSituacao', // the method in your Model
                    'orderable' => true,
                    'visibleInTable' => true, // no point, since it's a large text
                    'visibleInModal' => true, // would make the modal too big
                    'visibleInExport' => true, // not important enough
                    'visibleInShow' => true, // sure, why not
                ]
            );

            return $content;
        }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
