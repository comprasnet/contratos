<?php

namespace App\Http\Controllers\Gescon;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ContratoParametroRequest as StoreRequest;
use App\Http\Requests\ContratoParametroRequest as UpdateRequest;
use Illuminate\Support\Facades\DB;
use App\Http\Traits\PermissaoTrait;
use App\Models\Codigoitem;
use App\Models\ContratoParametros;

class ContratoParametroCrudController extends CrudController
{
    use PermissaoTrait;

    public function setup()
    {
        [$contrato_id, $contrato] = $this->verificarPermissaoContrato();

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */

        $this->crud->setModel('App\Models\ContratoParametros');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/gescon/contrato/' . $contrato_id . '/parametros');
        $this->crud->setEntityNameStrings('Parâmetro Contrato', 'Parâmetros de Contrato');
        $this->crud->addClause('where', 'contrato_id', '=', $contrato_id);
        $this->crud->orderBy('created_at', 'desc');
        $this->crud->addButtonFromView('top', 'voltar', 'voltarcontrato', 'end');
        $this->crud->enableExportButtons();
        $this->crud->allowAccess('create');
        $this->crud->allowAccess('update');
        $this->crud->allowAccess('delete');
        $this->crud->allowAccess('show');
        if($this->verificaPermissaoConsulta('contrato') === false){
            $this->crud->denyAccess('create');
            $this->crud->denyAccess('update');
            $this->crud->denyAccess('delete');
        }

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $colunas = $this->Colunas();
        $this->crud->addColumns($colunas);

        $campos = $this->Campos($contrato, \Route::current()->parameter('parametro'));
        $this->crud->addFields($campos);

        // add asterisk for fields that are required in ContratoParametroRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function Colunas()
    {
        $colunas = [
            [
                'name' => 'pz_recebimento_provisorio',
                'label' => 'Prazo para recebimento provisório',
                'type' => 'model_function',
                'function_name' => 'getPzRecebimentoProvisorio',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'pz_recebimento_definitivo',
                'label' => 'Prazo para recebimento definitivo',
                'type' => 'model_function',
                'function_name' => 'getPzRecebimentoDefinitivo',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'pz_pagamento',
                'label' => 'Prazo para pagamento',
                'type' => 'model_function',
                'function_name' => 'getPzPagamento',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'data_pagamento_recorrente',
                'label' => 'Data de pagamento recorrente',
                'type' => 'model_function',
                'function_name' => 'getDataPagamentoRecorrente',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not

            ],
            [
                'name' => 'registro',
                'label' => 'Registro de OS/F',
                'type' => 'boolean',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
                'options' => [
                    0 => 'Não',
                    1 => 'Sim'
                ]
            ],
        ];

        return $colunas;
    }

    public function Campos($contrato, $parametro = null)
    {
        $autorizacaoexecucoes = DB::table('autorizacaoexecucoes')
            ->where('contrato_id', $contrato->id)->get()->all();

        $parametro = ContratoParametros::find($parametro);

        $campos = [
            [   // Hidden
                'name' => 'contrato_id',
                'type' => 'hidden',
                'default' => $contrato->id,
            ],
            [
                'name' => 'pz_recebimento_provisorio',
                'label' => 'Prazo para recebimento provisório',
                'type' => 'number_parametros',
                'ico_help' => 'Informar o prazo previsto em normativos, edital e/ou contrato para finalização do Termo de Recebimento Provisório, a partir da execução do objeto.',
                'title1' => 'Úteis',
                'title2' => 'Corridos',
                'subTitle' => true,
                'typeValue' => $parametro->tipo_recebimento_provisorio ?? null
            ],
            [   // Hidden
                'name' => 'tipo_recebimento_provisorio',
                'type' => 'hidden',
            ],
            [
                'name' => 'pz_recebimento_definitivo',
                'label' => 'Prazo para recebimento definitivo',
                'type' => 'number_parametros',
                'ico_help' => 'Informar o prazo previsto em normativos, edital e/ou contrato para finalização do Termo de Recebimento Definitivo, a partir da conclusão do Termo de Recebimento Provisório.',
                'title1' => 'Úteis',
                'title2' => 'Corridos',
                'subTitle' => false,
                'typeValue' => $parametro->tipo_recebimento_definitivo ?? null
            ],
            [   // Hidden
                'name' => 'tipo_recebimento_definitivo',
                'type' => 'hidden',
            ],
            [
                'name' => 'pz_pagamento',
                'label' => 'Prazo para pagamento',
                'type' => 'number_parametros',
                'ico_help' => 'Informar o prazo previsto em normativos, edital e/ou contrato para finalização do pagamento, a partir da conclusão do Termo de Recebimento Definitivo.',
                'title1' => 'Úteis',
                'title2' => 'Corridos',
                'subTitle' => false,
                'typeValue' => $parametro->tipo_pagamento ?? null
            ],
            [   // Hidden
                'name' => 'tipo_pagamento',
                'type' => 'hidden',
            ],
            [
                'name' => 'data_pagamento_recorrente',
                'label' => 'Data de pagamento recorrente',
                'type' => 'number_parametros',
                'ico_help' => 'Informar o vencimento de um pagamento recorrente. Exemplo: 05 (5º dia útil do mês ou todo dia 05 do mês)."',
                'title1' => 'Útil',
                'title2' => 'Fixo',
                'subTitle' => false,
                'typeValue' => $parametro ? $parametro->getTipoPagamentoRecorrente() : null
            ],
            [   // Hidden
                'name' => 'tipo_pagamento_recorrente',
                'type' => 'hidden',
            ],
            [
                'name' => 'registro',
                'label' => 'Obrigatório registro de OS/F',
                'type' => 'radio_parametros',
                'options' => [true => 'Sim', false => 'Não'],
                'attributes' => array_merge(
                    $contrato->verificaEmpenhoOrCartaContrato() || $autorizacaoexecucoes ? ['disabled' => 'disabled'] : []
                ),
                'default' => !$contrato->verificaEmpenhoOrCartaContrato()
            ]
        ];

        return $campos;
    }

    public function store(StoreRequest $request)
    {
        if (
            !$request['pz_recebimento_provisorio'] &&
            !$request['pz_recebimento_definitivo'] &&
            !$request['pz_pagamento'] &&
            !$request['data_pagamento_recorrente']
        ) {
            \Alert::warning('Nada Informado.')->flash();
            return \Redirect::to($this->crud->route . '/create');
        }
        $redirect_location = parent::storeCrud($request);
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        if (
            !$request['pz_recebimento_provisorio'] &&
            !$request['pz_recebimento_definitivo'] &&
            !$request['pz_pagamento'] &&
            !$request['data_pagamento_recorrente']
        ) {
            \Alert::warning('Nada Informado.')->flash();
            return \Redirect::to($this->crud->route . '/create');
        }
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function show($id)
    {
        $content = parent::show($id);

        $this->crud->removeColumns([
            'contrato_id',
            'tipo_recebimento_provisorio',
            'tipo_recebimento_definitivo',
            'tipo_pagamento',
            'tipo_pagamento_recorrente'
        ]);

        return $content;
    }

    public function destroy($id)
    {
        $this->crud->hasAccessOrFail('delete');
        $this->crud->setOperation('delete');

        // get entry ID from Request (makes sure its the last ID for nested resources)
        $id = $this->crud->getCurrentEntryId() ?? $id;

        return $this->crud->delete($id);
    }
}
