<?php

namespace App\Http\Controllers\Gescon;

use App\Http\Traits\Formatador;
use App\Models\BackpackUser;
use App\Models\Codigo;
use App\Models\Codigoitem;
use App\Models\Contratoarquivo;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Backpack\CRUD\CrudPanel;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Http;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Response;
/**
 * Class ConsultaresponsavelCrudController
 * @package App\Http\Controllers\Gescon
 * @property-read CrudPanel $crud
 * @author Saulo Soares <saulosao@gmail.com>
 */
class ConsultaarquivoCrudController extends ConsultaContratoBaseCrudController
{
    use Formatador;

    /**
     * Adiciona as colunas específicas a serem exibidas bem como suas definições
     *
     * @author Saulo Soares <saulosao@gmail.com>
     */
    public function adicionaColunasEspecificasNaListagem()
    {
        $this->adicionaColunaTipo();
        $this->adicionaColunaProcesso();
        $this->adicionaColunaSei();
        $this->adicionaColunaDescricao();
        $this->adicionaColunaArquivo();
    }

    /**
     *Adiciona o campo Tipo na listagem
     *
     * @author Saulo Soares <saulosao@gmail.com>
     */
    private function adicionaColunaTipo()
    {
        $this->crud->addColumn([
            'name' => 'tipoArquivo',
            'label' => 'Tipo',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
            'searchLogic' => function (Builder $query, $column, $searchTerm) {
                $query->orWhere('contrato_arquivos.tipo', 'ilike', "%$searchTerm%");
                $query->orWhere('codigoitens.descricao', 'ilike', "%$searchTerm%");
            },
        ]);
    }

    private function adicionaColunaProcesso()
    {
        $this->crud->addColumn([
            'name' => 'contrato.processo',
            'label' => 'Processo',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true, // no point, since it's a large text
            'visibleInModal' => true, // would make the modal too big
            'visibleInExport' => true, // not important enough
            'visibleInShow' => true, // sure, why not
            'searchLogic' => function (Builder $query, $column, $searchTerm) {
                $query->orWhere('contratos.processo',
                    'ilike', '%' . $searchTerm . '%'
                );
            }
        ]);
    }

    private function adicionaColunaSei()
    {
        $this->crud->addColumn([   // Number
            'name' => 'sequencial_documento',
            'label' => 'Nº SEI / Chave Acesso Sapiens',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true, // no point, since it's a large text
            'visibleInModal' => true, // would make the modal too big
            'visibleInExport' => true, // not important enough
            'visibleInShow' => true, // sure, why not
        ]);
    }

    private function adicionaColunaDescricao()
    {
        $this->crud->addColumn([   // Number
            'name' => 'descricao',
            'label' => 'Descrição',
            'type' => 'text',
            'limit' => 1000,
            'orderable' => true,
            'visibleInTable' => true, // no point, since it's a large text
            'visibleInModal' => true, // would make the modal too big
            'visibleInExport' => true, // not important enough
            'visibleInShow' => true,
            'searchLogic' => function (Builder $query, $column, $searchTerm) {
                $query->orWhere('contrato_arquivos.descricao',
                    'ilike', '%' . $searchTerm . '%'
                );
            }
        ]);
    }

    private function adicionaColunaArquivo()
    {

        $this->crud->addColumn([

            'name' => 'getLinkAnexo',
            'label' => 'Arquivos',
            'type' => 'model_function_sei',
            'priority' => 1,
            'function_name' => 'getLinkAnexoTemporario',
            'orderable' => true,
            'visibleInTable' => true, // no point, since it's a large text
            'visibleInModal' => true, // would make the modal too big
            'visibleInExport' => true, // not important enough
            'visibleInShow' => true, // sure, why not
        ]);

    }

    /**
     * Adiciona filtros específicos a serem apresentados
     *
     * @author Saulo Soares <saulosao@gmail.com>
     */
    public function aplicaFiltrosEspecificos()
    {
        $this->aplicaFiltroTipo();
    }

    /**
     * Adiciona o filtro ao campo Tipo
     *
     * @author Saulo Soares <saulosaso@gmail.com>
     */
    private function aplicaFiltroTipo()
    {
        $campo = [
            'name' => 'tipo',
            'type' => 'select2_multiple',
            'label' => 'Tipo'
        ];

        $dados = $this->retornaTiposParaCombo();

        $this->crud->addFilter(
            $campo,
            $dados,
            function ($value) {
                $this->crud->addClause('whereIn'
                    , 'contrato_arquivos.tipo', json_decode($value));
            }
        );
    }

    /**
     * Retorna array de  para combo de filtro
     *
     * @return array
     * @author Saulo Soares <saulosao@gmail.com>
     */
    private function retornaTiposParaCombo()
    {
        $dados = Codigoitem::select('descricao', 'id');

        $dados->where('codigo_id', Codigo::TIPO_ARQUIVOS_CONTRATO);
        $dados->orderBy('descricao');

        return $dados->pluck('descricao', 'id')->toArray();

    }

    /**
     * Configurações iniciais do Backpack
     *
     * @throws Exception
     * @author Saulo Soares <saulosao@gmail.com>
     */
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */

        $this->crud->setModel('App\Models\Contratoarquivo');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/gescon/consulta/arquivos');
        $this->crud->setEntityNameStrings('Arquivo', 'Arquivos');
        $this->crud->setHeading('Consulta Arquivos por Contrato');

        $this->crud->addClause('leftJoin', 'contratos',
            'contratos.id', '=', 'contrato_arquivos.contrato_id'
        );
        $this->crud->addClause('leftJoin', 'fornecedores',
            'fornecedores.id', '=', 'contratos.fornecedor_id'
        );
        $this->crud->addClause('join', 'codigoitens',
            'codigoitens.id', '=', 'contrato_arquivos.tipo'
        );
        $this->crud->addClause('select', [
            'contratos.*',
            'fornecedores.*',
            'codigoitens.descricao as tipoArquivo',
            // Tabela principal deve ser sempre a última da listagem!
            'contrato_arquivos.*'
        ]);

        // Apenas ocorrências da unidade atual
        $this->crud->addClause('where', 'contratos.unidade_id', '=', session('user_ug_id'));

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $this->defineConfiguracaoPadrao();
    }

    /**
     * Action para exibição de um único registro
     *
     * @param int $id
     * @return \Backpack\CRUD\app\Http\Controllers\Operations\Response
     * @author Saulo Soares <saulosao@gmail.com>
     */
    public function show($id)
    {
        $content = parent::show($id);

        $this->crud->removeColumns([
            'contrato_id',
            'tipo',
            'processo',
        ]);

        return $content;
    }

    private function retornaDadosArquivo(int $id)
    {
        $dados = Contratoarquivo::select('arquivos','contrato_arquivos.descricao','codigoitens.descres as tipo',
            'contrato_arquivos.envio_v2', 'contrato_arquivos.rascunho')
            ->join('codigoitens','contrato_arquivos.tipo','=','codigoitens.id')
            ->where('contrato_arquivos.id', $id);

        $contratoArquivoRecuperador = $dados->get()->toArray()[0];

        if(is_array($contratoArquivoRecuperador['arquivos'])) {
            $contratoArquivoRecuperador['arquivoFormatado'] = $contratoArquivoRecuperador['arquivos'][0];
            $contratoArquivoRecuperador['tipo'] = $contratoArquivoRecuperador['arquivos'][0]['tipo'];
        } else {

            $contratoArquivoRecuperador['arquivoFormatado'] = $contratoArquivoRecuperador['arquivos'];
            $contratoArquivoRecuperador['tipo'] = $contratoArquivoRecuperador['tipo'];
        }

        $contratoArquivoRecuperador['descricao'] = $this->montarNomeArquivoDownload($contratoArquivoRecuperador['arquivoFormatado'], $contratoArquivoRecuperador['descricao']);

        return $contratoArquivoRecuperador;

    }

    public function donwloadArquivoContrato() {

        $idContratoArquivo = \Route::current()->parameter('id');

        $dadosContratoArquivo = $this->retornaDadosArquivo($idContratoArquivo);


        $storagePath = Storage::disk('local')->path($dadosContratoArquivo['arquivoFormatado']);

        $tipoSituacao = CodigoItem::where('descres', 'DECL_OPM')->first();


        if ($tipoSituacao->descres === $dadosContratoArquivo['tipo'] && $dadosContratoArquivo['envio_v2'] === true
            && $dadosContratoArquivo['rascunho'] === false) {
            ## Constrói o caminho para o arquivo dentro do diretório public
            $pdfPath = $dadosContratoArquivo['arquivoFormatado'];
            $host = str_replace('appredirect/', '', env('URL_CONTRATO_VERSAO_DOIS'));

            ## Monta o URL completo com o endereço, colocar IP do container
            //$pdfUrl = 'http://172.20.98.76:83/' .'storage/' . $pdfPath;

            // Monta o URL completo com o endereço desejado
            $pdfUrl = $host .'storage/' . $pdfPath;


            // Obtém o nome do arquivo a partir da URL (ou define manualmente)
            $fileName = basename($pdfUrl);


            $client = new Client();


            $response = $client->get($pdfUrl);


            // Obtém o corpo da resposta HTTP
            $fileContent = $response->getBody()->getContents();

            // Cria uma resposta de download com o conteúdo do arquivo
            return Response::streamDownload(function () use ($fileContent) {
                echo $fileContent;
            }, $fileName);


         //   return response()->download($pdfUrl, $dadosContratoArquivo['descricao']);
        }



        // return Storage::download($storagePath,$dadosContratoArquivo['descricao']);
        return response()->download($storagePath,$dadosContratoArquivo['descricao'] );
    }

}


