<?php

namespace App\Http\Controllers\Gescon;

use App\Jobs\RotinaatualizaindicesbacenJobs;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CalculadoraIndiceRequest as StoreRequest;
use App\Http\Requests\CalculadoraIndiceRequest as UpdateRequest;

use App\Models\TipoIndices;
use App\services\Indices\Implementations\Calculadora;
//use App\services\Indices\Implementations\ImportacaoIST;
//use App\services\Indices\Implementations\ImportacaoDadosBacenWebserviceToLocalTableService;
//use App\services\IPEA\Implementations\IpeaRest;
use Backpack\CRUD\CrudPanel;
use Carbon\Carbon;
use Exception;

/**
 * Class Contrato_minutaCrudController
 * @package App\Http\Controllers\Gescon
 * @property-read CrudPanel $crud
 */
class CalculadoraIndiceCrudController extends CrudController
{
    public $tipo_indice_id;
    public $minuta_id;
    public $tipoindice;
    public $num_processo;
    protected $servico_sei_status = true;
    protected $filtros_usuario = [
            'data-inicio' => '',
            'data-fim' => '',
            'valor' => '',
            'tipo-indice' => ''
    ];
    public function setup()
    {
        if (backpack_user()->hasRole('Desenvolvedor')) {
            abort('403', config('app.erro_permissao'));
        }

        $tipo_indice_id = \Route::current()->parameter('indice_id');
        $this->tipo_indice_id = $tipo_indice_id;
        if (!is_null($this->tipo_indice_id)) {
            $tipoindice = TipoIndices::where('id', '=', $tipo_indice_id);
            $this->tipoindice = $tipoindice;
        }

        // if (!$tipoindice) {
        //     abort('403', config('app.erro_permissao'));
        // }
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setListView('backpack::crud.calculadora_indices.list');
        $this->crud->setModel('App\Models\TipoIndices');

        //$this->crud->setRoute(config('backpack.base.route_prefix') . '/contrato_minuta');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/gescon/tipoindices/calculadora');

        $this->crud->setEntityNameStrings('Calculadora de índices', 'Calculadora de índices');
        $this->crud->autoFocusOnFirstField = false;
        //$this->crud->addClause('where', 'contrato_id', '=', $minuta->Contrato->id);

        //$this->crud->addButtonFromView('top', 'voltar', 'voltarcontrato', 'end');
        // $this->crud->enableExportButtons();


        //$this->crud->enableExportButtons();
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration Global
        |--------------------------------------------------------------------------
        */


        $this->crud->allowAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');
        $this->crud->denyAccess('show');

        //(backpack_user()->can('contrato_inserir')) ? $this->crud->allowAccess('create') : null;
        //(backpack_user()->can('contrato_editar')) ? $this->crud->allowAccess('update') : null;
        //(backpack_user()->can('contrato_deletar')) ? $this->crud->allowAccess('delete') : null;

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */
        // TODO: remove setFromDb() and manually define Fields and Columns
        //$this->crud->setFromDb();

        $campos = $this->Campos();
        $this->crud->addFields($campos);

        // add asterisk for fields that are required in Contrato_minutaRequest
        //$this->crud->setRequiredFields(StoreRequest::class, 'create');
        //$this->crud->setRequiredFields(UpdateRequest::class, 'edit');

    }



    public function Campos($filtros_usuario = '')
    {
        $data_inicio = Carbon::now()->format('Y-m-d');
        $data_fim = Carbon::now()->format('Y-m-d');
        if($filtros_usuario != ''){
            $data_inicio = Carbon::createFromFormat('d/m/Y',$filtros_usuario['data-inicio'])->format('Y-m-d');
            $data_fim = Carbon::createFromFormat('d/m/Y', $filtros_usuario['data-fim'])->format('Y-m-d');
        }
        /* $data_inicio_default = ($this->filtros_usuario['data-inicio'] != '') ? $this->filtros_usuario['data-inicio'] : Carbon::now()->format('d/m/Y');
        $data_fim_default = ($this->filtros_usuario['data-fim'] != '') ? $this->filtros_usuario['data-fim'] : Carbon::now()->format('d/m/Y'); */
        $valor_default = ($this->filtros_usuario['valor'] != '') ? $this->filtros_usuario['valor'] : '0';
        $tipo_indice_default = ($this->filtros_usuario['tipo-indice'] != '') ? $this->filtros_usuario['tipo-indice'] : '';
        try {
            $campos = [

                [
                    'name' => 'event_date_range', // a unique name for this field
                    'start_name' => 'data_inicio', // the db column that holds the start_date
                    'end_name' => 'data_fim', // the db column that holds the end_date
                    'label' => 'Período',
                    'type' => 'date_range',
                    'ico_help' => 'Serão considerados para composição do índice de reajuste os meses compreendidos no período indicado nos campos “Data início” e “Data fim”.',
                    // OPTIONALS
                    'start_default' => $data_inicio, // default value for start_date
                    'end_default' => $data_fim, // default value for end_date
                    'date_range_options' => [ // options sent to daterangepicker.js
                        'timePicker' => false,
                        'locale' => ['format' => 'DD/MM/YYYY'],
                        'language' => 'pt',
                        //'startDate' => $data_inicio_default,
                        //'endDate' => $data_fim_default,
                        'minDate' => '01/07/1994',
                        'maxDate' => Carbon::now()->format('d/m/Y'),
                    ],
                    'wrapperAttributes' => [
                        'class' => 'form-group col-xs-12 col-md-6'
                    ]
                ],
                [ // Indices
                    'name' => 'tipo_indice',
                    'label' => "Índices",
                    'type' => 'select_from_array',
                    'options' => TipoIndices::all()->pluck('nome', 'codigo'),
                    'allows_null' => false,
                    'default' => $tipo_indice_default,
                    'wrapperAttributes' => [
                        'class' => 'form-group col-xs-12 col-md-6'
                    ]
                    // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
                ],
                [ //valor
                    'name' => 'valor',
                    'label' => "Valor a ser corrigido",
                    'type' => 'text',
                    'prefix' => "R$",
                    'default' => number_format($valor_default, 2, '.', ','),
                    'wrapperAttributes' => [
                        'class' => 'form-group col-xs-12 valor-monetario'
                    ]
                ],
            ];

            return $campos;
        } catch (\Exception $e) {

            $unidades_origem = ['0' => 'A informação não foi carregada'];
            $processos_relacionados_field = ['0' => 'A informação não foi carregada#-'];
            return \Alert::warning($e->getMessage())->flash();
        }
    }
    function index()
    {
        return $this->create();
    }
    /**
     * Show the form for creating inserting a new row.
     *
     * @return Response
     */
    public function create()
    {
       //IMPORTAÇÃO IST/ANATEL
       /* $teste = new ImportacaoIST();
       $teste->handle(); */

       //IMPORTAÇÃO DAS SÉRIES DO BANCO CENTRAL
       /* $importacaoBacen = new ImportacaoDadosBacenWebserviceToLocalTableService();
       $importacaoBacen->handle(); */

       //IMPORTAÇÃO ICTI
      /* $importacaoIpea = new IpeaRest();
       $importacaoIpea->handle(); */


        if(!isset($this->data['resultado_calculo'])){
            $this->data['resultado_calculo'] = '';
        }
        if ($this->servico_sei_status == false) {
            return redirect('/gescon/tipoindices/' . $this->tipo_indice_id . '/calculadora');
        }
        $this->crud->hasAccessOrFail('create');
        $this->crud->setOperation('create');

        // prepare the fields you need to show
        $this->data['saveAction']['active']['label']='Calcular';
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getCreateFields();
        $this->data['title'] = $this->crud->getTitle() ?? trans('backpack::crud.add') . ' ' . $this->crud->entity_name;
        $this->data['novaRota'] = config('backpack.base.route_prefix') . '/gescon/tipoindices/calculadora/create';
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view('vendor.backpack.crud.calculadora_indices.create', $this->data);
    }

    private function formatDateDiaMesAno($value)
    {
        return  Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('d/m/Y');
    }

    private function convertMaskedMoneyValueToDecimal($maskedNumber = 0)
    {
        return number_format(str_replace(",", ".", str_replace(".", "", $maskedNumber)), 2, '.', '');
    }
    public function store(StoreRequest $request)
    {
        /* if(is_null($request->valor)){
            return redirect('/gescon/tipoindices/calculadora/create');
        } */
        $inicio = $request->data_inicio;
        $fim = $request->data_fim;
        $valor = $this->convertMaskedMoneyValueToDecimal($request->valor);
        $tipo_indice = $request->tipo_indice;
        $parametros_usuario = [
            'data_inicio' => $request->data_inicio,
            'data_fim' => $request->data_fim,
            'valor' => $request->valor,
            'codigo_indice' => $request->tipo_indice
        ];
        $this->data['parametros_usuario'] = $parametros_usuario;

        $this->filtros_usuario['data-inicio'] = $this->formatDateDiaMesAno($inicio);
        $this->filtros_usuario['data-fim'] = $this->formatDateDiaMesAno($fim);
        $this->filtros_usuario['valor'] = $valor;
        $this->filtros_usuario['tipo-indice'] = $tipo_indice;

        $campos = $this->Campos($this->filtros_usuario);
        $this->crud->addFields($campos);

        try{
            $calculadora = new Calculadora($inicio, $fim, $valor, $tipo_indice);
            $resultado_calculo = $calculadora->handle();
            $this->data['resultado_calculo'] = $resultado_calculo;
            //throw new Exception('deu ruim');
        }catch(Exception $e){
            $this->data['resultado_calculo'] = $e->getMessage();
            \Alert::warning($e->getMessage())->flash();
             return redirect('/gescon/tipoindices/calculadora/create');
        }

        return $this->create();

        // \Alert::success('Documento enviado com sucesso.')->flash();

        // return redirect('/admin/tipoindices/calculadora');
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
