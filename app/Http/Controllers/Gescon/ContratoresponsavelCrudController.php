<?php

namespace App\Http\Controllers\Gescon;

use App\Http\Traits\PermissaoTrait;
use App\Models\BackpackUser;
use App\Models\Codigoitem;
use App\Models\Contrato;
use App\Models\Role;
use App\Models\Instalacao;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ContratoresponsavelRequest as StoreRequest;
use App\Http\Requests\ContratoresponsavelRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use Illuminate\Support\Facades\DB;
use stdClass;

/**
 * Class ContratoresponsavelCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ContratoresponsavelCrudController extends CrudController
{
    use PermissaoTrait;
    public function setup()
    {
//        #226
//        //Proibir este controller para contrato do tipo Empenho(Gerado por uma Minuta de Empenho do Tipo contrato com força de empenho)
//        if(Contrato::find(\Route::current()->parameter('contrato_id'))->tipo->descricao === "Empenho") {
//            abort('403', config('app.erro_permissao'));
//        }

        [$contrato_id, $contrato] = $this->verificarPermissaoContrato();

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Contratoresponsavel');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/gescon/contrato/'.$contrato_id.'/responsaveis');
        $this->crud->setEntityNameStrings('Responsável do Contrato', 'Responsáveis - Contrato');
        $this->crud->addClause('select', 'contratoresponsaveis.*');
        $this->crud->addClause('join', 'users', 'users.id', '=', 'contratoresponsaveis.user_id' );
        $this->crud->addClause('where', 'contratoresponsaveis.contrato_id', '=', $contrato_id);
        $this->crud->addButtonFromView('top', 'voltar', 'voltarcontrato', 'end');
        $this->crud->enableExportButtons();
        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');
        $this->crud->allowAccess('show');

        (backpack_user()->can('responsavel_inserir') || backpack_user()->can('apoio_adm_responsavel_inserir')) ? $this->crud->allowAccess('create') : null;
        (backpack_user()->can('responsavel_editar') || backpack_user()->can('apoio_adm_responsavel_editar')) ? $this->crud->allowAccess('update') : null;
        (backpack_user()->can('responsavel_deletar') || backpack_user()->can('apoio_adm_responsavel_deletar')) ? $this->crud->allowAccess('delete') : null;
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
//        $this->crud->setFromDb();



        $this->crud->addColumns([
            [
                'name' => 'getContrato',
                'label' => 'Número do instrumento', // Table column heading
                'type' => 'model_function',
                'function_name' => 'getContrato', // the method in your Model
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
//                'searchLogic' => function ($query, $column, $searchTerm) {
//                    $query->orWhereHas('unidade_id', function ($q) use ($column, $searchTerm) {
//                        $q->where('nome', 'like', '%' . $searchTerm . '%');
//                        $q->where('codigo', 'like', '%' . $searchTerm . '%');
//                            ->orWhereDate('depart_at', '=', date($searchTerm));
//                    });
//                },
            ],
            [
                'name' => 'getUser',
                'label' => 'Usuário', // Table column heading
                'type' => 'model_function',
                'function_name' => 'getUser', // the method in your Model
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
                'searchLogic' => function ($query, $column, $searchTerm) {
                    $query->orWhere('users.name', 'ilike', "%" . $searchTerm . "%");
                    $query->orWhere('users.cpf', 'ilike', "%" . $searchTerm . "%");
                },
            ],
            [
                'name' => 'telefone_fixo',
                'label' => 'Telefone Fixo',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'telefone_celular',
                'label' => 'Telefone Celular',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'getFuncao',
                'label' => 'Função', // Table column heading
                'type' => 'model_function',
                'function_name' => 'getFuncao', // the method in your Model
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
//                'searchLogic' => function ($query, $column, $searchTerm) {
//                    $query->orWhereHas('unidade_id', function ($q) use ($column, $searchTerm) {
//                        $q->where('nome', 'like', '%' . $searchTerm . '%');
//                        $q->where('codigo', 'like', '%' . $searchTerm . '%');
//                            ->orWhereDate('depart_at', '=', date($searchTerm));
//                    });
//                },
            ],
            [
                'name' => 'getInstalacao',
                'label' => 'Instalação / Unidade', // Table column heading
                'type' => 'model_function',
                'function_name' => 'getInstalacao', // the method in your Model
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
//                'searchLogic' => function ($query, $column, $searchTerm) {
//                    $query->orWhereHas('unidade_id', function ($q) use ($column, $searchTerm) {
//                        $q->where('nome', 'like', '%' . $searchTerm . '%');
//                        $q->where('codigo', 'like', '%' . $searchTerm . '%');
//                            ->orWhereDate('depart_at', '=', date($searchTerm));
//                    });
//                },
            ],
            [
                'name' => 'portaria',
                'label' => 'Portaria',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'data_inicio',
                'label' => 'Data Início',
                'type' => 'date',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'data_fim',
                'label' => 'Data Fim',
                'type' => 'date',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'situacao',
                'label' => 'Situação',
                'type' => 'boolean',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
                // optionally override the Yes/No texts
                'options' => [0 => 'Inativo', 1 => 'Ativo']
            ],
        ]);


        $con = $contrato->where('id', '=', $contrato_id)
            ->pluck('numero', 'id')
            ->toArray();

      /*$users = BackpackUser::select(DB::raw("CONCAT(cpf,' - ',name) AS nome"), 'id')
            ->where('ugprimaria','=',session()->get('user_ug_id'))
            ->orWhereHas('unidades', function ($query) {
                $query->where('unidade_id', '=', session()->get('user_ug_id'));
            })
            ->orderBy('name', 'asc')
            ->pluck('nome', 'id')
            ->toArray();*/

        //issue 334
        $codResCont = Role::where('name', '=', 'Responsável por Contrato')->get()->first()->id;

        $users =  DB::table('users')
           ->select(DB::raw("CONCAT(CONCAT('***',substring(cpf, 4, 9),'**'), ' - ', name) as nome, substring(name, 0, 17) as ordenador"), "id")
           ->where(function ($query) {
               $query->where('ugprimaria', session()->get('user_ug_id'))
                   ->orWhereExists(function ($subquery) {
                       $subquery->select(DB::raw(1))
                           ->from('unidades')
                           ->join('unidadesusers', 'unidades.id', '=', 'unidadesusers.unidade_id')
                           ->whereColumn('users.id', 'unidadesusers.user_id')
                           ->where('unidade_id', session()->get('user_ug_id'))
                           ->whereNull('unidades.deleted_at');
                   });
           })
           ->whereNull('deleted_at')
           ->union(function ($query) use ($contrato_id, $codResCont) {
               $query->select(DB::raw("DISTINCT CASE WHEN und_sec.id IS NOT NULL THEN CONCAT(CONCAT('***',substring(cpf, 4, 9),'**'), ' - ', name, ' UG: ', und_sec.codigo) ELSE CONCAT(CONCAT('***',substring(cpf, 4, 9),'**'), ' - ', name, ' UG: ', und_prn.codigo) END AS nome, substring(name, 0, 17) as ordenador"), "users.id")
                   ->from('users')
                   ->join('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')
                   ->join('unidades AS und_prn', 'und_prn.id', '=', 'users.ugprimaria')
                   ->leftJoin('unidadesusers', 'unidadesusers.user_id', '=', 'users.id')
                   ->leftJoin('unidades AS und_sec', 'und_sec.id', '=', 'unidadesusers.unidade_id')
                   ->leftJoin('contratounidadesdescentralizadas AS c', function ($join) {
                       $join->on('und_prn.id', '=', 'c.unidade_id')
                           ->orOn('und_sec.id', '=', 'c.unidade_id');
                   })
                   ->where(function ($query) use ($contrato_id) {
                       $query->whereIn('ugprimaria', function ($subquery) use ($contrato_id) {
                           $subquery->select('unidade_id')
                               ->from('contratounidadesdescentralizadas')
                               ->where('contrato_id', $contrato_id);
                       })
                           ->orWhereExists(function ($subquery) use ($contrato_id) {
                               $subquery->select(DB::raw(1))
                                   ->from('unidades')
                                   ->join('unidadesusers', 'unidades.id', '=', 'unidadesusers.unidade_id')
                                   ->join('contratounidadesdescentralizadas', function ($join) {
                                       $join->on('unidadesusers.unidade_id', '=', 'contratounidadesdescentralizadas.unidade_id');
                                   })
                                   ->whereColumn('users.id', 'unidadesusers.user_id')
                                   ->whereNull('unidades.deleted_at')
                                   ->where('contratounidadesdescentralizadas.contrato_id', $contrato_id);
                           });
                   })
                   ->where('c.contrato_id', $contrato_id)
                   ->whereNull('users.deleted_at')
                   ->where('model_has_roles.role_id', $codResCont);
           })
           ->orderBy('ordenador', 'asc')
           ->pluck('nome', 'id')
           ->toArray();

            $funcoes = Codigoitem::whereHas('codigo', function ($query) {
                $query->where('descricao', '=', 'Função Contrato');
            })->orderBy('descricao')->pluck('descricao', 'id')->toArray();

            $instalacoes = Instalacao::orderBy('nome', 'asc')
                ->pluck('nome', 'id')
                ->toArray();

        $this->crud->addFields([
            [ // select_from_array
                'name' => 'contrato_id',
                'label' => "Número do instrumento",
                'type' => 'select_from_array',
                'options' => $con,
                'allows_null' => false,
//                'default' => 'one',
                // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
            ],
            [ // select_from_array
                'name' => 'user_id',
                'label' => "Usuário",
                'type' => 'select2_from_array',
                'options' => $users,
                'allows_null' => true,
//                'default' => 'one',
                // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
            ],
            [
                'name' => 'telefone_fixo',
                'label' => 'Telefone Fixo',
                'type' => 'telefone',
                'allows_null' => false,
            ],
            [
                'name' => 'telefone_celular',
                'label' => 'Telefone Celular',
                'type' => 'celular',
                'allows_null' => false,
            ],
            [ // select_from_array
                'name' => 'funcao_id',
                'label' => "Função",
                'type' => 'select2_from_array',
                'options' => $funcoes,
                'allows_null' => true,
//                'default' => 'one',
                // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
            ],
            [ // select_from_array
                'name' => 'instalacao_id',
                'label' => "Instalação / Unidade",
                'type' => 'select2_from_array',
                'options' => $instalacoes,
                'allows_null' => true,
//                'default' => 'one',
                // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
            ],
            [
                'name' => 'portaria',
                'label' => 'Portaria',
                'type' => 'text',
                'attributes' => [
                    'onkeyup' => "maiuscula(this)"
                ]
            ],
            [
                'name' => 'data_inicio',
                'label' => 'Data Início',
                'type' => 'date',
            ],
            [
                'name' => 'data_fim',
                'label' => 'Data Fim',
                'type' => 'date',
            ],
            [ // select_from_array
                'name' => 'situacao',
                'label' => "Situação",
                'type' => 'select_from_array',
                'options' => [1 => 'Ativo', 0 => 'Inativo'],
                'allows_null' => false,
//                'attributes' => [
//                    'disabled' => 'disabled',
//                ],
//                'default' => 'one',
                // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
            ],
        ]);

        // add asterisk for fields that are required in ContratoresponsavelRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
    public function show($id)
    {
        $content = parent::show($id);

        $this->crud->removeColumn('contrato_id');
        $this->crud->removeColumn('user_id');
        $this->crud->removeColumn('funcao_id');
        $this->crud->removeColumn('instalacao_id');

        return $content;
    }

}
