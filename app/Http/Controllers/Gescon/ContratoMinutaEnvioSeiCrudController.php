<?php

namespace App\Http\Controllers\Gescon;

use App\Http\Controllers\classSEI;
use App\Http\Traits\EnviaPncpTrait;
use App\Models\Contrato;

use App\Models\Contratohistorico;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\Contrato_minuta_envio_seiRequest as StoreRequest;
use App\Http\Requests\Contrato_minuta_envio_seiRequest as UpdateRequest;
use App\Models\Codigoitem;
use App\Models\Codigo;
use App\Models\Contratoarquivo;
use App\Models\ContratoMinuta;
use Backpack\CRUD\CrudPanel;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * Class Contrato_minutaCrudController
 * @package App\Http\Controllers\Gescon
 * @property-read CrudPanel $crud
 */
class ContratoMinutaEnvioSeiCrudController extends CrudController
{
    use EnviaPncpTrait;
    public $contrato_id;
    public $minuta_id;
    public $minuta;
    public $num_processo;
    protected $servico_sei_status = true;
    public function setup()
    {
        $minuta_id = \Route::current()->parameter('minuta_id');

        $minuta = ContratoMinuta::with('Contrato')->where('id', '=', $minuta_id);
        $this->minuta = $minuta;

        if (!$minuta) {
            abort('403', config('app.erro_permissao'));
        }
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\ContratoMinuta');

        //$this->crud->setRoute(config('backpack.base.route_prefix') . '/contrato_minuta');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/gescon/contrato/' . $minuta->first()->Contrato->id . '/minutas/' . $minuta->first()->id . '/sei');
        $this->contrato_id = $minuta->first()->Contrato->id;
        $this->minuta_id = $minuta->first()->id;
        $this->crud->setEntityNameStrings('Envio SEI', 'Envio SEI');
        //$this->crud->addClause('where', 'contrato_id', '=', $minuta->Contrato->id);

        //$this->crud->addButtonFromView('top', 'voltar', 'voltarcontrato', 'end');
       // $this->crud->enableExportButtons();

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration Global
        |--------------------------------------------------------------------------
        */


        $this->crud->allowAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');
        $this->crud->denyAccess('show');

        //(backpack_user()->can('contrato_inserir')) ? $this->crud->allowAccess('create') : null;
        //(backpack_user()->can('contrato_editar')) ? $this->crud->allowAccess('update') : null;
        //(backpack_user()->can('contrato_deletar')) ? $this->crud->allowAccess('delete') : null;

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */
        // TODO: remove setFromDb() and manually define Fields and Columns
        //$this->crud->setFromDb();


        $campos = $this->Campos($minuta);
        $this->crud->addFields($campos);

        // add asterisk for fields that are required in Contrato_minutaRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

    }


    public function Campos($minuta)
    {
        $processos_relacionados_field  = [];
        $contrato = $minuta->first()->Contrato;
        $num_instrumento = [
            $contrato->id => $contrato->numero
        ];
        $num_processo = $contrato->processo;
        $this->num_processo = $num_processo;
        $sei_online = true;
        $procedimento_id = '';
        $lista_serie = [];
        try{

            $sei = new classSEI();

            /*************************/
            /*****LISTA DE SÉRIES
            /*************************/
            $tipo_documento_compras = $minuta->first()->getTipoDeDocumento();

            $search_doctype = $tipo_documento_compras;
            $neadle_without_preposition = str_replace("de", "", $search_doctype);
            $search = [
                'original' => $search_doctype,
                'without_preposition' => $neadle_without_preposition
            ];
            $series = Collect($sei->listarSeries())->where('Aplicabilidade', '<>','E')->pluck('Nome', 'IdSerie');

            //localiza id série tipo documento ccontrato -> SEI
            $lista_serie = $series->filter(function ($item) use ($search) {
                return ((false !== stristr($item, $search['original']) || false !== stristr($item, $search['without_preposition'])));
            });

            if($lista_serie->count() == 0){
                $erro = 'O tipo de documento SEI não foi encontrado. Entrar em contato com o administrador do Super Super SEI de seu Órgão.';
                throw new \Exception($erro);

            }

            if(is_null($sei->PegaUrlSei()) == true){
                $this->servico_sei_status = false;
                throw new \Exception('Não existe URL do Super SEI associada a esta UASG! Entre em contato com o Administrador!');
            }
            $andamentos = $sei->consultarProcedimento(null, $num_processo);

            $procedimento_id = $andamentos->IdProcedimento;
            $unidades_origem = array();
            $processos_relacionados_field[$procedimento_id] = 'Processo atual';

            foreach ($andamentos->UnidadesProcedimentoAberto as $anda) {
                $unidades_origem[$anda->Unidade->IdUnidade] = $anda->Unidade->Sigla . ' - ' . $anda->Unidade->Descricao;
            }

            foreach($andamentos->ProcedimentosRelacionados as $processo){
                $processos_relacionados_field[$processo->IdProcedimento] = $processo->ProcedimentoFormatado.'#'.$processo->TipoProcedimento->Nome;
            }
        }catch(\Exception $e){

            $unidades_origem = ['0' => 'A informação não foi carregada'];
            $processos_relacionados_field = ['0' => 'A informação não foi carregada#-'];
            \Alert::warning($e->getMessage())->flash();
        }

        $campos = [

            [ //numero instrumento
                'name' => 'contrato_id',
                'label' => "Número do instrumento",
                'type' => 'select_from_array',
                'options' => $num_instrumento,
                'allows_null' => false,
                'wrapperAttributes' => [
                    'class' => 'form-group col-xs-12 col-md-4'
                ],
                'attributes' => [
                    'readonly' => "true"
                ]
            ],
            //numero processo
            [
                'name' => 'processo',
                'label' => 'Número Processo',
                'type' => 'numprocesso',
                'default' => $num_processo,
                'wrapperAttributes' => [
                    'class' => 'form-group col-xs-12 col-md-4'
                ],
                'attributes' => [
                    'readonly' => "true"
                ]
            ],
            [ //Processos relacionados
                'name' => 'processo_relacionado',
                'label' => "Processos Relacionados",
                'type' => 'select_from_array_processos_relacionados',
                'options' => $processos_relacionados_field,
                'allows_null' => false,
                'wrapperAttributes' => [
                    'class' => 'form-group col-xs-12 col-md-4'
                ]
            ],
            //tipo documento compras
            [
                'name' => 'tipo_documento_compras',
                'label' => 'Tipo Doc. Compras',
                'type' => 'text',
                'default' => $tipo_documento_compras,
                'wrapperAttributes' => [
                    'class' => 'form-group col-xs-12 col-md-5'
                ],
                'attributes' => [
                    'readonly' => "true"
                ]
            ],
            [   // ícone html
                'name' => 'icone',
                'type' => 'custom_html',
                'wrapperAttributes' => [
                    'class' => 'form-group text-center col-xs-12 col-md-2'
                ],
                'value' => '<div style="padding-top:2.5rem"><i class="fa fa-refresh" aria-hidden="true"></i></div>'
            ],
            //tipo documento sei
            [
                'name' => 'tipo_documento_sei',
                'label' => 'Tipos Doc. Super (SEI)',
                'type' => 'select_from_array',
                'options' => $lista_serie,
                'allows_null' => false,
                'wrapperAttributes' => [
                    'class' => 'form-group col-xs-12 col-md-5'
                ]
            ],
            [ //unidade de origem
                'name' => 'unidade_origem',
                'label' => "Unidades de origem",
                'type' => 'select_from_array',
                'options' => $unidades_origem,
                'allows_null' => false,
                'wrapperAttributes' => [
                    'class' => 'form-group col-xs-12 col-md-12'
                ]
            ],
            [   // Hidden
                'name' => 'procedimento_id',
                'type' => 'hidden',
                'value' => $procedimento_id
            ],
            [   // Hidden
                'name' => 'minuta_id',
                'type' => 'hidden',
                'value' => $minuta->first()->id
            ]
        ];

        return $campos;
    }
    /**
     * Show the form for creating inserting a new row.
     *
     * @return Response
     */
    public function create()
    {
        if($this->servico_sei_status == false){
            return redirect('/gescon/contrato/' . $this->contrato_id . '/minutas');
        }
        $this->crud->hasAccessOrFail('create');
        $this->crud->setOperation('create');
        // prepare the fields you need to show
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getCreateFields();
        $this->data['title'] = $this->crud->getTitle() ?? trans('backpack::crud.add') . ' ' . $this->crud->entity_name;
        $this->data['novaRota'] = config('backpack.base.route_prefix') . '/gescon/contrato/' . $this->contrato_id . '/minutas';
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view('vendor.backpack.crud.minutas_envio_sei.create', $this->data);
    }

    public function store(StoreRequest $request)
    {
        $minuta = ContratoMinuta::find($request->minuta_id);
        $status_enviado = Codigoitem::whereHas('codigo', function($q){
            $q->where('descricao', 'Status Contrato Minuta SEI');
        })
        ->where('descricao','Enviado')
        ->pluck('id')
        ->first();
        //recupera documento, procedimento_id
        //envia para o sei
        try{
            DB::beginTransaction();
            $minuta->contrato_minutas_status_id = $status_enviado;
            $minuta->save();
            $sei = new classSEI();
            $procedimento_id = $request->processo_relacionado;
            $id_unidade = $request->unidade_origem;
            $minuta_texto = $minuta->texto_minuta;
            $id_serie = $request->tipo_documento_sei;

            $minuta_texto = preg_replace('~<div class="item-cabecalho"(.*?)</div>~Usi', "", $minuta_texto);

            if(is_null($id_serie)==true){
                throw new \Exception('Não foi encontrado o tipo de Documento informado');
            }
            //envia SEI
            $result = $sei->incluirDocumento($id_unidade, $procedimento_id,'',null,null,null,$minuta_texto, $id_serie);
            $minuta->id_documento = $result->IdDocumento;
            $minuta->link_sei = $result->LinkAcesso;
            $minuta->documento_formatado = $result->DocumentoFormatado;
            $minuta->numero_super_sei = $result->DocumentoFormatado; #248

            $minuta->update();
            // Define tipo de documento para salvar na vinculação do documento e ser salvo na tb Arquivos

            //Verifica quais são os tipos de arquivo de Contrato (codigo_id = 17)
            $codigo_itens = ['Termo de Apostilamento', 'Termo de Rescisão', 'Termo Aditivo', 'Contrato'];

//            $tipo_arquivos = Codigoitem::whereHas('codigo', function($query){
//                $query->where('descricao','=',Codigo::TIPO_ARQUIVOS_CONTRATO);
//                })
//                ->pluck('id')
//                ->toArray();
            $tipo_arquivos = Codigoitem::whereIn('descricao', $codigo_itens)->pluck('id')->toArray();
                //Verifica se o tipo da Minuta do Documento é do Tipo Arquivos Contrato. Se não encontra correspondência, atribiu $tipo = Outros arquivos (141)
                foreach ($tipo_arquivos as $t){
                    if ($this->minuta->first()->codigoitens_id == $t){
                    $tipo = $t;
                    break;
                    } else {
                    $tipo = 141;
                    }
                }

            $documentoAssinado = 0;
            $origem = 2;
            $consultaDocumento = $sei->consultarDocumento($id_unidade, $minuta->documento_formatado);

            if($consultaDocumento->Assinaturas){
                $documentoAssinado = 1;
                $origem = 1;
            }

            // Cria vínculo na tabela contrato_arquivos:
            $vinculosei = new Contratoarquivo();
            $vinculosei->tipo = $tipo; //141=Outros arquivos ou $this->minuta->codigoitens_id
            $vinculosei->contrato_id = $this->contrato_id;
            $vinculosei->descricao = "Número Sei:".$minuta->documento_formatado." <br>Descrição Comprasgovbr: ". $this->minuta->first()->getTipoDeDocumento();
            $vinculosei->link_sei =  $minuta->link_sei;
            $vinculosei->processo = $this->num_processo;
            $vinculosei->arquivos = $minuta->link_sei;
            $vinculosei->origem = $origem;
            $vinculosei->assinaturas_documento = $documentoAssinado; //Assinatura = pendente
            $vinculosei->restrito = true;

//            $arrayDocumentosNaoRestritos = ['Termo Aditivo', 'Termo de Apostilamento', 'Termo de Rescisão', 'Contrato'];
//            if(  in_array($this->minuta->first()->getTipoDeDocumento(), $codigo_itens))
//            {
//                $vinculosei->restrito = false;
//            }
            $vinculosei->save();

            DB::commit();
        }catch(\Exception $e){

            DB::rollback();
            $status_erro = Codigoitem::whereHas('codigo', function ($q) {
                $q->where('descricao', 'Status Contrato Minuta SEI');
            })
            ->where('descricao', 'Erro')
            ->pluck('id')
            ->first();

            $minuta->contrato_minutas_status_id = $status_erro;
            $minuta->save();

            \Alert::warning($e->getMessage())->flash();
            return redirect('/gescon/contrato/' . $minuta->contrato_id . '/minutas');
        }

        \Alert::success('Documento enviado com sucesso.')->flash();

        return redirect('/gescon/contrato/'. $minuta->contrato_id .'/minutas');

    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
