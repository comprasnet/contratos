<?php

namespace App\Http\Controllers\Gescon;

use App\Models\Contrato;
use App\Models\ContratoAntecipagov;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ContratodomicilioRequest as StoreRequest;
use App\Http\Requests\ContratodomicilioRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class ContratodomicilioCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ContratodomicilioCrudController extends CrudController
{
    public function setup()
    {
        $contrato_id = \Route::current()->parameter('contrato_id');
        $contrato = Contrato::where('id', '=', $contrato_id)->where('unidade_id', '=', session()->get('user_ug_id'))->first();
        if (!$contrato) {
            abort('403', config('app.erro_permissao'));
        }
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\SfDomicilioBancario');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/gescon/contrato/' . $contrato_id . '/domiciliobancario');
        $this->crud->setEntityNameStrings('Domicílio Bancário', 'Domicílio Bancário');
       
        $this->crud->addClause('select', ['sfdomiciliobancario.conta', 'sfdomiciliobancario.banco', 'sfdomiciliobancario.agencia', 'sfdomiciliobancario.id']);
        $this->crud->addClause('join', 'sfpredoc', function ($join) {
            $join->on('sfdomiciliobancario.sfpredoc_id', '=', 'sfpredoc.id');
        });
        $this->crud->addClause('join', 'sfdadospgto', function ($join) {
            $join->on('sfpredoc.sfdadospgto_id', '=', 'sfdadospgto.id');
        });
        $this->crud->addClause('join', 'sfpadrao', function ($join) {
            $join->on('sfdadospgto.sfpadrao_id', '=', 'sfpadrao.id');
        });
        $this->crud->addClause('join', 'apropriacoes_faturas_contratofaturas as aprf', function ($join) {
            $join->on('sfpadrao.id', '=', 'aprf.sfpadrao_id');
        });
        $this->crud->addClause('join', 'contratofaturas', function ($join) {
            $join->on('aprf.contratofaturas_id', '=', 'contratofaturas.id');
        });
        $this->crud->addClause('join', 'contratos', function ($join) {
            $join->on('contratofaturas.contrato_id', '=', 'contratos.id');
        });
        $this->crud->addClause('join', 'fornecedores', function ($join) {
            $join->on('contratos.fornecedor_id', '=', 'fornecedores.id');
        });
        $this->crud->addClause('join', 'apropriacoes_faturas as apr', function ($join) {
            $join->on('aprf.apropriacoes_faturas_id', '=', 'apr.id');
        });
        $this->crud->addClause('join', 'codigoitens', function ($join) {
            $join->on('codigoitens.id', '=', 'apr.fase_id');
        });

        

        $this->crud->addClause('where', 'fornecedores.cpf_cnpj_idgener', $contrato->fornecedor->cpf_cnpj_idgener);
        $this->crud->addClause('where', 'sfdomiciliobancario.tipo', 'numDomiBancFavo');
        $this->crud->addClause('where', 'codigoitens.descres', 'APR');

        $this->crud->addClause('whereIn', 'sfdomiciliobancario.id', function ($query) {
            $query->selectRaw('MAX(sfdomiciliobancario.id)')
                  ->from('sfdomiciliobancario')
                  ->groupBy('sfdomiciliobancario.conta', 'sfdomiciliobancario.banco', 'sfdomiciliobancario.agencia');
        });
        
        $this->crud->addClause('orderBy', 'sfdomiciliobancario.id', 'desc');
        
        $this->crud->addButtonFromView('top', 'voltar', 'voltarcontrato', 'end');

        $this->crud->enableExportButtons();
        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');
        $this->crud->allowAccess('show');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        // $this->crud->setFromDb();

        $colunas = $this->Colunas();
        $this->crud->addColumns($colunas);

        // add asterisk for fields that are required in ContratodomicilioRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    // colunas para listagem
    public function Colunas()
    {
        $colunas = [
            [  
                'name' => 'getContrato',
                'label' => 'Número do instrumento', // Table column heading
                'type' => 'model_function',
                'function_name' => 'getContrato', // the method in your Model
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'banco',
                'label' => 'Banco',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'agencia',
                'label' => 'Agência',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'conta',
                'label' => 'Conta',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ]
        ];
        return $colunas;
    }

    /**
     * Adiciona todos os filtros desejados para esta funcionalidade
     *
     * @author Saulo Soares <saulosao@gmail.com>
     */
    public function adicionaFiltros() {}

    public function show($id)
    {
        $content = parent::show($id);

        $this->crud->removeColumns(['sfpredoc_id', 'tipo']);

        return $content;
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
