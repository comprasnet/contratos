<?php

namespace App\Http\Controllers\Gescon;

use App\User;
use App\Models\Contrato;
use App\Models\Contratoconta;
use App\Models\Codigoitem;
use App\Models\Encargo;
use App\Models\Contratoterceirizado;
use App\Models\Retiradacontratoconta;
use App\Models\Movimentacaocontratoconta;
use App\Models\Funcionarioscontratoconta;
use App\Models\Lancamento;
// use App\Models\Movimentacaocontratoconta;
// use App\Models\Lancamento;
use App\Models\Encerramentocontratoconta;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\EncerramentocontratocontaRequest as StoreRequest;
use App\Http\Requests\EncerramentocontratocontaRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

// inserido
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class DepositocontratocontaCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class EncerramentocontratocontaCrudController extends CrudController
{
    public function setup()
    {
        $user_id = backpack_user()->id;

        // como demissão / rescisão é um tipo de liberação, vamos buscar seu código
        $tipo_id_demissao = Encargo::getIdCodigoItemByDescricao('Liberação');

        $contratoconta_id = \Route::current()->parameter('contratoconta_id');
        $contratoConta = Contratoconta::where('id','=',$contratoconta_id)->first();
        if(!$contratoConta){
            abort('403', config('app.erro_permissao'));
        }

        $contrato_id = $contratoConta->contrato_id;
        \Route::current()->setParameter('contrato_id', $contrato_id);

        $saldoContaVinculada = Contratoconta::getSaldoContaVinculadaByIdContrato($contrato_id);

        $contrato = Contrato::where('id','=',$contrato_id)
            ->where('unidade_id','=',session()->get('user_ug_id'))->first();

        if(!$contrato){
            abort('403', config('app.erro_permissao'));
        }

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Encerramentocontratoconta');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/gescon/contrato/contratoconta/' . $contratoconta_id . '/encerramentocontratoconta');
        $this->crud->setEntityNameStrings('encerramento', 'Saldo da Conta Depósito Vinculada');

        $this->crud->addButtonFromView('top', 'voltar', 'voltarcontrato', 'end');
        $this->crud->enableExportButtons();

        // cláusulas para possibilitar buscas
        // $this->crud->addClause('where', 'contratoconta_id', '=', $contratoconta_id);
        // $this->crud->addClause('orderby', 'ano_competencia');
        // $this->crud->addClause('orderby', 'mes_competencia');

        // $this->crud->denyAccess('create');
        // $this->crud->denyAccess('update');
        // $this->crud->denyAccess('delete');
        // $this->crud->denyAccess('show');
        $this->crud->denyAccess('list');
        // $this->crud->allowAccess('emitirTermoDeEncerramento');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        // $this->crud->setFromDb();

        // $colunas = $this->Colunas();
        // $this->crud->addColumns($colunas);

        // $campos = $this->Campos($idTipoMovimentacaoDeposito, $contratoconta_id, $contrato_id, $quantidadeContratosTerceirizados);
        $campos = $this->Campos($contratoconta_id, $contrato_id, $user_id, $tipo_id_demissao, $saldoContaVinculada);
        $this->crud->addFields($campos);

        // add asterisk for fields that are required in DepositocontratocontaRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');



        $uri = $this->crud->request->getRequestUri();
        $verificacaoEmitirTermoDeEncerramento = strpos($uri, 'emitirTermoDeEncerramento');
        if($verificacaoEmitirTermoDeEncerramento > 1){
            // vamos chamar o método que emite o termo de encerramento. Só vai entrar aí se for a url certa pra isso.
            $this->emitirTermoDeEncerramento($contratoconta_id);
        }


    }
    // aqui é onde o termo é mostrado na tela.
    public static function emitirTermoDeEncerramento($contratoconta_id){
        ?>
        <center><h1>Termo de Encerramento de Conta Vinculada</h1`></center>
        <p>
        Certifico o encerramento da gestão da conta vinculada sobre o Contrato Administrativo No. xxxxx, diante do término da vigência contratual
        e da comprovação de quitação das pendências sobre obrigações trabalhistas e previdenciárias de responsabilidade da empresa contratada.
        </p>
        <?php
        exit;
    }
    // public function Campos($idTipoMovimentacaoDeposito, $contratoconta_id, $contrato_id, $quantidadeContratosTerceirizados)
    public function Campos($contratoconta_id, $contrato_id, $user_id, $tipo_id_demissao, $saldoContaVinculada)
    {
        // vamos pegar o mês e ano de agora, que serão nossos mês / ano competência.
        $dataHoje = date('d/m/Y');
        $mesHoje = substr($dataHoje, 3,2);
        $anoHoje = substr($dataHoje,-4);

        $campos = [
            [   // Hidden
                'name' => 'contratoconta_id',
                'type' => 'hidden',
                'default' => $contratoconta_id,
            ],
            [   // Hidden
                'name' => 'saldo_encerramento',
                'type' => 'hidden',
                'default' => $saldoContaVinculada,
            ],
            [   // Hidden
                'name' => 'data_encerramento',
                'type' => 'hidden',
                'default' => $dataHoje,
            ],
            [   // Hidden
                'name' => 'obs_encerramento',
                'type' => 'hidden',
                'default' => null,
            ],
            [   // Hidden
                'name' => 'situacao_retirada',
                'type' => 'hidden',
                'default' => 'Demissão',
            ],
            [   // Hidden
                'name' => 'situacao_movimentacao',
                'type' => 'hidden',
                'default' => 'Movimentação Criada',
            ],
            [   // Hidden
                'name' => 'tipo_id',
                'type' => 'hidden',
                'default' => $tipo_id_demissao,
            ],
            [   // Hidden
                'name' => 'user_id_encerramento',
                'type' => 'hidden',
                'default' => $user_id,
            ],
            [   // Hidden
                'name' => 'user_id',
                'type' => 'hidden',
                'default' => $user_id,
            ],

            [   // Hidden
                'name' => 'mes_competencia',
                'type' => 'hidden',
                'default' => $mesHoje,
            ],

            [   // Hidden
                'name' => 'ano_competencia',
                'type' => 'hidden',
                'default' => $anoHoje,
            ],

            [   // Hidden
                'name' => 'contrato_id',
                'type' => 'hidden',
                'default' => $contrato_id,
            ],
            [   // Hidden
                'name' => 'contratoconta_id',
                'type' => 'hidden',
                'default' => $contratoconta_id,
            ],
            [   // Hidden
                'name' => 'saldo_remanescente_conta_vinculada',
                'type' => 'hidden',
                'default' => $saldoContaVinculada,
            ],
            [ // select_from_array
                'name' => 'saldo_conta_vinculada',
                'label' => "Saldo da Conta-Depósito Vinculada",
                'default' => number_format($saldoContaVinculada,2,',','.'),
                'type' => 'text',
                // 'tab' => 'Rotina Diária',
                // 'wrapperAttributes' => [
                //     'class' => 'form-group col-md-6'
                // ],
                'prefix' => "R$ ",
                'attributes' => [
                    'readonly' => 'readonly',

                ],
            ],

            [ // select_from_array
                'name' => 'is_todos_empregados_rescindidos',
                'label' => "Todos os empregados tiveram contratos de trabalho rescindidos ou foram realocados em outro contrato?",
                'type' => 'radio',
                'options' => [0 => 'Não', 1 => 'Sim'],
                'default' => 0,
                'inline'  => true,
                // 'tab' => 'Rotina Diária',
                // 'wrapperAttributes' => [
                //     'class' => 'form-group col-md-6'
                // ],
            ],
            [ // select_from_array
                'name' => 'is_contratada_cumpriu_todas_obrigacoes_trabalhistas',
                'label' => "A Contratada cumpriu com todas as obrigações trabalhistas junto aos empregados?",
                'type' => 'radio',
                'options' => [0 => 'Não', 1 => 'Sim'],
                'default' => 0,
                'inline'  => true,
                // 'tab' => 'Rotina Diária',
                // 'wrapperAttributes' => [
                //     'class' => 'form-group col-md-6'
                // ],
            ],

        ];
        return $campos;
    }

    // public function Colunas()
    // {
    //     $colunas = [

    //         [
    //             'name'  => 'obs_encerramento',
    //             'label' => 'Observação',
    //             'type'  => 'text',
    //         ],
    //     ];
    //     return $colunas;
    // }
    // public function verificarSeMovimentacaoExiste($request){
    //     $objMovimentacao = new Movimentacaocontratoconta();
    //     if($objMovimentacao->verificarSeMovimentacaoExiste($request)){
    //         return true;
    //     }
    //     return false;
    // }
    // // verificar se no ano/mês de competência, o funcionário já tinha iniciado.
    // public function verificarSeCompetenciaECompativelComDataInicio($request, $objContratoTerceirizado){
    //     $mesCompetencia = $request->input('mes_competencia');
    //     $anoCompetencia = $request->input('ano_competencia');

    //     $dataInicio = $objContratoTerceirizado->data_inicio;
    //     $dataFim = $objContratoTerceirizado->data_fim;

    //     $mesDataInicio = substr($dataInicio, 5, 2);
    //     $anoDataInicio = substr($dataInicio, 0, 4);
    //     $diaDataInicio = substr($dataInicio, 8, 2);
    //     $mesDataFim = substr($dataFim, 5, 2);
    //     $anoDataFim = substr($dataFim, 0, 4);
    //     $diaDataFim = substr($dataFim, 8, 2);
    //     if( $anoCompetencia < $anoDataInicio ){
    //         return false;
    //     }
    //     if( $anoCompetencia == $anoDataInicio  && $mesCompetencia < $mesDataInicio){
    //         return false;
    //     }
    //     return true;
    // }
    // // verificar se o ano/mês de competência é menor do que o ano/mes atual.
    // public function verificarSeCompetenciaECompativelComDataAtual($request, $objContratoTerceirizado){
    //     $mesCompetencia = $request->input('mes_competencia');
    //     $anoCompetencia = $request->input('ano_competencia');

    //     $dataHoje = date("Y-m-d");
    //     $mesHoje = substr($dataHoje, 5, 2);
    //     $anoHoje = substr($dataHoje, 0, 4);
    //     $diaHoje = substr($dataHoje, 8, 2);

    //     if( $anoCompetencia > $anoHoje ){
    //         return false;
    //     }
    //     if( $anoCompetencia == $anoHoje  && $mesCompetencia >= $mesHoje){
    //         return false;
    //     }
    //     return true;
    // }
    // public function alterarStatusMovimentacao($idMovimentacao, $statusMovimentacao){
    //     $objMovimentacao = new Movimentacaocontratoconta();
    //     if($objMovimentacao->alterarStatusMovimentacao($idMovimentacao, $statusMovimentacao)){
    //         return true;
    //     }
    //     return false;
    // }
    // // este método não é o mesmo que tem em Movimentacaocontratoconta
    // public function criarMovimentacao($request){
    //     $objMovimentacaocontratoconta = new Movimentacaocontratoconta();
    //     if($id = $objMovimentacaocontratoconta->criarMovimentacao($request)){
    //         return $id;
    //     }
    //     return false;
    // }
    // public function excluirMovimentacao($idMovimentacao){
    //     $objMovimentacaocontratoconta = new Movimentacaocontratoconta();
    //     if($id = $objMovimentacaocontratoconta->excluirMovimentacao($idMovimentacao)){
    //         return true;
    //     }
    //     return false;
    // }


    public function verificarSeTodosEmpregadosEstaoInativados($request){
        $contrato_id = $request->input('contrato_id');
        // buscar funcionários com status true, o que quer dizer que não está inativado
        $qtd = Contratoterceirizado::where('contrato_id', $contrato_id)->where('situacao', true)->count();
        if($qtd>0){return false;}
        return true;
    }
    public function getTipoMovimentacao($nomeTipoMovimentacao){
        return $objTipoMovimentacaoRetirada = Codigoitem::whereHas('codigo', function ($query) {
            $query->where('descricao', '=', 'Tipo Movimentação');
        })
        ->where('descricao', '=', $nomeTipoMovimentacao)
        ->first();
    }
    public function gerarLancamentoDeRetiradaParaSaldoRemanescente($request){
        $contrato_id = $request->input('contrato_id');
        $dataHoje = date("Y-m-d");
        $anoHoje = substr($dataHoje, 0, 4);
        $mesHoje = substr($dataHoje, 5, 2);
        // $objTipoMovimentacaoRetirada = $this->getTipoMovimentacao('Encerramento da Conta-Depósito Vinculada');
        $objTipoMovimentacaoRetirada = $this->getTipoMovimentacao('Liberação');
        $idTipoMovimentacaoRetirada = $objTipoMovimentacaoRetirada->id;
        $request->request->set('tipo_id', $idTipoMovimentacaoRetirada);
        $request->request->set('mes_competencia', $mesHoje);
        $request->request->set('ano_competencia', $anoHoje);
        $request->request->set('situacao_movimentacao', 'Movimentação Em Andamento');
        // criar a movimentação para gerar o lançamento.
        if( !$idMovimentacao = self::criarMovimentacao($request) ){
            $mensagem = 'Problemas ao criar a movimentação.';
            \Alert::error($mensagem)->flash();
            return redirect()->back();
        }
        // agora que a movimentação foi criada, vamos varrer os contratos terceirizados e gerar os lançamentos de retirada.
        $arrayContratosTerceirizados = Contratoterceirizado::where('contrato_id','=',$contrato_id)->get();
        foreach($arrayContratosTerceirizados as $objContratoTerceirizado){
            $salarioContratoTerceirizado = $objContratoTerceirizado->salario;
            $idContratoTerceirizado = $objContratoTerceirizado->id;
            $saldo = Funcionarioscontratoconta::getSaldoContratoTerceirizadoByIdContratoTerceirizado($idContratoTerceirizado);
            // lançamento para encerramento de conta vinculada
            $objLancamento = new Lancamento();
            $objLancamento->contratoterceirizado_id = $idContratoTerceirizado;
            $objLancamento->encargo_id = null;
            $objLancamento->valor = $saldo;
            $objLancamento->movimentacao_id = $idMovimentacao;
            $objLancamento->salario_atual = $salarioContratoTerceirizado;
            $objLancamento->encargo_nome = 'Encerramento da Conta-Depósito Vinculada';
            $objLancamento->encargo_percentual = 0;
            if( !$objLancamento->save() ){
                $mensagem = 'Erro ao salvar o lançamento para realocado.';
                \Alert::error($mensagem)->flash();
                return false;
            }
        }
        self::alterarStatusMovimentacao($idMovimentacao, 'Movimentação Finalizada');
        return true;
    }
    public function criarMovimentacao($request){
        $objMovimentacaocontratoconta = new Movimentacaocontratoconta();
        if($id = $objMovimentacaocontratoconta->criarMovimentacao($request)){
            return $id;
        }
        return false;
    }
    public function alterarStatusMovimentacao($idMovimentacao, $statusMovimentacao){
        $objMovimentacao = new Movimentacaocontratoconta();
        if($objMovimentacao->alterarStatusMovimentacao($idMovimentacao, $statusMovimentacao)){
            return true;
        }
        return false;
    }

    public function store(StoreRequest $request)
    {
        // encerrar a conta vinculada
        $dataHoje = date("Y-m-d");
        $saldoContaVinculada = $request->input('saldo_remanescente_conta_vinculada');
        $contrato_id = $request->input('contrato_id');
        $contratoconta_id = $request->input('contratoconta_id');
        $user_id = $request->input('user_id');
        $dataHoje = date("Y-m-d");
        $is_todos_empregados_rescindidos = $request->input('is_todos_empregados_rescindidos');
        $is_contratada_cumpriu_todas_obrigacoes_trabalhistas = $request->input('is_contratada_cumpriu_todas_obrigacoes_trabalhistas');

        // buscar dados do usuário que está fazendo o encerramento
        $objUser = User::find($user_id);
        $nomeResponsavelEncerramento = $objUser->name;
        $cpfResponsavelEncerramento = $objUser->cpf;
        $saldoEncerramento = $request->input('saldo_encerramento');

        // verificar se o usuário respondeu afirmativamente às perguntas
        if( !$is_todos_empregados_rescindidos || !$is_contratada_cumpriu_todas_obrigacoes_trabalhistas ){
            $mensagem = 'Para encerrar a Conta-Depósito Vinculada, todos os empregados precisam estar rescindidos e a Contratada precisa ter cumprido com todas as obrigações trabalhistas.';
            \Alert::error($mensagem)->flash();
            $linkLocation = '/gescon/contrato/'.$contrato_id.'/contratocontas';
            return redirect($linkLocation);
        }
        // verificar se todos os empregados estão com status inativo
        if(!$this->verificarSeTodosEmpregadosEstaoInativados($request)){
            // $mensagem = 'Existe(m) empregado(s) cadastrado(s) no contrato que não está(ão) com status Inativo.';
            $mensagem = 'Há empregado(s) com contrato(s) de trabalho ainda ativo(s)!';
            \Alert::error($mensagem)->flash();
            $linkLocation = '/gescon/contrato/'.$contrato_id.'/contratocontas';
            return redirect($linkLocation);
        }


        // precisamos gerar um lançamento de retirada para o saldo remanescente
        if( !$this->gerarLancamentoDeRetiradaParaSaldoRemanescente($request) ){
            $mensagem = 'Problemas ao gerar lançamento de retirada para sado remanescente.';
            \Alert::error($mensagem)->flash();
            $linkLocation = '/gescon/contrato/'.$contrato_id.'/contratocontas';
            return redirect($linkLocation);
        }


        // se chegou aqui é porque a conta pode ser encerrada - salvar informações do encerramento em contrato conta
        $objContratoconta = Contratoconta::where('id', $contratoconta_id)->first();
        $objContratoconta->data_encerramento = $dataHoje;
        $objContratoconta->user_id_encerramento = $user_id;
        $objContratoconta->is_todos_empregados_rescindidos = $is_todos_empregados_rescindidos;
        $objContratoconta->is_contratada_cumpriu_todas_obrigacoes_trabalhistas = $is_contratada_cumpriu_todas_obrigacoes_trabalhistas;
        $objContratoconta->nome_responsavel_encerramento = $nomeResponsavelEncerramento;
        $objContratoconta->cpf_responsavel_encerramento = $cpfResponsavelEncerramento;
        $objContratoconta->saldo_encerramento = $saldoEncerramento;
        $objContratoconta->data_encerramento = $dataHoje;
        if( !$objContratoconta->save() ){
            $mensagem = 'Para encerrar a Conta-Depósito Vinculada, todos os empregados precisam estar rescindidos e a Contratada precisa ter cumprido com todas as obrigações trabalhistas.';
            \Alert::error($mensagem)->flash();
            $linkLocation = '/gescon/contrato/'.$contrato_id.'/contratocontas';
            return redirect($linkLocation);
        }
        // aqui a conta foi encerrada. Vamos mostrar a mensagem e redirecionar o usuário.
        $mensagem = 'Conta-Depósito Vinculada encerrada com sucesso!';
        \Alert::success($mensagem)->flash();
        $linkLocation = '/gescon/contrato/'.$contrato_id.'/contratocontas';
        // $linkLocation = '/gescon/contrato/contratoconta/'.$contratoconta_id.'/movimentacaocontratoconta';
        return redirect($linkLocation);

        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        // return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
