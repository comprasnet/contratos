<?php

namespace App\Http\Controllers\Gescon;

use App\Models\BackpackUser;
use App\Models\Codigo;
use App\Models\Codigoitem;
use App\Models\Instalacao;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;


/**
 * Class ConsultaresponsavelCrudController
 * @package App\Http\Controllers\Gescon
 * @property-read CrudPanel $crud
 */
class ConsultaresponsavelCrudController extends ConsultaContratoBaseCrudController
{
    /**
     * Configurações iniciais do Backpack
     *
     * @throws \Exception
     */
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */

        $this->crud->setModel('App\Models\Contratoresponsavel');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/gescon/consulta/responsaveis');
        $this->crud->setEntityNameStrings('Responsável', 'Responsáveis');
        $this->crud->setHeading('Consulta Responsáveis por Contrato');

        $this->crud->addClause('leftJoin', 'contratos', 'contratos.id', '=', 'contratoresponsaveis.contrato_id');
        $this->crud->addClause('leftJoin', 'fornecedores', 'fornecedores.id', '=', 'contratos.fornecedor_id');
        $this->crud->addClause('leftJoin', 'users', 'users.id', '=', 'user_id');
        $this->crud->addClause('leftJoin', 'instalacoes', 'instalacoes.id', '=', 'instalacao_id');

        $this->crud->addClause('select', [
            'contratos.*',
            'fornecedores.*',
            'users.*',
            // Tabela principal deve ser sempre a última da listagem!
            'contratoresponsaveis.*'
        ]);
        // Apenas ocorrências da unidade atual
        $this->crud->addClause('where', 'contratos.unidade_id', '=', session('user_ug_id'));
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */
        $this->defineConfiguracaoPadrao();
    }

    /**
     * Action para exibição de um único registro
     *
     * @param int $id
     * @return \Backpack\CRUD\app\Http\Controllers\Operations\Response
     */
    public function show($id)
    {
        $content = parent::show($id);

        $this->crud->removeColumns([
            'contrato_id',
            'user_id',
            'funcao_id',
            'instalacao_id',
            'data_inicio',
            'data_fim'
        ]);

        return $content;
    }

    /**
     * Adiciona filtros específicos a serem apresentados
     */
    public function aplicaFiltrosEspecificos()
    {
        $this->aplicaFiltroUsuario();
        $this->aplicaFiltroFuncao();
        $this->aplicaFiltroInstalacao();
        $this->aplicaFiltroPortaria();
        $this->aplicaFiltroCpf();
        $this->aplicaFiltroNome();
    }

    /**
     * Adiciona as colunas específicas a serem exibidas bem como suas definições
     */
    public function adicionaColunasEspecificasNaListagem()
    {
        $this->adicionaColunaUsuario();
        $this->adicionaColunaFuncao();
        $this->adicionaColunaInstalacao();
        $this->adicionaColunaPortaria();
        $this->adicionaColunaDataInicio();
        $this->adicionaColunaDataFim();
        $this->adicionaColunaSituacao();
    }
    /**
     * Adiciona o filtro ao campo Nome do Responsavel
     */
    private function aplicaFiltroNome()
    {
        $campo = [
            'name' => 'name',
            'type' => 'text',
            'label' => 'Nome do Responsável'
        ];
        $this->crud->addFilter(
            $campo,
            null,
            function ($value) {
                $this->crud->addClause('where',
                    'users.name', 'ilike',
                    '%' . $value . '%'
                );
            }
        );
    }
    /**
     * Adiciona o filtro ao campo CPF
     */
    private function aplicaFiltroCpf()
    {
        $campo = [
            'name' => 'cpf',
            'type' => 'text',
            'label' => 'CPF do Responsável'
        ];
        $this->crud->addFilter(
            $campo,
            null,
            function ($value) {
                $this->crud->addClause('where',
                    DB::raw("replace(replace(users.cpf, '.', ''), '-', '')"), 'ilike',
                    '%' . str_replace('.', '', str_replace('-', '', $value)) . '%'
                );
            }
        );
    }
    /**
     * Adiciona o filtro ao campo Usuário
     */
    private function aplicaFiltroUsuario()
    {
        $campo = [
            'name' => 'usuario',
            'type' => 'select2',
            'label' => 'Usuário'
        ];

        $usuarios = $this->retornaUsuariosParaCombo();

        $this->crud->addFilter(
            $campo,
            $usuarios,
            function ($value) {
                $this->crud->addClause('where', 'contratoresponsaveis.user_id', $value);
            }
        );
    }

    /**
     * Adiciona o filtro ao campo Função
     */
    private function aplicaFiltroFuncao()
    {
        $campo = [
            'name' => 'funcao',
            'type' => 'select2',
            'label' => 'Função'
        ];

        $funcoes = $this->retornaFuncoesParaCombo();

        $this->crud->addFilter(
            $campo,
            $funcoes,
            function ($value) {
                $this->crud->addClause('where', 'contratoresponsaveis.funcao_id', $value);
            }
        );
    }

    /**
     * Adiciona o filtro ao campo Instalação
     */
    private function aplicaFiltroInstalacao()
    {
        $campo = [
            'name' => 'instalacao',
            'type' => 'select2',
            'label' => 'Instalação / Unidade'
        ];

        $instalacoes = $this->retornaInstalacaoesParaCombo();

        $this->crud->addFilter(
            $campo,
            $instalacoes,
            function ($value) {
                $this->crud->addClause('where', 'contratoresponsaveis.instalacao_id', $value);
            }
        );
    }

    /**
     * Adiciona o filtro ao campo Portaria
     */
    private function aplicaFiltroPortaria()
    {
        $campo = [
            'name' => 'portaria',
            'type' => 'text',
            'label' => 'Portaria'
        ];

        $this->crud->addFilter(
            $campo,
            null,
            function ($value) {
                $this->crud->addClause('where',
                    'contratoresponsaveis.portaria', 'ilike',
                    '%' . $value . '%'
                );
            }
        );
    }

    /**
     * Adiciona o campo Usuário na listagem>
     */
    private function adicionaColunaUsuario()
    {
        $this->crud->addColumn([
            'name' => 'getUser',
            'label' => 'Responsável',
            'type' => 'model_function',
            'function_name' => 'getUser',
            'priority' => 10,
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
            'searchLogic' => function (Builder $query, $column, $searchTerm) {
                // esse search logic servirá para habilitar a busca no campo Pesquisar
                $query->orWhere('users.cpf', 'ilike', '%' . $searchTerm . '%');
                $query->orWhere('users.name', 'ilike', '%' . strtoupper($searchTerm) . '%');
            },
            // adicionado orderLogic como o join com users ja existe, vamos adicioná-lo e ordenar pelo name
            'orderLogic' => function ($query, $column, $columnDirection) {
                return $query->orderBy('users.name', $columnDirection);
            }
        ]);
    }

    /**
     * Adiciona o campo Função na listagem
     */
    private function adicionaColunaFuncao()
    {
        $this->crud->addColumn([
            'name' => 'getFuncao',
            'label' => 'Função',
            'type' => 'model_function',
            'function_name' => 'getFuncao',
            'priority' => 11,
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
            'orderLogic' => function ($query, $column, $columnDirection) {
                return $query->leftJoin('codigoitens', 'codigoitens.id', '=', 'contratoresponsaveis.funcao_id')
                    ->orderBy('codigoitens.descricao', $columnDirection);
            }
        ]);
    }

    /**
     * Adiciona o campo Instalação na listagem
     */
    private function adicionaColunaInstalacao()
    {
        $this->crud->addColumn([
            'name' => 'getInstalacao',
            'label' => 'Instalação / Unidade',
            'type' => 'model_function',
            'function_name' => 'getInstalacao',
            'priority' => 12,
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
            'searchLogic' => function (Builder $query, $column, $searchTerm) {
                // esse search logic servirá para habilitar a busca no campo Pesquisar
                $query->orWhere('instalacoes.nome', 'ilike', '%' . $searchTerm . '%');
            },
            'orderLogic' => function ($query, $column, $columnDirection) {
                return $query->leftJoin('instalacoes', 'instalacoes.id', '=', 'contratoresponsaveis.instalacao_id')
                    ->orderBy('instalacoes.nome', $columnDirection);
            }
        ]);
    }

    /**
     * Adiciona o campo Portaria na listagem
     */
    private function adicionaColunaPortaria()
    {
        $this->crud->addColumn([
            'name' => 'portaria',
            'label' => 'Portaria',
            'type' => 'string',
            'priority' => 13,
            'orderable' => true,
            'visibleInTable' => false,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true
        ]);
    }

    /**
     * Adiciona o campo Data Início na listagem
     */
    private function adicionaColunaDataInicio()
    {
        $this->crud->addColumn([
            'name' => 'getDataInicio',
            'label' => 'Data Início',
            'type' => 'model_function',
            'function_name' => 'getDataInicio',
            'priority' => 14,
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
            // adicionado orderLogic - como o join já existe, vamos adicionar apenas o order by
            'orderLogic' => function ($query, $column, $columnDirection) {
                return $query->orderBy('contratoresponsaveis.data_inicio', $columnDirection);
            }
        ]);
    }

    /**
     * Adiciona o campo Data Fim na listagem
     */
    private function adicionaColunaDataFim()
    {
        $this->crud->addColumn([
            'name' => 'getDataFim',
            'label' => 'Data Fim',
            'type' => 'model_function',
            'function_name' => 'getDataFim',
            'priority' => 15,
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
            // adicionado orderLogic - como o join já existe, vamos adicionar apenas o order by
            'orderLogic' => function ($query, $column, $columnDirection) {
                return $query->orderBy('contratoresponsaveis.data_fim', $columnDirection);
            }
        ]);
    }

    /**
     * Adiciona o campo Situação na listagem
     */
    private function adicionaColunaSituacao()
    {
        $this->crud->addColumn([
            'name' => 'situacao',
            'label' => 'Situação',
            'type' => 'boolean',
            'options' => [
                0 => 'Inativo',
                1 => 'Ativo'
            ],
            'priority' => 16,
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
            // adicionado orderLogic - como o join já existe, vamos adicionar apenas o order by situacao
            'orderLogic' => function ($query, $column, $columnDirection) {
                return $query->orderBy('contratoresponsaveis.situacao', $columnDirection);
            }
        ]);
    }

    /**
     * Retorna array de Usuários para combo de filtro
     *
     * @return array
     */
    private function retornaUsuariosParaCombo()
    {
        $dados = BackpackUser::select(
            DB::raw("CONCAT(cpf, ' - ', LEFT(name, 80)) as descricao"),
            'id'
        );

        $dados->where('ugprimaria', session()->get('user_ug_id'));
        $dados->where('situacao', true);
        $dados->orWhereHas('unidades', function ($query) {
            $query->where('unidade_id', session()->get('user_ug_id'));
        });
        $dados->orderBy('name');

        return $dados->pluck('descricao', 'id')->toArray();
    }

    /**
     * Retorna array de Funções para combo de filtro
     *
     * @return array
     */
    private function retornaFuncoesParaCombo()
    {
        $dados = Codigoitem::select('descricao', 'id');

        $dados->where('codigo_id', Codigo::FUNÇAO_CONTRATO);
        $dados->orderBy('descricao');

        return $dados->pluck('descricao', 'id')->toArray();
    }

    /**
     * Retorna array de Instalações para combo de filtro
     *
     * @return array
     */
    private function retornaInstalacaoesParaCombo()
    {
        $dados = Instalacao::select('nome as descricao', 'id');

        $dados->orderBy('nome');

        return $dados->pluck('descricao', 'id')->toArray();
    }

}
