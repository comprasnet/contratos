<?php

namespace App\Http\Controllers\Gescon;

use App\Http\Traits\PermissaoTrait;
use App\Models\Contrato;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ContratoempenhoRequest as StoreRequest;
use App\Http\Requests\ContratoempenhoRequest as UpdateRequest;
use App\services\ContratoEmpenhoService;
use Backpack\CRUD\CrudPanel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * Class ContratoempenhoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class MeuContratoempenhoCrudController extends CrudController
{
    use PermissaoTrait;

    protected $contratoEmpenhoService;

    public function __construct(ContratoEmpenhoService $contratoEmpenhoService) {
        parent::__construct();
        $this->contratoEmpenhoService = $contratoEmpenhoService;
    }
    
    public function setup()
    {
        $contrato_id = \Route::current()->parameter('contrato_id');

        $contrato = Contrato::where('id','=',$contrato_id)
        ->whereHas('responsaveis', function ($query) {
            $query->where('user_id', '=', backpack_user()->id);
        })->first();

        if (!$contrato) {
            abort('403', config('app.erro_permissao'));
        }

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Contratoempenho');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/gescon/meus-contratos/' . $contrato_id . '/empenhos');
        //$this->crud->setRoute(config('backpack.base.route_prefix') . '/gescon/meus-contratos/'.$contrato_id. '/empenhos');
        $this->crud->setEntityNameStrings('Empenho do Meu Contrato', 'Empenhos - Meus Contratos');
        $this->crud->addClause('where', 'contratoempenhos.contrato_id', '=', $contrato_id);
        $this->crud->addClause('select', [
            'contratoempenhos.*',
            DB::raw('MAX("contratofatura_empenhos"."contratofatura_id") AS last_contratofatura_id'),
            'faturas.deleted_at',
            DB::raw("tipo_empenhopor.descricao as tipo"),
            'contrato_minuta_empenho_pivot.minuta_empenho_id',
        ]);
        $this->crud->addClause('join', 'contratos', 'contratos.id', '=', 'contratoempenhos.contrato_id');
        $this->crud->addClause('join', 'fornecedores', 'fornecedores.id', '=', 'contratoempenhos.fornecedor_id');
        $this->crud->addClause('join', 'empenhos', 'empenhos.id', '=', 'contratoempenhos.empenho_id');
        $this->crud->addClause('leftjoin', 'unidades', 'unidades.id', '=', 'contratoempenhos.unidadeempenho_id');
        $this->crud->addClause('leftjoin', 'contratofatura_empenhos', 'contratofatura_empenhos.empenho_id', '=', 'empenhos.id');
        $this->crud->addClause('leftjoin', 'contratofaturas as faturas', 'faturas.id', '=', 'contratofatura_empenhos.contratofatura_id');
        $this->crud->addClause('leftjoin', 'contrato_minuta_empenho_pivot', 'contrato_minuta_empenho_pivot.minuta_empenho_id', '=', DB::raw('(select id from minutaempenhos where unidade_id = contratoempenhos.unidadeempenho_id and fornecedor_empenho_id = contratoempenhos.fornecedor_id and mensagem_siafi = (select numero from empenhos where id = contratoempenhos.empenho_id))'));
        $this->crud->addClause('leftjoin', 'minutaempenhos', 'minutaempenhos.id', '=', DB::raw('(select id from minutaempenhos where unidade_id = contratoempenhos.unidadeempenho_id and fornecedor_empenho_id = contratoempenhos.fornecedor_id and mensagem_siafi = (select numero from empenhos where id = contratoempenhos.empenho_id))'));
        $this->crud->addClause('leftjoin', 'codigoitens as tipo_empenhopor', 'tipo_empenhopor.id', '=', 'minutaempenhos.tipo_empenhopor_id');
        $this->crud->groupBy(["contratoempenhos.id", "contratoempenhos.contrato_id", "contratoempenhos.fornecedor_id", "contratoempenhos.empenho_id", "contratoempenhos.unidadeempenho_id","contratoempenhos.automatico","faturas.deleted_at", "contrato_minuta_empenho_pivot.minuta_empenho_id", "tipo_empenhopor.descricao"]);

        $this->crud->addButtonFromView('top', 'voltar', 'voltarmeucontrato', 'end');
        $this->crud->enableExportButtons();
        $this->crud->allowAccess('create');
        $this->crud->allowAccess('show');

        if($this->verificaPermissaoConsulta('meus-contratos')){
            $this->crud->addButtonFromView('line', 'update', 'contratoempenho_update', 'end');
            $this->crud->addButtonFromView('line', 'delete', 'contratoempenho_delete', 'end');
        }else{
            $this->crud->denyAccess('create');
            $this->crud->denyAccess('update');
            $this->crud->denyAccess('delete');
        }


        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        $colunas = $this->contratoEmpenhoService->colunas();
        $this->crud->addColumns($colunas);

        $unidade = [session()->get('user_ug_id') => session()->get('user_ug')];

        $campos = $this->contratoEmpenhoService->campos($contrato_id, $unidade);
        $this->crud->addFields($campos);

        // add asterisk for fields that are required in ContratoempenhoRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        try {
            DB::beginTransaction();

            $save = $this->contratoEmpenhoService->criarEmpenho($request);

            if ($save['error']) {
                return redirect()->back()->withInput();
            }
            $redirect_location = parent::storeCrud($request);
            // your additional operations after save here
            // use $this->data['entry'] or $this->crud->entry

            DB::commit();
            return $redirect_location;
            // your additional operations before save here
        } catch (\Exception $e) {
           DB::rollBack();
           Log::error(__METHOD__ .'Erro ao cadastrar empenho '.$e->getMessage());
           \Alert::error('Erro ao cadastrar empenho.')->flash();
           return redirect()->back()->withInput();
        }
    }

    public function update(UpdateRequest $request)
    {
        $idContratoEmpenho = $request->input('id');

        $request->request->set('user_id', backpack_user()->id);

        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function show($id)
    {
        $content = parent::show($id);

        $this->crud->removeColumn('contrato_id');
        $this->crud->removeColumn('fornecedor_id');
        $this->crud->removeColumn('empenho_id');
        $this->crud->removeColumn('user_id');

        return $content;
    }

    public function edit($id)
    {
        $content = parent::edit($id);

        if (backpack_user()->id != $content->entry->user_id && $content->entry->user_id != null) {
            $this->crud->denyAccess('update');
            return redirect()->route('crud.empenhos.index', $content->entry->contrato_id);
        }
        return $content;
    }
}
