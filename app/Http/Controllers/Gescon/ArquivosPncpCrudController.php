<?php

namespace App\Http\Controllers\Gescon;

use App\Http\Requests\ArquivosPncpRequest as UpdateRequest;
use App\Http\Traits\PermissaoTrait;
use App\Models\Contratoarquivo;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\CrudPanel;

// VALIDATION: change the requests to match your own file names if you need form validation

/**
 * Class ArquivosPncpCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ArquivosPncpCrudController extends CrudController
{
    use PermissaoTrait;
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        [$contratoId, $contrato] = $this->verificarPermissaoContrato();

        $this->crud->setModel(Contratoarquivo::class);
        $this->crud->addClause('where', 'contrato_id', '=', $contratoId);
        $this->crud->addClause('where', 'restrito', '=', false);
        $this->crud->addClause('join', 'contratos', 'contratos.id', '=', 'contrato_arquivos.contrato_id');
        $this->crud->addClause('join', 'unidades', 'unidades.id', '=', 'contratos.unidade_id');
        $this->crud->addClause('select', [
            'contrato_arquivos.*',
            'contratos.numero as numero_contrato',
            'unidades.codigo as codigo_unidade',
        ]);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/gescon/contrato/' . $contratoId . '/arquivos-pncp');

        $this->crud->setEntityNameStrings('Arquivo PNCP Contrato', 'Arquivos PNCP - Contrato');


        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        $this->crud->setFromDb();

        // add asterisk for fields that are required in ArquivosPncpRequest
        $this->crud->denyAccess(['create', 'delete', 'update']);

        $this->crud->removeColumns([
            'contrato_id'
        ]);
        $this->crud->addColumn([ //valor
            'name' => 'description',
            'label' => "Descrição",
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => false, // no point, since it's a large text
            'visibleInModal' => true, // would make the modal too big
            'visibleInExport' => true, // not important enough
            'visibleInShow' => true, // sure, why not
        ]);
        $this->crud->removeColumns((new Contratoarquivo())->getFillable());
        $this->crud->addColumn([ //valor
            'name' => 'contrato_id',
            'label' => "Contrato ID",
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true, // no point, since it's a large text
            'visibleInModal' => true, // would make the modal too big
            'visibleInExport' => true, // not important enough
            'visibleInShow' => true, // sure, why not
        ]);

        $this->crud->addColumn([ //valor
            'name' => 'numero_contrato',
            'label' => "Numero de contrato",
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true, // no point, since it's a large text
            'visibleInModal' => true, // would make the modal too big
            'visibleInExport' => true, // not important enough
            'visibleInShow' => true, // sure, why not
        ]);

        $this->crud->addColumn([ //valor
            'name' => 'codigo_unidade',
            'label' => "Unidade",
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true, // no point, since it's a large text
            'visibleInModal' => true, // would make the modal too big
            'visibleInExport' => true, // not important enough
            'visibleInShow' => true, // sure, why not
        ]);

        $this->crud->addColumn([ //valor
            'name' => 'descricao',
            'label' => "Descrição",
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true, // no point, since it's a large text
            'visibleInModal' => true, // would make the modal too big
            'visibleInExport' => true, // not important enough
            'visibleInShow' => true, // sure, why not
        ]);
        $this->crud->addColumn([ //valor
            'name' => 'retorno_pncp',
            'label' => "Retorno PNCP",
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true, // no point, since it's a large text
            'visibleInModal' => true, // would make the modal too big
            'visibleInExport' => true, // not important enough
            'visibleInShow' => true, // sure, why not
        ]);


        $this->crud->addColumn([ //valor
            'name' => 'link_pncp',
            'label' => "Link PNCP",
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true, // no point, since it's a large text
            'visibleInModal' => true, // would make the modal too big
            'visibleInExport' => true, // not important enough
            'visibleInShow' => true, // sure, why not
        ]);


        $this->crud->addColumn([ //valor
            'name' => 'arquivos',
            'label' => "Tipo de arquivo",
            'type'     => 'closure',
            'function' => function($entry) {
                $arquivos = $entry->arquivos;
                $arquivosParser = explode('.',$arquivos);
                return strtoupper(end($arquivosParser));
            }
        ]);

        $this->crud->addColumn([ //valor
            'name' => 'created_at',
            'label' => "Data de Criação",
            'type' => 'date',
            'orderable' => true,
            'visibleInTable' => true, // no point, since it's a large text
            'visibleInModal' => true, // would make the modal too big
            'visibleInExport' => true, // not important enough
            'visibleInShow' => true, // sure, why not
        ]);

        $campo = [
            'name' => 'retorno_pncp',
            'type' => 'text',
            'label' => 'Retorno PNCP'
        ];

        $this->crud->addFilter(
            $campo,
            false,
            function ($value) {
                $this->crud->addClause('where', 'contrato_arquivos.retorno_pncp', $value);
            }
        );
    }

    public function show($id)
    {

    }

    public function fields()
    {
        return [
            [ //valor
                'name' => 'description',
                'label' => "Descricao",
                'type' => 'text',
                'limit' => 1000,
                'orderable' => true,
                'visibleInTable' => false, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ]
        ];
    }

    public function store(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
