<?php
namespace App\Http\Controllers\Gescon;
use Backpack\CRUD\app\Http\Controllers\CrudController;
// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\RetiradacontratocontaRequest as StoreRequest;
use App\Http\Requests\RetiradacontratocontaRequest as UpdateRequest;
use App\Models\Contrato;
use App\Models\Contratoconta;
use App\Models\Codigoitem;
use App\Models\Contratoterceirizado;
use App\Models\Retiradacontratoconta;
use App\Models\Movimentacaocontratoconta;
use App\Models\Lancamento;
use App\Models\Encargo;
use App\Models\Unidade;
use Backpack\CRUD\CrudPanel;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;
/**
 * Class RetiradacontratocontaCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class RetiradacontratocontaCrudController extends CrudController
{
    public function setup()
    {
        $contratoterceirizado_id = \Route::current()->parameter('contratoterceirizado_id');
        $objContratoTerceirizado = Contratoterceirizado::where('id', '=', $contratoterceirizado_id)->first();
        $idFuncaoContratoTerceirizado = $objContratoTerceirizado->funcao_id;
        $objCodigoitensFuncaoContratoTerceirizado = Codigoitem::where('id', '=', $idFuncaoContratoTerceirizado)->first();
        $nomeFuncaoContratoTerceirizado = $objCodigoitensFuncaoContratoTerceirizado->descricao;
        $contrato_id = $objContratoTerceirizado->contrato_id;
        $objContratoConta = Contratoconta::where('contrato_id','=',$contrato_id)->first();
        $contratoconta_id = $objContratoConta->id;
        \Route::current()->setParameter('contratoconta_id', $contratoconta_id);
        $isContaVinculadaPelaResolucao169Cnj = $objContratoConta->is_conta_vinculada_pela_resolucao169_cnj;

        if($isContaVinculadaPelaResolucao169Cnj){
            // vamos chamar o método que busca os encargos para a conta vinculada pela resolução 169 cnj
            $arrayObjetosEncargo = Contratoconta::getArrayEncargosByIdContratocontaParaResolucao169Cnj($contratoconta_id);
        } else {
            $arrayObjetosEncargo = \DB::table('encargos')
                ->select('encargos.*', 'codigoitens.descricao')
                ->join('codigoitens', 'codigoitens.id', '=', 'encargos.tipo_id')
                ->get();
        }

        // buscar os tipos de movimentação em codigoitens para seleção
        $objTipoMovimentacaoRetirada = Codigoitem::whereHas('codigo', function ($query) {
            $query->where('descricao', '=', 'Tipo Movimentação');
        })
        ->where('descricao', '=', 'Liberação')  //inicialmente era tratada como retirada.
        ->first();
        $idTipoMovimentacaoRetirada = $objTipoMovimentacaoRetirada->id;
        $objRetiradacontratoconta = new Retiradacontratoconta();
        $arrayObjetosEncargoParaCombo = $objRetiradacontratoconta->getEncargosParaCombo();
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Retiradacontratoconta');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/gescon/contrato/contratoconta/contratoterceirizado/'.$contratoterceirizado_id.'/retiradacontratoconta');
        $this->crud->setEntityNameStrings('nova liberação', 'Liberação');
        $this->crud->enableExportButtons();
        $this->crud->denyAccess('list');
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */
        $campos = $this->Campos($objContratoTerceirizado, $arrayObjetosEncargo, $idTipoMovimentacaoRetirada, $nomeFuncaoContratoTerceirizado, $arrayObjetosEncargoParaCombo, $objContratoConta, $isContaVinculadaPelaResolucao169Cnj);
        $this->crud->addFields($campos);
        // add asterisk for fields that are required in RetiradacontratocontaRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function Campos($objContratoTerceirizado, $arrayObjetosEncargo, $idTipoMovimentacaoRetirada, $nomeFuncaoContratoTerceirizado, $arrayObjetosEncargoParaCombo, $objContratoConta, $isContaVinculadaPelaResolucao169Cnj){
        $saldoTotal = 0.00;

        if($isContaVinculadaPelaResolucao169Cnj){
            $campos = [
                [   
                    'name' => 'contratoterceirizado_id',
                    'type' => 'hidden',
                    'default' => $objContratoTerceirizado->id, // tipo da movimentação (dep, ret, rep)
                ],
                [   
                    'name' => 'tipo_id',
                    'type' => 'hidden',
                    'default' => $idTipoMovimentacaoRetirada, // tipo da movimentação (dep, ret, rep)
                ],
                [   
                    'name' => 'nome',
                    'label' => 'Nome do empregado',
                    'type' => 'text',
                    'attributes' => [
                        'readonly' => 'readonly',
                        'style' => 'pointer-events: none;touch-action: none;'
                    ], 
                    'default' => $objContratoTerceirizado->nome,
                ],
                [   
                    'name' => 'funcao',
                    'label' => 'Função do empregado',
                    'type' => 'text',
                    'attributes' => [
                        'readonly' => 'readonly',
                        'style' => 'pointer-events: none;touch-action: none;'
                    ], 
                    'default' => $nomeFuncaoContratoTerceirizado,
                ],
                [   
                    'name' => 'remuneracao',
                    'label' => 'Remuneração do empregado',
                    'type' => 'text',
                    'attributes' => [
                        'readonly' => 'readonly',
                        'style' => 'pointer-events: none;touch-action: none;'
                    ], 
                    'prefix' => "R$",
                    'default' => $objContratoTerceirizado->salario,
                ],
                [   
                    'name' => 'percentual_submodulo22',
                    'label' => 'Grupo A',
                    'type' => 'text',
                    'attributes' => [
                        'readonly' => 'readonly',
                        'style' => 'pointer-events: none;touch-action: none;'
                    ],
                    'default' => $objContratoConta->percentual_submodulo22,
                    'prefix' => "%",
                ],
                [   // Hidden
                    'name' => 'situacao_movimentacao',
                    'type' => 'hidden',
                    'default' => 'Movimentação Criada',
                ],
                [ // select_from_array
                    'name' => 'mes_competencia',
                    'label' => "Mês Liberação",
                    'type' => 'select2_from_array',
                    'options' => config('app.meses_referencia_fatura'), // vai buscar em app.php o array meses_referencia_fatura
                    'allows_null' => false,
                ],
                [ // select_from_array
                    'name' => 'ano_competencia',
                    'label' => "Ano Liberação",
                    'type' => 'select2_from_array',
                    'options' => config('app.anos_referencia_fatura'), // vai buscar em app.php o array anos_referencia_fatura
                    'default' => date('Y'),
                    'allows_null' => false,
                ],
                [ // select_from_array
                    'name' => 'situacao_retirada',
                    'label' => "Situação da Liberação",
                    'type' => 'select2_from_array_hidden_field',    //  tipo criado para possibilitar uso do jquery para esconder campos
                    'options' => $arrayObjetosEncargoParaCombo, // aqui é de onde vai buscar o array
                    'allows_null' => false,
                ],
                [   // Number
                    'name' => 'valor_ferias',
                    'label' => 'Valor para Férias',
                    'type' => 'money_fatura',
                    'attributes' => [
                        'id' => 'valor_ferias',
                    ],
                    'prefix' => "R$",
                ]
            ];


            // vamos gerar com o saldo de férias e abono de férias - para conta vinculada pela resolução 169 cnj
            // vamos mostrar o saldo abaixo do campo relativo
            foreach( $arrayObjetosEncargo as $objEncargo ){
                $nomeEncargo = $objEncargo->descricao;
                if($nomeEncargo == 'Férias' || $nomeEncargo == 'Abono de Férias'){
                    $objContratoConta = new Contratoconta();
                    $saldoEncargoContratoTerceirizado = $objContratoConta->getSaldoContratoContaPorNomeEncargoPorContratoTerceirizadoResolucao169Cnj(
                        $objContratoTerceirizado->id,
                        $nomeEncargo
                    );
                    $campos[] = [   
                        'name' => $nomeEncargo,
                        'label' => 'Saldo '.$nomeEncargo,
                        'type' => 'text',
                        'attributes' => [
                            'readonly' => 'readonly',
                            'style' => 'pointer-events: none;touch-action: none;'
                        ],
                        'prefix' => "R$",
                        'default' => number_format($saldoEncargoContratoTerceirizado, 2, '.', ','),
                    ];
                    // vamos gerando o saldo total para mostrarmos no formulário
                    $saldoTotal = $saldoTotal + $saldoEncargoContratoTerceirizado;
                }
            }

            $campos[] =
            [   // Number
                'name' => 'valor_13',
                'label' => 'Valor para Décimo Terceiro',
                'type' => 'money_fatura',
                'attributes' => [
                    'id' => 'valor_13',
                    'readonly' => 'readonly',
                ],
                'prefix' => "R$",
            ];

            // vamos gerar o campo com o saldo de décimo terceiro - para conta vinculada pela resolução 169 cnj
            // vamos mostrar o saldo abaixo do campo relativo
            foreach( $arrayObjetosEncargo as $objEncargo ){
                $nomeEncargo = $objEncargo->descricao;
                if($nomeEncargo == '13º (décimo terceiro) salário'){
                    $objContratoConta = new Contratoconta();
                    $saldoEncargoContratoTerceirizado = $objContratoConta->getSaldoContratoContaPorNomeEncargoPorContratoTerceirizadoResolucao169Cnj(
                        $objContratoTerceirizado->id, 
                        $nomeEncargo
                    );
                    $campos[] = [  
                        'name' => $nomeEncargo,
                        'label' => 'Saldo '.$nomeEncargo,
                        'type' => 'text',
                        'attributes' => [
                            'readonly' => 'readonly',
                            'style' => 'pointer-events: none;touch-action: none;'
                        ],
                        'prefix' => "R$",
                        'default' => number_format($saldoEncargoContratoTerceirizado, 2, '.', ','),
                    ];
                    // vamos gerando o saldo total para mostrarmos no formulário
                    $saldoTotal = $saldoTotal + $saldoEncargoContratoTerceirizado;
                }
            }

            $campos[] =
            [   // Number
                'name' => 'valor_multa_demissao',
                'label' => 'Valor da multa caso a rescisão não seja por justa causa',
                'type' => 'money_fatura',
                'attributes' => [
                    'id' => 'valor_multa_demissao',
                    'readonly' => 'readonly',
                ],
                'prefix' => "R$",
            ];

            // vamos gerar o campo com o saldo de multa sobre o fgts - para conta vinculada pela resolução 169 cnj
            // vamos mostrar o saldo abaixo do campo relativo
            foreach( $arrayObjetosEncargo as $objEncargo ){
                $nomeEncargo = $objEncargo->descricao;
                if($nomeEncargo == 'Multa sobre o FGTS para as rescisões sem justa causa'){
                    $objContratoConta = new Contratoconta();
                    $saldoEncargoContratoTerceirizado = $objContratoConta->getSaldoContratoContaPorNomeEncargoPorContratoTerceirizadoResolucao169Cnj(
                        $objContratoTerceirizado->id,
                        $nomeEncargo
                    );
                    $campos[] = [   //
                        'name' => $nomeEncargo,
                        'label' => 'Saldo '.$nomeEncargo,
                        'type' => 'text',
                        'attributes' => [
                            'readonly' => 'readonly',
                            'style' => 'pointer-events: none;touch-action: none;'
                        ],
                        'prefix' => "R$",
                        'default' => number_format($saldoEncargoContratoTerceirizado, 2, '.', ','),
                    ];
                    // vamos gerando o saldo total para mostrarmos no formulário
                    $saldoTotal = $saldoTotal + $saldoEncargoContratoTerceirizado;
                }
            }

            $campos[] =
            [
                'name' => 'data_demissao',
                'label' => "Data da rescisão / realocação",
                'type' => 'date',
                'default' => $objContratoTerceirizado->data_fim,
                'allows_null' => false,
                'attributes' => [
                    'id' => 'data_demissao',
                    'readonly' => 'readonly',
                ],
            ];

        } else {
            // Conta Vinculada pelo Caderno
            $campos = [
                [   // Hidden
                    'name' => 'contratoterceirizado_id',
                    'type' => 'hidden',
                    'default' => $objContratoTerceirizado->id, // tipo da movimentação (dep, ret, rep)
                ],
                [   // Hidden
                    'name' => 'tipo_id',
                    'type' => 'hidden',
                    'default' => $idTipoMovimentacaoRetirada, // tipo da movimentação (dep, ret, rep)
                ],
                [   
                    'name' => 'nome',
                    'label' => 'Nome do empregado',
                    'type' => 'text',
                    'attributes' => [
                        'readonly' => 'readonly',
                        'style' => 'pointer-events: none;touch-action: none;'
                    ], // allow decimals
                    'default' => $objContratoTerceirizado->nome,
                ],
                [
                    'name' => 'funcao',
                    'label' => 'Função do empregado',
                    'type' => 'text',
                    'attributes' => [
                        'readonly' => 'readonly',
                        'style' => 'pointer-events: none;touch-action: none;'
                    ], // allow decimals
                    'default' => $nomeFuncaoContratoTerceirizado,
                ],
                [  
                    'name' => 'remuneracao',
                    'label' => 'Remuneração do empregado',
                    'type' => 'text',
                    'attributes' => [
                        'readonly' => 'readonly',
                        'style' => 'pointer-events: none;touch-action: none;'
                    ], // allow decimals
                    'default' => $objContratoTerceirizado->salario,
                ],
                [  
                    'name' => 'fat_empresa',
                    'label' => 'Encargo',
                    'type' => 'text',
                    'attributes' => [
                        'readonly' => 'readonly',
                        'style' => 'pointer-events: none;touch-action: none;'
                    ], // allow decimals
                    'default' => $objContratoConta->fat_empresa,
                    'prefix' => "%",
                ],
                [   
                    'name' => 'percentual_submodulo22',
                    'label' => 'Submódulo 2.2', 
                    'type' => 'text',
                    'attributes' => [
                        'readonly' => 'readonly',
                        'style' => 'pointer-events: none;touch-action: none;'
                    ], // allow decimals
                    'default' => $objContratoConta->percentual_submodulo22,
                    'prefix' => "%",
                ],
                [   // Hidden
                    'name' => 'situacao_movimentacao',
                    'type' => 'hidden',
                    'default' => 'Movimentação Criada',
                ],
                [ // select_from_array
                    'name' => 'mes_competencia',
                    'label' => "Mês Liberação",
                    'type' => 'select2_from_array',
                    'options' => config('app.meses_referencia_fatura'), // vai buscar em app.php o array meses_referencia_fatura
                    'allows_null' => false,
                ],
                [ // select_from_array
                    'name' => 'ano_competencia',
                    'label' => "Ano Liberação",
                    'type' => 'select2_from_array',
                    'options' => config('app.anos_referencia_fatura'), // vai buscar em app.php o array anos_referencia_fatura
                    'default' => date('Y'),
                    'allows_null' => false,
                ],
                [ // select_from_array
                    'name' => 'situacao_retirada',
                    'label' => "Situação da Liberação",
                    'type' => 'select2_from_array_hidden_field',    //  tipo criado para possibilitar uso do jquery para esconder campos
                    'options' => $arrayObjetosEncargoParaCombo, // aqui é de onde vai buscar o array
                    'allows_null' => false,
                ],
                [   // Number
                    'name' => 'valor_ferias',
                    'label' => 'Valor para Férias',
                    'type' => 'money_fatura',
                    'attributes' => [
                        'id' => 'valor_ferias',
                    ],
                    'prefix' => "R$",
                ]
            ];

            foreach( $arrayObjetosEncargo as $objEncargo ){
                $nomeEncargo = $objEncargo->descricao;
                if($nomeEncargo == 'Férias e 1/3 (um terço) constitucional de férias'){
                    $tipoId = $objEncargo->tipo_id;
                    $objContratoConta = new Contratoconta();
                    $saldoEncargoContratoTerceirizado = $objContratoConta->getSaldoContratoContaPorTipoEncargoPorContratoTerceirizado($objContratoTerceirizado->id, $tipoId);
                    $saldoDepositoEncargoContratoTerceirizado = $objContratoConta->getSaldoDepositoPorTipoEncargoPorContratoTerceirizado($objContratoTerceirizado->id, $tipoId);
                    $saldoDepositoEncargoContratoTerceirizado = number_format($saldoDepositoEncargoContratoTerceirizado, 2, '.', ',' );
                    $saldoRetiradaEncargoContratoTerceirizado = $objContratoConta->getSaldoRetiradaPorTipoEncargoPorContratoTerceirizado($objContratoTerceirizado->id, $tipoId);
                    $saldoRetiradaEncargoContratoTerceirizado = number_format($saldoRetiradaEncargoContratoTerceirizado, 2, '.', ',' );
                    $campos[] = [ 
                        'name' => $nomeEncargo,
                        'label' => 'Saldo '.$nomeEncargo,
                        'type' => 'text',
                        'attributes' => [
                            'readonly' => 'readonly',
                            'style' => 'pointer-events: none;touch-action: none;'
                        ],
                        'prefix' => "R$",
                        'default' => number_format($saldoEncargoContratoTerceirizado, 2, '.', ','),
                    ];
                    // vamos gerando o saldo total para mostrarmos no formulário
                    $saldoTotal = $saldoTotal + $saldoEncargoContratoTerceirizado;
                }
            }

            $campos[] =
            [   // Number
                'name' => 'valor_13',
                'label' => 'Valor para Décimo Terceiro',
                'type' => 'money_fatura',
                'attributes' => [
                    'id' => 'valor_13',
                    'readonly' => 'readonly',
                ],
                'prefix' => "R$",
            ];

            foreach( $arrayObjetosEncargo as $objEncargo ){
                $nomeEncargo = $objEncargo->descricao;
                if($nomeEncargo == '13º (décimo terceiro) salário'){
                    $tipoId = $objEncargo->tipo_id;
                    $objContratoConta = new Contratoconta();
                    $saldoEncargoContratoTerceirizado = $objContratoConta->getSaldoContratoContaPorTipoEncargoPorContratoTerceirizado($objContratoTerceirizado->id, $tipoId);
                    $saldoDepositoEncargoContratoTerceirizado = $objContratoConta->getSaldoDepositoPorTipoEncargoPorContratoTerceirizado($objContratoTerceirizado->id, $tipoId);
                    $saldoDepositoEncargoContratoTerceirizado = number_format($saldoDepositoEncargoContratoTerceirizado, 2, '.', ',' );
                    $saldoRetiradaEncargoContratoTerceirizado = $objContratoConta->getSaldoRetiradaPorTipoEncargoPorContratoTerceirizado($objContratoTerceirizado->id, $tipoId);
                    $saldoRetiradaEncargoContratoTerceirizado = number_format($saldoRetiradaEncargoContratoTerceirizado, 2, '.', ',' );
                    $campos[] = [   //
                        'name' => $nomeEncargo,
                        'label' => 'Saldo '.$nomeEncargo,
                        'type' => 'text',
                        'attributes' => [
                            'readonly' => 'readonly',
                            'style' => 'pointer-events: none;touch-action: none;'
                        ],
                        'prefix' => "R$",
                        'default' => number_format($saldoEncargoContratoTerceirizado, 2, '.', ','),
                    ];
                    // vamos gerando o saldo total para mostrarmos no formulário
                    $saldoTotal = $saldoTotal + $saldoEncargoContratoTerceirizado;
                }
            }

            $campos[] =
            [   // Number
                'name' => 'valor_multa_demissao',
                'label' => 'Valor da multa caso a rescisão não seja por justa causa',
                'type' => 'money_fatura',
                'attributes' => [
                    'id' => 'valor_multa_demissao',
                    'readonly' => 'readonly',
                ],
                'prefix' => "R$",
            ];

            foreach( $arrayObjetosEncargo as $objEncargo ){
                $nomeEncargo = $objEncargo->descricao;
                if($nomeEncargo == 'Multa sobre o FGTS para as rescisões sem justa causa'){
                    $tipoId = $objEncargo->tipo_id;
                    $objContratoConta = new Contratoconta();
                    $saldoEncargoContratoTerceirizado = $objContratoConta->getSaldoContratoContaPorTipoEncargoPorContratoTerceirizado($objContratoTerceirizado->id, $tipoId);
                    $saldoDepositoEncargoContratoTerceirizado = $objContratoConta->getSaldoDepositoPorTipoEncargoPorContratoTerceirizado($objContratoTerceirizado->id, $tipoId);
                    $saldoDepositoEncargoContratoTerceirizado = number_format($saldoDepositoEncargoContratoTerceirizado, 2, '.', ',' );
                    $saldoRetiradaEncargoContratoTerceirizado = $objContratoConta->getSaldoRetiradaPorTipoEncargoPorContratoTerceirizado($objContratoTerceirizado->id, $tipoId);
                    $saldoRetiradaEncargoContratoTerceirizado = number_format($saldoRetiradaEncargoContratoTerceirizado, 2, '.', ',' );
                    $campos[] = [ 
                        'name' => $nomeEncargo,
                        'label' => 'Saldo '.$nomeEncargo,
                        'type' => 'text',
                        'attributes' => [
                            'readonly' => 'readonly',
                            'style' => 'pointer-events: none;touch-action: none;'
                        ],
                        'prefix' => "R$",
                        'default' => number_format($saldoEncargoContratoTerceirizado, 2, '.', ','),
                    ];
                    // vamos gerando o saldo total para mostrarmos no formulário
                    $saldoTotal = $saldoTotal + $saldoEncargoContratoTerceirizado;
                }
            }

            $campos[] =
            [
                'name' => 'data_demissao',
                'label' => "Data da rescisão / realocação",
                'type' => 'date',
                'default' => $objContratoTerceirizado->data_fim,
                'allows_null' => false,
                'attributes' => [
                    'id' => 'data_demissao',
                    'readonly' => 'readonly',
                ],
            ];

        }
        return $campos;
    }

    public function getIdContratoByIdContratoTerceirizado($idContratoTerceirizado){
        $obj = \DB::table('contratoterceirizados')
            ->select('contratocontas.contrato_id')
            ->where('contratoterceirizados.id','=',$idContratoTerceirizado)
            ->join('contratos', 'contratos.id', '=', 'contratoterceirizados.contrato_id')
            ->join('contratocontas', 'contratocontas.contrato_id', '=', 'contratos.id')
            ->first();
        $idContrato = $obj->contrato_id;
        return $idContrato;
    }

    public function getIdContratoContaByIdContratoTerceirizado($idContratoTerceirizado){
        $objContratoConta = new Contratoconta();
        return $idContratoConta = $objContratoConta->getIdContratoContaByIdContratoTerceirizado($idContratoTerceirizado);
    }

    public function criarMovimentacao($request){
        $objMovimentacaocontratoconta = new Movimentacaocontratoconta();
        if($id = $objMovimentacaocontratoconta->criarMovimentacao($request)){
            return $id;
        }
        return false;
    }

    public function excluirMovimentacao($idMovimentacao){
        $objMovimentacaocontratoconta = new Movimentacaocontratoconta();
        if($id = $objMovimentacaocontratoconta->excluirMovimentacao($idMovimentacao)){
            return true;
        }
        return false;
    }

    public function alterarStatusMovimentacao($idMovimentacao, $statusMovimentacao){
        $objMovimentacao = new Movimentacaocontratoconta();
        if($objMovimentacao->alterarStatusMovimentacao($idMovimentacao, $statusMovimentacao)){
            return true;
        }
        return false;
    }

    // verificar se no ano/mês de competência, o funcionário já tinha iniciado.
    public function verificarSeCompetenciaECompativelComDataInicio($request, $objContratoTerceirizado){
        $mesCompetencia = $request->input('mes_competencia');
        $anoCompetencia = $request->input('ano_competencia');
        $dataInicio = $objContratoTerceirizado->data_inicio;
        $dataFim = $objContratoTerceirizado->data_fim;
        $mesDataInicio = substr($dataInicio, 5, 2);
        $anoDataInicio = substr($dataInicio, 0, 4);
        $diaDataInicio = substr($dataInicio, 8, 2);
        $mesDataFim = substr($dataFim, 5, 2);
        $anoDataFim = substr($dataFim, 0, 4);
        $diaDataFim = substr($dataFim, 8, 2);
        if( $anoCompetencia < $anoDataInicio ){
            return false;
        }
        if( $anoCompetencia == $anoDataInicio  && $mesCompetencia < $mesDataInicio){
            return false;
        }
        return true;
    }

    /**
     * o percentual do grupo A, não seria mais armazenado nos encargos e
     * sim na tabela contratoconta, pois esse percentual irá variar de conta para conta.
     */
    public function getIdEncargoByNomeEncargo($nomeEncargo){
        // bucar em codigoitens, pela descrição, pegar o id e buscar o tipo id em encargos pelo id
        $obj = \DB::table('codigoitens')
        ->select('encargos.id')
        ->where('codigoitens.descricao','=',$nomeEncargo)
        ->join('encargos', 'encargos.tipo_id', '=', 'codigoitens.id')
        ->first();
        if( !is_object($obj) && $nomeEncargo != 'Férias e Décimo Terceiro' ){
            echo $nomeEncargo.' -> Encargo não localizado.';
            exit;
        } elseif($nomeEncargo == 'Férias e Décimo Terceiro'){
            return 'Férias e Décimo Terceiro';
        }
        return $obj->id;
    }

    public function getTipoIdEncargoByNomeEncargo($nomeEncargo){
        // bucar em codigoitens, pela descrição, pegar o id e buscar o tipo id em encargos pelo id
        return $tipoId = \DB::table('codigoitens')
        ->select('encargos.tipo_id')
        ->where('codigoitens.descricao','=',$nomeEncargo)
        ->join('encargos', 'encargos.tipo_id', '=', 'codigoitens.id')
        ->first()->tipo_id;
    }

    public function verificarSeValorRetiradaEstaDentroDoPermitidoEGerarLancamentos($valorInformadoRetirada13, $valorInformadoRetiradaFerias, $valor_multa_demissao, $objContratoTerceirizado, $request, $idMovimentacao, $situacaoRetirada, $dataDemissao, $isContaVinculadaPelaResolucao169Cnj){
        // vamos buscar o saldo do encargo grupo A sobre 13 salario e férias
        $idContratoTerceirizado = $objContratoTerceirizado->id;
        $idContratoConta = $request->input('contratoconta_id');
        $objContratoConta = Contratoconta::find($idContratoConta);
        $percentualSubmodulo22 = $objContratoConta->percentual_submodulo22;
        $valorTotalIncidenciaSubmodulo22 = 0;
        /**
         * o percentual do grupo A, não seria mais armazenado nos encargos e sim
         * na tabela contrato conta, pois esse percentual irá variar de conta pra conta.
         *
         * Por isso o idEncargoGrupoA será null.
         *
         */
        $idEncargoGrupoA = null;

        /**
         * A busca pelo grupo A, será diferente para conta vinculada baseada na resolução 169 Cnj
         * Se for pelo caderno, em lancamentos os encargo_id será null, mas se for pela resolução, todos os encargo_id serão null, mas,
         * teremos o nome do encargo, que será Incidência....
         */
        if($isContaVinculadaPelaResolucao169Cnj){
            $saldoEncargoGrupoA = $objContratoConta->getSaldoContratoContaGrupoAPorContratoTerceirizadoParaResolucao169Cnj($idContratoTerceirizado);
        } else {
            $saldoEncargoGrupoA = $objContratoConta->getSaldoContratoContaGrupoAPorContratoTerceirizado($idContratoTerceirizado);
        }
        $situacaoFuncionario = $objContratoTerceirizado->situacao;
        // Grupo A - vamos calcular o Grupo A, que é o percentual fat_empresa sobre o valor informado para retirada.
        /**
         * para uma liberação, deverá incidir o percentual do submódulo 2.2
         *
         * Então, o que vou fazer é o seguinte:
         * ao invés de pegar o fat_empresa, que é o valor de 1, 2 ou 3%, vou pegar o percentual submódulo 22
         */
        $fat_empresa = $objContratoConta->percentual_submodulo22;   // Cáculo do grupo A, que agora é o percentual do submódulo 2.2. Antes era o fat_empresa da tab contratocontas
        /**
        * haverá a possibilidade do usuário informar retirada para férias e 13o. ao mesmo tempo.
        * para esse caso, precisaremos somar os valores.
        */
        if($situacaoRetirada=='Férias e Décimo Terceiro'){
            if($isContaVinculadaPelaResolucao169Cnj){
                // para férias
                    /**
                     * A partir de agora, 08/2021, para conta vinculada pela resolução 169 cnj, não teremos mais o fat_empresa (1, 2 ou 3%)
                     */
                    /**
                     * a incidência para resolução será pelo percentual informado (percentual_submodulo22)
                     * e não mais pelo valor calculado para incidência
                     */
                    $fat_empresa = $objContratoConta->percentual_submodulo22;
                    $valorFatEmpresaGrupoAFerias = ( $valorInformadoRetiradaFerias * $fat_empresa ) / 100 ;
                    // vamos atualizar o valor da retirada, somando com o percentual do fat_empresa, que é o grupo A
                    $valorRetiradaFerias = ( $valorInformadoRetiradaFerias + $valorFatEmpresaGrupoAFerias );
                    // para 13
                    /**
                     * A partir de agora, 08/2021, para conta vinculada pela resolução 169 cnj, não teremos mais o fat_empresa (1, 2 ou 3%)
                     */
                    $fat_empresa = $objContratoConta->percentual_submodulo22;
                    $valorFatEmpresaGrupoA13 = ( $valorInformadoRetirada13 * $fat_empresa ) / 100 ;
                    // vamos atualizar o valor da retirada, somando com o percentual do fat_empresa, que é o grupo A
                    $valorRetirada13 = ( $valorInformadoRetirada13 + $valorFatEmpresaGrupoA13 );
            } else {
                // para férias
                    $valorFatEmpresaGrupoAFerias = ( $valorInformadoRetiradaFerias * $fat_empresa ) / 100 ;
                    // vamos atualizar o valor da retirada, somando com o percentual do fat_empresa, que é o grupo A
                    $valorRetiradaFerias = ( $valorInformadoRetiradaFerias + $valorFatEmpresaGrupoAFerias );
                // para 13
                    $valorFatEmpresaGrupoA13 = ( $valorInformadoRetirada13 * $fat_empresa ) / 100 ;
                    // vamos atualizar o valor da retirada, somando com o percentual do fat_empresa, que é o grupo A
                    $valorRetirada13 = ( $valorInformadoRetirada13 + $valorFatEmpresaGrupoA13 );
            }
        } elseif($situacaoRetirada=='Férias'){
            /**
             * para conta vinculada pela resolução 169 cnj, não teremos mais o fat_empresa, que era 1, 2 ou 3%
             */
            if($isContaVinculadaPelaResolucao169Cnj){
                /**
                 * a incidência para resolução será pelo percentual informado (percentual_submodulo22)
                 * e não mais pelo valor calculado para incidência
                 */
                $fat_empresa = $objContratoConta->percentual_submodulo22;
                $valorFatEmpresaGrupoA = ( $valorInformadoRetiradaFerias * $fat_empresa ) / 100 ;
            } else {
                $valorFatEmpresaGrupoA = ( $valorInformadoRetiradaFerias * $fat_empresa ) / 100 ;
            }
            // vamos atualizar o valor da retirada, somando com o percentual do fat_empresa, que é o grupo A
            $valorRetirada = ( $valorInformadoRetiradaFerias + $valorFatEmpresaGrupoA );
        } elseif($situacaoRetirada=='Décimo Terceiro'){
            /**
             * para conta vinculada pela resolução 169 cnj, não teremos mais o fat_empresa (1, 2 ou 3%)
             */
            if($isContaVinculadaPelaResolucao169Cnj){
                /**
                 * a incidência para resolução será pelo percentual informado (percentual_submodulo22)
                 * e não mais pelo valor calculado para incidência
                 */
                $fat_empresa = $objContratoConta->percentual_submodulo22;
                $valorFatEmpresaGrupoA = ( $valorInformadoRetirada13 * $fat_empresa ) / 100 ;
            } else {
                $valorFatEmpresaGrupoA = ( $valorInformadoRetirada13 * $fat_empresa ) / 100 ;
            }
            // vamos atualizar o valor da retirada, somando com o percentual do fat_empresa, que é o grupo A
            $valorRetirada = ( $valorInformadoRetirada13 + $valorFatEmpresaGrupoA );
        }
        // vamos verificar quanto o funcionário tem de saldo para o encargo informado.
        $salario = $objContratoTerceirizado->salario;
        $umTercoSalario = ( $salario / 3 );


        if($situacaoRetirada=='Realocado'){
            /**
             * aqui o usuário informou que a retirada é para realocação
             * Funcionará como se fosse uma demissão, sendo que não haverá valores envolvidos.
             *
             * Deverá ser criada uma movimentação de liberação
             *
             */
            // verificar se o funcionário já não é demitido
            if( !$situacaoFuncionario ){
                $mensagem = 'Este empregado já está demitido.';
                \Alert::error($mensagem)->flash();
                return false;
            }
            // verificar se informou a data de demissão / rescisão / realocação
            $dataDemissao = $request->input('data_demissao');
            if( $dataDemissao=='' ){
                $mensagem = 'Favor informar a data da rescisão / realocação.';
                \Alert::error($mensagem)->flash();
                return false;
            }
            // vamos chamar o método que altera a situação do funcionário para demitido.
            if( !$objContratoConta->alterarSituacaoFuncionárioParaDemitido($idContratoTerceirizado, $dataDemissao) ){
                $mensagem = 'Erro ao alterar a situação do funcioário para demitido.';
                \Alert::error($mensagem)->flash();
                return false;
            }
            // vamos chamar o método que altera a situação do funcionário para demitido.
            if( !$objContratoConta->alterarSituacaoFuncionárioParaRealocado($idContratoTerceirizado) ){
                $mensagem = 'Erro ao alterar a situação do funcioário para demitido.';
                \Alert::error($mensagem)->flash();
                return false;
            }
            // lançamento para realocação - valores zerados
            $objLancamento = new Lancamento();
            $objLancamento->contratoterceirizado_id = $idContratoTerceirizado;
            $objLancamento->encargo_id = null;
            $objLancamento->valor = 0.00;
            $objLancamento->movimentacao_id = $idMovimentacao;
            $objLancamento->salario_atual = $salario;
            $objLancamento->encargo_nome = 'Realocado';
            $objLancamento->encargo_percentual = 0;
            if( !$objLancamento->save() ){
                $mensagem = 'Erro ao salvar o lançamento para realocado.';
                \Alert::error($mensagem)->flash();
                return false;
            }
        } elseif($situacaoRetirada=='Demissão'){
            /**
             * a demissão / rescisão funciona da seguinte forma:
             *
             * 1 - usuário informa o valor da liberação;
             * 2 - sistema usa esse valor pra fazer liberações em cima das provisões existentes, seguindo a seguinte ordem:
             *      - 13o.
             *      - incidência módulo 2.2
             *      - férias e 1/3
             *      - multa sobre FGTS por último
             *
             */
            // aqui o usuário informou que a retirada é para demissão / rescisão
            // verificar se o funcionário já não é demitido
            if( !$situacaoFuncionario ){
                $mensagem = 'Este empregado já está demitido.';
                \Alert::error($mensagem)->flash();
                return false;
            }
            // verificar se informou a data de demissão / rescisão
            $dataDemissao = $request->input('data_demissao');
            if( $dataDemissao=='' ){
                $mensagem = 'Favor informar a data da rescisão / realocação.';
                \Alert::error($mensagem)->flash();
                return false;
            }
            // vamos verificar se o usuário informou pelo menos um dos valores para demissão / rescisão.
            // caso ele tenha informado todos os valores iguais a zero, vamos notificá-lo, mas não vamos interromper o processo.
            if($valorInformadoRetirada13 == 0 && $valorInformadoRetiradaFerias == 0 && $valor_multa_demissao == 0){
                $mensagem = 'Você informou todos os valores referentes à rescisão, iguais a zero.';
                \Alert::error($mensagem)->flash();
            }
            /**
             * Com a nova regra de demissão / rescisão, precisaremos calcular a incidência do submódulo 2.2 sobre 13o. e férias
             */
            // buscar os saldos dos encargos e gerar um lançamento de retirada pra cada.
            // primeiramente para 13o.
            $nomeEncargo13ParaDemissao = '13º (décimo terceiro) salário';
                $idEncargo13ParaDemissao = self::getIdEncargoByNomeEncargo($nomeEncargo13ParaDemissao);
                /**
                 * a conta vinculada pode ser pelo caderno (forma normal) ou pela resolução 169 cnj,
                 * onde os percentuais dos encargos estarão diretamente na tabela contratocontas, pois se tornaram variáveis.
                 */
                if($isContaVinculadaPelaResolucao169Cnj){
                    $saldoDecimoTerceiroParaDemissao = $objContratoConta->getSaldoContratoContaPorNomeEncargoPorContratoTerceirizadoResolucao169Cnj(
                        $idContratoTerceirizado, 
                        $nomeEncargo13ParaDemissao
                    );
                } else {
                    $saldoDecimoTerceiroParaDemissao = $objContratoConta->getSaldoContratoContaPorIdEncargoPorContratoTerceirizado(
                        $idContratoTerceirizado, 
                        $idEncargo13ParaDemissao
                    );
                }
                // vamos verificar se o saldo é maior ou igual ao valor informado
                if($saldoDecimoTerceiroParaDemissao < $valorInformadoRetirada13){
                    $mensagem = 'O saldo para 13o. é menor do que o valor informado.';
                    \Alert::error($mensagem)->flash();
                    return false;
                } else {
                    // com a nova regra de demissão / rescisão, o valor que deverá ser lançado, não é mais o valor do saldo e sim o valor informado.
                    $saldoDecimoTerceiroParaDemissao = $valorInformadoRetirada13;
                    // aqui vamos calcular a incidência do submódulo 2.2
                    if($isContaVinculadaPelaResolucao169Cnj){
                        /**
                         * conta vinculada pela resolução 169 cnj, não teremos mais o fat_empresa (1, 2 ou 3%)
                         * a incidência para resolução será pelo percentual informado (percentual_submodulo22)
                         * e não mais pelo valor calculado para incidência
                         */
                        $fat_empresa = $objContratoConta->percentual_submodulo22;
                        $valorIncidenciaSubmodulo22_13 = ( $valorInformadoRetirada13 * $fat_empresa ) / 100 ;
                    } else {
                        $valorIncidenciaSubmodulo22_13 = ( ($valorInformadoRetirada13 * $percentualSubmodulo22)/100 );
                    }
                }

            // agora para férias
            $nomeEncargoFeriasParaDemissao = 'Férias e 1/3 (um terço) constitucional de férias';
                $idEncargoFeriasParaDemissao = self::getIdEncargoByNomeEncargo($nomeEncargoFeriasParaDemissao);
                /**
                 *a conta vinculada pode ser pelo caderno (forma normal) ou pela resolução 169 cnj,
                 * onde os percentuais dos encargos estarão diretamente na tabela contratocontas, pois se tornaram variáveis.
                 */
                if($isContaVinculadaPelaResolucao169Cnj){
                    // vamos verificar quanto tem de saldo para férias + abono de férias, pois pela resolução 169, o saldo de férias será coposto pelos dois.
                    $saldoFerias = $objContratoConta->getSaldoContratoContaPorNomeEncargoPorContratoTerceirizadoResolucao169Cnj($idContratoTerceirizado, 'Férias');
                    $saldoAbonoFerias = $objContratoConta->getSaldoContratoContaPorNomeEncargoPorContratoTerceirizadoResolucao169Cnj($idContratoTerceirizado, 'Abono de Férias');
                    $saldoFeriasParaDemissao = ($saldoFerias + $saldoAbonoFerias);
                } else {
                    $saldoFeriasParaDemissao = $objContratoConta->getSaldoContratoContaPorIdEncargoPorContratoTerceirizado($idContratoTerceirizado, $idEncargoFeriasParaDemissao);
                }
                // vamos verificar se o saldo para férias é maior ou igual ao valor informado
                if($saldoFeriasParaDemissao < $valorInformadoRetiradaFerias){
                    $mensagem = 'O saldo para férias é menor do que o valor informado.';
                    \Alert::error($mensagem)->flash();
                    return false;
                } else {
                    // com a nova regra de demissão / rescisão, o valor que deverá ser lançado, não é mais o valor do saldo e sim o valor informado.
                    $saldoFeriasParaDemissao = $valorInformadoRetiradaFerias;
                    // aqui vamos calcular a incidência do submódulo 2.2
                    if($isContaVinculadaPelaResolucao169Cnj){
                        /**
                         * conta vinculada pela resolução 169 cnj, não teremos mais o fat_empresa (1, 2 ou 3%)
                         * a incidência para resolução será pelo percentual informado (percentual_submodulo22)
                         * e não mais pelo valor calculado para incidência
                         */
                        $fat_empresa = $objContratoConta->percentual_submodulo22;
                        $valorIncidenciaSubmodulo22_ferias = ( $valorInformadoRetiradaFerias * $fat_empresa ) / 100 ;
                    } else {
                        $valorIncidenciaSubmodulo22_ferias = ( ($valorInformadoRetiradaFerias * $percentualSubmodulo22)/100 );
                    }
                }
            if($valor_multa_demissao > 0){
                // aqui quer dizer que a demissão / rescisão não é por justa causa, pois tem valor de multa.
                $nomeEncargoRescisaoParaDemissao = 'Multa sobre o FGTS para as rescisões sem justa causa';
                $idEncargoRescisaoParaDemissao = self::getIdEncargoByNomeEncargo($nomeEncargoRescisaoParaDemissao);
                if($isContaVinculadaPelaResolucao169Cnj){
                    $saldoRescisaoParaDemissao = $objContratoConta->getSaldoContratoContaPorNomeEncargoPorContratoTerceirizadoResolucao169Cnj(
                        $idContratoTerceirizado, 
                        'Multa sobre o FGTS para as rescisões sem justa causa'
                    );
                } else {
                    $saldoRescisaoParaDemissao = $objContratoConta->getSaldoContratoContaPorIdEncargoPorContratoTerceirizado(
                        $idContratoTerceirizado, 
                        $idEncargoRescisaoParaDemissao
                    );
                }
                // vamos verificar se o saldo é maior ou igual ao valor informado
                if($saldoRescisaoParaDemissao < $valor_multa_demissao){
                    $mensagem = 'O saldo para multa é menor do que o valor informado.';
                    \Alert::error($mensagem)->flash();
                    return false;
                } else {
                    // com a nova regra de demissão / rescisão, o valor que deverá ser lançado, não é mais o valor do saldo e sim o valor informado.
                    $saldoRescisaoParaDemissao = $valor_multa_demissao;
                }
            } else {
                // aqui quer dizer que não tem multa, pois é justa causa
                $saldoRescisaoParaDemissao = 0;
            }
            // aqui já temos todos os valores de incidência do submódulo 2.2. Vamos somá-los
            $valorTotalIncidenciaSubmodulo22 = ($valorIncidenciaSubmodulo22_13 + $valorIncidenciaSubmodulo22_ferias );
            /**
             * o percentual do grupo A, não seria mais armazenado nos encargos e sim
             * na tabela contrato conta, pois esse percentual irá variar de conta pra conta.
             *
             * Por isso o idEncargoGrupoAParaDemissao será null.
             */
            $idEncargoGrupoAParaDemissao = null;
            /**
             * a conta vinculada pode ser pelo caderno (forma normal) ou pela resolução 169 cnj,
             * onde os percentuais dos encargos estarão diretamente na tabela contratocontas, pois se tornaram variáveis.
             */
            if($isContaVinculadaPelaResolucao169Cnj){
                $saldoGrupoAParaDemissao = $objContratoConta->getSaldoContratoContaGrupoAPorContratoTerceirizadoParaResolucao169Cnj($idContratoTerceirizado);
            } else {
                $saldoGrupoAParaDemissao = $objContratoConta->getSaldoContratoContaGrupoAPorContratoTerceirizado($idContratoTerceirizado);
            }
            if($saldoGrupoAParaDemissao < $valorTotalIncidenciaSubmodulo22){
                $mensagem = 'O saldo para o submódulo 2.2 ('.$saldoGrupoAParaDemissao.') é menor do que o valor calculado ('.$valorTotalIncidenciaSubmodulo22.').';
                \Alert::error($mensagem)->flash();
                return false;
            } else {
                // com a nova regra de demissão / rescisão, o valor a ser lançado não é mais o saldo e sim o valor calculado em cima dos valores informados para férias e 13.
                $saldoGrupoAParaDemissao = $valorTotalIncidenciaSubmodulo22;
            }
            /**
             *a conta vinculada pode ser pelo caderno (forma normal) ou pela resolução 169 cnj,
             * onde os percentuais dos encargos estarão diretamente na tabela contratocontas, pois se tornaram variáveis.
             */
            if($isContaVinculadaPelaResolucao169Cnj){
                // CONTA VINCULADA PELA RESOLUÇÃO 169 CNJ

                if($saldoDecimoTerceiroParaDemissao>0){
                    // lançamento para o 13o.
                    $objLancamento = new Lancamento();
                    $objLancamento->contratoterceirizado_id = $idContratoTerceirizado;
                    $objLancamento->encargo_id = null;
                    $objLancamento->valor = $saldoDecimoTerceiroParaDemissao;
                    $objLancamento->movimentacao_id = $idMovimentacao;
                    $objLancamento->salario_atual = $salario; 
                    $objLancamento->encargo_nome = '13º (décimo terceiro) salário';
                    $objLancamento->encargo_percentual = trim($objContratoConta->percentual_13);
                    if( !$objLancamento->save() ){
                        $mensagem = 'Erro ao salvar o lançamento para Décimo Terceiro.';
                        \Alert::error($mensagem)->flash();
                        return false;
                    }
                }
                if($saldoGrupoAParaDemissao>0){
                    // lançamento para grupo A sobre 13 e férias
                    $objLancamento = new Lancamento();
                    $objLancamento->contratoterceirizado_id = $idContratoTerceirizado;
                    $objLancamento->encargo_id = null;
                    $objLancamento->valor = $saldoGrupoAParaDemissao;
                    $objLancamento->movimentacao_id = $idMovimentacao;
                    $objLancamento->salario_atual = $salario;  
                    $objLancamento->encargo_nome = 'Incidência do Grupo A sobre férias, abono e 13o. salário';
                    $objLancamento->encargo_percentual = $objContratoConta->percentual_submodulo22;  //o percentual será o informado e não o calculado.
                    if( !$objLancamento->save() ){
                        $mensagem = 'Erro ao salvar o lançamento para grupo A sobre Décimo Terceiro e Férias.';
                        \Alert::error($mensagem)->flash();
                        return false;
                    }
                }
                if($saldoFeriasParaDemissao>0){
                    // lançamento para férias
                    $objLancamento = new Lancamento();
                    $objLancamento->contratoterceirizado_id = $idContratoTerceirizado;
                    $objLancamento->encargo_id = null;
                    $objLancamento->valor = $saldoFeriasParaDemissao;
                    $objLancamento->movimentacao_id = $idMovimentacao;
                    $objLancamento->salario_atual = $salario;
                    $objLancamento->encargo_nome = 'Férias';
                    $objLancamento->encargo_percentual = trim($objContratoConta->percentual_ferias);
                    if( !$objLancamento->save() ){
                        $mensagem = 'Erro ao salvar o lançamento para férias.';
                        \Alert::error($mensagem)->flash();
                        return false;
                    }
                }
                if($saldoRescisaoParaDemissao>0){
                    // lançamento para rescisão e adicional fgts
                    $objLancamento = new Lancamento();
                    $objLancamento->contratoterceirizado_id = $idContratoTerceirizado;
                    $objLancamento->encargo_id = null;
                    $objLancamento->valor = $saldoRescisaoParaDemissao;
                    $objLancamento->movimentacao_id = $idMovimentacao;
                    $objLancamento->salario_atual = $salario;
                    $objLancamento->encargo_nome = 'Multa sobre o FGTS para as rescisões sem justa causa';
                    $objLancamento->encargo_percentual = trim($objContratoConta->percentual_multa_sobre_fgts);
                    if( !$objLancamento->save() ){
                        $mensagem = 'Erro ao salvar o lançamento para rescisão e adicional fgts.';
                        \Alert::error($mensagem)->flash();
                        return false;
                    }
                }
            } else {
                // CONTA VINCULADA PELO CADERNO
                if($saldoDecimoTerceiroParaDemissao>0){
                    // lançamento para o 13o.
                    $objLancamento = new Lancamento();
                    $objLancamento->contratoterceirizado_id = $idContratoTerceirizado;
                    $objLancamento->encargo_id = $idEncargo13ParaDemissao;
                    $objLancamento->valor = $saldoDecimoTerceiroParaDemissao;
                    $objLancamento->movimentacao_id = $idMovimentacao;
                    $objLancamento->salario_atual = $salario;
                    if( !$objLancamento->save() ){
                        $mensagem = 'Erro ao salvar o lançamento para Décimo Terceiro.';
                        \Alert::error($mensagem)->flash();
                        return false;
                    }
                }
                if($saldoGrupoAParaDemissao>0){
                    // lançamento para grupo A sobre 13 e férias
                    $objLancamento = new Lancamento();
                    $objLancamento->contratoterceirizado_id = $idContratoTerceirizado;
                    $objLancamento->encargo_id = $idEncargoGrupoAParaDemissao;
                    $objLancamento->valor = $saldoGrupoAParaDemissao;
                    $objLancamento->movimentacao_id = $idMovimentacao;
                    $objLancamento->salario_atual = $salario;
                    if( !$objLancamento->save() ){
                        $mensagem = 'Erro ao salvar o lançamento para grupo A sobre Décimo Terceiro e Férias.';
                        \Alert::error($mensagem)->flash();
                        return false;
                    }
                }
                if($saldoFeriasParaDemissao>0){
                    // lançamento para férias
                    $objLancamento = new Lancamento();
                    $objLancamento->contratoterceirizado_id = $idContratoTerceirizado;
                    $objLancamento->encargo_id = $idEncargoFeriasParaDemissao;
                    $objLancamento->valor = $saldoFeriasParaDemissao;
                    $objLancamento->movimentacao_id = $idMovimentacao;
                    $objLancamento->salario_atual = $salario;
                    if( !$objLancamento->save() ){
                        $mensagem = 'Erro ao salvar o lançamento para férias.';
                        \Alert::error($mensagem)->flash();
                        return false;
                    }
                }
                if($saldoRescisaoParaDemissao>0){
                    // lançamento para rescisão e adicional fgts
                    $objLancamento = new Lancamento();
                    $objLancamento->contratoterceirizado_id = $idContratoTerceirizado;
                    $objLancamento->encargo_id = $idEncargoRescisaoParaDemissao;
                    $objLancamento->valor = $saldoRescisaoParaDemissao;
                    $objLancamento->movimentacao_id = $idMovimentacao;
                    $objLancamento->salario_atual = $salario;
                    if( !$objLancamento->save() ){
                        $mensagem = 'Erro ao salvar o lançamento para rescisão e adicional fgts.';
                        \Alert::error($mensagem)->flash();
                        return false;
                    }
                }
            }
            // com as alterações, o valor da retirada passou a ser a soma dos saldos que foram usados para gerar os lançamentos.
            $valorRetirada = ( $saldoDecimoTerceiroParaDemissao + $saldoGrupoAParaDemissao + $saldoFeriasParaDemissao + $saldoRescisaoParaDemissao );
            // vamos chamar o método que altera a situação do funcionário para demitido.
            if( !$objContratoConta->alterarSituacaoFuncionárioParaDemitido($idContratoTerceirizado, $dataDemissao) ){
                $mensagem = 'Erro ao alterar a situação do funcioário para demitido.';
                \Alert::error($mensagem)->flash();
                return false;
            }
        } else {
            // aqui o usuário informou que a retirada não é para demissão / rescisão
            /****
            * existirá a possibilidade do usuário selecionar como situação da retirada, férias e 13o. ao mesmo tempo.
            * Vamos tratar -> para esse caso, o saldo e demais valores serão a soma dos saldos e valores para 13 e férias.
            */
            if($situacaoRetirada == 'Férias e Décimo Terceiro'){
                /**
                * vamos calcular o saldo e fazer os lançamentos para 13
                */
                    $situacaoRetirada = 'Décimo Terceiro';
                    // aqui o usuário informou que a retirada não é para demissão / rescisão
                    $nomeEncargoInformado = self::getNomeEncargoBySituacaoRetirada($situacaoRetirada);
                    $idEncargoInformado = self::getIdEncargoByNomeEncargo($nomeEncargoInformado);
                    $tipoIdEncargo = $objContratoConta->getTipoIdEncargoByIdEncargo($idEncargoInformado);
                    // vamos verificar quanto tem de saldo para o encargo em questão.
                    /**
                     * verificar se a conta vinculada é baseada no caderno ou na resolução 169 cnj.
                     * Para esse caso (resolução) também não teremos o tipoIdEncargo, pois os dados estarão diretamente na tabela contratocontas
                     * Buscaremos pelo nome do encargo.
                     */
                    if($isContaVinculadaPelaResolucao169Cnj){
                        // vamos verificar quanto tem de saldo para férias + abono de férias, pois pela resolução 169, o saldo de férias será coposto pelos dois.
                        $saldoContratoContaPorTipoEncargo13 = $objContratoConta->getSaldoContratoContaPorNomeEncargoPorContratoTerceirizadoResolucao169Cnj($idContratoTerceirizado, '13º (décimo terceiro) salário');
                    } else {
                        // vamos verificar quanto tem de saldo para o encargo em questão.
                        $saldoContratoContaPorTipoEncargo13 = $objContratoConta->getSaldoContratoContaPorTipoEncargoPorContratoTerceirizado($idContratoTerceirizado, $tipoIdEncargo);
                    }

                    // início das verificações por encargo
                    $valorMaximoRetirada13 = 0; // inicializar o valor máximo para retirada, que será alterado de acordo com o encargo informado.
                    if( $nomeEncargoInformado == '13º (décimo terceiro) salário' ){
                        // para 13o. salário, retirada máxima = ( salário + grupo A )
                        $valorMaximoRetirada13 = ( $salario + $valorFatEmpresaGrupoA13 );
                    }

                    // fim das verificações por encargo
                    // vamos verificar se o valor do fat empresa (grupo A) não é maior do que o saldo do grupo A (encargo)
                    if($valorFatEmpresaGrupoA13 > $saldoEncargoGrupoA){
                        \Alert::error('O valor calculado para o Grupo A é maior do que o saldo do encargo Grupo A.')->flash();
                        return false;
                    }
                    // vamos verificar se o valor informado não é maior que o saldo para o encargo informado
                    if( $valorInformadoRetirada13 > $saldoContratoContaPorTipoEncargo13 ){
                        \Alert::error('O valor informado é maior do que o saldo do encargo.')->flash();
                        return false;
                    }
                    // agora que já calculamos o valor máximo para retirada, pelo encargo informado, vamos verificar se o valor informado é possível.
                    if( $valorMaximoRetirada13 < $valorRetirada13 ){
                        \Alert::error('O valor da retirada supera o valor máximo permitido.')->flash();
                        return false;
                    }

                    if($isContaVinculadaPelaResolucao169Cnj){
                        // gerar o lançamento para o encargo informado
                        $objLancamento = new Lancamento();
                        $objLancamento->contratoterceirizado_id = $idContratoTerceirizado;
                        $objLancamento->encargo_id = null;
                        $objLancamento->valor = $valorInformadoRetirada13;
                        $objLancamento->movimentacao_id = $idMovimentacao;
                        $objLancamento->salario_atual = $salario;
                        $objLancamento->encargo_nome = '13º (décimo terceiro) salário';
                        $objLancamento->encargo_percentual = $objContratoConta->percentual_13;
                        if( !$objLancamento->save() ){
                            $mensagem = 'Erro ao salvar o lançamento.';
                            \Alert::error($mensagem)->flash();
                            return false;
                        }
                        // Aqui vamos controlar os demais lançamentos, além do encargo selecionado pelo usuário
                        if( $nomeEncargoInformado == '13º (décimo terceiro) salário' ){
                            // GRUPO A
                            // para 13 é necessário gerar lançamento para o Grupo A
                            $objLancamento = new Lancamento();
                            $objLancamento->contratoterceirizado_id = $idContratoTerceirizado;
                            $objLancamento->encargo_id = null;
                            $objLancamento->valor = $valorFatEmpresaGrupoA13;
                            $objLancamento->movimentacao_id = $idMovimentacao;
                            $objLancamento->salario_atual = $salario;
                            // $objLancamento->encargo_nome = 'Incidência do Submódulo 2.2 sobre férias, 1/3 (um terço) constitucional de férias e 13o (décimo terceiro) salário';
                            $objLancamento->encargo_nome = 'Incidência do Grupo A sobre férias, abono e 13o. salário';
                            // $objLancamento->encargo_percentual = $objContratoConta->percentual_grupo_a_13_ferias;
                            $objLancamento->encargo_percentual = $objContratoConta->percentual_submodulo22;       // o percentual será o informado e não o calculado.
                            if( !$objLancamento->save() ){
                                $mensagem = 'Erro ao salvar o lançamento para o Grupo A.';
                                \Alert::error($mensagem)->flash();
                                return false;
                            }
                        }
                    } else {
                        // gerar o lançamento para o encargo informado
                        $objLancamento = new Lancamento();
                        $objLancamento->contratoterceirizado_id = $idContratoTerceirizado;
                        $objLancamento->encargo_id = $idEncargoInformado;
                        $objLancamento->valor = $valorInformadoRetirada13;
                        $objLancamento->movimentacao_id = $idMovimentacao;
                        $objLancamento->salario_atual = $salario;  
                        if( !$objLancamento->save() ){
                            $mensagem = 'Erro ao salvar o lançamento.';
                            \Alert::error($mensagem)->flash();
                            return false;
                        }
                        // Aqui vamos controlar os demais lançamentos, além do encargo selecionado pelo usuário
                        if( $nomeEncargoInformado == '13º (décimo terceiro) salário' ){
                            // GRUPO A
                            // para 13 é necessário gerar lançamento para o Grupo A
                            $objLancamento = new Lancamento();
                            $objLancamento->contratoterceirizado_id = $idContratoTerceirizado;
                            $objLancamento->encargo_id = $idEncargoGrupoA;
                            $objLancamento->valor = $valorFatEmpresaGrupoA13;
                            $objLancamento->movimentacao_id = $idMovimentacao;
                            $objLancamento->salario_atual = $salario;
                            if( !$objLancamento->save() ){
                                $mensagem = 'Erro ao salvar o lançamento para o Submódulo 2.2.'; 
                                \Alert::error($mensagem)->flash();
                                return false;
                            }
                        }
                    }
                /**
                * vamos calcular o saldo e fazer os lançamentos para férias
                */
                    $situacaoRetirada = 'Férias';
                    // aqui o usuário informou que a retirada não é para demissão / rescisão
                    $nomeEncargoInformado = self::getNomeEncargoBySituacaoRetirada($situacaoRetirada);
                    $idEncargoInformado = self::getIdEncargoByNomeEncargo($nomeEncargoInformado);
                    $tipoIdEncargo = $objContratoConta->getTipoIdEncargoByIdEncargo($idEncargoInformado);
                    /**
                    * Caso o usuário tenha selecionado Férias e Décimo Terceiro, o tipoIdEncargo chegará aqui 'Férias e Décimo Terceiro'.
                    *
                    * verificar se a conta vinculada é baseada no caderno ou na resolução 169 cnj.
                    * Caso seja pela resolução, o saldo de férias será a soma dos saldos de férias + abono de férias.
                    * Para esse caso também não teremos o tipoIdEncargo, pois os dados estarão diretamente na tabela contratocontas
                    */
                    if($isContaVinculadaPelaResolucao169Cnj){
                        // vamos verificar quanto tem de saldo para férias + abono de férias, pois pela resolução 169, o saldo de férias será coposto pelos dois.
                        $saldoFerias = $objContratoConta->getSaldoContratoContaPorNomeEncargoPorContratoTerceirizadoResolucao169Cnj($idContratoTerceirizado, 'Férias');
                        $saldoAbonoFerias = $objContratoConta->getSaldoContratoContaPorNomeEncargoPorContratoTerceirizadoResolucao169Cnj($idContratoTerceirizado, 'Abono de Férias');
                        $saldoContratoContaPorTipoEncargoFerias = ($saldoFerias + $saldoAbonoFerias);
                    } else {
                        // vamos verificar quanto tem de saldo para o encargo em questão.
                        $saldoContratoContaPorTipoEncargoFerias = $objContratoConta->getSaldoContratoContaPorTipoEncargoPorContratoTerceirizado($idContratoTerceirizado, $tipoIdEncargo);
                    }

                    // início das verificações por encargo
                    $valorMaximoRetiradaFerias = 0; // inicializar o valor máximo para retirada, que será alterado de acordo com o encargo informado.
                    if( $nomeEncargoInformado == 'Férias e 1/3 (um terço) constitucional de férias' ){
                        // para Férias, retirada máxima = ( salário + 1/3 do salário + grupo A)
                        $valorMaximoRetiradaFerias = ( $salario + $valorFatEmpresaGrupoAFerias + $umTercoSalario);
                    }

                    // fim das verificações por encargo
                    // vamos verificar se o valor do fat empresa (grupo A) não é maior do que o saldo do grupo A (encargo)
                    if($valorFatEmpresaGrupoAFerias > $saldoEncargoGrupoA){
                        if($isContaVinculadaPelaResolucao169Cnj){
                            \Alert::error('O valor calculado para o Grupo A é maior do que o saldo do encargo Grupo A.')->flash();
                        } else {
                            \Alert::error('O valor calculado para o Submódulo 2.2 é maior do que o saldo do encargo Submódulo 2.2.')->flash();
                        }
                        return false;
                    }
                    // vamos verificar se o valor informado não é maior que o saldo para o encargo informado
                    if( $valorInformadoRetiradaFerias > $saldoContratoContaPorTipoEncargoFerias ){
                        \Alert::error('O valor informado é maior do que o saldo do encargo.')->flash();
                        return false;
                    }
                    // agora que já calculamos o valor máximo para retirada, pelo encargo informado, vamos verificar se o valor informado é possível.
                    if( $valorMaximoRetiradaFerias < $valorRetiradaFerias ){
                        \Alert::error('O valor da retirada supera o valor máximo permitido.')->flash();
                        return false;
                    }
                    if($isContaVinculadaPelaResolucao169Cnj){
                        /**
                         * Para resolução 169 cnj:
                         * Vamos verificar se o valor a ser lançado para férias, ultrapassa o saldo de férias.
                         * Caso positivo, vamos gerar também, um lançamento para abono de férias.
                         */
                        if( $valorInformadoRetiradaFerias > $saldoFerias ){
                            // precisaremos fazer com que o valor do lançamento para férias seja apenas o valor do saldo para férias. O resto será lançamento para abono.
                            $valorLancarAbonoFerias = ( $valorInformadoRetiradaFerias - $saldoFerias );
                            $valorInformadoRetiradaFerias = $saldoFerias;
                            // FÉRIAS
                            $objLancamento = new Lancamento();
                            $objLancamento->contratoterceirizado_id = $idContratoTerceirizado;
                            $objLancamento->encargo_id = null;
                            $objLancamento->valor = $valorInformadoRetiradaFerias;
                            $objLancamento->movimentacao_id = $idMovimentacao;
                            $objLancamento->salario_atual = $salario; 
                            $objLancamento->encargo_nome = 'Férias';
                            $objLancamento->encargo_percentual = $objContratoConta->percentual_ferias;
                            if( !$objLancamento->save() ){
                                $mensagem = 'Erro ao salvar o lançamento.';
                                \Alert::error($mensagem)->flash();
                                return false;
                            }
                            // ABONO DE FÉRIAS - Caso o valor da retirada ultrapasse o saldo de férias
                            $objLancamento = new Lancamento();
                            $objLancamento->contratoterceirizado_id = $idContratoTerceirizado;
                            $objLancamento->encargo_id = null;
                            $objLancamento->valor = $valorLancarAbonoFerias;
                            $objLancamento->movimentacao_id = $idMovimentacao;
                            $objLancamento->salario_atual = $salario; 
                            $objLancamento->encargo_nome = 'Abono de Férias';
                            $objLancamento->encargo_percentual = $objContratoConta->percentual_abono_ferias;
                            if( !$objLancamento->save() ){
                                $mensagem = 'Erro ao salvar o lançamento.';
                                \Alert::error($mensagem)->flash();
                                return false;
                            }
                        } else {
                            $objLancamento = new Lancamento();
                            $objLancamento->contratoterceirizado_id = $idContratoTerceirizado;
                            $objLancamento->encargo_id = null;
                            $objLancamento->valor = $valorInformadoRetiradaFerias;
                            $objLancamento->movimentacao_id = $idMovimentacao;
                            $objLancamento->salario_atual = $salario;
                            $objLancamento->encargo_nome = 'Férias';
                            $objLancamento->encargo_percentual = $objContratoConta->percentual_ferias;
                            if( !$objLancamento->save() ){
                                $mensagem = 'Erro ao salvar o lançamento.';
                                \Alert::error($mensagem)->flash();
                                return false;
                            }
                        }
                        // GRUPO A
                        // para férias é necessário gerar lançamento para o Grupo A e se for o caso, para Abono de Férias
                        $objLancamento = new Lancamento();
                        $objLancamento->contratoterceirizado_id = $idContratoTerceirizado;
                        $objLancamento->encargo_id = null;
                        $objLancamento->valor = $valorFatEmpresaGrupoAFerias;
                        $objLancamento->movimentacao_id = $idMovimentacao;
                        $objLancamento->salario_atual = $salario;
                        $objLancamento->encargo_nome = 'Incidência do Grupo A sobre férias, abono e 13o. salário';
                        $objLancamento->encargo_percentual = $objContratoConta->percentual_submodulo22; // o percentual será o informado e não o calculado.
                        if( !$objLancamento->save() ){
                            $mensagem = 'Erro ao salvar o lançamento para o Grupo A.';
                            \Alert::error($mensagem)->flash();
                            return false;
                        }
                    } else {
                        // gerar o lançamento para o encargo informado
                        $objLancamento = new Lancamento();
                        $objLancamento->contratoterceirizado_id = $idContratoTerceirizado;
                        $objLancamento->encargo_id = $idEncargoInformado;
                        $objLancamento->valor = $valorInformadoRetiradaFerias;
                        $objLancamento->movimentacao_id = $idMovimentacao;
                        $objLancamento->salario_atual = $salario; 
                        if( !$objLancamento->save() ){
                            $mensagem = 'Erro ao salvar o lançamento.';
                            \Alert::error($mensagem)->flash();
                            return false;
                        }
                        // Aqui vamos controlar os demais lançamentos, além do encargo selecionado pelo usuário
                        if( $nomeEncargoInformado == 'Férias e 1/3 (um terço) constitucional de férias' ){
                            // GRUPO A
                            // para férias é necessário gerar lançamento para o Submódulo 2.2 (grupo A)
                            $objLancamento = new Lancamento();
                            $objLancamento->contratoterceirizado_id = $idContratoTerceirizado;
                            $objLancamento->encargo_id = $idEncargoGrupoA;
                            $objLancamento->valor = $valorFatEmpresaGrupoAFerias;
                            $objLancamento->movimentacao_id = $idMovimentacao;
                            $objLancamento->salario_atual = $salario; 
                            if( !$objLancamento->save() ){
                                $mensagem = 'Erro ao salvar o lançamento para o Submódulo 2.2'; 
                                \Alert::error($mensagem)->flash();
                                return false;
                            }
                        }
                    }
                // vamos tratar o valor retirada, pois, caso ele retorne o número zero, o sistema vai entender que retornou false.
                $valorRetirada = ($valorRetirada13 + $valorRetiradaFerias);
                if($valorRetirada==0){$valorRetirada = true;}

            } elseif($situacaoRetirada == 'Férias') {
                $nomeEncargoInformado = self::getNomeEncargoBySituacaoRetirada($situacaoRetirada);
                $idEncargoInformado = self::getIdEncargoByNomeEncargo($nomeEncargoInformado);
                $tipoIdEncargo = $objContratoConta->getTipoIdEncargoByIdEncargo($idEncargoInformado);
                /**
                * Caso o usuário tenha selecionado Férias e Décimo Terceiro, o tipoIdEncargo chegará aqui 'Férias e Décimo Terceiro'.
                */
                /**
                 * verificar se a conta vinculada é baseada no caderno ou na resolução 169 cnj.
                 * Caso seja pela resolução, o saldo de férias será a soma dos saldos de férias + abono de férias.
                 * Para esse caso também não teremos o tipoIdEncargo, pois os dados estarão diretamente na tabela contratocontas
                 */
                    if($isContaVinculadaPelaResolucao169Cnj){
                        // vamos verificar quanto tem de saldo para férias + abono de férias, pois pela resolução 169, o saldo de férias será coposto pelos dois.
                        $saldoFerias = $objContratoConta->getSaldoContratoContaPorNomeEncargoPorContratoTerceirizadoResolucao169Cnj($idContratoTerceirizado, 'Férias');
                        $saldoAbonoFerias = $objContratoConta->getSaldoContratoContaPorNomeEncargoPorContratoTerceirizadoResolucao169Cnj($idContratoTerceirizado, 'Abono de Férias');
                        $saldoContratoContaPorTipoEncargo = ($saldoFerias + $saldoAbonoFerias);
                    } else {
                        // vamos verificar quanto tem de saldo para o encargo em questão.
                        $saldoContratoContaPorTipoEncargo = $objContratoConta->getSaldoContratoContaPorTipoEncargoPorContratoTerceirizado($idContratoTerceirizado, $tipoIdEncargo);
                    }

                // início das verificações por encargo
                $valorMaximoRetirada = 0; // inicializar o valor máximo para retirada, que será alterado de acordo com o encargo informado.
                if( $nomeEncargoInformado == '13º (décimo terceiro) salário' ){
                    // para 13o. salário, retirada máxima = ( salário + grupo A )
                    $valorMaximoRetirada = ( $salario + $valorFatEmpresaGrupoA );
                } elseif( $nomeEncargoInformado == 'Férias e 1/3 (um terço) constitucional de férias' ){
                    // para Férias, retirada máxima = ( salário + 1/3 do salário + grupo A)
                    $valorMaximoRetirada = ( $salario + $valorFatEmpresaGrupoA + $umTercoSalario);
                }
                // fim das verificações por encargo
                // vamos verificar se o valor do fat empresa (grupo A) não é maior do que o saldo do grupo A (encargo)
                if($valorFatEmpresaGrupoA > $saldoEncargoGrupoA){
                    if($isContaVinculadaPelaResolucao169Cnj){
                        \Alert::error('O valor calculado para o Grupo A é maior do que o saldo do encargo Grupo A.')->flash();
                    } else {
                        \Alert::error('O valor calculado para o Submódulo 2.2 é maior do que o saldo do encargo Submódulo 2.2')->flash();
                    }
                    return false;
                }
                // vamos verificar se o valor informado não é maior que o saldo para o encargo informado
                if( $valorInformadoRetiradaFerias > $saldoContratoContaPorTipoEncargo ){
                    \Alert::error('O valor informado é maior do que o saldo do encargo.')->flash();
                    return false;
                }
                // agora que já calculamos o valor máximo para retirada, pelo encargo informado, vamos verificar se o valor informado é possível.
                if( $valorMaximoRetirada < $valorRetirada ){
                    \Alert::error('O valor da retirada supera o valor máximo permitido.')->flash();
                    return false;
                }
                /**
                 * Caso se trate de resolução 169 cnj, precisaremos alterar a forma de cadastro do lançamento
                 */
                if($isContaVinculadaPelaResolucao169Cnj){
                    /**
                     * Para resolução 169 cnj:
                     * Vamos verificar se o valor a ser lançado para férias, ultrapassa o saldo de férias.
                     * Caso positivo, vamos gerar também, um lançamento para abono de férias.
                     */
                    if( $valorInformadoRetiradaFerias > $saldoFerias ){
                        // precisaremos fazer com que o valor do lançamento para férias seja apenas o valor do saldo para férias. O resto será lançamento para abono.
                        $valorLancarAbonoFerias = ( $valorInformadoRetiradaFerias - $saldoFerias );
                        $valorInformadoRetiradaFerias = $saldoFerias;
                        // FÉRIAS
                        $objLancamento = new Lancamento();
                        $objLancamento->contratoterceirizado_id = $idContratoTerceirizado;
                        $objLancamento->encargo_id = null;
                        $objLancamento->valor = $valorInformadoRetiradaFerias;
                        $objLancamento->movimentacao_id = $idMovimentacao;
                        $objLancamento->salario_atual = $salario;
                        $objLancamento->encargo_nome = 'Férias';
                        $objLancamento->encargo_percentual = $objContratoConta->percentual_ferias;
                        if( !$objLancamento->save() ){
                            $mensagem = 'Erro ao salvar o lançamento.';
                            \Alert::error($mensagem)->flash();
                            return false;
                        }
                        // ABONO DE FÉRIAS - Caso o valor da retirada ultrapasse o saldo de férias
                        $objLancamento = new Lancamento();
                        $objLancamento->contratoterceirizado_id = $idContratoTerceirizado;
                        $objLancamento->encargo_id = null;
                        $objLancamento->valor = $valorLancarAbonoFerias;
                        $objLancamento->movimentacao_id = $idMovimentacao;
                        $objLancamento->salario_atual = $salario;
                        $objLancamento->encargo_nome = 'Abono de Férias';
                        $objLancamento->encargo_percentual = $objContratoConta->percentual_abono_ferias;
                        if( !$objLancamento->save() ){
                            $mensagem = 'Erro ao salvar o lançamento.';
                            \Alert::error($mensagem)->flash();
                            return false;
                        }
                    } else {
                        $objLancamento = new Lancamento();
                        $objLancamento->contratoterceirizado_id = $idContratoTerceirizado;
                        $objLancamento->encargo_id = null;
                        $objLancamento->valor = $valorInformadoRetiradaFerias;
                        $objLancamento->movimentacao_id = $idMovimentacao;
                        $objLancamento->salario_atual = $salario;
                        $objLancamento->encargo_nome = 'Férias';
                        $objLancamento->encargo_percentual = $objContratoConta->percentual_ferias;
                        if( !$objLancamento->save() ){
                            $mensagem = 'Erro ao salvar o lançamento.';
                            \Alert::error($mensagem)->flash();
                            return false;
                        }
                    }
                    // GRUPO A
                    // para férias é necessário gerar lançamento para o Grupo A e se for o caso, para Abono de Férias
                    $objLancamento = new Lancamento();
                    $objLancamento->contratoterceirizado_id = $idContratoTerceirizado;
                    $objLancamento->encargo_id = null;
                    $objLancamento->valor = $valorFatEmpresaGrupoA;
                    $objLancamento->movimentacao_id = $idMovimentacao;
                    $objLancamento->salario_atual = $salario;
                    $objLancamento->encargo_nome = 'Incidência do Grupo A sobre férias, abono e 13o. salário';
                    $objLancamento->encargo_percentual = $objContratoConta->percentual_submodulo22;  // o percentual será o informado e não o calculado.
                        if( !$objLancamento->save() ){
                        $mensagem = 'Erro ao salvar o lançamento para o Grupo A.';
                        \Alert::error($mensagem)->flash();
                        return false;
                    }
                } else {
                    // gerar o lançamento para o encargo informado
                    $objLancamento = new Lancamento();
                    $objLancamento->contratoterceirizado_id = $idContratoTerceirizado;
                    $objLancamento->encargo_id = $idEncargoInformado;
                    $objLancamento->valor = $valorInformadoRetiradaFerias;
                    $objLancamento->movimentacao_id = $idMovimentacao;
                    $objLancamento->salario_atual = $salario; 
                    if( !$objLancamento->save() ){
                        $mensagem = 'Erro ao salvar o lançamento.';
                        \Alert::error($mensagem)->flash();
                        return false;
                    }
                    // Aqui vamos controlar os demais lançamentos, além do encargo selecionado pelo usuário
                    if( $nomeEncargoInformado == '13º (décimo terceiro) salário' ){
                        // Submódulo 2.2 - GRUPO A
                        // para 13 é necessário gerar lançamento para o Submódulo 2.2
                        $objLancamento = new Lancamento();
                        $objLancamento->contratoterceirizado_id = $idContratoTerceirizado;
                        $objLancamento->encargo_id = $idEncargoGrupoA;
                        $objLancamento->valor = $valorFatEmpresaGrupoA;
                        $objLancamento->movimentacao_id = $idMovimentacao;
                        $objLancamento->salario_atual = $salario; 
                        if( !$objLancamento->save() ){
                            // $mensagem = 'Erro ao salvar o lançamento para o Grupo A.';
                            $mensagem = 'Erro ao salvar o lançamento para o Submódulo 2.2'; 
                            \Alert::error($mensagem)->flash();
                            return false;
                        }
                    } elseif( $nomeEncargoInformado == 'Férias e 1/3 (um terço) constitucional de férias' ){
                        // Submódulo 2.2 - GRUPO A
                        // para férias é necessário gerar lançamento para o Submódulo 2.2 (grupo A)
                        $objLancamento = new Lancamento();
                        $objLancamento->contratoterceirizado_id = $idContratoTerceirizado;
                        $objLancamento->encargo_id = $idEncargoGrupoA;
                        $objLancamento->valor = $valorFatEmpresaGrupoA;
                        $objLancamento->movimentacao_id = $idMovimentacao;
                        $objLancamento->salario_atual = $salario; 
                        if( !$objLancamento->save() ){
                            $mensagem = 'Erro ao salvar o lançamento para o Submódulo 2.2';
                            \Alert::error($mensagem)->flash();
                            return false;
                        }
                    }
                }
                // vamos tratar o valor retirada, pois, caso ele retorne o número zero, o sistema vai entender que retornou false.
                if($valorRetirada==0){$valorRetirada = true;}

            } elseif($situacaoRetirada == 'Décimo Terceiro') {
                $nomeEncargoInformado = self::getNomeEncargoBySituacaoRetirada($situacaoRetirada);
                $idEncargoInformado = self::getIdEncargoByNomeEncargo($nomeEncargoInformado);
                $tipoIdEncargo = $objContratoConta->getTipoIdEncargoByIdEncargo($idEncargoInformado);
                /**
                * Caso o usuário tenha selecionado Férias e Décimo Terceiro, o tipoIdEncargo chegará aqui 'Férias e Décimo Terceiro'.
                *
                * vamos verificar quanto tem de saldo para o encargo em questão.
                *
                * Verificar se a conta vinculada é baseada no caderno ou na resolução 169 cnj.
                * Para esse caso (resolução) também não teremos o tipoIdEncargo, pois os dados estarão diretamente na tabela contratocontas
                */
                if($isContaVinculadaPelaResolucao169Cnj){
                    // vamos verificar quanto tem de saldo para férias + abono de férias, pois pela resolução 169, o saldo de férias será coposto pelos dois.
                    $saldoContratoContaPorTipoEncargo = $objContratoConta->getSaldoContratoContaPorNomeEncargoPorContratoTerceirizadoResolucao169Cnj(
                        $idContratoTerceirizado, 
                        '13º (décimo terceiro) salário'
                    );
                } else {
                    // vamos verificar quanto tem de saldo para o encargo em questão.
                    $saldoContratoContaPorTipoEncargo = $objContratoConta->getSaldoContratoContaPorTipoEncargoPorContratoTerceirizado(
                        $idContratoTerceirizado, 
                        $tipoIdEncargo
                    );
                }
                // início das verificações por encargo
                $valorMaximoRetirada = 0; // inicializar o valor máximo para retirada, que será alterado de acordo com o encargo informado.
                if( $nomeEncargoInformado == '13º (décimo terceiro) salário' ){
                    // para 13o. salário, retirada máxima = ( salário + grupo A )
                    $valorMaximoRetirada = ( $salario + $valorFatEmpresaGrupoA );
                } elseif( $nomeEncargoInformado == 'Férias e 1/3 (um terço) constitucional de férias' ){
                    // para Férias, retirada máxima = ( salário + 1/3 do salário + grupo A)
                    $valorMaximoRetirada = ( $salario + $valorFatEmpresaGrupoA + $umTercoSalario);
                }
                // fim das verificações por encargo
                // vamos verificar se o valor do fat empresa (grupo A) não é maior do que o saldo do grupo A (encargo)
                if($valorFatEmpresaGrupoA > $saldoEncargoGrupoA){
                    if($isContaVinculadaPelaResolucao169Cnj){
                        \Alert::error('O valor calculado para o Grupo A é maior do que o saldo do encargo Grupo A.')->flash();
                    } else {
                        \Alert::error('O valor calculado para o Submódulo 2.2 é maior do que o saldo do encargo Submódulo 2.2')->flash();
                    }
                    return false;
                }
                // vamos verificar se o valor informado não é maior que o saldo para o encargo informado
                if( $valorInformadoRetirada13 > $saldoContratoContaPorTipoEncargo ){
                    \Alert::error('O valor informado é maior do que o saldo do encargo.')->flash();
                    return false;
                }
                // agora que já calculamos o valor máximo para retirada, pelo encargo informado, vamos verificar se o valor informado é possível.
                if( $valorMaximoRetirada < $valorRetirada ){
                    \Alert::error('O valor da retirada supera o valor máximo permitido.')->flash();
                    return false;
                }
                /**
                 * Caso se trate de resolução 169 cnj, precisaremos alterar a forma de cadastro do lançamento
                 */
                if($isContaVinculadaPelaResolucao169Cnj){
                    // gerar o lançamento para o encargo informado (13)
                    $objLancamento = new Lancamento();
                    $objLancamento->contratoterceirizado_id = $idContratoTerceirizado;
                    $objLancamento->encargo_id = null;
                    $objLancamento->valor = $valorInformadoRetirada13;
                    $objLancamento->movimentacao_id = $idMovimentacao;
                    $objLancamento->salario_atual = $salario;
                    $objLancamento->encargo_nome = '13º (décimo terceiro) salário';
                    $objLancamento->encargo_percentual = $objContratoConta->percentual_13;
                    if( !$objLancamento->save() ){
                        $mensagem = 'Erro ao salvar o lançamento.';
                        \Alert::error($mensagem)->flash();

                        return false;
                    }
                    // Aqui vamos controlar os demais lançamentos, além do encargo selecionado pelo usuário
                    if( $nomeEncargoInformado == '13º (décimo terceiro) salário' ){
                        // GRUPO A
                        // para 13 é necessário gerar lançamento para o Grupo A
                        $objLancamento = new Lancamento();
                        $objLancamento->contratoterceirizado_id = $idContratoTerceirizado;
                        $objLancamento->encargo_id = null;
                        $objLancamento->valor = $valorFatEmpresaGrupoA;
                        $objLancamento->movimentacao_id = $idMovimentacao;
                        $objLancamento->salario_atual = $salario; 
                        $objLancamento->encargo_nome = 'Incidência do Grupo A sobre férias, abono e 13o. salário';
                        $objLancamento->encargo_percentual = $objContratoConta->percentual_submodulo22; // o percentual será o informado e não o calculado.
                        if( !$objLancamento->save() ){
                            $mensagem = 'Erro ao salvar o lançamento para o Grupo A.';
                            \Alert::error($mensagem)->flash();

                            return false;
                        }
                    }
                } else {
                    // Conta Vinculada pelo Caderno
                    // gerar o lançamento para o encargo informado
                    $objLancamento = new Lancamento();
                    $objLancamento->contratoterceirizado_id = $idContratoTerceirizado;
                    $objLancamento->encargo_id = $idEncargoInformado;
                    $objLancamento->valor = $valorInformadoRetirada13;
                    $objLancamento->movimentacao_id = $idMovimentacao;
                    $objLancamento->salario_atual = $salario;
                    if( !$objLancamento->save() ){
                        $mensagem = 'Erro ao salvar o lançamento.';
                        \Alert::error($mensagem)->flash();
                        return false;
                    }
                    // Aqui vamos controlar os demais lançamentos, além do encargo selecionado pelo usuário
                    if( $nomeEncargoInformado == '13º (décimo terceiro) salário' ){
                        // Submódulo 2.2 - GRUPO A
                        // para 13 é necessário gerar lançamento para o Submódulo 2.2 - Grupo A
                        $objLancamento = new Lancamento();
                        $objLancamento->contratoterceirizado_id = $idContratoTerceirizado;
                        $objLancamento->encargo_id = $idEncargoGrupoA;
                        $objLancamento->valor = $valorFatEmpresaGrupoA;
                        $objLancamento->movimentacao_id = $idMovimentacao;
                        $objLancamento->salario_atual = $salario; 
                        if( !$objLancamento->save() ){
                            $mensagem = 'Erro ao salvar o lançamento para o Submódulo 2.2'; 
                            \Alert::error($mensagem)->flash();
                            return false;
                        }
                    } elseif( $nomeEncargoInformado == 'Férias e 1/3 (um terço) constitucional de férias' ){
                        // Submódulo 2.2 - GRUPO A
                        // para férias é necessário gerar lançamento para o Submódulo 2.2 - Grupo A
                        $objLancamento = new Lancamento();
                        $objLancamento->contratoterceirizado_id = $idContratoTerceirizado;
                        $objLancamento->encargo_id = $idEncargoGrupoA;
                        $objLancamento->valor = $valorFatEmpresaGrupoA;
                        $objLancamento->movimentacao_id = $idMovimentacao;
                        $objLancamento->salario_atual = $salario; 
                        if( !$objLancamento->save() ){
                            $mensagem = 'Erro ao salvar o lançamento para o Submódulo 2.2'; 
                            \Alert::error($mensagem)->flash();
                            return false;
                        }
                    }
                }
                // vamos tratar o valor retirada, pois, caso ele retorne o número zero, o sistema vai entender que retornou false.
                if($valorRetirada==0){$valorRetirada = true;}
            }
        }

        // o usuário poderá informar que a situação é de realocação. Para este caso o valor de retirada será zero.
        if($situacaoRetirada == 'Realocado'){
            return true;
        }
        return $valorRetirada;

    }

    public function getNomeEncargoBySituacaoRetirada($situacaoRetirada){
        if($situacaoRetirada=='Décimo Terceiro'){
            return '13º (décimo terceiro) salário';
        } elseif($situacaoRetirada=='Demissão'){
            return 'Demissão';
        } elseif($situacaoRetirada=='Férias'){
            return 'Férias e 1/3 (um terço) constitucional de férias';
        }
    }
    
    public function store(StoreRequest $request)
    {
        $contratoconta_id = \Route::current()->parameter('contratoconta_id');
        $objContratoConta = Contratoconta::where('id','=',$contratoconta_id)->first();
        // a conta vinculada poderá ser cadastrada com base no caderno de conta vinculada ou na resolução 169 cnj. Vamos verificar qual o caso.
        $isContaVinculadaPelaResolucao169Cnj = $objContratoConta->is_conta_vinculada_pela_resolucao169_cnj;
        $idContratoTerceirizado = $request->input('contratoterceirizado_id');
        $objContratoTerceirizado = \DB::table('contratoterceirizados')
            ->select('contratoterceirizados.*', 'contratos.numero')
            ->join('contratos', 'contratos.id', '=', 'contratoterceirizados.contrato_id')
            ->where('contratoterceirizados.id', '=', $idContratoTerceirizado)
            ->first();
        $idContratoConta = self::getIdContratoContaByIdContratoTerceirizado($idContratoTerceirizado);
        $numeroContrato = $objContratoTerceirizado->numero;
        $user_id = backpack_user()->id;
        $request->request->set('user_id', $user_id);

        $valorRetiradaFerias = $request->input('valor_ferias');
        $valorRetiradaFerias = str_replace('.', '', $valorRetiradaFerias);
        $valorRetiradaFerias = str_replace(',', '.', $valorRetiradaFerias);

        $valorRetirada13 = $request->input('valor_13');
        $valorRetirada13 = str_replace('.', '', $valorRetirada13);
        $valorRetirada13 = str_replace(',', '.', $valorRetirada13);

        $valor_multa_demissao = $request->input('valor_multa_demissao');
        $valor_multa_demissao = str_replace('.', '', $valor_multa_demissao);
        $valor_multa_demissao = str_replace(',', '.', $valor_multa_demissao);
        if($valor_multa_demissao == null){$valor_multa_demissao = 0;}
        // vamos verificar se aconteceu do usuário não ter informado nenhum valor ao solicitar a demissão / rescisão
        $situacaoRetirada = $request->input('situacao_retirada');
        if( $situacaoRetirada=='Demissão' && $valorRetirada13==0 && $valorRetiradaFerias==0 && $valor_multa_demissao==0 ){
            \Alert::error('Favor informar os valores referentes à rescisão.')->flash();

            return redirect()->back();
        }
        $idContrato = $objContratoTerceirizado->contrato_id;
        // vamos buscar o contratoconta_id pelo contratoterceirizado_id
        $request->request->set('contratoconta_id', $idContratoConta);
        // aqui quer dizer que ainda não existe a movimentação. Precisamos criá-la.
        if( !$idMovimentacao = self::criarMovimentacao($request) ){
            $mensagem = 'Problemas ao criar a movimentação.';
            \Alert::error($mensagem)->flash();

            return redirect()->back();
        }
        // aqui a movimentação já foi criada e já temos o $idMovimentacao - vamos atribuir seu valor ao request
        $request->request->set('movimentacao_id', $idMovimentacao);
        // vamos verificar se no mês/ano de competência, o funcionário já tinha iniciado
        if(!self::verificarSeCompetenciaECompativelComDataInicio($request, $objContratoTerceirizado)){
            $mensagem = 'Para o contrato número '.$numeroContrato.' o mês / ano de competência são incompatíveis com mês / ano de início do empregado.';
            \Alert::error($mensagem)->flash();
            if( !self::excluirMovimentacao($idMovimentacao) ){
                \Alert::error('Problemas ao excluir a movimentação.')->flash();
            }

            return redirect()->back();
        }
        // vamos alterar o status da movimentação
        self::alterarStatusMovimentacao($idMovimentacao, 'Movimentação Em Andamento');
        // vamos verificar o saldo total da conta
        $objContratoConta = new Contratoconta();
        $saldoContratoConta = $objContratoConta->getSaldoContratoContaPorContratoTerceirizado($idContratoTerceirizado);
        // vamos verificar o saldo por encargo
        $situacaoRetirada = $request->input('situacao_retirada');
        $dataDemissao = $request->input('data_demissao');
        if( !$valorRetirada = self::verificarSeValorRetiradaEstaDentroDoPermitidoEGerarLancamentos($valorRetirada13, $valorRetiradaFerias, $valor_multa_demissao, $objContratoTerceirizado, $request, $idMovimentacao, $situacaoRetirada, $dataDemissao, $isContaVinculadaPelaResolucao169Cnj) ){
            // aqui quer dizer que não existe saldo para esta retirada - vamos excluir a movimentação
            self::excluirMovimentacao($idMovimentacao);
            \Alert::error('Problemas ao salvar a movimentação.')->flash();

            return redirect()->back();
        }
        // aqui os lançamentos já foram gerados. Vamos alterar o status da movimentação
        self::alterarStatusMovimentacao($idMovimentacao, 'Movimentação Finalizada');
        $mensagem = 'Lançamento de liberação gerado com sucesso!';
        \Alert::success($mensagem)->flash();
        $linkLocation = '/gescon/contrato/contratoconta/'.$idContratoConta.'/movimentacaocontratoconta';

        return redirect($linkLocation);
    }

    public function update(UpdateRequest $request) {
        $redirect_location = parent::updateCrud($request);
 
        return $redirect_location;
    }
}
