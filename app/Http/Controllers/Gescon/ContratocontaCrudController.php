<?php
namespace App\Http\Controllers\Gescon;

use App\Http\Requests\ContratocontaRequest as StoreRequest;
use App\Http\Requests\ContratocontaRequest as UpdateRequest;
use App\Http\Traits\PermissaoTrait;
use App\Models\Codigoitem;
use App\Models\Contratoconta;
use App\Models\Contrato;
use App\Models\Unidade;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\CrudPanel;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ContratocontaCrudController
 * @package App\Http\Controllers\Gescon
 * @property-read CrudPanel $crud
 */
class ContratocontaCrudController extends CrudController
{
    use PermissaoTrait;
    public function setup()
    {
        $contrato_id = \Route::current()->parameter('contrato_id');

        $contrato = Contrato::where('id', '=', $contrato_id)
            ->whereHas('responsaveis', function ($query) {
                $query->where('user_id', '=', backpack_user()->id);
            })->first();

        if (!$contrato) {
            abort('403', config('app.erro_permissao'));
        }

        // verificar status da conta, para habilitar ou não o botão excluir
        $objContratoConta = Contratoconta::where('contrato_id', $contrato->id)->first();
        if(is_object($objContratoConta)){$statusDaContaVinculada = $objContratoConta->getStatusDaConta();}else{$statusDaContaVinculada = null;}

        // vamos verificar se a unidade do usuário é sisg ou não. Isso será importante para o tipo de cadastro da conta vinculada
        $unidade = Unidade::where('id','=',session()->get('user_ug_id'))->first();

        $isSisg = $unidade->sisg;
        $isJud = $unidade->poder == "Judiciário" ? true : false;

        // array de encargos (fat empresa) - só pode ser 1, 2 ou 3% - array será usado em campos()
        $arrayEncargosFatEmpresa = [1 => '1%', 2 => '2%', 3 => '3%'];
        // array com os códigos bancários
        $arrayCodigosBancarios = Contratoconta::getArrayComCodigosBancarios();

        //Setando view personalizada
        $this->crud->setCreateView('vendor.backpack.crud.contrato.contratocontas.create');
        $this->crud->setEditView('vendor.backpack.crud.contrato.contratocontas.edit');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Contratoconta');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/gescon/contrato/' . $contrato_id . '/contratocontas');
        $this->crud->setEntityNameStrings('Conta-Depósito Vinculada', 'Conta-Depósito Vinculada');

        $this->crud->addButtonFromView('top', 'Sobre', 'sobrecontratoconta', 'begin');
        // verificar de onde o usuário vem, para configurarmos o botão voltar (pode vir de contratos ou meus contratos)
        $this->getBotaoVoltar();
        $this->crud->addButtonFromView('line', 'morecontratoconta', 'morecontratoconta', 'end');
        $this->crud->addClause('where', 'contrato_id', '=', $contrato_id);

        $this->crud->allowAccess('show');

        // permissões -> verificar se o usuário tem permissões de acesso ao módulo
        (backpack_user()->can('contratoconta_inserir')) ? $this->crud->allowAccess('create') : $this->crud->denyAccess('create');
        (backpack_user()->can('contratoconta_editar')) ? $this->crud->allowAccess('update') : $this->crud->denyAccess('update');
        (backpack_user()->can('contratoconta_deletar')) ? $this->crud->allowAccess('delete') : $this->crud->denyAccess('delete');

        // mesmo sem as permissões, caso o usuário seja responsável pelo contrato, terá as permissões
        $conresp = $contrato->whereHas('responsaveis', function ($query) {
            $query->whereHas('user', function ($query) {
                $query->where('id', '=', backpack_user()->id);
            })->where('situacao', '=', true);
        })->where('id', '=', $contrato_id);
        if ($conresp && $this->verificaPermissaoConsulta('meus-contratos')) {
            $this->crud->AllowAccess('create');
            $this->crud->AllowAccess('update');
            $this->crud->AllowAccess('delete');
            \Session::put('isResponsavelPeloContrato', true);
        } else {
            \Session::put('isResponsavelPeloContrato', false);
        }

        // caso a conta vinculada esteja encerrada, não mostraremos o botão editar nem o excluir
        if($statusDaContaVinculada == 'Encerrada'){$this->crud->denyAccess('update');}
        if($statusDaContaVinculada == 'Encerrada'){$this->crud->denyAccess('delete');}

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */
        // removendo isJud e passando contrato_id para colunas
        $colunas = $this->Colunas($contrato_id);
        $campos = $this->Campos($contrato, $arrayEncargosFatEmpresa, $arrayCodigosBancarios, $isSisg, $isJud);
        $this->crud->addFields($campos);
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

    }
    public function getBotaoVoltar(){
        $deOndeUsuarioVem = session()->get('de_onde_usuario_vem_conta_vinculada');
        if( empty( $deOndeUsuarioVem ) ){
            $paginaAnterior = \URL::previous();
            $verificacao = strpos($paginaAnterior, '/contrato');    //aqui quer dizer que vem de contratos

            if(!$verificacao){
                \Session::put('de_onde_usuario_vem_conta_vinculada', 'contrato');
                $this->crud->addButtonFromView('top', 'voltar', 'voltarmeucontrato', 'end');
            }
            else{
                \Session::put('de_onde_usuario_vem_conta_vinculada', 'meuscontratos');
                $this->crud->addButtonFromView('top', 'voltar', 'voltarcontrato', 'end');
            }
        } else {
            if($deOndeUsuarioVem == 'contrato'){
                $this->crud->addButtonFromView('top', 'voltar', 'voltarmeucontrato', 'end');
            }
            elseif($deOndeUsuarioVem == 'meuscontratos'){
                $this->crud->addButtonFromView('top', 'voltar', 'voltarcontrato', 'end');
            }
        }
        return true;
    }
    public function verificarSeContratoJaPossuiConta($request){
        $contratoId = \Route::current()->parameter('contrato_id');
        if( Contratoconta::where('contrato_id', $contratoId)->count() > 0 ){return true;}
        return false;
    }
    public function getDadosPercentuaisGrupoAESubmodulo22ByEncargoResolucao169Cnj($request){
        // vamos verificar se o usuário informou 1, 2 ou 3% no Encargos e buscar os percentuais de grupo A e submódulo 2.2 relacionados.
        $arrayDadosPercentuais = Contratoconta::getDadosPercentuaisGrupoAESubmodulo22ByEncargoResolucao169Cnj($request);

        // vamos jogar os dados no request, para que sejam salvos
        $percentual_grupo_a_13_ferias = $arrayDadosPercentuais['percentual_grupo_a'];
        $percentual_grupo_a_13_ferias_id = $arrayDadosPercentuais['percentual_grupo_a_id'];
        $request->request->set('percentual_grupo_a_13_ferias', $percentual_grupo_a_13_ferias);
        $request->request->set('percentual_grupo_a_13_ferias_codigoitens_id', $percentual_grupo_a_13_ferias_id);

        $percentual_submodulo_22 = $arrayDadosPercentuais['percentual_submodulo_22'];
        $percentual_submodulo_22_id = $arrayDadosPercentuais['percentual_submodulo_22_id'];
        $request->request->set('percentual_submodulo22', $percentual_submodulo_22);
        $request->request->set('percentual_submodulo_22_id', $percentual_submodulo_22_id);

        $percentual_ferias = $arrayDadosPercentuais['percentual_ferias'];
        $request->request->set('percentual_ferias', $percentual_ferias);

        $percentual_abono_ferias = $arrayDadosPercentuais['percentual_abono_ferias'];
        $request->request->set('percentual_abono_ferias', $percentual_abono_ferias);

        $percentual_13 = $arrayDadosPercentuais['percentual_13'];
        $request->request->set('percentual_13', $percentual_13);

        $percentual_multa_sobre_fgts = $arrayDadosPercentuais['percentual_multa_sobre_fgts'];
        $request->request->set('percentual_multa_sobre_fgts', $percentual_multa_sobre_fgts);

        return true;
    }
    public function getDadosPercentuaisGrupoAESubmodulo22ByEncargo($request){
        // vamos verificar se o usuário informou 1, 2 ou 3% no Encargos e buscar os percentuais de grupo A e submódulo 2.2 relacionados.
        $arrayDadosPercentuais = Contratoconta::getDadosPercentuaisGrupoAESubmodulo22ByEncargo($request);
        // vamos jogar os dados no request, para que sejam salvos
        $percentual_grupo_a_13_ferias = $arrayDadosPercentuais['percentual_grupo_a'];
        $percentual_grupo_a_13_ferias_id = $arrayDadosPercentuais['percentual_grupo_a_id'];
        $request->request->set('percentual_grupo_a_13_ferias', $percentual_grupo_a_13_ferias);
        $request->request->set('percentual_grupo_a_13_ferias_codigoitens_id', $percentual_grupo_a_13_ferias_id);
        $percentual_submodulo_22 = $arrayDadosPercentuais['percentual_submodulo_22'];
        $percentual_submodulo_22_id = $arrayDadosPercentuais['percentual_submodulo_22_id'];
        $request->request->set('percentual_submodulo22', $percentual_submodulo_22);
        $request->request->set('percentual_submodulo_22_id', $percentual_submodulo_22_id);
        return true;
    }
    public function alterarVirgulasPorPontos($request){
        $percentual13 = str_replace(',', '.', $request->get('percentual_13'));
        $request->request->set('percentual_13', $percentual13);

        $percentualFerias = str_replace(',', '.', $request->get('percentual_ferias'));
        $request->request->set('percentual_ferias', $percentualFerias);

        $percentualAbonoFerias = str_replace(',', '.', $request->get('percentual_abono_ferias'));
        $request->request->set('percentual_abono_ferias', $percentualAbonoFerias);

        $percentualMultaSobreFgts = str_replace(',', '.', $request->get('percentual_multa_sobre_fgts'));
        $request->request->set('percentual_multa_sobre_fgts', $percentualMultaSobreFgts);

        $percentualSubmodulo22 = str_replace(',', '.', $request->get('percentual_submodulo22'));
        $request->request->set('percentual_submodulo22', $percentualSubmodulo22);
    }
    public function store(StoreRequest $request)
    {
        // vamos alterar as vírgulas por pontos no request
        self::alterarVirgulasPorPontos($request);
        // será permitida apenas uma conta por contrato. Vamos verificar
        if(self::verificarSeContratoJaPossuiConta($request)){
            \Alert::error('Já existe uma Conta-Depósito Vinculada a este contrato!')->flash();
            return redirect()->back();
        }
        // vamos verificar se este banco / agência / conta ainda não foram utilizados
        if(Contratoconta::verificarSeDadosAindaNaoExistemNaBase($request, 'insert')){
            \Alert::error('Estes dados de banco / agência / conta já foram utilizados.')->flash();
            return redirect()->back();
        }
        // o usuário poderá escolher se deseja cadastrar a conta vinculada pelo caderno ou pela resolução. Vamos verificar qual foi a escolha.
        $isContaVinculadaPelaResolucao169Cnj = $request->get('is_conta_vinculada_pela_resolucao169_cnj');
        if($isContaVinculadaPelaResolucao169Cnj == null){
            $isContaVinculadaPelaResolucao169Cnj = false;
            $request->request->set('is_conta_vinculada_pela_resolucao169_cnj', $isContaVinculadaPelaResolucao169Cnj);

            $request->request->set('percentual_13', 0);
            $request->request->set('percentual_ferias', 0);
            $request->request->set('percentual_abono_ferias', 0);
            $request->request->set('percentual_multa_sobre_fgts', 0);
            $request->request->set('percentual_submodulo22', 0);
        }

        if($isContaVinculadaPelaResolucao169Cnj){
            self::getDadosPercentuaisGrupoAESubmodulo22ByEncargoResolucao169Cnj($request);
        } else {
            self::getDadosPercentuaisGrupoAESubmodulo22ByEncargo($request);
        }
        $redirect_location = parent::storeCrud($request);

        return $redirect_location;
    }
    public function update(UpdateRequest $request)
    {
        // vamos alterar as vírgulas por pontos no request
        self::alterarVirgulasPorPontos($request);
        // vamos verificar se este banco / agência / conta ainda não foram utilizados
        if(Contratoconta::verificarSeDadosAindaNaoExistemNaBase($request, 'update')){
            \Alert::error('Estes dados de banco / agência / conta já foram utilizados por outra Conta-Depósito Vinculada.')->flash();
            return redirect()->back();
        }
        // o usuário poderá escolher se deseja cadastrar a conta vinculada pelo caderno ou pela resolução. Vamos verificar qual foi a escolha.
        $isContaVinculadaPelaResolucao169Cnj = $request->get('is_conta_vinculada_pela_resolucao169_cnj');

        if($isContaVinculadaPelaResolucao169Cnj == null){
            $isContaVinculadaPelaResolucao169Cnj = false;
            $request->request->set('is_conta_vinculada_pela_resolucao169_cnj', $isContaVinculadaPelaResolucao169Cnj);
            $request->request->set('percentual_13', 0);
            $request->request->set('percentual_ferias', 0);
            $request->request->set('percentual_abono_ferias', 0);
            $request->request->set('percentual_multa_sobre_fgts', 0);
            $request->request->set('percentual_submodulo22', 0);
        }

        // verificar se é não sisg e não jud para setar  o fat_empresa null quando resolucao 169
        $naoJudNaoSisg = $request->get('is_choice');

        if($isContaVinculadaPelaResolucao169Cnj && $naoJudNaoSisg) {
            $request->request->set('fat_empresa', null);
        }

        if($isContaVinculadaPelaResolucao169Cnj){
            self::getDadosPercentuaisGrupoAESubmodulo22ByEncargoResolucao169Cnj($request);
        } else {
            self::getDadosPercentuaisGrupoAESubmodulo22ByEncargo($request);
        }

        $redirect_location = parent::updateCrud($request);
        return $redirect_location;
    }
    /**
     * o usuário poderá informar que deseja cadastrar uma conta vinculada,
     * baseando-se no caderno conta vinculada ou na resolução 169 cnj.
     * Vamos fazer essa verificação, antes de mostrarmos os campos para cadastro.
     */
    public function Campos($contrato, $arrayEncargosFatEmpresa, $arrayCodigosBancarios, $isSisg, $isJud)
    {
        if($isSisg && !$isJud) {
            $isContaVinculadaPelaResolucao169Cnj = false;
            $isChoice = false;
        } elseif (!$isSisg && $isJud) {
            $isContaVinculadaPelaResolucao169Cnj = true;
            $isChoice = false;
        } else {
            $isChoice = true;
            $isContaVinculadaPelaResolucao169Cnj = true;
        }

        if($isContaVinculadaPelaResolucao169Cnj == true && $isChoice == false) {
            $campos = [
                [   // Hidden
                    'name' => 'is_choice',
                    'type' => 'hidden',
                    'default' => $isChoice,
                ],
                [   // Hidden
                    'name' => 'is_conta_vinculada_pela_resolucao169_cnj',
                    'type' => 'hidden',
                    'default' => $isContaVinculadaPelaResolucao169Cnj,
                ],
                [   // Hidden
                    'name' => 'contrato_id',
                    'type' => 'hidden',
                    'default' => $contrato->id,
                ],
                [ // select_from_array
                    'name' => 'banco',
                    'label' => "Banco",
                    'type' => 'select2_from_array',
                    'options' => $arrayCodigosBancarios,
                    'allows_null' => false,
                    'allows_multiple' => false, // OPTIONAL; needs you to cast this to array in your model;
                ],
                [
                    'name' => 'agencia',
                    'label' => 'Agência', // Table column heading
                    'type' => 'text',
                    'attributes' => [
                        'maxlength' => '20',
                    ],
                ],
                [
                    'name' => 'conta_corrente',
                    'label' => 'Conta Corrente', // Table column heading
                    'type' => 'text',
                    'attributes' => [
                        'maxlength' => '50',
                    ],
                ],
                [   
                    'name' => 'percentual_13',
                    'label' => 'Percentual 13º (décimo terceiro) salário',
                    'type' => 'text',
                    'prefix' => "%",
                    'attributes' => [
                        'required' => true,
                        'id' => 'percentual_13',
                    ],
                ],
                [   
                    'name' => 'percentual_ferias',
                    'label' => 'Percentual férias',
                    'type' => 'text',
                    'prefix' => "%",
                    'attributes' => [
                        'required' => true,
                        'id' => 'percentual_ferias',
                    ],
                ],
                [   
                    'name' => 'percentual_abono_ferias',
                    'label' => 'Percentual abono de férias',
                    'type' => 'text',
                    'prefix' => "%",
                    'attributes' => [
                        'required' => true,
                        'id' => 'percentual_abono_ferias',
                    ],
                ],
                [   
                    'name' => 'percentual_multa_sobre_fgts',
                    'label' => 'Percentual multa sobre o FGTS para as rescisões sem justa causa',
                    'type' => 'text',
                    'prefix' => "%",
                    'attributes' => [
                        'required' => true,
                        'id' => 'percentual_multa_sobre_fgts',
                    ],
                ],
                [   
                    'name' => 'percentual_submodulo22',
                    'label' => 'Grupo A', 
                    'type' => 'text',
                    'prefix' => "%",
                    'attributes' => [
                        'required' => true,
                        'id' => 'percentual_submodulo22',
                    ],
                ],
            ];
        } elseif ($isContaVinculadaPelaResolucao169Cnj == false && $isChoice == false) {
            $campos = [
                [   // Hidden
                    'name' => 'is_choice',
                    'type' => 'hidden',
                    'default' => $isChoice,
                ],
                [   // Hidden
                    'name' => 'is_conta_vinculada_pela_resolucao169_cnj',
                    'type' => 'hidden',
                    'default' => $isContaVinculadaPelaResolucao169Cnj,
                ],
                [   // Hidden
                    'name' => 'contrato_id',
                    'type' => 'hidden',
                    'default' => $contrato->id,
                ],
                [ // select_from_array
                    'name' => 'banco',
                    'label' => "Banco",
                    'type' => 'select2_from_array',
                    'options' => $arrayCodigosBancarios,
                    'allows_null' => false,
                    'allows_multiple' => false, // OPTIONAL; needs you to cast this to array in your model;
                    'attributes' => [
                        'required' => true,
                    ],
                ],
                [
                    'name' => 'agencia',
                    'label' => 'Agência', // Table column heading
                    'type' => 'text',
                    'attributes' => [
                        'maxlength' => '20',
                        'required' => true,
                    ],
                ],
                [
                    'name' => 'conta_corrente',
                    'label' => 'Conta Corrente', // Table column heading
                    'type' => 'text',
                    'attributes' => [
                        'maxlength' => '50',
                        'required' => true,
                    ],
                ],
                [
                    'name' => 'fat_empresa',
                    'label' => "Encargo SAT (%)",
                    'type' => 'radio',
                    'options' => $arrayEncargosFatEmpresa,
                    'allows_null' => false,
                    'allows_multiple' => false, // OPTIONAL; needs you to cast this to array in your model;
                    'inline' => true,
                    'default' => 1
                ],
            ];
        } else {
            $campos = [
                [   // Hidden
                    'name' => 'is_choice',
                    'type' => 'hidden',
                    'default' => $isChoice,
                ],
                [   // Hidden
                    'name' => 'is_conta_vinculada_pela_resolucao169_cnj',
                    'type' => 'hidden',
                    'default' => $isContaVinculadaPelaResolucao169Cnj,
                ],
                [   // Hidden
                    'name' => 'contrato_id',
                    'type' => 'hidden',
                    'default' => $contrato->id,
                ],
                [
                    'name' => 'is_conta_vinculada_radio',
                    'label' => "Norma",
                    'type' => 'radio',
                    'options' => [1 => 'IN 05/2017', 0 => 'Res. 169 CNJ'],
                    'allows_null' => false,
                    'allows_multiple' => false, // OPTIONAL; needs you to cast this to array in your model;
                    'inline' => true,
                    'default' => 1
                ],

                [ // select_from_array
                    'name' => 'banco',
                    'label' => "Banco",
                    'type' => 'select2_from_array',
                    'options' => $arrayCodigosBancarios,
                    'allows_null' => false,
                    'allows_multiple' => false, // OPTIONAL; needs you to cast this to array in your model;
                ],
                [
                    'name' => 'agencia',
                    'label' => 'Agência', // Table column heading
                    'type' => 'text',
                    'attributes' => [
                        'maxlength' => '20',
                    ],
                ],
                [
                    'name' => 'conta_corrente',
                    'label' => 'Conta Corrente', // Table column heading
                    'type' => 'text',
                    'attributes' => [
                        'maxlength' => '50',
                    ],
                ],
                [   
                    'name' => 'percentual_13',
                    'label' => 'Percentual 13º (décimo terceiro) salário',
                    'type' => 'text',
                    'prefix' => "%",
                    'attributes' => [
                        'maxlength' => '15',
                        'required' => true,
                        'id' => 'percentual_13',
                        'class' => 'form-control cnj'
                    ],
                ],
                [   
                    'name' => 'percentual_ferias',
                    'label' => 'Percentual férias',
                    'type' => 'text',
                    'prefix' => "%",
                    'attributes' => [
                        'maxlength' => '15',
                        'required' => true,
                        'id' => 'percentual_ferias',
                        'class' => 'form-control cnj'
                    ],
                ],
                [   
                    'name' => 'percentual_abono_ferias',
                    'label' => 'Percentual abono de férias',
                    'type' => 'text',
                    'prefix' => "%",
                    'attributes' => [
                        'maxlength' => '15',
                        'required' => true,
                        'id' => 'percentual_abono_ferias',
                        'class' => 'form-control cnj'
                    ],
                ],
                [   
                    'name' => 'percentual_multa_sobre_fgts',
                    'label' => 'Percentual multa sobre o FGTS para as rescisões sem justa causa',
                    'type' => 'text',
                    'prefix' => "%",
                    'attributes' => [
                        'maxlength' => '15',
                        'required' => true,
                        'id' => 'percentual_multa_sobre_fgts',
                        'class' => 'form-control cnj'
                    ],
                ],
                [   
                    'name' => 'percentual_submodulo22',
                    'label' => 'Grupo A',
                    'type' => 'text',
                    'prefix' => "%",
                    'attributes' => [
                        'maxlength' => '15',
                        'required' => true,
                        'id' => 'percentual_submodulo22',
                        'class' => 'form-control cnj'
                    ],
                ],
                [
                    'name' => 'fat_empresa',
                    'label' => "Encargo SAT (%)",
                    'type' => 'radio',
                    'options' => $arrayEncargosFatEmpresa,
                    'allows_null' => false,
                    'allows_multiple' => false, // OPTIONAL; needs you to cast this to array in your model;
                    'inline' => true,
                    'default' => 1
                ],
            ];
        }
        return $campos;
    }

    public function Colunas($contrato_id)
    {
        // buscando is_conta_vinculada_pela_resolucao169_cnj do banco
        if(Contratoconta::where('contrato_id', $contrato_id)->get()->count() > 0) {
            if(Contratoconta::where('contrato_id', $contrato_id)->first()->is_conta_vinculada_pela_resolucao169_cnj){
                // aqui é pela resolução 169
                $this->crud->addColumn(
                    [
                        'name' => 'getTipoContaVinculada',
                        'label' => 'Norma', // Table column heading
                        'type' => 'model_function',
                        'function_name' => 'getTipoContaVinculada', // the method in your Model
                        'orderable' => true,
                        'visibleInTable' => true, // no point, since it's a large text
                        'visibleInModal' => true, // would make the modal too big
                        'visibleInExport' => true, // not important enough
                        'visibleInShow' => true, // sure, why not
                    ]
                );
                $this->crud->addColumn(
                    [
                        'name' => 'banco',
                        'label' => 'Banco',
                        'type' => 'text',
                        'orderable' => true,
                        'visibleInTable' => true, // no point, since it's a large text
                        'visibleInModal' => true, // would make the modal too big
                        'visibleInExport' => true, // not important enough
                        'visibleInShow' => true, // sure, why not
                    ]
                );
                $this->crud->addColumn(
                    [
                        'name' => 'agencia',
                        'label' => 'Agência',
                        'type' => 'text',
                        'orderable' => true,
                        'visibleInTable' => true, // no point, since it's a large text
                        'visibleInModal' => true, // would make the modal too big
                        'visibleInExport' => true, // not important enough
                        'visibleInShow' => true, // sure, why not
                    ]
                );
                $this->crud->addColumn(
                    [
                        'name' => 'conta_corrente',
                        'label' => 'Conta Corrente',
                        'type' => 'text',
                        'orderable' => true,
                        'visibleInTable' => true, // no point, since it's a large text
                        'visibleInModal' => true, // would make the modal too big
                        'visibleInExport' => true, // not important enough
                        'visibleInShow' => true, // sure, why not
                    ]
                );
                $this->crud->addColumn(
                    [
                        'name' => 'percentual_13',
                        'label' => 'Percentual 13o. (décimo terceiro) salário', // Table column heading
                        'orderable' => true,
                        'visibleInTable' => true, // no point, since it's a large text
                        'visibleInModal' => true, // would make the modal too big
                        'visibleInExport' => true, // not important enough
                        'visibleInShow' => true, // sure, why not
                        'prefix' => "% ",
                    ]
                );
                $this->crud->addColumn(
                    [
                        'name' => 'percentual_ferias',
                        'label' => 'Percentual férias', // Table column heading
                        'orderable' => true,
                        'visibleInTable' => true, // no point, since it's a large text
                        'visibleInModal' => true, // would make the modal too big
                        'visibleInExport' => true, // not important enough
                        'visibleInShow' => true, // sure, why not
                        'prefix' => "% ",
                    ]
                );
                $this->crud->addColumn(
                    [
                        'name' => 'percentual_abono_ferias',
                        'label' => 'Percentual abono de férias', // Table column heading
                        'orderable' => true,
                        'visibleInTable' => true, // no point, since it's a large text
                        'visibleInModal' => true, // would make the modal too big
                        'visibleInExport' => true, // not important enough
                        'visibleInShow' => true, // sure, why not
                        'prefix' => "% ",
                    ]
                );
                $this->crud->addColumn(
                    [
                        'name' => 'percentual_multa_sobre_fgts',
                        'label' => 'Percentual multa sobre fgts', // Table column heading
                        'orderable' => true,
                        'visibleInTable' => true, // no point, since it's a large text
                        'visibleInModal' => true, // would make the modal too big
                        'visibleInExport' => true, // not important enough
                        'visibleInShow' => true, // sure, why not
                        'prefix' => "% ",
                    ]
                );
                $this->crud->addColumn(
                    [
                        'name' => 'percentual_submodulo22',
                        'label' => 'Grupo A',
                        'type' => 'number',
                        'decimals' => 2,
                        'orderable' => true,
                        'visibleInTable' => true, // no point, since it's a large text
                        'visibleInModal' => true, // would make the modal too big
                        'visibleInExport' => true, // not important enough
                        'visibleInShow' => true, // sure, why not
                        'prefix' => "% ",
                    ]
                );
                $this->crud->addColumn(
                    [
                        'name' => 'fat_empresa',
                        'label' => 'Fat empresa', // Table column heading
                        'orderable' => false,
                        'visibleInTable' => false, // no point, since it's a large text
                        'visibleInModal' => false, // would make the modal too big
                        'visibleInExport' => false, // not important enough
                        'visibleInShow' => false, // sure, why not
                    ]
                );
                $this->crud->addColumn(
                    [
                        'name' => 'percentual_grupo_a_13_ferias_codigoitens_id',
                        'label' => 'Percentual grupo a 13 ferias codigoitens', // Table column heading
                        'orderable' => false,
                        'visibleInTable' => false, // no point, since it's a large text
                        'visibleInModal' => false, // would make the modal too big
                        'visibleInExport' => false, // not important enough
                        'visibleInShow' => false, // sure, why not
                        'prefix' => "% ",
                    ]
                );
                $this->crud->addColumn(
                    [
                        'name' => 'percentual_submodulo_22_id',
                        'label' => 'Percentual submodulo 22 codigoitens', // Table column heading
                        'orderable' => false,
                        'visibleInTable' => false, // no point, since it's a large text
                        'visibleInModal' => false, // would make the modal too big
                        'visibleInExport' => false, // not important enough
                        'visibleInShow' => false, // sure, why not
                        'prefix' => "% ",
                    ]
                );
                $this->crud->addColumn(
                    [
                        'name' => 'is_conta_vinculada_pela_resolucao169_cnj',
                        'label' => 'Is conta vinculada pela resolucao169 cnj', // Table column heading
                        'orderable' => false,
                        'visibleInTable' => false, // no point, since it's a large text
                        'visibleInModal' => false, // would make the modal too big
                        'visibleInExport' => false, // not important enough
                        'visibleInShow' => false, // sure, why not
                    ]
                );
                $this->crud->addColumn(
                    [
                        'name' => 'percentual_grupo_a_13_ferias',
                        'label' => 'Incidência do Grupo A',
                        'type' => 'number',
                        'decimals' => 2,
                        'orderable' => true,
                        'visibleInTable' => true, // no point, since it's a large text
                        'visibleInModal' => true, // would make the modal too big
                        'visibleInExport' => true, // not important enough
                        'visibleInShow' => true, // sure, why not
                        'prefix' => "% ",
                    ]
                );
                $this->crud->addColumn(
                    [
                        'name' => 'getSaldoContratoContaParaColunas',
                        'label' => 'Saldo da Conta', // Table column heading
                        'type' => 'model_function',
                        'function_name' => 'getSaldoContratoContaParaColunas', // the method in your Model
                        'orderable' => true,
                        'visibleInTable' => true, // no point, since it's a large text
                        'visibleInModal' => true, // would make the modal too big
                        'visibleInExport' => true, // not important enough
                        'visibleInShow' => true, // sure, why not
                        'prefix' => "R$ ",
                    ]
                );
                $this->crud->addColumn(
                    [
                        'name' => 'saldo_encerramento',
                        'label' => 'Saldo Encerramento', // Table column heading
                        'orderable' => false,
                        'visibleInTable' => false, // no point, since it's a large text
                        'visibleInModal' => true, // would make the modal too big
                        'visibleInExport' => true, // not important enough
                        'visibleInShow' => true, // sure, why not
                        'prefix' => "R$ ",
                    ]
                );
                $this->crud->addColumn(
                    [
                        'name' => 'getStatusDaContaParaMostrarNaColuna',
                        'label' => 'Status da Conta', // Table column heading
                        'type' => 'model_function',
                        'function_name' => 'getStatusDaContaParaMostrarNaColuna', // the method in your Model
                        'orderable' => true,
                        'visibleInTable' => true, // no point, since it's a large text
                        'visibleInModal' => true, // would make the modal too big
                        'visibleInExport' => true, // not important enough
                        'visibleInShow' => true, // sure, why not
                    ]
                );
            } else {
                // aqui é pela IN 05/2017
                $this->crud->addColumn(
                    [
                        'name' => 'getTipoContaVinculada',
                        'label' => 'Norma', // Table column heading
                        'type' => 'model_function',
                        'function_name' => 'getTipoContaVinculada', // the method in your Model
                        'orderable' => true,
                        'visibleInTable' => true, // no point, since it's a large text
                        'visibleInModal' => true, // would make the modal too big
                        'visibleInExport' => true, // not important enough
                        'visibleInShow' => true, // sure, why not
                    ]
                );
                $this->crud->addColumn(
                    [
                        'name' => 'banco',
                        'label' => 'Banco',
                        'type' => 'text',
                        'orderable' => true,
                        'visibleInTable' => true, // no point, since it's a large text
                        'visibleInModal' => true, // would make the modal too big
                        'visibleInExport' => true, // not important enough
                        'visibleInShow' => true, // sure, why not
                    ]
                );
                $this->crud->addColumn(
                    [
                        'name' => 'agencia',
                        'label' => 'Agência',
                        'type' => 'text',
                        'orderable' => true,
                        'visibleInTable' => true, // no point, since it's a large text
                        'visibleInModal' => true, // would make the modal too big
                        'visibleInExport' => true, // not important enough
                        'visibleInShow' => true, // sure, why not
                    ]
                );
                $this->crud->addColumn(
                    [
                        'name' => 'conta_corrente',
                        'label' => 'Conta Corrente',
                        'type' => 'text',
                        'orderable' => true,
                        'visibleInTable' => true, // no point, since it's a large text
                        'visibleInModal' => true, // would make the modal too big
                        'visibleInExport' => true, // not important enough
                        'visibleInShow' => true, // sure, why not
                    ]
                );
                $this->crud->addColumn(
                    [
                        'name' => 'percentual_grupo_a_13_ferias',
                        'label' => 'Incidência do Submódulo 2.2',
                        'type' => 'number',
                        'decimals' => 2,
                        'orderable' => true,
                        'visibleInTable' => true, // no point, since it's a large text
                        'visibleInModal' => true, // would make the modal too big
                        'visibleInExport' => true, // not important enough
                        'visibleInShow' => true, // sure, why not
                        'prefix' => "% ",
                    ]
                );
                $this->crud->addColumn(
                    [
                        'name' => 'percentual_submodulo22',
                        'label' => 'Submódulo 2.2', 
                        'type' => 'number',
                        'decimals' => 2,
                        'orderable' => true,
                        'visibleInTable' => true, // no point, since it's a large text
                        'visibleInModal' => true, // would make the modal too big
                        'visibleInExport' => true, // not important enough
                        'visibleInShow' => true, // sure, why not
                        'prefix' => "% ",
                    ]
                );
                $this->crud->addColumn(
                    [
                        'name' => 'getFatEmpresa',
                        'label' => 'Encargo',
                        'type' => 'model_function',
                        'function_name' => 'getFatEmpresa', // the method in your Model
                        'orderable' => true,
                        'visibleInTable' => true, // no point, since it's a large text
                        'visibleInModal' => true, // would make the modal too big
                        'visibleInExport' => true, // not important enough
                        'visibleInShow' => true, // sure, why not
                        'prefix' => "% ",
                    ]
                );
                $this->crud->addColumn(
                    [
                        'name' => 'getSaldoContratoContaParaColunas',
                        'label' => 'Saldo da Conta', // Table column heading
                        'type' => 'model_function',
                        'function_name' => 'getSaldoContratoContaParaColunas', // the method in your Model
                        'orderable' => true,
                        'visibleInTable' => true, // no point, since it's a large text
                        'visibleInModal' => true, // would make the modal too big
                        'visibleInExport' => true, // not important enough
                        'visibleInShow' => true, // sure, why not
                        'prefix' => "R$ ",
                    ]
                );
                $this->crud->addColumn(
                    [
                        'name' => 'saldo_encerramento',
                        'label' => 'Saldo Encerramento', // Table column heading
                        'orderable' => false,
                        'visibleInTable' => false, // no point, since it's a large text
                        'visibleInModal' => true, // would make the modal too big
                        'visibleInExport' => true, // not important enough
                        'visibleInShow' => true, // sure, why not
                        'prefix' => "R$ ",
                    ]
                );
                $this->crud->addColumn(
                    [
                        'name' => 'getStatusDaContaParaMostrarNaColuna',
                        'label' => 'Status da Conta', // Table column heading
                        'type' => 'model_function',
                        'function_name' => 'getStatusDaContaParaMostrarNaColuna', // the method in your Model
                        'orderable' => true,
                        'visibleInTable' => true, // no point, since it's a large text
                        'visibleInModal' => true, // would make the modal too big
                        'visibleInExport' => true, // not important enough
                        'visibleInShow' => true, // sure, why not
                    ]
                );
                $this->crud->addColumn(
                    [
                        'name' => 'fat_empresa',
                        'label' => 'Fat empresa', // Table column heading
                        'orderable' => false,
                        'visibleInTable' => false, // no point, since it's a large text
                        'visibleInModal' => false, // would make the modal too big
                        'visibleInExport' => false, // not important enough
                        'visibleInShow' => false, // sure, why not
                    ]
                );
                $this->crud->addColumn(
                    [
                        'name' => 'percentual_grupo_a_13_ferias_codigoitens_id',
                        'label' => 'Percentual grupo a 13 ferias codigoitens', // Table column heading
                        'orderable' => false,
                        'visibleInTable' => false, // no point, since it's a large text
                        'visibleInModal' => false, // would make the modal too big
                        'visibleInExport' => false, // not important enough
                        'visibleInShow' => false, // sure, why not
                        'prefix' => "% ",
                    ]
                );
                $this->crud->addColumn(
                    [
                        'name' => 'percentual_submodulo_22_id',
                        'label' => 'Percentual submodulo 22 codigoitens', // Table column heading
                        'orderable' => false,
                        'visibleInTable' => false, // no point, since it's a large text
                        'visibleInModal' => false, // would make the modal too big
                        'visibleInExport' => false, // not important enough
                        'visibleInShow' => false, // sure, why not
                    ]
                );
                $this->crud->addColumn(
                    [
                        'name' => 'is_conta_vinculada_pela_resolucao169_cnj',
                        'label' => 'Is conta vinculada pela resolucao169 cnj', // Table column heading
                        'orderable' => false,
                        'visibleInTable' => false, // no point, since it's a large text
                        'visibleInModal' => false, // would make the modal too big
                        'visibleInExport' => false, // not important enough
                        'visibleInShow' => false, // sure, why not
                    ]
                );
                $this->crud->addColumn(
                    [
                        'name' => 'percentual_ferias',
                        'label' => 'Percentual ferias', // Table column heading
                        'orderable' => false,
                        'visibleInTable' => false, // no point, since it's a large text
                        'visibleInModal' => false, // would make the modal too big
                        'visibleInExport' => false, // not important enough
                        'visibleInShow' => false, // sure, why not
                        'prefix' => "% ",
                    ]
                );
                $this->crud->addColumn(
                    [
                        'name' => 'percentual_abono_ferias',
                        'label' => 'Percentual abono ferias', // Table column heading
                        'orderable' => false,
                        'visibleInTable' => false, // no point, since it's a large text
                        'visibleInModal' => false, // would make the modal too big
                        'visibleInExport' => false, // not important enough
                        'visibleInShow' => false, // sure, why not
                        'prefix' => "% ",
                    ]
                );
                $this->crud->addColumn(
                    [
                        'name' => 'percentual_multa_sobre_fgts',
                        'label' => 'Percentual multa sobre fgts', // Table column heading
                        'orderable' => false,
                        'visibleInTable' => false, // no point, since it's a large text
                        'visibleInModal' => false, // would make the modal too big
                        'visibleInExport' => false, // not important enough
                        'visibleInShow' => false, // sure, why not
                        'prefix' => "% ",
                    ]
                );
                $this->crud->addColumn(
                    [
                        'name' => 'percentual_13',
                        'label' => 'Percentual 13', // Table column heading
                        'orderable' => false,
                        'visibleInTable' => false, // no point, since it's a large text
                        'visibleInModal' => false, // would make the modal too big
                        'visibleInExport' => false, // not important enough
                        'visibleInShow' => false, // sure, why not
                        'prefix' => "% ",
                    ]
                );
            }
        } else {
            $this->crud->addColumn(
                [
                    'name' => 'getTipoContaVinculada',
                    'label' => 'Norma', // Table column heading
                    'type' => 'model_function',
                    'function_name' => 'getTipoContaVinculada', // the method in your Model
                    'orderable' => true,
                    'visibleInTable' => true, // no point, since it's a large text
                    'visibleInModal' => true, // would make the modal too big
                    'visibleInExport' => true, // not important enough
                    'visibleInShow' => true, // sure, why not
                ]
            );
            $this->crud->addColumn(
                [
                    'name' => 'banco',
                    'label' => 'Banco',
                    'type' => 'text',
                    'orderable' => true,
                    'visibleInTable' => true, // no point, since it's a large text
                    'visibleInModal' => true, // would make the modal too big
                    'visibleInExport' => true, // not important enough
                    'visibleInShow' => true, // sure, why not
                ]
            );
            $this->crud->addColumn(
                [
                    'name' => 'agencia',
                    'label' => 'Agência',
                    'type' => 'text',
                    'orderable' => true,
                    'visibleInTable' => true, // no point, since it's a large text
                    'visibleInModal' => true, // would make the modal too big
                    'visibleInExport' => true, // not important enough
                    'visibleInShow' => true, // sure, why not
                ]
            );
            $this->crud->addColumn(
                [
                    'name' => 'conta_corrente',
                    'label' => 'Conta Corrente',
                    'type' => 'text',
                    'orderable' => true,
                    'visibleInTable' => true, // no point, since it's a large text
                    'visibleInModal' => true, // would make the modal too big
                    'visibleInExport' => true, // not important enough
                    'visibleInShow' => true, // sure, why not
                ]
            );
            $this->crud->addColumn(
                [
                    'name' => 'percentual_grupo_a_13_ferias',
                    'label' => 'Incidência do Grupo A',
                    'type' => 'number',
                    'decimals' => 2,
                    'orderable' => true,
                    'visibleInTable' => true, // no point, since it's a large text
                    'visibleInModal' => true, // would make the modal too big
                    'visibleInExport' => true, // not important enough
                    'visibleInShow' => true, // sure, why not
                    'prefix' => "% ",
                ]
            );
            $this->crud->addColumn(
                [
                    'name' => 'percentual_submodulo22',
                    'label' => 'Grupo A',
                    'type' => 'number',
                    'decimals' => 2,
                    'orderable' => true,
                    'visibleInTable' => true, // no point, since it's a large text
                    'visibleInModal' => true, // would make the modal too big
                    'visibleInExport' => true, // not important enough
                    'visibleInShow' => true, // sure, why not
                    'prefix' => "% ",
                ]
            );
            $this->crud->addColumn(
                [
                    'name' => 'getSaldoContratoContaParaColunas',
                    'label' => 'Saldo da Conta', // Table column heading
                    'type' => 'model_function',
                    'function_name' => 'getSaldoContratoContaParaColunas', // the method in your Model
                    'orderable' => true,
                    'visibleInTable' => true, // no point, since it's a large text
                    'visibleInModal' => true, // would make the modal too big
                    'visibleInExport' => true, // not important enough
                    'visibleInShow' => true, // sure, why not
                    'prefix' => "R$ ",
                ]
            );
            $this->crud->addColumn(
                [
                    'name' => 'saldo_encerramento',
                    'label' => 'Saldo Encerramento', // Table column heading
                    'orderable' => false,
                    'visibleInTable' => false, // no point, since it's a large text
                    'visibleInModal' => true, // would make the modal too big
                    'visibleInExport' => true, // not important enough
                    'visibleInShow' => true, // sure, why not
                    'prefix' => "R$ ",
                ]
            );
            $this->crud->addColumn(
                [
                    'name' => 'getStatusDaContaParaMostrarNaColuna',
                    'label' => 'Status da Conta', // Table column heading
                    'type' => 'model_function',
                    'function_name' => 'getStatusDaContaParaMostrarNaColuna', // the method in your Model
                    'orderable' => true,
                    'visibleInTable' => true, // no point, since it's a large text
                    'visibleInModal' => true, // would make the modal too big
                    'visibleInExport' => true, // not important enough
                    'visibleInShow' => true, // sure, why not
                ]
            );
        }

    }
}
