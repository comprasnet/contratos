<?php

namespace App\Http\Controllers\Gescon;

use App\Http\Requests\ContratoLocalExecucaoRequest as UpdateRequest;
use App\Http\Requests\ContratoLocalExecucaoRequest as StoreRequest;
use App\Http\Traits\PermissaoTrait;
use App\Models\Contrato;
use App\Models\ContratoLocalExecucao;
use App\Models\Municipio;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\CrudPanel;
use Illuminate\Http\Request;
use Illuminate\Session\Store;
use Illuminate\Support\Facades\DB;

// VALIDATION: change the requests to match your own file names if you need form validation

/**
 * Class ContratoLocalExecucaoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ContratoLocalExecucaoCrudController extends CrudController
{
    use PermissaoTrait;
    protected $tab = '';

    public function setup()
    {
        [$contrato_id, $contrato] = $this->verificarPermissaoContrato();

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Enderecos');
        $this->crud->setRoute(config('backpack.base.route_prefix') . "/gescon/contrato/$contrato_id/localexecucao");
        $this->crud->setEntityNameStrings('Local de Execução', 'Local de Execução');

        $this->crud->enableExportButtons();
        $this->crud->addClause('join', 'contrato_local_execucao', 'contrato_local_execucao.endereco_id', '=', 'enderecos.id');
        $this->crud->addClause('join', 'contratos', 'contratos.id', '=', 'contrato_local_execucao.contrato_id');
        $this->crud->addClause('join', 'unidades', 'unidades.id', '=', 'contratos.unidade_id');
        // $this->crud->addClause('join', 'municipios', 'municipios.id', '=', 'enderecos.municipios_id');
        $this->crud->addClause('where', 'contrato_id', '=', $contrato_id);
        $this->crud->addClause('where', 'unidades.id', '=', session()->get('user_ug_id'));
        $this->crud->addClause('select', ['contrato_local_execucao.descricao', 'enderecos.*']);
        // dd($teste->toSql());


        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        $this->crud->addButtonFromView('top', 'voltar', 'voltarcontrato', 'end');

        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');
        $this->crud->allowAccess('show');

        (backpack_user()->can('contrato_inserir')) ? $this->crud->allowAccess('create') : null;
        (backpack_user()->can('contrato_deletar')) ? $this->crud->allowAccess('delete') : null;
        (backpack_user()->can('contrato_editar')) ? $this->crud->allowAccess('update') : null;

        $this->crud->addColumns($this->Colunas());

        // add asterisk for fields that are required in ContratoLocalExecucaoRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        $this->adicionaCampos();
    }

    protected function adicionaCampos()
    {
        $this->tab = 'Localização';

        $this->adicionarCampoDescricao();
        $this->adicionarCampoCep();
        $this->adicionarCampoLogradouroNome();
        $this->adicionarCampoBairro();
        $this->adicionarCampoLocalidadeUf();
        $this->adicionarCampoCheckBoxNumero();
        $this->adicionarCampoNumero();
        $this->adicionarCampoComplemento();
        $this->adicionarCampoContratoId();
        $this->adicionarCampoMunicipio();
    }

    protected function adicionarCampoDescricao()
    {
        $this->crud->addField([
            'label' => 'Descrição',
            'type' => 'textarea',
            'name' => 'descricao',
            'attributes' => [
                'class' => 'form-control primeiraLetraMaiucula'
            ],
            'tab' => $this->tab,
        ]);
    }

    protected function adicionarCampoCep()
    {
        $this->crud->addField([
            'label' => 'CEP',
            'type' => 'cep',
            'name' => 'cep',
            'attributes' => [
                'maxlength' => '8',
            ],
//            'wrapperAttributes' => [
//                'class' => 'form-group col-md-4'
//            ],
            'tab' => $this->tab,
        ]);
    }

    protected function adicionarCampoLogradouroNome()
    {
        $this->crud->addField([
            'label' => 'Logradouro/Nome',
            'type' => 'text',
            'name' => 'logradouro',
            'attributes' => ['readonly' => 'readonly', 'class' => 'form-control primeiraLetraMaiucula'],
            'tab' => $this->tab,
        ]);
    }

    protected function adicionarCampoBairro()
    {
        $this->crud->addField([
            'label' => 'Bairro/Distrito',
            'type' => 'text',
            'name' => 'bairro',
            'attributes' => ['readonly' => 'readonly', 'class' => 'form-control primeiraLetraMaiucula'],
            'tab' => $this->tab,
        ]);
    }

    protected function adicionarCampoLocalidadeUf()
    {
        $this->crud->addField([
            'label' => "Localidade/UF", // Table column heading
            'type' => "select2_from_ajax",
            'name' => 'municipios_id', // the column that contains the ID of that connected entity
            'entity' => 'municipio', // the method that defines the relationship in your Model
            'attribute' => "nome", // foreign key attribute that is shown to the user
            'attribute2' => "estadoSigla", // foreign key attribute that is shown to the user
            'model' => "App\Models\Municipio", // foreign key model
            'data_source' => url("api/busca/municipio"), // url to controller search function (with /{id} should return model)
            'placeholder' => "Selecione uma cidade", // placeholder for the select
            'minimum_input_length' => 0, // minimum characters to type before querying results
            'attributes' => ['disabled' => 'disabled'],
            'allows_null' => false,
            'tab' => $this->tab,
        ]);
    }

    protected function adicionarCampoComplemento()
    {
        $this->crud->addField([
            'label' => 'Complemento',
            'type' => 'text',
            'name' => 'complemento',
            'attributes' => ['class' => 'form-control primeiraLetraMaiucula'],
            'tab' => $this->tab,
        ]);
    }

    protected function adicionarCampoCheckBoxNumero()
    {
        $this->crud->addField([
            'name' => 'radio_numero',
            'label' => "Possui Número",
            'type' => 'radioContratoLocalExecucao',
            'options' => [1 => 'Sim', 0 => 'Não'],
            'allows_null' => false,
            'inline' => true,
            'default' => 1,
            'wrapperAttributes' => [
                'id' => 'radio_numero'
            ],
            'tab' => $this->tab,
        ]);
    }

    protected function adicionarCampoNumero()
    {
        $this->crud->addField([
            'name' => 'numero',
            'label' => "Número",
            'type' => 'text',
            'tab' => $this->tab,
        ]);
    }

    protected function adicionarCampoContratoId()
    {
        $contrato_id = \Route::current()->parameter('contrato_id');
        $this->crud->addField([
            'name' => 'contrato_id',
            'label' => "contrato_id",
            'type' => 'hidden',
            'value' => $contrato_id,
//            'wrapperAttributes' => [
//                'class' => 'form-group col-md-6'
//            ],
            'tab' => $this->tab,
        ]);
    }

    protected function adicionarCampoMunicipio()
    {
        $this->crud->modifyField('codMunicipio', [
            'name' => 'municipios_id',
            'label' => 'codMunicipio',
            'attribute' => "municipios_id",
            'entity' => 'municipio',
            'type' => 'hidden',
            'tab' => $this->tab,
        ]);
    }

    public function Colunas()
    {
        return [

            [
                'name' => 'descricao',
                'label' => 'Local de Execução', // Table column heading
                'type' => '',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'label' => 'CEP',
                'type' => 'text',
                'name' => 'cep',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'logradouro',
                'label' => 'Logradouro/Nome', // Table column heading
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'localidade_uf',
                'label' => 'Cidade/UF', // Table column heading
                'type' => 'text',
                //'function_name' => 'getNomeCidadeEMunicipioAttribute',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'bairro',
                'label' => 'Bairro/Distrito', // Table column heading
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    // $query->orWhere('contratolocalexecucao.bairro', 'like', "%" . strtoupper($searchTerm) . "%");
                },
            ],
            [
                'name' => 'complemento',
                'label' => 'Complemento', // Table column heading
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    // $query->orWhere('contratolocalexecucao.nome', 'like', "%" . strtoupper($searchTerm) . "%");
                },
            ],
        ];

    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        $id_endereco = $this->crud->entry->id;
        try {
            DB::beginTransaction();
            ContratoLocalExecucao::create([
                'contrato_id' => $request->input('contrato_id'),
                'endereco_id' => $id_endereco,
                'descricao' => $request['descricao']
            ]);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
        }

        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        try {
            DB::beginTransaction();
            $localExecucao = ContratoLocalExecucao::where('contrato_id', $request->input('contrato_id'))
                ->where('endereco_id', $request->input('id'))
                ->first();

            $localExecucao->descricao = $request->input('descricao');
            $localExecucao->save();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            \Alert::success('Não foi possível atualizar o registro.')->flash();
            return redirect("/gescon/contrato/".$request->input('contrato_id')."/localexecucao");

        }

        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry


        return $redirect_location;
    }

    public function show($id)
    {
        $content = parent::show($id);

        $this->crud->removeColumn('contrato_id');

        return $content;
    }

    public function destroy($id)
    {
        $this->crud->hasAccessOrFail('delete');
        $this->crud->setOperation('delete');

        $contrato_id = $id;
        $endereco_id = $this->crud->getCurrentEntryId();
        try {
            DB::beginTransaction();
            ContratoLocalExecucao::where('contrato_id', $contrato_id)
                ->where('endereco_id', $endereco_id)->delete();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            \Alert::success('Não foi possível atualizar o registro.')->flash();
        }


        $content = parent::destroy($id);

        $this->crud->removeColumn('contrato_id');

        return $content;
    }
}
