<?php

namespace App\Http\Controllers\Gescon;

use App\Http\Traits\CamposComunsTrait;
use App\Http\Traits\ConsultaCompra;
use App\Http\Traits\ModalBackpackTrait;
use App\Http\Traits\PermissaoTrait;
use App\Jobs\AtualizaFornecedorSicafJob;
use App\Models\Catmatseritem;
use App\Models\Codigoitem;
use App\Models\Contrato;
use App\Models\Contratoempenho;
use App\Models\Contratohistorico;
use App\Models\ContratoHistoricoMinutaEmpenho;
use App\Models\Contratoitem;
use App\Models\ContratoMinutaEmpenho;
use App\Models\Empenho;
use App\Models\Fornecedor;
use App\Models\MinutaEmpenho;
use App\Models\Saldohistoricoitem;
use App\Models\Autoridadesignataria;
use App\Models\ContratoAutoridadeSignataria;
use App\services\AmparoLegalService;
use App\services\CompraService;
use App\services\ContratoService;
use App\Models\ContratoFaturaEmpenho;
use App\services\EmpenhoService;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\InstrumentoinicialRequest as StoreRequest;
use App\Http\Requests\InstrumentoinicialRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Log;
use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\CompraTrait;
use App\Models\AmparoLegal;
use App\Models\Unidade;
use App\XML\ApiSiasg;
use Illuminate\Support\Facades\Input;
use Route;

/**
 * Class InstrumentoinicialCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class InstrumentoinicialCrudController extends CrudController
{
    use PermissaoTrait;
    use BuscaCodigoItens;
    use CompraTrait;
    use CamposComunsTrait;
    use ModalBackpackTrait;
    use ConsultaCompra;

    private $compraService;
    private $contratoService;

    protected $amparoLegalService;
    protected $empenhoService;
    public function __construct(
        ContratoService $contratoService,
        CompraService $compraService,
        AmparoLegalService $amparoLegalService,
        EmpenhoService $empenhoService
    )
    {
        parent::__construct();
        $this->compraService = $compraService;
        $this->contratoService = $contratoService;
        $this->amparoLegalService = $amparoLegalService;
        $this->empenhoService = $empenhoService;
    }

    public function setup()
    {
        [$contrato_id, $contrato] = $this->verificarPermissaoContrato();

        $instrumentoinicial_id = Route::current()->parameter('instrumentoinicial');


        $tps = Codigoitem::whereHas('codigo', function ($query) {
            $query->where('descricao', '=', 'Tipo de Contrato');
        })
            ->where('descricao', '=', 'Termo Aditivo')
            ->orWhere('descricao', '=', 'Termo de Apostilamento')
            ->orWhere('descricao', '=', 'Termo de Rescisão')
            ->orWhere('descricao', '=', 'Termo de Encerramento') #731
            ->pluck('id')
            ->toArray();

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Contratohistorico');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/gescon/contrato/' . $contrato_id . '/instrumentoinicial');
        $this->crud->setEntityNameStrings('Instrumento Inicial', 'Instrumento Inicial');
        $this->crud->setCreateContentClass('col-md-12');
        $this->crud->setEditContentClass('col-md-12');
        $this->crud->setEditView('vendor.backpack.crud.contrato.instrumentoinicial.edit');
        $this->crud->addClause('join', 'fornecedores', 'fornecedores.id', '=', 'contratohistorico.fornecedor_id');
        $this->crud->addClause('join', 'unidades', 'unidades.id', '=', 'contratohistorico.unidade_id');
        $this->crud->addClause('where', 'unidade_id', '=', session()->get('user_ug_id'));
        $this->crud->addClause('select', 'contratohistorico.*');
        $this->crud->addClause('where', 'contrato_id', '=', $contrato_id);
        $this->crud->addClause('where', 'contratohistorico.elaboracao', '=', false);
        foreach ($tps as $t) {
            $this->crud->addClause('where', 'tipo_id', '<>', $t);
        }

        $this->crud->addButtonFromView('top', 'voltar', 'voltarcontrato', 'end');
        $this->crud->enableExportButtons();
        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');
        $this->crud->allowAccess('show');

        (backpack_user()->can('contrato_editar')) ? $this->crud->allowAccess('update') : null;

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $colunas = $this->Colunas();
        $this->crud->addColumns($colunas);

        $unidade = [session()->get('user_ug_id') => session()->get('user_ug')];

        $categorias = Codigoitem::whereHas('codigo', function ($query) {
            $query->where('descricao', '=', 'Categoria Contrato');
        })->where('descricao', '<>', 'A definir')->orderBy('descricao')->pluck('descricao', 'id')->toArray();

        $modalidades = Codigoitem::whereHas('codigo', function ($query) {
            $query->where('descricao', '=', 'Modalidade Licitação');
        })->where('visivel', true)->orderBy('descricao')->pluck('descricao', 'id')->toArray();

        $tipos = Codigoitem::whereHas('codigo', function ($query) {
            $query->where('descricao', '=', 'Tipo de Contrato');
        })
            ->whereNotIn('descricao', config('app.tipo_contrato_not_in'))
            ->orderBy('descricao')
            ->pluck('descricao', 'id')
            ->toArray();

        $campos = $this->Campos(
            $unidade,
            $categorias,
            $modalidades,
            $tipos,
            $contrato_id,
            $instrumentoinicial_id,
            $contrato->tipo->descricao
        );
        $this->crud->addFields($campos);

        $this->adicionaCampoItensCompra();
        // inicializa crud->modal_fields com os campos modal_fields para aproveitar os campos do backpack nos modais
        // precisa ser depois de todos os campos adicionados com addModalField
        $this->crud->modal_fields = $this->modal_fields;

        // add asterisk for fields that are required in InstrumentoinicialRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function Colunas()
    {
        $colunas = [
            [
                'name' => 'getReceitaDespesaHistorico',
                'label' => 'Receita / Despesa',
                'type' => 'model_function',
                'function_name' => 'getReceitaDespesaHistorico',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'observacao',
                'label' => 'Observação',
                'type' => 'text',
                'limit' => 100,
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'numero',
                'label' => 'Número do instrumento',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'getUnidadeOrigemHistorico',
                'label' => 'Unidade Gestora Origem do Contrato',
                'type' => 'model_function',
                'function_name' => 'getUnidadeOrigemHistorico',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'getUnidadeHistorico',
                'label' => 'Unidade Gestora Atual',
                'type' => 'model_function',
                'function_name' => 'getUnidadeHistorico',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'unidades_requisitantes',
                'label' => 'Unidades Requisitantes',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'getTipoHistorico',
                'label' => 'Tipo',
                'type' => 'model_function',
                'function_name' => 'getTipoHistorico',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'getCategoriaHistorico',
                'label' => 'Categoria',
                'type' => 'model_function',
                'function_name' => 'getCategoriaHistorico',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'getSubCategoriaHistorico',
                'label' => 'Subcategoria',
                'type' => 'model_function',
                'function_name' => 'getSubCategoriaHistorico',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'getFornecedorHistorico',
                'label' => 'Fornecedor',
                'type' => 'model_function',
                'function_name' => 'getFornecedorHistorico',
                'orderable' => true,
                'limit' => 100,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'processo',
                'label' => 'Processo',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'objeto',
                'label' => 'Objeto',
                'type' => 'text',
                'limit' => 100,
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'info_complementar',
                'label' => 'Informações Complementares',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'data_assinatura',
                'label' => 'Data Assinatura',
                'type' => 'date',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'data_publicacao',
                'label' => 'Data Publicação',
                'type' => 'date',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'vigencia_inicio',
                'label' => 'Vig. Início',
                'type' => 'date',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'vigencia_fim',
                'label' => 'Vig. Fim',
                'type' => 'model_function',
                // retorna string 'indeterminado se o contrato for prazo indeterminado
                'function_name' => 'getVigenciaFimPrazoIndeterminado',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true
            ],
            [
                'name' => 'formatVlrGlobalHistorico',
                'label' => 'Valor Global',
                'type' => 'model_function',
                'function_name' => 'formatVlrGlobalHistorico',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'num_parcelas',
                'label' => 'Núm. Parcelas',
                'type' => 'model_function',
                //retorna string 'indeterminado se o contrato for prazo indeterminado
                'function_name' => 'getNumeroParcelasPrazoIndeterminado',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true
            ],
            [
                'name' => 'formatVlrParcelaHistorico',
                'label' => 'Valor Parcela',
                'type' => 'model_function',
                'function_name' => 'formatVlrParcelaHistorico',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'situacao',
                'label' => 'Situação',
                'type' => 'boolean',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'options' => [0 => 'Inativo', 1 => 'Ativo']
            ],
        ];

        return $colunas;
    }

    public function getAutoridadesSignatarias($unidade_id)
    {
        return Autoridadesignataria::where('unidade_id', $unidade_id)->where('ativo', true)
            ->pluck('autoridade_signataria', 'id')->toArray();
    }

    public function getContratoAutoridadeSignataria($contrato_id)
    {
        return ContratoAutoridadeSignataria::where('contrato_id', $contrato_id)->get();
    }

    public function Campos($unidade, $categorias, $modalidades, $tipos, $contrato_id, $instrumentoinicial_id, $contrato_tipo_descricao)
    {
        $idMaterial = $this->retornaIdCodigoItem('Tipo CATMAT e CATSER', 'Material');
        $idServico = $this->retornaIdCodigoItem('Tipo CATMAT e CATSER', 'Serviço');

        $objContrato = Contrato::where('id', $contrato_id)->first();
        $unidade_id = $objContrato->unidade_id;
        // Autoridades Signatárias
        $this->getAutoridadesSignatarias($unidade_id);

        // Autoridades que já estão salvas no contrato, independente de terem sido inativadas
        $arrayContratoAutoridadeSignataria = $this->getContratoAutoridadeSignataria($contrato_id);
        $dadosContratoAutoridadeSignataria = null;
        foreach ($arrayContratoAutoridadeSignataria as $contratoAutoridadeSignataria) {
            $nomeContratoAutoridadeSignataria = $contratoAutoridadeSignataria->nome_autoridade_signataria;
            $cargoContratoAutoridadeSignataria = $contratoAutoridadeSignataria->cargo_autoridade_signataria;
            $dadosContratoAutoridadeSignataria .= ' [ ' . $nomeContratoAutoridadeSignataria . ' - ' . $cargoContratoAutoridadeSignataria . ' ] , ';
        }

        $abaDados = 'Dados Contrato';
        $abaCaracteristicas = 'Características do contrato';
        $campos = [
            [   // Hidden
                'name' => 'contrato_id',
                'type' => 'hidden',
                'default' => $contrato_id,
            ],
            [   // Hidden
                'name' => 'tipo_contrato',
                'type' => 'hidden',
                'default' => $contrato_tipo_descricao,
                'attributes' => [
                    'id' => 'tipo_contrato'
                ]
            ],
            [   // Hidden
                'name' => 'instrumentoinicial_id',
                'type' => 'hidden',
                'default' => $instrumentoinicial_id,
            ],
            $this->contratoService->retornaCampoFornecedor($abaDados),
            $this->contratoService->retornaCampoMinutasEmpenho($abaDados),
            $this->contratoService->retornaCampoSubcontratados($abaDados),
            $this->contratoService->retornaCampoAssinatura($abaDados),
            // Campo não editável para mostrar as Autoridades Signatárias que já estão cadastradas para este contrato.
            [
                'name' => 'Autoridades Signatárias salvas no Contrato',
                'label' => 'Autoridades Signatárias salvas no Contrato',
                'type' => 'textarea',
                'default' => $dadosContratoAutoridadeSignataria,
                'attributes' => [
                    'disabled' => 'disabled',
                ],
                'tab' => 'Dados Contrato',
            ],
            [
                'name' => 'st_alterar_autoridades_signatarias',
                'label' => "Alterar Autoridades Signatárias?",
                'type' => 'radio',
                'options' => [1 => 'Sim', 0 => 'Não'],
                'default' => 0,
                'inline' => true,
                'tab' => 'Dados Contrato',
            ],
            $this->contratoService->retornaCampoAutoridades($unidade_id, $abaDados, true),
            $this->contratoService->retornaCampoDataPublicacao($abaDados),
            $this->contratoService->retornaCampoObjeto($abaDados),
            $this->contratoService->retornaCampoInfoComplementar($abaDados),
            $this->contratoService->retornaCampoModalidade($modalidades, $abaDados),
            $this->contratoService->retornaCampoAmparos($abaDados, true),
            $this->contratoService->retornaCampoContrataMaisBrasil($abaDados),
            $this->contratoService->retornaCampoContratacaoPNCP($abaDados),
            $this->contratoService->retornaCampoNumeroLicitacao($abaDados),
            $this->contratoService->retornaCampoUnidadeCompra($abaDados, true),
            $this->contratoService->retornaCampoUnidadeBeneficiaria($abaDados, true),
            $this->contratoService->retornaCampoReceitaDespesa($abaCaracteristicas),
            $this->contratoService->retornaCampoTipo($abaCaracteristicas),
            $this->contratoService->retornaCampoCronograma($abaCaracteristicas),
            $this->contratoService->retornaCampoSubTipo($abaCaracteristicas),
            $this->contratoService->retornaCampoCategoria($abaCaracteristicas),
            $this->contratoService->retornaCampoAplicavelDecreto($abaCaracteristicas),
            $this->contratoService->retornaCampoSubCategoria($abaCaracteristicas),
            $this->contratoService->retornaCampoNumeroContrato($abaCaracteristicas),
            $this->contratoService->retornaCampoCodigoSistemaExterno($abaCaracteristicas),
            $this->contratoService->retornaCampoProcesso($abaCaracteristicas),
            [
                'label' => "Unidade Gestora Origem do Contrato",
                'type' => "select2_from_ajax_single_unidade_gestora_origem",
                'name' => 'unidadeorigem_id',
                'entity' => 'unidadeorigem',
                'attribute' => "codigo",
                'attribute2' => "nomeresumido",
                'process_results_template' => 'gescon.process_results_unidade',
                'model' => "App\Models\Unidade",
                'data_source' => url("api/unidade"),
                'placeholder' => "Selecione a Unidade",
                'minimum_input_length' => 2,
                'tab' => 'Características do contrato',
                'attributes' =>['readonly' => 'readonly'],
            ],
            [
                'name' => 'unidade_id',
                'label' => "Unidade Gestora Atual",
                'type' => 'select2_from_array_unidade_gestora',
                'options' => $unidade,
                'allows_null' => false,
                'tab' => 'Características do contrato',
            ],
            [
                'name' => 'unidades_requisitantes',
                'label' => 'Unidades Requisitantes',
                'type' => 'text',
                'tab' => 'Características do contrato',
            ],
            [
                'name' => 'prorrogavel',
                'label' => 'Prorrogável',
                'type' => 'select_from_array',
                'options' => [
                    'n' => 'Não',
                    's' => 'Sim'
                ],
                'value' =>  $objContrato->prorrogavel === true ? 's' : 'n',
                'allows_null' => false,
                'tab' => 'Características do contrato'
            ],

            [
                'name' => 'is_prazo_indefinido',
                'label' => "Prazo indeterminado?",
                'type' => 'radio',
                'options' => [1 => 'Sim', 0 => 'Não'],
                'allows_null' => false,
                'allows_multiple' => false, // OPTIONAL; needs you to cast this to array in your model;
                'inline' => true,
                'default' => $objContrato->is_prazo_indefinido,
                'wrapperAttributes' => [
                    'class' => 'form-group col-xs-12 com-sem-valor'
                ],
                'tab' => 'Características do contrato'
            ],

            [ // select_from_array
                'name' => 'situacao',
                'label' => "Situação",
                'type' => 'select_from_array',
                'options' => [1 => 'Ativo', 0 => 'Inativo'],
                'allows_null' => false,
                'tab' => 'Características do contrato',
            ],
            [
                'name' => 'itens',
                'type' => 'itens_contrato_instrumento_inicial_list',
                'tab' => 'Itens do contrato',
                'material' => $idMaterial,
                'servico' => $idServico,
            ],
            [
                'label' => "adicionaCampoRecuperaGridItens",
                'type' => "hidden",
                'name' => 'adicionaCampoRecuperaGridItens',
                'default' => "{{old('name')}}",
                'tab' => 'Itens do contrato'
            ],
            [
                'label' => "Unidade Emitente do Empenho",
                'type' => "select2_from_ajax_multiple_alias",
                'name' => 'unidadesdescentralizadas',
                'entity' => 'unidade',
                'attribute' => "codigo",
                'attribute2' => "nomeresumido",
                'process_results_template' => 'gescon.process_results_unidade',
                'model' => "App\Models\Unidade",
                'data_source' => url("api/unidade"),
                'placeholder' => "Selecione a Unidade",
                'minimum_input_length' => 2,
                'tab' => 'Empenhos',
            ],
            [
                'label' => "Fornecedor do Empenho",
                'type' => "select2_from_ajax_multiple_alias",
                'name' => 'fornecedor_empenho',
                'entity' => 'fornecedor',
                'attribute' => "cpf_cnpj_idgener",
                'attribute2' => "nome",
                'process_results_template' => 'gescon.process_results_fornecedor',
                'model' => "App\Models\Fornecedor",
                'data_source' => url("api/fornecedor"),
                'placeholder' => "Selecione o fornecedor",
                'minimum_input_length' => 2,
                'tab' => 'Empenhos'
            ],
            [
                'label' => "Empenho &nbsp;<button type='button' id='buscarEmpenho' class='btn btn-sm btn-default ng-scope'>
                        <i class='fa fa-plus'></i> Adicionar</button>",
                'name' => 'search_empenho_field',
                'type' => 'text',
                'tab' => 'Empenhos',
            ],
            [   // Date
                'name' => 'data_proposta_comercial',
                'label' => 'Data da proposta',
                'type' => 'date',
                'tab' => 'Vigência / Valores',
            ],
            [   // Date
                'name' => 'vigencia_inicio',
                'label' => 'Data Vig. Início',
                'type' => 'date',
                'tab' => 'Vigência / Valores',
            ],
            [   // Date
                'name' => 'vigencia_fim',
                'label' => 'Data Vig. Fim',
                'type' => 'date',
                'tab' => 'Vigência / Valores',
            ],
            [   // Number
                'name' => 'valor_global',
                'label' => 'Valor Global',
                'type' => 'number',
                // optionals
                'attributes' => [
                    'id' => 'valor_global',
                    'step' => '0.0001',
                    'readOnly' => 'readOnly'
                ], // allow decimals
                'prefix' => "R$",
                'tab' => 'Vigência / Valores',
            ],
            [   // Number
                'name' => 'num_parcelas',
                'label' => 'Núm. Parcelas',
                'type' => 'number',
                // optionals
                'attributes' => [
                    "step" => "any",
                    "min" => '1',
                ], // allow decimals
                'tab' => 'Vigência / Valores',
            ],
            [   // Number
                'name' => 'valor_parcela',
                'label' => 'Valor Parcela',
                'type' => 'number',
                // optionals
                'attributes' => [
                    'id' => 'valor_parcela',
                    'step' => '0.0001',
                    'readOnly' => 'readOnly'
                ], // allow decimals
                'prefix' => "R$",
                'tab' => 'Vigência / Valores',
            ],
        ];

        return $campos;
    }

    //705
    //método não mais utilizado, nome alterado pois senão dá conflito com o da trait ConsultaCompra
    public function consultaCompraSiasg_antigo($objParams)
    {
        $modalidade = Codigoitem::find($objParams->modalidade_id);
        $uasgCompra = Unidade::find($objParams->unidadecompra_id);
        $numero_ano = explode('/', $objParams->licitacao_numero);

        $apiSiasg = new ApiSiasg();

        $params = $this->montarInformacoesConsultaCompraSISPP($modalidade->descres,$numero_ano[0] . $numero_ano[1],$uasgCompra->codigo);

        $compra = json_decode($apiSiasg->executaConsulta('COMPRASISPP', $params));

        return $compra;
    }

    public function store(StoreRequest $request)
    {
        $request->request->remove('unidadesdescentralizadas');
        session()->forget('editando_contrato_elaboracao');
        $valor_parcela = $request->input('valor_parcela');
        $request->request->set('valor_parcela', $valor_parcela);

        $valor_global = $request->input('valor_global');
        $request->request->set('valor_global', $valor_global);
        $request->request->set('valor_inicial', $valor_global);

        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);

        return $redirect_location;
    }

    // Método que atualiza as autoridades signatárias do contrato
    public function atualizarAutoridadesSignatariasDoContrato($request, $arrayContratoAutoridadeSignatariaSalvos)
    {
        $contrato_id = $request->input('contrato_id');
        $contratohistorico_id = $request->input('id');
        // vamos verificar se o usuário informou que deseja alterar as autoridades
        $stAlterarAutoridadesSignatarias = $request->input('st_alterar_autoridades_signatarias');
        if ($stAlterarAutoridadesSignatarias == 1) {
            $idContrato = $request->input('contrato_id');
            $arrayAutoridadesSignatariasSalvar = $request->contrato_autoridade_signataria;
            $contratohistorico_id = $request->id;
            $arrayContratoAutoridadeSignataria = ContratoAutoridadeSignataria::where('contratohistorico_id', $contratohistorico_id)->delete();
            $arrayContratoAutoridadeSignataria = ContratoAutoridadeSignataria::where('contrato_id', $contrato_id)->delete();
            $arrayContratoAutoridadeSignataria = ContratoAutoridadeSignataria::where('created_at', null)
                ->where('updated_at', null)
                ->where('nome_autoridade_signataria', null)
                ->delete();
            if (!is_null($arrayAutoridadesSignatariasSalvar)) {
                // aqui quer dizer que o que tinha pra ser limpo, já foi. Vamos salvar os dados
                foreach ($arrayAutoridadesSignatariasSalvar as $idAutoridadeSignataria) {
                    // vamos buscar os dados da autoridade signatária, para salvarmos em contrato autoridade signataria.
                    $objAutoridadeSignatariaSalvar = Autoridadesignataria::find($idAutoridadeSignataria);
                    $nomeAutoridadeSignatariaSalvar = $objAutoridadeSignatariaSalvar->autoridade_signataria;
                    $cargoAutoridadeSignatariaSalvar = $objAutoridadeSignatariaSalvar->cargo_autoridade_signataria;
                    // para cada autoridade, vamos salvar um registro
                    $objContratoAutoridadeSignataria = new ContratoAutoridadeSignataria();
                    $objContratoAutoridadeSignataria->contratohistorico_id = $contratohistorico_id;
                    $objContratoAutoridadeSignataria->autoridadesignataria_id = $idAutoridadeSignataria;
                    $objContratoAutoridadeSignataria->contrato_id = $idContrato;
                    $objContratoAutoridadeSignataria->nome_autoridade_signataria = $nomeAutoridadeSignatariaSalvar;
                    $objContratoAutoridadeSignataria->cargo_autoridade_signataria = $cargoAutoridadeSignatariaSalvar;
                    if (!$objContratoAutoridadeSignataria->save()) {
                        return false;
                    }
                }
            }
        } else {
            // Aqui o usuário informou que não deseja realizar alteração.
            // Como o sistema está limpando os registros de contrato autoridade signatária e registrando os selecionados (acho que pelo relacionamento)
            // , precisaremos contornar essa situação.
            // vamos limpar tudo desse histórico e salvar o que já está salvo.
            $arrayContratoAutoridadeSignatariaExcluir = ContratoAutoridadeSignataria::where('contratohistorico_id', $contratohistorico_id)->delete();
            foreach ($arrayContratoAutoridadeSignatariaSalvos as $dadosContratoAutoridadeSignatariaSalvar) {
                $objContratoAutoridadeSignatariaSalvar = new ContratoAutoridadeSignataria();
                $objContratoAutoridadeSignatariaSalvar->contratohistorico_id = $dadosContratoAutoridadeSignatariaSalvar->contratohistorico_id;
                $objContratoAutoridadeSignatariaSalvar->autoridadesignataria_id = $dadosContratoAutoridadeSignatariaSalvar->autoridadesignataria_id;
                $objContratoAutoridadeSignatariaSalvar->contrato_id = $dadosContratoAutoridadeSignatariaSalvar->contrato_id;
                $objContratoAutoridadeSignatariaSalvar->nome_autoridade_signataria = $dadosContratoAutoridadeSignatariaSalvar->nome_autoridade_signataria;
                $objContratoAutoridadeSignatariaSalvar->cargo_autoridade_signataria = $dadosContratoAutoridadeSignatariaSalvar->cargo_autoridade_signataria;
                $objContratoAutoridadeSignatariaSalvar->save();
            }
        }
        return true;
    }

    public function update(UpdateRequest $request)
    {
        if (!in_array($request->input('aplicavel_decreto'), ["0", "1"]))
            $request->request->set('aplicavel_decreto', false);

        $this->contratoService->setCampoProrrogavel($request);
        $this->contratoService->setCamposContratoComPrazoIndefinido($request);
        $this->contratoService->setCamposValores($request);
        $this->contratoService->setCamposComprasContrataMaisBr($request);

        $arrNumeroItemCompra = $request->input('numero_item_compra');
        if (!empty($arrNumeroItemCompra)) {
            $arrItens = array();
            foreach ($arrNumeroItemCompra as $k => $numeroItemCompra) {
                $key = $numeroItemCompra.$request->input('codigo_siasg')[$k];
                if (array_key_exists($key, $arrItens)) {
                    \Alert::error('Não é possível adicionar o mesmo número item da compra que possuam o mesmo código siasg no mesmo termo.')->flash();
                    return redirect()->back()->withInput();
                }
                $arrItens[$key] = ['numero_item_compra' => $numeroItemCompra, 'codigo_siasg' => $request->input('codigo_siasg')[$k]];
            }
        }

        $contrato_id = $request->input('contrato_id');
        // vamos recuperar tudo o que já está salvo em contrato autoridade signatária para este contrato
        $arrayContratoAutoridadeSignatariaSalvos = ContratoAutoridadeSignataria::where('contrato_id', $contrato_id)->get();
        $stAlterarAutoridadesSignatarias = $request->input('st_alterar_autoridades_signatarias');


        $modalidadeId = $request->input('modalidade_id');
        $validaItemNaoSeAplica = $this->validarIDCodigoItem($modalidadeId);

        $amparoslegais = $request->input('amparolegal');
        $validaLei14133 =
            $this->amparoLegalService->amparoLegalLei14133($amparoslegais);

        //todo: verificar se na linha abaixo é unidadecompra_id ou unidade_id
        $unidadeId = $request->input('unidadecompra_id');
        $validaNaoSisg = $this->validarUasgNaoSisg($unidadeId);

        try {
            DB::beginTransaction();

            $erro =  $this->contratoService->verificaValidacaoCompra(
                $validaItemNaoSeAplica,
                $validaLei14133,
                $validaNaoSisg,
                $request
            );

            if ($erro !== null) {
                return redirect()->back()->withInput(Input::all())->withErrors($erro);
            }

            AtualizaFornecedorSicafJob::dispatch(Fornecedor::where('id', $request->input('fornecedor_id'))->first(), $request->input('contrato_id'))->onQueue('pesquisa-fornecedor-sicaf-rfb');

            if (!empty($request->get('excluir_item'))) {
                $this->excluirSaldoHistoricoItem($request->get('excluir_item'));
            }


            // caso tinha ou tenha minuta, os itens são excluídos e cadastrados novamente conforme a grid recebida do front
            $arrMinutasContratoHistorico = ContratoHistoricoMinutaEmpenho::where('contrato_historico_id', $request->id)->get();
            if (!empty($request->minutasempenho) || count($arrMinutasContratoHistorico) > 0) {
                $arrItensSaldoHistoricoItens = Saldohistoricoitem::where('saldoable_id', $request->id)->pluck('id');
                $this->excluirSaldoHistoricoItem($arrItensSaldoHistoricoItens);
                $this->cadastrarItensAtualizadoSaldoHistorico($request->all());

                $oldIds = [];

                foreach ($arrMinutasContratoHistorico as $minutaContratoHistorico) {
                    $oldIds[] = $minutaContratoHistorico->minuta_empenho_id;
                }

                $minutaIdsRemovidas = empty($request->minutasempenho) ? $oldIds : array_diff($oldIds, $request->minutasempenho);

                foreach ($minutaIdsRemovidas as $minutaId) {
                    $findMinutaRemovida = MinutaEmpenho::where('id', $minutaId)->first();
                    $ugIdEmitenteMinuta = $findMinutaRemovida->saldo_contabil->unidade_id()->first()->id;
                    $minutaEmpenho = $this->encontraEmpenho($ugIdEmitenteMinuta, $minutaId);

                    if($minutaEmpenho != null) {
                        $contratoFaturaEmpenho = ContratoFaturaEmpenho::select('contratofaturas.id')
                            ->join('contratofaturas', 'contratofatura_empenhos.contratofatura_id', '=', 'contratofaturas.id')
                            ->where('contratofatura_empenhos.empenho_id', '=', $minutaEmpenho->empenho_id)
                            ->where('contratofaturas.contrato_id', '=', $request->input('contrato_id'))
                            ->pluck('contratofaturas.id')
                            ->toArray();

                        if (empty($contratoFaturaEmpenho)) {
                            Contratoempenho::where('contrato_id', $contrato_id)
                                ->where('empenho_id', $minutaEmpenho->empenho_id)
                                ->delete();
                        } else {
                            return redirect()->back()->withInput(Input::all())->withErrors(
                                [
                                    "minutasempenho.validarcompra"=>'Não é possível remover uma minuta de empenho vinculada a um instrumento de cobrança.',
                                ]
                            );
                        }
                    }else{
                        $minutaEmpenho = MinutaEmpenho::where('id', $minutaId)->first();
                        if($minutaEmpenho->situacao_id == 217) {
                            $empenho = Empenho::where('numero', $minutaEmpenho->mensagem_siafi)->first();
                            Contratoempenho::where('contrato_id', $contrato_id)
                                ->where('empenho_id', $empenho->id)
                                ->delete();
                        }
                    }
                }

                if (!empty($request->minutasempenho)) {
                    foreach ($request->minutasempenho as $minutaEmpenhoId) {
                        $minutaEmpenho = $this->encontraEmpenho($unidadeId, $minutaEmpenhoId);
                        if ($minutaEmpenho != null) {
                            Contratoempenho::updateOrCreate([
                                'contrato_id' => $contrato_id,
                                'empenho_id' => $minutaEmpenho->empenho_id,
                            ], [
                                'fornecedor_id' => $minutaEmpenho->fornecedor_id,
                                'unidadeempenho_id' => $minutaEmpenho->unidade_id,
                                'automatico' => true,
                            ]);
                        }else{
                            $minutaEmpenho = MinutaEmpenho::where('id', $minutaEmpenhoId)->first();
                            if($minutaEmpenho->situacao_id == 217) {
                                $empenho = Empenho::where('numero', $minutaEmpenho->mensagem_siafi)
                                    ->where('fornecedor_id', $minutaEmpenho->fornecedor_empenho_id)
                                    ->first();

                                Contratoempenho::updateOrCreate([
                                    'contrato_id' => $contrato_id,
                                    'empenho_id' => $empenho->id,
                                ], [
                                    'fornecedor_id' => $empenho->fornecedor_id,
                                    'unidadeempenho_id' => $empenho->unidade_id,
                                    'automatico' => true,
                                ]);
                            }
                        }
                    }
                }
            } else {
                // altera os itens do contrato
                if (!empty($request->get('qtd_item'))) {
                    $this->alterarItens($request->all());
                }
            }
            if(!isset($request->aplicavel_decreto) || $request->aplicavel_decreto == null){
                $request->merge(['aplicavel_decreto' => false]);
            }

            $this->contratoService->saveAbaEmpenhosContrato($contrato_id, $request, backpack_user()->id);

            // your additional operations before save here
            $redirect_location = parent::updateCrud($request);

            DB::commit();

            // O bloco abaixo precisa ficar após o commit.
            // atualizar as autoridades signatárias do contrato
            if (!self::atualizarAutoridadesSignatariasDoContrato($request, $arrayContratoAutoridadeSignatariaSalvos)) {
                \Alert::error('Problemas ao atualizar as Autoridades Signatárias do contrato.')->flash();
                return redirect()->back();
            }

        } catch (Exception $exc) {
            DB::rollback();
            Log::error($exc->getMessage());
        }

        return redirect()->route('crud.publicacao.index', ['contrato_id' => $request->input('contrato_id')]);
    }

    public function show($id)
    {
        $content = parent::show($id);

        $this->crud->removeColumn('fornecedor_id');
        $this->crud->removeColumn('tipo_id');
        $this->crud->removeColumn('categoria_id');
        $this->crud->removeColumn('unidade_id');
        $this->crud->removeColumn('fundamento_legal');
        $this->crud->removeColumn('modalidade_id');
        $this->crud->removeColumn('licitacao_numero');
        $this->crud->removeColumn('valor_inicial');
        $this->crud->removeColumn('valor_global');
        $this->crud->removeColumn('valor_parcela');
        $this->crud->removeColumn('valor_acumulado');
        $this->crud->removeColumn('situacao_siasg');
        $this->crud->removeColumn('contrato_id');
        $this->crud->removeColumn('receita_despesa');
        $this->crud->removeColumn('novo_valor_global');
        $this->crud->removeColumn('novo_num_parcelas');
        $this->crud->removeColumn('novo_valor_parcela');
        $this->crud->removeColumn('data_inicio_novo_valor');
        $this->crud->removeColumn('subcategoria');

        return $content;
    }
    public function edit($id)
    {
        $content = parent::edit($id);
        $contrato = Contrato::find($this->crud->entry->contrato_id);
        $this->crud->entry->contrata_mais_brasil = $contrato->contrata_mais_brasil;
        $this->crud->entry->numero_contratacao = $contrato->numero_contratacao;

        return $content;
    }

    public function encontraEmpenho($unidadeId, $minutaEmpenhoId)
    {
        return MinutaEmpenho::select([
                'minutaempenhos.contrato_id',
                'minutaempenhos.fornecedor_empenho_id as fornecedor_id',
                'empenhos.id as empenho_id',
                'empenhos.unidade_id as unidade_id',
            ])
                ->join('saldo_contabil', 'saldo_contabil.id', '=', 'minutaempenhos.saldo_contabil_id')
                ->join('empenhos', function ($join) {
                    $join->on('empenhos.numero', '=', 'minutaempenhos.mensagem_siafi')
                        ->on('empenhos.unidade_id', '=', 'saldo_contabil.unidade_id');
                })
                ->join('codigoitens as situacao', 'situacao.id', '=', 'minutaempenhos.situacao_id')
                ->join('codigoitens as tipo_empenhopor', 'tipo_empenhopor.id', '=', 'minutaempenhos.tipo_empenhopor_id')
                ->join('compras', 'compras.id', '=', 'minutaempenhos.compra_id')
                ->where('situacao.descricao', '=', 'EMPENHO EMITIDO')
                ->where('tipo_empenhopor.descricao', '=', 'Compra')
                ->where('empenhos.unidade_id', '=', $unidadeId)
                ->where('minutaempenhos.id', '=', $minutaEmpenhoId)
                ->first();
    }

    private function alterarItens($request)
    {
        foreach ($request['qtd_item'] as $key => $qtd) {
            if ($request['saldo_historico_id'][$key] !== 'undefined') {
                $tipo_material = null;
                if (array_key_exists($key, $request['tipo_material'])) {
                    $tipo_material = $request['tipo_material'][$key];
                }
                $saldoHistoricoIten = Saldohistoricoitem::find($request['saldo_historico_id'][$key]);
                $saldoHistoricoIten->quantidade = (double)$qtd;
                $saldoHistoricoIten->valorunitario = $request['vl_unit'][$key];
                $saldoHistoricoIten->valortotal = $request['vl_total'][$key];
                $saldoHistoricoIten->data_inicio = $request['data_inicio'][$key];
                $saldoHistoricoIten->periodicidade = $request['periodicidade'][$key];
                $saldoHistoricoIten->tipo_material = $tipo_material;
                if ($request['is_prazo_indefinido'] == "1" || $request['is_prazo_indefinido'] == true) {
                    $saldoHistoricoIten->periodicidade = 1;
                }
                $saldoHistoricoIten->numero_item_compra = $request['numero_item_compra'][$key];
                $saldoHistoricoIten->save();
            } else {
                $this->criarNovoContratoItem($key, $request);
            }
        }
    }

    private function cadastrarItensAtualizadoSaldoHistorico($request)
    {
        foreach ($request['qtd_item'] as $key => $qtd) {
            $this->criarNovoContratoItem($key, $request);
        }
    }

    private function criarNovoContratoItem($key, $request, $contratoHistoricoId = null)
    {
        $tipo_material = null;
        if (array_key_exists($key, $request['tipo_material'])) {
            $tipo_material = $request['tipo_material'][$key];
        }

        $catmatseritem_id = (int)$request['catmatseritem_id'][$key];
        $catmatseritem = Catmatseritem::find($catmatseritem_id);

        $contratoItem = new Contratoitem();
        $contratoItem->contrato_id = $request['contrato_id'];
        $contratoItem->tipo_id = $request['tipo_item_id'][$key];
        $contratoItem->grupo_id = $catmatseritem->grupo_id;
        $contratoItem->catmatseritem_id = $catmatseritem->id;
        $contratoItem->descricao_complementar = $catmatseritem->descricao;
        $contratoItem->quantidade = (double)$request['qtd_item'][$key];
        $contratoItem->valorunitario = $request['vl_unit'][$key];
        $contratoItem->valortotal = $request['vl_total'][$key];
        $contratoItem->data_inicio = $request['data_inicio'][$key];
        $contratoItem->periodicidade = $request['periodicidade'][$key];
        $contratoItem->numero_item_compra = $request['numero_item_compra'][$key];
        $contratoItem->tipo_material = $tipo_material;
        $contratoItem->contratohistorico_id = $contratoHistoricoId;
        $contratoItem->save();
    }

    private function excluirSaldoHistoricoItem($arrIdItens)
    {
        foreach ($arrIdItens as $id) {
            $item = Saldohistoricoitem::find($id);
            if($item) {
                $item->delete();
            }
        }
    }
}
