<?php

namespace App\Http\Controllers\Gescon;

use Alert;
use App\Forms\InserirItemContratoMinutaForm;
use App\Http\Controllers\Api\PNCP\UsuarioController;
use App\Http\Controllers\Empenho\CompraSiasgCrudController;
use App\Http\Traits\CamposComunsTrait;
use App\Http\Traits\ConsultaCompra;
use App\Http\Traits\Formatador;
use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\LogTrait;
use App\Http\Traits\ModalBackpackTrait;
use App\Http\Traits\PermissaoTrait;
use App\Http\Traits\SearchTrait;
use App\Http\Traits\HelperTrait;
use App\Jobs\AlertaContratoJob;
use App\Jobs\AtualizaFornecedorSicafJob;
use App\Models\AmparoLegalContrato;
use App\Models\Catmatseritem;
use App\Models\Codigoitem;
use App\Models\Compra;
use App\Models\CompraItem;
use App\Models\CompraItemMinutaEmpenho;
use App\Models\Comprasitemunidadecontratoitens;
use App\Models\Contrato;
use App\Models\Contratoempenho;
use App\Models\Contratoitem;
use App\Models\ContratoMinutaEmpenho;
use App\Models\Contratounidadedescentralizada;
use App\Models\Empenho;
use App\Models\FornecedoresSubContratados;
use App\Models\MinutaEmpenho;
use App\Models\Fornecedor;
use App\Models\Saldohistoricoitem;
use App\Models\Naturezadespesa;

use App\Http\Controllers\Api\PNCP\ContratoControllerPNCP;
use App\Http\Controllers\Api\PNCP\DocumentoContratoController;
use App\Http\Controllers\Api\PNCP\DocumentoTermoContratoController;

use App\Models\Autoridadesignataria;
use App\Models\ContratoAutoridadeSignataria;

use App\PDF\Pdf;
use App\services\EmpenhoService;
use App\services\MinutaService;
use App\services\AmparoLegalService;
use App\services\PNCP\PncpService;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use FormBuilder;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ContratoRequest as StoreRequest;
use App\Http\Requests\ContratoRequest as UpdateRequest;
use App\Http\Requests\AtivarContratoRequest as AtivarRequest;
use App\Http\Requests\Request;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;


// TODO: Apagar classes sem uso
use App\Models\Contratohistorico;
use App\Models\Contratoresponsavel;
use App\Models\Unidade;
use App\Notifications\RotinaAlertaContratoNotification;
use App\XML\ApiSiasg;
use Backpack\CRUD\CrudPanel;
use Codedge\Fpdf\Fpdf\Fpdf;
use Doctrine\DBAL\Query\QueryBuilder;
use App\Http\Traits\CompraTrait;
use App\Http\Traits\EnviaPncpTrait;
use App\Http\Traits\ContratoArquivoTrait;
use App\Models\AmparoLegal;
use App\Models\EnviaDadosPncp;
use Illuminate\Http\Request as HttpRequest;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request as FacadesRequest;

use App\services\CompraService;
use App\services\ContratoService;
use Illuminate\Support\Str;

/**
 * Class ContratoCrudController
 *
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ContratoCrudController extends CrudController
{
    use Formatador;
    use BuscaCodigoItens;
    use CompraTrait;
    use EnviaPncpTrait;
    use ContratoArquivoTrait;
    use ConsultaCompra;
    use CamposComunsTrait;
    use ModalBackpackTrait;
    use PermissaoTrait;
    use LogTrait;
    use SearchTrait;
    use HelperTrait;

    protected $tab = '';
    protected $compraService;
    protected $contratoService;
    protected $amparoLegalService;
    protected $pncpService;

    protected $minutaService;
    protected $empenhoService;

    public function __construct(ContratoService    $contratoService,
                                CompraService      $compraService,
                                MinutaService      $minutaService,
                                AmparoLegalService $amparoLegalService,
                                PncpService        $pncpService,
                                EmpenhoService     $empenhoService
    )
    {
        parent::__construct();
        $this->compraService = $compraService;
        $this->contratoService = $contratoService;
        $this->amparoLegalService = $amparoLegalService;
        $this->pncpService = $pncpService;
        $this->minutaService = $minutaService;
        $this->empenhoService = $empenhoService;
    }

    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $tipoSessao = config("session.driver");
        $dadosUsuarioRedis = null;
        if ($tipoSessao == 'redis') {
            $dadosUsuarioRedis = $this->recuperarDadosUsuarioRedis(auth()->id());
        }

        $userUgId = session()->get('user_ug_id');

        if (!empty($dadosUsuarioRedis)) {
            $userUgId = $dadosUsuarioRedis['user_ug_id'] ?? $dadosUsuarioRedis['ugprimaria'];
        }

        $isContratoEncerrado = strpos($this->request->request->get('situacao'), 'Encerrado') > 0 ? true : false;

        $this->crud->setModel('App\Models\Contrato');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/gescon/contrato');
        $this->crud->setEntityNameStrings('Contrato', 'Contratos');

        $this->crud->setCreateContentClass('col-md-12');
        $this->crud->setEditContentClass('col-md-12');
        $this->crud->setCreateView('vendor.backpack.crud.contrato.adicionar.create');
//        $this->crud->setEditView('vendor.backpack.crud.contrato.create');
//        $this->crud->setEditView('vendor.backpack.crud.contrato.adicionar.create');
        $this->crud->setEditView('vendor.backpack.crud.contrato.adicionar.edit');

        $this->crud->addClause('join', 'fornecedores', 'fornecedores.id', '=', 'contratos.fornecedor_id');
        $this->crud->addClause('join', 'unidades', 'unidades.id', '=', 'contratos.unidade_id');
        $this->crud->addClause('leftjoin', 'codigoitens', 'codigoitens.id', '=', 'contratos.modalidade_id');
        $this->crud->addClause('leftjoin', 'unidades as uc', 'uc.id', '=', 'contratos.unidadecompra_id');
        $this->crud->addClause('where', 'unidade_id', '=', $userUgId);

        if (backpack_user()->hasRole('Almoxarifado') && backpack_user()->roles->count() === 1) {
            $this->crud->addClause('whereHas', 'tipo', function ($query) {
                $query->where('descricao', 'Empenho');
            });
        }

        $this->crud->orderBy('updated_at', 'desc');
        $this->crud->addClause('select', 'contratos.*');

        if (isset($_GET['listar'])) {
            $listar = $_GET['listar'];
            $dataHoje = date("Y-m-d");
            $cincoDiasAtras = self::somaDiasAData(-5);
            if ($listar == 'contratosvencidos') {
                $this->crud->addClause('where', 'contratos.vigencia_fim', '<', $dataHoje);
            } elseif ($listar == 'novoscontratos') {
                $this->crud->addClause('where', 'contratos.vigencia_fim', '>=', $dataHoje);
                $this->crud->addClause('where', 'contratos.vigencia_inicio', '<=', $dataHoje);
                $this->crud->addClause('where', 'contratos.vigencia_inicio', '>=', $cincoDiasAtras);
            } elseif ($listar == 'contratosvigentes') {
                $this->crud->addClause('where', 'contratos.situacao', '=', true);
                $this->crud->addClause('where', function ($query) use ($dataHoje) {
                    $query->where(function ($subquery) use ($dataHoje) {
                        $subquery->where('contratos.vigencia_fim', '>=', $dataHoje)
                            ->where('contratos.vigencia_inicio', '<=', $dataHoje);
                    })->orWhere('contratos.is_prazo_indefinido', true);
                });
                $this->crud->addClause('where', 'contratos.elaboracao', '=', false);
            }
        }

        // dd(Str::replaceArray('?', $this->crud->query->getBindings(), $this->crud->query->toSql()));
        session()->forget('de_onde_usuario_vem_ocorrencias');
        session()->forget('de_onde_usuario_vem_faturas');
        session()->forget('de_onde_usuario_vem_terceirizados');
        session()->forget('de_onde_usuario_vem_conta_vinculada');
        session()->forget('isResponsavelPeloContrato');
        session()->forget('vemDoEncerramento');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration Global
        |--------------------------------------------------------------------------
        */

        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
        $this->crud->enableExportButtons();

        $this->crud->addButtonFromView('line', 'delete', 'delete_contrato', 'end');
        $this->crud->addButtonFromView('line', 'morecontrato', 'morecontrato', 'end');


        if ($this->verificaPermissaoConsulta('contrato') === true) {
            $this->crud->addButtonFromView('line', 'extratocontrato', 'extratocontrato', 'beginning');
        }

        $this->crud->addButtonFromView('line', 'linkpncp', 'linkpncp', 'beginning');
        if (backpack_user()->hasRole('Execução Financeira') || backpack_user()->hasRole('Setor Contratos') || backpack_user()->hasRole('Administrador') || backpack_user()->hasRole('Almoxarifado')) {
            $this->crud->addButtonFromView('line', 'update', 'elaboracao.update', 'beginning');
            // Button que chama a modal das minutas empenho força de contrato
            $this->crud->addButton(
                'top',
                'criar_contrato_do_tipo_empenho',
                'view',
                'crud::buttons.criar_contrato_do_tipo_empenho'
            );

            $this->crud->addButton(
                'line',
                'ativar_contrato',
                'view',
                'crud::buttons.ativar_contrato',
                'beginning'
            );
            $this->crud->addButton(
                'bottom',
                'elaboracao_modal',
                'view',
                'crud::buttons.elaboracao.modal'
            );
        }

        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');
        $this->crud->allowAccess('show');

        (backpack_user()->can('contrato_inserir')) ? $this->crud->allowAccess('create') : null;
        (backpack_user()->can('contrato_editar')) ? $this->crud->allowAccess('update') : null;
        (backpack_user()->can('contrato_deletar')) ? $this->crud->allowAccess('delete') : null;

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Custom
        |--------------------------------------------------------------------------
        */

        $modalidades = $this->retornaModalidadeCompra();

        $this->adicionaCampos($modalidades);
        $this->adicionaColunas();
        $this->aplicaFiltros($modalidades);

        // inicializo crud->modal_fields com os campos modal_fields para aproveitar os campos do backpack nos modais
        $this->crud->modal_fields = $this->modal_fields;
    }


    public function somaDiasAData($quantidadeDiasSomar)
    {
        $dataAtual = date('Y-m-d'); // Obtém a data atual no formato "AAAA-MM-DD"
        $novaData = date('Y-m-d', strtotime('+' . $quantidadeDiasSomar . 'days', strtotime($dataAtual))); // Soma 5 dias à data atual
        return $novaData; // Saída no formato "AAAA-MM-DD"
    }

    // Método que atualiza as autoridades signatárias do contrato
    public function atualizarAutoridadesSignatariasDoContrato($request)
    {
        $idContrato = $request->input('contrato_id');
        $objContratoHistorico = Contratohistorico::where('contrato_id', $idContrato)->first();
        $contratohistorico_id = $objContratoHistorico->id;
        $arrayAutoridadesSignatariasSalvar = $request->contrato_autoridade_signataria;
        $arrayContratoAutoridadeSignataria = ContratoAutoridadeSignataria::where('contrato_id', $idContrato)->delete();
        $arrayContratoAutoridadeSignataria = ContratoAutoridadeSignataria::where('created_at', null)->where('updated_at', null)->where('nome_autoridade_signataria', null)->delete();


        if (isset($arrayAutoridadesSignatariasSalvar)) {
            // aqui quer dizer que o que tinha pra ser limpo, já foi. Vamos salvar os dados
            foreach ($arrayAutoridadesSignatariasSalvar as $idAutoridadeSignataria) {
                // vamos buscar os dados da autoridade signatária, para salvarmos em contrato autoridade signataria.
                $objAutoridadeSignatariaSalvar = Autoridadesignataria::find($idAutoridadeSignataria);
                $nomeAutoridadeSignatariaSalvar = $objAutoridadeSignatariaSalvar->autoridade_signataria;
                $cargoAutoridadeSignatariaSalvar = $objAutoridadeSignatariaSalvar->cargo_autoridade_signataria;
                // para cada autoridade, vamos salvar um registro
                $objContratoAutoridadeSignataria = new ContratoAutoridadeSignataria();
                $objContratoAutoridadeSignataria->contratohistorico_id = $contratohistorico_id;
                $objContratoAutoridadeSignataria->autoridadesignataria_id = $idAutoridadeSignataria;
                $objContratoAutoridadeSignataria->contrato_id = $idContrato;
                $objContratoAutoridadeSignataria->nome_autoridade_signataria = $nomeAutoridadeSignatariaSalvar;
                $objContratoAutoridadeSignataria->cargo_autoridade_signataria = $cargoAutoridadeSignatariaSalvar;

                if (!$objContratoAutoridadeSignataria->save()) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Valida se o Codigo do item selecionado e diferente de Não se aplica
     */
    private function validarIDCodigoItem(int $idCodigoItem, string $descricao = 'Não se Aplica')
    {
        $arrayIdCodigoItem = Codigoitem::select('id')
            ->whereNull('deleted_at')
            ->where('descricao', $descricao)
            ->pluck('id')
            ->toArray();

        if (in_array($idCodigoItem, $arrayIdCodigoItem)) {
            return false;
        }

        return true;

    }

    public function store(StoreRequest $request)
    {
        session()->forget('editando_contrato_elaboracao');
        // Removo este request 'fake' para o backpack não tentar fazer o save dele
        // pois eu faço manualmente mais adiante com outros campos relacionados a empenho e unidades descentralizadas
        $request->request->remove('unidadesdescentralizadas');
        $request->request->remove('fornecedor_empenho');

        $this->contratoService->setCampoAplicavelDecreto($request);
        $this->contratoService->setCampoProrrogavel($request);
        $this->contratoService->setCamposContratoComPrazoIndefinido($request);
        $this->contratoService->setCamposCompras($request);
        $this->contratoService->setCamposValores($request);

        $modalidadeId = $request->input('modalidade_id');
        $validaItemNaoSeAplica = $this->validarIDCodigoItem($modalidadeId);

        $amparoslegais = $request->input('amparoslegais');
        $validaLei14133 =
            $this->amparoLegalService->amparoLegalLei14133($amparoslegais);

        $unidadeId = $request->input('unidade_id');
        $validaNaoSisg = $this->validarUasgNaoSisg($unidadeId);

        if (!DB::transactionLevel()) {
            DB::beginTransaction();
            try {
                $erro = $this->contratoService->verificaValidacaoCompra(
                    $validaItemNaoSeAplica,
                    $validaLei14133,
                    $validaNaoSisg,
                    $request
                );

                if ($erro !== null) {
                    DB::rollback();
                    return redirect()->back()->withInput($request->all())->withErrors($erro);
                }
                //salva contrato
                $redirect_location = parent::storeCrud($request);

                $contrato_id = $this->crud->getCurrentEntryId();
                $request->request->set('contrato_id', $contrato_id);
                $this->inserirItensContrato($request->all()); //itens
                $this->contratoService->processarVinculacoesContrato($request, $contrato_id, $unidadeId, $amparoslegais);

                # bloco de código para resolver o bug de contrato duplicado
                # caso encontre um contrato com os mesmos dados abaixo,
                # ele deleta o contrato atual pois vai ser salvo os dois.

                $verificaContratoExiste = Contrato::where('id', '<>', $contrato_id)
                    ->where('numero', $request->input('numero'))
                    ->where('tipo_id', $request->input('tipo_id'))
                    ->where('unidadeorigem_id', $request->input('unidadeorigem_id'))
                    ->first();

                if ($verificaContratoExiste) {
                    Contrato::where('id', $contrato_id)->delete();
                } else {
                    $this->contratoService->saveAbaEmpenhosContrato($contrato_id, $request, backpack_user()->id);
                }

                AtualizaFornecedorSicafJob::dispatch(Fornecedor::where('id', $request->input('fornecedor_id'))->first(), $contrato_id)->onQueue('pesquisa-fornecedor-sicaf-rfb');

                DB::commit();


                //se não for elaboracao
                if ($request->elaboracao === '0' && !$verificaContratoExiste) {
                    // vamos atualizar as autoridades signatárias do contrato
                    if (!$this->atualizarAutoridadesSignatariasDoContrato($request)) {
                        Alert::error('Problemas ao atualizar as Autoridades Signatárias do contrato.')->flash();
                        return redirect()->back();
                    }
                    return redirect()->route('crud.publicacao.index', ['contrato_id' => $contrato_id]);
                }

                return redirect()->route('crud.contrato.index');
            } catch (Exception $exc) {
                DB::rollback();
            }
        }

    }

    /**
     * @param string $mensagemAlerta
     * @return array|string|string[]
     */
    private function retiraQuebraDoInicio(string $mensagemAlerta)
    {
        $mensagemAlerta = str_replace("\\n", "\n", $mensagemAlerta); // Converte `\n` para nova linha real
        $mensagemAlerta = ltrim($mensagemAlerta); // Remove o primeiro `\n`
        return str_replace("\n", "\\n", $mensagemAlerta); // Converte de volta
    }

    private function enviarEmpenhoPNCP(int $contrato_id, $retSituacao = 'RETPEN')
    {
        $contratoHistorico = Contratohistorico::where("contrato_id", $contrato_id)->latest()->first();

        if ($this->isContratoTipoEmpenho($contratoHistorico->tipo->descricao)) {
            $encontrouSequencialPNCP = $this->atualizarInformacaoPNCPContratoMinuta($contratoHistorico);
            $dadosEnviaPNCP = EnviaDadosPncp::where("contrato_id", $contrato_id)->latest()->first();

            $situacao = $this->recuperarIDSituacao($retSituacao);

            if ($encontrouSequencialPNCP) {
                $dadosEnviaPNCP->situacao = $situacao;
                $dadosEnviaPNCP->save();
            }

            # Se o contrato do tipo empenho tiver sido publicado no PNCP
            if (!$encontrouSequencialPNCP && !empty($dadosEnviaPNCP->sequencialPNCP)) {
                $enviaDadosPncpMinutaEmpenho =
                    EnviaDadosPncp::where("pncpable_id", $contratoHistorico->minutasempenho[0]->id)
                        ->where('pncpable_type', MinutaEmpenho::class)
                        ->first();

                $situacaoEnviaDadosPncpMinutaEmpenho = $enviaDadosPncpMinutaEmpenho->status->descres;

                # Atualizar o envia dados pncp da minuta de empenho se a situação for INCPEN
                if (empty($enviaDadosPncpMinutaEmpenho->sequencialPNCP) &&
                    $situacaoEnviaDadosPncpMinutaEmpenho == 'INCPEN'
                ) {
                    $enviaDadosPncpMinutaEmpenho->situacao = $situacao;
                    $enviaDadosPncpMinutaEmpenho->link_pncp = $dadosEnviaPNCP->link_pncp;
                    $enviaDadosPncpMinutaEmpenho->sequencialPNCP = $dadosEnviaPNCP->sequencialPNCP;
                    $enviaDadosPncpMinutaEmpenho->save();
                }
            }
        }
    }

    private function buscarCamposBaseadosEmpenho($idEmpenho)
    {
        $camposContrato = MinutaEmpenho::select(
            "compras.modalidade_id",
            "compras.unidade_origem_id",
            "compras.numero_ano as compra_numero_ano"
        )
            ->join('compras', 'compras.id', '=', 'minutaempenhos.compra_id')
            ->where('minutaempenhos.id', $idEmpenho)->firstOrFail()->toArray();

        return $camposContrato;
    }

    public function inserirItensContrato($request): void
    {

        foreach ($request['qtd_item'] as $key => $qtd) {
            $catmatseritem_id = (int)$request['catmatseritem_id'][$key];
            $catmatseritem = Catmatseritem::find($catmatseritem_id);


            $descricaoComplementar = $catmatseritem->descricao;
            if (!empty($request['minutasempenho'])) {
                $itemCompra = MinutaEmpenho::join("compra_items", "compra_items.compra_id", "=", "minutaempenhos.compra_id")
                    ->whereIN("minutaempenhos.id", $request['minutasempenho'])
                    ->where("numero", $request['numero_item_compra'][$key])
                    ->where("catmatseritem_id", $request['catmatseritem_id'][$key])
                    ->get()
                    ->pluck("descricaodetalhada");

                if (!empty($itemCompra)) {
                    $descricaoComplementar = $itemCompra[0];
                }
            }

            $contratoItem = new Contratoitem();
            $contratoItem->contrato_id = $request['contrato_id'];
            $contratoItem->tipo_id = $request['tipo_item_id'][$key];
            $contratoItem->grupo_id = $catmatseritem->grupo_id;
            $contratoItem->catmatseritem_id = $catmatseritem->id;
            $contratoItem->descricao_complementar = mb_strtoupper($descricaoComplementar, 'UTF-8');
            $contratoItem->quantidade = (double)$qtd;
            $contratoItem->valorunitario = $request['vl_unit'][$key];
            $contratoItem->valortotal = $request['vl_total'][$key];
            $contratoItem->data_inicio = $request['data_inicio'][$key];
            $contratoItem->periodicidade = $request['periodicidade'][$key];
            $contratoItem->tipo_material = $request['tipo_material'][$key];

            if ($request['is_prazo_indefinido'] == "1" || $request['is_prazo_indefinido'] == true) {
                $contratoItem->periodicidade = 1;
            }
            $contratoItem->numero_item_compra = $request['numero_item_compra'][$key];
            $contratoItem->elaboracao = $request['elaboracao'];
            $contratoItem->save();
            if ($request['compra_item_unidade_id'][$key] !== 'undefined'
                && $request['compra_item_unidade_id'][$key] !== 'null') {
                $this->vincularContratoItensCompraItemUnidade($contratoItem, $request['compra_item_unidade_id'][$key]);
            }
        }
    }

    /**
     * Vincula um contrato item a uma compra item unidade.
     *
     * @param mixed $contratoItem o contrato item a ser vinculado
     * @param mixed $compra_item_unidade_id o ID da compra item unidade
     * @return void
     */
    public function vincularContratoItensCompraItemUnidade($contratoItem, $compra_item_unidade_id): void
    {
        $compraItemUnidade_ContratoItem = new Comprasitemunidadecontratoitens();
        $compraItemUnidade_ContratoItem->contratoitem_id = $contratoItem->id;
        $compraItemUnidade_ContratoItem->compra_item_unidade_id = $compra_item_unidade_id;
        $compraItemUnidade_ContratoItem->save();
    }

    public function update(UpdateRequest $request)
    {
        $this->contratoService->setCampoAplicavelDecreto($request);
        $this->contratoService->setCampoProrrogavel($request);
        $this->contratoService->setCamposContratoComPrazoIndefinido($request);
        $this->contratoService->setCamposCompras($request);
        $this->contratoService->setCamposValores($request);

        $modalidadeId = $request->input('modalidade_id');
        $validaItemNaoSeAplica = $this->validarIDCodigoItem($modalidadeId);

        $amparoslegais = $request->input('amparoslegais');
        $validaLei14133 =
            $this->amparoLegalService->amparoLegalLei14133($amparoslegais);

        $unidadeId = $request->input('unidade_id');
        $validaNaoSisg = $this->validarUasgNaoSisg($unidadeId);

        try {
            DB::beginTransaction();
            $erro = $this->contratoService->verificaValidacaoCompra(
                $validaItemNaoSeAplica,
                $validaLei14133,
                $validaNaoSisg,
                $request
            );

            if ($erro !== null) {
                return redirect()->back()->withInput($request->all())->withErrors($erro);
            }

            // verificar amparo legal vs lei retornada pelo siasg
            if ($validaItemNaoSeAplica) {
                // aqui é tanto para sisg quanto para não sisg
                $objectRequest = new \Illuminate\Http\Request();
                $objectRequest->setMethod('POST');
                $objectRequest->request->add([$request->all()]);

                $retornoVerificacao = $this->contratoService->verificarAmparoLegalVsLeiSiasg(
                    $objectRequest,
                    $validaLei14133
                );

                if ($retornoVerificacao !== true) {

                    return redirect()->back()->withInput(Input::all())->withErrors(
                        [
                            "texto_exibicao.validarcompra" => 'Dados da compra inválidos!',
                            "unidadecompra_id.validarcompra" => 'Unidade Da Compra.',
                            "modalidade_id.validarcompra" => 'Modalidade Da Licitação.',
                            "licitacao_numero.validarcompra" => 'Número Da Licitação.',
                            "amparoslegais.validarcompra" => $retornoVerificacao,
                        ]
                    );
                }
            }

            $contrato_id = $this->crud->getCurrentEntryId();

            //desvincular minutas, para vinculá-las logo em seguida caso necessário
            $contrato = Contrato::find($contrato_id);
            $contrato->minutasempenho()->detach();
            $contrato->contratoEmpenhos()->detach();

            session(['editando_contrato_elaboracao' => true]);

            //edita contrato
            $redirect_location = parent::updateCrud($request); //contrato

            session()->forget('editando_contrato_elaboracao');
            $request->request->set('contrato_id', $contrato_id);

            $this->deletarItensContrato($contrato_id);
            $this->inserirItensContrato($request->all()); //itens
            $this->contratoService->processarVinculacoesContrato($request, $contrato_id, $unidadeId, $amparoslegais);
            $this->contratoService->saveAbaEmpenhosContrato($contrato_id, $request, backpack_user()->id);

            //caso esteja ativando contrato atualiza arquivos
            if ($request->elaboracao === '0') {
                $this->contratoService->atualizarArquivosNoAtivar($contrato);
            }
            DB::commit();

            if ($request->elaboracao === '0') {
                // vamos atualizar as autoridades signatárias do contrato
                if (!$this->atualizarAutoridadesSignatariasDoContrato($request)) {
                    Alert::error('Problemas ao atualizar as Autoridades Signatárias do contrato.')->flash();
                    return redirect()->back();
                }
                return redirect()->route('crud.publicacao.index', ['contrato_id' => $contrato_id]);
            }
            return redirect()->route('crud.contrato.index');
        } catch (Exception $exc) {
            DB::rollback();
//            dd($exc);
        }
    }

    public function show($id)
    {
        $content = parent::show($id);

        $this->crud->removeColumn('fornecedor_id');
        $this->crud->removeColumn('tipo_id');
        $this->crud->removeColumn('categoria_id');
        $this->crud->removeColumn('unidade_id');
        $this->crud->removeColumn('info_complementar');
        $this->crud->removeColumn('fundamento_legal');
        $this->crud->removeColumn('modalidade_id');
        $this->crud->removeColumn('licitacao_numero');
        $this->crud->removeColumn('data_assinatura');
        $this->crud->removeColumn('data_publicacao');
        $this->crud->removeColumn('valor_inicial');
        $this->crud->removeColumn('valor_global');
        $this->crud->removeColumn('valor_parcela');
        $this->crud->removeColumn('valor_acumulado');
        $this->crud->removeColumn('situacao_siasg');
        $this->crud->removeColumn('receita_despesa');
        $this->crud->removeColumn('subcategoria_id');
        $this->crud->removeColumn('situacao');
        $this->crud->removeColumn('unidadeorigem_id');
        $this->crud->removeColumn('unidadecompra_id');
        $this->crud->removeColumn('publicado');
        $this->crud->removeColumn('numero_compra');
        $this->crud->removeColumn('is_objeto_contratual_entregue');
        $this->crud->removeColumn('is_cumpridas_obrigacoes_financeiras');
        $this->crud->removeColumn('is_saldo_conta_vinculada_liberado');
        $this->crud->removeColumn('justificativa_contrato_inativo_id');
        $this->crud->removeColumn('unidadebeneficiaria_id');
        $this->crud->removeColumn('is_prazo_indefinido');
        $this->crud->removeColumn('user_id_responsavel_encerramento');
        $this->crud->removeColumn('cpf_responsavel_encerramento');
        $this->crud->removeColumn('cronograma_automatico');
        $this->crud->removeColumn('is_garantia_contratual_devolvida');
        $this->crud->removeColumn('grau_satisfacao_desempenho_contrato');
        $this->crud->removeColumn('planejamento_contratacao_atendida');
        $this->crud->removeColumn('sugestao_licao_aprendida');
        $this->crud->removeColumn('consecucao_objetivos_contratacao');
        $this->crud->removeColumn('nome_responsavel_signatario_encerramento');
        $this->crud->removeColumn('cpf_responsavel_signatario_encerramento');
        $this->crud->removeColumn('user_id_responsavel_signatario_encerramento');
        $this->crud->removeColumn('hora_encerramento');
        $this->crud->removeColumn('prorrogavel');
        $this->crud->removeColumn('elaboracao');
        $this->crud->removeColumn('te_valor_total_executado');
        $this->crud->removeColumn('te_valor_total_pago');
        $this->crud->removeColumn('te_saldo_disponivel_ou_bloqueado');
        $this->crud->removeColumn('te_justificativa_nao_cumprimento');
        $this->crud->removeColumn('te_saldo_bloqueado_garantia_contratual');
        $this->crud->removeColumn('te_justificativa_bloqueio_garantia_contratual');
        $this->crud->removeColumn('te_saldo_bloqueado_conta_deposito');
        $this->crud->removeColumn('te_justificativa_bloqueio_conta_deposito');

        return $content;
    }

    public function index()
    {
        $content = parent::index();
        //148 Atributo que recupera listagem de minutas para modal;
        //Retirado do método setup() para não precisar fazer estas querys em outras telas
        $this->crud->arrMinutasModal = $this->recuperaMinutasModal();
        $this->adicionaCampoDataVigenciaInicioHidden();
        return $content;
    }

    public function edit($id)
    {
        $content = parent::edit($id);

        $idMaterial = $this->retornaIdCodigoItem('Tipo CATMAT e CATSER', 'Material');
        $idServico = $this->retornaIdCodigoItem('Tipo CATMAT e CATSER', 'Serviço');

        $this->crud->modifyField('itens', [
            'type' => 'itens_contrato_list_edit',
            'label' => 'Teste',
            'tab' => 'Itens do contrato',
            'material' => $idMaterial,
            'servico' => $idServico,
        ]);

        $this->crud->modifyField('prorrogavel', [
            'label' => 'Prorrogável',
            'type' => 'select_from_array',
            'options' => [
                'n' => 'Não',
                's' => 'Sim'
            ],
            'value' => $this->crud->getCurrentEntry()->prorrogavel ? 's' : 'n',
            'allows_null' => false,
            'tab' => 'Características do contrato'
        ]);

        $this->crud->addField([
            'name' => 'editar',
            'type' => 'hidden',
            'default' => true
        ]);


        return $content;
    }

    public function extratoPdf(int $contrato_id)
    {
        $contrato = Contrato::find($contrato_id);

        $pdf = new Pdf("P", "mm", "A4");
        $pdf->SetTitle("Extrato Contrato", 1);
        $pdf->AliasNbPages();
        $pdf->AddPage();

        // Dados do contratos
        $pdf->SetY("28");
        $pdf->SetFont('Arial', 'BIU', 10);
        $pdf->Cell(0, 5, utf8_decode("Dados do Contrato") . ' - Contrato num.: ' . utf8_decode($contrato->numero) . ' - UG: ' . utf8_decode($contrato->unidade->codigo . " - " . $contrato->unidade->nomeresumido), 0, 0, 'C');

        $pdf->SetY("35");
        $pdf->SetFont('Arial', '', 8);
        $pdf->Cell(31, 5, utf8_decode("Número do instrumento: "), 0, 0, 'L');
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(20, 5, utf8_decode($contrato->numero), 0, 0, 'L');

        $pdf->SetFont('Arial', '', 8);
        $pdf->Cell(20, 5, utf8_decode("Fornecedor: "), 0, 0, 'L');
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->Cell(18, 5, utf8_decode(strlen($contrato->fornecedor->nome) > 65 ? substr($contrato->fornecedor->nome, 0, 65) . " [...]" : $contrato->fornecedor->nome), 0, 0, 'L');

        $pdf->SetY("40");
        $pdf->SetFont('Arial', '', 8);
        $pdf->Cell(33, 5, utf8_decode("CNPJ/CPF/ID Genérico: "), 0, 0, 'L');
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->Cell(30, 5, utf8_decode($contrato->fornecedor->cpf_cnpj_idgener), 0, 0, 'L');


        $pdf->SetY("45");
        $pdf->SetFont('Arial', '', 8);
        $pdf->Cell(22, 5, utf8_decode("Processo Núm.: "), 0, 0, 'L');
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->Cell(35, 5, utf8_decode($contrato->processo), 0, 0, 'L');
        $pdf->SetFont('Arial', '', 8);
        $pdf->Cell(18, 5, utf8_decode("UG Recurso: "), 0, 0, 'L');
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->Cell(20, 5, utf8_decode($contrato->unidade->codigo . " - " . $contrato->unidade->nome), 0, 0, 'L');

        $pdf->SetY("50");
        $pdf->SetFont('Arial', '', 8);
        $pdf->Cell(23, 5, utf8_decode("Data Assinatura: "), 0, 0, 'L');
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->Cell(40, 5, utf8_decode(implode("/", array_reverse(explode("-", $contrato->data_assinatura)))), 0, 0, 'L');
        $pdf->SetFont('Arial', '', 8);
        $pdf->Cell(23, 5, utf8_decode("Tipo do Contrato: "), 0, 0, 'L');
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->Cell(20, 5, utf8_decode($contrato->tipo->descricao), 0, 0, 'L');

        $pdf->SetY("55");
        $pdf->SetFont('Arial', '', 8);
        $pdf->Cell(23, 5, utf8_decode("Tipo Licitação: "), 0, 0, 'L');
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->Cell(40, 5, utf8_decode($contrato->modalidade->descricao), 0, 0, 'L');
        $pdf->SetFont('Arial', '', 8);
        $pdf->Cell(25, 5, utf8_decode("Número Licitação: "), 0, 0, 'L');
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->Cell(20, 5, utf8_decode($contrato->licitacao_numero), 0, 0, 'L');

        $pdf->SetY("60");
        $pdf->SetFont('Arial', '', 8);
        $pdf->Cell(30, 5, utf8_decode("Data Vigência Início: "), 0, 0, 'L');
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->Cell(33, 5, utf8_decode(implode("/", array_reverse(explode("-", $contrato->vigencia_inicio)))), 0, 0, 'L');
        $pdf->SetFont('Arial', '', 8);
        $pdf->Cell(30, 5, utf8_decode("Data Vigência Fim: "), 0, 0, 'L');
        $pdf->SetFont('Arial', 'B', 9);
        if ($contrato->is_prazo_indefinido == '1' || $contrato->is_prazo_indefinido == true) {
            $pdf->Cell(33, 5, utf8_decode("Indeterminado"), 0, 0, 'L');
        } else {
            $pdf->Cell(33, 5, utf8_decode(implode("/", array_reverse(explode("-", $contrato->vigencia_fim)))), 0, 0, 'L');
        }
        $pdf->SetY("65");
        $pdf->SetFont('Arial', '', 8);
        $pdf->Cell(17, 5, utf8_decode("Valor Global: "), 0, 0, 'L');
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->Cell(30, 5, utf8_decode(number_format($contrato->valor_global, 2, ',', '.')), 0, 0, 'L');
        $pdf->SetFont('Arial', '', 8);
        $pdf->Cell(21, 5, utf8_decode("Núm. Parcelas: "), 0, 0, 'L');
        $pdf->SetFont('Arial', 'B', 9);
        if ($contrato->is_prazo_indefinido == '1' || $contrato->is_prazo_indefinido == true) {
            $pdf->Cell(33, 5, utf8_decode("Indeterminado"), 0, 0, 'L');
        } else {
            $pdf->Cell(10, 5, utf8_decode(number_format($contrato->num_parcelas, 0, '', '.')), 0, 0, 'L');
        }
        $pdf->SetFont('Arial', '', 8);
        $pdf->Cell(18, 5, utf8_decode("Valor Parcial: "), 0, 0, 'L');
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->Cell(25, 5, utf8_decode(number_format($contrato->valor_parcela, 2, ',', '.')), 0, 0, 'L');

        $pdf->SetY("70");
        $pdf->SetFont('Arial', '', 8);
        $pdf->Cell(24, 5, utf8_decode("Valor Acumulado: "), 0, 0, 'L');
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->Cell(30, 5, utf8_decode(number_format($contrato->valor_acumulado, 2, ',', '.')), 0, 0, 'L');
        $pdf->SetFont('Arial', '', 8);
        $pdf->Cell(33, 5, utf8_decode("Total Desp. Acessórias: "), 0, 0, 'L');
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->Cell(30, 5, utf8_decode(number_format($contrato->total_despesas_acessorias, 2, ',', '.')), 0, 0, 'L');

        $pdf->SetY("75");
        $pdf->SetFont('Arial', '', 8);
        $pdf->Cell(0, 5, utf8_decode("Objeto: "), 0, 0, 'L');
        $pdf->SetY("80");
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->MultiCell(0, 5, utf8_decode($contrato->objeto), 0, 'J');

        //numero de caracteres fonte 9 por linha 100

        $pdf->SetY(80 + ($pdf->NbLines(161, utf8_decode($contrato->objeto)) * 5));
        $pdf->SetFont('Arial', '', 8);
        $pdf->Cell(0, 5, utf8_decode("Informação Complementar: "), 0, 0, 'L');
        $pdf->SetY(85 + ($pdf->NbLines(161, utf8_decode($contrato->objeto)) * 5));
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->MultiCell(0, 5, utf8_decode($contrato->info_complementar), 0, 'J');

        //Histórico de Contrato
        $pdf->AddPage();
        $pdf->SetY("28");
        $pdf->SetFont('Arial', 'BIU', 10);
        $pdf->Cell(
            0,
            5,
            utf8_decode("Histórico do Contrato") . ' - Contrato num.: '
            . utf8_decode($contrato->numero) . ' - UG: '
            . utf8_decode($contrato->unidade->codigo . " - " . $contrato->unidade->nomeresumido),
            0,
            0,
            'C'
        );
        $cell_width = 23;
        $pdf->SetY(35);
        $pdf->SetFont('Arial', 'BU', 10);
        $pdf->Cell(0, 5, utf8_decode("Histórico"));

        $pdf->SetY(40);
        $pdf->SetFont('Arial', 'B', 7);
        $pdf->Cell($cell_width, 5, utf8_decode("Tipo"), 1, 0, 'C');

        $pdf->Cell($cell_width, 5, utf8_decode("Número"), 1, 0, 'C');
        $pdf->Cell($cell_width, 5, utf8_decode("Data Assinatura"), 1, 0, 'C');
        $pdf->Cell($cell_width, 5, utf8_decode("Data Início"), 1, 0, 'C');
        $pdf->Cell($cell_width, 5, utf8_decode("Data Fim"), 1, 0, 'C');
        $pdf->Cell($cell_width, 5, utf8_decode("Valor Global"), 1, 0, 'C');
        $pdf->Cell($cell_width, 5, utf8_decode("Parcelas"), 1, 0, 'C');
        $pdf->Cell($cell_width, 5, utf8_decode("Valor Parcela"), 1, 0, 'C');

        $row_resp = 45;
        $historico = $contrato->historico()->get();

        foreach ($historico as $registro) {
            if ($row_resp >= 245) {
                $row_resp = 40;
                $pdf->AddPage();
                $pdf->SetY($row_resp);
                $pdf->SetFont('Arial', 'B', 7);
                $pdf->Cell($cell_width, 5, utf8_decode("Tipo"), 1, 0, 'C');
                $pdf->Cell($cell_width, 5, utf8_decode("Número"), 1, 0, 'C');
                $pdf->Cell($cell_width, 5, utf8_decode("Data Assinatura"), 1, 0, 'C');
                $pdf->Cell($cell_width, 5, utf8_decode("Data Início"), 1, 0, 'C');
                $pdf->Cell($cell_width, 5, utf8_decode("Data Fim"), 1, 0, 'C');
                $pdf->Cell($cell_width, 5, utf8_decode("Valor Global"), 1, 0, 'C');
                $pdf->Cell($cell_width, 5, utf8_decode("Parcelas"), 1, 0, 'C');
                $pdf->Cell($cell_width, 5, utf8_decode("Valor Parcela"), 1, 0, 'C');
                $row_resp += 5;
            }

            $pdf->SetY($row_resp);

            $linhas = $pdf->NbLines($cell_width, utf8_decode(($registro->tipo()->first()->descricao))) * 5;
            $pdf->SetFont('Arial', 'B', 7);
            //A MultiCell quebra a linha atual após ser exibida e ao usá-la fora da última coluna o ponto XY deve
            //ser atualizado para continuar na linha atual.
            $current_y = $pdf->GetY();
            $current_x = $pdf->GetX();
            $pdf->MultiCell($cell_width, 5, utf8_decode($registro->tipo()->first()->descricao), 1, 'C');
            $pdf->SetXY($current_x + $cell_width, $current_y);

            $pdf->SetFont('Arial', '', 7);
            $pdf->Cell($cell_width, $linhas, $registro->numero, 1, 0, 'C');
            $pdf->Cell($cell_width, $linhas, implode('/', array_reverse(explode('-', $registro->data_assinatura))), 1, 0, 'C');
            $pdf->Cell($cell_width, $linhas, implode('/', array_reverse(explode('-', $registro->vigencia_inicio))), 1, 0, 'C');
            $pdf->Cell($cell_width, $linhas, implode('/', array_reverse(explode('-', $registro->vigencia_fim))), 1, 0, 'C');
            $pdf->Cell($cell_width, $linhas, number_format($registro->valor_global, 2, ',', "."), 1, 0, 'R');
            $pdf->Cell($cell_width, $linhas, $registro->num_parcelas, 1, 0, 'R');
            $pdf->Cell($cell_width, $linhas, number_format($registro->valor_parcela, 2, ',', "."), 1, 0, 'R');

            $row_resp += $linhas;
            $pdf->SetY($row_resp);

            $linhas = $pdf->NbLines(161, utf8_decode($registro->observacao)) * 5;
            $pdf->SetFont('Arial', 'B', 7);
            $pdf->Cell($cell_width, $linhas, utf8_decode("Observação"), 1, 0, 'C');

            $pdf->SetFont('Arial', '', 7);
            $pdf->MultiCell(161, 5, utf8_decode($registro->observacao), 1);

            $row_resp += $linhas + 5;
        }

        //responsaveis do contrato
        //Responsáveis
        $pdf->AddPage();
        $pdf->SetY("28");
        $pdf->SetFont('Arial', 'BIU', 10);
        $pdf->Cell(0, 5, utf8_decode("Responsáveis") . ' - Contrato num.: ' . utf8_decode($contrato->numero) . ' - UG: ' . utf8_decode($contrato->unidade->codigo . " - " . $contrato->unidade->nomeresumido), 0, 0, 'C');

        //busca responsaveis por situacao
        $responsaveis_ativos = $contrato->responsaveis()->where('situacao', true)->get();
        //no mapeamento da classe contrato com a classe contratoresponsavel, somente os responsaveis ativos são buscados
        $responsaveis_inativos = Contratoresponsavel::where('contrato_id', $contrato->id)->where('situacao', false)->get();

        //ativos
        $pdf->SetY("35");
        $pdf->SetFont('Arial', 'BU', 10);
        $pdf->Cell(28, 5, utf8_decode("Ativos"), 0, 0, 'L');

        $row_resp = 35 + 5;

        foreach ($responsaveis_ativos as $ativo) {
            if ($row_resp >= 260) {
                $row_resp = 35;
                $pdf->AddPage();
            }

            $pdf->SetY($row_resp);
            $pdf->SetFont('Arial', '', 8);
            $pdf->Cell(18, 5, utf8_decode("CPF / Nome: "), 'T', 0, 'L');
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(100, 5, utf8_decode($ativo->user->cpf . ' - ' . $ativo->user->name), 'T', 0, 'L');
            $pdf->SetFont('Arial', '', 8);
            $pdf->Cell(18, 5, utf8_decode("Função: "), 'T', 0, 'L');
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(0, 5, utf8_decode($ativo->funcao->descricao), 'T', 0, 'L');

            $row_resp = $row_resp + 5;

            $pdf->SetY($row_resp);
            $pdf->SetFont('Arial', '', 8);
            $pdf->Cell(18, 5, utf8_decode("Portaria: "), 0, 0, 'L');
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(20, 5, utf8_decode($ativo->portaria), 0, 0, 'L');

            $row_resp = $row_resp + 5;

            $pdf->SetY($row_resp);
            $pdf->SetFont('Arial', '', 8);
            $pdf->Cell(20, 5, utf8_decode("Telefone Fixo: "), 0, 0, 'L');
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(48, 5, utf8_decode($ativo->telefone_fixo), 0, 0, 'L');
            $pdf->SetFont('Arial', '', 8);
            $pdf->Cell(25, 5, utf8_decode("Telefone Celular: "), 0, 0, 'L');
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(50, 5, utf8_decode($ativo->telefone_celular), 0, 0, 'L');

            $row_resp = $row_resp + 5;

            $pdf->SetY($row_resp);
            $pdf->SetFont('Arial', '', 8);
            $pdf->Cell(18, 5, utf8_decode("Unidade: "), 0, 0, 'L');
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(0, 5, utf8_decode(($ativo->instalacao_id) ? $ativo->instalacao->nome : ''), 0, 0, 'L');

            $row_resp = $row_resp + 5;

            $pdf->SetY($row_resp);
            $pdf->SetFont('Arial', '', 8);
            $pdf->Cell(18, 5, utf8_decode("Data Início: "), "B", 0, 'L');
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(50, 5, utf8_decode(implode("/", array_reverse(explode("-", $ativo->data_inicio)))), 'B', 0, 'L');
            $pdf->SetFont('Arial', '', 8);
            $pdf->Cell(18, 5, utf8_decode("Data Fim: "), "B", 0, 'L');
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(0, 5, utf8_decode(implode("/", array_reverse(explode("-", $ativo->data_fim)))), 'B', 0, 'L');

            $row_resp = $row_resp + 5;
        }

        //inativos
        $row_resp = $row_resp + 5;
        $pdf->SetY($row_resp);
        $pdf->SetFont('Arial', 'BU', 10);
        $pdf->Cell(28, 5, utf8_decode("Inativos"), 0, 0, 'L');
        $row_resp = $row_resp + 5;

        foreach ($responsaveis_inativos as $inativo) {
            if ($row_resp >= 260) {
                $row_resp = 35;
                $pdf->AddPage();
            }

            $pdf->SetY($row_resp);
            $pdf->SetFont('Arial', '', 8);
            $pdf->Cell(18, 5, utf8_decode("CPF / Nome: "), 'T', 0, 'L');
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(100, 5, utf8_decode($inativo->user->cpf . ' - ' . $inativo->user->name), 'T', 0, 'L');
            $pdf->SetFont('Arial', '', 8);
            $pdf->Cell(18, 5, utf8_decode("Função: "), 'T', 0, 'L');
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(0, 5, utf8_decode($inativo->funcao->descricao), 'T', 0, 'L');

            $row_resp = $row_resp + 5;

            $pdf->SetY($row_resp);
            $pdf->SetFont('Arial', '', 8);
            $pdf->Cell(18, 5, utf8_decode("Portaria: "), 0, 0, 'L');
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(20, 5, utf8_decode($inativo->portaria), 0, 0, 'L');

            $row_resp = $row_resp + 5;

            $pdf->SetY($row_resp);
            $pdf->SetFont('Arial', '', 8);
            $pdf->Cell(20, 5, utf8_decode("Telefone Fixo: "), 0, 0, 'L');
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(48, 5, utf8_decode($inativo->telefone_fixo), 0, 0, 'L');
            $pdf->SetFont('Arial', '', 8);
            $pdf->Cell(25, 5, utf8_decode("Telefone Celular: "), 0, 0, 'L');
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(50, 5, utf8_decode($inativo->telefone_celular), 0, 0, 'L');

            $row_resp = $row_resp + 5;

            $pdf->SetY($row_resp);
            $pdf->SetFont('Arial', '', 8);
            $pdf->Cell(18, 5, utf8_decode("Unidade: "), 0, 0, 'L');
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(0, 5, utf8_decode(($inativo->instalacao_id) ? $inativo->instalacao->nome : ''), 0, 0, 'L');

            $row_resp = $row_resp + 5;

            $pdf->SetY($row_resp);
            $pdf->SetFont('Arial', '', 8);
            $pdf->Cell(18, 5, utf8_decode("Data Início: "), "B", 0, 'L');
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(50, 5, utf8_decode(implode("/", array_reverse(explode("-", $inativo->data_inicio)))), 'B', 0, 'L');
            $pdf->SetFont('Arial', '', 8);
            $pdf->Cell(18, 5, utf8_decode("Data Fim: "), "B", 0, 'L');
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(0, 5, utf8_decode(implode("/", array_reverse(explode("-", $inativo->data_fim)))), 'B', 0, 'L');

            $row_resp = $row_resp + 5;
        }

        //execuçao orcamentaria e financeira - empenhos
        $pdf->AddPage();
        $pdf->SetY("28");
        $pdf->SetFont('Arial', 'BIU', 10);
        $pdf->Cell(0, 5, utf8_decode("Execução Orçamentária e Financeira") . ' - Contrato num.: ' . utf8_decode($contrato->numero) . ' - UG: ' . utf8_decode($contrato->unidade->codigo . " - " . $contrato->unidade->nomeresumido), 0, 0, 'C');

        $pdf->SetY("35");
        $pdf->SetFont('Arial', 'BU', 10);
        $pdf->Cell(28, 5, utf8_decode("Empenhos"), 0, 0, 'L');
        $pdf->SetFont('Arial', '', 8);
        $pdf->Cell(0, 5, utf8_decode("R$"), 0, 0, 'R');

        $empenhos = $contrato->empenhos()->get();

        $pdf->SetY(40);
        $pdf->SetFont('Arial', 'B', 7);
        $pdf->Cell(21, 5, utf8_decode("Número"), 1, 0, 'C');
        $pdf->Cell(21, 5, utf8_decode("Empenhado"), 1, 0, 'C');
        $pdf->Cell(21, 5, utf8_decode("A Liquidar"), 1, 0, 'C');
        $pdf->Cell(21, 5, utf8_decode("Liquidado"), 1, 0, 'C');
        $pdf->Cell(21, 5, utf8_decode("Pago"), 1, 0, 'C');
        $pdf->Cell(21, 5, utf8_decode("RP Inscr."), 1, 0, 'C');
        $pdf->Cell(21, 5, utf8_decode("RP A Liq."), 1, 0, 'C');
        $pdf->Cell(21, 5, utf8_decode("RP Liquidado"), 1, 0, 'C');
        $pdf->Cell(21, 5, utf8_decode("RP Pago"), 1, 0, 'C');

        $t_empenhado = 0;
        $t_aliquidar = 0;
        $t_liquidado = 0;
        $t_pago = 0;
        $t_rpinscrito = 0;
        $t_rpaliquidar = 0;
        $t_rpliquidado = 0;
        $t_rppago = 0;

        $row_resp = 40 + 5;

        foreach ($empenhos as $empenho) {
            if ($row_resp >= 260) {
                $row_resp = 35;
                $pdf->AddPage();
                $pdf->SetY($row_resp);
                $pdf->SetFont('Arial', 'B', 7);
                $pdf->Cell(21, 5, utf8_decode("Número"), 1, 0, 'C');
                $pdf->Cell(21, 5, utf8_decode("Empenhado"), 1, 0, 'C');
                $pdf->Cell(21, 5, utf8_decode("A Liquidar"), 1, 0, 'C');
                $pdf->Cell(21, 5, utf8_decode("Liquidado"), 1, 0, 'C');
                $pdf->Cell(21, 5, utf8_decode("Pago"), 1, 0, 'C');
                $pdf->Cell(21, 5, utf8_decode("RP Inscr."), 1, 0, 'C');
                $pdf->Cell(21, 5, utf8_decode("RP A Liq."), 1, 0, 'C');
                $pdf->Cell(21, 5, utf8_decode("RP Liquidado"), 1, 0, 'C');
                $pdf->Cell(21, 5, utf8_decode("RP Pago"), 1, 0, 'C');
                $row_resp += 5;
            }

            $t_empenhado += $empenho->empenho->empenhado;
            $t_aliquidar += $empenho->empenho->aliquidar;
            $t_liquidado += $empenho->empenho->liquidado;
            $t_pago += $empenho->empenho->pago;
            $t_rpinscrito += $empenho->empenho->rpinscrito;
            $t_rpaliquidar += $empenho->empenho->rpaliquidar;
            $t_rpliquidado += $empenho->empenho->rpliquidado;
            $t_rppago += $empenho->empenho->rppago;

            $pdf->SetY($row_resp);
            $pdf->SetFont('Arial', '', 7);
            $pdf->Cell(21, 5, utf8_decode($empenho->empenho->numero), 1, 0, 'L');
            $pdf->Cell(21, 5, utf8_decode(number_format($empenho->empenho->empenhado, 2, ',', ".")), 1, 0, 'R');
            $pdf->Cell(21, 5, utf8_decode(number_format($empenho->empenho->aliquidar, 2, ',', ".")), 1, 0, 'R');
            $pdf->Cell(21, 5, utf8_decode(number_format($empenho->empenho->liquidado, 2, ',', ".")), 1, 0, 'R');
            $pdf->Cell(21, 5, utf8_decode(number_format($empenho->empenho->pago, 2, ',', ".")), 1, 0, 'R');
            $pdf->Cell(21, 5, utf8_decode(number_format($empenho->empenho->rpinscrito, 2, ',', ".")), 1, 0, 'R');
            $pdf->Cell(21, 5, utf8_decode(number_format($empenho->empenho->rpaliquidar, 2, ',', ".")), 1, 0, 'R');
            $pdf->Cell(21, 5, utf8_decode(number_format($empenho->empenho->rpliquidado, 2, ',', ".")), 1, 0, 'R');
            $pdf->Cell(21, 5, utf8_decode(number_format($empenho->empenho->rppago, 2, ',', ".")), 1, 0, 'R');

            $row_resp += 5;
        }

        $pdf->SetY($row_resp);
        $pdf->SetFont('Arial', 'B', 7);
        $pdf->Cell(21, 5, utf8_decode("Total"), 1, 0, 'R');
        $pdf->Cell(21, 5, utf8_decode(number_format($t_empenhado, 2, ',', ".")), 1, 0, 'R');
        $pdf->Cell(21, 5, utf8_decode(number_format($t_aliquidar, 2, ',', ".")), 1, 0, 'R');
        $pdf->Cell(21, 5, utf8_decode(number_format($t_liquidado, 2, ',', ".")), 1, 0, 'R');
        $pdf->Cell(21, 5, utf8_decode(number_format($t_pago, 2, ',', ".")), 1, 0, 'R');
        $pdf->Cell(21, 5, utf8_decode(number_format($t_rpinscrito, 2, ',', ".")), 1, 0, 'R');
        $pdf->Cell(21, 5, utf8_decode(number_format($t_rpaliquidar, 2, ',', ".")), 1, 0, 'R');
        $pdf->Cell(21, 5, utf8_decode(number_format($t_rpliquidado, 2, ',', ".")), 1, 0, 'R');
        $pdf->Cell(21, 5, utf8_decode(number_format($t_rppago, 2, ',', ".")), 1, 0, 'R');

        $nome_arquivo = str_replace('/', '', $contrato->numero) . ' - ' . str_replace(' ', '_', $contrato->fornecedor->nome) . '.pdf';

        $pdf->Output('D', $nome_arquivo);
    }

    private function calculaLinhasMultiCell($qtdcaracter, $ultimamedida)
    {
        $div = $qtdcaracter / 100;
        $ndiv = explode('.', $div);
        $linha = $ndiv[0] + 2;
        $tam = $linha * 5;
        $tamanho = $ultimamedida + $tam;

        return $tamanho;
    }

    public function notificaUsers()
    {
        $alerta_mensal = new AlertaContratoJob();

        return redirect()->back();
    }

    // retorna as autoridades signatárias da unidade
    public function getAutoridadesSignatarias($unidade_id)
    {
        $arrayAutoridadesSignatarias = Autoridadesignataria::where('unidade_id', $unidade_id)->where('ativo', true)->pluck('autoridade_signataria', 'id')->toArray();
        return $arrayAutoridadesSignatarias;
    }


    protected function adicionaCampos($modalidades)
    {
        $request = Request();

        $this->tab = 'Dados do contrato';

        $this->adicionaCampoFornecedor();
        $this->adicionarMinutasDeEmpenho();
        $this->adicionarFornecedoresSubcontratados();
        $this->adicionaCampoDataAssinatura();
        $this->adicionaCampoDataPublicacao();

        $this->adicionaCampoAutoridadesSignatarias();

        $this->adicionaCampoObjeto();
        $this->adicionaCampoInformacoesComplementares();
        $this->adicionaCampoModalidades($modalidades);
        $this->adicionaCampoAmparoLegal();
        $this->adicionaCampoContrataMaisBrasil();
        $this->adicionaContratacao();
        $this->adicionaCampoNumeroLicitacao();
        $this->adicionaCampoUnidadeCompra();
        $this->adicionaCampoUnidadeBeneficiaria();
        $this->adicionaCampoEmElaboracao();

        $this->tab = 'Características do contrato';

        $this->adicionaCampoReceitaDespesa();
        $this->adicionaCampoTipo();
        $this->adicionaCampoAdicionaCronograma();
        $this->adicionaCampoSubTipo();
        $this->adicionaCampoCategoria();
        $this->adicionaCampoAplicavelDecreto();
        $this->adicionaCampoSubCategoria();
        $this->adicionaCampoNumeroContrato();
        $this->adicionaCampoCodigoSistemaExterno();
        $this->adicionaCampoProcesso();
        $this->adicionaCampoUnidadeGestoraOrigem();
        $this->adicionaCampoUnidadeGestoraAtual();
        $this->adicionaCampoUnidadeRequisitante();
        $this->adicionaCampoProrrogavel();

        $this->adicionaCampoIsPrazoIndefinido();
        $this->adicionaCampoSituacao();

        $this->tab = 'Itens do contrato';

        $this->adicionaCampoItensContrato();
        $this->adicionaCampoItensCompra();
        $this->adicionaCampoRecuperaGridItens();

        $this->tab = 'Empenhos';

        $this->adicionaCampoUnidadeEmitenteEmpenho();
        $this->adicionaCampoFornecedorEmpenho();
        $this->adicionaCampoEmpenho();

        $this->tab = 'Vigência / Valores';

        $this->adicionaCampoDataPropostaComercial();
        $this->adicionaCampoDataVigenciaInicio();
        $this->adicionaCampoDataVigenciaTermino();
        $this->adicionaCampoValorGlobal();
        $this->adicionaCampoNumeroParcelas();
        $this->adicionaCampoValorParcela();
    }

    protected function adicionaColunas()
    {
        $this->adicionaColunaReceitaDespesa();
        $this->adicionaColunaNumeroInstrumento();
        $this->adicionaColunaCodigoSistemaExterno();
        $this->adicionaColunaUnidadeOrigem();
        $this->adicionaColunaUnidadeGestora();
        $this->adicionaColunaUnidadeRequisitante();
        $this->adicionaColunaTipo();
        $this->adicionaColunaSubTipo();
        $this->adicionaColunaCategoria();
        $this->adicionaColunaSubCategoria();
        $this->adicionaColunaFornecedor();
        $this->adicionaColunaProcesso();
        $this->adicionaColunaObjeto();
        $this->adicionaColunaDataPropostaComercial();
        $this->adicionaColunaInformacoesComplementares();
        $this->adicionaColunaVigenciaInicio();

        $this->adicionaColunaVigenciaTermino();
        $this->adicionaColunaValorGlobal();

        $this->adicionaColunaNumeroParcelas();
        $this->adicionaColunaValorParcela();
        $this->adicionaColunaValorAcumulado();
        $this->adicionaColunaTotalDespesasAcessorias();
        $this->adicionaColunaSituacao();
        $this->adicionaColunaProrrogavel();

        $this->adicionaColunaModalidadeCompra();
        $this->adicionaColunaAmparoLegal();
        $this->adicionaColunaNumeroCompra();
        $this->adicionaColunaUnidadeCompra();
        $this->adicionaColunaCriadoEm();
        $this->adicionaColunaAtualizadoEm();
        $this->adicionaColunaPorteFornecedor();
        $this->adicionaColunaMeiFornecedor();
        $this->adicionaColunaElaboracao();
    }

    protected function aplicaFiltros($modalidades)
    {
        // TODO: Melhor consulta do filtro de fornecedores, para não buscar a base inteira, mas sim apenas
        //       os fornecedores dos contratos da unidade ativa!
        $this->aplicaFiltroReceitaDespesa();
        $this->aplicaFiltroTipo();
        $this->aplicaFiltroCategoria();
        $this->aplicaFiltroDataVigenciaInicio();
        $this->aplicaFiltroDataVigenciaTermino();
        $this->aplicaFiltroValorGlobal();
        $this->aplicaFiltroValorParcela();
        $this->aplicaFiltroSituacao();
        $this->aplicaFiltroAmparoLegal();
        if (backpack_user()->hasRole('Administrador')) {
            $this->aplicaFiltroModalideCompra($modalidades);
            $this->aplicaFiltroNumeroCompra();
            $this->aplicaFiltroUnidadeCompra();
        }
    }

    protected function adicionaCampoFornecedor(): void
    {
        $this->crud->addField($this->contratoService->retornaCampoFornecedor($this->tab));
    }

    protected function adicionarMinutasDeEmpenho(): void
    {
        $this->crud->addField($this->contratoService->retornaCampoMinutasEmpenho($this->tab));
    }

    protected function adicionarFornecedoresSubcontratados(): void
    {
        $this->crud->addField($this->contratoService->retornaCampoSubcontratados($this->tab));
    }

    protected function adicionaCampoDataAssinatura(): void
    {
        $this->crud->addField($this->contratoService->retornaCampoAssinatura($this->tab));
    }

    protected function adicionaCampoDataPublicacao(): void
    {
        $this->crud->addField($this->contratoService->retornaCampoDataPublicacao($this->tab));
    }

    protected function adicionaCampoAutoridadesSignatarias(): void
    {
        $unidade_id = session('user_ug_id');

        $this->crud->addField(
            $this->contratoService->retornaCampoAutoridades($unidade_id, $this->tab)
        );
    }

    protected function adicionaCampoObjeto(): void
    {
        $this->crud->addField($this->contratoService->retornaCampoObjeto($this->tab));
    }

    protected function adicionaCampoInformacoesComplementares(): void
    {
        $this->crud->addField($this->contratoService->retornaCampoInfoComplementar($this->tab));
    }

    protected function adicionaCampoModalidades($modalidades): void
    {
        $this->crud->addField($this->contratoService->retornaCampoModalidade($modalidades, $this->tab));
    }

    protected function adicionaCampoRecuperaGridItens(): void
    {
        $this->crud->addField([
            'label' => "adicionaCampoRecuperaGridItens",
            'type' => "hidden",
            'name' => 'adicionaCampoRecuperaGridItens',
            'default' => "{{old('name')}}",
            'tab' => $this->tab
        ]);
    }

    protected function adicionaCampoEmpenho()
    {
        $this->crud->addField([
            'label' => "Empenho &nbsp;<button type='button' id='buscarEmpenho' class='btn btn-sm btn-default ng-scope'>
                        <i class='fa fa-plus'></i> Adicionar</button>",
            'name' => 'search_empenho_field',
            'type' => 'text',
            'tab' => $this->tab,
        ]);
    }

    protected function adicionaCampoFornecedorEmpenho()
    {

        $this->crud->addField([
            'label' => "Fornecedor do Empenho",
            'type' => "select2_from_ajax_multiple_alias",
            'name' => 'fornecedor_empenho',
            'entity' => 'fornecedor',
            'attribute' => "cpf_cnpj_idgener",
            'attribute2' => "nome",
            'process_results_template' => 'gescon.process_results_fornecedor',
            'model' => "App\Models\Fornecedor",
            'data_source' => url("api/fornecedor"),
            'placeholder' => "Selecione o fornecedor",
            'minimum_input_length' => 2,
            'tab' => $this->tab
        ]);
    }

    protected function adicionaCampoUnidadeEmitenteEmpenho()
    {

        $this->crud->addField([
            'label' => "Unidade Emitente do Empenho",
            'type' => "select2_from_ajax_multiple_alias",
            'name' => 'unidadesdescentralizadas',
            'entity' => 'unidade',
            'attribute' => "codigo",
            'attribute2' => "nomeresumido",
            'process_results_template' => 'gescon.process_results_unidade',
            'model' => "App\Models\Unidade",
            'data_source' => url("api/unidade"),
            'placeholder' => "Selecione a Unidade",
            'minimum_input_length' => 2,
            'tab' => $this->tab,
        ]);
    }

    protected function adicionaCampoIsPrazoIndefinido()
    {
        $this->crud->addField(
            [
                'name' => 'is_prazo_indefinido',
                'label' => "Prazo indeterminado?",
                'type' => 'radio',
                'options' => [1 => 'Sim', 0 => 'Não'],
                'allows_null' => false,
                'allows_multiple' => false, // OPTIONAL; needs you to cast this to array in your model;
                'inline' => true,
                'default' => 0,
                'wrapperAttributes' => [
                    'class' => 'form-group col-xs-12 com-sem-valor'
                ],
                'tab' => $this->tab
            ]
        );
    }

    protected function adicionaCampoNumeroLicitacao(): void
    {
        $this->crud->addField($this->contratoService->retornaCampoNumeroLicitacao($this->tab));
    }

    protected function adicionaCampoAmparoLegal()
    {
        $this->crud->addField($this->contratoService->retornaCampoAmparos($this->tab));
    }

    protected function adicionaCampoUnidadeCompra(): void
    {
        $this->crud->addField($this->contratoService->retornaCampoUnidadeCompra($this->tab));
    }

    protected function adicionaCampoUnidadeBeneficiaria(): void
    {
        $this->crud->addField($this->contratoService->retornaCampoUnidadeBeneficiaria($this->tab));
    }

    protected function adicionaCampoReceitaDespesa(): void
    {
        $this->crud->addField($this->contratoService->retornaCampoReceitaDespesa($this->tab));
    }

    protected function adicionaCampoTipo(): void
    {
        $this->crud->addField($this->contratoService->retornaCampoTipo($this->tab));
    }

    protected function adicionaCampoAdicionaCronograma(): void
    {
        $this->crud->addField($this->contratoService->retornaCampoCronograma($this->tab));
    }

    protected function adicionaCampoSubTipo(): void
    {
        $this->crud->addField($this->contratoService->retornaCampoSubTipo($this->tab));
    }

    protected function adicionaCampoCategoria(): void
    {
        $this->crud->addField($this->contratoService->retornaCampoCategoria($this->tab));
    }

    protected function adicionaCampoAplicavelDecreto(): void
    {
        $this->crud->addField($this->contratoService->retornaCampoAplicavelDecreto($this->tab));
    }

    protected function adicionaCampoSubCategoria(): void
    {
        $this->crud->addField($this->contratoService->retornaCampoSubCategoria($this->tab));
    }

    protected function adicionaCampoNumeroContrato(): void
    {
        $this->crud->addField($this->contratoService->retornaCampoNumeroContrato($this->tab));
    }

    protected function adicionaCampoCodigoSistemaExterno(): void
    {
        $this->crud->addField($this->contratoService->retornaCampoCodigoSistemaExterno($this->tab));
    }

    protected function adicionaCampoProcesso(): void
    {
        $this->crud->addField($this->contratoService->retornaCampoProcesso($this->tab));
    }

    protected function adicionaCampoItensContrato(): void
    {
        $idMaterial = $this->retornaIdCodigoItem('Tipo CATMAT e CATSER', 'Material');
        $idServico = $this->retornaIdCodigoItem('Tipo CATMAT e CATSER', 'Serviço');

        $this->crud->addField([
            'name' => 'itens',
            'type' => 'itens_contrato_list',
            'label' => 'Teste',
            'tab' => $this->tab,
            'material' => $idMaterial,
            'servico' => $idServico,
        ]);
    }

    protected function adicionaCampoUnidadeGestoraOrigem(): void
    {
        $uasgUsaSisg = session()->get('user_ug_sisg');

        if ($uasgUsaSisg) {
            $textoExibicao = session()->get('user_ug') . "-" . session()->get('user_ug_nome_resumido');

            $this->crud->addField([
                'label' => "Unidade Gestora Origem do Contrato",
                'type' => "text",
                'name' => 'unidadeorigem_id_exibicao',
                'attributes' => ['readonly' => 'readonly'],
                'ico_help' => 'Unidade que Formalizou o Contrato!',
                'default' => $textoExibicao,
                'tab' => $this->tab
            ]);
            $this->crud->addField([
                'type' => "hidden",
                'name' => 'unidadeorigem_id',
                'default' => session()->get('user_ug_id'),
                'tab' => $this->tab
            ]);
        } else {
            $this->crud->addField([
                'label' => "Unidade Gestora Origem do Contrato",
                'type' => "select2_from_ajax_single_unidade_gestora_origem",
                'name' => 'unidadeorigem_id',
                'entity' => 'unidadeorigem',
                'attribute' => "codigo",
                'attribute2' => "nomeresumido",
                'process_results_template' => 'gescon.process_results_unidade',
                'model' => "App\Models\Unidade",
                'ico_help' => 'Unidade que Formalizou o Contrato!',
                'data_source' => url("api/unidade"),
                'placeholder' => "Selecione a Unidade que formalizou o contrato, por padrão busque o código (" . session()->get('user_ug') . ")",
                'minimum_input_length' => 2,
                'default' => session()->get('user_ug_id'),
                'tab' => $this->tab
            ]);
        }
    }

    protected function adicionaCampoUnidadeGestoraAtual()
    {
        $unidade = [session()->get('user_ug_id') => session()->get('user_ug')];

        $this->crud->addField([
            'name' => 'unidade_id',
            'label' => "Unidade Gestora Atual",
            'type' => 'select2_from_array_unidade_gestora',
            'options' => $unidade,
            'allows_null' => false,
            'tab' => $this->tab
        ]);
    }

    protected function adicionaCampoUnidadeRequisitante()
    {
        $this->crud->addField([
            'name' => 'unidades_requisitantes',
            'label' => 'Unidades Requisitantes',
            'type' => 'text',
            'tab' => $this->tab
        ]);
    }

    protected function adicionaCampoProrrogavel()
    {

        $this->crud->addField([
            'name' => 'prorrogavel',
            'label' => 'Prorrogável',
            'type' => 'select_from_array',
            'options' => [
                'n' => 'Não',
                's' => 'Sim'
            ],
            'allows_null' => true,
            'tab' => $this->tab
        ]);
    }

    protected function adicionaCampoSituacao()
    {
        // caso o contrato_id não chegue preenchido, quer dizer que é um novo contrato - não daremos a opção de inativá-lo.
        if ($this->crud->getCurrentEntryId()) {
            $this->crud->addField([
                'name' => 'situacao',
                'label' => "Situação",
                'type' => 'select_from_array',
                'options' => [1 => 'Ativo', 0 => 'Inativo'],
                'allows_null' => false,
                'tab' => $this->tab
            ]);
        } else {
            $this->crud->addField([
                'name' => 'situacao',
                'label' => "Situação",
                'type' => 'select_from_array',
                'options' => [1 => 'Ativo'],
                'allows_null' => false,
                'tab' => $this->tab
            ]);
        }
    }

    protected function adicionaCampoJustificativaContratoInativo()
    {
        // array com as justificativas para contrato inativo
        $arrayJustificativas = Contrato::getArrayComJustificativas();

        $this->crud->addField([
            'name' => 'justificativa_contrato_inativo_id',
            'label' => "Justificativa para Contrato Inativo",
            'attribute' => [
                'id' => 'justificativa_contrato_inativo_id'
            ],
            'type' => 'select_from_array',
            'options' => $arrayJustificativas,
            'allows_null' => true,
            'tab' => $this->tab
        ]);
    }

    protected function adicionaCampoDataPropostaComercial()
    {
        $this->crud->addField([
            'name' => 'data_proposta_comercial',
            'label' => 'Data da proposta',
            'type' => 'date',
            'tab' => $this->tab,
        ]);
    }

    protected function adicionaCampoDataVigenciaInicio()
    {
        $this->crud->addField([
            'name' => 'vigencia_inicio',
            'label' => 'Data de início da vigência',
            'type' => 'date',
            'tab' => $this->tab
        ]);
    }

    /**
     * Adiciona Campo no modal de Ativar Contrato Em Elaboração
     * @return void
     */
    protected function adicionaCampoDataVigenciaInicioHidden(): void
    {
        $this->crud->addField([
            'name' => 'vigencia_inicio',
            'label' => 'Data de início da vigência',
            'type' => 'hidden',
            'modal_elaboracao' => true,
        ]);
    }

    protected function adicionaCampoDataVigenciaTermino()
    {
        $this->crud->addField([
            'name' => 'vigencia_fim',
            'label' => 'Data do término da vigência',
            'type' => 'date',
            'tab' => $this->tab
        ]);
    }

    protected function adicionaCampoValorGlobal()
    {
        $this->crud->addField([
            'name' => 'valor_global',
            'label' => 'Valor Global',
            'type' => 'number',
            'attributes' => [
                "step" => "0.01",
                'id' => 'valor_global',
                'step' => '0.0001',
                'readOnly' => 'readOnly',
            ],
            'prefix' => "R$",
            'tab' => $this->tab
        ]);
    }

    protected function adicionaCampoNumeroParcelas()
    {
        $this->crud->addField([
            'name' => 'num_parcelas',
            'label' => 'Núm. Parcelas',
            'type' => 'number',
            'default' => '1',
            'attributes' => [
                "step" => "any",
                "min" => '1',
            ],
            'tab' => $this->tab
        ]);
    }

    protected function adicionaCampoValorParcela()
    {
        $this->crud->addField([
            'name' => 'valor_parcela',
            'label' => 'Valor Parcela',
            'type' => 'number',
            'attributes' => [
                "step" => "0.01",
                'id' => 'valor_parcela',
                'step' => '0.0001',
                'readOnly' => 'readOnly',
            ],
            'prefix' => "R$",
            'tab' => $this->tab
        ]);
    }

    protected function adicionaColunaModalidadeCompra(): void
    {
        $this->crud->addColumn([
            'name' => 'getModalidade',
            'label' => 'Modalidade da Compra',
            'type' => 'model_function',
            'function_name' => 'getModalidade',
            'orderable' => true,
            'visibleInTable' => false,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
            'searchLogic' => function (Builder $query, $column, $searchTerm) {
                $query->orWhere('codigoitens.descricao', 'ilike', "%$searchTerm%");
            },
        ]);
    }

    protected function adicionaColunaAmparoLegal(): void
    {
        $this->crud->addColumn([
            'name' => 'retornaAmparo',
            'label' => 'Amparo Legal',
            'type' => 'model_function',
            'function_name' => 'retornaAmparo',
            'orderable' => true,
            'visibleInTable' => false,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true
        ]);
    }

    protected function adicionaColunaNumeroCompra()
    {
        $this->crud->addColumn([
            'name' => 'getNumeroCompra',
            'label' => 'Número Compra',
            'type' => 'model_function',
            'function_name' => 'getNumeroCompra',
            'orderable' => true,
            'visibleInTable' => false,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
            'searchLogic' => function (Builder $query, $column, $searchTerm) {
                $query->orWhere('contratos.licitacao_numero', 'ilike', "%$searchTerm%");
            },
        ]);
    }

    protected function adicionaColunaReceitaDespesa()
    {
        $this->crud->addColumn([
            'name' => 'getReceitaDespesa',
            'label' => 'Receita / Despesa',
            'type' => 'model_function',
            'function_name' => 'getReceitaDespesa',
            'orderable' => true,
            'visibleInTable' => false,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true
        ]);
    }

    protected function adicionaColunaNumeroInstrumento()
    {
        $this->crud->addColumn([
            'name' => 'numero',
            'label' => 'Número do instrumento',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true
        ]);
    }

    protected function adicionaColunaUnidadeOrigem()
    {
        $this->crud->addColumn([
            'name' => 'getUnidadeOrigem',
            'label' => 'Unidade Gestora Origem do Contrato',
            'type' => 'model_function',
            'function_name' => 'getUnidadeOrigem',
            'orderable' => true,
            'visibleInTable' => false,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true
        ]);
    }

    protected function adicionaColunaUnidadeCompra()
    {
        $this->crud->addColumn([
            'name' => 'getUnidadeCompra',
            'label' => 'Unidade da Compra',
            'type' => 'model_function',
            'function_name' => 'getUnidadeCompra',
            'orderable' => true,
            'visibleInTable' => false,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
            'searchLogic' => function (Builder $query, $column, $searchTerm) {
                $query->orWhere('uc.codigo', '=', $searchTerm);
            },
        ]);
    }

    protected function adicionaColunaUnidadeGestora()
    {
        $this->crud->addColumn([
            'name' => 'getUnidade',
            'label' => 'Unidade Gestora Atual',
            'type' => 'model_function',
            'function_name' => 'getUnidade',
            'orderable' => true,
            'visibleInTable' => false,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true
        ]);
    }

    protected function adicionaColunaUnidadeRequisitante()
    {
        $this->crud->addColumn([
            'name' => 'unidades_requisitantes',
            'label' => 'Unidades Requisitantes',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => false,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true
        ]);
    }

    protected function adicionaColunaTipo()
    {
        $this->crud->addColumn([
            'name' => 'getTipo',
            'label' => 'Tipo',
            'type' => 'model_function',
            'function_name' => 'getTipo',
            'orderable' => true,
            'visibleInTable' => false,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true
        ]);
    }

    protected function adicionaColunaSubTipo()
    {
        $this->crud->addColumn([
            'name' => 'subtipo',
            'label' => 'Subtipo',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => false,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true
        ]);
    }

    protected function adicionaColunaCategoria()
    {
        $this->crud->addColumn([
            'name' => 'getCategoria',
            'label' => 'Categoria',
            'type' => 'model_function',
            'function_name' => 'getCategoria',
            'orderable' => true,
            'visibleInTable' => false,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true
        ]);
    }

    protected function adicionaColunaSubCategoria()
    {
        $this->crud->addColumn([
            'name' => 'getSubCategoria',
            'label' => 'Subcategoria',
            'type' => 'model_function',
            'function_name' => 'getSubCategoria',
            'orderable' => true,
            'visibleInTable' => false,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true
        ]);
    }

    protected function adicionaColunaFornecedor()
    {
        $this->crud->addColumn([
            'name' => 'getFornecedor',
            'label' => 'Fornecedor',
            'type' => 'model_function',
            'function_name' => 'getFornecedor',
            'orderable' => true,
            'limit' => 50,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
            'searchLogic' => function (Builder $query, $column, $searchTerm) {
                $query->orWhere('fornecedores.cpf_cnpj_idgener', 'like', "%" . strtoupper($searchTerm) . "%");
                $query->orWhere('fornecedores.nome', 'like', "%" . strtoupper($searchTerm) . "%");
            },
        ]);
    }

    protected function adicionaColunaProcesso()
    {
        $this->crud->addColumn([
            'name' => 'processo',
            'label' => 'Processo',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => false,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true
        ]);
    }

    protected function adicionaColunaObjeto()
    {
        $this->crud->addColumn([
            'name' => 'objeto',
            'label' => 'Objeto',
            'type' => 'text',
            'limit' => 1000,
            'orderable' => true,
            'visibleInTable' => false,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true
        ]);
    }

    protected function adicionaColunaInformacoesComplementares()
    {
        $this->crud->addColumn([
            'name' => 'info_complementar',
            'label' => 'Informações Complementares',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => false,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true
        ]);
    }

    protected function adicionaColunaVigenciaInicio()
    {
        $this->crud->addColumn([
            'name' => 'vigencia_inicio',
            'label' => 'Vig. Início',
            'type' => 'date',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true
        ]);
    }

    protected function adicionaColunaVigenciaTermino()
    {

        // retorna string 'indeterminado se o contrato for prazo indeterminado
        $this->crud->addColumn([
            'name' => 'vigencia_fim',
            'label' => 'Vig. Fim',
            'type' => 'model_function',
            'function_name' => 'getVigenciaFimPrazoIndeterminado',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true
        ]);
    }

    protected function adicionaColunaValorGlobal()
    {
        $this->crud->addColumn([
            'name' => 'formatVlrGlobal',
            'label' => 'Valor Global',
            'type' => 'model_function',
            'function_name' => 'formatVlrGlobal',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true
        ]);
    }

    protected function adicionaColunaNumeroParcelas()
    {
        // retorna string 'indeterminado se o contrato for prazo indeterminado
        $this->crud->addColumn([
            'name' => 'num_parcelas',
            'label' => 'Núm. Parcelas',
            'type' => 'model_function',
            'function_name' => 'getNumeroParcelasPrazoIndeterminado',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true
        ]);
    }

    protected function adicionaColunaValorParcela()
    {
        $this->crud->addColumn([
            'name' => 'formatVlrParcela',
            'label' => 'Valor Parcela',
            'type' => 'model_function',
            'function_name' => 'formatVlrParcela',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true
        ]);
    }

    protected function adicionaColunaValorAcumulado()
    {
        $this->crud->addColumn([
            'name' => 'formatVlrAcumulado',
            'label' => 'Valor Acumulado',
            'type' => 'model_function',
            'function_name' => 'formatVlrAcumulado',
            'orderable' => true,
            'visibleInTable' => false,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true
        ]);
    }

    protected function adicionaColunaTotalDespesasAcessorias()
    {
        $this->crud->addColumn([
            'name' => 'formatTotalDespesasAcessorias',
            'label' => 'Total Despesas Acessórias',
            'type' => 'model_function',
            'function_name' => 'formatTotalDespesasAcessorias',
            'orderable' => true,
            'visibleInTable' => false,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true
        ]);
    }

    protected function adicionaColunaSituacao()
    {
        $this->crud->addColumn([
            'name' => 'getSituacao',
            'label' => 'Situação',
            'type' => 'model_function',
            'function_name' => 'getSituacao',
            'orderable' => true,
            'visibleInTable' => false,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true
        ]);
    }

    protected function adicionaColunaProrrogavel()
    {
        $this->crud->addColumn([
            'name' => 'prorrogavelColuna',
            'label' => 'Prorrogável',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => false,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
            'options' => [null => '', 0 => 'Não', 1 => 'Sim']
        ]);
    }

    protected function adicionaColunaCriadoEm()
    {
        $this->crud->addColumn([
            'name' => 'created_at',
            'label' => 'Criado em',
            'type' => 'datetime',
            'orderable' => true,
            'visibleInTable' => false,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
            'searchLogic' => $this->retornaSearchData('contratos', 'created_at')
        ]);
    }

    protected function adicionaColunaDataPropostaComercial()
    {
        $this->crud->addColumn([
            'name' => 'data_proposta_comercial',
            'label' => 'Data Proposta Comercial',
            'type' => 'datetime',
            'orderable' => true,
            'visibleInTable' => false,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
        ]);
    }

    protected function adicionaColunaCodigoSistemaExterno()
    {
        $this->crud->addColumn([
            'name' => 'codigo_sistema_externo',
            'label' => 'Código Sistema Externo',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => false,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
        ]);
    }

    protected function adicionaColunaAtualizadoEm()
    {
        $this->crud->addColumn([
            'name' => 'updated_at',
            'label' => 'Atualizado em',
            'type' => 'datetime',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
            'searchLogic' => $this->retornaSearchData('contratos', 'updated_at')
        ]);
    }

    protected function adicionaColunaPorteFornecedor()
    {
        $this->crud->addColumn([
            'name' => 'fornecedorContratosHistorico.porte',
            'label' => 'Porte',
            'type' => 'text',
            'value' => 'teste',
            'orderable' => false,
            'visibleInTable' => false,
            'visibleInModal' => false,
            'visibleInExport' => true,
            'visibleInShow' => true,
        ]);
    }

    protected function adicionaColunaMeiFornecedor()
    {
        $this->crud->addColumn([
            'name' => 'fornecedorContratosHistorico.mei',
            'label' => 'Mei',
            'type' => 'text',
            'value' => 'teste',
            'orderable' => false,
            'visibleInTable' => false,
            'visibleInModal' => false,
            'visibleInExport' => true,
            'visibleInShow' => true,
        ]);
    }

    protected function adicionaCampoContrataMaisBrasil(): void
    {
        $this->crud->addField($this->contratoService->retornaCampoContrataMaisBrasil($this->tab));
    }


    protected function adicionaContratacao(): void
    {
        $this->crud->addField($this->contratoService->retornaCampoContratacaoPNCP($this->tab));
    }

    protected function adicionaColunaElaboracao()
    {
//        $this->crud->addColumn([
//            'name' => 'elaboracao',
//            'label' => 'Em elaboração',
//            'type' => 'text',
//            'orderable' => true,
//            'visibleInTable' => false,
//            'visibleInModal' => true,
//            'visibleInExport' => true,
//            'visibleInShow' => true,
//            'options' => [null => 'Não', false => 'Não', true => 'Sim']
//        ]);
    }

    protected function aplicaFiltroFornecedor()
    {
        $this->crud->addFilter(
            [
                'name' => 'fornecedor',
                'type' => 'select2_multiple',
                'label' => 'Fornecedor'
            ],
            $this->retornaFornecedores(),
            function ($value) {
                $this->crud->addClause(
                    'whereIn',
                    'fornecedores.cpf_cnpj_idgener',
                    json_decode($value)
                );
            }
        );
    }

    protected function aplicaFiltroReceitaDespesa()
    {
        $this->crud->addFilter(
            [
                'name' => 'receita_despesa',
                'type' => 'select2_multiple',
                'label' => 'Receita / Despesa'
            ],
            [
                'R' => 'Receita',
                'D' => 'Despesa',
                'S' => 'Sem ônus',
//                'X' => 'Receita e Despesa',
            ],
            function ($value) {
                $this->crud->addClause(
                    'whereIn',
                    'contratos.receita_despesa',
                    json_decode($value)
                );
            }
        );
    }

    protected function aplicaFiltroTipo()
    {
        $this->crud->addFilter(
            [
                'name' => 'tipo_contrato',
                'type' => 'select2_multiple',
                'label' => 'Tipo'
            ],
            $this->contratoService->retornaTipos(),
            function ($value) {
                $this->crud->addClause(
                    'whereIn',
                    'contratos.tipo_id',
                    json_decode($value)
                );
            }
        );
    }

    protected function aplicaFiltroCategoria()
    {
        $this->crud->addFilter(
            [
                'name' => 'categorias',
                'type' => 'select2_multiple',
                'label' => 'Categorias'
            ],
            $this->retornaCategorias(),
            function ($values) {
                $this->crud->addClause(
                    'whereIn',
                    'contratos.categoria_id',
                    json_decode($values)
                );
            }
        );
    }

    protected function aplicaFiltroDataVigenciaInicio()
    {
        $this->crud->addFilter(
            [
                'type' => 'date_range',
                'name' => 'vigencia_inicio',
                'label' => 'Vigência Inicio'
            ],
            false,
            function ($value) {
                $dates = json_decode($value);

                $this->crud->addClause('where', 'contratos.vigencia_inicio', '>=', $dates->from);
                $this->crud->addClause('where', 'contratos.vigencia_inicio', '<=', $dates->to . ' 23:59:59');
            }
        );
    }

    protected function aplicaFiltroDataVigenciaTermino()
    {
        $this->crud->addFilter(
            [
                'type' => 'date_range',
                'name' => 'vigencia_fim',
                'label' => 'Vigência Fim'
            ],
            false,
            function ($value) {
                $dates = json_decode($value);

                $this->crud->addClause('where', 'contratos.vigencia_fim', '>=', $dates->from);
                $this->crud->addClause('where', 'contratos.vigencia_fim', '<=', $dates->to . ' 23:59:59');
            }
        );
    }

    protected function aplicaFiltroValorGlobal()
    {
        $this->crud->addFilter(
            [
                'name' => 'valor_global',
                'type' => 'range',
                'label' => 'Valor Global',
                'label_from' => 'Vlr Mínimo',
                'label_to' => 'Vlr Máximo'
            ],
            false,
            function ($value) {
                $range = json_decode($value);

                if ($range->from) {
                    $this->crud->addClause('where', 'contratos.valor_global', '>=', (float)$range->from);
                }
                if ($range->to) {
                    $this->crud->addClause('where', 'contratos.valor_global', '<=', (float)$range->to);
                }
            }
        );
    }

    protected function aplicaFiltroValorParcela()
    {
        $this->crud->addFilter(
            [
                'name' => 'valor_parcela',
                'type' => 'range',
                'label' => 'Valor Parcela',
                'label_from' => 'Vlr Mínimo',
                'label_to' => 'Vlr Máximo'
            ],
            false,
            function ($value) {
                $range = json_decode($value);

                if ($range->from) {
                    $this->crud->addClause('where', 'contratos.valor_parcela', '>=', (float)$range->from);
                }
                if ($range->to) {
                    $this->crud->addClause('where', 'contratos.valor_parcela', '<=', (float)$range->to);
                }
            }
        );
    }

    protected function aplicaFiltroSituacao()
    {
        $this->crud->addFilter(
            [
                'name' => 'situacao',
                'type' => 'select2_multiple',
                'label' => 'Situação'
            ],
            [
                '1' => 'Ativo',
                'Inativo' => 'Inativo',
                'Rescindido' => 'Rescindido',
                'Encerrado' => 'Encerrado'
            ],
            function ($value) {
                #895

                $ativoInativo = str_replace(['[', '"', ']', 'Rescindido', 'Encerrado'], "", $value);
                $statusAtivoInativo = str_replace("Inativo", "0", $ativoInativo);
                $statusGerais = str_replace(['[', '"', ']', '1', 'Inativo'], "", $value);

                $arrayAtivoInativo = array_filter(explode(",", $statusAtivoInativo), function ($valor) {
                    return $valor !== "";
                });

                $arrayAtivoInativoEncode = json_encode($arrayAtivoInativo);
                $situacoesFiltradas = array_filter(explode(",", $statusGerais), function ($valor) {
                    return $valor !== "";
                });

                //Verifica se possui apenas o status inativo dentro do array e nenhuma outra situação
                if (count($arrayAtivoInativo) === 1 && $arrayAtivoInativo[0] == "0" && count($situacoesFiltradas) === 0) {
                    $this->crud->addClause(
                        'whereIn',
                        'contratos.situacao',
                        json_decode($arrayAtivoInativoEncode)
                    );
                }
                //Verifica se possui apenas o status ativo dentro do array e nenhuma outra situação
                if (count($arrayAtivoInativo) === 1 && $arrayAtivoInativo[0] == "1" && count($situacoesFiltradas) === 0) {
                    $this->crud->addClause(
                        'whereIn',
                        'contratos.situacao',
                        json_decode($arrayAtivoInativoEncode)
                    );
                }

                $this->crud->addClause('leftjoin', 'codigoitens as justificativa', 'justificativa.id', '=', 'contratos.justificativa_contrato_inativo_id');
                if (json_decode(json_encode($situacoesFiltradas), true)) {
                    $this->crud->addClause('where', function ($q) use ($arrayAtivoInativoEncode, $situacoesFiltradas, $arrayAtivoInativo) {
                        $q->orWhereIn('justificativa.descricao', json_decode(json_encode($situacoesFiltradas), true));
                        if (count($arrayAtivoInativo) > 0) {
                            $q->whereIn('contratos.situacao', json_decode($arrayAtivoInativoEncode));
                        }
                    });
                }
            }
        );
    }

    protected function aplicaFiltroModalideCompra($modalidades)
    {
        $this->crud->addFilter(
            [
                'name' => 'modalidade_compra',
                'type' => 'select2_multiple',
                'label' => 'Modalidade Compra'
            ],
            $modalidades,
            function ($values) {
                $this->crud->addClause(
                    'whereIn',
                    'contratos.modalidade_id',
                    json_decode($values)
                );
            }
        );
    }

    protected function aplicaFiltroAmparoLegal()
    {
        $this->crud->addFilter(
            [
                'name' => 'amparo_legal',
                'type' => 'select2_ajax',
                'label' => 'Amparo Legal',
                'placeholder' => 'Selecione o Amparo Legal',
                'data_source' => url('api/amparolegalfiltercontratos'),
                'attribute' => 'campo_api_amparo',
            ],
            false,
            function ($values) {
                $options = AmparoLegal::select('amparo_legal_contrato.contrato_id')
                    ->join('amparo_legal_contrato', 'amparo_legal_contrato.amparo_legal_id', 'amparo_legal.id')
                    ->join('contratos', 'contratos.id', 'amparo_legal_contrato.contrato_id')
                    ->where('amparo_legal.ato_normativo', 'ilike', '%' . strtoupper($values) . '%')
                    ->where('contratos.unidade_id', '=', session()->get('user_ug_id'))
                    ->groupby('amparo_legal_contrato.contrato_id')
                    ->get()->toArray();

                $this->crud->addClause(
                    'whereIn',
                    'contratos.id',
                    $options
                );
            }
        );
    }

    protected function aplicaFiltroNumeroCompra()
    {
        $this->crud->addFilter(
            [
                'name' => 'numerocompra',
                'type' => 'text',
                'label' => 'Número Compra'
            ],
            false,
            function ($value) {
                $this->crud->addClause('where', 'contratos.licitacao_numero', 'ilike', "%$value%");
            }
        );
    }

    protected function aplicaFiltroUnidadeCompra()
    {
        $this->crud->addFilter(
            [
                'name' => 'unidade_compra',
                'type' => 'select2_ajax',
                'label' => 'Unidade Compra',
                'placeholder' => 'Selecione a Unidade da Compra',
                'data_source' => url('api/unidadefiltercontratos'),
                'attribute' => 'descricao',
            ],
            false,
            function ($values) {
                $this->crud->addClause(
                    'whereIn',
                    'uc.id',
                    array(array((string)json_decode($values)))
                );
            }
        );
    }


    private function retornaFornecedores()
    {
        return Fornecedor::select(
            DB::raw("CONCAT(cpf_cnpj_idgener,' - ',nome) AS nome"),
            'cpf_cnpj_idgener'
        )
            ->whereHas(
                'contratos',
                function ($u) {
                    $u->where('situacao', true);
                    $u->where('unidade_id', session('user_ug_id'));
                }
            )
            ->pluck('nome', 'cpf_cnpj_idgener')
            ->toArray();
    }


    public function retornaCategorias()
    {
        return Codigoitem::whereHas('codigo', function ($query) {
            $query->where('descricao', '=', 'Categoria Contrato');
        })
            ->where('descricao', '<>', 'A definir')
            ->orderBy('descricao')
            ->pluck('descricao', 'id')
            ->toArray();
    }

    private function retornaModalidadeCompra()
    {
        return Codigoitem::whereHas('codigo', function ($query) {
            $query->where('descricao', '=', 'Modalidade Licitação');
        })
            ->where('codigoitens.visivel', true)
            ->orderBy('codigoitens.descricao')
            ->pluck('codigoitens.descricao', 'codigoitens.id')
            ->toArray();
    }

    public function destroy($id)
    {
        $this->crud->hasAccessOrFail('delete');
        $this->crud->setOperation('delete');

        // get entry ID from Request (makes sure its the last ID for nested resources)
        $id = $this->crud->getCurrentEntryId() ?? $id;

        $contratos = Contrato::where('id', $id)->has('minutasempenho')->get();
        $minutas = MinutaEmpenho::where('contrato_id', $id)->get();

        //SE NÃO HOUVER CONTRATO VINCULADO A MINUTA, DEIXA DELETAR
        if ($contratos->isEmpty() && $minutas->isEmpty()) {
            return $this->crud->delete($id);
        }

        return 'delete_confirmation_not_deleted_message_vinculacao';
    }

    public function consultaCompraSiasg_antigo($objParams)
    {
        (isset($objParams->unidade_id))
            ? $objParams->unidade_id = $objParams->unidade_id
            : $objParams->unidade_id = $objParams->unidade_origem_id;

        (isset($objParams->licitacao_numero))
            ? $objParams->licitacao_numero = $objParams->licitacao_numero
            : $objParams->licitacao_numero = $objParams->numero_ano;

        if (isset($objParams->unidadecompra_id)) {
            $objParams->unidade_id = $objParams->unidadecompra_id;
        }

        $modalidade = Codigoitem::find($objParams->modalidade_id);
        $uasgCompra = Unidade::find($objParams->unidade_id);
        $numero_ano = explode('/', $objParams->licitacao_numero);

        $apiSiasg = new ApiSiasg();

        $params = [
            'modalidade' => $modalidade->descres,
            'numeroAno' => $numero_ano[0] . $numero_ano[1],
            'uasgCompra' => $uasgCompra->codigo,
            'uasgUsuario' => session('user_ug')
        ];

        $compra = json_decode($apiSiasg->executaConsulta('COMPRASISPP', $params));

        return $compra;
    }

    public function montaParametrosCompra($compraSiasg, $objParams): void
    {
        $unidade_subrogada = $compraSiasg->data->compraSispp->subrogada;

        $objParams->unidade_subrrogada_id = ($unidade_subrogada <> '000000') ? (int)$this->buscaIdUnidade($unidade_subrogada) : null;

        $origem = 2; // siasg

        if (isset($compraSiasg->data->api_origem)) {
            switch ($compraSiasg->data->api_origem) {
                case 'novo_divulgacao_compra':
                    $origem = 1;
                    break;
                case 'pncp':
                    $origem = 3;
                    break;
                default:
                    $origem = 2;
                    break;
            }
        }
        $objParams->origem = $origem;
        $objParams->tipo_compra_id = $this->contratoService->buscaTipoCompra($compraSiasg->data->compraSispp->tipoCompra);
        $objParams->inciso = $compraSiasg->data->compraSispp->inciso;
        $objParams->lei = $compraSiasg->data->compraSispp->lei;
        $objParams->id_unico = @$compraSiasg->data->compraSispp->idUnico;
        $objParams->cnpjOrgao = @$compraSiasg->data->compraSispp->cnpjOrgao;

        $artigoCompra = empty($compraSiasg->data->compraSispp->artigo) ? null : $compraSiasg->data->compraSispp->artigo;
        $objParams->artigo = $artigoCompra;
    }

    public function consultaApiCompraSiasg(\Illuminate\Http\Request $request)
    {
        if (is_array($request->objParams)) {
            $objParams = json_encode($request->objParams);
            $objParams = json_decode($objParams);
        } else {
            $objParams = json_decode($request->objParams);
        }

        if ($objParams->numero_contratacao !== null) {
            $retornoSiasg = $this->buscarCompra(
                '',
                '',
                '',
                '',
                '',
                '',
                $this->converterIdContratacaoPNCP($objParams->numero_contratacao)
            );

            if (isset($retornoSiasg->messagem) && $retornoSiasg->messagem === 'Sucesso') {
                $unidade = Unidade::where('codigo', $retornoSiasg->data->compraSispp->unidade)->first();

                $objParams->licitacao_numero = $retornoSiasg->data->compraSispp->numeroAno;
                $objParams->numero_ano = $retornoSiasg->data->compraSispp->numeroAno;
                $objParams->unidade_id = $unidade->id;
                $objParams->unidade_origem_id = $unidade->id;
                $objParams->origem = 3;
            }
        } else {
            $objParams->unidade_id = $objParams->unidade_id ?? $objParams->unidade_origem_id;
            $objParams->licitacao_numero = $objParams->licitacao_numero ?? $objParams->numero_ano;

            $modalidade = Codigoitem::find($objParams->modalidade_id);
            $uasgCompra = Unidade::find($objParams->unidade_id);
            $numero_ano = explode('/', $objParams->licitacao_numero);
            $retornoSiasg = $this->buscarCompra(
                $modalidade->descres,
                $uasgCompra->codigo,
                session()->get('user_ug'),
                null,
                $numero_ano[0],
                $numero_ano[1]
            );
        }

        try {
            if (is_null($retornoSiasg->data)) {
                $compra = $this->verificaCompraExiste([
                    'unidade_origem_id' => $objParams->unidade_origem_id,
                    'modalidade_id' => $objParams->modalidade_id,
                    'numero_ano' => $objParams->numero_ano
                ]);

                if (!$compra) {
                    $this->contratoService->validaRetornoSiasg($retornoSiasg);
                }
                $compraPertenceLei14133Derivadas = $this->amparoLegalService
                    ->existeAmparoLegalEmLeiCompra($compra['lei']);
                if ($compra->tipo_compra_desc === 'SISRP' && $compraPertenceLei14133Derivadas) {
                    return;
                }
            }

            $this->contratoService->validaRetornoSiasg($retornoSiasg);
        } catch (\Exception $e) {
            throw $e;
        }

        $this->montaParametrosCompra($retornoSiasg, $objParams);

        DB::beginTransaction();

        try {
            $compraPertenceLei14133Derivadas = $this->amparoLegalService
                ->existeAmparoLegalEmLeiCompra($retornoSiasg->data->compraSispp->lei);

            if ($retornoSiasg->data->compraSispp->tipoCompra == '2'
                && $compraPertenceLei14133Derivadas
            ) {
                return;
            }

            //todo trocar esta chamada para $this->compraService->updateOrCreateCompra
            $compra = $this->updateOrCreateCompra($objParams);

            if ($retornoSiasg->data->compraSispp->tipoCompra == 1) {
                $this->gravaParametroItensdaCompraSISPP($retornoSiasg, $compra);
            }

            if ($retornoSiasg->data->compraSispp->tipoCompra == 2) {
                $this->gravaParametroItensdaCompraSISRP($retornoSiasg, $compra);
            }

            DB::commit();
        } catch (Exception $exc) {
            DB::rollback();
        }
    }

    public function updateOrCreateCompra($objParams)
    {
        return Compra::updateOrCreate(
            [
                'unidade_origem_id' => (int)$objParams->unidade_origem_id,
                'modalidade_id' => (int)$objParams->modalidade_id,
                'numero_ano' => $objParams->numero_ano,
                'tipo_compra_id' => $objParams->tipo_compra_id
            ],
            [
                'unidade_subrrogada_id' => $objParams->unidade_subrrogada_id,
                'tipo_compra_id' => $objParams->tipo_compra_id,
                'inciso' => $objParams->inciso,
                'lei' => $objParams->lei,
                'id_unico' => $objParams->id_unico,
                'cnpjOrgao' => $objParams->cnpjOrgao,
                'numero_contratacao' => $objParams->numero_contratacao
            ]
        );
    }

    public function recuperaMinutasModal()
    {
        return $this->minutaService->getMinutasModalCriarContratoTipoEmpenho();
    }

    //148
    public function criarContratosTipoEmpenhoMinutasEmpenho(\Illuminate\Http\Request $request)
    {
        session()->forget('editando_contrato_elaboracao');
        $arrMinuta = $request->minuta;
        //dados para insereContrato()
        $cc = new ContratoControllerPNCP();
        $dcc = new DocumentoContratoController();
        $dtcc = new DocumentoTermoContratoController();
        $cscc = new CompraSiasgCrudController();

        //loop no array de minutas
        if ($arrMinuta != null) {
            $msgNaoCriados = '';
            $msgCriados = '';
            $msgErros = '';
            $errorCount = 0;

            foreach ($arrMinuta as $minutaId) {
                try {
                    DB::beginTransaction();

                    //busca minuta
                    $minutaEmpenho = MinutaEmpenho::where('id', '=', $minutaId)->first();

                    $podeEnviarPNCP = $this->pncpService->podeEnviarMinutaEmpenhoPNCP($minutaEmpenho);

                    /* 741 - impedir quando, um usuário em outra aba retirar a força de contrato da minuta no momento em
                    que a mesma já esteja carregada na modal de criação na tela dos contratos*/
                    if ($minutaEmpenho->minutaempenho_forcacontrato === 0) {
                        $msgNaoCriados .= '\n . A Minuta de Empenho ' .
                            $minutaEmpenho->mensagem_siafi .
                            ' não é mais substitutiva de contrato.\n';
                    }

                    //não pode repetir contrato tipo empenho dessa minuta e dessa unidade de origem
                    $modContrato = Contrato::whereHas('tipo', function ($t) {
                        $t->descricao = 'Empenho';
                    })
                        ->where('unidadeorigem_id', $minutaEmpenho->saldo_contabil->unidade_id)
                        ->where('numero', trim($minutaEmpenho->mensagem_siafi))
                        ->first();
                    //contrato tipo empenho já existente(mesmo nº da minuta - mensagem siafi
                    if ($modContrato !== null) {
                        $msgNaoCriados .= '\n . O Contrato ' . $modContrato->numero .
                            ' não foi salvo, pois já existe na base de dados.\n';
                    }

                    # Se o contrato já existir ou a minuta não tiver força de contrato, não tenta criar
                    if (($modContrato !== null) || ($minutaEmpenho->minutaempenho_forcacontrato !== 1)) {
                        continue;
                    }
                    //buscando código Tipo Contrato - Empenho
                    $idCodTipoEmpenho = Codigoitem::whereHas('codigo', function ($q) {
                        $q->where('descricao', 'Tipo de Contrato');
                    })
                        ->where('descricao', 'Empenho')
                        ->first()->id;

                    //cria contrato
                    $modNovoContrato = new Contrato();

                    //Aba Dados do Contrato
                    $modNovoContrato->fornecedor_id = $minutaEmpenho->fornecedor_empenho_id;
                    $modNovoContrato->data_assinatura = $minutaEmpenho->data_emissao;
                    if ($minutaEmpenho->amparo_legal->ato_normativo != 'LEI 14.133/2021')
                        $modNovoContrato->data_publicacao = $minutaEmpenho->data_emissao;
                    $modNovoContrato->objeto = $minutaEmpenho->descricao;
                    $modNovoContrato->info_complementar = $minutaEmpenho->informacao_complementar;
                    $modNovoContrato->unidadecompra_id = $minutaEmpenho->compra->unidade_origem_id;
                    $modNovoContrato->unidadebeneficiaria_id = $minutaEmpenho->compra->uasg_beneficiaria_id;
                    $modNovoContrato->modalidade_id = $minutaEmpenho->compra->modalidade_id;
                    $modNovoContrato->licitacao_numero = $minutaEmpenho->compra->numero_ano;
                    $request->request->set('amparoslegais', [$minutaEmpenho->amparo_legal_id]);

                    //Aba Características do Contrato
                    $modNovoContrato->receita_despesa = "D";
                    $modNovoContrato->tipo_id = $idCodTipoEmpenho;
                    $modNovoContrato->subtipo = null;
                    $modNovoContrato->numero = trim($minutaEmpenho->mensagem_siafi);
                    //Categoria: se minuta de empenho for de material será tipo "Compras",
                    // se for de serviço será "Serviços"
                    $catmatsergrupo =
                        $minutaEmpenho->compraItemMinutaEmpenho->first()->compra_item->catmatseritem->catmatsergrupo;
                    if ($catmatsergrupo->tipo->descricao == "Material") {
                        $modNovoContrato->categoria_id = Codigoitem::whereHas('codigo', function ($q) {
                            $q->where('descricao', 'Categoria Contrato');
                        })
                            ->where('descricao', 'Compras')
                            ->first()->id;
                    }
                    if ($catmatsergrupo->tipo->descricao == "Serviço") {
                        $modNovoContrato->categoria_id = Codigoitem::whereHas('codigo', function ($q) {
                            $q->where('descricao', 'Categoria Contrato');
                        })
                            ->where('descricao', 'Serviços')
                            ->first()->id;
                    }
                    $modNovoContrato->subcategoria_id = null;
                    $modNovoContrato->processo = $minutaEmpenho->processo;
                    $modNovoContrato->unidadeorigem_id = $minutaEmpenho->saldo_contabil->unidade_id; // Unidade emitente da minuta
                    $modNovoContrato->unidade_id = $minutaEmpenho->unidade_id;
                    $modNovoContrato->unidades_requisitantes = '';
                    $modNovoContrato->prorrogavel = false;
                    $modNovoContrato->situacao = true;

                    //Aba Vigência/Valores
                    $modNovoContrato->vigencia_inicio = $minutaEmpenho->data_emissao;
                    $modNovoContrato->vigencia_fim = substr(trim($minutaEmpenho->mensagem_siafi), 0, 4) .
                        '-12-31';
                    $modNovoContrato->valor_global = $minutaEmpenho->valor_total;
                    $modNovoContrato->valor_inicial = $minutaEmpenho->valor_total;//valor global
                    $modNovoContrato->num_parcelas = 1;
                    $modNovoContrato->valor_parcela = $minutaEmpenho->valor_total;

                    $compra = $minutaEmpenho->compra;
                    if ($compra->origem === 3) {
                        $modNovoContrato->contrata_mais_brasil = true;
                        $modNovoContrato->numero_contratacao = $compra->numero_contratacao;
                    }

                    /*
                    Antes de salvar o contrato, coloca a minuta em sessão: ao gerenciar o envio para o PNCP,
                    já deverá existir no PCNP (através do envio que foi feito pela minuta).
                    Se o link_pncp retornar vazio no header do retorno,
                    utiliza a minuta para pegar o link já salvo na base.
                    */
                    session()->put('minutaEmpenhoId', $minutaEmpenho->id);

                    //salvando o contrato
                    $modNovoContrato->save();

                    //salvando contrato minuta empenho
                    $minutaEmpenho->contrato()->save($modNovoContrato);

                    //salvando amparo legal contrato
                    $modNovoContrato->amparoslegais()->attach($minutaEmpenho->amparo_legal_id);

                    //Aba Itens do contrato
                    if ($request->session()->has('remessa_id')) {
                        // Removendo a sessão de remessa para entrar na condição correta do getItens()
                        // não é necessário mais a sessão aqui
                        $request->session()->forget('remessa_id');
                    }
                    $itensContrato = $this->minutaService->getItens($minutaEmpenho);

                    $itensArray = [];
                    foreach ($itensContrato as $itens) {

                        //salvar em contratoitens
                        $compraitem = CompraItem::where('id', $itens['compra_item_id'])->first();

                        $tipoMaterial = '';
                        if ($compraitem->tipo_item_id == 149) {
                            if (isset($itens['subelemento_id'])) {
                                $codNaturezaDespesa = Naturezadespesa::select("naturezadespesa.codigo")
                                    ->join('naturezasubitem',
                                        'naturezadespesa.id',
                                        '=',
                                        'naturezasubitem.naturezadespesa_id')
                                    ->where('naturezasubitem.id', $itens['subelemento_id'])->first();
                                if (isset($codNaturezaDespesa->codigo)) {
                                    if (substr($codNaturezaDespesa->codigo, 0, 2) == '33')
                                        $tipoMaterial = 'Consumo';
                                    elseif (substr($codNaturezaDespesa->codigo, 0, 2) == '44')
                                        $tipoMaterial = 'Permanente';
                                    else
                                        $tipoMaterial = "Não se aplica";
                                }
                            }
                        }

                        $itensArray[] = [
                            'contrato_id' => $modNovoContrato->id,
                            'tipo_id' => $compraitem->tipo_item_id,
                            'grupo_id' => $catmatsergrupo->id,
                            'catmatseritem_id' => $compraitem->catmatseritem_id,
                            'descricao_complementar' => $compraitem->descricaodetalhada,
                            'quantidade' => $itens['qtd_total_item'],
                            'valortotal' => $itens['vlr_total_item'],
                            'valorunitario' => $itens['vlr_unitario_item'],
                            'numero_item_compra' => $compraitem->numero,
                            'periodicidade' => 1,
                            'tipo_material' => $tipoMaterial
                        ];
                    }

                    //salvando os itens
                    foreach ($itensArray as $itens) {
                        Contratoitem::create($itens);
                    }

                    $request->request->set('minutasempenho', [$minutaId]);
                    $this->contratoService->vincularMinutaContratoHistorico($request->all(), $modNovoContrato->id, $modNovoContrato->unidade_id);

                    //741 Pega o amparo legal da minuta para enviar ao PNCP quando é 14.133
                    if ($podeEnviarPNCP) {

                        //Verificar se a minuta foi enviada para pncp (existe o sequencialPNCP preenchido e status diferente de EXCLUIDO)
                        if ($minutaEmpenho->minuta_enviada_pncp) {
                            $this->salvaArquivoEmpenhoComoArquivoContrato($minutaEmpenho, $modNovoContrato, true);
                        } else {
                            //quando não foi enviada ao pncp
                            $this->salvaArquivoEmpenhoComoArquivoContrato($minutaEmpenho, $modNovoContrato);
                        }
                    }//fim código de envio ao PNCP quando a minuta é 14.133

                    //741 Pega o amparo legal da minuta para não permitir enviar ao PNCP quando não é 14.133 mas é substitutiva de contrato
                    if (!$podeEnviarPNCP) {
                        $this->salvaArquivoEmpenhoComoArquivoContrato($minutaEmpenho, $modNovoContrato, true);
                    }

                    if ($modContrato === null) {
                        $msgCriados .= '\nContrato ' . trim($modNovoContrato->numero) . ' criado com sucesso;\n';
                    }


                    DB::commit();
                } catch (\Exception $e) {
                    $errorCount++;
                    $busca = $e->getMessage();
                    $search = "message";
                    # Se erro no PNCP
                    $numeroMinuta = trim($minutaEmpenho->mensagem_siafi);

                    if (str_contains($busca, $search)) {
                        $msgErros .= '\n\Minuta que falhou: ' . $numeroMinuta
                            . '\n PNCP '
                            . str_before(
                                str_replace(
                                    '"',
                                    '',
                                    stristr($e->getMessage(), "message")
                                ), ",") . '\n';
                        $this->inserirLogCustomizado('pncp', 'error', $e);
                    }

                    # Se erro no código da aplicação
                    if (!str_contains($busca, $search)) {
                        $msgErros .= "\n Falha na operação, o contrato não foi salvo da minuta {$numeroMinuta}.";
                        $this->inserirLogCustomizado('contrato', 'error', $e);
                        DB::rollback();
                    }
                }// fim do try catch
            }// fim do foreach principal(array de ids de minutas)

            $mensagemAlerta = trim($msgCriados . $msgNaoCriados . $msgErros);

            if ($errorCount == 0) {
                \Alert::success($this->retiraQuebraDoInicio($mensagemAlerta))->flash();
            }
            if ($errorCount > 0) {
                \Alert::warning($this->retiraQuebraDoInicio($mensagemAlerta))->flash();
            }

            return redirect($this->crud->route);
        } else {
            /* validar falha de javascript causando possível envio de form sem preenchimento
            (não marcou nemnhuma minuta e conseguiu submeter form) */
            Alert::error('Erro! Você enviou um formulário sem preenchimento! Por favor, habilite o Javascript no seu navegador.')->flash();
            return redirect($this->crud->route);
        }
        return true;
    }

    private function adicionaCampoEmElaboracao(): void
    {
        $this->crud->addField([ // select_from_array
            'name' => 'elaboracao',
            'type' => 'hidden',
            'default' => 0,
            'attributes' => [
                'id' => 'elaboracao'
            ],
        ]);
        /*$this->crud->addField([ // select_from_array
            'name' => 'elaboracao',
            'label' => "Situação",
            'type' => 'radio_elaboracao',
            'options' => [false => 'Ativo', true => 'Em Elaboracao'],
            'inline' => true,
            'allows_null' => false,
            'ico_help' => 'Situação Em Elaboração significa que o contrato será salvo como um rascunho.',
            'attributes' => [
                'title' => 'Situação Em Elaboração significa que o contrato será salvo como um rascunho.'
            ],
            'tab' => $this->tab
        ]);*/
    }

    public function ativar(AtivarRequest $request)
    {
        session()->forget('editando_contrato_elaboracao');
        $contratoId = $request->id;

        DB::beginTransaction();
        try {
            $contrato = Contrato::find($contratoId);
            $contrato->data_assinatura = $request->data_assinatura;
            $contrato->data_publicacao = $request->data_publicacao;
            $contrato->elaboracao = false;
            $contrato->save();

            $itens = Contratoitem::where('contrato_id', $contrato->id)->get();
            foreach ($itens as $item) {
                $item->elaboracao = false;
                $item->save();
            }
            $minutas = $contrato->minutasempenho;
            if (!$minutas->isEmpty()) {
                $arrMinutas['minutasempenho'] = $minutas->pluck('id')->toArray();
                $this->contratoService->vincularMinutaContratoHistorico(
                    $arrMinutas, $contratoId, $contrato->unidade_id);

                $amparoslegais = $contrato->amparoslegais;
                $arrAmparos['amparos'] = $amparoslegais->pluck('id')->toArray();
            }

            $fornecedoresSubcontratados = $contrato->fornecedoresSubcontratados;
            if (!empty($fornecedoresSubcontratados)) {
                $arrfornecedoresSubcontratados['fornecedoresSubcontratados']
                    = $fornecedoresSubcontratados->pluck('id')->toArray();
                $this->contratoService->vincularFornecedoresContratoHistorico(
                    $arrfornecedoresSubcontratados, $contratoId);
            }

            $this->contratoService->atualizarArquivosNoAtivar($contrato);

            $this->contratoService->saveAbaEmpenhosContrato($contratoId, $request, backpack_user()->id, true);

            DB::commit();
            Alert::success('Contrato ativado com sucesso!')->flash();
            return redirect()->route('crud.publicacao.index', ['contrato_id' => $request->contrato_id]);
        } catch (Exception $exc) {
            DB::rollback();
            dd($exc);
        }
    }

    /**
     * Método criado para deletar itens ao editar o contrato em elaboração
     * @param $contrato_id
     * @return void
     */
    private function deletarItensContrato($contrato_id): void
    {
        $itens = Contratoitem::where('contrato_id', $contrato_id)->forceDelete();
    }

    /**
     * @author: Aruã Magalhães
     * Issue 156
     * Valida se o Amparo Legal percente a lei normativa 14.133/2021
     */
    private function validarIdAmparoLegal(array $listaAmparoLegal, string $atoNormativo = '14.133/2021')
    {

        $arrayIdAmparoLegal = AmparoLegal::select('id')->whereNull('deleted_at')->where('ato_normativo', 'LIKE', "%{$atoNormativo}%")->pluck('id')->toArray();

        foreach ($listaAmparoLegal as $amparoLegal) {
            if (in_array($amparoLegal, $arrayIdAmparoLegal)) {
                return true;
            }
        }

        return false;
    }

    public function buscarEmpenho(\Illuminate\Http\Request $request)
    {
        $empenhoRequest = $request->input('empenho');
        $fornecedorId = $request->input('fornecedor_id');
        $fornecedorEmpenhoId = $request->input('fornecedor_empenho');
        $unidadeId = $request->input('unidade_empenho_id');

        if ($request->input('isFromMinuta') === "true") {
            $minutaEmpenhoDadosDoContrato = MinutaEmpenho::join('saldo_contabil', 'minutaempenhos.saldo_contabil_id', 'saldo_contabil.id')
                ->whereIn('minutaempenhos.id', $empenhoRequest)
                ->select([
                    'minutaempenhos.mensagem_siafi',
                    'saldo_contabil.unidade_id',
                ])
                ->get()
                ->toArray();

            return $this->buscarMinutaResponse($minutaEmpenhoDadosDoContrato, $fornecedorId);
        } else {
            return $this->buscarEmpenhoResponse($empenhoRequest, $fornecedorEmpenhoId, $unidadeId);
        }
    }

    private function buscarEmpenhoResponse($empenhoRequest, $fornecedorId, $unidadeId): JsonResponse
    {
        if ($empenhoRequest != null && $fornecedorId != null && $unidadeId != null) {
            $unidadeMinuta = null;
            $unidadeMinutaEmitente = null;
            $unidade = Unidade::where('id', $unidadeId)->first();
            $fornecedor = Fornecedor::where('id', $fornecedorId)->first();

            $consultaEmpenho = $this->empenhoService->getEmpenho(trim($empenhoRequest), $fornecedor, $unidade);

            //Faz a relação entre empenho da nossa base e a minuta caso exista
            $minutaEmpenho = $this->minutaService->getMinutaByEmpenho(trim($empenhoRequest), $fornecedor, $unidade);

            if ($minutaEmpenho) {
                $unidadeMinuta = $minutaEmpenho['codigo'];
            }

            if ($consultaEmpenho) {
                return response()->json([
                    'found' => true,
                    'unidade' => $unidade,
                    'unidadeMinuta' => $unidadeMinuta,
                    'unidadeMinutaEmitente' => $unidadeMinutaEmitente,
                    'isFromMinuta' => false
                ]);
            }

            $consultaEmpenhoSTA = $this->empenhoService->getEmpenhoSTA(trim($empenhoRequest), $fornecedor, $unidade);

            if ($consultaEmpenhoSTA) {
                return response()->json([
                    'found' => true,
                    'unidade' => $unidade,
                    'unidadeMinuta' => $unidadeMinuta,
                    'unidadeMinutaEmitente' => $unidadeMinutaEmitente,
                    'isFromMinuta' => false
                ]);
            }

            $consultaEmpenhoSIAFI = $this->empenhoService->getEmpenhoSIAFI(trim($empenhoRequest), $fornecedor, $unidade);

            if ($consultaEmpenhoSIAFI) {
                return response()->json([
                    'found' => true,
                    'unidade' => $unidade,
                    'unidadeMinuta' => $unidadeMinuta,
                    'unidadeMinutaEmitente' => $unidadeMinutaEmitente,
                    'isFromMinuta' => false
                ]);
            }
        }

        return response()->json(['found' => false, 'message' => 'Empenho não encontrado.']);
    }

    private function buscarMinutaResponse($minutaEmpenhoDadosDoContrato, $fornecedorId)
    {
        $fornecedor = Fornecedor::where('id', $fornecedorId)->first();
        $unidade = new Unidade();
        $minutasEmpenho = [];
        foreach ($minutaEmpenhoDadosDoContrato as $empenho) {
            $unidade->id = $empenho['unidade_id'];

            //Faz a relação entra empenho da nossa base e a minuta caso exista
            $minutasEmpenho[] = $this->minutaService->getMinutaByEmpenho(trim($empenho['mensagem_siafi']), $fornecedor, $unidade);

        }

        if ($minutasEmpenho) {
            return response()->json([
                'objMinuta' => $minutasEmpenho,
                'isFromMinuta' => true
            ]);
        }
    }

    public function buscarContratoEmpenho(\Illuminate\Http\Request $request)
    {
        $contratoId = $request->all('contrato_id');
        $contrato = Contrato::find($contratoId);

        return $this->contratoService->getContratoEmpenhoByContrato($contrato);
    }

}
