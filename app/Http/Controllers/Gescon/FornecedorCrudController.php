<?php

namespace App\Http\Controllers\Gescon;

use App\Http\Controllers\RFB;
use App\Http\Controllers\SICAF;
use App\Http\Traits\BuscaCodigoItens;
use App\Models\Codigoitem;
use App\services\Fornecedor\FornecedorSicafService;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\FornecedorRequest as StoreRequest;
use App\Http\Requests\FornecedorRequest as UpdateRequest;
use App\Http\Traits\Formatador;
use App\Models\Fornecedor;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * Class FornecedorCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class FornecedorCrudController extends CrudController
{
    use Formatador;
    use BuscaCodigoItens;

    protected $fornecedorSicafService;

    public function __construct(FornecedorSicafService $fornecedorSicafService)
    {
        parent::__construct();
        $this->fornecedorSicafService = $fornecedorSicafService;
    }

    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Fornecedor');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/gescon/fornecedor');
        $this->crud->setEntityNameStrings('Fornecedor', 'Fornecedores');
        $this->crud->enableExportButtons();
        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');
        $this->crud->allowAccess('show');

        (backpack_user()->can('fornecedor_inserir')) ? $this->crud->allowAccess('create') : null;
        (backpack_user()->can('fornecedor_editar')) ? $this->crud->allowAccess('update') : null;
        if (!backpack_user()->hasRole('Desenvolvedor'))
            $this->crud->addButtonFromView('line', 'sicaf', 'fornecedores_sicaf', 'end');

        $this->crud->addClause('leftJoin', 'fornecedorsicaf','fornecedorsicaf.fornecedor_id','=','fornecedores.id');
        $this->crud->addClause('leftJoin', 'codigoitens as nomePorte','nomePorte.id','=','fornecedorsicaf.porte');
        $this->crud->addClause('leftJoin', 'codigoitens as nomeOrigem','nomeOrigem.id','=','fornecedorsicaf.origem_sistema');
        $this->crud->addClause('select', 'fornecedores.*','fornecedorsicaf.mei','nomePorte.descricao','fornecedorsicaf.porte','fornecedorsicaf.origem_sistema');
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $this->crud->addColumns([
            [
                'name' => 'getTipo',
                'label' => 'Tipo Fornecedor', // Table column heading
                'type' => 'model_function',
                'function_name' => 'getTipo', // the method in your Model
            ],
        ]);

        $this->crud->addColumn([
            'name' => 'cpf_cnpj_idgener',
            'label' => 'CPF/CNPJ/UG/ID Genérico',
            'type' => 'text',
        ]);

        $this->crud->addColumn([
            'name' => 'nome',
            'limit' => 1000,
            'label' => 'Nome / Razão Social',
            'type' => 'text',
            'searchLogic' => function (Builder $query, $column, $searchTerm) {
                $query->orWhere('nome', 'like', "%" . strtoupper($searchTerm) . "%");
            },
        ]);

        $this->crud->addColumn([
            'name' => "fornecedorSicaf.mei",
            'label' => 'MEI',
            'type' => 'text',
            'orderable' => true,
            'orderLogic' => function ($query, $column, $columnDirection) {
                return $query->orderBy('mei', $columnDirection);
            }
        ]);

        $this->crud->addColumn([
            'name' => 'fornecedorSicaf.porte',
            'label' => 'Porte',
            'type' => 'text',
            'orderable' => true,
            'orderLogic' => function ($query, $column, $columnDirection) {
                return $query->orderBy('nomePorte.descricao', $columnDirection);
            },
            'searchLogic' => function (Builder $query, $column, $searchTerm) {
                $query->orWhere('nomePorte.descricao', 'like', "%" . strtoupper($searchTerm) . "%");
            },
        ]);

        $this->crud->addColumn([
            'name' => 'fornecedorSicaf.origem_sistema',
            'label' => 'Origem',
            'type' => 'text',
            'orderable' => true,
            'orderLogic' => function ($query, $column, $columnDirection) {
                return $query->orderBy('nomeOrigem.descricao', $columnDirection);
            },
            'searchLogic' => function (Builder $query, $column, $searchTerm) {
                $query->orWhere('nomeOrigem.descricao', 'like', "%" . strtoupper($searchTerm) . "%");
            },
        ]);

        $this->aplicaFiltros();

        $tipo_fornecedor = Codigoitem::whereHas('codigo', function ($q) {
            $q->where('descricao', '=', 'Tipo Fornecedor');
        })->pluck('descricao', 'descres')->toArray();

        $this->crud->addField([ // select_from_array
            'name' => 'tipo_fornecedor',
            'label' => 'Tipo Fornecedor',
            'type' => 'select_from_array',
            'attributes' => [
                'id' => 'tipo_fornecedor',
            ],
            'options' => $tipo_fornecedor,
            'allows_null' => true,
            'allows_multiple' => false, // OPTIONAL; needs you to cast this to array in your model;
        ]);

        $this->crud->addField([
            'name' => 'cpf_cnpj_idgener',
            'label' => 'CPF/CNPJ/UG/ID Genérico',
            'type' => 'cpf_cnpj_idgener',
        ]);

        $this->crud->addField([
            'name' => 'nome',
            'label' => 'Nome',
            'type' => 'text',
            'attributes' => [
                'onkeyup' => "maiuscula(this)"
            ]
        ]);

        $this->crud->enableAjaxTable();
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        $redirect_location = parent::storeCrud($request);

        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        $redirect_location = parent::updateCrud($request);

        return $redirect_location;
    }

    public function show($id)
    {
        $content = parent::show($id);

        $this->crud->removeColumn('tipo_fornecedor');

        return $content;
    }

    public function sicaf(Request $request)
    {
        $id = $request->get('fornecedor_id');
        $sicafApi = new SICAF();
        $fornecedor = Fornecedor::find($id);

        $sicaf = $this->consultaSicaf($fornecedor);

        if (is_object($sicaf)) {
            if (
                $sicaf->validade_receita_federal_pgfn &&
                $sicaf->validade_certidao_fgts &&
                $sicaf->validade_certidao_trabalhista &&
                $sicaf->validate_receita_estadual_distrital &&
                $sicaf->validade_receita_municipal) {

                if (!$sicaf->date_receita_federal_pgfn ||
                    !$sicaf->date_certidao_fgts ||
                    !$sicaf->date_certidao_trabalhista ||
                    !$sicaf->date_receita_estadual_distrital ||
                    !$sicaf->date_receita_municipal) {
                    $sicafStatus = 2;//Valida com data sem informação
                } else {
                    $sicafStatus = 1;//Válida com todas informações
                }
            } else {
                $sicafStatus = 3;//Invalida
            }
        } else {
            $sicafStatus = $sicaf;//Erro da api
        }

        if ($sicafStatus > 3) {
            $existeCertidao = false;
        } else {
            $existeCertidao = false;
            if ($this->retornaSomenteNumeros($fornecedor->cpf_cnpj_idgener)) {
                $existeCertidao = $sicafApi->regularidadeFiscalEconomicaFinanceira($this->retornaSomenteNumeros($fornecedor->cpf_cnpj_idgener));
            }
        }

        return json_encode(['sicaf' => $sicaf, 'certidao' => $existeCertidao]);
    }

    public function consultaSicaf(Fornecedor $fornecedorSicaf)
    {
        $fornecedor = $this->fornecedorSicafService->retornaConsultaSicaf($fornecedorSicaf);

        if (!is_object($fornecedor)) {
            $fornecedor = $this->fornecedorSicafService->retornaConsultaRFB($fornecedorSicaf);
        }

        return $fornecedor;
    }

    protected function aplicaFiltros()
    {
        $this->aplicaFiltroMei();
        $this->aplicaFiltroPorte();
    }

    protected function aplicaFiltroMei()
    {
        $this->crud->addFilter(
            [
                'name' => 'mei',
                'type' => 'select2_multiple',
                'label' => 'MEI'
            ],
            [
                '1' => 'Sim',
                '0' => 'Não'
            ],
            function ($value) {

                $ativoInativo = str_replace(['[', '"', ']'], "", $value);
                $arrayAtivoInativo = array_filter(explode(",", $ativoInativo), function ($valor) {
                    return $valor !== "";
                });

                //Verifica se possui os status ativo e inativo dentro do array e nenhuma outra situação
                if (count($arrayAtivoInativo) >= 1) {
                    $this->crud->query = $this->crud->query->whereHas('fornecedorSicaf', function ($query) use ($arrayAtivoInativo) {
                        $query->whereIn('mei', json_decode(json_encode($arrayAtivoInativo), true));
                    });
                }
            }
        );
    }

    protected function aplicaFiltroPorte()
    {
        $this->crud->addFilter(
            [
                'name' => 'porte',
                'type' => 'select2_multiple',
                'label' => 'Porte'
            ],
            [
                'PORT01' => '1 - MICROEMPRESA-ME',
                'PORT03' => '3 - EMPRESAS DE PEQUENO PORTE EPP',
                'PORT05' => '5 - DEMAIS EMPRESAS',
            ],
            function ($value) {

                $portes = str_replace(['[', '"', ']'], "", $value);
                $arrayPortesDescres = array_filter(explode(",", $portes), function ($valor) {
                    return $valor !== "";
                });

                $arrayPortes = array_map(function ($valor) {
                    return $this->retornaIdCodigoItemPorDescres($valor, 'Tipo Porte Empresa'); // Retorna o código do item pelo nome
                }, $arrayPortesDescres);

                //Verifica se possui os status ativo e inativo dentro do array e nenhuma outra situação
                if (count($arrayPortes) >= 1) {
                    $this->crud->query = $this->crud->query->whereHas('fornecedorSicaf', function ($query) use ($arrayPortes) {
                        $query->whereIn('porte', json_decode(json_encode($arrayPortes), true));
                    });
                }
            }
        );
    }

    public function certidao(Request $request)
    {
        $id = $request->route('fornecedor_id');
        $rota = $request->route('rota');

        $fornecedor = Fornecedor::find($id);
        $cnpj = $this->retornaSomenteNumeros($fornecedor->cpf_cnpj_idgener);

        $sicaf = new SICAF();

        $dados = $sicaf->regularidadeFiscalEconomicaFinanceira($cnpj);

        if ($rota == 'municipal') {

            $codigo = $dados->codigoArquivoRegFiscalMun;
            $hash = $dados->hashArquivoRegFiscalMun;

            $filename = 'Certidão municipal ' . $fornecedor->cpf_cnpj_idgener;
        }

        if ($rota == 'estadual') {

            $codigo = $dados->codigoArquivoRegFiscalEst;
            $hash = $dados->hashArquivoRegFiscalEst;

            $filename = 'Certidão estadual ' . $fornecedor->cpf_cnpj_idgener;
        }

        $arquivo = $sicaf->arquivo($codigo, $hash);

        return new Response($arquivo, 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="' . $filename . '"'
        ]);
    }
}
