<?php

namespace App\Http\Controllers\Gescon;

use App\Http\Requests\ConsultacontratofaturaRequest;
use App\Http\Traits\Formatador;
use App\Models\Contrato;
use App\Models\Contratofatura;
use App\Models\Empenho;
use App\Models\Fornecedor;
use App\Models\Justificativafatura;
use App\Models\Tipolistafatura;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use DB;
use Illuminate\Http\Request as StoreRequest;
use Backpack\CRUD\CrudPanel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Log;

/**
 * Class FaturaCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class InstrumentodecobrancaSituacaoCrudController extends CrudController
{
    use Formatador;
    
    public function setup()
    {
        #598 - incluído Administrador
        if (!backpack_user()->hasRole('Execução Financeira') && !backpack_user()->hasRole('Administrador')) {
            abort('403', config('app.erro_permissao'));
        }

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Contratofatura');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/gescon/instrumento-de-cobranca');
        $this->crud->setEntityNameStrings('Instrumento de Cobrança', 'Instrumentos de Cobrança');
        $this->crud->addClause('select',
            [
                'contratofaturas.*',
                'contratos.numero as numero_contrato',
                'contratos.processo as processo_contrato',
                'contratos.id as contrato_gerenciadora',
                'contratounidadesdescentralizadas.contrato_id as contrato_desc',
                'contratofaturas.id as id_para_empenho', //uso essa coluna para trazer os empenhos do instrumento
                'tipolistafatura.nome as nome_fatura',
                DB::raw("CONCAT(justificativafatura.nome , ' - ', justificativafatura.descricao) AS justificativa_fatura")
            ]);

        $this->crud->addClause('join', 'tipolistafatura',
            'tipolistafatura.id', '=', 'contratofaturas.tipolistafatura_id');

        $this->crud->addClause('leftJoin', 'contratos',
            'contratos.id', '=', 'contratofaturas.contrato_id'
        );

        $this->crud->addClause('leftJoin', 'contratounidadesdescentralizadas',
            'contratounidadesdescentralizadas.contrato_id', '=', 'contratos.id'
        );

        $this->crud->addClause('leftJoin', 'fornecedores',
            'fornecedores.id', '=', 'contratos.fornecedor_id'
        );

        $this->crud->addClause('leftJoin', 'justificativafatura',
        'justificativafatura.id', '=', 'contratofaturas.justificativafatura_id'
    );

        $this->crud->addClause('where', function ($query) {
            $query->where('contratos.unidade_id', '=', session('user_ug_id'))
                ->orWhere('contratounidadesdescentralizadas.unidade_id', '=', session('user_ug_id'));
        });

        $this->crud->addClause('orderBy', 'contratofaturas.vencimento', 'desc');

        $this->crud->addClause('distinct');

        $this->crud->orderBy('contratofaturas.updated_at', 'desc');

        $this->crud->denyAccess('create');
        $this->crud->denyAccess('delete');
        $this->crud->allowAccess('update');
        $this->crud->allowAccess('show');

        $faturaId = \Route::current()->parameter('instrumento_de_cobranca');
        if ($faturaId) {
            $fatura = Contratofatura::find($faturaId);

            $amparos = $fatura->contrato->amparos;
            $fornecedorId = $fatura->contrato->fornecedor_id;
            $fornecedorDesc = $fatura->getFornecedor();
        }
        else{
            $fornecedorId = 0;
            $fornecedorDesc = '';
            $amparos = null;
        }
        $this->crud->addFields($this->campos($fornecedorId, $fornecedorDesc, $amparos));

        $colunas = $this->Colunas();

        $this->crud->addColumn([
            'type' => 'checkbox_status_instrumentosdecobranca',
            'name' => 'bulk_actions',
            'label' => ' <input type="checkbox" class="crud_bulk_actions_main_checkbox" style="width: 16px; height: 16px;" />',
            'priority' => 1,
            'searchLogic' => false,
            'orderable' => false,
            'visibleInModal' => false,
        ])->makeFirstColumn();

        $this->crud->addColumns($colunas);

        $this->crud->situacoes = $this->retornaSelectSituacao();

        #598
        $this->crud->justificativasParaAlteracaoEmLote = $this->retornaJustificativasCombo($amparos);

        $this->crud->addButton(
            'top',
            'status_instrumento_cobranca',
            'view',
            'crud::buttons.bulk_status_fatura'
        );

        $this->adicionaFiltros();

        $this->crud->setRequiredFields(\App\Http\Requests\ContratofaturaRequest::class, 'create');
        $this->crud->setRequiredFields(\App\Http\Requests\ContratofaturaRequest::class, 'edit');


        #se existir sessao de apropriacao de faturas então remove-las

        if(session()->has('arrPadraoAlteracaoApropriacao')){
            session()->forget('arrPadraoAlteracaoApropriacao');
        }
    }


    private function campos($fornecedorId = 0, $fornecedorDesc = '', $amparos = null)
    {
        $justificativas = $this->retornaJustificativasCombo($amparos);

        $campos = array();

        $campos[] = [
            'name' => 'num_contrato',
            'label' => 'Número do instrumento',
            'attributes' => [
                'readonly'=>'readonly',
                'style' => 'pointer-events: none;touch-action: none;',
                'class' => 'form-control mostraCamposRelacionados',
                'data-campo' => 'numero'
            ]
        ];

        $campos[] = [
            'name' => 'desc_fornecedor',
            'label' => 'Fornecedor',
            'type' => 'text',
            'value' => $fornecedorDesc,
            'attributes' => [
                'readonly'=>'readonly',
                'style' => 'pointer-events: none;touch-action: none;',
            ]
        ];

        $campos[] = [
            'name' => 'justificativafatura_id',
            'label' => 'Justificativa',
            'type' => 'select_from_array',
            'options' => $justificativas,
            'default'    => null,
            'placeholder'    => '123',
            'allows_null' => false
        ];

        $campos[] = [
            'name' => 'situacao',
            'label' => 'Situação',
            'type' => 'select_from_array',
            'options' => config('app.situacao_fatura'),
            'default'    => 'PEN',
            'allows_null' => false
        ];

        $campos[] = [
            'name' => 'empenhos',
            'label' => 'Empenhos',
            'type' => 'select2_multiple',
            'model' => 'App\Models\Empenho',
            'entity' => 'empenhos',
            'attribute' => 'numero',
            'attribute2' => 'aliquidar',
            'attribute_separator' => ' - Valor a Liquidar: R$ ',
            'pivot' => true,
            'options' => (function ($query) use ($fornecedorId) {
                return $query->orderBy('numero', 'ASC')
                    ->where('unidade_id', session()->get('user_ug_id'))
                    ->where('fornecedor_id', $fornecedorId)
                    ->get();
            })
        ];

        $campos[] = [
            'name' => 'contrato',
            'label' => 'dados_contrato',
            'type' => 'hidden',
            'attributes' => [
                'id' => 'dados_contrato'
            ]
        ];

        return $campos;
    }

    public function Colunas()
    {
        $colunas = [
            [
                'name' => 'numero_contrato',
                'label' => 'Contrato',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'nome_fatura',
                'label' => 'Tipo Lista Fatura',
                'type' => 'text',
                'orderable' => true,
                'limit' => 1000,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere('tipolistafatura.nome', 'like', "%" . strtoupper($searchTerm) . "%");
                },
            ],
            [
                'name' => 'justificativa_fatura',
                'label' => 'Justificativa',
                'type' => 'text',
                'limit' => 1000,
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'getSfpadrao',
                'label' => 'Doc. Origem Siafi',
                'type' => 'model_function',
                'function_name' => 'getSfpadrao',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'numero',
                'label' => 'Número',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere('contratofaturas.numero', 'ilike', '%' . $searchTerm . '%')
                        ->orWhere('contratos.numero', 'ilike', '%' . $searchTerm . '%');
                }
            ],
            [
                'name' => 'emissao',
                'label' => 'Dt. Emissão',
                'type' => 'date',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'vencimento',
                'label' => 'Dt. Vencimento',
                'type' => 'date',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'formatValor',
                'label' => 'Valor',
                'type' => 'model_function',
                'function_name' => 'formatValor',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'formatJuros',
                'label' => 'Juros',
                'type' => 'model_function',
                'function_name' => 'formatJuros',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'formatMulta',
                'label' => 'Multa',
                'type' => 'model_function',
                'function_name' => 'formatMulta',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'formatGlosa',
                'label' => 'Glosa',
                'type' => 'model_function',
                'function_name' => 'formatGlosa',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'formatValorLiquido',
                'label' => 'Valor Líquido a pagar',
                'type' => 'model_function',
                'function_name' => 'formatValorLiquido',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'processo_contrato',
                'label' => 'Processo',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'protocolo',
                'label' => 'Dt. Protocolo',
                'type' => 'date',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'ateste',
                'label' => 'Dt. Ateste',
                'type' => 'date',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'prazo',
                'label' => 'Dt. Prazo Pagto.',
                'type' => 'date',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'empenhos',
                'label' => 'Empenhos',
                'type' => 'select_multiple',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'entity' => 'empenhos',
                'attribute' => 'numero',
                'model' => Empenho::class,
                'pivot' => true,
            ],
            [
                'name' => 'id_para_empenho',
                'label' => 'Empenhos',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'repactuacao',
                'label' => 'Repactuação',
                'type' => 'boolean',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'options' => [0 => 'Não', 1 => 'Sim']
            ],
            [
                'name' => 'infcomplementar',
                'label' => 'Informações Complementares',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'mesref',
                'label' => 'Mês Referência',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'anoref',
                'label' => 'Ano Referência',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'situacao',
                'label' => 'Situação',
                'type' => 'select_from_array',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'options' => config('app.situacao_fatura')
            ],
        ];

        return $colunas;
    }

    #598
    public function alterarSituacaoEmLote(StoreRequest $request){
        $arrayContratoFaturaIds = $request->input('arrayContratoFaturaIds');
        $situacaoDesejada = $request->input('situacaoDesejada');
        if($arrayContratoFaturaIds){
            DB::beginTransaction();
            foreach($arrayContratoFaturaIds as $contratofatura_id){
                try {
                    $contratofatura = Contratofatura::find($contratofatura_id);
                    $contratofatura->situacao = $situacaoDesejada;
                    $contratofatura->save();
                    \Alert::success("Operação realizada com sucesso!")->flash();
                } catch (\Exception $e) {
                    DB::rollback();
                    Log::info($e->getMessage());
                    \Alert::error("Não foi possível realizar a operação!")->flash();
                }
            }
            DB::commit();
        } else {
            \Alert::error("Você precisa selecionar pelo menos um registro!")->flash();
        }
        return redirect($this->crud->route);
    }


    public function store(StoreRequest $request)
    {
        $justificativafatura_id = $request->input("justificativafatura_id");    #598
        $situacao = $request->input("situacao_instrumento_combranca_id");
        $arrayContratoInstrumento = $request->input("contratofaturas_ids");

        try {
            DB::beginTransaction();

            Contratofatura::whereIn("id", $arrayContratoInstrumento)
                ->update([
                    'situacao' => $situacao,
                    'justificativafatura_id' => $justificativafatura_id]    #598
                );

            DB::commit();
            \Alert::success("Operação realizada com sucesso!")->flash();
        } catch (\Exception $e) {
            DB::rollback();
            Log::info($e->getMessage());
            \Alert::error("Não foi possível realizar a operação!")->flash();
        }

        #598
        $rotaComPendenteSelecionado = $this->crud->route . '?situacao=%5B%22PEN%22%5D';
        return redirect($rotaComPendenteSelecionado);
        // return redirect($this->crud->route);
    }

    public function update(ConsultacontratofaturaRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);

        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }


    public function show($id)
    {
        $content = parent::show($id);

        $contratoFaturas = $this->findContratoFaturaToShowView($id);

        if($contratoFaturas !== null)
        {
            $content->entry = $contratoFaturas;
        }

        $removeColumns = [  
            'numero_contrato',
            'nome_fatura',
            'justificativa_fatura',
        ];

        $addColumnsShowOnly = [
            'colunas' => [
                    [
                        'name' => 'numero_contrato',
                        'label' => 'Contrato',
                        'type' => 'model_function',
                        'function_name' => 'getContrato'
                    ],
                    [
                        'name' => 'nome_fatura',
                        'label' => 'Tipo Lista Fatura',
                        'type' => 'model_function',
                        'function_name' => 'getTipoListaFatura'
                    ],
                    [
                        'name' => 'justificativa_fatura',
                        'label' => 'Justificativa',
                        'type' => 'model_function',
                        'function_name' => 'getJustificativaFatura'
                    ]
                ]
            ];

        $this->crud->removeColumns($removeColumns);
        $this->addColumnsShowOnly($addColumnsShowOnly['colunas']);

        $this->crud->removeColumn('contrato_id');
        $this->crud->removeColumn('tipolistafatura_id');
        $this->crud->removeColumn('justificativafatura_id');
        $this->crud->removeColumn('sfpadrao_id');
        $this->crud->removeColumn('valor');
        $this->crud->removeColumn('juros');
        $this->crud->removeColumn('multa');
        $this->crud->removeColumn('glosa');
        $this->crud->removeColumn('valorliquido');

        return $content;
    }

    public function findContratoFaturaToShowView($idContratoFatura)
    {
        return ContratoFatura::select(
            'contratofaturas.*',
            'contratos.numero as numero_contrato',
            'contratos.processo as processo_contrato',
            'contratos.id as contrato_gerenciadora',
            'contratounidadesdescentralizadas.contrato_id as contrato_desc',
            'contratofaturas.id as id_para_empenho'
        )
            ->join('tipolistafatura', 'tipolistafatura.id', '=', 'contratofaturas.tipolistafatura_id')
            ->leftJoin('contratos', 'contratos.id', '=', 'contratofaturas.contrato_id')
            ->leftJoin('contratounidadesdescentralizadas', 'contratounidadesdescentralizadas.contrato_id', '=', 'contratos.id')
            ->leftJoin('fornecedores', 'fornecedores.id', '=', 'contratos.fornecedor_id')
            ->where(function ($query) {
                $query->where('contratos.unidade_id', '=', session('user_ug_id'))
                    ->orWhere('contratounidadesdescentralizadas.unidade_id', '=', session('user_ug_id'));
            })
            ->whereNull('contratofaturas.deleted_at')
            ->where('contratofaturas.id', $idContratoFatura)
            ->orderByDesc('contratofaturas.vencimento')
            ->first();
    }

    /**
     * Adiciona todos os filtros desejados para esta funcionalidade
     *
     * @author Anderson Sathler <asathler@gmail.com>
     */
    private function adicionaFiltros()
    {
        $this->adicionaFiltroNumeroFatura();
        $this->adicionaFiltroNumeroContrato();
        $this->adicionaFiltroFornecedor();
        $this->adicionaFiltroTipoLista();
        $this->adicionaFiltroJustificativa();
        $this->adicionaFiltroDataEmissao();
        $this->adicionaFiltroDataAteste();
        $this->adicionaFiltroDataVencimento();
        $this->adicionaFiltroDataPrazoPagamento();
        $this->adicionaFiltroDataProtocolo();
        $this->adicionaFiltroSituacao();
    }

    /**
     * Adiciona o filtro ao campo Número da Fatura
     *
     * @author Anderson Sathler <asathler@gmail.com>
     */
    private function adicionaFiltroNumeroFatura()
    {
        $campo = [
            'name' => 'numero',
            'type' => 'text',
            'label' => 'Núm. Fatura'
        ];

        $this->crud->addFilter(
            $campo,
            null,
            function ($value) {
                $this->crud->addClause('where', 'contratofaturas.numero', $value);
            }
        );
    }

    /**
     * Adiciona o filtro ao campo Número do Contrato
     *
     * @author Anderson Sathler <asathler@gmail.com>
     */
    private function adicionaFiltroNumeroContrato()
    {
        $campo = [
            'name' => 'contrato',
            'type' => 'select2',
            'label' => 'Núm. Contrato'
        ];

        $contratos = $this->retornaContratos();

        $this->crud->addFilter(
            $campo,
            $contratos,
            function ($value) {
                $this->crud->addClause('where', 'contratos.numero', $value);
            }
        );
    }

    /**
     * Adiciona o filtro ao campo Fornecedor
     *
     * @author Anderson Sathler <asathler@gmail.com>
     */
    private function adicionaFiltroFornecedor()
    {
        $campo = [
            'name' => 'cpf_cnpj',
            'type' => 'select2',
            'label' => 'Fornecedor'
        ];

        $fornecedores = $this->retornaFornecedores();

        $this->crud->addFilter(
            $campo,
            $fornecedores,
            function ($value) {
                $this->crud->addClause('where', 'fornecedores.cpf_cnpj_idgener', $value);
            }
        );
    }

    /**
     * Adiciona o filtro ao campo Tipo de Lista
     *
     * @author Anderson Sathler <asathler@gmail.com>
     */
    private function adicionaFiltroTipoLista()
    {
        $campo = [
            'name' => 'tipo_lista',
            'type' => 'select2',
            'label' => 'Tipo Lista'
        ];

        $tiposLista = $this->retornaTiposLista();

        $this->crud->addFilter(
            $campo,
            $tiposLista,
            function ($value) {
                $this->crud->addClause('where', 'contratofaturas.tipolistafatura_id', $value);
            }
        );
    }

    /**
     * Adiciona o filtro ao campo Justificativa
     *
     * @author Anderson Sathler <asathler@gmail.com>
     */
    private function adicionaFiltroJustificativa()
    {
        $campo = [
            'name' => 'justificativa',
            'type' => 'select2',
            'label' => 'Justificativa'
        ];

        $justificativas = $this->retornaJustificativas();

        $this->crud->addFilter(
            $campo,
            $justificativas,
            function ($value) {
                $this->crud->addClause('where', 'contratofaturas.justificativafatura_id', $value);
            }
        );
    }

    /**
     * Adiciona o filtro ao campo Data de Emissão
     *
     * @author Anderson Sathler <asathler@gmail.com>
     */
    private function adicionaFiltroDataEmissao()
    {
        $campo = [
            'name' => 'dt_emissao',
            'type' => 'date_range',
            'label' => 'Dt. Emissão'
        ];

        $this->crud->addFilter(
            $campo,
            null,
            function ($value) {
                $dates = json_decode($value);
                $this->crud->addClause('where', 'contratofaturas.emissao', '>=', $dates->from . ' 00:00:00');
                $this->crud->addClause('where', 'contratofaturas.emissao', '<=', $dates->to . ' 23:59:59');
            }
        );
    }

    /**
     * Adiciona o filtro ao campo Data de Ateste
     *
     * @author Anderson Sathler <asathler@gmail.com>
     */
    private function adicionaFiltroDataAteste()
    {
        $campo = [
            'name' => 'dt_ateste',
            'type' => 'date_range',
            'label' => 'Dt. Ateste'
        ];

        $this->crud->addFilter(
            $campo,
            null,
            function ($value) {
                $dates = json_decode($value);
                $this->crud->addClause('where', 'contratofaturas.ateste', '>=', $dates->from . ' 00:00:00');
                $this->crud->addClause('where', 'contratofaturas.ateste', '<=', $dates->to . ' 23:59:59');
            }
        );
    }

    /**
     * Adiciona o filtro ao campo Data de Vencimento
     *
     * @author Anderson Sathler <asathler@gmail.com>
     */
    private function adicionaFiltroDataVencimento()
    {
        $campo = [
            'name' => 'dt_vencimento',
            'type' => 'date_range',
            'label' => 'Dt. Vencimento'
        ];

        $this->crud->addFilter(
            $campo,
            null,
            function ($value) {
                $dates = json_decode($value);
                $this->crud->addClause('where', 'contratofaturas.vencimento', '>=', $dates->from . ' 00:00:00');
                $this->crud->addClause('where', 'contratofaturas.vencimento', '<=', $dates->to . ' 23:59:59');
            }
        );
    }

    /**
     * Adiciona o filtro ao campo Data do Prazo de Pagamento
     *
     * @author Anderson Sathler <asathler@gmail.com>
     */
    private function adicionaFiltroDataPrazoPagamento()
    {
        $campo = [
            'name' => 'dt_prazo',
            'type' => 'date_range',
            'label' => 'Prazo Pagamento'
        ];

        $this->crud->addFilter(
            $campo,
            null,
            function ($value) {
                $dates = json_decode($value);
                $this->crud->addClause('where', 'contratofaturas.prazo', '>=', $dates->from . ' 00:00:00');
                $this->crud->addClause('where', 'contratofaturas.prazo', '<=', $dates->to . ' 23:59:59');
            }
        );
    }

    /**
     * Adiciona o filtro ao campo Data do Protocolo
     *
     * @author Anderson Sathler <asathler@gmail.com>
     */
    private function adicionaFiltroDataProtocolo()
    {
        $campo = [
            'name' => 'dt_protocolo',
            'type' => 'date_range',
            'label' => 'Dt. Protocolo'
        ];

        $this->crud->addFilter(
            $campo,
            null,
            function ($value) {
                $dates = json_decode($value);
                $this->crud->addClause('where', 'contratofaturas.protocolo', '>=', $dates->from . ' 00:00:00');
                $this->crud->addClause('where', 'contratofaturas.protocolo', '<=', $dates->to . ' 23:59:59');
            }
        );
    }

    #598 - obs.: para que ja venha selecionado, passar a variável no link do menu.
    private function adicionaFiltroSituacao()
    {
        $campo = [
            'name' => 'situacao',
            // 'type' => 'select2',
            'type' => 'select2_multiple',
            'label' => 'Situação'
        ];
        $situacoes = config('app.situacao_fatura');
        $this->crud->addFilter(
            $campo,
            $situacoes,
            function ($value) {
                $this->crud->addClause('whereIn', 'contratofaturas.situacao', json_decode($value) );
            }
        );
    }

    /**
     * Retorna dados dos Contratos para exibição no controle de filtro
     *
     * @return array
     * @author Anderson Sathler <asathler@gmail.com>
     */
    private function retornaContratos()
    {
        $dados = Contrato::select(
            \Illuminate\Support\Facades\DB::raw("LEFT(CONCAT(numero, ' - ', objeto), 80) AS descricao"), 'numero'
        );

        $dados->where('situacao', true);
        $dados->whereHas('unidade', function ($u) {
            $u->where('codigo', session('user_ug'));
        });
        $dados->orderBy('id'); // 'data_publicacao'

        return $dados->pluck('descricao', 'numero')->toArray();
    }

    /**
     * Retorna dados de Fornecedores para exibição no controle de filtro
     *
     * @return array
     * @author Anderson Sathler <asathler@gmail.com>
     */
    private function retornaFornecedores()
    {
        $dados = Fornecedor::select(
            DB::raw("CONCAT(cpf_cnpj_idgener, ' - ', nome) AS descricao"), 'cpf_cnpj_idgener'
        );

        $dados->whereHas('contratos', function ($c) {
            $c->where('situacao', true);
            $c->where('contratos.elaboracao', false);
        });

        return $dados->pluck('descricao', 'cpf_cnpj_idgener')->toArray();
    }

    /**
     * Retorna dados de Tipos de Lista para exibição no controle de filtro
     *
     * @return array
     * @author Anderson Sathler <asathler@gmail.com>
     */
    private function retornaTiposLista()
    {
        $dados = Tipolistafatura::select('nome as descricao', 'id');

        return $dados->pluck('descricao', 'id')->toArray();
    }

    /**
     * Retorna dados das Justificativas para exibição no controle de filtro
     *
     * @return array
     * @author Anderson Sathler <asathler@gmail.com>
     */
    private function retornaJustificativas()
    {
        $dados = Justificativafatura::select(
            'id',
            DB::raw("CONCAT(nome, ' - ', LEFT(descricao, 80)) as descricao")
        );

        return $dados->pluck('descricao', 'id')->toArray();
    }

    /**
     * Retorna dados das Justificativas para exibição no combo de edição
     *
     * @return array
     * @author Anderson Sathler <asathler@gmail.com>
     */
    private function retornaJustificativasCombo()
    {
        $justificativas = $this->retornaJustificativas();
        $justificativas[''] = 'Selecione a justificativa';

        ksort($justificativas);

        return $justificativas;
    }

    public function retornaSelectSituacao()
    {
        return config('app.situacao_fatura');
    }

}
