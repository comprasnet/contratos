<?php

namespace App\Http\Controllers\Gescon;

use App\Http\Traits\ContratoFaturaTrait;
use App\Models\Contrato;
use App\Models\Contratofatura;
use App\Models\Fornecedor;
use App\Models\Justificativafatura;
use App\Models\Tipolistafatura;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\Formatador;
use DB;

// VALIDATION: change the requests to match your own file names if you need form validation
use Illuminate\Http\Request as StoreRequest;
use Illuminate\Http\Request as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class FaturaCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class FaturaCrudController extends CrudController
{
    use BuscaCodigoItens;
    use ContratoFaturaTrait;
    use Formatador;

    private $queryPrincipal;
    private $queryInstrumentoCobrancaUndDescentralizada;

    public function setup()
    {

        if (!backpack_user()->can('apropriar_fatura') && !backpack_user()->hasRole('Consulta Global')) {
            abort('403', config('app.erro_permissao'));
        }
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */

        $this->crud->setModel('App\Models\Contratofatura');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/gescon/fatura');
        $this->crud->setEntityNameStrings('Apropriação instrumento de cobrança', 'Apropriação instrumentos de cobrança');

        $this->crud->addClause('select',
            [
                'contratofaturas.*',
                'contratos.numero as numero_contrato',
                'contratos.processo as processo_contrato',
                'contratos.id as contrato_gerenciadora',
                'contratounidadesdescentralizadas.contrato_id as contrato_desc',
                'contratofaturas.id as id_para_empenho', //uso essa coluna para trazer os empenhos do instrumento
                \DB::raw("COALESCE(amparo_legal.ato_normativo, '-') as ato_normativo"),
                'tipolistafatura.order',
                \DB::raw("CONCAT(fornecedores.cpf_cnpj_idgener, ' - ', fornecedores.nome) AS descr_fornecedor"),
                'tipolistafatura.nome as Lista_tipo_fatura',
                \DB::raw("CONCAT(justificativafatura.nome , ' - ', justificativafatura.descricao) AS justificativa_fatura")

            ]);

        $this->crud->addClause('join', 'tipolistafatura',
            'tipolistafatura.id', '=', 'contratofaturas.tipolistafatura_id');

        $this->crud->addClause('leftJoin', 'contratos',
            'contratos.id', '=', 'contratofaturas.contrato_id'
        );

        $this->crud->addClause('leftJoin', 'contratounidadesdescentralizadas',
            'contratounidadesdescentralizadas.contrato_id', '=', 'contratos.id'
        );

        $this->crud->addClause('leftJoin', 'fornecedores',
            'fornecedores.id', '=', 'contratos.fornecedor_id'
        );
        $this->crud->addClause('leftJoin', 'amparo_legal_contrato',
            'amparo_legal_contrato.contrato_id', '=', 'contratos.id'
        );
        $this->crud->addClause('leftJoin', 'amparo_legal',
            'amparo_legal.id', '=', 'amparo_legal_contrato.amparo_legal_id'
        );
        $this->crud->addClause('leftJoin', 'justificativafatura',
        'justificativafatura.id', '=', 'contratofaturas.justificativafatura_id'
        );
        $this->crud->addClause('where', function ($query) {
            $query->where('contratos.unidade_id', '=', session('user_ug_id'))
                ->orWhere('contratounidadesdescentralizadas.unidade_id', '=', session('user_ug_id'));
        });

        $this->crud->addClause('distinct');
        $this->ordenacaoPadrao($this->crud);

        $this->crud->addButton('line', 'show', 'view', 'crud::buttons.show_inst_cobranca', 'end');
        $this->crud->addButton('line', 'update', 'view', 'crud::buttons.update_inst_cobranca', 'end');

        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');
        $this->crud->allowAccess('show');
        if (backpack_user()->can('contratofatura_editar')) {
            $this->crud->allowAccess('update');
        }

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // pega o id da fatura na url para fazer a buscar dos demais campos a partir do contrato relacionado a fatura
        $fatura_id = \Route::current()->parameter('fatura');
        $tipolistafatura = Tipolistafatura::where('situacao', true)
            ->orderBy('nome', 'ASC')
            ->pluck('nome', 'id')
            ->toArray();

        if($fatura_id){
            $fatura = Contratofatura::find($fatura_id);
            $contrato = Contrato::find($fatura->contrato_id);

            $con = $contrato->where('id', '=', $fatura->contrato_id)
                ->pluck('numero', 'id')
                ->toArray();

            $campos = $this->Campos($con, $tipolistafatura, $fatura->contrato_id);

            $this->crud->addFields($campos);
        }

        $colunas = $this->Colunas();

        /*PERMITIDO APENAS PARA ADMINISTRADOR ATÉ HOMOLOGAÇÃO*/
        if (backpack_user()->can('apropriar_fatura')) {

            $this->crud->addColumn([
                'type' => 'checkbox_apropriacao_fatura',
                'name' => 'bulk_actions',
                'label' => ' <input type="checkbox" class="crud_bulk_actions_main_checkbox" style="width: 16px; height: 16px;" />',
                'priority' => 1,
                'searchLogic' => false,
                'orderable' => false,
                'visibleInModal' => false,
            ])->makeFirstColumn();

            $this->crud->addButton(
                'line',
                'apropriacao_fatura',
                'view',
                'crud::buttons.apropriacao_fatura',
                'beginning'
            );

            $this->crud->addButton(
                'bottom',
                'apropriacao_fatura',
                'view',
                'crud::buttons.bulk_apropriacao_fatura'
            );

        }
        $this->crud->addColumns($colunas);

        $this->adicionaFiltros();

        $this->crud->setRequiredFields(\App\Http\Requests\ContratofaturaRequest::class, 'create');
        $this->crud->setRequiredFields(\App\Http\Requests\ContratofaturaRequest::class, 'edit');

        #se existir sessao de apropriacao de faturas então remove-las

        if(session()->has('arrPadraoAlteracaoApropriacao')){
            session()->forget('arrPadraoAlteracaoApropriacao');
        }
    }

    public function Colunas()
    {
        $colunas = [
            [
                'name' => 'numero_contrato',
                'label' => 'Contrato',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'contrato_gerenciadora',
                'label' => 'Unidade Gestora do Contrato',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'contrato_desc',
                'label' => 'Unidade Descentralizada',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'descr_fornecedor',
                'label' => 'Fornecedor',
                'type' => 'text',
                'orderable' => true,
                'limit' => 50,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere('fornecedores.cpf_cnpj_idgener', 'like', "%" . strtoupper($searchTerm) . "%");
                    $query->orWhere('fornecedores.nome', 'like', "%" . strtoupper($searchTerm) . "%");
                },
            ],
            [
                'name' => 'ato_normativo',
                'label' => 'Lei',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'Lista_tipo_fatura',
                'label' => 'Tipo Lista',
                'type' => 'text',
                'orderable' => true,
                'limit' => 1000,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere('tipolistafatura.nome', 'like', "%" . strtoupper($searchTerm) . "%");
                },
            ],
            [
                'name' => 'justificativa_fatura',
                'label' => 'Justificativa',
                'type' => 'text',
                'limit' => 1000,
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'getSfpadrao',
                'label' => 'Doc. Origem Siafi',
                'type' => 'model_function',
                'function_name' => 'getSfpadrao',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'numero',
                'label' => 'Número',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere('contratofaturas.numero', 'ilike', '%' . $searchTerm . '%')
                        ->orWhere('contratos.numero', 'ilike', '%' . $searchTerm . '%');
                }
            ],
            [
                'name' => 'emissao',
                'label' => 'Dt. Emissão',
                'type' => 'date',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'vencimento',
                'label' => 'Dt. Limite Pagamento',
                'type' => 'date',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'formatValor',
                'label' => 'Valor',
                'type' => 'model_function',
                'function_name' => 'formatValor',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'formatJuros',
                'label' => 'Juros',
                'type' => 'model_function',
                'function_name' => 'formatJuros',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'formatMulta',
                'label' => 'Multa',
                'type' => 'model_function',
                'function_name' => 'formatMulta',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'formatGlosa',
                'label' => 'Glosa',
                'type' => 'model_function',
                'function_name' => 'formatGlosa',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'formatValorLiquido',
                'label' => 'Valor Líquido a pagar',
                'type' => 'model_function',
                'function_name' => 'formatValorLiquido',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'processo_contrato',
                'label' => 'Processo',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'protocolo',
                'label' => 'Dt. Protocolo',
                'type' => 'date',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'ateste',
                'label' => 'Dt. Liquidação',
                'type' => 'date',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'prazo',
                'label' => 'Prazo',
                'type' => 'date',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'id_para_empenho',
                'label' => 'Empenhos',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'repactuacao',
                'label' => 'Repactuação',
                'type' => 'boolean',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'options' => [0 => 'Não', 1 => 'Sim']
            ],
            [
                'name' => 'infcomplementar',
                'label' => 'Informações Complementares',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'mesref',
                'label' => 'Mês Referência',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'anoref',
                'label' => 'Ano Referência',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'situacao',
                'label' => 'Situação',
                'type' => 'select_from_array',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'options' => $this->retornaArrayDescresDescricaoCodigoItens(
                    'Status Apropriação Instrumento de Cobrança SIAFI'
                )
            ],
        ];

        return $colunas;
    }

    public function Campos($contrato, $tipolistafatura, $contrato_id)
    {
        $con = Contrato::find($contrato_id);

        $campos = [
            [
                'name' => 'contrato_id',
                'label' => "Número do instrumento",
                'type' => 'select_from_array',
                'options' => $contrato,
                'allows_null' => false,
                'attributes' => [
                    'readonly' => 'readonly',
                    'style' => 'pointer-events: none;touch-action: none;'
                ],
                'tab' => 'Dados Fatura',
            ],
            [
                'name' => 'tipolistafatura_id',
                'label' => "Tipo Lista Fatura",
                'type' => 'select2_from_array',
                'options' => $tipolistafatura,
                'allows_null' => true,
                'tab' => 'Dados Fatura',
            ],
            [
                'name' => 'numero',
                'label' => "Número",
                'type' => 'text',
                'attributes' => [
                    'maxlength' => '17',
                    'onkeyup' => "maiuscula(this)",
                ],
                'tab' => 'Dados Fatura',
            ],
            [
                'name' => 'emissao',
                'label' => "Dt. Emissão",
                'type' => 'date',
                'tab' => 'Dados Fatura',
            ],
            [
                'name' => 'vencimento',
                'label' => "Dt. limite pagamento",
                'type' => 'date',
                'tab' => 'Dados Fatura',
            ],

            [
                'name' => 'valor',
                'label' => 'Valor',
                'type' => 'money_fatura',
                'attributes' => [
                    'id' => 'valor',
                ],
                'prefix' => "R$",
                'tab' => 'Dados Fatura',
            ],
            [
                'name' => 'juros',
                'label' => 'Juros',
                'type' => 'money_fatura',
                'attributes' => [
                    'id' => 'juros',
                ],
                'prefix' => "R$",
                'tab' => 'Dados Fatura',
            ],
            [
                'name' => 'multa',
                'label' => 'Multa',
                'type' => 'money_fatura',
                'attributes' => [
                    'id' => 'multa',
                ],
                'prefix' => "R$",
                'tab' => 'Dados Fatura',
            ],
            [
                'name' => 'glosa',
                'label' => 'Glosa',
                'type' => 'money_fatura',
                'attributes' => [
                    'id' => 'glosa',
                ],
                'prefix' => "R$",
                'tab' => 'Dados Fatura',
            ],
            [
                'name' => 'processo',
                'label' => "Processo",
                'type' => 'numprocesso',
                'tab' => 'Outras Informações',
            ],
            [
                'name' => 'protocolo',
                'label' => "Dt. Protocolo",
                'type' => 'date',
                'tab' => 'Outras Informações',
            ],
            [
                'name' => 'ateste',
                'label' => "Dt. Liquidação",
                'type' => 'date',
                'tab' => 'Outras Informações',
            ],
            [
                'name' => 'repactuacao',
                'label' => "Repactuação?",
                'type' => 'radio',
                'options' => [0 => 'Não', 1 => 'Sim'],
                'default' => 0,
                'inline' => true,
                'tab' => 'Outras Informações',
            ],
            [
                'name' => 'infcomplementar',
                'label' => "Informações Complementares",
                'type' => 'text',
                'attributes' => [
                    'onkeyup' => "maiuscula(this)",
                ],
                'tab' => 'Outras Informações',
            ],
            [
                'name' => 'mesref',
                'label' => "Mês Referência",
                'type' => 'select2_from_array',
                'options' => config('app.meses_referencia_fatura'),
                'allows_null' => false,
                'tab' => 'Outras Informações',
            ],
            [
                'name' => 'anoref',
                'label' => "Ano Referência",
                'type' => 'select2_from_array',
                'options' => config('app.anos_referencia_fatura'),
                'default' => date('Y'),
                'allows_null' => false,
                'tab' => 'Outras Informações',
            ],
            [
                'label' => "Empenhos",
                'type' => 'select2_multiple',
                'name' => 'empenhos',
                'entity' => 'empenhos',
                'attribute' => 'numero',
                'model' => "App\Models\Empenho",
                'pivot' => true,
                'options' => (function ($query) use ($con) {
                    return $query->orderBy('numero', 'ASC')
                        ->select(['id', DB::raw('case
                           when left(numero, 4) = date_part(\'year\', current_date)::text
                               then numero || \' - Saldo a Liquidar: R$ \' || aliquidar
                           else numero || \' - Saldo RP  a Liquidar: R$ \' || rpaliquidar
                           end as numero')
                        ])
                        ->where('unidade_id', session()->get('user_ug_id'))
                        ->where('fornecedor_id', $con->fornecedor_id)
                        ->get();
                }),
                'tab' => 'Outras Informações',
            ],
            [
                'name' => 'situacao',
                'label' => "Situação",
                'type' => 'select_from_array',
                'options' => $this->retornaArrayDescresDescricaoCodigoItens(
                    'Status Apropriação Instrumento de Cobrança SIAFI'
                ),
                'default' => 'PEN',
                'attributes' => [
                    'readonly' => 'readonly',
                    'style' => 'pointer-events: none;touch-action: none;'
                ],
                'allows_null' => false,
                'tab' => 'Outras Informações',
            ],
        ];

        return $campos;
    }

    public function store(StoreRequest $request)
    {
        $v = number_format(floatval(str_replace(',', '.', str_replace('.', '', $request->input('valor')))), 2, '.', '');
        $j = number_format(floatval(str_replace(',', '.', str_replace('.', '', $request->input('juros')))), 2, '.', '');
        $m = number_format(floatval(str_replace(',', '.', str_replace('.', '', $request->input('multa')))), 2, '.', '');
        $g = number_format(floatval(str_replace(',', '.', str_replace('.', '', $request->input('glosa')))), 2, '.', '');
        $vl = number_format(floatval($v + $j + $m - $g), 2, '.', '');

        if ($request->input('vencimento')) {
            $request->request->set('prazo', $request->input('vencimento'));
        } else {
            $tipolistafatura = $request->input('tipolistafatura_id');

            if ($tipolistafatura == '5') {
                $ateste = $request->input('ateste');
                $request->request->set('prazo', date('Y-m-d', strtotime("+5 days", strtotime($ateste))));
            } else {
                $ateste = $request->input('ateste');
                $request->request->set('prazo', date('Y-m-d', strtotime("+30 days", strtotime($ateste))));
            }
        }

        $request->request->set('valor', $v);
        $request->request->set('juros', $j);
        $request->request->set('multa', $m);
        $request->request->set('glosa', $g);
        $request->request->set('valorliquido', $vl);

        $request->request->set('situacao', 'PEN');

        $redirect_location = parent::storeCrud($request);

        return $redirect_location;
    }


    public function update(UpdateRequest $request)
    {
        $contrato_id = $request->input('contrato_id');
        $situacao = $request->input('situacao');

        if ($situacao == 'PEN') {
            $v = number_format(floatval(str_replace(',', '.', str_replace('.', '', $request->input('valor')))), 2, '.', '');
            $j = number_format(floatval(str_replace(',', '.', str_replace('.', '', $request->input('juros')))), 2, '.', '');
            $m = number_format(floatval(str_replace(',', '.', str_replace('.', '', $request->input('multa')))), 2, '.', '');
            $g = number_format(floatval(str_replace(',', '.', str_replace('.', '', $request->input('glosa')))), 2, '.', '');
            $vl = number_format(floatval($v + $j + $m - $g), 2, '.', '');

            if ($request->input('vencimento')) {
                $request->request->set('prazo', $request->input('vencimento'));
            } else {
                $tipolistafatura = $request->input('tipolistafatura_id');

                if ($tipolistafatura == '5') {
                    $ateste = $request->input('ateste');
                    $request->request->set('prazo', date('Y-m-d', strtotime("+5 days", strtotime($ateste))));
                } else {
                    $ateste = $request->input('ateste');
                    $request->request->set('prazo', date('Y-m-d', strtotime("+30 days", strtotime($ateste))));
                }
            }

            $request->request->set('valor', $v);
            $request->request->set('juros', $j);
            $request->request->set('multa', $m);
            $request->request->set('glosa', $g);
            $request->request->set('valorliquido', $vl);

            $redirect_location = parent::updateCrud($request);
            return $redirect_location;

        } else {
            \Alert::error('Essa Fatura não pode ser alterada!')->flash();
            return redirect('/gescon/meus-contratos/' . $contrato_id . '/faturas');
        }
    }

    public function show($id)
    {
        $content = parent::show($id);

        $contratoFaturas = $this->findContratoFaturaToShowView($id);

        if($contratoFaturas !== null)
        {
            $content->entry = $contratoFaturas;
        }

        $removeColumns = [  
            'numero_contrato',
            'descr_fornecedor',
            'Lista_tipo_fatura',
            'justificativa_fatura',
        ];

        $addColumnsShowOnly = [
            'colunas' => [
                    [
                        'name' => 'numero_contrato',
                        'label' => 'Contrato',
                        'type' => 'model_function',
                        'function_name' => 'getContrato'
                    ],
                    [
                        'name' => 'descr_fornecedor',
                        'label' => 'Fornecedor',
                        'type' => 'model_function',
                        'function_name' => 'getFornecedor'
                    ],
                    [
                        'name' => 'Lista_tipo_fatura',
                        'label' => 'Tipo Lista',
                        'type' => 'model_function',
                        'function_name' => 'getTipoListaFatura'
                    ],
                    [
                        'name' => 'justificativa_fatura',
                        'label' => 'Justificativa',
                        'type' => 'model_function',
                        'function_name' => 'getJustificativaFatura'
                    ]
                ]
            ];

        $this->crud->removeColumns($removeColumns);
        $this->addColumnsShowOnly($addColumnsShowOnly['colunas']);

        $this->crud->removeColumn('contrato_id');
        $this->crud->removeColumn('tipolistafatura_id');
        $this->crud->removeColumn('justificativafatura_id');
        $this->crud->removeColumn('sfpadrao_id');
        $this->crud->removeColumn('valor');
        $this->crud->removeColumn('juros');
        $this->crud->removeColumn('multa');
        $this->crud->removeColumn('glosa');
        $this->crud->removeColumn('valorliquido');

        return $content;
    }

    public function findContratoFaturaToShowView($idContratoFatura)
    {
        return ContratoFatura::select(
            'contratofaturas.*',
            'contratos.numero as numero_contrato',
            'contratos.processo as processo_contrato',
            'contratos.id as contrato_gerenciadora',
            'contratounidadesdescentralizadas.contrato_id as contrato_desc',
            'contratofaturas.id as id_para_empenho'
        )
            ->join('tipolistafatura', 'tipolistafatura.id', '=', 'contratofaturas.tipolistafatura_id')
            ->leftJoin('contratos', 'contratos.id', '=', 'contratofaturas.contrato_id')
            ->leftJoin('contratounidadesdescentralizadas', 'contratounidadesdescentralizadas.contrato_id', '=', 'contratos.id')
            ->leftJoin('fornecedores', 'fornecedores.id', '=', 'contratos.fornecedor_id')
            ->where(function ($query) {
                $userUgId = session('user_ug_id');
                $query->where('contratos.unidade_id', '=', $userUgId)
                    ->orWhere('contratounidadesdescentralizadas.unidade_id', '=', $userUgId);
            })
            ->whereNull('contratofaturas.deleted_at')
            ->where('contratofaturas.id', $idContratoFatura)
            ->orderByDesc('contratofaturas.vencimento')
            ->first();
    }

    /**
     * Adiciona todos os filtros desejados para esta funcionalidade
     *
     * @author Anderson Sathler <asathler@gmail.com>
     */
    private function adicionaFiltros()
    {
        $this->adicionaFiltroAno();
        $this->adicionaFiltroUgContrato();
        $this->adicionaFiltroUgDescentralizada();
        $this->adicionaFiltroNumeroFatura();
        $this->adicionaFiltroNumeroContrato();
        $this->adicionaFiltroJustificativa();
        $this->adicionaFiltroDataEmissao();
        $this->adicionaFiltroDataAteste();
        $this->adicionaFiltroDataVencimento();
       // $this->adicionaFiltroDataPrazoPagamento();
        $this->adicionaFiltroDataProtocolo();
        $this->adicionaFiltroSituacao();
        $this->adicionaFiltroLei($this->crud);
    }

    /**
     * Adiciona o filtro ao campo Número da Fatura
     *
     * @author Anderson Sathler <asathler@gmail.com>
     */
    private function adicionaFiltroNumeroFatura()
    {
        $campo = [
            'name' => 'numero',
            'type' => 'text',
            'label' => 'Núm. inst. cobrança'
        ];

        $this->crud->addFilter(
            $campo,
            null,
            function ($value) {
                $this->crud->addClause('where', 'contratofaturas.numero', $value);
            }
        );
    }

    /**
     * Adiciona o filtro ao campo Número do Contrato
     *
     * @author Anderson Sathler <asathler@gmail.com>
     */
    private function adicionaFiltroNumeroContrato()
    {
        $campo = [
            'name' => 'contrato',
            'type' => 'select2',
            'label' => 'Núm. Contrato'
        ];

        $contratos = $this->retornaContratos();

        $this->crud->addFilter(
            $campo,
            $contratos,
            function ($value) {
                $this->crud->addClause('where', 'contratos.numero', $value);
            }
        );
    }

    /**
     * Adiciona o filtro ao campo Justificativa
     *
     * @author Anderson Sathler <asathler@gmail.com>
     */
    private function adicionaFiltroJustificativa()
    {
        $campo = [
            'name' => 'justificativa',
            'type' => 'select2',
            'label' => 'Justificativa'
        ];

        $justificativas = $this->retornaJustificativas();

        $this->crud->addFilter(
            $campo,
            $justificativas,
            function ($value) {
                $this->crud->addClause('where', 'contratofaturas.justificativafatura_id', $value);
            }
        );
    }

    /**
     * Adiciona o filtro ao campo Data de Emissão
     *
     * @author Anderson Sathler <asathler@gmail.com>
     */
    private function adicionaFiltroDataEmissao()
    {
        $campo = [
            'name' => 'dt_emissao',
            'type' => 'date_range',
            'label' => 'Dt. Emissão'
        ];

        $this->crud->addFilter(
            $campo,
            null,
            function ($value) {
                $dates = json_decode($value);
                $this->crud->addClause('where', 'contratofaturas.emissao', '>=', $dates->from . ' 00:00:00');
                $this->crud->addClause('where', 'contratofaturas.emissao', '<=', $dates->to . ' 23:59:59');
            }
        );
    }

    /**
     * Adiciona o filtro ao campo Data de Ateste
     *
     * @author Anderson Sathler <asathler@gmail.com>
     */
    private function adicionaFiltroDataAteste()
    {
        $campo = [
            'name' => 'dt_ateste',
            'type' => 'date_range',
            'label' => 'Dt. Liquidação'
        ];

        $this->crud->addFilter(
            $campo,
            null,
            function ($value) {
                $dates = json_decode($value);
                $this->crud->addClause('where', 'contratofaturas.ateste', '>=', $dates->from . ' 00:00:00');
                $this->crud->addClause('where', 'contratofaturas.ateste', '<=', $dates->to . ' 23:59:59');
            }
        );
    }

    /**
     * Adiciona o filtro ao campo Data de Vencimento
     *
     * @author Anderson Sathler <asathler@gmail.com>
     */
    private function adicionaFiltroDataVencimento()
    {
        $campo = [
            'name' => 'dt_vencimento',
            'type' => 'date_range',
            'label' => 'Dt. limite pagamento'
        ];

        $this->crud->addFilter(
            $campo,
            null,
            function ($value) {
                $dates = json_decode($value);
                $this->crud->addClause('where', 'contratofaturas.vencimento', '>=', $dates->from . ' 00:00:00');
                $this->crud->addClause('where', 'contratofaturas.vencimento', '<=', $dates->to . ' 23:59:59');
            }
        );
    }

    /**
     * Adiciona o filtro ao campo Data do Prazo de Pagamento
     *
     * @author Anderson Sathler <asathler@gmail.com>
     */
    private function adicionaFiltroDataPrazoPagamento()
    {
        $campo = [
            'name' => 'dt_prazo',
            'type' => 'date_range',
            'label' => 'Prazo'
        ];

        $this->crud->addFilter(
            $campo,
            null,
            function ($value) {
                $dates = json_decode($value);
                $this->crud->addClause('where', 'contratofaturas.prazo', '>=', $dates->from . ' 00:00:00');
                $this->crud->addClause('where', 'contratofaturas.prazo', '<=', $dates->to . ' 23:59:59');
            }
        );
    }

    /**
     * Adiciona o filtro ao campo Data do Protocolo
     *
     * @author Anderson Sathler <asathler@gmail.com>
     */
    private function adicionaFiltroDataProtocolo()
    {
        $campo = [
            'name' => 'dt_protocolo',
            'type' => 'date_range',
            'label' => 'Dt. Protocolo'
        ];

        $this->crud->addFilter(
            $campo,
            null,
            function ($value) {
                $dates = json_decode($value);
                $this->crud->addClause('where', 'contratofaturas.protocolo', '>=', $dates->from . ' 00:00:00');
                $this->crud->addClause('where', 'contratofaturas.protocolo', '<=', $dates->to . ' 23:59:59');
            }
        );
    }

    /**
     * Adiciona o filtro ao campo Situação
     *
     * @author Anderson Sathler <asathler@gmail.com>
     */
    private function adicionaFiltroSituacao()
    {
        $campo = [
            'name' => 'situacao',
            'type' => 'select2',
            'label' => 'Situação'
        ];

        //$situacoes = config('app.situacao_fatura');
        $situacoes = $this->retornaArrayDescresDescricaoCodigoItens(
            'Status Apropriação Instrumento de Cobrança SIAFI'
        );

        $this->crud->addFilter(
            $campo,
            $situacoes,
            function ($value) {
                $this->crud->addClause('where', 'contratofaturas.situacao', $value);
            }
        );
    }

    private function adicionaFiltroUgContrato()
    {
        $campo = [
            'name' => 'ug_contrato',
            'type' => 'text',
            'label' => 'Ug Contrato'
        ];

        $this->crud->addFilter(
            $campo,
            null,
            function ($value) {
                $this->crud->addClause('leftJoin', 'unidades', 'unidades.id', '=', 'contratos.unidade_id');
                $this->crud->addClause('where', 'unidades.codigo', '=', $value);
            }
        );
    }

    private function adicionaFiltroUgDescentralizada()
    {
        $campo = [
            'name' => 'ug_descentralizada',
            'type' => 'text',
            'label' => 'Ug Descentralizada'
        ];

        $this->crud->addFilter(
            $campo,
            null,
            function ($value) {
                $this->crud->addClause('leftJoin', 'unidades', 'unidades.id', '=', 'contratounidadesdescentralizadas.unidade_id');
                $this->crud->addClause('where', 'unidades.codigo', '=', $value);
            }
        );
    }

    private function adicionaFiltroAno()
    {
        $anos = [];

        $campo = [
            'name' => 'emissao',
            'type' => 'select2',
            'label' => 'Ano Emissão'
        ];

        for ($ano = date('Y'); $ano >= 2019; $ano--) {
            $anos[$ano] = $ano;
        }        

        $this->crud->addFilter(
            $campo, $anos, function ($value) {
            $this->crud->addClause('whereYear', 'contratofaturas.emissao', $value);
        });
    }

    /**
     * Retorna dados dos Contratos para exibição no controle de filtro
     *
     * @return array
     * @author Anderson Sathler <asathler@gmail.com>
     */
    private function retornaContratos()
    {
        $dados = Contrato::select(
            \Illuminate\Support\Facades\DB::raw("LEFT(CONCAT(numero, ' - ', objeto), 80) AS descricao"), 'numero'
        );

        $dados->where('situacao', true);
        $dados->whereHas('unidade', function ($u) {
            $u->where('codigo', session('user_ug'));
        });
        $dados->orderBy('id'); // 'data_publicacao'

        return $dados->pluck('descricao', 'numero')->toArray();
    }

    /**
     * Retorna dados de Fornecedores para exibição no controle de filtro
     *
     * @return array
     * @author Anderson Sathler <asathler@gmail.com>
     */
    private function retornaFornecedores()
    {
        $dados = Fornecedor::select(
            DB::raw("CONCAT(cpf_cnpj_idgener, ' - ', nome) AS descricao"), 'cpf_cnpj_idgener'
        );

        $dados->whereHas('contratos', function ($c) {
            $c->where('situacao', true);
            $c->where('contratos.elaboracao', false);
        });

        return $dados->pluck('descricao', 'cpf_cnpj_idgener')->toArray();
    }

    /**
     * Retorna dados das Justificativas para exibição no controle de filtro
     *
     * @return array
     * @author Anderson Sathler <asathler@gmail.com>
     */
    private function retornaJustificativas()
    {
        $dados = Justificativafatura::select(
            'id',
            DB::raw("CONCAT(nome, ' - ', LEFT(descricao, 80)) as descricao")
        );

        return $dados->pluck('descricao', 'id')->toArray();
    }

    /**
     * Retorna dados das Justificativas para exibição no combo de edição
     *
     * @return array
     * @author Anderson Sathler <asathler@gmail.com>
     */
    private function retornaJustificativasCombo()
    {
        $justificativas = $this->retornaJustificativas();
        $justificativas[''] = 'Selecione a justificativa';

        ksort($justificativas);

        return $justificativas;
    }
}
