<?php

namespace App\Http\Controllers\Execfin;

use Alert;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\Transparencia\IndexController;
use App\Http\Traits\Busca;
use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\Formatador;
use App\Jobs\AtualizaNaturezaDespesasJob;
use App\Jobs\AtualizasaldosmpenhosJobs;
use App\Jobs\EnviaEmpenhoSiasgJob;
use App\Jobs\MigracaoCargaEmpenhoJob;
use App\Jobs\MigracaoempenhoJob;
use App\Jobs\MigracaoRpJob;
use App\Jobs\RotinaAtualizaSaldoPorUnidadeJob;
use App\Models\BackpackUser;

//use App\Models\Codigoitem;
//use App\Models\CompraItemUnidade;
use App\Models\DevolveMinutaSiasg;
use App\Models\Empenho;
use App\Models\Empenhodetalhado;
use App\Models\Fornecedor;
use App\Models\Jobs;
use App\Models\MinutaEmpenho;
use App\Models\Naturezadespesa;
use App\Models\Naturezasubitem;
use App\Models\Planointerno;
use App\Models\SfItemEmpenho;
use App\Models\SfOrcEmpenhoDados;
use App\Models\Unidade;
use App\Http\Traits\HelperTrait;

//use App\STA\ConsultaApiSta;
//use App\XML\ApiSiasg;
use App\services\STA\STAUrlFetcherService;
use App\STA\ConsultaApiSta;
use App\XML\Execsiafi;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\EmpenhoRequest as StoreRequest;
use App\Http\Requests\EmpenhoRequest as UpdateRequest;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class EmpenhoCrudController extends CrudController
{
    use Busca, BuscaCodigoItens, Formatador, HelperTrait;

    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */

        //pega a unidade para poder usar o codigo siafi issues - 578
        $unidadeLogada = Unidade::find(session()->get('user_ug_id'));
        //$isSiafiDifSiasg = $unidadeLogada->codigosiasg != $unidadeLogada->codigosiafi;

        $this->crud->setModel('App\Models\Empenho');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/execfin/empenho');
        $this->crud->setEntityNameStrings('Empenho', 'Empenhos');

        $this->crud->addClause('select', [
            'empenhos.*',
            \DB::raw("concat(unidades.codigosiafi, ' - ', unidades.nomeresumido) as unidade_emitente_empenho"),
            \DB::raw("concat(unidade_minuta.codigosiafi, ' - ', unidade_minuta.nomeresumido) as unidade_minuta_empenho"),
        ]);
        $this->crud->addClause('join', 'fornecedores', 'fornecedores.id', '=', 'empenhos.fornecedor_id');
        $this->crud->addClause('join', 'unidades', 'unidades.id', '=', 'empenhos.unidade_id');
        $this->crud->addClause('join', 'naturezadespesa', 'naturezadespesa.id', '=', 'empenhos.naturezadespesa_id');

        $this->crud->addClause('leftjoin', 'minutaempenhos', function ($join) {
            $join->on('empenhos.numero', '=', 'minutaempenhos.mensagem_siafi')
                ->on('empenhos.fornecedor_id', '=', 'minutaempenhos.fornecedor_empenho_id')
                ->on('empenhos.unidade_id', '=', 'minutaempenhos.unidade_id');
        });

        $this->crud->addClause('leftjoin', 'saldo_contabil', function ($join) {
            $join->on('saldo_contabil.id', '=', 'minutaempenhos.saldo_contabil_id')
                ->on('saldo_contabil.unidade_id', '=', 'empenhos.unidade_id');
        });

        $this->crud->addClause('leftjoin', 'unidades as unidade_minuta', function ($join) {
            $join->on('unidade_minuta.id', '=', 'minutaempenhos.unidade_id');
        });

        $this->crud->addClause('where', 'empenhos.unidade_id', '=', $unidadeLogada->id);//issues - 578
        $this->crud->orderBy('empenhos.numero', 'desc');

        # Se não for ambiente de produção e o usuário tem permissão de migrar empenhos
        if (/*config('app.app_amb') <> 'Ambiente Produção' &&*/ backpack_user()->can('migracao_empenhos')) {

            $this->crud->addButtonFromView(
                'top',
                'atualizanaturezassubitens',
                'atualizanaturezassubitens',
                'end'
            );

            $this->crud->addButtonFromView(
                'top',
                'migrarempenho',
                'migrarempenho',
                'end'
            );

            $this->crud->addButtonFromView(
                'top',
                'atualizasaldosempenhos',
                'atualizasaldosempenhos',
                'end'
            );

            $this->crud->addButtonFromView(
                'line',
                'delete',
                'delete_empenho',
                'end'
            );
        }


        $this->crud->addButtonFromView('line', 'moreempenho', 'moreempenho', 'end');

        $this->crud->enableExportButtons();
        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');
        $this->crud->allowAccess('show');

        (backpack_user()->hasRole('Administrador')) ? $this->crud->allowAccess('create') : null;
        (backpack_user()->can('empenho_editar')) ? $this->crud->allowAccess('update') : null;
        (backpack_user()->can('empenho_deletar')) ? $this->crud->allowAccess('delete') : null;

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns

        $colunas = $this->Colunas();
        $this->crud->addColumns($colunas);

        //$unidade = [session()->get('user_ug_id') => session()->get('user_ug')];
        $unidade = [$unidadeLogada->id => $unidadeLogada->codigosiafi];//issues - 578

        $campos = $this->Campos($unidade);

        $this->crud->addFields($campos);
        $this->aplicaFiltroAno();

        // Se não tiver o filtro de ano, ele lista apenas os empenhos do ano atual
        if($this->crud->getFilter('ano')->currentValue === null){
            $this->crud->addClause('where', DB::raw('left(empenhos."numero", 4)'), '=', date('Y'));
        }

        // add asterisk for fields that are required in EmpenhoRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function destroy($id)
    {
        # Apaga os itens do empenho
        Empenhodetalhado::where('empenho_id', $id)->delete();

        # Executa a exclusão do empenho pelo backpack
        $exclusao = parent::destroy($id);

        return $exclusao;
    }

    public function Colunas()
    {
        $colunas = [
            [
                'name' => 'unidade_emitente_empenho',
                'label' => 'Unidade Emitente do Empenho',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere('unidades.codigosiafi', 'like', "%$searchTerm%");
                    $query->orWhere('unidades.nome', 'like', "%" . strtoupper($searchTerm) . "%");
                    $query->orWhere('unidades.nomeresumido', 'like', "%" . strtoupper($searchTerm) . "%");
                },
            ],
            [
                'name' => 'unidade_minuta_empenho',
                'label' => 'Unidade da Minuta do Empenho',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere('unidade_minuta.codigosiafi', 'like', "%$searchTerm%");
                    $query->orWhere('unidade_minuta.nome', 'like', "%" . strtoupper($searchTerm) . "%");
                    $query->orWhere('unidade_minuta.nomeresumido', 'like', "%" . strtoupper($searchTerm) . "%");
                },
            ],
            [
                'name' => 'numero',
                'label' => 'Número Empenho',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'data_emissao',
                'label' => 'Data emissão',
                'type' => 'date',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'searchLogic' => $this->retornaSearchData('empenhos', 'data_emissao')
            ],
            [
                'name' => 'getFornecedor',
                'label' => 'Fornecedor',
                'type' => 'model_function',
                'function_name' => 'getFornecedor',
                'orderable' => true,
                'limit' => 1000,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere('fornecedores.cpf_cnpj_idgener', 'like', "%$searchTerm%");
                    $query->orWhere('fornecedores.nome', 'like', "%" . strtoupper($searchTerm) . "%");
                },
            ],
            [
                'name' => 'getPi',
                'label' => 'Plano Interno',
                'type' => 'model_function',
                'function_name' => 'getPi',
                'orderable' => true,
                'limit' => 1000,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'getNatureza',
                'label' => 'Natureza Despesa',
                'type' => 'model_function',
                'function_name' => 'getNatureza',
                'orderable' => true,
                'limit' => 1000,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere('naturezadespesa.codigo', 'like', "%" . strtoupper($searchTerm) . "%");
                    $query->orWhere('naturezadespesa.descricao', 'like', "%" . strtoupper($searchTerm) . "%");
                },
            ],
            [
                'name' => 'formatVlrEmpenhado',
                'label' => 'Empenhado',
                'type' => 'model_function',
                'function_name' => 'formatVlrEmpenhado',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'formatVlraLiquidar',
                'label' => 'a Liquidar',
                'type' => 'model_function',
                'function_name' => 'formatVlraLiquidar',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'formatVlrLiquidado',
                'label' => 'Liquidado',
                'type' => 'model_function',
                'function_name' => 'formatVlrLiquidado',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'formatVlrPago',
                'label' => 'Pago',
                'type' => 'model_function',
                'function_name' => 'formatVlrPago',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'formatVlrRpInscrito',
                'label' => 'RP Inscrito',
                'type' => 'model_function',
                'function_name' => 'formatVlrRpInscrito',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'formatVlrRpaLiquidar',
                'label' => 'RP a Liquidar',
                'type' => 'model_function',
                'function_name' => 'formatVlrRpaLiquidar',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'formatVlrRpLiquidado',
                'label' => 'RP Liquidado',
                'type' => 'model_function',
                'function_name' => 'formatVlrRpLiquidado',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'formatVlrRpPago',
                'label' => 'RP Pago',
                'type' => 'model_function',
                'function_name' => 'formatVlrRpPago',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],

        ];

        return $colunas;
    }

    public function Campos($unidade)
    {
        $campos = [
            [
                'name' => 'unidade_id',
                'label' => "Unidade Gestora",
                'type' => 'select2_from_array',
                'options' => $unidade,
                'allows_null' => false,
            ],
            [
                'name' => 'numero',
                'label' => "Número Empenho",
                'type' => 'empenhoexercicio',
            ],
            [
                'name' => 'data_emissao',
                'label' => "Data emissão",
                'type' => 'date',
            ],
            [
                'label' => "Credor / Fornecedor",
                'type' => "select2_from_ajax",
                'name' => 'fornecedor_id',
                'entity' => 'fornecedor',
                'attribute' => "cpf_cnpj_idgener",
                'attribute2' => "nome",
                'process_results_template' => 'gescon.process_results_fornecedor',
                'model' => "App\Models\Fornecedor",
                'data_source' => url("api/fornecedor"),
                'placeholder' => "Selecione o fornecedor",
                'minimum_input_length' => 2,
            ],
            [
                'label' => "Plano Interno (PI)",
                'type' => "select2_from_ajax",
                'name' => 'planointerno_id',
                'entity' => 'planointerno',
                'attribute' => "codigo",
                'attribute2' => "descricao",
                'process_results_template' => 'gescon.process_results_planointerno',
                'model' => "App\Models\Planointerno",
                'data_source' => url("api/planointerno"),
                'placeholder' => "Selecione o Plano Interno",
                'minimum_input_length' => 2,
            ],
            [
                'label' => "Natureza Despesa (ND)",
                'type' => "select2_from_ajax",
                'name' => 'naturezadespesa_id',
                'entity' => 'naturezadespesa',
                'attribute' => "codigo",
                'attribute2' => "descricao",
                'process_results_template' => 'gescon.process_results_planointerno',
                'model' => "App\Models\Naturezadespesa",
                'data_source' => url("api/naturezadespesa"),
                'placeholder' => "Selecione a Natureza de Despesa",
                'minimum_input_length' => 2,
            ],
        ];

        return $campos;
    }

    public function store(StoreRequest $request)
    {
        $fornecedores = Fornecedor::select(DB::raw("CONCAT(cpf_cnpj_idgener,' - ',nome) AS nome"), 'id')
            ->orderBy('nome', 'asc')->pluck('nome', 'id')->toArray();

        $redirect_location = parent::storeCrud($request);
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        $redirect_location = parent::updateCrud($request);
        return $redirect_location;
    }

    public function show($id)
    {
        $content = parent::show($id);

        $this->crud->removeColumn('fornecedor_id');
        $this->crud->removeColumn('unidade_id');
        $this->crud->removeColumn('planointerno_id');
        $this->crud->removeColumn('naturezadespesa_id');

        $this->crud->removeColumn('empenhado');
        $this->crud->removeColumn('aliquidar');
        $this->crud->removeColumn('liquidado');
        $this->crud->removeColumn('pago');
        $this->crud->removeColumn('rpinscrito');
        $this->crud->removeColumn('rpaliquidar');
        $this->crud->removeColumn('rpliquidado');
        $this->crud->removeColumn('rppago');

        return $content;
    }

    protected function aplicaFiltroAno()
    {
        $ano = date('Y');
        $rangeAno = $ano - 10;
        $arrayAnos = [];

        for ($ano; $rangeAno <= $ano; $ano--) {
            $arrayAnos[$ano] = $ano;
        }

        $this->crud->addFilter(
            [
                'name' => 'ano',
                'type' => 'select2',
                'label' => 'Ano'
            ],
            $arrayAnos,
        function ($value) use ($arrayAnos) {
                if(!in_array($value, $arrayAnos)) {
                    $ano = date('Y');
                    $this->crud->addClause(
                        'where',
                        DB::raw('left(empenhos."numero", 4)'), '=', $ano
                    );
                }else{
                    $this->crud->addClause(
                        'where',
                        DB::raw('left(empenhos."numero", 4)'), '=', json_decode($value)
                    );
                }
            }
        );
    }

    public function executaMigracaoEmpenho($ano = null, $un = null, $dia = null, bool $rp = false, $rponly = false)
    {
        # Se foi iniciado por tela, verifica se ainda há processos em andamento
        if (backpack_user()) {

            $quantidadeJobsNatureza = Jobs::where('queue', 'atualizacaond')->count();
            $quantidadeJobsEmpenho = Jobs::where('queue', 'migracaoempenho')->count();
            $haProcessosEmAndamento = false;
            $msg = 'Há processos de ';
            $conjuncao = ' ';

            if ($quantidadeJobsNatureza) {
                $msg .= 'atualização de natureza de despesa ';
                $conjuncao = ' e ';
                $haProcessosEmAndamento = true;
            }

            if ($quantidadeJobsEmpenho) {
                $msg .= $conjuncao . 'migração de empenho ';
                $haProcessosEmAndamento = true;
            }

            $msg .= 'em andamento. Por favor, aguarde.';

            if ($haProcessosEmAndamento) {
                Alert::warning($msg)->flash();
                return redirect('/execfin/empenho');
            }

        }

        $idDoProcesso = $id = Str::uuid();
        $prefix = __CLASS__ . ' | ' . $idDoProcesso . ' | ';

        Log::channel('migracaoempenho')->info($prefix . 'Início do processo: ' . date('d/m/Y H:i:s'));

        if (!$ano) {
            $ano = date('Y');
        }

        //se for início de exercício
        if (date('md') == '0101' or
            date('md') == '0102' or
            date('md') == '0103' or
            date('md') == '0104' or
            date('md') == '0105' or
            date('md') == '0106' or
            date('md') == '0107' or
            date('md') == '0108' or
            date('md') == '0109' or
            date('md') == '0110'
        ) {
            $ano = $ano - 1;
            $dia = $ano . '-12-20';
        }

        //inicio de exercicio carrega Restos a pagar
        if (!$rp and (date('md') == '0120') or date('md') == '0201' or date('md') == '0301') {
            $rp = true;
            $dia = $ano . '-01-01';
        }

        if (!$dia) {
            $dia = Carbon::parse(date('Y-m-d'))->subDay(10)->format('Y-m-d');
        }
        $param = [
            'ano' => $ano,
            'dia' => $dia
        ];
        //Executa Atualização de RP´s
        if (($rp or $rponly) and $ano == date('Y')) {

            if ($un) {
                $unidade = Unidade::where('codigosiafi', $un)->first();
                if (!isset($unidade->codigosiafi)) {
                    return false;
                }
                if ($rponly == true) {

                    Log::channel('migracaoempenho')
                        ->info($prefix . 'Esta execução iniciará apenas uma ocorrência do job MigracaoRpJob ' .
                            'para buscar os Restos a Pagar da unidade ' . $un);

                    MigracaoRpJob::dispatch($unidade->codigosiafi, $ano, $dia)->onQueue('migracaoempenho');

                    return true;
                } else {

                    Log::channel('migracaoempenho')
                        ->info($prefix . 'Esta execução iniciará apenas uma ocorrência de cada job ' .
                            '(MigracaoRpJob e MigracaoempenhoJob) ' .
                            'para buscar os Restos a Pagar da unidade ' . $un);

                    MigracaoRpJob::dispatch($unidade->codigosiafi, $ano, $dia)->onQueue('migracaoempenho');
                    MigracaoempenhoJob::dispatch($unidade->codigosiafi, $ano, $dia)->onQueue('migracaoempenho');

                    return true;
                }
            }

            // BUSCA AS UNIDADES COM RP CRIADOS NOS ULTIMOS
            // CINCO DIAS A PARTIR DA DATA ENVIADA (PADRÃO HOJE)
            $url = config('migracao.api_sta') .
                $this->replaceRouteParam(config('rotas-sta.unidades-com-rp-ano-dia'), $param);

            Log::channel('migracaoempenho')->info($prefix . 'Requisição à URL para trazer Unidades com RP: ' . $url);

            $urlFetchService = new STAUrlFetcherService($url);
            $unidades = $urlFetchService->fetchDataJson();

            if ($urlFetchService->isError($unidades)) {

                $error = $urlFetchService->getError($unidades);
                Log::channel('migracaoempenho')->error($prefix . ' - Erro ao buscar o unidades no STA ' .
                    '[' . $error->error_code . '] ' . $error->error_message);

                if (backpack_user()) {
                    Alert::error($error->error_message)->flash();
                    return redirect('/execfin/empenho');
                }
            }

            Log::channel('migracaoempenho')->info($prefix . 'Quantidade de unidades retornados ' . count($unidades));

            $unidadesAtivas =
                Unidade::whereHas('contratos', function ($c) {
                    $c->where('situacao', true);
                })
                    ->where('situacao', true)
                    ->select('codigosiafi')
                    ->whereIn('codigosiafi', $unidades)
                    ->get();

            Log::channel('migracaoempenho')
                ->info($prefix . 'Serão agendadas ' . $unidadesAtivas->count() . ' ocorrências do job MigracaoRpJob.');

            foreach ($unidadesAtivas as $unidade) {
                MigracaoRpJob::dispatch($unidade->codigosiafi, $ano, $dia)->onQueue('migracaoempenho');
            }
            if ($rponly == true) {
                return true;
            }
        }

        if ($un) {
            Log::channel('migracaoempenho')
                ->info($prefix . 'Esta execução iniciará apenas uma ocorrência do job MigracaoempenhoJob ' .
                    'para buscar os empenhos da unidade ' . $un . '.');

            $unidade = Unidade::where('codigosiafi', $un)->first();
            if (!isset($unidade->codigosiafi)) {
                return false;
            }

            MigracaoempenhoJob::dispatch($unidade->codigosiafi, $ano, $dia)->onQueue('migracaoempenho');//issue 607
            return true;
        }

        // BUSCA AS UNIDADES COM EMPENHOS CRIADOS NOS ULTIMOS
        // CINCO DIAS A PARTIR DA DATA ENVIADA (PADRÃO HOJE)
        $url = config('migracao.api_sta') .
            $this->replaceRouteParam(config('rotas-sta.unidades-com-empenho-ano-dia'), $param);

        Log::channel('migracaoempenho')
            ->info($prefix . 'Requisição à URL para trazer Unidades com Empenhos: ' . $url);

        $urlFetchService = new STAUrlFetcherService($url);
        $unidades = $urlFetchService->fetchDataJson();

        if ($urlFetchService->isError($unidades)) {

            $error = $urlFetchService->getError($unidades);

            Log::channel('migracaoempenho')->error($prefix . ' - Erro ao buscar o unidades no STA ' .
                '[' . $error->error_code . '] ' . $error->error_message);

            if (backpack_user()) {
                Alert::error($error->error_message)->flash();
                return redirect('/execfin/empenho');
            }
        }

        Log::channel('migracaoempenho')
            ->info($prefix . 'Quantidade de registros unidades com empenhos retornadas: ' . count($unidades));

        $unidadesAtivas = Unidade::whereHas('contratos', function ($c) {
            $c->where('situacao', true);
        })
            ->where('situacao', true)
            ->select('codigosiafi')
            ->whereIn('codigosiafi', $unidades)
            ->get();

        Log::channel('migracaoempenho')
            ->info($prefix . 'Serão agendadas ' . $unidadesAtivas->count() . ' ocorrências do job MigracaoempenhoJob.');

        foreach ($unidadesAtivas as $unidade) {
            MigracaoempenhoJob::dispatch($unidade->codigosiafi, $ano, $dia)
                ->onQueue('migracaoempenho');// issue 607
        }

        Log::channel('migracaoempenho')
            ->info($prefix . 'Fim da execução do processo (não coincidirá cronologicamente ' .
                ' com as execuções dos Jobs de migração de empenho).');

        if (backpack_user()) {
            Alert::success('Migração de Empenhos em andamento.')->flash();
            return redirect('/execfin/empenho');
        } else {
            return true;
        }
    }

    public function executaAtualizacaoNd()
    {
        # Se foi iniciado por tela, verifica se ainda há processos em andamento
        if (backpack_user()) {
            $quantidadeDeJobs = Jobs::where('queue', 'atualizacaond')->count();
            if ($quantidadeDeJobs > 0) {
                Alert::warning('Há processos de atualização de natureza em andamento. Por favor, aguarde.')->flash();
                return redirect('/execfin/empenho');
            }
        }

        AtualizaNaturezaDespesasJob::dispatch()->onQueue('atualizacaond');

        if (backpack_user()) {
            Alert::success('Atualização de Naturezas e Subitens em andamento.')->flash();
            return redirect('/execfin/empenho');
        }
    }

    public function NdAtualizacao()
    {
        $migracao_url = config('migracao.api_sta');
        $url = $migracao_url . config('rotas-sta.estrutura-natureza-despesa');

        $urlFetchService = new STAUrlFetcherService($url);
        $dados = $urlFetchService->fetchDataJson();

        if ($urlFetchService->isError($dados)) {

            $error = $urlFetchService->getError($dados);

            Log::info(__METHOD__ . ' - Erro ao buscar o empenho no STA ' .
                '[' . $error->error_code . '] ' . $error->error_message);

            if (backpack_user()) {
                Alert::error($error->error_message)->flash();
                return redirect('/execfin/empenho');
            }
        }

        foreach ($dados as $dado) {

            $nd = new Naturezadespesa();
            $busca_nd = $nd->buscaNaturezaDespesa($dado);

            $subitem = new Naturezasubitem();
            $busca_si = $subitem->buscaNaturezaSubitem($dado, $busca_nd);
        }

        return 'Atualização realizada com sucesso!';
    }

    public function rotinaAtualizaSaldoAgendamento($ano = null, $ug = null)
    {
        $idDoProcesso = $id = Str::uuid();
        $prefix = __METHOD__ . ' | ' . $idDoProcesso . ' | ';
        Log::channel('atualizasaldone')
            ->info($prefix . 'Início da rotina de atualização de saldos: ' . date('d/m/Y H:i:s'));

        $base = new AdminController();
        if (!$ano) {
            $ano = $base->retornaDataMaisOuMenosQtdTipoFormato('Y', '-', '5', 'Days', date('Y-m-d'));
        }

        if ($ug) {
            if ($ug->codigo == '153173') {
                $this->trataUnidadeFnde($ug, $ano);
            } else {
                RotinaAtualizaSaldoPorUnidadeJob::dispatch(
                    $ug,
                    $ano
                )->onQueue('atualizasaldone');

                //dispara RP
                if ($ano == date('Y')) {
                    RotinaAtualizaSaldoPorUnidadeJob::dispatch(
                        $ug,
                        $ano,
                        null,
                        null,
                        true
                    )->onQueue('atualizasaldone');
                }
            }
            return true;
        }

        $unidades = Unidade::whereHas('contratos', function ($c) {
            $c->where('situacao', true);
        })
            ->where('tipo', 'E')
            ->where('situacao', true)
            ->orderBy('codigo')
            ->get();

        foreach ($unidades as $unidade) {
            if ($unidade->codigo == '153173') {
                $this->trataUnidadeFnde($unidade, $ano);
            } else {
                //dispara empenhos
                RotinaAtualizaSaldoPorUnidadeJob::dispatch(
                    $unidade,
                    $ano,
                    null,
                    null,
                    false,
                    true
                )->onQueue('atualizasaldone');

                //dispara RP
//                if ($ano == date('Y')) {
//                    RotinaAtualizaSaldoPorUnidadeJob::dispatch(
//                        $unidade,
//                        $ano,
//                        null,
//                        null,
//                        true
//                    )->onQueue('atualizasaldone');
//                }
            }
        }
        return true;
    }

    public function trataUnidadeFnde(Unidade $unidade, $ano)
    {
        $count_ne = Empenho::where(DB::raw('left(numero,4)'), $ano)
            ->where('unidade_id', $unidade->id)
            ->count();

        $count_rp = Empenho::where('rp', true)
            ->where('unidade_id', $unidade->id)
            ->count();

        if ($count_ne > 1000 or $count_rp > 1000) {
            //trata Empenhos
            if ($count_ne > 1000) {
                $u = false;
                $offset_ne = 0;
                $limit_ne = 1000;
                while ($limit_ne <= $count_ne) {
                    RotinaAtualizaSaldoPorUnidadeJob::dispatch(
                        $unidade,
                        $ano,
                        $offset_ne,
                        $limit_ne
                    )->onQueue('atualizasaldone');

                    if ($u) {
                        break;
                    }

                    $offset_ne = $limit_ne;
                    $limit_ne = $limit_ne + 1000;
                    if ($limit_ne > $count_ne) {
                        $limit_ne = $count_ne;
                        $u = true;
                    }
                }
            } else {
                RotinaAtualizaSaldoPorUnidadeJob::dispatch(
                    $unidade,
                    $ano
                )->onQueue('atualizasaldone');
            }

            //trata RP
            if ($count_rp > 1000 and $ano == date('Y')) {
                $u = false;
                $offset_rp = 0;
                $limit_rp = 1000;

                while ($limit_rp <= $count_rp) {
                    RotinaAtualizaSaldoPorUnidadeJob::dispatch(
                        $unidade,
                        $ano,
                        $offset_rp,
                        $limit_rp,
                        true
                    )->onQueue('atualizasaldone');

                    if ($u) {
                        break;
                    }

                    $offset_rp = $limit_rp;
                    $limit_rp = $limit_rp + 1000;
                    if ($limit_rp > $count_rp) {
                        $limit_rp = $count_rp;
                        $u = true;
                    }

                }
            } elseif ($ano == date('Y')) {
                RotinaAtualizaSaldoPorUnidadeJob::dispatch(
                    $unidade,
                    $ano,
                    null,
                    null,
                    true
                )->onQueue('atualizasaldone');
            }
        } else {
            //dispara empenhos
            RotinaAtualizaSaldoPorUnidadeJob::dispatch(
                $unidade,
                $ano
            )->onQueue('atualizasaldone');

            //dispara RP
//            if ($ano == date('Y')) {
//                RotinaAtualizaSaldoPorUnidadeJob::dispatch(
//                    $unidade,
//                    $ano,
//                    null,
//                    null,
//                    true
//                )->onQueue('atualizasaldone');
//            }
        }
        return true;
    }

    public function executaAtualizaSaldosEmpenhosPorUnidade(
        Unidade $unidade,
                $rp = false,
                $ano = null,
                $offset = null,
                $limit = null,
                $rotinaAutomatica = false
    )
    {
        $base = new AdminController();
        if (!$ano) {
            $ano = $base->retornaDataMaisOuMenosQtdTipoFormato('Y', '-', '5', 'Days', date('Y-m-d'));
        }

        if($rotinaAutomatica == true){
            $this->executaAtualizaSaldosEmpenhosPorUnidadeRotinaAutomatica($unidade,
                $ano,
                $offset,
                $limit);
            return true;
        }

        if ($rp == true) {
            // Só busca os empenhos entre date('Y') e date('Y') - 2
            $empenhos = Empenho::where('rp', true)
                ->where('unidade_id', $unidade->id)
                ->whereBetween(DB::raw('LEFT(numero, 4)::INTEGER'), [now()->year - 2, now()->year])
                ->orderBy('numero');
        } else {
            $empenhos = Empenho::where(DB::raw('left(numero,4)'), $ano)
                ->where('unidade_id', $unidade->id)
                ->orderBy('numero');
        }

        if ($offset >= 0 and $limit != null) {
            $empenhos->skip($offset)->take($limit - $offset);
        }

        foreach ($empenhos->get() as $empenho) {
            $contas_contabeis = config('app.contas_contabeis_empenhodetalhado_exercicioatual');

            if ($rp == true) {
                $contas_contabeis = config('app.contas_contabeis_empenhodetalhado_exercicioanterior');
            }

            $empenhodetalhes = Empenhodetalhado::where('empenho_id', '=', $empenho->id)
                ->get();

            foreach ($empenhodetalhes as $empenhodetalhe) {
                $contacorrente = $empenho->numero . str_pad(
                        $empenhodetalhe->naturezasubitem->codigo,
                        2,
                        '0',
                        STR_PAD_LEFT
                    );

                $this->atualizaSaldoEmpenhos(
                    $ano,
                    $unidade->codigosiafi,
                    $unidade->gestao,
                    $contas_contabeis,
                    $contacorrente,
                    $empenhodetalhe,
                    $rp
                );
            }
        }

        return true;
    }

    private function executaAtualizaSaldosEmpenhosPorUnidadeRotinaAutomatica(Unidade $unidade, $ano = null, $offset = null, $limit = null)
    {

        $empenhos = Empenho::where('unidade_id', $unidade->id)
            ->whereBetween(DB::raw('LEFT(numero, 4)::INTEGER'), [now()->year - 2, now()->year])
            ->orderBy('numero');

        if ($offset >= 0 and $limit != null) {
            $empenhos->skip($offset)->take($limit - $offset);
        }

        foreach ($empenhos->get() as $empenho) {
            $rp = false;
            $contas_contabeis = config('app.contas_contabeis_empenhodetalhado_exercicioatual');

            if($empenho->rp == true){
                $rp = true;
                $contas_contabeis = config('app.contas_contabeis_empenhodetalhado_exercicioanterior');
            }else{
                if(substr($empenho->numero, 0, 4) != $ano){
                    continue;
                }
            }

            $empenhodetalhes = Empenhodetalhado::where('empenho_id', '=', $empenho->id)
                ->get();

            foreach ($empenhodetalhes as $empenhodetalhe) {
                $contacorrente = $empenho->numero . str_pad(
                        $empenhodetalhe->naturezasubitem->codigo,
                        2,
                        '0',
                        STR_PAD_LEFT
                    );

                $this->atualizaSaldoEmpenhos(
                    $ano,
                    $unidade->codigosiafi,
                    $unidade->gestao,
                    $contas_contabeis,
                    $contacorrente,
                    $empenhodetalhe,
                    $rp
                );
            }
        }

    }

    public function atualizaSaldoEmpenhos(
        string $ano,
        string $codigo_unidade,
        string $gestao_unidade,
        array $contas_contabeis,
        string $contacorrente,
        Empenhodetalhado $empenhodetalhado,
        $rp = false
    ) {
        try {
            $saldocontabilSta = new ConsultaApiSta();
            if ($rp) {
                $this->processarAnos($saldocontabilSta, $ano, $codigo_unidade, $gestao_unidade, $contas_contabeis, $contacorrente, $empenhodetalhado);
            } else {
                $this->processarAno($saldocontabilSta, $ano, $codigo_unidade, $gestao_unidade, $contas_contabeis, $contacorrente, $empenhodetalhado);
            }
        } catch (\Exception $e) {
            // Tratar exceção
        }
    }

    private function processarAnos(
        ConsultaApiSta $saldocontabilSta,
        string $ano,
        string $codigo_unidade,
        string $gestao_unidade,
        array $contas_contabeis,
        string $contacorrente,
        Empenhodetalhado $empenhodetalhado
    ) {
        $limite = $ano - 2;
        for ($ano; $ano >= $limite; $ano--) {
            $this->processarAno($saldocontabilSta, $ano, $codigo_unidade, $gestao_unidade, $contas_contabeis, $contacorrente, $empenhodetalhado);
        }
    }

    private function processarAno(
        ConsultaApiSta $saldocontabilSta,
        string $ano,
        string $codigo_unidade,
        string $gestao_unidade,
        array $contas_contabeis,
        string $contacorrente,
        Empenhodetalhado $empenhodetalhado
    ) {
        $retorno = $saldocontabilSta->saldocontabilAnoUgGestaoVariasContacontabeisContacorrente(
            $ano,
            $codigo_unidade,
            $gestao_unidade,
            array_values($contas_contabeis),
            $contacorrente
        );

        if (count($retorno) > 0) {
            $arraycontas = array_flip($contas_contabeis);
            $dados = [];
            foreach ($retorno as $key => $value) {
                $dados[$arraycontas[$key]] = $value;
            }
            $empenhodetalhado->fill($dados);
            $empenhodetalhado->push();
        }
    }
    public function executaAtualizaSaldosEmpenhos()
    {
        # Se foi iniciado por tela, verifica se ainda há processos em andamento
        if (backpack_user()) {

            $quantidadeJobsEmpenho = Jobs::where('queue', 'migracaoempenho')->count();
            $quantidadeJobsSaldo = Jobs::where('queue', 'atualizasaldone')->count();

            $haProcessosEmAndamento = false;
            $msg = 'Há processos de ';
            $conjuncao = ' ';


            if ($quantidadeJobsEmpenho) {
                $msg .= 'migração de empenho ';
                $conjuncao = 'e ';
                $haProcessosEmAndamento = true;
            }

            if ($quantidadeJobsSaldo) {
                $msg .= $conjuncao . 'atualização de saldos ';
                $haProcessosEmAndamento = true;
            }

            $msg .= 'em andamento. Por favor, aguarde.';

            if ($haProcessosEmAndamento) {
                Alert::warning($msg)->flash();
                return redirect('/execfin/empenho');
            }

        }

        $idDoProcesso = $id = Str::uuid();
        $prefix = __METHOD__ . ' | ' . $idDoProcesso . ' | ';
        Log::channel('atualizasaldone')
            ->info($prefix . 'Início da rotina de atualização de saldos: ' . date('d/m/Y H:i:s'));

        $base = new AdminController();
        $ano = $base->retornaDataMaisOuMenosQtdTipoFormato('Y', '-', '5', 'Days', date('Y-m-d'));

        $empenhos = Empenho::where(DB::raw('left(numero,4)'), $ano)
            ->orderBy('numero')
            ->get();

        $amb = 'PROD';
        $meses = array('', 'JAN', 'FEV', 'MAR', 'ABR', 'MAI', 'JUN', 'JUL', 'AGO', 'SET', 'OUT', 'NOV', 'DEZ');
        $mes = $meses[(int)date('m')];//$meses[(int) $registro['mes']];

        foreach ($empenhos as $empenho) {
            $contas_contabeis = [];
            $anoEmpenho = substr($empenho->numero, 0, 4);

            if ($anoEmpenho == $ano) {
                $contas_contabeis = config('app.contas_contabeis_empenhodetalhado_exercicioatual');
            } else {
                $contas_contabeis = config('app.contas_contabeis_empenhodetalhado_exercicioanterior');
            }

            $ug = $empenho->unidade->codigo;

            $empenhodetalhes = Empenhodetalhado::where('empenho_id', '=', $empenho->id)
                ->get();

            /*
             # Retirado pois pra cada item, vai gerar 1 log
             if ($empenhodetalhes->count()) {
                Log::channel('atualizasaldone')
                    ->info($prefix . 'Agendada(s) ' . $empenhodetalhes->count() .
                        ' ocorrência(s) do job AtualizasaldosmpenhosJobs.');
            }*/

            foreach ($empenhodetalhes as $empenhodetalhe) {
                $contacorrente = $empenho->numero . str_pad(
                        $empenhodetalhe->naturezasubitem->codigo,
                        2,
                        '0',
                        STR_PAD_LEFT
                    );

                AtualizasaldosmpenhosJobs::dispatch(
                    $ug,
                    $amb,
                    $ano,
                    $contacorrente,
                    $mes,
                    $empenhodetalhe,
                    $contas_contabeis
                )->onQueue('atualizasaldone');
            }
        }

        if (backpack_user()) {
            Alert::success('Atualização de Saldos em andamento.')->flash();
            return redirect('/execfin/empenho');
        } else {
            return true;
        }
    }

    public function executaAtualizaSaldosRPs()
    {
        $base = new AdminController();
        $ano = $base->retornaDataMaisOuMenosQtdTipoFormato('Y', '-', '5', 'Days', date('Y-m-d'));

        $empenhos = Empenho::where('rp', true)
            ->orderBy('numero')
            ->get();

        $amb = 'PROD';
        $meses = array('', 'JAN', 'FEV', 'MAR', 'ABR', 'MAI', 'JUN', 'JUL', 'AGO', 'SET', 'OUT', 'NOV', 'DEZ');
        $mes = $meses[(int)date('m')];//$meses[(int) $registro['mes']];

        foreach ($empenhos as $empenho) {
            $contas_contabeis = [];
            $anoEmpenho = substr($empenho->numero, 0, 4);

            if ($anoEmpenho == $ano) {
                $contas_contabeis = config('app.contas_contabeis_empenhodetalhado_exercicioatual');
            } else {
                $contas_contabeis = config('app.contas_contabeis_empenhodetalhado_exercicioanterior');
            }

            $ug = $empenho->unidade->codigo;

            $empenhodetalhes = Empenhodetalhado::where('empenho_id', '=', $empenho->id)
                ->get();

            foreach ($empenhodetalhes as $empenhodetalhe) {
                $contacorrente = $empenho->numero . str_pad(
                        $empenhodetalhe->naturezasubitem->codigo,
                        2,
                        '0',
                        STR_PAD_LEFT
                    );

                AtualizasaldosmpenhosJobs::dispatch(
                    $ug,
                    $amb,
                    $ano,
                    $contacorrente,
                    $mes,
                    $empenhodetalhe,
                    $contas_contabeis
                )->onQueue('atualizasaldone');
            }
        }

        if (backpack_user()) {
            Alert::success('Atualização de Empenhos em Andamento!')->flash();
            return redirect('/execfin/empenho');
        } else {
            return true;
        }
    }

    public function atualizaSaldosEmpenho($codigoUnidadeEmitente, $numeroEmpenho)
    {
        $base = new AdminController();
        $ano = $base->retornaDataMaisOuMenosQtdTipoFormato('Y', '-', '5', 'Days', date('Y-m-d'));

        $unidade = Unidade::where('codigo', $codigoUnidadeEmitente)->first();

        $empenho = Empenho::where('numero', $numeroEmpenho)
            ->where('unidade_id', $unidade->id)
            ->first();

        if ($empenho) {
            $amb = config('app.ambiente_siafi');
            $meses = array('', 'JAN', 'FEV', 'MAR', 'ABR', 'MAI', 'JUN', 'JUL', 'AGO', 'SET', 'OUT', 'NOV', 'DEZ');
            $mes = $meses[(int)date('m')];//$meses[(int) $registro['mes']];

            $contas_contabeis = [];
            $anoEmpenho = substr($empenho->numero, 0, 4);

            if ($anoEmpenho == $ano) {
                $contas_contabeis = config('app.contas_contabeis_empenhodetalhado_exercicioatual');
            } else {
                $contas_contabeis = config('app.contas_contabeis_empenhodetalhado_exercicioanterior');
            }

            $ug = $empenho->unidade->codigo;

            $empenhodetalhes = Empenhodetalhado::where('empenho_id', '=', $empenho->id)
                ->get();

            foreach ($empenhodetalhes as $empenhodetalhe) {
                $contacorrente = 'N' . $empenho->numero . str_pad(
                        $empenhodetalhe->naturezasubitem->codigo,
                        2,
                        '0',
                        STR_PAD_LEFT
                    );

                $this->atualizarSaldoEmpenhoDetalhado(
                    $ug,
                    $amb,
                    $ano,
                    $contacorrente,
                    $mes,
                    $empenhodetalhe,
                    $contas_contabeis
                );
            }
        }

        // Issue #233
        /*if (backpack_user()) {
            Alert::success('Atualização de Empenhos em Andamento!')->flash();
            return redirect('/execfin/empenho');
        }*/
    }

    public function atualizarSaldoEmpenhoDetalhado(
        $ug,
        $amb,
        $ano,
        $contacorrente,
        $mes,
        $empenhodetalhado,
        $contas_contabeis
    )
    {

        $dado = [];
        foreach ($contas_contabeis as $item => $valor) {
            $contacontabil1 = $valor;
            $saldoAtual = 0;

            $retorno = null;

            $execSiafi = new Execsiafi();
            $user = config('app.usuario_siafi');
            $senha = config('app.senha_siafi');

            $retorno = $execSiafi->conrazaoUserSystem($user, $senha, $amb, $ano, $ug, $contacontabil1, $contacorrente, $mes);
            if (isset($retorno->resultado[4])) {
                $sld = get_object_vars($retorno->resultado[4]);
                $saldoAtual = (isset($sld[0])) ? $sld[0] : 0;
                $dado[$item] = $saldoAtual;
            } else {
                $dado[$item] = $saldoAtual;
            }
        }

        $empenhodetalhado->fill($dado);
        $empenhodetalhado->push();
    }

    public function migracaoRp($ug_id)
    {
        $unidade = Unidade::find($ug_id);
        $rp_antigos = $this->atualizaEmpenhosRpsAntigos($ug_id);

        $ano = date('Y');

        $migracao_url = config('migracao.api_sta');
        $url = $migracao_url . $this->replaceRouteParam(config('rotas-sta.rp-por-ug'), ['ug' => $unidade->codigo]);

        $urlFetchService = new STAUrlFetcherService($url);
        $dados = $urlFetchService->fetchDataJson();

        if ($urlFetchService->isError($dados)) {

            $error = $urlFetchService->getError($dados);

            Log::info(__METHOD__ . ' Linha ' . __LINE__ . ' - Erro ao buscar o empenho no STA ' .
                '[' . $error->error_code . '] ' . $error->error_message);
            return false;
        }

        foreach ($dados as $d) {
            $credor = $this->buscaFornecedor($d);

            if ($d['picodigo'] != "") {
                $pi = $this->buscaPi($d);
            }

            if (isset($pi->id)) {
                $pi_id = $pi->id;
            } else {
                $pi_id = null;
            }

            $naturezadespesa = Naturezadespesa::where('codigo', $d['naturezadespesa'])
                ->first();

            $empenho = Empenho::where('numero', '=', trim($d['numero']))
                ->where('unidade_id', '=', $unidade->id)
                ->withTrashed()
                ->first();

            if (!isset($empenho->id)) {
                $empenho = Empenho::create([
                    'numero' => trim($d['numero']),
                    'unidade_id' => $unidade->id,
                    'fornecedor_id' => $credor->id,
                    'planointerno_id' => $pi_id,
                    'naturezadespesa_id' => $naturezadespesa->id,
                    'fonte' => trim($d['fonte']),
                    'rp' => 1
                ]);
            } else {
                $empenho->fornecedor_id = $credor->id;
                $empenho->planointerno_id = $pi_id;
                $empenho->naturezadespesa_id = $naturezadespesa->id;
                $empenho->fonte = trim($d['fonte']);
                $empenho->deleted_at = null;
                $empenho->rp = 1;
                $empenho->save();
            }

            foreach ($d['itens'] as $item) {
                $naturezasubitem = Naturezasubitem::where('codigo', $item['subitem'])
                    ->where('naturezadespesa_id', $naturezadespesa->id)
                    ->first();

                $empenhodetalhado = Empenhodetalhado::where('empenho_id', '=', $empenho->id)
                    ->where('naturezasubitem_id', '=', $naturezasubitem->id)
                    ->first();

                if (!isset($empenhodetalhado)) {
                    $empenhodetalhado = Empenhodetalhado::create([
                        'empenho_id' => $empenho->id,
                        'naturezasubitem_id' => $naturezasubitem->id
                    ]);
                }
            }
        }
    }

    public function atualizaEmpenhosRpsAntigos($unidade_id)
    {
        $empenhos = Empenho::where('unidade_id', $unidade_id)
            ->update(['rp' => false]);

        return $empenhos;
    }

    public function migracaoEmpenho($ug_id, $ano_request = null)
    {
        $unidade = Unidade::find($ug_id);

        $ano = date('Y');

        if ($ano_request) {
            $ano = $ano_request;
        }

        $param = [
            'ano' => $ano,
            'ug' => $unidade->codigo
        ];

        $migracao_url = config('migracao.api_sta');
        $url = $migracao_url . $this->replaceRouteParam(config('rotas-sta.empenho-por-ano-ug'), $param);

        $urlFetchService = new STAUrlFetcherService($url);
        $dados = $urlFetchService->fetchDataJson();

        if ($urlFetchService->isError($dados)) {
            $error = $urlFetchService->getError($dados);

            Log::info(__METHOD__ . ' Linha ' . __LINE__ . ' - Erro ao buscar o empenho no STA ' .
                '[' . $error->error_code . '] ' . $error->error_message);
            return false;
        }

        $pkcount = is_array($dados) ? count($dados) : 0;
        if ($pkcount > 0) {
            foreach ($dados as $d) {
                $credor = $this->buscaFornecedor($d);

                if ($d['picodigo']) {
                    $pi = $this->buscaPi($d);
                }

                $naturezadespesa = $this->trataPiNdSubitem($d['naturezadespesa'], 'ND', null, $d['naturezadespesadescricao']);

                if ($naturezadespesa) {
                    $empenho = Empenho::updateOrCreate(
                        [
                            'numero' => trim($d['numero']),
                            'unidade_id' => $unidade->id
                        ],
                        [
                            'fornecedor_id' => $credor->id,
                            'planointerno_id' => $pi->id,
                            'naturezadespesa_id' => $naturezadespesa,
                            'fonte' => trim($d['fonte']),
                        ]
                    );

                    foreach ($d['itens'] as $item) {
                        $naturezasubitem = $this->trataPiNdSubitem($item['subitem'], 'SUBITEM', $naturezadespesa, $item['subitemdescricao']);

                        if ($naturezasubitem) {
                            $empenhodetalhado = Empenhodetalhado::updateOrCreate([
                                'empenho_id' => $empenho->id,
                                'naturezasubitem_id' => $naturezasubitem
                            ]);
                        }
                    }
                }
            }
        }
    }

    private function buscaNd($codigo, $descricao)
    {
        $planointerno = Naturezadespesa::updateOrCreate(
            [
                'codigo' => $codigo
            ],
            [
                'descricao' => strtoupper($descricao),
                'situacao' => true
            ]
        );

        return $planointerno;
    }

    public function executaCargaEmpenhos()
    {
        $unidades = Unidade::whereHas('contratos', function ($c) {
            $c->where('situacao', true);
        })
            ->where('situacao', true)
            ->where('utiliza_siafi', true)
            ->get();

        $base = new AdminController();
        $ano_antigo = $base->retornaDataMaisOuMenosQtdTipoFormato('Y', '-', '10', 'years', date('Y-m-d'));
        $ano_corrente = date('Y');

        foreach ($unidades as $unidade) {
            for ($i = $ano_antigo; $i <= $ano_corrente; $i++) {
                MigracaoCargaEmpenhoJob::dispatch($unidade->id, $i)->onQueue('migracaoempenho');
            }
        }

        if (backpack_user()) {
            Alert::success('Migração de Empenhos em Andamento!')->flash();
            return redirect('/execfin/empenho');
        }
    }

    public function buscaFornecedor($credor)
    {
        $tipo = 'JURIDICA';
        if (strlen($credor['cpfcnpjugidgener']) == 14) {
            $tipo = 'FISICA';
        } elseif (strlen($credor['cpfcnpjugidgener']) == 9) {
            $tipo = 'IDGENERICO';
        } elseif (strlen($credor['cpfcnpjugidgener']) == 6) {
            $tipo = 'UG';
        }

        $fornecedor = Fornecedor::updateOrCreate(
            [
                'cpf_cnpj_idgener' => $credor['cpfcnpjugidgener']
            ],
            [
                'tipo_fornecedor' => $tipo,
                'nome' => strtoupper(trim($credor['nome']))
            ]
        );

        return $fornecedor;
    }

    public function buscaPi($pi)
    {
        $planointerno = Planointerno::updateOrCreate(
            [
                'codigo' => $pi['picodigo']
            ],
            [
                'descricao' => strtoupper($pi['pidescricao']),
                'situacao' => true
            ]
        );

        return $planointerno;
    }

    public function buscaDadosUrl($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_TIMEOUT, 90);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 90);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        $data = curl_exec($ch);

        curl_close($ch);

        return json_decode($data, true);
    }

    public function incluirEmpenhoSiafi()
    {
        $retorno = null;
        $empenhos = SfOrcEmpenhoDados::where('situacao', 'EM PROCESSAMENTO')
            ->get();

        if ($empenhos) {
            foreach ($empenhos as $empenho) {
                $user = BackpackUser::where('cpf', $empenho->cpf_user)
                    ->first();
                $ws_siafi = new Execsiafi;
                $ano = config('app.ano_minuta_empenho');
                $retorno = $ws_siafi->incluirNe($user, $empenho->ugemitente, env('AMBIENTE_SIAFI'), $ano, $empenho);
                $empenho->update($retorno);

                if ($retorno['situacao'] == 'EMITIDO') {
                    $empenho = $this->criaEmpenhoFromMinuta($empenho);

                    DevolveMinutaSiasg::create([
                        'minutaempenho_id' => $empenho->minutaempenho_id,
                        'situacao' => 'Pendente'
                    ]);
                }
            }
        }
    }

    public function criaEmpenhoFromMinuta(SfOrcEmpenhoDados $empenho)
    {
        $array_empenho1 = [
            'numero' => trim($empenho->mensagemretorno),
            'unidade_id' => $empenho->minuta_empenhos->saldo_contabil->unidade_id,
        ];

        $array_empenho2 = [
            'fornecedor_id' => $empenho->minuta_empenhos->fornecedor_empenho_id,
            'planointerno_id' => $this->trataPiNdSubitem(substr($empenho->minuta_empenhos->saldo_contabil->conta_corrente, 31, 11), 'PI'),
            'naturezadespesa_id' => $this->trataPiNdSubitem(substr($empenho->minuta_empenhos->saldo_contabil->conta_corrente, 17, 6), 'ND'),
            'fonte' => substr($empenho->minuta_empenhos->saldo_contabil->conta_corrente, 7, 10),
            'informacao_complementar' => @$empenho->txtinfocompl,
            'sistema_origem' => 'COMPRASNET CONTRATOS',
            'data_emissao' => $empenho->dtemis,
            'ano_emissao' => (int)substr(trim($empenho->mensagemretorno), 0, 4)
        ];

        $novo_empenho = Empenho::firstOrCreate(
            $array_empenho1,
            $array_empenho2
        );

        $itens = SfItemEmpenho::where('sforcempenhodado_id', $empenho->id)
            ->get();

        $array_empenhodetalhado = [];
        foreach ($itens as $item) {
            $array_empenhodetalhado = [
                'empenho_id' => $novo_empenho->id,
                'naturezasubitem_id' => $this->trataPiNdSubitem($item->codsubelemento, 'SUBITEM', $array_empenho2['naturezadespesa_id']),
                'empaliquidar' => $empenho->vlrempenho
            ];

            $novo_empenhodetalhado = Empenhodetalhado::firstOrCreate($array_empenhodetalhado);
        }
        return $novo_empenho;
    }

    public function trataPiNdSubitem($dado, $tipo, $fk = null, $descricao = null)
    {
        if ($dado == '') {
            return null;
        }

        if ($tipo == 'PI') {
            $pi = Planointerno::firstOrCreate(
                ['codigo' => trim($dado)],
                [
                    'descricao' => ($descricao) ? $descricao : 'ATUALIZAR PI',
                    'situacao' => true
                ]
            );

            return $pi->id;
        }
        if ($tipo == 'ND') {
            $nd = Naturezadespesa::firstOrCreate(
                ['codigo' => trim($dado)],
                [
                    'descricao' => ($descricao) ? $descricao : 'ATUALIZAR ND',
                    'situacao' => true
                ]
            );

            return $nd->id;
        }
        if ($tipo == 'SUBITEM') {
            $ndsubitem = Naturezasubitem::firstOrCreate(
                [
                    'naturezadespesa_id' => $fk,
                    'codigo' => trim($dado),
                ],
                [
                    'descricao' => ($descricao) ? $descricao : 'ATUALIZAR SUBITEM',
                    'situacao' => true
                ]
            );

            return $ndsubitem->id;
        }
    }

    public function enviaEmpenhoSiasg()
    {
        $minutas = MinutaEmpenho::join(
            'minutaempenhos_remessa',
            'minutaempenhos_remessa.minutaempenho_id',
            '=',
            'minutaempenhos.id'
        )
            ->join(
                'devolve_minuta_siasg',
                'devolve_minuta_siasg.minutaempenhos_remessa_id',
                '=',
                'minutaempenhos_remessa.id'
            )
            ->join('unidades', 'unidades.id', '=', 'minutaempenhos.unidade_id')
            ->join('compras', 'compras.id', '=', 'minutaempenhos.compra_id')
            ->join('codigoitens', 'codigoitens.id', '=', 'compras.modalidade_id')
            ->join('fornecedores', 'fornecedores.id', '=', 'minutaempenhos.fornecedor_empenho_id')
            ->join(
                'saldo_contabil',
                'saldo_contabil.id',
                '=',
                'minutaempenhos.saldo_contabil_id'
            )
            ->join(
                DB::raw('unidades as unidade_saldo_contabil'),
                'unidade_saldo_contabil.id',
                '=',
                'saldo_contabil.unidade_id'
            )
            ->join(
                DB::raw('codigoitens as tipo_compra'),
                'tipo_compra.id',
                '=',
                'compras.tipo_compra_id'
            )
            ->join(
                DB::raw('codigoitens as tipo_empenho'),
                'tipo_empenho.id',
                '=',
                'minutaempenhos.tipo_empenho_id'
            )
            ->join(
                DB::raw('codigoitens as tipo_empenhopor'),
                'tipo_empenhopor.id',
                '=',
                'minutaempenhos.tipo_empenhopor_id'
            )
            ->leftJoin(
                'contratos',
                'contratos.id',
                '=',
                'minutaempenhos.contrato_id'
            )
            ->leftJoin(
                DB::raw('unidades as unidade_contrato'),
                'unidade_contrato.id',
                '=',
                'contratos.unidadeorigem_id'
            )
            ->where('devolve_minuta_siasg.situacao', 'Pendente')
            ->select(
                'minutaempenhos.id',
                DB::raw('left(minutaempenhos.mensagem_siafi,4) as anoEmpenho'),
                'data_emissao',
                'unidades.codigo',
                'codigoitens.descres as modalidade',
                'compras.numero_ano as num_ano',
                'fornecedores.cpf_cnpj_idgener',
                DB::raw('SUBSTRING(saldo_contabil.conta_corrente,8,10) AS "fonte"'),
                DB::raw('SUBSTRING(saldo_contabil.conta_corrente,18,6) AS "nd"'),
                DB::raw('SUBSTRING(saldo_contabil.conta_corrente,2,6) AS "ptrs"'),
                DB::raw('SUBSTRING(saldo_contabil.conta_corrente,32,11) AS "plano_interno"'),
                DB::raw('SUBSTRING(saldo_contabil.conta_corrente,24,8) AS "ugr"'),
                DB::raw('unidade_saldo_contabil.codigo AS "ugEmitente"'),
                'tipo_compra.descres as tp_compra',
                'tipo_empenho.descres as tipo_empenho',
                'minutaempenhos.unidade_id',
                'minutaempenhos.mensagem_siafi',
                'minutaempenhos.valor_total',
                'minutaempenhos.tipo_empenhopor_id',
                'minutaempenhos_remessa.id as minutaempenhos_remessa_id',
                DB::raw('devolve_minuta_siasg.id as devolve_id'),
                DB::raw("CASE
                                   WHEN tipo_empenhopor.descricao = 'Compra' THEN unidades.codigo
                                   WHEN tipo_empenhopor.descricao = 'Contrato' THEN unidade_contrato.codigo
                                   END AS \"uasgUsuario\""),
                DB::raw("
                    CASE
           WHEN tipo_empenhopor.descricao = 'Compra' THEN ''
           WHEN tipo_empenhopor.descricao = 'Contrato' THEN
                   unidade_contrato.codigo || '50' || REPLACE(numero, '/', '') ||
                   CASE
                       WHEN contratos.unidade_id <> contratos.unidadeorigem_id
                           THEN unidades.codigo
                       ELSE '000000'
                       END
           END                                                                          AS \"chaveContratoContinuado\"
                ")
            )
//            ->where('minutaempenhos.tipo_empenhopor_id', 256)//todo tirar esta linha
//            ->where('minutaempenhos.id', 154579)//todo tirar esta linha compra
//            ->where('minutaempenhos.id', 30208)//todo tirar esta linha contrato
//            ->where('minutaempenhos.id', 167750)//todo tirar esta linha contrato
//            ->where('minutaempenhos.id', 171620)//todo tirar esta linha compra sispp
//            ->where('minutaempenhos.id', 42751)//todo tirar esta linha compra sispp QTD QUEBRADA
//            ->where('minutaempenhos.id', 117992)//todo tirar esta linha compra sispp operacao invalido
//            ->where('minutaempenhos.id', 171620)//todo tirar esta linha compra sispp subitem invalido

            //->where('minutaempenhos.unidade_id', 1)//todo tirar esta linha
//                ->whereNull('mensagem_siasg')
            ->where('devolve_minuta_siasg.situacao', 'Pendente')
            //->where('devolve_minuta_siasg.id', 13714)//todo retirar esta linha
            ->orderBy('devolve_minuta_siasg.created_at', 'asc')
//            ->take(2)
            ->get();

        foreach ($minutas as $index => $minuta) {
            $itens = $minuta->getItens($minuta->minutaempenhos_remessa_id)->toArray();

            if (!empty($itens)) {
                $tipoUASG = ($itens[0]['tipoUASG'] ?? 'G');

                $valorTotalEmpenho = ($itens[0]['valorTotalEmpenho'] < 0)
                    ? $itens[0]['valorTotalEmpenho'] * -1
                    : $itens[0]['valorTotalEmpenho'];
                $valorTotalEmpenho = number_format($valorTotalEmpenho, 2, ',', '');
                //$valorTotalEmpenho = number_format($valorTotalEmpenho, 2, ',', '.');

                $itens = array_map(
                    function ($itens) {
                        unset($itens['tipoUASG'], $itens['valorTotalEmpenho']);

                        // SOMENTE QUANTIDADE POSITIVA
                        $itens['quantidadeEmpenhada'] = ($itens['quantidadeEmpenhada'] < 0)
                            ? $itens['quantidadeEmpenhada'] * -1
                            : $itens['quantidadeEmpenhada'];

                        $itens['numeroItemEmpenho'] = str_pad($itens['numeroItemEmpenho'], 3, "0", STR_PAD_LEFT);
                        $itens['quantidadeEmpenhada'] = number_format($itens['numeroItemEmpenho'], 5, ',', '');
                        $itens['valorUnitarioItem'] = number_format($itens['valorUnitarioItem'], 4, ',', '');

                        return $itens;
                    },
                    $itens
                );

                $array = [
                    "anoEmpenho" => $minuta->anoempenho,
                    "chaveCompra" => $minuta->codigo . $minuta->modalidade . str_replace('/', '', $minuta->num_ano),
                    "chaveContratoContinuado" => $minuta->chaveContratoContinuado,//ugorigem/ugid
                    "dataEmissao" => str_replace('-', '', $minuta->data_emissao),
                    "favorecido" => $this->retornaSomenteNumeros($minuta->cpf_cnpj_idgener),  //ver detalhe do estrangeiro
                    "fonte" => $minuta->fonte,
                    "itemEmpenho" => $itens,
                    "nd" => $minuta->nd,
                    "numeroEmpenho" => substr($minuta->mensagem_siafi, 6, strlen($minuta->mensagem_siafi)),
                    "planoInterno" => $minuta->plano_interno,
                    "ptres" => $minuta->ptrs,
                    "tipoCompra" => (string)(int)$minuta->tp_compra,
                    "tipoEmpenho" => $minuta->tipo_empenho,
                    "tipoUASG" => $tipoUASG,
                    "uasgUsuario" => $minuta->uasgUsuario,//contrato -ug origem ////// compra uasg minuta
                    "ugEmitente" => $minuta->ugEmitente, //ug do saldo contabil
                    "ugr" => trim($minuta->ugr),
                    "valorTotalEmpenho" => (string)$valorTotalEmpenho
                ];
                EnviaEmpenhoSiasgJob::dispatch($array, $minuta->devolve_id)->onQueue('enviarempenhosiasg');
            }
        }
    }

    public function montaContextJsonGetPeerFalse()
    {
        $context_options = array(
//            'https' => array(
//                'method' => 'GET',
//                'header' => "Content-type: application/json"
//            ),
            'ssl' => array(
                'verify_peer' => false,
                "verify_peer_name" => false,
            )
        );

        return stream_context_create($context_options);
    }

    public function array_slice_assoc($array, $keys): array
    {
        return array_intersect_key($array, array_flip($keys));
    }

    public function array_pop_n(array $arr, $n)
    {
        array_splice($arr, 0, -$n);
        return $arr;
    }

//    public function executaAlteracaoEmpenho()
//    {
//        $id = 94;
//        $sforcempenhodados = SfOrcEmpenhoDados::find($id);
//
//        $user = BackpackUser::where('cpf', $sforcempenhodados->cpf_user)
//            ->first();
//        $ws_siafi = new Execsiafi;
//        $ano = config('app.ano_minuta_empenho');
//
//        $retorno = $ws_siafi->alterarNe($user, $sforcempenhodados->ugemitente, config('app.ambiente_siafi'), $ano, $sforcempenhodados);
//
//        $sforcempenhodados->update($retorno);
//    }
}
