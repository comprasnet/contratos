<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redis;

class EstaleiroCEP
{

    private $cpfUsuario;
    private $cepCidade;

    public function __construct($cpfUsuario, $cepCidade)
    {
        $this->cpfUsuario = $cpfUsuario;
        $this->cepCidade = $cepCidade;
    }

    public function getClient()
    {
        return new Client([
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => $this->tokenOauth2(),
                'x-cpf-usuario' => $this->cpfUsuario
            ],
            'base_uri' => env('API_CEP_ESTALEIRO_HOST')
        ]);
    }

    public function consultaCep()
    {
        return $this->getClient()->request("GET", "/api-cep/v1/consulta/cep/$this->cepCidade");
    }

    public function tokenOauth2()
    {
        $tokenJwt = Redis::get('tokenEstaleiroJWT');
        $dadosTokenJWT = json_decode($tokenJwt, true);

        if ($dadosTokenJWT && $dadosTokenJWT['expiracao'] >= now()->timestamp) {
            return $dadosTokenJWT['token'];
        }

        $client = new Client();
        $resposta = $client->request('POST', env('API_CEP_ESTALEIRO_HOST') . "/oauth2/jwt-token", [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Basic ' . base64_encode(env('API_CEP_ESTALEIRO_USUARIO') . ':' . env('API_CEP_ESTALEIRO_SENHA'))
            ],
            'json' => [
                'grant_type' => 'client_credentials',
            ]
        ]);
        $resultado = json_decode($resposta->getBody());
        $token = $resultado->token_type . ' ' . $resultado->access_token;

        $expirationTime = now()->addHours(1)->addMinutes(30)->setTimezone('America/Sao_Paulo');
        Redis::set('tokenEstaleiroJWT', json_encode(['token' => $token, 'expiracao' => $expirationTime->timestamp]));

        return $token;
    }
}
