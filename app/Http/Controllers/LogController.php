<?php

namespace App\Http\Controllers;

// use Backpack\LogManager\app\Classes\LogViewer;
use App\Models\LogViewer;  #722

use Illuminate\Routing\Controller;

use Illuminate\Pagination\LengthAwarePaginator;

class LogController extends Controller
{
    /**
     * Lists all log files.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $this->data['files'] = LogViewer::getFiles(true, true);
        $this->data['title'] = trans('backpack::logmanager.log_manager');
        return view('backpack::logs', $this->data);     #722
    }

    /**
     * Previews a log file.
     *
     * @throws \Exception
     */
    public function preview($file_name)
    {
        LogViewer::setFile(decrypt($file_name));

        $logs = LogViewer::all();

        if (count($logs) <= 0) {
            abort(404, trans('backpack::logmanager.log_file_doesnt_exist'));
        }

        // #722 Paginação dos logs
        $perPage = 30;
        $currentPage = request()->get('page', 1);
        $offset = ($currentPage - 1) * $perPage;
        $logsChunk = array_slice($logs, $offset, $perPage);
        $paginatedLogs = new LengthAwarePaginator($logsChunk, count($logs), $perPage, $currentPage, [
            'path' => url('/log/preview/' . $file_name),
            'pageName' => 'page',
        ]);
        $this->data['logs'] = $paginatedLogs;
        $this->data['title'] = trans('backpack::logmanager.preview').' '.trans('backpack::logmanager.logs');
        $this->data['file_name'] = decrypt($file_name);

        return view('backpack::log_item', $this->data);
    }

    /**
     * Downloads a log file.
     *
     * @param $file_name
     *
     * @throws \Exception
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function download($file_name)
    {
        return response()->download(LogViewer::pathToLogFile(decrypt($file_name)));
    }

    /**
     * Deletes a log file.
     *
     * @param $file_name
     *
     * @throws \Exception
     *
     * @return string
     */
    public function delete($file_name)
    {
        if (app('files')->delete(LogViewer::pathToLogFile(decrypt($file_name)))) {
            return 'success';
        }
        abort(404, trans('backpack::logmanager.log_file_doesnt_exist'));
    }

    /**
     * O objetivo deste método é criar arquivos de log que deveriam ser gerados por rotinas de migração e Jobs.
     * Esta ação foi necessária pelo fato de os Jobs não estarem conseguindo criar e/ou gravar nesses arquivos
     * devido a problemas de permissão nas estruturas dos servidores.
     *
     * @return void
     */
    public function criarArquivosDeLog()
    {
        # Define para quais channels será criado o arquivo de log
        $allowedChannels = ['atualizanaturezadespesa', 'migracaoempenho', 'atualizasaldone'];

        # Busca os channels definidos em logging.php
        $logginChannel = config('logging.channels');

        # Define a data a concatenar no nome do arquivo
        $date = date('Y-m-d');

        # Define a string padrão a concatenar no nome do arquivo
        $fileNamePattern = ".worker-{$date}.log";

        # Varre os canais e verifica quais estão na definição
        foreach ($logginChannel as $channelName => $channelConfig) {
            if (in_array($channelName, $allowedChannels)) {

                $fileName = "{$channelName}{$fileNamePattern}";

                # Caminho completo para o arquivo de log
                $fullPath = storage_path("logs/{$fileName}");

                # Cria o arquivo de log se não existir
                if (!file_exists($fullPath)) {
                    file_put_contents($fullPath, '');
                }

                # Define permissão total no arquivo
                chmod($fullPath, 0777);

            }

        }
    }

}
