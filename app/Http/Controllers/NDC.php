<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\Log;

class NDC
{

    private static $client;
    private static $access_token;
    private static $url;
    private static $login;
    private static $password;
    private static $env;
    private static $scope;
    private static $token_type;
    private static $expires_in;

    public static function consulta($endpoint, $params)
    {
        try {
            self::login();

            $url = self::getUrl($endpoint, $params);
            $response = self::$client->request('GET', $url, [
                RequestOptions::HEADERS => [
                    'Authorization' => "Bearer " . self::$access_token,
                ]
            ]);

            return json_decode($response->getBody()->getContents(), false);
        } catch (ClientException|ServerException $e) {
            $response = $e->getResponse();
            return json_decode($response->getBody()->getContents(), false);
        } catch (ConnectException $e) {
            Log::error($e);
            return (object)[
                'data' => null,
                'messagem' => 'Compra não encontrada.',
                'codigoRetorno' => 204
            ];
        }
    }

    public static function login(): void
    {
        self::$url      = env('V2_HOST') ;
        self::$login    = env('API_NOVO_DIVULGACAO_LOGIN');
        self::$password = env('API_NOVO_DIVULGACAO_PASSWORD');

        self::$client = new Client([
            'verify' => false,
        ]);

        self::token();
    }

    public static function token()
    {
        if (self::$access_token == null) {
            $credentials = "login?password=" . self::$password . "&login=" . self::$login;

            $response = self::$client->request('POST', self::$url . $credentials);

            $response = json_decode($response->getBody()->getContents());

            self::$access_token = $response->access_token;
            self::$token_type = $response->token_type;
            self::$expires_in = $response->expires_in;

            return $response;
        }

        return true;
    }

    private static function getUrl($endpoint, $params): string
    {
        $queryString = http_build_query($params);
        switch ($endpoint) {
            case 'contratacao':
                $url = self::$url . "novodivulgacao/contratacao?$queryString";
                break;
            case 'itemUrl':
                $url = $params['itemUrl'];
                break;
            case 'contratacao_pncp/compra':
                $url = self::$url . "contratacao_pncp/compra?$queryString";
                break;
        }
        return $url;
    }
}
