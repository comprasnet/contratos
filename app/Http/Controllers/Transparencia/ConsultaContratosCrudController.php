<?php

namespace App\Http\Controllers\Transparencia;

use App\Models\AmparoLegal;
use App\Models\Codigoitem;
use App\services\AmparoLegalService;
use Backpack\CRUD\CrudPanel;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ConsultaContratosCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ConsultaContratosCrudController extends ConsultaContratoBaseCrudController
{
    protected $amparoLegalService;

    public function __construct(AmparoLegalService $amparoLegalService)
    {
        parent::__construct();
        $this->amparoLegalService = $amparoLegalService;
    }

    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */

        $this->crud->setModel('App\Models\Contrato');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/transparencia/contratos');
        $this->crud->setEntityNameStrings('Consulta Contrato', 'Consulta Contratos');
        $this->crud->setShowView('vendor.backpack.crud.transparencia.show');

        $this->crud->addClause('select', 'contratos.*');
        $this->crud->addClause('join', 'fornecedores', 'fornecedores.id', '=', 'contratos.fornecedor_id');
        $this->crud->addClause('join', 'unidades', 'unidades.id', '=', 'contratos.unidade_id');
        $this->crud->addClause('join', 'orgaos', 'orgaos.id', '=', 'unidades.orgao_id');
        $this->crud->addClause('where', 'contratos.situacao', '=', true);
        $this->crud->addClause('where', 'contratos.elaboracao', '=', false);
        $this->crud->addClause('where', 'unidades.sigilo', '=', false);

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $this->defineConfiguracaoPadrao();
    }

    public function show($id)
    {
        $content = parent::show($id);

        if(!$this->amparoLegalService->existeAmparoLegal14133PorContrato($id)){
            $this->crud->removeColumns(['acesso_pncp']);
        }

        $this->crud->removeColumns([
            'fornecedor_id',
            'unidade_id',
            'tipo_id',
            'categoria_id',
            'fundamento_legal',
            'modalidade_id',
            'licitacao_numero',
            'data_assinatura',
            'data_publicacao',
            'valor_inicial',
            'valor_global',
            'valor_parcela',
            'valor_acumulado',
            'total_despesas_acessorias',
            'receita_despesa',
            'subcategoria_id',
            'situacao_siasg',
            'situacao',
            'unidadeorigem_id',
            'unidadecompra_id',
            'numero_compra',
            'subtipo',
            'publicado',
            'prorrogavel',
            'unidadebeneficiaria_id',
            'is_prazo_indefinido',
            'data_encerramento',
            'is_objeto_contratual_entregue',
            'is_cumpridas_obrigacoes_financeiras',
            'is_saldo_conta_vinculada_liberado',
            'is_garantia_contratual_devolvida',
            'justificativa_contrato_inativo_id',
            'hora_encerramento',
            'nome_responsavel_encerramento',
            'cpf_responsavel_encerramento',
            'user_id_responsavel_encerramento',
            'cronograma_automatico',
            'grau_satisfacao_desempenho_contrato',
            'planejamento_contratacao_atendida',
            'sugestao_licao_aprendida',
            'data_proposta_comercial',

        ]);

        $this->crud->removeColumns([
            'codigo_sistema_externo',
            'consecucao_objetivos_contratacao',
            'user_id_responsavel_signatario_encerramento',
            'nome_responsavel_signatario_encerramento',
            'cpf_responsavel_signatario_encerramento',
        ]);

        $this->crud->removeColumn('elaboracao');
        $this->crud->removeColumn('te_valor_total_executado');
        $this->crud->removeColumn('te_valor_total_pago');
        $this->crud->removeColumn('te_saldo_disponivel_ou_bloqueado');
        $this->crud->removeColumn('te_justificativa_nao_cumprimento');
        $this->crud->removeColumn('te_saldo_bloqueado_garantia_contratual');
        $this->crud->removeColumn('te_justificativa_bloqueio_garantia_contratual');
        $this->crud->removeColumn('te_saldo_bloqueado_conta_deposito');
        $this->crud->removeColumn('te_justificativa_bloqueio_conta_deposito');
        
        return $content;
    }

    protected function aplicaFiltrosEspecificos(): void
    {

        $this->crud->addFilter(
            [
            'name' => 'numero',
            'type' => 'text',
            'label' => 'Número Contrato'
            ],
            false,
            function ($value) {
                $this->crud->addClause('where', 'contratos.numero', 'LIKE', "%$value%");
            }
        );

        $this->crud->addFilter([
            'name' => 'receita_despesa',
            'type' => 'select2_multiple',
            'label' => 'Receita / Despesa'
        ], [
            'R' => 'Receita',
            'D' => 'Despesa',
            'S' => 'Sem ônus',
        ], function ($value) {
            $this->crud->addClause(
                'whereIn',
                'contratos.receita_despesa',
                json_decode($value)
            );
        });

        $tipos = Codigoitem::whereHas('codigo', function ($query) {
            $query->where('descricao', '=', 'Tipo de Contrato');
        })
            ->where('descricao', '<>', 'Termo Aditivo')
            ->where('descricao', '<>', 'Termo de Apostilamento')
            ->orderBy('descricao')
            ->pluck('descricao', 'id')
            ->toArray();

        $this->crud->addFilter(
            [
                'name' => 'tipo_contrato',
                'type' => 'select2_multiple',
                'label' => 'Tipo'
            ],
            $tipos,
            function ($value) {
                $this->crud->addClause(
                    'whereIn',
                    'contratos.tipo_id',
                    json_decode($value)
                );
            }
        );

        $categorias = Codigoitem::whereHas('codigo', function ($query) {
            $query->where('descricao', '=', 'Categoria Contrato');
        })->orderBy('descricao')->pluck('descricao', 'id')->toArray();

        $this->crud->addFilter(
            [
                'name' => 'categorias',
                'type' => 'select2_multiple',
                'label' => 'Categorias'
            ],
            $categorias,
            function ($values) {
                $this->crud->addClause(
                    'whereIn',
                    'contratos.categoria_id',
                    json_decode($values)
                );
            }
        );

        $this->crud->addFilter(
            [
                'type' => 'date_range',
                'name' => 'vigencia_inicio',
                'label' => 'Vigência Inicio'
            ],
            false,
            function ($value) {
                // if the filter is active, apply these constraints
                $dates = json_decode($value);
                $this->crud->addClause('where', 'contratos.vigencia_inicio', '>=', $dates->from);
                $this->crud->addClause('where', 'contratos.vigencia_inicio', '<=', $dates->to . ' 23:59:59');
            }
        );

        $this->crud->addFilter(
            [
                'type' => 'date_range',
                'name' => 'vigencia_fim',
                'label' => 'Vigência Fim'
            ],
            false,
            function ($value) {
                // if the filter is active, apply these constraints
                $dates = json_decode($value);
                $this->crud->addClause('where', 'contratos.vigencia_fim', '>=', $dates->from);
                $this->crud->addClause('where', 'contratos.vigencia_fim', '<=', $dates->to . ' 23:59:59');
            }
        );

        $this->crud->addFilter(
            [
                'name' => 'valor_global',
                'type' => 'range',
                'label' => 'Valor Global',
                'label_from' => 'Vlr Mínimo',
                'label_to' => 'Vlr Máximo'
            ],
            false,
            function ($value) {
                // if the filter is active
                $range = json_decode($value);
                if ($range->from) {
                    $this->crud->addClause('where', 'contratos.valor_global', '>=', (float)$range->from);
                }
                if ($range->to) {
                    $this->crud->addClause('where', 'contratos.valor_global', '<=', (float)$range->to);
                }
            }
        );

        $this->crud->addFilter(
            [
                'name' => 'valor_parcela',
                'type' => 'range',
                'label' => 'Valor Parcela',
                'label_from' => 'Vlr Mínimo',
                'label_to' => 'Vlr Máximo'
            ],
            false,
            function ($value) {
                // if the filter is active
                $range = json_decode($value);
                if ($range->from) {
                    $this->crud->addClause('where', 'contratos.valor_parcela', '>=', (float)$range->from);
                }
                if ($range->to) {
                    $this->crud->addClause('where', 'contratos.valor_parcela', '<=', (float)$range->to);
                }
            }
        );
        $this->crud->addFilter(
            [
                'name' => 'aplicavel_decreto',
                'type' => 'select2',
                'label' => 'Decreto 11.430'
            ],
            [
                true => 'Sim',
                false => 'Não',
            ],
            function ($value) {
                $this->crud->addClause(
                    'where',
                    'contratos.aplicavel_decreto',
                    $value
                );
            }
        );
    }

    protected function adicionaColunasEspecificasNaListagem(): void
    {

        $this->crud->addColumns([
            [
                'name' => 'getUnidadeOrigem',
                'label' => 'Unidade Gestora Origem do Contrato',
                'type' => 'model_function',
                'function_name' => 'getUnidadeOrigem',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'getReceitaDespesa',
                'label' => 'Receita / Despesa',
                'type' => 'model_function',
                'function_name' => 'getReceitaDespesa',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'numero',
                'label' => 'Número Contrato',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'acesso_pncp',
                'label' => 'PNCP',
                'type' => 'model_function',
                'function_name' => 'getLinkPNCP',
                'function_parameters' => [true],
                'limit' => 1000,
                'visibleInTable' => false,
                'visibleInModal' => false,
                'visibleInExport' => false,
                'visibleInShow' => true,
            ],
            [
                'name' => 'getUnidadeCompra',
                'label' => 'Unidade Realizadora da Compra',
                'type' => 'model_function',
                'function_name' => 'getUnidadeCompra',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'getNumeroCompra',
                'label' => 'Número da Compra',
                'type' => 'model_function',
                'function_name' => 'getNumeroCompra',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'modalidade_compra',
                'label' => 'Modalidade da Compra',
                'type' => 'model_function',
                'function_name' => 'getModalidade',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'getAmparoLegal',
                'label' => 'Amparo Legal',
                'type' => 'model_function',
                'function_name' => 'getAmparoLegal',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'unidades_requisitantes',
                'label' => 'Unidades Requisitantes',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'getTipo',
                'label' => 'Tipo',
                'type' => 'model_function',
                'function_name' => 'getTipo',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'getCategoria',
                'label' => 'Categoria',
                'type' => 'model_function',
                'function_name' => 'getCategoria',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'getSubCategoria',
                'label' => 'Subcategoria',
                'type' => 'model_function',
                'function_name' => 'getSubCategoria',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'getFornecedor',
                'label' => 'Fornecedor',
                'type' => 'model_function',
                'function_name' => 'getFornecedor',
                'orderable' => true,
                'limit' => 1000,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere('fornecedores.cpf_cnpj_idgener', 'like', "%" . strtoupper($searchTerm) . "%");
                    $query->orWhere('fornecedores.nome', 'like', "%" . strtoupper($searchTerm) . "%");
                }
            ],
            [
                'name' => 'processo',
                'label' => 'Processo',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'objeto',
                'label' => 'Objeto',
                'type' => 'text',
                'limit' => 1000,
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'info_complementar',
                'label' => 'Informações Complementares',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'vigencia_inicio',
                'label' => 'Vig. Início',
                'type' => 'date',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'vigencia_fim',
                'label' => 'Vig. Fim',
                'type' => 'model_function',
                'function_name' => 'getVigenciaFimPrazoIndeterminado',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true
            ],
            [
                'name' => 'formatVlrGlobal',
                'label' => 'Valor Global',
                'type' => 'model_function',
                'function_name' => 'formatVlrGlobal',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'num_parcelas',
                'label' => 'Núm. Parcelas',
                'type' => 'model_function',
                'function_name' => 'getNumeroParcelasPrazoIndeterminado',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true
            ],
            [
                'name' => 'formatVlrParcela',
                'label' => 'Valor Parcela',
                'type' => 'model_function',
                'function_name' => 'formatVlrParcela',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'formatVlrAcumulado',
                'label' => 'Valor Acumulado',
                'type' => 'model_function',
                'function_name' => 'formatVlrAcumulado',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'formatTotalDespesasAcessorias',
                'label' => 'Total Despesas Acessórias',
                'type' => 'model_function',
                'function_name' => 'formatTotalDespesasAcessorias',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'historico',
                'label' => 'Histórico',
                'type' => 'contratohistoricotable',
                'visibleInTable' => false,
                'visibleInModal' => false,
                'visibleInExport' => false,
                'visibleInShow' => true,
            ],
            [
                'name' => 'despesasacessorias',
                'label' => 'Despesas Acessórias',
                'type' => 'table',
                'columns' => [
                    'descricao_complementar' => 'Descrição',
                    'vencimento' => 'Vencimento',
                    'valor' => 'Valor'
                ],
                'visibleInTable' => false,
                'visibleInModal' => false,
                'visibleInExport' => false,
                'visibleInShow' => true,
            ],
            [
                'name' => 'empenho',
                'label' => 'Empenhos',
                'type' => 'empenhotable',
                'visibleInTable' => false,
                'visibleInModal' => false,
                'visibleInExport' => false,
                'visibleInShow' => true,
            ],
            [
                'name' => 'faturas',
                'label' => 'Faturas',
                'type' => 'table',
                'columns' => [
                    'numero' => 'Número',
                    'emissao' => 'Data Emissão',
                    'processo' => 'Processo',
                    'ateste' => 'Data Ateste',
                    'valor' => 'Valor'
                ],
                'visibleInTable' => false,
                'visibleInModal' => false,
                'visibleInExport' => false,
                'visibleInShow' => true,
            ],
            [
                'name' => 'garantias',
                'label' => 'Garantias',
                'type' => 'table',
                'columns' => [
                    'descricao_tipo' => 'Tipo',
                    'vencimento' => 'Vencimento',
                    'valor' => 'Valor'
                ],
                'visibleInTable' => false,
                'visibleInModal' => false,
                'visibleInExport' => false,
                'visibleInShow' => true,
            ],
            [
                'name' => 'itens',
                'label' => 'Itens',
                'type' => 'table',
                'columns' => [
                    'descricao_tipo' => 'Tipo',
                    'descricao_item' => 'Item',
                    'quantidade' => 'Quantidade',
                    'valorunitario' => 'Valor Unitário',
                    'valortotal' => 'Valor Total'
                ],
                'visibleInTable' => false,
                'visibleInModal' => false,
                'visibleInExport' => false,
                'visibleInShow' => true,
            ],
            [
                'name' => 'prepostos',
                'label' => 'Prepostos',
                'type' => 'table',
                'columns' => [
                    'masked_cpf' => 'CPF',
                    'nome' => 'Nome'
                ],
                'visibleInTable' => false,
                'visibleInModal' => false,
                'visibleInExport' => false,
                'visibleInShow' => true,
            ],
            [
                'name' => 'responsaveis',
                'label' => 'Responsáveis',
                'type' => 'table',
                'columns' => [
                    'masked_cpf' => 'CPF',
                    'usuario_nome' => 'Nome',
                    'descricao_tipo' => 'Tipo'
                ],
                'visibleInTable' => false,
                'visibleInModal' => false,
                'visibleInExport' => false,
                'visibleInShow' => true
            ],
            [
                'name' => 'instrumentocobranca',
                'label' => 'Instrumentos de Cobrança',
                'type' => 'model_function',
                'function_name' => "getLinkTransparencia",
                'function_parameters' => ['faturas'],
                'limit' => 1000,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'terceirizados',
                'label' => 'Terceirizados',
                'type' => 'model_function',
                'function_name' => "getLinkTransparencia",
                'function_parameters' => ['terceirizados'],
                'limit' =>  1000,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                    'name' => 'arquivos',
                    'label' => 'Arquivos',
                    'type' => 'arquivos',
                    'disk' => 'local',
                    'visibleInTable' => false,
                    'visibleInModal' => true,
                    'visibleInExport' => true,
                    'visibleInShow' => true,
            ],
            [
                'name' => 'aplicavel_decreto',
                'label' => 'Decreto 11.430',
                'type' => 'boolean',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ]
        ]);
    }
}
