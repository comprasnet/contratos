<?php

namespace App\Http\Controllers\Transparencia;

use App\Forms\FiltroRelatorioContratosForm;
use App\Forms\MeusdadosForm;
use App\Forms\MudarUgForm;
use App\Forms\TransparenciaIndexForm;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\Controller;
use App\Models\BackpackUser;
use App\Models\CalendarEvent;
use App\Models\Codigoitem;
use App\Models\Contrato;
use App\Models\Fornecedor;
use App\Models\Orgao;
use App\Models\Unidade;
use FormBuilder;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;
use MaddHatter\LaravelFullcalendar\Calendar;
use function foo\func;

class IndexController extends Controller
{
    protected $data = []; // the information we send to the view
    public const TIPO_NUMERO_CONTRATOS_TOTAL = 'TOTAL';
    public const TIPO_NUMERO_CONTRATOS_30 = '30';
    public const TIPO_NUMERO_CONTRATOS_3060 = '3060';
    public const TIPO_NUMERO_CONTRATOS_6090 = '6090';
    public const TIPO_NUMERO_CONTRATOS_90180 = '90180';
    public const TIPO_NUMERO_CONTRATOS_180 = '180';

    /**
     * Show the admin dashboard.
     *
     * @return Factory|View
     */
    public function index(Request $request)
    {
        $base = new AdminController();

        $this->data['title'] = "Área Consulta Pública"; // set the page title
        $filtro = [];

        $form = FormBuilder::create(
            TransparenciaIndexForm::class,
            [
                'method' => 'GET',
                'model' => ($request->input()) ? $request->input() : '',
                'url' => route('transparencia.index'),
            ]
        );

        if ($request->query()) {
            $filtro = $request->input();
            $this->data['fields'] = $this->trataDadosView($filtro);
        }

        $dadosCategoriaContratoValorContratado =  $this->recuperarDadosCategoriaContratoEValorContratado($filtro);

        $this->data['totalcontratado_numero'] = $dadosCategoriaContratoValorContratado['sum'];
        // remoção de uma consulta no banco $this->calculaTotalContratado($filtro);
        $graficoCategoriaContratos = $this->geraGraficoCategoriaContratos($dadosCategoriaContratoValorContratado);

        //remocao de uma consulto no banco
        $graficoContratosPorAno = $this->geraGraficoContratosPorAno($filtro);

        $datas_anoref['inicio_cronograma'] = $base->retornaDataMaisOuMenosQtdTipoFormato(
            'Y',
            '-',
            '2',
            'years',
            date('Y-m-d')
        );
        $datas_anoref['fim_cronograma'] = $base->retornaDataMaisOuMenosQtdTipoFormato(
            'Y',
            '+',
            '2',
            'years',
            date('Y-m-d')
        );

        //remocao de uma consulto no banco
        $graficoContratosCronograma = $this->geraGraficoContratosCronograma($filtro, $datas_anoref);

        $dt30 = $base->retornaDataMaisOuMenosQtdTipoFormato(
            'Y-m-d',
            '+',
            '30',
            'days',
            date('Y-m-d')
        );
        $dt60 = $base->retornaDataMaisOuMenosQtdTipoFormato(
            'Y-m-d',
            '+',
            '60',
            'days',
            date('Y-m-d')
        );
        $dt90 = $base->retornaDataMaisOuMenosQtdTipoFormato(
            'Y-m-d',
            '+',
            '90',
            'days',
            date('Y-m-d')
        );
        $dt180 = $base->retornaDataMaisOuMenosQtdTipoFormato(
            'Y-m-d',
            '+',
            '180',
            'days',
            date('Y-m-d')
        );
        $dt999 = $base->retornaDataMaisOuMenosQtdTipoFormato(
            'Y-m-d',
            '+',
            '99',
            'years',
            date('Y-m-d')
        );
        $dt000 = $base->retornaDataMaisOuMenosQtdTipoFormato(
            'Y-m-d',
            '-',
            '99',
            'years',
            date('Y-m-d')
        );

        $url_datas['dt30'] = '{"from":"' . $dt000 . '","to":"' . $dt30 . '"}';
        $url_datas['dt3060'] = '{"from":"' . $dt30 . '","to":"' . $dt60 . '"}';
        $url_datas['dt6090'] = '{"from":"' . $dt60 . '","to":"' . $dt90 . '"}';
        $url_datas['dt90180'] = '{"from":"' . $dt90 . '","to":"' . $dt180 . '"}';
        $url_datas['dt180'] = '{"from":"' . $dt180 . '","to":"' . $dt999 . '"}';

        $url_filtro = '';
        if (isset($filtro['orgao'])) {
            $url_filtro .= 'orgao=["' . (string)$filtro['orgao'] . '"]&';
        }
        if (isset($filtro['unidade'])) {
            $url_filtro .= 'unidade=' . (string)$filtro['unidade'] . '&';
        }
        if (isset($filtro['fornecedor'])) {
            $url_filtro .= 'fornecedor=' . (string)$filtro['fornecedor'] . '&';
        }
        if (isset($filtro['contrato'])) {
            $url_filtro .= 'numero=' . (string)$filtro['contrato'] . '&';

        }

        $this->data['contratos_total_numero'] = $this->buscaNumeroContratosPorPeriodoVencimento(
            self::TIPO_NUMERO_CONTRATOS_TOTAL,
            $filtro
        );
        $this->data['contratos_vencer30_numero'] = $this->buscaNumeroContratosPorPeriodoVencimento(
            self::TIPO_NUMERO_CONTRATOS_30,
            $filtro,
            [$dt30]
        );
        $this->data['contratos_vencer3060_numero'] = $this->buscaNumeroContratosPorPeriodoVencimento(
            self::TIPO_NUMERO_CONTRATOS_3060,
            $filtro,
            [$dt30, $dt60]
        );
        $this->data['contratos_vencer6090_numero'] = $this->buscaNumeroContratosPorPeriodoVencimento(
            self::TIPO_NUMERO_CONTRATOS_6090,
            $filtro,
            [$dt60, $dt90]
        );
        $this->data['contratos_vencer90180_numero'] = $this->buscaNumeroContratosPorPeriodoVencimento(
            self::TIPO_NUMERO_CONTRATOS_90180,
            $filtro,
            [$dt90, $dt180]
        );
        $this->data['contratos_vencer180_numero'] = $this->buscaNumeroContratosPorPeriodoVencimento(
            self::TIPO_NUMERO_CONTRATOS_180,
            $filtro,
            [$dt180]
        );
        $this->data['contratos_total_percentual'] = '100%';

        if ($this->data['contratos_total_numero'] > 0) {
            $this->data['contratos_vencer30_percentual'] = number_format(
                $this->data['contratos_vencer30_numero'] / $this->data['contratos_total_numero'] * 100,
                0,
                ',',
                ''
            ) . '%';
            $this->data['contratos_vencer3060_percentual'] = number_format(
                $this->data['contratos_vencer3060_numero'] / $this->data['contratos_total_numero'] * 100,
                0,
                ',',
                ''
            ) . '%';
            $this->data['contratos_vencer6090_percentual'] = number_format(
                $this->data['contratos_vencer6090_numero'] / $this->data['contratos_total_numero'] * 100,
                0,
                ',',
                ''
            ) . '%';
            $this->data['contratos_vencer90180_percentual'] = number_format(
                $this->data['contratos_vencer90180_numero'] / $this->data['contratos_total_numero'] * 100,
                0,
                ',',
                ''
            ) . '%';
            $this->data['contratos_vencer180_percentual'] = number_format(
                $this->data['contratos_vencer180_numero'] / $this->data['contratos_total_numero'] * 100,
                0,
                ',',
                ''
            ) . '%';
        } else {
            $this->data['contratos_vencer30_percentual'] = '0%';
            $this->data['contratos_vencer3060_percentual'] = '0%';
            $this->data['contratos_vencer6090_percentual'] = '0%';
            $this->data['contratos_vencer90180_percentual'] = '0%';
            $this->data['contratos_vencer180_percentual'] = '0%';
        }

        return view('backpack::consultapublica', [
            'data' => $this->data,
            'form' => $form,
            'graficocategoriacontratos' => $graficoCategoriaContratos,
            'graficocontratosporano' => $graficoContratosPorAno,
            'graficocontratoscronograma' => $graficoContratosCronograma,
            'anoref' => $datas_anoref,
            'url_filtro' => $url_filtro,
            'url_datas' => $url_datas
        ]);
    }

    public function indexTemp(Request $request)
    {
        $this->data['title'] = "Área Consulta Pública - Temporariamente fora do ar";

        return view('backpack::consultapublica_temp', [
            'data' => $this->data
        ]);
    }

    private function geraGraficoContratosCronograma(array $filtro = null, array $datas = null)
    {
        $base = new AdminController();

        $anoref = [];
        for ($datas['inicio_cronograma']; $datas['inicio_cronograma'] <= $datas['fim_cronograma']; $datas['inicio_cronograma']++) {
            $anoref[] = strval($datas['inicio_cronograma']);
        }

        $uri = $_SERVER['REQUEST_URI'] ?? null;
        $isTransparencia = strpos($uri, '/transp') !== false;
        $cacheKey = 'total_dados_contratados_cronograma';
        $ttl = 30; // Tempo de vida do cache (em minutos)

        // Usa Cache::remember apenas se for transparência e não houver filtros aplicado
        if ($isTransparencia && empty($filtro)) {

            // Cria ou recupera os dados do cache
            $dados = Cache::remember($cacheKey, $ttl, function () use ($ttl, $anoref, $base) {
                //Log::info('Consulta contratos por cronograma ao banco de dados executada!');

                $valores = DB::table('contratocronograma');
                $valores->select(DB::raw('CONCAT(contratocronograma.mesref,\'/\',contratocronograma.anoref) AS referencia, sum(contratocronograma.valor)'));
                $valores->join('contratos', 'contratos.id', '=', 'contratocronograma.contrato_id');
                $valores->join('unidades', 'unidades.id', '=', 'contratos.unidade_id');
                $valores->join('orgaos', 'orgaos.id', '=', 'unidades.orgao_id');
                $valores->join('fornecedores', 'fornecedores.id', '=', 'contratos.fornecedor_id');
                $valores->where('unidades.sigilo', '=', false);
                $valores->where('contratos.situacao', '=', true);
                $valores->where('contratos.elaboracao', '=', false);
                $valores->where('contratocronograma.deleted_at', '=', null);
                $valores->whereIn('contratocronograma.anoref', $anoref);
                $valores->groupBy(['anoref', 'mesref']);
                $valores->orderBy('anoref', 'asc');
                $valores->orderBy('mesref', 'asc');

                $vals = $valores->pluck('sum')->toArray();
                $refs = $valores->pluck('referencia')->toArray();

                $colors = $base->colors(count($vals));

                $chartjs = app()->chartjs
                    ->name('contratosCronograma')
                    ->type('bar')
                    ->size(['width' => 650, 'height' => 200])
                    ->labels($refs)
                    ->datasets([
                        [
                            'label' => 'Total do Mês',
                            'backgroundColor' => $colors,
                            'borderColor' => $colors,
                            'data' => $vals,
                        ]
                    ])
                    ->optionsRaw("{
                    legend: {
                        display:false
                    },
                    scales: {
                        xAxes: [{
                            gridLines: {
                                display:true
                            }
                        }]
                    },
                    tooltips: {
                        enabled: true,
                        mode: 'single',
                        callbacks: {
                            label: function(tooltipItem, data) {
                            var label = data.datasets[tooltipItem.datasetIndex].label || '';

                            if (label) {
                                label += ': ';
                            }
                            label += new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(Math.round(tooltipItem.yLabel * 100) / 100);
                            return label;
                        }
                        }
                    }
                }");

            return $chartjs;

            });

            return $dados;
        }

        // Consulta normal sem cache (com filtros ou fora de transparência)
        $valores = DB::table('contratocronograma');
        $valores->select(DB::raw('CONCAT(contratocronograma.mesref,\'/\',contratocronograma.anoref) AS referencia, sum(contratocronograma.valor)'));
        $valores->join('contratos', 'contratos.id', '=', 'contratocronograma.contrato_id');
        $valores->join('unidades', 'unidades.id', '=', 'contratos.unidade_id');
        $valores->join('orgaos', 'orgaos.id', '=', 'unidades.orgao_id');
        $valores->join('fornecedores', 'fornecedores.id', '=', 'contratos.fornecedor_id');
        $valores->where('unidades.sigilo', '=', false);
        $valores->where('contratos.situacao', '=', true);
        $valores->where('contratos.elaboracao', '=', false);
        $valores->where('contratocronograma.deleted_at', '=', null);
        $valores->whereIn('contratocronograma.anoref', $anoref);
        $valores->groupBy(['anoref', 'mesref']);
        $valores->orderBy('anoref', 'asc');
        $valores->orderBy('mesref', 'asc');

        if (isset($filtro['orgao'])) {
            $valores->where('orgaos.codigo', $filtro['orgao']);
        }
        if (isset($filtro['unidade'])) {
            $valores->where('unidades.codigo', $filtro['unidade']);
        }
        if (isset($filtro['fornecedor'])) {
            $valores->where('fornecedores.cpf_cnpj_idgener', $filtro['fornecedor']);
        }
        if (isset($filtro['contrato'])) {
            $valores->where('contratos.numero', $filtro['contrato']);
        }
        $vals = $valores->pluck('sum')->toArray();
        $refs = $valores->pluck('referencia')->toArray();

        $colors = $base->colors(count($vals));

        $chartjs = app()->chartjs
            ->name('contratosCronograma')
            ->type('bar')
            ->size(['width' => 650, 'height' => 200])
            ->labels($refs)
            ->datasets([
                [
                    'label' => 'Total do Mês',
                    'backgroundColor' => $colors,
                    'borderColor' => $colors,
                    'data' => $vals,
                ]
            ])
            ->optionsRaw("{
            legend: {
                display:false
            },
            scales: {
                xAxes: [{
                    gridLines: {
                        display:true
                    }
                }]
            },
            tooltips: {
                enabled: true,
                mode: 'single',
                callbacks: {
                    label: function(tooltipItem, data) {
                    var label = data.datasets[tooltipItem.datasetIndex].label || '';

                    if (label) {
                        label += ': ';
                    }
                    label += new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(Math.round(tooltipItem.yLabel * 100) / 100);
                    return label;
                }
                }
            }
        }");

        return $chartjs;
    }

    private function geraGraficoContratosPorAno(array $filtro = null)
    {
        $base = new AdminController();

        $uri = $_SERVER['REQUEST_URI'] ?? null;
        $isTransparencia = strpos($uri, '/transp') !== false;
        $cacheKey = 'total_dados_contratados_ano';
        $ttl = 30; // Tempo de vida do cache (em minutos)

        // Usa Cache::remember apenas se for transparência e não houver filtros aplicado
        if ($isTransparencia && empty($filtro)) {

            // Cria ou recupera os dados do cache
            $dados = Cache::remember($cacheKey, $ttl, function () use ($ttl,  $base) {
                //Log::info('Consulta contratos por Ano ao banco de dados executada!');

                $contratos = DB::table('contratos');
                $contratos->select(DB::raw('extract(year from contratos.data_assinatura) as ano, count(contratos.numero)'));
                $contratos->join('unidades', 'unidades.id', '=', 'contratos.unidade_id');
                $contratos->join('orgaos', 'orgaos.id', '=', 'unidades.orgao_id');
                $contratos->join('fornecedores', 'fornecedores.id', '=', 'contratos.fornecedor_id');
                $contratos->where('contratos.situacao', '=', true);
                $contratos->where('contratos.elaboracao', '=', false);
                $contratos->where('unidades.sigilo', '=', false);
                $contratos->whereNull('contratos.deleted_at');
                $contratos->orderBy('ano', 'desc');
                $contratos->groupBy('ano');

                $data = $contratos->pluck('count')->toArray();
                $anos = $contratos->pluck('ano')->toArray();

                $colors = $base->colors(count($data));

                $chartjs = app()->chartjs
                ->name('contratosPorAno')
                ->type('horizontalBar')
                ->size(['width' => 150, 'height' => 303])
                ->labels($anos)
                ->datasets([
                    [
                        'label' => 'Núm. Contratos',
                        'backgroundColor' => $colors,
                        'borderColor' => $colors,
                        'data' => $data,
                    ]
                ])
                ->optionsRaw([
                    'legend' => [
                        'display' => false,
                    ],
                ]);

                return $chartjs;
            });

            return $dados;
        }

        // Consulta normal sem cache (com filtros ou fora de transparência)
        $contratos = DB::table('contratos');
        $contratos->select(DB::raw('extract(year from contratos.data_assinatura) as ano, count(contratos.numero)'));
        $contratos->join('unidades', 'unidades.id', '=', 'contratos.unidade_id');
        $contratos->join('orgaos', 'orgaos.id', '=', 'unidades.orgao_id');
        $contratos->join('fornecedores', 'fornecedores.id', '=', 'contratos.fornecedor_id');
        $contratos->where('contratos.situacao', '=', true);
        $contratos->where('contratos.elaboracao', '=', false);
        $contratos->where('unidades.sigilo', '=', false);
        $contratos->whereNull('contratos.deleted_at');
        $contratos->orderBy('ano', 'desc');
        $contratos->groupBy('ano');

        if (isset($filtro['orgao'])) {
            $contratos->where('orgaos.codigo', $filtro['orgao']);
        }
        if (isset($filtro['unidade'])) {
            $contratos->where('unidades.codigo', $filtro['unidade']);
        }
        if (isset($filtro['fornecedor'])) {
            $contratos->where('fornecedores.cpf_cnpj_idgener', $filtro['fornecedor']);
        }
        if (isset($filtro['contrato'])) {
            $contratos->where('contratos.numero', $filtro['contrato']);
        }

        $data = $contratos->pluck('count')->toArray();
        $anos = $contratos->pluck('ano')->toArray();

        $colors = $base->colors(count($data));

        $chartjs = app()->chartjs
            ->name('contratosPorAno')
            ->type('horizontalBar')
            ->size(['width' => 150, 'height' => 303])
            ->labels($anos)
            ->datasets([
                [
                    'label' => 'Núm. Contratos',
                    'backgroundColor' => $colors,
                    'borderColor' => $colors,
                    'data' => $data,
                ]
            ])
            ->optionsRaw([
                'legend' => [
                    'display' => false,
                ],
            ]);

        return $chartjs;
    }

    private function buscaNumeroContratosPorPeriodoVencimento(string $tipo, array $filtro = null, array $datas = null)
    {

        $uri = $_SERVER['REQUEST_URI'] ?? null;
        $isTransparencia = strpos($uri, '/transp') !== false;
        $cacheKey = 'total_dados_contratados_por_periodo';
        $ttl = 30; // Tempo de vida do cache (em minutos)

        // Usa Cache::remember apenas se for transparência e não houver filtros aplicado
        if ($isTransparencia && empty($filtro)) {

            // Cria ou recupera os dados do cache
            $dados = Cache::remember($cacheKey, $ttl, function () use ($ttl, $tipo, $datas) {
                //Log::info('Consulta contratos por periodo ao banco de dados executada!');

                $contratos = DB::table('contratos');
                $contratos->join('unidades', 'unidades.id', '=', 'contratos.unidade_id');
                $contratos->join('orgaos', 'orgaos.id', '=', 'unidades.orgao_id');
                $contratos->join('fornecedores', 'fornecedores.id', '=', 'contratos.fornecedor_id');
                $contratos->where('unidades.sigilo', '=', false);
                $contratos->where('contratos.situacao', '=', true);
                $contratos->where('contratos.elaboracao', '=', false);

                if ($tipo == self::TIPO_NUMERO_CONTRATOS_TOTAL) {
                    $total = $contratos->count();
                }
                if ($tipo == self::TIPO_NUMERO_CONTRATOS_30) {
                    $contratos->where('contratos.vigencia_fim', '<', $datas[0]);
                    $total = $contratos->count();
                }
                if ($tipo == self::TIPO_NUMERO_CONTRATOS_3060 or $tipo == self::TIPO_NUMERO_CONTRATOS_6090 or $tipo == self::TIPO_NUMERO_CONTRATOS_90180) {
                    $contratos->where('contratos.vigencia_fim', '>=', $datas[0]);
                    $contratos->where('contratos.vigencia_fim', '<', $datas[1]);
                    $total = $contratos->count();
                }
                if ($tipo == self::TIPO_NUMERO_CONTRATOS_180) {
                    $contratos->where('contratos.vigencia_fim', '>=', $datas[0]);
                    $total = $contratos->count();
                }

                return $total;

                });

            return $dados;
        }

        // Consulta normal sem cache (com filtros ou fora de transparência)
        $contratos = DB::table('contratos');
        $contratos->join('unidades', 'unidades.id', '=', 'contratos.unidade_id');
        $contratos->join('orgaos', 'orgaos.id', '=', 'unidades.orgao_id');
        $contratos->join('fornecedores', 'fornecedores.id', '=', 'contratos.fornecedor_id');
        $contratos->where('unidades.sigilo', '=', false);
        $contratos->where('contratos.situacao', '=', true);
        $contratos->where('contratos.elaboracao', '=', false);

        if (isset($filtro['orgao'])) {
            $contratos->where('orgaos.codigo', $filtro['orgao']);
        }
        if (isset($filtro['unidade'])) {
            $contratos->where('unidades.codigo', $filtro['unidade']);
        }
        if (isset($filtro['fornecedor'])) {
            $contratos->where('fornecedores.cpf_cnpj_idgener', $filtro['fornecedor']);
        }
        if (isset($filtro['contrato'])) {
            $contratos->where('contratos.numero', $filtro['contrato']);
        }

        if ($tipo == self::TIPO_NUMERO_CONTRATOS_TOTAL) {
            $total = $contratos->count();
        }
        if ($tipo == self::TIPO_NUMERO_CONTRATOS_30) {
            $contratos->where('contratos.vigencia_fim', '<', $datas[0]);
            $total = $contratos->count();
        }
        if ($tipo == self::TIPO_NUMERO_CONTRATOS_3060 or $tipo == self::TIPO_NUMERO_CONTRATOS_6090 or $tipo == self::TIPO_NUMERO_CONTRATOS_90180) {
            $contratos->where('contratos.vigencia_fim', '>=', $datas[0]);
            $contratos->where('contratos.vigencia_fim', '<', $datas[1]);
            $total = $contratos->count();
        }
        if ($tipo == self::TIPO_NUMERO_CONTRATOS_180) {
            $contratos->where('contratos.vigencia_fim', '>=', $datas[0]);
            $total = $contratos->count();
        }

        return $total;
    }

    private function recuperarDadosCategoriaContratoEValorContratado(array $filtro = null)
    {

        $uri = $_SERVER['REQUEST_URI'] ?? null;
        $isTransparencia = strpos($uri, '/transp') !== false;
        $cacheKey = 'total_dados_contratados_valor';
        $ttl = 30; // Tempo de vida do cache (em minutos)

        // Usa Cache::remember apenas se for transparência e não houver filtros aplicado
        if ($isTransparencia && empty($filtro)) {

            // Cria ou recupera os dados do cache
            $dados = Cache::remember($cacheKey, $ttl, function () use ($ttl) {
                //Log::info('Consulta ao banco de dados por valor executada!');

                $contratos = DB::table('contratos')
                    ->select(DB::raw('categoria_id, count(categoria_id) as count, sum(valor_global) as total'))
                    ->join('unidades', 'unidades.id', '=', 'contratos.unidade_id')
                    ->join('orgaos', 'orgaos.id', '=', 'unidades.orgao_id')
                    ->join('fornecedores', 'fornecedores.id', '=', 'contratos.fornecedor_id')
                    ->where('contratos.situacao', true)
                    ->where('contratos.elaboracao', false)
                    ->where('unidades.sigilo', false)
                    ->orderBy('categoria_id', 'asc')
                    ->groupBy('categoria_id')
                    ->get();

                return [
                    'sum' => $contratos->sum('total'),
                    'valoresGraficoCategoria' => $contratos->pluck('categoria_id')->toArray(),
                ];
            });

            return $dados;
        }

        // Consulta normal sem cache (com filtros ou fora de transparência)
        $contratos = DB::table('contratos')
            ->select(DB::raw('categoria_id, count(categoria_id) as count, sum(valor_global) as total'))
            ->join('unidades', 'unidades.id', '=', 'contratos.unidade_id')
            ->join('orgaos', 'orgaos.id', '=', 'unidades.orgao_id')
            ->join('fornecedores', 'fornecedores.id', '=', 'contratos.fornecedor_id')
            ->where('contratos.situacao', true)
            ->where('contratos.elaboracao', false)
            ->where('unidades.sigilo', false)
            ->orderBy('categoria_id', 'asc')
            ->groupBy('categoria_id');

        if (!empty($filtro)) {
            if (isset($filtro['orgao'])) {
                $contratos->where('orgaos.codigo', $filtro['orgao']);
            }
            if (isset($filtro['unidade'])) {
                $contratos->where('unidades.codigo', $filtro['unidade']);
            }
            if (isset($filtro['fornecedor'])) {
                $contratos->where('fornecedores.cpf_cnpj_idgener', $filtro['fornecedor']);
            }
            if (isset($filtro['contrato'])) {
                $contratos->where('contratos.numero', $filtro['contrato']);
            }
        }

        $resultado = $contratos->get();
        return [
            'sum' => $resultado->sum('total'),
            'valoresGraficoCategoria' => $resultado->pluck('count', 'categoria_id')->toArray(),
        ];
    }

    private function calculaTotalContratado(array $filtro = null)
    {

        $contratos = DB::table('contratos');
        $contratos->select(DB::raw('sum(valor_global)'));
        $contratos->join('unidades', 'unidades.id', '=', 'contratos.unidade_id');
        $contratos->join('orgaos', 'orgaos.id', '=', 'unidades.orgao_id');
        $contratos->join('fornecedores', 'fornecedores.id', '=', 'contratos.fornecedor_id');
        $contratos->where('contratos.situacao', '=', true);
        $contratos->where('contratos.elaboracao', '=', false);
        $contratos->where('unidades.sigilo', '=', false);

        if (isset($filtro['orgao'])) {
            $contratos->where('orgaos.codigo', $filtro['orgao']);
        }
        if (isset($filtro['unidade'])) {
            $contratos->where('unidades.codigo', $filtro['unidade']);
        }
        if (isset($filtro['fornecedor'])) {
            $contratos->where('fornecedores.cpf_cnpj_idgener', $filtro['fornecedor']);
        }
        if (isset($filtro['contrato'])) {
            $contratos->where('contratos.numero', $filtro['contrato']);
        }
        $data = $contratos->pluck('sum')->first();

        return $data;
    }

    private function geraGraficoCategoriaContratos(array $dadosCategoriaContratoValorContratado = null)
    {
        $base = new AdminController();

        $uri = $_SERVER['REQUEST_URI'] ?? null;
        $isTransparencia = strpos($uri, '/transp') !== false;
        $cacheKey = 'total_dados_contratados_categoria';
        $ttl = 30; // Tempo de vida do cache (em minutos)

        /* Usa Cache::remember apenas se for transparência
           Essa função foi cacheada pois ela consultava no banco
           para buscar as categorias do contrato como ex:
            0 => "Compras"
            160376 => "Informática (TIC)"
            173596 => "Locação Imóveis"
            176891 => "Mão de Obra"
            186359 => "Internacional"
            186870 => "Obras"
            192053 => "Serviços de Engenharia"
            203698 => "Serviços"
            340333 => "Cessão"
            345029 => "A definir"
            391937 => "Serviços de Saúde"
        */
        if ($isTransparencia) {

            // Cria ou recupera os dados do cache
            $dados = Cache::remember($cacheKey, $ttl, function () use ($ttl, $dadosCategoriaContratoValorContratado, $base) {

                $categoria_contrato = Codigoitem::whereHas('codigo', function ($q) {
                    $q->where('descricao', '=', 'Categoria Contrato');
                })
                    ->join('contratos', function ($j) {
                        $j->on('codigoitens.id', '=', 'contratos.categoria_id');
                    })
                    ->join('unidades', function ($j) {
                        $j->on('unidades.id', '=', 'contratos.unidade_id');
                    })
                    ->join('orgaos', function ($j) {
                        $j->on('orgaos.id', '=', 'unidades.orgao_id');
                    })
                    ->join('fornecedores', function ($j) {
                        $j->on('fornecedores.id', '=', 'contratos.fornecedor_id');
                    })
                    ->orderBy('codigoitens.id', 'asc');

                $cat = array_unique($categoria_contrato->pluck('descricao')->toArray());

                $categorias = [];
                foreach ($cat as $c) {
                    $categorias[] = $c;
                }

                $colors = $base->colors(count($dadosCategoriaContratoValorContratado['valoresGraficoCategoria']));

                $chartjs = app()->chartjs
                    ->name('categoriasContratos')
                    ->type('doughnut')
                    ->size(['width' => 400, 'height' => 275])
                    ->labels($categorias)
                    ->datasets([
                        [
                            'backgroundColor' => $colors,
                            'borderColor' => $colors,
                            'data' => $dadosCategoriaContratoValorContratado['valoresGraficoCategoria'],
                        ]
                    ]);

                return $chartjs;
            });

            return $dados;
        }

        // Consulta normal sem cache (com filtros ou fora de transparência)
        $categoria_contrato = Codigoitem::whereHas('codigo', function ($q) {
            $q->where('descricao', '=', 'Categoria Contrato');
        })
            ->join('contratos', function ($j) {
                $j->on('codigoitens.id', '=', 'contratos.categoria_id');
            })
            ->join('unidades', function ($j) {
                $j->on('unidades.id', '=', 'contratos.unidade_id');
            })
            ->join('orgaos', function ($j) {
                $j->on('orgaos.id', '=', 'unidades.orgao_id');
            })
            ->join('fornecedores', function ($j) {
                $j->on('fornecedores.id', '=', 'contratos.fornecedor_id');
            })
            ->orderBy('codigoitens.id', 'asc');

        if (isset($filtro['orgao'])) {
            $categoria_contrato->where('orgaos.codigo', $filtro['orgao']);
        }
        if (isset($filtro['unidade'])) {
            $categoria_contrato->where('unidades.codigo', $filtro['unidade']);
        }
        if (isset($filtro['fornecedor'])) {
            $categoria_contrato->where('fornecedores.cpf_cnpj_idgener', $filtro['fornecedor']);
        }
        if (isset($filtro['contrato'])) {
            $categoria_contrato->where('contratos.numero', $filtro['contrato']);
        }

        $cat = array_unique($categoria_contrato->pluck('descricao')->toArray());

        $categorias = [];
        foreach ($cat as $c) {
            $categorias[] = $c;
        }

        $colors = $base->colors(count($dadosCategoriaContratoValorContratado['valoresGraficoCategoria']));

        $chartjs = app()->chartjs
            ->name('categoriasContratos')
            ->type('doughnut')
            ->size(['width' => 400, 'height' => 275])
            ->labels($categorias)
            ->datasets([
                [
                    'backgroundColor' => $colors,
                    'borderColor' => $colors,
                    'data' => $dadosCategoriaContratoValorContratado['valoresGraficoCategoria'],
                ]
            ]);

        return $chartjs;
    }

    private function trataDadosView(array $campos)
    {
        $orgao = [];
        $unidade = [];
        $fornecedor = [];
        $contrato = [];

        foreach ($campos as $key => $value) {
            if ($key == 'orgao') {
                $orgao = Orgao::select(DB::raw("CONCAT(codigo,' - ',nome) AS nome"), 'codigo')
                    ->where('codigo', $value)
                    ->pluck('nome', 'codigo')
                    ->toArray();
            }

            if ($key == 'unidade') {
                $unidade = Unidade::select(DB::raw("CONCAT(codigo,' - ',nomeresumido) AS nome"), 'codigo')
                    ->where('codigo', $value)
                    ->pluck('nome', 'codigo')
                    ->toArray();
            }

            if ($key == 'fornecedor') {
                $fornecedor = Fornecedor::select(
                    DB::raw("CONCAT(cpf_cnpj_idgener,' - ',nome) AS nome"),
                    'cpf_cnpj_idgener'
                )
                    ->where('cpf_cnpj_idgener', $value)
                    ->pluck('nome', 'cpf_cnpj_idgener')
                    ->toArray();
            }

            if ($key == 'contrato') {
                $contrato = Contrato::select('numero')
                    ->where('numero', $value)
                    ->pluck('numero', 'numero')
                    ->toArray();
            }
        }

        return [
            'orgao' => $orgao,
            'unidade' => $unidade,
            'fornecedor' => $fornecedor,
            'contrato' => $contrato
        ];
    }
}
