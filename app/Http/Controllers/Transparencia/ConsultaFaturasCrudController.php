<?php

namespace App\Http\Controllers\Transparencia;

use App\Http\Traits\ContratoFaturaTrait;
use App\Models\Contrato;
use App\Models\Justificativafatura;
use App\Models\Tipolistafatura;
use Backpack\CRUD\CrudPanel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

/**
 * Class ConsultaFaturasCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ConsultaFaturasCrudController extends ConsultaContratoBaseCrudController
{
    use ContratoFaturaTrait;

    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */

        $this->crud->setModel('App\Models\Contratofatura');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/transparencia/faturas');
        $this->crud->setListView('backpack::crud.gescon.consulta.instrumento_de_cobranca.list');
        $this->crud->setEntityNameStrings('Instrumento de Cobrança', 'Instrumentos de Cobrança');
        $this->crud->setHeading('Consulta Instrumentos de Cobrança por Contrato');
        $this->crud->setSubheading('Ordem Cronológica de Pagamento', 'index');
        $this->crud->enableExportButtons();

        $this->crud->addClause('leftJoin', 'contratos',
            'contratos.id', '=', 'contratofaturas.contrato_id'
        );
        $this->crud->addClause('leftJoin', 'fornecedores',
            'fornecedores.id', '=', 'contratos.fornecedor_id'
        );
        $this->crud->addClause('leftJoin', 'unidades',
            'unidades.id', '=', 'contratos.unidade_id'
        );
        $this->crud->addClause('leftJoin', 'orgaos',
            'orgaos.id', '=', 'unidades.orgao_id'
        );
        $this->crud->addClause('leftJoin', 'tipolistafatura',
            'tipolistafatura.id', '=', 'contratofaturas.tipolistafatura_id'
        );
        $this->crud->addClause('leftJoin', 'justificativafatura',
            'justificativafatura.id', '=', 'contratofaturas.justificativafatura_id'
        );
        $this->crud->addClause('leftJoin', 'apropriacoes_faturas_contratofaturas',
            'apropriacoes_faturas_contratofaturas.contratofaturas_id', '=', 'contratofaturas.id'
        );
        $this->crud->addClause('leftJoin', 'contratohistorico',
            'contratohistorico.contrato_id', '=', 'contratos.id'
        );

        $this->crud->addClause('leftJoin', 'amparo_legal_contratohistorico',
            'amparo_legal_contratohistorico.contratohistorico_id', '=', 'contratohistorico.id'
        );
        $this->crud->addClause('leftJoin', 'amparo_legal',
            'amparo_legal.id', '=', 'amparo_legal_contratohistorico.amparo_legal_id'
        );
        $this->crud->addClause('leftJoin', 'codigoitens',
            'codigoitens.id', '=', 'contratohistorico.tipo_id'
        );
        $this->crud->addClause('where', 'codigoitens.descricao', 'NOT ILIKE', '%termo%');

        $this->crud->addClause('select', [
            \DB::raw("COALESCE(amparo_legal.ato_normativo, '-') as ato_normativo"),
            'contratofaturas.*',
            'contratos.numero as numero_contrato',
            'apropriacoes_faturas_contratofaturas.contratofaturas_id'
        ]);

        $situacaoParam = strpos(url()->full(), 'PGO');
        if($situacaoParam === false) {
            $this->crud->addClause('where', 'contratofaturas.situacao', '<>', 'PGO');
        }

        $this->ordenacaoPadrao($this->crud);

        $this->defineConfiguracaoPadrao();

    }

    public function show($id)
    {
        $content = parent::show($id);

        $this->crud->removeColumns([
            'contrato_id',
            'tipolistafatura_id',
            'justificativafatura_id',
            'valor',
            'juros',
            'multa',
            'glosa',
            'valorliquido',
        ]);

        return $content;
    }

    protected function aplicaFiltrosEspecificos(): void
    {
        $tipolista = Tipolistafatura::where('situacao', true)
            ->pluck('nome', "id")
            ->toArray();

        $this->crud->addFilter([ // dropdown filter
            'name' => 'tipolista',
            'type' => 'select2_multiple',
            'label' => 'Tipo Lista'
        ], $tipolista
            , function ($value) {
                $this->crud->addClause('whereIN'
                    , 'contratofaturas.tipolistafatura_id', json_decode($value));
            });

        $justificativa = Justificativafatura::where('situacao', true)
            ->pluck('nome', "id")
            ->toArray();

        $this->crud->addFilter([ // dropdown filter
            'name' => 'justificativa',
            'type' => 'select2_multiple',
            'label' => 'Justificativa'
        ], $justificativa
            , function ($value) {
                $this->crud->addClause('whereIn'
                    , 'contratofaturas.justificativafatura_id', json_decode($value));
            });

        // 3
        $situacaoFatura = config('app.situacao_fatura');
        $situacaoFaturaInicial = $situacaoFatura;
        unset($situacaoFaturaInicial['PGO']);
        $situacaoFaturaInicial = array_keys($situacaoFaturaInicial);

        $this->crud->addFilter([ // dropdown filter
            'name' => 'situacao',
            'type' => 'select2_multiple',
            'label' => 'Situação'
        ], $situacaoFatura
            , function ($value){
                $this->crud->addClause('whereIn'
                    , 'contratofaturas.situacao', json_decode($value));
            },
            function () use ($situacaoFaturaInicial){
                $this->crud->addClause('whereIn', 'contratofaturas.situacao', $situacaoFaturaInicial);
            }
        );

        $this->crud->addFilter([ // dropdown filter
            'name' => 'prazo',
            'type' => 'select2',
            'label' => 'Prazo',
        ], [
            'asc' => 'Crescente',
            'desc' => 'Decrescente',
        ], function ($value) {
            if ($value === 'asc') {
                $this->crud->addClause('orderBy', 'contratofaturas.prazo', 'asc');
            } else {
                $this->crud->addClause('orderBy', 'contratofaturas.prazo', 'desc');
            }
        });

        // 1
        $this->adicionaFiltroLei($this->crud);

        $this->crud->addFilter(
            [
                'type' => 'text',
                'name' => 'numerocontrato',
                'label' => 'Número Contrato'
            ],
            false,
            function ($value) {
                $this->crud->query->where(function ($query) use ($value) {
                    $query->where('contratos.numero', 'ILIKE', "%{$value}%");
                });
            }
        );
    }

    protected function adicionaColunasEspecificasNaListagem(): void
    {
        $this->crud->addColumns([
            [
                'name' => 'getFornecedor',
                'label' => 'Fornecedor',
                'type' => 'model_function',
                'function_name' => 'getFornecedor',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere('fornecedores.cpf_cnpj_idgener', 'ilike', "%" . strtoupper($searchTerm) . "%");
                    $query->orWhere('fornecedores.nome', 'ilike', "%" . strtoupper($searchTerm) . "%");
                },
                'priority' => 1
            ],
            [
                'name' => 'getContratoNumero',
                'label' => 'Número Contrato',
                'type' => 'model_function',
                'function_name' => 'getContrato',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere('contratos.numero', 'ilike', "%" . strtoupper($searchTerm) . "%");
                },
                'priority' => 2
            ],
            [
                'name' => 'ato_normativo',
                'label' => 'Lei',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'priority' => 5
            ],
            [
                'name' => 'getTipoLista',
                'label' => 'Tipo Lista',
                'type' => 'model_function',
                'function_name' => 'getTipoLista',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'priority' => 3
            ],
            [
                'name' => 'getJustificativa',
                'label' => 'Justificativa',
                'type' => 'model_function',
                'function_name' => 'getJustificativa',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'priority' => 9
            ],
            [
                'name' => 'getNumero',
                'label' => 'Número',
                'type' => 'model_function',
                'function_name' => 'getNumero',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere('contratofaturas.numero', 'like', "%" . strtoupper($searchTerm) . "%");
                },
                'priority' => 8
            ],
            [
                'name' => 'emissao',
                'label' => 'Dt. Emissão',
                'type' => 'date',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'priority' => 7
            ],
            [
                'name' => 'ateste',
                'label' => 'Dt. Ateste',
                'type' => 'date',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'priority' => 6
            ],
            [
                'name' => 'vencimento',
                'label' => ' Dt. limite Pagamento',
                'type' => 'date',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'priority' => 4
            ],
            [
                'name' => 'prazo',
                'label' => 'Prazo',
                'type' => 'date',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'priority' => 2
            ],
            [
                'name' => 'formatValor',
                'label' => 'Valor',
                'type' => 'model_function',
                'function_name' => 'formatValor',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere('contratofaturas.valor', 'like', "%" . $searchTerm . "%");
                },
                'priority' => 10
            ],
            [
                'name' => 'formatJuros',
                'label' => 'Juros',
                'type' => 'model_function',
                'function_name' => 'formatJuros',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'priority' => 11
            ],
            [
                'name' => 'formatMulta',
                'label' => 'Multa',
                'type' => 'model_function',
                'function_name' => 'formatMulta',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'priority' => 12
            ],
            [
                'name' => 'formatGlosa',
                'label' => 'Glosa',
                'type' => 'model_function',
                'function_name' => 'formatGlosa',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'priority' => 13
            ],
            [
                'name' => 'formatValorLiquido',
                'label' => 'Valor Líquido a pagar',
                'type' => 'model_function',
                'function_name' => 'formatValorLiquido',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'priority' => 14
            ],
            [
                'name' => 'getProcesso',
                'label' => 'Processo',
                'type' => 'model_function',
                'function_name' => 'getProcesso',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere('contratofaturas.processo', 'like', "%" . strtoupper($searchTerm) . "%");
                },
                'priority' => 15
            ],
            [
                'name' => 'protocolo',
                'label' => 'Dt. Protocolo',
                'type' => 'date',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'priority' => 16
            ],
            [
                'name' => 'infcomplementar',
                'label' => 'Informações Complementares',
                'type' => 'text',
                'limit' => 255,
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'priority' => 17
            ],
            [
                'name' => 'repactuacao',
                'label' => 'Repactuação',
                'type' => 'boolean',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                // optionally override the Yes/No texts
                'options' => [0 => 'Não', 1 => 'Sim'],
                'priority' => 18
            ],
            [
                'name' => 'mesref',
                'label' => 'Mês Referência',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'priority' => 19
            ],
            [
                'name' => 'anoref',
                'label' => 'Ano Referência',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'priority' => 20
            ],
            [
                'name' => 'situacao',
                'label' => 'Situação',
                'type' => 'select_from_array',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'options' => config('app.situacao_fatura'),
                'priority' => 21
            ],
        ]);
    }
}
