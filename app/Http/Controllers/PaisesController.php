<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Paises;
use Illuminate\Http\Request;
use DB;

class PaisesController extends Controller
{


    public function listaPaises(Request $request) {
        $search_term = $request->input('q');

        $brasil = Paises::BRASIL;
        $desconhecido = Paises::DESCONHECIDO;

        if ($search_term) {
            $query = Paises::where('nome', 'ILIKE', '%' . strtoupper($search_term) . '%')
                ->orderBy(DB::raw("id != {$brasil}, id != {$desconhecido}, nome"));

            $result = $query
                ->paginate(10);
        } else {
            $result = Paises::orderBy(DB::raw("id != {$brasil}, id != {$desconhecido}, nome"))->paginate(10);
        }

        return $result;
    }

    public function atualizaPaises()
    {
        Paises::atualiza();
    }

}
