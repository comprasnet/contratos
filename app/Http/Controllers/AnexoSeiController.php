<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contratoarquivo;
use App\Http\Controllers\classSEI;
use App\Http\Controllers\classSIP;

class AnexoSeiController extends Controller
{
    public $link;

    /* Para pesquisar os anexos do processo no SEI
    @  nrProcesso : $request->processo
    @  idProcesso : $request->idProcesso
    @  retorno    : Link da lista de anexos
    */
    public function ajaxPesquisaSei(Request $request, classSEI $objSei ){
        $nrProcesso = !empty($request->nrProcessoSei) ? $request->nrProcessoSei : '12600.100102/2020-04';
        $idProcesso = $request->idProcesso;

        $listaProcedimentos = $objSei->consultarProcedimento(null,$nrProcesso); //objAP->getNr_processo());
        if ($listaProcedimentos){
            $linkAcessoSei = $listaProcedimentos->LinkAcesso;
        }

        $listaAnexos = $this->ajaxListaAnexosProcesso($linkAcessoSei, $idProcesso, $objSei);
        return $listaAnexos;
    }

    /* Para pesquisar os anexos do processo no SEI
    @  nrProcesso : $request->processo
    @  idProcesso : $request->idProcesso
    @  retorno    : HTML com a lista de anexos
    */
    public function ajaxListaAnexosProcesso($linkAcessoSei, $idProcesso, classSEI $objSei) {
        return $objSei->listarAnexosProcesso($linkAcessoSei,$idProcesso);
    }

    /* Para pesquisar os processos relacionados no SEI
    @  nrProcesso : $request->processo
    @  retorno    : array com todos os processos relacionados
    */
    public function ajaxPesquisaSeiVinculado(Request $request){

        $nrProcesso = !empty($request->nrProcessoSei) ? $request->nrProcessoSei : '12600.100102/2020-04';
        $result = [
            'dados' => '',
            'erros' => ''
        ];
        try
        {
            $objSei = new classSEI();
            $processosRelacionados  = $objSei->ListarProcessosFilho($nrProcesso);
            $result['dados']        = $processosRelacionados->ProcedimentosRelacionados;

        }
        catch (\Exception $e)
        {
            $pos = strrpos($e->__toString(), "não encontrado");
            if ($pos > 0) { // note: three equal signs
                $processosRelacionados = "";
            }
            $result['erros'] = $e->getMessage();
        }

        return $result;
    }

    /* Para pesquisar os processos relacionados no SEI
    @  nrProcesso : $request->processo
    @  retorno    : array com todos os processos relacionados
    */
    public function ajaxSeiLinksGravados(Request $request){
        if (!empty($request->idProcesso)) {
            $arquivoAnexos = new Contratoarquivo();
            $resultado = $arquivoAnexos->Where('contrato_arquivos.contrato_id',$request->idProcesso)
            ->join('contratohistorico', 'contratohistorico.id', '=', 'contrato_arquivos.contratohistorico_id')
            ->select(
               'contrato_arquivos.*',
               'contratohistorico.numero as numero',
            )
            ->get();
        }
        return $resultado;
    }

    public function ajaxTestaConexaoSeiSip(Request $request){
        $urlSei = $request->api_integracao_sei;
        $chaveAcesso = $request->api_integracao_sei_chave_acesso;

        $this->urlWsdl = $urlSei; //'https://seihom.economia.gov.br/sei/controlador_ws.php?servico=sei';
        try {
            $this->soapClient = new \SoapClient($this->urlWsdl, ['exceptions' => true]);
            $this->soapClient->listarEstados(config('app.siglaSistema'), $chaveAcesso, null);
            echo "Conectado com sucesso !!!"; die();
        } catch ( \Exception $e ) {
            
                echo "<div class='alert alert-warning' role='alert'>Não foi possível conectar com a URL <strong>$urlSei</strong>. Verifique se o endereço está correto e tente novamente.</div>";
                echo "Informe a mensagem abaixo ao Administrador, caso o erro continue!<br><br>";
                echo "<font color=red>". $e->getMessage(). "</font>"; die();
        }
    }

}
