<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Painel\OrcamentarioController;
use App\Http\Traits\Authorizes;
use App\Models\Apropriacao;
use App\Models\Contrato;
use App\Models\Empenho;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class InfoContratosController extends Controller
{

    use Authorizes;

    public function index() {

        if($this->bloquearInformacaoPHP()) {
            return abort(404);
        }

        phpinfo(); 
    }

}
