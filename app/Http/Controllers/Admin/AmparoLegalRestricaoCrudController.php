<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AmparoLegalRestricaoRequest as StoreRequest;
use App\Http\Requests\AmparoLegalRestricaoRequest as UpdateRequest;
use App\Models\AmparoLegalRestricao;
use App\Models\AmparoLegalRestricaoCodigo;
use App\Models\Codigoitem;
use App\Models\SfRestricaoAmparoLegal;
use App\Models\SfRestricaAmparoLegal;
use App\XML\Execsiafi;
use Backpack\CRUD\app\Http\Controllers\CrudController;

/**
 * Class CodigoitemCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class AmparoLegalRestricaoCrudController extends CrudController
{
    public function setup()
    {
        if (backpack_user()->hasRole('Administrador Suporte')) {
            abort('403', config('app.erro_permissao'));
        }

        if (!backpack_user()->hasRole('Administrador')) {
            abort('403', config('app.erro_permissao'));
        }
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $codigo_id = \Route::current()->parameter('amparo_legal_id');

        $this->crud->setModel(AmparoLegalRestricao::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/admin/amparolegal/' . $codigo_id . '/restricao');
        $this->crud->setEntityNameStrings('Amparo Legal Restrições', 'Amparo Legal Restrição');

        $this->crud->addClause('where', 'amparo_legal_id', '=', $codigo_id);

        $this->crud->enableExportButtons();
        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');
        $this->crud->allowAccess('show');

        (backpack_user()->can('codigoitem_inserir')) ? $this->crud->allowAccess('create') : null;
        (backpack_user()->can('codigoitem_editar')) ? $this->crud->allowAccess('update') : null;
        (backpack_user()->can('codigoitem_deletar')) ? $this->crud->allowAccess('delete') : null;

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $this->crud->addColumn([    // SELECT
            'name' => 'getCodigoRestricao',
            'label' => 'Tipo de Restrição', // Table column heading
            'type' => 'model_function',
            'function_name' => 'getCodigoRestricao'
        ]);

        $this->crud->addColumn([    // SELECT
            'label' => 'Código da Restrição',
            'type' => 'text',
            'name' => 'codigo_restricao'
        ]);

        $this->crud->addColumn([    // SELECT
            'label' => 'Valor da Restrição',
            'type' => 'model_function',
            'name' => 'nome',
            'model' => AmparoLegalRestricaoCodigo::class,
            'function_name' => 'labelRestricao'
        ]);

        $codigosItem = Codigoitem::with('codigo')
            ->whereHas('codigo', function ($q) {
                return $q->where('descricao', 'Restrições do amparo legal');
            })
            ->orderBy('descres')
            ->get()
            ->map(function ($item) {
                return [
                    'id' => $item->id,
                    'nome' => $item->descres . ' - ' . $item->descricao
                ];
            })
            ->pluck('nome', 'id')
            ->toArray();

        $this->crud->addField([
            'name' => 'amparo_legal_id',
            'type' => 'hidden',
            'value' => $codigo_id
        ]);
        $this->crud->addField([
            'label' => "Restricoes",
            'type' => 'select2_from_array',
            'name' => 'tipo_restricao_id',
            'options' => $codigosItem,
            'allows_null' => true
        ]);

        $this->crud->addField([
            'label' => "Regra",
            'type' => 'select2_from_array',
            'name' => 'regra',
            'placeholder' => 'Selecione',
            'options' => ['SOMENTE' => 'Somente', 'EXCETO' => "Exceto" ],
            'allows_null' => true
        ]);

        $this->crud->addField([
            'type' => 'select2_from_ajax',
            'name' => 'codigo_restricao',
            'label' => 'Código da Restrição',
            'placeholder' => 'Selecione',
            'model' => AmparoLegalRestricaoCodigo::class,
            'entity' => 'amparoLegalRestricaoCodigo',
            'attribute' => 'nome',
            'minimum_input_length' => 2,
            'data_source' => url("api/amparolegalrestricao"),
            'dependencies' => ['tipo_restricao_id']
        ]);

        // add asterisk for fields that are required in CodigoitemRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
        $this->crud->addButtonFromView('top', 'voltar', 'voltaramparolegal', 'end');
    }

    public function store(StoreRequest $request)
    {
        $params = $this->filtraCampos($request);

        $amparoLegalRestricao = AmparoLegalRestricao::create($params);
        \Alert::success(trans('backpack::crud.insert_success'))->flash();
        $url = "admin/amparolegal/{$request->amparo_legal_id}/restricao";
        return redirect($url);
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $amparoLegalRestricao = AmparoLegalRestricao::find($request->id);
        $params = $this->filtraCampos($request);
        $amparoLegalRestricao->update($params);
        \Alert::success(trans('backpack::crud.update_success'))->flash();
        $url = "admin/amparolegal/{$request->amparo_legal_id}/restricao";
        return redirect($url);
    }

    public function destroy($id)
    {
        $id = $this->crud->getCurrentEntryId() ?? $id;
        $amparoLegalRestricao = AmparoLegalRestricao::find($id);
        $amparoLegalRestricao->delete();
    }

    private function filtraCampos($request)
    {
        $params = $request->toArray();
        $descres = Codigoitem::find($request->tipo_restricao_id)->descres;

        switch ($descres) {
            case 'ADMIN':
                $params['codigo_restricao'] = sprintf("%02d", $params['codigo_restricao']);
                break;

            case 'AGEXEC':
                $params['codigo_restricao'] = 'S';
                break;

            default:
                # code...
                break;
        }

        return $params;
    }

    public function show($id)
    {
        $content = parent::show($id);

        $this->crud->removeColumn('visivel');

        return $content;
    }

    private function salvarDadosSiafi($amparoLegalRestricao, $exclusao = false)
    {
        $sfAmparoLegalDados = $amparoLegalRestricao->amparosLegais;
        $sfOrcAmparoLegalDados = $sfAmparoLegalDados->sfOrcAmparoLegalDados()->latest('updated_at')->first();
        $sfRestricaoAmparoLegal = SfRestricaoAmparoLegal::firstOrNew(['sforcamparolegaldados_id' => $sfOrcAmparoLegalDados->id])->first();
        if ($exclusao) {
            $sfRestricaoAmparoLegal->delete();
        } else {
            $sfRestricaoAmparoLegal->sforcamparolegaldados_id = $sfOrcAmparoLegalDados->id;
            $codigoSiafi = config('app.tipo_restricao_codigo_siafi');
            $sfRestricaoAmparoLegal->tiporestricao = $codigoSiafi[$amparoLegalRestricao->codigoItem->descres];
            $sfRestricaoAmparoLegal->regra = 'SOMENTE';
            $sfRestricaoAmparoLegal->valor = $amparoLegalRestricao->codigo_restricao;
            $sfRestricaoAmparoLegal->save();
        }

        $ws_siafi = new Execsiafi;
        $resposta = $ws_siafi->enviarAmparoLegal(
            session('user_ug'),
            env('AMBIENTE_SIAFI'),
            date('Y'),
            $sfOrcAmparoLegalDados
        );
        preg_match('/<resultado>(.+)<\/resultado>/', $resposta, $matches);

        switch ($matches[1]) {
            case 'FALHA':
                preg_match('/<txtMsg>(.+)<\/txtMsg>/', $resposta, $mensagem);
                $sfOrcAmparoLegalDados->retornosiafi = $resposta;
                $sfOrcAmparoLegalDados->save();
                if (isset($mensagem[1]))
                    \Alert::error($mensagem[1])->flash();
                break;
            case 'SUCESSO':
                preg_match('/<codAmparoLegal>(.+)<\/codAmparoLegal>/', $resposta, $codAmparoLegal);
                $sfOrcAmparoLegalDados->codamparolegal = $codAmparoLegal[1];
                $sfOrcAmparoLegalDados->retornosiafi = $resposta;
                $sfOrcAmparoLegalDados->save();
                break;

            default:
                # code...
                break;
        }
    }
}
