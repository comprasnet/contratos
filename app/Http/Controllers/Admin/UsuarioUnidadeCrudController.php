<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UsuarioUnidadeRequest as StoreRequest;
use App\Http\Requests\UsuarioUnidadeRequest as UpdateRequest;
use App\Jobs\UserMailPasswordJob;
use App\Models\BackpackUser;
use App\Models\Role;
use App\Models\Unidade;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\CrudPanel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class UsuarioUnidadeCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class UsuarioUnidadeCrudController extends CrudController
{
    private $unidadeUser;
    private $todasUnidadesUser;
    private $usuarioLogadoCPF;
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        if (!backpack_user()->hasRole('Administrador Unidade') && !backpack_user()->hasRole('Administrador Suporte')) { //alterar para novo grupo de Administrador Orgão
            abort('403', config('app.erro_permissao'));
        }

        $unidadesUserSessao = array(session()->get('user_ug_id'));
        $this->unidadeUser = Unidade::find(session()->get('user_ug_id'));
        $this->usuarioLogadoCPF = BackpackUser::find(backpack_user()->id);
          
        $this->todasUnidadesUser = Unidade::whereHas('users', function ($us) {
            $us->where('cpf', backpack_user()->cpf);
        })->orWhereHas('user', function ($usu) {
            $usu->where('cpf', backpack_user()->cpf);
        })->pluck('id')->toArray();
    
        $this->crud->setModel('App\Models\BackpackUser');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/admin/usuariounidade');

        $this->crud->setEntityNameStrings('Usuário Unidade(s)', 'Usuários Unidade(s)');

        $this->crud->addClause('leftJoin', 'unidades', 'unidades.id', '=', 'users.ugprimaria');
       /**  
        * Adiciona o leftJoin com uma condição adicional para evitar duplicidade 
       */
        $this->crud->addClause('leftJoin', 'unidadesusers', function ($join) use ($unidadesUserSessao) {
            $join->on('unidadesusers.user_id', '=', 'users.id')
                ->where('unidadesusers.unidade_id', '=', $unidadesUserSessao);
        });

        $this->crud->query->where(function ($q) use ($unidadesUserSessao) {        
            $q->WhereIn('users.ugprimaria', $unidadesUserSessao)        
                ->orWhereIn('unidadesusers.unidade_id', $unidadesUserSessao);  
        });

        $this->crud->query->select('users.*');

        $this->crud->enableExportButtons();
        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');

        (backpack_user()->can('usuariounidade_inserir')) ? $this->crud->allowAccess('create') : null;
        (backpack_user()->can('usuariounidade_editar')) ? $this->crud->allowAccess('update') : null;

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $colunas = $this->Colunas();
        $this->crud->addColumns($colunas);

        $ugs = Unidade::select(DB::raw("CONCAT(codigo,' - ',nomeresumido) AS nome"), 'id')
            ->where('situacao', '=', true)
            ->whereIn('id', $this->todasUnidadesUser)
            ->orderBy('codigo', 'asc')
            ->pluck('nome', 'id')
            ->toArray();

        $campos = $this->Campos($ugs, $this->todasUnidadesUser);
        $this->crud->addFields($campos);

        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function colunas()
    {
        $colunas = [
            [
                'name' => 'cpf',
                'label' => 'CPF',
                'type' => 'text',
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhereRaw("translate(unaccent(users.cpf), '.-/','') like translate(unaccent('%".$searchTerm."%'), '.-/','')");
                },
            ],
            [
                'name' => 'name',
                'label' => 'Nome',
                'type' => 'text',
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere('users.name', 'ilike', "%" . utf8_encode(utf8_decode(strtoupper($searchTerm))) . "%");
                },
            ],
            [
                'name' => 'email',
                'label' => 'E-mail',
                'type' => 'email',
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere('users.email', 'ilike', "%" . utf8_encode(utf8_decode(strtoupper($searchTerm))) . "%");
                },
            ],
            [
                'name' => 'situacao',
                'label' => 'Situação',
                'type' => 'boolean',
                'options' => [0 => 'Inativo', 1 => 'Ativo'],
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    if (strtolower($searchTerm) == 'inativo') {
                        $query->orWhere('users.situacao', 0);
                    }

                    if (strtolower($searchTerm) == 'ativo') {
                        $query->orWhere('users.situacao', 1);
                    }
                }
            ],
            [
                'name' => 'getUGPrimaria',
                'label' => 'UG/UASG Padrão',
                'type' => 'model_function',
                'function_name' => 'getUGPrimaria', // the method in your Model
                'orderable' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere('unidades.codigo', 'like', "%" . utf8_encode(utf8_decode(strtoupper($searchTerm))) . "%");
                    $query->orWhere('unidades.nomeresumido', 'like', "%" . utf8_encode(utf8_decode(strtoupper($searchTerm))) . "%");
                },
            ],
            [ // n-n relationship (with pivot table)
                'label' => trans('backpack::permissionmanager.roles'), // Table column heading
                'type' => 'select_multiple',
                'name' => 'roles', // the method that defines the relationship in your Model
                'entity' => 'roles', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model' => config('permission.models.role'), // foreign key model
            ],
        ];

        return $colunas;
    }

    public function campos($ug, $unidade)
    {
        $campos = [
            [
                'name' => 'cpf',
                'label' => 'CPF',
                'type' => 'cpf_unidade',
                'tab' => 'Dados Pessoais',
            ],
            [
                'name' => 'name',
                'label' => 'Nome Completo',
                'type' => 'text',
                'tab' => 'Dados Pessoais',
                'attributes' => [
                    'onkeyup' => "maiuscula(this)",
                    'disabled' => 'disabled',
                    'class' => 'editavel form-control'
                ],
            ],
            [
                'name' => 'email',
                'label' => 'E-mail',
                'type' => 'email',
                'tab' => 'Dados Pessoais',
                'attributes' => [
                    'disabled' => 'disabled',
                    'class' => 'editavel form-control'
                  ],
            ],
            [
                'name' => 'situacao',
                'label' => "Situação",
                'type' => 'select_from_array',
                'options' => [1 => 'Ativo', 0 => 'Inativo'],
                'allows_null' => false,
                'tab' => 'Dados Pessoais',
                'attributes' => [
                    'disabled' => 'disabled',
                  ],
            ],
            [ // select2_from_array
                'name' => 'ugprimaria',
                'label' => 'UG/UASG Padrão',
                'type' => 'select2_from_array',
                'options' => $ug,
                'allows_null' => true,
                'tab' => 'Outros',
                'allows_multiple' => false, // OPTIONAL; needs you to cast this to array in your model;
                'attributes' => [
                    'disabled' => 'disabled',
                  ],
            ],
            [
                'label' => "UGs/UASGs",
                'type' =>  "select2_from_ajax_multiple_single",
                'name' => 'unidades2',
                'entity' => 'unidades2',
                'entity' => 'unidades',
                'attribute' => "codigo",
                'attribute2' => "nomeresumido",
                'process_results_template' => 'gescon.process_results_multiple_unidade',
                'model' => "App\Models\Unidade",
                'data_source' => url("api/unidade"),
                'placeholder' => "Selecione a(s) Unidade(s)",
                'minimum_input_length' => 2,
                'tab' => 'Outros',
                'pivot' => true,
                'attributes' => [
                        "disabled" => 'disabled'
                    ]
            ],
            [       // Select2Multiple = n-n relationship (with pivot table)
                'label' => 'Demais UGs/UASGs',
                'type' => 'select2_multiple',
                'name' => 'unidades', // the method that defines the relationship in your Model
                'entity' => 'unidades', // the method that defines the relationship in your Model
                'attribute' => 'codigo', // foreign key attribute that is shown to user
                'attribute2' => 'nomeresumido', // foreign key attribute that is shown to user
                'attribute_separator' => ' - ', // foreign key attribute that is shown to user
                'model' => "App\Models\Unidade", // foreign key model
                'allows_null' => true,
                'pivot' => true, // on create&update, do you need to add/delete pivot table entries?
                'select_all' => true,
                'tab' => 'Outros',
                'options' => (function ($query) use ($unidade) {
                    return $query->orderBy('codigo', 'ASC')
                        ->whereIn('id', $unidade)
                        ->get();
                }),
                'attributes' => [
                    'disabled' => 'disabled',
                  ],
            ],
            [       // Select2Multiple = n-n relationship (with pivot table)
                'label' => 'Grupos de Usuário',
                'type' => 'select2_multiple',
                'name' => 'roles', // the method that defines the relationship in your Model
                'entity' => 'roles', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model' => config('permission.models.role'), // foreign key model
                'allows_null' => true,
                'pivot' => true, // on create&update, do you need to add/delete pivot table entries?
                'select_all' => true,
                'tab' => 'Outros',
                'options' => function ($query) {
                    $userRoles = backpack_user()->getRoleNames()->toArray();
                    $query = $query->orderBy('name', 'ASC');

                    if (in_array('Administrador', $userRoles)) {
                        return $query->get();
                    }

                    if (in_array('Administrador Suporte', $userRoles)) {
                        return $query->whereIn('name', BackpackUser::getAdminSuporteRoles($this->crud->entry->id ?? ''))->get();
                    }

                    if (in_array('Administrador Unidade', $userRoles)) {
                        //$unidade = Unidade::find(session()->get('user_ug_id'));
                        if ($this->unidadeUser->exclusivo_gestao_atas === true ) {

                            return $query->whereIn('name', BackpackUser::getExclusivoGestaoAtasRoles($this->crud->entry->id ?? ''))->get();

                        }else{

                            return $query->whereIn('name', BackpackUser::getAdminUnidadeRoles($this->crud->entry->id ?? ''))->get();
                        }
                    }

                    return $query->whereNotIn('name', BackpackUser::getExcludedRoles())->get();
                },
                'attributes' => [
                    'disabled' => 'disabled',
                  ],
            ],
        ];

        return $campos;
    }

    public function store(StoreRequest $request)
    {
        $chars = '0123456789';
        $max = strlen($chars) - 1;
        $senha = "NOVA";
        for ($i = 0; $i < 4; $i++) {
            $senha .= $chars{mt_rand(0, $max)};
        }

        $request->request->set('password', bcrypt($senha));

        $dados = [
            'email' => $request->input('email'),
            'cpf' => $request->input('cpf'),
            'nome' => $request->input('name'),
            'senha' => $senha,
        ];
        
        $redirect_location = parent::storeCrud($request);
        $usuario = BackpackUser::where('cpf', '=', $dados['cpf'])->first();

        if ($usuario) {
            UserMailPasswordJob::dispatch($usuario, $dados);
        }

        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        $usuario = BackpackUser::where('cpf', '=', $request->input('cpf'))->first();
        $rolesExclude = BackpackUser::getExcludedRoles();

        /*  Retirando Adm de orgao e Adm de Unidade do array, 
            e pegando só Administrador e Administrador Suporte.
        */
         $filteredRoles = array_diff($rolesExclude, ['Administrador Órgão','Administrador Unidade']);

        //Busca roles do usuario a ser editado.
        $rolesUsuarioEditar = BackpackUser::getPermissionAdmin($request->id);
 
        //Busca roles doe o usuario logado e verifica permissao Administrador para editar
        $rolesUsuarioLogado= BackpackUser::getPermissionAdmin($this->usuarioLogadoCPF->id);

        //Verifica se usuario logado tem permissão acima de Adm de unidade.
        $permissaoAdmUsuarioLogado = array_intersect($rolesUsuarioLogado, ['Administrador Órgão']);
        $permissaoUsuarioEditar = array_intersect($rolesUsuarioEditar,  $filteredRoles);
 
        /*  Sem permissão para editar
            ('Administrador','Administrador Suporte',
            'Consulta Global','Acesso API') 
        */
        if($permissaoAdmUsuarioLogado && $permissaoUsuarioEditar){

            \Alert::error('Não é permitido a alteração de grupos de usuários com nível superior!')->flash();
            return redirect()->back();
        }

        //Se for somente Adm de suporte 
        $permissaoUsuarioLogadoAdmSuporte= array_intersect($rolesUsuarioLogado, ['Administrador Suporte']);
        $permissaoUsuarioSuporteEditarUnidade = array_intersect($rolesUsuarioEditar, ['Administrador Suporte', 'Administrador']);
 
        // Se usuario logado tem permissão para editar usuario nivel superior Adm de unidade
        if(!empty($permissaoUsuarioLogadoAdmSuporte) && !empty($permissaoUsuarioSuporteEditarUnidade)){

            \Alert::error('Não é permitido a alteração de grupos de usuários com nível superior!')->flash();
            return redirect()->back();
        }
        
        //Se for somente Adm de unidade só edita perfil inferio a Adm de unidade 
        $permissaoAdmUnidade = array_intersect($rolesUsuarioLogado, array_diff($rolesExclude, ['Administrador Unidade']));
        $permissaoUsuarioEditarUnidade = array_intersect($rolesUsuarioEditar,  array_diff($rolesExclude, ['Administrador Unidade']));
     
        if(empty($permissaoAdmUnidade) && !empty($permissaoUsuarioEditarUnidade)){

            \Alert::error('Não é permitido a alteração de grupos de usuários com nível superior!')->flash();
            return redirect()->back();
        }
      
        $roles = BackpackUser::getPermissionAdmin($request->id);
        //Verificar permissão gestor de atas
        $CodigoGestorAtas[] = array_key_first($roles);
        $validaPermissaoGestaoAtas = empty(array_diff(array_values($CodigoGestorAtas), array_values($request->roles)));
        $usuario = BackpackUser::where('cpf', '=', $request->input('cpf'))->first();
    

        if($this->unidadeUser->exclusivo_gestao_atas === true){

            if (!$validaPermissaoGestaoAtas && $usuario->cpf !== $this->usuarioLogadoCPF->cpf) {
                \Alert::error('Sem permissão para alterar este Usuário!')->flash();
                return redirect()->back();
            }
        }

        // caso a situação for inativa, limpa os campos de ugPrimaria, Unidades e permissões
        if ($request->input('situacao') == 0) { // 0 = false = inativo
            $request->request->set('ugprimaria', null);
            $request->request->set('unidades', null);
            $request->request->set('unidades2', null);
            $request->request->set('roles', null);
        }

        $redirect_location = parent::updateCrud($request);

        return $redirect_location;
    }

    public function ajaxunidade(Request $request){

        $usuario = BackpackUser::where('cpf', '=', $request->input('cpfunidade'))->first();
      
        $dados = ["empty" => true];

        if(!empty($usuario)){

            if($usuario->ugprimaria){
                $userUgPrimaria = (string)$usuario->ugprimaria;
                $unidadeUserPrimaria = $usuario->unidadeprimaria($userUgPrimaria);
                $unidadeUserEdit = ["$unidadeUserPrimaria->id" => $unidadeUserPrimaria->codigo . " - " . $unidadeUserPrimaria->nomeresumido];
                $validacaoEdicaoUg = in_array($userUgPrimaria, $this->todasUnidadesUser);
            }else{
                $validacaoEdicaoUg = [];
                $unidadeUserEdit = "";
            }
            $dados = ["id" => $usuario->id, "nome" => $usuario->name, "email" => $usuario->email, 
                        "validaUg" => $validacaoEdicaoUg, "unidadeUserEdit" => $unidadeUserEdit];
        }
        
        return json_encode($dados);
    }
}
