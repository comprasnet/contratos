<?php

namespace App\Http\Controllers\Admin;

use App\Models\CompraItem;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CompraItemRequest as StoreRequest;
use App\Http\Requests\CompraItemRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use Illuminate\Support\Facades\DB;
use Route;

/**
 * Class CompraItemCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class CompraItemCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        if (!backpack_user()->hasRole('Administrador')) {
            abort('403', config('app.erro_permissao'));
        }
        $compra_id = Route::current()->parameter('compra_id');

        $this->crud->setModel(CompraItem::class);
        $this->crud->setRoute(
            config('backpack.base.route_prefix')
            . '/admin/compra/' . $compra_id
            . "/compraitem"
        );

        $this->crud->setEntityNameStrings('compraitem', 'Itens da Compra');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */
        $this->crud->enableExportButtons();

        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');
        $this->crud->allowAccess('show');

        $this->crud->addClause('select', [
            DB::raw('tipo_compra.descricao'),
            // Tabela principal deve ser sempre a última da listagem!
            'compra_items.*'
        ]);
        $this->crud->addClause(
            'join',
            'compras',
            'compras.id',
            '=',
            'compra_items.compra_id'
        );
        $this->crud->addClause(
            'join',
            'unidades',
            'unidades.id',
            '=',
            'compras.unidade_origem_id'
        );
        $this->crud->addClause(
            'join',
            DB::raw('codigoitens tipo_compra'),
            DB::raw('tipo_compra.id'),
            '=',
            DB::raw("compra_items.tipo_item_id")
        );

        $this->crud->addClause('where', 'compra_id', '=', $compra_id);
        $this->crud->addClause('orderBy', 'numero');

        $this->adicionaColunas();
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    protected function adicionaColunas(): void
    {
        $this->adicionaColunaCompra();
        $this->adicionaColunaDescricao();
        $this->adicionaColunaQuantidadeTotal();
        $this->adicionaColunaNumero();
        $this->adicionaColunaAtaVigenciaInicio();
        $this->adicionaColunaAtaVigenciaFim();
        $this->adicionaColunaCriterioJulgamento();
        $this->adicionaColunaSituacao();
        $this->adicionaColunaCreatedAt();
        $this->adicionaColunaUpdatedAt();
    }

    protected function adicionaColunaCompra(): void
    {
        $this->crud->addColumn([
            'name' => 'descricao',
            'label' => 'Tipo Item',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true
        ]);
    }

    protected function adicionaColunaDescricao(): void
    {
        $this->crud->addColumn([
            'name' => 'descricaodetalhada',
            'label' => 'Descricao',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true
        ]);
    }

    protected function adicionaColunaQuantidadeTotal(): void
    {
        $this->crud->addColumn([
            'name' => 'qtd_total',
            'label' => 'Qtd Total',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true
        ]);
    }

    protected function adicionaColunaNumero(): void
    {
        $this->crud->addColumn([
            'name' => 'numero',
            'label' => 'Numero',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true
        ]);
    }

    private function adicionaColunaAtaVigenciaInicio()
    {
        $this->crud->addColumn([
            'name' => 'ata_vigencia_inicio',
            'label' => 'Vig. Início',
            'type' => 'date',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true
        ]);
    }

    private function adicionaColunaAtaVigenciaFim(): void
    {
        $this->crud->addColumn([
            'name' => 'ata_vigencia_fim',
            'label' => 'Vig. Fim',
            'type' => 'date',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true
        ]);
    }

    protected function adicionaColunaCriterioJulgamento(): void
    {
        $this->crud->addColumn([
            'name' => 'criterio_julgamento',
            'label' => 'Crit. Julg.',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true
        ]);
    }

    private function adicionaColunaSituacao(): void
    {
        $this->crud->addColumn([
            'name' => 'situacao',
            'label' => 'Situação',
            'type' => 'boolean',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
            'options' => [0 => 'Inativo', 1 => 'Ativo']
        ]);
    }

    protected function adicionaColunaCreatedAt(): void
    {
        $this->crud->addColumn([
            'name' => 'created_at',
            'label' => 'Criado em',
            'type' => 'datetime',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true
        ]);
    }

    protected function adicionaColunaUpdatedAt(): void
    {
        $this->crud->addColumn([
            'name' => 'updated_at',
            'label' => 'Atualizado em',
            'type' => 'datetime',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true
        ]);
    }
}
