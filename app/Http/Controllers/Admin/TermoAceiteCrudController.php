<?php

namespace App\Http\Controllers\Admin;

use App\Models\Codigoitem;
use App\Models\TermoAceite;
use Backpack\CRUD\app\Http\Controllers\CrudController;

use Backpack\CRUD\CrudPanel;

/**
 * Class TermoAceiteCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class TermoAceiteCrudController extends CrudController
{

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            if (!backpack_user()->hasRole('Administrador') && !backpack_user()->hasRole('Desenvolvedor')) {
                abort(403, config('app.erro_permissao'));
            }
            return $next($request);
        })->except(['aceitaTermo', 'termoAceite']);
    }

    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\TermoAceite');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/admin/termoacesso');
        $this->crud->setEntityNameStrings('Termos de aceite', 'Termos de aceites');
        $this->crud->orderBy('created_at', 'desc');
        $this->crud->allowAccess('show');
        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');
        $this->crud->enableExportButtons();

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $tipos = Codigoitem::whereHas('codigo', function ($e) {
            $e->where('descricao', 'Termo de Aceite');
        })->pluck('descricao', 'id')->toArray();

        $this->crud->addFilter(
            [
                'name' => 'tipo',
                'type' => 'select2',
                'label' => 'Tipo'
            ],
            $tipos,
            function ($value) {
                $this->crud->addClause('where', 'tipo_id', $value);
            }
        );

        $this->crud->addColumns([
            [
                'name' => 'user_id',
                'label' => 'Usuário',
                'type' => 'model_function',
                'function_name' => 'getUser',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'tipo_id',
                'label' => 'Tipo',
                'type' => 'model_function',
                'function_name' => 'getTipo',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'status_id',
                'label' => 'Status',
                'type' => 'model_function',
                'function_name' => 'getStatus',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'info',
                'label' => 'Outras Informações',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'created_at',
                'label' => 'Aceito em:',
                'type' => 'datetime',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ]
        ]);

    }

    public function show($id)
    {
        $content = parent::show($id);

        return $content;
    }

    public function aceitaTermo()
    {
        $user = backpack_auth()->user();

        $verificaTermo = TermoAceite::where('user_id', $user->id)
        ->whereHas('tipo', function ($e) {
            $e->where('descres', 'ACESSO');
        })
        ->first();

        if (!$verificaTermo) {
            $tipo = Codigoitem::where('descres', 'ACESSO')->first();
            TermoAceite::create(['user_id' => $user->id, 'tipo_id' => $tipo->id]);
        }

        session(['termoAcesso' => true]);

        return redirect()->to('/inicio');
    }

    public function termoAceite()
    {
        return session('termoAcesso') ? redirect()->back() : view('backpack::termo_acesso.termo_aceite');
    }
}
