<?php

namespace App\Http\Controllers\Admin;

use App\Models\JobUser;
use App\Models\Orgao;
use App\Repositories\Siorg;
use App\services\SIORG\Implementations\SiorgRest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SiorgCrudController extends CrudController
{
    private $orgao_id;
    private $codigo_siorg;

    public function __construct()
    {
        parent::__construct();
        $this->orgao_id = \Route::current()->parameter('orgao_id');
        $this->codigo_siorg = \Route::current()->parameter('codigo_siorg');
    }

    public function setup()
    {

        if (!backpack_user()->hasRole('Administrador') and
            !backpack_user()->hasRole('Administrador Órgão') and
            !backpack_user()->hasRole('Administrador Suporte')) {
            abort('403', 'Você não tem permissão para acessar essa página.');
        }

        $this->crud->setModel('App\Models\Siorg');
        $this->crud->setRoute(config('backpack.base.route_prefix') . "/admin/orgao/$this->orgao_id/siorg/$this->codigo_siorg/lista");
        $this->crud->setEntityNameStrings("Estrutura Organizacional", "Estruturas Organizacionais");
        $this->crud->addClause('leftjoin', 'endereco_siorg', 'endereco_siorg.codigo_siorg', '=', 'siorg.codigo');
        $this->crud->addClause('leftjoin', 'contato_siorg', 'contato_siorg.codigo_siorg', '=', 'siorg.codigo');
        $this->crud->addClause('leftjoin', 'siorg as siorg_pai', 'siorg_pai.codigo', '=', 'siorg.codigo_pai');
        $this->crud->addClause('where', 'siorg.codigo_principal', '=', $this->codigo_siorg);
        $this->crud->addClause('orderByRaw', 'CAST(siorg.codigo AS INTEGER) asc');
        $this->crud->addClause('select', [
            'siorg.codigo as codigo_siorg_principal',
            'siorg.denominacao as siorg_nome',
            'siorg.sigla as siorg_sigla',
            'siorg.categoria as siorg_categoria',
            'siorg.nivel_normatizacao as siorg_nivel_normatizacao',
            'siorg.esfera as siorg_esfera',
            'siorg.poder as siorg_poder',
            'siorg.subnatureza_juridica as siorg_subnatureza_juridica',
            'siorg.cnpj as siorg_cnpj',
            'siorg.competencia as siorg_competencia',
            'siorg.finalidade as siorg_finalidade',
            'siorg.missao as siorg_missao',
            'siorg.updated_at as siorg_atualizado_em',
            'endereco_siorg.*',
            'contato_siorg.*',
            DB::raw("CONCAT(siorg_pai.codigo ,' - ', siorg_pai.denominacao) as denominacao_codigo_pai")
        ]);

        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');
        $this->crud->denyAccess('show');
        $this->crud->enableExportButtons();

        $orgao = Orgao::where('id', $this->orgao_id)->first()->nome;
        $this->crud->orgao = $orgao;

        $this->crud->addButton(
            'top',
            'atualizar_siorg',
            'view',
            'crud::buttons.atualizar_siorg'
        );

        $colunas = $this->Colunas();
        $this->crud->addColumns($colunas);

    }


    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function Colunas()
    {
        $columns = [
            $this->siorgColumns(),
            $this->enderecoColumns(),
            $this->contatoColumns()
        ];

        return array_merge(...$columns);
    }

    public function updateEstruturaOrganizacional()
    {
        $jobs = JobUser::where('job_user_type', 'App\Models\Siorg')
            ->where('job_user_id', $this->codigo_siorg)->count();

        if ($jobs > 0) {
            \Alert::info('A atualização para este órgão ainda está em andamento.')->flash();
            return redirect("/admin/orgao");
        }

        $rest = new SiorgRest($this->codigo_siorg);

        $estrutura = $rest->getUnidadeOrganizacionalEstruturaCompleta();

        if(isset($estrutura['error'])) {
            \Alert::error($estrutura['error'])->flash();
            Log::info($estrutura['error']);
            return redirect("/admin/orgao/$this->orgao_id/siorg/$this->codigo_siorg/lista");
        }

        if ($estrutura['servico']['codigoErro'] != 0) {
            \Alert::error($estrutura['servico']['mensagem'])->flash();
            Log::info($estrutura['servico']['mensagem']);
            return redirect("/admin/orgao/$this->orgao_id/siorg/$this->codigo_siorg/lista");
        }

        $siorgRepository = new Siorg();

        $siorgRepository->updateUnidadeOrganizacionalEstrutura($estrutura, $this->codigo_siorg);

        \Alert::info('Estou buscando a Estrutura. Por favor volte em alguns minutos!')->flash();
        return redirect("/admin/orgao");
    }

    private function siorgColumns()
    {
        $columns = [
            [
                'name' => 'codigo_siorg_principal',
                'label' => 'Código Siorg',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'denominacao_codigo_pai',
                'label' => 'Unidade Pai',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'siorg_nome',
                'label' => 'Nome',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'siorg_sigla',
                'label' => 'Sigla',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'siorg_categoria',
                'label' => 'Categoria',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'siorg_nivel_normatizacao',
                'label' => 'Nível de Normatização',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'siorg_atualizado_em',
                'label' => 'Atualizado Em',
                'type' => 'datetime',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'siorg_esfera',
                'label' => 'Esfera',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'siorg_poder',
                'label' => 'Poder',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'siorg_natureza_juridica',
                'label' => 'Natureza Jurídica',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'siorg_subnatureza_juridica',
                'label' => 'Subnatureza Jurídica',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'siorg_cnpj',
                'label' => 'CNPJ',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'siorg_competencia',
                'label' => 'Competência',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'siorg_finalidade',
                'label' => 'Finalidade',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'siorg_missao',
                'label' => 'Missão',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
        ];

        return $columns;
    }

    private function enderecoColumns()
    {
        $columns = [
            [
                'name' => 'logradouro',
                'label' => 'Logradouro',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'cep',
                'label' => 'CEP',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'numero',
                'label' => 'Número',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'complemento',
                'label' => 'Complemento',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'bairro',
                'label' => 'Bairro',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'municipio',
                'label' => 'Município',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'uf',
                'label' => 'UF',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'horario_funcionamento',
                'label' => 'Horário',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'tipo_endereco',
                'label' => 'Tipo de Endereço',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
        ];

        return $columns;
    }

    private function contatoColumns()
    {
        $columns = [
            [
                'name' => 'email',
                'label' => 'Email',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'telefone',
                'label' => 'Telefone',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
            [
                'name' => 'site',
                'label' => 'Site',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
        ];

        return $columns;
    }
}
