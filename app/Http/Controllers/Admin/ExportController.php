<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Painel\OrcamentarioController;
use App\Models\Apropriacao;
//use App\Models\Permission;
//use App\Models\Role;
//use App\Models\Unidade;
use App\Models\Contrato;
use App\Models\Empenho;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ExportController extends Controller
{

    public function downloadListaTodosContratos(Request $request, $type)
    {
        $filtro = null;
        if ($request->input()) {
            $filtro = $request->input();
        }
        $modelo = new Contrato();
        $dados = $modelo->buscaListaTodosContratos($filtro);
        $dados = $dados->toArray();
        $data = [];
        foreach ($dados as $dado){
            $valores_replace=[
                'data_assinatura' => implode('/',array_reverse(explode('-',$dado['data_assinatura']))),
                'data_publicacao' => implode('/',array_reverse(explode('-',$dado['data_publicacao']))),
                'vigencia_inicio' => implode('/',array_reverse(explode('-',$dado['vigencia_inicio']))),
                'vigencia_fim' => implode('/',array_reverse(explode('-',$dado['vigencia_fim']))),
                'valor_inicial' => number_format($dado['valor_inicial'],2,',','.'),
                'valor_global' => number_format($dado['valor_global'],2,',','.'),
                'num_parcelas' => number_format($dado['num_parcelas'],0),
                'valor_parcela' => number_format($dado['valor_parcela'],2,',','.'),
                'valor_acumulado' => number_format($dado['valor_acumulado'],2,',','.'),
                'situacao' => $dado['situacao'],
            ];
            $data[]=array_replace($dado,$valores_replace);
        }

        return Excel::create('todos_contratos_'. date('YmdHis'), function ($excel) use ($data) {
            $excel->sheet('lista', function ($sheet) use ($data) {
                $sheet->fromArray($data);
            });
        })->export($type);
    }

    public function downloadListaContratosOrgao(Request $request, $type)
    {
        $filtro = null;
        if ($request->input()) {
            $filtro = $request->input();
        }

        $modelo = new Contrato();
        $dados = $modelo->buscaListaContratosOrgao($filtro);

        if(!is_array($dados)){
            $dados = $dados->toArray();
        }


        $data = [];

        foreach ($dados as $dado){

            if( !is_array($dado) ){
                // transformar o objeto que chegou, em array
                $dado = array(
                    'orgao' => $dado->orgao,
                    'unidade' => $dado->unidade,
                    'receita_despesa' => $dado->receita_despesa,
                    'unidades_requisitantes' => $dado->unidades_requisitantes,
                    'numero' => $dado->numero,
                    'fornecedor_codigo' => $dado->fornecedor_codigo,
                    'fornecedor_nome' => $dado->fornecedor_nome,
                    'tipo' => $dado->tipo,
                    'categoria' => $dado->categoria,
                    'subcategoria' => $dado->subcategoria,
                    'processo' => $dado->processo,
                    'objeto' => $dado->objeto,
                    'info_complementar' => $dado->info_complementar,
                    'modalidade' => $dado->modalidade,
                    'licitacao_numero' => $dado->licitacao_numero,
                    'data_assinatura' => $dado->data_assinatura,
                    'data_publicacao' => $dado->data_publicacao,
                    'vigencia_inicio' => $dado->vigencia_inicio,
                    'vigencia_fim' => $dado->vigencia_fim,
                    'valor_inicial' => $dado->valor_inicial,
                    'valor_global' => $dado->valor_global,
                    'num_parcelas' => $dado->num_parcelas,
                    'valor_parcela' => $dado->valor_parcela,
                    'valor_acumulado' => $dado->valor_acumulado,
                    'situacao' => $dado->situacao,
                    'prorrogavel' => $dado->prorrogavel,
                    'autoridade_signataria' => $dado->autoridade_signataria,
                    'qulificacao_da_autoridade' => $dado->qualificacao_da_autoridade
                );
            }

            $valores_replace=[
                'data_assinatura' => implode('/',array_reverse(explode('-',$dado['data_assinatura']))),
                'data_publicacao' => implode('/',array_reverse(explode('-',$dado['data_publicacao']))),
                'vigencia_inicio' => implode('/',array_reverse(explode('-',$dado['vigencia_inicio']))),
                'vigencia_fim' => implode('/',array_reverse(explode('-',$dado['vigencia_fim']))),
                'valor_inicial' => number_format($dado['valor_inicial'],2,',','.'),
                'valor_global' => number_format($dado['valor_global'],2,',','.'),
                'num_parcelas' => number_format($dado['num_parcelas'],0),
                'valor_parcela' => number_format($dado['valor_parcela'],2,',','.'),
                'valor_acumulado' => number_format($dado['valor_acumulado'],2,',','.'),
                'situacao' => $dado['situacao'],
            ];

            $data[]=array_replace($dado,$valores_replace);

        }

        return Excel::create('contratos_orgao_'. date('YmdHis'), function ($excel) use ($data) {
            $excel->sheet('lista', function ($sheet) use ($data) {
                $sheet->fromArray($data);
            });
        })->export($type);
    }

    public function downloadListaContratosUg(Request $request, $type)
    {
        $filtro = null;
        if ($request->input()) {
            $filtro = $request->input();
        }
        $modelo = new Contrato();
        $dados = $modelo->buscaListaContratosUg($filtro);
        $data = [];
        foreach ($dados as $dado){
            if( !is_array($dado) ){
                // transformar o objeto que chegou, em array 
                $dado = array(
                    'orgao' => $dado->orgao,
                    'unidade' => $dado->unidade,
                    'receita_despesa' => $dado->receita_despesa,
                    'unidades_requisitantes' => $dado->unidades_requisitantes,
                    'numero' => $dado->numero,
                    'fornecedor_codigo' => $dado->fornecedor_codigo,
                    'fornecedor_nome' => $dado->fornecedor_nome,
                    'tipo' => $dado->tipo,
                    'categoria' => $dado->categoria,
                    'subcategoria' => $dado->subcategoria,
                    'processo' => $dado->processo,
                    'objeto' => $dado->objeto,
                    'info_complementar' => $dado->info_complementar,
                    'modalidade' => $dado->modalidade,
                    'licitacao_numero' => $dado->licitacao_numero,
                    'data_assinatura' => $dado->data_assinatura,
                    'data_publicacao' => $dado->data_publicacao,
                    'vigencia_inicio' => $dado->vigencia_inicio,
                    'vigencia_fim' => $dado->vigencia_fim,
                    'valor_inicial' => $dado->valor_inicial,
                    'valor_global' => $dado->valor_global,
                    'num_parcelas' => $dado->num_parcelas,
                    'valor_parcela' => $dado->valor_parcela,
                    'valor_acumulado' => $dado->valor_acumulado,
                    'situacao' => $dado->situacao,
                    'prorrogavel' => $dado->prorrogavel,
                    'autoridade_signataria' => $dado->autoridade_signataria,
                    'qualificacao_da_autoridade' => $dado->qualificacao_da_autoridade
                );
            }
            $valores_replace=[
                'data_assinatura' => implode('/',array_reverse(explode('-',$dado['data_assinatura']))),
                'data_publicacao' => implode('/',array_reverse(explode('-',$dado['data_publicacao']))),
                'vigencia_inicio' => implode('/',array_reverse(explode('-',$dado['vigencia_inicio']))),
                'vigencia_fim' => implode('/',array_reverse(explode('-',$dado['vigencia_fim']))),
                'valor_inicial' => number_format($dado['valor_inicial'],2,',','.'),
                'valor_global' => number_format($dado['valor_global'],2,',','.'),
                'num_parcelas' => number_format($dado['num_parcelas'],0),
                'valor_parcela' => number_format($dado['valor_parcela'],2,',','.'),
                'valor_acumulado' => number_format($dado['valor_acumulado'],2,',','.'),
                'situacao' => $dado['situacao'],
            ];
            $data[]=array_replace($dado,$valores_replace);
        }
        return Excel::create('contratos_ug_'. date('YmdHis'), function ($excel) use ($data) {
            $excel->sheet('lista', function ($sheet) use ($data) {
                $sheet->fromArray($data);
            });
        })->export($type);
    }

    public function downloadapropriacao(Request $request, $type)
    {
        $modelo = new Apropriacao();
        $apropriacao = $modelo->retornaListagem();

        $data = $apropriacao->toArray();

        return Excel::create('apropriacao_'. date('YmdHis'), function ($excel) use ($data) {
            $excel->sheet('lista', function ($sheet) use ($data) {
                $sheet->fromArray($data);
            });
        })->export($type);
    }

    public function downloadExecucaoPorEmpenho(Request $request, $type)
    {

        $modelo = new Empenho();
        $dados = $modelo->retornaDadosEmpenhosGroupUgArray();
        $totais = $modelo->retornaDadosEmpenhosSumArray();

        $totais_linha = [];
        foreach ($totais as $total){
            $totais_linha[] = [
                'nome' => 'Total',
                'empenhado' => $total['empenhado'],
                'aliquidar' => $total['aliquidar'],
                'liquidado' => $total['liquidado'],
                'pago' => $total['pago'],
            ];
        }
        $data = array_merge($dados,$totais_linha);

        return Excel::create('ExecucaoPorEmpenho_'. date('YmdHis'), function ($excel) use ($data) {
            $excel->sheet('lista', function ($sheet) use ($data) {
                $sheet->fromArray($data);
            });
        })->export($type);
    }

    public function downloadPlanilhaTerceirizados(Request $request){
        
        $filePath = public_path('../docs/arquivos/gestaoContratual/meusContratos/terceirizados/Modelo_Terceirizados_-_Versao_Final.xlsm');
    
        return response()->download($filePath);
    }

}
