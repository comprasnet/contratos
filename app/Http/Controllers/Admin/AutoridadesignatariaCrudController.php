<?php

namespace App\Http\Controllers\Admin;

use App\Models\Autoridadesignataria;
use App\Models\Unidade;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\AutoridadesignatariaRequest as StoreRequest;
use App\Http\Requests\AutoridadesignatariaRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;


/**
 * Class AutoridadesignatariaCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class AutoridadesignatariaCrudController extends CrudController
{
    public function setup()
    {
        $arrayAtivoInativo = [true => 'Ativa', false => 'Inativa'];
        $arrayTitularSubstituta = [true => 'Titular', false => 'Substituta'];

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $unidade_id = \Route::current()->parameter('unidade_id');
        $unidade = Unidade::where('id', '=', $unidade_id)->first();
        if (!$unidade) {
            abort('403', config('app.erro_permissao'));
        }

        $this->crud->setModel('App\Models\Autoridadesignataria');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/admin/unidade/' . $unidade_id . '/autoridadesignataria');
        $this->crud->setEntityNameStrings('Autoridade Signatária', 'Autoridades Signatárias');
        $this->crud->addClause('where', 'unidade_id', '=', $unidade_id);    // garantir que apenas as autoridades da unidade sejam listadas
        $this->crud->addClause('orderby', 'ativo', 1);
        $this->crud->addClause('orderby', 'titular', 1);
        $this->crud->addClause('orderby', 'autoridade_signataria', 'ASC');
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $colunas = $this->Colunas();
        $campos = $this->Campos($unidade, $arrayAtivoInativo, $arrayTitularSubstituta);
        $this->crud->addFields($campos);

        // add asterisk for fields that are required in AutoridadesignatariaRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
        $this->crud->removeButton('delete', 'line');
        $this->crud->addButtonFromView('line', 'delete', 'deleteautoridadesignataria', 'end');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function Campos($unidade, $arrayAtivoInativo, $arrayTitularSubstituta)
    {

        $campos = [
            [   // Hidden
                'name' => 'unidade_id',
                'type' => 'hidden',
                'default' => $unidade->id,
            ],
            [
                'name' => 'autoridade_signataria',
                'label' => 'Nome da Autoridade Signatária', // Table column heading
                'type' => 'text',
            ],
            [
                'name' => 'cargo_autoridade_signataria',
                'label' => 'Cargo da Autoridade Signatária', // Table column heading
                'type' => 'text',
            ],
            [ // select_from_array
                'name' => 'titular',
                'label' => "Titular / Substituta",
                'type' => 'select2_from_array',
                'options' => $arrayTitularSubstituta,
                'allows_null' => false,
                'allows_multiple' => false, // OPTIONAL; needs you to cast this to array in your model;
            ],
            [ // select_from_array
                'name' => 'ativo',
                'label' => "Ativa / Inativa",
                'type' => 'select2_from_array',
                'options' => $arrayAtivoInativo,
                'allows_null' => false,
                'allows_multiple' => false, // OPTIONAL; needs you to cast this to array in your model;
            ],

        ];

        return $campos;
    }

    public function Colunas()
    {
        $this->crud->addColumn(
            [
                'name' => 'autoridade_signataria',
                'label' => 'Nome da Autoridade Signatária',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ]
        );
        $this->crud->addColumn(
            [
                'name' => 'cargo_autoridade_signataria',
                'label' => 'Cargo da Autoridade Signatária',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ]
        );
        $this->crud->addColumn(
            [
                'name' => 'getTitular',
                'label' => 'Titular / Substituta', // Table column heading
                'type' => 'model_function',
                'function_name' => 'getTitular', // the method in your Model
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
        );
        $this->crud->addColumn(
            [
                'name' => 'getAtivo',
                'label' => 'Ativa / Inativa', // Table column heading
                'type' => 'model_function',
                'function_name' => 'getAtivo', // the method in your Model
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
        );
    }
}
