<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Api\PNCP\ContratoControllerPNCP;
use App\Http\Controllers\Api\PNCP\DocumentoContratoController;
use App\Http\Controllers\Api\PNCP\DocumentoTermoContratoController;
use App\Http\Controllers\Api\PNCP\TermoContratoController;
use App\Http\Controllers\Api\PNCP\UsuarioController;
use App\Http\Traits\Formatador;
use App\Http\Traits\MinutaEmpenhoTrait;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Empenho\CompraSiasgCrudController;
use App\Http\Traits\EnviaPncpTrait;
use App\Http\Traits\LogTrait;
use App\Models\Codigoitem;
use App\Models\Contrato;
use App\Models\Contratohistorico;
use App\Models\EnviaDadosPncp;
use App\Models\MinutaEmpenho;
use App\services\ErrosPNCP\TrataErrosContratoTermoService;
use Exception;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class EnviaPNCPController extends Controller
{
    use EnviaPncpTrait, MinutaEmpenhoTrait, LogTrait, Formatador;

    protected $cc;
    protected $cscc;
    protected $dcc;
    protected $dtcc;
    protected $tcc;
    protected $tiposContratoNegados;

    # Método responsável por realizar todas as ações apertando o botão 'Enviar Todos'
    public function enviaPNCP()
    {
        $this->insereContratos();
        $this->atualizaContratos();
        $this->excluirContratos();

        //pode ganhar uma rotina propria
        $this->enviaArquivosPNCP();
        return redirect('admin/pncp');
    }

    public function enviarContratoTermosPncpKernel()
    {
        $this->insereContratos();
    }

    public function retificarContratoPncpKernel()
    {
        $this->atualizaContratos();
    }

    public function removerContratoPncpKernel()
    {
        $this->excluirContratos();
    }

    public function enviarArquivoContratoPncpKernel()
    {
        $this->enviaArquivosPNCP();
    }

    public function enviaArquivosPNCP()
    {
        $this->enviaArquivos();
    }

    public function enviaUnidade($key)
    {
        $enviaDadosPncp = EnviaDadosPncp::where('id', $key)
            ->where('pncpable_type', Contratohistorico::class)
            ->whereHas('status', function ($ci) {
                $ci->whereIn('descres', ['INCPEN', 'RETPEN', 'ARQPEN', 'EXCPEN']);
            })
            ->first();

        if ($enviaDadosPncp) {
            # Em desenvolvimento e homologação, atualiza o login no PNCP para poder realizar as ações
            # evitando que precise sempre clicar no botão de autenticação
            if (config('app.app_amb') != 'Ambiente Produção') {
                $this->loginPNCP(false);
            }
            $status = $enviaDadosPncp->status->descres;
            $isTermo = Contrato::isTermoDeContrato($enviaDadosPncp->tipo_contrato);
            if ($isTermo) {
                if (!TrataErrosContratoTermoService::errosContratosPncp($key)) {
                    \Alert::error('Não há contrato enviado para este termo')->flash();
                    return redirect('admin/pncp');
                }
            }
            try {
                if ($enviaDadosPncp->pncpable_type == Contratohistorico::class) {
                    switch ($status) {
                        case 'INCPEN':
                        case 'ANALISE':
                            ($isTermo) ?
                                $this->insereTermoContrato($enviaDadosPncp) :
                                $this->insereContrato($enviaDadosPncp, $this->cc, $this->dcc, $this->dtcc, $this->cscc);
                            break;
                        case 'RETPEN':
                            ($isTermo) ?
                                $this->atualizaTermoContrato($enviaDadosPncp) :
                                $this->atualizaContrato($enviaDadosPncp);
                            break;
                        case 'ARQPEN':
                            $this->enviaContratoArquivos($enviaDadosPncp, $this->dcc, $this->dtcc);
                            break;
                        case 'EXCPEN':
                            ($isTermo) ?
                                $this->excluiTermoContrato($enviaDadosPncp) :
                                $this->excluiContrato($enviaDadosPncp);
                            break;
                        default:
                            break;
                    }
                }
            } catch (Exception $e) {
                // Log::error($e);
            }
        } else {
            \Alert::error('Situação ou tipo inválidos para enviar ao PNCP.')->flash();
        }
        return redirect('admin/pncp');
    }

    # Para realizar qualquer ação no PNCP, é necessário realizar o login
    public function loginPNCP($redirect = true)
    {
        $uc = new UsuarioController();
        $uc->login();
        if ($redirect) {
            return redirect('admin/pncp');
        }
    }

    public function preencheLinksArquivoEmpenho()
    {
        //Log::channel('schedule')->info("Sincronizando arquivos");
        DB::beginTransaction();
        try {
            $cc = new ContratoControllerPNCP();
            $situacao = $this->recuperarIDSituacao('SUCESSO');
            // Codigoitem::whereHas('codigo', function ($q) {
            //     $q->where('descricao', 'Situação Envia Dados PNCP');
            // })->where('descres', 'SUCESSO')->first()->id;

            $situacaoARQ = $this->recuperarIDSituacao('ARQPEN');
            // Codigoitem::whereHas('codigo', function ($q) {
            //     $q->where('descricao', 'Situação Envia Dados PNCP');
            // })->where('descres', 'ARQPEN')->first()->id;

            DB::table('envia_dados_pncp')->where([
                ['pncpable_type', MinutaEmpenho::class],
                ['linkArquivoEmpenho', null],
                ['link_pncp', '<>', null],
            ])
                ->chunkById(100, function ($envia) use ($cc, $situacao, $situacaoARQ) {
                    foreach ($envia as $e) {
                        $linkArquivo = @$cc->consultarContratoUrlMontada($e->link_pncp . '/arquivos')[0]["url"];
                        if ($linkArquivo !== null) {
                            DB::table('envia_dados_pncp')
                                ->where('id', $e->id)
                                ->update(['linkArquivoEmpenho' => $linkArquivo]);
                            if ($e->situacao == $situacaoARQ) {
                                DB::table('envia_dados_pncp')
                                    ->where('id', $e->id)
                                    ->update(['situacao' => $situacao]);
                            }
                        }
                    }
                });
            DB::commit();
        } catch (Exception $e) {
            //Log::error($e);
            DB::rollback();
        }
        return redirect('admin/pncp');
    }

    # Busca os arquivos assinados a partir do Kernel
    public function buscaArquivoEmpenhoAssinadoKernel()
    {
        $minutaEmpenhoPendenteAssinatura = $this->getMinutaEmpenhoAssinaturaPendenteSubstitutivoDeContrato();
        $contratoControllerPNCP = new ContratoControllerPNCP();
        $idTipoArquivoMinutaEmpenho = Codigoitem::where([
            'descres' => 'ARQ_MINUTA_EMPENHO',
            'descricao' => 'Arquivo minuta de empenho'
        ])->first()->id;
        if ($minutaEmpenhoPendenteAssinatura->count() > 0) {
            # Realizar login no PNCP
            $this->realizarLoginNovo();
        }
        foreach ($minutaEmpenhoPendenteAssinatura as $enviaDadosPNCP) {
            try {
                $minuta = $enviaDadosPNCP->pncpable;
                if (!empty($minuta)) {
                    # Se a minuta estiver emitida
                    # Criamos o job para poder consultar no SIAFI se está assinado
                    if ($minuta->situacao->descricao == 'EMPENHO EMITIDO') {
                        $arquivoMinutaEmpenho = $this->gerarPdfNovo($minuta, $idTipoArquivoMinutaEmpenho);
                        if ($arquivoMinutaEmpenho->sucesso) {
                            $this->incluirMinutaEmpenhoPNCP($enviaDadosPNCP, $contratoControllerPNCP);
                            continue;
                        }

                        if (!$arquivoMinutaEmpenho->sucesso) {
                            $enviaDadosPNCP->retorno_pncp = $arquivoMinutaEmpenho->mensagem;
                            $enviaDadosPNCP->save();
                        }
                    }
                }
            } catch (Exception $ex) {
                if (strpos(
                        $ex->getMessage(),
                        "Inclusão não permitida pois já existe contrato para os parâmetros informados"
                    ) !== false) {

                    $enviaDadosPNCPErro = EnviaDadosPncp::where('pncpable_id', $enviaDadosPNCP->pncpable_id)
                        ->where('pncpable_type', MinutaEmpenho::class)
                        ->first();

                    $enviaDadosPNCPErro->retorno_pncp = $ex->getMessage();
                    $enviaDadosPNCPErro->save();

                    continue;
                }
                $this->inserirLogCustomizado('minuta-empenho', 'error', $ex);
            }
        }
    }

    public function buscaArquivoEmpenhoAssinadoDiferente14133()
    {
        $cacheKey = 'codigoitens_ARQ_MINUTA_EMPENHO';
        $idTipoArquivoMinutaEmpenho = Cache::get($cacheKey);

        $minutaEmpenhoPendenteAssinatura =
            MinutaEmpenho::getMinutaEmpenhoSubstitutivoContratoDiferenteLeiQuatorzeUmTresTres();

        if (is_null($idTipoArquivoMinutaEmpenho)) {
            $idTipoArquivoMinutaEmpenho = Codigoitem::where([
                'descres' => 'ARQ_MINUTA_EMPENHO',
                'descricao' => 'Arquivo minuta de empenho'
            ])->first()->id;
            Cache::put($cacheKey, $idTipoArquivoMinutaEmpenho, 60 * 5);
        }

        $this->inserirLogDebug('teste-pdf', 'info', 'iniciando busca de minuta de empenho');
        $this->inserirLogDebug('teste-pdf', 'info', $minutaEmpenhoPendenteAssinatura->count());

        foreach ($minutaEmpenhoPendenteAssinatura as $minuta) {
            $this->inserirLogDebug('teste-pdf', 'info', 'inicio id minuta: ' . $minuta->id);
            try {
                $this->gerarPdfNovo($minuta, $idTipoArquivoMinutaEmpenho);
            } catch (Exception $ex) {
                $this->inserirLogCustomizado('minuta-empenho', 'error', $ex, 'id minuta: ' . $minuta);
                $this->inserirLogCustomizado('teste-pdf', 'error', $ex, 'id minuta: ' . $minuta);
            }
            $this->inserirLogDebug('teste-pdf', 'info', 'fim    id minuta: ' . $minuta->id);
        }
        $this->inserirLogDebug('teste-pdf', 'info', 'finalizando busca de minuta de empenho');
    }

    public function __construct()
    {
        //otimizar
        $this->cc = new ContratoControllerPNCP();
        $this->cscc = new CompraSiasgCrudController();
        $this->dcc = new DocumentoContratoController();
        $this->dtcc = new DocumentoTermoContratoController();
        $this->tcc = new TermoContratoController();
        # Recupera os contratos negados
        $this->tiposContratoNegados = config('api-pncp.tipos_contrato_negados');
        // ['Termo Rescisão', 'Termo de Rescisão','Termo Aditivo', 'Termo Apostilamento', 'Termo de Apostilamento', 'Credenciamento', 'Outros'];
    }

    # Insere os contratos no PNCP
    public function insereContratos()
    {
        $envia_contratos_pncp = EnviaDadosPncp::whereHas('status', function ($ci) {
            $ci->where('descres', 'INCPEN');
        })->where('pncpable_type', Contratohistorico::class)->oldest()->get();
        foreach ($envia_contratos_pncp as $contrato_pncp) {
            try {
                if (Contrato::isTermoDeContrato($contrato_pncp->tipo_contrato)) {
                    $this->insereTermoContrato($contrato_pncp);
                } else {
                    $this->insereContrato($contrato_pncp, $this->cc, $this->dcc, $this->dtcc, $this->cscc);
                }
            } catch (Exception $e) {
                $this->inserirLogPncp($e, __METHOD__, __LINE__);
            }
        }
    }

    public function atualizaContratos()
    {
        $envia_contratos_pncp = EnviaDadosPncp::whereHas('status', function ($ci) {
            $ci->where('descres', 'RETPEN');
        })
            ->where('pncpable_type', Contratohistorico::class)
            ->get();
        foreach ($envia_contratos_pncp as $contrato_pncp) {
            #Verifica se o Empenho está assinado
            $validarEmpenhoAssinado = $this->validarEnviarContratoTipoEmpenho($contrato_pncp);
            # Se não tiver sido enviado, não continua o envio
            if (empty($validarEmpenhoAssinado->sequencialPNCP)) {
                continue;
            }
            try {
                if (Contrato::isTermoDeContrato($contrato_pncp->tipo_contrato)) {
                    $this->atualizaTermoContrato($contrato_pncp);
                } else {
                    $this->atualizaContrato($contrato_pncp);
                }
            } catch (Exception $e) {
                $this->inserirLogPncp($e, __METHOD__, __LINE__);
            }
        }
    }

    public function excluirContratos()
    {
        $envia_contratos_pncp = EnviaDadosPncp::whereHas('status', function ($ci) {
            $ci->where('descres', 'EXCPEN');
        })->where('pncpable_type', Contratohistorico::class)->get();
        foreach ($envia_contratos_pncp as $contrato_pncp) {
            try {
                (!in_array($contrato_pncp->tipo_contrato, $this->tiposContratoNegados)) ?
                    $this->excluiContrato($contrato_pncp) : $this->excluiTermoContrato($contrato_pncp);
            } catch (Exception $e) {
                $this->inserirLogPncp($e, __METHOD__, __LINE__);
            }
        }
    }

    # Envia os arquivos pendentes de arquivo
    public function enviaArquivos()
    {
        $envia_arquivos_pncp = EnviaDadosPncp::whereHas('status', function ($ci) {
            $ci->where('descres', 'ARQPEN');
        })
            ->whereIn('pncpable_type', [Contratohistorico::class, MinutaEmpenho::class])
            ->get();

        foreach ($envia_arquivos_pncp as $envia_pncp) {
            try {
                if ($envia_pncp->pncpable_type == Contratohistorico::class) {
                    $this->enviaContratoArquivos($envia_pncp, $this->dcc, $this->dtcc);
                } else if ($envia_pncp->pncpable_type == MinutaEmpenho::class) {
                    $this->enviaEmpenhoArquivo($envia_pncp, $this->dcc);
                }
            } catch (Exception $e) {
                $this->inserirLogCustomizado('pncp_debug', 'error', $e);
            }
        }
    }

    public function atualizaEmpenhos()
    {
        $envia_contratos_pncp = EnviaDadosPncp::whereHas('status', function ($ci) {
            $ci->where('descres', 'RETPEN');
        })->where('pncpable_type', MinutaEmpenho::class)->get();

        foreach ($envia_contratos_pncp as $contrato_pncp) {
            try {
                $this->atualizaEmpenho($contrato_pncp);
            } catch (Exception $e) {
                $this->inserirLogCustomizado('pncp_debug', 'error', $e);
            }
        }
    }

    public function atualizaContrato(EnviaDadosPncp $linhaPncpDoContrato)
    {
        $validarEmpenhoAssinado = $this->validarEnviarContratoTipoEmpenho($linhaPncpDoContrato);
        if (empty($validarEmpenhoAssinado->sequencialPNCP)) {
            \Alert::error('Não existe Minuta de Empenho assinada para este contrato.')->flash();
            return redirect()->back()->withInput();
        }
        try {
            $this->atualizarContratoPNCP($validarEmpenhoAssinado, $this->cc, $this->dcc, $this->dtcc, $this->cscc);
        } catch (Exception $e) {
            $this->registraErroNaTabelaEnviaDadosPncp($linhaPncpDoContrato, true, $e);
            throw $e;
        }
    }

    public function excluiContrato(EnviaDadosPncp $linhaPncpDoContrato)
    {
        $mensagemDeErroPadrao = "Erro ao tentar remover um contrato. ";
        try {
            $cnpjOrgao = $this->formatarCnpjOrgao($linhaPncpDoContrato->sequencialPNCP);
            $dadosDaLinhaPncpDoContratoParaLog =
                "ID do Contrato: {$linhaPncpDoContrato->contrato_id}. " .
                "ID do Contrato histórico: {$linhaPncpDoContrato->pncpable_id}). " .
                "Linha PNCP: {$linhaPncpDoContrato->id}";
            if (empty($cnpjOrgao)) {
                $errorMessage = $mensagemDeErroPadrao .
                    "Não foi possível obter o CNPJ do orgão. " .
                    $dadosDaLinhaPncpDoContratoParaLog;
                # Se não encontrou o CNPJ, há algum problema no registro. Neste caso, mantém o registro Em análise.
                $this->alteraSituacaoParaAnalise(
                    $linhaPncpDoContrato,
                    $errorMessage
                );
                $this->inserirLogPncp($errorMessage, __METHOD__, __LINE__);
                return false;
            }
            $ano = $this->formatarAno($linhaPncpDoContrato->sequencialPNCP);
            if (empty($ano)) {
                $errorMessage = $mensagemDeErroPadrao . "Não foi possível obter o ano. " .
                    $dadosDaLinhaPncpDoContratoParaLog;
                # Se não encontrou o ano, há algum problema no registro. Neste caso, mantém o registro Em análise.
                # O 'if' abaixo é feito em função do legado: qdo tipo do contrato era alterado, a alteração não
                # refletia na tabela envia_dados_pncp.
                # Neste caso, não move para análise, pois a issue #1503 fará a correção.
                $contrato = Contratohistorico::find($linhaPncpDoContrato->pncpable_id);
                if ($contrato->tipo->descricao == $linhaPncpDoContrato->situacao) {
                    $this->alteraSituacaoParaAnalise($linhaPncpDoContrato, $errorMessage);
                } else {
                    # Só vai entrar aqui quando tiver o problema de legado.
                    # Neste caso, registra o erro, mas mantém a situação.
                    $this->registraErroNaTabelaEnviaDadosPncp($linhaPncpDoContrato, true,
                        'O tipo do contrato na tabela envia_dados_pncp ' .
                        'está diferente do tipo do contrato no cadastro.');
                }
                $this->inserirLogPncp($errorMessage, __METHOD__, __LINE__);
                return false;
            }
            $sequencialPNCP = $this->formatarSequencialPncp($linhaPncpDoContrato->sequencialPNCP);
            if (empty($sequencialPNCP)) {
                $errorMessage = $mensagemDeErroPadrao .
                    "Não foi possível obter o número sequencial PNCP do Contrato. " .
                    $dadosDaLinhaPncpDoContratoParaLog;
                $this->inserirLogPncp($errorMessage, __METHOD__, __LINE__);
                $this->alteraSituacaoParaAnalise($linhaPncpDoContrato, $errorMessage);
                return false;
            }
            $contratoExcluidoPNCP = $this->cc->excluirContrato($cnpjOrgao, $ano, $sequencialPNCP);
            $this->trataRetornoExclusao($linhaPncpDoContrato, $contratoExcluidoPNCP);
        } catch (Exception $e) {
            # Se está tentando excluir o termo (EXCPEN) e ele já não existe no PNCP, define a situação como EXCLUIDO.
            if (preg_match('/Contrato não cadastrado/', $e->getMessage())) {
                $linhaPncpDoContrato->situacao = $this->recuperarIDSituacao('EXCLUIDO');
                $linhaPncpDoContrato->retorno_pncp = "";
                $linhaPncpDoContrato->save();
                return false;
            }
            $this->inserirLogPncp($e, __METHOD__, __LINE__);
            $this->registraErroNaTabelaEnviaDadosPncp($linhaPncpDoContrato, true, $e);
            #throw $e;
        }

    }

    public function atualizaTermoContrato(EnviaDadosPncp $linhaDoTermoDeContrato)
    {
        $mensagemDeErroPadrao = "Erro ao tentar atualizar(retificar) um termo de contrato. ";
        try {
            $termo = Contratohistorico::find($linhaDoTermoDeContrato->pncpable_id);
            if (empty($termo)) {
                $errorMessage = $mensagemDeErroPadrao .
                    "Termo de contrato de ID {$linhaDoTermoDeContrato->pncpable_id} " .
                    "não encontrado na tabela contratohistorico. " .
                    "ID do Contrato: {$linhaDoTermoDeContrato->contrato_id}. " .
                    "Linha PNCP: {$linhaDoTermoDeContrato->id}.";
                $this->inserirLogPncp($errorMessage, __METHOD__, __LINE__);
                $this->alteraSituacaoParaAnalise($linhaDoTermoDeContrato, $errorMessage);
                return false;
            }
            # Se o termo está em elaboração, não dá continuidade no envio
            if ($termo->elaboracao === true) {
                return false;
            }
            # Busca o contrato 'pai' do termo
            $linhaPncpContratoPaiDoTermo = EnviaDadosPncp::withTrashed()
                ->where([
                    ['contrato_id', $termo->contrato_id],
                    ['pncpable_type', Contratohistorico::class]
                ])
                ->whereIn('tipo_contrato', config('api-pncp.tipos_contrato_aceitos_pncp'))
                ->oldest()
                ->first();
            # Se o registro do contrato 'pai' foi excluído, exclui também o registro do termo
            if (empty($linhaPncpContratoPaiDoTermo) || $linhaPncpContratoPaiDoTermo->deleted_at !== null) {
                $linhaDoTermoDeContrato->delete();
                $errorMessage = "A linha PNCP {$linhaDoTermoDeContrato->id} " .
                    "referente ao termo de contrato " .
                    "{$linhaDoTermoDeContrato->pncpable_id} foi excluída " .
                    "pois o contrato pai {$linhaDoTermoDeContrato->contrato_id} " .
                    "também está excluído na tabela envia_dados_pncp.";
                $this->inserirLogPncp($errorMessage, __METHOD__, __LINE__);
                return null;
            }
            $dadosContratoPaiParaLogDeErro = "ID da linha PNPC do Contrato 'pai': ".
                "{$linhaPncpContratoPaiDoTermo->id}. " .
                "ID da linha PNCP do termo: {$linhaDoTermoDeContrato->id}. " .
                "ID do Contrato: {$termo->contrato_id}. " .
                "ID do Termo: {$termo->id}";
            if (empty($linhaPncpContratoPaiDoTermo->sequencialPNCP)) {
                $errorMessage = $mensagemDeErroPadrao . "Não foi possível obter o sequencial do contrato 'pai'. " .
                    $dadosContratoPaiParaLogDeErro;
                $this->inserirLogPncp($errorMessage, __METHOD__, __LINE__);
                $this->alteraSituacaoParaAnalise($linhaDoTermoDeContrato, $errorMessage);
                return false;
            }
            $cnpjOrgao = $this->formatarCnpjOrgao($linhaPncpContratoPaiDoTermo->sequencialPNCP);
            if (empty($cnpjOrgao)) {
                $errorMessage = $mensagemDeErroPadrao .
                    "Não foi possível obter o CNPJ do órgão através do sequencialPNPC do contrato 'pai'. " .
                    $dadosContratoPaiParaLogDeErro;
                $this->inserirLogPncp($errorMessage, __METHOD__, __LINE__);
                $this->alteraSituacaoParaAnalise($linhaDoTermoDeContrato, $errorMessage);
                return false;
            }
            $ano = $this->formatarAno($linhaPncpContratoPaiDoTermo->sequencialPNCP);
            if (empty($ano)) {
                $errorMessage = $mensagemDeErroPadrao .
                    "Não foi possível obter o ano através do sequencialPNPC do contrato 'pai'. " .
                    $dadosContratoPaiParaLogDeErro;
                $this->inserirLogPncp($errorMessage, __METHOD__, __LINE__);
                $this->alteraSituacaoParaAnalise($linhaDoTermoDeContrato, $errorMessage);
                return false;
            }
            $sequencialPNCP = $this->formatarSequencialPncp($linhaPncpContratoPaiDoTermo->sequencialPNCP);
            if (empty($sequencialPNCP)) {
                $errorMessage = $mensagemDeErroPadrao .
                    "Não foi possível obter o número sequencial PNCP do Contrato 'pai'. " .
                    $dadosContratoPaiParaLogDeErro;
                $this->inserirLogPncp($errorMessage, __METHOD__, __LINE__);
                $this->alteraSituacaoParaAnalise($linhaDoTermoDeContrato, $errorMessage);
                return false;
            }
            $termoRetificado = $this->tcc->retificarTermoContrato(
                $termo, $cnpjOrgao, $ano, $sequencialPNCP, $linhaDoTermoDeContrato);
            if ($termoRetificado === false) {
                return false;
            }
            $enviaArquivos = $this->trataRetorno($linhaDoTermoDeContrato, $termoRetificado, true, '', $this->cc);
            if ($enviaArquivos) {
                $this->enviaContratoArquivos(
                    $linhaDoTermoDeContrato,
                    $this->dcc,
                    $this->dtcc
                );
            }
            return true;
        } catch (Exception $e) {
            $errorMessage = $e->getMessage() .
                "\r\nLinha PNCP: " . $linhaDoTermoDeContrato .
                "\r\nTermo de contrato: " . $termo;
            $this->inserirLogPncp($errorMessage, __METHOD__, __LINE__);
            $this->registraErroNaTabelaEnviaDadosPncp($linhaDoTermoDeContrato, true, $e);
            return false;
        }
    }

    public function excluiTermoContrato(EnviaDadosPncp $linhaDoTermoDeContrato)
    {
        $mensagemDeErroPadrao = "Erro ao tentar remover um termo de contrato. ";
        try {
            $envia_pncp = EnviaDadosPncp::find($linhaDoTermoDeContrato->id);
            $linhaPncpContratoPaiDoTermo = EnviaDadosPncp::where([
                ['contrato_id', '=', $envia_pncp->contrato_id],
                ['pncpable_type', Contratohistorico::class],
            ])
                ->whereIn('tipo_contrato', config('api-pncp.tipos_contrato_aceitos_pncp'))
                ->oldest()
                ->first();
            if (empty($linhaPncpContratoPaiDoTermo) || $linhaPncpContratoPaiDoTermo->deleted_at !== null) {
                $linhaDoTermoDeContrato->delete();
                $errorMessage = "A linha PNCP {$linhaDoTermoDeContrato->id} " .
                    "referente ao termo de contrato {$linhaDoTermoDeContrato->pncpable_id} foi excluída " .
                    "pois o contrato pai {$linhaDoTermoDeContrato->contrato_id} " .
                    "também está excluído na tabela envia_dados_pncp.";
                $this->inserirLogPncp($errorMessage, __METHOD__, __LINE__);
                return null;
            }
            $dadosContratoPaiParaLog = "ID da linha PNPC do Contrato 'pai': {$linhaPncpContratoPaiDoTermo->id}. " .
                "ID da linha PNCP do termo: {$linhaDoTermoDeContrato->id}. " .
                "ID do Contrato: {$linhaDoTermoDeContrato->contrato_id}. " .
                "ID do Termo: {$linhaDoTermoDeContrato->pncpable_id}";
            if (empty($linhaPncpContratoPaiDoTermo->sequencialPNCP)) {
                $errorMessage = $mensagemDeErroPadrao .
                    "Não foi possível obter o sequencial do Contrato 'pai'. " .
                    $dadosContratoPaiParaLog;
                $this->inserirLogPncp($errorMessage, __METHOD__, __LINE__);
                $this->alteraSituacaoParaAnalise($linhaDoTermoDeContrato, $errorMessage);
                return false;
            }
            $cnpjOrgao = $this->formatarCnpjOrgao($linhaPncpContratoPaiDoTermo->sequencialPNCP);
            if (empty($cnpjOrgao)) {
                $errorMessage = $mensagemDeErroPadrao .
                    "Não foi possível obter o CNPJ do orgão. " .
                    $dadosContratoPaiParaLog;
                # Se não encontrou o CNPJ, há algum problema no registro. Neste caso, mantém o registro Em análise.
                $this->alteraSituacaoParaAnalise($linhaDoTermoDeContrato, $errorMessage);
                $this->inserirLogPncp($errorMessage, __METHOD__, __LINE__);
                return false;
            }
            $ano = $this->formatarAno($linhaPncpContratoPaiDoTermo->sequencialPNCP);
            if (empty($ano)) {
                $errorMessage = $mensagemDeErroPadrao .
                    "Não foi possível obter o ano. " .
                    $dadosContratoPaiParaLog;
                # Se não encontrou o ano, há algum problema no registro. Neste caso, mantém o registro Em análise.
                $this->alteraSituacaoParaAnalise($linhaDoTermoDeContrato, $errorMessage);
                $this->inserirLogPncp($errorMessage, __METHOD__, __LINE__);
                return false;
            }
            $sequencialPNCP = $this->formatarSequencialPncp($linhaPncpContratoPaiDoTermo->sequencialPNCP);
            if (empty($sequencialPNCP)) {
                $errorMessage = $mensagemDeErroPadrao .
                    "Não foi possível obter o número sequencial PNCP do Contrato 'pai'. " .
                    $dadosContratoPaiParaLog;
                $this->inserirLogPncp($errorMessage, __METHOD__, __LINE__);
                $this->alteraSituacaoParaAnalise($linhaDoTermoDeContrato, $errorMessage);
                return false;
            }
            $contratoExcluidoPNCP = $this->tcc->excluirTermoContrato(
                $cnpjOrgao, $ano, $sequencialPNCP, $linhaDoTermoDeContrato->sequencialPNCP);
            $this->trataRetornoExclusao($envia_pncp, $contratoExcluidoPNCP);
        } catch (Exception $e) {
            # Se está tentando excluir o termo (EXCPEN) e ele já não existe no PNCP, define a situação como EXCLUIDO.
            if (preg_match('/Termo de Contrato não cadastrado/', $e->getMessage())) {
                $envia_pncp->situacao = $this->recuperarIDSituacao('EXCLUIDO');
                $envia_pncp->retorno_pncp = "";
                $envia_pncp->save();
                return false;
            }
            $this->inserirLogPncp($e, __METHOD__, __LINE__);
            $this->registraErroNaTabelaEnviaDadosPncp($envia_pncp, true, $e);
        }
    }

    public function insereTermoContrato(EnviaDadosPncp $linhaDoTermoDeContrato)
    {
        $this->inserirTermoContratoPNCP($linhaDoTermoDeContrato, $this->tcc, $this->dcc, $this->dtcc);
    }
}
