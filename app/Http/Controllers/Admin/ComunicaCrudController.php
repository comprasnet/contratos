<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ComunicaRequest as StoreRequest;
use App\Http\Requests\ComunicaRequest as UpdateRequest;
use App\Jobs\NotificaUsuarioComunicaJob;
use App\Models\BackpackUser;
use App\Models\Comunica;
use App\Models\Orgao;
use App\Models\Unidade;
use App\Repositories\Comunica as Repo;
use App\Repositories\OrgaoSuperior as RepoOrgaoSuperior;
use App\Repositories\Unidade as RepoUnidade;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\CrudPanel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Spatie\Permission\Models\Role;

/**
 * Class ComunicaCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ComunicaCrudController extends CrudController
{

    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        if (backpack_user()->hasRole('Administrador Suporte')) {
            abort('403', config('app.erro_permissao'));
        }

        $this->crud->setModel('App\Models\Comunica');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/admin/comunica');
        $this->crud->setEntityNameStrings('Comunica', 'Comunica');
        $this->crud->enableExportButtons();
        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');
        $this->crud->allowAccess('show');

        (backpack_user()->can('comunica_inserir')) ? $this->crud->allowAccess('create') : null;
        (backpack_user()->can('comunica_editar')) ? $this->crud->allowAccess('update') : null;
        (backpack_user()->can('comunica_deletar')) ? $this->crud->allowAccess('delete') : null;

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $this->crud->addColumns($this->Colunas());
        $this->crud->addFields($this->Campos());

        // add asterisk for fields that are required in ComunicaRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        $redirect_location = parent::storeCrud($request);

        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        $redirect_location = parent::updateCrud($request);

        return $redirect_location;
    }

    public function show($id)
    {

        $content = parent::show($id);

        $this->crud->removeColumns([
            'orgao_id',
            'unidade_id',
            'role_id',
            'mensagem',
            'situacao',
        ]);

        return $content;
    }

    /**
     * Retorna array de colunas a serem exibidas no grid de listagem
     *
     * @return array
     */
    public function Colunas()
    {
        return [
            [
                'name' => 'created_at',
                'label' => 'Data de Crição',
                'type' => 'datetime',
                'format' => 'd/m/Y H:i:s',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true
            ],
            [
                'name' => 'agendamento',
                'label' => 'Data de Envio',
                'type' => 'datetime',
                'format' => 'd/m/Y 22:00',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true
            ],
            [
                'name' => 'getOrgao',
                'label' => 'Órgão',
                'type' => 'model_function',
                'function_name' => 'getOrgao',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true
            ],
            [
                'name' => 'getUnidade',
                'label' => 'Unidade Gestora', // Table column heading
                'type' => 'model_function',
                'function_name' => 'getUnidade', // the method in your Model
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'getGrupo',
                'label' => 'Grupo', // Table column heading
                'type' => 'model_function',
                'function_name' => 'getGrupo', // the method in your Model
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'assunto',
                'label' => 'Assunto', // Table column heading
                'type' => 'text',
                'limit' => 1000,
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'getMensagem',
                'label' => 'Mensagem', // Table column heading
                'type' => 'model_function',
                'function_name' => 'getMensagem', // the method in your Model
                'limit' => 1000,
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'anexos',
                'label' => 'Anexos', // Table column heading
                'type' => 'upload_multiple',
                'disk' => 'local',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'getSituacao',
                'label' => 'Situação', // Table column heading
                'type' => 'model_function',
                'function_name' => 'getSituacao', // the method in your Model
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ]
        ];
    }

    /**
     * Retorna array dos campos para exibição no form
     *
     * @return array
     */
    public function Campos()
    {

        $repo = new Repo();
        $repoOrgaosuperior = new RepoOrgaoSuperior();
        $repoUnidade = new RepoUnidade();

        $grupos = Role::pluck('name', 'id')->toArray();
        $orgaos = $repoOrgaosuperior->getOrgaosParaCombo();
        $unidades = $repoUnidade->getUnidadesParaComboPorPerfil(session('user_orgao_id'), session('user_ug_id'), $repo->getOrgao(), $repo->getUnidade());

        $campos = array();

        if (backpack_user()->hasRole('Administrador')) {
            $campos[] = [
                'name' => 'orgao_id',
                'label' => "Órgão",
                'type' => 'select2_from_array',
                'options' => $orgaos,
                'placeholder' => "Todos",
                'allows_null' => true
            ];
        } else {
            $campos[] = [
                'name' => 'orgao_id',
                'label' => "Órgão",
                'type' => 'hidden',
                'value' => session('user_orgao_id'),
                'allows_null' => true
            ];
        }

        $campos[] = [
            'label' => "Unidade",
            'type' => "select2_from_ajax",
            'name' => 'unidade_id',
            'model' => "App\Models\Unidade",
            'attribute' => "codigo",
            'entity' => 'unidade',
            'process_results_template' => 'gescon.process_results_unidade_codigo_descricao',
            'data_source' => url("api/unidade"),
            'placeholder' => "Selecione...",
            'minimum_input_length' => 2
        ];

        $campos[] = [
            'name' => 'role_id',
            'label' => "Grupos",
            'type' => 'select2_from_array',
            'options' => $grupos,
            'placeholder' => "Todos",
            'allows_null' => true
        ];

        $campos[] = [
            'name' => 'assunto',
            'label' => "Assunto",
            'type' => 'text',
            'minimum_input_length' => 2,
            'attributes' => [
                'onkeyup' => "maiuscula(this)"
            ]
        ];

        $campos[] = [
            'name' => 'mensagem',
            'label' => "Mensagem",
            'type' => 'ckeditor',
            'attributes' => [
                'id' => "mensagem"
            ]
        ];

        $campos[] = [
            'name' => 'anexos',
            'label' => 'Anexos',
            'type' => 'upload_multiple',
            'upload' => true,
            'disk' => 'local'
        ];

        $campos[] = [ // select_from_array
            'name' => 'situacao',
            'label' => "Situação",
            'type' => 'select_from_array',
            'options' => $this->retornaSituacoesComunica(),
            'allows_null' => true,
            'attributes' => [
                'onchange' => "habilitarCampoAgendamentoComunica(this)"
            ]
        ];

        $newDateTime = Carbon::now()->addHour()->format('Y-m-d H:i');

        $campos[] = [ // select_from_array
            'name' => 'agendamento',
            'label' => "Agendar para o envio",
            'type' => 'datetime',
            'default' => $newDateTime,
            'ico_help' => 'Alterar o valor caso o status seja Enviar por Agendamento!',
            'attributes' => [
                'id' => "agendamento",
                'readonly' => 'readonly',
            ],
            'format' => 'Y-m-d H:i'
        ];

        $campos[] = [
            'name' => 'parent.title',
            'label' => 'Aviso',
            'type' => 'text',
            'default' => 'Caso não seja selecionado o Orgão ou Unidade ou Grupo, a mensagem será encaminhada para todos os usuários do sistema.',
            'attributes' => [
                'disabled' => 'disabled',
            ]
        ];

        return $campos;
    }

    /**
     * Retorna array com todas as situações da comunicação
     *
     * @return array
     * @author Anderson Sathler <asathler@gmail.com>
     */
    private function retornaSituacoesComunica()
    {

        $repo = new Repo();
        return $repo->getSituacoes();
    }

    /**
     * Retorna Orgão conforme $idUnidade
     *
     * @param $idUnidade
     * @return string
     * @author Anderson Sathler <asathler@gmail.com>
     */
    private function retornaOrgaoPorUnidade($idUnidade)
    {

        $repo = new Repo();
        return $repo->retornaOrgaoPorUnidade($idUnidade);
    }

    /**
     * Dispara notificação para os usuários correspondentes, conforme $comunica
     *
     * @param Comunica $comunica
     */
    public function disparaNotificacao()
    {
        $comunicaProntoEnvio = Comunica::whereIn("situacao", ['P', 'A'])
            ->whereNull("deleted_at")
            ->get();

        foreach ($comunicaProntoEnvio as $comunica) {

            //Se for pronto para enviar
            if ($comunica->situacao == 'P') {
                $this->prepararEnvio($comunica);
            }

            if ($comunica->situacao == 'A') {

                $data = Carbon::createFromFormat('Y-m-d H:i:s', $comunica->agendamento);

                if ($data->lte(Carbon::now())) {
                    $this->prepararEnvio($comunica);
                }
            }
        }
    }

    private function prepararEnvio(Comunica $comunica)
    {
        $usuarios = BackpackUser::with('unidade');

        $comunica->situacao = 'E';
        $comunica->save();

        $orgao = $comunica->orgao_id;
        if (!is_null($orgao) && $orgao != '') {
            $usuarios->whereHas('unidade', function (Builder $query) use ($orgao) {
                $query->where('orgao_id', $orgao);
            });
        }

        $unidade = $comunica->unidade_id;
        if (!is_null($unidade) && $unidade != '') {
            $usuarios->where('ugprimaria', $unidade);
        }

        $users = $usuarios->get();

        foreach ($users as $user) {
            $notifica = $this->deveNotificarUsuario($user, $comunica->role_id);

            if ($notifica) {
                NotificaUsuarioComunicaJob::dispatch($comunica, $user)->onQueue('default');
            }
        }

        $comunica->hora_envio = Carbon::now();
        $comunica->save();
    }

    /**
     * Efetua validação para notificar ou não dado usuário
     *
     * @param BackpackUser $user
     * @param int $perfil
     * @return bool
     * @author Anderson Sathler <asathler@gmail.com>
     */
    private function deveNotificarUsuario(BackpackUser $user, $perfil = null)
    {
        $notifica = true;

        if ($perfil) {
            $role = Role::find($perfil);

            if (!$user->hasRole($role->name)) {
                $notifica = false;
            }
        }

        return $notifica;
    }
}
