<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ExecsfsituacaoRequest as StoreRequest;
use App\Http\Requests\ExecsfsituacaoRequest as UpdateRequest;
use App\Models\Execsfsituacao;
use App\Models\SfPco;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\CrudPanel;
use Illuminate\Support\Facades\DB;
use function backpack_user;
use function config;

// VALIDATION: change the requests to match your own file names if you need form validation

/**
 * Class ExecsfsituacaoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ExecsfsituacaoCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        if(!backpack_user()->hasRole('Administrador')){
            abort('403', config('app.erro_permissao'));
        }

        $this->crud->setModel('App\Models\Execsfsituacao');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/admin/situacaosiafi');
        $this->crud->setEntityNameStrings('Situação Siafi', 'Situações Siafi');

        $this->crud->enableExportButtons();
        $this->crud->addButtonFromModelFunction('line', 'camposVariaveis', 'camposVariaveis', 'end');
        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');
        $this->crud->allowAccess('show');



        (backpack_user()->can('situacaosiafi_inserir')) ? $this->crud->allowAccess('create') : null;
        (backpack_user()->can('situacaosiafi_editar')) ? $this->crud->allowAccess('update') : null;
        (backpack_user()->can('situacaosiafi_deletar')) ? $this->crud->allowAccess('delete') : null;



        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        $colunas = $this->Colunas();
        $this->crud->addColumns($colunas);

        $execsfsituacao = Execsfsituacao::select(DB::raw("CONCAT(codigo,' - ',descricao) AS nome"), 'id')
            ->where('status','=','true')
            ->where('aba','<>','DESPESA_ANULAR')
            ->orderBy('codigo', 'asc')
            ->pluck('nome', 'id')
            ->toArray();


        $campos = $this->Campos($execsfsituacao);

        $this->crud->addFields($campos);

        // add asterisk for fields that are required in ExecsfsituacaoRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function Colunas()
    {
        $colunas = [
            [
                'name' => 'codigo',
                'label' => 'Código',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'descricao',
                'label' => 'Descrição',
                'type' => 'text',
                'limit' => 1000,
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'getExecsfsituacao',
                'label' => 'Anula Situação', // Table column heading
                'type' => 'model_function',
                'function_name' => 'getExecsfsituacao', // the method in your Model
                'orderable' => true,
                'limit' => 1000,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'aba',
                'label' => 'Aba',
                'type' => 'select_from_array',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
                // optionally override the Yes/No texts
                'options' => config('app.abas')
            ],

            [
                'name' => 'afeta_custo',
                'label' => 'Afeta Custo?',
                'type' => 'boolean',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
                // optionally override the Yes/No texts
                'options' => [0 => 'Não', 1 => 'Sim']
            ],
            [
                'name' => 'permite_contrato',
                'label' => 'Contrato?',
                'type' => 'boolean',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'options' => [0 => 'Não', 1 => 'Sim']
            ],
            [
                'name' => 'darf',
                'label' => "Possui tipo DARF?",
                'type' => 'boolean',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'options' => [0 => 'Não', 1 => 'Sim']
            ],
            [
                'name' => 'darf_numerado',
                'label' => "Possui tipo DARF Numerado?",
                'type' => 'boolean',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'options' => [0 => 'Não', 1 => 'Sim']
            ],
            [
                'name' => 'darf_numerado_decomposto',
                'label' => "Possui tipo DARF Numerado Decomposto?",
                'type' => 'boolean',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'options' => [0 => 'Não', 1 => 'Sim']
            ],
            [
                'name' => 'status',
                'label' => 'Situação',
                'type' => 'boolean',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
                // optionally override the Yes/No texts
                'options' => [0 => 'Inativo', 1 => 'Ativo']
            ],
        ];

        return $colunas;

    }

    public function Campos($execsfsituacao)
    {

        $campos = [
            [ // select_from_array
                'name' => 'codigo',
                'label' => "Código",
                'type' => 'codigosituacaosiafi',
            ],
            [ // select_from_array
                'name' => 'descricao',
                'label' => "Descrição",
                'type' => 'text',
                'attributes' => [
                    'onkeyup' => "maiuscula(this)"
                ]
            ],
            [  // Select2
                'label' => "Tipo Situação",
                'type' => 'select2',
                'name' => 'tipo_situacao', // the db column for the foreign key
                'entity' => 'tipoSituacao', // the method that defines the relationship in your Model
                'attribute' => 'full_desc', // foreign key attribute that is shown to user
                'model' => "App\Models\Codigoitem", // foreign key model

                // optional
                'options'   => (function ($query) {
                    return $query->whereHas('codigo', function ($q){
                        $q->where('descricao','Tipos Situações DH SIAFI');
                    })->orderBy('descres', 'ASC')->get();
                }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
            ],
            [ // select_from_array
                'name' => 'execsfsituacao_id',
                'label' => "Anula Situação",
                'type' => 'select2_from_array',
                'options' => $execsfsituacao,
                'allows_null' => true,
//                'default' => 'one',
                // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
            ],
            [ // select_from_array
                'name' => 'aba',
                'label' => "Aba",
                'type' => 'select_from_array',
                'options' => config('app.abas'),
                'allows_null' => true,
            ],
            [       // Select2Multiple = n-n relationship (with pivot table)
                'label' => "Documentos",
                'type' => 'select2_multiple',
                'name' => 'tipodocs', // the method that defines the relationship in your Model
                'entity' => 'tipodocs', // the method that defines the relationship in your Model
                'attribute' => 'full_desc', // foreign key attribute that is shown to user
                'model' => "App\Models\Codigoitem", // foreign key model
                'pivot' => true, // on create&update, do you need to add/delete pivot table entries?
                // 'select_all' => true, // show Select All and Clear buttons?

                // optional
                'options'   => (function ($query) {
                    return $query->whereHas('codigo', function ($q){
                        $q->where('descricao','Tipos DH SIAFI');
                    })->orderBy('descres', 'ASC')->get();
                }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
            ],
            [ // select_from_array
                'name' => 'afeta_custo',
                'label' => "Afeta Custo?",
                'type' => 'radio',
                'options' => [1 => 'Sim', 0 => 'Não'],
                'inline' => true,
                'default' => 1
            ],
            [ // select_from_array
                'name' => 'permite_contrato',
                'label' => "Contrato?",
                'type' => 'radio',
                'options' => [1 => 'Sim', 0 => 'Não'],
                'inline' => true,
                'default' => 1
            ],
            [
                'name' => 'darf',
                'label' => "Possui tipo DARF?",
                'type' => 'radio',
                'options' => [1 => 'Sim', 0 => 'Não'],
                'inline' => true,
                'default' => 0
            ],
            [
                'name' => 'darf_numerado',
                'label' => "Possui tipo DARF Numerado?",
                'type' => 'radio',
                'options' => [1 => 'Sim', 0 => 'Não'],
                'inline' => true,
                'default' => 0
            ],
            [
                'name' => 'darf_numerado_decomposto',
                'label' => "Possui tipo DARF Numerado Decomposto?",
                'type' => 'radio',
                'options' => [1 => 'Sim', 0 => 'Não'],
                'inline' => true,
                'default' => 0
            ],
            [ // select_from_array
                'name' => 'status',
                'label' => "Situação",
                'type' => 'select_from_array',
                'options' => [1 => 'Ativo', 0 => 'Inativo'],
                'allows_null' => false,
            ],

        ];

        return $campos;
    }

    public function store(UpdateRequest $request)
    {
        // your additional operations before save here

        $request->request->set(
            'codigo',
            strtoupper($request->input('codigo'))
        );

        $request->request->set(
            'descricao',
            strtoupper($request->input('descricao'))
        );

        $request->request->set(
            'categoria_ddp',
            array_search($request->input('aba'), config('app.aba_x_categoria_ddp'))
        );

        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        $request->request->set(
            'codigo',
            strtoupper($request->input('codigo'))
        );

        $request->request->set(
            'descricao',
            strtoupper($request->input('descricao'))
        );

        $request->request->set(
            'categoria_ddp',
            array_search($request->input('aba'), config('app.aba_x_categoria_ddp'))
        );
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function show($id)
    {
        $content = parent::show($id);

        $this->crud->removeColumn('execsfsituacao_id');

        return $content;
    }
}
