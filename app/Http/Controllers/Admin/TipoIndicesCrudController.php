<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\TipoIndicesRequest as StoreRequest;
use App\Http\Requests\TipoIndicesRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class TipoIndicesCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class TipoIndicesCrudController extends CrudController
{
    public function setup()
    {
 
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\TipoIndices');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/admin/tipoindices');
        $this->crud->setEntityNameStrings('Tipos de índice', 'Tipos de índice');
        $this->crud->setTitle('Cadastro de Tipos de Índice', 'create');
        $this->crud->setHeading('Cadastro de Tipos de Índice', 'create');
        $this->crud->enableExportButtons();
        $this->crud->allowAccess('show');
        $this->crud->addButtonFromView('line', 'tipoindicecalcular', 'tipoindicecalcular', 'end');
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        $this->crud->setFromDb();
        $campos = $this->Campos();       
        $this->crud->addFields($campos);
        
        // add asterisk for fields that are required in TipoIndicesRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function Campos(){
        $fontes = [
            'bacen' => 'Bacen',
            'outras' => 'Outras'
        ];
        $forma = [
            'auto' => 'Automático',
            'personalizado' => 'Personalizado'
        ];
        $campos = [

            [
                'name' => 'codigo',
                'label' => 'Código',
                'type' => 'text',
                'attributes' => [
                    'required' => true,
                ]
            ],
            [
                'name' => 'nome',
                'label' => 'Nome',
                'type' => 'text',
                'attributes' => [
                    'required' => true,
                ]
            ],
            [
                'name' => 'descricao',
                'label' => 'Descrição',
                'type' => 'textarea',
                'attributes' => [
                    'required' => true,
                ]
            ],
            [ // select_from_array
                'name' => 'fonte',
                'label' => "Fonte",
                'type' => 'select_from_array',
                'options' => $fontes,
                'allows_null' => false,
                'wrapperAttributes' => [
                    'class' => 'form-group col-xs-12 col-md-6'
                ]
            ],
            [ // select_from_array
                'name' => 'forma_calculo',
                'label' => "Forma de cálculo",
                'type' => 'select_from_array',
                'options' => $forma,
                'allows_null' => false,
                'wrapperAttributes' => [
                    'class' => 'form-group col-xs-12 col-md-6'
                ]
            ],
        ];
        return $campos;
    }
    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
    /**
     * Display all rows in the database for this entity.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {

        $this->data['classes_controle_acesso_botoes'] = 'update delete';
        $this->crud->hasAccessOrFail('list');
        $this->crud->setOperation('list');

        $this->data['crud'] = $this->crud;
        $this->data['title'] = $this->crud->getTitle() ?? mb_ucfirst($this->crud->entity_name_plural);

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view('vendor.backpack.crud.calculadora_indices.list', $this->data);
    }
}
