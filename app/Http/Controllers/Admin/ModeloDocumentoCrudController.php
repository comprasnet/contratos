<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ModeloDocumentoRequest as StoreRequest;
use App\Http\Requests\ModeloDocumentoRequest as UpdateRequest;
use App\Models\Codigoitem;
use App\Models\RecomposicaoCusto\ModeloDocumento;
use Backpack\CRUD\CrudPanel;
use Illuminate\Http\Request;

/**
 * Class ModeloDocumentoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ModeloDocumentoCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        #171
        if (backpack_user()->hasRole('Administrador Suporte')) {
            abort('403', config('app.erro_permissao'));
        }

        $this->crud->setModel('App\Models\RecomposicaoCusto\ModeloDocumento');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/admin/modelodocumento');
        $this->crud->setEntityNameStrings('Modelo de Documento', 'Padrões de Minutas de Documento');
        $this->crud->setTitle('Cadastro de Modelos', 'create');
        $this->crud->setHeading('Cadastro de Modelos', 'create');
        $this->crud->enableExportButtons();
        $this->crud->allowAccess('show');


        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        //$this->crud->setFromDb();

        // add asterisk for fields that are required in ModeloDocumentoRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        /*
        |--------------------------------------------------------------------------
        | Configurar Campos do Formulário
        |--------------------------------------------------------------------------
        */
        $campos = $this->campos();
        $this->crud->addFields($campos);

        $colunas = $this->colunas();
        $this->crud->addColumns($colunas);
    }



    public function colunas(){
        return [
            [
                'name' => 'descricao',
                'label' => 'Descrição',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'getTipoDocumento',
                'label' => 'Tipo de Documento',
                'type' => 'model_function',
                'function_name' => 'getTipoDocumento',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,

            ],

            [
                'name' => 'ativo',
                'label' => 'Ativo',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'codigoitens_id',
                'label' => 'Tipo Documento',
                'type' => 'text',
                'orderable' => false,
                'visibleInTable' => false, // no point, since it's a large text
                'visibleInModal' => false, // would make the modal too big
                'visibleInExport' => false, // not important enough
                'visibleInShow' => false, // sure, why not
            ],

            [
                'name' => 'template',
                'label' => 'Template',
                'type' => 'view',
                'view' => 'backpack::crud.columns.show_modelo_documento',
                'orderable' => false,
                'visibleInTable' => false, // no point, since it's a large text
                'visibleInModal' => false, // would make the modal too big
                'visibleInExport' => false, // not important enough
                'visibleInShow' => true, // sure, why not
            ],

        ];
    }

    public function campos(){

//        $tipo_documento = Codigoitem::where('tipo_minuta', true)->get()->pluck('descricao','id')->toArray();
        $tipo_documento =  Codigoitem::whereHas('codigo', function ($q){ //inclusao para evitar da criação da coluna nova no Codigo Itens
            $q->where('descricao', '=', 'Tipos Documentos SEI');
        })->get()->pluck('descricao','id')->toArray();

        return [
            [
                'name' => 'descricao',
                'label' => "Descrição",
                'type' => 'text',
                'allows_null' => false,
            ],
            [
                'name' => 'codigoitens_id',
                'label' => "Tipo Documento",
                'type' => 'select2_from_array',
                'options' => $tipo_documento,
                'allows_null' => true,
            ],
            [   // CustomHTML
                'name' => 'botao_token',
                'type' => 'custom_html',
                'value' => '<a href="#" class="btn btn-primary" id="buscaCampo" style="margin-bottom:15px" data-toggle="modal" data-target="#myModal"><span><span class="ico-badge-militar"></span> Relacionar dados do contrato a este modelo de documento</span></a>'
            ],
            [   // CKEditor
                'name' => 'template',
                'label' => 'Template',
                'type' => 'ckeditor',
                // optional:
                'extra_plugins' => ['oembed', 'widget'],
                'options' => [
                'autoGrow_minHeight' => 200,
                'autoGrow_bottomSpace' => 50,
                ]
            ],
            [   // Checkbox
                'name' => 'ativo',
                'label' => 'Ativo',
                'type' => 'checkbox',
                'default'=>true,
            ],
        ];
    }


    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function getTemplate(Request $request){

        $modelo_id = $request->modelo_id;
        $modelo = ModeloDocumento::find($modelo_id);
        return json_encode(array('template' => $modelo->template));
    }
}
