<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Mail\CertificadoVencendoMail;
use App\Models\BackpackUser;
use App\Models\SfCertificado;
use Backpack\CRUD\CrudPanel;
use Illuminate\Support\Facades\Mail;

/**
 * Class SfcertificadoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class SfcertificadoController extends Controller
{
    public function verificaVencimentoCertificadoSiafi(){
        $certificado = SfCertificado::orderBy('id', 'DESC')->first();
        $emails = [];

        if($certificado->verificaVencimentoCertificadoSiafi()){
            $users = new BackpackUser();
            $usuariosAdministradores = $users->getAdmins();

            foreach ($usuariosAdministradores as $administradores){
                $emails[] = $administradores->email;
            }

            Mail::to($emails)->send(
                new CertificadoVencendoMail(
                    $certificado->verificaVencimentoCertificadoSiafi()
                )
            );
        }

    }
}
