<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\RotinaatualizaindicesbacenJobs;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\IndiceValoresRequest as StoreRequest;
use App\Http\Requests\IndiceValoresRequest as UpdateRequest;
use App\Models\TipoIndices;
use App\services\Indices\Implementations\ImportacaoDadosBacenWebserviceToLocalTableService;
use App\services\Indices\Implementations\ImportacaoIST;
use App\services\IPEA\Implementations\IpeaRest;


use Backpack\CRUD\CrudPanel;

/**
 * Class IndiceValoresCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class IndiceValoresCrudController extends CrudController
{
    public $codigo_indice;

    public function setup()
    {

        $this->codigo_indice = TipoIndices::where('fonte','outras')->pluck('nome', 'codigo');
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\IndiceValores');
        //$this->crud->setListView('backpack::crud.minutas.list'  );//nova list view
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/admin/indicevalores');        
        $this->crud->setEntityNameStrings('Valor Índice Econômico', 'Índices Econômicos');
        $this->crud->setTitle('Cadastro de Valor do Índice', 'create');
        $this->crud->setHeading('Cadastro de Valor do Índice', 'create');
        $this->crud->query = $this->crud->query->orderBy('ano', 'desc')->orderBy('tipo_indices_codigo','asc')->orderBy('mes','desc');
        $this->crud->enableExportButtons();

        $this->crud->allowAccess('show');
        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');

        if(backpack_user()->can('administrar_indice_economico') == true){
            $this->crud->allowAccess('create');
            $this->crud->allowAccess('update');
            $this->crud->allowAccess('delete');
        }

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        //$this->crud->setFromDb();
        $campos = $this->Campos();
        $colunas = $this->Colunas();

        $this->crud->addFields($campos);
        $this->crud->addColumns($colunas);

        // add asterisk for fields that are required in IndiceValoresRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');     
        
    }

    public function Colunas(){
        $colunas = [
            [
                'name'=>'IndiceNome',
                'label'=>'Índice',
                'type'=>'model_function',
                'function_name'=>'getIndice_Nome',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
                'searchLogic' => function ($query, $column, $searchTerm) {
                    $query->orWhereHas('TipoIndice', function($q) use($searchTerm){
                        $q->where('nome','like', '%' . $searchTerm . '%');
                    });
                }
            ],
            [
                'name'=>'mes',
                'label'=>'Mês',
                'orderable' => true,
            ],
            [
                'name'=>'ano',
                'label'=>'Ano',
                'orderable' => true,                
            ],
            
            [
                'name'=>'valor',
                'label'=>'Valor',
            ],
            [
                'name'=>'variacao',
                'label'=>'Variação',
            ],
            [
                'name'=>'acumulado_mes',
                'label'=>'Acumulado',
            ],
            [
                'name'=>'acumulado_12_meses',
                'label'=>'Acumulado 12 meses',
                'visibleInTable' => false
            ],
            [
                'name'  => 'getFonte()',
                'label' => 'Fonte',
                'type'  => 'model_function',
                'function_name' => 'getFonte',
                'searchLogic' => 'text'
            ],
            [
                'name'          => 'getForma_Calculo',
                'label'         => 'Forma de Cálculo', // Table column heading
                'type'          => 'model_function',
                'function_name' => 'getForma_Calculo', // the method in your Model
            ],
            [
                'name' => 'tipo_indices_codigo',
                'label' => 'Código do Índice',
            ],
        ];
        return $colunas;
    }

    public function Campos(){
       // $nome_indice = TipoIndices::where('codigo',$this->tipo_indice_codigo)->pluck('nome');
        $campos = [
            [
                'name' => 'tipo_indices_codigo',
                'label' => 'Código',
                'type' => 'select_from_array',
                'options' => $this->codigo_indice,
                'allows_null' => false,
                'wrapperAttributes' => [
                    'class' => 'form-group col-xs-12 col-md-6'
                ]
            ],
            [
                'name' => 'mes',
                'label' => 'Mês',
                'type' => 'number',
                'attributes' => [
                    'required' => true,
                    'min'=> 1,
                    'max' => 12,
                ]
            ],
            [
                'name' => 'ano',
                'label' => 'Ano',
                'type' => 'number',
                'attributes' => [
                    'required' => true,
                ]
            ],
            [
                'name' => 'valor',
                'label' => 'Valor',
                'type' => 'text',
                'attributes' => [
                    'required' => true,
                ]
            ],
            [
                'name' => 'variacao',
                'label' => 'Variação',
                'type' => 'text',
            ],
            [
                'name' => 'acumulado_mes',
                'label' => 'Acumulado no mês',
                'type' => 'text',

            ],
            [
                'name' => 'acumulado_12_meses',
                'label' => 'Acumulado 12 meses',
                'type' => 'text',

            ],

        ];
        return $campos;
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function index()
    {
        // $ipea = new IpeaRest();
        // $ipea->import();
        

        $this->data['classes_controle_acesso_botoes'] = 'update delete';
        $this->crud->hasAccessOrFail('list');
        $this->crud->setOperation('list');

        $this->data['crud'] = $this->crud;
        $this->data['title'] = $this->crud->getTitle() ?? mb_ucfirst($this->crud->entity_name_plural);

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view('vendor.backpack.crud.calculadora_indices.list', $this->data);
    }

    public function executaJobsRotinaAtualizaIndicesBacen()
    {
        RotinaatualizaindicesbacenJobs::dispatch();
    }
}
