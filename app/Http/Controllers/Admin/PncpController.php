<?php

namespace App\Http\Controllers\Admin;

use App\Models\Codigoitem;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\BuscaCodigoItens;
use App\Models\Contratohistorico;
use App\Models\EnviaDadosPncp;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Exception;

class PncpController extends CrudController
{
    use BuscaCodigoItens;

    public function setup()
    {
        #171
        if (backpack_user()->hasRole('Administrador Suporte')) {
            abort('403', config('app.erro_permissao'));
        }

        if (!backpack_user()->hasRole('Administrador') && !backpack_user()->hasRole('Desenvolvedor')) {
            abort('403', config('app.erro_permissao'));
        }

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
//        $this->crud->setModel(ContratoPublicacoes::class);
        $this->crud->setModel('App\Models\EnviaDadosPncp');
        $this->crud->setRoute(config('backpack.base.route_prefix')
            . "/admin/pncp");
        $this->crud->setEntityNameStrings('PNCP', 'PNCP');
        $this->crud->orderBy('updated_at', 'desc');
        $this->crud->addClause('select', 'envia_dados_pncp.*');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */
        $this->crud->enableExportButtons();
        if (backpack_user()->hasRole('Administrador'))
            $this->crud->addButtonFromView('line', 'envia_individual_pncp', 'envia_individual_pncp', 'beginning');

        backpack_user()->hasRole('Administrador') ? $this->crud->allowAccess('delete') : $this->crud->denyAccess('delete');
        backpack_user()->hasRole('Administrador') ? $this->crud->allowAccess('update') : $this->crud->denyAccess('update');
        $this->crud->denyAccess('create');
        $this->crud->allowAccess('show');


        // TODO: remove setFromDb() and manually define Fields and Columns
//        $this->crud->setFromDb();

        $this->crud->addButtonFromView('top', 'voltar', 'voltarcontrato', 'end');
        (backpack_user()->hasRole('Administrador')) ? $this->crud->addButtonFromView('top', 'Login', 'login_pncp', 'end') : null;
        (backpack_user()->hasRole('Administrador')) ? $this->crud->addButtonFromView('top', 'Preencher Links', 'busca_arquivo_empenho_pncp', 'end') : null;
        (backpack_user()->hasRole('Administrador')) ? $this->crud->addButtonFromView('top', 'Gerar Arquivos de Empenhos', 'gera_arquivos_empenhos', 'end') : null;
        (backpack_user()->hasRole('Administrador')) ? $this->crud->addButtonFromView('top', 'Enviar todos', 'envia_pncp', 'end') : null;
        //TODO SUPRIMIR O BTN DE ATUALIZAR SITUAÇÃO ATÉ SEGUNDA ORDEM
//        $this->crud->addButtonFromView('line', 'atualizarsituacaopublicacao', 'atualizarsituacaopublicacao');
        $this->crud->enableExportButtons();

        $this->adicionaColunas();
        $this->adicionaCampos();
        $this->adicionaFiltros();

    }

    protected function adicionaColunas()
    {
        $this->adicionaColunaId();
        $this->adicionaColunaTipo();
        $this->adicionaColunaTipoContrato();
        $this->adicionaColunaIdEntidade();
        $this->adicionaColunaSituacao();
        $this->adicionaColunaEnviadoI();
        $this->adicionaColunaEnviadoA();
        $this->adicionaColunaRetorno();
        $this->adicionaColunaLink();
        $this->adicionaColunaSequencial();
        $this->adicionaColunaContratoId();
        $this->adicionaColunalinkArquivoEmpenho();
        $this->adicionaUpdatedAt();
    }

    protected function adicionaCampos()
    {
        $this->adicionaCampoSituacao();
        $this->adicionaCampoEnviadoI();
        $this->adicionaCampoEnviadoA();
        $this->adicionaCampoRetorno();
        $this->adicionaCampoLink();
        $this->adicionaCampoSequencial();
    }

    protected function adicionaFiltros()
    {
        $this->adicionaFiltroId();
        $this->adicionaFiltroTipo();
        $this->adicionaFiltroSituacao();
        $this->adicionaFiltroTipoDeContrato();
        $this->adicionaFiltroIdEntidade();
        $this->adicionaFiltroContratoId();
    }

    /************************************
     *** ÁREA DE MONTAGEM DOS FILTROS ***
     ************************************/
    protected function adicionaFiltroId()
    {
        $this->crud->addFilter(
            [
                'name' => 'id',
                'type' => 'text',
                'label' => 'ID'
            ],
            false,
            function ($value) {
                $ids = array_map('trim', explode(',', $value));
                $ids = array_filter($ids, function ($id) {
                    return ctype_digit($id);
                });
                $this->crud->addClause(
                    'whereIn',
                    'envia_dados_pncp.id',
                    $ids
                );
            }
        );
    }
    protected function adicionaFiltroTipo()
    {
        $this->crud->addFilter(
            [
                'name' => 'pncpable_type',
                'type' => 'select2_multiple',
                'label' => 'Tipo'
            ],
            [
                "App\Models\Arp" => "Arp",
                "App\Models\MinutaEmpenho" => "MinutaEmpenho",
                "App\Models\Contratohistorico" => "Contratohistorico",
                "App\Models\ArpArquivo" => "ArpArquivo"
            ],
            function ($value) {
                $this->crud->addClause(
                    'whereIn',
                    'envia_dados_pncp.pncpable_type',
                    json_decode($value)
                );
            }
        );
    }


    protected function adicionaFiltroSituacao()
    {
        $this->crud->addFilter(
            [
                'name' => 'situacao',
                'type' => 'select2_multiple',
                'label' => 'Situação'
            ],
            $this->retornaSituacoesParaFiltro(),
            function ($value) {
                $this->crud->addClause(
                    'whereIn',
                    'envia_dados_pncp.situacao',
                    json_decode($value)
                );
            }
        );
    }

    private function retornaSituacoesParaFiltro()
    {
        return Codigoitem::whereHas('codigo', function ($query) {
            $query->where('descricao', '=', 'Situação Envia Dados PNCP');
        })
            ->orderBy('descricao')
            ->get()
            ->mapWithKeys(function ($item) {
                return [$item->id => "{$item->descres} - {$item->descricao}"];
            })
            ->toArray();
    }

    protected function adicionaFiltroTipoDeContrato()
    {
        $this->crud->addFilter(
            [
                'name' => 'tipo_contrato',
                'type' => 'select2_multiple',
                'label' => 'Tipo Contrato'
            ],
            $this->retornaTiposDeContratoParaFiltro(),
            function ($value) {
                $this->crud->addClause(
                    'whereIn',
                    'envia_dados_pncp.tipo_contrato',
                    json_decode($value)
                );
            }
        );
    }

    protected function adicionaFiltroIdEntidade()
    {
        $this->crud->addFilter(
            [
                'name' => 'pncpable_id',
                'type' => 'text',
                'label' => 'ID Entidade'
            ],
            false,
            function ($value) {
                $ids = array_map('trim', explode(',', $value));
                $ids = array_filter($ids, function ($id) {
                    return ctype_digit($id);
                });
                $this->crud->addClause(
                    'whereIn',
                    'envia_dados_pncp.pncpable_id',
                    $ids
                );
            }
        );
    }

    protected function adicionaFiltroContratoId()
    {
        $this->crud->addFilter(
            [
                'name' => 'contrato_id',
                'type' => 'text',
                'label' => 'Contrato ID'
            ],
            false,
            function ($value) {
                $ids = array_map('trim', explode(',', $value));
                $ids = array_filter($ids, function ($id) {
                    return ctype_digit($id);
                });
                $this->crud->addClause(
                    'whereIn',
                    'envia_dados_pncp.contrato_id',
                    $ids
                );
            }
        );
    }


    private function retornaTiposDeContratoParaFiltro()
    {
        return Codigoitem::whereHas('codigo', function ($query) {
            $query->where('descricao', '=', 'Tipo de Contrato');
        })
            ->whereNotIn('descricao', [
                'Acordo de Cooperação Técnica (ACT)',
                'Convênio',
                'Credenciamento',
                'Erro de Execução',
                'Executado',
                'Pendente de Execução',
                'Termo de Compromisso',
                'Termo de Encerramento',
                'Termo de Execução Descentralizada (TED)'
            ])
            ->orderBy('descricao')
            ->pluck('descricao', 'descricao')
            ->toArray();
    }

    /********************************************
     *** ÁREA DE MONTAGEM DA TABELA PRINCIPAL ***
     ********************************************/

    private function adicionaColunaId(): void
    {
        $this->crud->addColumn([
            'name' => 'id',
            'label' => 'Id',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
        ]);
    }

    /**
     * Cofigura a coluna
     */
    private function adicionaColunaTipo(): void
    {
        $this->crud->addColumn([
            'name' => 'pncpable_type',
            'label' => 'Tipo',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
        ]);
    }

    /**
     * Cofigura a coluna
     */
    private function adicionaColunaTipoContrato(): void
    {
        $this->crud->addColumn([
            'name' => 'tipo_contrato',
            'label' => 'Tipo Contrato',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
        ]);
    }

    /**
     * Cofigura a coluna
     */
    private function adicionaColunaIdEntidade(): void
    {
        $this->crud->addColumn([
            'name' => 'pncpable_id',
            'label' => 'Id Entidade',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
        ]);
    }

    /**
     * Cofigura a coluna
     */
    private function adicionaUpdatedAt(): void
    {
        $this->crud->addColumn([
            'name' => 'updated_at',
            'label' => 'Atualizado em',
            'type' => 'datetime',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
        ]);
    }

    /**
     * Cofigura a coluna
     */
    private function adicionaColunaSituacao(): void
    {
        $this->crud->addColumn([
            'name' => 'status.descres',
            'label' => 'Situação',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
            'searchLogic' => function ($query, $column, $searchTerm) {
                $query->orWhereHas('status', function ($q) use ($column, $searchTerm) {
                    $q->where('codigoitens.descres', 'like', '%' . $searchTerm . '%');
                });
            }
        ]);
    }

    /**
     * Cofigura a coluna
     */
    private function adicionaColunaEnviadoI(): void
    {
        $this->crud->addColumn([
            'name' => 'json_enviado_inclusao',
            'label' => 'Json Enviado Inclusão',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
            //'limit' => 20000,
        ]);
    }

    /**
     * Cofigura a coluna
     */
    private function adicionaColunaEnviadoA(): void
    {
        $this->crud->addColumn([
            'name' => 'json_enviado_alteracao',
            'label' => 'Json Enviado Alteração',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
        ]);
    }

    /**
     * Cofigura a coluna
     */
    private function adicionaColunaRetorno(): void
    {
        $this->crud->addColumn([
            'name' => 'retorno_pncp',
            'label' => 'Retorno',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
        ]);
    }

    /**
     * Cofigura a coluna
     */
    private function adicionaColunaLink(): void
    {
        $this->crud->addColumn([
            'name' => 'link_pncp',
            'label' => 'Link',
            'type' => 'closure',
            'function' => function ($entry) {
                if ($url = $entry->link_pncp) {
                    $shortUrl = mb_strimwidth($url, 0, 30, '...');
                    return
                        '<a ' .
                        'href="' . $url . '"' .
                        'target="_blank" class="link-preview" data-toggle="tooltip" title="' . htmlspecialchars($url) . '">'
                        . $shortUrl .
                        '<i class="fa fa-info-circle" aria-hidden="true"></i></a>';
                }
            },
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
        ]);
    }

    /**
     * Cofigura a coluna
     */
    private function adicionaColunaSequencial(): void
    {
        $this->crud->addColumn([
            'name' => 'sequencialPNCP',
            'label' => 'Sequencial',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
        ]);
    }

    /**
     * Cofigura a coluna
     */
    private function adicionaColunaContratoId(): void
    {
        $this->crud->addColumn([
            'name' => 'contrato_id',
            'label' => 'contrato_id',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
        ]);
    }

    /**
     * Cofigura a coluna
     */
    private function adicionaColunaLinkArquivoEmpenho(): void
    {
        $this->crud->addColumn([
            'name' => 'linkArquivoEmpenho',
            'label' => 'Link Arquivo Empenho',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
        ]);
    }

    /*************************************************
     *** ÁREA DE MONTAGEM DOS CAMPOS DE INFORMAÇÃO ***
     *************************************************/

    private function adicionaCampoSituacao(): void
    {
        $this->crud->addField([
            'name' => 'status.descres',
            'label' => 'Situação',
            'type' => 'text',
            /*'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ]*/
        ]);
    }

    private function adicionaCampoEnviadoI(): void
    {
        $this->crud->addField([
            'name' => 'json_enviado_inclusao',
            'label' => 'Json Enviado Inclusão',
            'type' => 'text',
            /*'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ]*/
        ]);
    }

    private function adicionaCampoEnviadoA(): void
    {
        $this->crud->addField([
            'name' => 'json_enviado_alteracao',
            'label' => 'Json Enviado Alteração',
            'type' => 'text',
            /*'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ]*/
        ]);
    }

    private function adicionaCampoRetorno(): void
    {
        $this->crud->addField([
            'name' => 'retorno_pncp',
            'label' => 'Retorno',
            'type' => 'text',
            /*'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ]*/
        ]);
    }

    private function adicionaCampoLink(): void
    {
        $this->crud->addField([
            'name' => 'link_pncp',
            'label' => 'Link',
            'type' => 'text',
            /*'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ]*/
        ]);
    }

    private function adicionaCampoSequencial(): void
    {
        $this->crud->addField([
            'name' => 'sequencialPNCP',
            'label' => 'Sequencial',
            'type' => 'text',
            /*'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ]*/
        ]);
    }

    public function destroy($id)
    {
        $this->crud->hasAccessOrFail('delete');
        $this->crud->setOperation('delete');

        $this->tiposContratoNBase = ['Termo Rescisão', 'Termo de Rescisão', 'Termo Aditivo', 'Termo Apostilamento', 'Termo de Apostilamento', 'Empenho', 'Credenciamento', 'Outros'];
        $enviaDadosPncp = EnviaDadosPncp::find($id);
        if (!in_array($enviaDadosPncp->tipo_contrato, $this->tiposContratoNBase) &&
            $enviaDadosPncp->pncpable_type == Contratohistorico::class &&
            isset($enviaDadosPncp->contrato_id)) {
            $filhos = EnviaDadosPncp::where([
                ['contrato_id', '=', $enviaDadosPncp->contrato_id],
                ['pncpable_type', Contratohistorico::class],
            ])->orderBy('created_at', 'ASC')->get();
            if ($filhos->count() > 1) {
                throw new Exception("Exclua os aditivos antes");
            }
        }

        $this->crud->delete($id);

        return 'delete_confirmation_not_deleted_message_vinculacao';
    }

}
