<?php

namespace App\Http\Controllers\Admin;

use App\Http\Traits\Authorizes;
use App\Jobs\UserMailPasswordJob;
use App\Models\BackpackUser;
use App\User;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\SystemUsersRequest as StoreRequest;
use App\Http\Requests\SystemUsersRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Encryption\Encrypter;
use Illuminate\Http\Request;
use Spatie\Activitylog\Models\Activity;

/**
 * Class SystemUsersCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class SystemUsersCrudController extends CrudController
{
    use Authorizes;

    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */

        if (!backpack_user()->hasRole('Administrador') && !backpack_user()->hasRole('Administrador Suporte')) {
            abort('403', config('app.erro_permissao'));
        }
        $this->crud->setModel('App\Models\BackpackUser');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/admin/usuario-sistema');
        $this->crud->setEntityNameStrings('Usuário', 'Usuários');
        $this->crud->addClause('select', 'users.*');
        $this->crud->addClause('leftJoin', 'unidades', 'unidades.id', '=', 'users.ugprimaria');
        $this->crud->addClause('where', 'user_system', '=', true);
        $this->crud->enableExportButtons();
        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');
        $this->crud->denyAccess('show');



        (backpack_user()->can('usuario_inserir')) ? $this->crud->allowAccess('create') : null;
        (backpack_user()->can('usuario_editar')) ? $this->crud->allowAccess('update') : null;
        (backpack_user()->can('usuario_deletar')) ? $this->crud->allowAccess('delete') : null;
        (backpack_user()->can('activitylog_consultar')) ? $this->crud->allowAccess('show') : null;

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Columns
        |--------------------------------------------------------------------------
        */

        $this->crud->setColumns([
            [
                'name' => 'cpf',
                'label' => 'Login usuário',
                'type' => 'text',
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere('users.cpf', 'ilike', "%" . utf8_encode(utf8_decode(strtoupper($searchTerm))) . "%");
                },
            ],
            [
                'name' => 'name',
                'label' => 'Nome sistema / Objetivo',
                'type' => 'text',
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere('users.name', 'ilike', "%" . utf8_encode(utf8_decode(strtoupper($searchTerm))) . "%");
                },
            ],
            [
                'name' => 'email',
                'label' => 'E-mail responsável ou setor responsável',
                'type' => 'email',
            ],
            [
                'name' => 'situacao',
                'label' => 'Situação',
                'type' => 'boolean',
                'options' => [0 => 'Inativo', 1 => 'Ativo'],
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    if (strtolower($searchTerm) == 'inativo') {
                        $query->orWhere('users.situacao', 0);
                    }

                    if (strtolower($searchTerm) == 'ativo') {
                        $query->orWhere('users.situacao', 1);
                    }
                }
            ],
            [
                'name' => 'getUGPrimaria',
                'label' => 'UG/UASG Padrão',
                'type' => 'model_function',
                'function_name' => 'getUGPrimaria',
                'orderable' => true,
                'searchLogic' => function (Builder $q, $column, $searchTerm) {
                    $q->orWhere('unidades.codigo', 'ilike', "%" . utf8_encode(utf8_decode(strtoupper($searchTerm))) . "%");
                    $q->orWhere('unidades.nomeresumido', 'ilike', "%" . utf8_encode(utf8_decode(strtoupper($searchTerm))) . "%");
                },
            ],
            [ // n-n relationship (with pivot table)
                'label' => trans('backpack::permissionmanager.roles'),
                'type' => 'select_multiple',
                'name' => 'roles',
                'entity' => 'roles',
                'attribute' => 'name',
                'model' => config('permission.models.role'),
            ],
            [
                'name' => 'last_login_at',
                'label' => 'Último Acesso',
                'type' => 'datetime',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
            ],
        ]);

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Fields
        |--------------------------------------------------------------------------
        */

        $fields = [
            [
                'name' => 'cpf',
                'label' => 'Login usuário',
                'type' => 'text',
                'tab' => 'Dados da Integração',
                'hint' => 'Exemplo: usr_[setor]_[sistema]_api_[ambiente]',
            ],
            [
                'name' => 'name',
                'label' => 'Nome sistema / Objetivo',
                'type' => 'text',
                'tab' => 'Dados da Integração',
                'attributes' => [
                    'onkeyup' => "maiuscula(this)"
                ],
            ],
            [
                'name' => 'email',
                'label' => 'E-mail responsável ou setor responsável',
                'type' => 'email',
                'tab' => 'Dados da Integração',
            ],
            [
                'name' => 'situacao',
                'label' => "Situação",
                'type' => 'select_from_array',
                'options' => [1 => 'Ativo', 0 => 'Inativo'],
                'allows_null' => false,
                'tab' => 'Dados da Integração'
            ],
            [
                'label' => "UG/UASG Padrão",
                'type' => "select2_from_ajax_single",
                'name' => 'ugprimaria',
                'entity' => 'ugPrimariaRelation',
                'attribute' => "codigo",
                'attribute2' => "nomeresumido",
                'process_results_template' => 'gescon.process_results_unidade',
                'model' => "App\Models\Unidade",
                'data_source' => url("api/unidade"),
                'placeholder' => "Selecione a Unidade",
                'minimum_input_length' => 2,
                'tab' => 'Outros',
            ],
            [
                'label' => "Demais UGs/UASGs",
                'type' => "select2_from_ajax_multiple_single",
                'name' => 'unidades',
                'entity' => 'unidades',
                'attribute' => "codigo",
                'attribute2' => "nomeresumido",
                'process_results_template' => 'gescon.process_results_multiple_unidade',
                'model' => "App\Models\Unidade",
                'data_source' => url("api/unidade"),
                'placeholder' => "Selecione a(s) Unidade(s)",
                'minimum_input_length' => 2,
                'tab' => 'Outros',
                'pivot' => true,
            ],
            [       // Select2Multiple = n-n relationship (with pivot table)
                'label' => 'Grupos de Usuário',
                'type' => 'select2_multiple',
                'name' => 'roles',
                'entity' => 'roles',
                'attribute' => 'name',
                'model' => config('permission.models.role'),
                'allows_null' => true,
                'pivot' => true,
                'select_all' => true,
                'tab' => 'Outros',
                'options' => (function ($query) {
                    $userRoles = backpack_user()->getRoleNames()->toArray();
                    if (in_array('Administrador', $userRoles)) {
                        return $query->get();
                    }

                    if (in_array('Administrador Suporte', $userRoles)) {
                        return $query->whereIn('name', BackpackUser::getAdminSuporteRoles())->get();
                    }
                }),
            ],
            [
                'name' => 'senha',
                'label' => 'Senha',
                'type' => 'password',
                'tab' => 'Outros',

            ]
        ];
        /** @var \Backpack\CRUD\CrudPanel $crud */
        if($operation = request()->route()->getName() === 'crud.usuario-sistema.create'){
            $fields[count($fields)-1]['attributes'] = [
                'required' => true
            ];
        }
        $this->crud->addFields($fields);

        if (backpack_user()->can('usuario_telegram')){
            $this->crud->addField([
                'name' => 'telegram_id',
                'label' => 'Telegram Id',
                'type' => 'password',
                'tab' => 'Dados da Integração'
            ]);
        }

        $this->crud->setRequiredFields(\App\Http\Requests\SystemUsersRequest::class, 'create');
        $this->crud->setRequiredFields(\App\Http\Requests\SystemUsersRequest::class, 'edit');
        $this->crud->enableExportButtons();
    }

    public function loadJson(Request $request, $id)
    {
        if (!backpack_user()->can('activitylog_consultar')) {
            abort('403', config('app.erro_permissao'));
        }
        $user = BackpackUser::findOrFail($id);
        $activities = Activity::causedBy($user)->select(
            'log_name',
            'description',
            'subject_id',
            'subject_type',
            'properties',
            'created_at'
        )->orderBy('created_at', 'desc')->get();

        return response()->json($activities);
    }

    public function show($id)
    {
        $content = parent::show($id);

        $this->crud->removeColumn('ugprimaria');
        $this->crud->removeColumn('senhasiafi');
        $this->crud->removeColumn('acessogov');
        $this->crud->removeColumn('id_sistema_origem');
        $this->crud->removeColumn('telegram_id');

        $this->crud->addColumn([
            'name' => 'last_login_ip',
            'label' => 'Último IP',
            'orderable' => true,
            'visibleInTable' => false,
            'visibleInModal' => false,
            'visibleInExport' => false,
            'visibleInShow' => true,
        ]);
        $this->crud->addColumn([
            'name' => 'last_login_browser',
            'label' => 'Último Navegador',
            'orderable' => true,
            'visibleInTable' => false,
            'visibleInModal' => false,
            'visibleInExport' => false,
            'visibleInShow' => true,
        ]);

        return $content;
    }

    public function store(StoreRequest $request)
    {
        $senha = $request->input('senha');

        if($senha and !empty($senha)) {
            $request->request->set('password', bcrypt($senha));
        }
        $request->request->set('user_system', true);

        $this->setTelegramId($request);

        $redirect_location = parent::storeCrud($request);

        $usuario = BackpackUser::where('cpf', '=', $request->input('cpf'))->first();
        $usuario->user_system = true;
        $usuario->save();

        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        if ($request->input('situacao') == 0) {
            // 0 = false = inativo
            $request->request->set('ugprimaria', null);
            $request->request->set('unidades', null);
            $request->request->set('roles', null);
        }

        $senha = $request->input('senha');

        if($senha and !empty($senha)) {
            $request->request->set('password', bcrypt($senha));
        }

        $this->setTelegramId($request);

        $redirect_location = parent::updateCrud($request);

        return $redirect_location;
    }

    public function setTelegramId($request): void
    {
        $telegram_id_encrypted = null;

        if (!is_null($request->input('telegram_id'))) {
            // salva TelegramID criptografado
            $Encrypter = new Encrypter(base64_decode(substr(env('TELEGRAM_KEY'), 7)), 'AES-256-CBC');
            $telegram_id_encrypted = $Encrypter->encryptString(request()->input('telegram_id'));
        }
        $request->request->set('telegram_id', $telegram_id_encrypted);
    }
}
