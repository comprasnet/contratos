<?php

namespace App\Http\Controllers\Admin;

use App\Http\Traits\BuscaCodigoItens;
use App\Models\AmparoLegalRestricao;
use App\Models\Codigoitem;
use App\Models\AmparoLegal;

use App\Models\SfRestricaoAmparoLegal;
use Backpack\CRUD\app\Http\Controllers\CrudController;


use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;

use App\Http\Requests\AmparoLegalRequest as StoreRequest;
use App\Http\Requests\AmparoLegalRequest as UpdateRequest;
use App\Models\SfOrcAmparoLegalDados;
use App\Repositories\Base;
use App\XML\Execsiafi;
use Backpack\CRUD\CrudPanel;
use Illuminate\View\View;
use Exception;
use Route;
use URL;

/**
 * Class AmparoLegalCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class AmparoLegalCrudController extends CrudController
{
    use BuscaCodigoItens;

    /**
     * @throws Exception
     */
    public function setup(): void
    {
        if (backpack_user()->hasRole('Administrador')) {

            /*
            |--------------------------------------------------------------------------
            | CrudPanel Basic Information
            |--------------------------------------------------------------------------
            */
            $this->crud->setModel(AmparoLegal::class);
            $this->crud->setRoute(config('backpack.base.route_prefix') . '/admin/amparolegal');
            $this->crud->setEntityNameStrings('Amparo Legal', 'Amparos Legais');


            $this->crud->addClause('select', [
                'codigoitens.id as id_codigoitens',
                // Tabela principal deve ser sempre a última da listagem!
                'amparo_legal.*',
            ]);

            $this->crud->addClause('join', 'codigoitens',
                'codigoitens.id', '=', 'amparo_legal.modalidade_id'
            );

            $this->crud->addClause('orderBy', 'id', 'DESC');

            // modalidades para o select
            $arrayModalidades = Codigoitem::whereHas('codigo', function ($query) {
                $query->where('descricao', '=', 'Modalidade Licitação');
            })
                ->where('visivel', true)
                ->orderBy('descricao')
                ->pluck('descricao', 'id')
                ->toArray();

            $idAmparoLegal = \Route::current()->parameter('amparolegal');
            $codigoRestricao = null;
            if (isset($idAmparoLegal)) {
                $array = AmparoLegalRestricao::where('amparo_legal_id', $idAmparoLegal)->get();
                if (count($array) > 0) {
                    $codigoRestricao = $array->first()->codigo_restricao;
                }
            }


            $this->crud->enableExportButtons();
            $this->crud->addColumns($this->colunas());
            $this->crud->addFields($this->campos($arrayModalidades, $codigoRestricao));
            $this->crud->addButtonFromView('line', 'enviaramparolegalsiafi', 'enviaramparolegalsiafi', 'beginning');

            // $this->crud->denyAccess('create');
            // $this->crud->denyAccess('update');
            // $this->crud->denyAccess('delete');
            $this->crud->allowAccess('show');

            /*
            |--------------------------------------------------------------------------
            | CrudPanel Configuration
            |--------------------------------------------------------------------------
            */

            // add asterisk for fields that are required in IndicadorRequest
            $this->crud->setRequiredFields(StoreRequest::class, 'create');
            $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
            $this->crud->addButtonFromModelFunction('line', 'amparoLegalRestricoes', 'amparoLegalRestricoes', 'end');
        } else {
            abort('403', config('app.erro_permissao'));
        }
    }

    private function colunas(): array
    {
        return [

            [
                'name' => 'getModalidade',
                'label' => 'Modalidade', // Table column heading
                'type' => 'model_function',
                'function_name' => 'getModalidade', // the method in your Model
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
                'orderLogic' => function ($query, $column, $columnDirection) {
                    return $query->orderBy('descricao_modalidade_id', $columnDirection);
                },
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere('codigoitens.descricao', 'ilike', "%" . $searchTerm . "%");
                },
            ],
            [
                'name' => 'ato_normativo',
                'label' => 'Ato Normativo',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere('ato_normativo', 'ilike', "%" . $searchTerm . "%");
                },
            ],
            [
                'name' => 'artigo',
                'label' => 'Artigo',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere('artigo', 'ilike', "%" . $searchTerm . "%");
                },
            ],
            [
                'name' => 'paragrafo',
                'label' => 'Parágrafo',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere('paragrafo', 'ilike', "%" . $searchTerm . "%");
                },
            ],
            [
                'name' => 'inciso',
                'label' => 'Inciso',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere('inciso', 'ilike', "%" . $searchTerm . "%");
                },
            ],
            [
                'name' => 'alinea',
                'label' => 'Alínea',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere('alinea', 'ilike', "%" . $searchTerm . "%");
                },
            ],
            [
                'name' => 'codigo',
                'label' => 'Código',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere('codigo', 'ilike', "%" . $searchTerm . "%");
                },
            ],
            [
                'name' => 'complemento_14133',
                'label' => 'Enviar para o PNCP',
                'type' => 'model_function',
                'function_name' => 'getAmparoLegalEnviarPncp',
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $searchTermConvert = false;
                    if (strcasecmp($searchTerm, 'Sim') === 0) {
                        $searchTermConvert = true;
                    }

                    $query->orWhere('complemento_14133', $searchTermConvert);
                },
            ]
        ];
    }

    private function campos($arrayModalidades, $codigoRestricao): array
    {
        return [
            [ // select_from_array
                'name' => 'modalidade_id',
                'label' => "Modalidade",
                'type' => 'select2_from_array',
                'options' => $arrayModalidades,
                'allows_null' => false,
                'allows_multiple' => false, // OPTIONAL; needs you to cast this to array in your model;
            ],
            [
                'name' => 'ato_normativo',
                'label' => 'Ato Normativo',
                'type' => 'text',
                'attributes' => [
                    'onkeyup' => "maiuscula(this)"
                ],
            ],
            [
                'name' => 'artigo',
                'label' => 'Artigo',
                'type' => 'text',
                'attributes' => [
                    'onkeyup' => "maiuscula(this)"
                ],
            ],
            [
                'name' => 'paragrafo',
                'label' => 'Parágrafo',
                'type' => 'text',
                'attributes' => [
                    'onkeyup' => "maiuscula(this)"
                ],
            ],
            [
                'name' => 'inciso',
                'label' => 'Inciso',
                'type' => 'text',
                'attributes' => [
                    'onkeyup' => "maiuscula(this)"
                ],
            ],
            [
                'name' => 'alinea',
                'label' => 'Alínea',
                'type' => 'text',
                'attributes' => [
                    'onkeyup' => "maiuscula(this)"
                ],
            ],
            [   // Checkbox
                'name' => 'complemento_14133',
                'label' => 'Enviar para o PNCP',
                'type' => 'checkbox',
            ],
        ];
    }

    public function store(StoreRequest $request)
    {
        try {
            DB::beginTransaction();
            // your additional operations before save here
            $redirect_location = parent::storeCrud($request);

//            $sfAmparoLegalDados = $this->createDadosSiafi( $this->crud->entry, 'I');
//            if ($sfAmparoLegalDados) {
//                $this->enviarDadosSiafi($sfAmparoLegalDados);
//            }

            DB::commit();
            // // use $this->data['entry'] or $this->crud->entry
            return $redirect_location;
        } catch (\Exception $e) {
            DB::rollback();
            throw new \Exception('Não foi possível inserir o amparo legal.');
        }
    }

    public function update(UpdateRequest $request)
    {
        try {
            DB::beginTransaction();

            $redirect_location = parent::updateCrud($request);
            $complemento = $request->all()['complemento_14133'];
            $atoNormativo = $request->all()['ato_normativo'];

            $verificaNaoSeAplic = AmparoLegal::join('codigoitens', 'amparo_legal.modalidade_id', '=', 'codigoitens.id')
            ->where(function ($query) {
                $query->where('codigoitens.descres', 'NAOSEAPLIC')
                    ->where('ato_normativo', 'LEI 14.133/2021');
            })
            ->where('amparo_legal.id', $this->crud->getCurrentEntryId())
            ->select('amparo_legal.*')
            ->first();
    
            if (!$verificaNaoSeAplic) {
                AmparoLegal::join('codigoitens', 'amparo_legal.modalidade_id', '=', 'codigoitens.id')
                ->where('ato_normativo', 'ilike', "%$atoNormativo%")
                ->where(function ($query) {
                    $query->where('codigoitens.descres', '!=', 'NAOSEAPLIC');
                })->update(['complemento_14133' => $complemento]);
            }

//dd($atoNormativo, $complemento, $request->all()['ato_normativo']);
//            $sfAmparoLegalDados = $this->createDadosSiafi($this->crud->entry, 'A');
//            if ($sfAmparoLegalDados) {
//                $this->enviarDadosSiafi($sfAmparoLegalDados);
//            }

            DB::commit();

            return $redirect_location;
        } catch (\Exception $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $amparoLegal = AmparoLegal::find($id);
            $this->createDadosSiafi($amparoLegal, 'D');
            $amparoLegal->delete();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
//            throw new \Exception($e->getMessage());
        }
    }

    public function show($id): View
    {
        $content = parent::show($id);

        $this->crud->removeColumns([
            'modalidade_id'
        ]);

        $this->crud->addColumn([
            'name' => 'getRestricoes',
            'label' => 'Restrições', // Table column heading
            'type' => 'model_function',
            'function_name' => 'getRestricoes', // the method in your Model
        ]);
        return $content;
    }

    private function enviarDadosSiafi($sfAmparoLegalDados)
    {
        if ($sfAmparoLegalDados->codmodalidadelicitacao <= 9 || $sfAmparoLegalDados->codmodalidadelicitacao == 99) {
            $ws_siafi = new Execsiafi;
            $resposta = $ws_siafi->enviarAmparoLegal(
                session('user_ug'),
                config('app.ambiente_siafi'),
                date('Y'),
                $sfAmparoLegalDados
            );
            preg_match('/<resultado>(.+)<\/resultado>/', $resposta, $matches);

            switch ($matches[1]) {
                case 'FALHA':
                    preg_match('/<txtMsg>(.+)<\/txtMsg>/', $resposta, $mensagem);
                    $sfAmparoLegalDados->retornosiafi = $resposta;
                    $sfAmparoLegalDados->save();
                    if (isset($mensagem[1]))
                        \Alert::error($mensagem[1])->flash();
                    break;
                case 'SUCESSO':
                    preg_match('/<codAmparoLegal>(.+)<\/codAmparoLegal>/', $resposta, $codAmparoLegal);
                    $sfAmparoLegalDados->codamparolegal = $codAmparoLegal[1];
                    $sfAmparoLegalDados->retornosiafi = $resposta;
                    $sfAmparoLegalDados->save();
                    break;

                default:
                    # code...
                    break;
            }
        }
    }

    /**
     * Criação dos dados de envio, método updated de Observer não funcionava
     */
    private function createDadosSiafi(AmparoLegal $amparoLegal, $tipo)
    {

        if (!isset(config('app.de_para_amparo_legal_siasg_siafi')[$amparoLegal->modalidade->descres])) {
            return false;
        }

        $codmodalidadelicitacao = config('app.de_para_amparo_legal_siasg_siafi')[$amparoLegal->modalidade->descres];

        $textoTipo = [
            'I' => 'INCLUSÃO',
            'A' => 'ALTERAÇÂO',
            'D' => 'EXCLUSÃO'
        ];
        $action = $textoTipo[$tipo];
        $data = date('d/m/Y');

        if($tipo == 'D'){
            $sfOrcAmparoLegalDados = new SfOrcAmparoLegalDados();
            $sfOrcAmparoLegalDados->amparo_legal_id = $amparoLegal->id;
            $sfOrcAmparoLegalDados->codamparolegal = $amparoLegal->codigo;
            $sfOrcAmparoLegalDados->nonce = '';
            $sfOrcAmparoLegalDados->retornosiafi = '';
            $sfOrcAmparoLegalDados->tipo = $tipo;
            $sfOrcAmparoLegalDados->motivo = "$action do AMPARO $amparoLegal->ato_normativo, $amparoLegal->artigo, " .
                "$amparoLegal->paragrafo, $amparoLegal->inciso, $amparoLegal->alinea EM $data";
            $sfOrcAmparoLegalDados->save();
            return true;
        }

        $sfOrcAmparoLegalDados = new SfOrcAmparoLegalDados();
        $sfOrcAmparoLegalDados->amparo_legal_id = $amparoLegal->id;
        $sfOrcAmparoLegalDados->codamparolegal = $amparoLegal->codigo;
        $sfOrcAmparoLegalDados->codmodalidadelicitacao = $codmodalidadelicitacao;
        $sfOrcAmparoLegalDados->atonormativo = $amparoLegal->ato_normativo;
        $sfOrcAmparoLegalDados->artigo = $amparoLegal->artigo;
        $sfOrcAmparoLegalDados->paragrafo = $amparoLegal->paragrafo;
        $sfOrcAmparoLegalDados->inciso = $amparoLegal->inciso;
        $sfOrcAmparoLegalDados->alinea = $amparoLegal->alinea;
        $sfOrcAmparoLegalDados->nonce = '';
        $sfOrcAmparoLegalDados->retornosiafi = '';
        $sfOrcAmparoLegalDados->tipo = $tipo;
        $sfOrcAmparoLegalDados->motivo = "$action do AMPARO $amparoLegal->ato_normativo, $amparoLegal->artigo, " .
            "$amparoLegal->paragrafo, $amparoLegal->inciso, $amparoLegal->alinea EM $data";
        $sfOrcAmparoLegalDados->save();
        return true;
    }

    public function salvarSfAmparoLegal()
    {
        $amparolegal_id = Route::current()->parameter('amparolegal_id');
        $amparolegal = AmparoLegal::find($amparolegal_id);
        $tipo = $amparolegal->codigo ? 'A' : 'I';
        try {
            $this->createDadosSiafi($amparolegal, $tipo);
            return redirect(URL::previous());
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function salvarSfRestricaoAmparoLegal($amparoLegalRestricoes, $sfOrcAmparoLegalDados_id, $exclusao = false)
    {
        foreach ($amparoLegalRestricoes as $amparoLegalRestricao) {
            $sfRestricaoAmparoLegal = new SfRestricaoAmparoLegal();

            $sfRestricaoAmparoLegal->sforcamparolegaldados_id = $sfOrcAmparoLegalDados_id;
            $codigoSiafi = config('app.tipo_restricao_codigo_siafi');
            $sfRestricaoAmparoLegal->tiporestricao = $codigoSiafi[$amparoLegalRestricao->codigoItem->descres];
            $sfRestricaoAmparoLegal->regra = $amparoLegalRestricao->regra;
            $sfRestricaoAmparoLegal->valor = ($codigoSiafi[$amparoLegalRestricao->codigoItem->descres] == 'AGENCIA_EXECUTIVA') ? '' : $amparoLegalRestricao->codigo_restricao;
            $sfRestricaoAmparoLegal->save();
        }


    }
}
