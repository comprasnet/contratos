<?php

namespace App\Http\Controllers\Admin;

use Alert;
use App\Http\Controllers\AdminController;
use App\Http\Requests\UnidadeRequest as StoreRequest;
use App\Http\Requests\UnidadeRequest as UpdateRequest;
use App\Jobs\AlertaContratoJob;
use App\Models\Orgao;
use App\Models\Unidade;
use App\services\STA\STAUrlFetcherService;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\CrudPanel;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

// VALIDATION: change the requests to match your own file names if you need form validation

/**
 * Class UnidadeCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class UnidadeCrudController extends CrudController
{
    /**
     * @throws Exception
     */
    public function setup()
    {
        $user_id = backpack_user()->id; // id do usuário logado

        if (
            backpack_user()->hasRole('Administrador') ||
            backpack_user()->hasRole('Administrador Órgão') ||
            backpack_user()->hasRole('Administrador Unidade') ||
            backpack_user()->hasRole('Administrador Suporte')   ||
            backpack_user()->hasRole('Consulta Global')
        ) {
            /*
            |--------------------------------------------------------------------------
            | CrudPanel Basic Information
            |--------------------------------------------------------------------------
            */
            $this->crud->setModel('App\Models\Unidade');
            $this->crud->setRoute(config('backpack.base.route_prefix') . '/admin/unidade');
            $this->crud->setEntityNameStrings('Unidade', 'Unidades');
            $this->crud->setCreateView('vendor.backpack.crud.admin.unidades.create');
            $this->crud->setUpdateView('vendor.backpack.crud.admin.unidades.edit');
            $this->crud->enableExportButtons();
            if(
                backpack_user()->hasRole('Administrador') ||
                backpack_user()->hasRole('Administrador Órgão') ||
                backpack_user()->hasRole('Administrador Unidade') ||
                backpack_user()->hasRole('Administrador Suporte')
            ){
                $this->crud->addButtonFromView('line', 'moreunidade', 'moreunidade', 'end');
            }

            $this->crud->denyAccess('create');
            $this->crud->denyAccess('update');
            $this->crud->denyAccess('delete');
            $this->crud->allowAccess('show');

            // se for adm de órgão, listar as unidades dos órgãos do usuário
            if (!backpack_user()->hasRole('Administrador') && backpack_user()->hasRole('Administrador Órgão')) {

                // buscar os órgãos do usuário logado e jogar no crud / eloquent
                $orgaos = Orgao::whereHas('unidades', function ($u) {
                    $u->whereHas('users', function ($us) {
                        $us->where('cpf', backpack_user()->cpf);
                    })->orWhereHas('user', function ($usu) {
                        $usu->where('cpf', backpack_user()->cpf);
                    });
                })->pluck('id')->toArray();
                // pegar o array de órgãos e jogar no crud, pelo eloquent. (query é do eloquent)
                $this->crud->query->whereIn('orgao_id', $orgaos);

            } elseif (backpack_user()->hasRole('Administrador Unidade') && !backpack_user()->hasRole('Administrador')) {
                // se for adm de unidade, listar apenas a unidade do usuário.
                $this->crud->query->whereIn('unidades.id', backpack_user()->unidades()->pluck('id')->toArray());
                $this->crud->query->orWhere('unidades.id', backpack_user()->unidade->id);
            }
            $this->crud->addClause('select',
                [
                    'unidades.id',
                    'unidades.codigosiasg as unidade_siasg',
                    'unidades.*',
                    'orgaos.nome',
                    'orgaos.codigo'
                ]
            );
            $this->crud->addClause('join', 'orgaos', 'unidades.orgao_id', '=', 'orgaos.id');
            $this->crud->addClause('orderBy', 'nomeresumido');

            (backpack_user()->can('atualizar_unidade_utiliza_antecipa_gov')) ?
                $this->crud->addButtonFromView('top', 'atualiza_unidade_antecipa_gov',
                    'atualiza_unidade_antecipa_gov', 'end') : null;

            if(backpack_user()->hasRole('Administrador')){
                $this->crud->addButtonFromView('top', 'atualizaunidade',
                    'atualizaunidade', 'end');
            }

            (backpack_user()->can('unidade_inserir')) ? $this->crud->allowAccess('create') : null;
            (backpack_user()->can('unidade_editar')) ? $this->crud->allowAccess('update') : null;
            (backpack_user()->can('unidade_deletar')) ? $this->crud->allowAccess('delete') : null;

            (backpack_user()->can('executa_rotina_alerta_mensal')) ? $this->crud->addButtonFromView('top', 'rotinaalertamensal',
                'rotinaalertamensal', 'end') : null;

            /*
            |--------------------------------------------------------------------------
            | CrudPanel Configuration
            |--------------------------------------------------------------------------
            */

            $colunas = $this->Colunas();
            $this->crud->addColumns($colunas);
            $this->adicionaFiltroAntecipaGov();
            $this->adicionaFiltroSisg();
            $this->adicionaFiltroUtilizaSiafi();
            $this->adicionaFiltroEsfera();
            $this->adicionaFiltroSiasg();
            $this->adicionaFiltroOrgao();

            $orgaos = Orgao::where('situacao', '=', true)->pluck('nome', 'id')->toArray();

            //pegar o órgão logado para passar o id na consulta de unidades
            $campos = $this->Campos($orgaos, session()->get('user_orgao_id'));
            $this->crud->addFields($campos);

            // add asterisk for fields that are required in UnidadeRequest
            $this->crud->setRequiredFields(StoreRequest::class, 'create');
            $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        } else {
            abort('403', config('app.erro_permissao'));
        }
    }

    public function Colunas()
    {
        $flag = (backpack_user()->can('atualizar_unidade_utiliza_antecipa_gov')) ? true : false;

        $colunas = [
            [
                'name' => 'check',
                'label' => ' <input type="checkbox" class="crud_bulk_actions_main_checkbox" style="width: 16px; height: 16px;" />',
                'type' => 'checkbox',
                'orderable' => false,
                'visibleInTable' => $flag, // no point, since it's a large text
                'visibleInModal' => false, // would make the modal too big
                'visibleInExport' => false, // not important enough
                'visibleInShow' => false, // sure, why not
            ],
            [
                'name' => 'unidade_siasg',
                'label' => 'UASG SIASG', // Table column heading
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'codigosiafi',
                'label' => 'UG SIAFI', // Table column heading
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
                'searchLogic' => function ($query, $column, $searchTerm) {
                    $query->orWhere('unidades.codigo', 'ilike', "%$searchTerm%");
                },
            ],
            [
                'name' => 'gestao',
                'label' => 'Gestão', // Table column heading
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'unidades.nome',
                'label' => 'Nome', // Table column heading
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => false, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
                'searchLogic' => function ($query, $column, $searchTerm) {
                    $query->orWhere('unidades.nome', 'ilike', "%$searchTerm%");
                },
            ],
            [
                'name' => 'nomeresumido',
                'label' => 'Nome Resumido', // Table column heading
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
                'searchLogic' => function ($query, $column, $searchTerm) {
                    $query->orWhere('nomeresumido', 'ilike', "%$searchTerm%");

                }
            ],
            [
                'name' => 'getOrgao',
                'label' => 'Órgão', // Table column heading
                'type' => 'model_function',
                'function_name' => 'getOrgao', // the method in your Model
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
                'searchLogic' => function ($query, $column, $searchTerm) {
                    $query->whereHas('orgao', function ($query) use ($searchTerm) {
                        $query->where('nome', 'ilike', "%$searchTerm%")
                            ->orWhere('codigo', 'ilike', "%$searchTerm%");
                    });
                }
            ],
            [
                'name' => 'unidades.cnpj',
                'label' => 'CNPJ', // Table column heading
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
                'searchLogic' => function ($query, $column, $searchTerm) {
                    $query->orWhere('unidades.cnpj', 'ilike', "%$searchTerm%");
                }
            ],
            [
                'name' => 'telefone',
                'label' => 'Telefone', // Table column heading
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'getTipo',
                'label' => 'Tipo', // Table column heading
                'type' => 'model_function',
                'function_name' => 'getTipo', // the method in your Model
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'sisg',
                'label' => 'Sisg',
                'type' => 'boolean',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'getUF',
                'label' => 'UF',
                'type' => 'model_function',
                'function_name' => 'getUF',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'getMunicipio',
                'label' => 'Município',
                'type' => 'model_function',
                'function_name' => 'getMunicipio',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'esfera',
                'label' => 'Esfera',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'despacho_autorizatorio',
                'label' => 'Despacho Autorizatório',
                'type' => 'boolean',
                'orderable' => true,
                'visibleInTable' => false, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
               'options' => [0 => 'Não', 1 => 'Sim']
            ],
            [
                'name' => 'poder',
                'label' => 'Poder',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'tipo_adm',
                'label' => 'Administração',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'aderiu_siasg',
                'label' => 'SIASG',
                'type' => 'boolean',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'utiliza_siafi',
                'label' => 'SIAFI',
                'type' => 'boolean',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'utiliza_custos',
                'label' => 'Utiliza Custos SIAFI',
                'type' => 'boolean',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
                // optionally override the Yes/No texts
                'options' => [0 => 'Não', 1 => 'Sim']
            ],
            [
                'name' => 'utiliza_antecipagov',
                'label' => 'AntecipaGOV',
                'type' => 'boolean',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'unidades.codigo_siorg',
                'label' => 'SIORG',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'sigilo',
                'label' => 'Sigilo',
                'type' => 'boolean',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
                // optionally override the Yes/No texts
                'options' => [0 => 'Não', 1 => 'Sim']
            ],
            [
                'name' => 'situacao',
                'label' => 'Situação',
                'type' => 'boolean',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
                // optionally override the Yes/No texts
                'options' => [0 => 'Inativo', 1 => 'Ativo']
            ],
        ];

        return $colunas;
    }

    public function Campos($orgaos, $orgaologado_id = null)
    {
        $campos = [
            [
                'name' => 'orgao_id',
                'label' => "Órgão",
                'type' => 'select2_from_array',
                'options' => $orgaos,
                'allows_null' => false,
            ],
            [
                'name' => 'codigosiafi',
                'label' => "Código SIAFI",
                'type' => 'unidade',
            ],
            [
                'name' => 'gestao',
                'label' => "Gestão",
                'type' => 'gestao',
            ],
            [
                'name' => 'codigo',
                'label' => "Código SIASG",
                'type' => 'unidade',
            ],
            [
                'name' => 'nome',
                'label' => "Nome",
                'type' => 'text',
                'attributes' => [
                    'onkeyup' => "maiuscula(this)",
                ]
            ],
            [
                'name' => 'nomeresumido',
                'label' => "Nome Resumido",
                'type' => 'text',
                'attributes' => [
                    'onkeyup' => "maiuscula(this)",
                    'maxlength' => "19"
                ]
            ],
            [
                'name' => 'cnpj',
                'label' => "CNPJ",
                'type' => 'text',
            ],
            [
                'name' => 'telefone',
                'label' => "Telefone Fixo",
                'type' => 'telefone',
            ],
            [
                'name' => 'tipo',
                'label' => "Tipo",
                'type' => 'select_from_array',
                'options' => [
                    'C' => 'Controle',
                    'E' => 'Executora',
                    'S' => 'Setorial Contábil',
                ],
                'allows_null' => true,
                'default' => 'E',
            ],
            [
                'name' => 'sisg',
                'label' => "Sisg",
                'type' => 'select_from_array',
                'options' => [1 => 'Ativo', 0 => 'Inativo'],
                'allows_null' => false,
                'attributes' => [
                    'id' => 'is-sisg',
                    'onchange' => 'defineExclusivoGestaoAtas()'
                ]
            ],
            [
                'name' => 'uf',
                'label' => "Estado",
                'type' => 'select2_from_array',
                'options' =>
                    [
                        12 => "Acre",
                        27 => "Alagoas",
                        13 => "Amazonas",
                        16 => "Amapá",
                        29 => "Bahia",
                        23 => "Ceará",
                        53 => "Distrito Federal",
                        32 => "Espírito Santo",
                        52 => "Goiás",
                        21 => "Maranhão",
                        51 => "Mato Grosso",
                        50 => "Mato Grosso do Sul",
                        31 => "Minas Gerais",
                        15 => "Pará",
                        25 => "Paraíba",
                        41 => "Paraná",
                        26 => "Pernambuco",
                        22 => "Piauí",
                        33 => "Rio de Janeiro",
                        24 => "Rio Grande do Norte",
                        11 => "Rondônia",
                        43 => "Rio Grande do Sul",
                        14 => "Roraima",
                        42 => "Santa Catarina",
                        28 => "Sergipe",
                        35 => "São Paulo",
                        17 => "Tocantins",
                        99 => "Exterior"
                    ],
                'allows_null' => true,
                'default' => $this->estadoId(),
                'attributes' => [
                    'onchange' => 'defineDespachoAutorizatorio()',
                ]
            ],
            [ // select_from_array
                'name' => 'municipio_id', // the column that contains the ID of that connected entity
                'label' => "Município", // Table column heading
                'type' => 'select2_from_ajax',
                'model' => 'App\Models\Municipio',
                'entity' => 'municipio', // the method that defines the relationship in your Model
                'attribute' => 'nome', // foreign key attribute that is shown to user
                'data_source' => url('api/municipios'), // url to controller search function (with /{id} should return model)
                'placeholder' => 'Selecione...', // placeholder for the select
                'minimum_input_length' => 0, // minimum characters to type before querying results
                'dependencies' => ['uf'], // when a dependency changes, this select2 is reset to null
                'method' => 'GET', // optional - HTTP method to use for the AJAX call (GET, POST)
                'attributes' => [
                    'onchange' => 'defineDespachoAutorizatorio()',
                ]
            ],
            [ // select_from_array
                'name' => 'esfera',
                'label' => "Esfera",
                'type' => 'select_from_array',
                'options' => ['Estadual' => 'Estadual', 'Federal' => 'Federal', 'Municipal' => 'Municipal'],
                'allows_null' => true,
                'attributes' => [
                    'onchange' => 'defineExclusivoGestaoAtas(); defineDespachoAutorizatorio();',
                ]
            ],
            [
                'name' => 'despacho_autorizatorio',
                'label' => "Despacho Autorizatório",
                'type' => 'radio',
                'options' => [
                    1 => "Sim",
                    0 => "Não"
                ],
                'inline' => true,
                'attributes' => [
                    'disabled' => 'disabled'
                ]
            ],
            [
                'name' => 'exclusivo_gestao_atas',
                'label' => "Exclusivamente Gestão de Atas",
                'type' => 'radio',
                'options' => [
                    1 => "Sim",
                    0 => "Não"
                ],
                'inline' => true,
                'attributes' => [
                    'disabled' => 'disabled'
                ]
            ],
            [
                'name' => 'poder',
                'label' => "Poder",
                'type' => 'select_from_array',
                'options' => ['Executivo' => 'Executivo',
                    'Judiciário' => 'Judiciário',
                    'Legislativo' => 'Legislativo'],
                'allows_null' => true,
            ],
            [
                'name' => 'tipo_adm',
                'label' => "Administração",
                'type' => 'select_from_array',
                'options' => ['ADMINISTRAÇÃO DIRETA' => 'ADMINISTRAÇÃO DIRETA',
                    'ADMINISTRAÇÃO DIRETA ESTADUAL' => 'ADMINISTRAÇÃO DIRETA ESTADUAL',
                    'ADMINISTRAÇÃO DIRETA MUNICIPAL' => 'ADMINISTRAÇÃO DIRETA MUNICIPAL',
                    'AUTARQUIA' => 'AUTARQUIA',
                    'ECONOMIA MISTA' => 'ECONOMIA MISTA',
                    'EMPRESA PÚBLICA COM. E FIN.' => 'EMPRESA PÚBLICA COM. E FIN.',
                    'FUNDAÇÃO' => 'FUNDAÇÃO',
                    'FUNDOS' => 'FUNDOS'],
                'allows_null' => true,
            ],
            [
                'name' => 'aderiu_siasg',
                'label' => "Aderiu SIASG",
                'type' => 'select_from_array',
                'options' => ['Não', 'Sim'],
                'allows_null' => false,
            ],
            [
                'name' => 'utiliza_siafi',
                'label' => "Utiliza SIAFI",
                'type' => 'select_from_array',
                'options' => ['Não', 'Sim'],
                'allows_null' => false,
            ],
            [ // select_from_array
                'name' => 'utiliza_custos',
                'label' => "Utiliza Custos SIAFI",
                'type' => 'select_from_array',
                'options' => [0 => 'Não', 1 => 'Sim'],
                'allows_null' => false,
            ],
            [
                'name' => 'utiliza_antecipagov',
                'label' => "Utiliza AntecipaGOV",
                'type' => 'select_from_array',
                'options' => ['Não', 'Sim'],
                'allows_null' => false,
                'attributes' => [
                    'id' => 'utiliza-antecipagov',
                ],
            ],
            [
                'label' => "Unidade(s) Executora(s) Financeira(s)",
                'type' => "select2_from_ajax_multiple_single",
                'name' => 'unidadesexecutorasfinanceiras',
                'entity' => 'unidadesExecutorasFinanceiras',
                'attribute' => "codigo",
                'attribute2' => "nomeresumido",
                'process_results_template' => 'administracao.unidade.process_results_multiple_unidadesexecutorasfinanceiras',
                'model' => "App\Models\Unidade",
                'data_source' => url("api/unidadesexecutoras/{$orgaologado_id}/"),
                'placeholder' => "Selecione o(s) Fornecedor(s)",
                'minimum_input_length' => 2,
                'pivot' => true,
                'wrapperAttributes' => [
                    'id' => 'div-unidades_executoras',
                ]
            ],
            [
                'name' => 'codigo_siorg',
                'label' => "Codigo Siorg",
                'type' => 'text',
                'allows_null' => true
            ],
            [
                'name' => 'sigilo',
                'label' => "Sigilo",
                'type' => 'select_from_array',
                'options' => [0 => 'Não', 1 => 'Sim',],
                'allows_null' => false,
            ],
            [
                'name' => 'situacao',
                'label' => "Situação",
                'type' => 'select_from_array',
                'options' => [1 => 'Ativo', 0 => 'Inativo'],
                'allows_null' => false,
            ],
        ];

        return $campos;
    }

    private function adicionaFiltroAntecipaGov()
    {
        $this->crud->addFilter(
            [
                'name' => 'utiliza_antecipagov',
                'type' => 'select2',
                'label' => 'Utiliza Antecipa GOV'
            ],
            ['false' => 'Não', 'true' => 'Sim'],
            function ($value) {
                $this->crud->addClause('where', 'unidades.utiliza_antecipagov', '=', $value);
            }
        );
    }

    private function adicionaFiltroSisg(){
        $this->crud->addFilter(
            [
                'name' => 'sisg',
                'type' => 'select2',
                'label' => 'Sisg'
            ],
            ['false' => 'Não', 'true' => 'Sim'],
            function ($value) {
                $this->crud->addClause('where', 'unidades.sisg', '=', $value);
            }
        );
    }

    private function adicionaFiltroUtilizaSiafi(){
        $this->crud->addFilter(
            [
                'name' => 'utiliza_siafi',
                'type' => 'select2',
                'label' => 'Utiliza Siafi'
            ],
            ['false' => 'Não', 'true' => 'Sim'],
            function ($value) {
                $this->crud->addClause('where', 'unidades.utiliza_siafi', '=', $value);
            }
        );
    }
    private function adicionaFiltroEsfera(){
        $this->crud->addFilter(
            [
                'name' => 'esfera',
                'type' => 'select2',
                'label' => 'Esfera'
            ],
            ['Federal' => 'Federal', 'Estadual' => 'Estadual', 'Municipal' => 'Municipal'],
            function ($value) {
                $this->crud->addClause('where', 'unidades.esfera', '=', $value);
            }
        );
    }
    private function adicionaFiltroSiasg(){
        $this->crud->addFilter(
            [
                'name' => 'aderiu_siasg',
                'type' => 'select2',
                'label' => 'Siasg'
            ],
            ['false' => 'Não', 'true' => 'Sim'],
            function ($value) {
                $this->crud->addClause('where', 'unidades.aderiu_siasg', '=', $value);
            }
        );
    }
    private function adicionaFiltroOrgao(){
        $this->crud->addFilter(
            [
                'name' => 'orgaonome',
                'type' => 'text',
                'label' => 'Orgão'
            ],
            null,
            function ($value) {
                $this->crud->query->whereHas('orgao', function($query) use ($value) {
                    $query->where('nome', 'ilike', "%$value%")
                        ->orWhere('codigo', 'ilike', "%$value%")
                        ->orWhere(DB::raw("CONCAT(codigo, ' - ', nome)"), 'ilike', "%$value%");
                });
            }
        );
    }

    public function store(StoreRequest $request)
    {
        if ($request->input('esfera') == 'Federal' && $request->input('sisg') == 1)
            $request->request->set('exclusivo_gestao_atas', 0);

        // Se for diferente de Esfera Municipal, UF São Paulo e município São Paulo, o despacho autorizatório deverá ser falso sempre
        if ($request->input('esfera') != 'Municipal' || $request->input('uf') != '35' ||  $request->input('municipio_id') != '4854')
            $request->request->set('despacho_autorizatorio', 0);

        $request->request->set('codigosiasg', $request->input('codigo'));
        $redirect_location = parent::storeCrud($request);

        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        if ($request->input('esfera') == 'Federal' && $request->input('sisg') == 1)
            $request->request->set('exclusivo_gestao_atas', 0);

        // Se for diferente de Esfera Municipal, UF São Paulo e município São Paulo, o despacho autorizatório deverá ser falso sempre
        if ($request->input('esfera') != 'Municipal' || $request->input('uf') != '35' ||  $request->input('municipio_id') != '4854')
            $request->request->set('despacho_autorizatorio', 0);


        $request->request->set('codigosiasg', $request->input('codigo'));
        $redirect_location = parent::updateCrud($request);

        return $redirect_location;
    }

    public function show($id)
    {
        $content = parent::show($id);

        $this->crud->removeColumn('orgao_id');
        $this->crud->removeColumn('tipo');
        $this->crud->removeColumn('municipio_id');

        return $content;
    }

    public function executaRotinaAlertaMensal()
    {
        $alerta = new AlertaContratoJob();

        if (backpack_user()) {
            Alert::success('Alerta Mensal executado com Sucesso!')->flash();
            return redirect('/admin/unidade');
        }
    }

    public function executaAtualizacaoCadastroUnidade()
    {
        if (!backpack_user()->hasRole('Administrador')) {
            abort('403', config('app.erro_permissao'));
        }

        $url = config('migracao.api_sta') . config('rotas-sta.estrutura-unidades');

        $urlFetchService = new STAUrlFetcherService($url);
        $dados = $urlFetchService->fetchDataJson();

        if ($urlFetchService->isError($dados)) {

            $error = $urlFetchService->getError($dados);

            if (backpack_user()) {
                Alert::error($error->error_message)->flash();
                return redirect('admin/unidade');
            }

        }

        foreach ($dados as $dado) {

            $cnpj = ('00000000000000' == $dado['cnpj']) ? null : $dado['cnpj'];

            $unidade = Unidade::where('codigo', $dado['codigo'])
                ->withTrashed()
                ->first();

            if (!isset($unidade->codigo)) {

                $orgao = Orgao::where('codigo', $dado['orgao'])
                    ->first();

                if (isset($orgao->id)) {
                    $novo = new Unidade();
                    $novo->orgao_id = $orgao->id;
                    $novo->codigo = $dado['codigo'];
                    $novo->gestao = $dado['gestao'];
                    $novo->codigosiasg = ($dado['funcao'] == 'Executora') ? $dado['codigo'] : '';
                    $novo->nome = $dado['nome'];
                    $novo->nomeresumido = $dado['nomeresumido'];
                    $novo->cnpj = $cnpj;
                    $novo->tipo = ($dado['funcao'] == 'Executora') ? 'E' : 'C';
                    $novo->situacao = true;
                    $novo->save();
                }

            } else {
                if ($unidade->nome != $dado['nome'] or
                    $unidade->nomeresumido != $dado['nomeresumido'] or
                    $unidade->orgao->codigo != $dado['orgao'] or
                    $unidade->gestao != $dado['gestao'] or
                    $unidade->cnpj != $cnpj) {

                    $orgao = Orgao::where('codigo', $dado['orgao'])
                        ->first();

                    if (isset($orgao->id)) {
                        $unidade->orgao_id = $orgao->id;
                        $unidade->gestao = $dado['gestao'];
                        $unidade->nome = $dado['nome'];
                        $unidade->nomeresumido = $dado['nomeresumido'];
                        $unidade->cnpj = $cnpj;
                        $unidade->save();
                    }

                }
            }
        }

        if (backpack_user()) {
            Alert::success('Atualização de Unidades executada com sucesso.')->flash();
            return redirect('admin/unidade');
        }
    }

    private function estadoId()
    {
        $estado = '';
        if ($this->crud->getActionMethod() === 'edit'
            && $this->crud->getEntry($this->crud->getCurrentEntryId())->municipio !== null
        ) {
            $estado = $this->crud->getEntry($this->crud->getCurrentEntryId())->municipio->estado->id;
        }
        return $estado;
    }

    public function executaAtualizacaoAntecipaGov()
    {
        $route = \Illuminate\Support\Facades\Route::current();
        $ids = $route->parameters()['ids'];
        $idsArray = explode(',', $ids);

        DB::beginTransaction();
        if ($route->parameters()['bool'] == 'desabilitar') {
            try {
                Unidade::whereIn('id', $idsArray)->update(['utiliza_antecipagov' => false]);
                DB::commit();
                return \Alert::success('Unidade alterada com sucesso!')->flash();

            } catch (Exception $e) {
                Log::info("Erro ao salvar utiliza_antecipagov = false - ids: $ids, mensagem: " . $e->getMessage());
                DB::rollBack();
                return "erro";
            }
        }

        if ($route->parameters()['bool'] == 'habilitar') {

            try {
                Unidade::whereIn('id', $idsArray)->update(['utiliza_antecipagov' => true]);
                DB::commit();
                return \Alert::success('Unidade alterada com sucesso!')->flash();

            } catch (Exception $e) {
                Log::info("Erro ao salvar utiliza_antecipagov = true - ids: $ids, mensagem: " . $e->getMessage());
                DB::rollBack();
                return "erro";
            }
        }
    }

}
