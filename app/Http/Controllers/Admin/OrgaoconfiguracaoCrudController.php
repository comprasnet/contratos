<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\OrgaoconfiguracaoRequest as StoreRequest;
use App\Http\Requests\OrgaoconfiguracaoRequest as UpdateRequest;
use App\Models\Orgao;
use App\Models\Unidade;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\CrudPanel;

// VALIDATION: change the requests to match your own file names if you need form validation

/**
 * Class OrgaoconfiguracaoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class OrgaoconfiguracaoCrudController extends CrudController
{
    public function setup()
    {
        $orgao_id = \Route::current()->parameter('orgao_id');

        $orgao = Orgao::find($orgao_id);
        if (!$orgao or (!(backpack_user()->hasRole('Administrador')) and (!backpack_user()->hasRole('Administrador Órgão')))) {
            abort('403', config('app.erro_permissao'));
        }

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Orgaoconfiguracao');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/admin/orgao/' . $orgao_id . '/configuracao');
        $this->crud->setEntityNameStrings('Configuração do Órgão', 'Configuração do Órgão');
        $this->crud->addButtonFromView('top', 'voltar', 'voltarorgao', 'end');


        $this->crud->addClause('where', 'orgao_id', '=', $orgao_id);
        $this->crud->addClause('where', 'orgao_id', '=', $orgao_id);
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */
        $this->crud->enableExportButtons();
        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');
        $this->crud->allowAccess('show');

        (backpack_user()->can('orgaosubcategorias_inserir')) ? $this->crud->allowAccess('create') : null;
        (backpack_user()->can('orgaosubcategorias_editar')) ? $this->crud->allowAccess('update') : null;
        (backpack_user()->can('orgaosubcategorias_deletar')) ? $this->crud->allowAccess('delete') : null;
        (backpack_user()->can('orgaosubcategorias_inserir')) ? $this->crud->allowAccess('delete') : null;

        (backpack_user()->hasRole('Administrador')) ? $this->crud->addButtonFromView('line', 'executarmigracao', 'executarmigracao', 'beginning') : null;

        $colunas = $this->Colunas();
        $this->crud->addColumns($colunas);

        $campos = $this->Campos($orgao_id);
        $this->crud->addFields($campos);

        // add asterisk for fields that are required in OrgaoconfiguracaoRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function colunas()
    {
        $colunas = [
            [
                'name' => 'getOrgao',
                'label' => 'Órgão', // Table column heading
                'type' => 'model_function',
                'function_name' => 'getOrgao', // the method in your Model
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
//                'searchLogic' => function (Builder $q, $column, $searchTerm) {
//                    $q->where('orgaos.codigo', 'like', "%".strtoupper($searchTerm)."%");
//                    $q->orWhere('orgaos.nome', 'like', "%".strtoupper($searchTerm)."%");
//                },
            ],
            [
                'name' => 'padrao_processo_marcara',
                'label' => 'Padrão Formato Processo',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'api_migracao_conta_url',
                'label' => 'URL API Migração',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'api_migracao_conta_token',
                'label' => 'Token API Migração',
                'type' => 'text',
                'limit' => 20,
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'getTipoMigracao',
                'label' => 'Tipo Migração', // Table column heading
                'type' => 'model_function',
                'function_name' => 'getTipoMigracao', // the method in your Model
                'orderable' => true,
                'visibleInTable' => false, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'url_sei',
                'label' => 'URL API SEI',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],

//            [
//                'name' => 'url_sip',
//                'label' => 'URL API SIP',
//                'type' => 'text',
//                'orderable' => true,
//                'visibleInTable' => true, // no point, since it's a large text
//                'visibleInModal' => true, // would make the modal too big
//                'visibleInExport' => true, // not important enough
//                'visibleInShow' => true, // sure, why not
//            ],
        ];

        return $colunas;
    }


    public function Campos($orgao)
    {
        $campos = [
            [ // select_from_array
                'name' => 'orgao_id',
                'type' => 'hidden',
                'value' => $orgao,
            ],
            [
                'name' => 'padrao_processo_marcara',
                'label' => 'Padrão Formato Processo',
                'type' => 'text',
                'default' => '99999.999999/9999-99',
                'tab' => 'Básico',
            ],
            [
                'name' => 'sistema_financeiro_externo',
                'label' => 'Sistema Financeiro Externo',
                'type' => 'text',
                'tab' => 'Básico',
            ],
            [
                'name' => 'codigo_sistema_externo_obrigatorio',
                'label' => "Código do contrato no Sistema Externo obrigatório?",
                'type' => 'radio',
                'options'     => [
                    1 => "Sim",
                    0 => "Não"
                ],
                'default' => 0,
                'inline' => true,
                'tab' => 'Básico',
            ],
            [
                'name' => 'api_migracao_conta_url',
                'label' => 'URL API Migração',
                'type' => 'text',
                'attributes' => [
                    'onkeyup' => "minusculo(this)"
                ],
                'tab' => 'Migração',
            ],
            [
                'name' => 'api_migracao_conta_token',
                'label' => 'Token API Migração',
                'type' => 'text',
                'tab' => 'Migração',
            ],
            [ // select_from_array
                'name' => 'tipomigracao',
                'label' => "Tipo Migração",
                'type' => 'select_from_array',
                'options' => ['CONTA' => 'Sistema Conta', "CCONTRATOS" => 'Contratos.gov.br'],
                'allows_null' => false,
                'tab' => 'Migração',
            ]
        ];

        if (backpack_user()->hasRole('Administrador')) {
            $campos = [
                [ // select_from_array
                    'name' => 'orgao_id',
                    'type' => 'hidden',
                    'value' => $orgao,
                ],
                [
                    'name' => 'padrao_processo_marcara',
                    'label' => 'Padrão Formato Processo',
                    'type' => 'text',
                    'default' => '99999.999999/9999-99',
                    'tab' => 'Básico',
                ],
                [
                    'name' => 'sistema_financeiro_externo',
                    'label' => 'Sistema Financeiro Externo',
                    'type' => 'text',
                    'tab' => 'Básico',
                ],
                [
                    'name' => 'codigo_sistema_externo_obrigatorio',
                    'label' => "Código do contrato no Sistema Externo obrigatório?",
                    'type' => 'radio',
                    'options'     => [
                        1 => "Sim",
                        0 => "Não"
                    ],
                    'default' => 0,
                    'inline' => true,
                    'tab' => 'Básico',
                ],
                [
                    'name' => 'api_migracao_conta_url',
                    'label' => 'URL API Migração',
                    'type' => 'text',
                    'attributes' => [
                        'onkeyup' => "minusculo(this)"
                    ],
                    'tab' => 'Migração',
                ],
                [
                    'name' => 'api_migracao_conta_token',
                    'label' => 'Token API Migração',
                    'type' => 'text',
                    'tab' => 'Migração',
                ],
                [ // select_from_array
                    'name' => 'tipomigracao',
                    'label' => "Tipo Migração",
                    'type' => 'select_from_array',
                    'options' => ['CONTA' => 'Sistema Conta', "CCONTRATOS" => 'Comprasnet Contratos'],
                    'allows_null' => false,
                    'tab' => 'Migração',
                ],
            ];
        }

        if (backpack_user()->hasRole('Administrador') || backpack_user()->hasRole('Administrador Órgão')) {

            $campos[] = [
                'name' => 'url_sei',
                'type' => 'teste_conexao_sei_sip',
                'attributes' => [
                    'onkeyup' => "minusculo(this)"
                ],
                'tab' => 'Integração SEI',
            ];
            $campos[] = [
                'name' => 'chave_acesso_sei',
                'label' => "Chave de Acesso",
                'type' => 'teste_conexao_sei_sip_chaveacesso',
                'attributes' => [
                    'onkeyup' => "minusculo(this)"
                ],
                'tab' => 'Integração SEI',
            ];
        }

        return $campos;
    }

    public function store(StoreRequest $request)
    {
        if (backpack_user()->unidade->sisg) {
            $request->request->set('padrao_processo_marcara', env('FORMATO_PROCESSO_PADRAO_SISG', '99999.999999/9999-99'));
        }

        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        if (backpack_user()->unidade->sisg) {
            $request->request->set('padrao_processo_marcara', env('FORMATO_PROCESSO_PADRAO_SISG', '99999.999999/9999-99'));
        }
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
