<?php

namespace App\Http\Controllers\Admin;

use App\Http\Traits\BuscaCodigoItens;
use App\Models\Arp;
use App\Models\ArpItem;
use App\Models\Codigo;
use App\Models\Codigoitem;
use App\Models\Compra;
use App\Models\CompraItem;
use App\Models\CompraItemFornecedor;
use App\Models\CompraItemMinutaEmpenho;
use App\Models\CompraItemUnidade;
use App\Models\CompraItemUnidadeLocalEntrega;
use App\Models\ContratoItemMinutaEmpenho;
use App\Models\MinutaEmpenho;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CompraRequest as StoreRequest;
use App\Http\Requests\CompraRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

/**
 * Class CompraCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class CompraCrudController extends CrudController
{
    use BuscaCodigoItens;
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        if (!backpack_user()->hasRole('Administrador') && !backpack_user()->hasRole('Desenvolvedor')) {
            abort('403', config('app.erro_permissao'));
        }

        $this->crud->setModel(Compra::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/admin/compra');
        $this->crud->setEntityNameStrings('Compra', 'Compras');
        $this->crud->setShowView('vendor.backpack.crud.compras.show');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */
        $this->crud->enableExportButtons();

        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');
        $this->crud->allowAccess('show');

        $this->crud->addClause('select', [
            DB::raw("unidades.codigo || ' - ' || unidades.nomeresumido as unidade_origem"),
            DB::raw("beneficiaria.codigo || ' - ' || beneficiaria.nomeresumido as unidade_beneficiaria"),
            DB::raw("subrrogada.codigo || ' - ' || subrrogada.nomeresumido as unidade_subrrogada"),
            DB::raw('modalidade.descricao as modalidade'),
            DB::raw('tipo_compra.descricao as tipo_compra'),
            DB::raw("CASE
                                        WHEN compras.origem = 1
                                            THEN 'NDC'
                                        ELSE 'SIASG'
                                    END  AS compra_origem"),
            // Tabela principal deve ser sempre a última da listagem!
            'compras.*'
        ]);

        $this->crud->addClause(
            'join',
            'unidades',
            'unidades.id',
            '=',
            'compras.unidade_origem_id'
        );

        $this->crud->addClause(
            'join',
            DB::raw('codigoitens modalidade'),
            DB::raw('modalidade.id'),
            '=',
            DB::raw("compras.modalidade_id")
        );

        $this->crud->addClause(
            'join',
            DB::raw('codigoitens tipo_compra'),
            DB::raw('tipo_compra.id'),
            '=',
            DB::raw("compras.tipo_compra_id")
        );

        $this->crud->addClause(
            'leftJoin',
            DB::raw('unidades beneficiaria'),
            DB::raw('beneficiaria.id'),
            '=',
            DB::raw("compras.uasg_beneficiaria_id")
        );

        $this->crud->addClause(
            'leftJoin',
            DB::raw('unidades subrrogada'),
            DB::raw('subrrogada.id'),
            '=',
            DB::raw("compras.unidade_subrrogada_id")
        );

        $this->crud->addClause('orderBy', 'id', 'DESC');
        $this->crud->addClause('distinct');

//        dd(str_replace_array('?', $this->crud->query->getBindings(), $this->crud->query->toSql()));

        $this->adicionaCampos();
        $this->adicionaColunas();
        $this->aplicaFiltros();
    }

    protected function adicionaCampos(): void
    {
    }

    protected function adicionaColunas(): void
    {
        $this->adicionaColunaUnidadeOrigem();
        $this->adicionaColunaUnidadeSubrrogada();
        $this->adicionaColunaUnidadeBeneficiaria();
        $this->adicionaColunaModalidade();
        $this->adicionaColunaTipoCompra();
        $this->adicionaColunaNumeroAnoCompra();
        $this->adicionaColunaIncisoCompra();
        $this->adicionaColunaLeiCompra();
        $this->adicionaColunaOrigem();
    }

    protected function adicionaColunaUnidadeOrigem(): void
    {
        $this->crud->addColumn([
            'name'            => 'unidade_origem',
            'label'           => 'Unidade Origem',
            'type'            => 'text',
            'orderable'       => true,
            'visibleInTable'  => true,
            'visibleInModal'  => true,
            'visibleInExport' => true,
            'visibleInShow'   => true,
            'searchLogic' => function (Builder $query, $column, $searchTerm) {
                $query->orWhere('unidades.codigo', 'like', "%$searchTerm%");
            },
        ]);
    }

    protected function adicionaColunaUnidadeSubrrogada(): void
    {
        $this->crud->addColumn([
            'name'            => 'unidade_subrrogada',
            'label'           => 'Subrrogada',
            'type'            => 'text',
            'orderable'       => true,
            'visibleInTable'  => false,
            'visibleInModal'  => true,
            'visibleInExport' => true,
            'visibleInShow'   => true,
        ]);
    }

    protected function adicionaColunaUnidadeBeneficiaria(): void
    {
        $this->crud->addColumn([
            'name'            => 'unidade_beneficiaria',
            'label'           => 'Beneficiaria',
            'type'            => 'text',
            'orderable'       => true,
            'visibleInTable'  => false,
            'visibleInModal'  => true,
            'visibleInExport' => true,
            'visibleInShow'   => true,
        ]);
    }

    protected function adicionaColunaModalidade(): void
    {
        $this->crud->addColumn([
            'name'            => 'modalidade',
            'label'           => 'Modalidade',
            'type'            => 'text',
            'priority'        => 2,
            'orderable'       => true,
            'visibleInTable'  => true,
            'visibleInModal'  => true,
            'visibleInExport' => true,
            'visibleInShow'   => true
        ]);
    }

    protected function adicionaColunaTipoCompra(): void
    {
        $this->crud->addColumn([
            'name'            => 'tipo_compra',
            'label'           => 'Tipo',
            'type'            => 'text',
            'orderable'       => true,
            'visibleInTable'  => true,
            'visibleInModal'  => true,
            'visibleInExport' => true,
            'visibleInShow'   => true,
        ]);
    }

    public function adicionaColunaNumeroAnoCompra(): void
    {
        $this->crud->addColumn([
            'name'            => 'numero_ano',
            'label'           => 'Numero/Ano',
            'type'            => 'text',
            'priority'        => 2,
            'orderable'       => true,
            'visibleInTable'  => true,
            'visibleInModal'  => true,
            'visibleInExport' => true,
            'visibleInShow'   => true,
            'searchLogic' => function ($query, $column, $searchTerm) {
                $query->orWhere(
                    DB::raw("REPLACE(numero_ano,'/','')"),
                    'ilike',
                    DB::raw("REPLACE('%$searchTerm%', '/', '')")
                );
            }
        ]);
    }

    public function adicionaColunaOrigem(): void
    {
        $this->crud->addColumn([
            'name'            => 'compra_origem',
            'label'           => 'Origem',
            'type'            => 'text',
            'priority'        => 2,
            'orderable'       => true,
            'visibleInTable'  => true,
            'visibleInModal'  => true,
            'visibleInExport' => true,
            'visibleInShow'   => true
        ]);
    }

    protected function adicionaColunaIncisoCompra(): void
    {
        $this->crud->addColumn([
            'name'            => 'inciso',
            'label'           => 'Inciso',
            'type'            => 'text',
            'orderable'       => true,
            'visibleInTable'  => true,
            'visibleInModal'  => false,
            'visibleInExport' => false,
            'visibleInShow'   => true,
        ]);
    }

    protected function adicionaColunaLeiCompra()
    {
        $this->crud->addColumn([
            'name'            => 'lei',
            'label'           => 'Lei',
            'type'            => 'text',
            'orderable'       => true,
            'visibleInTable'  => true,
            'visibleInModal'  => true,
            'visibleInExport' => true,
            'visibleInShow'   => true,
        ]);
    }

    protected function aplicaFiltros(): void
    {
        $this->aplicaFiltroUnidadeOrigem();
        $this->aplicaFiltroModalidade();
        $this->aplicaFiltroTipo();
        $this->aplicaFiltroLei();
        $this->aplicaFiltroOrigem();
        $this->aplicaFiltroFornecedor();
        $this->aplicaFiltroMinuta();
        $this->aplicaFiltroSubrrogada();
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function show($id)
    {
        $content = parent::show($id);

        $this->crud->removeColumn('unidade_origem_id');
        $this->crud->removeColumn('unidade_subrrogada_id');
        $this->crud->removeColumn('modalidade_id');
        $this->crud->removeColumn('tipo_compra_id');
        $this->crud->removeColumn('uasg_beneficiaria_id');
        $this->crud->removeColumn('origem');
        $this->crud->removeColumn('compra_origem');

        $this->crud->addColumn([
            'name' => 'unidade_origem',
            'label' => 'Unidade Origem',
            'type' => 'model_function',
            'function_name' => 'getUnidadeOrigem',
            'visibleInShow' => true
        ]);

        $this->crud->addColumn([
            'name' => 'unidade_subrrogada',
            'label' => 'Subrrogada',
            'type' => 'model_function',
            'function_name' => 'getUnidadeSubrrogada',
            'visibleInShow' => true
        ]);

        $this->crud->addColumn([
            'name' => 'unidade_beneficiaria',
            'label' => 'Beneficiaria',
            'type' => 'model_function',
            'function_name' => 'getUnidadeBeneficiaria',
            'visibleInShow' => true
        ]);

        $this->crud->addColumn([
            'name' => 'modalidade',
            'label' => 'Modalidade',
            'type' => 'model_function',
            'function_name' => 'getModalidade',
            'visibleInShow' => true
        ]);

        $this->crud->addColumn([
            'name' => 'tipo_compra',
            'label' => 'Tipo',
            'type' => 'text',
            'visibleInShow' => true
        ]);

        $this->crud->addColumn([
            'name' => 'compra_origem_desc',
            'label' => 'Origem',
            'type' => 'model_function',
            'function_name' => 'getCompraOrigemDescAttribute',
            'visibleInShow' => true
        ]);

        $this->addColumnCompraItens();
        $this->addColumnCompraItemUnidades();
        $this->addColumnLocaisEntrega();
        $this->addColumnEnderecos();
        $this->addColumnCompraItemFornecedores();
        $this->addColumnContratoItemMinutaEmpenho();
        $this->addColumnCompraItemMinutaEmpenho();
        $this->addColumnMinutas();
        $this->addColumnArpItem();
        $this->addColumnAtas();

        return $content;
    }

    private function aplicaFiltroUnidadeOrigem(): void
    {
        $campo = [
            'name' => 'unidade_origem',
            'type' => 'text',
            'label' => 'Unidade Origem'
        ];

        $this->crud->addFilter(
            $campo,
            false,
            function ($value) {
                $this->crud->query->where(function ($query) use ($value) {
                    $query->where('unidades.nome', 'ILIKE', "%{$value}%")
                        ->orWhere('unidades.nomeresumido', 'ILIKE', "%{$value}%")
                        ->orWhere('unidades.codigo', '=', $value);
                });
            }
        );
    }
    private function aplicaFiltroModalidade(): void
    {
        $campo = [
            'name' => 'modalidade',
            'type' => 'select2_multiple',
            'label' => 'Modalidade'
        ];

        $tipos = [
             73 => '01 - Convite',
             77 => '02 - Tomada de Preços',
             71 => '03 - Concorrência',
            187 => '04 - Concorrência Internacional',
             76 => '05 - Pregão',
             74 => '06 - Dispensa',
             75 => '07 - Inexigibilidade',
             72 => '20 - Concurso',
            184 => '22 - Tomada de Preços por Técnica e Preço',
            185 => '33 - Concorrência por Técnica e Preço',
            186 => '44 - Concorrência Internacional por Técnica e Preço',
            160 => '99 - Regime Diferenciado de Contratação',

        ];

        $this->crud->addFilter(
            $campo,
            $tipos,
            function ($value) {
                $this->crud->addClause(
                    'whereIn',
                    'compras.modalidade_id',
                    json_decode($value)
                );
            }
        );
    }
    private function aplicaFiltroTipo(): void
    {
        $campo = [
            'name' => 'tipo',
            'type' => 'select2',
            'label' => 'Tipo'
        ];

        $tipos = $this->retornaTiposParaCombo();

        $this->crud->addFilter(
            $campo,
            $tipos,
            function ($value) {
                $this->crud->addClause('where', 'tipo_compra.id', $value);
            }
        );
    }
    private function aplicaFiltroSubrrogada(): void
    {
        $campo = [
            'name' => 'subrrogada',
            'type' => 'select2',
            'label' => 'Subrrogada'
        ];

        $tipos = [1 => 'Sim', 0 => 'Não'];

        $this->crud->addFilter(
            $campo,
            $tipos,
            function ($value) {
                if ($value == '1') {
                    $this->crud->addClause("where", function ($query) {
                        $query->whereNotNull("unidade_subrrogada_id");
                    });
                } else {
                    $this->crud->addClause("where", function ($query) {
                        $query->whereNull("unidade_subrrogada_id");
                    });
                }
            }
        );
    }

    private function aplicaFiltroLei(): void
    {
        $campo = [
            'name' => 'lei',
            'type' => 'select2_multiple',
            'label' => 'Lei'
        ];

        $leis = $this->retornaLeisParaCombo();

        $this->crud->addFilter(
            $campo,
            $leis,
            function ($value) {
                $this->crud->addClause(
                    'whereIn',
                    'compras.lei',
                    json_decode($value)
                );
            }
        );
    }

    private function aplicaFiltroOrigem(): void
    {
        $campo = [
            'name' => 'origem',
            'type' => 'select2',
            'label' => 'Origem'
        ];

        $tipos = [1 => 'NDC', 2 => 'Siasg'];

        $this->crud->addFilter(
            $campo,
            $tipos,
            function ($value) {
                $this->crud->addClause('where', 'compras.origem', $value);
            }
        );
    }

    /**
     * Adiciona o filtro ao campo Fornecedor
     *
     * @author Saulo Soares <saulosao@gmail.com>
     */
    protected function aplicaFiltroFornecedor(): void
    {
        $this->crud->addFilter(
            [
                'type' => 'text',
                'name' => 'fornecedor',
                'label' => 'Fornecedor'
            ],
            false,
            function ($value) {
                $this->crud->query->join('compra_items', 'compra_items.compra_id', '=', 'compras.id');
                $this->crud->query->join('compra_item_fornecedor', 'compra_item_fornecedor.compra_item_id', '=', 'compra_items.id');
                $this->crud->query->join('fornecedores', 'fornecedores.id', '=', 'compra_item_fornecedor.fornecedor_id');

                $this->crud->query->where(function ($query) use ($value) {
                    $query->where(
                        DB::raw("REPLACE(REPLACE(REPLACE(fornecedores.cpf_cnpj_idgener,
                         '.', ''), '-', ''), '/', '')::TEXT"),
                        'ilike',
                        DB::raw("REPLACE(REPLACE(REPLACE('{$value}', '.', ''), '-', ''), '/', '')::TEXT")
                    )
                        ->orWhere('fornecedores.nome', 'ILIKE', "%{$value}%");
                });
            }
        );
    }

    /**
     * Adiciona o filtro ao campo Fornecedor
     *
     * @author Saulo Soares <saulosao@gmail.com>
     */
    protected function aplicaFiltroMinuta(): void
    {
        $this->crud->addFilter(
            [
                'type' => 'text',
                'name' => 'minuta',
                'label' => 'Minuta',
                'title' => 'Pode ser usado tanto o ID como o N° da Minuta',
                'placeholder' => 'ID/Número da Minuta'
            ],
            false,
            function ($value) {
                $this->crud->query->join('minutaempenhos', 'minutaempenhos.compra_id', '=', 'compras.id');

                $this->crud->query->where(function ($query) use ($value) {
                    if (is_numeric($value)) {
                        $query->where('minutaempenhos.id', '=', $value);
                        return;
                    }

                    $query->where('minutaempenhos.mensagem_siafi', '=', $value);

                });
            }
        );
    }

    private function retornaTiposParaCombo()
    {
        return $this->retornaArrayCodigosItens('Tipo Compra');
    }

    private function retornaLeisParaCombo()
    {
        return Compra::select('lei as id', 'lei')
            ->distinct()
            ->orderBy('lei','asc')
            ->get()
            ->pluck('lei', 'lei')
            ->toArray();
    }

    /**
     * @return void
     */
    public function addColumnCompraItens(): void
    {
        $compraID = request()->route('compra');
        $compraItens = collect(DB::select(DB::raw("
        SELECT *
            FROM (
                SELECT
                    compra_items.*,
                    codigoitens.descricao AS tipo_item_compra,
                    CASE
                        WHEN citens.descricao_complementar IS NULL THEN UPPER(compra_items.descricaodetalhada)
                        ELSE UPPER(citens.descricao_complementar)
                    END AS descricaodetalhada,
                    CASE
                        WHEN compra_items.permite_carona = true THEN 'Sim'
                        WHEN compra_items.permite_carona = false THEN 'Não'
                    END AS permite_carona,
                    -- Gera uma numeração sequencial (ROW_NUMBER) para os itens agrupados por compra_items.id
                    -- Ordena pela descrição complementar de contrato, colocando os itens sem descrição complementar por último
                    ROW_NUMBER() OVER (
                        PARTITION BY compra_items.id  -- Agrupa os itens pelo id do compra_item
                        ORDER BY citens.descricao_complementar NULLS LAST  -- Ordena pela descrição complementar, considerando NULLs por último
                    ) AS row_num -- Atribui um número sequencial para cada linha do grupo
                FROM compra_items
                INNER JOIN codigoitens ON codigoitens.id = compra_items.tipo_item_id
                JOIN compras ON compra_items.compra_id = compras.id
                LEFT JOIN contratos ON compras.modalidade_id = contratos.modalidade_id
                    AND compras.unidade_origem_id = contratos.unidadecompra_id
                    AND compras.numero_ano = contratos.licitacao_numero
                LEFT JOIN contratoitens citens ON contratos.id = citens.contrato_id
                    AND citens.numero_item_compra = compra_items.numero
                WHERE compra_items.compra_id = $compraID
                    AND compra_items.deleted_at IS NULL
            ) subquery
            -- Filtra para pegar apenas o primeiro item de cada grupo, considerando a numeração gerada
            -- Assim não gerando duplicidade de itens considerando que o primeiro item que vem sempre é o correto
            WHERE row_num = 1
            ORDER BY id ASC
        ")));

        $compraItens = $compraItens->map(function ($item) {
            $item = (array) $item;
            $item['situacao'] = $item['situacao'] ? 'Ativo' : 'Inativo';
            $item['botaoAtivarItem'] = '';
            if (backpack_user()->hasRole('Administrador')) {
                $item['botaoAtivarItem'] = ($item['situacao'] == 'Ativo')
                    ? "<a href='/admin/compra/ativar-inativar-compra-item/compra-item?id={$item['id']}&rota=item&inativar=1'>Inativar</a>"
                    : "<a href='/admin/compra/ativar-inativar-compra-item/compra-item?id={$item['id']}&rota=item&inativar=0'>Ativar</a>";
            }
            return $item;
        })->toArray();

        $this->crud->addColumn([
            'name' => 'itens',
            'label' => 'Compra Itens',
            'type' => 'compras_table',
            'values' => $compraItens,
            'table_id' => 'compra_item',
            'columns' => [
                'botaoAtivarItem' => 'Ativar/Inativar Item',
                'id' => 'CompraItemID',
                'numero' => 'NumeroItem',
                'situacao' => 'Situacao',
                'tipo_item_compra' => 'Tipo',
                'descricaodetalhada' => 'Descricaodetalhada',
                'qtd_total' => 'Qtd_total',
                'permite_carona' => 'Permite carona',
                'maximo_adesao' => 'Qtd. limite adesão',
                'maximo_adesao_informado_compra' => 'Qtd. limite informado compra',
                'created_at' => 'Created_at',
                'updated_at' => 'Updated_at',
                'ata_vigencia_inicio' => 'Ata_vigencia_inicio',
                'ata_vigencia_fim' => 'Ata_vigencia_fim',
                'criterio_julgamento' => 'Criterio_julgamento',
                'codigo_ncmnbs' => 'Código NCM/NBS',
                'aplica_margem_ncmnbs' => 'Aplica Margem Ncmnbs',
                'descricao_ncmnbs' => 'Descricao Ncmnbs',
            ],
            'visibleInShow' => true
        ]);
    }

    /**
     * @return void
     */
    public function addColumnCompraItemUnidades(): void
    {
        $compraItensUnidades = CompraItemUnidade::where('compras.id', '=', $this->crud->entry->id)
            ->join('compra_items', 'compra_item_unidade.compra_item_id', '=', 'compra_items.id')
            ->join('compras', 'compra_items.compra_id', '=', 'compras.id')
            ->join('unidades', 'compra_item_unidade.unidade_id', '=', 'unidades.id')
            ->leftJoin('fornecedores', 'compra_item_unidade.fornecedor_id', '=', 'fornecedores.id')
            ->select('compra_item_unidade.*', 'unidades.codigo', 'fornecedores.cpf_cnpj_idgener', 'compra_items.numero')
            ->orderBy('unidades.codigo')
            ->orderBy('compra_items.id')
            ->get()->map(function ($item) {
                $item['situacao'] = $item->situacao ? 'Ativo' : 'Inativo';
                $item['botaoAtivarItem'] = '';
                if(backpack_user()->hasRole('Administrador')) {
                    $item['botaoAtivarItem'] = ($item->situacao == 'Ativo')
                        ? "<a href='/admin/compra/ativar-inativar-compra-item/compra-item?id={$item->id}&rota=ciu&inativar=1'>Inativar</a>"
                        : "<a href='/admin/compra/ativar-inativar-compra-item/compra-item?id={$item->id}&rota=ciu&inativar=0'>Ativar</a>";
                }
                return $item;
            })->toArray();

        $this->crud->addColumn([
            'name' => 'compraItensUnidades',
            'label' => 'Compra Itens Unidade',
            'type' => 'compras_table',
            'table_id' => 'compra_item_unidade',
            'values' => $compraItensUnidades,
            'columns' => [
                'botaoAtivarItem' => 'Ativar/Inativar Item',
                'compra_item_id' => 'CompraItemID',
                'numero' => 'NumeroItem',
                'id' => 'CompraItemUnidadeId',
                'situacao' => 'Situacao',
                'codigo' => 'Unidade',
                'fornecedor_id' => 'Fornecedor_id',
                'cpf_cnpj_idgener' => 'CPF/CNPJ/IDGENER',
                'quantidade_autorizada' => 'Quantidade_autorizada',
                'quantidade_saldo' => 'Quantidade_saldo',
                'tipo_uasg' => 'Tipo_uasg',
                'created_at' => 'Created_at',
                'updated_at' => 'Updated_at',
                'quantidade_adquirir' => 'Quantidade_adquirir',
                'quantidade_adquirida' => 'Quantidade_adquirida',
                'quantidade_saldo_contratado' => 'Quantidade_saldo_contratado',
            ],
            'visibleInShow' => true
        ]);
    }

    /**
     * @return void
     */
    public function addColumnLocaisEntrega(): void
    {
        $ciule = CompraItemUnidadeLocalEntrega::join(
            'compra_item_unidade',
            'compra_item_unidade_local_entrega.compra_item_unidade_id',
            '=',
            'compra_item_unidade.id'
        )
            ->join('compra_items', 'compra_item_unidade.compra_item_id', '=', 'compra_items.id')
            ->join('unidades as entrega', 'entrega.id', '=', 'compra_item_unidade_local_entrega.unidade_entrega_id')
            ->select(
                'compra_item_unidade_local_entrega.id as local_entrega_id',
                'compra_item_unidade_local_entrega.endereco_id',
                'compra_item_unidade_local_entrega.compra_item_unidade_id',
                'compra_item_unidade_local_entrega.quantidade',
                'compra_item_unidade_local_entrega.endereco_id_novo_divulgacao',
                'compra_item_unidade_local_entrega.created_at as ciu_created_at',
                'compra_item_unidade_local_entrega.updated_at as ciu_updated_at',
                'entrega.codigo as unidade_entrega'
            )
            ->where('compra_id', $this->crud->entry->id)
            ->get()
            ->toArray();
//        dd($ciule);

        $this->crud->addColumn([
            'name' => 'localEntrega',
            'label' => 'Locais de Entrega',
            'type' => 'compras_table',
            'table_id' => 'compra_item_unidade_local_entrega',
            'values' => $ciule,
            'columns' => [
                'local_entrega_id'            => 'LocalEntregaId',
                'compra_item_unidade_id'      => 'CompraItemUnidadeId',
                'endereco_id'                 => 'EnderecoId',
                'quantidade'                  => 'Quantidade',
                'endereco_id_novo_divulgacao' => 'Id_novo_divulgacao',
                'ciu_created_at'              => 'Created_at',
                'ciu_updated_at'              => 'Updated_at',
                'unidade_entrega'             => 'Unidade Entrega'
            ],
            'visibleInShow' => true
        ]);

    }
    /**
     * @return void
     */
    public function addColumnEnderecos(): void
    {
        $enderecos = CompraItemUnidadeLocalEntrega::join(
            'enderecos',
            'enderecos.id',
            '=',
            'compra_item_unidade_local_entrega.endereco_id'
        )
            ->join('municipios', 'enderecos.municipios_id', '=', 'municipios.id')
            ->join('estados', 'municipios.estado_id', '=', 'estados.id')
            ->join(
                'compra_item_unidade',
                'compra_item_unidade_local_entrega.compra_item_unidade_id',
                '=',
                'compra_item_unidade.id'
            )
            ->join('compra_items', 'compra_item_unidade.compra_item_id', '=', 'compra_items.id')
            ->distinct()
            ->select(
                'enderecos.id as endereco_id',
                'municipios.codigo_ibge',
                'municipios.nome as municipio',
                'estados.sigla as uf',
                'enderecos.cep',
                'enderecos.bairro',
                'enderecos.logradouro',
                'enderecos.complemento',
                'enderecos.numero',
                'enderecos.created_at as enderecos_created_at',
                'enderecos.updated_at as enderecos_updated_at',
                'enderecos.codigo_municipio_ndc'
            )
            ->where('compra_id', $this->crud->entry->id)
            ->get()
            ->toArray();

        $this->crud->addColumn([
            'name' => 'enderecos',
            'label' => 'Enderecos',
            'type' => 'compras_table',
            'table_id' => 'enderecos',
            'values' => $enderecos,
            'columns' => [
                'endereco_id'          => 'EnderecoId',
                'codigo_ibge'          => 'Ibge',
                'municipio'            => 'Municipio',
                'uf'                   => 'UF',
                'cep'                  => 'CEP',
                'bairro'               => 'Bairro',
                'logradouro'           => 'Logradouro',
                'complemento'          => 'Complemento',
                'numero'               => 'Número',
                'enderecos_created_at' => 'Created_at',
                'enderecos_updated_at' => 'Updated_at',
                'codigo_municipio_ndc' => 'Codigo_municipio_ndc',
            ],
            'visibleInShow' => true
        ]);
    }

    /**
     * @return void
     */
    public function addColumnCompraItemFornecedores(): void
    {
        $compraItensFornecedores = CompraItemFornecedor::where('compras.id', '=', $this->crud->entry->id)
            ->join('compra_items', 'compra_item_fornecedor.compra_item_id', '=', 'compra_items.id')
            ->join('compras', 'compra_items.compra_id', '=', 'compras.id')
            ->select('compra_item_fornecedor.*', 'compra_items.numero')
            ->orderBy('compra_items.id')
            ->orderBy('classificacao', 'asc')
            ->get()->map(function ($item) {
                $item['situacao'] = $item->situacao ? 'Ativo' : 'Inativo';
                $item['botaoAtivarItem'] = '';
                if(backpack_user()->hasRole('Administrador')) {
                    $item['botaoAtivarItem'] = ($item->situacao == 'Ativo')
                        ? "<a href='/admin/compra/ativar-inativar-compra-item/compra-item?id={$item->id}&rota=forn&inativar=1'>Inativar</a>"
                        : "<a href='/admin/compra/ativar-inativar-compra-item/compra-item?id={$item->id}&rota=forn&inativar=0'>Ativar</a>";
                }
                return $item;
            })->toArray();

        $this->crud->addColumn([
            'name' => 'compraItensFornecedores',
            'label' => 'Compra Itens Fornecedores',
            'type' => 'compras_table',
            'table_id' => 'compra_item_fornecedor',
            'values' => $compraItensFornecedores,
            'columns' => [
                'botaoAtivarItem' => 'Ativar/Inativar Item',
                'compra_item_id' => 'CompraItemID',
                'numero' => 'NumeroItem',
                'id' => 'CompraItemFornecedorId',
                'situacao' => 'Situacao',
                'fornecedor_id' => 'Fornecedor_id',
                'ni_fornecedor' => 'Ni_fornecedor',
                'classificacao' => 'Classificacao',
                'situacao_sicaf' => 'Situacao_sicaf',
                'quantidade_homologada_vencedor' => 'Quantidade_homologada_vencedor',
                'valor_unitario' => 'Valor_unitario',
                'valor_negociado' => 'Valor_negociado',
                'quantidade_empenhada' => 'Quantidade_empenhada',
                'percentual_maior_desconto' => 'Percentual_maior_desconto',
                'ata_vigencia_inicio' => 'Ata_vigencia_inicio',
                'ata_vigencia_fim' => 'Ata_vigencia_fim',
                'created_at' => 'Created_at',
                'updated_at' => 'Updated_at',
            ],
            'visibleInShow' => true
        ]);
    }

    /**
     * @return void
     */
    public function addColumnMinutas(): void
    {
        $minutas = MinutaEmpenho::where('compras.id', '=', $this->crud->entry->id)
            ->join('unidades', 'minutaempenhos.unidade_id', '=', 'unidades.id')
            ->leftJoin('saldo_contabil', 'minutaempenhos.saldo_contabil_id', '=', 'saldo_contabil.id')
            ->leftJoin(DB::raw('unidades emitente'), DB::raw('emitente.id'), '=', 'saldo_contabil.unidade_id')
            ->join('compras', 'minutaempenhos.compra_id', '=', 'compras.id')
            ->join(
                DB::raw('unidades unidade_compra'),
                DB::raw('unidade_compra.id'),
                '=',
                DB::raw("compras.unidade_origem_id")
            )
            ->join(
                DB::raw('codigoitens modalidade'),
                DB::raw('modalidade.id'),
                '=',
                DB::raw("compras.modalidade_id")
            )
            ->join(
                DB::raw('codigoitens tipo_minuta'),
                DB::raw('tipo_minuta.id'),
                '=',
                DB::raw("minutaempenhos.tipo_empenhopor_id")
            )
            ->join(
                DB::raw('codigoitens situacao_minuta'),
                DB::raw('situacao_minuta.id'),
                '=',
                DB::raw("minutaempenhos.situacao_id")
            )
            ->select(
                'minutaempenhos.*',
                DB::raw("unidades.codigo || ' - ' || unidades.nomeresumido as unidade_gestora"),
                DB::raw("emitente.codigo || ' - ' || emitente.nomeresumido as unidade_emitente"),
                DB::raw("unidade_compra.codigo || ' - ' || unidade_compra.nomeresumido as unidade_compra"),
                DB::raw('compras.numero_ano as compras_numero_ano'),
                DB::raw('modalidade.descricao as modalidade'),
                DB::raw('tipo_minuta.descricao as tipo_minuta'),
                DB::raw('situacao_minuta.descricao as situacao_minuta'),
                DB::raw("'<a href=\"" . url('empenho/minuta') . "/'|| minutaempenhos.id || '\" " .
                    "class=\"btn btn-xs btn-default\"  ><i class=\"fa fa-eye\"></i></a>' as link"),
            )
            ->orderBy('minutaempenhos.id')->get()->toArray();

        $this->crud->addColumn([
            'name' => 'minutas',
            'label' => 'Minutas',
            'type' => 'compras_table',
            'table_id' => 'minuta',
            'values' => $minutas,
            'columns' => [
                'id' => 'Minuta_id',
                'link' => 'Link',
                'tipo_minuta' => 'Tipo Minuta',
                'situacao_minuta' => 'Situação Minuta',
                'mensagem_siafi' => 'Mensagem Siafi',
                'unidade_gestora' => 'Unidade Gestora',
                'unidade_emitente' => 'Unidade Emitente',
                'unidade_compra' => 'Unidade Compra',
                'fornecedor_empenho_id' => 'Fornecedor_ID Empenho',
            ],
            'visibleInShow' => true
        ]);
    }

    public function addColumnCompraItemMinutaEmpenho(): void
    {
        $cime = CompraItemMinutaEmpenho::where('compras.id', '=', $this->crud->entry->id)
            ->join('compra_items', 'compra_item_minuta_empenho.compra_item_id', '=', 'compra_items.id')
            ->join('minutaempenhos', 'compra_item_minuta_empenho.minutaempenho_id', '=', 'minutaempenhos.id')
            ->join('compras', 'minutaempenhos.compra_id', '=', 'compras.id')
            ->join('codigoitens', 'compra_item_minuta_empenho.operacao_id', '=', 'codigoitens.id')
            ->leftjoin('naturezasubitem', 'naturezasubitem.id', '=', 'compra_item_minuta_empenho.subelemento_id')
            ->select(
                'compra_item_minuta_empenho.*',
                'codigoitens.descricao',
                'naturezasubitem.descricao as subitem'
            )
            ->orderBy('minutaempenhos.id', 'asc')
            ->orderBy('compra_item_minuta_empenho.id', 'asc')
            ->get()->toArray();

        $this->crud->addColumn([
            'name' => 'CompraItemMinutaEmpenho',
            'label' => 'Compra Item Minuta Empenho',
            'type' => 'compras_table',
            'table_id' => 'compra_item_minuta_empenho',
            'values' => $cime,
            'columns' => [
                'id' => 'CompraItemMinutaEmpenhoID',
                'compra_item_id' => 'CompraItemID',
                'minutaempenho_id' => 'minutaempenho_id',
                'compra_item_fornecedor_id' => 'compra_item_fornecedor_id',
                'compra_item_unidade_id' => 'compra_item_unidade_id',
                'subitem' => 'Sub Item',
                'quantidade' => 'Quantidade',
                'valor' => 'Valor',
                'descricao' => 'Operação',
                'created_at' => 'created_at',
             //   'updated_at ' => 'updated_at',
                'minutaempenhos_remessa_id' => 'minutaempenhos_remessa_id',
                'numseq' => 'numseq'
            ],
            'visibleInShow' => true
        ]);
    }

    public function addColumnContratoItemMinutaEmpenho(): void
    {
        $cime = ContratoItemMinutaEmpenho::where('compras.id', '=', $this->crud->entry->id)
            ->join('contratoitens', 'contrato_item_minuta_empenho.contrato_item_id', '=', 'contratoitens.id')
            ->join('minutaempenhos', 'contrato_item_minuta_empenho.minutaempenho_id', '=', 'minutaempenhos.id')
            ->join('compras', 'minutaempenhos.compra_id', '=', 'compras.id')
            ->join('codigoitens', 'contrato_item_minuta_empenho.operacao_id', '=', 'codigoitens.id')
            ->leftjoin('naturezasubitem', 'naturezasubitem.id', '=', 'contrato_item_minuta_empenho.subelemento_id')
            ->select(
                'contrato_item_minuta_empenho.*',
                'codigoitens.descricao',
                'naturezasubitem.descricao as subitem'
            )
            ->orderBy('minutaempenhos.id', 'asc')
            ->orderBy('contrato_item_minuta_empenho.id', 'asc')
            ->get()->toArray();

        $this->crud->addColumn([
            'name' => 'ContratoItemMinutaEmpenho',
            'label' => 'Contrato Item Minuta Empenho',
            'type' => 'compras_table',
            'table_id' => 'contrato_item_minuta_empenho',
            'values' => $cime,
            'columns' => [
                'id' => 'ContratoItemMinutaEmpenhoID',
                'contrato_item_id' => 'ContratoItemID',
                'minutaempenho_id' => 'minutaempenho_id',
                'subitem' => 'Sub Item',
                'quantidade' => 'Quantidade',
                'valor' => 'Valor',
                'descricao' => 'Operação',
                'created_at' => 'created_at',
                   //'updated_at ' => 'updated_at',
                'minutaempenhos_remessa_id' => 'minutaempenhos_remessa_id',
                'numseq' => 'numseq'
            ],
            'visibleInShow' => true
        ]);
    }

    /**
     * @return void
     */
    public function addColumnAtas(): void
    {
        $atas = Arp::where('arp.compra_id', '=', $this->crud->entry->id)
            ->join(
                DB::raw('unidades unidade_origem'),
                DB::raw('unidade_origem.id'),
                '=',
                DB::raw("arp.unidade_origem_id")
            )
            ->select(
                'arp.*',
                DB::raw("unidade_origem.codigo || ' - ' || unidade_origem.nomeresumido as unidade_origem"),
            )
            ->get()->toArray();

        $this->crud->addColumn([
            'name' => 'atas',
            'label' => 'Atas',
            'type' => 'compras_table',
            'table_id' => 'ata',
            'values' => $atas,
            'columns' => [
                'id' => 'Ata Id',
                'unidade_origem' => 'Unidade Origem',
                'numero' => 'NumeroItem',
                'ano' => 'Ano',
                'data_assinatura' => 'Data Assinatura',
                'vigencia_inicial' => 'Vigencia Inicial',
                'vigencia_final' => 'Vigencia Final',
                'valor_total' => 'Valor Total',
//                'link' => 'Link',
            ],
            'visibleInShow' => true
        ]);
    }
    /**
     * Ativa manualmente itens inativos dependendo da rota
     *
     * @param \Illuminate\Http\Request $request The HTTP request object.
     * @throws Exception If an error occurs during the transaction.
     * @return mixed Returns the result of the function.
     */
    public function ativarOuInativarItens(\Illuminate\Http\Request $request)
    {
        $rota = $request->rota;
        $id = $request->id;
        $inativar = $request->inativar;

        $rotaModelMap = [
            'ciu' => CompraItemUnidade::class,
            'forn' => CompraItemFornecedor::class,
            'item' => CompraItem::class
        ];

        if (backpack_user()->hasRole('Administrador')) {
            DB::beginTransaction();
            try {
                if (array_key_exists($rota, $rotaModelMap)) {
                    $model = $rotaModelMap[$rota];
                    if ($inativar == 0) {
                        $model::where('id', $id)->update(['situacao' => 1]);
                    } else {
                        $model::where('id', $id)->update(['situacao' => 0]);
                    }
                    DB::commit();
                }
                return redirect()->back();
            } catch (Exception $e) {
                DB::rollBack();
                return $e->getMessage();
            }
        } else {
            abort('403', config('app.erro_permissao'));
        }
    }

    public function addColumnArpItem()
    {
        $arpItem = ArpItem::where('compras.id', '=', $this->crud->entry->id)
            ->join('compra_item_fornecedor', 'compra_item_fornecedor.id', '=', 'arp_item.compra_item_fornecedor_id')
            ->join('compra_items', 'compra_items.id', '=', 'compra_item_fornecedor.compra_item_id')
            ->join('compras', 'compras.id', '=', 'compra_items.compra_id')
            ->orderBy('compras.id', 'asc')
            ->get()->toArray();

        $this->crud->addColumn([
            'name' => 'arpItem',
            'label' => 'Ata Itens',
            'type' => 'compras_table',
            'table_id' => 'arpItem',
            'values' => $arpItem,
            'columns' => [
                'id' => 'Ata_item_id',
                'arp_id' => 'ata_id',
                'compra_item_fornecedor_id' => 'compra_item_fornecedor_id',
                'created_at' => 'created_at',
                'updated_at' => 'updated_at',
                'deleted_at' => 'deleted_at'
            ],
            'visibleInShow' => true
        ]);
    }
}
