<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UsuarioOrgaoRequest as StoreRequest;
use App\Http\Requests\UsuarioOrgaoRequest as UpdateRequest;
use App\Jobs\UserMailPasswordJob;
use App\Models\BackpackUser;
use App\Models\Orgao;
use App\Models\Role;
use App\Models\Unidade;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\CrudPanel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use function foo\func;

/**
 * Class UsuarioOrgaoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class UsuarioOrgaoCrudController extends CrudController
{

    private $unidadeUser;
    private $todasUnidadesUserOrgao;

    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */

        if (!backpack_user()->hasRole('Administrador Órgão') && !backpack_user()->hasRole('Administrador Suporte')) { //alterar para novo grupo de Administrador Orgão
            abort('403', config('app.erro_permissao'));
        }
        
        $this->unidadeUser = Unidade::find(session()->get('user_ug_id'));
        /**
         *  Pegar unidades pelo orgão do usuario logado.
        */
        $orgaos = Orgao::whereHas('unidades', function ($u) {
            $u->whereHas('users', function ($us) {
                $us->where('cpf', backpack_user()->cpf);
            })->orWhereHas('user', function ($usu) {
                $usu->where('cpf', backpack_user()->cpf);
            });
        })->pluck('id')->toArray();

        $this->todasUnidadesUserOrgao = Unidade::whereIn('orgao_id',$orgaos)
            ->pluck('id')->toArray();
        
        $this->crud->setModel('App\Models\BackpackUser');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/admin/usuarioorgao');
        $this->crud->setEntityNameStrings('Usuário Órgão', 'Usuários Órgãos');

        $this->crud->addClause('leftJoin', 'unidades', 'unidades.id', '=', 'users.ugprimaria');

        if (!empty($this->unidadeUser) && isset($this->unidadeUser->id)) {
            $this->crud->addClause('leftJoin', 'unidadesusers', function ($join) {
                $join->on('unidadesusers.user_id', '=', 'users.id')
                    ->where('unidadesusers.unidade_id', '=', $this->unidadeUser->id);
            });
        }
        
        if (!empty($this->unidadeUser) && isset($this->unidadeUser->orgao_id)) {
            $this->crud->addClause('where', 'unidades.orgao_id', '=', $this->unidadeUser->orgao_id);
        }
        $this->crud->addClause('select', ['users.*', 'unidades.orgao_id']);
        $this->crud->addClause('where', 'user_system', '=', false);
        $this->crud->enableExportButtons();
        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');

        (backpack_user()->can('usuarioorgao_inserir')) ? $this->crud->allowAccess('create') : null;
        (backpack_user()->can('usuarioorgao_editar')) ? $this->crud->allowAccess('update') : null;

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $colunas = $this->Colunas();
        $this->crud->addColumns($colunas);

        $ugs = Unidade::select(DB::raw("CONCAT(codigo,' - ',nomeresumido) AS nome"), 'id')
            ->where('situacao', '=', true)
            ->whereIn('id', $this->todasUnidadesUserOrgao)
            ->orderBy('codigo', 'asc')
            ->pluck('nome', 'id')
            ->toArray();

        $campos = $this->Campos($ugs, $this->todasUnidadesUserOrgao);
        $this->crud->addFields($campos);

        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function colunas()
    {
        $colunas = [
            [
                'name' => 'cpf',
                'label' => 'CPF',
                'type' => 'text',
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhereRaw("translate(unaccent(users.cpf), '.-/','') like translate(unaccent('%".$searchTerm."%'), '.-/','')");
                },
            ],
            [
                'name' => 'name',
                'label' => 'Nome',
                'type' => 'text',
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere('users.name', 'ilike', "%" . utf8_encode(utf8_decode(strtoupper($searchTerm))) . "%");
                },
            ],
            [
                'name' => 'email',
                'label' => 'E-mail',
                'type' => 'email',
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere('users.email', 'like', "%" . utf8_encode(utf8_decode(strtolower($searchTerm))) . "%");
                },
            ],
            [
                'name' => 'situacao',
                'label' => 'Situação',
                'type' => 'boolean',
                'options' => [0 => 'Inativo', 1 => 'Ativo'],
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInExport' => true,
                'visibleInShow' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    if (strtolower($searchTerm) == 'inativo') {
                        $query->orWhere('users.situacao', 0);
                    }

                    if (strtolower($searchTerm) == 'ativo') {
                        $query->orWhere('users.situacao', 1);
                    }
                }
            ],
            [
                'name' => 'getUGPrimaria',
                'label' => 'UG/UASG Padrão',
                'type' => 'model_function',
                'function_name' => 'getUGPrimaria', // the method in your Model
                'orderable' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere('unidades.codigo', 'like', "%" . utf8_encode(utf8_decode(strtoupper($searchTerm))) . "%");
                    $query->orWhere('unidades.nomeresumido', 'like', "%" . utf8_encode(utf8_decode(strtoupper($searchTerm))) . "%");
                },
            ],
            [ // n-n relationship (with pivot table)
                'label' => trans('backpack::permissionmanager.roles'), // Table column heading
                'type' => 'select_multiple',
                'name' => 'roles', // the method that defines the relationship in your Model
                'entity' => 'roles', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model' => config('permission.models.role'), // foreign key model
            ],
        ];

        return $colunas;
    }

    public function campos($ugs, $unidades_orgao)
    {
        $campos = [
            [
                'name' => 'cpf',
                'label' => 'CPF',
                'type' => 'cpf_orgao',
                'tab' => 'Dados Pessoais',

            ],
            [
                'name' => 'name',
                'label' => 'Nome Completo',
                'type' => 'text',
                'tab' => 'Dados Pessoais',
                'attributes' => [
                    'onkeyup' => "maiuscula(this)",
                    'disabled' => 'disabled'
                ],
            ],
            [
                'name' => 'email',
                'label' => 'E-mail',
                'type' => 'email',
                'tab' => 'Dados Pessoais',
                'attributes' => [
                    'disabled' => 'disabled'
                ]
            ],
            [
                'name' => 'situacao',
                'label' => "Situação",
                'type' => 'select_from_array',
                'options' => [1 => 'Ativo', 0 => 'Inativo'],
                'allows_null' => false,
                'tab' => 'Dados Pessoais',
                'attributes' => [
                    'disabled' => 'disabled'
                ]
            ],
            [ // select2_from_array
                'name' => 'ugprimaria',
                'label' => 'UG/UASG Padrão',
                'type' => 'select2_from_array',
                'options' => $ugs,
                'allows_null' => true,
                'tab' => 'Outros',
                'allows_multiple' => false, // OPTIONAL; needs you to cast this to array in your model;
                'attributes' => [
                    'disabled' => 'disabled'
                ]
            ],
            [
            'label' => "UGs/UASGs",
            'type' =>  "select2_from_ajax_multiple_single",
            'name' => 'unidades2',
            'entity' => 'unidades',
            'entity' => 'unidades',
            'attribute' => "codigo",
            'attribute2' => "nomeresumido",
            'process_results_template' => 'gescon.process_results_multiple_unidade',
            'model' => "App\Models\Unidade",
            'data_source' => url("api/unidade"),
            'placeholder' => "Selecione a(s) Unidade(s)",
            'minimum_input_length' => 2,
            'tab' => 'Outros',
            'pivot' => true,
            'attributes' => [
                    "disabled" => 'disabled'
                ]
            ],
            
            [       // Select2Multiple = n-n relationship (with pivot table)
                'label' => 'Demais UGs/UASGs',
                'type' => 'select2_multiple',
                'name' => 'unidades', // the method that defines the relationship in your Model
                'entity' => 'unidades', // the method that defines the relationship in your Model
                'attribute' => 'codigo', // foreign key attribute that is shown to user
                'attribute2' => 'nomeresumido', // foreign key attribute that is shown to user
                'attribute_separator' => ' - ', // foreign key attribute that is shown to user
                'model' => "App\Models\Unidade", // foreign key model
                'allows_null' => true,
                'pivot' => true, // on create&update, do you need to add/delete pivot table entries?
                'select_all' => true,
                'tab' => 'Outros',
                'options' => (function ($query) use ($unidades_orgao) {
                    return $query->orderBy('codigo', 'ASC')
                        ->whereIn('id', $unidades_orgao)
                        ->get();
                }),
                'attributes' => [
                    'disabled' => 'disabled'
                ]
            ],
            [       // Select2Multiple = n-n relationship (with pivot table)
                'label' => 'Grupos de Usuário',
                'type' => 'select2_multiple',
                'name' => 'roles', // the method that defines the relationship in your Model
                'entity' => 'roles', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model' => config('permission.models.role'), // foreign key model
                'allows_null' => true,
                'pivot' => true, // on create&update, do you need to add/delete pivot table entries?
                'select_all' => true,
                'tab' => 'Outros',
                'options' => function ($query) {
                    $userRoles = backpack_user()->getRoleNames()->toArray();
                    $query = $query->orderBy('name', 'ASC');

                    if (in_array('Administrador', $userRoles)) {
                        return $query->get();
                    }

                    if (in_array('Administrador Suporte', $userRoles)) {
                        return $query->whereIn('name', BackpackUser::getAdminSuporteRoles($this->crud->entry->id ?? ''))->get();
                    }

                    if (in_array('Administrador Órgão', $userRoles)) {

                        // $unidade = Unidade::find(session()->get('user_ug_id'));
                        if ($this->unidadeUser->exclusivo_gestao_atas === true ) {
                             return $query->whereIn('name', BackpackUser::getExclusivoGestaoAtasRoles($this->crud->entry->id ?? ''))->get();
                        }else{
                            return $query->whereIn('name', BackpackUser::getAdminOrgaoRoles($this->crud->entry->id ?? ''))->get();
                        }
                    }

                    return $query->whereNotIn('name', BackpackUser::getExcludedRoles())->get();
                },
                'attributes' => [
                    'disabled' => 'disabled'
                ]
            ],
        ];

        return $campos;
    }

    public function store(StoreRequest $request)
    {
        $chars = '0123456789';
        $max = strlen($chars) - 1;
        $senha = "NOVA";
        for ($i = 0; $i < 4; $i++) {
            $senha .= $chars{mt_rand(0, $max)};
        }

        $request->request->set('password', bcrypt($senha));

        $dados = [
            'email' => $request->input('email'),
            'cpf' => $request->input('cpf'),
            'nome' => $request->input('name'),
            'senha' => $senha,
        ];

        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);

        $usuario = BackpackUser::where('cpf', '=', $dados['cpf'])->first();

        if ($usuario) {
            UserMailPasswordJob::dispatch($usuario, $dados);
        }

        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        $usuario = BackpackUser::where('cpf', '=', $request->input('cpf'))->first();
        $usuarioLogadoCPF = BackpackUser::find(backpack_user()->id);
        $rolesExclude = BackpackUser::getExcludedRoles();

        //Retirando Adm de orgao e Unidade do Array, e pegando só Administrador e Administrador Suporte.
        $filteredRoles = array_diff($rolesExclude, ['Administrador Órgão','Administrador Unidade']);
 
        //Busca roles do usuario a ser editado.
        $rolesUsuarioEdicao = BackpackUser::getPermissionAdmin($request->id);

        //Busca roles doe o usuario logado e verifica permissao Administrador para editar
        $rolesUsuarioLogado= BackpackUser::getPermissionAdmin($usuarioLogadoCPF->id);
 
        //Verifica se usuario logado tem permissao acima de Adm de orgão
        $permissaoAdmUsuarioLogado = array_intersect($rolesUsuarioLogado, $filteredRoles);
   
        //Verifica se usuario ser editado tem nível superior de Adm de orgão.
        $permissaoUsuarioParaEdicao = array_intersect($rolesUsuarioEdicao, $filteredRoles);

        //Se o usuario para edição for nível acima do Adm de orgão não será editado.
        if((empty($permissaoAdmUsuarioLogado) && !empty($permissaoUsuarioParaEdicao))){
 
            \Alert::error('Não é permitido a alteração de grupos de usuários com nível superior!')->flash();
            return redirect()->back();
        }
        
        //Busca roles do Adm suporte para editar Consulta Global no orgão
        $permissaoAdmSuporteLogado = array_intersect($rolesUsuarioLogado, 
            array_diff($rolesExclude, 
            ['Administrador Órgão','Administrador Unidade','Consulta Global']));
        $permissaoUsuarioParaEdicaoConsulta = array_intersect($rolesUsuarioEdicao, 
            array_diff($rolesExclude, 
            ['Administrador Órgão','Administrador Unidade', 'Consulta Global']));

        //Se o usuario logado for nivel Adm de Órgão e tentar editar nivel superior Adm de orgão não será editado.
        if((!empty($permissaoAdmUsuarioLogado) 
            && !empty($permissaoUsuarioParaEdicaoConsulta) && !empty($permissaoAdmSuporteLogado))){
 
             \Alert::error('Não é permitido a alteração de grupos de usuários com nível superior!')->flash();
             return redirect()->back();
         }

        //Verifica se usuario logado tem permissao acima de Adm de suporte
        $permissaoAdmSuporteEditarConsultaGlobal = array_intersect($rolesUsuarioLogado, 
            ['Administrador Suporte']);
        $permissaoUsuarioEdicaoConsultaGlobal = array_intersect($rolesUsuarioEdicao, 
            array_diff($rolesExclude, ['Administrador', 'Acesso API']));
        
        //Se o usuario logado for nivel Adm de suporte podera editar.
        if((!empty($permissaoAdmSuporteEditarConsultaGlobal) && empty($permissaoUsuarioEdicaoConsultaGlobal))){

            \Alert::error('Não é permitido a alteração de grupos de usuários com nível superior!')->flash();
            return redirect()->back();
        }
   
        //Verificar permissão gestor de atas
        $CodigoGestorAtas[] = array_key_first($rolesUsuarioEdicao);
        $validaPermissaoGestaoAtas = empty(array_diff(array_values($CodigoGestorAtas), array_values($request->roles)));

        if($this->unidadeUser->exclusivo_gestao_atas === true){

            if (!$validaPermissaoGestaoAtas && $usuario->cpf !== $usuarioLogadoCPF->cpf) {
                \Alert::error('Sem permissão para alterar este Usuário!')->flash();
                return redirect()->back();
            }
        }

        // your additional operations before save here
        if ($request->input('situacao') == false) { // 0 = false = inativo
            $request->request->set('ugprimaria', null);
            $request->request->set('unidades', null);
            $request->request->set('roles', null);
        }
        $redirect_location = parent::updateCrud($request);

        return $redirect_location;
    }

    public function ajaxorgao(Request $request){

        $usuario = BackpackUser::where('cpf', '=', $request->input('cpforgao'))->first();
        $dados = ["empty" => true];

        if(!empty($usuario)){

            if($usuario->ugprimaria){
                $userUgPrimaria = $usuario->ugprimaria;
                $unidadeUserPrimaria = $usuario->unidadeprimaria($userUgPrimaria);
                $unidadeUserEdit = ["$unidadeUserPrimaria->id" => $unidadeUserPrimaria->codigo . " - " . $unidadeUserPrimaria->nomeresumido];
                $validacaoEdicaoUg = in_array($userUgPrimaria, $this->todasUnidadesUserOrgao);
            }else{
                $validacaoEdicaoUg = [];
                $unidadeUserEdit = "";
            }
                $dados = ["id" => $usuario->id, "nome" => $usuario->name, "email" => $usuario->email,
                            "validaUg" => $validacaoEdicaoUg, "unidadeUserEdit" => $unidadeUserEdit];
            }

        return json_encode($dados);
    }
}
