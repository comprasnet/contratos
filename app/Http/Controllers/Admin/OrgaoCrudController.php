<?php

namespace App\Http\Controllers\Admin;

use Alert;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\Execfin\EmpenhoCrudController;
use App\Models\Orgao;
use App\services\STA\STAUrlFetcherService;
use Backpack\CRUD\CrudPanel;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Illuminate\Database\Eloquent\Builder;
use App\Models\OrgaoSuperior;
use App\Models\Unidade;
use Illuminate\Support\Facades\DB;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\OrgaoRequest as StoreRequest;
use App\Http\Requests\OrgaoRequest as UpdateRequest;
use Illuminate\Support\Facades\Log;

/**
 * Class OrgaoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class OrgaoCrudController extends CrudController
{
    public function setup()
    {

        if (
            backpack_user()->hasRole('Administrador')
            or backpack_user()->hasRole('Administrador Órgão')
            or backpack_user()->hasRole('Administrador Suporte')
            or backpack_user()->hasRole('Consulta Global')
        ) {
            /*
            |--------------------------------------------------------------------------
            | CrudPanel Basic Information
            |--------------------------------------------------------------------------
            */
            $user_id = backpack_user()->id; // id do usuário logado

            $this->crud->setModel('App\Models\Orgao');
            $this->crud->setRoute(config('backpack.base.route_prefix') . '/admin/orgao');
            $this->crud->setEntityNameStrings('Órgão', 'Órgãos');
            if(
                backpack_user()->hasRole('Administrador')
                or backpack_user()->hasRole('Administrador Órgão')
                or backpack_user()->hasRole('Administrador Suporte')){
                $this->crud->addButtonFromView('line', 'moreorgao', 'moreorgao', 'end');
            }

            $this->crud->enableExportButtons();
            $this->crud->denyAccess('create');
            $this->crud->denyAccess('update');
            $this->crud->denyAccess('delete');
            $this->crud->allowAccess('show');

            $this->crud->addClause('orderBy', 'nome');

            (backpack_user()->hasRole('Administrador')) ? $this->crud->addButtonFromView('top', 'atualizaorgao',
                'atualizaorgao', 'end') : null;

            (backpack_user()->can('orgao_inserir')) ? $this->crud->allowAccess('create') : null;
            (backpack_user()->can('orgao_editar')) ? $this->crud->allowAccess('update') : null;
            (backpack_user()->can('orgao_deletar')) ? $this->crud->allowAccess('delete') : null;

            // se for adm órgão e não for adm, visualizar apenas seus órgãos
            if (!backpack_user()->hasRole('Administrador') && backpack_user()->hasRole('Administrador Órgão')) {

                // buscar os órgãos do usuário logado e jogar no crud / eloquent
                $orgaos = Orgao::whereHas('unidades', function ($u) {
                    $u->whereHas('users', function ($us) {
                        $us->where('cpf', backpack_user()->cpf);
                    })->orWhereHas('user', function ($usu) {
                        $usu->where('cpf', backpack_user()->cpf);
                    });
                })->pluck('id')->toArray();
                // pegar o array de órgãos e jogar no crud, pelo eloquent. (query é do eloquent)
                $this->crud->query->whereIn('id', $orgaos);

            }

            /*
            |--------------------------------------------------------------------------
            | CrudPanel Configuration
            |--------------------------------------------------------------------------
            */

            $colunas = $this->Colunas();
            $this->crud->addColumns($colunas);

            $orgaossuperiores = OrgaoSuperior::where('situacao', '=', true)->pluck('nome', 'id')->toArray();

            $campos = $this->Campos($orgaossuperiores);
            $this->crud->addFields($campos);

            // add asterisk for fields that are required in OrgaoRequest
            $this->crud->setRequiredFields(StoreRequest::class, 'create');
            $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
        } else {
            abort('403', config('app.erro_permissao'));
        }
    }

    public function Colunas()
    {
        $colunas = [
            [
                'name' => 'codigosiasg',
                'label' => 'Código SIASG', // Table column heading
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'codigo',
                'label' => 'Código SIAFI', // Table column heading
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'nome',
                'label' => 'Nome', // Table column heading
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'getOrgaoSuperior',
                'label' => 'Órgão Superior', // Table column heading
                'type' => 'model_function',
                'function_name' => 'getOrgaoSuperior', // the method in your Model
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'situacao',
                'label' => 'Situação',
                'type' => 'boolean',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
                // optionally override the Yes/No texts
                'options' => [0 => 'Inativo', 1 => 'Ativo']
            ],
            [
                'name' => 'cnpj',
                'label' => 'CNPJ', // Table column heading
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],

        ];

        return $colunas;
    }

    public function Campos($orgaossuperiores)
    {
        $campos = [
            [ // select_from_array
                'name' => 'orgaosuperior_id',
                'label' => "Órgão Superior",
                'type' => 'select2_from_array',
                'options' => $orgaossuperiores,
                'allows_null' => false,
            ],
            [ // select_from_array
                'name' => 'codigo',
                'label' => "Código SIAFI",
                'type' => 'orgao',

            ],
            [ // select_from_array
                'name' => 'codigosiasg',
                'label' => "Código SIASG",
                'type' => 'orgao',

            ],
            [
                'name' => 'codigo_siorg',
                'label' => "Código SIORG",
                'type' => 'orgao_siorg',
            ],
            [ // select_from_array
                'name' => 'nome',
                'label' => "Nome",
                'type' => 'text',
                'attributes' => [
                    'onkeyup' => "maiuscula(this)"
                ]
            ],
            [ // select_from_array
                'name' => 'cnpj',
                'label' => "CNPJ",
                'type' => 'text',
            ],
            [ // select_from_array
                'name' => 'situacao',
                'label' => "Situação",
                'type' => 'select_from_array',
                'options' => [1 => 'Ativo', 0 => 'Inativo'],
                'allows_null' => false,
            ],

        ];

        return $campos;
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {

        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function show($id)
    {
        $content = parent::show($id);
        $this->crud->removeColumn('orgaosuperior_id');

        return $content;
    }

    public function executaAtualizacaoCadastroOrgao()
    {
        if (!backpack_user()->hasRole('Administrador')) {
            abort('403', config('app.erro_permissao'));
        }

        $url = config('migracao.api_sta') . config('rotas-sta.estrutura-orgaos');

        $urlFetchService = new STAUrlFetcherService($url);
        $dados = $urlFetchService->fetchDataJson();

        if ($urlFetchService->isError($dados)) {

            $error = $urlFetchService->getError($dados);

            Log::error($error->error_message);
            if (backpack_user()) {
                Alert::error($error->error_message)->flash();
                return redirect('/admin/orgao');
            }
        }

        foreach ($dados as $dado) {

            $orgao = Orgao::where('codigo', $dado['codigo'])
                ->withTrashed()
                ->first();

            if (!isset($orgao->codigo)) {

                $orgao_superior = OrgaoSuperior::where('codigo', $dado['orgao_superior'])
                    ->first();

                if (isset($orgao_superior->id)) {
                    $novo = new Orgao();
                    $novo->orgaosuperior_id = $orgao_superior->id;
                    $novo->codigo = $dado['codigo'];
                    $novo->codigosiasg = $dado['codigo'];
                    $novo->nome = $dado['nome'];
                    $novo->cnpj = @$dado['cnpj'];
                    $novo->situacao = true;
                    $novo->save();
                }

            } else {
                if ($orgao->nome != $dado['nome'] or
                    $orgao->orgaosuperior->codigo != $dado['orgao_superior'] or
                    $orgao->cnpj != $dado['cnpj']) {

                    $orgao_superior = OrgaoSuperior::where('codigo', $dado['orgao_superior'])
                        ->first();

                    if (isset($orgao_superior->id)) {
                        $orgao->orgaosuperior_id = $orgao_superior->id;
                        $orgao->nome = $dado['nome'];
                        $orgao->cnpj = @$dado['cnpj'];
                        $orgao->save();
                    }
                }
            }
        }

        Alert::success('Atualização de Órgãos executada com sucesso.')->flash();
        return redirect('admin/orgao');
    }

}
