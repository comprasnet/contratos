<?php

namespace App\Http\Controllers\Admin;

use App\Models\Catmatsergrupo;
use App\Models\Codigo;
use App\Models\Codigoitem;
use App\Models\MinutaEmpenho;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\Response;
use Backpack\CRUD\CrudPanel;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Route;

/**
 * Class ConsultaMinutaCompraCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 * @author Saulo Soares <saulosao@gmail.com>
 */
class ConsultaMinutaCompraCrudController extends CrudController //ConsultaContratoBaseCrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */

        if (!backpack_user()->hasRole('Administrador')) {
            abort('403', config('app.erro_permissao'));
        }

        $compra_id = Route::current()->parameter('compra_id');

        $this->crud->setModel(MinutaEmpenho::class);
        $this->crud->setRoute(
            config('backpack.base.route_prefix')
            . "/admin/compra/{$compra_id}"
            . '/minutas'
        );

        $this->crud->setEntityNameStrings('Minuta', 'Minutas');
        $this->crud->setHeading('Minutas de Empenho por Compra');
        $this->crud->addButtonFromView('line', 'seguirminuta', 'seguirminuta', 'end');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $this->crud->enableExportButtons();

        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');
        $this->crud->denyAccess('show');

        $this->crud->addClause('select', [
            DB::raw("unidades.codigo || ' - ' || unidades.nomeresumido as unidade_gestora"),
            DB::raw("emitente.codigo || ' - ' || emitente.nomeresumido as unidade_emitente"),
            DB::raw("unidade_compra.codigo || ' - ' || unidade_compra.nomeresumido as unidade_compra"),
            DB::raw('compras.numero_ano as compras_numero_ano'),
            DB::raw('modalidade.descricao as modalidade'),
            DB::raw('tipo_minuta.descricao as tipo_minuta'),
            DB::raw('situacao_minuta.descricao as situacao_minuta'),
            // Tabela principal deve ser sempre a última da listagem!
            'minutaempenhos.*'
        ]);

        $this->crud->addClause(
            'join',
            'unidades',
            'unidades.id',
            '=',
            'minutaempenhos.unidade_id'
        );

        $this->crud->addClause(
            'leftJoin',
            'saldo_contabil',
            'saldo_contabil.id',
            '=',
            'minutaempenhos.saldo_contabil_id'
        );

        $this->crud->addClause(
            'leftJoin',
            DB::raw('unidades emitente'),
            DB::raw('emitente.id'),
            '=',
            DB::raw("saldo_contabil.unidade_id")
        );

        $this->crud->addClause(
            'join',
            'compras',
            'compras.id',
            '=',
            'minutaempenhos.compra_id'
        );

        $this->crud->addClause(
            'join',
            DB::raw('unidades unidade_compra'),
            DB::raw('unidade_compra.id'),
            '=',
            DB::raw("compras.unidade_origem_id")
        );

        $this->crud->addClause(
            'join',
            DB::raw('codigoitens modalidade'),
            DB::raw('modalidade.id'),
            '=',
            DB::raw("compras.modalidade_id")
        );

        $this->crud->addClause(
            'join',
            DB::raw('codigoitens tipo_minuta'),
            DB::raw('tipo_minuta.id'),
            '=',
            DB::raw("minutaempenhos.tipo_empenhopor_id")
        );

        $this->crud->addClause(
            'join',
            DB::raw('codigoitens situacao_minuta'),
            DB::raw('situacao_minuta.id'),
            '=',
            DB::raw("minutaempenhos.situacao_id")
        );


        $this->crud->addClause('where', 'compra_id', '=', $compra_id);
        $this->adicionaColunas();
    }

    protected function adicionaColunas(): void
    {
        $this->adicionaColunaUnidadeGestora();
        $this->adicionaColunaUnidadeEmitente();
        $this->adicionaColunaUnidadeCompra();
        $this->adicionaColunaModalidade();
        $this->adicionaColunaTipoEmpenhoPor();
        $this->adicionaColunaNumeroAnoCompra();
        $this->adicionaColunaMensagemSiafi();
        $this->adicionaColunaSituacao();
    }

    protected function adicionaColunaUnidadeGestora(): void
    {
        $this->crud->addColumn([
            'name'            => 'unidade_gestora',
            'label'           => 'Unidade Gestora',
            'type'            => 'text',
            'orderable'       => true,
            'visibleInTable'  => true,
            'visibleInModal'  => true,
            'visibleInExport' => true,
            'visibleInShow'   => true,
            'searchLogic' => function (Builder $query, $column, $searchTerm) {
                $query->orWhere('unidades.codigo', 'like', "%$searchTerm%");
            },
        ]);
    }

    protected function adicionaColunaUnidadeEmitente(): void
    {
        $this->crud->addColumn([
            'name'            => 'unidade_emitente',
            'label'           => 'UG Emitente',
            'type'            => 'text',
            'orderable'       => true,
            'visibleInTable'  => true,
            'visibleInModal'  => true,
            'visibleInExport' => true,
            'visibleInShow'   => true
        ]);
    }

    protected function adicionaColunaUnidadeCompra(): void
    {
        $this->crud->addColumn([
            'name'            => 'unidade_compra',
            'label'           => 'UASG Compra',
            'type'            => 'text',
            'orderable'       => true,
            'visibleInTable'  => true,
            'visibleInModal'  => true,
            'visibleInExport' => true,
            'visibleInShow'   => true
        ]);
    }

    protected function adicionaColunaModalidade(): void
    {
        $this->crud->addColumn([
            'name'            => 'modalidade',
            'label'           => 'Modalidade',
            'type'            => 'text',
            'orderable'       => true,
            'visibleInTable'  => true,
            'visibleInModal'  => true,
            'visibleInExport' => true,
            'visibleInShow'   => true,
        ]);
    }

    protected function adicionaColunaTipoEmpenhoPor(): void
    {
        $this->crud->addColumn([
            'name'            => 'tipo_minuta',
            'label'           => 'Tipo de Minuta',
            'type'            => 'text',
            'orderable'       => true,
            'visibleInTable'  => true,
            'visibleInModal'  => true,
            'visibleInExport' => true,
            'visibleInShow'   => true,
        ]);
    }

    protected function adicionaColunaNumeroAnoCompra(): void
    {
        $this->crud->addColumn([
            'name'            => 'compras_numero_ano',
            'label'           => 'Numero/Ano',
            'type'            => 'text',
            'orderable'       => true,
            'visibleInTable'  => true,
            'visibleInModal'  => true,
            'visibleInExport' => true,
            'visibleInShow'   => true,
            'searchLogic' => function ($query, $column, $searchTerm) {
                $query->orWhere(
                    DB::raw("REPLACE(numero_ano,'/','')"),
                    'ilike',
                    DB::raw("REPLACE('%$searchTerm%', '/', '')")
                );
            }
        ]);
    }

    protected function adicionaColunaMensagemSiafi(): void
    {
        $this->crud->addColumn([
            'name'            => 'getMensagemSiafiTratada',
            'label'           => 'Mensagem SIAFI',
            'type'            => 'model_function',
            'function_name'   => 'getMensagemSiafiTratada',
            'orderable'       => true,
            'visibleInTable'  => true,
            'visibleInModal'  => true,
            'visibleInExport' => true,
            'visibleInShow'   => true,
            'searchLogic' => function ($query, $column, $searchTerm) {
                $query->orWhere('mensagem_siafi', 'ilike', '%' . $searchTerm . '%');
            },
        ]);
    }

    protected function adicionaColunaSituacao(): void
    {
        $this->crud->addColumn([
            'name'            => 'situacao_minuta',
            'label'           => 'Situação',
            'type'            => 'text',
            'orderable'       => true,
            'visibleInTable'  => true,
            'visibleInModal'  => true,
            'visibleInExport' => true,
            'visibleInShow'   => true
        ]);
    }
}
