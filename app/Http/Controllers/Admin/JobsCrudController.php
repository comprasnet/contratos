<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\JobsRequest as StoreRequest;
use App\Http\Requests\JobsRequest as UpdateRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\CrudPanel;

/**
 * Class JobsCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class JobsCrudController extends CrudController
{

    public function setup()
    {
        if (backpack_user()->hasRole('Administrador Suporte')) {
            abort('403', config('app.erro_permissao'));
        }

        if(!backpack_user()->hasRole('Administrador') && !backpack_user()->hasRole('Desenvolvedor')){
            abort('403', config('app.erro_permissao'));
        }

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Jobs');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/admin/jobs');
        $this->crud->setEntityNameStrings('jobs', 'jobs');

        $this->crud->addClause('orderby', 'attempts', 'DESC');

        backpack_user()->hasRole('Administrador') || backpack_user()->hasRole('Desenvolvedor') ? $this->crud->allowAccess('show') : $this->crud->denyAccess('show');
        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');

        $this->crud->enableExportButtons();

        // colunas da listagem
        $colunas = $this->Colunas();
        $this->crud->addColumns($colunas);

        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function Colunas()
    {

        $colunas = [

            [
                'name'  => 'queue',
                'label' => 'Queue',
                'type'  => 'text',
                'limit' => 100,
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name'  => 'payload',
                'label' => 'Payload',
                'type'  => 'text',
                'limit' => 9999,
                'orderable' => true,
                'visibleInTable' => false, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name'  => 'attempts',
                'label' => 'Attempts',
                'type'  => 'text',
                'limit' => 9999,
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name'  => 'reserved_at',
                'label' => 'Reserved at',
                'type'  => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name'  => 'available_at',
                'label' => 'Available at',
                'type'  => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name'  => 'created_at',
                'label' => 'Created at',
                'type'  => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
        ];
        return $colunas;
    }

    public function store(StoreRequest $request)
    {
        $redirect_location = parent::storeCrud($request);

        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        $redirect_location = parent::updateCrud($request);

        return $redirect_location;
    }
}
