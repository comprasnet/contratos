<?php

namespace App\Http\Controllers\Admin;

use App\Models\Migration;
use Backpack\CRUD\app\Http\Controllers\CrudController;

use Backpack\CRUD\CrudPanel;

/**
 * Class CompraCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class MigrationCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        if (!backpack_user()->hasRole('Administrador')) {
            abort('403', config('app.erro_permissao'));
        }

        $this->crud->setModel(Migration::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/admin/migration');
        $this->crud->setEntityNameStrings('Migration', 'Migration');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');
        $this->crud->allowAccess('show');

        if (!$this->request->has('order')) {
            $this->crud->orderBy('batch', 'desc');
        }

        $this->adicionaColunas();
    }

    protected function adicionaColunas(): void
    {
        $this->adicionaColunaId();
        $this->adicionaColunaMigration();
        $this->adicionaColunaBatch();
    }

    protected function adicionaColunaId(): void
    {
        $this->crud->addColumn([
            'name'            => 'id',
            'label'           => 'Id',
            'type'            => 'text',
            'orderable'       => true,
            'visibleInTable'  => true,
            'visibleInModal'  => true,
            'visibleInExport' => true,
            'visibleInShow'   => true
        ]);
    }

    protected function adicionaColunaMigration(): void
    {
        $this->crud->addColumn([
            'name'            => 'migration',
            'label'           => 'Migration',
            'type'            => 'text',
            'orderable'       => true,
            'visibleInTable'  => true,
            'visibleInModal'  => true,
            'visibleInExport' => true,
            'visibleInShow'   => true,
        ]);
    }

    protected function adicionaColunaBatch(): void
    {
        $this->crud->addColumn([
            'name'            => 'batch',
            'label'           => 'Batch',
            'type'            => 'text',
            'orderable'       => true,
            'visibleInTable'  => true,
            'visibleInModal'  => true,
            'visibleInExport' => true,
            'visibleInShow'   => true,
        ]);
    }

}
