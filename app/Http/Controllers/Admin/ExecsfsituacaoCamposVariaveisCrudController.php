<?php

namespace App\Http\Controllers\Admin;

use App\Models\Codigo;
use App\Models\Execsfsituacao;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ExecsfsituacaoCamposVariaveisRequest as StoreRequest;
use App\Http\Requests\ExecsfsituacaoCamposVariaveisRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use Illuminate\Support\Facades\DB;

/**
 * Class ExecsfsituacaoCamposVariaveisCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ExecsfsituacaoCamposVariaveisCrudController extends CrudController
{
    public function setup()
    {
        if(!backpack_user()->hasRole('Administrador')){
            abort('403', config('app.erro_permissao'));
        }
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $execsfsituacao_id = \Route::current()->parameter('execsfsituacao_id');

        $execsfsituacao = Execsfsituacao::find($execsfsituacao_id);

        $this->crud->setModel('App\Models\ExecsfsituacaoCamposVariaveis');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/admin/situacaosiafi/'.$execsfsituacao_id.'/camposvariaveis');
        $this->crud->setEntityNameStrings('Campo Variável', 'Campos Variáveis');

        $this->crud->addClause('where', 'execsfsituacao_id', '=', $execsfsituacao_id);

        $this->crud->enableExportButtons();
        $this->crud->addButtonFromView('top', 'voltar', 'voltarexecsfsituacao', 'end');
        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');
        $this->crud->allowAccess('show');

        (backpack_user()->can('situacaosiafi_inserir')) ? $this->crud->allowAccess('create') : null;
        (backpack_user()->can('situacaosiafi_editar')) ? $this->crud->allowAccess('update') : null;
        (backpack_user()->can('situacaosiafi_deletar')) ? $this->crud->allowAccess('delete') : null;

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */


        $colunas = $this->Colunas();
        $this->crud->addColumns($colunas);

        $campos = $this->Campos($execsfsituacao);
        $this->crud->addFields($campos);

        // add asterisk for fields that are required in ExecsfsituacaoCamposVariaveisRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function Colunas()
    {
        $colunas = [
            [
                'name' => 'execsfsituacao_id',
                'label' => 'Situação SIAFI',
                'type' => 'model_function',
                'function_name' => 'getExecsfsituacao', // the method in your Model
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'aba',
                'label' => 'Aba',
                'type' => 'select_from_array',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
                // optionally override the Yes/No texts
                'options' => config('app.abas')
            ],
            [
                'name' => 'campo',
                'label' => 'Campo',
                'type' => 'select_from_array',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
                // optionally override the Yes/No texts
                'options' => config('app.campos_variaveis_siafi')
            ],
            [
                'name' => 'tipo',
                'label' => 'Tipo',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
                // optionally override the Yes/No texts
            ],
            [
                'name' => 'mascara',
                'label' => 'Máscara',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
                // optionally override the Yes/No texts
            ],
            [
                'name' => 'rotulo',
                'label' => 'Rótulo',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
                // optionally override the Yes/No texts
            ],
            [
                'name' => 'restricao',
                'label' => 'Restrição',
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
                // optionally override the Yes/No texts
            ],
        ];

        return $colunas;

    }

    public function Campos($execsfsituacao)
    {

        $campos = [
            [ // select_from_array
                'name' => 'execsfsituacao_id',
                'label' => "situacao",
                'type' => 'select2_from_array',
                'options' => [$execsfsituacao->id => $execsfsituacao->codigo ." - ".$execsfsituacao->descricao],
//                'default' => 'one',
                // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
            ],
            [ // select_from_array
                'name' => 'aba',
                'label' => "Aba",
                'type' => 'select2_from_array',
                'options' => config('app.abas'),
                'allows_null' => true,
            ],
            [ // select_from_array
                'name' => 'campo',
                'label' => "Campo",
                'type' => 'select2_from_array',
                'options' => config('app.campos_variaveis_siafi'),
                'allows_null' => true,
            ],
            [ // select_from_array
                'name' => 'tipo',
                'label' => "Tipo",
                'type' => 'text',
            ],
            [ // select_from_array
                'name' => 'mascara',
                'label' => "Máscara",
                'type' => 'text',
            ],
            [ // select_from_array
                'name' => 'rotulo',
                'label' => "Rótulo",
                'type' => 'text',
            ],
            [ // select_from_array
                'name' => 'restricao',
                'label' => "Restrição",
                'type' => 'text',
            ],

        ];

        return $campos;
    }

    public function store(StoreRequest $request)
    {
       if(substr($request->input('campo'),0,8) == 'txtinscr'){
           $request->request->set('mascara', null);
       }

        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        if(substr($request->input('campo'),0,8) == 'txtinscr'){
            $request->request->set('mascara', null);
        }

        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
