<?php

namespace App\Http\Controllers\Api;

use App\Models\SfPadrao;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Contrato;
use App\Models\Contratofatura;

class ContratofaturaController extends Controller
{
    public function recuperarPadrao(Request $request)
    {
        $contrato = Contrato::find($request->id_contrato);

        if ($contrato->getTipo() == 'Empenho') {
           return Contratofatura::whereHas('contrato', function ($query) use ($contrato) {
                $query->where('fornecedor_id', $contrato->fornecedor_id)
                      ->where('unidade_id', $contrato->unidade_id)
                      ->where('tipo_id', $contrato->tipo_id);    
            })
            ->whereNotNull('sfpadrao_id')
            ->where('situacao', 'APR')
            ->get()
            ->map(function ($fatura) {
                return [
                    'id' => $fatura->sfpadrao_id,
                    'dh' => $fatura->getSfpadrao(),
                    'dtemis' => optional($fatura->sfPadrao->dadosBasicos)->dtemis, // Obtem dtemis
                ];
            })
            ->sortByDesc('dtemis')
            ->values();
        }

        return Contratofatura::where('contrato_id', $request->id_contrato)
            ->whereNotNull('sfpadrao_id')
            ->where('situacao', 'APR')
            ->get()
            ->map(function ($fatura) {
                return [
                    'id' => $fatura->sfpadrao_id,
                    'dh' => $fatura->getSfpadrao(),
                    'dtemis' => optional($fatura->sfPadrao->dadosBasicos)->dtemis, // Obtem dtemis
                ];
            })
            ->sortByDesc('dtemis')
            ->values(); 
    }
}
