<?php

namespace App\Http\Controllers\Api;

use App\Models\Unidade;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use function foo\func;
use App\Models\Contratoresponsavel;

class UnidadeController extends Controller
{

    public function index(Request $request)
    {
        $search_term = $request->input('q');
        $page = $request->input('page');

        if ($search_term)
        {
            $results = Unidade::where('codigo', 'LIKE', '%'.strtoupper($search_term).'%')
                ->orWhere('nome', 'LIKE', '%'.strtoupper($search_term).'%')
                ->orWhere('nomeresumido', 'LIKE', '%'.strtoupper($search_term).'%')
                ->paginate(10);
        }
        else
        {
            $results = Unidade::paginate(10);
        }

        return $results;
    }

    public function unidadesfiltercontratos(Request $request)
    {
        $search_term = $request->input('q');
        $page = $request->input('page');

        if ($search_term)
        {
            $results = Unidade::select([
                'id',
                \DB::raw("codigo || ' - ' || nomeresumido as descricao")])
                ->where('codigo', 'LIKE', '%'.strtoupper($search_term).'%')
                ->orWhere('nome', 'LIKE', '%'.strtoupper($search_term).'%')
                ->orWhere('nomeresumido', 'LIKE', '%'.strtoupper($search_term).'%')
                ->paginate(10);
        }
        else
        {
            $results = Unidade::select(['id', \DB::raw("codigo || ' - ' || nomeresumido as descricao")])->paginate(10);
        }

        return $results;
    }

    public function show($id)
    {
        return Unidade::find($id);
    }

    //issue 499
    public function unidadesexecutorasfinanceiras(Request $request)
    {
        $search_term = $request->input('q');
        $page = $request->input('page');
        $orgao_id = $request->orgao_id;

        if ($search_term) {
            return Unidade::where('orgao_id', '=', $orgao_id)
                ->where(function ($query) use ($search_term) {
                    $query->where('codigo', 'ilike', '%' . strtoupper($search_term). '%')
                        ->orWhere('nome', 'ilike', '%' . strtoupper($search_term). '%')
                        ->orWhere('nomeresumido', 'ilike', '%' . strtoupper($search_term). '%');
                })
                ->where('tipo', '=', 'E')
                ->where('situacao', '=', true)
                ->whereNull('deleted_at')
                ->orderBy('codigo', 'asc')
                ->paginate(10);
        }
    }

    public function responsaveisContratosPorUnidade($codigo)
    {
        $usuario_consulta_api = false;
        if (backpack_user()->can('usuario_consulta_api')) {
            $usuario_consulta_api = true;
        }

        $unidadeContratos = Unidade::select([
                    'unidades.id as unidade_id',
                    'unidades.codigo as unidade_codigo',
                    'unidades.nomeresumido as unidade_nomeresumido',
                    'unidades.nome as unidade_nome',
                    'contratos.id as contrato_id'])
                ->join('contratos', 'contratos.unidade_id', '=', 'unidades.id')
                ->where('unidades.codigo', '=', $codigo)
                ->where('unidades.sigilo', '=', false)
                ->get();

        $unidades = [];

        foreach ($unidadeContratos as $contrato) {
            if (!isset($unidades[$contrato['unidade_id']])) {
                $unidades[$contrato['unidade_id']] = [
                    'unidade_id' => $contrato['unidade_id'],
                    'codigo' => $contrato['unidade_codigo'],
                    'nome_resumido' => $contrato['unidade_nomeresumido'],
                    'nome' => $contrato['unidade_nome'],
                    'contratos' => []
                ];
            }

            $responsaveis = Contratoresponsavel::select([
                'users.cpf as users_cpf',
                'users.name as users_name',
                'users.email as users_email',
                'codigoitens.descricao as users_funcao',
                'users.situacao as users_situacao',
                'users.updated_at as users_updated_at'
            ])
                ->join('users', 'users.id', '=', 'contratoresponsaveis.user_id')
                ->join('codigoitens', 'codigoitens.id', '=', 'contratoresponsaveis.funcao_id')
                ->where('contratoresponsaveis.contrato_id', '=', $contrato['contrato_id'])
                ->get();

            $arrResponsaveis = [];

            foreach ($responsaveis as $responsavel) {
                $arrResponsaveis[] = [
                    'cpf' => $usuario_consulta_api ? $responsavel['users_cpf'] : ('***' . substr($responsavel['users_cpf'], 3, 9) . '**'),
                    'nome' => $responsavel['users_name'],
                    'email' => $responsavel['users_email'],
                    'funcao' => $responsavel['users_funcao'],
                    'situacao' => $responsavel['users_situacao'] ? 'Ativo' : 'Inativo',
                    'updated_at' => $responsavel['users_updated_at'],
                ];
            }

            $unidades[$contrato['unidade_id']]['contratos'][] = [
                'contrato_id' => $contrato['contrato_id'],
                'responsaveis' => $arrResponsaveis
            ];
        }

        return response()->json(array_values($unidades));
    }

}
