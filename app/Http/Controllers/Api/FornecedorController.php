<?php

namespace App\Http\Controllers\Api;

use App\Models\Fornecedor;
use App\Models\Contrato;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use OpenApi\Annotations as OA;

class FornecedorController extends Controller
{
    public function index(Request $request)
    {
        $search_term = $request->input('q');
        $page = $request->input('page');

        if ($search_term) {
            $results = Fornecedor::where('cpf_cnpj_idgener', 'LIKE', '%' . strtoupper($search_term) . '%')
                ->orWhere('nome', 'LIKE', '%' . strtoupper($search_term) . '%')
                ->orderBy('nome', 'asc')
                ->paginate(10);
        } else {
            $results = Fornecedor::orderBy('nome', 'asc')
                ->paginate(10);
        }

        return $results;
    }

    public function suprido(Request $request)
    {
        $search_term = $request->input('q');
        $page = $request->input('page');

        if ($search_term) {
            return Fornecedor::where(function ($query) use ($search_term) {
                $query->where('cpf_cnpj_idgener', 'LIKE', '%' . strtoupper($search_term) . '%')
                    ->orWhere('nome', 'LIKE', '%' . strtoupper($search_term) . '%');
            })
                ->whereIn('tipo_fornecedor', ['FISICA', 'UG'])
                ->select([
                    'fornecedores.id',
                    DB::raw("fornecedores.cpf_cnpj_idgener || ' - ' || fornecedores.nome as cpf_cnpj_idgener")
                ])
                ->orderBy('nome', 'asc')
                ->paginate(10);
        }

        return Fornecedor::orderBy('nome', 'asc')
            ->paginate(10);
    }

    public function subcontratados(Request $request)
    {
        $search_term = $request->input('q');
        $page = $request->input('page');

        if ($search_term) {
            return Fornecedor::where(function ($query) use ($search_term) {
                $query->where('cpf_cnpj_idgener', 'LIKE', '%' . strtoupper($search_term) . '%')
                    ->orWhere('nome', 'LIKE', '%' . strtoupper($search_term) . '%');
            })
               /* ->select([
                    'fornecedores.id',
                    DB::raw("fornecedores.cpf_cnpj_idgener || ' - ' || fornecedores.nome as cpf_cnpj_idgener")
                ])*/
                ->orderBy('nome', 'asc')
                ->paginate(10);
        }

        return Fornecedor::orderBy('nome', 'asc')
            ->paginate(10);
    }

    public function show($id)
    {
        return Fornecedor::find($id);
    }

    /**
     * @OA\Get(
     *     tags={"Fornecedores"},
     *     summary="Retorna todos os contratos ativos e/ou inativos de um fornecedor.",
     *     description="",
     *     path="/api/v1/fornecedor/contratos",
     *     security={ {"bearerAuth": {} }},
     *
     *     @OA\Parameter(
     *         name="identificador",
     *         in="query",
     *         description="Identificador do fornecedor (cnpj, cpf, id) **com** máscara.",
     *         example="02.341.470/0001-44",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *        )
     *     ),
     *
     *     @OA\Parameter(
     *         name="status_contrato",
     *         in="query",
     *         description="Status ou situação (ativo ou inativo) dos contratos.",
     *         example="ativo",
     *         required=false,
     *         @OA\Schema(
     *              type="string"
     *        )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *         @OA\JsonContent(ref="#/components/schemas/ContratosUnidadeGestora")
     *     ),
     *
     *     @OA\Response(
     *         response=401,
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/FalhaAutenticação")
     *     ),
     *
     *     @OA\Response(
     *         response=404,
     *         description="Error",
     *         @OA\JsonContent(@OA\Property(property="error",type="string",example="Fornecedor não localizado."))
     *     ),
     *
     *
     * )
     */
    public function getContratos (Request $request) {

        $cpf_cnpj_idgener = $request->identificador; //Parâmetro obrigatório
        $situacao = $request->status_contrato; //Parâmetro opcional
        $contratos_array = [];

        if (isset($cpf_cnpj_idgener)) {
            try {
                $query = Contrato::select('contratos.*')
                        ->join('fornecedores', 'contratos.fornecedor_id', '=', 'fornecedores.id')
                        ->where('fornecedores.cpf_cnpj_idgener', $cpf_cnpj_idgener)
                        ->whereNull('fornecedores.deleted_at')
                        ->get();

                //Lança uma exceção caso não localize o fornecedor.
                if (!$query->count()) throw new ModelNotFoundException("Fornecedor não localizado!");

                //Caso informado, filtra os contratos pela situação (ativo ou inativo)
                if (isset($situacao)) {
                    if (($situacao == 'ativo') || ($situacao == 'inativo')) {
                        $situacao = ($situacao == 'ativo') ? true : false;
                        $query = $query->where('situacao', $situacao);
                    }
                }
                //ajusta a URL referente aos links do contrato.
                $url_links_contrato = str_replace('fornecedor', 'contrato', Route::current()->getPrefix());

                //Formata a saída do json
                foreach ($query as $contrato) {
                    $contratos_array[] = $contrato->contratoAPI($url_links_contrato);
                }

            } catch (ModelNotFoundException $e) {
                return response()->json(['error' => $e->getMessage()], 404);

            } catch (\Exception $e) {
                return response()->json(['error' => $e->getMessage()], 500);
            }
        }

        return response()->json($contratos_array);
    }
}
