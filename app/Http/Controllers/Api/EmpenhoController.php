<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Catmatseritem;
use App\Models\Contratoempenho;
use App\Models\Empenho;
use Illuminate\Http\Request;
use App\Http\Controllers\APIController;
use App\Http\Traits\CatmatpdmsTrait;
use App\Models\Empenhodetalhado;
use App\Models\Fornecedor;
use App\Models\Naturezadespesa;
use App\Models\Naturezasubitem;
use App\Models\Planointerno;
use App\Models\Unidade;
use App\XML\Execsiafi;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use OpenApi\Tests\Fixtures\Processors\Nesting\ApiController as NestingApiController;
use Illuminate\Support\Str;
use App\services\EmpenhoService;

class EmpenhoController extends Controller
{
    use CatmatpdmsTrait;

    /**
     * @var EmpenhoService
     */
    private $empenhoService;

    public function __construct(EmpenhoService $empenhoService)
    {
        $this->empenhoService = $empenhoService;
    }

    public function index(Request $request)
    {
        $search_term = $request->input('q');
        $form = collect($request->input('form'))->pluck('value', 'name');
        $options = Empenho::query();
        $empenhados = Contratoempenho::where('fornecedor_id', $form['fornecedor_id'])
                    ->select('empenho_id')
                    ->get()
                    ->toArray();

        // if no category has been selected, show no options
        if (!$form['fornecedor_id'] or !$form['unidadeempenho_id']) {
            return [];
        }

        // if a category has been selected, only show articles in that category
        if ($form['fornecedor_id'] and $form['unidadeempenho_id']) {
            $options->select(['empenhos.*']);
            $options->join('minutaempenhos', function ($join) {
                $join->on('empenhos.numero', '=', 'minutaempenhos.mensagem_siafi');
            });
            $options = $options->where('empenhos.fornecedor_id', $form['fornecedor_id'])
                ->where('minutaempenhos.unidade_id', '=', $form['unidadeempenho_id'])
                ->whereNotIn('empenhos.id', $empenhados);
        }


        if ($search_term) {
            $results = $options->where('empenhos.numero', 'LIKE', '%' . $search_term . '%')->paginate(10);
        } else {
            $results = $options->paginate(10);
        }


        return $results;
    }

    public function listaEmpenhosParaFaturas(Request $request)
    {
        $search_term = $request->input('q');
        $contract_id = $request->contrato_id;

        $options = Empenho::query()
            ->select('empenhos.id', 'empenhos.numero', 'fornecedores.nome as fornecedor', 'empenhos.data_emissao', DB::raw("empenhos.created_at::TIMESTAMP::DATE"), DB::raw("CASE WHEN unidades.codigosiafi IS NULL THEN unidades.codigo ELSE unidades.codigosiafi END AS codigo"))
            ->leftJoin('contratoempenhos', 'empenhos.id', '=', 'contratoempenhos.empenho_id')
            ->leftJoin('fornecedores', 'fornecedores.id', '=', 'empenhos.fornecedor_id')
            ->leftJoin('unidades', 'unidades.id', '=', 'empenhos.unidade_id')
            ->where('contratoempenhos.contrato_id', $contract_id)
            ->distinct()
            ->orderBy('empenhos.numero');

        if ($search_term) {
            $results = $options->where('empenhos.numero', 'LIKE', '%' . $search_term . '%')->paginate(10);
        } else {
            $results = $options->paginate(10);
        }

        return $results;
    }


    public function autocomplete(Request $request){
        $fornecedor = $request->all()['fornecedor'];
        $cnpjCpfIdGenerico = $this->retornaFornecedor($fornecedor);
        $unidade = Unidade::where('codigosiafi', $request->all()['unidade_id'])->first();
        $term = $request->all()['query'];
        $unidade_id = $unidade->id;

        $fornecedor_id = Fornecedor::where(['cpf_cnpj_idgener' => trim($cnpjCpfIdGenerico)])->first();

        if(!$fornecedor_id){
            return response()->json(['erro' => 'Fornecedor não encontrado']);
        }
        $results = Empenho::where('numero', 'like', '%' . $term . '%')
            ->where('fornecedor_id', $fornecedor_id->id)
            ->where('unidade_id', $unidade_id)
            ->get()
            ->toArray();

        if(!$results){
            return response()->json([0 => ['id'=>$term.'empenho_nao_encontrado' ,'numero' => $term]]);

        }
        return response()->json($results);
    }
    private function retornaFornecedor($fornecedor){
        $primeiroCharFornecedor = substr($fornecedor, 0, 1);
        $padrao = '/^(.*?)-/'; // Expressão regular para pegar fornecedor estrangeiro
        $cnpj = null;

        //Expressão regular para encontrar o CNPJ, CPF
        // Se o primeiro caractere for um digito
        if(ctype_digit($primeiroCharFornecedor)){
            $padrao = '/^(.*?-[\d.]+)/';
        }

        if (preg_match($padrao, $fornecedor, $matches)) {
            $cnpj = $matches[1];
        }

        // Caso o fornecedor seja tipo UG
        if(!$cnpj){
            $padrao = '/^(.*?[\d.]+)/';
            if (preg_match($padrao, $fornecedor, $matches)) {
                $cnpj = $matches[1];
            }
        }

        return $cnpj;
    }
    public function show($id)
    {
        return Empenho::find($id);
    }

    /**
     * @OA\Get(
     *     tags={"Empenhos"},
     *     summary="Retorna todos os empenhos de uma determinada UG pelo ano informado.",
     *     description="",
     *     path="/api/v1/empenho/ano/{ano}/ug/{unidade}",
     *    security={ {"bearerAuth": {} }},
     *
     *     @OA\Parameter(
     *         name="ano",
     *         in="path",
     *         description="Ano",
     *         example="2022",
     *         required=true,
     *         @OA\Schema(
     *              type="integer"
     *        )
     *     ),
     *
     *      @OA\Parameter(
     *         name="unidade",
     *         in="path",
     *         description="Código da UG",
     *         example="201053",
     *         required=true,
     *         @OA\Schema(
     *            type="integer"
     *        ),
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="Lista de empenhos da unidade retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/EmpenhosUnidadeGestora")
     *     ),
     *
     *     @OA\Response(
     *        response=401,
     *        description="Error",
     *        @OA\JsonContent(ref="#/components/schemas/FalhaAutenticação")
     *     ),
     *
     * )
     */

    public function empenhosPorAnoUg(int $ano, int $unidade)
    {
        $empenhos_array = [];
        $emp = new Empenho();
        $empenhos = $emp->buscaEmpenhosPorAnoUg($ano, $unidade);

        foreach ($empenhos as $empenho) {
            $empenhos_array[] = [
                'id' => $empenho->id,
                'numero' => $empenho->numero,
                'data_emissao'=> @$empenho->data_emissao,
                'unidade' => $empenho->unidade->codigo . ' - ' . $empenho->unidade->nomeresumido,
                'gestao' => @$empenho->unidade->gestao,
                'fornecedor' => $empenho->fornecedor->cpf_cnpj_idgener . ' - ' . $empenho->fornecedor->nome,
                'fonte' => @$empenho->fonte,
                'ptres' => @$empenho->ptres,
                'modalidade_licitacao_siafi' => @$empenho->modalidade_licitacao_siafi,
                'naturezadespesa' => $empenho->naturezadespesa->codigo . ' - ' . $empenho->naturezadespesa->descricao,
                'empenhado' => number_format($empenho->empenhado, 2, ',', '.'),
                'aliquidar' => number_format($empenho->aliquidar, 2, ',', '.'),
                'liquidado' => number_format($empenho->liquidado, 2, ',', '.'),
                'pago' => number_format($empenho->pago, 2, ',', '.'),
                'rpinscrito' => number_format($empenho->rpinscrito, 2, ',', '.'),
                'rpaliquidar' => number_format($empenho->rpaliquidar, 2, ',', '.'),
                'rpaliquidado' => number_format($empenho->rpaliquidado, 2, ',', '.'),
                'rppago' => number_format($empenho->rppago, 2, ',', '.'),
                'informacao_complementar' => @$empenho->informacao_complementar,
                'sistema_origem' => @$empenho->sistema_origem,
            ];
        }

        return json_encode($empenhos_array);
    }

    /**
     * @OA\Get(
     *     tags={"Empenhos"},
     *     summary="Retorna todos os empenhos de uma determinada UG.",
     *     description="",
     *     path="/api/v1/empenho/ug/{unidade}",
     *     security={ {"bearerAuth": {} }},
     *
     *     @OA\Parameter(
     *         name="unidade",
     *         in="path",
     *         description="Código da UG",
     *         example="201053",
     *         required=true,
     *         @OA\Schema(
     *            type="integer"
     *        ),
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="Lista de empenhos da unidade retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/EmpenhosUnidadeGestora")
     *     ),
     *
     *     @OA\Response(
     *        response=401,
     *        description="Error",
     *        @OA\JsonContent(ref="#/components/schemas/FalhaAutenticação")
     *     ),
     *
     * )
     */

    public function empenhosPorUg(int $unidade)
    {
        $empenhos_array = [];
        $emp = new Empenho();
        $empenhos = $emp->buscaEmpenhosPorUg($unidade);

        foreach ($empenhos as $empenho) {
            $empenhos_array[] = [
                'id' => $empenho->id,
                'numero' => $empenho->numero,
                'data_emissao'=> @$empenho->data_emissao,
                'unidade' => $empenho->unidade->codigo . ' - ' . $empenho->unidade->nomeresumido,
                'gestao' => @$empenho->unidade->gestao,
                'fornecedor' => $empenho->fornecedor->cpf_cnpj_idgener . ' - ' . $empenho->fornecedor->nome,
                'fonte' => @$empenho->fonte,
                'ptres' => @$empenho->ptres,
                'modalidade_licitacao_siafi' => @$empenho->modalidade_licitacao_siafi,
                'naturezadespesa' => $empenho->naturezadespesa->codigo . ' - ' . $empenho->naturezadespesa->descricao,
                'empenhado' => number_format($empenho->empenhado, 2, ',', '.'),
                'aliquidar' => number_format($empenho->aliquidar, 2, ',', '.'),
                'liquidado' => number_format($empenho->liquidado, 2, ',', '.'),
                'pago' => number_format($empenho->pago, 2, ',', '.'),
                'rpinscrito' => number_format($empenho->rpinscrito, 2, ',', '.'),
                'rpaliquidar' => number_format($empenho->rpaliquidar, 2, ',', '.'),
                'rpaliquidado' => number_format($empenho->rpaliquidado, 2, ',', '.'),
                'rppago' => number_format($empenho->rppago, 2, ',', '.'),
                'informacao_complementar' => @$empenho->informacao_complementar,
                'sistema_origem' => @$empenho->sistema_origem,
                'fornecedor_obj' => [
                    'tipo' => $empenho->fornecedor->tipo_fornecedor,
                    'cnpj_cpf_idgener' => $empenho->fornecedor->cpf_cnpj_idgener,
                    'nome' => $empenho->fornecedor->nome
                ],
            ];
        }

        return response()->json($empenhos_array);
    }

    /**
     * @OA\Get(
     *     tags={"Empenhos"},
     *     summary="Retorna todos os empenhos alterados em um determinado período.",
     *     description="",
     *     path="/api/v1/empenho",
     *     security={ {"bearerAuth": {} }},
     *
     *     @OA\Parameter(
     *         name="dt_alteracao_min",
     *         in="query",
     *         description="Data de alteração inicial (formato ISO 8601-1:2019)",
     *         example="2022-08-05T08:00:00Z",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *        )
     *     ),
     *
     *     @OA\Parameter(
     *         name="dt_alteracao_max",
     *         in="query",
     *         description="Data de alteração final (formato ISO 8601-1:2019)",
     *         example="2022-08-05T18:00:00Z",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *        )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="Lista de empenhos retornados com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/EmpenhosV2")
     *     ),
     *
     *     @OA\Response(
     *        response=401,
     *        description="Error",
     *        @OA\JsonContent(ref="#/components/schemas/FalhaAutenticação")
     *     ),
     *
     * )
     */

    public function empenhosAll(Request $request)
    {

        $api_controller = new APIController();
        $api_controller->rangeObrigatorio($request->dt_alteracao_min, $request->dt_alteracao_max);

        $empenhos_array = [];
        $emp = new Empenho();
        $empenhos = $emp->buscaEmpenhosAll($api_controller->range($request->dt_alteracao_min, $request->dt_alteracao_max));

        foreach ($empenhos as $empenho) {
            $empenhos_array[] = [
                'id' => $empenho->id,
                'unidade' => $empenho->unidade->codigo,
                'unidade_nome' => $empenho->unidade->nome,
                'gestao' => @$empenho->unidade->gestao,
                'numero_empenho' => $empenho->numero,
                'data_emissao' => @$empenho->data_emissao,
                'cpf_cnpj_credor' => $empenho->fornecedor->cpf_cnpj_idgener,
                'credor' =>  $empenho->fornecedor->nome,
                'fonte_recurso' => @$empenho->fonte,
                'ptres' => @$empenho->ptres,
                'modalidade_licitacao_siafi' => @$empenho->modalidade_licitacao_siafi,
                'naturezadespesa' => $empenho->naturezadespesa->codigo,
                'naturezadespesa_descricao' => $empenho->naturezadespesa->descricao,
                'planointerno' => @$empenho->planointerno->codigo,
                'planointerno_descricao' => @$empenho->planointerno->descricao,
                'valor_empenhado' => floatval($empenho->empenhado),
                'valor_aliquidar' => floatval($empenho->aliquidar),
                'valor_liquidado' => floatval($empenho->liquidado),
                'valor_pago' => floatval($empenho->pago),
                'valor_rpinscrito' => floatval($empenho->rpinscrito),
                'valor_rpaliquidar' => floatval($empenho->rpaliquidar),
                'valor_rpaliquidado' => floatval($empenho->rpaliquidado),
                'valor_rppago' => floatval($empenho->rppago),
                'informacao_complementar' => @$empenho->informacao_complementar,
                'sistema_origem' => @$empenho->sistema_origem,
                'contrato_id' => @$empenho->contratoempenho->contrato_id,
                'id_cipi' => strpos(@$empenho->informacao_complementar,'CIPI:') ? Str::after(@$empenho->informacao_complementar,'CIPI: ') : null,
                'created_at' => @$empenho->created_at,
                'updated_at' => @$empenho->updated_at
            ];
        }

        return response()->json($empenhos_array);
    }

    public function empenhosAllCipi(Request $request)
    {

        $api_controller = new APIController();
        $api_controller->rangeObrigatorio($request->dt_alteracao_min, $request->dt_alteracao_max);

        $empenhos_array = [];
        $emp = new Empenho();
        $empenhos = $emp->buscaEmpenhosAllCipi($api_controller->range($request->dt_alteracao_min, $request->dt_alteracao_max));

        foreach ($empenhos as $empenho) {
            $empenhos_array[] = [
                'id' => $empenho->id_empenho,
                'unidade' => $empenho->codigo_unidade_empenho,
                'unidade_nome' => $empenho->nome_unidade_empenho,
                'gestao' => @$empenho->gestao_unidade_empenho,
                'numero_empenho' => $empenho->numero_empenho,
                'data_emissao' => @$empenho->data_emissao_empenho,
                'cpf_cnpj_credor' => substr($empenho->doc_id_nome_fornecedor, 0, strpos($empenho->doc_id_nome_fornecedor, "|")-1),
                'credor' =>  substr($empenho->doc_id_nome_fornecedor, strpos($empenho->doc_id_nome_fornecedor, "|")+2, strlen($empenho->doc_id_nome_fornecedor)),
                'fonte_recurso' => @$empenho->fonte_empenho,
                'ptres' => @$empenho->ptres_empenho,
                'esfera_orcamentaria' => $empenho->esfera,
                'modalidade_licitacao_siafi' => @$empenho->modalidade_licitacao_siafi,
                'naturezadespesa' => substr($empenho->codigo_descricao_nd, 0, strpos($empenho->codigo_descricao_nd, "|")-1),
                'naturezadespesa_descricao' => substr($empenho->codigo_descricao_nd, strpos($empenho->codigo_descricao_nd, "|")+2, strlen($empenho->codigo_descricao_nd)),
                'planointerno' => substr($empenho->codigo_descricao_planointerno, 0, strpos($empenho->codigo_descricao_planointerno, "|")-1) ?: null,
                'planointerno_descricao' => substr($empenho->codigo_descricao_planointerno, strpos($empenho->codigo_descricao_planointerno, "|")+2, strlen($empenho->codigo_descricao_planointerno)) ?: null,
                'valor_empenhado' => floatval($empenho->valor_empenhado),
                'valor_aliquidar' => floatval($empenho->valor_aliquidar),
                'valor_liquidado' => floatval($empenho->valor_liquidado),
                'valor_pago' => floatval($empenho->valor_pago),
                'valor_rpinscrito' => floatval($empenho->valor_rpinscrito),
                'valor_rpaliquidar' => floatval($empenho->valor_rpaliquidar),
                'valor_rpaliquidado' => floatval($empenho->valor_rpliquidado),
                'valor_rppago' => floatval($empenho->valor_rppago),
                'informacao_complementar' => @$empenho->informacao_complementar,
                'sistema_origem' => @$empenho->sistema_origem,
                'contrato_id' => @$empenho->contrato_id,
                'compra_id' => $empenho->compra_id,
                'id_cipi' => $empenho->id_cipi,
                'tipo_minuta' => $empenho->tipo_minuta,
                'origem_tipo_minuta' => $empenho->origem_tipo_minuta,
                'tipo_empenho' => @$empenho->tipo_empenho,
                'numero_processo' => @$empenho->processo,
                'descricao_empenho' => @$empenho->descricao,
                'codigo_amparo_legal' => intval($empenho->amparo_legal),
                'ug_responsavel' => trim($empenho->ugr),
                'local_entrega' => @$empenho->local_entrega,
                'created_at' => @$empenho->created_at,
                'updated_at' => @$empenho->updated_at
            ];
        }

        return response()->json($empenhos_array);
    }



    /**
     * @OA\Get(
     *     tags={"Empenhos"},
     *     summary="Retorna todos os restos a pagar alterados em um determinado período.",
     *     description="",
     *     path="/api/v1/empenho/rp",
     *     security={ {"bearerAuth": {} }},
     *
     *     @OA\Parameter(
     *         name="dt_alteracao_min",
     *         in="query",
     *         description="Data de alteração inicial (formato ISO 8601-1:2019)",
     *         example="2022-08-05T08:00:00Z",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *        )
     *     ),
     *
     *     @OA\Parameter(
     *         name="dt_alteracao_max",
     *         in="query",
     *         description="Data de alteração final (formato ISO 8601-1:2019)",
     *         example="2022-08-05T18:00:00Z",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *        )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="Lista de empenhos retornados com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/EmpenhosV2")
     *     ),
     *
     *     @OA\Response(
     *        response=401,
     *        description="Error",
     *        @OA\JsonContent(ref="#/components/schemas/FalhaAutenticação")
     *     ),
     *
     * )
     */
    public function restosapagarAll(Request $request)
    {

        $api_controller = new APIController();
        $api_controller->rangeObrigatorio($request->dt_alteracao_min, $request->dt_alteracao_max);

        $empenhos_array = [];
        $emp = new Empenho();

        $empenhos = $emp->buscaEmpenhosAll($api_controller->range($request->dt_alteracao_min, $request->dt_alteracao_max), true);

        foreach ($empenhos as $empenho) {
            $empenhos_array[] = [
                'id' => $empenho->id,
                'unidade' => $empenho->unidade->codigo,
                'unidade_nome' => $empenho->unidade->nome,
                'gestao' => @$empenho->unidade->gestao,
                'numero_empenho' => $empenho->numero,
                'data_emissao' => @$empenho->data_emissao,
                'cpf_cnpj_credor' => $empenho->fornecedor->cpf_cnpj_idgener,
                'credor' =>  $empenho->fornecedor->nome,
                'fonte_recurso' => @$empenho->fonte,
                'ptres' => @$empenho->ptres,
                'modalidade_licitacao_siafi' => @$empenho->modalidade_licitacao_siafi,
                'naturezadespesa' => $empenho->naturezadespesa->codigo,
                'naturezadespesa_descricao' => $empenho->naturezadespesa->descricao,
                'planointerno' => @$empenho->planointerno->codigo,
                'planointerno_descricao' => @$empenho->planointerno->descricao,
                'valor_empenhado' => floatval($empenho->empenhado),
                'valor_aliquidar' => floatval($empenho->aliquidar),
                'valor_liquidado' => floatval($empenho->liquidado),
                'valor_pago' => floatval($empenho->pago),
                'valor_rpinscrito' => floatval($empenho->rpinscrito),
                'valor_rpaliquidar' => floatval($empenho->rpaliquidar),
                'valor_rpaliquidado' => floatval($empenho->rpaliquidado),
                'valor_rppago' => floatval($empenho->rppago),
                'informacao_complementar' => @$empenho->informacao_complementar,
                'sistema_origem' => @$empenho->sistema_origem,
                'contrato_id' => @$empenho->contratoempenho->contrato_id,
                'created_at' => @$empenho->created_at,
                'updated_at' => @$empenho->updated_at
            ];
        }

        return response()->json($empenhos_array);
    }

    /**
     * Grava registro em contratoempenho, conforme $empenho, $fornecedor e $contrato
     *
     * @param $empenho
     * @param $fornecedor
     * @param $contrato
     * @return array
     * @author Anderson Sathler <asathler@gmail.com>
     */
    public function gravaContratoEmpenho($empenho, $fornecedor, $contrato)
    {
        $modelo = new Contratoempenho();

        $modelo->empenho_id = $empenho;
        $modelo->fornecedor_id = $fornecedor;
        $modelo->contrato_id = $contrato;

        $modelo->save();

        return json_encode([true]);
    }

    /**
     * @OA\Get(
     *     path="/api/v1/empenho/pdm/{pdm}/ug/{unidade}/ano/{ano}",
     *     security={ {"bearerAuth": {} }},
     *     summary="Retorna empenhos por PDM",
     *     tags={"Empenhos"},
     *     @OA\Parameter(
     *         name="pdm",
     *         in="path",
     *         description="Código do PDM",
     *         example="00910",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="unidade",
     *         in="path",
     *         description="Código da UG",
     *         example="110161",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ano",
     *         in="path",
     *         description="Ano",
     *         example="2023",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Valor de empenhos por PDM retornados com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/EmpenhosPDM")
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="Sem valor para PDM informado",
     *         @OA\JsonContent(ref="#/components/schemas/SemDados")
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/FalhaAutenticação")
     *     ),
     * )
     */
    public function empenhosPorPDM($pdm, $ug, $ano){

        $catpdm = $this->retornaEmpenhosPorPDM(str_pad($pdm, 5, 0, STR_PAD_LEFT), $ug, $ano);

        if(!$catpdm){
            return response()->json("Não foram encontrados registros.", JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
        }

        return response()->json($catpdm, 200,[],JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }
    /**
     * @OA\Get(
     *     path="/api/v1/empenho/codigoservico/{codigo}/ug/{unidade}/ano/{ano}",
     *     security={ {"bearerAuth": {} }},
     *     summary="Retorna empenhos por código de serviço",
     *     tags={"Empenhos"},
     *     @OA\Parameter(
     *         name="codigo",
     *         in="path",
     *         description="Código de serviço",
     *         example="25909",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="unidade",
     *         in="path",
     *         description="Código da UG",
     *         example="110161",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ano",
     *         in="path",
     *         description="Ano",
     *         example="2023",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Empenhos por serviço retornados com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/EmpenhosCodigoServico")
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/FalhaAutenticação")
     *     ),
     * )
     */
    public function empenhosPorServico($codigo, $ug, $ano)
    {
        $arrayServicoEmpenho = [];

        $unidade_id = Unidade::where('codigo', $ug)->first()->id;

        $servicoEmpenho = Catmatseritem::select([
            'catmatseritens.id',
            'catmatseritens.codigo_siasg',
            \DB::raw("EXTRACT('Year' FROM minutaempenhos.data_emissao) as ano_minuta_emissao"),
            \DB::raw("LTRIM(TO_CHAR(SUM(compra_item_minuta_empenho.valor), 'FM999G999G000D0099'), '0') AS valor"),
        ])
            ->join('catmatsergrupos', 'catmatseritens.grupo_id', '=', 'catmatsergrupos.id')
            ->join('compra_items', 'compra_items.catmatseritem_id', '=', 'catmatseritens.id')
            ->join('codigoitens', 'codigoitens.id', '=', 'compra_items.tipo_item_id')
            ->join('codigos', 'codigos.id', '=', 'codigoitens.codigo_id')
            ->join('compra_item_minuta_empenho', 'compra_item_minuta_empenho.compra_item_id', '=', 'compra_items.id')
            ->join('minutaempenhos', 'minutaempenhos.id', '=', 'compra_item_minuta_empenho.minutaempenho_id')
            ->join('saldo_contabil', 'saldo_contabil.id', '=', 'minutaempenhos.saldo_contabil_id')
            ->join('unidades', 'unidades.id', '=', 'saldo_contabil.unidade_id')
            ->join('minutaempenhos_remessa', 'compra_item_minuta_empenho.minutaempenhos_remessa_id', '=', 'minutaempenhos_remessa.id')
            ->where('minutaempenhos.situacao_id', '=', 217)
            ->where('minutaempenhos.unidade_id', '=', $unidade_id)
            ->where('minutaempenhos.mensagem_siafi', 'like', "%$ano%")
            ->where('catmatseritens.codigo_siasg', '=', $codigo)
            ->where(function ($query) {
                $query->where(function ($subquery) {
                    $subquery->where('minutaempenhos_remessa.situacao_id', '=', 215)
                        ->where('minutaempenhos_remessa.remessa', '=', 0);
                })->orWhere(function ($subquery) {
                    $subquery->where('minutaempenhos_remessa.remessa', '>', 0)
                        ->where('minutaempenhos_remessa.situacao_id', '=', 217);
                });
            })
            ->where('codigoitens.descricao', '=', 'Serviço')
            ->where('codigos.descricao', '=', 'Tipo CATMAT e CATSER')
            ->groupBy("catmatseritens.id", "catmatseritens.codigo_siasg", "catmatseritens.descricao", \DB::raw("EXTRACT('Year' FROM minutaempenhos.data_emissao)"))
            ->havingRaw("SUM(compra_item_minuta_empenho.valor) != 0");

        $servicoEmpenhoUnion = Catmatseritem::select([
            'catmatseritens.id',
            'catmatseritens.codigo_siasg',
            \DB::raw("EXTRACT('Year' FROM minutaempenhos.data_emissao) as ano_minuta_emissao"),
            \DB::raw("LTRIM(TO_CHAR(SUM(cime.valor), 'FM999G999G000D0099'), '0') AS valor"),
        ])
            ->join('catmatsergrupos', 'catmatseritens.grupo_id', '=', 'catmatsergrupos.id')
            ->join('contratoitens as c', 'c.catmatseritem_id', '=', 'catmatseritens.id')
            ->join('contrato_item_minuta_empenho as cime', 'cime.contrato_item_id', '=', 'c.id')
            ->join('minutaempenhos', 'minutaempenhos.id', '=', 'cime.minutaempenho_id')
            ->join('saldo_contabil', 'saldo_contabil.id', '=', 'minutaempenhos.saldo_contabil_id')
            ->join('unidades', 'unidades.id', '=', 'saldo_contabil.unidade_id')
            ->join('minutaempenhos_remessa', 'cime.minutaempenhos_remessa_id', '=', 'minutaempenhos_remessa.id')
            ->join('codigoitens', 'codigoitens.id', '=', 'c.tipo_id')
            ->join('codigos', 'codigos.id', '=', 'codigoitens.codigo_id')
            ->where('minutaempenhos.situacao_id', '=', 217)
            ->where('minutaempenhos.unidade_id', '=', $unidade_id)
            ->where('catmatseritens.codigo_siasg', '=', $codigo)
            ->where('minutaempenhos.mensagem_siafi', 'like', "%$ano%")
            ->where(function ($query) {
                $query->where(function ($subquery) {
                    $subquery->where('minutaempenhos_remessa.situacao_id', '=', 215)
                        ->where('minutaempenhos_remessa.remessa', '=', 0);
                })->orWhere(function ($subquery) {
                    $subquery->where('minutaempenhos_remessa.remessa', '>', 0)
                        ->where('minutaempenhos_remessa.situacao_id', '=', 217);
                });
            })
            ->where('codigoitens.descricao', '=', 'Serviço')
            ->where('codigos.descricao', '=', 'Tipo CATMAT e CATSER')
            ->groupBy("catmatseritens.id", "catmatseritens.codigo_siasg", "catmatseritens.descricao", \DB::raw("EXTRACT('Year' FROM minutaempenhos.data_emissao)"))
            ->havingRaw("SUM(cime.valor) != 0")
            ->orderByDesc(\DB::raw("EXTRACT('Year' FROM minutaempenhos.data_emissao)"));

        $resultServicoEmpenho = $servicoEmpenho->union($servicoEmpenhoUnion)->get()->first();

        if($resultServicoEmpenho){
            $arrayServicoEmpenho = $this->montaServicoEmpenhoAPI($resultServicoEmpenho, $unidade_id, $ano);
        }

        if (!$arrayServicoEmpenho) {
            return response()->json("Não foram encontrados registros.", 404, [], JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);

        }
        return response()->json($arrayServicoEmpenho, 200, [], JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
    }

    private function montaServicoEmpenhoAPI($servicoEmpenho, $unidade_id, $ano)
    {

        $servicoEmpenhoItens = Catmatseritem::select([
            'uc.codigo                       as unidade_compra',
            'compras.numero_ano              as numero_ano_compra',
            'catmatseritens.descricao        as descricao_item',
            'catmatseritens.codigo_siasg     as codigo_item',
            'unidade_minuta.codigo           as unidade_minuta',
            'codigoitens.descricao           as modalidade_compra',
            'minutaempenhos.mensagem_siafi   as numero_empenho',
            'unidades.codigosiafi            as unidade_emitente_empenho',
            'amparo_legal_view.amparo_legal',
            \DB::raw("CASE
            WHEN SUM(compra_item_minuta_empenho.valor) >= 1
            THEN LTRIM(TO_CHAR(SUM(compra_item_minuta_empenho.valor), 'FM999G999G000D0099'), '0')
            ELSE TO_CHAR(SUM(compra_item_minuta_empenho.valor), 'FM99G999G0D0099')
            END AS valor_empenhado"),
        ])
            ->distinct()
            ->join('catmatsergrupos', 'catmatseritens.grupo_id', '=', 'catmatsergrupos.id')
            ->join('compra_items', 'compra_items.catmatseritem_id', '=', 'catmatseritens.id')
            ->join('compra_item_minuta_empenho', 'compra_item_minuta_empenho.compra_item_id', '=', 'compra_items.id')
            ->join('minutaempenhos', 'minutaempenhos.id', '=', 'compra_item_minuta_empenho.minutaempenho_id')
            ->join('saldo_contabil', 'saldo_contabil.id', '=', 'minutaempenhos.saldo_contabil_id')
            ->join('unidades', 'unidades.id', '=', 'saldo_contabil.unidade_id')
            ->join('minutaempenhos_remessa', 'compra_item_minuta_empenho.minutaempenhos_remessa_id', '=', 'minutaempenhos_remessa.id')
            ->join('codigoitens', 'codigoitens.id', '=', 'compra_items.tipo_item_id')
            ->join('codigos', 'codigos.id', '=', 'codigoitens.codigo_id')
            ->join('compras', 'compras.id', '=', 'minutaempenhos.compra_id')
            ->join('codigoitens as ci', 'ci.id', '=', 'compras.modalidade_id')
            ->join('unidades as unidade_minuta', 'unidade_minuta.id', '=', 'minutaempenhos.unidade_id')
            ->join('unidades as uc', 'uc.id', '=', 'compras.unidade_origem_id')
            ->join('amparo_legal_view', 'amparo_legal_view.id', '=', 'minutaempenhos.amparo_legal_id')
            ->where('catmatseritens.id', $servicoEmpenho->id)
            ->where('minutaempenhos.unidade_id', '=', $unidade_id)
            ->where('minutaempenhos.mensagem_siafi', 'like', "%$ano%")
            ->where(function ($query) {
                $query->where(function ($subquery) {
                    $subquery->where('minutaempenhos_remessa.situacao_id', '=', 215)
                        ->where('minutaempenhos_remessa.remessa', '=', 0);
                })->orWhere(function ($subquery) {
                    $subquery->where('minutaempenhos_remessa.remessa', '>', 0)
                        ->where('minutaempenhos_remessa.situacao_id', '=', 217);
                });
            })
            ->groupBy(
                'compra_items.descricaodetalhada',
                \DB::raw("EXTRACT('Year' FROM minutaempenhos.data_emissao)"),
                'uc.codigo',
                'compras.numero_ano',
                'codigoitens.descricao',
                'compras.lei',
                'catmatseritens.descricao',
                'catmatseritens.codigo_siasg',
                'unidade_minuta.codigo',
                'minutaempenhos.mensagem_siafi',
                'unidades.codigosiafi',
                'amparo_legal_view.amparo_legal'
            )
            ->havingRaw("SUM(compra_item_minuta_empenho.valor) != 0");

        $servicoEmpenhoItensUnion = Catmatseritem::select([
            'uc.codigo                       as unidade_compra',
            'compras.numero_ano              as numero_ano_compra',
            'catmatseritens.descricao        as descricao_item',
            'catmatseritens.codigo_siasg     as codigo_item',
            'unidade_minuta.codigo           as unidade_minuta',
            'codigoitens.descricao           as modalidade_compra',
            'minutaempenhos.mensagem_siafi   as numero_empenho',
            'unidades.codigosiafi            as unidade_emitente_empenho',
            'amparo_legal_view.amparo_legal',
            \DB::raw("CASE
            WHEN SUM(cime.valor) >= 1
            THEN LTRIM(TO_CHAR(SUM(cime.valor), 'FM999G999G000D0099'), '0')
            ELSE TO_CHAR(SUM(cime.valor), 'FM99G999G0D0099')
            END AS valor_empenhado"),
        ])
            ->distinct()
            ->join('catmatsergrupos', 'catmatseritens.grupo_id', '=', 'catmatsergrupos.id')
            ->join('contratoitens', 'contratoitens.catmatseritem_id', '=', 'catmatseritens.id')
            ->join('contrato_item_minuta_empenho as cime', 'contratoitens.id', '=', 'cime.contrato_item_id')
            ->join('minutaempenhos', 'minutaempenhos.id', '=', 'cime.minutaempenho_id')
            ->join('saldo_contabil', 'saldo_contabil.id', '=', 'minutaempenhos.saldo_contabil_id')
            ->join('unidades', 'unidades.id', '=', 'saldo_contabil.unidade_id')
            ->join('minutaempenhos_remessa', 'cime.minutaempenhos_remessa_id', '=', 'minutaempenhos_remessa.id')
            ->join('codigoitens', 'codigoitens.id', '=', 'contratoitens.tipo_id')
            ->join('codigos', 'codigos.id', '=', 'codigoitens.codigo_id')
            ->join('compras', 'compras.id', '=', 'minutaempenhos.compra_id')
            ->join('codigoitens as ci', 'ci.id', '=', 'compras.modalidade_id')
            ->join('unidades as unidade_minuta', 'unidade_minuta.id', '=', 'minutaempenhos.unidade_id')
            ->join('unidades as uc', 'uc.id', '=', 'compras.unidade_origem_id')
            ->join('amparo_legal_view', 'amparo_legal_view.id', '=', 'minutaempenhos.amparo_legal_id')
            ->where('catmatseritens.id', $servicoEmpenho->id)
            ->where('minutaempenhos.unidade_id', '=', $unidade_id)
            ->where('minutaempenhos.mensagem_siafi', 'like', "%$ano%")
            ->where(function ($query) {
                $query->where(function ($subquery) {
                    $subquery->where('minutaempenhos_remessa.situacao_id', '=', 215)
                        ->where('minutaempenhos_remessa.remessa', '=', 0);
                })->orWhere(function ($subquery) {
                    $subquery->where('minutaempenhos_remessa.remessa', '>', 0)
                        ->where('minutaempenhos_remessa.situacao_id', '=', 217);
                });
            })
            ->groupBy(
                'contratoitens.descricao_complementar',
                \DB::raw("EXTRACT('Year' FROM minutaempenhos.data_emissao)"),
                'uc.codigo',
                'compras.numero_ano',
                'codigoitens.descricao',
                'compras.lei',
                'catmatseritens.descricao',
                'catmatseritens.codigo_siasg',
                'unidade_minuta.codigo',
                'minutaempenhos.mensagem_siafi',
                'unidades.codigosiafi',
                'amparo_legal_view.amparo_legal'
            )
            ->havingRaw("SUM(cime.valor) != 0");

        $resultServicoEmpenhoItens = $servicoEmpenhoItens->union($servicoEmpenhoItensUnion)->get()->toArray();

        $array = [
            'ano' => $servicoEmpenho->ano_minuta_emissao,
            'codigo' => $servicoEmpenho->codigo_siasg,
            'valor' => $servicoEmpenho->valor,
            'empenhos' => $resultServicoEmpenhoItens
        ];

        return $array;
    }

    /**
     * @OA\Get(
     *     tags={"Empenhos"},
     *     summary="Retorna uma lista de empenhos, de um determinado ano, que possuam contratos vinculados.",
     *     description="",
     *     path="/api/v1/empenho/ano/{ano}",
     *     security={ {"bearerAuth": {} }},
     *
     *     @OA\Parameter(
     *         name="ano",
     *         in="path",
     *         description="Ano do empenho",
     *         example="2024",
     *         required=true,
     *         @OA\Schema(
     *              type="integer"
     *        )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="Registros retornados com sucesso.",
     *         @OA\JsonContent(ref="#/components/schemas/EmpenhosComContratosVinculados")
     *     ),
     *
     *     @OA\Response(
     *         response=401,
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/FalhaAutenticação")
     *     ),
     *
     * )
    */
    public function empenhosComContratosVinculados(int $ano)
    {
        $empenhos_array = [];
        if (isset($ano)) {
            try {
                $empenhos_array = Contratoempenho::contratosEmpenhosPorAno($ano)->toArray();
            } catch (\Exception $e) {
                return response()->json(['error' => $e->getMessage()], 500);
            }
        }
        return response()->json($empenhos_array);
    }

    /**
     *
     * *
     *          @OA\Schema(
     *             schema="auth_failure_jwt_token_expired",
     *             type="object",
     *             @OA\Property(property="error",type="string",example="Token is Expired"),
     *             @OA\Property(property="error_token_not_found",type="string",example="Authorization Token not found"),
     *             @OA\Property(property="error_token_invalid",type="string",example="Token is Invalid"),
     *       ),
     *
     *      @OA\Schema(
     *             schema="empenho_not_found",
     *             type="object",
     *             @OA\Property(property="error",type="string",example="Empenho não encontrado"),
     *       ),
     *      @OA\Schema(
     *             schema="empenho_invalid_id",
     *             type="object",
     *             @OA\Property(property="error",type="string",example="Valor numérico fora do intervalo permitido"),
     *       ),
     *
     *  *          @OA\Schema(
     *              schema="consulta_empenho_por_id",
     *              type="object",
     *             @OA\Property(property="id_empenho",type="integer",example="14136"),
     *             @OA\Property(property="data_emissao_empenho", type="string",example="2021-03-11", format="yyyy-mm-dd"),
     *             @OA\Property(property="ug_emitente_empenho",type="integer",example="200350"),
     *             @OA\Property(property="numero_empenho",type="string", example="2021NE000058"),
     *             @OA\Property(property="cpf_cnpj_fornecedor_empenho",type="string",example="04.664.903/0001-28"),
     *             @OA\Property(property="nome_fornecedor_empenho",type="string",example="FCIA VETER PET LTDA"),
     *             @OA\Property(property="tipo_empenho",type="string",example="Contrato"),
     *             @OA\Property(property="uasg_compra",type="integer", example="200350"),
     *             @OA\Property(property="modalidade_compra",type="string",example="Pregão"),
     *             @OA\Property(property="numero_compra",type="string",example="00004/2019"),
     *             @OA\Property(property="uasg_beneficiaria",type="integer",example="null"),
     *             @OA\Property(property="numero_contrato",type="string",example="00004/2019"),
     *             @OA\Property(property="chave",type="string",example="20035005000042019 - UASG Minuta: 110161"),
     *             @OA\Property(property="empenhado",type="number",example="1.361.640,02"),
     *             @OA\Property(property="aliquidar",type="number",example="231.667,64"),
     *             @OA\Property(property="liquidado",type="number",example="20088.56"),
     *             @OA\Property(property="pago",type="number",example="1.129.972,38"),
     *             @OA\Property(property="rpinscrito",type="number",example="231.667,64"),
     *             @OA\Property(property="rpaliquidar",type="number",example="128.941,72"),
     *             @OA\Property(property="rpliquidado",type="number",example="128.941,72"),
     *             @OA\Property(property="rppago",type="number",example="102.725,92"),
     *             @OA\Property(property="id_sistema_origem",type="string",example="SIAFI"),
     *             @OA\Property(property="itens_minuta", type="array", @OA\Items(type="object", ref="#/components/schemas/consulta_empenho_por_id_itens_compra")),
     * )
     *
     * @OA\Schema(
     *     schema="consulta_empenho_por_id_itens_compra",
     *     type="object",
     *             @OA\Property(property="sequencial_siafi", type="integer", example="1"),
     *             @OA\Property(property="numero_item_compra", type="integer", example="00013"),
     *             @OA\Property(property="codigo_item", type="integer", example="439541"),
     *             @OA\Property(property="subelemento", type="string", example="339037 - LOCACAO DE MAO-DE-OBRA"),
     *             @OA\Property(property="descricao", type="string", example="VACINA"),
     *             @OA\Property(property="descricao_detalhada", type="string", example="null"),
     *             @OA\Property(property="quantidade", type="number", example="2,00000"),
     *             @OA\Property(property="valor_unitario", type="number", example="40,0000"),
     *             @OA\Property(property="valor_total", type="number", example="80,00"),
     *     @OA\Property(property="alteracao_itens", type="array", @OA\Items(type="object", ref="#/components/schemas/consulta_empenho_por_id_itens_compra_alteracao")),
     * )
     *
     * @OA\Schema(
     *     schema="consulta_empenho_por_id_itens_compra_alteracao",
     *     type="object",
     *             @OA\Property(property="tipo_operacao",type="string", example="ANULACAO"),
     *             @OA\Property(property="quantidade_alteracao",type="number", example="-5.00000"),
     *             @OA\Property(property="valor_unitario_alteracao",type="number", example="62996.8800"),
     *             @OA\Property(property="valor_total_alteracao",type="number", example="-314984.40"),
     * )
     *
     * @OA\Get(
     *     path="/api/v1/contrato/empenho/consultar/{empenho_id}",
     *     security={{"bearerAuth": {}}},
     *     summary="Consulta os dados do empenho pelo id do empenho informado",
     *     tags={"Empenhos"},
     *     @OA\Parameter(
     *         name="empenho_id",
     *         in="path",
     *         description="Id do empenho",
     *         example="14136",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="Consulta os dados do empenho pelo id do empenho informado",
     *         @OA\JsonContent(ref="#/components/schemas/consulta_empenho_por_id")
     *     ),
     *
     *     @OA\Response(
     *         response=401,
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/auth_failure_jwt_token_expired"),
     *     ),
     *
     *     @OA\Response(
     *         response=404,
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/empenho_not_found")
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/empenho_invalid_id")
     *     ),
     *
     *     @OA\Response(
     *         response=403,
     *         description="Usuário sem permissão no sistema",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="status", type="string", example="error"),
     *             @OA\Property(property="message", type="string", example="Usuário sem permissão no sistema")
     *         )
     *     ),
     * )
     *
     */

    public function consultarEmpenhoPorId($empenhoId)
    {
            return $this->empenhoService->consultarEmpenhoPorId($empenhoId);
    }

}
