<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Traits\NonceTrait;
use App\Models\Apropriacao;

use App\Repositories\Nonce\NonceRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Exception;

class ApropriacaoController
{
    use NonceTrait;
    /**
     * @var NonceRepository
     */
    private $nonceRepository;

    /**
     * @param NonceRepository $nonceRepository
     */
    public function __construct(NonceRepository $nonceRepository)
    {
        $this->nonceRepository = $nonceRepository;
    }
    public function excluirApropriacao(Request $request)
    {
        try {

            # Executar a funcionalidade da rota (Cadastro, exclusão, etc.)

            $id = 123; # Simula o id do registro (que foi cadastrado, que será excluído, etc.)

            /*
            EXEMPLO RETORNO DE ERRO 1 - Retorno de erro conhecido
            Pode ser retornado uma mensagem de erro ou de qualquer tipo que for necessário
            Essa mensagem será salva na requisicoes_nonce pelo NonceMiddleware
            */
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Mensagem de erro'
                ],
                Response::HTTP_NOT_FOUND # substituir pelo tipo do erro que se deseja retornar
            );

            /*
            EXEMPLO RETORNO DE ERRO 2 - Retorno de erro forçado
            Pode-se retornar um erro gerando uma Execpetion quando necessário
            */
            #throw new Exception('Mensagem de erro', /*Tipo do erro*/Response::HTTP_UNPROCESSABLE_ENTITY);

            # Se a rotina deu certo (cadastrou, excluiu, etc.), registra o nonce passando o ID do registro
            $nonceId = $this->nonceRepository->createNonce(
                $request->all(),
                request()->ip(),
                true,
                # Esta é a mensagem que indica o que aconteceu com o registro
                # (se foi cadastrado, excluído, se deu erro, etc)
                'Registro excluído com sucesso',
                $id
            );

            # Se tudo deu certo na rotina, retorna uma mensagem de sucesso
            return response()->json([
                'status' => 'success',
                'message' => 'Mensagem de cadastro realizado com sucesso' # exemplo: "Registro excluído com sucesso"
            ]);
        } catch (Exception $e) {
            throw new Exception($this->formatExceptionMessage($e), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

    }

    public function messageErrorNotFound(): string
    {
        return 'Apropriação não encontrada';
    }

}
