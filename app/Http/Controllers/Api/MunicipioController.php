<?php

namespace App\Http\Controllers\Api;

use App\Models\Municipio;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MunicipioController extends Controller
{
    public function index(Request $request)
    {
        $search_term = $request->input('q');
        $form = collect($request->input('form'))->pluck('value', 'name');

        $options = Municipio::query();

        // if no category has been selected, show no options
        if (!$form['uf']) {
            return [];
        }

        // if a category has been selected, only show articles in that category
        if ($form['uf']) {
            $options = $options->where('estado_id', $form['uf']);
        }

        if ($search_term) {
            return $options->where('nome', 'ilike', '%' . strtoupper($search_term) . '%')
                ->orderBy('nome')
                ->paginate(10);
        }

        return $options->paginate(10);
    }

    public function show($id)
    {
        return Municipio::find($id);
    }

    public function buscaMunicipio(Request $request)
    {
        $search_term = $request->input('q');

        $options = Municipio::query();

        if ($search_term) {
            $estado = null;

            if(strpos($search_term, ' - ') !== false){
                list($cidade, $estado) = explode(' - ', $search_term, 2);
            }else{
                $cidade = $search_term;
            }

            return $options->selectRaw("municipios.id,CONCAT(municipios.nome, ' - ', estados.sigla) as nome")
                ->join('estados', 'estados.id', '=', 'municipios.estado_id')
                ->where('municipios.nome', 'ilike', '%' . mb_convert_case($cidade, MB_CASE_TITLE, "UTF-8") . '%')
                ->when($estado, function ($query) use ($estado) {
                    return $query->where('estados.sigla', 'ilike', '%' . strtoupper($estado) . '%');
                })
                ->orderBy('municipios.nome')
                ->paginate(10);
        }

        return $options
            ->selectRaw("municipios.id,CONCAT(municipios.nome, ' - ', estados.sigla) as nome")
            ->join('estados', 'estados.id', '=', 'municipios.estado_id')
            ->paginate(10);
    }

    public function buscaMunicipioApropriacao(Request $request)
    {
        $search_term = $request->input('q');

        $options = Municipio::query();

        if ($search_term) {
            $estado = null;

            if (strpos($search_term, ' - ') !== false) {
                list($cidade, $estado) = explode(' - ', $search_term, 2);
            } else {
                $cidade = $search_term;
            }

            return $options->selectRaw("municipios.codigo_municipio, CONCAT(municipios.nome, ' - ', estados.sigla) as nome")
                ->join('estados', 'estados.id', '=', 'municipios.estado_id')
                ->where(function ($query) use ($cidade) {
                    $query->whereRaw("unaccent(municipios.nome) ILIKE unaccent(?)", ['%' . mb_convert_case($cidade, MB_CASE_TITLE, "UTF-8") . '%'])
                        ->orWhere('municipios.codigo_municipio', $cidade);
                })
                ->when($estado, function ($query) use ($estado) {
                    return $query->whereRaw("unaccent(estados.sigla) ILIKE unaccent(?)", ['%' . strtoupper($estado) . '%']);
                })
                ->orderBy('municipios.nome')
                ->paginate(10);
        }

        // Caso não haja termos de pesquisa, retorna todos os resultados paginados
        return $options
            ->selectRaw("municipios.codigo_municipio, CONCAT(municipios.nome, ' - ', estados.sigla) as nome")
            ->join('estados', 'estados.id', '=', 'municipios.estado_id')
            ->paginate(10);
    }
}
