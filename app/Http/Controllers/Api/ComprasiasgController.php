<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\NDC;
use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\ConsultaCompra;
use App\Http\Traits\Formatador;
use App\Http\Traits\UpdateOrCreateCompraTrait;
use App\Models\Catmatseritem;
use App\Models\Compra;
use App\Models\Fornecedor;
use App\Models\Siasgcompra;
use App\Models\Unidade;
use App\XML\ApiSiasg;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\services\CompraService;

class ComprasiasgController extends Controller
{
    use ConsultaCompra;
    use BuscaCodigoItens;
    use UpdateOrCreateCompraTrait;
    use Formatador;

    protected $compraService;

    public function __construct(CompraService $compraService)
    {
        $this->compraService = $compraService;
    }

    public function index(Request $request)
    {
        $search_term = $request->input('q');
        $page = $request->input('page');

        if ($search_term) {
            $results = Siasgcompra::select(
                DB::raw("CONCAT(unidades.codigosiasg,'-',unidades.nomeresumido) AS unidadecompra"),
                DB::raw("CONCAT(siasgcompras.numero,'/',siasgcompras.ano) AS numerocompra"),
                'siasgcompras.id AS id'
            )
                ->join('unidades', 'siasgcompras.unidade_id', '=', 'unidades.id')
                ->where('unidades.codigosiasg', 'LIKE', '%' . strtoupper($search_term) . '%')
                ->orWhere('unidades.nomeresumido', 'LIKE', '%' . strtoupper($search_term) . '%')
                ->orWhere('siasgcompras.numero', 'LIKE', '%' . strtoupper($search_term) . '%')
                ->orWhere('siasgcompras.ano', 'LIKE', '%' . strtoupper($search_term) . '%')
                ->paginate(10);
        } else {
            $results = Siasgcompra::select(
                DB::raw("CONCAT(unidades.codigosiasg,' - ',unidades.nomeresumido) AS unidadecompra"),
                DB::raw("CONCAT(siasgcompras.numero,' - ',siasgcompras.ano) AS numerocompra"),
                'siasgcompras.id AS id'
            )
                ->join('unidades', 'siasgcompras.unidade_id', '=', 'unidades.id')
                ->paginate(10);
        }

        return $results;
    }

    public function show($id)
    {
        return Siasgcompra::select(
            DB::raw("CONCAT(unidades.codigosiasg,' - ',unidades.nomeresumido) AS unidadecompra"),
            DB::raw("CONCAT(siasgcompras.numero,' - ',siasgcompras.ano) AS numerocompra"),
            'siasgcompras.id AS id'
        )
            ->join('unidades', 'siasgcompras.unidade_id', '=', 'unidades.id')
            ->where('siasgcompras.id', $id)
            ->first();
    }

    public function retornarItensCompra(Request $request)
    {
        $search_term = $request->input('q');
        $page = $request->input('page');

        $form = collect($request->input('form'))->pluck('value', 'name');
        $itensFiltrados = collect($request->input('form'))
            ->filter(
                function ($value) {
                    return $value['name'] === 'numero_item_compra';
                }
            )
            ->map(
                function ($item) {
                    return $item['value'];
                }
            )->toArray();

        $modalidade = $this->retornaDescresPorId($form['modalidade_id']);
        $numero_contratacao = null;
        if ($form['contrata_mais_brasil'] == '1') {
            $retornoSiasg = $this->buscarCompra(
                '',
                '',
                '',
                '',
                '',
                '',
                $this->converterIdContratacaoPNCP($form['numero_contratacao'])
            );
            if (isset($retornoSiasg->data)) {
                $unidade = new Unidade();
                $form['unidadecompra_id'] = $unidade->buscaUnidadeExecutoraPorCodigo(
                    $retornoSiasg->data->compraSispp->unidade
                );

                $form['licitacao_numero'] = $retornoSiasg->data->compraSispp->numeroAno;
                $form['modalidade_id'] = $this->retornaIdCodigoItemPorDescres(
                    $retornoSiasg->data->compraSispp->codigoModalidade, 'Modalidade Licitação'
                );
                if ($retornoSiasg->data->compraSispp->subrogada !== '000000') {

                    $form['unidadebeneficiaria_id'] = $retornoSiasg->data->compraSispp->subrogada;
                }
                $fornecedor = $this->formataCnpjCpf($retornoSiasg->data->itemCompraSisppDTO[0]->niFornecedor);
                $form['fornecedor_id'] = $fornecedor_id = Fornecedor::where(['cpf_cnpj_idgener' => trim($fornecedor)])->first()->id;
                $numero_contratacao = $form['numero_contratacao'];

            }

        } else {


            $fornecedor_id = $form['fornecedor_id'];
            $uasgCompra = Unidade::find($form['unidadecompra_id'])->codigo;
            $uasgUser = session('user_ug');
            $numero_ano = implode('', explode('/', $form['licitacao_numero']));
            $numero_compra = explode('/', $form['licitacao_numero']);
            $uasgBeneficiaria = isset($form['unidadebeneficiaria_id'])
                ? Unidade::find($form['unidadebeneficiaria_id'])->codigo : null;

            $retornoSiasg = $this->buscarCompra(
                $modalidade,
                $uasgCompra,
                $uasgUser,
                $uasgBeneficiaria,
                $numero_compra[0],
                $numero_compra[1]
            );
        }

        if (is_null($retornoSiasg->data) && $retornoSiasg->messagem == 'UASG beneficiária não pertence a compra') {
            return false;
        }

        if (is_null($retornoSiasg->data)) {
            //TODO transformar o codigo abaixo em um metodo na ConsultaCompra, ele está duplicado no arquivo ValidarCompra
            $compra = $this->verificaCompraExiste([
                'unidade_origem_id' => $form['unidadecompra_id'],
                'modalidade_id' => $form['modalidade_id'],
                'numero_ano' => $form['licitacao_numero']
            ]);

            if (!$compra) {
                return false;
            }

            $compraPertenceLei14133Derivadas = $this->compraService->compraPertenceLei14133Derivadas($compra->lei);

            //todo verificar compras de sisrp
            if ($compra->tipo_compra_desc === 'SISRP' && $compraPertenceLei14133Derivadas) {
                if (!($compra->verificaPermissaoSisrp(session('user_ug_id')))) {
                    return false;
                }
                $compra['subrrogada'] = '000000';
                if ($compra->unidade_subrrogada_id !== null) {
                    $compra['subrrogada'] = $compra->unidade_subrrogada->codigo;
                }

                $retornoSiasg = (object)[
                    'data' => (object)[
                        'compraSispp' => (object)[
                            'tipoCompra' => '2',
                            'inciso' => $compra['inciso'],
                            'lei' => $compra['lei'],
                            'artigo' => $compra['artigo'],
                            'subrogada' => $compra['subrrogada'],
                            'idUnico' => $compra['id_unico'],
                            'cnpjOrgao' => $compra['cnpjOrgao'],
                            'ano' => substr($compra['numero_ano'], 6, 6),
                        ]
                    ],
                    'messagem' => 'Sucesso',
                    'codigoRetorno' => 200
                ];
            }
        }

        $verificacao = $this->verificaPermissaoUasgCompra($retornoSiasg, $form['unidadecompra_id']);
        if (is_null($verificacao)) {
            return false;
        }

        # compras do NDC deve validar a modalidade
        $origemCompra = $this->compraService->getApiOrigem($retornoSiasg);

        if (($origemCompra === 1 || $origemCompra === 3) && $retornoSiasg->data->compraSispp->codigoModalidade != $modalidade) {
            return false;
        }

        $compraPertenceLei14133Derivadas =
            $this->compraService->compraPertenceLei14133Derivadas($retornoSiasg->data->compraSispp->lei);

        if (!($compraPertenceLei14133Derivadas
            && $retornoSiasg->data->compraSispp->tipoCompra == '2')
        ) {
            $compra = $this->compraService->updateOrCreateCompra([
                'unidade_origem_id' => $form['unidadecompra_id'],
                'modalidade_id' => $form['modalidade_id'],
                'numero_ano' => $form['licitacao_numero'],
                'tipo_compra_id' => $this->retornaIdCodigoItemPorDescres(
                    '0' . $retornoSiasg->data->compraSispp->tipoCompra, 'Tipo Compra'
                ),
                'unidade_subrrogada_id' => $form['unidadebeneficiaria_id'] ?? NULL,
                'inciso' => $retornoSiasg->data->compraSispp->inciso,
                'lei' => $retornoSiasg->data->compraSispp->lei,
                'artigo' => $retornoSiasg->data->compraSispp->artigo,
                'id_unico' => $retornoSiasg->data->compraSispp->idUnico,
                'cnpjOrgao' => $retornoSiasg->data->compraSispp->cnpjOrgao,
                'origem' => $this->compraService->getApiOrigem($retornoSiasg),
                'numero_contratacao' => $numero_contratacao
            ]);
            //COMPRA SISPP
            if ($retornoSiasg->data->compraSispp->tipoCompra == '1') {
                $this->gravaParametroItensdaCompraSISPP($retornoSiasg, $compra);
            } else {
                $this->gravaParametroItensdaCompraSISRP($retornoSiasg, $compra);
            }
        }

        if ($compraPertenceLei14133Derivadas
            && $retornoSiasg->data->compraSispp->tipoCompra == '2'
        ) {
            //recupera a compra
            $compra = Compra::where(
                [
                    'modalidade_id' => $form['modalidade_id'],
                    'numero_ano' => $form['licitacao_numero'],
                    'unidade_origem_id' => $form['unidadecompra_id'],
                ]
            )->first();

            $this->compraService->atribuirCatmatpdmToItems($compra, $retornoSiasg);
        }

        $leiFormatada14133Derivadas = $this->compraService->recuperarAmparoLegalFormatadoPorLeiCompra($compra->lei);

        $query = Compra::join('compra_items', 'compra_items.compra_id', '=', 'compras.id')
            ->join('compra_item_unidade', 'compra_item_unidade.compra_item_id', '=', 'compra_items.id')
            ->join('compra_item_fornecedor', 'compra_item_fornecedor.compra_item_id', '=', 'compra_items.id')
            ->join('codigoitens', 'codigoitens.id', '=', 'compra_items.tipo_item_id')
            ->join('catmatseritens', 'catmatseritens.id', '=', 'compra_items.catmatseritem_id')
            ->join('codigoitens as mod', 'mod.id', '=', 'compras.modalidade_id')
            ->select(
                'compra_items.id',
                'compra_items.compra_id',
                'compra_items.tipo_item_id',
                'compra_items.catmatseritem_id',
                'compra_items.descricaodetalhada',
                'compra_items.qtd_total',
                'compra_items.numero',
                'compra_items.ata_vigencia_inicio',
                'compra_items.ata_vigencia_fim',
                'codigoitens.descricao AS tipo_item',
                'compra_item_unidade.id AS compra_item_unidade_id',
                'compra_item_unidade.quantidade_autorizada',
                'compra_item_unidade.quantidade_saldo',
                'compra_item_fornecedor.valor_negociado',
                'catmatseritens.codigo_siasg',
                'catmatseritens.descricao AS descricaocatmatseritens',
                'compra_items.criterio_julgamento',
                'compra_item_fornecedor.percentual_maior_desconto',
                'compra_item_fornecedor.quantidade_homologada_vencedor',

                DB::raw(
                    'CASE WHEN compras.lei in(\'' . implode("','", $leiFormatada14133Derivadas) . '\')
                    AND compras.tipo_compra_id = 245 THEN LEAST
                    (
                    compra_item_fornecedor.quantidade_homologada_vencedor,
                    compra_item_unidade.quantidade_autorizada
                    ) ELSE
                    compra_item_unidade.quantidade_autorizada
                    END AS quantidade_autorizada'
                ),

                DB::raw(
                    '"codigoitens"."descricao" || \' | \' || "compra_items"."numero"|| \' | \' ' .
                    '|| "compra_items"."descricaodetalhada" AS descricaomodal'
                ),

                DB::raw('CASE WHEN compra_item_fornecedor.percentual_maior_desconto IS NOT NULL
                      AND compra_items.criterio_julgamento = \'D\' AND compra_item_fornecedor.percentual_maior_desconto > 0
                      AND mod.descres <> \'99\'
                      AND mod.descres <> \'06\'
                      THEN TO_CHAR(compra_item_fornecedor.valor_unitario * ((100 - compra_item_fornecedor.percentual_maior_desconto) / 100), \'FM999999999.0000\')
                     ELSE TO_CHAR(compra_item_fornecedor.valor_unitario, \'FM999999999.0000\')
                    END AS valor_unitario'),
                'codigo_ncmnbs'
            )
            ->whereNotIn('compra_items.numero', $itensFiltrados)
            ->where('compras.id', $compra->id)
            ->where('compra_item_unidade.unidade_id', session('user_ug_id'))
            ->where('compra_item_fornecedor.fornecedor_id', $fornecedor_id)
            ->where(
                function ($query) use ($fornecedor_id) {
                    $query->whereNull('compra_item_unidade.fornecedor_id')
                        ->orWhere('compra_item_unidade.fornecedor_id', '=', $fornecedor_id);
                }
            )
            ->where(
                function ($query) use ($leiFormatada14133Derivadas) {
                    $query->where('compra_items.ata_vigencia_fim', '>=', date('Y-m-d'))
                        ->orWhere(
                            function ($query) use ($leiFormatada14133Derivadas) {
                                $query->whereNull('compra_items.ata_vigencia_fim')
                                    ->where(
                                        function ($query) use ($leiFormatada14133Derivadas) {
                                            $query->where('compras.tipo_compra_id', '<>', 245)
                                                ->orWhere(function ($query) use ($leiFormatada14133Derivadas) {
                                                    $query->whereNotIn('compras.lei', $leiFormatada14133Derivadas);
                                                });
                                        }
                                    )
                                    ->orWhere(
                                        function ($query) use ($leiFormatada14133Derivadas) {
                                            $query->where('compras.tipo_compra_id', 245)
                                                ->whereIn('compras.lei', $leiFormatada14133Derivadas)
                                                ->where('compra_item_fornecedor.ata_vigencia_inicio', '<=', date('Y-m-d'))
                                                ->where('compra_item_fornecedor.ata_vigencia_fim', '>=', date('Y-m-d'));
                                        }
                                    );
                            }
                        );
                }
            )
            ->where('compra_item_unidade.situacao', true)
            ->where('compra_item_fornecedor.situacao', true)
            ->where('compra_items.situacao', true)
            ->groupBy(
                'compra_items.id',
                'compra_items.compra_id',
                'compra_items.tipo_item_id',
                'compra_items.catmatseritem_id',
                'compra_items.descricaodetalhada',
                'compra_items.qtd_total',
                'compra_items.numero',
                'compra_items.ata_vigencia_inicio',
                'compra_items.ata_vigencia_fim',
                'codigoitens.descricao',
                'compra_item_unidade.id',
                'compra_item_unidade.quantidade_autorizada',
                'compra_item_unidade.quantidade_saldo',
                'compra_item_fornecedor.valor_unitario',
                'compra_item_fornecedor.valor_negociado',
                'compra_item_fornecedor.quantidade_homologada_vencedor',
                'catmatseritens.codigo_siasg',
                'catmatseritens.descricao',
                'compra_item_fornecedor.percentual_maior_desconto',
                'mod.descres',
                'catmatseritens.descricao',
                'compras.lei',
                'compras.tipo_compra_id',
                'codigo_ncmnbs'
            );

        if ($search_term) {
            $query->where(
                function ($query) use ($search_term) {
                    $query->where('compra_items.descricaodetalhada', 'ilike', '%' . $search_term . '%')
                        ->orWhere('codigoitens.descricao', 'ilike', '%' . $search_term . '%')
                        ->orWhere('compra_items.numero', 'ilike', '%' . $search_term . '%')
                        ->orWhere('catmatseritens.descricao', 'ilike', '%' . $search_term . '%');
                }
            );
        }

        return $query->paginate(100);
    }
}
