<?php

namespace App\Http\Controllers\Api\Nonce;

use App\Http\Controllers\Controller;
use App\Repositories\Nonce\NonceRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 *
 */
class NonceApiController extends Controller
{
    /**
     * @var NonceRepository
     */
    private $nonceRepository;

    /**
     * @param NonceRepository $nonceRepository
     */
    public function __construct(NonceRepository $nonceRepository)
    {
        $this->nonceRepository = $nonceRepository;
    }

    /**
     * @param $nonce
     * @return JsonResponse
     */
    public function show($nonce): JsonResponse
    {
        try {

            return response()->json([
                'status' => 'success',
                'message' => "Nonce $nonce encontrado com sucesso.",
                'nonce' => $this->nonceRepository->show($nonce)
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "status" => "error",
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    /**
     * @param Request $reques
     * @param mixed $nonce
     * @return JsonResponse
     */
    public function requisicoesNonce(Request $reques, $nonce): JsonResponse
    {
        try {

            return response()->json([
                'status' => 'success',
                'message' => "Nonce $nonce encontrado com sucesso.",
                'nonce' => $this->nonceRepository->requisicoesNonce($reques->all(), $nonce)
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "status" => "error",
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }

    /**
     * @param Request $reques
     * @param mixed $nonce
     * @return JsonResponse
     */
    public function todasRequisicoesNonce(Request $reques): JsonResponse
    {
        try {

            return response()->json([
                'status' => 'success',
                'requisicoes_nonce' => $this->nonceRepository->todasRequisicoesNonce($reques->all())
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "status" => "error",
                "message" => $e->getMessage()
            ], $e->getCode());
        }
    }
}
