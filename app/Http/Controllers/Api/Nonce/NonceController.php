<?php

namespace App\Http\Controllers\Api\Nonce;

use App\Helpers\ErrorMessage;
use App\Http\Controllers\Controller;
use App\Repositories\Nonce\NonceRepository;
use Illuminate\Http\JsonResponse;

/**
 *
 */
class NonceController extends Controller
{
    private $nonceRepository;

    public function __construct(NonceRepository $nonceRepository)
    {
        $this->nonceRepository = $nonceRepository;
    }

    /**
     * @OA\Get(
     *     path="/api/v1/contrato/nonce/consultar/{nonce}",
     *     security={ {"bearerAuth": {} }},
     *     summary="Consulta um NONCE",
     *     description="Consulta um NONCE com base no valor do parâmetro 'nonce'.",
     *     operationId="consultarNonce",
     *     tags={"Nonce"},
     *     @OA\Parameter(
     *         name="nonce",
     *         in="path",
     *         description="O valor do NONCE a ser consultado.",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     * @OA\Response(
     *     response=200,
     *     description="Sucesso",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string", example="success"),
     *         @OA\Property(property="message", type="string", example="Nonce encontrado com sucesso."),
     *         @OA\Property(
     *             property="nonce",
     *             type="object",
     *             @OA\Property(property="nonce", type="string", example="nonce1"),
     *             @OA\Property(property="status", type="boolean", example=true),
     *             @OA\Property(property="funcionalidade", type="string", example="Func1"),
     *             @OA\Property(
     *                 property="json_request",
     *                 type="object",
     *                 @OA\Property(property="key1", type="string", example="value1")
     *             ),
     *             @OA\Property(
     *                 property="json_response",
     *                 type="object",
     *                 @OA\Property(property="response1", type="string", example="data1")
     *             )
     *         )
     *     )
     * ),
     *     @OA\Response(
     *         response=403,
     *         description="Usuário sem permissão no sistema",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="status", type="string", example="error"),
     *             @OA\Property(property="message", type="string", example="Usuário sem permissão no sistema")
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Token expirado",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="status", type="string", example="error"),
     *             @OA\Property(property="message", type="string", example="Token expirado")
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Nonce não encontrado",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="status", type="string", example="error"),
     *             @OA\Property(property="message", type="string", example="Nonce não encontrado")
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Campo {campo} é obrigatório",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="status", type="string", example="error"),
     *             @OA\Property(property="message", type="string", example="Campo {campo} é obrigatório")
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Erro interno do servidor",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="status", type="string", example="error"),
     *             @OA\Property(property="message", type="string", example="Erro interno do servidor")
     *         )
     *     )
     * )
     */

    /**
     * @param $nonce
     * @return JsonResponse
     */

    public function consultar($nonce): JsonResponse
    {
        try {
            $data = $this->nonceRepository->show($nonce);

            return response()->json([
                'status' => 'success',
                'message' => "Nonce $nonce encontrado com sucesso.",
                'nonce' => $data
            ]);
        } catch (\Exception $e) {
            $statusCode = $e->getCode();
            $errorMessage = ErrorMessage::getErrorMessage($statusCode);

            return response()->json([
                "status" => "error",
                "message" => $errorMessage,
            ], $statusCode);
        }
    }
}
