<?php

namespace App\Http\Controllers\Api\MinutaEmpenho;

use App\Helpers\ErrorMessageAmparoLegal;
use App\services\AmparoLegalService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\AmparoLegalConsultaRequest;
use App\Http\Traits\ValidacoesMinutaEmpenhoTrait;

class AmparoLegalController extends Controller
{
    protected $amparoLegalService;

    public function __construct(AmparoLegalService $amparoLegalService)
    {
        $this->amparoLegalService = $amparoLegalService;
    }

    /**
     * @OA\Post(
     *     path="/api/v1/amparo/consultar",
     *     summary="Amparo Legal",
     *     tags={"Amparo Legal"},
     *     security={ {"bearerAuth": {} }},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             required={"ato_normativo", "artigo", "modalidade"},
     *             @OA\Property(property="ato_normativo", type="string", example="LEI 10.520 / 2002", description="Referência ao ato normativo do amparo"),
     *             @OA\Property(property="artigo", type="string", example="1", description="Número do artigo do amparo"),
     *             @OA\Property(property="modalidade", type="string", example="Pregão", description="Tipo de modalidade do amparo"),
     *             @OA\Property(property="paragrafo", type="string", example="1", description="Número do parágrafo do amparo"),
     *             @OA\Property(property="inciso", type="string", example="III", description="Número do inciso do amparo"),
     *             @OA\Property(property="alinea", type="string", example="A", description="Letra da alínea do amparo"),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Consulta de amparo legal realizada com sucesso",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="string"),
     *             @OA\Property(property="message", type="array", @OA\Items(
     *                 @OA\Property(property="id_amparo", type="integer"),
     *                 @OA\Property(property="descricao_amparo", type="string"),
     *             )),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Token expirado",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="string"),
     *             @OA\Property(property="message", type="string"),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Usuário sem permissão no sistema.",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="string"),
     *             @OA\Property(property="message", type="string"),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Amparo legal não encontrado",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="string"),
     *             @OA\Property(property="message", type="string"),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Erro interno do servidor",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="string"),
     *             @OA\Property(property="message", type="string"),
     *         ),
     *     ),
     * )
     */

    public function consultar(AmparoLegalConsultaRequest $request)
    {
        try {
            $permissao = ErrorMessageAmparoLegal::verificaPermissaoSistema();
            if ($permissao instanceof \Illuminate\Http\JsonResponse) {
                return $permissao;
            }

            $modalidade = request()->input('modalidade');
            $ato_normativo = request()->input('ato_normativo');
            $artigo = request()->input('artigo');
            $paragrafo = request()->input('paragrafo');
            $inciso = request()->input('inciso');
            $alinea = request()->input('alinea');

            $result = $this->amparoLegalService->consultarAmparoLegal(
                $modalidade,
                $ato_normativo,
                $artigo,
                $paragrafo,
                $inciso,
                $alinea
            );

            $validarResultado = ErrorMessageAmparoLegal::validarResultado($result);
            if ($validarResultado instanceof \Illuminate\Http\JsonResponse) {
                return $validarResultado;
            }

            $amparos_legais = [];
            foreach ($result as $item) {
                $descricao = 'Ato Normativo: ' . $item->ato_normativo .
                    ' - Artigo: ' . $item->artigo;

                if (!is_null($item->paragrafo)) {
                    $descricao .= ' - Parágrafo: ' . $item->paragrafo;
                }

                if (!is_null($item->inciso)) {
                    $descricao .= ' - Inciso: ' . $item->inciso;
                }

                if (!is_null($item->alinea)) {
                    $descricao .= ' - Alínea: ' . $item->alinea;
                }

                $amparos_legais[] = [
                    'id_amparo' => $item->id,
                    'descricao_amparo' => $descricao
                ];
            }

            $data = [
                'Amparo Legal' => $amparos_legais,
            ];

            return response()->json([
                'status' => 'success',
                'message' => $data,
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => ErrorMessageAmparoLegal::getErrorMessage('erro_desconhecido')
            ], 500);
        }
    }
}
