<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\APIController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\Formatador;
use App\Models\BackpackUser;
use App\Models\Role;
use App\Models\Unidade;
use App\User;
use Carbon\Carbon;
use PHPUnit\Util\Json;
use Illuminate\Support\Collection;
use Throwable;

class UsuarioAntecipaGovController extends APIController
{   
    use Formatador;
    
    //perfis={"perfis":["Administrador", "Acesso API"]}

    /**
     * @OA\Get(
     *     tags={"Usuários"},
     *     summary="Retorna todos os usuários de uma determinada UG (Unidade Gestora)",
     *     description="",
     *     path="/api/v1/usuario/ug/{unidade_codigo}",
     *     security={ {"bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="unidade_codigo",
     *         in="path",
     *         description="codigo da unidade",
     *         required=true,
     *         @OA\Schema(
     *              type="integer"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Lista de usuários da UG retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/Usuarios")
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/FalhaAutenticação")
     *     ),
     *  
     * )
     */
    public function usuariosPorUG(String $ug, Request $request)
    {   
        $usuarios_array = [];
        $usuarios = $this->buscaUsuariosPorUG($this->verificaUG($ug), $this->range($request->dt_alteracao_min, $request->dt_alteracao_max), $this->verificaPerfis($request->perfis));
     
        foreach ($usuarios as $usuario) {
            $usuarios_array[] = $this->formataUsuarioAPI($usuario);
        }

        return json_encode($usuarios_array);
    }

    /**
     * @OA\Get(
     *     tags={"Usuários"},
     *     summary="Retorna todos os usuários de uma determinada UG (Unidade Gestora) por Perfil (Grupo)",
     *     description="",
     *     path="/api/v1/usuario/ug/{unidade_codigo}/perfil/{grupo_id}",
     *     security={ {"bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="unidade_codigo",
     *         in="path",
     *         description="codigo da unidade",
     *         example="201053",
     *         required=true,
     *         @OA\Schema(
     *              type="integer"
     *         )
     *     ),
     * 
     *      @OA\Parameter(
     *         name="grupo_id",
     *         in="path",
     *         description="id do grupo",
     *         example="11",
     *         required=true,
     *         @OA\Schema(
     *              type="integer"
     *         )
     *     ),
     * 
     *     @OA\Response(
     *         response=200,
     *         description="Lista de usuários da UG por Perfil retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/UsuariosUgPorPerfil")
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/FalhaAutenticação")
     *     ),
     *  
     * )
     */
    public function usuariosPorUGPerfil(String $ug, int $grupo_id)
    {   
        $usuarios_array = [];

        $nome_perfil = Role::find($grupo_id);

        $usuarios = $this->buscaUsuariosPorUG($this->verificaUG($ug), null, $nome_perfil);
     
        if ($usuarios->count() > 0) {
            $usuarios_array['unidade_id'] = $usuarios[0]->ugprimaria;
            $usuarios_array['perfil'] = $nome_perfil->name;
        }

        foreach ($usuarios as $usuario) {
            $usuarios_array['usuarios'][] = [
                                              'nome' => $usuario->name,
                                              'email' => $usuario->email,                                              
                                              'situacao' => $usuario->situacao == true ? 'Ativo' : 'Inativo',
                                              'updated_at' => $usuario->updated_at->format('Y-m-d H:i:s')
            ];
                        
        }

        return response()->json($usuarios_array);
    }

    /**
     * @OA\Get(
     *     tags={"Usuários"},
     *     summary="Retorna um determinado usuário pelo CPF informado.",
     *     description="",
     *     path="/api/v1/usuario/cpf/{cpf}",
     *     security={ {"bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="cpf",
     *         in="path",
     *         description="CPF do usuário (somente números)",
     *         required=true,
     *         @OA\Schema(
     *              type="integer"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Usuário retornado com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/Usuarios")
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/FalhaAutenticação")
     *     ),    
     * 
     * )
     */
    public function usuarioPorCPF(String $cpf, Request $request)
    {   
        $usuarios_array = [];
        $usuarios = $this->buscaUsuarioPorCPF($cpf, $this->range($request->dt_alteracao_min, $request->dt_alteracao_max));
        
        foreach ($usuarios as $usuario) {
            $usuarios_array[] = $this->formataUsuarioAPI($usuario);
        }

        return json_encode($usuarios_array);

    }

    private function buscaUsuariosPorUG(String $ug, $range, $perfis)
    {   
        
        $usuarios = BackpackUser::whereHas('unidade', function ($x) use($ug) {
            $x->where('codigo',$ug);
        })
        ->when($range != null, function ($d) use ($range) {
            $d->whereBetween('users.updated_at', [$range[0], $range[1]]);
        })
            ->when($perfis!=null, function ($f) use ($perfis){
                $f->whereHas('roles', function($q) use ($perfis){
                    $q->whereIn('name', $perfis);
                });
            })
            ->get();
        return $usuarios;
    }

    private function buscaUsuarioPorCPF(String $cpf, $range)
    {   
        $usuario = BackpackUser::where('cpf', $this->formataCPF($cpf))
        ->when($range != null, function ($d) use ($range) {
            $d->whereBetween('users.updated_at', [$range[0], $range[1]]);
        })
            ->get();

        return $usuario;
    }

    //Deve ser refatorada para a Classe de usuários
    private function formataUsuarioAPI(BackpackUser $usuario){
        
        return [
            'cpf' => $usuario->cpf,
            'nome' => $usuario->name,
            'email' => $usuario->email,
            'ugprimaria' => $usuario->unidadeprimaria($usuario->ugprimaria)->codigo,
            'ugssecundarias' => $usuario->unidadesSecundarias(),
            'perfis' => array_column($usuario->roles->toArray(), 'name'),
            'situacao' => $usuario->situacao == true ? 'Ativo' : 'Inativo'
        ];   
    }

    //Avaliar Refatoração, (verificaListaParametro, ja criado na APIController)
    private function verificaPerfis($perfis)
    {
        if ($perfis != null) {
            try {
                //Formato lista sistema: ["Administrador", "Acesso API"]
                //$perfis = json_decode($perfis);
                //Formato lista simples: Administrador,Acesso API
                $perfis = explode(",", $perfis);
            } catch (Throwable $e) {
                abort(response()->json($e->getMessage()));
                return $e;
            }
            
            if(!is_array($perfis)){
                abort(response()->json(['errors' => "Array (perfis) em formato invalido",], 422));
            }

            if (empty($perfis)) {
                return null;
            }
            
            $errados = array_diff($perfis, Role::get()->pluck('name')->toArray());

            if (!empty($errados)) {
                abort(response()->json(['errors' => "Perfis inválidos: [". implode(",",$errados) ."];",], 422));
                return;
            }

            return $perfis;
        } else {
            return null;
        }
    }

}
