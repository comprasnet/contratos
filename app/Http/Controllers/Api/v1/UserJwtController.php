<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use OpenApi\Annotations\OpenApi;
use OpenApi\Annotations as OA;

class UserJwtController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */

    /**
     * @OA\Post(
     *     tags={"Usuários"},
     *     summary="Autenticação",
     *     description="",
     *     path="/api/v1/auth/login",
     *     @OA\RequestBody(
     *         required=true,
     *     @OA\JsonContent(
     *         required={"cpf","password"},
     *     @OA\Property(property="cpf", type="string", example="111.111.111-11"),
     *     @OA\Property(property="password", type="string", example="senha" ),
     *     ),
     *    ), 
     *    @OA\Response(
     *        response=200,
     *        description="OK", 
     *        @OA\JsonContent(ref="#/components/schemas/TokenBearer")
     *    ),
     *    @OA\Response(
     *        response=401,
     *        description="Error",
     *    ),
     * )
     */
    public function login()
    {
        $credentials = request(['cpf', 'password']);

        if (! $token = auth('api')->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth('api')->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */

    /**
     * @OA\Post(
     *     tags={"Usuários"},
     *     summary="Logout",
     *     description="Invalida o token JWT do usuário, efetivamente desconectando-o.",
     *     path="/api/v1/auth/logout",
     *     security={ {"bearerAuth": {} }},
     * 
     *    @OA\Response(
     *        response=200,
     *        description="Successfully logged out",
     *        @OA\JsonContent(@OA\Property(property="message", type="string", example="Successfully logged out"))
     *    ),
     * 
     *    @OA\Response(
     *        response=401,
     *        description="Error",
     *    ),
     * )
     */     
    public function logout()
    {
        auth('api')->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth('api')->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60
        ]);
    }
}
