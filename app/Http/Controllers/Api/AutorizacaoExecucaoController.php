<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\AutorizacaoExecucaoRepository;
use App\Http\Resources\AutorizacaoExecucaoResource;
use App\Exceptions\EmptyCollectionException;

class AutorizacaoExecucaoController extends Controller
{
    
    protected $autorizacaoExecucaoRepository;

    public function __construct(AutorizacaoExecucaoRepository $autorizacaoExecucaoRepository)
    {
        $this->autorizacaoExecucaoRepository = $autorizacaoExecucaoRepository;
    }

    /**
     * @OA\Get(
     *     tags={"Fiscalização"},
     *     summary="Retorna uma lista de ordens de serviço ou fornecimento associados a um contrato.",
     *     description="",
     *     path="/api/v1/execucoes/contratos/{contrato_id}",
     *     security={ {"bearerAuth": {} }},
     *
     *     @OA\Parameter(
     *         name="contrato_id",
     *         in="path",
     *         description="id do contrato",
     *         required=true,
     *         @OA\Schema(
     *              type="integer"
     *        )
     *     ),
     * 
     *     @OA\Parameter(
     *         name="autorizacao_execucao_id",
     *         in="query",
     *         description="id da ordem de serviço/fornecimento",
     *         required=false,
     *         @OA\Schema(
     *              type="integer"
     *        )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK.",
     *         @OA\JsonContent(ref="#/components/schemas/OrdensServicoFornecimento")
     *     ),
     * 
     *     @OA\Response(
     *         response=401,
     *         description="Error: Unauthorized.",
     *         @OA\JsonContent(ref="#/components/schemas/FalhaAutenticação")
     *     ),
     * 
     *     @OA\Response(
     *         response=404,
     *         description="Error: Not Found",
     *         @OA\JsonContent(@OA\Property(property="error",type="string",example="Nenhum registro localizado."))
     *     ),
     * )
    */	    
    public function consultarOrdemServicoFornecimento($contrato_id, Request $request) {

        try {
            $autorizacao_execucao_id = $request->input('autorizacao_execucao_id'); //Parâmetro opcional na requisição

            $collect_autorizacao_execucao = $this->autorizacaoExecucaoRepository
                                                ->getListaOrdemServicoFornecimentoPorContrato($contrato_id, $autorizacao_execucao_id);

            if (empty($collect_autorizacao_execucao->count())) throw new EmptyCollectionException("Nenhum registro localizado.");
    
            return AutorizacaoExecucaoResource::collection($collect_autorizacao_execucao);
        } catch (EmptyCollectionException $e) {
            return response()->json(['error' => $e->getMessage()], 404);
        } catch (\Throwable $th) {
            return response()->json(['error' => $th->getMessage()], $th->getCode());
        }
    }

    /*
    |--------------------------------------------------------------------------
    | SWAGGER ANNOTATIONS
    |--------------------------------------------------------------------------
    */

    /**
     * @OA\Schema(
     *     schema="OrdensServicoFornecimento",
     *     type="object",
     *     @OA\Property(property="id",type="integer",example="89"),
     *     @OA\Property(property="contrato", type="object",
     *         @OA\Property(property="contrato_id", type="integer", example=236198),
     *         @OA\Property(property="numero_contrato", type="string", example="00036/2023")
     *     ),
     *     @OA\Property(property="fornecedor", type="object",
     *         @OA\Property(property="id", type="integer", example=699847),
     *         @OA\Property(property="tipo", type="string", example="JURIDICA"),
     *         @OA\Property(property="cnpj_cpf_idgener", type="string", example="19.168.683/0001-19"),
     *         @OA\Property(property="nome", type="string", example="SERGIO ALVES DA SILVA")
     *     ),
     *     @OA\Property(property="numero_processo_sei", type="string", example="0015607-29.2023.6.17.8000"),
     *     @OA\Property(property="tipo", type="string", example="Ordem de Fornecimento"),
     *     @OA\Property(property="numero", type="string", example="00004/2024"),
     *     @OA\Property(property="data_assinatura",type="string", format="date", example="2024-05-14"),
     *     @OA\Property(property="vigencia_inicio", type="string", format="date", example="2024-05-14"),
     *     @OA\Property(property="vigencia_fim", type="string", format="date", example="2024-05-20"),
     *     @OA\Property(property="numero_ordem_sistema_origem", type="string", example="2559068 e 2560727"),
     *     @OA\Property(property="unidades_requisitantes", type="array",
     *         @OA\Items(type="object",
     *                   @OA\Property(property="id", type="integer", example=554),
     *                   @OA\Property(property="codigo", type="string", example="070010"),
     *                   @OA\Property(property="nome", type="string", example="TRE/PE")
     *                  )
     *     ),
     *     @OA\Property(property="empenhos", type="array",
     *         @OA\Items(type="object",
     *                   @OA\Property(property="id", type="integer", example=11548382),
     *                   @OA\Property(property="numero", type="string", example="2024NE000074"),
     *                   @OA\Property(property="unidade_id", type="integer", example=554),
     *                   @OA\Property(property="codigo_unidade", type="string", example="070010"),
     *                   @OA\Property(property="empenhado", type="string", example="3.459,20"),
     *                   @OA\Property(property="aliquidar", type="string", example="1.399,30"),
     *                   @OA\Property(property="liquidado", type="string", example="0,00"),
     *                   @OA\Property(property="pago", type="string", example="2.059,90")
     *                  )
     *     ),
     *     @OA\Property(property="itens", type="array",
     *         @OA\Items(type="object",
     *                   @OA\Property(property="id", type="integer", example=507),
     *                   @OA\Property(property="periodo_vigencia_inicio", type="string", format="date", example="2024-01-02"),
     *                   @OA\Property(property="periodo_vigencia_fim", type="string", format="date", example="2024-12-31"),
     *                   @OA\Property(property="tipo_item", type="string", example="MATERIAL"),
     *                   @OA\Property(property="numero_item_compra", type="string", example="00001"),
     *                   @OA\Property(property="item", type="string", example="CARIMBO"),
     *                   @OA\Property(property="quantidade", type="string", example="6,000000000000"),
     *                   @OA\Property(property="parcela", type="integer", example=1),
     *                   @OA\Property(property="valor_unitario", type="string", example="13,00"),
     *                   @OA\Property(property="subcontratacao", type="string", example="Não")
     *                  )
     *     ),
     *     @OA\Property(property="valor_total", type="string", example="78,00"),
     *     @OA\Property(property="informacoes_complementares", type="string", example="Referente Remessas 6 e 7 , pedidos 25 a 27"),
     *     @OA\Property(property="situacao", type="string", example="Em execução")
     * )
     */

}