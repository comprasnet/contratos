<?php

namespace App\Http\Controllers\Api\ChaveNfe;

use App\Http\Controllers\Controller;

class ChaveNfeController extends Controller
{

    /**
     *
     * Retona se a chave NFe é válida
     * @param $chave chave NFe
     * @return \Illuminate\Http\JsonResponse
     */
    public function validarChaveNfe($chave)
    {
        $chaveDesmembrada = Keys::decompile($chave);

        $buildChave = Keys::build(
            $chaveDesmembrada['cuf'],
            $chaveDesmembrada['ano'],
            $chaveDesmembrada['mes'],
            $chaveDesmembrada['cnpj'],
            $chaveDesmembrada['modelo'],
            $chaveDesmembrada['serie'],
            $chaveDesmembrada['nnf'],
            $chaveDesmembrada['tpemis'],
            $chaveDesmembrada['cnf']
        );

        $booKeyIsValid =  Keys::isValid($chave);

        return response()->json($booKeyIsValid);
    }
}
