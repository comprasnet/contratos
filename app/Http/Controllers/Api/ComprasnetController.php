<?php

namespace App\Http\Controllers\Api;

use App\Http\Traits\Formatador;
use Illuminate\Support\Facades\DB;
use App\Models\Codigoitem;
use App\Models\Compra;
use App\Models\Contrato;
use App\Models\Contratoitem;
use App\Models\ContratoPublicacoes;
use App\Models\Empenho;
use App\Models\Unidade;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\ConsultaAtualizaSaldoSiafi;
use App\Models\Arp;
use App\Models\MinutaEmpenho;

class ComprasnetController extends Controller
{
    use Formatador;
    use ConsultaAtualizaSaldoSiafi;

    public function getContratosEmpenhosPorItens(Request $request)
    {
        $retorno = [];

        if (empty($request->uasg) or empty($request->modalidade) or empty($request->numero) or empty($request->ano) or empty($request->itens)) {
            return $retorno;
        }

        $dados['uasg'] = @str_pad($request->uasg, 6, "0", STR_PAD_LEFT);
        $dados['modalidade'] = @str_pad($request->modalidade, 2, "0", STR_PAD_LEFT);
        $dados['numeroAno'] = @str_pad($request->numero, 5, "0", STR_PAD_LEFT) . '/' . @$request->ano;
        $dados['itens'] = $this->trataItens($request->itens);

        $unidade = $this->buscaUnidadePorCodigo($dados['uasg']);
        $modalidade = $this->buscaModalidadePorCodigo($dados['modalidade']);

        if (isset($unidade->id) and isset($modalidade->id)) {

            $retorno['itens'] = [];

            foreach ($dados['itens'] as $item) {
                $num_item = str_pad($item, 5, "0", STR_PAD_LEFT);

                $contratos = $this->buscaContratosItemUnidadeCompra($num_item, $modalidade->id, $unidade->id, $dados['numeroAno']);

                $array_contratos = [];
                foreach ($contratos as $contrato) {
                    $unidadeorigem = $contrato->unidadeorigem->codigo;
                    $unidadeatual = $contrato->unidade->codigo;
                    $unidadesubrrogacao = ($unidadeorigem == $unidadeatual) ? '000000' : $unidadeatual;
                    $tipo = $contrato->tipo->descres;
                    $numero_contrato = str_replace('/', '', $contrato->numero);

                    $array_contratos[] .= $unidadeorigem . $tipo . $numero_contrato . $unidadesubrrogacao;
                }

                //busca empenhos pela tb empenhos verificar coluna RP
//                $array_empenhos = $this->buscarEmpenhos($num_item, $modalidade->id, $unidade->id, $dados['numeroAno']);
                //busca empenhos pela tb compras corrigir query
                $array_empenhos = $this->buscarEmpenhos2($num_item, $modalidade->id, $unidade->id, $dados['numeroAno']);
//                $arrEmpenhosMerge = array_merge($arrEmpenhos1, $arrEmpenhos2);
//                $arrEmpenhosUnique = array_unique($arrEmpenhosMerge, SORT_REGULAR);
//                $array_empenhos = [];
//                foreach ($arrEmpenhosUnique as $empenhos){
//                    $array_empenhos[] = $empenhos['idempenho'];
//                }

                $a_empenhos = [];
                if(is_array($array_empenhos)){
                    foreach ($array_empenhos as $a_empenho){
                        $a_empenhos[] = $a_empenho['idempenho'];
                    }
                }

                $retorno['itens'][] = [
                    'nroItem' => $num_item,
                    'contratosAtivos' => $array_contratos,
                    'empenhos' => $a_empenhos
                ];
            }

//            $retorno = [
//                'itens' => [
//                    [
//                        'nroItem' => '0001',
//                        'contratosAtivos' => [
//                            '11016150000012019000000',
//                            '11062150000022019000000',
//                        ],
//                        'empenhos' => [
//                            '110161000012020NE000001',
//                            '110621000012020NE000001',
//                        ],
//                    ],
//                    [
//                        'nroItem' => '0002',
//                        'contratosAtivos' => [
//
//                        ],
//                        'empenhos' => [
//
//                        ],
//                    ],
//                ]
//            ];
        }
        return $retorno;
    }

    private function buscarEmpenhos(string $nuItem, int $modalidade, int $unidade, string $numeroAnoCompra){
        return Empenho::select(
            DB::raw('u.codigo || u.gestao || empenhos.numero AS idempenho')
        )
            ->join('contratoempenhos', 'empenhos.id', '=', 'contratoempenhos.empenho_id')
            ->join('contratos', 'contratos.id', '=', 'contratoempenhos.contrato_id')
            ->join('contratoitens', 'contratos.id', '=', 'contratoitens.contrato_id')
            ->join('unidades AS u', 'u.id', '=', 'empenhos.unidade_id')
            ->where('modalidade_id', $modalidade)
            ->where('unidadecompra_id', $unidade)
            ->where('licitacao_numero', $numeroAnoCompra)
            ->where('contratoitens.numero_item_compra', $nuItem)
            ->where('contratoitens.quantidade', '>', 0)
            ->where('empenhos.aliquidar', '>', 0)
            ->where('empenhos.numero', 'LIKE', date('Y') ."NE%")
            ->distinct()
            ->get()
            ->toArray();
    }

    private function buscarEmpenhos2(string $nuItem, int $modalidade, int $unidade, string $numeroAnoCompra){
        $situacao = Codigoitem::whereHas('codigo', function ($query) {
            $query->where('descricao', 'Situações Minuta Empenho');
        })
            ->where('descricao', 'EMPENHO EMITIDO')
            ->select('codigoitens.id')->first();

        return Compra::select(
            DB::raw('u.codigo || u.gestao || m.mensagem_siafi AS idempenho')
        )
            ->join('compra_items AS ci', 'ci.compra_id', '=', 'compras.id')
            ->join('compra_item_minuta_empenho AS cime', 'cime.compra_item_id', '=', 'ci.id')
            ->join('minutaempenhos AS m', 'cime.minutaempenho_id', '=', 'm.id')
            ->join('unidades AS u', 'u.id', '=', 'm.unidade_id')
            ->join('empenhos AS e', 'm.mensagem_siafi', '=', 'e.numero')
            ->where('compras.modalidade_id', $modalidade)
            ->where('compras.numero_ano', $numeroAnoCompra)
            ->where('m.unidade_id', $unidade)
            ->where('ci.numero', $nuItem)
            ->where('m.situacao_id', $situacao->id)
            ->whereRaw('left (m.mensagem_siafi,	4) = date_part(\'year\', current_date)::text')
            ->where(function ($q) {
                $q->orWhere('e.aliquidar', '>', 0)
                ->orWhere('e.rpaliquidar', '>', 0);
            })
            ->distinct()
            ->get()
            ->toArray();
    }

    private function buscaContratosItemUnidadeCompra(string $item, int $modalidade, int $unidade, string $numeroAnoCompra)
    {
        $contratos = Contrato::whereHas('itens', function ($i) use ($item) {
            $i->where('numero_item_compra', $item)
                ->where('valortotal', '>', 0);
        })
            ->where('modalidade_id', $modalidade)
            ->where('unidadecompra_id', $unidade)
            ->where('licitacao_numero', $numeroAnoCompra);

        return $contratos->get();

    }

    private function trataItens(string $itens)
    {
        $array = [];

        if ($itens) {
            $itens = str_replace(']', '', str_replace('[', '', $itens));

            if (strpos($itens, ',') !== false) {
                $array = explode(',', $itens);
            } else {
                $array[] = $itens;
            }
        }

        return $array;
    }

    public function getDadosContratosPorItem(Request $request)
    {

        $retorno = [];

        if (empty($request->uasgCompra) or empty($request->modalidade) or empty($request->numeroCompra) or empty($request->anoCompra)) {
            return $retorno;
        }

        //obrigatorios
        $dados['uasgCompra'] = str_pad($request->uasgCompra, 6, "0", STR_PAD_LEFT);
        $dados['modalidade'] = str_pad($request->modalidade, 2, "0", STR_PAD_LEFT);
        $dados['numeroAnoCompra'] = str_pad($request->numeroCompra, 5, "0", STR_PAD_LEFT) . '/' . $request->anoCompra;

        //opcionais
        $dados['uasg_contrato'] = @str_pad($request->uasgContrato, 6, "0", STR_PAD_LEFT);
        $dados['fornecedor'] = @$request->fornecedor;

        $unidade_compra = ($dados['uasgCompra']) ? $this->buscaUnidadePorCodigo($dados['uasgCompra']) : null;
        $modalidade = ($dados['modalidade']) ? $this->buscaModalidadePorCodigo($dados['modalidade']) : null;
        $unidade_contrato = ($dados['uasg_contrato']) ? $this->buscaUnidadePorCodigo($dados['uasg_contrato']) : null;


        if (isset($unidade_compra->id) and isset($modalidade->id)) {
            $dados = Contratoitem::select(['contrato_id', 'valortotal', 'valorunitario', 'quantidade'])
                ->whereHas('contrato', function ($q) use ($dados, $unidade_compra, $modalidade, $unidade_contrato) {
                    $q->where('unidadecompra_id', $unidade_compra->id)
                        ->where('modalidade_id', $modalidade->id)
                        ->where('licitacao_numero', $dados['numeroAnoCompra'])
                        ->select('id');
                    if (isset($unidade_contrato->id)) {
                        $q->where('unidade_id', $unidade_contrato->id);
                    }
                    if ($dados['fornecedor']) {
                        $q->whereHas('fornecedor', function ($f) use ($dados) {
                            $f->where('cpf_cnpj_idgener', $this->formataCnpjCpf($dados['fornecedor']));
                        });
                    }
                });

            if (!empty($request->numeroItem)) {
                $dados->where('numero_item_compra', str_pad($request->numeroItem, 5, "0", STR_PAD_LEFT));
            };

            foreach ($dados->get() as $dado) {
                $instrumento_inicial = $dado->contrato->historico()->whereHas('tipo', function ($t) {
                    $t->where('descricao', '<>', 'Termo Aditivo')
                        ->where('descricao', '<>', 'Termo de Apostilamento')
                        ->where('descricao', '<>', 'Termo de Rescisão');
                })->first();

                $contrato_id = $dado->contrato->id;

                $publicacao = ContratoPublicacoes::whereHas('contratohistorico', function ($q) use ($contrato_id){
                    $q->where('contrato_id',$contrato_id);
                })
                    ->latest()
                    ->first();

                if (isset($publicacao)) {
                    if ($publicacao->contratohistorico->tipo->descricao == 'Termo de Rescisão') {
                        $situacao_publicacao = @$publicacao->StatusPublicacaoDescress;
                        if ($publicacao->StatusPublicacaoDescres == '02') {
                            $situacao_publicacao = '08';
                        }
                        if ($publicacao->StatusPublicacaoDescres == '05') {
                            $situacao_publicacao = '09';
                        }
                    } else {
                        $situacao_publicacao = @$publicacao->StatusPublicacaoDescres;
                    }
                }

                $unidade_atual = ($dado->contrato->unidade->codigo == $dado->contrato->unidadeorigem->codigo) ? null : $dado->contrato->unidade->codigo;

                $retorno[] = [
                    'unidade_origem' => @$dado->contrato->unidadeorigem->codigo,
                    'unidade_atual' => @$unidade_atual,
                    'numero_contrato' => @$dado->contrato->numero,
                    'tipo' => @$dado->contrato->tipo->descres,
                    'fornecedor' => @$dado->contrato->fornecedor->cpf_cnpj_idgener,
                    'vigencia_fim_inicial' => @$instrumento_inicial->vigencia_fim,
                    'vigencia_fim' => @$dado->contrato->vigencia_fim,
                    'quantidade_item' => @number_format($dado->quantidade, 0, '', ''),
                    'valor_unitario_item' => @$dado->valorunitario,
                    'valor_total_item' => @$dado->valortotal,
                    'situacao_publicacao' => @$situacao_publicacao,
                    /*
                     *  todo implementar esse retorno.
                     *  01 - TRANSFERIDO PARA IMPRENSA
                     *  02 - PUBLICADO
                     *  03 - INFORMADO
                     *  05 - A PUBLICAR
                     *  07 - DEVOLVIDO PELA IMPRENSA
                     *  08 - EVENTO DE RESCISÃO PUBLICADO
                     *  09 - EVENTO DE RESCISÃO A PUBLICAR
                     */
                ];
            }
        }

        return $retorno;
    }


    /**
     * @OA\Post(
     *     tags={"Compras"},
     *     summary="Retorna se itens de uma compra estão vinculados a Atas, Contratos e/ou Empenhos",
     *     description="",
     *     path="/api/v1/comprasnet/compras/impedimentos",
     *     security={ {"bearerAuth": {} }},
     *
     *     @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"numeroUasg", "modalidade", "numero", "ano", "numerosItens"},
     *              @OA\Property(property="numeroUasg", type="string", example="200999"),
     *              @OA\Property(property="modalidade", type="string", example="05"),
     *              @OA\Property(property="numero", type="string", example="01301"),
     *              @OA\Property(property="ano", type="string", example="2023"),
     *              @OA\Property(property="numerosItens", type="array", example={"00001","00002","00003","00004","00005"}, @OA\Items(type="string")),
     *          ),
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *         @OA\JsonContent(ref="#/components/schemas/ItensCompra")
     *     ),
     *
     *     @OA\Response(
     *         response=401,
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/FalhaAutenticação")
     *     ),
     *
     * )
     */
    public function getImpedimentosPorItem(Request $request) {

        //Parâmetros da consulta
        if ( empty($data_request['numeroUasg'] = $request->numeroUasg) or
             empty($data_request['modalidade'] = $request->modalidade) or
             empty($data_request['numero'] = $request->numero) or
             empty($data_request['ano'] = $request->ano) or
             empty($data_request['numerosItens'] = $request->numerosItens) ) {
                return [];
             }

        /*
        Pesquisa os itens na ordem abaixo e de forma excludente, ou seja, uma vez localizado o item em uma determinada consulta, o mesmo é excluído nas consultas consecutivas.
            1) Atas
            2) Contratos
            3) Empenhos        
        */

        //Seta array principal
        $itens_retorno['itens'] = [];

        //Predefine os items sem impedimentos.
        for($i = 0; $i < count($data_request['numerosItens']); $i++) {

            $item['numeroItem'] = $data_request['numerosItens'][$i];
            $item['possuiImpeditivo'] = false;
            $item['textoImpeditivo'] = "";
            $item['url'] = "";

            $itens_retorno['itens'][$i] = $item;
        }

        //Nas consultas seguintes, considera somente os itens que realmente fazem parte da compra informada.
        $search_itens = Compra::select('ci.numero')
                        ->join('compra_items AS ci', 'ci.compra_id', '=', 'compras.id')
                        ->join('unidades AS u', 'u.id', '=', 'compras.unidade_origem_id')
                        ->join('codigoitens AS coi', 'coi.id', '=', 'compras.modalidade_id')
                        ->where('u.codigo', $data_request['numeroUasg'])
                        ->where('coi.descres', $data_request['modalidade'])
                        ->where('compras.numero_ano', $data_request['numero'] .'/'. $data_request['ano'])
                        ->whereIn('ci.numero', $data_request['numerosItens'])
                        ->distinct()->pluck('ci.numero')->toArray();


        /*Consulta se há itens vinculados a Atas de Registro de Preço (Arp).
          Regras de impedimento:
             a) Atas em situação 'Ativa'
             b) Atas que não possuam itens cancelados.
        */
        if (count($search_itens) > 0) {
            $itens_arp = Arp::select(
                            'compra_items.numero',
                            'compras.id as compra_id',
                            'compra_items.id as compra_item_id'
                        )
                        ->join('arp_item', 'arp.id', '=', 'arp_item.arp_id')
                        ->join('compra_item_fornecedor', 'arp_item.compra_item_fornecedor_id', '=', 'compra_item_fornecedor.id')
                        ->join('compra_items', 'compra_item_fornecedor.compra_item_id', '=', 'compra_items.id')
                        ->join('compras', 'arp.compra_id', '=', 'compras.id')
                        ->join('unidades', 'compras.unidade_origem_id', '=', 'unidades.id')
                        ->join('codigoitens', 'compras.modalidade_id', '=', 'codigoitens.id')
                        ->where('unidades.codigo', $data_request['numeroUasg'])
                        ->where('codigoitens.descres', $data_request['modalidade'])
                        ->where('compras.numero_ano', $data_request['numero'] .'/'. $data_request['ano'])
                        ->whereIn('compra_items.numero', $search_itens)
                        ->where('arp.rascunho', false)
                        ->where('compra_item_fornecedor.situacao', true)
                        ->whereNull('arp_item.deleted_at')
                        ->whereNotExists(function ($query) {
                            $query->select(DB::raw(1))
                                ->from('arp_item_historico as aih')
                                ->join('arp_item as ai2', 'aih.arp_item_id', '=', 'ai2.id')
                                ->join('arp as a2', 'ai2.arp_id', '=', 'a2.id')
                                ->whereColumn('a2.id', 'arp.id')
                                ->whereColumn('aih.arp_item_id', 'arp_item.id')
                                ->where('aih.item_cancelado', true);
                        })
                        ->distinct()->get()->toArray();

            //Redefinindo os itens localizados em Atas.
            $this->redefineItensComImpedimentos($data_request, $itens_arp, $itens_retorno, $search_itens, 'Ata de Registro de Preços');
        }

        /*Consulta se há itens vinculados a Contratos.
          Regras de impedimento:
             a) Contratos do tipo Empenho que não possuam Termos de Encerramento.
             b) Contratos (não do tipo Empenho) que não possuam Termos de Rescisão.
             c) Os Contratos não podem estar em modo rascunho.
        */

        if (count($search_itens) > 0) {
            $itens_contratos = Contrato::select(
                                'contratoitens.numero_item_compra as numero',
                                'compras.id as compra_id',
                                'compra_items.id as compra_item_id'
                                )
                                ->join('contratoitens', 'contratos.id', '=', 'contratoitens.contrato_id')
                                ->join('unidades', 'contratos.unidadecompra_id', '=', 'unidades.id')
                                ->join('codigoitens', 'contratos.modalidade_id', '=', 'codigoitens.id')
                                ->leftJoin('codigoitens as ci', 'contratos.justificativa_contrato_inativo_id', '=', 'ci.id')
                                ->join('codigoitens as ci2', 'contratos.tipo_id', '=', 'ci2.id')
                                ->join('compras', function ($join) {
                                    $join->on('contratos.unidadecompra_id', '=', 'compras.unidade_origem_id')
                                            ->on('contratos.modalidade_id', '=', 'compras.modalidade_id')
                                            ->on('contratos.licitacao_numero', '=', 'compras.numero_ano');
                                })
                                ->join('compra_items', function ($join) {
                                    $join->on('compras.id', '=', 'compra_items.compra_id')
                                        ->on('contratoitens.catmatseritem_id', '=', 'compra_items.catmatseritem_id')
                                        ->on('contratoitens.numero_item_compra', '=', 'compra_items.numero');
                                })
                                ->where('unidades.codigo', $data_request['numeroUasg'])
                                ->where('codigoitens.descres', $data_request['modalidade'])
                                ->where('contratos.licitacao_numero', 'LIKE', $data_request['numero'] .'/'. $data_request['ano'])
                                ->whereIn('contratoitens.numero_item_compra', $search_itens)
                                ->whereNull('contratos.deleted_at')
                                ->whereNull('contratoitens.deleted_at')
                                ->where('contratos.elaboracao', false)
                                ->where(function ($query) {
                                    $query->where(function ($query) {
                                        $query->where('ci2.descricao', '=', 'Empenho')
                                            ->whereNull('ci.descricao');
                                    })
                                    ->orWhere(function ($query) {
                                        $query->where('ci2.descricao', '!=', 'Empenho')
                                            ->whereNotExists(function ($query) {
                                                $query->select(DB::raw(1))
                                                        ->from('contratohistorico as ch')
                                                        ->join('codigoitens as ci', 'ci.id', '=', 'ch.tipo_id')
                                                        ->join('codigos as co', 'co.id', '=', 'ci.codigo_id')
                                                        ->whereNull('ch.deleted_at')
                                                        ->whereColumn('ch.contrato_id', 'contratos.id')
                                                        ->where('ci.descricao', 'Termo de Rescisão')
                                                        ->where('co.descricao', 'Tipo de Contrato');
                                            });
                                    });
                                })
                                ->distinct()->get()->toArray();

            //Redefinindo os itens localizados em Contratos.
            $this->redefineItensComImpedimentos($data_request, $itens_contratos, $itens_retorno, $search_itens, 'Contrato ou Empenho');
        }

        /*Neste ponto do fluxo, verifica se:
             a) Há itens vinculados a Contratos rescindidos e,
             b) Há itens vinculados a Contratos (do tipo Empenho) encerrados.
          Em qualquer das condições acima, tais itens devem ser ignorados na próxima verificação de Minutas. (#issues 1125, 1361)
        */
        if (count($search_itens) > 0) {
            $this->redefineItensDeContratosRescindidosOuEncerrados($data_request, $search_itens);
        }

        /*Neste ponto do fluxo, os eventuais itens remanescentes não estão vinculados a nenhum contrato, seja de forma direta ou indireta (via ContratoEmpenhos).
          Desse modo, tais itens não sofrerão impedimentos, pois só restará a possibilidade de estarem associados a Empenhos/Minutas sem vinculação com contratos (#issue 1704).
        */
        $search_itens = [];

        /*Consulta se há itens vinculados a Minutas de Empenho.
          Regras de impedimento:
             a) Minutas emitidas, do tipo Compra e com saldo disponível.
        */
        if (count($search_itens) > 0) {
            $itens_minutas = MinutaEmpenho::select(
                            'compra_items.numero',
                            'compras.id as compra_id',
                            'compra_items.id as compra_item_id'
                            )
                            ->join('unidades', 'minutaempenhos.unidade_id', '=', 'unidades.id')
                            ->join('compra_item_minuta_empenho', 'minutaempenhos.id', '=', 'compra_item_minuta_empenho.minutaempenho_id')
                            ->join('compra_items', 'compra_item_minuta_empenho.compra_item_id', '=', 'compra_items.id')
                            ->join('compras', 'compra_items.compra_id', '=', 'compras.id')
                            ->join('codigoitens as mod', 'compras.modalidade_id', '=', 'mod.id')
                            ->join('codigoitens as sit', 'minutaempenhos.situacao_id', '=', 'sit.id')
                            ->join('codigos', 'sit.codigo_id', '=', 'codigos.id')
                            ->join('compra_item_unidade', 'compra_items.id', '=', 'compra_item_unidade.compra_item_id')
                            ->join('unidades as uo', 'compras.unidade_origem_id', '=', 'uo.id')
                            ->where('uo.codigo', $data_request['numeroUasg'])
                            ->where('mod.descres', $data_request['modalidade'])
                            ->where('compras.numero_ano', 'LIKE', $data_request['numero'] .'/'. $data_request['ano'])
                            ->whereIn('compra_items.numero', $search_itens)
                            ->where('sit.descricao', 'LIKE', 'EMPENHO EMITIDO')
                            ->where('codigos.id', '=', 30)
                            ->whereRaw('compra_item_unidade.quantidade_saldo < compra_item_unidade.quantidade_autorizada')
                            ->distinct()->get()->toArray();

            //Redefinindo os itens localizados em Minutas de Empenho.
            $this->redefineItensComImpedimentos($data_request, $itens_minutas, $itens_retorno, $search_itens, 'Empenho');
        }

        return $itens_retorno;
    }

    private function redefineItensDeContratosRescindidosOuEncerrados($data_request, &$search_itens): void {

        //Consulta se há itens vinculados a Contratos Rescindidos do tipo Contrato ou Encerrados do tipo Empenho.
        $itens_contratos_rescindidos_encerrados = Contrato::select(
                                       'contratoitens.numero_item_compra as numero',
                                       'compras.id as compra_id',
                                       'compra_items.id as compra_item_id'
                                       )
                                       ->join('contratoitens', 'contratos.id', '=', 'contratoitens.contrato_id')
                                       ->join('unidades', 'contratos.unidadecompra_id', '=', 'unidades.id')
                                       ->join('codigoitens', 'contratos.modalidade_id', '=', 'codigoitens.id')
                                       ->leftJoin('codigoitens as ci', 'contratos.justificativa_contrato_inativo_id', '=', 'ci.id')
                                       ->join('codigoitens as ci2', 'contratos.tipo_id', '=', 'ci2.id')
                                       ->join('compras', function ($join) {
                                           $join->on('contratos.unidadecompra_id', '=', 'compras.unidade_origem_id')
                                                ->on('contratos.modalidade_id', '=', 'compras.modalidade_id')
                                                ->on('contratos.licitacao_numero', '=', 'compras.numero_ano');
                                       })
                                       ->join('compra_items', function ($join) {
                                           $join->on('compras.id', '=', 'compra_items.compra_id')
                                                ->on('contratoitens.catmatseritem_id', '=', 'compra_items.catmatseritem_id')
                                                ->on('contratoitens.numero_item_compra', '=', 'compra_items.numero');
                                       })
                                       ->where('unidades.codigo', $data_request['numeroUasg'])
                                       ->where('codigoitens.descres', $data_request['modalidade'])
                                       ->where('contratos.licitacao_numero', 'LIKE', $data_request['numero'] .'/'. $data_request['ano'])
                                       ->whereIn('contratoitens.numero_item_compra', $search_itens)
                                       ->whereNull('contratos.deleted_at')
                                       ->whereNull('contratoitens.deleted_at')
                                       ->where('contratos.elaboracao', false)
                                       ->where(function ($query) {
                                            $query->where(function ($query) {
                                                $query->where('ci2.descricao', '=', 'Empenho')
                                                      ->whereNotNull('ci.descricao');
                                            })
                                            ->orWhere(function ($query) {
                                                $query->where('ci2.descricao', '!=', 'Empenho')
                                                      ->whereExists(function ($query) {
                                                          $query->select(DB::raw(1))
                                                                ->from('contratohistorico as ch')
                                                                ->join('codigoitens as ci', 'ci.id', '=', 'ch.tipo_id')
                                                                ->join('codigos as co', 'co.id', '=', 'ci.codigo_id')
                                                                ->whereNull('ch.deleted_at')
                                                                ->whereColumn('ch.contrato_id', 'contratos.id')
                                                                ->where('ci.descricao', 'Termo de Rescisão')
                                                                ->where('co.descricao', 'Tipo de Contrato');
                                                      });
                                            });
                                       })
                                       ->distinct()->get()->toArray();
                
        for($i = 0; $i < count($data_request['numerosItens']); $i++) {
        $num_item = $data_request['numerosItens'][$i];

            for($j = 0; $j < count($itens_contratos_rescindidos_encerrados); $j++) {
                $num_item_consulta = $itens_contratos_rescindidos_encerrados[$j]['numero'];

                if ($num_item == $num_item_consulta) {
                    //Exclui o item localizado para não ser processado na próxima busca por Minutas de Empenho.
                    unset($search_itens[$i]);
                }
            }
        }
    }

    private function buscaUltimaPublicacao(int $contratohistorico_id)
    {

    }

    private function buscaUnidadePorCodigo(string $codigo)
    {
        $unidade = Unidade::where('codigo', $codigo)->first();
        return $unidade;
    }

    private function buscaModalidadePorCodigo(string $codigo)
    {
        $modalidade = Codigoitem::whereHas('codigo', function ($q) {
            $q->where('descricao', 'Modalidade Licitação');
        })
            ->where('descres', $codigo)
            ->first();

        return $modalidade;
    }

    //Função genérica utilizada pelas consultas que buscam itens com algum impedimento.
    private function redefineItensComImpedimentos($data, $itens_consulta, &$itens_retorno, &$search_itens, $msg): void {

        for($i = 0; $i < count($data['numerosItens']); $i++) {
            $num_item = $data['numerosItens'][$i];

                for($j = 0; $j < count($itens_consulta); $j++) {
                    $num_item_consulta = $itens_consulta[$j]['numero'];

                    if ($num_item == $num_item_consulta) {
                        $item['numeroItem'] = $num_item;
                        $item['possuiImpeditivo'] = true;
                        $item['textoImpeditivo'] = "Existe ". $msg ." vinculado(a) ao item no Compras Contratos. Favor verificar no sistema.";
                        $item['url'] = "https://contratos.comprasnet.gov.br/api/v1/compra/". $itens_consulta[$j]['compra_id'] ."/item/". $itens_consulta[$j]['compra_item_id'] ."/show";

                        $itens_retorno['itens'][$i] = $item;

                        //Exclui o item já localizado em Ata, Contrato ou Empenho.
                        unset($search_itens[$i]);
                    }
            }
        }
    }

}
