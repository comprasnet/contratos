<?php

namespace App\Http\Controllers\Api\PNCP;

use App\Models\Contrato;
use App\Models\EnviaDadosPncp;
use App\services\ExcessoesPNCP\Handlers\InserirContrato\ManipuladorExcecoesInserirContrato;
use Carbon\Carbon;
use Exception;
use App\Http\Controllers\Empenho\CompraSiasgCrudController;
use App\Http\Traits\LogTrait;
use App\Models\MinutaEmpenho;
use Illuminate\Support\Facades\Log;
use App\Http\Traits\EnviaPncpTrait;

class ContratoControllerPNCP extends PncpController
{
    use LogTrait, EnviaPncpTrait;

    protected $tiposContratoNegados = ['Termo Rescisão', 'Termo de Rescisão', 'Termo Aditivo', 'Termo Apostilamento', 'Termo de Apostilamento', 'Credenciamento', 'Outros'];

    public function __construct()
    {
        parent::__construct();
    }

    public function consultarContrato(String $cnpj, String $ano, String $sequencial)
    {
        $url = $this->montaUrl($this->urlEndpoint($cnpj, $ano, $sequencial), null);
        $response = $this->get($url, $this->montaHeader(true, []));

        return json_decode($response->getBody()->getContents(), true);
    }

    public function consultarContratoUrlMontada(String $url)
    {
        try {
            $response = $this->get($url, $this->montaHeader(true, []));
            return json_decode($response->getBody()->getContents(), true);
        } catch (Exception $e) {

//            $this->inserirLogCustomizado('pncp', 'error', $e, 'consultarContratoUrlMontada 38');
            throw $e;
        }
    }

    public function retificarContrato(
        $envio,
        $cnpj,
        $idUnico,
        $ano,
        $cnpjOrgao,
        $sequencialPNCP,
        EnviaDadosPncp $enviaDadosPncp = null
    )
    {
        if ($envio->vigencia_fim == null) {
            $envio->vigencia_fim = Carbon::parse($envio->vigencia_inicio)
                ->addYears(5)
                ->format('Y-m-d');
        }
        try {
            $url = $this->montaUrl($this->urlEndpoint($cnpjOrgao, $ano, $sequencialPNCP), null);
            $body = $this->montaBody(
                $envio->serializaPNCP($cnpj, $idUnico, true, $envio)
            );
            $header = array_merge($this->headerPadrao, ["Accept-Encoding" => 'gzip,deflate']);

            $response = $this->put($url, $this->montaHeader(true, $header), $body);
            $response->JSONEnviado = $body;
            return $response;
        } catch (Exception $e) {
            $this->inserirLogPncp($e, __METHOD__, __LINE__);
            if ($enviaDadosPncp) {
                if ($this->deveMoverLinhaPncpParaAnalise($e)) {
                    $this->alteraSituacaoParaAnalise($enviaDadosPncp, $e);
                    return (object) ['tratarRetorno' => false];
                }
                $this->registraErroNaTabelaEnviaDadosPncp($enviaDadosPncp, true, $e);
            }
            return (object) ['tratarRetorno' => false];
            #throw $e;
        }
    }
    //remover comentários quando o Comprasnet habilitar o softdeletes para os Contratos.
    //public function excluirContrato($envio, String $cnpj,String $ano,String $sequencial){
    public function excluirContrato(String $cnpj, String $ano, String $sequencial)
    {
        try {
            $url = $this->montaUrl($this->urlEndpoint($cnpj, $ano, $sequencial), null);
            $body = $this->montaBody(
                ['justificativa' => '']
            );
            $response = $this->delete($url, $this->montaHeader(true, []), $body);
            return $response;
        } catch (Exception $e) {
//            $this->inserirLogCustomizado('pncp', 'error', $e, 'excluirContrato 74');
            throw $e;
        }
    }

    public function inserirContrato($envio, $cnpj, $idUnico, $cnpjOrgao, EnviaDadosPncp $enviaDadosPncp = null)
    {
        if ($envio->vigencia_fim == null) {
            $envio->vigencia_fim = Carbon::parse($envio->vigencia_inicio)
                ->addYears(5)
                ->format('Y-m-d');
        }
        try {
            $url = $this->montaUrl("v1/orgaos/" . $cnpjOrgao . "/contratos", null);
            $body = $this->montaBody(
                $envio->serializaPNCP($cnpj, $idUnico, false)
            );
            $response = $this->post($url, $this->montaHeader(true, $this->headerPadrao), $body);
            $response->JSONEnviado = $body;
            return $response;
        } catch (Exception $e) {
            # Verifica se é um erro que deve ser movido para análise
            if ($enviaDadosPncp && $this->deveMoverLinhaPncpParaAnalise($e)) {
                $this->alteraSituacaoParaAnalise($enviaDadosPncp, $e, $this->deveForcarSituacaoParaAnalise($e));
                return (object) ['tratarRetorno' => false];
            }

            $data = [
                'envio' => $envio,
                'cnpj' => $cnpj,
                'id_unico' => $idUnico
            ];
            if ($cnpjOrgao && get_class($data['envio']) === 'App\Models\Contratohistorico') {
                $manipulador = app(ManipuladorExcecoesInserirContrato::class);

                $retornoManipulador = $manipulador->handle(
                    intval($e->status),
                        $e->getMessage() ?? '',
                    $cnpjOrgao,
                    $data,
                    $e);

                $informacoesAdicionais = [
                    # Se caiu nesta exceção, devolve esta informação indicando para a rotina
                    # que não precisa fazer o tratamento do retorno do PNCP,
                    # pois o tratamento já foi feito no manipulador
                    'tratarRetorno' => false
                ];
                $retorno = array_merge((array)$retornoManipulador, $informacoesAdicionais);
                return (object) $retorno;
            }
            $mensagem = 'Catch inserirContrato após ManipuladorExcecoesInserirContrato';
            $this->inserirLogDebug('pncp_debug', 'error', $mensagem);

            $this->inserirLogCustomizado('pncp_debug', 'error', $e);
            throw $e;
        }
    }

    private function urlEndpoint(String $cnpj, String $ano, String $sequencial)
    {
        return "v1/orgaos/" . $cnpj . "/contratos/" . $ano . "/" . $sequencial;
    }
}
