<?php

namespace App\Http\Controllers\Api\PNCP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Contratoarquivo;
use App\Models\MinutaEmpenho;
use Exception;
use Illuminate\Support\Facades\Log;
use App\Http\Traits\Formatador;

class DocumentoContratoController extends PncpController
{
    use Formatador;
    public function __construct()
    {
        parent::__construct();
    }

    public function consultarDocumentoContrato(string $cnpj, string $ano, string $sequencial)
    {

        $url = $this->montaUrl($this->urlEndpoint($cnpj, $ano, $sequencial), null);
        $response = $this->get($url, $this->montaHeader(true, []));

        return json_decode($response->getBody()->getContents(), true);
    }

    public function inserirDocumentoContrato($arquivo, string $cnpj, string $ano, string $sequencial)
    {
        try {
            if ($arquivo instanceof MinutaEmpenho) {
                $arquivo->descricao = 'Empenho ' . $arquivo->informacao_complementar;
                $codigoItem = new \stdClass;
                $codigoItem->descres = "CONTRATO";
                $codigoItem->descricao = "Contrato";
                $arquivo->codigoItem = $codigoItem;
            }
            $urlParams = [
                'cnpj' => $cnpj,
                'ano' => $ano,
                'sequencial' => $sequencial
            ];
            $url = $this->replaceRouteParam(
                config('api-pncp.url-contratos-arquivos-inserir-consultar'),
                $urlParams);

            $conteudo = $arquivo->serializaArquivoPNCP();
            if (!file_exists($conteudo)) {
                Log::error("Arquivo não encontrado", ['path' => $conteudo]);
                return; 
            }
            $tipo = $arquivo->codigoItem->descricao;
            $descricao = $arquivo->descricao ?? $tipo;
            if (isset($arquivo->link_sei) && $tipo === 'Contrato') {
                $descricao = $tipo;
            }
            $descricao = mb_substr($descricao, 0, 250);
            # Permite que todos os arquivos sejam enviados
            # Se for de um tipo específico, é enviado como código do tipo, se não, é enviado como 'Outros Documentos'
            $tipoDocumentoId =
                config('api-pncp.tipo_documento_contrato')[$arquivo->codigoItem->descres] ??
                config('api-pncp.tipo_documento_contrato')['OUTROS'];
            $header = ["Titulo-Documento" => utf8_decode($descricao), "Tipo-Documento-Id" => $tipoDocumentoId];
            $response = $this->enviarArquivoParaPncp(
                $url,
                $this->montaHeader(true, $header),
                'arquivo', $conteudo);
            if (is_string($response)) {
                throw new Exception($response);
            }
            return $response->getData();
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function baixarDocumentoContrato(string $cnpj, string $ano, string $sequencial, string $sequencialDocumento)
    {

        $url = $this->montaUrl($this->urlEndpoint($cnpj, $ano, $sequencial) . "/" . $sequencialDocumento, null);

        $response = $this->get($url, $this->montaHeader(true, []));

        return json_decode($response->getBody()->getContents(), true);
    }


    public function excluirDocumentoContrato($envio, string $cnpj, string $ano, string $sequencial, string $sequencialDocumento)
    {
        try {
            $url = $this->montaUrl($this->urlEndpoint($cnpj, $ano, $sequencial) . "/" . $sequencialDocumento, null);
            $body = $this->montaBody(
                $envio->serializaExclusaoPNCP()
            );
            $response = $this->deletePncpFile($url, $this->montaHeader(true, $this->headerPadrao), $body);
            return $response->getData();
        } catch (Exception $e) {
            throw $e;
        }
    }

    private function urlEndpoint(string $cnpj, string $ano, string $sequencial)
    {
        return "v1/orgaos/" . $cnpj . "/contratos/" . $ano . "/" . $sequencial . "/arquivos";
    }
}
