<?php

namespace App\Http\Controllers\Api\PNCP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Unidade;
use Exception;
use Illuminate\Support\Facades\DB;

class UnidadeController extends PncpController
{
    public function __construct(){

        parent::__construct();
    }

    public function insereUnidade($cnpj){
        try{

        $unidade = Unidade::where('cnpj',$cnpj)->first();
        $url = $this->montaUrl("v1/orgaos/".$cnpj."/unidades",null);
        $body = $this->montaBody(
            $unidade->serializaPNCP()
        );

        $response = $this->post($url, $this->montaHeader(true, $this->headerPadrao), $body);

        return json_decode($response->getBody()->getContents(), true);

        }catch(Exception $e){
            throw $e;
        }
    }

    public function consultaUnidades(String $cnpj){

        $params = ['cnpj'=>$cnpj];
        $url = $this->montaUrl("v1/orgaos/".$cnpj."/unidades",$params);

        $response = $this->get($url, $this->montaHeader(true, []));

        return json_decode($response->getBody()->getContents(), true);
    }

    public function buscaUnidade(String $cnpj, String $unidade){

        $params = ['cnpj'=>$cnpj];
        $url = $this->montaUrl("v1/orgaos/".$cnpj."/unidades/".$unidade, $params);

        $response = $this->get($url, $this->montaHeader(true, []));

        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @param string $cnpj
     * @param string $codigoUnidade
     * @return mixed
     * @throws Exception
     */
    public function inserirUnidadePNCP(string $cnpj, string $codigoUnidade)
    {
        try{

            $unidade = Unidade::where('codigo', $codigoUnidade)
                ->first();

            $url = $this->montaUrl("v1/orgaos/".$cnpj."/unidades",null);
            $body = $this->montaBody(
                $unidade->montarBodyPNCP()
            );

            $response = $this->post($url, $this->montaHeader(true, $this->headerPadrao), $body);

            return json_decode($response->getBody()->getContents(), true);

        }catch(Exception $e){
            throw $e;
        }
    }

}
