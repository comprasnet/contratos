<?php

namespace App\Http\Controllers\Api\PNCP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Codigoitem;
use App\Models\Contrato;
use App\Models\Contratoarquivo;
use App\Models\ContratoMinuta;
use App\Models\EnviaDadosPncp;
use Exception;
use Illuminate\Support\Facades\Log;

class DocumentoTermoContratoController extends PncpController
{

    public function __construct()
    {

        parent::__construct();
    }

    public function consultarDocumentoTermoContrato(string $cnpj, string $ano, string $sequencial, string $sequencialTermo)
    {

        $url = $this->montaUrl($this->urlEndpoint($cnpj, $ano, $sequencial, $sequencialTermo), null);
        $response = $this->get($url, $this->montaHeader(true, []));

        return json_decode($response->getBody()->getContents(), true);
    }

    public function inserirDocumentoTermoContrato($arquivo, string $cnpj, string $ano, string $sequencial, string $sequencialTermo)
    {
        try {
            $url = $this->montaUrl($this->urlEndpoint($cnpj, $ano, $sequencial, $sequencialTermo), null);
            $conteudo = $arquivo->serializaArquivoPNCP();
            if (!file_exists($conteudo)) {
                Log::error("Arquivo não encontrado", ['path' => $conteudo]);
                return; 
            }
            $tipo = $arquivo->codigoItem->descricao;
            $descricao = $arquivo->descricao ?? $tipo;
            if (isset($arquivo->link_sei) && $tipo === 'Contrato') {
                $descricao = $tipo;
            }
            $descricao = mb_substr($descricao, 0, 250);

            # Mantendo para tratar legado quando minutas eram enviadas
            if ($arquivo->codigoItem->descres == 'OUTROS') {
                $contratoMinuta = ContratoMinuta::join('codigoitens', "codigoitens.id", "=", "codigoitens_id")->where('link_sei', $arquivo->link_sei)->first();

                switch ($contratoMinuta['descricao']) {
                    case 'Termo de Apostilamento':
                        $contratoMinuta['descricao'] = 'Termo Apostilamento';
                        break;
                    case 'Termo de Rescisão':
                        $contratoMinuta['descricao'] = 'Termo Rescisão';
                        break;
                }

                $descr = Codigoitem::where(['codigo_id' => 17, "descricao" => $contratoMinuta['descricao']])->first();
                $arquivo->codigoItem->descres = $descr['descres'];
            }
            $header = [
                "Titulo-Documento" => utf8_decode($descricao),
                "Tipo-Documento-Id" => config('api-pncp.tipo_documento_contrato')[$arquivo->codigoItem->descres]
            ];
            $response = $this->enviarArquivoParaPncp(
                $url,
                $this->montaHeader(true, $header),
                'arquivo', $conteudo
            );
            if (is_string($response)) {
                throw new Exception($response);
            }
            return $response->getData();
        } catch (Exception $e) {
            throw $e;
        }

    }

    public function baixarDocumentoTermosDeContrato(string $cnpj, string $ano, string $sequencial, string $sequencialTermo, string $sequencialDocumento)
    {

        $url = $this->montaUrl($this->urlEndpoint($cnpj, $ano, $sequencial, $sequencialTermo) . "/" . $sequencialDocumento, null);

        $response = $this->get($url, $this->montaHeader(true, []));

        return json_decode($response->getBody()->getContents(), true);
    }


    public function excluirDocumentoTermoContrato($envio, string $cnpj, string $ano, string $sequencial, string $sequencialTermo, string $sequencialDocumento)
    {
        try {
            $url = $this->montaUrl($this->urlEndpoint($cnpj, $ano, $sequencial, $sequencialTermo) . "/" . $sequencialDocumento, null);
            $body = $this->montaBody(
                $envio->serializaExclusaoPNCP()
            );
            $response = $this->deletePncpFile($url, $this->montaHeader(true, $this->headerPadrao), $body);
            return $response->getData();
        } catch (Exception $e) {
            throw $e;
        }
    }

    private function urlEndpoint(string $cnpj, string $ano, string $sequencial, string $sequencialTermo)
    {
        return "v1/orgaos/" . $cnpj . "/contratos/" . $ano . "/" . $sequencial . "/termos/" . $sequencialTermo . "/arquivos";
    }
}
