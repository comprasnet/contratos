<?php

namespace App\Http\Controllers\Api\PNCP;

use App\Models\EnviaDadosPncp;
use http\Env;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Contrato;
use Exception;
use Illuminate\Support\Facades\Log;
use App\Http\Traits\EnviaPncpTrait;

class TermoContratoController extends PncpController
{
    use EnviaPncpTrait;

    public function consultarTermoContrato(string $cnpj, string $ano, string $sequencial, string $sequencialTermo)
    {

        $url = $this->montaUrl($this->urlEndpoint($cnpj, $ano, $sequencial) . "/" . $sequencialTermo, null);

        $response = $this->get($url, $this->montaHeader(true, []));

        return json_decode($response->getBody()->getContents(), true);
    }

    public function retificarTermoContrato($envio, string $cnpj, string $ano, string $sequencial,
                                           EnviaDadosPncp $registroDoTermoNaTabelaEnviaDadosPncp)
    {
        try {
            $url = $this->montaUrl(
                $this->urlEndpoint($cnpj, $ano, $sequencial) . "/" .
                $registroDoTermoNaTabelaEnviaDadosPncp->sequencialPNCP, null);
            $body = $this->montaBody(
                $envio->serializaTermoPNCP(true)
            );
            $header = array_merge($this->headerPadrao, ["Accept-Encoding" => 'gzip,deflate']);
            $response = $this->put($url, $this->montaHeader(true, $header), $body);
            $response->JSONEnviado = $body;
            return $response;
        } catch (Exception $e) {
            if ($registroDoTermoNaTabelaEnviaDadosPncp && $this->deveMoverLinhaPncpParaAnalise($e)) {
                $this->alteraSituacaoParaAnalise($registroDoTermoNaTabelaEnviaDadosPncp, $e);
                return false;
            }
            throw $e;
        }
    }

    public function excluirTermoContrato(string $cnpj, string $ano, string $sequencial, string $sequencialTermo)
    {
        try {
            $url = $this->montaUrl($this->urlEndpoint($cnpj, $ano, $sequencial) . "/" . $sequencialTermo, null);
            $body = $this->montaBody(
                ['justificativa' => '']
            );
            $response = $this->delete($url, $this->montaHeader(true, []), $body);
            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function consultarTermosDeContratoNoPNCP(string $cnpj, string $ano, string $sequencial)
    {
        $url = $this->montaUrl($this->urlEndpoint($cnpj, $ano, $sequencial), null);
        $response = $this->get($url, $this->montaHeader(true, []));
        return json_decode($response->getBody()->getContents(), true);
    }

    public function inserirTermoContrato(
        $envio,
        string $cnpj,
        string $ano,
        string $sequencial,
        EnviaDadosPncp $enviaDadosPNCP)
    {
        try {
            $url = $this->montaUrl($this->urlEndpoint($cnpj, $ano, $sequencial), null);
            $body = $this->montaBody($envio->serializaTermoPNCP(false));
            $response = $this->post($url, $this->montaHeader(true, $this->headerPadrao), $body);
            $response->JSONEnviado = $body;
            return $response;
        } catch (Exception $e) {
            $registroDoTermoNaTabelaEnviaDadosPncp = EnviaDadosPncp::find($enviaDadosPNCP->id);
            # Verifica se é um erro que deve ser movido para análise
            if ($registroDoTermoNaTabelaEnviaDadosPncp && $this->deveMoverLinhaPncpParaAnalise($e)) {
                $this->alteraSituacaoParaAnalise(
                    $registroDoTermoNaTabelaEnviaDadosPncp,
                    $e,
                    $this->deveForcarSituacaoParaAnalise($e)
                );
                return true;
            }
            # Se retornou erro devido ao termo já existir no PNCP, busca o termo no PNCP para obter o sequencial
            # e atualiza na tabela envia_dados_pncp
            $msgTermoJaExiste = 'Inclusão não permitida pois já existe termo de contrato para os parâmetros informados';
            if (preg_match("/{$msgTermoJaExiste}/", $e->getMessage())) {
                $termos = $this->consultarTermosDeContratoNoPNCP($cnpj, $ano, $sequencial);
                $bodyJson = json_decode($body, true); # para utilizar os dados
                $termoEncontrado = [];
                foreach ($termos as $termo) {
                    if (
                        $termo['numeroTermoContrato'] == $bodyJson['numeroTermoContrato'] &&
                        $termo['tipoTermoContratoNome'] == $registroDoTermoNaTabelaEnviaDadosPncp->tipo_contrato
                    ) {
                        $termoEncontrado = $termo;
                        break;
                    }
                }
                if (!empty($termoEncontrado)) {
                    # Atribui RETPEN, pois o termo pode ter sofrido alterações e não estava sendo enviado ao PNCP
                    $situacao = $this->recuperarIDSituacao('RETPEN');
                    $sequencialPNCP = $termo['sequencialTermoContrato'];
                    $json_enviado_inclusao = $body;
                    $link_pncp = $url . '/' . $sequencialPNCP;

                    $registroDoTermoNaTabelaEnviaDadosPncp->situacao = $situacao;
                    $registroDoTermoNaTabelaEnviaDadosPncp->sequencialPNCP = $sequencialPNCP;
                    $registroDoTermoNaTabelaEnviaDadosPncp->json_enviado_inclusao = $json_enviado_inclusao;
                    $registroDoTermoNaTabelaEnviaDadosPncp->retorno_pncp = '';
                    $registroDoTermoNaTabelaEnviaDadosPncp->link_pncp = $link_pncp;
                    $registroDoTermoNaTabelaEnviaDadosPncp->save();

                    $mensagemLog = 'Tentativa de incluir um termo que já existe no PNCP. ' .
                        'Termo marcado para retificação na tabela envia_dados_pncp: ' .
                        $registroDoTermoNaTabelaEnviaDadosPncp;
                    $this->inserirLogPncp($mensagemLog, __METHOD__, __LINE__, 'info');
                }
                return true;
            }
            $errorMessage = $e->getMessage() .
                "\r\nLinha PNCP: " . $enviaDadosPNCP . #json_encode($enviaDadosPNCP, JSON_PRETTY_PRINT) .
                "\r\nTermo de contrato: " . $envio /*json_encode($envio, JSON_PRETTY_PRINT)*/;
            $this->inserirLogPncp($errorMessage, __METHOD__, __LINE__);
            $this->registraErroNaTabelaEnviaDadosPncp($enviaDadosPNCP, false, $e);
            return false;
        }
    }

    private function urlEndpoint(string $cnpj, string $ano, string $sequencial)
    {
        return "v1/orgaos/" . $cnpj . "/contratos/" . $ano . "/" . $sequencial . "/termos";
    }
}
