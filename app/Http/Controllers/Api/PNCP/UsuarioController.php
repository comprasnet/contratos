<?php

namespace App\Http\Controllers\Api\PNCP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\RestAPIController;
use App\Models\PncpUsuario;
use App\PncpToken;
use Exception;
use Illuminate\Cookie\Middleware\EncryptCookies;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UsuarioController extends PncpController
{
    private $expiresIn;
    private $cacheExpiresIn;
    private $safetyMargin;

    public function __construct()
    {
        parent::__construct();
        $this->expiresIn = config('api-pncp.token_expiration.EXPIRES_IN');
        $this->safetyMargin = config('api-pncp.token_expiration.SAFETY_MARGIN');
        $this->cacheExpiresIn = config('api-pncp.token_expiration.CACHE_EXPIRES_IN')();
    }

    public function getUsuario(string $identificador)
    {

        $params = ['identificador' => $identificador];
        $url = $this->montaUrl("v1/usuarios", $params);

        $response = $this->get($url, $this->montaHeader(true, []));

        return json_decode($response->getBody()->getContents(), true);
    }

    public function putUsuario(int $id)
    {
        try {
            $pncpUsuario = PncpUsuario::where('id', $id)->first();

            $url = $this->montaUrl("v1/usuarios/" . $pncpUsuario->id_pncp, null);
            $body = $this->montaBody(
                $pncpUsuario->serializaPNCP()
            );
            $header = array_merge($this->headerPadrao, ["Accept-Encoding" => 'gzip,deflate']);
            $response = $this->put($url, $this->montaHeader(true, $header), $body);

        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Realiza o login no sistema PNCP, utilizando o cache para evitar autenticações desnecessárias.
     *
     * @param bool $forceLogin Força a geração de um novo token, ignorando o cache.
     * Há um job que executa a cada 30 min para atualizar o token. Quando a chamada é feita por este job,
     * sempre atualiza o token.
     * @return int|object Retorna o código de status HTTP (200 em caso de sucesso) ou um objeto de erro.
     * @throws Exception Em caso de erro durante a autenticação.
     */
    public function login($forceLogin = false)
    {
        try {
            # Se tiver o token no cache e não estiver expirado, não refaz a autenticação
            $cachedToken = Cache::get('pncp_token');
            if ($this->validaTokenNoCache($cachedToken) && !$forceLogin) {
                return 200;
            }
            # Se o token expirou (ou não existe) faz a autenticação
            return $this->gerarNovoToken();
        } catch (Exception $e) {
            Log::error('Erro ao tentar autenticar no PNCP: ' . $e->getMessage());
            if (strpos($e->getMessage(), '403 Forbidden') !== false) {
                return (object)[
                    'code' => 403,
                    'message' => 'Erro ao logar no PNCP'
                ];
            }
        }
    }

    /**
     * Valida o token armazenado no cache, verificando se ele ainda não expirou.
     *
     * @param array $cachedToken Conteúdo armazenado no cache, contendo o 'token' e 'expires' (data/hora de expiração).
     * @return bool Retorna true se o token for válido, false caso contrário.
     */
    private function validaTokenNoCache($cachedToken)
    {
        if (!$cachedToken) {
            return false;
        }
        # $token = $cachedToken['token'];
        $expires = $cachedToken['expires'] ?? null;
        # Verifica se o token não expirou
        if (!$expires || now()->greaterThanOrEqualTo($expires)) {
            return false;
        }
        return true;
    }

    /**
     * Gera um novo token de autenticação no PNCP e atualiza o cache e o banco de dados.
     *
     * @return int Retorna o código de status HTTP da requisição de autenticação.
     * @throws Exception Em caso de erro ao gerar o token ou se nenhum usuário ativo for encontrado.
     */
    private function gerarNovoToken()
    {
        $pncpUsuario = PncpUsuario::where('ativo', true)->first();
        if (!$pncpUsuario) {
            throw new Exception('Nenhum usuário ativo encontrado.');
        }
        $url = $this->montaUrl("v1/usuarios/login", null);
        $body = $this->montaBody([
            'login' => $pncpUsuario->login,
            'senha' => $pncpUsuario->senha,
        ]);
        $response = $this->post($url, $this->montaHeader(false, $this->headerPadrao), $body);
        if ($response->getStatusCode() < 200 || $response->getStatusCode() >= 300) {
            throw new Exception('Erro ao gerar novo token.');
        }
        $token = $response->getHeader('authorization')[0];
        $expiresInMilliseconds = (int)$response->getHeader('expires')[0]; # Tempo de expiração PNCP vem em milissegundos
        if ($expiresInMilliseconds) {
            $this->expiresIn = $expiresInMilliseconds / 1000;
        } # Se não retornar tempo de expiração, usa o default

        # Calcula o tempo de expiração com 10% a menos de tempo como margem de segurança
        $this->cacheExpiresIn = $this->expiresIn - $this->safetyMargin;

        # Usa o transaction pra garantir que o cache e o banco tenham os mesmos valores
        DB::transaction(function () use ($token, $pncpUsuario) {
            $pncpUsuario->expires_in = $this->expiresIn;
            $pncpUsuario->expires = now()->addSeconds($this->expiresIn);
            $pncpUsuario->token = $token;
            $pncpUsuario->save();

            Cache::put('pncp_token', [
                'token' => $token,
                'expires' => now()->addSeconds($this->expiresIn),
            ], now()->addSeconds($this->cacheExpiresIn));
        });

        return $response->getStatusCode();
    }

}
