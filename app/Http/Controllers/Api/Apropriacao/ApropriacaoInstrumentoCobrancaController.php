<?php

namespace App\Http\Controllers\Api\Apropriacao;


use App\Helpers\ErrorMessage;
use App\Http\Controllers\Apropriacao\FaturaController;
use App\Models\Contratounidadedescentralizada;
use App\Repositories\Nonce\NonceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\ErrorMessageApropriarApi;
use App\Http\Controllers\Apropriacao\ApropriacaoFaturaSiafiController;
use App\Http\Requests\ApropriarRequest;
use App\Http\Requests\EditarApropriacaoRequest;
use App\Http\Traits\BuscaCodigoItens;
use App\Jobs\EnviarApropriacaoParaSiafiJob;
use App\Http\Traits\ContratoFaturaTrait;
use App\Http\Traits\ValidacoesInstrumentoCobrancaApiTrait;
use App\Models\ApropriacaoContratoFaturas;
use App\Http\Controllers\Apropriacao\AlteracaoApropriacaoController;
use App\Http\Requests\ConsultarInstumentoCobrancaNotaFiscalRequest;
use App\Models\SfCentroCusto;
use App\Models\SfDadosBasicos;
use App\Models\SfPco;
use App\services\Apropriacao\ApropriacaoInstrumentoCobrancaService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Exception;
use Illuminate\Http\Response;
use App\Jobs\CancelarApropriacaoJob;
use App\Models\Catmatseritem;
use App\Models\Codigoitem;
use App\Models\CompraItemMinutaEmpenho;
use App\Models\Contratofatura;
use App\Models\ContratoFaturasItens;
use App\Models\ContratoItemMinutaEmpenho;
use App\Models\ContratoMinutaEmpenhoPivot;
use App\Models\MinutaEmpenho;

class ApropriacaoInstrumentoCobrancaController extends Controller
{
    use ValidacoesInstrumentoCobrancaApiTrait;
    use BuscaCodigoItens;
    use ContratoFaturaTrait;

    private $faturaController;
    private $apropriacaoFaturaSiafiController;
    private $apropriacaoService;
    private $nonceRepository;
    private $alteracaoApropriacaoController;

    public function __construct(
        FaturaController $faturaController,
        ApropriacaoFaturaSiafiController $apropriacaoFaturaSiafiController,
        AlteracaoApropriacaoController $alteracaoApropriacaoController,
        NonceRepository $nonceRepository,
        ApropriacaoInstrumentoCobrancaService $apropriacaoService
    ) {
        $this->faturaController = $faturaController;
        $this->apropriacaoFaturaSiafiController = $apropriacaoFaturaSiafiController;
        $this->apropriacaoService = $apropriacaoService;
        $this->nonceRepository = $nonceRepository;
        $this->alteracaoApropriacaoController = $alteracaoApropriacaoController;
    }

    /**
     * @OA\Post(
     *     path="/api/v1/contrato/instrumento_cobranca/apropriar",
     *     summary="Apropria instrumento de cobrança",
     *     tags={"Apropriação"},
     *     security={ {"bearerAuth": {} }},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             required={"nonce", "id_inst_cobranca", "cpf_usuario", "tipo_dh", "cod_ug_emitente", "data_emissao_contabil", "data_vencimento", "taxa_cambio", "processo", "data_ateste", "observacao", "informacoes_adicionais", "sf_pco", "data_pagamento", "sf_centro_custo"},
     *             @OA\Property(property="nonce", type="string", maxLength=100),
     *             @OA\Property(property="id_inst_cobranca", type="array", @OA\Items(type="integer")),
     *             @OA\Property(property="cpf_usuario", type="string", maxLength=11, description="CPF do usuário"),
     *             @OA\Property(property="tipo_dh", type="string"),
     *             @OA\Property(property="cod_ug_emitente", type="integer"),
     *             @OA\Property(property="data_emissao_contabil", type="string", format="date"),
     *             @OA\Property(property="data_vencimento", type="string", format="date"),
     *             @OA\Property(property="taxa_cambio", type="number"),
     *             @OA\Property(property="processo", type="string", maxLength=255),
     *             @OA\Property(property="data_ateste", type="string", format="date"),
     *             @OA\Property(property="observacao", type="string", maxLength=468, nullable=true),
     *             @OA\Property(property="informacoes_adicionais", type="string", maxLength=500),
     *              @OA\Property(property="sf_pco", type="array", @OA\Items(
     *                 @OA\Property(property="cod_situacao", type="string", maxLength=255),
     *                 @OA\Property(property="indicador_contrato", type="boolean"),
     *                 @OA\Property(property="despesa_antecipada", type="boolean"),
     *                 @OA\Property(property="txtinscrd", type="string", maxLength=500, nullable=true),
     *                 @OA\Property(property="numclassd", type="integer", nullable=true),
     *                 @OA\Property(property="txtinscre", type="string", maxLength=500, nullable=true),
     *                 @OA\Property(property="numclasse", type="integer", nullable=true),
     *                 @OA\Property(property="sf_pco_item", type="array", @OA\Items(
     *                     @OA\Property(property="numero_empenho", type="string", maxLength=255),
     *                     @OA\Property(property="subelemento", type="string", maxLength=255),
     *                     @OA\Property(property="indicador_liquidado", type="boolean"),
     *                     @OA\Property(property="valor_item", type="number"),
     *                     @OA\Property(property="txtinscra", type="string", maxLength=500, nullable=true),
     *                     @OA\Property(property="numclassa", type="integer", nullable=true),
     *                     @OA\Property(property="txtinscrb", type="string", maxLength=500, nullable=true),
     *                     @OA\Property(property="numclassb", type="integer", nullable=true),
     *                     @OA\Property(property="txtinscrc", type="string", maxLength=500, nullable=true),
     *                     @OA\Property(property="numclassc", type="integer", nullable=true)
     *                     )),
     *              )),
     *             @OA\Property(property="data_pagamento", type="string", format="date"),
     *             @OA\Property(property="sf_centro_custo", type="array", @OA\Items(
     *                 @OA\Property(property="cod_centro_custo", type="string", maxLength=255),
     *                 @OA\Property(property="mes", type="integer", nullable=true),
     *                 @OA\Property(property="ano", type="integer", nullable=true),
     *                 @OA\Property(property="codigo_siorg", type="integer", nullable=true),
     *                 @OA\Property(property="ug_beneficiada", type="integer", nullable=true),
     *                 @OA\Property(property="item_vlrcc", type="array", @OA\Items(
     *                     @OA\Property(property="cod_situacao", type="string", maxLength=255),
     *                     @OA\Property(property="numero_empenho", type="string", maxLength=255),
     *                     @OA\Property(property="valor_item", type="number"),
     *                 )),
     *             )),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Solicitação de apropriação enviada com sucesso e está sendo processada",
     *         @OA\JsonContent(
     *             @OA\Property(property="Apropriacao", type="object",
     *                 @OA\Property(property="id_apropriacao_inst_cobranca", type="integer"),
     *                 @OA\Property(property="status", type="string"),
     *                 @OA\Property(property="mensagem", type="string"),
     *                 @OA\Property(property="situacao", type="string"),
     *             ),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Token expirado",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="string"),
     *             @OA\Property(property="message", type="string"),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Sem permissão",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="string"),
     *             @OA\Property(property="message", type="string"),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Erro de validação",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="string"),
     *             @OA\Property(property="message", type="string"),
     *             @OA\Property(property="errors", type="object"),
     *         ),
     *     ),
     * )
     */

    public function apropriar(ApropriarRequest $request)
    {

        try {
            $request->validated();

            $faturaIds = $request->id_inst_cobranca;

            $data = [];
            $camposEnviados = [];

            $camposEnviados = $this->preencherCamposEnviados($request);

            $this->validaAntesApropriarApiTrait(
                $request,
                ...$camposEnviados
            );

            $arrayVerificacao = $this->verificaPermissoes($faturaIds[0]);

            if (!empty($arrayVerificacao)) {
                $responseData = json_decode($arrayVerificacao->getContent(), true);
                if ($responseData['status'] === 'error') {
                    return response()->json([
                        'status' => 'error',
                        'message' => $responseData['message']
                    ], $arrayVerificacao->getStatusCode());
                }
            }
            //dd('paree');
            // Realiza a chamada para criar apropriação
            $faturaApropriada = $this->faturaController->createComNovoPadrao($request);

            $padraoId = $faturaApropriada->original['padraoId'];

            $faturaApropriada->getStatusCode() === 200 ? EnviarApropriacaoParaSiafiJob::dispatch($padraoId, backpack_user()->id) : null;

            $this->nonceRepository->createNonce(
                $request->all(),
                $request->ip(),
                true,
                'Em processamento',
                $faturaApropriada->original['idApropriacao']
            );

            $data = [
                'id_apropriacao_inst_cobranca' => $faturaApropriada->original['idApropriacao'],
                'mensagem' => 'Solicitação de apropriação enviada com sucesso e está sendo processada',
            ];

            return response()->json([
                'status' => 'success',
                'message' => $data,
            ]);
        } catch (\Exception $e) {
            $errorMessage = $this->mapearErroParaMensagem($e->getMessage());
            $errorCode = $e->getMessage();
            return response()->json([
                'statuss' => 'error',
                'message' => $errorMessage ?? ErrorMessageApropriarApi::getErrorMessage('erro_desconhecido')
            ], ErrorMessageApropriarApi::getHttpErrorCode($errorCode));
        }
    }

    /**
     * @OA\Put(
     *     path="/api/v1/contrato/apropriacao/editar",
     *     summary="Edita apropriação de instrumento de cobrança",
     *     tags={"Apropriação"},
     *     security={ {"bearerAuth": {} }},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             required={"nonce", "id_apropriacao_inst_cobranca", "cpf_usuario", "tipo_dh", "cod_ug_emitente", "data_emissao_contabil", "data_vencimento", "taxa_cambio", "processo", "data_ateste", "observacao", "informacoes_adicionais", "sf_pco", "data_pagamento", "sf_centro_custo"},
     *             @OA\Property(property="nonce", type="string", maxLength=100),
     *             @OA\Property(property="id_apropriacao_inst_cobranca", type="integer"),
     *             @OA\Property(property="cpf_usuario", type="string", maxLength=11, description="CPF do usuário"),
     *             @OA\Property(property="cod_ug_emitente", type="integer"),
     *             @OA\Property(property="data_emissao_contabil", type="string", format="date"),
     *             @OA\Property(property="data_vencimento", type="string", format="date"),
     *             @OA\Property(property="taxa_cambio", type="number"),
     *             @OA\Property(property="processo", type="string", maxLength=255),
     *             @OA\Property(property="data_ateste", type="string", format="date"),
     *             @OA\Property(property="observacao", type="string", maxLength=468, nullable=true),
     *             @OA\Property(property="informacoes_adicionais", type="string", maxLength=500),
     *              @OA\Property(property="sf_pco", type="array", @OA\Items(
     *                 @OA\Property(property="cod_situacao", type="string", maxLength=255),
     *                 @OA\Property(property="indicador_contrato", type="boolean"),
     *                 @OA\Property(property="despesa_antecipada", type="boolean"),
     *                 @OA\Property(property="txtinscrd", type="string", maxLength=500, nullable=true),
     *                 @OA\Property(property="numclassd", type="integer", nullable=true),
     *                 @OA\Property(property="txtinscre", type="string", maxLength=500, nullable=true),
     *                 @OA\Property(property="numclasse", type="integer", nullable=true),
     *                 @OA\Property(property="sf_pco_item", type="array", @OA\Items(
     *                     @OA\Property(property="numero_empenho", type="string", maxLength=255),
     *                     @OA\Property(property="subelemento", type="string", maxLength=255),
     *                     @OA\Property(property="indicador_liquidado", type="boolean"),
     *                     @OA\Property(property="valor_item", type="number"),
     *                     @OA\Property(property="txtinscra", type="string", maxLength=500, nullable=true),
     *                     @OA\Property(property="numclassa", type="integer", nullable=true),
     *                     @OA\Property(property="txtinscrb", type="string", maxLength=500, nullable=true),
     *                     @OA\Property(property="numclassb", type="integer", nullable=true),
     *                     @OA\Property(property="txtinscrc", type="string", maxLength=500, nullable=true),
     *                     @OA\Property(property="numclassc", type="integer", nullable=true)
     *                     )),
     *              )),
     *             @OA\Property(property="data_pagamento", type="string", format="date"),
     *             @OA\Property(property="sf_centro_custo", type="array", @OA\Items(
     *                 @OA\Property(property="cod_centro_custo", type="string", maxLength=255),
     *                 @OA\Property(property="mes", type="integer", nullable=true),
     *                 @OA\Property(property="ano", type="integer", nullable=true),
     *                 @OA\Property(property="codigo_siorg", type="integer", nullable=true),
     *                 @OA\Property(property="ug_beneficiada", type="integer", nullable=true),
     *                 @OA\Property(property="item_vlrcc", type="array", @OA\Items(
     *                     @OA\Property(property="cod_situacao", type="string", maxLength=255),
     *                     @OA\Property(property="numero_empenho", type="string", maxLength=255),
     *                     @OA\Property(property="valor_item", type="number"),
     *                 )),
     *             )),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Solicitação de edição de apropriação enviada com sucesso e está sendo processada",
     *         @OA\JsonContent(
     *             @OA\Property(property="Apropriacao", type="object",
     *                 @OA\Property(property="id_apropriacao_inst_cobranca", type="integer"),
     *                 @OA\Property(property="status", type="string"),
     *                 @OA\Property(property="mensagem", type="string"),
     *                 @OA\Property(property="situacao", type="string"),
     *             ),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Token expirado",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="string"),
     *             @OA\Property(property="message", type="string"),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Sem permissão",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="string"),
     *             @OA\Property(property="message", type="string"),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Erro de validação",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="string"),
     *             @OA\Property(property="message", type="string"),
     *             @OA\Property(property="errors", type="object"),
     *         ),
     *     ),
     * )
     */

    public function editarApropriacao(EditarApropriacaoRequest $request)
    {
        try {
            $request->validated();

            $data = [];
            $camposEnviados = [];

            $camposEnviados = $this->preencherCamposEnviados($request);

            $this->validarExistenciaApropriacao($request->id_apropriacao_inst_cobranca);

            $apropriacaoContratoFatura = ApropriacaoContratoFaturas::where('apropriacoes_faturas_id', $request->id_apropriacao_inst_cobranca)
                ->first();

            $sfpadrao_id = $apropriacaoContratoFatura->sfpadrao_id;
            $contrato_fatura_id = $apropriacaoContratoFatura->contratofaturas_id;

            $arrayVerificacao = $this->verificaPermissoes($contrato_fatura_id);

            if (!empty($arrayVerificacao)) {
                $responseData = json_decode($arrayVerificacao->getContent(), true);
                if ($responseData['status'] === 'error') {
                    return response()->json([
                        'status' => 'error',
                        'message' => $responseData['message']
                    ], $arrayVerificacao->getStatusCode());
                }
            }

            $this->validaAntesEditarApropriacaoApiTrait(
                $contrato_fatura_id,
                $request->input("sf_pco.0.cod_situacao"),
                ...$camposEnviados
            );

            $apropriacaoAtualizada = $this->atualizarDados($request, $sfpadrao_id);

            $apropriacaoAtualizada->getStatusCode() === 200 ? EnviarApropriacaoParaSiafiJob::dispatch($sfpadrao_id, backpack_user()->id) : null;

            $this->nonceRepository->createNonce(
                $request->all(),
                $request->ip(),
                true,
                'Em processamento',
                $request->id_apropriacao_inst_cobranca
            );

            $data = [
                'id_apropriacao_inst_cobranca' => $request->id_apropriacao_inst_cobranca,
                'mensagem' => 'Solicitação de edição de apropriação enviada com sucesso e está sendo processada',
            ];

            return response()->json([
                'status' => 'success',
                'message' => $data,
            ]);
        } catch (\Exception $e) {
            $errorMessage = $this->mapearErroParaMensagem($e->getMessage());
            $errorCode = $e->getMessage();
            return response()->json([
                'status' => 'error',
                'message' => $errorMessage ?? ErrorMessageApropriarApi::getErrorMessage('erro_desconhecido')
            ], ErrorMessageApropriarApi::getHttpErrorCode($errorCode));
        }
    }

    public function atualizarDados(Request $request, $sfpadrao_id)
    {
        DB::beginTransaction();
        try {

            $this->alterarDadosBasicos($request, $sfpadrao_id);
            $this->atualizarDadosPco($request, $sfpadrao_id);
            $this->atualizarDadosCentroCusto($request, $sfpadrao_id);

            DB::commit();
        } catch (\Exception $exc) {
            DB::rollback();
        }

        return response()->json(
            [
                'result' => 'success',
                'message' => 'Dados da apropriação alterados.',
            ]
        );
    }

    public function alterarDadosBasicos(Request $request, $sfpadrao_id)
    {
        $sfDadosBasicos = SfDadosBasicos::where('sfpadrao_id', $sfpadrao_id)->first();

        if ($sfDadosBasicos) {
            $mapeamentoCampos = [
                'observacao' => 'txtobser',
                'informacoes_adicionais' => 'txtinfoadic',
                'data_emissao_contabil' => 'dtemis',
                'ateste' => 'dtateste',
                'data_vencimento' => 'dtvenc',
                'processo' => 'txtprocesso',
                'vlr' => 'vlrtaxacambio',
            ];

            foreach ($mapeamentoCampos as $campoRequest => $campoBanco) {

                $valor = $request->{$campoRequest};
                if (!is_null($valor)) {
                    $sfDadosBasicos->{$campoBanco} = $valor;
                }
            }

            $sfDadosBasicos->save();
        }
    }

    public function atualizarDadosPco($request, $sfpadrao_id)
    {
        try {
            DB::beginTransaction();

            $sfPco = SfPco::where('sfpadrao_id', $sfpadrao_id)->first();

            if ($sfPco) {
                $sfPco->delete();

                $this->faturaController->criarSfPco($sfpadrao_id, $request);
            }

            DB::commit();

            return response()->json(['statuss' => 'sucesso']);
        } catch (\Exception $e) {
            DB::rollBack();

            Log::error('Erro ao atualizar dados do Pco: ' . $e->getMessage(), ['exception' => $e]);

            throw $e;
        }
    }

    public function atualizarDadosCentroCusto($request, $sfpadrao_id)
    {
        try {
            DB::beginTransaction();

            $sfCentroCusto = SfCentroCusto::where('sfpadrao_id', $sfpadrao_id)->first();

            if ($sfCentroCusto) {
                $sfCentroCusto->delete();

                $this->faturaController->criarSfCentroCusto($sfpadrao_id, $request);
            }

            DB::commit();

            return response()->json(['statuss' => 'sucesso']);
        } catch (\Exception $e) {
            DB::rollBack();

            Log::error('Erro ao atualizar dados do Pco: ' . $e->getMessage(), ['exception' => $e]);

            throw $e;
        }
    }

    private function preencherCamposEnviados(Request $request)
    {
        $camposEnviados = [];
        $camposPermitidos = ['numclassd', 'numclasse', 'numclassa', 'numclassb', 'numclassc'];

        foreach ($camposPermitidos as $campo) {
            $valorCampoSfpco = $request->input("sf_pco.0.$campo");

            if (!is_null($valorCampoSfpco)) {
                $camposEnviados[] = ['nome' => $campo, 'valor' => $valorCampoSfpco];
            }

            $valorCampoSfpcoItem = $request->input("sf_pco.0.sf_pco_item.0.$campo");

            if (!is_null($valorCampoSfpcoItem)) {
                $camposEnviados[] = ['nome' => $campo, 'valor' => $valorCampoSfpcoItem];
            }
        }

        return $camposEnviados;
    }

    /**
     * @OA\Delete(
     *     path="/api/v1/contrato/apropriacao/excluir",
     *     security={ {"bearerAuth": {} }},
     *     operationId="excluirApropriacao",
     *     tags={"Apropriação"},
     *     summary="Excluir apropriação",
     *     description="Endpoint para excluir apropriação que ainda não foi enviada para o SIAFI",
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             @OA\Property(property="nonce", type="string"),
     *             @OA\Property(property="id_apropriacao_inst_cobranca", type="integer", description="ID da apropriação a ser excluída"),
     *             @OA\Property(property="cpf_usuario", type="string", maxLength=11, description="CPF do usuário")
     *         ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Apropriação excluída com sucesso",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Apropriação excluída com sucesso."),
     *             @OA\Property(property="id_apropriacao_inst_cobranca", type="integer", example=111111),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Erro de permissão",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Usuário sem permissão no sistema."),
     *             @OA\Property(property="id_apropriacao_inst_cobranca", type="integer", example=111111),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Apropriação não encontrada",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Apropriação não encontrada."),
     *             @OA\Property(property="id_apropriacao_inst_cobranca", type="integer", example=111111),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Erro interno do servidor",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Erro interno do servidor."),
     *             @OA\Property(property="id_apropriacao_inst_cobranca", type="integer", example=111111),
     *         ),
     *     ),
     * )
     */

    public function excluirApropriacao(Request $request)
    {

        try {
            $validator = Validator::make($request->all(), [
                'id_apropriacao_inst_cobranca' => 'required|int',
                'cpf_usuario' => 'required|string'
            ]);
            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()->first()], Response::HTTP_BAD_REQUEST);
            }

            //Verifica se usuário tem permissão para esta ação
            $user =  backpack_auth()->user();
            $hasPermission = $user->hasRole(['Execução Financeira']) || $user->hasRole(['Administrador']);
            if (!$hasPermission) {
                return response()->json(['message' => 'Usuário sem permissão no sistema.'], Response::HTTP_FORBIDDEN);
            }

            $apropriacao = ApropriacaoContratoFaturas::where('apropriacoes_faturas_id', $request->id_apropriacao_inst_cobranca)->first();

            if (!$apropriacao) {
                return response()->json(['message' => 'Apropriação não encontrada.'], Response::HTTP_NOT_FOUND);
            }

            $hasUserUasgPermission = $this->verificaUnidadeContratoFaturaAPI($apropriacao->fatura, backpack_user());
            //Verifica se usuário tem permissãso de unidade (UASG) nesse contrato
            if (!$hasUserUasgPermission) {
                return response()->json(['message' => 'Usuário sem permissão no contrato.'], Response::HTTP_FORBIDDEN);
            }

            //Verifica se o status das apropriações permitem excluisões.
            if (!($apropriacao->fatura->situacao == "AND" || $apropriacao->fatura->situacao == "ERR")) {
                return response()->json([
                    'message' => 'Operação não concluída, status da apropriação não permite exclusão.'
                ], Response::HTTP_FORBIDDEN);
            }

            //chama a função q exclui apropriações passando apenas os IDs com status q permite exclusão
            $this->alteracaoApropriacaoController->removerApropriacaoFatura(
                $apropriacao->sfpadrao_id,
                $apropriacao->apropriacoes_faturas_id,
                $apropriacao->contratofaturas_id,
                true
            );

            $this->nonceRepository->createNonce(
                $request->all(),
                $request->ip(),
                true,
                'Registro excluído com sucesso',
                $request->id_apropriacao_inst_cobranca
            );

            return response()->json([
                'message' => 'Apropriação excluída com sucesso.',
                'id_apropriacao_inst_cobranca' => $request->id_apropriacao_inst_cobranca
            ], 200);
        } catch (Exception $e) {
            Log::error($e->getMessage() . "na linha " . $e->getLine() . "no arquivo " . $e->getFile());
            response()->json(['message' => 'Erro desconhecido.'], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * @OA\Put(
     *     path="/api/v1/contrato/apropriacao/cancelar",
     *     security={ {"bearerAuth": {} }},
     *     summary="Cancela apropriação",
     *     operationId="cancelaApropriacao",
     *     tags={"Apropriação"},
     *     security={
     *         {"bearerAuth": {}}
     *     },
     *     @OA\RequestBody(
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="nonce", type="string", description="Valor único para garantir a não duplicação da requisição."),
     *             @OA\Property(property="id_apropriacao_inst_cobranca", type="integer", description="ID da apropriação a ser cancelada"),
     *             @OA\Property(property="cpf_usuario", type="string", maxLength=11, description="CPF do usuário")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Apropriação cancelada com sucesso",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Apropriação cancelada com sucesso."),
     *             @OA\Property(property="id_apropriacao_inst_cobranca", type="integer", example=123)
     *         )
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Usuário sem permissão no sistema ou no contrato",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Usuário sem permissão no sistema."),
     *             @OA\Property(property="id_apropriacao_inst_cobranca", type="integer", example=123)
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Apropriação não encontrada",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Apropriação não encontrada."),
     *             @OA\Property(property="id_apropriacao_inst_cobranca", type="integer", example=123)
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Erro interno do servidor",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Erro interno do servidor. - Mensagem de erro"),
     *             @OA\Property(property="id_apropriacao_inst_cobranca", type="integer", example=123)
     *         )
     *     )
     * )
     */

    public function cancelarApropriacaoSiafi(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'id_apropriacao_inst_cobranca' => 'required|int',
                'cpf_usuario' => 'required|string'
            ]);
            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()->first()], Response::HTTP_BAD_REQUEST);
            }

            //Verifica a permissão de usuario (adm solo, ou adm\acesso API + execucao financeira) ta no middleware auth
            $user =  backpack_auth()->user();
            $hasPermission = $user->hasRole(['Execução Financeira']) || $user->hasRole(['Administrador']);
            if (!$hasPermission) {
                return response()->json(['message' => 'Usuário sem permissão no sistema.'], Response::HTTP_FORBIDDEN);
            }

            $apropriacao = ApropriacaoContratoFaturas::where('apropriacoes_faturas_id', $request->id_apropriacao_inst_cobranca)->first();

            if (!$apropriacao) {
                return response()->json(['message' => 'Apropriação não encontrada.'], Response::HTTP_NOT_FOUND);
            }

            //caso encontre apropriacao, continua e verifica permissao no contrato...
            $hasUserUasgPermission = $this->verificaUnidadeContratoFaturaAPI($apropriacao->fatura, backpack_user());
            //Verifica se usuário tem permissãso de unidade (UASG) nesse contrato
            if (!$hasUserUasgPermission) {
                return response()->json(['message' => 'Usuário sem permissão no contrato.'], Response::HTTP_FORBIDDEN);
            }


            //Verifica se o status das apropriações q permitem cancelamento.
            if ($apropriacao->fatura->situacao !== "APR") {
                return response()->json([
                    'message' => 'Operação não concluída, status da apropriação não permite o cancelamento.'
                ], Response::HTTP_FORBIDDEN);
            }
            $dadosRequisicao = [
                "all" => $request->all(),
                "ip" => $request->ip(),
                "idApropriacao" => $request->id_apropriacao_inst_cobranca
            ];

            //faz o cancelamento da apropriação
            CancelarApropriacaoJob::dispatch($apropriacao->sfpadrao_id, backpack_user()->id);

            $this->nonceRepository->createNonce(
                $request->all(),
                $request->ip(),
                true,
                'Solicitação de cancelamento enviada com sucesso',
                $request->id_apropriacao_inst_cobranca,
                $user->id
            );

            return response()->json([
                'message' => 'Solicitação de cancelamento enviada com sucesso e está sendo processada',
                'id_apropriacao_inst_cobranca' => $request->id_apropriacao_inst_cobranca
            ], 200);
        } catch (Exception $e) {
            Log::error($e->getMessage() . "na linha " . $e->getLine() . "no arquivo " . $e->getFile());
            return response()->json(['message' => 'Erro desconhecido.'], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * @OA\Get(
     *     tags={"Apropriação"},
     *     summary="Retorna os registros de apropriações por contrato.",
     *     description="",
     *     path="/api/v1/contrato/apropriacao/consultar/{contrato_id}",
     *     security={ {"bearerAuth": {} }},
     *
     *     @OA\Parameter(
     *         name="contrato_id",
     *         in="path",
     *         description="id do contrato",
     *         required=true,
     *         @OA\Schema(
     *              type="integer"
     *        )
     *     ),
     *
     *  @OA\Response(
     *      response=200,
     *      description="Sucesso na operação",
     *      @OA\JsonContent(
     *          type="object",
     *          @OA\Property(property="id_contrato", type="integer"),
     *          @OA\Property(property="numero_contrato", type="string"),
     *          @OA\Property(property="apropriacao", type="array",
     *              @OA\Items(
     *                  type="object",
     *                  @OA\Property(property="id_apropriacao_inst_cobranca", type="integer"),
     * 
     *                  @OA\Property(property="inst_cobranca", type="array",
     *                      @OA\Items(
     *                          type="array",
     *                          @OA\Items(
     *                              type="object",
     *                              @OA\Property(property="id_inst_cobranca", type="integer"),
     *                              @OA\Property(property="valor_apropriacao_inst_cobranca", type="string"),
     *                          )
     *                      )
     *                  ),
     *                  @OA\Property(property="situacao_apropriacao", type="string"),
     *                  @OA\Property(property="emissao", type="string", format="date"),
     *                  @OA\Property(property="sistema_origem", type="string"),
     *                  @OA\Property(property="tipo_dh", type="string"),
     *                  @OA\Property(property="numero_dh_siafi", type="string", nullable=true),
     *                  @OA\Property(property="ug_emitente", type="integer"),
     *                  @OA\Property(property="valor_documento", type="string"),
     *                  @OA\Property(property="dados_empenho", type="array",
     *                      @OA\Items(
     *                          type="object",
     *                          @OA\Property(property="id", type="integer"),
     *                          @OA\Property(property="numero", type="string"),
     *                          @OA\Property(property="sub_elemento_item", type="integer"),
     *                          @OA\Property(property="valor_empenho", type="string"),
     *                      )
     *                  ),
     *              )
     *          ),
     *      ),
     *     ),
     *
     *     @OA\Response(
     *         response=401,
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/FalhaAutenticação")
     *     ),
     *
     *     @OA\Response(
     *         response=404,
     *         description="Recurso não localizado.",
     *         @OA\JsonContent(@OA\Property(property="error",type="string",example="Apropriação não encontrada."))
     *     ),
     *
     *     @OA\Response(
     *          response=403,
     *          description="Usuário sem permissão no sistema.",
     *          @OA\JsonContent(@OA\Property(property="error",type="string",example="Usuário sem permissão no contrato."))
     *      ),
     *
     * )
     */

    public function consultaApropriacao(int $contrato_id)
    {
        if (!backpack_user()->hasRole('Administrador')) {

            $unidadesDescentralizadasContrato = Contratounidadedescentralizada::where('contrato_id', $contrato_id)->pluck('unidade_id')->toArray();
            $unidadesUsuario = array_merge(
                backpack_user()->unidades->pluck('id')->toArray(),
                [backpack_user()->ugprimaria]
            );

            $unidadesUsuario = array_unique($unidadesUsuario);
        }

        try {
            return $this->apropriacaoService->consultaApropriacaoContrato($contrato_id, $unidadesUsuario ?? null, $unidadesDescentralizadasContrato ?? null);
        } catch (\Exception $e) {
            $statusCode = $e->getCode();
            $errorMessage = ErrorMessage::getErrorMessage($statusCode);

            return response()->json([
                "status" => "error",
                "message" => $errorMessage,
            ], $statusCode);
        }
    }

    /**
     * @OA\Get(
     *     tags={"Instrumento de Cobrança"},
     *     summary="Retorna pagamento dos instrumentos de cobrança.",
     *     description="Pagamento dos instrumentos de cobrança no sistema Contratos.gov.br",
     *     path="/api/v1/inst_cobranca/consultar",
     *     security={{"bearerAuth": {}}},
     *
     *     @OA\Parameter(
     *         name="ug_pagadora",
     *         in="query",
     *         description="Código da unidade pagadora.",
     *         required=true,
     *         @OA\Schema(
     *              type="integer"
     *         )
     *     ),
     *
     *     @OA\Parameter(
     *         name="numero_nota_fiscal",
     *         in="query",
     *         description="Número da nota fiscal",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *         )
     *     ),
     *
     *     @OA\Parameter(
     *         name="serie_nota_fiscal",
     *         in="query",
     *         description="Não é obrigatório serie, mas deve ser informado quando o instrumento e cobrança tiver",
     *         required=false,
     *         @OA\Schema(
     *              type="integer",
     *         )
     *     ),
     *
     *     @OA\Parameter(
     *         name="identificador_fornecedor",
     *         in="query",
     *         description="CNPJ do fornecedor",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *         )
     *     ),
     * 
     *      @OA\Parameter(
     *         name="numero_empenho",
     *         in="query",
     *         description="Número do empenho.",
     *         required=false,
     *         @OA\Schema(
     *              type="string"
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="Sucesso na operação",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="status", type="string", example="success"),
     *             @OA\Property(property="message", type="string", example="Consulta realizada com sucesso."),
     *             @OA\Property(property="data", type="object",
     *                 @OA\Property(property="numero_contrato", type="string", example="000006/2024"),
     *                 @OA\Property(property="nota_fiscal_id", type="integer", example=353865),
     *                 @OA\Property(property="numero_nota_fiscal", type="string", example="66112"),
     *                 @OA\Property(property="serie_nota_fiscal", type="string", nullable=true, example=null),
     *                 @OA\Property(property="data_emissao_nota_fiscal", type="string", format="date", example="2024-06-27"),
     *                 @OA\Property(property="tipo_entrada", type="array",
     *                     @OA\Items(type="string", example="DSP102")),
     *                 @OA\Property(property="ug_pagadora", type="string", example="183039"),
     *                 @OA\Property(property="data_emissao_contabil", type="string", format="date", example="2024-06-27"),
     *                 @OA\Property(property="data_ateste", type="string", format="date", example="2024-07-16"),
     *                 @OA\Property(property="numero_processo", type="string", example="52602.003365/2023-13"),
     *                 @OA\Property(property="tipo_fornecedor", type="string", example="JURIDICA"),
     *                 @OA\Property(property="identificador_fornecedor", type="string", example="35.820.448/0063-39"),
     *                 @OA\Property(property="nome_fornecedor", type="string", example="WHITE MARTINS GASES INDUSTRIAIS LTDA"),
     *                 @OA\Property(property="valor_total_nota", type="string", example="4121.37"),
     *                 @OA\Property(property="documento_habil", type="array",
     *                     @OA\Items(
     *                         type="object",
     *                         @OA\Property(property="numero_dh_siafi", type="string", example="2024NP232"),
     *                         @OA\Property(property="situacao_dh", type="string", example="Siafi Apropriado"),
     *                         @OA\Property(property="centro_custo", type="array",
     *                             @OA\Items(
     *                                 type="object",
     *                                 @OA\Property(property="codigo_centro_custo", type="string", example="CC-GENERICO")
     *                             )
     *                         ),
     *                         @OA\Property(property="empenho", type="array",
     *                             @OA\Items(
     *                                 type="object",
     *                                 @OA\Property(property="numero_empenho", type="string", example="2024NE000161"),
     *                                 @OA\Property(property="natureza_despeza", type="string", example="339030"),
     *                                 @OA\Property(property="data_emissao_empenho", type="string", format="date", example="2024-04-15"),
     *                                 @OA\Property(property="valor_total_empenho", type="string", example="12364.14"),
     *                                 @OA\Property(property="ano_exercicio_empenho", type="string", example="2024"),
     *                                 @OA\Property(property="ug_emitente_empenho", type="string", example="183039"),
     *                                 @OA\Property(property="itens_empenho", type="array",
     *                                     @OA\Items(
     *                                         type="object",
     *                                         @OA\Property(property="codigo_catmat", type="integer", example=416289),
     *                                         @OA\Property(property="descricao_catmat", type="string", example="COMPOSIÇÃO: 3,5% MONÓXIDO DE CARBONO, 6% DIÓXIDO DE CARBONO, CARACTERÍSTICAS ADICIONAIS: PROPANO 800 PPM"),
     *                                         @OA\Property(property="descricao_detalhada", type="string", example="COMPOSIÇÃO: 3,5% MONÓXIDO DE CARBONO, 6% DIÓXIDO DE CARBONO, CARACTERÍSTICAS ADICIONAIS: PROPANO 800 PPM"),
     *                                         @OA\Property(property="quantidade_item", type="string", example="1.00000000000000000"),
     *                                         @OA\Property(property="valor_unitario", type="string", example="4121.37"),
     *                                         @OA\Property(property="valor_total_item", type="string", example="4121.37"),
     *                                         @OA\Property(property="sub_elemento_item", type="string", example="04"),
     *                                         @OA\Property(property="numseq", type="string", example="1"),
     *                                         @OA\Property(property="conta_contabil", type="array",
     *                                             @OA\Items(type="object", 
     *                                                 @OA\Property(property="DSP102", type="array", @OA\Items(type="integer", example=115610100))
     *                                             )
     *                                         )
     *                                     )
     *                                 )
     *                             )
     *                         )
     *                     )
     *                 )
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=401,
     *         description="Token expirado",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="string"),
     *             @OA\Property(property="message", type="string"),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Sem permissão",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="string"),
     *             @OA\Property(property="message", type="string"),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Instrumento de cobrança não encontrado.",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="string", example="error"),
     *             @OA\Property(property="errors", type="object",
     *                 @OA\Property(property="errors", type="array", @OA\Items(type="string", example="Instrumento de cobrança não encontrado"))
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Erro ao consultar IC.",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="string", example="error"),
     *             @OA\Property(property="errors", type="object",
     *                 @OA\Property(property="errors", type="array", @OA\Items(type="string", example="Erro ao consultar IC."))
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Instrumento de cobrança não apropriado.",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="string", example="error"),
     *             @OA\Property(property="errors", type="object",
     *                 @OA\Property(property="errors", type="array", @OA\Items(type="string", example="Instrumento de cobrança não apropriado."))
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="O número do empenho é obrigatório para notas fiscais vinculadas a mais de um contrato.",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="string", example="error"),
     *             @OA\Property(property="errors", type="object",
     *                 @OA\Property(property="errors", type="array", @OA\Items(type="string", example="O número do empenho é obrigatório para notas fiscais vinculadas a mais de um contrato."))
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Instrumento de cobrança é de serviço.",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="string", example="error"),
     *             @OA\Property(property="errors", type="object",
     *                 @OA\Property(property="errors", type="array", 
     *                     @OA\Items(type="string", example="Instrumento de cobrança é de serviço.")
     *                 )
     *             )
     *         )
     *     )
     * )
     */
    public function consultarInstumentoCobrancaNotaFiscal(ConsultarInstumentoCobrancaNotaFiscalRequest $request)
    {
        try {
            $instrumentoCobranca = Contratofatura::with([
                'contrato.fornecedor',
                'contratofaturasitem',
                'apropriacaoContratoFaturasApropriacao' => function ($query) {
                    $query->whereHas('apropriacao.fase', function ($q) {
                        $q->where('descres', 'APR');
                    })->with([
                        'sfpadrao.pco.pcoItens',
                        'sfpadrao.centroCusto',
                        'sfpadrao.dadosBasicos'
                    ]);
                }
            ])
                ->whereHas('apropriacaoContratoFaturasApropriacao.sfpadrao.dadosBasicos', function ($query) use ($request) {
                    $query->where('codugpgto', $request->ug_pagadora);
                })
                ->where('numero', $request->numero_nota_fiscal)
                ->whereHas('contrato.fornecedor', function ($query) use ($request) {
                    $query->where('cpf_cnpj_idgener', $request->identificador_fornecedor);
                })
                ->when(
                    $request->filled('serie_nota_fiscal'), function ($query) use ($request) {
                        $query->where('serie', $request->serie_nota_fiscal);
                    }, function ($query) {
                        $query->where(function ($query) {
                            $query->whereNull('serie')->orWhere('serie', '');
                        });
                    }
                )
                ->when($request->filled('numero_empenho'), function ($query) use ($request) {
                    $query->whereHas('contratoFaturaEmpenhos.empenho', function ($q) use ($request) {
                        $q->where('numero', $request->numero_empenho);
                    });
                })
                ->get();

            if ($instrumentoCobranca->isEmpty()) {
                return response()->json(
                    [
                        'status' => 'error',
                        'errors' => [
                            'errors' => [
                                "Instrumento de cobrança não encontrado"
                            ]
                        ]
                    ],
                    404
                );
            }

            if ($instrumentoCobranca->count() > 1) {
                return response()->json(
                    [
                        'status' => 'error',
                        'errors' => [
                            'errors' => [
                                "O número do empenho é obrigatório para notas fiscais vinculadas a mais de um contrato."
                            ]
                        ]
                    ],
                    400
                );
            }

            $instrumentoCobranca = $instrumentoCobranca->first();

            if (!in_array($instrumentoCobranca->situacao, ["APR", "PGO"])) {
                return response()->json(
                    [
                        'status' => 'error',
                        'errors' => [
                            'errors' => [
                                "Instrumento de cobrança não apropriado."
                            ]
                        ]
                    ],
                    422
                );
            }

            if ($instrumentoCobranca->getTipoLista() != "FORNECIMENTO DE BENS") {
                return response()->json(
                    [
                        'status' => 'error',
                        'errors' => [
                            'errors' => [
                                "Instrumento de cobrança é de serviço."
                            ]
                        ]
                    ],
                    405
                );
            }

            $sfPadrao = $instrumentoCobranca->apropriacaoContratoFaturasApropriacao->last()->sfpadrao;

            $format = [
                'numero_contrato' => $instrumentoCobranca->contrato->numero,
                'nota_fiscal_id' => $instrumentoCobranca->id,
                'numero_nota_fiscal' => $instrumentoCobranca->numero,
                'serie_nota_fiscal' => $instrumentoCobranca->serie,
                'data_emissao_nota_fiscal' => $instrumentoCobranca->emissao,
                'tipo_entrada' => $sfPadrao->pco->map(function ($pco) {
                    return $pco->codsit;
                })->toArray(),
                'ug_pagadora' => $sfPadrao->dadosBasicos->codugpgto,
                'data_emissao_contabil' => $sfPadrao->dadosBasicos->dtemis,
                'data_ateste' => $instrumentoCobranca->ateste,
                'numero_processo' => $instrumentoCobranca->processo,
                'tipo_fornecedor' => $instrumentoCobranca->contrato->fornecedor->tipo_fornecedor,
                'identificador_fornecedor' => $instrumentoCobranca->contrato->fornecedor->cpf_cnpj_idgener,
                'nome_fornecedor' => $instrumentoCobranca->contrato->fornecedor->nome,
                'valor_total_nota' => number_format((float) $instrumentoCobranca->contratofaturaItens->sum('valortotal_faturado'), 2, '.', ''),
                'documento_habil' => [
                    [
                        'numero_dh_siafi'   => $sfPadrao->getSfpadrao(),
                        'situacao_dh'       => $instrumentoCobranca->retornaSituacao(),
                        'centro_custo'      => $sfPadrao->centroCusto->map(function ($centroCusto) {
                            return [
                                'codigo_centro_custo' => $centroCusto->codcentrocusto ?? null
                            ];
                        })->toArray(),
                        'empenho' => $instrumentoCobranca->contratoFaturaEmpenhos->map(function ($contratoFaturaEmpenho) use ($instrumentoCobranca, $sfPadrao) {

                            static $empenhosProcessados = [];

                            $numeroEmpenho = $contratoFaturaEmpenho->empenho->numero;

                            if (in_array($numeroEmpenho, $empenhosProcessados)) {
                                return null;
                            }

                            $empenhosProcessados[] = $numeroEmpenho;

                            $itensArray = [];
                            $minuta = MinutaEmpenho::with(['saldo_contabil.unidade', 'tipo_empenhopor'])
                            ->where('mensagem_siafi', $numeroEmpenho)
                                ->where('fornecedor_empenho_id', $instrumentoCobranca->contrato->fornecedor_id)
                                ->whereHas('saldo_contabil', function ($q) use ($instrumentoCobranca) {
                                    $q->where('unidade_id', $instrumentoCobranca->contrato->unidadeorigem_id)
                                        ->orWhere('unidade_id', $instrumentoCobranca->contrato->unidade_id)
                                        ->orWhereIn('unidade_id', $instrumentoCobranca->contrato->unidadesdescentralizadas->pluck('unidade_id')->toArray());
                                })
                                ->where(function ($query) use ($instrumentoCobranca) {
                                    $query->where('contrato_id', $instrumentoCobranca->contrato_id)
                                        ->orWhereNull('contrato_id');
                                })
                                ->where(function ($query) use ($instrumentoCobranca) {
                                    $query->where('numero_contrato', $instrumentoCobranca->contrato->numero)
                                        ->orWhereNull('numero_contrato');
                                })
                                ->whereHas('situacao', function ($q) {
                                    $q->where('descres', 'EMITIDO');
                                })
                                ->first();

                            $itens = $this->apropriacaoService->getItens($minuta);

                            foreach ($itens as $item) {
                                $result = $instrumentoCobranca->contratofaturasitem()
                                    ->whereHas('saldohistoricoitem', function ($q) use ($item) {
                                        $q->where('numero_item_compra', $item['numeroItem'])
                                        ->whereHas('contratoItem.item', function ($q) use ($item) {
                                            $q->where('codigo_siasg', $item['catmat']);
                                        });
                                    })
                                    ->first();

                                if ($result) {
                                    $itensArray[] = [
                                        'codigo_catmat'     => $item['catmat'],
                                        'descricao_catmat'  => $item['descricao'],
                                        'descricao_detalhada' => $item['descricaoDetalhada'],
                                        'quantidade_item'   => $result->quantidade_faturado,
                                        'valor_unitario'    => number_format($result->valorunitario_faturado, 2, '.', ''),
                                        'valor_total_item'  => $result->valortotal_faturado,
                                        'sub_elemento_item' => $item['subelementoItem'],
                                        'numseq'            => $item['numseq'],
                                        'conta_contabil'    => $sfPadrao->pco->map(function ($pco) {
                                            return [
                                                $pco->codsit => $pco->pcoItens->flatMap(function ($pcoItem) {
                                                    return collect([$pcoItem->numclassa, $pcoItem->numclassb, $pcoItem->numclassc])
                                                        ->filter(function ($numClass) {
                                                            return substr($numClass, 0, 1) === '1';
                                                        });
                                                })->unique()->values()
                                            ];
                                        })
                                    ];
                                }
                            }

                            return [
                                'numero_empenho'        => $numeroEmpenho,
                                'natureza_despeza'      => $this->apropriacaoService->getNaturezaDespeza($minuta->id),
                                'data_emissao_empenho'  => $contratoFaturaEmpenho->empenho->data_emissao,
                                'valor_total_empenho'   => number_format($minuta->valor_total, 2, '.', ''),
                                'ano_exercicio_empenho' => substr($numeroEmpenho, 0, 4),
                                'ug_emitente_empenho'   => $minuta->saldo_contabil->unidade->codigo,
                                'itens_empenho'         => $itensArray
                            ];
                        })->filter()->values()->toArray()
                    ]
                ]
            ];
            return response()->json(
                [
                    'status' => 'success',
                    'message' => 'Consulta realizada com sucesso.',
                    'data' => $format
                ],
                200
            );
        } catch (\Exception $e) {
            Log::error(__METHOD__ . ' Linha ' . __LINE__ . ' - Erro ao consultar IC.' .
            '[' . $e->getMessage() . ']');
            return response()->json(
                [
                    'status' => 'error',
                    'errors' => [
                        'errors' => [
                            "Erro ao consultar IC."
                        ]
                    ]
                ],
                500
            );
        }
    }
}
