<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\RecomposicaoCusto\ModeloDocumento;
use Illuminate\Support\Facades\DB;

class ModeloDocumentoController extends Controller
{
    public function index(Request $request)
    {
        $search_term = $request->input('q');

        // NOTE: this is a Backpack helper that parses your form input into an usable array. 
        // you still have the original request as `request('form')`
        $form = collect($request->input('form'))->pluck('value', 'name');
       
        $options = ModeloDocumento::query();

        // if no category has been selected, show no options
        if (! $form['codigoitens_id']) {
            return [];
        }

        // if a category has been selected, only show articles in that category
        if ($form['codigoitens_id']) {
            $options = $options->where('codigoitens_id', $form['codigoitens_id']);
        }

        if ($search_term) {
            $results = $options->where('title', 'LIKE', '%'.$search_term.'%')->paginate(10);
        } else {
            $results = $options->paginate(10);
        }

        return $results;
    }

    public function show($id)
    {
        return ModeloDocumento::find($id);
    }
}
