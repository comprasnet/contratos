<?php

namespace App\Http\Controllers\Api\OrdemBancaria;

use App\Helpers\ErrorMessage;
use App\Http\Controllers\Controller;
use App\Http\Requests\OrdemBancariaApropriacaoRequest;
use App\Http\Resources\OrdemBancariaApropriacaoContrato\OrdemBancariaApropriacaoPorContratoCollection;
use App\Models\Unidade;
use App\services\STA\STAOrdemBancariaService;
use Illuminate\Http\Response;

class OrdemBancariaApropriacaoController extends Controller
{
    private $staOrdemBancariaService;
    public function __construct(STAOrdemBancariaService $staOrdemBancariaService)
    {
        $this->staOrdemBancariaService = $staOrdemBancariaService;
    }


    /**
     * @OA\Get(
     *     tags={"Ordem Bancária"},
     *     summary="Retorna os registros de ordem bancária por apropriação.",
     *     description="Retorna a lista de ordem bancária para uma apropriação específica.",
     *     path="/api/v1/ordembancaria/consultar/{id_apropriacao}",
     *     security={ {"bearerAuth": {} }},
     *
     *     @OA\Parameter(
     *         name="id_apropriacao",
     *         in="path",
     *         description="ID da apropriação",
     *         required=true,
     *         @OA\Schema(
     *              type="integer"
     *        )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="Sucesso na operação",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="ordem_bancaria", type="array",
     *                 @OA\Items(
     *                     type="object",
     *                     @OA\Property(property="conta_bancaria", type="string"),
     *                     @OA\Property(property="agencia", type="string"),
     *                     @OA\Property(property="banco", type="string"),
     *                     @OA\Property(property="numero_ob", type="string"),
     *                     @OA\Property(property="numero_cancelamento_ob", type="string", nullable=true),
     *                     @OA\Property(property="data_emissao", type="string", format="date"),
     *                     @OA\Property(property="valor_ob", type="string"),
     *                     @OA\Property(property="observacao", type="string"),
     *                 )
     *             ),
     *         ),
     *     ),
     *
     *     @OA\Response(
     *         response=401,
     *         description="Não autorizado",
     *         @OA\JsonContent(ref="#/components/schemas/FalhaAutenticação")
     *     ),
     *
     *     @OA\Response(
     *         response=404,
     *         description="Recurso não encontrado.",
     *         @OA\JsonContent(@OA\Property(property="error", type="string", example="Não foi localizada nenhuma ordem bancária para esta apropriação."))
     *     ),
     *
     *     @OA\Response(
     *         response=422,
     *         description="Entidade não processável devido a erros de validação ou dados inválidos.",
     *         @OA\JsonContent(
     *         @OA\Property(property="error", type="string", example="Apropriação específica não foi encontrada.")
     *     )
     *  ),
     * )
     */
    public function consultaOrdemBancaria(int $id_apropriacao, $possuiPermissao = true)
    {
        try {
            $ordemBancaria = $this->staOrdemBancariaService->retornaNumeroFaturaOB($id_apropriacao, $possuiPermissao);
            return response()->json($ordemBancaria);

        } catch (\Exception $e) {
            $statusCode = $e->getCode();
            $errorMessage = ErrorMessage::getErrorMessage($statusCode);
            return response()->json([
                "status" => "error",
                "message" => $e->getMessage() == '' ? $errorMessage : $e->getMessage(),
            ], $statusCode);
        }

    }

    public function consultaOrdemBancariaInstrumentoCobranca(array $instrumento_cobranca_id)
    {
        try {
            $ordemBancaria = $this->staOrdemBancariaService->retornaNumeroFaturaOBIntrumentoCobranca($instrumento_cobranca_id);
            return response()->json($ordemBancaria);

        } catch (\Exception $e) {
            $statusCode = $e->getCode();
            $errorMessage = ErrorMessage::getErrorMessage($statusCode);
            return response()->json([
                "status" => "error",
                "message" => $e->getMessage() == '' ? $errorMessage : $e->getMessage(),
            ], $statusCode);
        }

    }

    public function consultaOrdemBancariaPorContrato(OrdemBancariaApropriacaoRequest $request)
    {
        try {

            if (!backpack_user()->can('ordem_bancaria_apropriacao_instcobranca_acesso')) {
                throw new \Exception("Usuário sem permissão no sistema", Response::HTTP_NOT_FOUND);
            }

            if ($request->ug_emitente_contrato) {
                $unidade = Unidade::where('codigo', str_pad($request->ug_emitente_contrato, 6, "0", STR_PAD_LEFT))->first();

                if (!$unidade) {
                    throw new \Exception("Código da UG do contrato {$request->ug_emitente_contrato} não encontrada.", Response::HTTP_NOT_FOUND);
                }
            }

            $contratoIds = $this->staOrdemBancariaService->retornaContratoIds($request, $unidade->id ?? null);

            if (count($contratoIds) == 0) {
                return response()->json([
                    "status" => "error",
                    "message" => "Nenhum contrato encontrado para os parâmetros fornecidos.",
                ], Response::HTTP_NOT_FOUND);
            }
            return [
                'status' => 'success',
                'data' => new OrdemBancariaApropriacaoPorContratoCollection($contratoIds)
            ];

        } catch (\Exception $e) {
            $statusCode = $e->getCode();
            $errorMessage = ErrorMessage::getErrorMessage($statusCode);
            return response()->json([
                "status" => "error",
                "message" => $e->getMessage() == '' ? $errorMessage : $e->getMessage(),
            ], $statusCode);
        }
    }
}
