<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Execfin\EmpenhoCrudController;
use App\Http\Controllers\NDC;
use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\ConsultaCompra;
use App\Http\Traits\Formatador;
use App\Http\Traits\LogTrait;
use App\Models\AmparoLegal;
use App\Models\Codigoitem;
use App\Models\Compra;
use App\Models\CompraItem;
use App\Models\CompraItemMinutaEmpenho;
use App\Models\CompraItemUnidade;
use App\Models\ContaCorrentePassivoAnterior;
use App\Models\ContratoItemMinutaEmpenho;
use App\Models\ContratoMinutaEmpenho;
use App\Models\Fornecedor;
use App\Models\MinutaEmpenho;
use App\Models\MinutaEmpenhoRemessa;
use App\Models\Naturezasubitem;
use App\Models\SaldoContabil;
use App\Models\SfCelulaOrcamentaria;
use App\Models\SfItemEmpenho;
use App\Models\SfOperacaoItemEmpenho;
use App\Models\SfOrcEmpenhoDados;
use App\Models\SfPassivoAnterior;
use App\Models\SfPassivoPermanente;
use App\Models\SfRegistroAlteracao;
use App\Models\Unidade;
use App\Models\Catmatseritem;
use App\services\AmparoLegalService;
use App\XML\ApiSiasg;
use App\XML\Execsiafi;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Route;
use App\Http\Controllers\Controller;
use App\Http\Traits\CompraTrait;
use App\Http\Traits\ContratoItemMinutaEmpenhoTrait;
use App\Http\Controllers\APIController;

class MinutaEmpenhoController extends Controller
{
    use CompraTrait;
    use BuscaCodigoItens;
    use Formatador;
    use ConsultaCompra;
    use LogTrait;
    use ContratoItemMinutaEmpenhoTrait;

    public function populaTabelasSiafi(Request $request): array
    {
        $retorno['resultado'] = false;
        $vigencia = true;
        $minuta_id = Route::current() !== null ? Route::current()->parameter('minuta_id') : $request->minuta_id;
        $situacao = 'EM PROCESSAMENTO';

        if (!empty($request->alteracao_fonte_minutaempenho_id)) {
            $situacao = 'AGUARDANDO ANULAÇÃO';
            $minuta_id = $request->alteracao_fonte_minutaempenho_id;
        }

        if (backpack_user() !== null) {
            $cpf = backpack_user()->cpf;
        } elseif (!empty($request->cpf_user)) {
            $cpf = $request->cpf_user;
        }

        $modMinutaEmpenho = MinutaEmpenho::find($minuta_id);
        $modSaldoContabil = SaldoContabil::find($modMinutaEmpenho->saldo_contabil_id);
        $modRemessa = MinutaEmpenhoRemessa::where('minutaempenho_id', $minuta_id)->first();

        DB::beginTransaction();
        try {
            $this->removeSfOrcEmpenhoDadosErroAndamento($modMinutaEmpenho, $modRemessa);

            $sforcempenhodados = $this->gravaSfOrcEmpenhoDados($modMinutaEmpenho, $situacao, $cpf);

            $this->gravaSfCelulaOrcamentaria($sforcempenhodados, $modSaldoContabil);

            if ($modMinutaEmpenho->passivo_anterior) {
                $this->gravaSfPassivoAnterior($sforcempenhodados, $modMinutaEmpenho, $modRemessa);
            }

            $vigencia = $this->gravaSfItensEmpenho(
                $modMinutaEmpenho,
                $sforcempenhodados,
                $modRemessa->id,
                'inclusao'
            );

            if (!$vigencia) {
                $retorno['msg'] = 'Não é possível continuar a emissão, porque existe(m) item(ns) não vigente(s). ' .
                    'Por favor, retornar a etapa 3.';
                return $retorno ;
            }

            if (empty($request->alteracao_fonte_minutaempenho_id)) {
                $this->gravaMinuta($modMinutaEmpenho);

                $this->gravaRemessaOriginal($modRemessa);
            }

            session(['situacao' => 'EM PROCESSAMENTO']);
            DB::commit();
            $retorno['resultado'] = true;
        } catch (Exception $exc) {
            if (strpos($exc->getMessage(), 'não possui saldo de compra') !== false) {
                $retorno['msg'] = substr($exc->getMessage(), 0, strpos($exc->getMessage(), '.') + 1);
            }
            $this->inserirLogCustomizado('emissao-minuta-empenho', 'error', $exc);
            DB::rollback();
        }

        return $retorno;
    }

    public function gravaSfOrcEmpenhoDados(MinutaEmpenho $modMinutaEmpenho, string $situacao, string $cpf)
    {

        $modSfOrcEmpenhoDados = new SfOrcEmpenhoDados();
        $tipoEmpenho = Codigoitem::find($modMinutaEmpenho->tipo_empenho_id);
        $favorecido = Fornecedor::find($modMinutaEmpenho->fornecedor_empenho_id);
        $amparoLegal = AmparoLegal::find($modMinutaEmpenho->amparo_legal_id);
        $ugemitente = Unidade::find($modMinutaEmpenho->saldo_contabil->unidade_id);
        $codfavorecido = (str_replace('-', '', str_replace('/', '', str_replace('.', '', $favorecido->cpf_cnpj_idgener))));
        $informacao_complementar = $modMinutaEmpenho->informacao_complementar;
        if ($modMinutaEmpenho->numero_cipi) {
            $informacao_complementar = $informacao_complementar . ' - CIPI: ' . $modMinutaEmpenho->numero_cipi;
        }

        $modSfOrcEmpenhoDados->minutaempenho_id = $modMinutaEmpenho->id;
        $modSfOrcEmpenhoDados->ugemitente = $ugemitente->codigo;// 429
        //$modSfOrcEmpenhoDados->ugemitente = $ugemitente->codigosiafi;
        $modSfOrcEmpenhoDados->anoempenho = (int)config('app.ano_minuta_empenho');
        $modSfOrcEmpenhoDados->tipoempenho = $tipoEmpenho->descres;
        $modSfOrcEmpenhoDados->numempenho = (!is_null($modMinutaEmpenho->numero_empenho_sequencial)) ? $modMinutaEmpenho->numero_empenho_sequencial : null;
        $modSfOrcEmpenhoDados->dtemis = $modMinutaEmpenho->data_emissao;
        $modSfOrcEmpenhoDados->txtprocesso = (!is_null($modMinutaEmpenho->processo)) ? $modMinutaEmpenho->processo : null;
        $modSfOrcEmpenhoDados->vlrtaxacambio = (!is_null($modMinutaEmpenho->taxa_cambio)) ? $modMinutaEmpenho->taxa_cambio : null;
        $modSfOrcEmpenhoDados->vlrempenho = (!is_null($modMinutaEmpenho->valor_total)) ? $modMinutaEmpenho->valor_total : null;
        $modSfOrcEmpenhoDados->codfavorecido = $codfavorecido;
        $modSfOrcEmpenhoDados->codamparolegal = $amparoLegal->codigo;
        $modSfOrcEmpenhoDados->txtinfocompl = $informacao_complementar;
        $modSfOrcEmpenhoDados->txtlocalentrega = $modMinutaEmpenho->local_entrega;
        $modSfOrcEmpenhoDados->txtdescricao = $modMinutaEmpenho->descricao;
        $modSfOrcEmpenhoDados->situacao = $situacao;
        $modSfOrcEmpenhoDados->cpf_user = $cpf;
        $modSfOrcEmpenhoDados->minutaempenhos_remessa_id = $modMinutaEmpenho->max_remessa;
        $modSfOrcEmpenhoDados->sfnonce = $modMinutaEmpenho->remessa()->find($modMinutaEmpenho->max_remessa)->sfnonce;
        $modSfOrcEmpenhoDados->save();

        return $modSfOrcEmpenhoDados;
    }


    public function gravaSfCelulaOrcamentaria(SfOrcEmpenhoDados $sforcempenhodados, SaldoContabil $modSaldoContabil)
    {
        $modSfCelulaOrcamentaria = new SfCelulaOrcamentaria();
        $modSfCelulaOrcamentaria->sforcempenhodado_id = $sforcempenhodados->id;
        $modSfCelulaOrcamentaria->esfera = (int)substr($modSaldoContabil->conta_corrente, 0, 1);
        $modSfCelulaOrcamentaria->codptres = substr($modSaldoContabil->conta_corrente, 1, 6);
        $modSfCelulaOrcamentaria->codfonterec = substr($modSaldoContabil->conta_corrente, 7, 10);
        $modSfCelulaOrcamentaria->codnatdesp = (int)substr($modSaldoContabil->conta_corrente, 17, 6);
        $modSfCelulaOrcamentaria->ugresponsavel = (int)substr($modSaldoContabil->conta_corrente, 23, 8);
        $modSfCelulaOrcamentaria->codplanointerno = substr($modSaldoContabil->conta_corrente, 31, 11);

        $modSfCelulaOrcamentaria->save();
        return $modSfCelulaOrcamentaria;
    }

    public function gravaSfPassivoAnterior(
        SfOrcEmpenhoDados $sforcempenhodados,
        MinutaEmpenho $modMinutaEmpenho,
        MinutaEmpenhoRemessa $modRemessa
    ) {
        $modSfPassivoAnterior = new SfPassivoAnterior();
        $modSfPassivoAnterior->sforcempenhodado_id = $sforcempenhodados->id;
        $modSfPassivoAnterior->codcontacontabil = $modMinutaEmpenho->conta_contabil_passivo_anterior;
        $modSfPassivoAnterior->save();

        $this->gravaSfPassivoPermanente($modSfPassivoAnterior, $modMinutaEmpenho, $modRemessa);

        return $modSfPassivoAnterior;
    }

    public function gravaSfPassivoPermanente(
        SfPassivoAnterior $sfpassivoanterior,
        MinutaEmpenho $modMinutaEmpenho,
        MinutaEmpenhoRemessa $modRemessa
    ) {
        $modCCPassivoAnterior = ContaCorrentePassivoAnterior::where('minutaempenho_id', $modMinutaEmpenho->id)
            ->where('minutaempenhos_remessa_id', $modRemessa->id)
            ->get();

        foreach ($modCCPassivoAnterior as $key => $conta) {
            $modSfPassivoPermanente = new SfPassivoPermanente();
            $modSfPassivoPermanente->sfpassivoanterior_id = $sfpassivoanterior->id;
            $modSfPassivoPermanente->contacorrente = "P" . $conta->conta_corrente;
            $modSfPassivoPermanente->vlrrelacionado = $conta->valor;
            $modSfPassivoPermanente->save();
        }
        return $modSfPassivoPermanente;
    }

    public function gravaSfItensEmpenho(
        MinutaEmpenho $modMinutaEmpenho,
        SfOrcEmpenhoDados $sforcempenhodados,
        $remessa_id = 0,
        $tipo_minuta = null
    ) {
        $tipo = $modMinutaEmpenho->tipo_empenhopor->descricao;

        $itens = $this->getItens($tipo, $modMinutaEmpenho->id, $remessa_id);

        foreach ($itens as $key => $item) {
            if ($item->operacao !== 'NENHUMA') {
                if (isset($item->compra_item)) {
                    $compra = $item->compra_item->compra;
                    $tipoCompra =  $compra->tipo_compra;

                    //caso seja srp e a não exista ata vigente, retorna false
                    if ($tipoCompra === 'SISRP') {
                        $vigenciaFim = $item->compra_item->ata_vigencia_fim;
                        $compraPertenceLei14133Derivadas =
                            resolve(AmparoLegalService::class)->existeAmparoLegalEmLeiCompra($compra->lei);
                        if ($compraPertenceLei14133Derivadas) {
                            $cif = $item->compra_item->compra_item_fornecedor
                                ->where('fornecedor_id', $modMinutaEmpenho->fornecedor_compra_id)
                                ->first();
                            $vigenciaFim = $cif->ata_vigencia_fim;
                        }

                        if (is_null($vigenciaFim)) {
                            return  false;
                        }

                        if (is_string($vigenciaFim) && trim($vigenciaFim) !== '') {
                            $dataVigenciaFim = Carbon::createFromFormat('Y-m-d', $vigenciaFim);
                            $hoje = Carbon::today();
                            //Caso não seja Alteracao e anulação, verificar vigencia
                            if (!($tipo_minuta === 'alteracao'
                                    && ($item->operacao === 'ANULAÇÃO'
                                        || $item->operacao === 'IRRISORIO'
                                        || $item->operacao === 'CANCELAMENTO'
                                        || $item->operacao === 'ANULAÇÃO SALDO IRRISÓRIO'
                                    )
                                )
                                && $hoje->gt($dataVigenciaFim)) {
                                return false;
                            }
                        }
                    }
                }

                $descricao = $this->getDescItem($item, $tipo);
                $this->verificaSaldo($item, $tipo, $modMinutaEmpenho);

                $modSfItemEmpenho = new SfItemEmpenho();
                $modSubelemento = Naturezasubitem::find($item->subelemento_id);
                $modSfItemEmpenho->sforcempenhodado_id = $sforcempenhodados->id;
                $modSfItemEmpenho->numseqitem = $item->numseq;
                $modSfItemEmpenho->codsubelemento = $modSubelemento->codigo;
                $modSfItemEmpenho->descricao = $descricao;

                $modSfItemEmpenho->save();

                $this->gravaSfOperacaoItemEmpenho($modSfItemEmpenho, $item);
            }
        }

        return true;
    }

    public function gravaSfOperacaoItemEmpenho(SfItemEmpenho $modSfItemEmpenho, $item)
    {
        $vlroperacao      = ($item->valor > 0) ? $item->valor : $item->valor * -1;
        $quantidade       = ($item->valor < 0) ? $item->quantidade * -1 : $item->quantidade;
        $operacao_descres = $item->operacao_descres;
        $vlrunitario      = ($item->valor / $item->quantidade);

        if ($item->operacao_descres === 'IRRISORIO') {
            $operacao_descres = 'ANULACAO';
            $quantidade       = 1;
            $vlrunitario      = $vlroperacao;
        }

        $modSfOpItemEmpenho                          = new SfOperacaoItemEmpenho();
        $modSfOpItemEmpenho->sfitemempenho_id        = $modSfItemEmpenho->id;
        $modSfOpItemEmpenho->tipooperacaoitemempenho = $operacao_descres;
        $modSfOpItemEmpenho->quantidade              = $quantidade;
        $modSfOpItemEmpenho->vlrunitario             = $vlrunitario;
        $modSfOpItemEmpenho->vlroperacao             = $vlroperacao;
        $modSfOpItemEmpenho->save();
    }

    public function gravaMinuta(MinutaEmpenho $modMinutaEmpenho)
    {

        $situacao = Codigoitem::wherehas('codigo', function ($q) {
            $q->where('descricao', '=', 'Situações Minuta Empenho');
        })
            ->where('descricao', 'EM PROCESSAMENTO')
            ->first();

        $modMinutaEmpenho->situacao_id = $situacao->id;
        $modMinutaEmpenho->save();
    }

    public function novoEmpenhoMesmaCompra()
    {
        $minuta_id = Route::current()->parameter('minuta_id');
        $modMinutaEmpenho = MinutaEmpenho::find($minuta_id);
        $situacao_id = $this->retornaIdCodigoItem('Situações Minuta Empenho', 'EM ANDAMENTO');

        DB::beginTransaction();
        try {
            $this->atualizaSaldoCompraItemUnidade($modMinutaEmpenho);
            $novoEmpenho = new MinutaEmpenho();
            $novoEmpenho->unidade_id = $modMinutaEmpenho->unidade_id;
            $novoEmpenho->compra_id = $modMinutaEmpenho->compra_id;
            $novoEmpenho->informacao_complementar = $modMinutaEmpenho->informacao_complementar;
            $novoEmpenho->situacao_id = $situacao_id;//em andamento
            $novoEmpenho->tipo_empenhopor_id = $modMinutaEmpenho->tipo_empenhopor_id;
            $novoEmpenho->etapa = 2;
            $novoEmpenho->save();
            DB::commit();
            return json_encode($novoEmpenho->id);
        } catch (Exception $exc) {
            $this->inserirLogCustomizado('emissao-minuta-empenho', 'error', $exc);
            DB::rollback();
        }
    }

    public function atualizaSaldoCompraItemUnidade(MinutaEmpenho $modMinutaEmpenho)
    {
        $compra = Compra::find($modMinutaEmpenho->compra_id);

        $compraSiasg = $this->buscaCompraSiasg($compra);

        //COMPRA SISPP
        if ($compraSiasg->data->compraSispp->tipoCompra == 1) {
            $this->gravaParametroItensdaCompraSISPP($compraSiasg, $compra);
            return;
        }

        //COMPRA SISRP: tipoCompra === '2'
        //Caso seja sisrp e nao Seja lei 14133 grava os itens
        $compraPertenceLei14133Derivadas = resolve(AmparoLegalService::class)
            ->existeAmparoLegalEmLeiCompra($compraSiasg->data->compraSispp->lei);

        if (!$compraPertenceLei14133Derivadas) {
            $this->gravaParametroItensdaCompraSISRP($compraSiasg, $compra);
        }
    }

    public function buscaCompraSiasg(Compra $compra)
    {
        $uasgCompra_id = (!is_null($compra->unidade_subrrogada_id))
            ? $compra->unidade_subrrogada_id
            : $compra->unidade_origem_id;

        $modalidade = Codigoitem::find($compra->modalidade_id);
        $uasgCompra = Unidade::find($uasgCompra_id);
        $numero_ano = explode('/', $compra->numero_ano);
        $uasgBeneficiaria     = !is_null($compra->uasg_beneficiaria_id)
            ? Unidade::find($compra->uasg_beneficiaria_id)->codigo
            : null;
        [$numero_compra, $ano_compra] = explode('/', $compra->numero_ano);

        return  $this->buscarCompra(
            $modalidade->descres,
            $uasgCompra->codigo,
            session('user_ug'),
            $uasgBeneficiaria,
            $numero_compra,
            $ano_compra
        );
    }


    /**
     * Método para buscar as minutas de empenho de acordo com uasg da pessoa logada
     * e o id do fornecedor passado na request utilizado no formulário de contrato.
     *
     * @return  array $minutaEmpenho
     */

    public function minutaempenhoparacontrato(Request $request)
    {
        $search_term = $request->input('q');
        $compra_id = null;
        $form = collect($request->input('form'))->pluck('value', 'name');

        $situacao = Codigoitem::whereHas('codigo', function ($query) {
            $query->where('descricao', 'Situações Minuta Empenho');
        })
            ->where('descricao', 'EMPENHO EMITIDO')
            ->select('codigoitens.id')->first();

        $tipoCompraId = Codigoitem::where('descricao', 'Compra')->first()->id;

        $options = MinutaEmpenho::query();

        if (!$form['fornecedor_id']) {
            return [];
        }

        if ($form['fornecedor_id']) {
            $options
                ->select(['minutaempenhos.id',
                    DB::raw("CONCAT(minutaempenhos.mensagem_siafi, ' - ', to_char(data_emissao, 'DD/MM/YYYY')  )
                             as nome_minuta_empenho"),
                    'minutaempenhos.compra_id'])
                ->distinct('minutaempenhos.id')
                ->join('compras', 'minutaempenhos.compra_id', '=', 'compras.id')
                ->join('codigoitens', 'codigoitens.id', '=', 'compras.modalidade_id')
                ->join('unidades', 'minutaempenhos.unidade_id', '=', 'unidades.id')
                ->leftJoin('contrato_minuta_empenho_pivot', 'minutaempenhos.id', '=', 'contrato_minuta_empenho_pivot.minuta_empenho_id')
                ->where('minutaempenhos.fornecedor_empenho_id', $form['fornecedor_id'])
                ->where('minutaempenhos.unidade_id', '=', session()->get('user_ug_id'))
                ->where('minutaempenhos.situacao_id', '=', $situacao->id)
                ->where('minutaempenhos.tipo_empenhopor_id', $tipoCompraId)
                ->whereNotExists(function ($query) {
                    $query->select(DB::raw(1))
                        ->from('contrato_minuta_empenho_pivot AS cmep')
                        ->whereRaw('cmep.minuta_empenho_id = minutaempenhos.id');
                    if (!empty($form['contrato_id'])) {
                        $query->where('contrato_id', '<>', $form['contrato_id']);
                    }
                });
        }

        if ($search_term) {
            $options->where('minutaempenhos.mensagem_siafi', 'LIKE', "%$search_term%");
        }

        if (isset($form['minutasempenho']) && $form['minutasempenho']) {
            $compra_id = MinutaEmpenho::where('id', $form['minutasempenho'])->first()->compra_id;
            $options->where('minutaempenhos.compra_id', $compra_id);
        }

        return $options->paginate(10);
    }

    private function getItens($tipo, $minuta_id, $remessa_id)
    {
        if ($tipo === 'Contrato') {
            return ContratoItemMinutaEmpenho::where('minutaempenho_id', $minuta_id)
                ->where('minutaempenhos_remessa_id', $remessa_id)
                ->orderBy('numseq', 'asc')
                ->get();
        }

        return CompraItemMinutaEmpenho::where('minutaempenho_id', $minuta_id)
            ->where('minutaempenhos_remessa_id', $remessa_id)
            ->orderBy('numseq', 'asc')
            ->with(['compra_item.compra.tipo_compra'])
            ->get();
    }

    private function getDescItem($item, $tipo)
    {
        if ($tipo === 'Contrato') {
            $contrato_item = DB::table('contratoitens')
                ->where('id', $item->contrato_item_id)
                ->first();

            $desc = $contrato_item->descricao_complementar;

            $descricao = (!is_null($desc) && $desc !== 'undefined')
                ? $desc
                :  Catmatseritem::find($contrato_item->catmatseritem_id)->descricao;
            $descricao = 'Item compra: '. $contrato_item->numero_item_compra . ' - ' . $descricao;

            return (strlen($descricao) < 1248) ? $descricao : mb_substr($descricao, 0, 1248);
        }

        $descricao = '';
        $modCompraItem = CompraItem::find($item->compra_item_id);
        $modcatMatSerItem = Catmatseritem::find($modCompraItem->catmatseritem_id);

        (!empty($modCompraItem->descricaodetalhada))
            ? $descricao = 'Item compra: '. $modCompraItem->numero . ' - ' .  $modCompraItem->descricaodetalhada
            : $descricao = 'Item compra: '. $modCompraItem->numero . ' - ' .  $modcatMatSerItem->descricao;

        return (strlen($descricao) < 1248) ? $descricao : mb_substr($descricao, 0, 1248);
    }

    public function populaTabelasSiafiAlteracao(): array
    {
        $retorno['resultado'] = false;
        $minuta_id = Route::current()->parameter('minuta_id');
        $remessa_id = Route::current()->parameter('remessa');

        $modMinutaEmpenho = MinutaEmpenho::find($minuta_id);
        $modRemessa = MinutaEmpenhoRemessa::find($remessa_id);

        DB::beginTransaction();
        try {
            $this->removeSfOrcEmpenhoDadosErroAndamento($modMinutaEmpenho, $modRemessa);

            $sforcempenhodadosalt = $this->gravaSfOrcEmpenhoDadosAlt($modMinutaEmpenho, $modRemessa);

            if ($modMinutaEmpenho->passivo_anterior) {
                $this->gravaSfPassivoAnterior($sforcempenhodadosalt, $modMinutaEmpenho, $modRemessa);
            }

            $vigencia = $this->gravaSfItensEmpenho($modMinutaEmpenho, $sforcempenhodadosalt, $remessa_id, 'alteracao');

            if (!$vigencia) {
                $retorno['msg'] = 'Não é possível continuar a emissão, porque existe(m) item(ns) não vigente(s). ' .
                    'Por favor, retornar a etapa 1.';
                return $retorno ;
            }

            $txt_motivo = $this->getTxtMotivo($modMinutaEmpenho);

            $this->gravaSfRegistroAlteracao($sforcempenhodadosalt, $modMinutaEmpenho->data_emissao, $txt_motivo);

            $this->gravaRemessa($modRemessa);

            if ($modRemessa->alteracao_fonte_minutaempenho_id !== null) {
                $request = new Request();
                $request->merge([
                    'alteracao_fonte_minutaempenho_id' => $modRemessa->alteracao_fonte_minutaempenho_id,
                ]);
                $this->populaTabelasSiafi($request);
            }

            session(['situacao' => 'EM PROCESSAMENTO']);
            DB::commit();
            $retorno['resultado'] = true;
        } catch (Exception $exc) {
            $this->inserirLogCustomizado('emissao-minuta-empenho', 'error', $exc);
            DB::rollback();
        }

        return $retorno;
    }

    public function gravaSfOrcEmpenhoDadosAlt(MinutaEmpenho $modMinutaEmpenho, MinutaEmpenhoRemessa $modRemessa)
    {
        $modSfOrcEmpenhoDados = new SfOrcEmpenhoDados();

        $ugemitente = Unidade::find($modMinutaEmpenho->saldo_contabil->unidade_id);

        $modSfOrcEmpenhoDados->minutaempenho_id = $modMinutaEmpenho->id;
        $modSfOrcEmpenhoDados->ugemitente = $ugemitente->codigo;
        $modSfOrcEmpenhoDados->anoempenho = (int)config('app.ano_minuta_empenho');
        $modSfOrcEmpenhoDados->numempenho = (int)substr($modMinutaEmpenho->mensagem_siafi, 6, 6);
        $modSfOrcEmpenhoDados->txtlocalentrega = $modMinutaEmpenho->local_entrega;
        $modSfOrcEmpenhoDados->txtdescricao = $modMinutaEmpenho->descricao;

        $modSfOrcEmpenhoDados->situacao = 'EM PROCESSAMENTO';
        $modSfOrcEmpenhoDados->cpf_user = backpack_user()->cpf;
        $modSfOrcEmpenhoDados->alteracao = true;
        $modSfOrcEmpenhoDados->minutaempenhos_remessa_id = $modRemessa->id;
        $modSfOrcEmpenhoDados->sfnonce = $modRemessa->sfnonce;
        $modSfOrcEmpenhoDados->save();

        return $modSfOrcEmpenhoDados;
    }

    public function gravaSfRegistroAlteracao(SfOrcEmpenhoDados $sforcempenhodados, string $dtemis, string $txtmotivo)
    {
        $sfRegistroAlteracao = new SfRegistroAlteracao();
        $sfRegistroAlteracao->sforcempenhodado_id = $sforcempenhodados->id;
        $sfRegistroAlteracao->dtemis = date('Y-m-d');
        $sfRegistroAlteracao->txtmotivo = $txtmotivo;
        $sfRegistroAlteracao->save();
    }

    public function gravaRemessa(MinutaEmpenhoRemessa $modRemessa)
    {
        $situacao = Codigoitem::wherehas('codigo', function ($q) {
            $q->where('descricao', '=', 'Situações Minuta Empenho');
        })
            ->where('descricao', 'EM PROCESSAMENTO')
            ->first();
        $modRemessa->situacao_id = $situacao->id;
        $modRemessa->etapa = 3;
        $modRemessa->save();
    }

    public function gravaRemessaOriginal(MinutaEmpenhoRemessa $modRemessa)
    {
        $situacao = Codigoitem::wherehas('codigo', function ($q) {
            $q->where('descricao', '=', 'Situações Minuta Empenho');
        })
            ->where('descricao', 'EM PROCESSAMENTO')
            ->first();
        $modRemessa->situacao_id = $situacao->id;
        $modRemessa->save();
    }

    private function getTxtMotivo($modMinutaEmpenho)
    {

        $tipo = $modMinutaEmpenho->empenho_por;

        if ($tipo === 'Compra' || $tipo === 'Suprimento') {
            $data_emissao = Carbon::createFromFormat('Y-m-d', $modMinutaEmpenho->data_emissao)->format('d/m/Y');

            return "REGISTRO DE ANULAÇÃO/REFORÇO/CANCELAMENTO DO EMPENHO N° $modMinutaEmpenho->mensagem_siafi " .
                "EMITIDO EM $data_emissao COMPRA: $modMinutaEmpenho->informacao_complementar.";
        }

        if ($tipo === 'Contrato') {
            DB::enableQueryLog();
            $data_emissao = Carbon::createFromFormat('Y-m-d', $modMinutaEmpenho->data_emissao)->format('d/m/Y');

            $ugOrigemContrato = $modMinutaEmpenho->contrato_vinculado->unidadeorigem->codigo; //429
            //$ugOrigemContrato = $modMinutaEmpenho->contrato_vinculado->unidadeorigem->codigosiafi;
            $tipoContrato = $modMinutaEmpenho->contrato_vinculado->tipo->descres;
            $numeroAno = implode('', explode('/', $modMinutaEmpenho->contrato_vinculado->numero));

            return "REGISTRO DE ANULAÇÃO/REFORÇO/CANCELAMENTO DO EMPENHO N° $modMinutaEmpenho->mensagem_siafi " .
                "EMITIDO EM $data_emissao CONTRATO: $ugOrigemContrato$tipoContrato$numeroAno.";
        }
    }

    public function removeSfOrcEmpenhoDadosErroAndamento(
        MinutaEmpenho $modMinutaEmpenho,
        MinutaEmpenhoRemessa $modRemessa
    ) {
        return SfOrcEmpenhoDados::where('minutaempenho_id', $modMinutaEmpenho->id)
            ->where('minutaempenhos_remessa_id', $modRemessa->id)
            ->whereIn('situacao', ['ERRO', 'EM ANDAMENTO'])->forceDelete();
    }

    public function atualizaCreditoOrcamentario(Request $request)
    {
        $minuta_id = Route::current()->parameter('minuta_id');
        $modMinutaEmpenho = MinutaEmpenho::find($minuta_id);
        $modSaldoContabil = SaldoContabil::find($modMinutaEmpenho->saldo_contabil_id);
        return $modSaldoContabil->saldo;
    }

    public function verificarSeAnoEmpenhoCorrespondeAoAnoAtual($numeroEmpenho)
    {
        $anoEmpenho = substr($numeroEmpenho, 0, 4);
        $dataHoje = date("Y-m-d");
        $anoHoje = substr($dataHoje, 0, 4);
        if ($anoEmpenho == $anoHoje) {
            return true;
        }
        return false;
    }

    /**
     * @OA\Get(
     *     tags={"Empenhos"},
     *     summary="Retorna uma lista de minutas que têm força de contrato, pela uasg e pelo número do empenho",
     *     description="",
     *     path="/api/v1/contrato/empenhoporuasg/{codigo_unidade_emitente}/{numero_empenho}",
     *     security={ {"bearerAuth": {} }},
     *
     *     @OA\Parameter(
     *         name="codigo_unidade_emitente",
     *         in="path",
     *         description="Codigo da UASG emitente",
     *         example="167245",
     *         required=true,
     *         @OA\Schema(
     *              type="integer"
     *        )
     *     ),
     *     @OA\Parameter(
     *         name="numero_empenho",
     *         in="path",
     *         description="Número do empenho",
     *         example="2021NE000042",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *        )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Lista de minutas com força de contrato retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/Minutas")
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/FalhaAutenticação")
     *      ),
     *
     * )
     */
     public function minutasPorUasgPorNumeroEmpenho($codigoUnidadeEmitente, $numeroEmpenho)
     {
         /*
            Códigos de status de respostas HTTP:
                Respostas de informação (100-199),
                Respostas de sucesso (200-299),
                Redirecionamentos (300-399)
                Erros do cliente (400-499)
                Erros do servidor (500-599).

           Códigos utilizados pela API:

           100 - informação ao cliente
           200 - sucesso
           400 - erro do cliente
           401 - erro do cliente
           500 - erro do servidor

        */
        $status = 200;
        $mensagem = 'Sucesso';
        $arrayMinutas = [];
        // vamos verificar se algum campo chegou em branco
        if ($codigoUnidadeEmitente == null || $codigoUnidadeEmitente == '') {
            $status = 400;
            $mensagem = 'Código da Unidade Emitente chegou vazio.';
        }
        if ($numeroEmpenho == null || $numeroEmpenho == '') {
            $status = 400;
            $mensagem = 'Número do Empenho chegou vazio.';
        }

        // vamos verificar se o status continua 200 e caso positivo, vamos continuar
        if ($status == 200) {

            /**
             * O trecho abaixo foi inserido, para que os saldos do empenho sejam atualizados
             * antes do retorno.
             */
            $empenhoCrudController = new EmpenhoCrudController();
            $empenhoCrudController->atualizaSaldosEmpenho($codigoUnidadeEmitente, $numeroEmpenho);

            $minuta = new MinutaEmpenho();
            $minutas = $minuta->getTodosEmpenhosComForcaDeContratoByUasgByNumeroEmpenho($codigoUnidadeEmitente, $numeroEmpenho);
            if (!is_array($minutas)) {
                $status = 500;
                $mensagem = 'Erro ao buscar as minutas.';
            } elseif (count($minutas) == 0) {
                // por enquanto a mensagem é essa abaixo. Se a minuta for de contrato, alteraremos
                $status = 100;
                $mensagem = 'Nenhuma minuta encontrada.';
                // vamos verificar se essa minuta é de contrato
                if ($minuta->verificarSeMinutaEDeContrato($codigoUnidadeEmitente, $numeroEmpenho)) {
                    $status = 100;
                    $mensagem = 'Empenho do tipo contrato.';
                }
            } else {
                foreach ($minutas as $minuta) {
                    $arrayMinutas[] = [
                        'minutaempenho_id' => @$minuta->minutaempenho_id,
                        'minutaempenho_data_criacao' => @$minuta->minutaempenho_data_criacao,
                        'minutaempenho_tipo_empenho' => @$minuta->minutaempenho_tipo_empenhopor,
                        'minutaempenho_unidade_id' => @$minuta->minutaempenho_unidade_id,
                        'minutaempenho_mensagem_siafi' => @$minuta->minutaempenho_mensagem_siafi,
                        'contrato_id' => $minuta->contrato_id,
                        'compra_id' => @$minuta->compra_id,
                        'numero_empenho' => @$minuta->numero_empenho,
                        'orgao_nome' => @$minuta->orgao_nome,
                        'orgao_codigo' => @$minuta->orgao_codigo,
                        'unidade_id' => @$minuta->unidade_id,
                        'unidade_codigo' => @$minuta->unidade_codigo,
                        'unidade_nomeresumido' => @$minuta->unidade_nomeresumido,
                        'unidade_nomecompleto' => @$minuta->unidade_nomecompleto,
                        'fornecedor_compra_nome' => @$minuta->fornecedor_compra_nome,
                        'fornecedor_compra_cpf_cnpj_idgener' => @$minuta->fornecedor_compra_cpf_cnpj_idgener,
                        'fornecedor_empenho_nome' => @$minuta->fornecedor_empenho_nome,
                        'fornecedor_empenho_cpf_cnpj_idgener' => @$minuta->fornecedor_empenho_cpf_cnpj_idgener,
                        'empenho_numero' => @$minuta->empenho_numero,
                        'empenho_valor_empenhado' => number_format(@$minuta->empenho_valor_empenhado, 2, ',', '.'),
                        'empenho_valor_a_liquidar' => number_format(@$minuta->empenho_valor_a_liquidar, 2, ',', '.'),
                        'empenho_valor_liquidado' => number_format(@$minuta->empenho_valor_liquidado, 2, ',', '.'),
                        'empenho_valor_pago' => number_format(@$minuta->empenho_valor_pago, 2, ',', '.'),
                        'empenho_valor_rpinscrito' => number_format(@$minuta->empenho_valor_rpinscrito, 2, ',', '.'),
                        'empenho_valor_rpaliquidar' => number_format(@$minuta->empenho_valor_rpaliquidar, 2, ',', '.'),
                        'empenho_valor_rpliquidado' => number_format(@$minuta->empenho_valor_rpliquidado, 2, ',', '.'),
                        'empenho_valor_rppago' => number_format(@$minuta->empenho_valor_rppago, 2, ',', '.'),
                        'empenho_unidade_id' => @$minuta->empenho_unidade_id,
                        'empenho_id' => @$minuta->empenho_id,
                        'naturezadespesa_codigo' => @$minuta->naturezadespesa_codigo,
                        'naturezadespesa_descricao' => @$minuta->naturezadespesa_descricao,
                        'contrato_numero' => @$minuta->contrato_numero,
                        'uasg_compra' => @$minuta->compra->unidade_origem->codigo,
                        'uasg_compra_subrogada' => @$minuta->compra->unidade_subrrogada->codigo,
                        'numero_compra' => @$minuta->compra->numero_ano,
                        'tipo_compra' => @$minuta->compra->modalidade->descricao,
                    ];
                }
            }
        }

        // Vamos preparar o array de resposta
        $response = [
           'status' => $status,
           'message' => $mensagem,
           'minutas' => $arrayMinutas,
        ];
        return response()->json($response);
    }

    /**
     * @OA\Get(
     *     tags={"Empenhos"},
     *     summary="Retorna todas as minutas de empenho de uma UASG em um determinado Ano.",
     *     description="",
     *     path="/api/v1/minuta/uasg/{uasg_da_minuta}/ano/{ano}",
     *     security={ {"bearerAuth": {} }},
     *
     *     @OA\Parameter(
     *         name="uasg_da_minuta",
     *         in="path",
     *         description="Código da UASG",
     *         example="201053",
     *         required=true,
     *         @OA\Schema(
     *            type="integer"
     *        ),
     *     ),
     *     @OA\Parameter(
     *         name="ano",
     *         in="path",
     *         description="Ano da minuta (4 dígitos)",
     *         example="2022",
     *         required=true,
     *         @OA\Schema(
     *                 type="integer",
     *                 minLength=4,
     *                 maxLength=4,
     *         ),
     *     ),
     *
     *      @OA\Parameter(
     *         name="dt_alteracao_min",
     *         in="query",
     *         description="Data de alteração inicial (formato ISO 8601-1:2019)",
     *         example="2022-03-18T08:00:00Z",
     *         required=false,
     *         @OA\Schema(
     *              type="string"
     *        )
     *     ),
     *
     *      @OA\Parameter(
     *         name="dt_alteracao_max",
     *         in="query",
     *         description="Data de alteração final (formato ISO 8601-1:2019)",
     *         example="2022-04-18T18:00:00Z",
     *         required=false,
     *         @OA\Schema(
     *              type="string"
     *        )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="Minutas de empenho retornadas com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/MinutasUasgAno")
     *         ),
     *
     *      @OA\Response(
     *         response=401,
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/FalhaAutenticação")
     *        ),
     *
     *   )
     * )
     */
    public function minutasPorUasgPorAno($uasg, $ano, Request $request)
    {

        //Validação dos parâmetros "REST"
        //$uasg deve ser um valor inteiro de 6 dígitos.
        //$ano deve ser um valor inteiro de 4 dígitos.
        if (!ctype_digit($uasg) or strlen($uasg) != 6) {
            $response = array("error 400" => "Código da Uasg inválido.");
        } elseif (!ctype_digit($ano) or strlen($ano) != 4) {
            $response = array("error 400" => "Número do Ano inválido.");
        } else {
           //Validação dos parâmetros "RPC" opcionais
            $obj_APIController = new APIController();
            $obj_APIController->range($request->dt_alteracao_min, $request->dt_alteracao_max);

           //Retorna os dados de Minuta
            $minuta = new MinutaEmpenho();
            $minutas = $minuta->buscaMinutasEmpenhosPorUasgEAno($uasg, $ano, $request->dt_alteracao_min, $request->dt_alteracao_max);

            //Cabeçalho da Minuta
            $minuta_array = [];
            $idx = 0;

            //Itens da Minuta
            $itens_minuta_array = [];

            foreach ($minutas as $minuta) {
             //Cabeçalho da Minuta
                $minuta_array[] = $minuta;

             //Tratamento dos itens da Minuta
             //Minutas do tipo Contrato
                if ($minuta->tipo_minuta == 'Contrato') {
                      //Busca os itens da minuta
                      $itens_minuta_contrato =  (new ContratoItemMinutaEmpenho())->retornaItensMinuta($minuta->id_minuta);

                      //Filtra os itens únicos (não duplicados)
                      $itens_distintos =  $itens_minuta_contrato->unique('contrato_item_id');

                      //Para cada item, calcula os saldos por tipo de operação.
                    foreach ($itens_distintos as $item) {
                        $lista_por_item = $itens_minuta_contrato->where('contrato_item_id', $item->contrato_item_id);

                     //Nas operações de INCLUSAO do item, soma a Quantidade e o Valor.
                        $soma_quantidade_item_inclusao = $lista_por_item->where('tipo_operacao', 'INCLUSAO')->sum('quantidade');
                        $soma_valor_item_inclusao = $lista_por_item->where('tipo_operacao', 'INCLUSAO')->sum('valor');

                     //Nas operações de REFORÇO do item, soma a Quantidade e o Valor.
                        $soma_quantidade_item_reforco = 0; // $lista_por_item->where('tipo_operacao', 'REFORCO')->sum('quantidade');
                        $soma_valor_item_reforco = 0; //$lista_por_item->where('tipo_operacao', 'REFORCO')->sum('valor');

                     //Nas operações de ANULACAO do item, soma a Quantidade e o Valor.
                        $soma_quantidade_item_anulacao = 0; // $lista_por_item->where('tipo_operacao', 'ANULACAO')->sum('quantidade');
                        $soma_valor_item_anulacao = 0; //$lista_por_item->where('tipo_operacao', 'ANULACAO')->sum('valor');

                        $saldo_quantidade_item = $soma_quantidade_item_inclusao + $soma_quantidade_item_reforco + $soma_quantidade_item_anulacao;
                        $saldo_valor_total_item = $soma_valor_item_inclusao + $soma_valor_item_reforco + $soma_valor_item_anulacao;

                        $itens_minuta_array[] = $item->itemAPI($saldo_quantidade_item, $saldo_valor_total_item);
                    }

                  //Minutas do tipo Compra ou Suprimento
                } else {
                     //Busca os itens da minuta
                     $itens_minuta_compra =  (new CompraItemMinutaEmpenho())->retornaItensMinuta($minuta->id_minuta);

                     //Aplica ordernacao dos itens pelo sequencial_siafi
                     $itens_minuta_compra = $itens_minuta_compra->sortBy('sequencial_siafi');

                     //Filtra os itens únicos (não duplicados)
                     $itens_distintos =  $itens_minuta_compra->unique('compra_item_id');

                     //Para cada item, calcula os saldos por tipo de operação.
                    foreach ($itens_distintos as $item) {
                        $lista_por_item = $itens_minuta_compra->where('compra_item_id', $item->compra_item_id);

                     //Nas operações de INCLUSAO do item, soma a Quantidade e o Valor.
                        $soma_quantidade_item_inclusao = $lista_por_item->where('tipo_operacao', 'INCLUSAO')->sum('quantidade');
                        $soma_valor_item_inclusao = $lista_por_item->where('tipo_operacao', 'INCLUSAO')->sum('valor');

                     //Nas operações de REFORÇO do item, soma a Quantidade e o Valor.
                        $soma_quantidade_item_reforco = 0; // $lista_por_item->where('tipo_operacao', 'REFORCO')->sum('quantidade');
                        $soma_valor_item_reforco = 0; //$lista_por_item->where('tipo_operacao', 'REFORCO')->sum('valor');

                     //Nas operações de ANULACAO do item, soma a Quantidade e o Valor.
                        $soma_quantidade_item_anulacao = 0; // $lista_por_item->where('tipo_operacao', 'ANULACAO')->sum('quantidade');
                        $soma_valor_item_anulacao = 0; //$lista_por_item->where('tipo_operacao', 'ANULACAO')->sum('valor');

                        $saldo_quantidade_item = $soma_quantidade_item_inclusao + $soma_quantidade_item_reforco + $soma_quantidade_item_anulacao;
                        $saldo_valor_total_item = $soma_valor_item_inclusao + $soma_valor_item_reforco + $soma_valor_item_anulacao;

                        $itens_minuta_array[] = $item->itemAPI($saldo_quantidade_item, $saldo_valor_total_item);
                    }
                }

             //Adiciona os itens ao cabeçalho da minuta
                $minuta_array[$idx++]['itens_minuta'] = $itens_minuta_array;

             //Seta o array dos itens da minuta
                $itens_minuta_array = [];
            }
           $response = $minuta_array;

        }

        return json_encode($response);
    }

    /**
     * enviar minutas aguardando anulação cujas remessas já foram enviadas
     * @return void
     */
    public function populaTabelasSiafiAnulacaoKernel()
    {
        $situacaoAguardandoAnulacao = $this->retornaIdCodigoItem('Situações Minuta Empenho', 'AGUARDANDO ANULAÇÃO');

        $situacaoEmpenhoEmitido = $this->retornaIdCodigoItem('Situações Minuta Empenho', 'EMPENHO EMITIDO');

        $minutas = Minutaempenho::select(
            'minutaempenhos.id',
            'minutaempenhos.updated_at',
            'minutaempenhos.informacao_complementar',
            'minutaempenhos_remessa.minutaempenho_id',
            'minutaempenhos_remessa.updated_at',
            'sforcempenhodados.cpf_user'
        )
            ->distinct()
            ->join('minutaempenhos_remessa', 'minutaempenhos.id', '=', 'minutaempenhos_remessa.alteracao_fonte_minutaempenho_id')
            ->join('sforcempenhodados', 'minutaempenhos_remessa.id', '=', 'sforcempenhodados.minutaempenhos_remessa_id')
            ->where('minutaempenhos.situacao_id', '=', $situacaoAguardandoAnulacao)
            ->where('minutaempenhos_remessa.situacao_id', '=', $situacaoEmpenhoEmitido)
            ->get();

        foreach ($minutas as $minuta) {
            $request = new Request();
            $request->merge([
                'minuta_id' => $minuta->id,
                'cpf_user'  => $minuta->cpf_user
            ]);
            $this->populaTabelasSiafi($request);
        }
    }

    private function buscarCompra($modalidade, $uasgCompra, $uasgUser, $uasgBeneficiaria, $numero_compra, $ano_compra)
    {
        $retornoSiasg  = null;
        if ($ano_compra == '2023') {
            //primeiro consulta siasg
            $retornoSiasg = $this->consultaCompraSiasg(
                $modalidade,
                $uasgCompra,
                $uasgUser,
                $uasgBeneficiaria,
                $numero_compra . $ano_compra
            );
            if (!(is_null($retornoSiasg->data))) {
                return $retornoSiasg;
            }

            $consulta_ndc = NDC::consulta(
                'contratacao',
                [
                    'numeroCompra' => $numero_compra,
                    'anoCompra' => $ano_compra,
                    'uasgCompra' => $uasgCompra
                ]
            );

            if ($consulta_ndc->codigoRetorno == 200) {
                return $consulta_ndc;
            }
        }

        if ($ano_compra > '2023') {
            //primeiro consulta NDC
            $consulta_ndc = NDC::consulta(
                'contratacao',
                [
                    'numeroCompra' => $numero_compra,
                    'anoCompra' => $ano_compra,
                    'uasgCompra' => $uasgCompra
                ]
            );

            if ($consulta_ndc->codigoRetorno == 200) {
                return $consulta_ndc;
            }

            $retornoSiasg = $this->consultaCompraSiasg(
                $modalidade,
                $uasgCompra,
                $uasgUser,
                $uasgBeneficiaria,
                $numero_compra . $ano_compra
            );
            if (!(is_null($retornoSiasg->data))) {
                return $retornoSiasg;
            }
        }

        if ($ano_compra < '2023') {
            //primeiro consulta SIASG
            $retornoSiasg = $this->consultaCompraSiasg(
                $modalidade,
                $uasgCompra,
                $uasgUser,
                $uasgBeneficiaria,
                $numero_compra . $ano_compra
            );
            if (!(is_null($retornoSiasg->data))) {
                return $retornoSiasg;
            }
        }
    }

    private function verificaSaldo($item, $tipo, $modMinutaEmpenho)
    {
        if ($item->operacao === 'REFORÇO') {
            $exercicio_atual = $modMinutaEmpenho->exercicio_atual_ou_msg_null;
            if ($tipo === 'Compra' && $exercicio_atual) {
                $ciu = $item->compra_item_unidade;

                $saldo = $this->retornaSaldoAtualizado(
                    $item->compra_item_id,
                    $modMinutaEmpenho->unidade_id,
                    $ciu->id
                );

                $ciu->quantidade_saldo = $saldo->saldo;
                $ciu->save();

                if ($saldo->saldo < 0) {
                    throw new \Exception(
                        'O item: ' . $item->compra_item->numero .
                        ' não possui saldo de compra suficiente para esta minuta. Minuta: ' . $modMinutaEmpenho->id
                    );
                }
            }
        }
    }
}

