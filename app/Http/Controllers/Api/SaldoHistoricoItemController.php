<?php

namespace App\Http\Controllers\Api;

use App\Models\Saldohistoricoitem;
use Route;
use App\Http\Controllers\Controller;
use App\Models\Contratofatura;
use App\Models\Codigoitem;
use App\Models\Contratohistorico;
use App\Models\Contrato;
use App\Models\Unidade;
use App\Http\Traits\ConsultaCompra;


class SaldoHistoricoItemController extends Controller
{

    use ConsultaCompra;

    public const SISPP = 1;
    public const SISRP = 2;

    /* Metodo para retonar os itens contrato item
     * utilizado para listar os items em: Termo aditivo e termo de apostilamento
     *
     * return array contratoitens
     */
    public function retonaSaldoHistoricoItens($id)
    {
        #variável de controle de situação do compra_itens
        $situacao = 1;

        $arrayIdCodigoItem = Codigoitem::select('codigoitens.id')
        ->join('codigos', 'codigos.id', '=', 'codigoitens.codigo_id')
        ->whereNull('codigos.deleted_at')
        ->where('codigoitens.descricao','Não se Aplica')
        ->where('codigos.descricao','Modalidade Licitação')
        ->pluck('codigoitens.id')->toArray();
        $contratoHistorico = Contratohistorico::where('id', $id)->first();

        #se for = 0, quer dizer que não tem itens ativos para compra neste contrato
        #então busca os itens inativos
        if($this->contaItensContrato($id) == 0){
            $situacao = 0;
        }

        $sql = Saldohistoricoitem::where('saldoable_id', $id)
            ->select(
                'saldohistoricoitens.id',
                'codigoitens.descricao',
                'catmatseritens.codigo_siasg',
                \DB::raw('upper(catmatseritens.descricao) as descricao_resumida'),
                \DB::raw('upper(contratoitens.descricao_complementar) as descricao_complementar'),
                'saldohistoricoitens.quantidade',
                'saldohistoricoitens.valorunitario',
                'saldohistoricoitens.valortotal',
                'saldohistoricoitens.periodicidade',
                'saldohistoricoitens.data_inicio',
                'saldohistoricoitens.tipo_material',
                'contratoitens.catmatseritem_id',
                'contratoitens.tipo_id as tipo_item_id',
                'contratoitens.numero_item_compra as numero',
                \DB::raw('upper(ci.descricaodetalhada) as descricaodetalhada'),
                'contratos.id as contrato_id',
                'modalidade.descres as modalidade',
                'tipo_compra.descricao as tipo_compra_descricao',
                'comp.numero_ano',
                'unidade_compra.codigo as uasgCompra',
                'unidade_beneficiaria.codigo as uasgBeneficiaria',
                'codigo_ncmnbs'
            )
            ->leftJoin('contratoitens', 'saldohistoricoitens.contratoitem_id', '=', 'contratoitens.id')
            ->leftJoin('codigoitens', 'codigoitens.id', '=', 'contratoitens.tipo_id')
            ->leftJoin('catmatseritens', 'catmatseritens.id', '=', 'contratoitens.catmatseritem_id')
            ->leftjoin('contratos', 'contratos.id', '=', 'contratoitens.contrato_id')
            ->leftJoin('unidades as unidade_compra', 'unidade_compra.id', '=', 'contratos.unidadecompra_id')
            ->leftJoin('unidades as unidade_beneficiaria', 'unidade_beneficiaria.id', '=', 'contratos.unidadebeneficiaria_id')
            ->leftjoin('compras as comp', function ($join) {
                $join->on('comp.numero_ano', '=', 'contratos.licitacao_numero')
                    ->on('comp.modalidade_id', '=', 'contratos.modalidade_id')
                    ->on('comp.unidade_origem_id', '=', 'contratos.unidadecompra_id');
            })
            ->leftJoin('codigoitens as modalidade', 'modalidade.id', '=', 'comp.modalidade_id')
            ->leftJoin('codigoitens as tipo_compra', 'tipo_compra.id', '=', 'comp.tipo_compra_id')
            ->leftjoin('compra_items as ci', function ($join) {
                $join->on('ci.compra_id', '=', 'comp.id')
                    ->on('ci.numero', '=', 'contratoitens.numero_item_compra')
                    ->on('ci.catmatseritem_id', '=', 'catmatseritens.id')
                    ->on('ci.tipo_item_id', '=', 'contratoitens.tipo_id');
            })
            ->leftjoin('compra_item_fornecedor','compra_item_fornecedor.compra_item_id', '=', 'ci.id')
            ->leftjoin('minutaempenhos', 'minutaempenhos.fornecedor_compra_id', '=', 'compra_item_fornecedor.fornecedor_id')
            ->leftjoin('compra_item_unidade', function($join) {
                $join->on('compra_item_unidade.compra_item_id', '=', 'ci.id')
                    ->on('compra_item_unidade.unidade_id', '=', 'contratos.unidade_id');
            })
            ->whereNull('contratoitens.deleted_at')
            ->whereNull('comp.deleted_at')
            ->where(function($query) use ($situacao) {
                $query->where('compra_item_unidade.situacao', '=', "$situacao")
                    ->orWhere('compra_item_unidade.situacao', NULL);
            })
            ->where(function($query) {
                $query->where(\DB::raw('compra_item_unidade.fornecedor_id'), '=', \DB::raw('minutaempenhos.fornecedor_compra_id'))
                    ->orWhere('compra_item_unidade.fornecedor_id', NULL)
                    ->orWhere(function ($query) {
                        $query->whereNotNull('compra_item_unidade.fornecedor_id')
                            ->whereNull('minutaempenhos.fornecedor_compra_id');
                    });
            })
            ->orderBy("contratoitens.numero_item_compra","ASC")
            ->groupBy(
                'saldohistoricoitens.id',
                'codigoitens.descricao',
                'catmatseritens.codigo_siasg',
                'descricao_resumida',
                'contratoitens.descricao_complementar',
                'saldohistoricoitens.quantidade',
                'saldohistoricoitens.valorunitario',
                'saldohistoricoitens.valortotal',
                'saldohistoricoitens.periodicidade',
                'saldohistoricoitens.data_inicio',
                'saldohistoricoitens.tipo_material',
                'contratoitens.catmatseritem_id',
                'contratoitens.numero_item_compra',
                'contratoitens.tipo_id',
                'ci.descricaodetalhada',
                'contratos.id',
                'modalidade.descres',
                'tipo_compra.descricao',
                'comp.numero_ano',
                'unidade_compra.codigo',
                'unidade_beneficiaria.codigo',
                'codigo_ncmnbs',
            )->get()->toArray();

        if ($contratoHistorico != null) {
            if (!in_array($contratoHistorico->modalidade_id, $arrayIdCodigoItem)) {
                $qtdPorItens = array_count_values(array_column($sql , 'id'));
                $sqlPos = 0;
                foreach ($qtdPorItens as $qtd) {
                    if ($qtd > 1) {
                        $contrato = Contrato::find($sql[0]['contrato_id']);
                        $unidade_origem_id = $contrato->unidadeorigem_id;
                        $uasgUser = Unidade::find($unidade_origem_id)->codigo;
                        [$numero_compra, $ano_compra] = explode('/', $sql[0]['numero_ano']);

                        $retornoSiasg = $this->buscarCompra(
                            $sql[0]['modalidade'],
                            $sql[0]['uasgCompra'],
                            $uasgUser,
                            $sql[0]['uasgBeneficiaria'],
                            $numero_compra,
                            $ano_compra
                        );

                        if ($retornoSiasg->data) {
                            if ($retornoSiasg->data->compraSispp->tipoCompra == $this::SISPP)
                                $tipoCompra = 'SISPP';
                            elseif ($retornoSiasg->data->compraSispp->tipoCompra == $this::SISRP)
                                $tipoCompra = 'SISRP';

                            $sqlAgrupPorTipoCompra = array_column($sql, 'tipo_compra_descricao');

                            foreach ($sqlAgrupPorTipoCompra as $pos=>$tipo) {
                                if ($tipo != $tipoCompra)
                                    unset($sql[$pos]);
                            }

                            $item = [];
                            foreach ($sql as $dado)
                                $item[] = $dado;
                            $sql = $item;

                            return $sql;
                        } else {
                            unset($sql[$sqlPos]);
                            $item = [];
                            foreach ($sql as $dado)
                                $item[] = $dado;
                            $sql = $item;
                            return $sql;
                        }
                    }
                    $sqlPos++;
                }
            }
        }

        return $sql;
    }

    public function retonarItensTermos($id_contrato, $catmatseritem, $cod_siasg, $tipo, $numero) {
        $codItens = ['Termo Aditivo','Termo de Apostilamento'];

        $termos = Saldohistoricoitem::select('saldohistoricoitens.id')
        ->join('contratohistorico', 'contratohistorico.id', '=', 'saldohistoricoitens.saldoable_id')
        ->join('codigoitens', 'codigoitens.id', '=', 'contratohistorico.tipo_id')
        ->join('contratoitens', 'contratoitens.id', '=', 'saldohistoricoitens.contratoitem_id')
        ->join('catmatseritens', 'catmatseritens.id', '=', 'contratoitens.catmatseritem_id')
        ->whereIn('codigoitens.descricao', $codItens)
        ->where('contratohistorico.contrato_id', '=', $id_contrato)
        ->where('contratoitens.catmatseritem_id', '=', $catmatseritem)
        ->where('catmatseritens.codigo_siasg', '=', $cod_siasg)
        ->where('contratoitens.tipo_id', '=', $tipo)
        ->where('contratoitens.numero_item_compra', '=', $numero)

        ->whereNull('contratohistorico.deleted_at')
        ->whereNull('contratoitens.deleted_at')
        ->get()
        ->toArray();


        $fatura = Contratofatura::where('contratofaturas.contrato_id', $id_contrato)
        ->select('saldohistoricoitens.id')
        ->join('contratofaturas_itens', 'contratofaturas.id', '=', 'contratofaturas_itens.contratofaturas_id')
        ->join('saldohistoricoitens', 'contratofaturas_itens.saldohistoricoitens_id', '=', 'saldohistoricoitens.id')
        ->join('contratohistorico', 'contratohistorico.id', '=', 'saldohistoricoitens.saldoable_id')
        ->join('contratoitens', 'contratoitens.id', '=', 'saldohistoricoitens.contratoitem_id')
        ->join('catmatseritens', 'catmatseritens.id', '=', 'contratoitens.catmatseritem_id')
        ->where('contratohistorico.contrato_id', '=', $id_contrato)
        ->where('contratoitens.catmatseritem_id', '=', $catmatseritem)
        ->where('catmatseritens.codigo_siasg', '=', $cod_siasg)
        ->where('contratoitens.tipo_id', '=', $tipo)
        ->where('contratoitens.numero_item_compra', '=', $numero)
        ->whereNull('contratofaturas_itens.deleted_at')
        ->whereNull('saldohistoricoitens.deleted_at')
        ->whereNull('contratohistorico.deleted_at')
        ->whereNull('contratoitens.deleted_at')
        ->get()
        ->toArray();

        $arr = array_merge($termos, $fatura);

        return $arr;
    }

    /**
     * Conta os istens da compra do contrato para saber se utiliza itens ativos ou inativos
     * @return int
     */
    private function contaItensContrato($id) : int{
        $countItens = Saldohistoricoitem::leftJoin('contratoitens', 'saldohistoricoitens.contratoitem_id', '=', 'contratoitens.id')
            ->leftJoin('codigoitens', 'codigoitens.id', '=', 'contratoitens.tipo_id')
            ->leftJoin('catmatseritens', 'catmatseritens.id', '=', 'contratoitens.catmatseritem_id')
            ->leftJoin('contratos', 'contratos.id', '=', 'contratoitens.contrato_id')
            ->leftJoin('unidades as unidade_compra', 'unidade_compra.id', '=', 'contratos.unidadecompra_id')
            ->leftJoin('unidades as unidade_beneficiaria', 'unidade_beneficiaria.id', '=', 'contratos.unidadebeneficiaria_id')
            ->leftJoin('compras as comp', function ($join) {
                $join->on('comp.numero_ano', '=', 'contratos.licitacao_numero')
                    ->on('comp.modalidade_id', '=', 'contratos.modalidade_id')
                    ->on('comp.unidade_origem_id', '=', 'contratos.unidadecompra_id');
            })
            ->leftJoin('codigoitens as modalidade', 'modalidade.id', '=', 'comp.modalidade_id')
            ->leftJoin('codigoitens as tipo_compra', 'tipo_compra.id', '=', 'comp.tipo_compra_id')
            ->leftJoin('compra_items as ci', function ($join) {
                $join->on('ci.compra_id', '=', 'comp.id')
                    ->on('ci.numero', '=', 'contratoitens.numero_item_compra')
                    ->on('ci.catmatseritem_id', '=', 'catmatseritens.id')
                    ->on('ci.tipo_item_id', '=', 'contratoitens.tipo_id');
            })
            ->leftJoin('compra_item_unidade', function ($join) {
                $join->on('compra_item_unidade.compra_item_id', '=', 'ci.id')
                    ->on('compra_item_unidade.unidade_id', '=', 'contratos.unidade_id');
            })
            ->where('saldoable_id', $id)
            ->whereNull('contratoitens.deleted_at')
            ->whereNull('comp.deleted_at')
            ->whereNull('saldohistoricoitens.deleted_at')
            ->where(function ($query) {
                $query->where('compra_item_unidade.situacao', '1')
                    ->orWhereNull('compra_item_unidade.situacao');
            })
            ->distinct('saldohistoricoitens.id')
            ->count('saldohistoricoitens.id');

        return $countItens;
    }
}
