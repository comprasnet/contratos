<?php

namespace App\Http\Controllers\Api;

use App\Models\Orgao;
use App\Models\Unidade;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AmparoLegalRestricaoCodigo;
use App\Models\Codigoitem;
use App\Models\Naturezadespesa;

class AmparoLegalRestricaoController extends Controller
{
    public function index(Request $request)
    {
        $search_term = $request->input('q');
        $form = collect($request->input('form'))->pluck('value', 'name');
        $tipoRestricaoId = $form['tipo_restricao_id'];
        $descres = Codigoitem::find($tipoRestricaoId)->descres;

        switch ($descres) {
            case 'ADMIN':
                return $this->showTipoAdministracao($search_term);
                break;

            case 'UGEMITENTE':
            case 'UGFAVOR':
                return $this->showUnidade($search_term);
                break;

            case 'AGEXEC':
                return null;
                break;

            case 'NATDESP':
                return $this->showNaturezaDespesa($search_term);
                break;

            default:
                # code...
                break;
        }
    }

    public function show($id)
    {
        return AmparoLegalRestricaoCodigo::find($id);
    }

    private function showUnidade($search_term)
    {
        preg_match('/^[0-9]*$/', $search_term, $verificaApenasNumero);
        if ($verificaApenasNumero) {
            return Orgao::where('codigo', $search_term)
                ->select(['codigo as id', 'nome as nome'])
                ->paginate(10);
        }
        return Orgao::whereRaw("lower(nome) like '%$search_term%'")
            ->select(['codigo as id', 'nome as nome'])
             ->paginate(10);
    }

    private function showTipoAdministracao($search_term)
    {
        preg_match('/^[0-9]*$/', $search_term, $verificaApenasNumero);
        $codigoItem =  Codigoitem::with(['codigo'])
            ->select(['descres as id', 'descricao as nome'])
            ->whereHas('codigo', function ($q) {
                return $q->where('descricao', 'Tipo Administração');
            });
        if ($verificaApenasNumero) {
            return $codigoItem
                ->where('descres', $search_term)
                ->paginate(10);
        }
        return $codigoItem->whereRaw("lower(descricao) like '%$search_term%'")
             ->paginate(10);
    }

    private function showNaturezaDespesa($search_term)
    {
        preg_match('/^[0-9]*$/', $search_term, $verificaApenasNumero);
        $naturezaDespesa = Naturezadespesa::select(['codigo as id', 'descricao as nome']);
        if ($verificaApenasNumero) {
            return $naturezaDespesa->where('codigo', $search_term)
                ->paginate(10);
        }
        return $naturezaDespesa->whereRaw("lower(descricao) like '%$search_term%'")
             ->paginate(10);
    }
}
