<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\APIController;
use App\Http\Controllers\EstaleiroCEP;
use App\Http\Traits\ContratoFaturaTrait;
use App\Http\Traits\Formatador;
use App\Models\ContratoFaturaEmpenho;
use App\Models\Empenhos;
use App\Models\Justificativafatura;
use App\Models\Orgao;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use PhpParser\Node\Expr\BinaryOp\Concat;
use function foo\func;
use App\Models\Unidade;
use App\Models\Contrato;
use App\Models\Contratoitem;
use App\Models\Contratofatura;
use App\Models\Contratoarquivo;
use App\Models\Contratoempenho;
use App\Models\Contratogarantia;
use App\Models\Contratopreposto;
use App\Models\Contratohistorico;
use App\Models\Contratocronograma;
use App\Models\Contratoocorrencia;
use App\Models\Contratoresponsavel;
use App\Http\Controllers\Controller;
use App\Models\BackpackUser;
use App\Models\Contratoterceirizado;
use App\Models\Contratodespesaacessoria;
use App\Models\ContratoPublicacoes;
use OpenApi\Annotations as OA;
use App\Models\MinutaEmpenho;
use Illuminate\Support\Facades\Route;
use App\Models\Empenho;
use App\Models\Fornecedor;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Exceptions\EmptyCollectionException;
use Throwable;
use JWTAuth;

class ContratoController extends APIController
{
    use ContratoFaturaTrait;
    use Formatador;

    public function index(Request $request)
    {
        $search_term = $request->input('q');

        if ($search_term) {
            $userUgId = session()->get('user_ug_id');
            $validaCNPJ = $this->validaCNPJ($search_term);
            $validaCPF = $this->validaCPF($search_term);

            $contrato = Contrato::select(DB::raw("DISTINCT CONCAT(unidades.codigo, ' | ', contratos.numero, ' | '," .
                " fornecedores.cpf_cnpj_idgener, ' - ', fornecedores.nome) AS numero"), 'contratos.id')
                ->join('fornecedores', 'fornecedores.id', '=', 'contratos.fornecedor_id')
                ->join('unidades', 'unidades.id', '=', 'contratos.unidadeorigem_id')
                ->where('contratos.elaboracao', '=', false)
                ->where('contratos.situacao', true)
                ->whereNotNull('contratos.unidadecompra_id')
                ->where(function ($query) use ($search_term, $userUgId) {
                    $query->where('contratos.unidade_id', $userUgId)
                        ->orWhereIn('contratos.id', function ($query) use ($userUgId) {
                            $query->select('contrato_id')
                                ->from('contratounidadesdescentralizadas')
                                ->where('unidade_id', $userUgId);
                        });
                });

            if ($validaCNPJ || $validaCPF) {
                $search_term = preg_replace('/[^0-9]/', '', $search_term);
                $contrato = $contrato->whereRaw(
                    "REGEXP_REPLACE(fornecedores.cpf_cnpj_idgener, '[^0-9]', '', 'g') = ?"
                    , [$search_term]
                );
            } elseif (
                preg_match('/^ex\d{7}$/i', $search_term)
                || stripos($search_term, 'estrangeiro') !== false
            ) {
                //estrangeiro
                $contrato = $contrato->where(
                    'fornecedores.cpf_cnpj_idgener',
                    'ilike',
                    "%{$search_term}%"
                );
            } elseif (preg_match('/^\d{4}NE\d{6}$/i', $search_term)) {
                //contrato do tipo empenho
                $contrato = $contrato->where('contratos.numero', '=', $search_term);
            } elseif (preg_match('/[a-zA-Z]/', $search_term)) {
                $contrato = $contrato->where('fornecedores.nome', 'ilike', "%{$search_term}%");
            } else {
                $contrato = $contrato->where('contratos.numero', 'like', "%{$search_term}%");
            }

            return $contrato->paginate(20);
        }
    }

    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna uma lista de orgãos com contratos ativos",
     *     description="",
     *     path="/api/contrato/orgaos",
     *
     *     @OA\Response(
     *         response=200,
     *         description="Lista de orgãos com contratos ativos retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/OrgaoId")
     *         )
     *
     * )
     */
    public function orgaosComContratosAtivos()
    {
        return response()->json($this->buscaOrgaosComContratosAtivos());
    }

    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna todas as unidades com contratos ativos",
     *     description="",
     *     path="/api/contrato/unidades",
     *     @OA\Response(
     *         response=200,
     *         description="Lista de unidades com contratos ativos retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/Unidade")
     *         )
     * )
     */
    public function unidadesComContratosAtivos()
    {
        return response()->json($this->buscaUnidadesComContratosAtivos());
    }

    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna todos os cronogramas alterados em um determinado período.",
     *     description="",
     *     path="/api/v1/contrato/cronogramas",
     *     security={ {"bearerAuth": {} }},
     *
     *     @OA\Parameter(
     *         name="dt_alteracao_min",
     *         in="query",
     *         description="Data de alteração inicial (formato ISO 8601-1:2019)",
     *         example="2021-11-24T08:00:00Z",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *        )
     *     ),
     *
     *     @OA\Parameter(
     *         name="dt_alteracao_max",
     *         in="query",
     *         description="Data de alteração final (formato ISO 8601-1:2019)",
     *         example="2021-11-25T18:00:00Z",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *        )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="Cronogramas retornados com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/Cronograma")
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/FalhaAutenticação")
     *     ),
     *
     * )
     */
    public function cronogramas(Request $request)
    {
        $this->rangeObrigatorio($request->dt_alteracao_min, $request->dt_alteracao_max);

        $cronograma_array = [];
        $cronogramas = (new Contratocronograma())->buscaCronogramasAPI($this->range($request->dt_alteracao_min, $request->dt_alteracao_max));

        foreach ($cronogramas as $cronograma) {
            $cronograma_array[] = $cronograma->cronogramaAPI();
        }

        return response()->json($cronograma_array);
    }

    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna uma lista com todos os cronogramas do contrato",
     *     description="",
     *     path="/api/contrato/{contrato_id}/cronograma",
     *     @OA\Parameter(
     *         name="contrato_id",
     *         in="path",
     *         description="id do contrato",
     *         required=true,
     *          @OA\Schema(
     *              type="integer"
     *        )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Cronograma do contrato retornado com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/Cronograma")
     *         ),
     *     )
     * )
     */
    public function cronogramaPorContratoId(int $contrato_id, Request $request)
    {
        $cronograma_array = [];
        $cronogramas = (new Contratocronograma())->buscaCronogramasPorContratoIdAPI($contrato_id, $this->range($request->dt_alteracao_min, $request->dt_alteracao_max));

        foreach ($cronogramas as $cronograma) {
            $cronograma_array[] = $cronograma->cronogramaAPI();
        }

        return response()->json($cronograma_array);
    }

    /**
     * @OA\Get(
     *     tags={"Empenhos"},
     *     summary="Retorna todos os empenhos (do tipo contrato) alterados em um determinado período.",
     *     description="",
     *     path="/api/v1/contrato/empenhos",
     *     security={ {"bearerAuth": {} }},
     *
     *     @OA\Parameter(
     *         name="dt_alteracao_min",
     *         in="query",
     *         description="Data de alteração inicial (formato ISO 8601-1:2019)",
     *         example="2021-11-24T08:00:00Z",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *        )
     *     ),
     *
     *     @OA\Parameter(
     *         name="dt_alteracao_max",
     *         in="query",
     *         description="Data de alteração final (formato ISO 8601-1:2019)",
     *         example="2021-11-25T18:00:00Z",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *        )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="Lista de empenhos retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/EmpenhosId")
     *         ),
     *     @OA\Response(
     *         response=401,
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/FalhaAutenticação")
     *     ),
     * )
     */
    public function empenhosPorContratos(Request $request)
    {
        $this->rangeObrigatorio($request->dt_alteracao_min, $request->dt_alteracao_max);
        $empenhos_array = [];
        $emp = new Contratoempenho();
        $empenhos = $emp->buscaTodosEmpenhosContratosAtivos($this->range($request->dt_alteracao_min, $request->dt_alteracao_max));

        foreach ($empenhos as $e) {
            $empenhos_array[] = [
                'id' => @$e->empenho->id,
                'contrato_id' => $e->contrato->id,
                'numero' => @$e->empenho->numero,
                'credor' => @$e->fornecedor->cpf_cnpj_idgener . ' - ' . @$e->fornecedor->nome ?? '',
                'fonte_recurso' => @$e->empenho->fonte,
                'programa_trabalho' => @$e->empenho->programa_trabalho,
                'planointerno' => @$e->empenho->planointerno->codigo . ' - ' . @$e->empenho->planointerno->descricao ?? '',
                'naturezadespesa' => @$e->empenho->naturezadespesa->codigo . ' - ' . @$e->empenho->naturezadespesa->descricao,
                'empenhado' => number_format(@$e->empenho->empenhado, 2, ',', '.'),
                'aliquidar' => number_format(@$e->empenho->aliquidar, 2, ',', '.'),
                'liquidado' => number_format(@$e->empenho->liquidado, 2, ',', '.'),
                'pago' => number_format(@$e->empenho->pago, 2, ',', '.'),
                'rpinscrito' => number_format(@$e->empenho->rpinscrito, 2, ',', '.'),
                'rpaliquidar' => number_format(@$e->empenho->rpaliquidar, 2, ',', '.'),
                'rpliquidado' => number_format(@$e->empenho->rpliquidado, 2, ',', '.'),
                'rppago' => number_format(@$e->empenho->rppago, 2, ',', '.'),
                'informacao_complementar' => @$e->empenho->informacao_complementar,
                'sistema_origem' => @$e->empenho->sistema_origem,
            ];
        }

        return response()->json($empenhos_array);
    }

    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna uma lista com todos os empenhos do contrato",
     *     description="",
     *     path="/api/contrato/{contrato_id}/empenhos",
     *     @OA\Parameter(
     *         name="contrato_id",
     *         in="path",
     *         description="id do contrato",
     *         required=true,
     *          @OA\Schema(
     *              type="integer"
     *        )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Lista de empenhos do contrato retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/Empenhos")
     *     ),
     * )
     */
    public function empenhosPorContratoId(int $contrato_id, Request $request)
    {
        $empenhos_array = [];
        $empenhos = $this->buscaEmpenhosPorContratoId($contrato_id, $this->range($request->dt_alteracao_min, $request->dt_alteracao_max));

        foreach ($empenhos as $e) {

            $numeroEmpenho = @$e->empenho->unidade->codigo . @$e->empenho->unidade->gestao . @$e->empenho->numero;

            $empenhos_array[] = [
                'id' => $e->empenho->id,
                'unidade_gestora' => @$e->empenho->unidade->codigo,
                'gestao' => @$e->empenho->unidade->gestao,
                'numero' => @$e->empenho->numero,
                'data_emissao' => @$e->empenho->data_emissao,
                'credor' => @$e->fornecedor->cpf_cnpj_idgener . ' - ' . @$e->fornecedor->nome ?? '',
                'fonte_recurso' => @$e->empenho->fonte,
                'programa_trabalho' => @$e->empenho->programa_trabalho,
                'planointerno' => @$e->empenho->planointerno->codigo . ' - ' . @$e->empenho->planointerno->descricao ?? '',
                'naturezadespesa' => @$e->empenho->naturezadespesa->codigo . ' - ' . @$e->empenho->naturezadespesa->descricao,
                'empenhado' => number_format(@$e->empenho->empenhado, 2, ',', '.'),
                'aliquidar' => number_format(@$e->empenho->aliquidar, 2, ',', '.'),
                'liquidado' => number_format(@$e->empenho->liquidado, 2, ',', '.'),
                'pago' => number_format(@$e->empenho->pago, 2, ',', '.'),
                'rpinscrito' => number_format(@$e->empenho->rpinscrito, 2, ',', '.'),
                'rpaliquidar' => number_format(@$e->empenho->rpaliquidar, 2, ',', '.'),
                'rpliquidado' => number_format(@$e->empenho->rpliquidado, 2, ',', '.'),
                'rppago' => number_format(@$e->empenho->rppago, 2, ',', '.'),
                'informacao_complementar' => @$e->empenho->informacao_complementar,
                'sistema_origem' => @$e->empenho->sistema_origem,
                'links' => [
                    'documento_pagamento' => env('API_STA_HOST') . '/api/ordembancaria/empenho/' . $numeroEmpenho
                ],
                'credor_obj' => [
                    'tipo' => $e->fornecedor->tipo_fornecedor,
                    'cnpj_cpf_idgener' => $e->fornecedor->cpf_cnpj_idgener,
                    'nome' => $e->fornecedor->nome
                ],
            ];
        }

        return response()->json($empenhos_array);
    }

    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna todos os historicos de contrato alterados em um determinado período.",
     *     description="",
     *     path="/api/v1/contrato/historicos",
     *      security={ {"bearerAuth": {} }},
     *
     *      @OA\Parameter(
     *         name="dt_alteracao_min",
     *         in="query",
     *         description="Data de alteração inicial (formato ISO 8601-1:2019)",
     *         example="2021-11-24T08:00:00Z",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *        )
     *     ),
     *
     *     @OA\Parameter(
     *         name="dt_alteracao_max",
     *         in="query",
     *         description="Data de alteração final (formato ISO 8601-1:2019)",
     *         example="2021-11-25T18:00:00Z",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *        )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="Lista de historicos do contrato retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/Historicos")
     *         ),
     *    @OA\Response(
     *         response=401,
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/FalhaAutenticação")
     *     )
     * )
     */
    public function historicos(Request $request)
    {
        $this->rangeObrigatorio($request->dt_alteracao_min, $request->dt_alteracao_max);
        $historico_array = [];
        $historicos = (new Contratohistorico())->buscaHistoricos($this->range($request->dt_alteracao_min, $request->dt_alteracao_max));

        foreach ($historicos as $historico) {
            $historico_array[] = $historico->historicoAPI();
        }

        return response()->json($historico_array);
    }

    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna uma lista com todos os históricos do contrato",
     *     description="",
     *     path="/api/contrato/{contrato_id}/historico",
     *     @OA\Parameter(
     *         name="contrato_id",
     *         in="path",
     *         description="id do contrato",
     *         required=true,
     *          @OA\Schema(
     *              type="integer"
     *        )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="Lista de historicos do contrato retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/Historicos")
     *         ),
     *     )
     * )
     */
    public function historicoPorContratoId(int $contrato_id, Request $request)
    {
        $historico_array = [];
        $historicos = (new Contratohistorico())->buscaHistoricoPorContratoId($contrato_id, $this->range($request->dt_alteracao_min, $request->dt_alteracao_max));

        foreach ($historicos as $historico) {
            $historico_array[] = $historico->historicoAPI();
        }

        return response()->json($historico_array);
    }

    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna todas as garantias de contrato alteradas em um determinado período.",
     *     description="",
     *     path="/api/v1/contrato/garantias",
     *     security={ {"bearerAuth": {} }},
     *
     *     @OA\Parameter(
     *         name="dt_alteracao_min",
     *         in="query",
     *         description="Data de alteração inicial (formato ISO 8601-1:2019)",
     *         example="2021-11-24T08:00:00Z",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *        )
     *     ),
     *
     *     @OA\Parameter(
     *         name="dt_alteracao_max",
     *         in="query",
     *         description="Data de alteração final (formato ISO 8601-1:2019)",
     *         example="2021-11-25T18:00:00Z",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *        )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="Lista de garantias do contrato retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/Garantias")
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/FalhaAutenticação")
     *     ),
     * )
     */
    public function garantias(Request $request)
    {
        $this->rangeObrigatorio($request->dt_alteracao_min, $request->dt_alteracao_max);
        $garantias_array = [];
        $garantias = (new Contratogarantia())->buscaGarantias($this->range($request->dt_alteracao_min, $request->dt_alteracao_max));

        foreach ($garantias as $garantia) {

            $garantias_array[] = $garantia->garantiaAPI();

        }

        return response()->json($garantias_array);
    }

    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna uma lista com todas as garantias do contrato",
     *     description="",
     *     path="/api/contrato/{contrato_id}/garantias",
     *     @OA\Parameter(
     *         name="contrato_id",
     *         in="path",
     *         description="id do contrato",
     *         required=true,
     *          @OA\Schema(
     *              type="integer"
     *        )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Lista de garantias do contrato retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/Garantias")
     *         ),
     *     )
     * )
     */
    public function garantiasPorContratoId(int $contrato_id, Request $request)
    {
        $garantias_array = [];
        $garantias = (new Contratogarantia())->buscaGarantiasPorContratoId($contrato_id, $this->range($request->dt_alteracao_min, $request->dt_alteracao_max));

        foreach ($garantias as $garantia) {

            $garantias_array[] = $garantia->garantiaAPI();

        }

        return response()->json($garantias_array);
    }

    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna todos os itens dos contratos alterados em um determinado período.",
     *     description="",
     *     path="/api/v1/contrato/itens",
     *     security={ {"bearerAuth": {} }},
     *
     *     @OA\Parameter(
     *         name="dt_alteracao_min",
     *         in="query",
     *         description="Data de alteração inicial (formato ISO 8601-1:2019)",
     *         example="2021-11-24T08:00:00Z",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *        )
     *     ),
     *
     *     @OA\Parameter(
     *         name="dt_alteracao_max",
     *         in="query",
     *         description="Data de alteração final (formato ISO 8601-1:2019)",
     *         example="2021-11-25T18:00:00Z",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *        )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="Lista de itens do contrato retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/Itens")
     *         ),
     *
     *      @OA\Response(
     *         response=401,
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/FalhaAutenticação")
     *     ),
     * )
     */
    public function itens(Request $request)
    {
        $this->rangeObrigatorio($request->dt_alteracao_min, $request->dt_alteracao_max);
        $itens_array = [];
        $itens = (new Contratoitem())->buscaItens($this->range($request->dt_alteracao_min, $request->dt_alteracao_max));

        foreach ($itens as $item) {
            $itens_array[] = $item->itemAPI();
        }

        return response()->json($itens_array);
    }
    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna uma lista com todos os itens do contrato",
     *     description="",
     *     path="/api/v1/contrato/{contrato_id}/itens",
     *     security={ {"bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="contrato_id",
     *         in="path",
     *         description="id do contrato",
     *         required=true,
     *         @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Lista de itens do contrato retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/Itens")
     *         ),
     *     )
     * )
     */
    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna uma lista com todos os itens do contrato",
     *     description="",
     *     path="/api/contrato/{contrato_id}/itens",
     *     @OA\Parameter(
     *         name="contrato_id",
     *         in="path",
     *         description="id do contrato",
     *         required=true,
     *         @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Lista de itens do contrato retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/Itens")
     *         ),
     *     )
     * )
     */
    public function itensPorContratoId(int $contrato_id, Request $request)
    {
        $itens_array = [];

        //$this->range($request->dt_alteracao_min, $request->dt_alteracao_max)
        $itens = (new Contratoitem())->buscaItensPorContratoId($contrato_id, $request->request->all());

        if (isset($itens['code'])) {
            return response()->json($itens, $itens['code']);
        }

        foreach ($itens as $k => $item) {
            $itens_array[] = $item->itemAPI();
            $contratohistorico = (new Contratohistorico())->getIdByContratoIdItens($itens_array[$k]['contrato_id']);
            $itens_array[$k]['contrato_historico_id'] = $contratohistorico;
        }

        return response()->json($itens_array);
    }

    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna todos os prepostos de contrato alterados em um determinado período.",
     *     description="",
     *     path="/api/v1/contrato/prepostos",
     *     security={ {"bearerAuth": {} }},
     *
     *     @OA\Parameter(
     *         name="dt_alteracao_min",
     *         in="query",
     *         description="Data de alteração inicial (formato ISO 8601-1:2019)",
     *         example="2021-11-24T08:00:00Z",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *        )
     *     ),
     *
     *     @OA\Parameter(
     *         name="dt_alteracao_max",
     *         in="query",
     *         description="Data de alteração final (formato ISO 8601-1:2019)",
     *         example="2021-11-25T18:00:00Z",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *        )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="Lista de prepostos do contrato retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/Prepostos")
     *         ),
     *
     *     @OA\Response(
     *         response=401,
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/FalhaAutenticação")
     *     ),
     *
     * )
     */
    public function prepostos(Request $request)
    {
        $this->rangeObrigatorio($request->dt_alteracao_min, $request->dt_alteracao_max);
        $prepostos_array = [];
        $prepostos = (new Contratopreposto())->buscaPrepostos($this->range($request->dt_alteracao_min, $request->dt_alteracao_max));
        $dadosAbertos = $this->dadosAbertos();

        foreach ($prepostos as $preposto) {
            $prepostos_array[] = $preposto->prepostoAPI($this->usuarioTransparencia($preposto->nome, $preposto->cpf, $dadosAbertos));
        }

        return response()->json($prepostos_array);
    }

    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna uma lista com todos os prepostos do contrato",
     *     description="",
     *     path="/api/contrato/{contrato_id}/prepostos",
     *     @OA\Parameter(
     *         name="contrato_id",
     *         in="path",
     *         description="id do contrato",
     *         required=true,
     *         @OA\Schema(
     *              type="integer"
     *        )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Lista de prepostos do contrato retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/Prepostos")
     *         ),
     *     )
     * )
     */
    public function prepostosPorContratoId(int $contrato_id, Request $request)
    {
        $prepostos_array = [];
        $prepostos = (new Contratopreposto())->buscaPrepostosPorContratoId($contrato_id, $this->range($request->dt_alteracao_min, $request->dt_alteracao_max));
        $dadosAbertos = $this->dadosAbertos();

        foreach ($prepostos as $preposto) {
            $prepostos_array[] = $preposto->prepostoAPI($this->usuarioTransparencia($preposto->nome, $preposto->cpf, $dadosAbertos));
        }

        return response()->json($prepostos_array);
    }

    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna todos os responsáveis de contratos alterados em um determinado período.",
     *     description="",
     *     path="/api/v1/contrato/responsaveis",
     *     security={ {"bearerAuth": {} }},
     *
     *     @OA\Parameter(
     *         name="dt_alteracao_min",
     *         in="query",
     *         description="Data de alteração inicial (formato ISO 8601-1:2019)",
     *         example="2021-11-24T08:00:00Z",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *        )
     *     ),
     *
     *     @OA\Parameter(
     *         name="dt_alteracao_max",
     *         in="query",
     *         description="Data de alteração final (formato ISO 8601-1:2019)",
     *         example="2021-11-25T18:00:00Z",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *        )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="Lista de responsaveis de contrato retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/Responsaveis")
     *         ),
     *
     *     @OA\Response(
     *         response=401,
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/FalhaAutenticação")
     *     ),
     *
     * )
     */
    public function responsaveis(Request $request)
    {
        $this->rangeObrigatorio($request->dt_alteracao_min, $request->dt_alteracao_max);
        $responsaveis_array = [];
        $responsaveis = (new Contratoresponsavel())->buscaResponsaveis($this->range($request->dt_alteracao_min, $request->dt_alteracao_max));
        $dadosAbertos = $this->dadosAbertos();
        foreach ($responsaveis as $responsavel) {
            $responsaveis_array[] = $responsavel->responsavelAPI($this->usuarioTransparencia($responsavel->user->name, $responsavel->user->cpf, $dadosAbertos));
        }

        return response()->json($responsaveis_array);
    }

    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna uma lista com todos os responsáveis do contrato",
     *     description="",
     *     path="/api/contrato/{contrato_id}/responsaveis",
     *     @OA\Parameter(
     *         name="contrato_id",
     *         in="path",
     *         description="id do contrato",
     *         required=true,
     *         @OA\Schema(
     *              type="integer"
     *        )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="Lista de responsaveis do contrato retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/Responsaveis")
     *         ),
     *     )
     * )
     */
    public function responsaveisPorContratoId(int $contrato_id, Request $request)
    {
        $responsaveis_array = [];
        $responsaveis = (new Contratoresponsavel())->buscaResponsaveisPorContratoId($contrato_id, $this->range($request->dt_alteracao_min, $request->dt_alteracao_max));
        $dadosAbertos = $this->dadosAbertos();

        foreach ($responsaveis as $responsavel) {

            $responsaveis_array[] = $responsavel->responsavelAPI($this->usuarioTransparencia($responsavel->user->name, $responsavel->user->cpf, $dadosAbertos));

        }

        return response()->json($responsaveis_array);
    }


    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna uma lista com todos os responsáveis de um determinado contrato",
     *     description="",
     *     path="/api/v2/contrato/{contrato_id}/responsaveis",
     *     security={ {"bearerAuth": {} }},
     *
     *     @OA\Parameter(
     *         name="contrato_id",
     *         in="path",
     *         description="id do contrato",
     *         required=true,
     *         @OA\Schema(
     *              type="integer"
     *        )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="Lista dos responsaveis do contrato retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/ResponsaveisV2")
     *         ),
     *     )
     * )
     */
    public function responsaveisPorContratoIdv2(int $contrato_id)
    {
        $responsaveis_array = [];
        $responsaveis = (new Contratoresponsavel())->buscaResponsaveisPorContratoId($contrato_id, null);

        if ($responsaveis->count() > 0) {
            $responsaveis_array['contrato_id'] = $responsaveis[0]->contrato_id;
        }

        foreach ($responsaveis as $responsavel) {

            $responsaveis_array['responsaveis'][] = [
                'nome' => $responsavel->user->name,
                'email' => $responsavel->user->email,
                'funcao' => $responsavel->funcao->descricao,
                'situacao' => $responsavel->situacao == true ? 'Ativo' : 'Inativo',
                'updated_at' => $responsavel->updated_at->format('Y-m-d H:i:s')
            ];

        }

        return response()->json($responsaveis_array);
    }

    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna todas as despesas acessórias de contrato alteradas em um determinado período.",
     *     description="",
     *     path="/api/v1/contrato/despesas_acessorias",
     *     security={ {"bearerAuth": {} }},
     *
     *     @OA\Parameter(
     *         name="dt_alteracao_min",
     *         in="query",
     *         description="Data de alteração inicial (formato ISO 8601-1:2019)",
     *         example="2021-11-24T08:00:00Z",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *        )
     *     ),
     *
     *     @OA\Parameter(
     *         name="dt_alteracao_max",
     *         in="query",
     *         description="Data de alteração final (formato ISO 8601-1:2019)",
     *         example="2021-11-25T18:00:00Z",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *        )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="Lista de despesas acessorias do contrato retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/DespesasAcessorias")
     *         ),
     *      @OA\Response(
     *         response=401,
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/FalhaAutenticação")
     *     ),
     * )
     */
    public function despesasAcessorias(Request $request)
    {
        $this->rangeObrigatorio($request->dt_alteracao_min, $request->dt_alteracao_max);
        $despesasAcessorias_array = [];
        $despesasAcessorias = (new Contratodespesaacessoria())->buscaDespesasAcessorias($this->range($request->dt_alteracao_min, $request->dt_alteracao_max));

        foreach ($despesasAcessorias as $despesaAcessoria) {
            $despesasAcessorias_array[] = $despesaAcessoria->despesaAcessoriaAPI();
        }

        return response()->json($despesasAcessorias_array);
    }

    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna uma lista com todas as despesas acessórias do contrato",
     *     description="",
     *     path="/api/contrato/{contrato_id}/despesas_acessorias",
     *     @OA\Parameter(
     *         name="contrato_id",
     *         in="path",
     *         description="id do contrato",
     *         required=true,
     *          @OA\Schema(
     *              type="integer"
     *        )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Lista de despesas acessorias do contrato retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/DespesasAcessorias")
     *         ),
     *     )
     * )
     */
    public function despesasAcessoriasPorContratoId(int $contrato_id, Request $request)
    {
        $despesasAcessorias_array = [];
        $despesasAcessorias = (new Contratodespesaacessoria())->buscaDespesasAcessoriasPorContratoId($contrato_id, $this->range($request->dt_alteracao_min, $request->dt_alteracao_max));

        foreach ($despesasAcessorias as $despesaAcessoria) {
            $despesasAcessorias_array[] = $despesaAcessoria->despesaAcessoriaAPI();
        }

        return response()->json($despesasAcessorias_array);
    }

    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna todas as faturas de contrato alteradas em um determinado período.",
     *     description="",
     *     path="/api/v1/contrato/faturas",
     *     security={ {"bearerAuth": {} }},
     *
     *     @OA\Parameter(
     *         name="dt_alteracao_min",
     *         in="query",
     *         description="Data de alteração inicial (formato ISO 8601-1:2019)",
     *         example="2021-11-24T08:00:00Z",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *        )
     *     ),
     *
     *     @OA\Parameter(
     *         name="dt_alteracao_max",
     *         in="query",
     *         description="Data de alteração final (formato ISO 8601-1:2019)",
     *         example="2021-11-25T18:00:00Z",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *        )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="Lista de faturas do contrato retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/Faturas")
     *         ),
     *     @OA\Response(
     *         response=401,
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/FalhaAutenticação")
     *     ),
     * )
     */
    public function faturas(Request $request)
    {
        $this->rangeObrigatorio($request->dt_alteracao_min, $request->dt_alteracao_max);
        $faturas_array = [];
        $faturas = (new Contratofatura())->buscaFaturas($this->range($request->dt_alteracao_min, $request->dt_alteracao_max));

        foreach ($faturas as $fatura) {
            $faturas_array[] = $fatura->faturaAPI();
        }

        return response()->json($faturas_array);
    }

    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna uma lista com todas as faturas do contrato",
     *     description="",
     *     path="/api/contrato/{contrato_id}/faturas",
     *     @OA\Parameter(
     *         name="contrato_id",
     *         in="path",
     *         description="id do contrato",
     *         required=true,
     *          @OA\Schema(
     *              type="integer"
     *        )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Lista de faturas do contrato retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/FaturasPorId")
     *         ),
     *     )
     * )
     */
    public function faturasPorContratoId(int $contrato_id, Request $request)
    {
        $faturas_array = [];
        //$this->range($request->dt_alteracao_min, $request->dt_alteracao_max)
        $faturas = (new Contratofatura())->buscaFaturasPorContratoId($contrato_id, $request->all());

        foreach ($faturas as $fatura) {
            $faturas_array[] = $fatura->faturaPorContratoAPI();
        }

        return response()->json($faturas_array);
    }

    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna todas as ocorrências alteradas em um determinado período.",
     *     description="",
     *     path="/api/v1/contrato/ocorrencias",
     *     security={ {"bearerAuth": {} }},
     *
     *     @OA\Parameter(
     *         name="dt_alteracao_min",
     *         in="query",
     *         description="Data de alteração inicial (formato ISO 8601-1:2019)",
     *         example="2021-11-24T08:00:00Z",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *        )
     *     ),
     *
     *     @OA\Parameter(
     *         name="dt_alteracao_max",
     *         in="query",
     *         description="Data de alteração final (formato ISO 8601-1:2019)",
     *         example="2021-11-25T18:00:00Z",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *        )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="Lista de ocorrencias do contrato retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/Ocorrencias")
     *         ),
     *     @OA\Response(
     *         response=401,
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/FalhaAutenticação")
     *     )
     * )
     */
    public function ocorrencias(Request $request)
    {
        $this->rangeObrigatorio($request->dt_alteracao_min, $request->dt_alteracao_max);
        $ocorrencias_array = [];
        $ocorrencias = (new Contratoocorrencia())->buscaOcorrencias($this->range($request->dt_alteracao_min, $request->dt_alteracao_max));
        $dadosAbertos = $this->dadosAbertos();

        foreach ($ocorrencias as $ocorrencia) {
            $ocorrencias_array[] = $ocorrencia->ocorrenciaAPI($this->usuarioTransparencia($ocorrencia->usuario->name, $ocorrencia->usuario->cpf, $dadosAbertos));
        }

        return response()->json($ocorrencias_array);
    }

    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna uma lista com todas as ocorrências do contrato",
     *     description="",
     *     path="/api/contrato/{contrato_id}/ocorrencias",
     *     @OA\Parameter(
     *         name="contrato_id",
     *         in="path",
     *         description="id do contrato",
     *         required=true,
     *         @OA\Schema(
     *              type="integer"
     *        )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Lista de ocorrencias do contrato retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/Ocorrencias")
     *         ),
     *     )
     * )
     */
    public function ocorrenciasPorContratoId(int $contrato_id, Request $request)
    {
        $ocorrencias_array = [];
        $ocorrencias = (new Contratoocorrencia())->buscaOcorrenciasPorContratoId($contrato_id, $this->range($request->dt_alteracao_min, $request->dt_alteracao_max));
        $dadosAbertos = $this->dadosAbertos();

        foreach ($ocorrencias as $ocorrencia) {
            $ocorrencias_array[] = $ocorrencia->ocorrenciaAPI($this->usuarioTransparencia($ocorrencia->usuario->name, $ocorrencia->usuario->cpf, $dadosAbertos));
        }

        return response()->json($ocorrencias_array);
    }

    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna todos os terceirizados de contratos alterados em um determinado período.",
     *     description="",
     *     path="/api/v1/contrato/terceirizados",
     *     security={ {"bearerAuth": {} }},
     *
     *     @OA\Parameter(
     *         name="dt_alteracao_min",
     *         in="query",
     *         description="Data de alteração inicial (formato ISO 8601-1:2019)",
     *         example="2021-11-24T08:00:00Z",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *        )
     *     ),
     *
     *     @OA\Parameter(
     *         name="dt_alteracao_max",
     *         in="query",
     *         description="Data de alteração final (formato ISO 8601-1:2019)",
     *         example="2021-11-25T18:00:00Z",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *        )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="Lista de terceirizados do contrato retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/Terceirizados")
     *     ),
     *
     *     @OA\Response(
     *         response=401,
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/FalhaAutenticação")
     *     ),
     *
     * )
     */
    public function terceirizados(Request $request)
    {
        $this->rangeObrigatorio($request->dt_alteracao_min, $request->dt_alteracao_max);
        $terceirizados_array = [];
        $terceirizados = (new Contratoterceirizado())->buscaTerceirizados($this->range($request->dt_alteracao_min, $request->dt_alteracao_max));
        $dadosAbertos = $this->dadosAbertos();

        foreach ($terceirizados as $terceirizado) {
            $terceirizados_array[] = $terceirizado->terceirizadoAPI($this->usuarioTransparencia($terceirizado->nome, $terceirizado->cpf, $dadosAbertos));
        }

        return response()->json($terceirizados_array);
    }

    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna uma lista com todos os terceirizados do contrato",
     *     description="",
     *     path="/api/contrato/{contrato_id}/terceirizados",
     *     @OA\Parameter(
     *         name="contrato_id",
     *         in="path",
     *         description="id do contrato",
     *         required=true,
     *         @OA\Schema(
     *              type="integer"
     *        )
     *
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Lista de terceirizados do contrato retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/Terceirizados")
     *         )
     *     )
     * )
     */
    public function terceirizadosPorContratoId(int $contrato_id, Request $request)
    {

        $terceirizados_array = [];
        $terceirizados = (new Contratoterceirizado())->buscaTerceirizadosPorContratoId($contrato_id, $this->range($request->dt_alteracao_min, $request->dt_alteracao_max));
        $dadosAbertos = $this->dadosAbertos();

        foreach ($terceirizados as $terceirizado) {
            $terceirizados_array[] = $terceirizado->terceirizadoAPI($this->usuarioTransparencia($terceirizado->nome, $terceirizado->cpf, $dadosAbertos));
        }

        return response()->json($terceirizados_array);
    }

    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna todos os arquivos de contrato alterados em um determinado período.",
     *     description="",
     *     path="/api/v1/contrato/arquivos",
     *     security={ {"bearerAuth": {} }},
     *
     *     @OA\Parameter(
     *        name="dt_alteracao_min",
     *        in="query",
     *        description="Data de alteração inicial (formato ISO 8601-1:2019)",
     *        example="2021-11-24T08:00:00Z",
     *        required=true,
     *        @OA\Schema(
     *             type="string"
     *        )
     *     ),
     *
     *    @OA\Parameter(
     *        name="dt_alteracao_max",
     *        in="query",
     *        description="Data de alteração final (formato ISO 8601-1:2019)",
     *        example="2021-11-25T18:00:00Z",
     *        required=true,
     *        @OA\Schema(
     *             type="string"
     *        )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="Lista de arquivos do contrato retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/Arquivos")
     *    ),
     *    @OA\Response(
     *         response=401,
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/FalhaAutenticação")
     *     ),
     * )
     */
    public function arquivos(Request $request)
    {
        $this->rangeObrigatorio($request->dt_alteracao_min, $request->dt_alteracao_max);
        $arquivos_array = [];
        $arquivos = (new Contratoarquivo())->buscaArquivos($this->range($request->dt_alteracao_min, $request->dt_alteracao_max));

        foreach ($arquivos as $arquivo) {
            $arquivos_array[] = $arquivo->arquivoAPI();
        }

        return response()->json($arquivos_array);
    }

    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna uma lista com todos os arquivos do contrato",
     *     description="",
     *     path="/api/contrato/{contrato_id}/arquivos",
     *
     *     @OA\Parameter(
     *         name="contrato_id",
     *         in="path",
     *         description="id do contrato",
     *         required=true,
     *         @OA\Schema(
     *              type="integer"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Lista de arquivos do contrato retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/Arquivos")
     *     )
     * )
     */
    public function arquivosPorContratoId(int $contrato_id, Request $request)
    {
        $arquivos_array = [];
        $arquivos = (new Contratoarquivo())->buscaArquivosPorContratoId($contrato_id, $this->range($request->dt_alteracao_min, $request->dt_alteracao_max));

        foreach ($arquivos as $arquivo) {
            $arquivos_array[] = $arquivo->arquivoAPI();
        }

        return response()->json($arquivos_array);
    }

    private function buscaOrgaosComContratosAtivos()
    {
        $orgaos = Orgao::select('codigo')
            ->whereHas('unidades', function ($u) {
                $u->whereHas('contratos', function ($c) {
                    $c->where('situacao', true);
                    $c->where('contratos.elaboracao', false);
                })->where('sigilo', false);
            })
            ->orderBy('codigo');

        return $orgaos->get();
    }

    private function buscaUnidadesComContratosAtivos()
    {
        $unidades = Unidade::select('codigo')
            ->whereHas('contratos', function ($c) {
                $c->where('contratos.elaboracao', false);
                $c->where('situacao', true);
            })->where('sigilo', false)
            ->orderBy('codigo');

        return $unidades->get();
    }

    private function buscaEmpenhosPorContratoId(int $contrato_id, $range)
    {
        $empenhos = Contratoempenho::join('contratos', 'contratos.id', '=', 'contratoempenhos.contrato_id')
            ->join('unidades', 'unidades.id', '=', 'contratos.unidade_id')
            ->join('empenhos', 'empenhos.id', '=', 'contratoempenhos.empenho_id')
            ->where('contrato_id', $contrato_id)
            ->where('unidades.sigilo', "=", false)
            ->when($range != null, function ($d) use ($range) {
                $d->whereBetween('empenhos.updated_at', [$range[0], $range[1]]);
            })
            ->get();

        return $empenhos;
    }


    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna todos os contratos alterados em um determinado período.",
     *     description="",
     *     path="/api/v1/contrato",
     *     security={ {"bearerAuth": {} }},
     *
     *     @OA\Parameter(
     *         name="dt_alteracao_min",
     *         in="query",
     *         description="Data de alteração inicial (formato ISO 8601-1:2019)",
     *         example="2021-11-24T08:00:00Z",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *        )
     *     ),
     *
     *     @OA\Parameter(
     *         name="dt_alteracao_max",
     *         in="query",
     *         description="Data de alteração final (formato ISO 8601-1:2019)",
     *         example="2021-11-25T18:00:00Z",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *        )
     *     ),*
     *
     *     @OA\Response(
     *         response=200,
     *         description="Lista de contratos retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/Contratos")
     *         ),
     *
     *      @OA\Response(
     *         response=401,
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/FalhaAutenticação")
     *        ),
     *
     * )
     */
    public function contratoAtivoAll(Request $request)
    {
        $contratos_array = [];
        $contratos = $this->buscaContratos($this->range($request->dt_alteracao_min, $request->dt_alteracao_max));
        $prefixoAPI = Route::current()->getPrefix();

        foreach ($contratos as $contrato) {
            $contratos_array[] = [
                'id' => $contrato->id,
                'receita_despesa' => ($contrato->receita_despesa) == 'D' ? 'Despesa' : 'Receita',
                'numero' => $contrato->numero,
                'orgao_codigo' => $contrato->unidade->orgao->codigo,
                'orgao_nome' => $contrato->unidade->orgao->nome,
                'unidade_codigo' => $contrato->unidade->codigo,
                'gestao' => $contrato->unidade->gestao,
                'unidade_nome_resumido' => $contrato->unidade->nomeresumido,
                'unidade_nome' => $contrato->unidade->nome,
                'unidade_origem_codigo' => @$contrato->unidadeorigem->codigo,
                'unidade_origem_nome' => @$contrato->unidadeorigem->nome,
                'fornecedor_tipo' => $contrato->fornecedor->tipo_fornecedor,
                'fonecedor_cnpj_cpf_idgener' => $contrato->fornecedor->cpf_cnpj_idgener,
                'fornecedor_nome' => $contrato->fornecedor->nome,
                'codigo_tipo' => @$contrato->tipo->descres,
                'tipo' => $contrato->tipo->descricao,
                'categoria' => $contrato->categoria->descricao,
                'processo' => $contrato->processo,
                'objeto' => $contrato->objeto,
                'fundamento_legal' => @$contrato->fundamento_legal,
                'informacao_complementar' => $contrato->info_complementar,
                'codigo_modalidade' => @$contrato->modalidade->descres,
                'modalidade' => $contrato->modalidade->descricao,
                'unidade_compra' => @$contrato->unidadecompra->codigo,
                'licitacao_numero' => $contrato->licitacao_numero,
                'sistema_origem_licitacao' => @$contrato->sistema_origem_licitacao,
                'data_assinatura' => $contrato->data_assinatura,
                'data_publicacao' => $contrato->data_publicacao,
                'vigencia_inicio' => $contrato->vigencia_inicio,
                'vigencia_fim' => $contrato->vigencia_fim,
                'valor_inicial' => number_format($contrato->valor_inicial, 2, ',', '.'),
                'valor_global' => number_format($contrato->valor_global, 2, ',', '.'),
                'num_parcelas' => $contrato->num_parcelas,
                'valor_parcela' => number_format($contrato->valor_parcela, 2, ',', '.'),
                'valor_acumulado' => number_format($contrato->valor_acumulado, 2, ',', '.'),
                'situacao' => $contrato->situacao == true ? 'Ativo' : 'Inativo',
                'link_historico' => url($prefixoAPI . '/' . $contrato->id . '/historico/'),
                'link_empenhos' => url($prefixoAPI . '/' . $contrato->id . '/empenhos/'),
                'link_cronograma' => url($prefixoAPI . '/' . $contrato->id . '/cronograma/'),
                'link_garantias' => url($prefixoAPI . '/' . $contrato->id . '/garantias/'),
                'link_itens' => url($prefixoAPI . '/' . $contrato->id . '/itens/'),
                'link_prepostos' => url($prefixoAPI . '/' . $contrato->id . '/prepostos/'),
                'link_responsaveis' => url($prefixoAPI . '/' . $contrato->id . '/responsaveis/'),
                'link_despesas_acessorias' => url($prefixoAPI . '/' . $contrato->id . '/despesas_acessorias/'),
                'link_faturas' => url($prefixoAPI . '/' . $contrato->id . '/faturas/'),
                'link_ocorrencias' => url($prefixoAPI . '/' . $contrato->id . '/ocorrencias/'),
                'link_terceirizados' => url($prefixoAPI . '/' . $contrato->id . '/terceirizados/'),
                'link_arquivos' => url($prefixoAPI . '/' . $contrato->id . '/arquivos/'),
            ];
        }

        return response()->json($contratos_array);
    }

    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna um Contrato ou Empenho (com força de contrato) pelo código da UASG de origem e seu número.",
     *     description="",
     *     path="/api/contrato/ugorigem/{codigo_uasg}/numeroano/{numero_contrato}",
     *
     *     @OA\Parameter(
     *         name="codigo_uasg",
     *         in="path",
     *         description="Código da UASG de origem",
     *         example="201053",
     *         required=true,
     *         @OA\Schema(
     *            type="integer"
     *        ),
     *     ),
     *     @OA\Parameter(
     *         name="numero_contrato",
     *         in="path",
     *         description="O formato do número do Contrato deve ter 9 caracteres (sem a barra). Já o formato do número do Empenho (com força de Contrato) deve ter 12 caracteres.",
     *         example="000452023 ou 2023NE000123",
     *         required=true,
     *         @OA\Schema(
     *                 type="string",
     *                 minLength=9,
     *                 maxLength=12,
     *         ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Contrato por UASG e pelo número retornado com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/ContratosUnidadeGestora")
     *         ),
     *     @OA\Response(
     *         response=422,
     *         description="O parâmetro 'numero_contrato' foi enviado com um tamanho inválido.",
     *         @OA\JsonContent(
     *              @OA\Property(property="errors", type="string", example="O parametro 'numero_contrato' foi enviado com um tamanho invalido"),
     *         )
     *         ),
     *   )
     * )
     */
    public function contratoUASGeContratoAno(string $codigo_uasg, string $numero_contrato, Request $request)
    {
        //Valida o tamanho do número: Contrato (9 caracteres) ou Empenho (12 caracteres)
        if (strlen($numero_contrato) != 9 && strlen($numero_contrato) != 12) {
            abort(response()->json(['errors' => "O parametro 'numero_contrato' foi enviado com um tamanho invalido",], 422));
        }

        //Inclui a barra para o formato Contrato (Ex.: 123452023 => 12345/2023)
        if (strlen($numero_contrato) == 9) {
            $numero_contrato = substr_replace($numero_contrato, '/', 5, 0);
        }

        $contratos_array = [];
        $contratos = $this->buscaContratoPorUASGeNumero($codigo_uasg, $numero_contrato, $this->range($request->dt_alteracao_min, $request->dt_alteracao_max));
        foreach ($contratos as $contrato) {
            $contratos_array[] = $contrato->contratoAPI(Route::current()->getPrefix());
        }
        return response()->json($contratos_array);
    }

    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna todos os contratos ativos de uma determinada UG",
     *     description="",
     *     path="/api/contrato/ug/{unidade_codigo}",
     *
     *     @OA\Parameter(
     *         name="unidade_codigo",
     *         in="path",
     *         description="codigo da unidade",
     *         required=true,
     *         @OA\Schema(
     *              type="integer"
     *        )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Lista de contratos ativos da UG retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/ContratosUnidadeGestora")
     *         )
     * )
     */
    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna todos os contratos ativos de uma determinada UG",
     *     description="",
     *     path="/api/v1/contrato/ug/{unidade_codigo}",
     *     security={ {"bearerAuth": {} }},
     *
     *     @OA\Parameter(
     *         name="unidade_codigo",
     *         in="path",
     *         description="codigo da unidade",
     *         required=true,
     *         @OA\Schema(
     *              type="integer"
     *        )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Lista de contratos ativos da UG retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/ContratosUnidadeGestora")
     *         ),
     *
     *     @OA\Response(
     *         response=401,
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/FalhaAutenticação")
     *     ),
     * )
     */
    public function contratoAtivoPorUg(string $unidade, Request $request)
    {
        $contratos_array = [];
        $contratos = $this->buscaContratosPorUg($unidade, $this->range($request->dt_alteracao_min, $request->dt_alteracao_max));

        foreach ($contratos as $contrato) {
            $contratos_array[] = $contrato->contratoAPI(Route::current()->getPrefix());
        }

        return response()->json($contratos_array);
    }

    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna todos os contratos ativos de um determinado Órgão",
     *     description="",
     *     path="/api/v1/contrato/orgao/{orgao}",
     *    security={ {"bearerAuth": {} }},
     *
     *     @OA\Parameter(
     *         name="orgao",
     *         in="path",
     *         description="código do orgao",
     *         required=true,
     *         @OA\Schema(
     *              type="integer"
     *        )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Lista de contratos ativos do orgão retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/ContratosUnidadeGestora")
     *     ),
     *
     *     @OA\Response(
     *        response=401,
     *        description="Error",
     *        @OA\JsonContent(ref="#/components/schemas/FalhaAutenticação")
     *     ),
     *
     * )
     */
    public function contratoAtivoPorOrgao(string $orgao, Request $request)
    {
        $contratos_array = [];
        $contratos = $this->buscaContratosPorOrgao($orgao, $this->range($request->dt_alteracao_min, $request->dt_alteracao_max));

        foreach ($contratos as $contrato) {
            $contratos_array[] = $contrato->contratoAPI(Route::current()->getPrefix());
        }

        return response()->json($contratos_array);
    }

    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna todos os contratos inativos de uma determinada UG",
     *     description="",
     *     path="/api/contrato/inativo/ug/{unidade_codigo}",
     *
     *     @OA\Parameter(
     *         name="unidade_codigo",
     *         in="path",
     *         description="codigo da unidade",
     *         required=true,
     *         @OA\Schema(
     *              type="integer"
     *        )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Lista de contratos inativos da UG retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/ContratosUnidadeGestora")
     *         ),
     *     )
     * )
     */
    public function contratoInativoPorUg(string $unidade, Request $request)
    {
        $contratos_array = [];
        $contratos = $this->buscaContratosInativosPorUg($unidade, $this->range($request->dt_alteracao_min, $request->dt_alteracao_max));

        foreach ($contratos as $contrato) {
            $contratos_array[] = $contrato->contratoAPI(Route::current()->getPrefix());
        }

        return response()->json($contratos_array);
    }

    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna uma lista com todos os contratos inativos por Órgão",
     *     description="",
     *     path="/api/v1/contrato/inativo/orgao/{orgao}",
     *     security={ {"bearerAuth": {} }},
     *
     *     @OA\Parameter(
     *         name="orgao",
     *         in="path",
     *         description="numero do orgao",
     *         required=true,
     *         @OA\Schema(
     *              type="integer"
     *        )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Lista de contratos inativos do orgão retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/ContratosUnidadeGestora")
     *         ),
     *
     *      @OA\Response(
     *         response=401,
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/FalhaAutenticação")
     *        ),
     *
     * )
     */
    public function contratoInativoPorOrgao(string $orgao, Request $request)
    {
        $contratos_array = [];
        $contratos = $this->buscaContratosInativosPorOrgao($orgao, $this->range($request->dt_alteracao_min, $request->dt_alteracao_max));

        foreach ($contratos as $contrato) {
            $contratos_array[] = $contrato->contratoAPI(Route::current()->getPrefix());
        }

        return response()->json($contratos_array);
    }

    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna todas as publicações de contratos alterados em um determinado período.",
     *     description="",
     *     path="/api/v1/contrato/publicacoes",
     *     security={ {"bearerAuth": {} }},
     *
     *     @OA\Parameter(
     *         name="dt_alteracao_min",
     *         in="query",
     *         description="Data de alteração inicial (formato ISO 8601-1:2019)",
     *         example="2021-11-24T08:00:00Z",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *        )
     *     ),
     *
     *     @OA\Parameter(
     *         name="dt_alteracao_max",
     *         in="query",
     *         description="Data de alteração final (formato ISO 8601-1:2019)",
     *         example="2021-11-25T18:00:00Z",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *        )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="Lista de publicações retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/Publicacoes")
     *     ),
     *
     *     @OA\Response(
     *         response=401,
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/FalhaAutenticação")
     *     ),
     *
     * )
     */

    public function publicacoes(Request $request)
    {
        $this->rangeObrigatorio($request->dt_alteracao_min, $request->dt_alteracao_max);
        $publicacoes_array = [];
        $publicacoes = (new Contratopublicacoes())->buscaPublicacoes($this->range($request->dt_alteracao_min, $request->dt_alteracao_max));

        foreach ($publicacoes as $publicacao) {
            $publicacoes_array[] = $publicacao->publicacaoAPI();
        }

        return response()->json($publicacoes_array);
    }


    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna uma lista com todas as publicações do contrato",
     *     description="",
     *     path="/api/contrato/{contrato_id}/publicacoes",
     *
     *     @OA\Parameter(
     *         name="contrato_id",
     *         in="path",
     *         description="id do contrato",
     *         required=true,
     *         @OA\Schema(
     *              type="integer"
     *        )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="Lista de publicacoes do contrato retornada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/Publicacoes")
     *         ),
     *     )
     * )
     */

    public function publicacoesPorContratoId(int $contrato_id, Request $request)
    {
        $publicacoes_array = [];
        $publicacoes = (new Contratopublicacoes())->buscaPublicacoesPorContratoIdAPI($contrato_id, $this->range($request->dt_alteracao_min, $request->dt_alteracao_max));

        foreach ($publicacoes as $publicacao) {
            $publicacoes_array[] = $publicacao->publicacaoAPI();
        }

        return response()->json($publicacoes_array);
    }


    private function buscaContratosPorUg(string $unidade, $range)
    {
        $contratos = Contrato::whereHas('unidade', function ($q) use ($unidade) {
            $q->whereHas('orgao', function ($o) {
                $o->where('situacao', true);
            })
                ->where('codigo', $unidade)
                ->where('sigilo', false)
                ->where('situacao', true);
        })
            ->where('situacao', true)
            ->when($range != null, function ($d) use ($range) {
                $d->whereBetween('updated_at', [$range[0], $range[1]]);
            })
            ->orderBy('id')
            ->get();

        return $contratos;
    }

    private function buscaContratosInativosPorUg(string $unidade, $range)
    {
        $contratos = Contrato::whereHas('unidade', function ($q) use ($unidade) {
            $q->whereHas('orgao', function ($o) {
                $o->where('situacao', true);
            })
                ->where('codigo', $unidade)
                ->where('sigilo', false)
                ->where('situacao', true);
        })
            ->where('situacao', false)
            ->when($range != null, function ($d) use ($range) {
                $d->whereBetween('updated_at', [$range[0], $range[1]]);
            })
            ->orderBy('id')
            ->get();

        return $contratos;
    }

    private function buscaContratosPorOrgao(string $orgao, $range)
    {
        $contratos = Contrato::whereHas('unidade', function ($q) use ($orgao) {
            $q->whereHas('orgao', function ($o) use ($orgao) {
                $o->where('codigo', $orgao)
                    ->where('situacao', true);
            })->where('sigilo', false);
        })
            ->where('situacao', true)
            ->when($range != null, function ($d) use ($range) {
                $d->whereBetween('updated_at', [$range[0], $range[1]]);
            })
            ->orderBy('id')
            ->get();

        return $contratos;
    }

    private function buscaContratoPorUASGeNumero(string $codigo_uasg, string $numeroano_contrato, $range)
    {
        $contratos = Contrato::whereHas('unidadeorigem', function ($q) use ($codigo_uasg) {
            $q->whereHas('orgao', function ($o) {
                $o->where('situacao', true);
            })
                ->where('sigilo', false)
                ->where('codigo', $codigo_uasg)
                ->where('situacao', true);
        })
            ->where('numero', $numeroano_contrato)
            ->when($range != null, function ($d) use ($range) {
                $d->whereBetween('updated_at', [$range[0], $range[1]]);
            })
            ->orderBy('id')
            ->get();

        return $contratos;
    }

    private function buscaContratosInativosPorOrgao(string $orgao, $range)
    {
        $contratos = Contrato::whereHas('unidade', function ($q) use ($orgao) {
            $q->whereHas('orgao', function ($o) use ($orgao) {
                $o->where('codigo', $orgao)
                    ->where('situacao', true);
            })->where('sigilo', false);
        })
            ->where('situacao', false)
            ->when($range != null, function ($d) use ($range) {
                $d->whereBetween('updated_at', [$range[0], $range[1]]);
            })
            ->orderBy('id')
            ->get();

        return $contratos;
    }

    private function buscaContratos($range)
    {
        $contratos = Contrato::whereHas('unidade', function ($q) {
            $q->whereHas('orgao', function ($o) {
                $o->where('situacao', true);
            })->where('sigilo', false);
        })
            ->when($range != null, function ($d) use ($range) {
                $d->whereBetween('updated_at', [$range[0], $range[1]]);
            })
            ->orderBy('id')
            ->get();

        return $contratos;
    }

    public function buscarCamposParaCadastroContratoPorIdEmpenho($id)
    {
        $camposContrato = MinutaEmpenho::select(
            "compras.modalidade_id",
            "compras.unidade_origem_id as unidade_id",
            "unidades.codigo",
            "unidades.nomeresumido",
            "amparo_legal.ato_normativo",
            "amparo_legal.artigo",
            "minutaempenhos.amparo_legal_id",
            "compras.numero_ano as compra_numero_ano",
            "minutaempenhos.data_emissao",
            "minutaempenhos.mensagem_siafi",
            "minutaempenhos.data_emissao",
            "minutaempenhos.descricao",
            "minutaempenhos.processo",
            "compras.origem",
            "compras.numero_contratacao"
        )
            ->selectRaw("EXTRACT(MONTH FROM minutaempenhos.data_emissao) mes_empenho")
            ->selectRaw("EXTRACT(YEAR FROM minutaempenhos.data_emissao) ano_empenho")
            ->join('compras', 'compras.id', '=', 'minutaempenhos.compra_id')
            ->join('unidades', 'unidades.id', '=', 'compras.unidade_origem_id')
            ->join('amparo_legal', 'amparo_legal.id', '=', 'minutaempenhos.amparo_legal_id')
            ->where('minutaempenhos.id', $id)->firstOrFail()->toArray();

        $camposContrato['fim_vigencia'] = "{$camposContrato['ano_empenho']}-12-31";

        return $camposContrato;
    }

    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna um contrato pelo seu identificador único (id)",
     *     description="",
     *     path="/api/contrato/id/{contrato_id}",
     *     @OA\Parameter(
     *         name="contrato_id",
     *         in="path",
     *         description="id do contrato",
     *         required=true,
     *         @OA\Schema(
     *              type="integer"
     *        )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="Contrato retornado com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/ContratoPorId")
     *         ),
     *     )
     * )
     */

    public function contratoPorId(int $contrato_id)
    {
        $contrato_array = [];
        $contrato_obj = (new Contrato())->buscaContratoPorId($contrato_id);

        foreach ($contrato_obj as $contrato) {
            $contrato_array[] = [
                'id' => $contrato->id,
                'receita_despesa' => ($contrato->receita_despesa) == 'D' ? 'Despesa' : 'Receita',
                'numero' => $contrato->numero,
                'orgao_codigo' => $contrato->unidade->orgao->codigo,
                'orgao_nome' => $contrato->unidade->orgao->nome,
                'unidade_codigo' => $contrato->unidade->codigo,
                'gestao' => $contrato->unidade->gestao,
                'unidade_nome_resumido' => $contrato->unidade->nomeresumido,
                'unidade_nome' => $contrato->unidade->nome,
                'unidade_origem_codigo' => @$contrato->unidadeorigem->codigo,
                'unidade_origem_nome' => @$contrato->unidadeorigem->nome,
                'fornecedor_tipo' => $contrato->fornecedor->tipo_fornecedor,
                'fonecedor_cnpj_cpf_idgener' => $contrato->fornecedor->cpf_cnpj_idgener,
                'fornecedor_nome' => $contrato->fornecedor->nome,
                'codigo_tipo' => @$contrato->tipo->descres,
                'tipo' => $contrato->tipo->descricao,
                'categoria' => $contrato->categoria->descricao,
                'processo' => $contrato->processo,
                'objeto' => $contrato->objeto,
                'fundamento_legal' => @$contrato->fundamento_legal,
                'informacao_complementar' => $contrato->info_complementar,
                'codigo_modalidade' => @$contrato->modalidade->descres,
                'modalidade' => $contrato->modalidade->descricao,
                'unidade_compra' => @$contrato->unidadecompra->codigo,
                'licitacao_numero' => $contrato->licitacao_numero,
                'sistema_origem_licitacao' => @$contrato->sistema_origem_licitacao,
                'data_assinatura' => $contrato->data_assinatura,
                'data_publicacao' => $contrato->data_publicacao,
                'vigencia_inicio' => $contrato->vigencia_inicio,
                'vigencia_fim' => $contrato->vigencia_fim,
                'valor_inicial' => number_format($contrato->valor_inicial, 2, ',', '.'),
                'valor_global' => number_format($contrato->valor_global, 2, ',', '.'),
                'num_parcelas' => $contrato->num_parcelas,
                'valor_parcela' => number_format($contrato->valor_parcela, 2, ',', '.'),
                'valor_acumulado' => number_format($contrato->valor_acumulado, 2, ',', '.'),
                'situacao' => $contrato->situacao == true ? 'Ativo' : 'Inativo',
            ];
        }

        return response()->json($contrato_array);

    }

    public function responsaveisContratoOrgao(int $contrato_id)
    {
        $responsaveis_array = [];
        $user = new BackpackUser();
        $orgaos = $user->getOrgaoByUserId(backpack_user()->id);
        $idOrgao = [];
        foreach ($orgaos as $orgao) {
            $idOrgao[] = $orgao->id;
        }

        if ($this->dadosAbertos() && backpack_user()->hasRole('Acesso API Elevado') || backpack_user()->hasRole('Administrador')) {
            $responsaveis = (new Contratoresponsavel())->buscaContratoResponsaveisOrgao($contrato_id, $idOrgao);

            if (count($responsaveis) > 0) {
                $responsaveis_array['contrato_id'] = $responsaveis[0]->id;
            } else {
                return response()->json(['response' => 'Contrato sem responsável(is) ou Órgão do Contrato diverge do Usuário.'], 404);
            }

            foreach ($responsaveis as $responsavel) {

                $responsaveis_array['responsaveis'][] =
                    [
                        'responsavel_id' => $responsavel->responsavel_id,
                        'nome' => $responsavel->name,
                        'cpf' => preg_replace('/[^0-9]/', '', $responsavel->cpf),
                        'funcao' => $responsavel->funcao,
                        'instalacao' => $responsavel->instalacoes,
                        'portaria' => $responsavel->portaria,
                        'situacao' => $responsavel->situacao == true ? 'Ativo' : 'Inativo',
                        'data_inicio' => $responsavel->data_inicio,
                        'data_fim' => $responsavel->data_fim,

                    ];
            }
        } else {
            return response()->json(['error' => 'Acesso ou permissão negada.'], 401);
        }

        return response()->json($responsaveis_array);
    }

    /**
     * @OA\Put(
     *     tags={"Contratos"},
     *     summary="Atualiza uma fatura do contrato",
     *     description="",
     *     path="/api/v1/contrato/faturas/edit/id_fatura/{fatura_id}/justificativa/{justificativa_id}/situacao/{situacao}/empenhos/{empenhos}",
     *     security={ {"bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="fatura_id",
     *         in="path",
     *         description="id da fatura",
     *         required=true,
     *         @OA\Schema(
     *              type="integer"
     *        )
     *     ),
     *     @OA\Parameter(
     *         name="justificativa_id",
     *         in="path",
     *         description="id da justificativa",
     *         required=true,
     *         @OA\Schema(
     *              type="integer"
     *        )
     *     ),
     *     @OA\Parameter(
     *         name="situacao",
     *         in="path",
     *         description="situação da fatura",
     *         required=true,
     *         @OA\Schema(
     *              type="string"
     *        )
     *     ),
     *     @OA\Parameter(
     *         name="empenhos",
     *         in="path",
     *         description="um ou mais empenhos",
     *         required=true,
     *         @OA\Schema(
     *              type="array",
     *              @OA\Items(type="string")
     *        )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="fatura editada com sucesso",
     *         @OA\JsonContent(ref="#/components/schemas/AtualizaFatura")
     *         ),
     *     )
     * )
     */
    public function editaInstrumentoCobranca(int $id_fatura, int $justificativa, string $situacao, string $empenhos)
    {

        if (!backpack_user()->hasRole('Administrador')) {

            if (!backpack_user()->hasRole('Acesso API Elevado')) {
                return response()->json(['error' => 'Acesso ou permissão negada.'], 401,
                    ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                    JSON_UNESCAPED_UNICODE);
            }
        }
        $empenhosJson = json_decode($empenhos, true);
        // Tratando formatos caso venha do swagger ou em formato de array no postman
        // ex swagger: ...empenhos/7182262,7182224
        // ex postman: ...empenhos/[7182262,7182224]
        if (!$empenhosJson) {
            $empenhosJson = explode(',', $empenhos);

        }
        $empenhos = $empenhosJson;

        $contrato_fatura = Contratofatura::where('id', $id_fatura)->first();

        if (!$contrato_fatura) {
            return response()->json(['error' => 'Fatura não encontrada.'], 404,
                ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                JSON_UNESCAPED_UNICODE);
        }

        $array_amparos_contrato_fatura = $contrato_fatura->contrato->amparos;
        $amparo_lei_14133 = in_array('LEI 14.133/2021', array_column($array_amparos_contrato_fatura, 'ato_normativo'));
        $amparos = $this->retornaJustificativas($amparo_lei_14133);
        $situacoes = config('app.situacao_fatura');

        //Função para verificar se cada Empenho informado tem vínculo com a Fatura.
        if (!$this->verificaVinculoEmpenhosFatura($empenhosJson, $contrato_fatura)) {
            return response()->json(['error' => 'Empenho(s) informado(s) não estão vinculado(s) a fatura.'], 400,
                ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                JSON_UNESCAPED_UNICODE);
        }

        if (!$this->verificaEmpenhos($empenhosJson, backpack_user(), $contrato_fatura->contrato->fornecedor_id)) {
            return response()->json(['error' => 'Empenho(s) informado(s) sem vínculo(s) com a unidade do usuário.'], 400,
                ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                JSON_UNESCAPED_UNICODE);
        }

        if (!$this->verificaSituacao($situacao, $situacoes)) {
            return response()->json(['error' => 'Situação informada inválida.'], 400,
                ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                JSON_UNESCAPED_UNICODE);
        }

        if (!$this->verificaJustificativa($justificativa, $amparos)) {
            return response()->json(['error' => 'Justificativa informada inválida.'], 400,
                ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                JSON_UNESCAPED_UNICODE);
        }


        if (!$this->verificaUnidadeContratoFaturaAPI($contrato_fatura, backpack_user())) {
            return response()->json(['error' => 'Usuário não tem permissão para acesso a contratos desta unidade.'], 400,
                ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                JSON_UNESCAPED_UNICODE);
        }

        try {
            DB::beginTransaction();

            Contratofatura::where('id', $id_fatura)
                ->update(
                    ['situacao' => strtoupper($situacao), 'justificativafatura_id' => $justificativa]
                );

            // update manual - deleta todos os registros para inserir os novos dados
            ContratoFaturaEmpenho::where('contratofatura_id', $id_fatura)->delete();
            if (is_array($empenhos)) {
                foreach ($empenhos as $empenho) {
                    ContratoFaturaEmpenho::insert(['contratofatura_id' => $id_fatura, 'empenho_id' => $empenho]);
                }
            } else {
                ContratoFaturaEmpenho::insert(['contratofatura_id' => $id_fatura, 'empenho_id' => $empenhos]);
            }

            DB::commit();
            return response()->json(['success' => 'Fatura editada com sucesso.'], 200);

        } catch (\Exception $e) {
            Log::info('Erro ao editar fatura: ' . $e->getMessage());
            DB::rollBack();
            return response()->json(['error' => 'Ocorreu um erro ao editar a fatura.'], 400);
        }
    }

    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna os registros do domicílio bancário de um determinado contrato.",
     *     description="",
     *     path="/api/contrato/{contrato_id}/domiciliobancario",
     *
     *     @OA\Parameter(
     *         name="contrato_id",
     *         in="path",
     *         description="id do contrato",
     *         required=true,
     *         @OA\Schema(
     *              type="integer"
     *        )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="Registros retornados com sucesso.",
     *         @OA\JsonContent(ref="#/components/schemas/DomicilioBancario")
     *     ),
     *
     *     @OA\Response(
     *         response=404,
     *         description="Recurso não localizado.",
     *         @OA\JsonContent(@OA\Property(property="error",type="string",example="Contrato não localizado ou Contrato sem domicílio bancário registrado pelo AntecipaGov."))
     *     ),
     *
     * )
     */
    /**
     * @OA\Get(
     *     tags={"Contratos"},
     *     summary="Retorna os registros do domicílio bancário de um determinado contrato.",
     *     description="",
     *     path="/api/v1/contrato/{contrato_id}/domiciliobancario",
     *     security={ {"bearerAuth": {} }},
     *
     *     @OA\Parameter(
     *         name="contrato_id",
     *         in="path",
     *         description="id do contrato",
     *         required=true,
     *         @OA\Schema(
     *              type="integer"
     *        )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="Registros retornados com sucesso.",
     *         @OA\JsonContent(ref="#/components/schemas/DomicilioBancario")
     *     ),
     *
     *     @OA\Response(
     *         response=401,
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/FalhaAutenticação")
     *     ),
     *
     *     @OA\Response(
     *         response=404,
     *         description="Recurso não localizado.",
     *         @OA\JsonContent(@OA\Property(property="error",type="string",example="Contrato não localizado ou Contrato sem domicílio bancário registrado pelo AntecipaGov."))
     *     ),
     *
     * )
     */
    public function operacoesDomicilioBancarioPorContrato(int $contrato_id)
    {
        $arrOperacoesAPI = [];

        if (isset($contrato_id)) {
            try {
                $contrato = Contrato::find($contrato_id);
                if (is_null($contrato)) throw new ModelNotFoundException("Contrato não localizado.");

                $operacoesDB = Contrato::find($contrato_id)->antecipagovs()->get();
                if (empty($operacoesDB->count())) throw new EmptyCollectionException("Contrato sem domicílio bancário registrado pelo AntecipaGov.");

                $arrOperacoesAPI['unidade_gestora'] = $contrato->unidade->codigo;
                $arrOperacoesAPI['contrato_id'] = $contrato->id;
                $arrOperacoesAPI['contrato'] = $contrato->numero;
                foreach ($operacoesDB as $operacaoDB) {
                    $arrOperacoesAPI['domiciliosbancarios'][] = [
                        'id' => $operacaoDB->id,
                        'situacao' => $operacaoDB->statusSituacao->descricao,
                        'banco' => $operacaoDB->domicilioBancario->banco,
                        'agencia' => $operacaoDB->domicilioBancario->agencia,
                        'conta_bancaria' => $operacaoDB->domicilioBancario->conta,
                        'codigo_operacao_credito' => $operacaoDB->num_operacao,
                        'data_registro' => $operacaoDB->created_at->format('d/m/Y H:i:s')
                    ];
                }

            } catch (ModelNotFoundException $e) {
                return response()->json(['error' => $e->getMessage()], 404);
            } catch (EmptyCollectionException $e) {
                return response()->json(['error' => $e->getMessage()], 404);
            } catch (\Exception $e) {
                return response()->json(['error' => $e->getMessage()], 500);
            }

        }

        return response()->json($arrOperacoesAPI);
    }

    // Endpoint específico para atender ao Julios
    public function domicilioBancarioPorContrato(int $contrato_id)
    {
        $arrOperacoesAPI = [];

        if (isset($contrato_id)) {
            try {
                $contrato = Contrato::find($contrato_id);
                if (is_null($contrato)) throw new ModelNotFoundException("Contrato não localizado.");

                $operacoesDB = Contrato::find($contrato_id)->antecipagovs()->get();
                if (empty($operacoesDB->count())) throw new EmptyCollectionException("Contrato sem domicilio bancário.");

                foreach ($operacoesDB as $operacaoDB) {
                    $chaveConta = $operacaoDB->domicilioBancario->conta . '-' .
                        $operacaoDB->domicilioBancario->agencia . '-' .
                        $operacaoDB->domicilioBancario->banco;

                    if (!isset($domicilios[$chaveConta])) {
                        $domicilios[$chaveConta] = [
                            'conta_bancaria' => $operacaoDB->domicilioBancario->conta,
                            'num_agencia' => $operacaoDB->domicilioBancario->agencia,
                            'num_banco' => $operacaoDB->domicilioBancario->banco,
                            'antecipa_gov' => $operacaoDB->domicilioBancario->antecipa_gov,
                            'historico_antecipa_gov' => []
                        ];
                    }

                    $domicilios[$chaveConta]['historico_antecipa_gov'][] = [
                        'status_operacao' => $operacaoDB->statusSituacao->descricao,
                        'num_operacao' => $operacaoDB->num_operacao,
                        'num_cotacao' => $operacaoDB->num_cotacao,
                        'identificador_unico' => $operacaoDB->identificador_unico,
                        'data_acao' => $operacaoDB->data_acao,
                        'valor_operacao' => $operacaoDB->valor_operacao,
                        'valor_parcela' => $operacaoDB->valor_parcela
                    ];
                }

                $arrOperacoesAPI['domicilio_bancario'] = array_values($domicilios);

            } catch (ModelNotFoundException $e) {
                return response()->json(['error' => $e->getMessage()], 404);
            } catch (EmptyCollectionException $e) {
                return response()->json(['error' => $e->getMessage()], 404);
            } catch (\Exception $e) {
                return response()->json(['error' => $e->getMessage()], 500);
            }
        }
        return response()->json($arrOperacoesAPI);
    }

    // Endpoint específico para atender ao Obrasgov.br (issue #1016)
    public function contratosComEmpenhosCipi(Request $request)
    {

        //Valida a faixa de datas informadas.
        $api_controller = new APIController();
        $api_controller->rangeObrigatorio($request->dt_alteracao_min, $request->dt_alteracao_max);
        $range = $api_controller->range($request->dt_alteracao_min, $request->dt_alteracao_max);

        $contratos_cipi_array = [];
        try {
            $contratos_cipi_array = Contrato::buscaContratosComEmpenhosCipi($range);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
        return response()->json($contratos_cipi_array);
    }

    public function buscaCEP(Request $request)
    {
        try {
            return (new EstaleiroCEP(str_replace('.', '', str_replace('-', '', backpack_user()->cpf)), $request->cep))->consultaCep();
        } catch (\Exception $e) {

            switch ($e->getCode()) {
                case 404:
                    Log::info('cep não encontrado ' . $e->getMessage());
                    return response()->json(['error' => 'Cep não encontrado.'], 404);
                case 403:
                    Log::info('Erro Consulta CEP ' . $e->getMessage());
                    return response()->json(['error' => $e->getCode()], 403);
            }
        }

    }

    /**
     *
     * @OA\Components(
     *         @OA\Schema(
     *             schema="Contratos",
     *             type="object",
     *             @OA\Property(property="id",type="integer",example="7602"),
     *             @OA\Property(property="receita_despesa",type="string",example="Despesa"),
     *             @OA\Property(property="numero",type="string",example="00030/2013"),
     *             @OA\Property(property="orgao_codigo",type="integer",example="36213"),
     *             @OA\Property(property="orgao_nome",type="string",example="AGENCIA NACIONAL DE SAUDE SUPLEMENTAR"),
     *             @OA\Property(property="unidade_codigo",type="integer",example="253003"),
     *             @OA\Property(property="gestao",type="string",example="253003"),
     *             @OA\Property(property="unidade_nome_resumido",type="string",example="ANS"),
     *             @OA\Property(property="unidade_nome",type="string",example="AGENCIA NACIONAL DE SAUDE SUPLEMENTAR"),
     *             @OA\Property(property="unidade_origem_codigo",type="integer",example="253034"),
     *             @OA\Property(property="unidade_origem_nome",type="string",example="AGENCIA NACIONAL DE SAUDE S. - SAO PAULO"),
     *             @OA\Property(property="fornecedor_tipo",type="string",example="JURIDICA"),
     *             @OA\Property(property="fonecedor_cnpj_cpf_idgener",type="string",example="03.555.423/0001-66"),
     *             @OA\Property(property="fornecedor_nome",type="string",example="SALIBA PARTICIPACOES LTDA."),
     *             @OA\Property(property="codigo_tipo",type="integer",example="50"),
     *             @OA\Property(property="tipo",type="string",example="Contrato"),
     *             @OA\Property(property="categoria",type="string",example="Locação Imóveis"),
     *             @OA\Property(property="processo",type="string",example="33902.568502/2012-71"),
     *             @OA\Property(property="objeto",type="string",example="LOCACAO DOS CONJUNTOS COMERCIAIS DO 9 ANDAR DO EDIFICIO RACHID SALIBA, LOCALIZADO NA RUA BELA CINTRA, 986 - SAO PAULO/SP. COM AREA TOTAL DE 764,54M2 E DIREITO DE USO DE 08 VAGAS DE GARAGEM."),
     *             @OA\Property(property="fundamento_legal",type="string",example="LEI 8666/93, LEI 8245/91, LEI 9961/00, DECRETO 7689/12, P"),
     *             @OA\Property(property="informacao_complementar",type="string",example="null"),
     *             @OA\Property(property="codigo_modalidade",type="string",example="06"),
     *             @OA\Property(property="modalidade",type="string",example="Dispensa"),
     *             @OA\Property(property="unidade_compra",type="integer",example="253003"),
     *             @OA\Property(property="licitacao_numero",type="string",example="00184/2013"),
     *             @OA\Property(property="sistema_origem_licitacao",type="string",example="null"),
     *             @OA\Property(property="data_assinatura",type="string",example="2013-08-12",format="yyyy-mm-dd"),
     *             @OA\Property(property="data_publicacao",type="string",example="2013-10-03",format="yyyy-mm-dd"),
     *             @OA\Property(property="data_inicio_item",type="string",example="2022-05-31",format="yyyy-mm-dd"),
     *             @OA\Property(property="numero_item_compra", type="integer", example="00068"),
     *             @OA\Property(property="vigencia_inicio",type="string",example="2013-08-12",format="yyyy-mm-dd"),
     *             @OA\Property(property="vigencia_fim",type="string",example="2021-11-30",format="yyyy-mm-dd"),
     *             @OA\Property(property="valor_inicial",type="number",example="3.669.792,00"),
     *             @OA\Property(property="valor_global",type="number",example="3.669.792,00"),
     *             @OA\Property(property="num_parcelas",type="integer",example="1"),
     *             @OA\Property(property="valor_parcela",type="number",example="3.669.792,00"),
     *             @OA\Property(property="valor_acumulado",type="number",example="7.339.584,00"),
     *             @OA\Property(property="situacao",type="string",example="Inativo"),
     *             @OA\Property(property="link_historico",type="string",example="https://contratos.comprasnet.gov.br/api/contrato/1/historico"),
     *             @OA\Property(property="link_empenhos",type="string",example="https://contratos.comprasnet.gov.br/api/contrato/1/empenhos"),
     *             @OA\Property(property="link_cronograma",type="string",example="https://contratos.comprasnet.gov.br/api/contrato/1/cronograma"),
     *             @OA\Property(property="link_garantias",type="string",example="https://contratos.comprasnet.gov.br/api/contrato/1/garantias"),
     *             @OA\Property(property="link_itens",type="string",example="https://contratos.comprasnet.gov.br/api/contrato/1/itens"),
     *             @OA\Property(property="link_prepostos",type="string",example="https://contratos.comprasnet.gov.br/api/contrato/1/prepostos"),
     *             @OA\Property(property="link_responsaveis",type="string",example="https://contratos.comprasnet.gov.br/api/contrato/1/responsaveis"),
     *             @OA\Property(property="link_despesas_acessorias",type="string",example="https://contratos.comprasnet.gov.br/api/contrato/1/despesas_acessorias"),
     *             @OA\Property(property="link_faturas",type="string",example="https://contratos.comprasnet.gov.br/api/contrato/1/faturas"),
     *             @OA\Property(property="link_ocorrencias",type="string",example="https://contratos.comprasnet.gov.br/api/contrato/1/ocorrencias"),
     *             @OA\Property(property="link_terceirizados",type="string",example="https://contratos.comprasnet.gov.br/api/contrato/1/terceirizados"),
     *             @OA\Property(property="link_arquivos",type="string",example="https://contratos.comprasnet.gov.br/api/contrato/1/arquivos"),
     *         ),
     *
     *         @OA\Schema(
     *             schema="UnidadeGestora",
     *             type="object",
     *             @OA\Property(property="codigo",type="string",example="110161"),*
     *             @OA\Property(property="nome_resumido",type="string",example="SAD/DF/AGU"),
     *             @OA\Property(property="nome",type="string",example="SUPERINTENDENCIA DE ADM. NO DISTRITO FEDERAL"),
     *             @OA\Property(property="sisg",type="string",example="Sim"),
     *             @OA\Property(property="utiliza_siafi",type="string",example="Sim"),
     *             @OA\Property(property="utiliza_antecipagov",type="string",example="Sim")
     *         ),
     *
     *         @OA\Schema(
     *             schema="OrgaoOrigem",
     *             type="object",
     *             @OA\Property(property="codigo",type="string",example="63000"),
     *             @OA\Property(property="nome",type="string",example="ADVOCACIA-GERAL DA UNIAO"),
     *             @OA\Property(property="unidade_gestora_origem", type="object", ref="#/components/schemas/UnidadeGestora")
     *         ),
     *
     *
     *         @OA\Schema(
     *             schema="Orgao",
     *             type="object",
     *             @OA\Property(property="codigo",type="string",example="63000"),
     *             @OA\Property(property="nome",type="string",example="ADVOCACIA-GERAL DA UNIAO"),
     *             @OA\Property(property="unidade_gestora", type="object", ref="#/components/schemas/UnidadeGestora")
     *         ),
     *
     *         @OA\Schema(
     *             schema="Contratante",
     *             type="object",
     *             @OA\Property(property="orgao_origem", type="object", ref="#/components/schemas/OrgaoOrigem"),
     *             @OA\Property(property="orgao", type="object", ref="#/components/schemas/Orgao")
     *         ),
     *
     *         @OA\Schema(
     *             schema="Fornecedor",
     *             type="object",
     *             @OA\Property(property="tipo",type="string",example="JURIDICA"),
     *             @OA\Property(property="cnpj_cpf_idgener",type="string",example="02.341.470/0001-44"),
     *             @OA\Property(property="nome", type="string", example="RORAIMA ENERGIA S.A"),
     *         ),
     *
     *         @OA\Schema(
     *             schema="Qualificacao",
     *             type="object",
     *             @OA\Property(property="codigo",type="integer",example="2"),
     *             @OA\Property(property="descricao",type="string",example="VIGÊNCIA")
     *         ),
     *
     *         @OA\Schema(
     *             schema="Links",
     *             type="object",
     *             @OA\Property(property="historico",type="string",example="https://contratos.comprasnet.gov.br/api/contrato/2957/historico"),
     *             @OA\Property(property="empenhos",type="string",example="https://contratos.comprasnet.gov.br/api/contrato/2957/empenhos"),
     *             @OA\Property(property="cronograma",type="string",example="https://contratos.comprasnet.gov.br/api/contrato/2957/cronograma"),
     *             @OA\Property(property="garantias",type="string",example="https://contratos.comprasnet.gov.br/api/contrato/2957/garantias"),
     *             @OA\Property(property="itens",type="string",example="https://contratos.comprasnet.gov.br/api/contrato/2957/itens"),
     *             @OA\Property(property="prepostos",type="string",example="https://contratos.comprasnet.gov.br/api/contrato/2957/prepostos"),
     *             @OA\Property(property="responsaveis",type="string",example="https://contratos.comprasnet.gov.br/api/contrato/2957/responsaveis"),
     *             @OA\Property(property="despesas_acessorias",type="string",example="https://contratos.comprasnet.gov.br/api/contrato/2957/despesas_acessorias"),
     *             @OA\Property(property="faturas",type="string",example="https://contratos.comprasnet.gov.br/api/contrato/2957/faturas"),
     *             @OA\Property(property="ocorrencias",type="string",example="https://contratos.comprasnet.gov.br/api/contrato/2957/ocorrencias"),
     *             @OA\Property(property="terceirizados",type="string",example="https://contratos.comprasnet.gov.br/api/contrato/2957/terceirizados"),
     *             @OA\Property(property="arquivos",type="string",example="https://contratos.comprasnet.gov.br/api/contrato/2957/arquivos"),
     *         ),
     *
     *         @OA\Schema(
     *             schema="ContratosUnidadeGestora",
     *             type="object",
     *             @OA\Property(property="id",type="integer",example="2957"),
     *             @OA\Property(property="receita_despesa",type="string",example="Despesa"),
     *             @OA\Property(property="numero",type="string",example="00059/2009"),
     *             @OA\Property(property="contratante", type="object", ref="#/components/schemas/Contratante"),
     *             @OA\Property(property="fornecedor", type="object", ref="#/components/schemas/Fornecedor"),
     *             @OA\Property(property="codigo_tipo",type="string",example="50"),
     *             @OA\Property(property="tipo",type="string",example="Contrato"),
     *             @OA\Property(property="subtipo",type="string",example="TERMO DE COOPERAÇÃO"),
     *             @OA\Property(property="prorrogavel",type="string",example="Sim"),
     *             @OA\Property(property="situacao",type="string",example="Ativo"),
     *             @OA\Property(property="justificativa_inativo",type="string",example="Encerrado"),
     *             @OA\Property(property="categoria",type="string",example="Compras"),
     *             @OA\Property(property="subcategoria",type="string",example="null"),
     *             @OA\Property(property="unidades_requisitantes",type="string",example="null"),
     *             @OA\Property(property="processo",type="string",example="00549.001460/2008-23"),
     *             @OA\Property(property="objeto",type="string",example="O PRESENTE CONTRATO TEM POR OBJETO REGULAR, EXCLUSIVAMENTE, SEGUNDO A ESTRUTURA DA TARIFA DO GRUPO B EM BAIXA TENSÃO, O FORNECIMENTO AO CONTRATANTE, PELA BOA VISTA ENERGIA S.A., DA ENERGIA ELÉTRICA NECESSÁRIA AO FUNCIONAMENTO DE SUAS INSTALAÇÕES, LOCALIZADAS NAS UNIDADES DA AGU NO ESTADO DE RORAIMA."),
     *             @OA\Property(property="amparo_legal",type="string",example="LEI 14.133/2021 - Artigo: 75 - Inciso: II"),
     *             @OA\Property(property="informacao_complementar",type="string",example=""),
     *             @OA\Property(property="codigo_modalidade",type="string",example="05"),
     *             @OA\Property(property="modalidade",type="string",example="Pregão"),
     *             @OA\Property(property="unidade_compra",type="string",example="158128"),
     *             @OA\Property(property="licitacao_numero",type="string",example="00022/2009"),
     *             @OA\Property(property="sistema_origem_licitacao",type="string",example="null"),
     *             @OA\Property(property="data_assinatura",type="string",example="2009-06-23",format="yyyy-mm-dd"),
     *             @OA\Property(property="data_publicacao",type="string",example="2009-06-23",format="yyyy-mm-dd"),
     *             @OA\Property(property="vigencia_inicio",type="string",example="2012-06-23",format="yyyy-mm-dd"),
     *             @OA\Property(property="vigencia_fim",type="string",example="2099-06-30",format="yyyy-mm-dd"),
     *             @OA\Property(property="valor_inicial",type="number",example="43.538,12"),
     *             @OA\Property(property="valor_global",type="number",example="102.000,00"),
     *             @OA\Property(property="num_parcelas",type="integer",example="12"),
     *             @OA\Property(property="valor_parcela",type="number",example="8.500,00"),
     *             @OA\Property(property="valor_acumulado",type="number",example="8.486.798,21"),
     *             @OA\Property(property="links", type="object", ref="#/components/schemas/Links"),
     *         ),
     *
     *         @OA\Schema(
     *             schema="Historicos",
     *             type="object",
     *             @OA\Property(property="id",type="integer",example="307874"),
     *             @OA\Property(property="contrato_id",type="integer",example="61665"),
     *             @OA\Property(property="receita_despesa",type="string",example="Despesa"),
     *             @OA\Property(property="numero",type="string",example="00420/2019"),
     *             @OA\Property(property="observacao",type="string",example="CELEBRAÇÃO DO CONTRATO: 0006/2017 DE ACORDO COM PROCESSO NÚMERO: 00589.000328/2016-38"),
     *             @OA\Property(property="ug",type="string",example="110099"),
     *             @OA\Property(property="gestao",type="string",example="39252"),
     *             @OA\Property(property="fornecedor", type="object", ref="#/components/schemas/Fornecedor"),
     *             @OA\Property(property="codigo_tipo",type="string",example="55"),
     *             @OA\Property(property="tipo",type="string",example="Termo Aditivo"),
     *             @OA\Property(property="categoria",type="string",example=""),
     *             @OA\Property(property="qualificacao_termo", type="array", @OA\Items(type="object", ref="#/components/schemas/Qualificacao")),
     *             @OA\Property(property="processo",type="string",example="00589.000328/2016-38"),
     *             @OA\Property(property="objeto",type="string",example="CONTRATAÇÃO DE SERVIÇOS DE PORTEIRO/VIGIA PARA AS UNIDADES DA AGU EM OSASCO, SP, PRESIDENTE PRUDENTE, SÃO JOSÉ DO RIO PRETO, RIBEIRÃO PRETO E MARILIA, CONFORME EDITAL E SEUS ANEXOS."),
     *             @OA\Property(property="fundamento_legal_aditivo",type="string",example="null"),
     *             @OA\Property(property="informacao_complementar",type="string",example="UNIDADES PSF OSASCO, SAD SÃO PAULO (BACEUNAS), PSU PRESIDENTE PRUDENTE, PSU SÃO JOSÉ DO RIO PRETO, PSU RIBEIRÃO PRETO E PSU MARÍLIA. "),
     *             @OA\Property(property="modalidade",type="string",example="Adesão"),
     *             @OA\Property(property="licitacao_numero",type="string",example="00300/2019"),
     *             @OA\Property(property="codigo_unidade_origem",type="string",example="300201"),
     *             @OA\Property(property="nome_unidade_origem",type="string",example="SUPERINTENDENCIA REG. NO ESTADO RS - DNIT"),
     *             @OA\Property(property="data_assinatura",type="string",example="2020-01-01",format=" yyyy-mm-dd"),
     *             @OA\Property(property="data_publicacao",type="string",example="2020-01-01",format=" yyyy-mm-dd"),
     *             @OA\Property(property="vigencia_inicio",type="string",example="2020-01-01",format=" yyyy-mm-dd"),
     *             @OA\Property(property="vigencia_fim",type="string",example="2020-01-01",format=" yyyy-mm-dd"),
     *             @OA\Property(property="valor_inicial",type="number",example="1.200,25"),
     *             @OA\Property(property="valor_global",type="number",example="1.200,25"),
     *             @OA\Property(property="num_parcelas",type="integer",example="10"),
     *             @OA\Property(property="valor_parcela",type="number",example="1.200,25"),
     *             @OA\Property(property="novo_valor_global",type="number",example="1.200,25"),
     *             @OA\Property(property="novo_num_parcelas",type="integer",example="15"),
     *             @OA\Property(property="novo_valor_parcela",type="number",example="1.200,25"),
     *             @OA\Property(property="data_inicio_novo_valor",type="string",example="2020-01-01",format=" yyyy-mm-dd"),
     *             @OA\Property(property="retroativo",type="string",example="Não"),
     *             @OA\Property(property="retroativo_mesref_de",type="integer",example="01"),
     *             @OA\Property(property="retroativo_anoref_de",type="integer",example="2020"),
     *             @OA\Property(property="retroativo_mesref_ate",type="integer",example="05"),
     *             @OA\Property(property="retroativo_anoref_ate",type="integer",example="2020"),
     *             @OA\Property(property="retroativo_vencimento",type="integer",example="04"),
     *             @OA\Property(property="retroativo_valor",type="number",example="1.200,25"),
     *         ),
     *
     *          @OA\Schema(
     *             schema="Garantias",
     *             type="object",
     *             @OA\Property(property="id",type="integer",example="9062"),
     *             @OA\Property(property="contrato_id",type="integer",example="47411"),
     *             @OA\Property(property="tipo",type="string",example="Fiança Bancária"),
     *             @OA\Property(property="valor",type="number",example="70.200,25"),
     *             @OA\Property(property="vencimento",type="string",example="2021-01-01",format=" yyyy-mm-dd"),
     *         ),
     *
     *         @OA\Schema(
     *             schema="DataInicioItem",
     *             type="object",
     *             @OA\Property(property="date",type="string",example="2022-02-24 11:01:59.000000",format="yyyy-mm-dd hh:mm:ss"),
     *             @OA\Property(property="timezone_type",type="integer",example="3"),
     *             @OA\Property(property="timezone", type="string", example="America/Sao_Paulo"),
     *         ),
     *
     *          @OA\Schema(
     *             schema="Itens",
     *             type="object",
     *             @OA\Property(property="id",type="integer",example="769310"),
     *             @OA\Property(property="contrato_id",type="integer",example="122964"),
     *             @OA\Property(property="tipo_id",type="string",example="Serviço"),
     *             @OA\Property(property="tipo_material",type="string",example="Tipo Material"),
     *             @OA\Property(property="grupo_id",type="string",example="GRUPO GENERICO SERVICO"),
     *             @OA\Property(property="catmatseritem_id",type="string",example="8729 - PRESTACAO DE SERVICOS DE PORTARIA / RECEPCAO"),
     *             @OA\Property(property="descricao_complementar",type="string",example="null"),
     *             @OA\Property(property="quantidade",type="integer",example="20"),
     *             @OA\Property(property="valorunitario",type="number",example="7.163,26"),
     *             @OA\Property(property="valortotal",type="number",example="143.265,20"),
     *             @OA\Property(property="numero_item_compra",type="string",example="00001"),
     *             @OA\Property(property="data_inicio_item", type="object", ref="#/components/schemas/DataInicioItem"),
     *             @OA\Property(property="contrato_historico_id", type="array", @OA\Items(type="integer"), example={616816,429461,522753,560087,306720,458360}),
     *         ),
     *
     *          @OA\Schema(
     *             schema="Prepostos",
     *             type="object",
     *             @OA\Property(property="id",type="integer",example="2798"),
     *             @OA\Property(property="contrato_id",type="integer",example="119744"),
     *             @OA\Property(property="usuario",type="string",example="***.111.111-** FULANO DE TAL"),
     *             @OA\Property(property="email",type="string",example="email@emailpreposto.com"),
     *             @OA\Property(property="telefonefixo",type="string",example="(61) 9999-8888"),
     *             @OA\Property(property="celular",type="string",example="(61) 91234-5678"),
     *             @OA\Property(property="doc_formalizacao",type="string",example="200"),
     *             @OA\Property(property="informacao_complementar",type="string",example="Informações complementares"),
     *             @OA\Property(property="data_inicio",type="string",example="2020-01-01",format=" yyyy-mm-dd"),
     *             @OA\Property(property="data_fim",type="string",example="2020-01-01",format=" yyyy-mm-dd"),
     *             @OA\Property(property="situacao",type="string",example="Ativo"),
     *         ),
     *
     *          @OA\Schema(
     *             schema="Responsaveis",
     *             type="object",
     *             @OA\Property(property="id",type="integer",example="107213"),
     *             @OA\Property(property="contrato_id",type="integer",example="122730"),
     *             @OA\Property(property="usuario",type="string",example="***.111.111-** FULANO DE TAL"),
     *             @OA\Property(property="funcao_id",type="string",example="Fiscal Administrativo Substituto"),
     *             @OA\Property(property="instalacao_id",type="string",example="DF - Brasília - Sede I"),
     *             @OA\Property(property="portaria",type="string",example="PORTARIA Nº 80, DE 06 DE FEVEREIRO DE 2020"),
     *             @OA\Property(property="situacao",type="string",example="Ativo"),
     *             @OA\Property(property="data_inicio",type="string",example="2020-01-01",format=" yyyy-mm-dd"),
     *             @OA\Property(property="data_fim",type="string",example="2021-01-01",format=" yyyy-mm-dd")
     *         ),
     *
     *         @OA\Schema(
     *             schema="ResponsaveisV2",
     *             type="object",
     *             @OA\Property(property="contrato_id",type="integer",example="107213"),
     *             @OA\Property(property="responsaveis",type="array", @OA\Items(type="object", ref="#/components/schemas/ListaResponsaveis")),
     *         ),
     *
     *          @OA\Schema(
     *             schema="ListaResponsaveis",
     *             type="object",
     *             @OA\Property(property="nome",type="string",example="FULANO DE TAL"),
     *             @OA\Property(property="email",type="string",example="fulano@dominio.gov.br"),
     *             @OA\Property(property="funcao",type="string",example="Fiscal Técnico"),
     *             @OA\Property(property="situacao",type="string",example="Ativo"),
     *             @OA\Property(property="updated_at",type="string",example="2022-05-31 08:36:37",format="yyyy-mm-dd hh:mm:ss"),
     *         ),
     *
     *
     *          @OA\Schema(
     *             schema="DespesasAcessorias",
     *             type="object",
     *             @OA\Property(property="id",type="integer",example="1"),
     *             @OA\Property(property="contrato_id",type="integer",example="3087"),
     *             @OA\Property(property="tipo_id",type="string",example="Garantia Estendida"),
     *             @OA\Property(property="recorrencia_id",type="string",example="Mensal"),
     *             @OA\Property(property="descricao_complementar",type="string",example="DESCRIÇÃO COMPLEMENTAR"),
     *             @OA\Property(property="vencimento",type="string",example="2020-01-01",format=" yyyy-mm-dd"),
     *             @OA\Property(property="valor",type="number",example="1.200,25"),
     *         ),
     *
     *          @OA\Schema(
     *             schema="Publicações",
     *             type="object",
     *             @OA\Property(property="id",type="string",example="Atualizar"),
     *         ),
     *
     *          @OA\Schema(
     *             schema="Faturas",
     *             type="object",
     *             @OA\Property(property="id",type="integer",example="84480"),
     *             @OA\Property(property="contrato_id",type="integer",example="71378"),
     *             @OA\Property(property="tipolistafatura_id",type="string",example="PRESTAÇÃO DE SERVIÇOS"),
     *             @OA\Property(property="justificativafatura_id",type="string",example="Ordem Lista: Seguindo a ordem cronológica da lista."),
     *             @OA\Property(property="sfadrao_id",type="string",example=""),
     *             @OA\Property(property="numero",type="string",example="0572PORT/PSUSSR05"),
     *             @OA\Property(property="emissao",type="string",example="2020-01-01",format=" yyyy-mm-dd"),
     *             @OA\Property(property="prazo",type="string",example="2020-01-01",format=" yyyy-mm-dd"),
     *             @OA\Property(property="vencimento",type="string",example="2021-01-01",format=" yyyy-mm-dd"),
     *             @OA\Property(property="valor",type="number",example="16.587,52"),
     *             @OA\Property(property="juros",type="number",example="0,00"),
     *             @OA\Property(property="multa",type="number",example="0,00"),
     *             @OA\Property(property="glosa",type="number",example="0,00"),
     *             @OA\Property(property="valorliquido",type="number",example="16.587,52"),
     *             @OA\Property(property="processo",type="string",example="50600.003651/2015-20"),
     *             @OA\Property(property="protocolo",type="string",example="2020-01-01",format=" yyyy-mm-dd"),
     *             @OA\Property(property="ateste",type="string",example="2020-01-01",format=" yyyy-mm-dd"),
     *             @OA\Property(property="repactuacao",type="string",example="Sim"),
     *             @OA\Property(property="infcomplementar",type="string",example="AUTOMÁTICA"),
     *             @OA\Property(property="mesref",type="string",example="01"),
     *             @OA\Property(property="anoref",type="string",example="2020"),
     *             @OA\Property(property="situacao",type="string",example="Pendente"),
     *         ),
     *
     *          @OA\Schema(
     *             schema="FaturasPorId",
     *             type="object",
     *             @OA\Property(property="id",type="integer",example="84480"),
     *             @OA\Property(property="contrato_id",type="integer",example="71378"),
     *             @OA\Property(property="tipolistafatura_id",type="string",example="PRESTAÇÃO DE SERVIÇOS"),
     *             @OA\Property(property="justificativafatura_id",type="string",example="Ordem Lista: Seguindo a ordem cronológica da lista."),
     *             @OA\Property(property="sfadrao_id",type="string",example=""),
     *             @OA\Property(property="numero",type="string",example="0572PORT/PSUSSR05"),
     *             @OA\Property(property="emissao",type="string",example="2020-01-01",format=" yyyy-mm-dd"),
     *             @OA\Property(property="prazo",type="string",example="2020-01-01",format=" yyyy-mm-dd"),
     *             @OA\Property(property="vencimento",type="string",example="2021-01-01",format=" yyyy-mm-dd"),
     *             @OA\Property(property="valor",type="number",example="16.587,52"),
     *             @OA\Property(property="juros",type="number",example="0,00"),
     *             @OA\Property(property="multa",type="number",example="0,00"),
     *             @OA\Property(property="glosa",type="number",example="0,00"),
     *             @OA\Property(property="valorliquido",type="number",example="16.587,52"),
     *             @OA\Property(property="processo",type="string",example="50600.003651/2015-20"),
     *             @OA\Property(property="protocolo",type="string",example="2020-01-01",format=" yyyy-mm-dd"),
     *             @OA\Property(property="ateste",type="string",example="2020-01-01",format=" yyyy-mm-dd"),
     *             @OA\Property(property="repactuacao",type="string",example="Sim"),
     *             @OA\Property(property="infcomplementar",type="string",example="AUTOMÁTICA"),
     *             @OA\Property(property="mesref",type="string",example="01"),
     *             @OA\Property(property="anoref",type="string",example="2020"),
     *             @OA\Property(property="situacao",type="string",example="Pendente"),
     *             @OA\Property(property="chave_nfe",type="string",example="35–2005–01442791000129–55–001–000005917–2–17417368–1"),
     *             @OA\Property(property="dados_empenho",type="array", @OA\Items(type="object", ref="#/components/schemas/DadosEmpenho")),
     *             @OA\Property(property="dados_referencia",type="array", @OA\Items(type="object", ref="#/components/schemas/DadosReferencia")),
     *             @OA\Property(property="dados_item_faturado",type="array", @OA\Items(type="object", ref="#/components/schemas/DadosItemFaturado")),
     *         ),
     *
     *         @OA\Schema(
     *             schema="AtualizaFatura",
     *             type="object",
     *             @OA\Property(property="id_fatura", type="integer", example="39483"),
     *             @OA\Property(property="justificativa", type="integer", example="8"),
     *             @OA\Property(property="situacao", type="string", example="PEN"),
     *             @OA\Property(
     *                  property="empenhos",
     *                  type="array",
     *                  @OA\Items(type="string", example={"7182262", "7182249"})
     *             ),
     *         ),
     *
     *         @OA\Schema(
     *             schema="DadosEmpenho",
     *             type="object",
     *             @OA\Property(property="id_empenho", type="integer", example="8821459"),
     *             @OA\Property(property="numero_empenho", type="string", example="2022NE000166"),
     *             @OA\Property(property="subelemento", type="string", example="00")
     *         ),
     *
     *         @OA\Schema(
     *             schema="DadosReferencia",
     *             type="object",
     *             @OA\Property(property="mesref", type="integer", example="01"),
     *             @OA\Property(property="anoref", type="integer", example="2023"),
     *             @OA\Property(property="valorref", type="number", example="207,71"),
     *         ),
     *
     *         @OA\Schema(
     *             schema="DadosItemFaturado",
     *             type="object",
     *             @OA\Property(property="id_item_contrato", type="integer", example="926820"),
     *             @OA\Property(property="quantidade_faturado", type="number", example="3,00000"),
     *             @OA\Property(property="valorunitario_faturado", type="number", example="28,6200"),
     *             @OA\Property(property="valortotal_faturado", type="number", example="85,86"),
     *         ),
     *
     *          @OA\Schema(
     *             schema="Ocorrencias",
     *             type="object",
     *              @OA\Property(property="id",type="integer",example="20"),
     *              @OA\Property(property="contrato_id",type="integer",example="115"),
     *             @OA\Property(property="numero",type="integer",example="10"),
     *             @OA\Property(property="usuario",type="string",example="***.111.111-** FULANO DE TAL"),
     *             @OA\Property(property="data",type="string",example="2020-01-01",format=" yyyy-mm-dd"),
     *             @OA\Property(property="ocorrencia",type="string",example="EXTRATO DE CONTRATO"),
     *             @OA\Property(property="notificapreposto",type="string",example="Sim"),
     *             @OA\Property(property="emailpreposto",type="string",example="email@cconta.com"),
     *             @OA\Property(property="numeroocorrencia",type="integer",example="3"),
     *             @OA\Property(property="novasituacao",type="string",example="Atendida"),
     *             @OA\Property(property="situacao",type="string",example="Atendida Parcial"),
     *             @OA\Property(property="arquivos",type="array", @OA\Items(type="object", example="[{ 'arquivo_1': 'localhost:8000/storage/contrato/1_00420_2019/580e4da71ac02ec0ecf4f09728b51bc0.pdf'},{'arquivo_2': 'localhost:8000/storage/contrato/1_00420_2019/4e35d0c021543920a41402dfaa0ab89b.pdf'}]")),
     *         ),
     *
     *          @OA\Schema(
     *             schema="Terceirizados",
     *             type="object",
     *             @OA\Property(property="id",type="integer",example="21293"),
     *             @OA\Property(property="contrato_id",type="integer",example="59011"),
     *             @OA\Property(property="usuario",type="string",example="111.111.111-00 FULANO DE TAL"),
     *             @OA\Property(property="funcao_id",type="string",example="Ajudante"),
     *             @OA\Property(property="descricao_complementar",type="string",example="Ajudante de almoxarifado"),
     *             @OA\Property(property="jornada",type="integer",example="12"),
     *             @OA\Property(property="unidade",type="string",example="AGU-SEDE"),
     *             @OA\Property(property="salario",type="number",example="1.200,25"),
     *             @OA\Property(property="custo",type="number",example="0,00"),
     *             @OA\Property(property="escolaridade_id",type="string",example="Superior completo"),
     *             @OA\Property(property="data_inicio",type="string",example="2020-01-01",format=" yyyy-mm-dd"),
     *             @OA\Property(property="data_fim",type="string",example="2020-01-31",format=" yyyy-mm-dd"),
     *             @OA\Property(property="situacao",type="string",example="ativo",),
     *             @OA\Property(property="aux_transporte",type="number",example="190,00"),
     *             @OA\Property(property="vale_alimentacao",type="number",example="560,00")
     *         ),
     *
     *          @OA\Schema(
     *             schema="Arquivos",
     *             type="object",
     *             @OA\Property(property="id",type="integer",example="34821"),
     *             @OA\Property(property="contrato_id",type="integer",example="28281"),
     *             @OA\Property(property="tipo",type="string",example="Garantia"),
     *             @OA\Property(property="processo",type="string",example="23117.076550/2018-25"),
     *             @OA\Property(property="sequencial_documento",type="integer",example="2456656"),
     *             @OA\Property(property="descricao",type="string",example="APOLICE SEGURO GARANTIA - C 045-18"),
     *             @OA\Property(property="path_arquivo",type="string", example="https://contratos.comprasnet.gov.br/storage/contrato/28281_00045_2018/7e74013b022d25e159dc508e03fa6732.pdf"),
     *             @OA\Property(property="origem",type="string",example="SEI"),
     *             @OA\Property(property="link_sei",type="string",example="null"),
     *       ),
     *          @OA\Schema(
     *             schema="Empenhos",
     *             type="object",
     *             @OA\Property(property="id",type="integer",example="8838998"),
     *             @OA\Property(property="unidade_gestora",type="string",example="110099"),
     *             @OA\Property(property="gestao",type="string",example="00001"),
     *             @OA\Property(property="numero",type="string",example="2019NE800022"),
     *             @OA\Property(property="data_emissao",type="string",example="2022-08-05",format=" yyyy-mm-dd"),
     *             @OA\Property(property="credor",type="string",example="09.439.320/0001-17 - GLOBAL SERVICOS & COMERCIO LTDA"),
     *             @OA\Property(property="fonte_recurso",type="string",example="0100000000"),
     *             @OA\Property(property="programa_trabalho",type="string",example="null"),
     *             @OA\Property(property="planointerno",type="string",example="AGU0047 - SERVICOS DE PORTARIA"),
     *             @OA\Property(property="naturezadespesa",type="string",example="339039 - OUTROS SERVICOS DE TERCEIROS - PESSOA JURIDICA"),
     *             @OA\Property(property="empenhado",type="number",example="1.361.640,02"),
     *             @OA\Property(property="aliquidar",type="number",example="231.667,64"),
     *             @OA\Property(property="liquidado",type="number",example="0,00"),
     *             @OA\Property(property="pago",type="number",example="1.129.972,38"),
     *             @OA\Property(property="rpinscrito",type="number",example="231.667,64"),
     *             @OA\Property(property="rpaliquidar",type="number",example="128.941,72"),
     *             @OA\Property(property="rpliquidado",type="number",example="128.941,72"),
     *             @OA\Property(property="rppago",type="number",example="102.725,92"),
     *             @OA\Property(property="informacao_complementar",type="string",example="null"),
     *             @OA\Property(property="sistema_origem",type="string",example="null"),
     *             @OA\Property(property="links",type="array", @OA\Items(type="object", example="{'documento_pagamento': 'http:\/\/sta.agu.gov.br\/api\/ordembancaria\/empenho\/110099000012017NE800559'}"))
     *       ),
     *          @OA\Schema(
     *             schema="EmpenhosId",
     *             type="object",
     *             @OA\Property(property="id",type="integer",example="8167673"),
     *             @OA\Property(property="contrato_id",type="integer",example="3260"),
     *             @OA\Property(property="numero",type="string",example="2019NE800022"),
     *             @OA\Property(property="credor",type="string",example="09.439.320/0001-17 - GLOBAL SERVICOS & COMERCIO LTDA"),
     *             @OA\Property(property="fonte_recurso",type="string",example="0100000000"),
     *             @OA\Property(property="programa_trabalho",type="string",example="null"),
     *             @OA\Property(property="planointerno",type="string",example="AGU0047 - SERVICOS DE PORTARIA"),
     *             @OA\Property(property="naturezadespesa",type="string",example="339039 - OUTROS SERVICOS DE TERCEIROS - PESSOA JURIDICA"),
     *             @OA\Property(property="empenhado",type="number",example="1.361.640,02"),
     *             @OA\Property(property="aliquidar",type="number",example="231.667,64"),
     *             @OA\Property(property="liquidado",type="number",example="0,00"),
     *             @OA\Property(property="pago",type="number",example="1.129.972,38"),
     *             @OA\Property(property="rpinscrito",type="number",example="231.667,64"),
     *             @OA\Property(property="rpaliquidar",type="number",example="128.941,72"),
     *             @OA\Property(property="rpliquidado",type="number",example="128.941,72"),
     *             @OA\Property(property="rppago",type="number",example="102.725,92"),
     *             @OA\Property(property="informacao_complementar",type="string",example="null"),
     *             @OA\Property(property="sistema_origem",type="string",example="null")
     *       ),
     *
     *           @OA\Schema(
     *             schema="EmpenhosUnidadeGestora",
     *             type="object",
     *             @OA\Property(property="id",type="integer",example="8838998"),
     *             @OA\Property(property="numero",type="string",example="2019NE800022"),
     *             @OA\Property(property="data_emissao",type="string",example="2022-08-05",format=" yyyy-mm-dd"),
     *             @OA\Property(property="unidade",type="string",example="201053 - SEGES/ME"),
     *             @OA\Property(property="gestao",type="string",example="00001"),
     *             @OA\Property(property="fornecedor",type="string",example="09.439.320/0001-17 - GLOBAL SERVICOS & COMERCIO LTDA"),
     *             @OA\Property(property="fonte",type="string",example="0100000000"),
     *             @OA\Property(property="naturezadespesa",type="string",example="339039 - OUTROS SERVICOS DE TERCEIROS - PESSOA JURIDICA"),
     *             @OA\Property(property="empenhado",type="number",example="1.361.640,02"),
     *             @OA\Property(property="aliquidar",type="number",example="231.667,64"),
     *             @OA\Property(property="liquidado",type="number",example="0,00"),
     *             @OA\Property(property="pago",type="number",example="1.129.972,38"),
     *             @OA\Property(property="rpinscrito",type="number",example="231.667,64"),
     *             @OA\Property(property="rpaliquidar",type="number",example="128.941,72"),
     *             @OA\Property(property="rpliquidado",type="number",example="128.941,72"),
     *             @OA\Property(property="rppago",type="number",example="102.725,92"),
     *             @OA\Property(property="informacao_complementar",type="string",example="null"),
     *             @OA\Property(property="sistema_origem",type="string",example="null")
     *       ),
     *
     *           @OA\Schema(
     *             schema="EmpenhosV2",
     *             type="object",
     *             @OA\Property(property="id",type="integer",example="8841076"),
     *             @OA\Property(property="unidade",type="string",example="155022"),
     *             @OA\Property(property="unidade_nome",type="string",example="EBSERH HC-UFPE"),
     *             @OA\Property(property="gestao",type="string",example="26443"),
     *             @OA\Property(property="numero_empenho",type="string",example="2022NE002693"),
     *             @OA\Property(property="data_emissao",type="string",example="2022-08-05",format=" yyyy-mm-dd"),
     *             @OA\Property(property="cpf_cnpj_credor",type="string",example="52.201.456/0001-13"),
     *             @OA\Property(property="credor",type="string",example="LEICA DO BRASIL IMPORTACAO E COMERCIO LTDA."),
     *             @OA\Property(property="fonte_recurso",type="string",example="8100915107"),
     *             @OA\Property(property="naturezadespesa",type="string",example="339030"),
     *             @OA\Property(property="naturezadespesa_descricao",type="string",example="MATERIAL DE CONSUMO"),
     *             @OA\Property(property="planointerno",type="string",example="EKRRL000000"),
     *             @OA\Property(property="planointerno_descricao",type="string",example="ATUALIZAR PI"),
     *             @OA\Property(property="valor_empenhado",type="number",example="58850.12"),
     *             @OA\Property(property="valor_aliquidar",type="number",example="58850.12"),
     *             @OA\Property(property="valor_liquidado",type="number",example="0"),
     *             @OA\Property(property="valor_pago",type="number",example="0"),
     *             @OA\Property(property="valor_rpinscrito",type="number",example="0"),
     *             @OA\Property(property="valor_rpaliquidar",type="number",example="0"),
     *             @OA\Property(property="valor_rpaliquidado",type="number",example="0"),
     *             @OA\Property(property="valor_rppago",type="number",example="0"),
     *             @OA\Property(property="informacao_complementar",type="string",example="15502205000132022 - UASG Minuta: 155022"),
     *             @OA\Property(property="sistema_origem",type="string",example="COMPRASNET CONTRATOS"),
     *             @OA\Property(property="contrato_id",type="integer", example="152762"),
     *             @OA\Property(property="id_cipi",type="string", example="13505.51-42"),
     *             @OA\Property(property="created_at",type="string", example="timestamp"),
     *             @OA\Property(property="updated_at",type="string", example="timestamp"),
     *       ),
     *
     *
     *          @OA\Schema(
     *              schema="EmpenhoItem",
     *              type="object",
     *              @OA\Property(property="unidade_compra", type="string", example="200999"),
     *              @OA\Property(property="numero_ano_compra", type="string", example="00064/2023"),
     *              @OA\Property(property="descricao_item", type="string", example="SORVETE"),
     *              @OA\Property(property="codigo_item", type="integer", example=388838),
     *              @OA\Property(property="unidade_minuta", type="string", example="110161"),
     *              @OA\Property(property="modalidade_compra", type="string", example="Pregão"),
     *              @OA\Property(property="numero_empenho", type="string", example="2023NE000312"),
     *              @OA\Property(property="unidade_emitente_empenho", type="string", example="110161"),
     *              @OA\Property(property="amparo_legal", type="string", example="LEI 14.133/2021 - Artigo: 28 - Inciso: I"),
     *              @OA\Property(property="valor", type="string", example="109,00"),
     *     ),
     *
     *          @OA\Schema(
     *              schema="EmpenhoItemServico",
     *              type="object",
     *              @OA\Property(property="unidade_compra", type="string", example="110161"),
     *              @OA\Property(property="numero_ano_compra", type="string", example="00002/2023"),
     *              @OA\Property(property="descricao_item", type="string", example="LIMPEZA DE SOFÁ"),
     *              @OA\Property(property="codigo_item", type="integer", example=25909),
     *              @OA\Property(property="unidade_minuta", type="string", example="110161"),
     *              @OA\Property(property="modalidade_compra", type="string", example="Serviço"),
     *              @OA\Property(property="numero_empenho", type="string", example="2023NE000155"),
     *              @OA\Property(property="unidade_emitente_empenho", type="string", example="110161"),
     *              @OA\Property(property="amparo_legal", type="string", example="LEI 14.133/2021 - Artigo: 75 - Inciso: II"),
     *              @OA\Property(property="valor", type="string", example="250,00")
     *          ),
     *          @OA\Schema(
     *              schema="EmpenhosCodigoServico",
     *              type="object",
     *              @OA\Property(property="ano", type="string", example="2023"),
     *              @OA\Property(property="codigo", type="string", example="25909"),
     *              @OA\Property(property="valor_empenhado", type="string", example="250,00"),
     *              @OA\Property(
     *                  property="empenhos",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/EmpenhoItemServico")
     *               )
     *          ),
     *          @OA\Schema(
     *              schema="EmpenhosPDM",
     *              type="object",
     *              @OA\Property(property="valor",type="double",example="7.574,00"),
     *              @OA\Property(property="ano", type="string", example="2023"),
     *              @OA\Property(property="codigo_pdm", type="string", example="00910"),
     *              @OA\Property(property="valor_empenhado", type="string", example="109,00"),
     *              @OA\Property(
     *                  property="empenhos",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/EmpenhoItem")
     *               )
     *          ),
     *          @OA\Schema(
     *             schema="SemDados",
     *             type="object",
     *             @OA\Property(property="error",type="string",example="Não foram encontrados registros."),
     *          ),
     *          @OA\Schema(
     *             schema="Cronograma",
     *             type="object",
     *             @OA\Property(property="id",type="integer",example="26660364"),
     *             @OA\Property(property="contrato_id",type="integer",example="35795"),
     *             @OA\Property(property="tipo",type="string",example="Termo Aditivo"),
     *             @OA\Property(property="numero",type="string",example="00003/2021"),
     *             @OA\Property(property="receita_despesa",type="string",example="Despesa"),
     *             @OA\Property(property="observacao",type="string",example="CONTRATAÇÃO DE SERVIÇOS DE RECOLHIMENTO DE RESÍDUOS CLASSE II A( LIXO EXTRAORDINÁRIO)"),
     *             @OA\Property(property="mesref",type="integer",example="10"),
     *             @OA\Property(property="anoref",type="integer",example="2017"),
     *             @OA\Property(property="vencimento",type="string",example="2017-05-01",format="yyyy-mm-dd"),
     *             @OA\Property(property="retroativo",type="string",example="Não"),
     *             @OA\Property(property="valor",type="number",example="185,05"),
     *       ),
     *          @OA\Schema(
     *             schema="OrgaoId",
     *             type="object",
     *             @OA\Property(property="codigo",type="integer",example="14000"),
     *       ),
     *
     *          @OA\Schema(
     *             schema="FalhaAutenticação",
     *             type="object",
     *             @OA\Property(property="status",type="string",example="Authorization Token not found."),
     *       ),
     *
     *          @OA\Schema(
     *             schema="Unidade",
     *             type="object",
     *             @OA\Property(property="codigo",type="string",example="070001"),
     *       ),
     *          @OA\Schema(
     *             schema="Usuarios",
     *             type="object",
     *             @OA\Property(property="cpf",type="string",example="111.111.111-11"),
     *             @OA\Property(property="nome",type="string",example="Fulano de Souza"),
     *             @OA\Property(property="email",type="string",example="fulano@email.com"),
     *             @OA\Property(property="ugprimaria",type="string",example="110161"),
     *             @OA\Property(property="ugssecundarias",type="array", @OA\Items(type="object", example="{['110001','020001','170200','110203']}")),
     *             @OA\Property(property="perfis",type="array", @OA\Items(type="object", example="{['Setor Contratos','Administrador Unidade','Administrador','Acesso API']}")),
     *             @OA\Property(property="situacao",type="string",example="Ativo"),
     *         ),
     *
     *        @OA\Schema(
     *             schema="UsuariosUgPorPerfil",
     *             type="object",
     *             @OA\Property(property="unidade_id",type="integer",example="123456"),
     *             @OA\Property(property="perfil",type="string",example="Execução Financeira"),
     *             @OA\Property(property="usuarios",type="array", @OA\Items(type="object", ref="#/components/schemas/ListaUsuariosUgPerfil")),
     *         ),
     *
     *       @OA\Schema(
     *             schema="ListaUsuariosUgPerfil",
     *             type="object",
     *             @OA\Property(property="nome",type="string",example="FULANO DE TAL"),
     *             @OA\Property(property="email",type="string",example="fulano@dominio.gov.br"),
     *             @OA\Property(property="situacao",type="string",example="Ativo"),
     *             @OA\Property(property="updated_at",type="string",example="2022-05-31 08:36:37",format="yyyy-mm-dd hh:mm:ss"),
     *         ),
     *
     *         @OA\Schema(
     *             schema="Minutas",
     *             type="object",
     *             @OA\Property(property="minutaempenho_id",type="integer",example="162292"),
     *             @OA\Property(property="minutaempenho_data_criacao",type="string",example="2021-03-11 10:25:09", format="yyyy-mm-dd hh:mm:ss"),
     *             @OA\Property(property="minutaempenho_tipo_empenho",type="string",example="Compra"),
     *             @OA\Property(property="minutaempenho_unidade_id",type="integer",example="9983"),
     *             @OA\Property(property="minutaempenho_mensagem_siafi",type="string", example="2021NE000064"),
     *             @OA\Property(property="contrato_id",type="integer", example="110885"),
     *             @OA\Property(property="compra_id",type="integer",example="97647"),
     *             @OA\Property(property="numero_empenho",type="string",example="2021NE000042"),
     *             @OA\Property(property="orgao_nome",type="string",example="FUNDO DO EXERCITO"),
     *             @OA\Property(property="orgao_codigo",type="integer",example="52904"),
     *             @OA\Property(property="unidade_id",type="integer",example="35852"),
     *             @OA\Property(property="unidade_codigo",type="integer",example="167245"),
     *             @OA\Property(property="unidade_nomeresumido",type="string",example="POL MIL NITEROI"),
     *             @OA\Property(property="unidade_nomecompleto",type="string",example="POLICLINICA MILITAR DE NITEROI"),
     *             @OA\Property(property="fornecedor_compra_nome",type="string",example="HELENO CONSTRUCOES EIRELI"),
     *             @OA\Property(property="fornecedor_compra_cpf_cnpj_idgener",type="string",example="20.856.023/0001-00"),
     *             @OA\Property(property="fornecedor_empenho_nome",type="string",example="HELENO CONSTRUCOES EIRELI"),
     *             @OA\Property(property="fornecedor_empenho_cpf_cnpj_idgener",type="string",example="20.856.023/0001-00"),
     *             @OA\Property(property="empenho_numero",type="string",example="2021NE000042"),
     *             @OA\Property(property="empenho_valor_empenhado",type="number",example="1.870.000,00"),
     *             @OA\Property(property="empenho_valor_a_liquidar",type="number",example="1.474.434,41"),
     *             @OA\Property(property="empenho_valor_liquidado",type="number",example="40.962,27"),
     *             @OA\Property(property="empenho_valor_pago",type="number",example="354.603,32"),
     *             @OA\Property(property="empenho_valor_rpinscrito",type="number",example="0,00"),
     *             @OA\Property(property="empenho_valor_rpaliquidar",type="number",example="0,00"),
     *             @OA\Property(property="empenho_valor_rpliquidado",type="number",example="0,00"),
     *             @OA\Property(property="empenho_valor_rppago",type="number",example="0,00"),
     *             @OA\Property(property="empenho_unidade_id",type="integer",example="35852"),
     *             @OA\Property(property="empenho_id",type="integer",example="7410565"),
     *             @OA\Property(property="naturezadespesa_codigo",type="string",example="339039"),
     *             @OA\Property(property="naturezadespesa_descricao",type="string",example="OUTROS SERVICOS DE TERCEIROS - PESSOA JURIDICA"),
     *             @OA\Property(property="contrato_numero",type="string",example="00004/2021"),
     *             @OA\Property(property="uasg_compra",type="integer",example="160245"),
     *             @OA\Property(property="uasg_compra_subrogada",type="integer",example="null"),
     *             @OA\Property(property="numero_compra",type="string",example="00001/2020"),
     *             @OA\Property(property="tipo_compra",type="string",example="Regime Diferenciado de Contratações"),
     *         ),
     *
     *         @OA\Schema(
     *             schema="TokenBearer",
     *             type="object",
     *             @OA\Property(property="access_token",type="string",example="token"),
     *             @OA\Property(property="token_type",type="string",example="bearer"),
     *             @OA\Property(property="expires_in",type="integer",example="3600"),
     *       ),
     *
     *      @OA\Schema(
     *             schema="Publicacoes",
     *             type="object",
     *             @OA\Property(property="id",type="integer",example="322951"),
     *             @OA\Property(property="contratohistorico_id",type="integer",example="263306"),
     *             @OA\Property(property="data_publicacao",type="string",example="2021-11-24",format="yyyy-mm-dd"),
     *             @OA\Property(property="status_publicacao_id",type="string",example="02"),
     *             @OA\Property(property="status",type="string",example="PUBLICADA"),
     *             @OA\Property(property="texto_dou",type="string",example="##ATO EXTRATO DE CONTRATO Nº 3/2021 - UASG 153155 - UFRJ/ME\r\n\r\nNº Processo: 23079.217997/2021-60.\r\n\r\n##TEX Pregão Nº 6/2021. Contratante: INSTITUTO PUERIC. PED MAT. GESTEIRA DA UFRJ.\r\nContratado: 27.500.404/0001-09 - W A SIQUEIRA ENGENHARIA LTDA. Objeto: Serviço de apoio administrativo.\r\nFundamento Legal: LEI 10.520 / 2002 - Artigo: 1. Vigência: 01/07/2021 a 30/06/2022. Valor Total: R$ 501.770,16. Data de Assinatura: 01/07/2021.\r\n\r\n##OFI (COMPRASNET 4.0 - 23/11/2021)."),
     *             @OA\Property(property="link_publicacao",type="string",example="http://pesquisa.in.gov.br/imprensa/jsp/visualiza/index.jsp?jornal=530&pagina=107&data=24/11/2021"),
     *       ),
     *
     *       @OA\Schema(
     *             schema="ContratoPorId",
     *             type="object",
     *             @OA\Property(property="id",type="integer",example="7602"),
     *             @OA\Property(property="receita_despesa",type="string",example="Despesa"),
     *             @OA\Property(property="numero",type="string",example="00030/2013"),
     *             @OA\Property(property="orgao_codigo",type="integer",example="36213"),
     *             @OA\Property(property="orgao_nome",type="string",example="AGENCIA NACIONAL DE SAUDE SUPLEMENTAR"),
     *             @OA\Property(property="unidade_codigo",type="integer",example="253003"),
     *             @OA\Property(property="gestao",type="string",example="253003"),
     *             @OA\Property(property="unidade_nome_resumido",type="string",example="ANS"),
     *             @OA\Property(property="unidade_nome",type="string",example="AGENCIA NACIONAL DE SAUDE SUPLEMENTAR"),
     *             @OA\Property(property="unidade_origem_codigo",type="integer",example="253034"),
     *             @OA\Property(property="unidade_origem_nome",type="string",example="AGENCIA NACIONAL DE SAUDE S. - SAO PAULO"),
     *             @OA\Property(property="fornecedor_tipo",type="string",example="JURIDICA"),
     *             @OA\Property(property="fonecedor_cnpj_cpf_idgener",type="string",example="03.555.423/0001-66"),
     *             @OA\Property(property="fornecedor_nome",type="string",example="SALIBA PARTICIPACOES LTDA."),
     *             @OA\Property(property="codigo_tipo",type="integer",example="50"),
     *             @OA\Property(property="tipo",type="string",example="Contrato"),
     *             @OA\Property(property="categoria",type="string",example="Locação Imóveis"),
     *             @OA\Property(property="processo",type="string",example="33902.568502/2012-71"),
     *             @OA\Property(property="objeto",type="string",example="LOCACAO DOS CONJUNTOS COMERCIAIS DO 9 ANDAR DO EDIFICIO RACHID SALIBA, LOCALIZADO NA RUA BELA CINTRA, 986 - SAO PAULO/SP. COM AREA TOTAL DE 764,54M2 E DIREITO DE USO DE 08 VAGAS DE GARAGEM."),
     *             @OA\Property(property="fundamento_legal",type="string",example="LEI 8666/93, LEI 8245/91, LEI 9961/00, DECRETO 7689/12, P"),
     *             @OA\Property(property="informacao_complementar",type="string",example="null"),
     *             @OA\Property(property="codigo_modalidade",type="string",example="06"),
     *             @OA\Property(property="modalidade",type="string",example="Dispensa"),
     *             @OA\Property(property="unidade_compra",type="integer",example="253003"),
     *             @OA\Property(property="licitacao_numero",type="string",example="00184/2013"),
     *             @OA\Property(property="sistema_origem_licitacao",type="string",example="null"),
     *             @OA\Property(property="data_assinatura",type="string",example="2013-08-12",format="yyyy-mm-dd"),
     *             @OA\Property(property="data_publicacao",type="string",example="2013-10-03",format="yyyy-mm-dd"),
     *             @OA\Property(property="vigencia_inicio",type="string",example="2013-08-12",format="yyyy-mm-dd"),
     *             @OA\Property(property="vigencia_fim",type="string",example="2021-11-30",format="yyyy-mm-dd"),
     *             @OA\Property(property="valor_inicial",type="number",example="3.669.792,00"),
     *             @OA\Property(property="valor_global",type="number",example="3.669.792,00"),
     *             @OA\Property(property="num_parcelas",type="integer",example="1"),
     *             @OA\Property(property="valor_parcela",type="number",example="3.669.792,00"),
     *             @OA\Property(property="valor_acumulado",type="number",example="7.339.584,00"),
     *             @OA\Property(property="situacao",type="string",example="Inativo"),
     *         ),
     *
     *         @OA\Schema(
     *             schema="MinutasUasgAno",
     *             type="array", @OA\Items(type="object", ref="#/components/schemas/Minuta")),
     *         ),
     *
     * @OA\Schema(
     *             schema="Minuta",
     *             type="object",
     *             @OA\Property(property="id_minuta",type="integer",example="14136"),
     *             @OA\Property(property="tipo_minuta",type="string",example="Contrato"),
     *             @OA\Property(property="uasg_minuta",type="integer",example="200350"),
     *             @OA\Property(property="data_emissao_minuta", type="string",example="2021-03-11", format="yyyy-mm-dd"),
     *             @OA\Property(property="ug_emitente",type="integer", example="200350"),
     *             @OA\Property(property="numero_empenho",type="string", example="2021NE000058"),
     *             @OA\Property(property="cpf_cnpj_fornecedor_empenho",type="string",example="04.664.903/0001-28"),
     *             @OA\Property(property="nome_fornecedor_empenho",type="string",example="FCIA VETER PET LTDA"),
     *             @OA\Property(property="tipo_empenho",type="string",example="Estimativo"),
     *             @OA\Property(property="uasg_compra",type="integer",example="200350"),
     *             @OA\Property(property="modalidade_compra",type="string",example="Pregão"),
     *             @OA\Property(property="numero_compra",type="string",example="00004/2019"),
     *             @OA\Property(property="uasg_beneficiaria",type="integer",example="null"),
     *             @OA\Property(property="numero_contrato",type="string",example="00004/2019"),
     *             @OA\Property(property="chave",type="string",example="20035005000042019"),
     *             @OA\Property(property="valor_total_minuta",type="number",example="1.980,98"),
     *             @OA\Property(property="itens_minuta",type="array", @OA\Items(type="object", ref="#/components/schemas/ItensMinuta")),
     *         ),
     *
     * @OA\Schema(
     *             schema="ItensMinuta",
     *             type="object",
     *             @OA\Property(property="sequencial_siafi", type="integer", example="1"),
     *             @OA\Property(property="numero_item_compra", type="integer", example="00013"),
     *             @OA\Property(property="codigo_item", type="integer", example="439541"),
     *             @OA\Property(property="descricao", type="string", example="VACINA"),
     *             @OA\Property(property="descricao_detalhada", type="string", example="null"),
     *             @OA\Property(property="quantidade", type="number", example="2,00000"),
     *             @OA\Property(property="valor_unitario", type="number", example="40,0000"),
     *             @OA\Property(property="valor_total", type="number", example="80,00"),
     *         ),
     *
     * @OA\Schema(
     *             schema="ItensCompra",
     *             type="object",
     *             @OA\Property(property="itens",type="array",example={  {"numeroItem": "00001",
     *                                                                    "possuiImpeditivo": true,
     *                                                                    "textoImpeditivo": "Existe Ata de Registro de Preços vinculado(a) ao item no Compras Contratos. Favor verificar no sistema.",
     *                                                                    "url": "https://contratos.comprasnet.gov.br/api/v1/compra/233/item/853/show"
     *                                                                    },
     *                                                                   {"numeroItem": "00002",
     *                                                                    "possuiImpeditivo": true,
     *                                                                    "textoImpeditivo": "Existe Ata de Registro de Preços vinculado(a) ao item no Compras Contratos. Favor verificar no sistema.",
     *                                                                    "url": "https://contratos.comprasnet.gov.br/api/v1/compra/233/item/854/show"
     *                                                                   },
     *                                                                   {"numeroItem": "00003",
     *                                                                    "possuiImpeditivo": false,
     *                                                                    "textoImpeditivo": "",
     *                                                                    "url": ""
     *                                                                   },
     *                                                                   {"numeroItem": "00004",
     *                                                                    "possuiImpeditivo": true,
     *                                                                    "textoImpeditivo": "Existe Contrato vinculado(a) ao item no Compras Contratos. Favor verificar no sistema.",
     *                                                                    "url": "https://contratos.comprasnet.gov.br/api/v1/compra/233/item/1179/show"
     *                                                                   },
     *                                                                   {"numeroItem": "00005",
     *                                                                    "possuiImpeditivo": true,
     *                                                                    "textoImpeditivo": "Existe Empenho vinculado(a) ao item no Compras Contratos. Favor verificar no sistema.",
     *                                                                    "url": "https://contratos.comprasnet.gov.br/api/v1/compra/233/item/861/show"
     *                                                                   },
     *                                                                }, @OA\Items(type="object", ref="#/components/schemas/DetalheItensCompra")),
     *
     *         ),
     *
     * @OA\Schema(
     *             schema="DetalheItensCompra",
     *             type="object",
     *             @OA\Property(property="numeroItem",type="string"),
     *             @OA\Property(property="possuiImpeditivo",type="boolean"),
     *             @OA\Property(property="textoImpeditivo",type="string"),
     *             @OA\Property(property="url",type="string"),
     *         ),
     *
     * @OA\Schema(
     *             schema="DomicilioBancario",
     *             type="object",
     *             @OA\Property(property="unidade_gestora",type="string",example="158128"),
     *             @OA\Property(property="contrato_id",type="integer",example="107213"),
     *             @OA\Property(property="contrato",type="string",example="00030/2023"),
     *             @OA\Property(property="domiciliosbancarios",type="array", @OA\Items(type="object", ref="#/components/schemas/ListaDomicilioBancario")),
     *         ),
     *
     * @OA\Schema(
     *             schema="ListaDomicilioBancario",
     *             type="object",
     *             @OA\Property(property="id",type="integer",example="1"),
     *             @OA\Property(property="situacao",type="string",example="Registrado"),
     *             @OA\Property(property="banco",type="string",example="001"),
     *             @OA\Property(property="agencia",type="string",example="0256"),
     *             @OA\Property(property="conta_bancaria",type="string",example="78899"),
     *             @OA\Property(property="codigo_operacao_credito",type="string",example="0001"),
     *             @OA\Property(property="data_registro",type="string",example="31/10/2023 14:11:23",format="dd/mm/yyyy hh:mm:ss"),
     *         ),
     *
     * @OA\Schema(
     *             schema="EmpenhosComContratosVinculados",
     *             type="object",
     *             @OA\Property(property="id_documento_empenho",type="string",example="158147264022023NE800055"),
     *             @OA\Property(property="contrato_id",type="integer",example="107213"),
     *         ),
     *
     * )
     *
     */

}
