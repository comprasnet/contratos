<?php

namespace App\Http\Controllers\Api;

use App\Models\Contrato;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Codigoitem;
use App\Models\Contratohistorico;
use Illuminate\Support\Facades\Log;

class TermosPorTipoController extends Controller
{
    public function index(Request $request)
    {
        $tipo = $request['q'];
        ($tipo == 'Termo Apostilamento') ? $tipo = 'Termo de Apostilamento' : '';
        ($tipo == 'Termo Rescisão')      ? $tipo = 'Termo de Rescisão'      : '';

        $id_contrato = $request['contrato_id'];
        $consulta = ContratoHistorico::where('contrato_id', $id_contrato)
            ->where('elaboracao', false);

        //caso seja rascunho, retornar o contrato direto
        if ($request['elaboracao'] === '1') {
            $consulta = Contrato::where('id', $id_contrato)
                ->where('elaboracao', true);
        }

        if ($tipo == 'Contrato') {
            $consulta = $consulta->select('id', 'numero')
                ->orderBy('created_at')
                ->limit(1)
                ->get();
        } else {
            $consulta = $consulta->select('id', 'numero')
                ->whereHas('tipo', function ($query) use ($tipo) {
                    $query->where('descricao', $tipo);
                })
                ->get();
        }

        if ($consulta) {
            return $consulta;
        }

        return [];

//        $search_term = $request->input('q');
//        $form = collect($request->input('form'))->pluck('value', 'name');
//
//        $options = Contratohistorico::query();
//
//        // if no category has been selected, show no options
//        //if (!$form['tipo']) {
//        if (!$request->input('tipo')) {
//            return [];
//        }
//
//        // if a category has been selected, only show articles in that category
////        if ($form['tipo']) {
//        if ($request->input('tipo')) {
////            $tipo = Codigoitem::find($form['tipo'])->descricao;
//            $tipo = Codigoitem::find($request->input('tipo'))->descricao;
//            if(in_array($tipo, config('app.tipos_obrigatorios_arquivo'))){
//                ($tipo == 'Termo Apostilamento') ? $tipo = 'Termo de Apostilamento': '';
//                ($tipo == 'Termo Rescisão') ? $tipo = 'Termo de Rescisão': '';
//
//                $options = $options->whereIn('tipo_id',function($q) use ($tipo){
//                    $q->select('id')->from('codigoitens')->where('descricao',$tipo);
//                })->where('contrato_id', $request->input('contrato_id'));
////dd($options, $tipo, $request->input('contrato_id'));
//            }else{
//                return [];
//            }
//        }
//
//
//
//        if ($search_term) {
//            return $options->where('numero', 'ilike', '%' . strtoupper($search_term) . '%')
//            ->orderBy('numero')
//            ->paginate(10);
//        }
//        return $options->paginate(10);
    }

    public function show($id)
    {
        return Contratohistorico::where('contratohistorico.elaboracao', false)->find($id);
    }
}
