<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegistrarAntecipagovRequest as StoreRequest;
use App\Http\Requests\AlterarAntecipagovRequest as UpdateRequest;
use App\Http\Traits\AntecipaGovTrait;
use App\Models\AntecipaGov;
use Exception;
////use App\Http\Requests\LiquidarAntecipagovRequest as LiquidarRequest;
////use App\Http\Requests\CancelarAntecipagovRequest as CancelarRequest;
use Illuminate\Support\Facades\DB;

class AntecipaGovController extends Controller
{
   
    use AntecipaGovTrait;
    private $descCodItem = null;
    
    public function registrar(StoreRequest $request)
    {
        $this->descCodItem = 'Registrado';
        $params = $request->toArray();

        $retornoAcao = $this->validarRequest($params,$this->descCodItem);
        
        $executarAcao = $this->salvarRequest($params, $retornoAcao,$this->descCodItem);

        return response()->json([ 'operation' => $executarAcao['operation'], 'message' =>  $executarAcao['message'] ], $executarAcao['code'] ) ;
    }

    public function liquidar(UpdateRequest $request)
    {
        $this->descCodItem = 'Liquidado';
        $params = $request->toArray();
        
        $retornoAcao = $this->validarRequest($params,$this->descCodItem);
        
        $executarAcao = $this->salvarRequest($params, $retornoAcao,$this->descCodItem);

        return response()->json([ 'operation' => $executarAcao['operation'], 'message' =>  $executarAcao['message'] ], $executarAcao['code'] ) ;
    }

    public function cancelar(UpdateRequest $request)
    {
        $this->descCodItem = 'Cancelado';
        $params = $request->toArray();
        
        $retornoAcao = $this->validarRequest($params,$this->descCodItem);
        
        $executarAcao = $this->salvarRequest($params, $retornoAcao,$this->descCodItem);

        return response()->json([ 'operation' => $executarAcao['operation'], 'message' =>  $executarAcao['message'] ], $executarAcao['code'] ) ;
    }

    public function alterar(UpdateRequest $request)
    {
        $this->descCodItem = 'Alterado';
        $params = $request->toArray();
        
        $retornoAcao = $this->validarRequest($params,$this->descCodItem);
        
        $executarAcao = $this->salvarRequest($params, $retornoAcao,$this->descCodItem);

        return response()->json([ 'operation' => $executarAcao['operation'], 'message' =>  $executarAcao['message'] ], $executarAcao['code'] ) ;
    }

}
