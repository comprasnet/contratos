<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Artisan;

class CkeditorController extends Controller
{

    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request)
    {
        if ($request->hasFile('upload')) {

            //resolvendo pasta com nome: mes_ano atual
            $atual = Carbon::now();
            $nomePastaAtual = $atual->year . '_' . $atual->month;
            $caminho = 'app/public/imagens_comunica/' . $nomePastaAtual;

            if ($request->hasFile('upload')) {

                if (!File::exists(storage_path('app/public'))) {
                    File::makeDirectory(storage_path('app/public'));
                }
                if (!File::exists(storage_path('app/public/imagens_comunica'))) {
                    File::makeDirectory(storage_path('app/public/imagens_comunica'));
                }
                if (!File::exists(storage_path($caminho))) {
                    File::makeDirectory(storage_path($caminho));
                }

                //verificar se existe a public
                if (is_dir(base_path('public'))) {
                    //se existe, verificar se a storage está linkada
                    if (!File::exists(public_path('storage'))) {
                        Artisan::call('storage:link');
                    }
                } else {
                    //se não existe, crie a public e linke a storage
                    File::makeDirectory(base_path('public'));
                    Artisan::call('storage:link');
                }

                $originName = $request->file('upload')->getClientOriginalName();
                $fileName = pathinfo($originName, PATHINFO_FILENAME);
                $extension = $request->file('upload')->getClientOriginalExtension();
                $fileName = $fileName . '_' . time() . '.' . $extension;

                $request->file('upload')->move(storage_path($caminho), $fileName);


                $CKEditorFuncNum = $request->input('CKEditorFuncNum');

                $url = asset('storage/imagens_comunica/' . $nomePastaAtual . '/' . $fileName);

                $msg = 'Imagem salva com sucesso!';
                $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";

                @header('Content-type: text/html; charset=utf-8');
                echo $response;
            }
        }
    }
}
