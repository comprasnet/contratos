<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

class IBGE
{

    private $client;

    public function __construct()
    {
        $this->client = new Client([
            'verify' => false,
            'base_uri' => 'https://servicodados.ibge.gov.br/api/v1/'
        ]);

    }

    public function paises()
    {
        $response = $this->client->request('GET', 'paises');

        return json_decode($response->getBody()->getContents());
    }

}
