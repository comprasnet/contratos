<?php

namespace App\Http\Controllers;

use App\Http\Traits\Formatador;
use App\Http\Traits\LogTrait;
use App\Models\SfCertificado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DownloadsController extends Controller
{
    use Formatador;
    use LogTrait;

    public function contrato(string $pasta, string $file, ?string $nomeArquivo = null)
    {
        $caminhoArquivo  = env('APP_PATH')."storage/app/contrato/{$pasta}/{$file}";

        if (! file_exists($caminhoArquivo)) {

            return redirect()
                ->back()
                ->with('error', 'Arquivo não existe!')
                ->withInput();
        }

        if($nomeArquivo){

            $nomeArquivo = $this->montarNomeArquivoDownload($caminhoArquivo, $nomeArquivo);

            return response()->download($caminhoArquivo,$nomeArquivo);

        }

        return response()->download($caminhoArquivo,$nomeArquivo );

    }

    public function anexoscomunica($file)
    {

        if (! file_exists(env('APP_PATH')."storage/app/comunica/anexos/".$file)) {

            return redirect()
                ->back()
                ->with('error', 'Arquivo não existe!')
                ->withInput();
        }

        return Storage::download('comunica/anexos/'.$file);
    }

    public function anexosocorrencia($path,$file)
    {
        if (! file_exists(env('APP_PATH')."storage/app/ocorrencia/".$path."/".$file)) {

            return redirect()
                ->back()
                ->with('error', 'Arquivo não existe!')
                ->withInput();
        }

        return Storage::download('ocorrencia/'.$path."/".$file);
    }

    public function declaration($file)
    {
        if (! file_exists(env('APP_PATH')."storage/app/declaration/".$file)) {
            return redirect()
                ->back()
                ->with('error', 'Arquivo não existe!')
                ->withInput();
        }

        return Storage::download('declaration/'.$file);
    }

    public function importacao($path,$file)
    {
        if (! file_exists(env('APP_PATH')."storage/app/importacao/".$path."/".$file)) {

            return redirect()
                ->back()
                ->with('error', 'Arquivo não existe!')
                ->withInput();
        }

        return Storage::download('importacao/'.$path."/".$file);
    }

    /**
     * Define o WSDL localmente,
     * considerando o corte definido na variável de ambiente ANO_SIAFI_CORTE.
     *
     * @return void
     */
    public function setWsdl(): void
    {
        $ano = (int)date('Y');
        $anoSiafiCorte = env('ANO_SIAFI_CORTE', 1);

        for ($i = 0; $i < $anoSiafiCorte; $i++) {
            $this->downloadWsdl($ano - $i);
        }
    }

    /**
     * Baixa para a máquina o wsdl
     *
     * @return void
     */
    public function downloadWsdl($exercicio)
    {
        $amb = config('app.ambiente_siafi');
        try {

            $wsdlUrls = [
                'ORCAMENTARIO' => 'https://servicos-siafi.tesouro.gov.br/siafi' . $exercicio . '/services/orcamentario/manterOrcamentario?wsdl',
                'CPR' => 'https://servicos-siafi.tesouro.gov.br/siafi' . $exercicio . '/services/cpr/manterContasPagarReceber?wsdl',
                'CONSULTA' => 'https://servicos-siafi.tesouro.gov.br/siafi' . $exercicio . '/services/tabelas/consultarTabelasAdministrativas?wsdl',
            ];

            if ($amb == 'HOM') {
                $wsdlUrls = [
                    'ORCAMENTARIO' => 'https://homextservicos-siafi.tesouro.gov.br/siafi' . $exercicio . 'he/services/orcamentario/manterOrcamentario?wsdl',
                    'CPR' => 'https://homextservicos-siafi.tesouro.gov.br/siafi' . $exercicio . 'he/services/cpr/manterContasPagarReceber?wsdl',
                    'CONSULTA' => 'https://homextservicos-siafi.tesouro.gov.br/siafi' . $exercicio . 'he/services/tabelas/consultarTabelasAdministrativas?wsdl',
                ];
            }

            foreach ($wsdlUrls as $wsdl) {
                $this->baixaESalvaWsdl($wsdl, $exercicio);
            }

        } catch (\Exception $e) {
            $this->inserirLogCustomizado('erro-wsdl', 'error', $e);
        }
    }

    public function baixaESalvaWsdl(string $wsdl, $exercicio): void
    {

        $nomeArquivo = preg_match('/\/([^\/\?]+)\?/', $wsdl, $matches) ? $matches[1] : null;

        $certificado = SfCertificado::where('situacao', '=', 1)->orderBy('id', 'desc')->first();

        $dado = null;
        foreach ($certificado->chaveprivada as $c) {
            $dado = explode('/', $c);
        }
        $chave = $dado[2];

        $dado = null;
        foreach ($certificado->certificado as $c) {
            $dado = explode('/', $c);
        }
        $cert = $dado[2];

        //certificado
        $key = config('app.app_path') . config('app.app_path_cert') . $chave;
        $crtkey = config('app.app_path') . config('app.app_path_cert') . $cert;

        $context = stream_context_create([
            'ssl' => [
                'local_cert' => $crtkey,
                'local_pk' => $key,
                'verify_peer' => false,
                'passphrase' => base64_decode($certificado->senhacertificado)
            ]
        ]);

        /*$client = new \SoapClient($wsdl, [
            'trace' => 1,
            'stream_context' => $context,
        ]);*/

        // Salva o WSDL localmente
        $caminho = "wsdl/{$exercicio}_{$nomeArquivo}.wsdl";
        $conteudo = file_get_contents($wsdl, false, $context);
        Storage::put($caminho, $conteudo);
    }
}
