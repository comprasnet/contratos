<?php

namespace App\Http\Controllers;

use App\Models\WsdlViewer;
use Illuminate\Routing\Controller;

class WsdlController extends Controller
{
    public function index()
    {
        if (backpack_user()->can('wsdl_listar')) {
            $this->data['files'] = WsdlViewer::getFiles(true, true);
            $this->data['title'] = 'Gerenciador de WSDL';
            return view('backpack::wsdl', $this->data);
        }
        abort('403', config('app.erro_permissao'));
    }

    public function download($file_name)
    {
        if (backpack_user()->can('wsdl_baixar')) {
            return response()->download(WsdlViewer::pathToLogFile(decrypt($file_name)));
        }
    }

}
