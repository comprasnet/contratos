<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\Log;

class RFB
{

    private $url;
    private $username;
    private $password;

    private $client;

    private $access_token;
    private $cpf;

    public function __construct($cpf = null)
    {
        $this->url = env('API_CONSULTA_RECEITA_HOST');
        $this->username = env('API_CONSULTA_RECEITA_USUARIO');
        $this->password = env('API_CONSULTA_RECEITA_SENHA');
        $this->cpf = $cpf;

        $this->client = new Client([
            'verify' => false,
        ]);

        $this->token();
    }

    /**
     * Retorna os dados de situação fornecedor nacional da receita federal
     * @param $identificador
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function dadosCompletosEmpresa($identificador)
    {
        $response = $this->client->request('GET', "{$this->url}/api-cnpj-empresa/v2/empresa/{$identificador}", [
            RequestOptions::HEADERS => [
                'Authorization' => "Bearer {$this->access_token}",
                'x-cpf-usuario' => $this->cpf ?? env('API_CONSULTA_RECEITA_CPF_PADRAO')
            ]
        ]);

        return json_decode($response->getBody()->getContents());
    }

    /**
     * Autentica ao rfb e retorna o token
     * @return boolean
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function token()
    {
        if ($this->access_token == null) {
            $credentials = base64_encode("{$this->username}:{$this->password}");

            $response = $this->client->request('POST', "{$this->url}/oauth2/jwt-token", [
                RequestOptions::HEADERS => [
                    'Authorization' => "Basic $credentials",
                ],
                RequestOptions::FORM_PARAMS => [
                    'grant_type' => 'client_credentials',
                ]
            ]);

            $response = json_decode($response->getBody()->getContents());
            $this->access_token = $response->access_token;

            return $response;
        }

        return true;
    }

}
