<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\Log;

class SICAF
{

    private $url;
    private $username;
    private $password;
    private $env;

    private $client;

    private $access_token;
    private $scope;
    private $token_type;
    private $expires_in;

    public function __construct()
    {
        $this->url = config('app.SICAF_HOST');
        $this->username = config('app.SICAF_USUARIO');
        $this->password = config('app.SICAF_SENHA');
        $this->env = config('app.SICAF_ENV');

        $this->client = new Client([
            'verify' => false,
        ]);

        $this->token();
    }

    /**
     * Retorna os dados de situação dos níveis de Credenciamento (I a VI) de um fornecedor nacional
     * @param $identificador
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function dadosSituacaoNivel($identificador)
    {
        $response = $this->client->request('GET', "{$this->url}/{$this->env}/v1/dadossituacaonivel/{$identificador}", [
            RequestOptions::HEADERS => [
                'Authorization' => "Bearer {$this->access_token}",
            ]
        ]);

        return json_decode($response->getBody()->getContents());
    }

    public function regularidadeFiscalEconomicaFinanceira($identificador)
    {
        try {
            $response = $this->client->request('GET', "{$this->url}/{$this->env}/v1/regularidadefiscaleconomicafinanceira/{$identificador}", [
                RequestOptions::HEADERS => [
                    'Authorization' => "Bearer {$this->access_token}",
                ]
            ]);
            return json_decode($response->getBody()->getContents());
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            return $e->getCode();
        }
    }

    /**
     * @throws GuzzleException
     */
    public function arquivo($codigoArquivo, $hashArquivo)
    {
        $response = $this->client->request('GET', "{$this->url}/{$this->env}/v1/arquivo/$codigoArquivo", [
            RequestOptions::HEADERS => [
                'Authorization' => "Bearer {$this->access_token}",
            ],
            RequestOptions::QUERY => [
                'hashArquivo' => $hashArquivo,
            ]
        ]);

        return $response->getBody()->getContents();
    }

    /**
     * @throws GuzzleException
     */
    public function dadosAquivo($codigoArquivo)
    {
        $response = $this->client->request('GET', "{$this->url}/{$this->env}/v1/dadosarquivo/$codigoArquivo", [
            RequestOptions::HEADERS => [
                'Authorization' => "Bearer {$this->access_token}",
            ]
        ]);

        return json_decode($response->getBody()->getContents());
    }

    /**
     * Autentica ao sicaf e retorna o token
     * @return boolean
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function token()
    {
        if ($this->access_token == null) {
            try {

                $credentials = base64_encode("{$this->username}:{$this->password}");

                $response = $this->client->request('POST', "{$this->url}/token", [
                    RequestOptions::HEADERS => [
                        'Authorization' => "Basic $credentials",
                    ],
                    RequestOptions::FORM_PARAMS => [
                        'grant_type' => 'client_credentials',
                    ]
                ]);
                $response = json_decode($response->getBody()->getContents());

                $this->access_token = $response->access_token;
                $this->scope = $response->scope;
                $this->token_type = $response->token_type;
                $this->expires_in = $response->expires_in;

                return $response;
            } catch (ServerException|ClientException $e) {
                Log::info($e->getMessage());
            }
            return true;
        }
    }

    public function getAccessToken()
    {
        return $this->access_token;
    }

    /**
     * @throws GuzzleException
     */
    public function dadosBasicos($identificador)
    {

        $response = $this->client->request('GET', "{$this->url}/{$this->env}/v1/dadosbasicos/{$identificador}", [
            RequestOptions::HEADERS => [
                'Authorization' => "Bearer {$this->access_token}",
            ]
        ]);

        return json_decode($response->getBody()->getContents());
    }

}
