<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AutorizacaoExecucaoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'contrato' => [
                'contrato_id' => $this->contrato_id,
                'numero_contrato' => $this->contrato->numero,
            ],
            'fornecedor' => [
                'id' => $this->contrato->fornecedor->id,
                'tipo' => $this->contrato->fornecedor->tipo_fornecedor,
                'cnpj_cpf_idgener' => $this->contrato->fornecedor->cpf_cnpj_idgener,
                'nome' => $this->contrato->fornecedor->nome,
            ],
            'numero_processo_sei' => $this->processo,
            'tipo' => $this->tipo->descricao,
            'numero' => $this->numero,
            'data_assinatura' => $this->data_assinatura,
            'vigencia_inicio' => $this->data_vigencia_inicio,
            'vigencia_fim' => $this->data_vigencia_fim,
            'numero_ordem_sistema_origem' => $this->numero_sistema_origem,
            'unidades_requisitantes' => $this->getUnidadesRequisitantesByAttribute(["id" => "id",
                                                                                   "codigo" => "codigo",
                                                                                   "nomeresumido" => "nome"
                                                                                   ]),
            'empenhos' => $this->getEmpenhosByAttribute(["id" => "id",
                                                        "numero" => "numero",
                                                        "unidade_id" => "unidade_id",
                                                        "unidade.codigo" => "codigo_unidade",
                                                        "empenhado" => "empenhado",
                                                        "aliquidar" => "aliquidar",
                                                        "liquidado" => "liquidado",
                                                        "pago" => "pago"
                                                        ]),
            'itens' => $this->getItens(),
            'valor_total' => number_format($this->valor_total, 2, ',', '.'),
            'informacoes_complementares' => $this->clearTagOnString($this->informacoes_complementares),
            'situacao' => $this->situacao->descricao,
        ];
    }

    //Limpa string tageada.
    private function clearTagOnString($texto)
    {
        $texto = preg_replace('/&[a-zA-Z0-9#]+;/', '', strip_tags($texto));
        $texto = str_replace(array("\r", "\n"), ' ', $texto);
        $texto = trim(preg_replace('/\s+/', ' ', $texto));

        return $texto;
    }

}
