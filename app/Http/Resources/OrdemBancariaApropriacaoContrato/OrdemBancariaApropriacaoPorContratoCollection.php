<?php

namespace App\Http\Resources\OrdemBancariaApropriacaoContrato;

use Illuminate\Http\Resources\Json\ResourceCollection;

class OrdemBancariaApropriacaoPorContratoCollection extends ResourceCollection
{

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */

    public function toArray($request)
    {
        return [
            'contrato' => ContratoResource::collection($this->collection),
        ];
    }
}
