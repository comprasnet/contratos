<?php

namespace App\Http\Resources\OrdemBancariaApropriacaoContrato;

use App\Http\Controllers\Api\OrdemBancaria\OrdemBancariaApropriacaoController;
use App\Http\Resources\OrdemBancaria\OrdemBancariaApropriacaoResource;
use App\services\STA\STAOrdemBancariaService;
use Illuminate\Http\Resources\Json\JsonResource;

class ContratoResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */

    public function toArray($request)
    {
        $faturas = $this->getFaturas();
        $array_apropriacao = [];
        $array_inst_cobranca = [];

        $array_apropriacao = $this->getArrayApropriacao($faturas, $array_apropriacao);

        $array_inst_cobranca = $this->getArrayInstCobranca($faturas, $array_inst_cobranca);

        return [
            "numero_contrato" => $this->numero,
            "id_contrato" => $this->id,
            "antecipa_gov" => $this->verificarSePossuiDomicilioBancario(),
            'apropriacao' => $array_apropriacao,
            'inst_cobranca' => $array_inst_cobranca,
        ];
    }

    /**
     * @param array $array_inst_cobranca
     * @return array
     */
    public function getArrayInstCobranca($faturas, array $array_inst_cobranca): array
    {
        $documentosCobranca = $this->getDocumentosLoteInstCobranca($faturas);

        $ordensBancarias = new STAOrdemBancariaService();
        $ordensBancariasLote = $ordensBancarias->cadastraOrdemBancariaPorLote($documentosCobranca);

        if (isset($ordensBancariasLote) && sizeof($ordensBancariasLote) > 0) {

            foreach ($faturas as $fatura) {
                if (isset($fatura['faturasSemApropriacao'])) {

                    foreach ($fatura['faturasSemApropriacao'] as $faturaSemApropriacao) {
                        if (in_array($faturaSemApropriacao['id'], $ordensBancariasLote)) {

                            $dadosInstrumentoCobranca = [
                                'codigo' => $this->unidade->codigo,
                                'gestao' => $this->unidade->gestao,
                                'emissao' => $faturaSemApropriacao['emissao'],
                                'id_inst_cobranca' => $faturaSemApropriacao['id']
                            ];
                            $ordensBancarias = app(OrdemBancariaApropriacaoController::class)
                                ->consultaOrdemBancariaInstrumentoCobranca($dadosInstrumentoCobranca);

                            if ($ordensBancarias && isset($ordensBancarias->original)) {
                                $array_ordem_bancaria_inst_cobranca = [];

                                $array_ordem_bancaria_inst_cobranca = $this->getOrdemBancaria($ordensBancarias, $array_ordem_bancaria_inst_cobranca);

                                if (!empty($array_ordem_bancaria_inst_cobranca)) {
                                    $array_inst_cobranca[] = new InstrumentoCobrancaResource($faturaSemApropriacao, $array_ordem_bancaria_inst_cobranca);
                                }
                            }
                        }
                    }
                }
            }
        }
        return $array_inst_cobranca;
    }

    /**
     * @return mixed
     */
    public function getFaturas()
    {
        return $this->faturas->flatMap(function ($fatura) {
            return $fatura->apropriacaoContratoFaturasApropriacao->map(function ($apropriacaoFatura) use ($fatura) {
                return [
                    'id_apropriacao' => $apropriacaoFatura->apropriacao->id,
                    'valor' => $apropriacaoFatura->apropriacao->valor,
                    'fase_id' => $apropriacaoFatura->apropriacao->fase_id,
                    'faturas' => [
                        [
                            'id' => $fatura->id,
                            'numero' => $fatura->numero,
                            'situacao' => $fatura->situacao,
                        ]
                    ],
                    'sfpadrao' => [
                        'id' => $apropriacaoFatura->sfpadrao->id ?? null,
                        'numdh' => $apropriacaoFatura->sfpadrao->numdh ?? null,
                    ],
                ];
            });
        })->groupBy('id_apropriacao') // Agrupa faturas por apropriação
        ->map(function ($apropriacoes) {
            $firstApropriacao = $apropriacoes->first();
            // Mescla todas as faturas para a mesma apropriação
            $faturas = $apropriacoes->flatMap(function ($apropriacao) {
                return $apropriacao['faturas'];
            });

            return [
                'id_apropriacao' => $firstApropriacao['id_apropriacao'],
                'valor' => $firstApropriacao['valor'],
                'fase_id' => $firstApropriacao['fase_id'],
                'faturas' => $faturas->unique('id'),
                'sfpadrao' => $firstApropriacao['sfpadrao'],
            ];
        })// Adiciona faturas sem apropriacao
        ->concat(
            $this->faturas->filter(function ($fatura) {
                return $fatura->apropriacaoContratoFaturasApropriacao->isEmpty();
            })->map(function ($fatura) {
                return [
                    'faturasSemApropriacao' => [
                        [
                            'id' => $fatura->id,
                            'numero' => $fatura->numero,
                            'situacao' => $fatura->situacao,
                            'emissao' => $fatura->emissao,
                        ]
                    ]
                ];
            })
        )->values();
    }

    /**
     * @param $faturas
     * @param array $array_apropriacao
     * @return array
     */
    public function getArrayApropriacao($faturas, array $array_apropriacao): array
    {
        $montaLoteApropriacao = collect($faturas)->map(function ($apropriacao) {
            if (isset($apropriacao['sfpadrao']['numdh']) && $apropriacao['sfpadrao']['numdh']) {
                return [
                    'id_apropriacao' => $apropriacao['id_apropriacao'],
                    'id_sfpadrao' => $apropriacao['sfpadrao']['id']
                ];
            }
            return null;
        })->filter()->values()->toArray();

        $ordensBancarias = new STAOrdemBancariaService();
        $ordensBancariasLote = $ordensBancarias->retornaNumeroFaturaOBLote($montaLoteApropriacao);

        if (isset($ordensBancariasLote) && sizeof($ordensBancariasLote) > 0) {
            foreach ($faturas as $apropriacao) {

                $existeOB = in_array($apropriacao['id_apropriacao'] ?? null, $ordensBancariasLote);
                if ($existeOB) {

                    $ordensBancarias = app(OrdemBancariaApropriacaoController::class)
                        ->consultaOrdemBancaria($apropriacao['id_apropriacao'], false);

                    if ($ordensBancarias->original) {
                        $array_ordemBancaria = [];

                        $array_ordemBancaria = $this->getOrdemBancaria($ordensBancarias, $array_ordemBancaria);

                        if (!empty($array_ordemBancaria)) {
                            $array_apropriacao[] = new ApropriacaoResource($apropriacao, $array_ordemBancaria);
                        }
                    }
                }
            }
        }
        return $array_apropriacao;
    }

    /**
     * @param \Illuminate\Http\JsonResponse $ordensBancarias
     * @param array $array_ordemBancaria
     * @return array
     */
    public function getOrdemBancaria(\Illuminate\Http\JsonResponse $ordensBancarias, array $array_ordemBancaria): array
    {
        foreach ($ordensBancarias->original as $ordemBancaria) {
            if (is_object($ordemBancaria)) {
                $array_ordemBancaria[] = new OrdemBancariaApropriacaoResource($ordemBancaria);
            }
        }
        return $array_ordemBancaria;
    }

    /**
     * @param $faturas
     * @return array
     */
    public function getDocumentosLoteInstCobranca($faturas): array
    {
        $documentosCobranca = collect($faturas)->flatMap(function ($fatura) {
            // Verifica se existem faturas sem apropriação
            if (isset($fatura['faturasSemApropriacao'])) {
                // Mapeia cada fatura sem apropriação
                return collect($fatura['faturasSemApropriacao'])->map(function ($faturaSemApropriacao) {
                    //if ($faturaSemApropriacao['situacao'] == 'PGO') {
                        return [
                            'documentoOrigem' => $this->unidade->codigo .
                                $this->unidade->gestao .
                                date('Y', strtotime($faturaSemApropriacao['emissao'])) .
                                'IC' .
                                str_pad($faturaSemApropriacao['id'], 6, "0", STR_PAD_LEFT),
                            'id_sfpadrao' => null,
                            'id_inst_cobranca' => $faturaSemApropriacao['id']
                        ];
                    //}
                    //return null;
                })->filter();
            }
            return [];
        })->values()->toArray();
        return $documentosCobranca;
    }
}
