<?php

namespace App\Http\Resources\OrdemBancariaApropriacaoContrato;

use App\Http\Controllers\Api\OrdemBancaria\OrdemBancariaApropriacaoController;
use App\Http\Resources\OrdemBancaria\OrdemBancariaApropriacaoResource;
use App\Http\Traits\BuscaCodigoItens;
use Illuminate\Http\Resources\Json\JsonResource;

class InstCobrancaApropriacaoResource extends JsonResource
{
    use BuscaCodigoItens;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */

    public function toArray($request)
    {
        return [
            'id_inst_cobranca' => $this['id'],
            'numero_inst_cobranca' => $this['numero'],
            'situacao_inst_cobranca' => $this->retornaDescricaoPorDescres(trim($this['situacao']))
        ];
    }
}
