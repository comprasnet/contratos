<?php

namespace App\Http\Resources\OrdemBancariaApropriacaoContrato;

use App\Http\Traits\BuscaCodigoItens;
use Illuminate\Http\Resources\Json\JsonResource;

class ApropriacaoResource extends JsonResource
{
    use BuscaCodigoItens;

    protected $ordensBancarias;

    public function __construct($resource, $ordensBancarias = [])
    {
        parent::__construct($resource);
        $this->ordensBancarias = $ordensBancarias;
    }

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */

    public function toArray($request)
    {
        $array_inst_cobranca = [];

        foreach ($this['faturas'] as $apropriacao) {
            if (!empty($apropriacao)) {
                $array_inst_cobranca[] = new InstCobrancaApropriacaoResource($apropriacao);
            }
        }

        return [
            'id_apropriacao' => $this['id_apropriacao'],
            'numero_dh_siafi' => $this['sfpadrao']['numdh'],
            'situacao_apropriacao' => $this->retornaDescCodigoItem(trim($this['fase_id'])),
            'inst_cobranca' => $array_inst_cobranca,
            'ordem_bancaria_apropriacao' => $this->ordensBancarias
        ];
    }
}
