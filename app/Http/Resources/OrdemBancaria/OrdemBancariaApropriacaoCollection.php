<?php

namespace App\Http\Resources\OrdemBancaria;

use Illuminate\Http\Resources\Json\ResourceCollection;

class OrdemBancariaApropriacaoCollection extends ResourceCollection
{

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */

    public function toArray($request)
    {
        return [
            'ordem_bancaria' => OrdemBancariaApropriacaoResource::collection($this->collection),
        ];
    }
}
