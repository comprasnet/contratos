<?php

namespace App\Http\Resources\OrdemBancaria;

use App\Http\Traits\BuscaCodigoItens;
use Illuminate\Http\Resources\Json\JsonResource;

class OrdemBancariaApropriacaoResource extends JsonResource
{
    use BuscaCodigoItens;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */

    public function toArray($request)
    {
        return [
            'conta_bancaria' => $this->domicilioBancario->conta,
            'agencia' => $this->domicilioBancario->agencia,
            'banco' => $this->domicilioBancario->banco,
            'numero_ob' => $this->numero,
            'numero_cancelamento_ob' => $this->numeroobcancelamento,
            'data_emissao' => $this->emissao,
            'valor_ob' => $this->valor_nao_formatado,
            'observacao' => $this->observacao,
        ];
    }
}
