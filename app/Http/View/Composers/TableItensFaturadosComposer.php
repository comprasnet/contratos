<?php

namespace App\Http\View\Composers;

use App\Models\Contratofatura;
use Illuminate\View\View;
use Yajra\DataTables\Html\Builder;

class TableItensFaturadosComposer
{
    public function __construct(Builder $htmlBuilder)
    {
        $this->htmlBuilder = $htmlBuilder;
    }

    /**
     * Bind data to the view.
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        $contrato_id = \Route::current()->parameter('contrato_id');
        $fatura_id = \Route::current()->parameter('fatura') ?? null;
        $contratoItems = request()->old('contratoItems') ?? request()->post('contratoItems');
        /*
        $contratoItems = $fatura_id
            ? Contratofatura::find($fatura_id)->contratohistorico_id
            : request()->old('contratohistorico_id') ??
            key($view->getData()['fields']['contratohistorico_id']['options']->toArray()) ??
            null
        */

        $html = $this->retornaGridItens($this->htmlBuilder, $contrato_id, $view, $fatura_id, $contratoItems);

        $view->with(compact('html'));

    }

    /**
     * @param $htmlBuilder
     * @param $contrato_id
     * @return mixed
     */
    private function retornaGridItens($htmlBuilder, $contrato_id, $view, $fatura_id, $contratoItems)
    {
        $old_values_itens_faturados = $this->verificaFormataOldValuesItensFaturados($contratoItems);

        $rota = route('crud.faturas.contratoitens.ajax', ['contrato_id' => $contrato_id]);

        $html = $htmlBuilder->addColumn([
            'data' => 'tipo_id',
            'name' => 'tipo_id',
            'title' => 'Tipo Item',
        ])->addColumn([
            'data' => 'numero_item_compra',
            'name' => 'numero_item_compra',
            'title' => 'Número Item Compra',
        ])->addColumn([
            'data' => 'catmatseritem_id',
            'name' => 'catmatseritem_id',
            'title' => 'Item',
        ])->addColumn([
            'data' => 'quantidade',
            'name' => 'quantidade',
            'title' => 'Quantidade Contratada',
        ])->addColumn([
            'data' => 'valorunitario',
            'name' => 'valorunitario',
            'title' => 'Valor Unitário Contratado',
        ])->addColumn([
            'data' => 'quantidade_faturado',
            'name' => 'quantidade_faturado',
            'title' => 'Quantidade Faturada <span class="required"></span>',
        ])->addColumn([
            'data' => 'valorunitario_faturado',
            'name' => 'valorunitario_faturado',
            'title' => 'Valor Unitário Faturado  <span class="required"></span>',
        ])->addColumn([
            'data' => 'valortotal_faturado',
            'name' => 'valortotal_faturado',
            'title' => 'Valor Total',
        ])->addColumn([
            'data' => 'paisfabricacao_id',
            'name' => 'paisfabricacao_id',
            'title' => 'País de Fabricação <span class="required"></span>',
        ])->ajax([
            'url' => $rota,
            'type' => 'POST',
            'data' => [
                'old_values_itens_faturados' => json_encode($old_values_itens_faturados),
                'actionMethod' => $view->crud->getActionMethod(),
                'fatura_id' => $fatura_id,
                'contratoItems' => $contratoItems,
            ],
        ])
            ->parameters([
                'processing' => true,
                'serverSide' => true,
                'responsive' => true,
                'info' => true,
                'order' => false,
                'autoWidth' => false,
                'bAutoWidth' => false,
                'paging' => false,
                'lengthChange' => true,
                'lengthMenu' => [
                    [100, 200, -1],
                    ['100', '200', 'Todos']
                ],
                'ordering' => false,
                'language' => [
                    'url' => asset('/json/pt_br.json')
                ],
            ]);

        return $html;
    }

    /**
     * Formata os campos referentes a aba de itens faturados formata o old values dele para que seja possivel exibir
     * em caso que o request retorne de alguma Rule
     * @return array|null
     */
    private function verificaFormataOldValuesItensFaturados($contratoItems)
    {
        $old_quantidade_faturado = request()->old('quantidade_faturado') ?? null;
        $old_valorunitario_faturado = request()->old('valorunitario_faturado') ?? null;
        $old_valortotal_faturado = request()->old('valortotal_faturado') ?? null;
        $old_paisfabricacao_id = request()->old('paisfabricacao_id') ?? null;

        return [
            'old_quantidade_faturado' => $old_quantidade_faturado,
            'old_valorunitario_faturado' => $old_valorunitario_faturado,
            'old_valortotal_faturado' => $old_valortotal_faturado,
            'old_contratoitems' => $contratoItems,
            'old_paisfabricacao_id' => $old_paisfabricacao_id
        ];
    }
}
