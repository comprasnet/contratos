<?php

namespace App\Http\View\Composers;

use App\Models\ContratoFaturaEmpenho;
use App\Models\ContratoFaturaMesAno;
use Illuminate\View\View;

class EmpenhoTableComposer
{

    public function __construct()
    {

    }

    /**
     * Bind data to the view.
     * Formata campos de mesref anoref e valor para o formato esperado pelo campo json default do backpack
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        //verifica se é edicao do campo fatura_mes_ano_table
        if ($view->action === 'edit') {
            //add elemento para guardar os ids_contratofaturasempenho
            $view->field['ids_contratofaturasempenho'] = [];

            //recupera dados de COntratoFaturaMesAno pelo id da fatura
            $contratofaturasmesano = ContratoFaturaEmpenho::where('contratofatura_id', $view->crud->entry->getKey())->get();

            //inicia formatacao de string para ser adicionada ao value do input
            $strMesValor = '[';


            foreach ($contratofaturasmesano as $key => $value) {
                //decodifica string json
                $data = [];
                //formata o campo valor ref com mascara
                $data['empenhovalorref'] = number_format($value['valorref'], 2, ",", ".");
                $data['empenho_id'] = $value->empenho_id;
                $data['text'] = $value->empenho->descricao();
                $data['numeroEmpenho'] = $value->empenho->numero;
                $data['codUg'] = $value->empenho->unidade->codigosiafi ?: $value->empenho->unidade->codigo;
                $data['empenhosubelemento'] = $value->subelemento ? $value->subelemento->id : null;
                $data['textSubelemento'] = $value->subelemento ?  "{$value->subelemento->codigo} - {$value->subelemento->descricao}" : null;

                $strMesValor .= json_encode($data);

                if (($key + 1) < count($contratofaturasmesano)) {
                    $strMesValor .= ',';
                }

                //insere inputs hidden com os ids de contratofaturasempenho
                array_push($view->field['ids_contratofaturasempenho'], $value->id);
            }

            $strMesValor .= ']';

            $view->field['value'] = $strMesValor ?? null;
        }
    }

}
