<?php

namespace App\Http\View\Composers;

use App\Models\ContratoFaturaMesAno;
use Illuminate\View\View;

class FaturaMesAnoTableComposer
{
    public function __construct()
    {

    }

    /**
     * Bind data to the view.
     * Formata campos de mesref anoref e valor para o formato esperado pelo campo json default do backpack
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        //verifica se é edicao do campo fatura_mes_ano_table
        if ($view->action === 'edit') {
            //add elemento para guardar os ids_contratofaturasmesano
            $view->field['ids_contratofaturasmesano'] = [];

            //recupera dados de COntratoFaturaMesAno pelo id da fatura
            $contratofaturasmesano = ContratoFaturaMesAno::where('contratofaturas_id', $view->crud->entry->getKey())->get()->toArray();

            //inicia formatacao de string para ser adicionada ao value do input
            $strMesAnoValor = '[';


            foreach ($contratofaturasmesano as $key => $value) {
                //decodifica string json
                $jsonFormat = json_decode($value['mesref_anoref_valor_json']);
                //formata o campo valor ref com mascara
                $jsonFormat->valorref = number_format($jsonFormat->valorref, 2, ",", ".");
                $jsonFormat->idcontratofaturasmesano = $value['id'];
                //retorna string para uma representação json
                $strMesAnoValor .= json_encode($jsonFormat);

                if (($key + 1) < count($contratofaturasmesano)) {
                    $strMesAnoValor .= ',';
                }

                //insere inputs hidden com os ids de contratofaturasmesano
                array_push($view->field['ids_contratofaturasmesano'], $value['id']);
            }

            $strMesAnoValor .= ']';

            $view->field['value'] = $strMesAnoValor ?? null;
        }
    }
}
