<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class ContratoLocalExecucaoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'descricao' => 'required|min:5|max:255',
            'numero' => 'required|min:1|max:10',
            'cep' => 'required|min:8',
            'bairro' => 'required|min:1|max:70',
            'logradouro' => 'required|min:1|max:100',
            'municipios_id' => 'required',
            'complemento' => 'max:255'
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [

        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'descricao.required' => 'O campo Descrição é obrigatório.',
            'municipios_id.required' => 'O campo Localidade/UF é obrigatório.',
            'numero.required' => 'O campo Número é obrigatório.',
        ];
    }
}
