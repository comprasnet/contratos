<?php

namespace App\Http\Requests;

use App\Rules\NaoAceitarFeriado;
use App\Rules\NaoAceitarFimDeSemana;
use Illuminate\Foundation\Http\FormRequest;

class ContratoPublicacaoMenuFerramentaSaveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'data_publicacao' => [
                'required',
                new NaoAceitarFeriado(), new NaoAceitarFimDeSemana()
            ],
            'texto_dou' => 'required',
            'cpf' => "required|same:cpf_logado",
        ];
    }


    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'data_publicacao' => 'Data Publicação',
            'texto_dou' => 'Texto DOU',
            'cpf' => 'CPF',
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'data_publicacao.required' => 'O campo :attribute é obrigatório.',
            'texto_dou.required' => 'O campo :attribute é obrigatório.',
            'cpf.required' => 'O campo :attribute é obrigatório.',
            'cpf.same' => 'O  :attribute informado é diferente do CPF do usuário logado.'
        ];
    }
}
