<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\Codigoitem;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ExecsfsituacaoCamposVariaveisRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'execsfsituacao_id' => 'required',
            'aba' => 'required',
            'campo' => 'required',
            'tipo' => 'required|min:5|max:255',
            'mascara' => Rule::requiredIf(function () {
                if (substr($this->campo,0,8) == 'txtinscr') {
                        return false;
                }
                return true;
            }),
            'rotulo' => 'required|min:5|max:255',
            'restricao' => 'required|min:5|max:255',
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
