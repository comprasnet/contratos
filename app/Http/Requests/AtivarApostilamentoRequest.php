<?php

namespace App\Http\Requests;

use App\Models\AmparoLegal;
use App\Models\Contrato;

use App\Http\Requests\Request;
use App\Http\Traits\RegrasDataPublicacao;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Unique;
use App\Rules\ValidarDataAssinaturaAditivo;

class AtivarApostilamentoRequest extends FormRequest
{

    use RegrasDataPublicacao;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $id = $this->id ?? "NULL";
        $contrato_id = $this->contrato_id ?? "NULL";
        $tipo_id = $this->tipo_id ?? "NULL";
        $data_assinatura = $this->data_assinatura ?? NULL;

        $this->hoje = date('Y-m-d');

        if($contrato_id != "NULL") {
            $contrato = Contrato::find($contrato_id);
            $ids = array_column($contrato->getAmparosAttribute(), 'id');
            $amparos = AmparoLegal::verificarLei14133($ids);
            $rules['data_publicacao'] = $this->ruleDataPublicacao($tipo_id, $this->id, $amparos);
        }


        if( !$this->ruleDataAssinatura($contrato_id, $tipo_id, $id) ){
            $rules['data_assinatura'] = 'required|max:2';       // coloquei o max, para criticar o campo. A mensagem está no método messages.
        } else {
            // aqui a regra será como era anteriormente.
            $rules['data_assinatura'] = [
                'required',
                'date',
                "before_or_equal:{$this->hoje}"
            ];
        }
        $rules['data_inicio_novo_valor'] = "required|date|after_or_equal:data_assinatura";
        return $rules;
    }

    public function ruleDataAssinatura($contrato_id, $tipo_id, $id){
        if( $contrato_id != 'NULL' && $contrato_id != 'null' && !is_null($contrato_id) ){
            $objContrato = Contrato::where('id', $contrato_id)->first();
            // buscar o instrumento inicial do contrato
            $objInstrumentoInicial = $objContrato->getInstrumentoInicial();
            $dataAssinaturaInstrumentoInicial = $objInstrumentoInicial->data_assinatura;
            $dataAssinaturaApostilamento = $this->request->get('data_assinatura');

            return strtotime($dataAssinaturaApostilamento) >= strtotime($dataAssinaturaInstrumentoInicial);
        }
        return true;
    }



    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'data_publicacao' => 'Data da Publicação',
            'data_assinatura' => 'Data Assinatura Apostilamento'
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'numero.max' => "Ano do Termo de Apostilamento não permitido ",
            'data_assinatura.max' => "A data de assinatura do Termo deve ser igual ou posterior à data de assinatura do Contrato ",
            'data_publicacao.required_if' => 'O campo :attribute é obrigatório quando o termo de apostilamento é Ativo.',
            'data_assinatura.required_if' => 'O campo :attribute é obrigatório quando o termo de apostilamento é Ativo.',
        ];
    }
}
