<?php

namespace App\Http\Requests;

use App\Models\AmparoLegal;
use App\Models\Contrato;

use App\Http\Requests\Request;
use App\Http\Traits\RegrasDataPublicacao;
use App\Rules\ValidarDataAssinaturaAditivo;
use App\Rules\ValidarDataVigenciaFimAditivo;
use App\Rules\ValidarDataVigenciaInicioAditivo;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Unique;
use App\Models\Codigoitem;

class AditivoRequest extends FormRequest
{
    use RegrasDataPublicacao;

    protected $data_limite;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $amparos = false;
        $id = $this->id ?? "NULL";
        $tipo_id = $this->tipo_id ?? "NULL";
        $contrato_id = $this->contrato_id ?? "NULL";
        $this->data_limite = date('Y-m-d', strtotime('+50 year'));
        $this->hoje = date('Y-m-d');
        $this->data_amanha = date('Y-m-d', strtotime('+1 day'));
        $data_assinatura = $this->data_assinatura ?? null;
        $vigencia_inicio = $this->vigencia_inicio ?? null;
        $vigencia_fim    = $this->vigencia_fim    ?? null;
        $data_fim_vigencia_contrato = null;

        if($contrato_id != "NULL") {
            $contrato = Contrato::find($contrato_id);
            $data_fim_vigencia_contrato = date('Y-m-d', strtotime($contrato->vigencia_fim . ' +1 day'));

            $ids = array_column($contrato->getAmparosAttribute(), 'id');

            $amparos = AmparoLegal::verificarLei14133($ids);
//            clock($ids);
        }


        $rules = [
            'fornecedor_id' => 'required',
            'contrato_id' => 'required',
            'unidade_id' => 'required',
            'vigencia_inicio' => [
                'required',
                'date',
                'before_or_equal:vigencia_fim',
                'after_or_equal:data_assinatura',
                new ValidarDataVigenciaInicioAditivo($data_fim_vigencia_contrato, $vigencia_inicio)
            ],
            'vigencia_fim' => [
                'required',
                'date',
                'after_or_equal:vigencia_inicio',
                "before:{$this->data_limite}",
                new ValidarDataVigenciaFimAditivo($vigencia_fim, $data_assinatura)
            ],
            'valor_global' => 'required',
            'num_parcelas' => 'required', 'valor_parcela' => 'required',
            'observacao' => 'required',
            'retroativo' => 'required',
            'qualificacoes' => 'required',
            'retroativo_mesref_de' => 'required_if:retroativo,==,1',
            'retroativo_anoref_de' => 'required_if:retroativo,==,1',
            'retroativo_mesref_ate' => 'required_if:retroativo,==,1',
            'retroativo_anoref_ate' => 'required_if:retroativo,==,1',
            'retroativo_vencimento' => 'required_if:retroativo,==,1',
            'retroativo_valor' => 'required_if:retroativo,==,1', //ver com Schoolofnet como exigir que o valor seja maior que 0 quando tiver retroativo.
        ];

        // As regras estão em RegrasDataPublicacao.php e são compartilhadas por outras chamadas.
        if($contrato_id != "NULL") {
            $rules['data_publicacao'] = $this->ruleDataPublicacao($tipo_id, $this->id, $amparos, true);
        }

        //regras novo campo data_inicio_novo_valor
        if ($this->qualificacoes) {
            foreach ($this->qualificacoes as $qualificacao) {
                $codigo_item = Codigoitem::where('id', $qualificacao)->get()->toArray();
                if ($codigo_item[0]['descricao'] == "REAJUSTE" || $this->is_com_valor == 1) {
                    $rules['data_inicio_novo_valor'] = ['required', 'after_or_equal:data_assinatura', function ($attribute, $value, $fail) {
                        $vInicio = $this->input('vigencia_inicio');
                        $vFim = $this->input('vigencia_fim');

                        if ($value < $vInicio || $value > $vFim) {
                            $fail("O campo Data Início Novo Valor deve estar entre a Data de Vigência Início e Data" .
                                " Vigência Fim do Aditivo");
                        }
                    }];
                }
            }
        }

        // Tratar a regra da data de assinatura do termo aditivo
        if (!$this->ruleDataAssinatura($contrato_id, $tipo_id, $id)) {
            $rules['data_assinatura'] = 'required|max:2';       // coloquei o max, para criticar o campo. A mensagem está no método messages.
        } else {
            // aqui a regra será como era anteriormente.
            $data_assinatura = $this->data_assinatura ?? null;
            $vigencia_inicio = $this->vigencia_inicio ?? null;
            $vigencia_fim    = $this->vigencia_fim    ?? null;
            $rules['data_assinatura'] = [
                'required',
                ( new ValidarDataAssinaturaAditivo($data_assinatura, $vigencia_inicio, $vigencia_fim) ),
                'date',
                "before_or_equal:{$this->hoje}"
            ];

        }


        // Tratar a regra do número do termo aditivo
        if (!$this->ruleNumero($contrato_id, $tipo_id, $id)) {
            $rules['numero'] = 'required|max:2';       // coloquei o max, para criticar o campo. A mensagem está no método messages.
        } else {
            $rules['numero'] = [
                'required',
                (new Unique('contratohistorico', 'numero'))
                    ->where('contrato_id', $contrato_id)
                    ->where('tipo_id', $tipo_id)
                    ->ignore($id)
            ];
        }
        return $rules;
    }


    // Tratar a regra da data de assinautra do termo aditivo - não pode ser anterior à data de assinatura do instrumento inicial
    public function ruleDataAssinatura($contrato_id, $tipo_id, $id)
    {
        if ($contrato_id != 'NULL'
            && $contrato_id != 'null'
            && !is_null($contrato_id)
        ) {
            $objContrato = Contrato::where('id', $contrato_id)->first();
            // buscar o instrumento inicial do contrato
            $objInstrumentoInicial = $objContrato->getInstrumentoInicial();
            $dataAssinaturaInstrumentoInicial = $objInstrumentoInicial->data_assinatura;
            $dataAssinaturaAditivo = $this->request->get('data_assinatura');
            // Comparando as Datas
            return strtotime($dataAssinaturaAditivo) >= strtotime($dataAssinaturaInstrumentoInicial);
        }

        return true;
    }


    // Tratar a regra do ano do número do termo aditivo - não pode ser anterior ao ano do contrato e nem posterior ao ano atual
    public function ruleNumero($contrato_id, $tipo_id, $id)
    {
        if ($contrato_id != 'NULL' && $contrato_id != 'null' && !is_null($contrato_id)) {
            $objContrato = Contrato::where('id', $contrato_id)->first();
            $numeroContrato = $objContrato->numero;
            $numeroDoTermoAditivo = $this->numero;
            $anoDoNumeroDoTermoAditivo = substr($numeroDoTermoAditivo, -4);
            $numeroContrato = $objContrato->numero;
            $anoDoNumeroDoContrato = substr($numeroContrato, -4);
            $anoAtual = date('Y');
            if ($anoDoNumeroDoTermoAditivo < $anoDoNumeroDoContrato || $anoDoNumeroDoTermoAditivo > $anoAtual) {
                return false;
            }
        }
        return true;
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'data_inicio_novo_valor' => "Data Início Novo Valor",
            'vigencia_inicio' => "Data de Vigência Início",
            'vigencia_fim' => "Data de Vigência Fim",
            'data_publicacao' => "Data Publicação Aditivo",
            'data_assinatura' => "Data Assinatura Aditivo",
            'numero' => "Número Termo Aditivo",
            'qualificacoes' => "Qualificação",
            'observacao' => "Objeto do TA",
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        $data_limite = implode('/',array_reverse(explode('-',$this->data_limite)));
        $data_amanha = date('d/m/Y', strtotime('+1 day'));
        $data_hoje = date('d/m/Y');

        return [
            'data_inicio_novo_valor.required' => "O campo :attribute é obrigatório",
            'data_inicio_novo_valor.between' => "O campo :attribute deve ser entre a Data de Vigência Início e Data de Vigência Fim do Aditivo! ",
            'data_inicio_novo_valor.after' => "A :attribute deve ser uma data igual ou posterior a Data Assinatura Aditivo",
            'vigencia_inicio.before' => "A :attribute deverá ser anterior a Data de Vigência Fim do Aditivo!",
            'vigencia_inicio.after' => "A :attribute deverá ser igual ou posterior a Data de Assinatura do Aditivo!",
            'vigencia_fim.before' => "A :attribute deverá ser uma data anterior a {$data_limite}!",
            'vigencia_fim.after' => "A :attribute deverá ser posterior a Data de Vigência Início do Aditivo!",
            'data_publicacao.after' => "A :attribute deve ser posterior à Data de Assinatura Aditivo",
            'data_publicacao.required_without' => "O campo :attribute é obrigatório",
            "data_assinatura.before_or_equal" => "O campo Data de assinatura deve ser anterior ou igual à data atual {$data_hoje}",
            'data_assinatura.max' => "A :attribute deve ser igual ou posterior à data de assinatura do Contrato ",
            'numero.max' => ":attribute não permitido "
        ];
    }
}
