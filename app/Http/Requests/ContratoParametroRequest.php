<?php

namespace App\Http\Requests;

use App\Models\Contrato;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;

class ContratoParametroRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contrato_id' => 'required',
            'pz_recebimento_provisorio' => 'required',
            'pz_recebimento_definitivo' => 'required',
            'pz_pagamento' => 'nullable',
            'data_pagamento_recorrente' => 'nullable',
            'registro' => 'required_with:pz_recebimento_provisorio,pz_recebimento_definitivo,pz_pagamento,data_pagamento_recorrente'
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'pz_recebimento_provisorio' => 'Prazo para recebimento provisório',
            'pz_recebimento_definitivo' => 'Prazo para recebimento definitivo',
            'pz_pagamento' => 'Prazo para pagamento',
            'data_pagamento_recorrente' => 'Data de pagamento recorrente',
            'registro' => 'Obrigatório registro de OS/F'
        ];
    }


    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $campos = [];
        
        $contrato = Contrato::find($this->contrato_id);

        $autorizacaoexecucoes = DB::table('autorizacaoexecucoes')
            ->where('contrato_id', $this->contrato_id)->get()->all();
      
        $campos['registro'] = !empty($autorizacaoexecucoes);

        if ($contrato->verificaEmpenhoOrCartaContrato()) {
            $campos['registro'] = false;
        }

        $campos['pz_recebimento_provisorio'] = $this->pz_recebimento_provisorio_uteis ?: $this->pz_recebimento_provisorio_corridos;
        $campos['tipo_recebimento_provisorio'] = $this->pz_recebimento_provisorio_uteis ? "Úteis" : ($this->pz_recebimento_provisorio_corridos ? "Corridos" : null);
        
        $campos['pz_recebimento_definitivo'] = $this->pz_recebimento_definitivo_uteis ?: $this->pz_recebimento_definitivo_corridos;
        $campos['tipo_recebimento_definitivo'] = $this->pz_recebimento_definitivo_uteis ? "Úteis" : ($this->pz_recebimento_definitivo_corridos ? "Corridos" : null);
        
        $campos['pz_pagamento'] = $this->pz_pagamento_uteis ?: $this->pz_pagamento_corridos;
        $campos['tipo_pagamento'] = $this->pz_pagamento_uteis ? "Úteis" : ($this->pz_pagamento_corridos ? "Corridos" : null);
        
        $campos['data_pagamento_recorrente'] = str_replace('°', '', $this->data_pagamento_recorrente_uteis) ?: $this->data_pagamento_recorrente_corridos;
        $campos['tipo_pagamento_recorrente'] = $this->data_pagamento_recorrente_uteis ? "Útil" : ($this->data_pagamento_recorrente_corridos ? "Fixo" : null);

        return $this->merge($campos);
    }

    public function messages()
    {
        return [
            'registro.required_with' => 'O campo registro de OS/F é obrigatório.'
        ];
    }
}
