<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\Codigoitem;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ContratoarquivoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $arquivos = ($this->arquivos) ? 'file|max:30000|mimetypes:application/pdf' : "NULL";

        $rules = [
            'contrato_id' => 'required',
           // 'tipo' => 'required',            
           // 'tipo'=>'required_if:arquivos,nullable',
            'tipo' => 'arquivos' ? 'required_if:arquivos,nullable' : 'required',                           
           
            'contratohistorico_id' => Rule::requiredIf(function () {
                $tipo = Codigoitem::find($this->tipo);
                if (isset($tipo->descricao)) {
                    if (in_array($tipo->descricao, config('app.tipos_obrigatorios_arquivo'))) {
                        return true;
                    }
                }
                return false;
            }),
            'processo' => 'required_with:arquivos|max:255',//'processo' => 'required|max:255',
            'descricao' => Rule::requiredIf(function() {
                if ($this->origem=='0') {
                    return true;
                }
            }), //'required|max:255', //'descricao' => 'max:255' ??????????
            'arquivos.*' => $arquivos,              
            'tipoSei' => 'required_with:link_sei',
            'nomeSei' => 'required_with:link_sei',
            'descricaoSei' => 'required_with:link_sei',
        ];


        return $rules;
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'contratohistorico_id.required' => 'O campo \'Termo\' é obrigatório, quando o \'Tipo\' é \'Termo Aditivo\', \'Termo Apostilamento\', ou \'Termo Rescisão\' ',
            'arquivos*.max' => 'O arquivo não pode ter o tamanho superior a 30MB'
        ];
    }
}
