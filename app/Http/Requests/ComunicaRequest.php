<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class ComunicaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'assunto' => 'required|min:2',
            'mensagem' => 'required',
            'situacao' => 'required'
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'assunto' => 'Assunto',
            'mensagem' => 'Mensagem',
            'situacao' => 'Situação',
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'assunto.required' => 'O campo :attribute é de preenchimento obrigatório',
            'assunto.min' => 'O campo :attribute deve ser preenchido com no mínimo 2 caracteres',
            'mensagem.required' => 'O campo :attribute é de preenchimento obrigatório',
            'situacao.required' => 'O campo :attribute é de preenchimento obrigatório'
        ];
    }
}
