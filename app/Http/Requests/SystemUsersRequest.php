<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class SystemUsersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }



    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->id ?? "NULL";

        return [
            'cpf' => "required|unique:users,cpf,{$id}",
            'name' => 'required|max:255',
            'email' => "required|email|max:255|unique:users,email,{$id},id",
            'senha' => $this->id ? 'required|min:6':'nullable|min:6'
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }



    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'cpf.required' => 'O CPF é obrigatório.',
            'cpf.unique' => 'Este usuário já está cadastrado.',

            'name.required' => 'O nome é obrigatório.',
            'name.max' => 'O nome não pode ter mais que 255 caracteres.',

            'email.required' => 'O email é obrigatório.',
            'email.email' => 'O email informado não é válido.',
            'email.max' => 'O email não pode ter mais que 255 caracteres.',
            'email.unique' => 'Este email já está cadastrado.',

            'senha.required' => 'A senha é obrigatória ao criar um novo registro.',
            'senha.min' => 'A senha deve ter no mínimo 8 caracteres.',
        ];
    }
}
