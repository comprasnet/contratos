<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ConsultarInstumentoCobrancaNotaFiscalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {     
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return  [
            'ug_pagadora' => 'required|int|exists:unidades,codigo',
            'numero_nota_fiscal' => 'required|string|exists:contratofaturas,numero',
            'serie_nota_fiscal' => 'nullable|string|exists:contratofaturas,serie',
            'identificador_fornecedor' => 'required|string|exists:fornecedores,cpf_cnpj_idgener',
            'numero_empenho' => 'nullable|string'
        ];
    }
}
