<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Log;

class OrdemBancariaApropriacaoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'ug_emitente_contrato' => 'nullable',
            'ano_contrato' => 'nullable',
            'numero_contrato' => 'nullable',
            'id_contrato' => 'nullable',
            'antecipa_gov' => 'nullable',
        ];

        // Validar ug_emitente_contrato quando ano_contrato estiver presente
        if ($this->ano_contrato && !$this->ug_emitente_contrato) {
            $rules['ug_emitente_contrato'] = 'required_with:ano_contrato';
        }

        // Validar ano_contrato quando ug_emitente_contrato estiver presente
        if ($this->ug_emitente_contrato && !$this->ano_contrato) {
            $rules['ano_contrato'] = 'required_with:ug_emitente_contrato';
        }

        // Se numero_contrato estiver presente, ug_emitente_contrato e ano_contrato também devem ser obrigatórios
        if ($this->numero_contrato && (!$this->ug_emitente_contrato || !$this->ano_contrato)) {
            $rules['ug_emitente_contrato'] = 'required_with:numero_contrato';
            $rules['ano_contrato'] = 'required_with:numero_contrato';
        }

        // Validar ug_emitente_contrato e ano_contrato se id_contrato estiver ausente e antecipa_gov presente
        if (!$this->id_contrato && !is_null($this->antecipa_gov)) {
            $rules['ug_emitente_contrato'] = 'required_with:antecipa_gov';
            $rules['ano_contrato'] = 'required_with:antecipa_gov';
        }

        return $rules;
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = $validator->errors()->toArray();
        //$formattedErrors = $this->formatErrors($errors);

        throw new HttpResponseException(response()->json([
            'status' => 'error',
            'message' => $errors,
        ], 422));
    }

    public function messages()
    {
        return [
            'ug_emitente_contrato.required_with' => 'O campo :attribute é obrigatório para concluir a consulta.',
            'ano_contrato.required_with' => 'O campo :attribute é obrigatório para concluir a consulta.',
            'required_without_all' => 'Por favor, preencha pelo menos um parâmetro de pesquisa.',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if (!$this->hasAny(['ug_emitente_contrato', 'ano_contrato', 'numero_contrato', 'antecipa_gov', 'id_contrato'])) {
                $validator->errors()->add('error', 'Requisição não atende aos requisitos do contrato da API');
            }
        });


    }
}
