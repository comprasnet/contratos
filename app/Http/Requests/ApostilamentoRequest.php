<?php

namespace App\Http\Requests;

use App\Models\AmparoLegal;
use App\Models\Contrato;

use App\Http\Requests\Request;
use App\Http\Traits\RegrasDataPublicacao;
use App\Rules\NaoAceitarInativarContratoHistorico;
use App\Rules\ValidarDataAssinaturaAditivo;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Unique;

class ApostilamentoRequest extends FormRequest
{

    use RegrasDataPublicacao;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $amparos = false;
        $id = $this->id ?? "NULL";
        $contrato_id = $this->contrato_id ?? "NULL";
        $tipo_id = $this->tipo_id ?? "NULL";

        $this->hoje = date('Y-m-d');


        $rules = [
            'contrato_id' => 'required',
            'unidade_id' => 'required',
            'data_inicio_novo_valor' => 'required|date|after_or_equal:data_assinatura',
            'data_fim_novo_valor' => 'nullable|after_or_equal:data_inicio_novo_valor',
            'novo_num_parcelas' => 'required',
            'novo_valor_parcela' => 'required',
            'novo_valor_global' => 'required',
            'observacao' => 'required',
            'retroativo' => 'required',
            'retroativo_mesref_de' => 'required_if:retroativo,==,1',
            'retroativo_anoref_de' => 'required_if:retroativo,==,1',
            'retroativo_mesref_ate' => 'required_if:retroativo,==,1',
            'retroativo_anoref_ate' => 'required_if:retroativo,==,1',
            'retroativo_vencimento' => 'required_if:retroativo,==,1',
            'retroativo_valor' => 'required_if:retroativo,==,1', //ver com Schoolofnet como exigir que o valor seja maior que 0 quando tiver retroativo.
            'elaboracao' => ['required', new NaoAceitarInativarContratoHistorico($id)],
        ];

        if($contrato_id != "NULL") {
            $contrato = Contrato::find($contrato_id);
            $ids = array_column($contrato->getAmparosAttribute(), 'id');
            $amparos = AmparoLegal::verificarLei14133($ids);

            $rules['data_publicacao'] = $this->ruleDataPublicacao($tipo_id, $this->id, $amparos);
        }



        // Tratar a regra da data de assinatura do termo Apostilamento
        if( !$this->ruleDataAssinatura($contrato_id, $tipo_id, $id) ){
            $rules['data_assinatura'] = 'required_if:elaboracao,0|max:2';       // coloquei o max, para criticar o campo. A mensagem está no método messages.
        } else {
            // aqui a regra será como era anteriormente.
            $rules['data_assinatura'] = [
                'required_if:elaboracao,0',
                'date',
                "before_or_equal:{$this->hoje}"
            ];
        }

        // Tratar a regra do número do termo Apostilamento
        if( !$this->ruleNumero($contrato_id, $tipo_id, $id) ){
            $rules['numero'] = 'required|max:2';       // coloquei o max, para criticar o campo. A mensagem está no método messages.
        } else {
            $rules['numero'] = [
                'required',
                (new Unique('contratohistorico','numero'))
                    ->where('contrato_id',$contrato_id)
                    ->where('tipo_id',$tipo_id)
                    ->ignore($id)
            ];
        }

        return $rules;
    }

    // Tratar a regra da data de assinautra do termo apostilamento - não pode ser anterior à data de assinatura do instrumento inicial
    public function ruleDataAssinatura($contrato_id, $tipo_id, $id)
    {
        if ($contrato_id != 'NULL'
            && $contrato_id != 'null'
            && !is_null($contrato_id)
        ) {
            $objContrato = Contrato::where('id', $contrato_id)->first();
            // buscar o instrumento inicial do contrato
            $objInstrumentoInicial = $objContrato->getInstrumentoInicial();
            $dataAssinaturaInstrumentoInicial = $objInstrumentoInicial->data_assinatura;
            $dataAssinaturaApostilamento = $this->request->get('data_assinatura');

            return strtotime($dataAssinaturaApostilamento) >= strtotime($dataAssinaturaInstrumentoInicial);
        }

        return true;
    }

    // Tratar a regra do ano do número do termo apostilamento - não pode ser anterior ao ano do contrato e nem posterior ao ano atual
    public function ruleNumero($contrato_id, $tipo_id, $id){
        if( $contrato_id != 'NULL' && $contrato_id != 'null' && !is_null($contrato_id) ){
            $objContrato = Contrato::where('id', $contrato_id)->first();
            $numeroContrato = $objContrato->numero;
            $numeroDoTermoApostilamento = $this->numero;
            $anoDoNumeroDoTermoApostilamento = substr($numeroDoTermoApostilamento, -4);
            $numeroContrato = $objContrato->numero;
            $anoDoNumeroDoContrato = substr($numeroContrato, -4);
            $anoAtual = date('Y');
            if($anoDoNumeroDoTermoApostilamento < $anoDoNumeroDoContrato || $anoDoNumeroDoTermoApostilamento > $anoAtual){
                return false;
            }
        }
        return true;
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'numero' => 'Número Termo Apostilamento',
            'data_publicacao' => 'Data da Publicação',
            'observacao' => 'Objeto do Apostilamento',
            'elaboracao' => 'Situação',
            'data_assinatura' => 'Data Assinatura Apostilamento',
            'data_inicio_novo_valor' => 'Data Início Novo Valor',
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        $data_hoje = date('d/m/Y');

        return [
            'data_publicacao.required_if' => 'O campo :attribute é obrigatório quando o termo de apostilamento é Ativo.',
            'data_assinatura.required_if' => 'O campo :attribute é obrigatório quando o termo de apostilamento é Ativo.',
            'numero.max' => ":attribute não permitido ",
            'data_assinatura.max' => "A :attribute deve ser igual ou posterior à data de assinatura do Contrato ",
            "data_assinatura.before_or_equal" => "O campo Data de assinatura deve ser anterior ou igual à data atual {$data_hoje}",
        ];
    }
}
