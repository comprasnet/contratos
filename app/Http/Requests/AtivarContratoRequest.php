<?php

namespace App\Http\Requests;

use App\Models\Contrato;

use App\Http\Requests\Request;
use App\Http\Traits\RegrasDataPublicacao;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Unique;
use App\Rules\ValidarDataAssinaturaAditivo;

class AtivarContratoRequest extends FormRequest
{

    use RegrasDataPublicacao;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $contrato = Contrato::with(['amparoslegais'])->find($this->input('id'));
        $atos_normativos = $contrato->amparoslegais->pluck('ato_normativo');
        $lei14133 = $atos_normativos->contains(function ($value, $key) {
            return preg_match('/LEI 14\.133/i', $value) === 1;
        });
        $this->merge([
            'lei14133' => $lei14133,
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->id ?? "NULL";
        $contrato_id = $this->contrato_id ?? "NULL";
        $unidadeorigem_id        = $this->unidadeorigem_id               ?? "NULL";
        $tipo_id = $this->tipo_id ?? "NULL";
        $this->data_limitefim    = date('Y-m-d', strtotime('+50 year'));
        $this->data_limiteinicio = date('Y-m-d', strtotime('-50 year'));
        $this->data_atual        = date('Y-m-d');

        $rules = [
            'numero' => [
                'required',
                (new Unique('contratos', 'numero'))
                    ->ignore($id)
                    ->where('unidadeorigem_id', $unidadeorigem_id)
                    ->where('tipo_id', $tipo_id)
            ],
            'data_assinatura' => "required|date|after:{$this->data_limiteinicio}|before_or_equal:{$this->data_atual}",
            'vigencia_inicio' => 'required|date|after_or_equal:data_assinatura',
            'elaboracao' => 'required|in:0',
        ];

        $rules['data_publicacao'] = $this->ruleDataPublicacao($tipo_id, $this->id, $this->lei14133);

        return $rules;
    }


    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return
            [
            'data_publicacao' => 'Data da Publicação',
            'data_assinatura' => 'Data da Assinatura',
            'vigencia_inicio' => 'Data de início da vigência'
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'data_publicacao.after' => "A Data da Publicação deve ser posterior à Data de Assinatura do Contrato.",
            'data_assinatura.required' => 'O campo :attribute é obrigatório.',
            'data_publicacao.required_if' => 'O campo :attribute é obrigatório.',
        ];
    }
}
