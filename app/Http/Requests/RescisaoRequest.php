<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Http\Traits\RegrasDataPublicacao;
use App\Models\Contrato;
use App\Rules\ValidarDtVigenciaFimQuandoMenorDtVigenciaInicio;
use App\Models\AmparoLegal;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Carbon;


class RescisaoRequest extends FormRequest
{
    use RegrasDataPublicacao;
    protected $data_limite;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $tipo_id = $this->tipo_id ?? "NULL";
        $this->data_limitefim = date('Y-m-d', strtotime('+50 year'));
        $this->data_limiteinicio = date('Y-m-d', strtotime('-50 year'));
        $data_atual = date('Y-m-d');

        $dtAssinaturaRescisao = isset($this->data_assinatura) ? Carbon::createFromFormat('Y-m-d', $this->data_assinatura) : null;

        $afterOrEqualVigenciaInicio = $dtAssinaturaRescisao && $dtAssinaturaRescisao->gte($this->contratoModel()->vigencia_inicio) ?
            "after_or_equal:{$this->contratoModel()->vigencia_inicio}" : '';
        $contrato_id = $this->contrato_id ?? "NULL";

        $rules = [
            'observacao' => 'required',
            'processo' => 'required',
            'data_assinatura' => [
                'required',
                'date',
                "after:{$this->data_limiteinicio}",
                function ($attribute, $value, $fail) use ($data_atual) {
                    if ($value > $data_atual) {
                        $fail('A data de assinatura não pode ser maior que a data atual.');
                    }
                },
                function ($attribute, $value, $fail) use($dtAssinaturaRescisao){

                    if($this->data_assinatura < $this->contratoModel()->vigencia_inicio && $this->vigencia_fim === $this->contratoModel()->vigencia_inicio)
                    {
                        return true;
                    }

                    if ($this->data_assinatura < $this->contratoModel()->data_assinatura) {

                        $dtAssinaturaContrato = implode('/',array_reverse(explode('-',$this->contratoModel()->data_assinatura)));;
                        $fail("Não é possível rescindir um contrato antes da sua data de assinatura (Data de assinatura do contrato $dtAssinaturaContrato).");
                    }

                    return false;
                },
                isset($this->vigencia_fim) ?
                function ($attribute, $value, $fail) {
                    if ($this->data_assinatura > $this->vigencia_fim) {
                        $fail('Data de assinatura da rescisão não pode ser posterior à data fim da vigência do contrato' .
                            ' informada na rescisão.');
                    }
                } : null,
                function ($attribute, $value, $fail) {
                    if (isset($this->contratoModel()->vigencia_fim) && $this->data_assinatura > $this->contratoModel()->vigencia_fim) {
                        $fail('Não é possível rescindir um contrato não vigente.');
                    }
                },
                isset($this->contratoModel()->vigencia_fim) ? "before_or_equal:{$this->contratoModel()->vigencia_fim}" : "before_or_equal:{$this->vigencia_fim}" ,
            ],
            'vigencia_fim' => [
                'required',
                'date',
                $afterOrEqualVigenciaInicio,
                isset($this->contratoModel()->vigencia_fim) ? "before_or_equal:{$this->contratoModel()->vigencia_fim}" : "before_or_equal:{$this->vigencia_fim}" ,
                "before:{$this->data_limitefim}",
                function ($attribute, $value, $fail) {
                    
                    if (isset($this->contratoModel()->vigencia_fim) && $this->vigencia_fim > $this->contratoModel()->vigencia_fim) {
                        $fail('Não é possível rescindir um contrato não vigente.');
                    }
                },
                isset($this->vigencia_fim) ? new ValidarDtVigenciaFimQuandoMenorDtVigenciaInicio(
                    $this->data_assinatura,
                    $this->contratoModel()->vigencia_inicio,
                    isset($this->contratoModel()->vigencia_fim) ? $this->contratoModel()->vigencia_fim  : $this->vigencia_fim,
                    $this->vigencia_fim
                ) : null
            ],
        ];
        if($contrato_id != "NULL") {
            $contrato = Contrato::find($contrato_id);
            $ids = array_column($contrato->getAmparosAttribute(), 'id');
            $amparos = AmparoLegal::verificarLei14133($ids);
            // a partir de agora, 12/2021, data de publicação não será mais obrigatória. Vamos verificar se foi informada, para só então chamarmos o tratamento da regra.
            // ponto verificacao 1
            $rules['data_publicacao'] = $this->ruleDataPublicacao($tipo_id, $this->id, $amparos);
        }


        return $rules;
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'observacao' => 'Observação',
            'processo' => 'Número Processo',
            'data_publicacao' => 'Data Publicação',
            'data_assinatura' => 'Data Assinatura Rescisão',
            'vigencia_fim' => 'Data Vig. Fim',
            'vigencia_inicio' => 'Data de início da vigência',
        ];
    }

    public function contratoModel()
    {
        $contratoId = \Route::current()->parameter('contrato_id');

        return Contrato::findOrFail($contratoId);
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        $data_limite = implode('/',array_reverse(explode('-',$this->data_limite)));
        $data_assinatura_contrato = implode('/',array_reverse(explode('-',$this->contratoModel()->data_assinatura)));
        $data_fim_vigencia_contrato = implode('/',array_reverse(explode('-',$this->contratoModel()->vigencia_fim)));
        $vigencia_inicio_contrato = implode('/',array_reverse(explode('-', $this->contratoModel()->vigencia_inicio)));
        $dtlimiteInicio = implode('/',array_reverse(explode('-',$this->data_limiteinicio)));

        return [
            'data_publicacao.after_or_equal' => "A :attribute precisa ser igual ou posterior à Data de Assinatura " .
                "da Rescisão.",
            'data_assinatura.after_or_equal' => "O campo :attribute deve ser uma data posterior ou igual a " .
                "{$data_assinatura_contrato}.",
            'data_assinatura.required' => "O campo :attribute é obrigatório.",
            'data_assinatura.before_or_equal' => "O campo :attribute deve ser uma data igual ou anterior a Vigência fim do contrato $data_fim_vigencia_contrato.",
            'data_assinatura.date' => "O campo :attribute deve ser uma data.",
            'data_assinatura.after' => "O campo :attribute deve ser depois de {$dtlimiteInicio}.",
            'vigencia_fim.after_or_equal' => "O campo :attribute deve ser uma data posterior ou igual a " .
                "{$vigencia_inicio_contrato}.",
            'vigencia_fim.before_or_equal' => "O campo :attribute deve ser uma data igual ou anterior a " .
                "Vigência fim do contrato {$data_fim_vigencia_contrato}.",
            'vigencia_fim.before' => "A :attribute deve ser uma data anterior a {$data_limite}!",
            'vigencia_fim.after' => "A :attribute deve ser uma data após a Data de início da vigência.",
            'data_publicacao.required_without' => "O campo :attribute é obrigatório",
        ];
    }
}
