<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class UsuarioOrgaoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->id ?? "NULL";

        return [
            'cpf' => "required|cpf|unique:users,cpf,{$id}",
            'name' => 'required|max:255',
            'email' => "required|email|max:255|unique:users,email,{$id}",
            'ugprimaria' => "required_if:situacao,1",
            'roles' => "required_if:situacao,1",
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'cpf.required' => 'O campo "CPF" é obrigatório!',
            'cpf.cpf' => 'CPF inválido!',
            'cpf.unique' => 'Este CPF já está cadastrado!',
            'name.required' => 'O campo "Nome" é obrigatório!',
            'name.max' => 'O campo "Nome" deve ser no máximo 255 caracteres!',
            'email.unique' => 'Este E-mail já está cadastrado!',
            'email.email' => 'E-mail inválido!',
            'email.required' => 'O campo "E-mail" é obrigatório!',
            'email.max' => 'O campo "E-mail" deve ser no máximo 255 caracteres!',
            'ugprimaria.required_if' => 'O campo UG/UASG Padrão é obrigatório quando a situação for ativo',
            'roles.required_if' => 'O campo Grupos de Usuário é obrigatório quando a situação for ativo',
        ];
    }

    protected function prepareForValidation()
    {
        $data = $this->all();
        $unidadeLogada = (string)session()->get('user_ug_id');
    
        if (isset($data['unidades2']) && isset(($data['unidades']))) {

            if((!in_array($unidadeLogada, $data['unidades'])) && (in_array($unidadeLogada, $data['unidades2']))){
                $unidades = array();
                foreach($data['unidades2'] as $key => $res){
                        if(($res !== $unidadeLogada)){
                            $unidades2[] = $res;
                        }
                }

                $unidades = array_unique(array_merge($data['unidades'], $unidades2));
            }else{

            $unidades = array_unique(array_merge($data['unidades'], $data['unidades2']));
        }

            $this->merge([
                'unidades' => $unidades,
            ]);

            // Isso remove unidades2 do array de dados depois de ter feito a mesclagem.
            unset($data['unidades2']);
            $this->replace($data);

            return $this->merge([
                'unidades' => $unidades,
            ]);
        }

        if(isset($data['unidades2'])){
            $val = "";
            $unidades = array();
            foreach($data['unidades2'] as $res){
            $val++;
            if(($res !== $unidadeLogada && $val >= 1 )){
                    $unidades[] = $res;
                }
            }
            
            if($val <= 1 && empty($unidades[0])){
                unset($data['unidades2']);
                unset($data['unidades']);
               return $this->replace($data);
            }
          
            unset($data['unidades2']);

           return $this->merge([
                'unidades' => $unidades,
            ]);
            
        }else{
            return $data;
        }
    }
}
