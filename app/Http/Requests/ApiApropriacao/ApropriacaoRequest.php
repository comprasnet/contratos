<?php

namespace App\Http\Requests\ApiApropiacao;

use Illuminate\Foundation\Http\FormRequest;

class ApropriacaoRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nonce' => 'required|string|max:100',
            'inst_cobranca.id_inst_cobranca' => 'required|integer',
            'tipo_dh' => 'required|boolean',
            'data_emissao_contabil' => 'required|date|date_format:Y-m-d',
            'data_vencimento' => 'required|date|date_format:Y-m-d',
            'taxa_cambio' => 'required|numeric|between:0,9999999999.99',
            'processo' => 'required|string|max:255',
            'data_ateste' => 'required|date|date_format:Y-m-d',
            'observacao' => 'string|max:468',
            'informacoes_adicionais' => 'required|string|max:500',
            'situacao.cod_situacao' => 'required|string|max:255',
            'situacao.indicador_contrato' => 'required|boolean',
            'situacao.despesa_antecipada' => 'boolean',
            'situacao.conta_contrato' => 'integer',
            'situacao.favorecido_contrato' => 'string|max:500',
            'situacao.inscricao_generica' => 'string|max:500',
            'situacao.Empenho.numero_empenho' => 'required|string|max:255',
            'situacao.Empenho.subelemento' => 'required|integer',
            'situacao.Empenho.indicador_liquidado' => 'required|boolean',
            'situacao.Empenho.valor_item' => 'required|numeric|between:0,9999999999.99',
        ];
    }
}
