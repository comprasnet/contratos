<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class ApropriarRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nonce' => 'required|string|max:100',
            'id_inst_cobranca' => 'required|array|min:1',
            'id_inst_cobranca.*' => 'required|integer',
            'cpf_usuario' => 'required|string|regex:/^\d{3}\.\d{3}\.\d{3}\-\d{2}$/',
            'tipo_dh' => 'required|string',
            'cod_ug_emitente' => 'required|integer',
            'data_emissao_contabil' => 'required|date_format:Y-m-d',
            'data_vencimento' => 'required|date_format:Y-m-d',
            'taxa_cambio' => 'required|numeric',
            'processo' => 'required|string|max:255',
            'data_ateste' => 'required|date_format:Y-m-d',
            'observacao' => 'nullable|string|max:468',
            'informacoes_adicionais' => 'required|string|max:500',
            'sf_pco' => 'required|array|min:1',
            'sf_pco.*.cod_situacao' => 'required|string|max:255',
            'sf_pco.*.indicador_contrato' => 'required|boolean',
            'sf_pco.*.despesa_antecipada' => 'required|boolean',
            'sf_pco.*.txtinscrd' => 'nullable|string|max:500',
            'sf_pco.*.numclassd' => 'nullable|integer',
            'sf_pco.*.txtinscre' => 'nullable|string|max:500',
            'sf_pco.*.numclasse' => 'nullable|integer',
            'sf_pco.*.sf_pco_item' => 'required|array|min:1',
            'sf_pco.*.sf_pco_item.*.numero_empenho' => 'required|string|max:255',
            'sf_pco.*.sf_pco_item.*.subelemento' => 'required|string|max:255',
            'sf_pco.*.sf_pco_item.*.indicador_liquidado' => 'required|boolean',
            'sf_pco.*.sf_pco_item.*.valor_item' => 'required|numeric',
            'sf_pco.*.sf_pco_item.*.txtinscra' => 'nullable|string|max:500',
            'sf_pco.*.sf_pco_item.*.numclassa' => 'nullable|integer',
            'sf_pco.*.sf_pco_item.*.txtinscrb' => 'nullable|string|max:500',
            'sf_pco.*.sf_pco_item.*.numclassb' => 'nullable|integer',
            'sf_pco.*.sf_pco_item.*.txtinscrc' => 'nullable|string|max:500',
            'sf_pco.*.sf_pco_item.*.numclassc' => 'nullable|integer',
            'data_pagamento' => 'required|date_format:Y-m-d',
            'sf_centro_custo.*.cod_centro_custo' => 'required|string|max:255',
            'sf_centro_custo.*.mes' => 'nullable|integer',
            'sf_centro_custo.*.ano' => 'nullable|integer',
            'sf_centro_custo.*.codigo_siorg' => 'nullable|integer',
            'sf_centro_custo.*.ug_beneficiada' => 'nullable|integer',
            'sf_centro_custo.*.item_vlrcc.*.cod_situacao' => 'required|string|max:255',
            'sf_centro_custo.*.item_vlrcc.*.numero_empenho' => 'required|string|max:255',
            'sf_centro_custo.*.item_vlrcc.*.valor_item' => 'required|numeric',
        ];
    }



    protected function failedValidation(Validator $validator)
    {
        $errors = $validator->errors()->toArray();
        $formattedErrors = $this->formatErrors($errors);

        throw new HttpResponseException(response()->json([
            'status' => 'error',
            'message' =>  $formattedErrors,
        ], 422));
    }

    private function formatErrors(array $errors): string
    {
        $formattedErrors = [];

        foreach ($errors as $field => $messages) {
            foreach ($messages as $message) {
                $formattedErrors[] = "$field: $message";
            }
        }
        $formattedErrorsString = implode(', ', $formattedErrors);
        return $formattedErrorsString;
    }

    public function messages()
    {
        return [
            'nonce.required' => 'O campo nonce é obrigatório',
            'nonce.string' => 'O campo nonce deve ser uma string',
            'nonce.max' => 'O campo nonce não deve exceder :max caracteres',

            'id_inst_cobranca.*.required' => 'O campo id_inst_cobranca é obrigatório',
            'id_inst_cobranca.*.integer' => 'O campo id_inst_cobranca deve ser um número inteiro',

            'tipo_dh.required' => 'O campo tipo_dh é obrigatório',
            'tipo_dh.string' => 'O campo tipo_dh deve ser uma string',

            'cod_ug_emitente.required' => 'O campo cod_ug_emitente é obrigatório',
            'cod_ug_emitente.integer' => 'O campo cod_ug_emitente deve ser um número inteiro',

            'data_emissao_contabil.required' => 'O campo data_emissao_contabil é obrigatório',
            'data_emissao_contabil.date_format' => 'O campo data_emissao_contabil deve estar no formato Y-m-d',

            'data_vencimento.required' => 'O campo data_vencimento é obrigatório',
            'data_vencimento.date_format' => 'O campo data_vencimento deve estar no formato Y-m-d',

            'taxa_cambio.required' => 'O campo taxa_cambio é obrigatório',
            'taxa_cambio.numeric' => 'O campo taxa_cambio deve ser um número',

            'processo.required' => 'O campo processo é obrigatório',
            'processo.string' => 'O campo processo deve ser uma string',
            'processo.max' => 'O campo processo não deve exceder :max caracteres',

            'data_ateste.required' => 'O campo data_ateste é obrigatório',
            'data_ateste.date_format' => 'O campo data_ateste deve estar no formato Y-m-d',

            'observacao.string' => 'O campo observacao deve ser uma string',
            'observacao.max' => 'O campo observacao não deve exceder :max caracteres',

            'informacoes_adicionais.required' => 'O campo informacoes_adicionais é obrigatório',
            'informacoes_adicionais.string' => 'O campo informacoes_adicionais deve ser uma string',
            'informacoes_adicionais.max' => 'O campo informacoes_adicionais não deve exceder :max caracteres',

            'sfpco.*.cod_situacao.required' => 'O campo cod_situacao é obrigatório',
            'sfpco.*.cod_situacao.string' => 'O campo cod_situacao deve ser uma string',
            'sfpco.*.cod_situacao.max' => 'O campo cod_situacao não deve exceder :max caracteres',

            'sfpco.*.indicador_contrato.required' => 'O campo indicador_contrato é obrigatório',
            'sfpco.*.indicador_contrato.boolean' => 'O campo indicador_contrato deve ser um valor booleano',

            'sfpco.*.despesa_antecipada.required' => 'O campo despesa_antecipada é obrigatório',
            'sfpco.*.despesa_antecipada.boolean' => 'O campo despesa_antecipada deve ser um valor booleano',

            'sfpco.*.txtinscrd.string' => 'O campo txtinscrd deve ser uma string',
            'sfpco.*.txtinscrd.max' => 'O campo txtinscrd não deve exceder :max caracteres',

            'sfpco.*.numclassd.integer' => 'O campo numclassd deve ser um número inteiro',

            'sfpco.*.txtinscre.string' => 'O campo txtinscre deve ser uma string',
            'sfpco.*.txtinscre.max' => 'O campo txtinscre não deve exceder :max caracteres',

            'sfpco.*.numclasse.integer' => 'O campo numclasse deve ser um número inteiro',

            'sfpco.*.sfpcoitem.*.numero_empenho.required' => 'O campo numero_empenho é obrigatório',
            'sfpco.*.sfpcoitem.*.numero_empenho.string' => 'O campo numero_empenho deve ser uma string',
            'sfpco.*.sfpcoitem.*.numero_empenho.max' => 'O campo numero_empenho não deve exceder :max caracteres',

            'sfpco.*.sfpcoitem.*.subelemento.required' => 'O campo subelemento é obrigatório',
            'sfpco.*.sfpcoitem.*.subelemento.string' => 'O campo subelemento deve ser uma string',
            'sfpco.*.sfpcoitem.*.subelemento.max' => 'O campo subelemento não deve exceder :max caracteres',

            'sfpco.*.sfpcoitem.*.indicador_liquidado.required' => 'O campo indicador_liquidado é obrigatório',
            'sfpco.*.sfpcoitem.*.indicador_liquidado.boolean' => 'O campo indicador_liquidado deve ser um valor booleano',

            'sfpco.*.sfpcoitem.*.valor_item.required' => 'O campo valor_item é obrigatório',
            'sfpco.*.sfpcoitem.*.valor_item.numeric' => 'O campo valor_item deve ser um número',

            'sfpco.*.sfpcoitem.*.txtinscra.string' => 'O campo txtinscra deve ser uma string',
            'sfpco.*.sfpcoitem.*.txtinscra.max' => 'O campo txtinscra não deve exceder :max caracteres',

            'sfpco.*.sfpcoitem.*.numclassa.integer' => 'O campo numclassa deve ser um número inteiro',

            'sfpco.*.sfpcoitem.*.txtinscrb.string' => 'O campo txtinscrb deve ser uma string',
            'sfpco.*.sfpcoitem.*.txtinscrb.max' => 'O campo txtinscrb não deve exceder :max caracteres',

            'sfpco.*.sfpcoitem.*.numclassb.integer' => 'O campo numclassb deve ser um número inteiro',

            'sfpco.*.sfpcoitem.*.txtinscrc.string' => 'O campo txtinscrc deve ser uma string',
            'sfpco.*.sfpcoitem.*.txtinscrc.max' => 'O campo txtinscrc não deve exceder :max caracteres',

            'sfpco.*.sfpcoitem.*.numclassc.integer' => 'O campo numclassc deve ser um número inteiro',

            'data_pagamento.required' => 'O campo data_pagamento é obrigatório',
            'data_pagamento.date_format' => 'O campo data_pagamento deve estar no formato Y-m-d',

            'sfcentrocusto.*.cod_centro_custo.required' => 'O campo cod_centro_custo é obrigatório',
            'sfcentrocusto.*.cod_centro_custo.string' => 'O campo cod_centro_custo deve ser uma string',
            'sfcentrocusto.*.cod_centro_custo.max' => 'O campo cod_centro_custo não deve exceder :max caracteres',

            'sfcentrocusto.*.mes.integer' => 'O campo mes deve ser um número inteiro',

            'sfcentrocusto.*.ano.integer' => 'O campo ano deve ser um número inteiro',

            'sfcentrocusto.*.codigo_siorg.integer' => 'O campo codigo_siorg deve ser um número inteiro',

            'sfcentrocusto.*.ug_beneficiada.integer' => 'O campo ug_beneficiada deve ser um número inteiro',

            'sfcentrocusto.*.itemvlrcc.*.cod_situacao.required' => 'O campo cod_situacao é obrigatório',
            'sfcentrocusto.*.itemvlrcc.*.cod_situacao.string' => 'O campo cod_situacao deve ser uma string',
            'sfcentrocusto.*.itemvlrcc.*.cod_situacao.max' => 'O campo cod_situacao não deve exceder :max caracteres',

            'sfcentrocusto.*.itemvlrcc.*.numero_empenho.required' => 'O campo numero_empenho é obrigatório',
            'sfcentrocusto.*.itemvlrcc.*.numero_empenho.string' => 'O campo numero_empenho deve ser uma string',
            'sfcentrocusto.*.itemvlrcc.*.numero_empenho.max' => 'O campo numero_empenho não deve exceder :max caracteres',

            'sfcentrocusto.*.itemvlrcc.*.valor_item.required' => 'O campo valor_item é obrigatório',
            'sfcentrocusto.*.itemvlrcc.*.valor_item.numeric' => 'O campo valor_item deve ser um número',
        ];
    }
}
