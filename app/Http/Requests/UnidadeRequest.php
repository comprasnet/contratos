<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class UnidadeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->id ?? "NULL";

        return [
            'orgao_id' => "required",
            'codigosiafi' => "required|max:6|min:6|unique:unidades,codigosiafi,{$id}",
            'gestao' => "required|max:5|min:5",
            'codigo' => "required|max:6|min:6|unique:unidades,codigo,{$id}",
            'nome' => "required|max:255",
            'nomeresumido' => "required|max:19",
            'tipo' => 'required',
            'uf' => 'required',
            'sisg' => 'required',
            'aderiu_siasg' => 'required',
            'utiliza_siafi' => 'required',
            'municipio_id' => 'required',
            'situacao' => 'required',
            'cnpj' => 'nullable|cnpj',
            'esfera' => 'required',
            'poder' => 'required',
            'tipo_adm' => 'required',
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'orgao_id' => "Órgão",
            'codigosiafi' => "Código SIAFI",
            'gestao' => "Gestão",
            'codigo' => "Código SIASG",
            'nome' => "Nome",
            'tipo' => "Tipo",
            'nomeresumido' => "Nome Resumido",
            'situacao' => "Órgão",
            'cnpj' => "CNPJ"
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'orgao_id.required' => 'O campo :attribute é obrigatório.',
            'codigosiafi.required' => 'O campo :attribute é obrigatório.',
            'codigosiafi.unique' => 'Este :attribute já está cadastrado.',
            'codigosiafi.max' => 'O campo :attribute deve ter no máximo 6 caracteres.',
            'codigosiafi.min' => 'O campo :attribute deve ter no mínimo 6 caracteres.',
            'gestao.required' => 'O campo :attribute é obrigatório.',
            'codigo.required' => 'O campo :attribute é obrigatório.',
            'codigo.unique' => 'Este :attribute já está cadastrado.',
            'codigo.max' => 'O campo :attribute deve ter no máximo 6 caracteres.',
            'codigo.min' => 'O campo :attribute deve ter no mínimo 6 caracteres.',
            'nome.required' => 'O campo :attribute é obrigatório.',
            'nome.max' => 'O campo :attribute deve ser no máximo 255 caracteres.',
            'nomeresumido.required' => 'O campo :attribute é obrigatório.',
            'nomeresumido.max' => 'O campo :attribute deve ser no máximo 19 caracteres.',
            'tipo.required' => 'O campo :attribute é obrigatório.',
            'situacao.required' => 'O campo :attribute é obrigatório.',
            'cnpj.cnpj' => 'Campo :attribute inválido.',
            'orgao_id.required' => 'O campo :attribute é obrigatório.',
            'poder.required' => 'O campo Poder é obrigatório.',
            'tipo_adm.required' => 'O campo Administração é obrigatório.',
            'uf.required' => 'O campo Estado é obrigatório.',
            'municipio_id.required' => 'O campo Município é obrigatório.',
            'esfera.required' => 'O campo Esfera é obrigatório.',
        ];
    }
}
