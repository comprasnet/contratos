<?php

namespace App\Http\Requests;

use App\Rules\ValidarDomicilioBancario;
use App\Rules\ValidarExistsIndividual;
use Illuminate\Foundation\Http\FormRequest;

class RegistrarAntecipagovRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $num_banco      = $this->num_banco      ?? null;
        $num_agencia    = $this->num_agencia    ?? null;
        $conta_bancaria = $this->conta_bancaria ?? null;

        return [
            'id_contrato'         => ['required', 'array',
                new ValidarExistsIndividual('contratos','id'),
                new ValidarDomicilioBancario(
                    $num_banco, $num_agencia, $conta_bancaria
                )
            ],
            'id_contrato.*'      => 'distinct',
            'conta_bancaria'      => 'required',
            'num_agencia'         => 'required|numeric',
            'num_banco'           => 'required|numeric',
            'status_operacao'     => 'required',
            'num_operacao'        => 'required',
            'num_cotacao'         => 'required',
            'identificador_unico' => 'required',
            'data_acao'           => 'required',
            'valor_operacao'      => 'required|numeric|min:0|max:99999999999999.99',
            'valor_parcela'       => 'required|numeric|min:0|max:99999999999999.99'
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
