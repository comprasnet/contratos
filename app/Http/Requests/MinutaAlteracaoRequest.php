<?php

namespace App\Http\Requests;


// início alteracao regra
use App\Models\MinutaEmpenho;
use App\Models\CompraItemFornecedor;
use App\Models\CompraItemUnidade;
// fim alteracao regra

use App\Http\Requests\Request;
use App\Rules\NaoAceitarEstrangeiro;
use App\Rules\NaoAceitarTodasOperacoesNenhuma;
use App\Rules\NaoAceitarValorMaiorTotal;
use App\Rules\NaoAceitarZero;
use App\Rules\NaoAceitarNumeroNegativo;
use App\Rules\NaoAceitarFloatParaSuprimentoESisrp;
use App\Rules\VerificarValoresIrrisorios;
use Illuminate\Foundation\Http\FormRequest;

class MinutaAlteracaoRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $valor_utilizado = number_format($this->valor_utilizado, 2, '.', '');

        // vamos confirmar se é do tipo Compra. Caso seja, chamaremos o método que calcula o valor máximo permitido
        $minuta_id = $this->get('minuta_id');
        $modMinuta = MinutaEmpenho::find($minuta_id);
        $tipo = $modMinuta->empenho_por;

        //Deve ser escolhido um Tipo de Operação de Reforço ou Anulação nos itens alterados

        if($tipo === 'Compra'){
            // vamos chamar o método que verifica se é reforço e, caso seja, calcula o valor máximo permitido
            $arrayRetorno = self::calcularValorMaximoPermitidoParaReforco();
        } else {
            $arrayRetorno = $this->valor_total_item;
        }

        return [

            // linha que verifica se o saldo é negativo.
            'credito' => 'gte:'.$valor_utilizado,
            'tipo_alteracao'=>[ new NaoAceitarTodasOperacoesNenhuma() ],
            'valor_total.*' => [
                'filled',
                new NaoAceitarZero($this->tipo_alteracao),
                new VerificarValoresIrrisorios($this->tipo_alteracao, $this->valorunitario, $this->numero_item),
                new NaoAceitarNumeroNegativo($this->tipo_alteracao),
                new NaoAceitarValorMaiorTotal(
                    $this->tipo_alteracao,
                    $arrayRetorno,              // ao invés do array acima, vamos enviar um array idêntico, mas com o novo valor calculado
                    $this->vlr_total_item,
                    $this->tipo_empenho_por
                )
            ],
            'qtd.*' => [
                'filled',
                new NaoAceitarZero($this->tipo_alteracao),
                new NaoAceitarNumeroNegativo($this->tipo_alteracao),
            ]
        ];

    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'valor_total.*' => 'Valor Total',
            'qtd.*' => 'Qtd'
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'credito.gte' => 'O saldo não pode ser negativo.',
            'valor_total.*.filled' => 'O campo :attribute não pode estar vazio.'
        ];
    }

    // método que pega o request (this) e calcula o valor máximo permitido para reforço
    // este valor será usado pelo método rules para saber se o valor é permitido.
    public function calcularValorMaximoPermitidoParaReforco(){
        $minuta_id = $this->get('minuta_id');
        $modMinuta = MinutaEmpenho::find($minuta_id);
        $tipo = $modMinuta->empenho_por;
        $valores = $this->valor_total;

        // precisaremos um array de retorno idêntico ao array valor_total_item.
        $arrayRetorno = $this->valor_total_item;
        try {
            if ($tipo === 'Compra') {
                foreach ($valores as $key => $value) {

                    $operacao = explode('|', $this->tipo_alteracao[$key]);
                    switch ($operacao[1]) {
                        case 'REFORÇO':

                            /***
                            Permitir que o valor Total obtido possa ser até 25% maior do que a Valor Total do item na compra - saldo já empenhado:
                            Valor Total <= 'compra_item_unidade.quantidade_autorizada' valor unitário 1.25 - ('compra_item_unidade.quantidade_autorizada' - 'compra_item_unidade.quantidade_saldo') * valor unitário
                            **/
                            $valorInformado = $this->valor_total[$key];
                            $valorInformado =  str_replace("." , "" , $valorInformado);
                            $valorInformado =  str_replace("," , "." , $valorInformado);
                            $valorInformado = number_format($valorInformado, 2, '.', '');
                            $quantidadeInformada = $this->qtd[$key];
                            $valorTotalDoItem = $this->valor_total_item[$key];
                            $compraItemId = $this->compra_item_id[$key];
                            $objCompraItemFornecedor = CompraItemFornecedor::where('compra_item_id', $compraItemId)->first();
                            $valorUnitarioCompraItemFornecedor = $this->valor_unitario[$key];
                            $valorUnitarioCompraItemFornecedor = floatval($objCompraItemFornecedor->valor_unitario);
                            $compraItemUnidade = CompraItemUnidade::where('compra_item_id', $compraItemId)
                                ->where('unidade_id', session('user_ug_id'))
                                ->first();
                            $quantidadeAutorizadaCompraItemUnidade = $compraItemUnidade->quantidade_autorizada;
                            $quantidadeSaldoCompraItemUnidade = $compraItemUnidade->quantidade_saldo;
                            $valorTotalDaCompra = ( $quantidadeAutorizadaCompraItemUnidade * $valorUnitarioCompraItemFornecedor );

                            $valorTotalCompra25PorCento = $valorTotalDaCompra;

                            $valorFinalParaVerificacao = ( $valorTotalCompra25PorCento - ( $quantidadeAutorizadaCompraItemUnidade - $quantidadeSaldoCompraItemUnidade ) * $valorUnitarioCompraItemFornecedor );
                            $valorFinalParaVerificacaoArredondado = round($valorFinalParaVerificacao, 2);

                            // preparar o array retorno, setando o valor indexado pela key
                            $arrayRetorno[$key] = (string)$valorFinalParaVerificacaoArredondado;    // precisamos que o valor seja retornado em string.
                            break;
                    }
                }
                return $arrayRetorno;
            }
        } catch (Exception $exc) {
            Log::error($exc);
        }
    }
}
