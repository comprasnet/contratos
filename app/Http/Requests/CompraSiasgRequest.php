<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Http\Traits\Formatador;
use Illuminate\Foundation\Http\FormRequest;

class CompraSiasgRequest extends FormRequest
{
    use Formatador;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required_if:tipoEmpenho,1',
            'unidade_origem_id' => 'required_if:tipoEmpenho,2',
            'modalidade_id' => 'required_if:tipoEmpenho,2',
            'numero_ano' => 'required_if:tipoEmpenho,2',
            'fornecedor_empenho_id' => 'required_if:tipoEmpenho,3',
            'numero_contratacao' => [
                'required_if:tipoEmpenho,4'
            ],
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'id' => "Contrato",
            'unidade_origem_id' => "Unidade Compra",
            'modalidade_id' => "Modalidade Licitação",
            'numero_ano' => "Numero / Ano",
            'fornecedor_empenho_id' => "Suprido",
            'tipoEmpenho' => "Tipo",
            'numero_contratacao' => "Id contratação PNCP",
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'id.required_if' => 'O campo :attribute é obrigatório quando Tipo é Contrato.',
            'unidade_origem_id.required_if' => 'O campo :attribute é obrigatório quando Tipo é Compra.',
            'modalidade_id.required_if' => 'O campo :attribute é obrigatório quando Tipo é Compra.',
            'numero_ano.required_if' => 'O campo :attribute é obrigatório quando Tipo é Compra.',
            'fornecedor_empenho_id.required_if' => 'O campo :attribute é obrigatório quando Tipo é Suprimento.',
            'numero_contratacao.required_if' => 'O campo :attribute é obrigatório quando Tipo é Contrata+Brasil.',
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation(): void
    {
        $numero_ano = explode('/', $this->input('numero_ano'));
        $this->merge([
            'numero_compra' => !empty($numero_ano[0]) ? $numero_ano[0] : null,
            'ano_compra' => $numero_ano[1] ?? null,
            'uasg_beneficiaria_id' => $this->input('uasg_beneficiaria_id'),
            'contrato_id' => $this->input('id'),
            'numero_contratacao' => $this->converterIdContratacaoPNCP($this->input('numero_contratacao')),
            'numero_contratacao_original' => $this->input('numero_contratacao'),
        ]);
    }


}
