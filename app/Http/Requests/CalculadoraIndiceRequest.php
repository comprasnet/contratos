<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\services\Indices\Implementations\Calculadora;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Validator;

class CalculadoraIndiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'data_inicio' => 'required',
            'data_fim' => 'required',
            'valor' => 'required',
            'tipo_indice' => 'required',
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }

    public function withValidator(Validator $validator): void
    {
        $validator->after(function (Validator $validator) {
            $calculadora = new Calculadora($this->data_inicio, $this->data_fim, $this->valor, $this->tipo_indice);
            if ($calculadora->checkDateRange($this->data_fim) == false) {
                $validator->errors()->add('data_fim', 'Não existe série registrada para o período selecionado.');
            }
            if(is_null($this->valor)){
                $validator->errors()->add('valor', 'É necessário informar um valor válido.');
            }
        });

    }
}
