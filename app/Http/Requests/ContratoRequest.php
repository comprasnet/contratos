<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Http\Traits\RegrasDataPublicacao;
use App\Models\AmparoLegal;
use App\Models\Codigoitem;
use App\Models\Unidade;
use App\Rules\AnoContratoValido;
use App\Models\Contrato;
use App\Rules\NaoAceitarInativarContratoHistorico;
use App\Rules\NaoAceitarMinutaCompraDiferente;
use App\Rules\NaoPermitirItensZeradosOuVazios;
use App\Rules\NaoPermitirTipoMaterialVazio;
use App\Rules\NaoPermitirValorTotalZeradoItemContrato;
use App\Rules\NaoPermitirValorUnitarioZeradoItemContrato;
use App\Rules\ValidarCompra;
use App\Rules\ValidarUnidadeSisgContrato;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Unique;

class ContratoRequest extends FormRequest
{
    use RegrasDataPublicacao;

    protected $data_limite;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /*
         *
         * IMPORTANTE PARA OS DESENVOLVEDORES!!!!
         *
         * TODA E QUALQUER VALIDAÇÃO RELACIONADA A COMPRA DEVE SER ADICIONADA
         * NA REGRA DE VALIDARCOMPRA.
         *
         *
         * FAZEMOS ISTO PARA EVITAR REDUNDÂNCIA DE CONSUMO DA API SIASG/NDC.
         *
         * */

        $request = request()->all();
        $amparos = false;
        if (array_key_exists('amparoslegais', $request)) {
            $amparos = AmparoLegal::verificarLei14133($request['amparoslegais']);
        }
        $id = $this->id ?? "NULL";
        $unidadeorigem_id = $this->unidadeorigem_id ?? "NULL";
        $unidade_id = $this->unidade_id ?? "NULL";
        $tipo_id = $this->tipo_id ?? "NULL";
        $this->data_limitefim = date('Y-m-d', strtotime('+50 year'));
        $this->data_limiteinicio = date('Y-m-d', strtotime('-50 year'));
        $this->data_atual = date('Y-m-d');
        $this->minutasempenho = request()->input('minutasempenho') ?? [];
        $modalidade_id = $this->modalidade_id ?? null;
        $unidadecompra_id = $this->unidadecompra_id ?? null;
        $unidadebeneficiaria_id = $this->unidadebeneficiaria_id ?? null;
        $licitacao_numero = $this->licitacao_numero ?? null;
        $valor_global = $this->valor_global ?? null;
        $elaboracao = $this->elaboracao ?? null;
        $arrayEmpenho = $request['empenho'] ?? null;
        $this->anoContrato = Contrato::extractAno($request['numero'] ?? null);
        $modalidade = $modalidade_id !== null ? Codigoitem::find($modalidade_id) : null;
        $contrataMaisBrasil = request()->input('contrata_mais_brasil');

        $rules = [
            'numero' => [
                'required',
                (new Unique('contratos', 'numero'))
                    ->ignore($id)
                    ->where('unidadeorigem_id', $unidadeorigem_id)
                    ->where('tipo_id', $tipo_id),
                new AnoContratoValido()
            ],
            'qtd_item' => 'required|array|min:1',
            'qtd_item.*' => new NaoPermitirItensZeradosOuVazios($this->numero_item_compra),
            'tipo_material.*' => new NaoPermitirTipoMaterialVazio($this->tipo_material, $this->tipo_item_id),
            'fornecedor_id' => 'required',
            'minutasempenho' => new NaoAceitarMinutaCompraDiferente,
            'numero_contratacao' => 'required_if:contrata_mais_brasil,1',
            'tipo_id' => [
                'required',
                new ValidarCompra(
                    $modalidade_id,
                    $unidadecompra_id,
                    $unidadebeneficiaria_id,
                    $licitacao_numero,
                    $valor_global,
                    $unidadeorigem_id,
                    $amparos,
                    null,
                    $request['amparoslegais'] ?? [],
                    $unidade_id,
                    $this->anoContrato,
                    $tipo_id,
                    $modalidade,
                    $contrataMaisBrasil,
                    request()->input('numero_contratacao')
                )],
            'categoria_id' => 'required',
            'receita_despesa' => [
                'required',
                new ValidarUnidadeSisgContrato($this->receita_despesa, $this->minutasempenho, $arrayEmpenho, $this->modalidade_id)
            ],
            'unidade_id' => 'required',
            'unidadeorigem_id' => 'required',
            'processo' => 'required',
            'objeto' => ['required', function ($attribute, $value, $fail) {
                // Normalizar quebras de linha para `\n`
                $normalizedValue = str_replace(["\r\n", "\r"], "\n", $value);
                $charCount = mb_strlen($normalizedValue, 'UTF-8');
                if ($charCount > 5120) {
                    $fail('O campo objeto deve ter no máximo 5120 caracteres.');
                }
            }],
            'info_complementar' => [function ($attribute, $value, $fail) {
                // Normalizar quebras de linha para `\n`
                $normalizedValue = str_replace(["\r\n", "\r"], "\n", $value);
                $charCount = mb_strlen($normalizedValue, 'UTF-8');
                if ($charCount > 5120) {
                    $fail('O campo informação complementar deve ter no máximo 5120 caracteres.');
                }
            }],
            'modalidade_id' => 'required',

            'licitacao_numero' => [
                Rule::requiredIf(
                    function () use ($modalidade, $contrataMaisBrasil) {
                        return $this->validarCompraSemExigencia($modalidade, $contrataMaisBrasil);
                    }
                ),
                function ($attribute, $value, $fail) {
                    if (empty($value)) {
                        return;
                    }

                    list(, $ano) = explode('/', $value);
                    $anoLimite = (date('Y') + 1);
                    if ($ano > $anoLimite) {
                        $fail("Não é permitido o registro de compra com ano superior a {$anoLimite}.");
                    }
                }
            ],
            'unidadecompra_id' => Rule::requiredIf(
                function () use ($modalidade, $contrataMaisBrasil) {
                    return $this->validarCompraSemExigencia($modalidade, $contrataMaisBrasil);
                }
            ),
            'data_assinatura' => "nullable|required_if:elaboracao,0|date|after:{$this->data_limiteinicio}|before_or_equal:{$this->data_atual}",
            'valor_global' => 'required',
            'num_parcelas' => 'required',
            'valor_parcela' => 'required',
            'amparoslegais' => 'required',
            'prorrogavel' => 'required',
            'situacao' => 'required',
            'codigo_sistema_externo' => Rule::requiredIf(
                function () {
                    $objUnidade = Unidade::join('orgaoconfiguracao', 'unidades.orgao_id', '=', 'orgaoconfiguracao.orgao_id')
                        ->where('unidades.id', '=', session()->get('user_ug_id'))
                        ->select('codigo_sistema_externo_obrigatorio')
                        ->first();
                    if ($objUnidade == null) {
                        return false;
                    } else {
                        if ($objUnidade->codigo_sistema_externo_obrigatorio === true) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
            ),
            'elaboracao' => ['required', new NaoAceitarInativarContratoHistorico($id, Contrato::class)],
            'aplicavel_decreto' => Rule::requiredIf(function () {
                $tipo = Codigoitem::find($this->tipo_id);
                $categoria = Codigoitem::find($this->categoria_id);

                if (empty($tipo) || empty($categoria)) {
                    return false;
                }

                if ($tipo->descricao == 'Contrato' && $categoria->descricao == 'Mão de Obra') {
                    return true;
                }

                return false;
            }),
            'contrato_autoridade_signataria' => 'nullable|array|max:1',
        ];

        if ($elaboracao != null) {
            $rules['data_publicacao'] = $this->ruleDataPublicacao($tipo_id, $this->id, $amparos);
        }
        if ($this->receita_despesa != 'S') {
            $rules['vl_unit.*'] = new NaoPermitirValorUnitarioZeradoItemContrato($this->vl_unit);
            $rules['vl_total.*'] = new NaoPermitirValorTotalZeradoItemContrato($this->vl_total);
        }

        $rules['data_publicacao'] = $this->ruleDataPublicacao($tipo_id, $this->id, $amparos);

        if (request()->get('data_proposta_comercial') != null) {
            $rules['data_proposta_comercial'] = 'required|date|before_or_equal:vigencia_inicio';
        }

        if (request()->get('is_prazo_indefinido') == "1") {
            $rules['vigencia_inicio'] = 'required|date|after_or_equal:data_assinatura';
        } else {
            $rules['vigencia_inicio'] = 'required|date|after_or_equal:data_assinatura|before_or_equal:vigencia_fim';
            $rules['vigencia_fim'] = "required|date|after_or_equal:vigencia_inicio|before:{$this->data_limitefim}";
        }

        return $rules;
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'licitacao_numero' => 'Número Compra',
            'contrato_autoridade_signataria' => 'Autoridades Signatárias',
            'data_publicacao' => 'Data da Publicação',
            'numero_contratacao' => 'Id contratação PNCP'
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        $data_limite = implode('/', array_reverse(explode('-', $this->data_limite)));
        $data_hoje = date('d/m/Y');

        return [
            'aplicavel_decreto.required' => 'O campo "Aplicável o Decreto 11.430/2023 à contratação" é obrigatório quando o Tipo for "Contrato" e Categoria for "Mão de Obra".',
            'amparoslegais.required' => 'O campo amparo legal é obrigatório.',
            'categoria_id.required' => 'O campo categoria é obrigatório.',
            'codigo_sistema_externo.required' => 'O campo código sistema externo é obrigatório.',
            'contrato_autoridade_signataria.max' => 'Favor selecionar apenas uma Autoridade Signatária.',
            'data_assinatura.before_or_equal' => "O campo Data de assinatura deve ser anterior ou igual à data atual {$data_hoje}.",
            'data_assinatura.required' => 'O campo data da assinatura é obrigatório.',
            'data_assinatura.required_if' => 'O campo :attribute é obrigatório quando o contrato é Ativo.',
            'data_proposta_comercial.before_or_equal' => "O campo data da proposta deve ser uma data anterior" .
                " ou igual ao início da vigência",
            'data_publicacao.after' => "A Data da Publicação no DOU deve ser posterior à" .
                " Data de Assinatura do Contrato.",
            'data_publicacao.required' => 'O campo data da publicação no DOU é obrigatório quando o contrato é Ativo.',
            'data_publicacao.required_if' => 'O campo :attribute é obrigatório quando o contrato é Ativo.',
            'fornecedor_id.required' => 'O campo fornecedor é obrigatório.',
            'licitacao_numero.required' => 'O campo :attribute é obrigatório.',
            'modalidade_id.required' => 'O campo modalidade licitação é obrigatório.',
            'numero.required' => 'O campo número do contrato é obrigatório.',
            'numero.unique' => 'O número do contrato já está sendo utilizado.',
            'processo.required' => 'O campo número do processo é obrigatório.',
            'prorrogavel.required' => 'O campo Prorrogável é obrigatório.',
            'qtd_item.required' => 'Para salvar um contrato, insira ao menos um item.',
            'tipo_id.required' => 'O campo tipo é obrigatório.',
            'unidadecompra_id.required' => 'O campo unidade da compra é obrigatório.',
            'unidadeorigem_id.required' => 'O campo unidade gestora origem é obrigatório.',
            'valor_parcela.required' => 'O campo valor da parcela é obrigatório.',
            'vigencia_fim.before' => "A :attribute deve ser uma data anterior a {$data_limite}!",
            'vigencia_fim.required' => 'O campo data fim da vigência é obrigatório.',
            'vigencia_inicio.required' => 'O campo data de início da vigência é obrigatório.',
            'objeto.required' => 'O campo objeto é obrigatório.',
            'numero_contratacao.required_if' => 'O campo :attribute é obrigatório quando ' .
                'o campo Contrata+Brasil é Sim.',
        ];
    }

    public function validarCompraSemExigencia($modalidade, $contrataMaisBrasil): bool
    {
        if (isset($modalidade->descricao)
            && in_array($modalidade->descricao, config('app.modalidades_sem_exigencia'))) {
            return false;
        }

        if ($contrataMaisBrasil == '1') {
            return false;
        }

        return true;
    }

    protected function prepareForValidation()
    {
        if (isset($this->objeto) || isset($this->info_complementar)) {
            // Normalizar as quebras de linha para `\n`, caso as chaves existam
            $normalizedObjeto = str_replace(["\r\n", "\r"], "\n", $this->objeto);
            $normalizedInfoComplementar = str_replace(["\r\n", "\r"], "\n", $this->info_complementar);

            return $this->merge([
                'objeto' => $normalizedObjeto,
                'info_complementar' => $normalizedInfoComplementar,
            ]);
        }
    }
}
