<?php

namespace App\Http\Requests;

use App\Models\Contrato;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Unique;

class EncerramentoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->id ?? "NULL";
        $contrato_id = $this->contrato_id ?? "NULL";
        $tipo_id = $this->tipo_id ?? "NULL";

        $this->hoje = date('Y-m-d');

        $validateSaldoDisponivelBloqueado = $this->is_cumpridas_obrigacoes_financeiras === '0' ? 'required|min:0.01|numeric' : '';
        $validateSaldoBloqueadoContaDeposito = $this->is_saldo_conta_vinculada_liberado === '0' ? 'required|min:0.01|numeric' : '';
        $validateSaldoBloqueadoGarantiaContratual = $this->is_garantia_contratual_devolvida === '0' ? 'required|min:0.01|numeric' : '';

        $rules = [
            'contrato_id' => 'required',
            'unidade_id' => 'required',
            'processo' => 'required',
            'vigencia_fim' => 'required|date',
            'data_encerramento' => "required|date|before_or_equal:{$this->hoje}|after_or_equal:vigencia_fim",
            'is_cumpridas_obrigacoes_financeiras' => "required|numeric",
            'is_saldo_conta_vinculada_liberado' => "required|numeric|max:2",
            'is_garantia_contratual_devolvida' => "required|numeric|max:2",
            'planejamento_contratacao_atendida' => 'required',
            'user_id_responsavel_signatario_encerramento' => 'required',
            'te_valor_total_executado' => 'required',
            'te_valor_total_pago' => 'required',
            'te_saldo_disponivel_ou_bloqueado' => $validateSaldoDisponivelBloqueado,
            'te_saldo_bloqueado_garantia_contratual' => $validateSaldoBloqueadoGarantiaContratual,
            'te_saldo_bloqueado_conta_deposito' => $validateSaldoBloqueadoContaDeposito,
            'te_justificativa_nao_cumprimento' => 'required_if:is_cumpridas_obrigacoes_financeiras,==,0',
            'te_justificativa_bloqueio_garantia_contratual' => 'required_if:is_garantia_contratual_devolvida,==,0',
            'te_justificativa_bloqueio_conta_deposito' => 'required_if:is_saldo_conta_vinculada_liberado,==,0',
            'consecucao_objetivos_contratacao' => 'required',
            'sugestao_licao_aprendida' => 'required'
        ];

        // Tratar a regra do número do termo encerramento
        if( !$this->ruleNumero($contrato_id, $tipo_id, $id) ){
            $rules['numero'] = 'required|max:2';       // coloquei o max, para criticar o campo. A mensagem está no método messages.
        } else {
            $rules['numero'] = [
                'required',
                (new Unique('contratohistorico','numero'))
                    ->where('contrato_id',$contrato_id)
                    ->where('tipo_id',$tipo_id)
                    ->ignore($id)
            ];
        }
        return $rules;
    }

    // Tratar a regra do ano do número do termo encerramento - não pode ser anterior ao ano do contrato
    public function ruleNumero($contrato_id, $tipo_id, $id){
        if( $contrato_id != 'NULL' && $contrato_id != 'null' && !is_null($contrato_id) ){
            $objContrato = Contrato::where('id', $contrato_id)->first();
            $numeroContrato = $objContrato->numero;
            $numeroDoTermoEncerramento = $this->numero;
            $anoDoNumeroDoTermoEncerramento = substr($numeroDoTermoEncerramento, -4);
            $numeroContrato = $objContrato->numero;
            $anoDoNumeroDoContrato = substr($numeroContrato, -4);
            if($anoDoNumeroDoTermoEncerramento < $anoDoNumeroDoContrato){
                return false;
            }
        }
        return true;
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'te_valor_total_executado' => 'Valor total executado',
            'te_valor_total_pago' => 'Valor total pago',
            'te_saldo_disponivel_ou_bloqueado' => 'Saldo disponível ou bloqueado',
            'te_saldo_bloqueado_garantia_contratual' => 'Saldo bloqueado',
            'te_saldo_bloqueado_conta_deposito' => 'Saldo bloqueado',
            'te_justificativa_nao_cumprimento' => 'Justificativa de não cumprimento das obrigações financeiras junto à Contratada',
            'te_justificativa_bloqueio_garantia_contratual' => 'Justificativa de bloqueio da garantia contratual',
            'te_justificativa_bloqueio_conta_deposito' => 'Justificativa de bloqueio da Conta-Depósito Vinculada',
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        $data_hoje = date('d/m/Y');

        return [
            'numero.max' => "Ano do Termo de Encerramento não permitido ",
            'data_encerramento.after_or_equal' => 'A data de encerramento não pode ser inferior à data fim da vigência. Verificar a utilização do Termo de Rescisão ',
            'data_encerramento.before_or_equal' => "O campo Data Encerramento do Contrato deve ser anterior ou igual à data atual {$data_hoje}.",
            'is_objeto_contratual_entregue.min' => 'Há pendências contratuais a serem sanadas antes do seu encerramento ',
            'is_cumpridas_obrigacoes_financeiras.min' => 'Há pendências contratuais a serem sanadas antes do seu encerramento ',
            'is_saldo_conta_vinculada_liberado.min' => 'Há pendências contratuais a serem sanadas antes do seu encerramento ',
            'is_saldo_conta_vinculada_liberado.numeric' => 'Há pendências contratuais a serem sanadas antes do seu encerramento ',
            'is_garantia_contratual_devolvida.min' => 'Há pendências contratuais a serem sanadas antes do seu encerramento ',
            'is_garantia_contratual_devolvida.numeric' => 'Há pendências contratuais a serem sanadas antes do seu encerramento ',
            'planejamento_contratacao_atendida.required' => 'Favor responder a pergunta A necessidade formalizada no Planejamento da Contratação (DOD/DFD/ETP/TR/PB) foi plenamente atendida pelo contrato. ',
            'user_id_responsavel_signatario_encerramento.required' => 'Favor informar o signatário. ',
            'te_justificativa_nao_cumprimento.required_if' => 'Campo obrigatório',
            'te_justificativa_bloqueio_garantia_contratual.required_if' => 'Campo obrigatório',
            'te_justificativa_bloqueio_conta_deposito.required_if' => 'Campo obrigatório',
            'consecucao_objetivos_contratacao.required' => 'O campo Consecução dos objetivos da contratação é obrigatório.',
            'sugestao_licao_aprendida.required' => 'O campo Sugestões / Lições aprendidas é obrigatório.',


        ];
    }

    protected function prepareForValidation(): void
    {

        $saldoDispOuBloq = $this->input('te_saldo_disponivel_ou_bloqueado');
        $saldoDispOuBloq = str_replace(['.', ','], ['', '.'], $saldoDispOuBloq);

        $valorTotalExecutado = $this->input('te_valor_total_executado');
        $valorTotalExecutado = str_replace(['.', ','], ['', '.'], $valorTotalExecutado);

        $valorTotalPago = $this->input('te_valor_total_pago');
        $valorTotalPago = str_replace(['.', ','], ['', '.'], $valorTotalPago);

        $saldoBloqueadoGarantiaContratual = $this->input('te_saldo_bloqueado_garantia_contratual');
        $saldoBloqueadoGarantiaContratual = str_replace(['.', ','], ['', '.'], $saldoBloqueadoGarantiaContratual);

        $saldoBloqueadoContaDeposito = $this->input('te_saldo_bloqueado_conta_deposito');
        $saldoBloqueadoContaDeposito = str_replace(['.', ','], ['', '.'], $saldoBloqueadoContaDeposito);


        $this->merge([
            'te_saldo_disponivel_ou_bloqueado' => $saldoDispOuBloq,
            'te_valor_total_executado' => $valorTotalExecutado,
            'te_valor_total_pago' => $valorTotalPago,
            'te_saldo_bloqueado_garantia_contratual' => $saldoBloqueadoGarantiaContratual,
            'te_saldo_bloqueado_conta_deposito' => $saldoBloqueadoContaDeposito,
        ]);
    }
}
