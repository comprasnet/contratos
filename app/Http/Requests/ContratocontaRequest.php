<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class ContratocontaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // aqui é preciso verificar o tipo de conta
        if($this->is_conta_vinculada_pela_resolucao169_cnj){
            return [
                'banco' => 'required|min:2|max:255',
                'agencia' => 'required|min:2|max:255',
                'conta_corrente' => 'required|min:2|max:255',
                'percentual_13' => 'required',
                'percentual_ferias' => 'required',
                'percentual_abono_ferias' => 'required',
                'percentual_multa_sobre_fgts' => 'required',
                'percentual_submodulo22' => 'required',
            ];

        } else {
            return [
                'banco' => 'required|min:2|max:255',
                'agencia' => 'required|min:2|max:255',
                'conta_corrente' => 'required|min:2|max:255',
            ];
        }
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
