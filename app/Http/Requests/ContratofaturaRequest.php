<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Rules\EmpenhoRequired;
use App\Rules\TotalEmpenhoIgualValorTotalFaturado;
use App\Rules\ValidaChaveNfeIC;
use Illuminate\Foundation\Http\FormRequest;
use App\Rules\MesAnoFaturaRequired;
use App\Rules\TotalMesReferenciaIgualValorTotalFaturado;
use Illuminate\Validation\Rule;

class ContratofaturaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->hoje = date('Y-m-d');
        return [
            'contrato_id' => 'required',
            'tipolistafatura_id' => 'required',
            'numero' => [
                'required',
                $this->getUniqueSerienumero()
            ],
            'chave_nfe' => [
                new ValidaChaveNfeIC($this->tipo_de_instrumento_de_cobranca_id)
            ],
            'tipo_de_instrumento_de_cobranca_id' => 'required',
            'emissao' => "required|date|before_or_equal:{$this->hoje}",
            'vencimento' => 'required|date|after_or_equal:emissao',
            'processo' => 'required|max:20',
            'protocolo' => "required|date|after_or_equal:emissao|before_or_equal:ateste",
            'ateste' => "required|date|after_or_equal:protocolo|before_or_equal:{$this->hoje}",
            'repactuacao' => 'required',
            'infcomplementar' => 'max:255',
            'mes_ano_json' => [
                new MesAnoFaturaRequired,
                new TotalMesReferenciaIgualValorTotalFaturado($this->valortotalfaturado_calc)
            ],
            'empenho_json' => [
                new EmpenhoRequired($this->contrato_id),
                new TotalEmpenhoIgualValorTotalFaturado($this->valortotalfaturado_calc, $this->contrato_id)
            ],
            'valorref.*' => 'required',
            'mesref.*' => 'required',
            'anoref.*' => 'required',
            //'numero_empenho.*' => 'required',
            // 'empenhovalorref.*' => 'required',
            'paisfabricacao_id.*' => 'required',
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'emissao' => "Dt. Emissão",
            'tipolistafatura_id' => "Tipo Lista Fatura",
            'tipo_de_instrumento_de_cobranca_id' => "Tipo de Instrumento de Cobrança",
            'vencimento' => "Dt. Limite Pagamento",
            'protocolo' => "Dt. Recebimento",
            'ateste' => "Dt. Liquidação de Despesa",
            'mesref.*' => 'Mês de Referência',
            'anoref.*' => 'Ano de Referência',
            'valorref.*' => 'Valor de Referência',
            'numero_empenho.*' => 'Empenho',
            'empenhovalorref.*' => 'Valor',
            'valortotal_faturado' => 'Valor Total Faturado',
            'valor' => 'Valor',
            'valorliquido' => 'Valor Total Faturado',
            'paisfabricacao_id.*' => 'País de Fabricação',
            'serie' => 'Série',
            'numero' => 'Número',
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'emissao.before_or_equal' => "O campo :attribute deve ser uma data anterior ou igual a Hoje!",
            'vencimento.after_or_equal' => "O campo :attribute deve ser uma data posterior ou igual a Dt. Emissão!",
            'protocolo.after_or_equal' => "O campo :attribute deve ser uma data posterior ou igual a Dt. Emissão!",
            'ateste.before_or_equal' => "O campo :attribute deve ser uma data anterior ou igual à data atual!",
            'numero.unique' => 'Já existe este :attribute de instrumento de cobrança para este contrato.'
        ];
    }

    /**
     * @return \Illuminate\Validation\Rules\Unique
     */
    public function getUniqueSerienumero(): \Illuminate\Validation\Rules\Unique
    {
        return Rule::unique('contratofaturas')->where(function ($query) {
            return $query->where('contrato_id', $this->input('contrato_id'))
                ->where('serie', $this->input('serie'))
                ->where('numero', $this->input('numero'))
                ->whereNull('deleted_at');
        })->ignore($this->input('id'));
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'mes_ano_json' => json_decode($this->input('mes_ano_json'), true),
            'empenho_json' => json_decode($this->input('empenho_json'), true),
        ]);
    }
}
