<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AmparoLegalConsultaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'modalidade' => 'nullable|string',
            'ato_normativo' => 'nullable|string',
            'artigo' => 'nullable|integer',
            'paragrafo' => 'nullable|integer',
            'inciso' => 'nullable|string',
            'alinea' => 'nullable|string',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            // Verifica se todos os filtros estão nulos
            if (!$this->hasAnyFilter()) {
                $validator->errors()->add('filters', 'Pelo menos um filtro deve ser preenchido.');
            }
        });
    }

    private function hasAnyFilter()
    {
        return $this->filled('modalidade') ||
               $this->filled('ato_normativo') ||
               $this->filled('artigo') ||
               $this->filled('paragrafo') ||
               $this->filled('inciso') ||
               $this->filled('alinea');
    }
}
