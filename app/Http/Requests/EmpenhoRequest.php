<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Validators\EmpenhoValidation;

class EmpenhoRequest extends FormRequest
{
    public function authorize()
    {
        return backpack_auth()->check();
    }

    public function rules()
    {
        return EmpenhoValidation::rules($this, $this->id);
    }

    public function messages()
    {
        return EmpenhoValidation::messages();
    }
}
