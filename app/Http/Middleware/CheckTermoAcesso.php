<?php

namespace App\Http\Middleware;

use App\Http\Traits\UsuarioAcessoGovBrTrait;
use App\Models\TermoAceite;
use Carbon\Carbon;
use Closure;

class CheckTermoAcesso
{
    use UsuarioAcessoGovBrTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $dataAcessoSomenteGov = config('acessogov.data_acesso_somente_gov');

        $dataLimite = Carbon::parse($dataAcessoSomenteGov);

        if ($dataLimite->lessThanOrEqualTo(Carbon::now())) {
            return $next($request);
        }

        if (auth()->check()) {
            if (session('logged_in') == 'gov') {
                $verificaTermo = TermoAceite::where('user_id', backpack_auth()->user()->id)
                ->whereHas('tipo', function ($e) {
                    $e->where('descres', 'ACESSO');
                })->first();

                return $verificaTermo ? $next($request) : redirect()->to('/termo-acesso');
            }

            return session('termoAcesso') ? $next($request) : redirect()->to('/termo-acesso');
        }
        return $next($request);
    }
}
