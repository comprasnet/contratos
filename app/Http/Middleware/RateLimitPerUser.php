<?php

    namespace App\Http\Middleware;

    use Closure;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Cache;

    class RateLimitPerUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('api')->check()) {
            $request->merge(['authenticated_user' => Auth::user()]);
        }

        // Identificar pelo ID do usuário ou pelo IP
        $userId = Auth::user() ? Auth::user()->id : $request->ip();
        $cacheKey = 'rate_limit:' . $userId;
        $maxRequests = 1000;
        $timeWindow = 60; // Janela de tempo em segundos (1 minuto)

        // Recuperar ou inicializar os dados da janela
        $rateLimitData = Cache::get($cacheKey);

        if (!$rateLimitData) {
            // Inicializar a janela de limite se não existir no cache
            $rateLimitData = [
                'requests' => 0,
                'start_time' => now()->timestamp // Início da janela
            ];
        }

        // Calcular o tempo restante da janela
        $currentTime = now()->timestamp;
        $elapsedTime = $currentTime - $rateLimitData['start_time'];
        $remainingTime = max($timeWindow - $elapsedTime, 0);

        // Reiniciar a janela se o tempo expirou
        if ($elapsedTime >= $timeWindow) {
            $rateLimitData = [
                'requests' => 0,
                'start_time' => $currentTime
            ];
            $remainingTime = $timeWindow; // Reinicia o tempo restante
        }

        // Incrementar o contador de requisições
        $rateLimitData['requests']++;

        if ($rateLimitData['requests'] > $maxRequests) {
            return response()->json([
                'message' => 'Too many requests. Please try again later.',
                'userId' => $userId,
                'time_to_reset' => $remainingTime
            ], 429)->header('X-RateLimit-Limit', $maxRequests)
                ->header('X-RateLimit-Remaining', 0)
                ->header('X-RateLimit-Reset', $remainingTime);
        }

        // Atualizar o cache com os novos dados e tempo de expiração
        Cache::put($cacheKey, $rateLimitData, $remainingTime);

        // Processar a requisição
        $response = $next($request);

        // Calcular requisições restantes
        $remaining = $maxRequests - $rateLimitData['requests'];

        // Adicionar cabeçalhos na resposta
        return $response->header('X-RateLimit-Limit', $maxRequests)
            ->header('X-RateLimit-Remaining', max($remaining, 0))
            ->header('X-RateLimit-Reset', $remainingTime) // Tempo restante para expiração
            ->header('X-User-ID', $userId); // ID do usuário no cabeçalho
    }
}
