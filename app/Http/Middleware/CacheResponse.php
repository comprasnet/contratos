<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class CacheResponse
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $cacheKey = $this->getCacheKey($request);

        $validParams = ['orgao', 'unidade', 'fornecedor', 'contrato'];

        if ($request->hasAny($validParams)) {
            return $next($request);
        }

        //Verifica se o cache existe
        if (Cache::has($cacheKey)) {
            // Recupera o conteúdo do cache e cria uma nova resposta
            $responseContent = Cache::get($cacheKey);
            // Log::info('Cache hit', ['key' => $cacheKey]);

            return response($responseContent);
        }

        // Log::info('Cache miss', ['key' => $cacheKey]);

        // Se o cache não existe, gera uma nova resposta
        $response = $next($request);

        // Verifica se a resposta é uma instância de SymfonyResponse
        if ($response instanceof SymfonyResponse) {
            // Define o tempo de expiração para 30 minutos
            $expiresAt = now()->addMinutes(30);

            // Armazena a resposta no cache
            Cache::put($cacheKey, $response->getContent(), $expiresAt);

            // Log::info('Cache created', ['key' => $cacheKey, 'expires_at' => $expiresAt]);
        }

        return $response;
    
    }

    protected function getCacheKey($request)
    {
        $filters = $request->query();
        return 'route_' . md5($request->url() . json_encode($filters));
    }
}