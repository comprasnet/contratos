<?php

namespace App\Http\Middleware;

use App\Http\Traits\Formatador;
use App\User;
use Closure;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Redis;

class UgprimariaMiddleware
{
    use Formatador;
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (backpack_user()) {
            $dadosUsuario = backpack_user()->toArray();
            $dadosUsuarioRedis = null;
            $tipoSessao = config("session.driver");

            $dadosUsuarioRedis = $this->recuperarDadosUsuarioRedis(auth()->id());
            if (!empty($dadosUsuarioRedis)) {
                $unidade = backpack_user()->unidadeprimaria($dadosUsuarioRedis['ugprimaria']);
                if (is_object($unidade)) {
                    session(['user_ug' => $unidade->codigo]);
                    session(['user_ug_id' => $unidade->id]);
                    session(['user_orgao_id' => $unidade->orgao_id]);
                    session(['user_ug_sisg' => $unidade->sisg]);
                    session(['user_ug_nome_resumido' => $unidade->nomeresumido]);
                } else {
                    session(['user_ug' => null]);
                    session(['user_ug_id' => null]);
                    session(['user_orgao_id' => null]);
                    session(['user_ug_sisg' => null]);
                    session(['user_ug_nome_resumido' => null]);
                }
                $this->salvarDadosUsuarioRedis($dadosUsuarioRedis);
                return $next($request);
            }

            if (!empty(session()->get('user_ug')) and ! empty(session()->get('user_ug_id'))) {
                if (backpack_user()->ugprimaria) {
                    $unidade = backpack_user()->unidadeprimaria(session()->get('user_ug_id'));

                    if ($unidade) {

                        $userUg = $dadosUsuarioRedis['user_ug'] ?? $unidade->codigo;
                        $userUgId = $dadosUsuarioRedis['user_ug_id'] ?? $unidade->id;
                        $userOrgaoId = $dadosUsuarioRedis['user_orgao_id'] ?? $unidade->orgao_id;

                        session(['user_ug' => $userUg]);
                        session(['user_ug_id' => $userUgId]);
                        session(['user_orgao_id' => $userOrgaoId]);

                        $dadosUsuario['user_ug'] = $userUg;
                        $dadosUsuario['user_ug_id'] = $userUgId;
                        $dadosUsuario['user_orgao_id'] =  $userOrgaoId;

                        session(['user_ug_sisg' => $unidade->sisg]);
                        session(['user_ug_nome_resumido' => $unidade->nomeresumido]);
                        session(['user_ug_nome' => $unidade->nome]);
                    } else {
                        session(['user_ug' => null]);
                        session(['user_ug_id' => null]);
                        session(['user_orgao_id' => null]);

                        $dadosUsuario['user_ug'] = null;
                        $dadosUsuario['user_ug_id'] = null;
                        $dadosUsuario['user_orgao_id'] = null;
                        session(['user_ug_sisg' => null]);
                        session(['user_ug_nome_resumido' => null]);
                        session(['user_ug_nome' => null]);
                    }
                } else {
                    session(['user_ug' => null]);
                    session(['user_ug_id' => null]);

                    $dadosUsuario['user_ug'] = null;
                    $dadosUsuario['user_ug_id'] = null;

                    session(['user_ug_sisg' => null]);
                    session(['user_ug_nome_resumido' => null]);
                    session(['user_ug_nome' => null]);
                }

                $this->salvarDadosUsuarioRedis($dadosUsuario);

            } elseif (session()->get('user_ug') == null and session()->get('user_ug_id') == null) {
                if (backpack_user()->ugprimaria) {
                    $unidade = backpack_user()->unidadeprimaria(backpack_user()->ugprimaria);
                    if ($unidade) {

                        $userUg = $dadosUsuarioRedis['user_ug'] ?? $unidade->codigo;
                        $userUgId = $dadosUsuarioRedis['user_ug_id'] ?? $unidade->id;
                        $userOrgaoId = $dadosUsuarioRedis['user_orgao_id'] ?? $unidade->orgao_id;

                        session(['user_ug' => $userUg]);
                        session(['user_ug_id' => $userUgId]);
                        session(['user_orgao_id' => $userOrgaoId]);

                        $dadosUsuario['user_ug'] = $userUg;
                        $dadosUsuario['user_ug_id'] = $userUgId;
                        $dadosUsuario['user_orgao_id'] =  $userOrgaoId;

                        session(['user_ug_sisg' => $unidade->sisg]);
                        session(['user_ug_nome_resumido' => $unidade->nomeresumido]);
                        session(['user_ug_nome' => $unidade->nome]);
                    } else {
                        session(['user_ug' => null]);
                        session(['user_ug_id' => null]);
                        session(['user_orgao_id' => null]);

                        $dadosUsuario['user_ug'] = null;
                        $dadosUsuario['user_ug_id'] = null;
                        $dadosUsuario['user_orgao_id'] = null;
                        session(['user_ug_sisg' => null]);
                        session(['user_ug_nome_resumido' => null]);
                        session(['user_ug_nome' => null]);
                    }
                } else {
                    session(['user_ug' => null]);
                    session(['user_ug_id' => null]);
                    session(['user_orgao_id' => null]);

                    $dadosUsuario['user_ug'] = null;
                    $dadosUsuario['user_ug_id'] = null;
                    $dadosUsuario['user_orgao_id'] = null;

                    session(['user_ug_sisg' => null]);
                    session(['user_ug_nome_resumido' => null]);
                    session(['user_ug_nome' => null]);
                }

                $this->salvarDadosUsuarioRedis($dadosUsuario);

            } else {
                // se chegou aqui é porque tem o user_ug e user_ug_id - se for adm, vamos deixar passar
                if (backpack_user()->hasRole('Administrador')
                    || backpack_user()->hasRole('Administrador Suporte')
                    || backpack_user()->hasRole('Consulta Global'))
                {
                    $ok = true;
                } else {
                    $ok = backpack_user()->havePermissionUg(session()->get('user_ug_id'));
                }

                if ($ok == false) {
                    \Session::flush();
                    backpack_auth()->logout();
                    return Redirect::to('/inicio');
                }
            }
        }

        if(!filter_var($dadosUsuario['email'], FILTER_VALIDATE_EMAIL)){
            \Session::flush();
            backpack_auth()->logout();
            return Redirect::to('/login')->withErrors(
                [
                    'email_erro_govbr' => 'O e-mail do seu cadastro está inválido.
                        Acesse o sistema pelo Acesso Gov.br ou contate o Administrador de seu órgão
                        ou unidade para atualizar seu cadastro.'
                ]);
        }

        return $next($request);
    }
}
