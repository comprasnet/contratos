<?php
 
namespace App\Http\Middleware;
 
use App\Models\Empenho;
use App\Models\EmpenhoDetalhado;
use App\Models\Nonce;
use App\Models\RequisicoesNonce;
#use App\Repositories\V2\NonceRepository;
use App\Repositories\Nonce\NonceRepository;
use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Traits\NonceTrait;
 
 
/**
 *
 */
class NonceMiddlewareRequest
{
    /**
     * @var NonceRepository
     */
    private $nonceRepository;
 
    /**
     * @param NonceRepository $nonceRepository
     */
    public function __construct(NonceRepository $nonceRepository)
    {
        $this->nonceRepository = $nonceRepository;
    }
 
    /**
     * @var int[]
     */
    private $codigosHTTPSucesso = [
        200, #OK
        201, #Created
        /*204, #No Content*/
    ];
 
    use NonceTrait;
 
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse) $next
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function handle(Request $request, Closure $next)
    {
        # Armazena os dados que vieram no request
        $requestArray = request()->all();
       
       
        # Obtém o nonce recebido na requisição
        $nonceRecebidoNaRequisicao = $requestArray['nonce'] ?? null;
    
              # Verifica se a requisição enviou o número nonce
   
              # Verifica se a requisição enviou o número nonce
        if ($nonceRecebidoNaRequisicao) {
 
            # Verifica se o nonce já existe
            if ($nonce = $this->nonceRepository->nonceExists($nonceRecebidoNaRequisicao)) {
                # Monta informações específicas para quando o nonce existe
                $error = [
                    'error' => $nonce->error_message,
                    'dados_nonce' => [
                        'nonce' => $nonce->nonce,
                        'status' => $nonce->status,
                        'json_response' => (json_decode($nonce->json_response) !== null) ? json_decode($nonce->json_response) : $nonce->json_response,
                        'funcionalidade' => $nonce->funcionalidade,
                        'json_request' => (json_decode($nonce->json_request) !== null) ? json_decode($nonce->json_request) : $nonce->json_request,
                    ]
                ];
 
                # Cria o histórico desta requisição
                $this->createRequisicoesNonce($nonce->id, $request->all(), $error);
                return response()->json($error, Response::HTTP_BAD_REQUEST);
            }
 
            # Executa a rota da requisição e obtém a resposta para tratar o nonce
            $response = $next($request);
            Log::info('Dados da resposta', ['response' => $response]);
            # Busca os dados do nonce para registrar na resposta e no histórico
            $nonceCadastrado = Nonce::where('nonce', $nonceRecebidoNaRequisicao)
            ->where('user_id', Auth::user()->id)
            ->first();
            
            # Se a rota retornou algum erro (não retornar um código considerado sucesso)
            # vai registrar um nonce informando que deu erro
            if (!in_array($response->getStatusCode(), $this->codigosHTTPSucesso)) {
                 # Obtém as mensagens de erro retornadas
                $returning = json_decode($response->getContent());
                $message = $returning->message ?? null;
                $errors = $returning->errors->generalized[0] ?? null; # Quando ocorre uma exceção
                
                $mensagemDeRetorno = $this->formatatMensagemDeErro($message, $errors);
                
                # Obtém a funcionalidade da rota
                $action = $this->getActionInfo();
                
                # Formata o retorno
                $error = [
                    'status' => 'error',
                    'message' => $mensagemDeRetorno
                ];
                # Se o nonce não foi cadastrado na rota, é porque deu algum erro desconhecido
                # Neste caso, cadastra o nonce para registrar o erro na execução
                if (!$nonceCadastrado) {
                    $nonceCadastrado = $this->createdNonceError($request->nonce, $mensagemDeRetorno, $action, $request->all());
                }
                
                # Registra o histórico de requisições
                $this->createRequisicoesNonce($nonceCadastrado->id, $request->all(), $error);
                return response()->json(
                    $error,
                    $response->getStatusCode()
                );
            }
          
            # Se não ocorreu erro na execução da rota, registra a resposta no histórico de nonce
            $this->createRequisicoesNonce($nonceCadastrado->id, $request->all(), $response->getContent());
            return $response;
        }
        # Se não foi enviado um nonce na requisição
        return response()->json([
            'status' => 'error',
            'message' => 'O campo Nonce é obrigatório.'
        ]);
    }
 
    /**
     * @param string $nonce
     * @param string $response
     * @param array $action
     * @param array $request
     * @return Nonce|null
     */
    private function createdNonceError(string $nonce, string $response, array $action, array $request)
    {
        # Foi utilizado DB::table pois utilizando diretamente o modelo,
        # o campo nonceable_type sempre era salvo com 'null'
        $id = DB::table('nonce')->insertGetId([
            'user_id' => Auth::user()->id,
            'nonce' => $nonce,
            'ip' => request()->ip(),
            'nonceable_type' => $action['nonceable_type'],
            'nonceable_id' => null,
            'status' => false,
            'funcionalidade' => $action['funcionalidade'],
            'json_request' => json_encode($request),
            'json_response' => $response,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        return Nonce::find($id);
    }
 
    /**
     * @param int $nonceId
     * @param array $requisicaoEnviada
     * @param $respostaApi
     * @return void
     */
    private function createRequisicoesNonce(int $nonceId, array $requisicaoEnviada, $respostaApi)
    {
        RequisicoesNonce::create([
            'nonce_id' => $nonceId,
            'requisicao_enviada' => $requisicaoEnviada,
            'resposta_api' => $respostaApi
        ]);
    }
 
    /**
     * @param string|null $message
     * @param string|null $errors
     * @return string|null
     */
    private function formatatMensagemDeErro(?string $message, ?string $errors)
    {
        if (!empty($message) && !empty($errors)) {
            return "$message: $errors";
        } elseif (!empty($message)) {
            return $message;
        } elseif (!empty($errors)) {
            return $errors;
        } else {
            return null;
        }
    }
}
