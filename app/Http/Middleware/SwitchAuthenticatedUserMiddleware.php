<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Models\BackpackUser as User;
use Illuminate\Support\Facades\Log;

class SwitchAuthenticatedUserMiddleware
{
    public function handle($request, Closure $next)
    {
        //var_dump('primeiro login' . JWTAuth::user());
        // Verifica se o CPF foi passado na requisição
        $cpf = $request->input('cpf_usuario');

        if (!$cpf) {
            return response()->json(['status' => 'cpf_usuario is required'], 400);
        }

        // Tenta encontrar o usuário pelo CPF
        $user = User::where('cpf', $cpf)->first();

        if (!$user) {
            return response()->json(['status' => 'User not found'], 404);
        }

        try {
            // Autentica o usuário com base no CPF e gera um novo token JWT
            if (!$token = JWTAuth::fromUser($user)) {

                return response()->json(['status' => 'Could not create token'], 500);
            }

            JWTAuth::setToken($token);
            auth()->setUser($user);
            // var_dump('segundo login' . JWTAuth::user());

        } catch (JWTException $e) {
            return response()->json(['status' => 'Could not create token'], 500);
        }

        // Adiciona o token no cabeçalho de autorização
        $request->headers->set('Authorization', 'Bearer ' . $token);

        return $next($request);
    }
}
