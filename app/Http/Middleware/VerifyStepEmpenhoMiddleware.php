<?php

namespace App\Http\Middleware;

use App\Http\Traits\BuscaCodigoItens;
use App\Models\ContaCorrentePassivoAnterior;
use App\Models\Codigoitem;
use App\Models\MinutaEmpenhoRemessa;
use Closure;
use Illuminate\Http\Request;
use Alert;
use Route;
use App\Models\MinutaEmpenho;
use URL;

class VerifyStepEmpenhoMiddleware
{
    use BuscaCodigoItens;
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */

    /*
     * Rotas para verificação
     */
    public $rotas_minuta_original = [
        'empenho.minuta.etapa.compra'           => 1,
        'empenho.minuta.etapa.fornecedor'       => 2,
        'empenho.minuta.etapa.item'             => 3,
        'empenho.minuta.etapa.saldocontabil'    => 4,
        'empenho.minuta.etapa.subelemento'      => 5,
//        'empenho.minuta.etapa.subelemento.edit' => 5,
        'empenho.crud./minuta.edit'             => 6,
        'empenho.minuta.etapa.passivo-anterior' => 7,
        'empenho.crud.passivo-anterior.edit'    => 7,
        'empenho.crud./minuta.show'             => 8

    ];

    public $rotas_minuta_alteracao = [
        'empenho.crud.alteracao.create'                => 1,
        'empenho.crud.alteracao.edit'                  => 1,
        'empenho.crud.alteracao.passivo-anterior'      => 2,
        'empenho.crud.alteracao.passivo-anterior.edit' => 2,
        'empenho.crud.alteracao.show'                  => 3

    ];

    public $rotas_minuta_alteracao_fonte = [
        'empenho.crud.alteracaoFonte.index'            => 1,
        'empenho.crud.alteracaoFonte.edit'             => 1,
        'empenho.crud.alteracaoFonte.subelemento'      => 2,
        'empenho.crud.alteracaoFonte.subelemento.edit' => 2,
//        'empenho.crud.alteracao.passivo-anterior.edit' => 2,
        'empenho.crud.alteracaoFonte.show'             => 3

    ];

    public function handle($request, Closure $next)
    {
        session(['fluxo_minuta'     => '']);
        session(['cabecalho_minuta' => '']);
        session(['nova_minuta_id' => null]);

        //SE A ROTA EXISTE NA LISTA DE ROTAS DA MINUTA ORIGINAL
        if (array_key_exists(Route::current()->action['as'], $this->rotas_minuta_original)) {
            session(['fluxo_minuta'     => 'minutaOriginal']);
            session(['cabecalho_minuta' => 'cabecalho']);
            //se for a rota 1 limpa tudo
            if ($this->rotas_minuta_original[Route::current()->action['as']] === 1) {
                session(['empenho_etapa' => '']);
                session(['conta_id' => '']);
                session(['fornecedor_compra' => '']);
                session(['fornecedor_cpf_cnpj_idgener' => '']);
                session(['situacao' => '']);
                session(['unidade_ajax_id' => '']);
                return $next($request);
            }
//            se for a rota 4
            if ($this->rotas_minuta_original[Route::current()->action['as']] === 1) {
                session(['unidade_ajax_id' => '']);
            }

            $minuta_id = Route::current()->parameter('minuta_id')
                ?? Route::current()->parameter('minutum');

            if (is_null($minuta_id)) {
                $conta = ContaCorrentePassivoAnterior::find(Route::current()->parameter('passivo_anterior'));
                $minuta_id = $conta->minutaempenho_id;
                session(['conta_id' => $conta->id]);
            } else {
                $conta = ContaCorrentePassivoAnterior::where('minutaempenho_id', $minuta_id)->first();

                session(['conta_id' => '']);

                if ($conta) {
                    session(['conta_id' => $conta->id]);
                }
            }

            $minuta = MinutaEmpenho::find($minuta_id);
            $anoRota = Route::current()->parameter('ano') ?? now()->year;

            $etapas = [1,2,3];
            if (in_array($this->rotas_minuta_original[Route::current()->action['as']], $etapas)
                && $minuta->fonte_alterada) {

                Alert::warning('<i class="fa fa-warning text-yellow"></i> ' .
                    'Não é possível retornar para as etapas 1, 2 e 3 ao editar ' .
                    'uma minuta de empenho gerada a partir de uma Alteração de Fonte!')->flash();

                return redirect(URL::previous());
            }

            if (
                $minuta->situacao_descricao === "EMPENHO EMITIDO"
                && $this->rotas_minuta_original[Route::current()->action['as']] !== 8
            ) {
                return redirect(URL::previous());
            }
            //caso a minuta seja de outra unidade, redireciona
            if (session('user_ug_id') !== $minuta->unidade_id) {
                return redirect(URL::previous());
            }

            if ($minuta->empenho_por === 'Suprimento'
                && $this->rotas_minuta_original[Route::current()->action['as']] === 2) {
                return redirect(route(
                    'empenho.minuta.etapa.item',
                    ['minuta_id' => $minuta->id, 'fornecedor_id' => $minuta->fornecedor_empenho_id]
                ));
            }

            if ($minuta->situacao->descricao === 'ERRO') {
                $situacao = Codigoitem::wherehas('codigo', function ($q) {
                    $q->where('descricao', '=', 'Situações Minuta Empenho');
                })
                    ->where('descricao', 'EM ANDAMENTO')
                    ->first();
                $minuta->situacao_id = $situacao->id;
                $minuta->save();
            }

            if ($minuta && ($minuta->etapa >= $this->rotas_minuta_original[Route::current()->action['as']]
                    || ($minuta->etapa === 2 && $this->rotas_minuta_original[Route::current()->action['as']] === 3))
            ) {
                session(['minuta_id' => $minuta->id]);
                session(['empenho_etapa' => $minuta->etapa]);
                session(['fornecedor_compra' => $minuta->fornecedor_compra_id]);
                session([
                    'fornecedor_cpf_cnpj_idgener' => $minuta->fornecedor_empenho_cpfcnpjidgener_sessao
                ]);
                session(['situacao' => $minuta->situacao->descricao]);

                return $next($request);
            }

            session(['minuta_id' => '']);
            session(['empenho_etapa' => '']);
            session(['fornecedor_compra' => '']);
            session(['fornecedor_cpf_cnpj_idgener' => '']);
            session(['conta_id' => '']);
            session(['situacao' => '']);

            if ($this->rotas_minuta_original[Route::current()->action['as']] === 8) {
                return $next($request);
            }

            return redirect()->route('empenho.crud./minuta.index', ['ano' => $anoRota])->withError('Não permitido');
        }

        //SE A ROTA EXISTE NA LISTA DE ROTAS DA MINUTA DE ALTERAÇÃO
        if (array_key_exists(Route::current()->action['as'], $this->rotas_minuta_alteracao)) {

            session(['fluxo_minuta'     => 'minutaAlteracao']);
            session(['cabecalho_minuta' => 'cabecalho_alteracao']);

            $minuta_id = Route::current()->parameter('minuta_id');
            $minuta = MinutaEmpenho::find($minuta_id);

            //caso a minuta seja de outra unidade, redireciona
            if (session('user_ug_id') !== $minuta->unidade_id) {
                return redirect(URL::previous());
            }

            $remessa_id = Route::current()->parameter('remessa')
                ?? $minuta->max_remessa;
            $remessa_alteracao_id = Route::current()->parameter('remessa');
            $remessa = MinutaEmpenhoRemessa::find($remessa_id);

            //CASO SEJA ALTERACAO DE FONTE
            if ($remessa->alteracao_fonte_minutaempenho_id !== null && $remessa->situacao_descricao !== 'EMPENHO EMITIDO') {
                return redirect(route('empenho.crud.alteracaoFonte.index', [
                    'minuta_id' => $minuta_id
                ]));
            }

            session(['remessa_id' => $remessa_alteracao_id]);
            session(['empenho_etapa' => '']);
            session(['conta_id' => '']);
            session(['fornecedor_compra' => '']);
            session(['fornecedor_cpf_cnpj_idgener' => '']);
            session(['situacao' => '']);
            session(['unidade_ajax_id' => '']);
            session(['etapa' => '']);
            session(['situacao_remessa' => '']);
            session(['passivo_anterior' => $minuta->passivo_anterior]);

            if (
                $remessa->situacao_descricao === "EMPENHO EMITIDO"
                && $this->rotas_minuta_alteracao[Route::current()->action['as']] !== 3
                && Route::current()->action['as'] != 'empenho.crud.alteracao.create'
            ) {
                session(['situacao' => $remessa->situacao->descricao]);
                session(['empenho_etapa' => 3]);
                session(['passivo_anterior' => $minuta->passivo_anterior]);

                return redirect(route('empenho.crud.alteracao.show', [
                    'minuta_id' => $minuta_id,
                    'remessa' => $remessa_id,
                    'minuta' => $minuta_id
                ]));
            }


            if ($this->rotas_minuta_alteracao[Route::current()->action['as']] === 1) {
                if (($remessa->situacao->descricao !== 'ERRO'
                    && $remessa->situacao->descricao !== 'EM ANDAMENTO'
                    && $remessa->situacao->descricao !== 'EMPENHO EMITIDO'
                    && $remessa->remessa !== 0 )) {
                    return redirect(route('empenho.crud.alteracao.show', [
                        'minuta_id' => $minuta_id,
                        'remessa' => $remessa_id,
                        'minuta' => $minuta_id
                    ]));
                }
                session(['situacao' => 'EM ANDAMENTO']);
                session(['empenho_etapa' => 1]);
                session(['passivo_anterior' => $minuta->passivo_anterior]);

                if (strpos(Route::current()->action['as'], 'create') !== false) {
                    if ($remessa->remessa === 0) {
                        return $next($request);
                    }

                    if (($remessa->situacao->descricao === 'ERRO' || $remessa->situacao->descricao === 'EM ANDAMENTO')) {
                        return redirect(route('empenho.crud.alteracao.edit', [
                            'minuta_id' => $minuta_id,
                            'remessa' => $remessa->id,
                            'minuta' => $minuta_id
                        ]));
                    }
                }
            }
            if ($this->rotas_minuta_alteracao[Route::current()->action['as']] === 2) {

                if (($remessa->situacao->descricao !== 'ERRO' && $remessa->situacao->descricao !== 'EM ANDAMENTO')) {
                    return redirect(route('empenho.crud.alteracao.show', [
                        'minuta_id' => $minuta_id,
                        'remessa' => $remessa_id,
                        'minuta' => $minuta_id
                    ]));
                }

                session(['situacao' => 'EM ANDAMENTO']);
                session(['empenho_etapa' => 2]);
                session(['passivo_anterior' => $minuta->passivo_anterior]);

                //se for create
                if (strpos(Route::current()->action['as'], 'edit') === false) {
                    if (count($remessa->contacorrente()->get()) > 0) {
                        return redirect(route('empenho.crud.alteracao.passivo-anterior.edit', [
                            'minuta_id' => $minuta_id,
                            'remessa' => $remessa->id,
                        ]));
                    }
                    return $next($request);
                }
            }

            //caso a rota seja a 3
            session(['situacao' => $remessa->situacao->descricao]);
            session(['empenho_etapa' => 3]);
            session(['passivo_anterior' => $minuta->passivo_anterior]);
        }

        //SE A ROTA EXISTE NA LISTA DE ROTAS DA MINUTA DE ALTERAÇÃO DE FONTE
        if (array_key_exists(Route::current()->action['as'], $this->rotas_minuta_alteracao_fonte)) {
            session(['fluxo_minuta' => 'minutaAlteracaoFonte']);
            session(['cabecalho_minuta' => 'cabecalho_alt_fonte']);
            session(['remessa_id' => null]);
            session(['situacao' => '']);
            session(['empenho_etapa' => '']);
            session(['nova_minuta_id' => null]);

            $minuta_id = Route::current()->parameter('minuta_id');
            $minuta = MinutaEmpenho::find($minuta_id);

            //caso a minuta seja de outra unidade, redireciona
            if (session('user_ug_id') !== $minuta->unidade_id) {
                return redirect(URL::previous());
            }

            if ($minuta->empenho_por === 'Suprimento' || $minuta->empenho_por === 'Contrato'){
                Alert::warning('<i class="fa fa-warning text-white"></i> ' .
                    ' &nbsp;&nbsp;&nbsp;A alteração de fonte só é permitida para empenhos do tipo Compra. ' )->flash();

                return redirect(URL::previous());
            }

            if (!$minuta->exercicio_atual){
                Alert::warning('<i class="fa fa-warning text-white"></i> ' .
                    ' &nbsp;&nbsp;&nbsp;A alteração de fonte não é permitida para exercícios anteriores. ' )->flash();

                return redirect(URL::previous());
            }

            $remessa_id = Route::current()->parameter('remessa') ?? $minuta->max_remessa;
            $remessa = MinutaEmpenhoRemessa::find($remessa_id);
            session(['nova_minuta_id' => $remessa->alteracao_fonte_minutaempenho_id]);

            if ($remessa->remessa !== 0 && !$remessa->alteracao_fonte && $remessa->situacao_descricao != 'EMPENHO EMITIDO'){
                Alert::warning('<i class="fa fa-warning text-white"></i> ' .
                    ' &nbsp;&nbsp;&nbsp;A alteração de fonte não é permitida para minutas com outras alterações em andamento. ' )->flash();

                return redirect(route('empenho.crud.alteracao.index',['minuta_id' => $minuta_id]));
            }

            if ($this->rotas_minuta_alteracao_fonte[Route::current()->action['as']] === 1) {
                session(['empenho_etapa' => 1]);
                if (Route::current()->action['as'] === 'empenho.crud.alteracaoFonte.edit') {
                    session(['situacao' => $remessa->situacao->descricao]);
                }
                if (
                    $remessa->remessa !== 0
                    && $remessa->situacao_descricao !== 'EMPENHO EMITIDO'
                    && $remessa->alteracao_fonte_minutaempenho_id !== null
                    && Route::current()->action['as'] === 'empenho.crud.alteracaoFonte.index'
                ) {
                    return redirect(route('empenho.crud.alteracaoFonte.edit', [
                        'minuta_id' => $minuta_id,
                        'nova_minuta_id' => $remessa->alteracao_fonte_minutaempenho_id
                    ]));
                }
            }
            if ($this->rotas_minuta_alteracao_fonte[Route::current()->action['as']] === 2) {
                session(['situacao' => $remessa->situacao->descricao]);
                session(['empenho_etapa' => 2]);
                if (
                    $remessa->remessa !== 0
                    && $remessa->situacao_descricao !== 'EMPENHO EMITIDO'
                    && $remessa->alteracao_fonte_minutaempenho_id !== null
                    && count($remessa->itens) > 0
                    && Route::current()->action['as'] === 'empenho.crud.alteracaoFonte.subelemento'
                ) {
                    return redirect(route('empenho.crud.alteracaoFonte.subelemento.edit', [
                        'minuta_id' => $minuta_id,
                        'nova_minuta_id' => $remessa->alteracao_fonte_minutaempenho_id
                    ]));
                }

                if ($remessa->remessa !== 0
                    && Route::current()->action['as'] === 'empenho.crud.alteracaoFonte.subelemento.edit'
                    && count($remessa->itens) === 0) {

                    session(['remessa_id' => $remessa_id]);

                    return redirect(route('empenho.crud.alteracaoFonte.subelemento', [
                        'minuta_id' => $minuta_id,
                        'nova_minuta_id' => $remessa->alteracao_fonte_minutaempenho_id
                    ]));
                }

            }
            if ($this->rotas_minuta_alteracao_fonte[Route::current()->action['as']] === 3) {
                session(['situacao' => $remessa->situacao->descricao]);
                session(['empenho_etapa' => 3]);
            }

            if ($remessa->remessa !== 0 && Route::current()->action['as'] !== 'empenho.crud.alteracaoFonte.subelemento') {
                session(['remessa_id' => $remessa_id]);
            }

            if (
                ($remessa->alteracao_fonte_minutaempenho_id !== null)
                && ($remessa->situacao_descricao === 'EM ANDAMENTO' || $remessa->situacao_descricao === 'Erro')
                && (Route::current()->action['as'] === 'empenho.crud.alteracaoFonte.index')
            ) {
                return redirect(route('empenho.crud.alteracaoFonte.edit', [
                    'minuta_id' => $minuta_id,
                    'nova_minuta_id' => $remessa->alteracao_fonte_minutaempenho_id
                ]));
            }

        }

        return $next($request);
    }
}
