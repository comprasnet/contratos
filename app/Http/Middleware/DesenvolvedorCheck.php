<?php

namespace App\Http\Middleware;

use Closure;

class DesenvolvedorCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!backpack_user()->hasRole('Administrador') && !backpack_user()->hasRole('Desenvolvedor')) {
            abort('403', config('app.erro_permissao'));
        }
        return $next($request);
    }
}
