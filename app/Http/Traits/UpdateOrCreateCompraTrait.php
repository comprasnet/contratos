<?php

namespace App\Http\Traits;

use App\Models\Compra;
use App\Models\Unidade;
use App\services\AmparoLegalService;
use Illuminate\Support\Facades\DB;

trait UpdateOrCreateCompraTrait
{
    use ConsultaCompra;
    use BuscaCodigoItens;
    use CompraTrait;

    private function criarOuAtualizarCompra(
        $modalidade,
        $uasgCompra,
        $uasgUser,
        $numero_ano,
        $uasgBeneficiaria = null,
        array $suplementares = []
    ) {
        DB::beginTransaction();
        try {
            $consultaCompra = $this->consultaCompraSiasg($modalidade, $uasgCompra, $uasgUser, $uasgBeneficiaria, $numero_ano);

            // coloca a barra no numero_ano caso não houver
            if (strpos($numero_ano, "/") === false) {
                $ultimos_quatro = substr($numero_ano, -4);
                $numero_ano = str_replace($ultimos_quatro, "/" . $ultimos_quatro, $numero_ano);
            }

            $compraSispp = $consultaCompra->data->compraSispp;

            $unidade_subrogada = $compraSispp->subrogada;
            $unidade_subrrogada_id = null;
            if ($unidade_subrogada <> '000000') {
                $unidade_subrrogada_id = (int)$this->buscaIdUnidade($unidade_subrogada);
            }

            $unidade_origem_id = $suplementares['unidade_origem_id'] ?? $this->buscaIdUnidade($compraSispp->uasg);
            $modalidade_id     = $suplementares['modalidade_id']
                ?? $this->retornaIdCodigoItemPorDescres($modalidade, 'Modalidade Licitação');

            $params = [
                'unidade_origem_id'     => $unidade_origem_id,
                'modalidade_id'         => $modalidade_id,
                'numero_ano'            => $numero_ano,
                'tipo_compra_id'        =>
                    $this->retornaIdCodigoItemPorDescres('0'.$compraSispp->tipoCompra, 'Tipo Compra'),
                'unidade_subrrogada_id' => $unidade_subrrogada_id,
                'inciso'                => $compraSispp->inciso,
                'lei'                   => $compraSispp->lei,
                'artigo'                => $compraSispp->artigo ?? null,
                'id_unico'              => $compraSispp->idUnico ?? null,
                'cnpjOrgao'             => $compraSispp->cnpjOrgao ?? null
            ];
            $compra = $this->updateOrCreateCompra($params);

            //COMPRA SISPP
            if ($compraSispp->tipoCompra == 1) {
                $this->gravaParametroItensdaCompraSISPP($consultaCompra, $compra, $unidade_origem_id);
                DB::commit();
                return $compra;
            }

            //caso não seja da lei 14133
            $compraPertenceLei14133Derivadas =
                resolve(AmparoLegalService::class)->existeAmparoLegalEmLeiCompra($compraSispp->lei);

            if (!$compraPertenceLei14133Derivadas) {
                $this->gravaParametroItensdaCompraSISRP($consultaCompra, $compra);
            }

            DB::commit();
            return $compra;
        } catch (Exception $exc) {
            DB::rollback();
        }
    }

    public function updateOrCreateCompra($params)
    {
        $compraPertenceLei14133Derivadas =
            resolve(AmparoLegalService::class)->existeAmparoLegalEmLeiCompra($params['lei']);

        if ($compraPertenceLei14133Derivadas) {
            $sisrpId = $this->retornaIdCodigoItem('Tipo Compra', 'SISRP');

            //Não atualiza compra caso SISRP e LEI14133. A compra é atualizada no gestão de atas
            if ($params['tipo_compra_id'] == $sisrpId) {
                return Compra::where('unidade_origem_id', (int)$params['unidade_origem_id'])
                    ->where('modalidade_id', (int)$params['modalidade_id'])
                    ->where('numero_ano', $params['numero_ano'])
                    ->where('tipo_compra_id', $params['tipo_compra_id'])
                    ->first();
            }
        }
        return Compra::updateOrCreate(
            [
                'unidade_origem_id' => $params['unidade_origem_id'],
                'modalidade_id'     => (int)$params['modalidade_id'],
                'numero_ano'        => $params['numero_ano'],
                'tipo_compra_id'    => $params['tipo_compra_id']
            ],
            [
                'unidade_subrrogada_id' => $params['unidade_subrrogada_id'],
                'tipo_compra_id'        => $params['tipo_compra_id'],
                'inciso'                => $params['inciso'],
                'lei'                   => $params['lei'],
                'artigo'                => $params['artigo'],
                'id_unico'              => $params['id_unico'],
                'cnpjOrgao'             => $params['cnpjOrgao']
            ]
        );
    }
}
