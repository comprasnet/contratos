<?php

namespace App\Http\Traits;

use App\Models\Contrato;
use App\Models\Contratounidadedescentralizada;
use App\Models\Empenhos;
use App\Models\Justificativafatura;
use App\Models\Unidade;
use App\User;
use Backpack\CRUD\CrudPanel;
use Illuminate\Support\Facades\DB;

trait ContratoFaturaTrait
{

/**
     * Adiciona o filtro ao campo Tipo de Lista
     *
     * @author Juliano Pires <julianopiresdossantos@gmail.com>
     */
    private function adicionaFiltroLei(CrudPanel $crud)
    {
        $campo = [
            'name' => 'ato_normativo',
            'type' => 'select2',
            'label' => 'Lei'
        ];

        $leis =[
            'LEI_14133' => 'Lei 14.133/2021',
            'COMPLEMENTO_14133' => 'Complemento 14.133/2021',
            'Outras' => 'Outras Leis',
            'SEM_AMPARO' => 'Não informado'
        ];

        $crud->addFilter(
            $campo,
            $leis,
            function ($value) use ($crud) {
                if (in_array($value, ['LEI_14133', 'COMPLEMENTO_14133'])) {
                    $crud->query->where('amparo_legal.complemento_14133', true);
                    return;
                }

                if ($value === 'SEM_AMPARO') {
                    $crud->query->whereNull('amparo_legal.ato_normativo');
                    return;
                }

                $crud->query->where('amparo_legal.complemento_14133', false);
            }
        );
    }

    private function ordenacaoPadrao(CrudPanel $crud ) {
        $crud->addClause('orderby', 'contratofaturas.vencimento', 'ASC');
        $crud->addClause('orderby', 'tipolistafatura.order', 'ASC');
        $crud->addClause('orderby', 'contratofaturas.created_at', 'ASC');
    }

    private function verificaSituacao($situacao, $situacoes){
        if(!array_key_exists(strtoupper($situacao), $situacoes)){
             return false;
        }
        return true;
    }

    private function verificaJustificativa($justificativa, $amparos){
        if($amparos != null){
            if (!array_key_exists($justificativa, $amparos)){
                return false;
            }
        }
        $justificativaFatura = Justificativafatura::where('situacao', true)->where('id', $justificativa)->first();

        return $justificativaFatura;

    }

    private function verificaEmpenhos($empenhos, $user, $fornecedor_id){

        //Retorna um array com todos os ids das Unidades (primária e secudárias) do usuário.
        $unidades_id = $user->getAllUnidadesUser()->pluck('id')->toArray();

        //Retirado filtro por fornecedor (#issue 853)
        $empenhosAll = Empenhos::whereIn('unidade_id', $unidades_id)->get()->toArray();

        if(is_array($empenhos)){
            foreach ($empenhos as $id) {
                if (!$this->verificaEmpenhosHelper($id, $empenhosAll)) {
                    return false;
                }
            }
        }else{
            if(!$this->verificaEmpenhosHelper($empenhos, $empenhosAll)){
                return false;
            }
        }

       return true;
    }

    private function verificaEmpenhosHelper($id, $empenhosAll){
        foreach ($empenhosAll as $item) {
            if ($item['id'] == $id) {
                return true;
            }
        }
        return false;
    }

    private function retornaJustificativas(bool $amparoLei14133 = false)
    {
        //verificando se existem amparos
        if ($amparoLei14133) {
            $dados = Justificativafatura::select(
                'id',
                DB::raw("CONCAT(nome, ' - ', LEFT(descricao, 80)) as descricao")
            )->where('nome', '=', 'Ordem Lista')
                ->orWhere('nome', 'ilike', '%Art. 9º%');

            return $dados->pluck('descricao', 'id')->toArray();
        }

        //caso não tenha amparo da lei LEI 14.133/2021
        $dados = Justificativafatura::select(
            'id',
            DB::raw("CONCAT(nome, ' - ', LEFT(descricao, 80)) as descricao")
        )->where('nome', '=', 'Ordem Lista')
            ->orWhere('nome', '=', 'Pendências Cadastrais')
            ->orWhere('nome', 'ilike', '%Art. 5º%');

        return $dados->pluck('descricao', 'id')->toArray();
    }

    private function verificaUnidadeContratoFaturaAPI($contratoFatura, $user){

        $contrato_unidade = Contrato::where('id', $contratoFatura->contrato_id)->first();
        $users_unidades_array = [];

        // Verifica se o usuário está em alguma unidade que esteja no contrato
        $unidades = $user->getAllUnidadesUser();

        foreach ($unidades as $user_unidade){
            $users_unidades_array[] = $user_unidade->id;
            if($user_unidade->id == $contrato_unidade->unidade_id){
               return true;
            }
        }

        $unidadesDescentralizadas = Contratounidadedescentralizada::where(
                'contrato_id',
                $contratoFatura->contrato_id
            )
            ->get()->toArray();

        // Verifica se o usuario está em alguma unidade DESCENTRALIZADA do contrato
        if($unidadesDescentralizadas){
            foreach($unidadesDescentralizadas as $unidadesDescentralizada){
               if(in_array($unidadesDescentralizada['unidade_id'], $users_unidades_array)){
                   return true;
               }
            }
        }

        return false;
    }

    private function verificaVinculoEmpenhosFatura($empenhos, $contratoFatura){

        //Array com todos os ids dos empenhos vinculados a fatura.
        $array_empenhos_fatura = $contratoFatura->empenhos()->pluck('empenhos.id')->toArray();

        //Array com os ids dos empenhos informados no endpoint.
        $array_empenhos_endpoint = is_array($empenhos) ? $empenhos : explode(',', $empenhos);

        //Compara se todos os empenhos informados no endpoint constam como empenhos vinculados a fatura.
        if (!empty(array_diff($array_empenhos_endpoint, $array_empenhos_fatura))) {
            return false;
        }

       return true;
    }

}
