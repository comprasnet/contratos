<?php

namespace App\Http\Traits;

use App\Models\Catmatpdms as ModelsCatmatpdms;
use App\Models\Unidade;
use Illuminate\Support\Facades\DB;

trait CatmatpdmsTrait
{
    public function retornaEmpenhosPorPDM($pdm, $ug, $ano){
        $arrayPdmEmpenho = [];
        $valor = 0;

        $unidade = Unidade::where('codigo', $ug)->first();

        if (empty($unidade))
            return false;

        $pdmEmpenho = ModelsCatmatpdms::select([
            "catmatpdms.id",
            \DB::raw("EXTRACT('Year' FROM minutaempenhos.data_emissao) as ano_minuta_emissao"),
            "catmatpdms.codigo",
            "catmatpdms.descricao",
            "codigoitens.descricao AS tipo_item",
            \DB::raw("LTRIM(TO_CHAR(SUM(compra_item_minuta_empenho.valor), 'FM999G999G000D0099'), '0') AS valor")
        ])
        ->whereRaw("minutaempenhos.situacao_id = 217
                        AND (
                        (minutaempenhos_remessa.situacao_id = 215 AND minutaempenhos_remessa.remessa = 0)
                        OR  (minutaempenhos_remessa.remessa > 0 AND minutaempenhos_remessa.situacao_id = 217)
                    )")
            ->join("catmatseritens", "catmatseritens.catmatpdm_id", "=", "catmatpdms.id")
            ->join("compra_items", "compra_items.catmatseritem_id", "=", "catmatseritens.id")
            ->join("codigoitens", "compra_items.tipo_item_id", "=", "codigoitens.id")
            ->join("compra_item_minuta_empenho", "compra_item_minuta_empenho.compra_item_id", "=", "compra_items.id")
            ->join("minutaempenhos", "minutaempenhos.id", "=", "compra_item_minuta_empenho.minutaempenho_id")
            ->join("saldo_contabil", "saldo_contabil.id", "=", "minutaempenhos.saldo_contabil_id")
            ->join("unidades", "unidades.id", "=", "saldo_contabil.unidade_id")
            ->join("minutaempenhos_remessa", "compra_item_minuta_empenho.minutaempenhos_remessa_id", "=", "minutaempenhos_remessa.id")
            ->where('minutaempenhos.unidade_id', '=', $unidade->id)
            ->where('catmatpdms.codigo', '=', $pdm)
            ->where('minutaempenhos.mensagem_siafi', 'like', "%$ano%")
            ->groupBy("catmatpdms.id", "catmatpdms.codigo", \DB::raw("EXTRACT('Year' FROM minutaempenhos.data_emissao)"), "codigoitens.descricao", "catmatpdms.descricao")
            ->havingRaw("SUM(compra_item_minuta_empenho.valor) != 0")
            ->orderByDesc(\DB::raw("EXTRACT('Year' FROM minutaempenhos.data_emissao)"));

        $pdmEmpenhoUnion = ModelsCatmatpdms::select([
            "catmatpdms.id",
            \DB::raw("EXTRACT('Year' FROM minutaempenhos.data_emissao) as ano_minuta_emissao"),
            "catmatpdms.codigo",
            "catmatpdms.descricao",
            "codigoitens.descricao AS tipo_item",
            \DB::raw("LTRIM(TO_CHAR(SUM(contrato_item_minuta_empenho.valor), 'FM999G999G000D0099'), '0') AS valor")
        ])
            ->join('catmatseritens', 'catmatseritens.catmatpdm_id', '=', 'catmatpdms.id')
            ->join('contratoitens', 'contratoitens.catmatseritem_id', '=', 'catmatseritens.id')
            ->join('contrato_item_minuta_empenho', 'contrato_item_minuta_empenho.contrato_item_id', '=', 'contratoitens.id')
            ->join('codigoitens', 'codigoitens.id', '=', 'contrato_item_minuta_empenho.operacao_id')
            ->join('minutaempenhos', 'minutaempenhos.id', '=', 'contrato_item_minuta_empenho.minutaempenho_id')
            ->join('saldo_contabil', 'saldo_contabil.id', '=', 'minutaempenhos.saldo_contabil_id')
            ->join('unidades', 'unidades.id', '=', 'saldo_contabil.unidade_id')
            ->join('minutaempenhos_remessa', 'contrato_item_minuta_empenho.minutaempenhos_remessa_id', '=', 'minutaempenhos_remessa.id')
            ->where('minutaempenhos.unidade_id', $unidade->id)
            ->where('catmatpdms.codigo', $pdm)
            ->where('minutaempenhos.mensagem_siafi', 'like', "%$ano%")
            ->whereRaw("minutaempenhos.situacao_id = 217
                                AND (
                                (minutaempenhos_remessa.situacao_id = 215 AND minutaempenhos_remessa.remessa = 0)
                                OR  (minutaempenhos_remessa.remessa > 0 AND minutaempenhos_remessa.situacao_id = 217)
                            )")
            ->groupBy(
                "catmatpdms.id",
                "ano_minuta_emissao",
                "catmatpdms.codigo",
                "catmatpdms.descricao",
                "codigoitens.descricao"
            )
            ->havingRaw("SUM(contrato_item_minuta_empenho.valor) != 0")
            ->orderByDesc(\DB::raw("EXTRACT('Year' FROM minutaempenhos.data_emissao)"));;

        $union = $pdmEmpenho->union($pdmEmpenhoUnion)->get()->first();

        if (empty($union))
            return false;

        $pdmEmpenhoItens = $this->getPDMItensAPI($union->id, $ano, $unidade->id);

        // recuperando o valor empenhado dos itens e convertendo para o formato americano
        // para poder fazer a soma e depois convertendo pro formato braisleiro novamente
        foreach ($pdmEmpenhoItens as $pdmEmpenhoItem) {
            $valor_empenhado_americano = str_replace('.', '', $pdmEmpenhoItem['valor_empenhado']);
            $valor_empenhado = str_replace(',', '.', $valor_empenhado_americano);
            $valor += (float)$valor_empenhado;
        }

        $valor_formatado = number_format($valor, 2, ',', '.');


        $arrayPdmEmpenho = [
            'ano' => $union->ano_minuta_emissao,
            'codigo_pdm' => $union->codigo,
            'valor' => "$valor_formatado", // transformando em string pois a API já trabalhava com este campo assim
            'empenhos' => $pdmEmpenhoItens
        ];

        return $arrayPdmEmpenho;
    }

    public function getPDMItens($id, $ano = null){
        $unidadeLogada = session()->get('user_ug_id');

        // Ano aqui deve vir direto da rota concatenado com o id
        if($ano == null){
            $idAno = explode("-", $id);
            $id = $idAno[0];
            $ano = $idAno[1];
        }

        $sql = ModelsCatmatpdms::select([
                'compra_items.descricaodetalhada',
                \DB::raw("EXTRACT('Year' FROM minutaempenhos.data_emissao) as ano_minuta_emissao"),
                'catmatseritens.codigo_siasg',
                'minutaempenhos.mensagem_siafi',
                'unidades.nomeresumido as unidadenome',
                'unidades.codigo',
                \DB::raw("CASE
                  WHEN SUM(compra_item_minuta_empenho.valor) >= 1
                  THEN LTRIM(TO_CHAR(SUM(compra_item_minuta_empenho.valor), 'FM999G999G000D0099'), '0')
                  ELSE TO_CHAR(SUM(compra_item_minuta_empenho.valor), 'FM999G999G0D0099')
                  END AS valor"),
                DB::raw("'Compra' as tipo_item")
                ])
            ->distinct()
            ->join('catmatseritens', 'catmatseritens.catmatpdm_id', '=', 'catmatpdms.id')
            ->join('compra_items', 'compra_items.catmatseritem_id', '=', 'catmatseritens.id')
            ->join('compra_item_minuta_empenho', 'compra_item_minuta_empenho.compra_item_id', '=', 'compra_items.id')
            ->join('minutaempenhos', 'minutaempenhos.id', '=', 'compra_item_minuta_empenho.minutaempenho_id')
            ->join('saldo_contabil', 'saldo_contabil.id', '=', 'minutaempenhos.saldo_contabil_id')
            ->join('unidades', 'unidades.id', '=', 'saldo_contabil.unidade_id')
            ->join('minutaempenhos_remessa', 'compra_item_minuta_empenho.minutaempenhos_remessa_id', '=', 'minutaempenhos_remessa.id')
            ->where('catmatpdms.id', $id)
            ->where('minutaempenhos.unidade_id', $unidadeLogada)
            ->where('minutaempenhos.mensagem_siafi', 'like', "%$ano%")
            ->whereRaw("minutaempenhos.situacao_id = 217
                        AND (
                        (minutaempenhos_remessa.situacao_id = 215 AND minutaempenhos_remessa.remessa = 0)
                        OR  (minutaempenhos_remessa.remessa > 0 AND minutaempenhos_remessa.situacao_id = 217)
                    )")
            ->groupBy('compra_items.descricaodetalhada', \DB::raw("EXTRACT('Year' FROM minutaempenhos.data_emissao)"),'catmatseritens.codigo_siasg', 'minutaempenhos.mensagem_siafi', 'unidades.nomeresumido', 'unidades.codigo')
            ->havingRaw("SUM(compra_item_minuta_empenho.valor) != 0");

        $sqlUnion = ModelsCatmatpdms::select([
            'contratoitens.descricao_complementar',
            \DB::raw("EXTRACT('Year' FROM minutaempenhos.data_emissao) as ano_minuta_emissao"),
            'catmatseritens.codigo_siasg',
            'minutaempenhos.mensagem_siafi',
            'unidades.nomeresumido as unidadenome',
            'unidades.codigo',
            \DB::raw("CASE
                          WHEN SUM(contrato_item_minuta_empenho.valor) >= 1
                          THEN LTRIM(TO_CHAR(SUM(contrato_item_minuta_empenho.valor), 'FM999G999G000D0099'), '0')
                          ELSE TO_CHAR(SUM(contrato_item_minuta_empenho.valor), 'FM999G999G0D0099')
                          END AS valor"),
            DB::raw("'Contrato' as tipo_item")
        ])
            ->distinct()
            ->join('catmatseritens', 'catmatseritens.catmatpdm_id', '=', 'catmatpdms.id')
            ->join('contratoitens', 'contratoitens.catmatseritem_id', '=', 'catmatseritens.id')
            ->join('contrato_item_minuta_empenho', 'contrato_item_minuta_empenho.contrato_item_id', '=', 'contratoitens.id')
            ->join('minutaempenhos', 'minutaempenhos.id', '=', 'contrato_item_minuta_empenho.minutaempenho_id')
            ->join('saldo_contabil', 'saldo_contabil.id', '=', 'minutaempenhos.saldo_contabil_id')
            ->join('unidades', 'unidades.id', '=', 'saldo_contabil.unidade_id')
            ->join('minutaempenhos_remessa', 'contrato_item_minuta_empenho.minutaempenhos_remessa_id', '=', 'minutaempenhos_remessa.id')
            ->where('catmatpdms.id', $id)
            ->where('minutaempenhos.unidade_id', $unidadeLogada)
            ->where('minutaempenhos.mensagem_siafi', 'like', "%$ano%")
            ->whereRaw("minutaempenhos.situacao_id = 217
                                AND (
                                (minutaempenhos_remessa.situacao_id = 215 AND minutaempenhos_remessa.remessa = 0)
                                OR  (minutaempenhos_remessa.remessa > 0 AND minutaempenhos_remessa.situacao_id = 217)
                            )")
            ->groupBy('contratoitens.descricao_complementar', \DB::raw("EXTRACT('Year' FROM minutaempenhos.data_emissao)"),'catmatseritens.codigo_siasg', 'minutaempenhos.mensagem_siafi', 'unidades.nomeresumido', 'unidades.codigo')
            ->havingRaw("SUM(contrato_item_minuta_empenho.valor) != 0");

        $union = $sql->union($sqlUnion)->get()->toArray();

        return $union;
    }

    private function getPDMItensAPI($id_pdm, $ano, $unidade_id){

        $pdmEmpenho = ModelsCatmatpdms::select([
            'uc.codigo                       as unidade_compra',
            'compras.numero_ano              as numero_ano_compra',
            'catmatseritens.descricao        as descricao_item',
            'catmatseritens.codigo_siasg     as codigo_item',
            'unidades.codigo                 as unidade_minuta',
            'codigoitens.descricao           as modalidade_compra',
            'minutaempenhos.mensagem_siafi   as numero_empenho',
            'usc.codigosiafi                 as unidade_emitente_empenho',
            'amparo_legal_view.amparo_legal',
            \DB::raw("LTRIM(TO_CHAR(SUM(compra_item_minuta_empenho.valor), 'FM999G999G000D0099'), '0') AS valor_empenhado")
        ])
            ->distinct()
            ->join('catmatseritens', 'catmatseritens.catmatpdm_id', '=', 'catmatpdms.id')
            ->join('compra_items', 'compra_items.catmatseritem_id', '=', 'catmatseritens.id')
            ->join('compra_item_minuta_empenho', 'compra_item_minuta_empenho.compra_item_id', '=', 'compra_items.id')
            ->join('minutaempenhos', 'minutaempenhos.id', '=', 'compra_item_minuta_empenho.minutaempenho_id')
            ->join('minutaempenhos_remessa', 'compra_item_minuta_empenho.minutaempenhos_remessa_id', '=', 'minutaempenhos_remessa.id')
            ->join('compras', 'compras.id', '=', 'minutaempenhos.compra_id')
            ->join('saldo_contabil', 'saldo_contabil.id', '=', 'minutaempenhos.saldo_contabil_id')
            ->join('unidades', 'unidades.id', '=', 'minutaempenhos.unidade_id')
            ->join('unidades as usc', 'usc.id', '=', 'saldo_contabil.unidade_id')
            ->join('unidades as uc', 'uc.id', '=', 'compras.unidade_origem_id')
            ->join('codigoitens', 'codigoitens.id', '=', 'compras.modalidade_id')
            ->join('amparo_legal_view', 'amparo_legal_view.id', '=', 'minutaempenhos.amparo_legal_id')
            ->where('catmatpdms.id', $id_pdm)
            ->where('minutaempenhos.unidade_id', $unidade_id)
            ->where('minutaempenhos.mensagem_siafi', 'like', "%$ano%")
            ->whereRaw("minutaempenhos.situacao_id = 217
                        AND (
                        (minutaempenhos_remessa.situacao_id = 215 AND minutaempenhos_remessa.remessa = 0)
                        OR  (minutaempenhos_remessa.remessa > 0 AND minutaempenhos_remessa.situacao_id = 217)
                    )")
            ->groupBy('compra_items.descricaodetalhada', \DB::raw("EXTRACT('Year' FROM minutaempenhos.data_emissao)"),'catmatseritens.codigo_siasg', 'minutaempenhos.mensagem_siafi', 'unidades.nomeresumido', 'unidades.codigo')
            ->havingRaw("SUM(compra_item_minuta_empenho.valor) != 0")
            ->groupBy(
                'uc.codigo',
                'compras.numero_ano',
                'codigoitens.descricao',
                'catmatseritens.descricao',
                'catmatseritens.codigo_siasg',
                'unidades.codigo',
                'minutaempenhos.mensagem_siafi',
                'usc.codigosiafi',
                'amparo_legal_view.amparo_legal'
            );

        $pdmEmpenhoUnion = ModelsCatmatpdms::select([
            'uc.codigo                       as unidade_compra',
            'contratos.numero                as numero_ano_compra',
            'catmatseritens.descricao        as descricao_item',
            'catmatseritens.codigo_siasg     as codigo_item',
            'unidades.codigo                 as unidade_minuta',
            'codigoitens.descricao           as modalidade_compra',
            'minutaempenhos.mensagem_siafi   as numero_empenho',
            'usc.codigosiafi                 as unidade_emitente_empenho',
            'amparo_legal_view.amparo_legal',
            \DB::raw("LTRIM(TO_CHAR(SUM(contrato_item_minuta_empenho.valor), 'FM999G999G000D0099'), '0') AS valor_empenhado")
        ])
            ->distinct()
            ->join('catmatseritens', 'catmatseritens.catmatpdm_id', '=', 'catmatpdms.id')
            ->join('contratoitens', 'contratoitens.catmatseritem_id', '=', 'catmatseritens.id')
            ->join('contrato_item_minuta_empenho', 'contrato_item_minuta_empenho.contrato_item_id', '=', 'contratoitens.id')
            ->join('minutaempenhos', 'minutaempenhos.id', '=', 'contrato_item_minuta_empenho.minutaempenho_id')
            ->join('saldo_contabil', 'saldo_contabil.id', '=', 'minutaempenhos.saldo_contabil_id')
            ->join('unidades as usc', 'usc.id', '=', 'saldo_contabil.unidade_id')
            ->join('minutaempenhos_remessa', 'contrato_item_minuta_empenho.minutaempenhos_remessa_id', '=', 'minutaempenhos_remessa.id')
            ->join('contratos', 'contratos.id', '=', 'minutaempenhos.contrato_id')
            ->join('unidades', 'unidades.id', '=', 'minutaempenhos.unidade_id')
            ->join('unidades as uc', 'uc.id', '=', 'contratos.unidade_id')
            ->join('codigoitens', 'codigoitens.id', '=', 'contratos.tipo_id')
            ->join('amparo_legal_view', 'amparo_legal_view.id', '=', 'minutaempenhos.amparo_legal_id')
            ->where('catmatpdms.id', $id_pdm)
            ->where('minutaempenhos.unidade_id', $unidade_id)
            ->where('minutaempenhos.mensagem_siafi', 'like', "%$ano%")
            ->whereRaw("minutaempenhos.situacao_id = 217
                                AND (
                                (minutaempenhos_remessa.situacao_id = 215 AND minutaempenhos_remessa.remessa = 0)
                                OR  (minutaempenhos_remessa.remessa > 0 AND minutaempenhos_remessa.situacao_id = 217)
                            )")
            ->groupBy(
                'uc.codigo',
                'contratos.numero',
                'codigoitens.descricao',
                'catmatseritens.descricao',
                'catmatseritens.codigo_siasg',
                'unidades.codigo',
                'minutaempenhos.mensagem_siafi',
                'usc.codigosiafi',
                'amparo_legal_view.amparo_legal'
            )
            ->havingRaw("SUM(contrato_item_minuta_empenho.valor) != 0");

        $sql = $pdmEmpenho->union($pdmEmpenhoUnion)->get()->toArray();
        return $sql;
    }
}
