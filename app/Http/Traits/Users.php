<?php

namespace App\Http\Traits;

use App\Models\BackpackUser;
use App\Models\Unidade;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

trait Users
{
    public function buscaUg()
    {
        $ug = [];

        $ugprimaria = Unidade::select(DB::raw("CONCAT(codigosiafi,' - ',nomeresumido) AS nome"), 'id')//429
            ->where('id', '=', backpack_user()->ugprimaria)
            ->where('tipo', '=', 'E')
            ->pluck('nome', 'id')
            ->toArray();

        $ugsecundaria = Unidade::select(DB::raw("CONCAT(codigosiafi,' - ',nomeresumido) AS nome"), 'id') //429
            ->whereHas('users', function ($query) {
                $query->where('user_id', '=', backpack_user()->id);
            })
            ->where('tipo', '=', 'E')
            ->pluck('nome', 'id')
            ->toArray();

        $ug = $ugprimaria + $ugsecundaria;

        asort($ug);
        return $ug;
    }

    public function geraSenhaAleatoria()
    {
        $chars = '0123456789';
        $max = strlen($chars) - 1;
        $senha = "NOVA";
        for ($i = 0; $i < 4; $i++) {
            $senha .= $chars{mt_rand(0, $max)};
        }

        return $senha;
    }

    public function verificarUsuarioTemApenasPerfilFornecedor(): bool
    {
        $user = backpack_user();

        if ($user->roles->count() == 1 && $user->roles->first()->name == 'Fornecedor') {
            return true;
        }

        return false;
    }

    public function verificarUsuarioTemApenasPerfilFornecedorEmEsqueciSenha(BackpackUser $user): bool
    {
        if ($user->roles->count() == 1 && $user->roles->first()->name == 'Fornecedor') {
            return true;
        }

        return false;
    }
    
    /**
     * Verificar se a unidade do empenho é igual a unidade primaria/secundaria do usuario
     * @param $unidadeDoEmpenho
     * @return bool
     */
    public function usuarioTemPermissaoParaEmpenho($unidadeDoEmpenho): bool
    {
        $unidade = Unidade::where('codigo', $unidadeDoEmpenho)->first();
        $unidadesDoUsuario = backpack_user()->getAllUnidadesUser();
        
        foreach ($unidadesDoUsuario as $user_unidade){
            if($user_unidade->id == $unidade->id){
                return true;
            }
        }
        return false;
    }

    /**
     * Verifica se usuario é admin pela helper backpack_user
     * @return mixed
     */
    public function userIsAdmin()
    {
       return backpack_user()->hasRole('Administrador');
    }
}
