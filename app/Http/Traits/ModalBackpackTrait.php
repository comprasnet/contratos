<?php

namespace App\Http\Traits;

/*
 * Trait criada com o propósito de adicionar campos padrao do backpack a qualquer modal que seja necessário
 * métodos criados com base no backpack
 *
 * */
trait ModalBackpackTrait
{
    public $modal_fields = [];


    /**
     * Add a field to the create/update form or both in Modal.
     * método criado com base no método addField do backpack:
     * \Backpack\CRUD\PanelTraits\Fields::addField
     *
     * Exemplo de uso:
     * \App\Http\Traits\ModalBackpackTrait::addModalField
     *
     * Lembre-se de adicionar o trait no crudController que deseja utilizar,
     * adicionar o método addModalField no crudController
     * e também adicionar $this->crud->modal_fields = $this->modal_fields; no método setup
     */
    public function addModalField($field, $form = 'both')
    {
        // if the field_definition_array array is a string, it means the programmer was lazy and has only passed the name
        // set some default values, so the field will still work
        if (is_string($field)) {
            $completeFieldsArray['name'] = $field;
        } else {
            $completeFieldsArray = $field;
        }

        // if this is a relation type field and no corresponding model was specified, get it from the relation method
        // defined in the main model
        if (isset($completeFieldsArray['entity']) && ! isset($completeFieldsArray['model'])) {
            $completeFieldsArray['model'] = $this->getRelationModel($completeFieldsArray['entity']);
        }

        // if the label is missing, we should set it
        if (! isset($completeFieldsArray['label'])) {
            $completeFieldsArray['label'] = mb_ucfirst(str_replace('_', ' ', $completeFieldsArray['name']));
        }

        // if the field type is missing, we should set it
        if (! isset($completeFieldsArray['type'])) {
            $completeFieldsArray['type'] = $this->getFieldTypeFromDbColumnType($completeFieldsArray['name']);
        }

        $this->transformModalFields($form, function ($fields) use ($completeFieldsArray) {
            $fields[$completeFieldsArray['name']] = $completeFieldsArray;

            return $fields;
        });

        return $this;
    }

    /**
     * Apply the given callback to the form fields.
     *
     * @param string   $form     The CRUD form. Can be 'create', 'update' or 'both'.
     * @param callable $callback The callback function to run for the given form fields.
     */
    private function transformModalFields($form, callable $callback): void
    {
        switch (strtolower($form)) {
            /*case 'create':
                $this->create_fields = $callback($this->create_fields);
                break;

            case 'update':
                $this->update_fields = $callback($this->update_fields);
                break;*/

            default:
                $this->modal_fields = $callback($this->modal_fields);
                $this->modal_fields = $callback($this->modal_fields);
                break;
        }
    }

}
