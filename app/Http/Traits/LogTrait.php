<?php

namespace App\Http\Traits;

use Exception;
use Illuminate\Support\Facades\Log;

trait LogTrait
{
    public function inserirLogCustomizado(string $channel, string $acao, $e, string $linhaOpcional = null)
    {
        if (!empty($linhaOpcional)) {
            $e = $linhaOpcional . "\n " . $e->getMessage() . "\n " . $e->getTraceAsString();
        }

        switch ($acao) {
            case 'info':
                Log::channel($channel)->info($e);
                break;
            case 'error':
                Log::channel($channel)->error($e);
                break;
        }
    }

    public function inserirLogDebug(string $channel, string $acao, string $mensagem)
    {
        switch ($acao) {
            case 'info':
                Log::channel($channel)->info($mensagem);
                break;
            case 'error':
                Log::channel($channel)->error($mensagem);
                break;
        }
    }

    /**
     * Padroniza o registro de log no canal 'pncp_debug'.
     *
     * Se receber uma exceção, inclui detalhes como mensagem, arquivo e linha onde ocorreu.
     * Se receber uma string, apenas inclui a mensagem.
     *
     * @param string|Exception $messageOrException Mensagem de erro ou objeto Exception.
     * @param string|null $method Nome do método onde o erro foi capturado (opcional).
     * @param int|null $line Número da linha onde o erro foi capturado (opcional).
     * @param string $logLevel Nível de log ('info', 'debug', 'error', etc.). Padrão: 'error'.
     *
     * Exemplo de saída:
     * - Com exceção: "ContratoCrud::store (22) -> PncpService.php (184): Erro ao enviar contrato"
     */
    public function inserirLogPncp($messageOrException, $method = null, $line = null, $logLevel = 'error')
    {
        $channel = 'pncp_debug';
        $logException = '';
        # Se $messageOrExeption for uma exceção, obtém a mensagem real e as informações de onde o erro ocorreu
        if ($messageOrException instanceof Exception) {
            $errorMessage = $messageOrException->getMessage();
            $logException = sprintf(' -> %s (%d)',
                basename($messageOrException->getFile()),
                $messageOrException->getLine());
        } elseif (is_string($messageOrException)) {
            $errorMessage = $messageOrException;
        } else {
            # Apenas por garantia, caso não seja passado nem uma exception nem uma string
            $errorMessage = 'Erro não identificado';
        }
        # Exibe o método e a linha onde ocorreu o erro
        $logReference = "{$method} ({$line})";
        # Se o erro for uma exceção, adiciona as informações de onde ocorreu
        $logReference .= $logException;

        // Valida o nível de log
        $logLevel = method_exists(Log::channel($channel), $logLevel) ? $logLevel : 'error';

        // Registra o log
        call_user_func([Log::channel($channel), $logLevel], $logReference . ': ' . $errorMessage);
    }


}
