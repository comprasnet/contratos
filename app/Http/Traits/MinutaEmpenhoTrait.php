<?php

namespace App\Http\Traits;

use App\Http\Controllers\Api\PNCP\ContratoControllerPNCP;
use App\Http\Controllers\Api\PNCP\DocumentoContratoController;
use App\Models\BackpackUser;
use App\Models\Codigoitem;
use App\Models\ArquivoGenerico;
use App\Models\CompraItemUnidade;
use App\Models\EnviaDadosPncp;
use App\Models\MinutaEmpenho;
use App\Models\MinutaEmpenhoRemessa;
use App\XML\Execsiafi;
use Exception;
use Illuminate\Support\Facades\Cache;

trait MinutaEmpenhoTrait
{
    use EnviaPncpTrait, ArquivoBase64ToPdf, Formatador;

    public function apagarMinuta(MinutaEmpenho $minuta): string
    {
        if ($minuta->empenho_por === 'Compra') {
            $cime = $minuta->compraItemMinutaEmpenho();
            $cime_deletar = $cime->get();
            $cime->delete();

            foreach ($cime_deletar as $item) {
                $compraItemUnidade = CompraItemUnidade::where('compra_item_unidade.compra_item_id', $item->compra_item_id)
                    ->where('compra_item_unidade.unidade_id', session('user_ug_id'))
                    ->where('compra_item_unidade.id', $item->compra_item_unidade_id)
                    ->join('compra_items', 'compra_items.id', '=', 'compra_item_unidade.compra_item_id')
                    ->join('compras', 'compras.id', '=', 'compra_items.compra_id')
                    ->join('codigoitens as tipo_compra', 'tipo_compra.id', '=', 'compras.tipo_compra_id')
                    ->select('compra_item_unidade.*')
                    ->first();

                $compraItemUnidade->quantidade_saldo =
                    $this->retornaSaldoAtualizado(
                        $item->compra_item_id,
                        null,
                        $compraItemUnidade->id
                    )->saldo;
                $compraItemUnidade->save();
            }
            $minuta->forceDelete();
            return 'Minuta Deletada com sucesso!';

        }

        // Deletar minuta do contrato
        $minuta->forceDelete();

        return 'Minuta Deletada com sucesso!';
    }

    public function apagarRemessaMinuta(MinutaEmpenho $minuta, MinutaEmpenhoRemessa $remessa): string
    {
        if ($minuta->empenho_por === 'Compra') {
            $cime = $remessa->retornaCompraItemMinutaEmpenho();
            $cime_deletar = $cime->get();
            $cime->delete();

            foreach ($cime_deletar as $item) {
                $compraItemUnidade = CompraItemUnidade::where('compra_item_unidade.compra_item_id', $item->compra_item_id)
                    ->where('compra_item_unidade.unidade_id', session('user_ug_id'))
                    ->whereRaw("
                            CASE WHEN compra_item_unidade.tipo_uasg = 'C'
                                    or tipo_compra.descricao = 'SISPP'
                                THEN compra_item_unidade.fornecedor_id = '" . $minuta->fornecedor_compra_id . "'
                                ELSE compra_item_unidade.fornecedor_id IS NULL
                            END")
                    ->join('compra_items', 'compra_items.id', '=', 'compra_item_unidade.compra_item_id')
                    ->join('compras', 'compras.id', '=', 'compra_items.compra_id')
                    ->join('codigoitens as tipo_compra', 'tipo_compra.id', '=', 'compras.tipo_compra_id')
                    ->select('compra_item_unidade.*')
                    ->first();
                $compraItemUnidade->quantidade_saldo =
                    $this->retornaSaldoAtualizado(
                        $item->compra_item_id,
                        null,
                        $compraItemUnidade->id
                    )->saldo;
                $compraItemUnidade->save();
            }

            $remessa->forceDelete();
            return 'Minuta Deletada com sucesso!';

        }

        // Deletar remessa
        $remessa->forceDelete();
        return 'Minuta Deletada com sucesso!';
    }

    private function deletarMinutaEmpenhoPNCP(EnviaDadosPncp $enviaPncp, ContratoControllerPNCP $contratoControllerPNCP)
    {
        if (empty($enviaPncp->sequencialPNCP)) {
            return;
        }
        $cnpjOrgao = $this->formatarCnpjOrgao($enviaPncp->sequencialPNCP);
        $ano = $this->formatarAno($enviaPncp->sequencialPNCP);
        $sequencialPNCP = $this->formatarSequencialPncp($enviaPncp->sequencialPNCP);
        $contratoExcluidoPNCP = $contratoControllerPNCP->excluirContrato($cnpjOrgao, $ano, $sequencialPNCP);
        $this->trataRetornoNovo($enviaPncp, $contratoExcluidoPNCP, $contratoControllerPNCP, "EXCLUIDO", true);
    }

    private function incluirMinutaEmpenhoPNCP(EnviaDadosPncp $enviaPncp, ContratoControllerPNCP $contratoControllerPNCP)
    {
        $empenho = MinutaEmpenho::find($enviaPncp->pncpable_id);
        # O empenho será enviado para o PNCP somente se existir o arquivo salvo
        if (file_exists($empenho->serializaArquivoPNCP())) {
            $situacao = Codigoitem::find($enviaPncp->situacao);

            $arraySituacaoNaoExistePncp = ['EXCLUIDO', 'INCPEN', 'ASNPEN'];

            //Verific situação é diferente de excluido, inclusão pendente e assinatura pendente
            if (!in_array($situacao->descres, $arraySituacaoNaoExistePncp)) {
                $enviaPncpAtualizado = $this->getEnviaDadosPncpAtualizado($enviaPncp->pncpable_id);

                $dcc = new DocumentoContratoController();
                $this->enviaEmpenhoArquivo($enviaPncpAtualizado, $dcc);
                return;
            }

            $dadosCompraEmpenho = $empenho->compra;

            $dadosCompra = $this->buscaCNPJContratanteNovo(
                $dadosCompraEmpenho->modalidade->descres,
                $dadosCompraEmpenho->numero_ano,
                $dadosCompraEmpenho->unidade_origem->codigo,
                $empenho->unidade_id()->first()->codigo
            );

            $cnpjOrgao = $empenho->saldo_contabil->unidade_id()->first()->orgao->cnpj;

            $empenhoEnviado = $contratoControllerPNCP->inserirContrato($empenho, $dadosCompra->cnpjOrgao, $dadosCompra->idUnico, $cnpjOrgao);

            $enviaPncpAtualizado = $this->getEnviaDadosPncpAtualizado($enviaPncp->pncpable_id);

            $sucessoTratamento = $this->trataRetornoNovo($enviaPncpAtualizado, $empenhoEnviado, $contratoControllerPNCP);
            if ($sucessoTratamento) {
                $dcc = new DocumentoContratoController();
                $enviaPncpAtualizado = $this->getEnviaDadosPncpAtualizado($enviaPncp->pncpable_id);
                $this->enviaEmpenhoArquivo($enviaPncpAtualizado, $dcc);
            }
        }
    }

    private function minutaEmpenhoPNCP(MinutaEmpenho $minutaEmpenho)
    {
        # O empenho será enviado para o PNCP somente se existir o arquivo salvo
        if (file_exists($minutaEmpenho->serializaArquivoPNCP())) {
            $contratoControllerPNCP = new ContratoControllerPNCP();
            $enviaPncp = $minutaEmpenho->envia_dados_pncp->first();

            # Quando o empenho na etapa seis for Não para força de contrato
            if (empty($enviaPncp) && $minutaEmpenho->minutaempenho_forcacontrato) {
                $enviaPncp = $this->manipularSituacaoPNCPNovo(
                    $minutaEmpenho->id,
                    MinutaEmpenho::class,
                    "ASNPEN"
                );
            }

            # Se o arquivo da minuta de empenho for recuperado via kernel no método buscaArquivoEmpenhoAssinadoKernel
            if ($enviaPncp->status->descres === 'INCARQ' && $minutaEmpenho->minutaempenho_forcacontrato) {
                return;
            }

            # Se a ação for "Remover Substitutivo de Contrato"
            if (!$minutaEmpenho->minutaempenho_forcacontrato) {
                $this->deletarMinutaEmpenhoPNCP($enviaPncp, $contratoControllerPNCP);
                return;
            }

            # Se a ação for "Definir Substitutiva de Contrato"
            $this->incluirMinutaEmpenhoPNCP($enviaPncp, $contratoControllerPNCP);
        }
    }

    private function mountDataReturnFilePdfMinuta(
        bool    $sucesso,
        string  $mensagem,
        ?string $pathFileEmpenho = null
    ): \stdClass
    {
        $returnPdfMinuta = new \stdClass();

        $returnPdfMinuta->sucesso = $sucesso;
        $returnPdfMinuta->mensagem = $mensagem;
        $returnPdfMinuta->pathFileEmpenho = $pathFileEmpenho;

        return $returnPdfMinuta;
    }


    /**
     * Método responsável em salvar o arquivo PDF gerado pelo SIAFI
     * @param MinutaEmpenho $minutaEmpenho
     * @return \stdClass|null
     */
    private function gerarPdfNovo(
        MinutaEmpenho $minutaEmpenho,
        int           $idTipoArquivoMinutaEmpenho,
        bool          $force = false
    ): ?\stdClass
    {
        if ($minutaEmpenho->situacao->descricao != 'EMPENHO EMITIDO') {
            return $this->mountDataReturnFilePdfMinuta(false, 'Minuta sem Número de Empenho!');
        }

        $ug_emitente = $minutaEmpenho->saldo_contabil->unidade_id()->first()->codigo;
        $dados_empenho = [
            'ugemitente' => $ug_emitente,
            'anoempenho' => substr($minutaEmpenho->mensagem_siafi, 0, 4),
            'numempenho' => (int)substr($minutaEmpenho->mensagem_siafi, 6, 6),
        ];

        $numeroEmpenho = str_pad($dados_empenho['numempenho'], 6, "0", STR_PAD_LEFT);
        $nomeArquivoEmpenho = "{$dados_empenho['ugemitente']}_{$dados_empenho['anoempenho']}NE{$numeroEmpenho}";
        $path = "empenhos_pdf/{$nomeArquivoEmpenho}/";
        $nomeArquivoEmpenhoPdf = "$nomeArquivoEmpenho.pdf";

        $caminhoCompletoArquivoEmpenhoPdf =
            config('app.app_path') . "storage/app/$path$nomeArquivoEmpenhoPdf";

        $caminhoArquivoEmpenhoPdfDownload = "$path$nomeArquivoEmpenhoPdf";

        # Se o arquivo existir, então retorna as informações do arquivo
        if (file_exists($caminhoCompletoArquivoEmpenhoPdf)) {
            return $this->mountDataReturnFilePdfMinuta(
                true,
                'Arquivo na pasta local!',
                $caminhoArquivoEmpenhoPdfDownload);
        }

        $xml = new Execsiafi();

        $ug = $ug_emitente;
        # Se existir o usuário logado, então recupera as informações dele
        if (backpack_user() != null && !$force) {
            $cpf = backpack_user()->cpf;
        } else {
            //Usuário E0350
            $cpf = config('siapews.credenciais.cpf');
            $mask = '###.###.###-##';
            $cacheKey = 'ug_primaria_usuario_ROTINA_BAIXAR_PDF_EMPENHO';
            $userCache = Cache::get($cacheKey);

            if (is_null($userCache)) {
                $user = BackpackUser::where('cpf', $this->mask($cpf, $mask))->first();
                if (!$user) {
                    $user = BackpackUser::where('cpf', $cpf)->first();
                }
                Cache::put($cacheKey, $user, 60 * 5);
            }
        }

        $pdf_empenho = $xml->consultaRelarorioEmpenho($cpf, $dados_empenho, $ug);

        if ($pdf_empenho['situacao'] == 'ERRO') {
            if (backpack_user() == null) {
                $mensagemException = new Exception('Erro! ' . $pdf_empenho['mensagemretorno']);
                $this->inserirLogCustomizado('minuta-empenho', 'error', $mensagemException);
            }

            return $this->mountDataReturnFilePdfMinuta(false, $pdf_empenho['mensagemretorno']);
        }

        if ($pdf_empenho['situacao'] == 'SUCESSO') {
            $salva_arquivo = $this->salvarArquivoBase64ToPdf($path, $nomeArquivoEmpenho, $pdf_empenho['base64']);
            if ($salva_arquivo) {
                $this->inserirArquivoGenerico(
                    $minutaEmpenho->id,
                    $idTipoArquivoMinutaEmpenho,
                    $nomeArquivoEmpenho,
                    $caminhoArquivoEmpenhoPdfDownload
                );
                return $this->mountDataReturnFilePdfMinuta(
                    true,
                    'Arquivo salvo com sucesso!',
                    $caminhoArquivoEmpenhoPdfDownload
                );
            }
        }
    }

    private function inserirArquivoGenerico(
        int    $idMinutaEmpenho,
        int    $idTipoArquivoMinutaEmpenho,
        string $nomeArquivoEmpenho,
        string $caminhoCompletoArquivoEmpenhoPdf
    )
    {
        $arrayFiltro = [
            'arquivoable_type' => MinutaEmpenho::class,
            'arquivoable_id' => $idMinutaEmpenho,
            'nome' => $nomeArquivoEmpenho,
            'tipo_id' => $idTipoArquivoMinutaEmpenho
        ];

        $arrayDadosArquivoGenerico = [
            'descricao' => 'Arquivo salvo na pasta',
            'url' => $caminhoCompletoArquivoEmpenhoPdf,
            'restrito' => true
        ];

        #Se o usuário realizar a ação
        if (backpack_user() != null) {
            $arrayDadosArquivoGenerico['user_id'] = backpack_user()->id;
        }

        ArquivoGenerico::updateOrCreate($arrayFiltro, $arrayDadosArquivoGenerico);
    }

    /**
     *
     * Método responsável em retornar todos os registros em que a Minuta de Empenho foram
     * emitidas pelo sistema mas estão aguardando a assinatura no SIAFI e são Substitutivo de contrato
     *
     * @return mixed
     */
    private function getMinutaEmpenhoAssinaturaPendenteSubstitutivoDeContrato()
    {
        return EnviaDadosPncp::whereHas('status', function ($ci) {
            $ci->whereIN('descres', ['ASNPEN', 'INCPEN']);
        })->join('minutaempenhos', function ($query) {
            $query->on('minutaempenhos.id', 'envia_dados_pncp.pncpable_id')
                ->where('minutaempenhos.minutaempenho_forcacontrato', 1)
                ->whereNull('minutaempenhos.deleted_at');
        })
            ->where([
                ['pncpable_type', MinutaEmpenho::class]
            ])
            ->orderBy('minutaempenhos.id', 'desc')
            ->get();
    }

    /**
     *
     * Método responsável em retornar todos os registros em que a Minuta de Empenho foram
     * emitidas pelo sistema mas estão aguardando a assinatura no SIAFI
     *
     * @return mixed
     */
    private function getMinutaEmpenhoAssinaturaPendente()
    {
        return EnviaDadosPncp::whereHas('status', function ($ci) {
            $ci->where('descres', 'ASNPEN');
        })->where([
            ['pncpable_type', MinutaEmpenho::class],
            ['linkArquivoEmpenho', null]
        ])
            ->get();
    }

    private function retornarArray($return, $return_soma, $tipo): array
    {
        $return = array_map(
            function ($return) use ($return_soma, $tipo) {
                $id = array_search($return[$tipo], array_column($return_soma, $tipo));
                $return['qtd_total_item'] = $return_soma[$id]['qtd_total_item'];
                $return['vlr_total_item'] = $return_soma[$id]['vlr_total_item'];
                $vlr_unitario_item = 0;
                if (($return_soma[$id]['vlr_total_item'] > 0) && ($return_soma[$id]['qtd_total_item'] > 0)) {
                    $vlr_unitario_item = round(($return_soma[$id]['vlr_total_item'] / $return_soma[$id]['qtd_total_item']), 4);
                }
                $return['vlr_unitario_item'] = $this->ceil_dec($vlr_unitario_item, 2);
                return $return;
            },
            $return
        );

        return $return;
    }

    private function ceil_dec($val, $dec)
    {
        $pow = pow(10, $dec);

        return number_format($pow * $val, 2, '.', '') / $pow;
    }

    public function getEnviaDadosPncpAtualizado(int $pncpableId)
    {
        return EnviaDadosPncp::where('pncpable_id', $pncpableId)
            ->where('pncpable_type', MinutaEmpenho::class)->first();
    }
}
