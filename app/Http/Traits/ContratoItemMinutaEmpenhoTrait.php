<?php

namespace App\Http\Traits;

use App\Models\CompraItemMinutaEmpenho;
use App\Models\ContratoItemMinutaEmpenho;
use App\Models\MinutaEmpenho;

trait ContratoItemMinutaEmpenhoTrait
{

    /**
     *Trata os itens da minuta para retorno da API
     *
     * @param MinutaEmpenho $minuta
     * @return array
     */
    public function tratarItensMinutaEmpenho(MinutaEmpenho $minuta = null, bool $trazerAlteracaoItem = false): array
    {

        if($minuta === null)
        {
            return [];
        }
        //Itens da Minuta
        $itens_minuta_array = [];

        if ($minuta->tipo_minuta == 'Contrato') {
            //Busca os itens da minuta
            $itens_minuta_contrato =  (new ContratoItemMinutaEmpenho())->retornaItensMinuta($minuta->id_minuta);
            //Filtra os itens únicos (não duplicados)
            $itens_distintos =  $itens_minuta_contrato->unique('contrato_item_id');


            #Busca as alterações de itens da minuta
            $alteracaDeItensMinuta =  (new ContratoItemMinutaEmpenho())->retornaAlteracaoDeItensMinuta($minuta->id_minuta);

            //Para cada item, calcula os saldos por tipo de operação.
            foreach ($itens_distintos as $item) {

                $lista_por_item = $itens_minuta_contrato->where('contrato_item_id', $item->contrato_item_id);

                //Nas operações de INCLUSAO do item, soma a Quantidade e o Valor.
                $soma_quantidade_item_inclusao = $lista_por_item->where('tipo_operacao', 'INCLUSAO')->sum('quantidade');
                $soma_valor_item_inclusao = $lista_por_item->where('tipo_operacao', 'INCLUSAO')->sum('valor');

                //Nas operações de REFORÇO do item, soma a Quantidade e o Valor.
                $soma_quantidade_item_reforco = 0; // $lista_por_item->where('tipo_operacao', 'REFORCO')->sum('quantidade');
                $soma_valor_item_reforco = 0; //$lista_por_item->where('tipo_operacao', 'REFORCO')->sum('valor');

                //Nas operações de ANULACAO do item, soma a Quantidade e o Valor.
                $soma_quantidade_item_anulacao = 0; // $lista_por_item->where('tipo_operacao', 'ANULACAO')->sum('quantidade');
                $soma_valor_item_anulacao = 0; //$lista_por_item->where('tipo_operacao', 'ANULACAO')->sum('valor');

                $saldo_quantidade_item = $soma_quantidade_item_inclusao + $soma_quantidade_item_reforco + $soma_quantidade_item_anulacao;
                $saldo_valor_total_item = $soma_valor_item_inclusao + $soma_valor_item_reforco + $soma_valor_item_anulacao;

                $itens_minuta_array[] = $item->itemAPI(
                    $saldo_quantidade_item,
                    $saldo_valor_total_item,
                    $alteracaDeItensMinuta,
                    $trazerAlteracaoItem
                );
            }

            //Minutas do tipo Compra ou Suprimento
        } else {

            //Busca os itens da minuta
            $itens_minuta_compra =  (new CompraItemMinutaEmpenho())->retornaItensMinuta($minuta->id_minuta);
            //Aplica ordernacao dos itens pelo sequencial_siafi
            $itens_minuta_compra = $itens_minuta_compra->sortBy('sequencial_siafi');
            //Filtra os itens únicos (não duplicados)
            $itens_distintos =  $itens_minuta_compra->unique('compra_item_id');

            #Busca as alterações de itens da minuta
            $alteracaDeItensMinuta = (new CompraItemMinutaEmpenho())->retornaAlteracaoDeItensMinuta($minuta->id_minuta);
            //Aplica ordernacao dos itens pelo sequencial_siafi
            $alteracaDeItensMinuta = $alteracaDeItensMinuta->sortBy('sequencial_siafi');

            //Para cada item, calcula os saldos por tipo de operação.
            foreach ($itens_distintos as $item) {

                $lista_por_item = $itens_minuta_compra->where('compra_item_id', $item->compra_item_id);

                //Nas operações de INCLUSAO do item, soma a Quantidade e o Valor.
                $soma_quantidade_item_inclusao = $lista_por_item->where('tipo_operacao', 'INCLUSAO')->sum('quantidade');
                $soma_valor_item_inclusao = $lista_por_item->where('tipo_operacao', 'INCLUSAO')->sum('valor');

                //Nas operações de REFORÇO do item, soma a Quantidade e o Valor.
                $soma_quantidade_item_reforco = 0; // $lista_por_item->where('tipo_operacao', 'REFORCO')->sum('quantidade');
                $soma_valor_item_reforco = 0; //$lista_por_item->where('tipo_operacao', 'REFORCO')->sum('valor');

                //Nas operações de ANULACAO do item, soma a Quantidade e o Valor.
                $soma_quantidade_item_anulacao = 0; // $lista_por_item->where('tipo_operacao', 'ANULACAO')->sum('quantidade');
                $soma_valor_item_anulacao = 0; //$lista_por_item->where('tipo_operacao', 'ANULACAO')->sum('valor');

                $saldo_quantidade_item = $soma_quantidade_item_inclusao + $soma_quantidade_item_reforco + $soma_quantidade_item_anulacao;
                $saldo_valor_total_item = $soma_valor_item_inclusao + $soma_valor_item_reforco + $soma_valor_item_anulacao;

                $itens_minuta_array[] = $item->itemAPI(
                    $saldo_quantidade_item,
                    $saldo_valor_total_item,
                    $alteracaDeItensMinuta,
                    $trazerAlteracaoItem
                );

            }

        }

        return $itens_minuta_array;
    }
}
