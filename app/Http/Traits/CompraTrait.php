<?php

namespace App\Http\Traits;

use App\Http\Controllers\NDC;
use App\Models\AmparoLegal;
use App\Models\Catmatclasse;
use App\Models\Catmatpdms;
use App\Models\Catmatseritem;
use App\Models\Codigoitem;
use App\Models\Compra;
use App\Models\CompraItem;
use App\Models\CompraItemFornecedor;
use App\Models\CompraItemMinutaEmpenho;
use App\Models\CompraItemUnidade;
use App\Models\CompraItemUnidadeLocalEntrega;
use App\Models\Enderecos;
use App\Models\Fornecedor;
use App\Models\MinutaEmpenho;
use App\Models\Municipio;
use App\Models\Unidade;
use App\XML\ApiSiasg;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use stdClass;

trait CompraTrait
{

    public function retornaSaldoAtualizado($compraitem_id, $unidade_id = null, $compra_item_unidade_id = null)
    {
        if ($unidade_id === null) {
            $unidade_id = session('user_ug_id');
        }

        $retorno = CompraItemMinutaEmpenho::select(
            DB::raw(
                "
            COALESCE(COALESCE(compra_item_unidade.quantidade_autorizada, 0)
                    - (
                    SELECT COALESCE(sum(cime.quantidade), 0)
                    FROM compra_item_minuta_empenho cime
                             JOIN minutaempenhos m ON cime.minutaempenho_id = m.id
                             LEFT JOIN contrato_minuta_empenho_pivot cmep ON m.id = cmep.minuta_empenho_id
                    WHERE cime.compra_item_id = $compraitem_id
                        AND unidade_id = $unidade_id
                        AND cime.compra_item_unidade_id = $compra_item_unidade_id
                        AND cmep.contrato_id IS NULL
                    )
           , 0) AS saldo
            "
            )
        )
            ->join(
                'compra_items',
                'compra_items.id',
                '=',
                'compra_item_minuta_empenho.compra_item_id'
            )
            ->rightJoin(
                'compra_item_unidade',
                'compra_item_unidade.compra_item_id',
                '=',
                'compra_items.id'
            )
            ->where('compra_item_unidade.compra_item_id', $compraitem_id)
            ->where('compra_item_unidade.unidade_id', $unidade_id)
//            ->where('compra_item_unidade.situacao', '=', true)
            ->groupBy('compra_item_unidade.quantidade_autorizada', 'compra_item_unidade.id');

        if ($compra_item_unidade_id !== null){
            $retorno->where('compra_item_unidade.id', $compra_item_unidade_id);
        }

        return $retorno->first();
    }

    public function gravaParametroItensdaCompraSISPP($compraSiasg, $compra, $unidade_autorizada_id = null, $unidade_contrato = null): void
    {

        $unidade_autorizada_id = ($unidade_contrato) ? $unidade_contrato : session('user_ug_id');
        $this->gravaParametroSISPP($compraSiasg, $compra, $unidade_autorizada_id);
    }

    public function gravaParametroItensdaCompraSISPPCommand($compraSiasg, $compra): void
    {
        $unidade_autorizada_id = $compra->unidade_origem_id;
        $this->gravaParametroSISPP($compraSiasg, $compra, $unidade_autorizada_id);
    }

    private function gravaParametroSISPP($compraSiasg, $compra, $unidade_autorizada_id): void
    {
        if (!is_null($compraSiasg->data->itemCompraSisppDTO)) {
            CompraItem::where('compra_id', (int)$compra->id)->update(['situacao' => false]);
            $compraItemFornecedor = $compraItemUnidade = [];
            foreach ($compraSiasg->data->itemCompraSisppDTO as $key => $item) {
                $catmatseritem = $this->gravaCatmatseritem($item);

                $compraitem = $this->updateOrCreateCompraItemSispp($compra, $catmatseritem, $item);

                $fornecedor = $this->retornaFornecedor($item);

                $compraItemFornecedor[] = $this->gravaCompraItemFornecedor($compraitem->id, $item, $fornecedor);
                $compraItemUnidade[]    = $this->gravaCompraItemUnidadeSispp(
                    $compraitem->id,
                    $item,
                    $unidade_autorizada_id,
                    $fornecedor
                );
            }

            //Inativa os itens que não vieram do SIASG
            CompraItemFornecedor::join('compra_items', 'compra_items.id', '=', 'compra_item_fornecedor.compra_item_id')
                ->where('compra_items.compra_id', (int)$compra->id)
                ->whereNotIn('compra_item_fornecedor.id', $compraItemFornecedor)
                ->update(['situacao' => false]);

            CompraItemUnidade::join('compra_items', 'compra_items.id', '=', 'compra_item_unidade.compra_item_id')
                ->where('compra_items.compra_id', (int)$compra->id)
                ->whereNotIn('compra_item_unidade.id', $compraItemUnidade)
                ->update(['situacao' => false]);
        }
    }

    /**
     * @param stdClass $item
     * @return int|null
     */
    public function getCatmatpdm_id(stdClass $item): ?int
    {
        //caso seja material
        if ($item->tipo === 'M') {
            $catmatpdm_id = Catmatpdms::firstOrCreate(
                ['codigo' => $item->pdm],
                [
                    'descricao' => $item->nomePdm,
                    'catmatclasse_id' => Catmatclasse::where('descricao', 'CLASSE GENERICA MATERIAIS')
                        ->select('id')->first()->id
                ]
            );
            return $catmatpdm_id->id;
        }
        return null;
    }

    private function gravaParametrosSuprimento(Compra $compra, string $fornecedor_empenho_id): void
    {
        $fornecedor = Fornecedor::find($fornecedor_empenho_id);

        $item = new stdClass;
        $item->tipo = 'S';
        $item->numero = '00001';
        $item->descricaoDetalhada = 'Serviço';
        $item->quantidadeTotal = 1;

        //campos para gravar no CompraItemFornecedor
        $item->classicacao = '';
        $item->situacaoSicaf = '-';
        $item->quantidadeHomologadaVencedor = 0;
        $item->valorUnitario = 1;
        $item->valorTotal = 0;
        $item->quantidadeEmpenhada = 0;

        $this->gravaSuprimento('SERVIÇO PARA SUPRIMENTO DE FUNDOS', $compra, $item, $fornecedor);

        $item->tipo = 'M';
        $item->numero = '00002';
        $item->descricaoDetalhada = 'Material';

        $this->gravaSuprimento('MATERIAL PARA SUPRIMENTO DE FUNDOS', $compra, $item, $fornecedor);
    }

    private function gravaSuprimento($descricao, $compra, $item, $fornecedor): void
    {
        $catmatseritem = Catmatseritem::where('descricao', $descricao)
            ->select('id')->first();

        $compraitem = $this->updateOrCreateCompraItemSispp($compra, $catmatseritem, $item);
        $this->gravaCompraItemFornecedor($compraitem->id, $item, $fornecedor);
        $this->gravaCompraItemUnidadeSuprimento($compraitem->id);
    }

    public function gravaParametroItensdaCompraSISRP($compraSiasg, $compra, $unidade_contrato = null): void
    {

        $unidade_autorizada_id = ($unidade_contrato) ? $unidade_contrato : session('user_ug_id');

        $this->gravaParametroSISRP($compraSiasg, $compra, $unidade_autorizada_id);
    }

    public function gravaParametroItensdaCompraSISRPCommand($compraSiasg, $compra): void
    {
        $unidade_autorizada_id = $compra->unidade_origem_id;
        $this->gravaParametroSISRP($compraSiasg, $compra, $unidade_autorizada_id);
    }

    private function gravaParametroSISRP($compraSiasg, $compra, $unidade_autorizada_id): void
    {
        $consultaCompra = new ApiSiasg();
        $api_origem = $compraSiasg->data->api_origem ?? 'siasg';

        if (!isset($compraSiasg->data->linkSisrpCompleto)) {
            Log::channel('minuta-empenho')->error('gravaParametroSISRP Sem linkSisrpCompleto data-> : ' . json_encode($compraSiasg->data));
            Log::channel('minuta-empenho')->error('gravaParametroSISRP Sem linkSisrpCompleto compra-> : ' . json_encode($compra->toJson()));
        }

        if (!is_null($compraSiasg->data->linkSisrpCompleto)) {
            $tipo = substr($compraSiasg->data->linkSisrpCompleto[0]->linkSisrpCompleto, -1);

            if ($tipo === 'G') {
                CompraItem::where('compra_id', (int)$compra->id)->update(['situacao' => false]);
            }

            $compraItemFornecedor = $compraItemUnidade = $uasgs = [];
            foreach ($compraSiasg->data->linkSisrpCompleto as $key => $item) {
                if ($api_origem === 'novo_divulgacao_compra') {
                    $dadosItemCompra = NDC::consulta(
                        'itemUrl',
                        [
                            'itemUrl' => $item->linkSisrpCompleto
                        ]
                    );
                } else {
                    $dadosItemCompra = ($consultaCompra->consultaCompraByUrl($item->linkSisrpCompleto));
                }

                if (is_null($dadosItemCompra['data'])) {
                    continue;
                }

                $tipoUasg = (substr($item->linkSisrpCompleto, -1));
                $dadosata = (object)$dadosItemCompra['data']['dadosAta'];
                $gerenciadoraParticipante = (object)$dadosItemCompra['data']['dadosGerenciadoraParticipante'];
                $carona = $dadosItemCompra['data']['dadosCarona'];
                $dadosFornecedor = $dadosItemCompra['data']['dadosFornecedor'];
                $catmatseritem = $this->gravaCatmatseritem($dadosata);

                $modcompraItem = new CompraItem();
                $compraItem = $modcompraItem->updateOrCreateCompraItemSisrp($compra, $catmatseritem, $dadosata);

                if (!isset($dadosFornecedor)) {
                    Log::channel('minuta-empenho')->error('gravaParametroSISRP Sem dadosFornecedor dadosItemCompra-> : ' . json_encode($dadosItemCompra));
                    Log::channel('minuta-empenho')->error('gravaParametroSISRP Sem dadosFornecedor linkSisrpCompleto-> : ' . json_encode($item->linkSisrpCompleto));
                }

                foreach ($dadosFornecedor as $key => $itemfornecedor) {
                    $fornecedor = $this->retornaFornecedor((object)$itemfornecedor);

                    $compraItemFornecedor[] = $this->gravaCompraItemFornecedor(
                        $compraItem->id,
                        (object)$itemfornecedor,
                        $fornecedor
                    );
                }
                $compraItemUnidade[] = $this->gravaCompraItemUnidadeSisrp(
                    $compraItem,
                    $unidade_autorizada_id,
                    $item,
                    $gerenciadoraParticipante,
                    $carona,
                    $dadosFornecedor,
                    $tipoUasg
                );
            }

            //Inativa os CompraItemFornecedor que não vieram do SIASG
            //caso seja gerenciadora
            if ($tipo === 'G') {
                CompraItemFornecedor::join(
                    'compra_items',
                    'compra_items.id',
                    '=',
                    'compra_item_fornecedor.compra_item_id'
                )
                    ->where('compra_items.compra_id', (int)$compra->id)
                    ->whereNotIn('compra_item_fornecedor.id', $compraItemFornecedor)
                    ->update(['situacao' => false]);
            }

            //Inativa os CompraItemUnidade que não vieram do SIASG PARA ESTA UNIDADE
                CompraItemUnidade::join('compra_items', 'compra_items.id', '=', 'compra_item_unidade.compra_item_id')
                    ->where('compra_items.compra_id', (int)$compra->id)
                    ->whereNotIn('compra_item_unidade.id', $compraItemUnidade)
                    ->where('compra_item_unidade.unidade_id', session('user_ug_id'))
                    ->update(['situacao' => false]);
        }
    }

    public function retornaUnidadeAutorizada($compraSiasg, $compra)
    {
        $SISPP = 1;
        $SISRP = 2;

        $unidade_autorizada_id = null;
        $tipoCompra = $compraSiasg->data->compraSispp->tipoCompra;
        $subrrogada = $compraSiasg->data->compraSispp->subrogada;
        if ($tipoCompra == $SISPP) {
            ($subrrogada <> '000000')
                ? $unidade_autorizada_id = (int)$this->buscaIdUnidade($subrrogada)
                : $unidade_autorizada_id = $compra->unidade_origem_id;
        }
        if ($tipoCompra == $SISRP) {
            ($subrrogada <> '000000')
                ? $unidade_autorizada_id = (int)$this->buscaIdUnidade($subrrogada)
                : $unidade_autorizada_id = $compra->unidade_origem_id;
        }

        return $unidade_autorizada_id;
    }

    public function buscaIdUnidade($uasg)
    {
        $unidade = Unidade::where('codigo', $uasg)->first();
        return $unidade->id ?? null;
    }

    public function gravaCatmatseritem($item)
    {
        $MATERIAL = [149, 194];
        $SERVICO = [150, 195];

        $codigo_siasg = (isset($item->codigo)) ? $item->codigo : $item->codigoItem;
        $tipo = ['S' => $SERVICO[0], 'M' => $MATERIAL[0]];
        $catGrupo = ['S' => $SERVICO[1], 'M' => $MATERIAL[1]];

        $catmatpdm_id = $this->getCatmatpdm_id($item);

        $descricao = $item->descricao ?: $codigo_siasg . " - Descrição não informada pelo serviço.";

        $catmatseritem = Catmatseritem::updateOrCreate(
            ['codigo_siasg' => (int)$codigo_siasg, 'grupo_id' => (int)$catGrupo[$item->tipo]],
            ['descricao' => $descricao, 'grupo_id' => $catGrupo[$item->tipo], 'catmatpdm_id' => $catmatpdm_id]
        );

        return $catmatseritem;
    }

    public function updateOrCreateCompraItemSispp($compra, $catmatseritem, $item)
    {
        $MATERIAL = [149, 194];
        $SERVICO = [150, 195];
        $tipo = ['S' => $SERVICO[0], 'M' => $MATERIAL[0]];

        $compraitem = CompraItem::updateOrCreate(
            [
                'compra_id' => (int)$compra->id,
                'tipo_item_id' => (int)$tipo[$item->tipo],
                'catmatseritem_id' => (int)$catmatseritem->id,
                'numero' => (string)$item->numero,
            ],
            [
                'descricaodetalhada' => (string)$item->descricaoDetalhada,
                'qtd_total' => $item->quantidadeTotal,
                'criterio_julgamento' => $item->criterioJulgamento ?? null,
                'criterio_julgamento_id' => $item->criterioJulgamentoId ?? null,
                'situacao' => true,
                'codigo_ncmnbs' => $item->codigoNcmNbs ?? null,
                'aplica_margem_ncmnbs' => $item->aplicaMargemNcmNbs ?? false,
                'descricao_ncmnbs' => $item->descricaoNcmNbs ?? null,
            ]
        );
        return $compraitem;
    }

    public function retornaFornecedor($item)
    {
        $fornecedor = new Fornecedor();
        if (mb_strtoupper($item->niFornecedor, 'UTF-8') === 'ESTRANGEIRO'
            || ctype_alpha(substr(mb_strtoupper($item->niFornecedor, 'UTF-8'),0,2))
            || strlen(mb_strtoupper($item->niFornecedor, 'UTF-8')) == 9) {

            $cpf_cnpj_idgener = 'ESTRANGEIRO_' . mb_strtoupper(preg_replace('/\s/', '_', $item->nomeFornecedor), 'UTF-8');

            $retorno = $fornecedor->buscaFornecedorPorNumero($cpf_cnpj_idgener);

            if (is_null($retorno)) {
                $fornecedor->tipo_fornecedor = 'IDGENERICO';
                $fornecedor->cpf_cnpj_idgener = $cpf_cnpj_idgener;
                $fornecedor->nome = $item->nomeFornecedor;
                $fornecedor->save();
                return $fornecedor;
            }
            return $retorno;
        }

        $retorno = $fornecedor->buscaFornecedorPorNumero($item->niFornecedor);

        //TODO UPDATE OR INSERT FORNECEDOR
        if (is_null($retorno)) {
            $fornecedor->tipo_fornecedor = $fornecedor->retornaTipoFornecedor($item->niFornecedor);
            $fornecedor->cpf_cnpj_idgener = $fornecedor->formataCnpjCpf($item->niFornecedor);
            $fornecedor->nome = $item->nomeFornecedor;
            $fornecedor->save();
            return $fornecedor;
        }
        return $retorno;
    }

    public function gravaCompraItemFornecedor($compraitem_id, $item, $fornecedor, $percentualMaiorDesconto = '0')
    {
        $percentMaiorDesconto = $item->percentualMaiorDesconto ?? $percentualMaiorDesconto;

        $qtd_empenhada = (isset($item->quantidadeEmpenhada))
            ? preg_replace('/[^0-9]/', '', $item->quantidadeEmpenhada)
            : 0;

        $cif = CompraItemFornecedor::updateOrCreate(
            [
                'compra_item_id' => $compraitem_id,
                'fornecedor_id' => $fornecedor->id
            ],
            [
                'ni_fornecedor' => $fornecedor->cpf_cnpj_idgener,
                'classificacao' => $item->classificacao ?? '',
                'situacao_sicaf' => $item->situacaoSicaf,
                'quantidade_homologada_vencedor' => $item->quantidadeHomologadaVencedor ?? 0,
                'valor_unitario' => $item->valorUnitario,
                'valor_negociado' => $item->valorTotal ?? $item->valorNegociado,
                'quantidade_empenhada' => $qtd_empenhada,
                'percentual_maior_desconto' => $percentMaiorDesconto,
                'situacao' => true
            ]
        );
        return $cif->id;
    }

    public function gravaCompraItemFornecedorSuprimento($minuta, $fornecedor_id)
    {
        $fornecedor = Fornecedor::find($fornecedor_id);

        foreach ($minuta->compra->compra_item as $compra_item) {
            CompraItemFornecedor::updateOrCreate(
                [
                    'compra_item_id' => $compra_item->id,
                    'fornecedor_id' => $fornecedor->id
                ],
                [
                    'ni_fornecedor' => $fornecedor->cpf_cnpj_idgener,
                    'classificacao' => '',
                    'situacao_sicaf' => '-',
                    'quantidade_homologada_vencedor' => 0,
                    'valor_unitario' => 1,
                    'valor_negociado' => 0,
                    'quantidade_empenhada' => 0
                ]
            );
        }
    }

    public function gravaCompraItemUnidadeSispp(
        $compraitem_id,
        $item,
        $unidade_autorizada_id,
        $fornecedor
    ) {
        $compraItemUnidade = CompraItemUnidade::updateOrCreate(
            [
                'compra_item_id' => $compraitem_id,
                'unidade_id' => $unidade_autorizada_id,
                'fornecedor_id' => $fornecedor->id
            ],
            [
                'quantidade_saldo' => $item->quantidadeTotal,
                'quantidade_autorizada' => $item->quantidadeTotal,
                'situacao' => true
            ]
        );
        $saldo = $this->retornaSaldoAtualizado(
            $compraitem_id,
            $unidade_autorizada_id,
            $compraItemUnidade->id
        );

        $compraItemUnidade->quantidade_saldo = $saldo->saldo;
        $compraItemUnidade->save();

        //caso venha do novo ndc salva locais de entrega
        if (isset($item->locaisEntregaPorUnidade)) {
            $unidade_autorizada = Unidade::find($unidade_autorizada_id);

            foreach ($item->locaisEntregaPorUnidade as $index => $value) {
                $this->gravaCompraItemUnidadeLocaisEntrega(
                    $compraItemUnidade->id,
                    $value,
                    $index
                );
            }
        }

        return $compraItemUnidade->id;
    }

    public function gravaCompraItemUnidadeSuprimento($compraitem_id): void
    {
        CompraItemUnidade::updateOrCreate(
            [
                'compra_item_id' => $compraitem_id,
                'unidade_id' => session('user_ug_id'),
            ],
            [
                'quantidade_saldo' => 1,
                'quantidade_autorizada' => 1
            ]
        );
    }

    public function gravaCompraItemUnidadeLocaisEntrega($compraItemUnidade_id, $locaisEntrega, $unidade_entrega): void
    {
        if (!empty($locaisEntrega->localEntrega)) {
            $unidade_entrega_id = $this->buscaIdUnidade($unidade_entrega);

            if (empty($unidade_entrega_id)) {
                throw new \Exception(
                    'Erro ao consultar o novo divulgação de compras: ' .
                    'Contratação com local de entrega inválido. Favor retificar no Compras.gov.br',
                    500
                );
            }

            foreach ($locaisEntrega->localEntrega as $local) {
                $municipio = (new Municipio)->getMunicipio($local->codigoMunicipio, $local->uf, $local->municipio);
                if (empty($municipio)) {
                    throw new \Exception(
                        'Erro ao consultar o novo divulgação de compras: ' .
                        'Contratação com local de entrega inválido. Favor retificar no Compras.gov.br',
                        500
                    );
                }

                $endereco = Enderecos::updateOrCreate([
                    'municipios_id' => $municipio->id,
                    'cep' => $local->cep ?? null,
                    'logradouro' => $local->logradouro ?? null,
                    'numero' => $local->numero ?? null,
                    'complemento' => $local->complemento ?? null,
                    'bairro' => $local->bairro ?? null,
                    'codigo_municipio_ndc' => $local->codigoMunicipio ?? null,
                ]);

                CompraItemUnidadeLocalEntrega::updateOrCreate(
                    [
                        'compra_item_unidade_id' => $compraItemUnidade_id,
                        'endereco_id' => $endereco->id,
                        'unidade_entrega_id' => $unidade_entrega_id,
                    ],
                    [
                        'endereco_id_novo_divulgacao' => $local->id,
                        'quantidade' => $local->quantidade,
                    ]
                );
            }
        }
    }

    public function gravaCompraItemUnidadeSisrp(
        $compraitem,
        $unidade_autorizada_id,
        $item,
        $dadosGerenciadoraParticipante,
        $carona,
        $dadosFornecedor,
        $tipoUasg
    ) {
        $fornecedor_id = null;
        if (!is_null($carona)) {
            $carona = (object)$carona[0];
            $qtd_autorizada = $carona->quantidadeAutorizada;
            $fornecedor = $this->retornaFornecedor((object)$dadosFornecedor[0]);
            $fornecedor_id = $fornecedor->id;
            $quantidadeAAdquirir = $qtd_autorizada;
            $quantidadeAdquirida = 0;
        } else {
            $qtd_autorizada = $dadosGerenciadoraParticipante->quantidadeAAdquirir
                - $dadosGerenciadoraParticipante->quantidadeAdquirida;
            $quantidadeAAdquirir = $dadosGerenciadoraParticipante->quantidadeAAdquirir;
            $quantidadeAdquirida = $dadosGerenciadoraParticipante->quantidadeAdquirida;
        }
        $compraItemUnidade = CompraItemUnidade::where(
            [
                'compra_item_id' => $compraitem->id,
                'unidade_id' => $unidade_autorizada_id,
                'fornecedor_id' => $fornecedor_id,
            ]
        )->first();

        if (is_null($compraItemUnidade)) {
            $compraItemUnidade = new CompraItemUnidade;
            $compraItemUnidade->compra_item_id = $compraitem->id;
            $compraItemUnidade->unidade_id = $unidade_autorizada_id;
            $compraItemUnidade->fornecedor_id = $fornecedor_id;
        }
        $compraItemUnidade->quantidade_autorizada = $qtd_autorizada;
        $compraItemUnidade->quantidade_saldo = $qtd_autorizada;
        $compraItemUnidade->tipo_uasg = $tipoUasg;
        $compraItemUnidade->quantidade_adquirir = $quantidadeAAdquirir;
        $compraItemUnidade->quantidade_adquirida = $quantidadeAdquirida;
        $compraItemUnidade->situacao = true;
        $compraItemUnidade->save();

        $saldo = $this->retornaSaldoAtualizado(
            $compraitem->id,
            $unidade_autorizada_id,
            $compraItemUnidade->id
        );
        $compraItemUnidade->quantidade_saldo = (isset($saldo->saldo)) ? $saldo->saldo : $qtd_autorizada;
        $compraItemUnidade->save();

        return $compraItemUnidade->id;
    }

    private function setCondicaoFornecedor(
        $minuta,
        $itens,
        string $descricao,
        $fornecedor_id,
        $fornecedor_compra_id
    ) {
        //SE FOR ESTRANGEIRO
        if ($fornecedor_id != $fornecedor_compra_id) {
            $fornecedor_id = $fornecedor_compra_id;
        }
        $itens = $itens->where('compra_item_fornecedor.fornecedor_id', $fornecedor_id);

        if ($descricao === 'Suprimento') {
            return $itens;
        }

        $tipo_compra = $minuta->tipo_compra;

        if ($tipo_compra === 'SISPP') {
            return $itens->where('compra_item_unidade.fornecedor_id', $fornecedor_id);
        }

        return $itens;

    }

    private function setColunaContratoQuantidade(array $item, $tipos = null): string
    {
        $quantidade = $item['quantidade'];
        if ($tipos !== null) {
            if (array_key_exists($item['operacao_id'], $tipos) && $tipos[$item['operacao_id']] === "ANULAÇÃO") {
                $quantidade *= -1;
            }
            $quantidade = sprintf('%.5f', (float)$quantidade);
        }

        $qtd_field = " <input  type='number' max='" . $item['qtd_item'] . "' min='1' " .
            "class='form-control qtd qtd" . $item['contrato_item_id'] . "' " .
            "id='qtd" . $item['contrato_item_id'] . "' " .
            "data-tipo='' name='qtd[]' value='$quantidade' " .
            "data-item-id='" . $item['contrato_item_id'] . "' " .
            "data-qtd_item='" . $item['qtd_item'] . "' name='valor_total[]' value='" . $item['valor'] . "' " .
            "data-contrato_item_id='" . $item['contrato_item_id'] . "' ";

        if (isset($item['vlr_unitario_item'])) {
            $qtd_field .= "data-vlr_unitario_item='" . $item['vlr_unitario_item'] . "' ";
        }
        $qtd_field .= "data-valor_unitario='" . $item['valorunitario'] . "' " .
            "onchange='calculaValorTotal(this)' >" .
            "<input  type='hidden' id='quantidade_total" . $item['contrato_item_id'] . "' " .
            "data-tipo='' name='quantidade_total[]' value='" . $item['qtd_item'] . " '> ";
        return $qtd_field;
    }

    private function setColunaContratoValorTotal(array $item, $tipos = null): string
    {
        $valor = (float)$item['valor'];

        if (!is_null($tipos)) {
            if (array_key_exists($item['operacao_id'], $tipos) && $tipos[$item['operacao_id']] === "ANULAÇÃO") {
                $valor *= -1;
            }
        }
        $valor = number_format($valor, '2', '.', '');

        $numseq = $item['numseq'] ?? '';

        $valorTotal = " <input type='text' id='vrtotal" . $item['contrato_item_id'] . "' " .
            "class='form-control col-md-12 valor_total vrtotal" . $item['contrato_item_id'] . "' " .
            "data-qtd_item='" . $item['qtd_item'] . "' name='valor_total[]' value='$valor' " .
            "data-contrato_item_id='" . $item['contrato_item_id'] . "' " .
            "data-item-id='" . $item['contrato_item_id'] . "' " .
            "data-numseq='$numseq' " .
            "data-qtditem='" . $item['valortotal'] . "' " .
            "data-valor_unitario='" . $item['valorunitario'] . "' ";
        if (isset($item['vlr_unitario_item'])) {
            $valorTotal .= "data-vlr_unitario_item='" . $item['vlr_unitario_item'] . "' ";
        }
        $valorTotal .= "onkeyup='calculaQuantidade(this)' >";
        return $valorTotal;
    }

    private function setColunaSuprimentoQuantidade(array $item, $tipos = null): string
    {
        $quantidade = $item['quantidade'];
        if ($tipos !== null) {
            if (array_key_exists($item['operacao_id'], $tipos) && $tipos[$item['operacao_id']] === "ANULAÇÃO") {
                $quantidade *= -1;
            }
            $quantidade = sprintf('%.5f', (float)$quantidade);
        }

        return " <input  type='number' max='" . $item['qtd_item'] . "' min='1' " .
            "class='form-control qtd" . $item['compra_item_id'] . "' id='qtd" . $item['compra_item_id'] . "' " .
            "data-item-id='" . $item['compra_item_id'] . "' " .
            "data-tipo='' name='qtd[]' value='$quantidade' readonly  > "
            . " <input  type='hidden' id='quantidade_total" . $item['compra_item_id']
            . "' data-tipo='' name='quantidade_total[]' value='"
            . $item['qtd_item'] . " readonly'> ";
    }

    private function setColunaSuprimentoValorTotal(array $item, $tipos = null): string
    {
        $valor = (float)$item['valor'];
        if (!is_null($tipos)) {
            if (array_key_exists($item['operacao_id'], $tipos) && $tipos[$item['operacao_id']] === "ANULAÇÃO") {
                $valor *= -1;
            }
        }
        $valor = number_format($valor, '2', '.', '');

        $numseq = $item['numseq'] ?? null;

        $valorTotal = " <input type='text' id='vrtotal" . $item['compra_item_id'] . "' " .
            "class='form-control col-md-12 valor_total vrtotal" . $item['compra_item_id'] . "' " .
            "data-qtd_item='" . $item['qtd_item'] . "' name='valor_total[]' value='$valor' " .
            "data-compra_item_id='" . $item['compra_item_id'] . "' " .
            "data-item-id='" . $item['compra_item_id'] . "' " .
            "data-numseq='$numseq' " .
            "data-qtditem='" . $item['valortotal'] . "' " .
            "data-valor_unitario='" . $item['valorunitario'] . "' ";
        if (isset($item['vlr_unitario_item'])) {
            $valorTotal .= "data-vlr_unitario_item='" . $item['vlr_unitario_item'] . "' ";
        }
        $valorTotal .= "onkeyup='calculaQuantidade(this)' >";
        return $valorTotal;
    }

    private function setColunaCompraSisrpQuantidade(array $item, $tipos = null): string
    {
        $quantidade = $item['quantidade'];
        if ($tipos !== null) {
            if (array_key_exists($item['operacao_id'], $tipos) && $tipos[$item['operacao_id']] === "ANULAÇÃO") {
                $quantidade *= -1;
            }
            $quantidade = sprintf('%.5f', (float)$quantidade);
        }

        $qtd_field = " <input type='number' max='" . $item['qtd_item'] . "' min='1' " .
            "id='qtd" . $item['compra_item_id'] . "' " .
            "data-compra_item_id='" . $item['compra_item_id'] . "' " .
            "data-valor_unitario='" . $item['valorunitario'] . "' name='qtd[]' " .
            "data-item-id='" . $item['compra_item_id'] . "' " .
            "data-qtditem='" . $item['qtd_item'] . "' " .
            "class='form-control qtd' value='$quantidade' ";

        if (isset($item['vlr_unitario_item'])) {
            $qtd_field .= "data-vlr_unitario_item='" . $item['vlr_unitario_item'] . "' ";
        }
        $qtd_field .= "onchange='calculaValorTotal(this)' >" .
            "<input  type='hidden' id='quantidade_total" . $item['compra_item_id'] . "' " .
            "data-tipo='' name='quantidade_total[]' value='" . $item['qtd_item'] . "'> ";
        return $qtd_field;
    }

    private function setColunaCompraSisrpValorTotal(array $item, $tipos = null): string
    {
        $valor = (float)$item['valor'];
        if (!is_null($tipos)) {
            if (array_key_exists($item['operacao_id'], $tipos) && $tipos[$item['operacao_id']] === "ANULAÇÃO") {
                $valor *= -1;
            }
        }
        $valor = number_format($valor, '2', '.', '');

        $numseq = $item['numseq'] ?? null;

        return " <input  type='text' class='form-control valor_total vrtotal" . $item['compra_item_id'] . "' " .
            "id='vrtotal" . $item['compra_item_id'] . "' " .
            "data-item-id='" . $item['compra_item_id'] . "' " .
            "data-numseq='$numseq' " .
            "data-tipo='' name='valor_total[]' value='$valor' disabled > ";
    }

    private function setColunaCompraSisppMaterialQuantidade(array $item, $tipos = null): string
    {
        $quantidade = $item['quantidade'];
        if ($tipos !== null) {
            if (array_key_exists($item['operacao_id'], $tipos) && $tipos[$item['operacao_id']] === "ANULAÇÃO") {
                $quantidade *= -1;
            }
            $quantidade = sprintf('%.5f', (float)$quantidade);
        }

        $qtd_field = " <input type='number' max='" . $item['qtd_item'] . "' min='1' " .
            "id='qtd" . $item['compra_item_id'] . "' data-compra_item_id='" . $item['compra_item_id'] . "' " .
            "data-valor_unitario='" . $item['valorunitario'] . "' name='qtd[]' " .
            "data-item-id='" . $item['compra_item_id'] . "' " .
            "data-qtditem='" . $item['qtd_item'] . "' " .
            "class='form-control qtd' value='$quantidade' ";
        if (isset($item['vlr_unitario_item'])) {
            $qtd_field .= "data-vlr_unitario_item='" . $item['vlr_unitario_item'] . "' ";
        }
        $qtd_field .= "onchange='calculaValorTotal(this)' >" .
            "<input  type='hidden' id='quantidade_total" . $item['compra_item_id'] . "' " .
            "data-tipo='' name='quantidade_total[]' value='" . $item['qtd_item'] . "'> ";
        return $qtd_field;
    }

    private function setColunaCompraSisppMaterialValorTotal(array $item, $tipos = null): string
    {
        $valor = (float)$item['valor'];
        if (!is_null($tipos)) {
            if (array_key_exists($item['operacao_id'], $tipos) && $tipos[$item['operacao_id']] === "ANULAÇÃO") {
                $valor *= -1;
            }
        }
        $valor = number_format($valor, '2', '.', '');

        $numseq = $item['numseq'] ?? null;

        return " <input  type='text' class='form-control valor_total vrtotal" . $item['compra_item_id'] . "' " .
            "id='vrtotal" . $item['compra_item_id'] . "' " .
            "data-item-id='" . $item['compra_item_id'] . "' " .
            "data-numseq='$numseq' " .
            "data-tipo='' name='valor_total[]' value='$valor' disabled > ";
    }

    private function setColunaCompraSisppServicoQuantidade(array $item, $tipos = null): string
    {
        $quantidade = $item['quantidade'];
        if ($tipos !== null) {
            if (array_key_exists($item['operacao_id'], $tipos) && $tipos[$item['operacao_id']] === "ANULAÇÃO") {
                $quantidade *= -1;
            }
            $quantidade = sprintf('%.5f', (float)$quantidade);
        }
        return " <input  type='number' max='" . $item['qtd_item'] . "' min='1' " .
            "class='form-control qtd" . $item['compra_item_id'] . "' " .
            "id='qtd" . $item['compra_item_id'] . "' " .
            "data-item-id='" . $item['compra_item_id'] . "' " .
            "data-tipo='' name='qtd[]' value='$quantidade' readonly> " .
            "<input type='hidden' id='quantidade_total" . $item['compra_item_id'] . "' " .
            "data-tipo='' name='quantidade_total[]' value='" . $item['qtd_item'] . " '> ";
    }

    private function setColunaCompraSisppServicoValorTotal(array $item, $tipos = null): string
    {
        $valor = (float)$item['valor'];
        $disabled = '';
        if (!is_null($tipos)) {
            $disabled = 'disabled';

            if (array_key_exists($item['operacao_id'], $tipos) && $tipos[$item['operacao_id']] === "ANULAÇÃO") {
                $valor *= -1;
            }
        }
        $valor = number_format($valor, '2', '.', '');

        $numseq = $item['numseq'] ?? null;

        $valorTotal = " <input type='text' " .
            "class='form-control col-md-12 valor_total vrtotal" . $item['compra_item_id'] . "' " .
            "id='vrtotal" . $item['compra_item_id'] . "' data-qtd_item='" . $item['qtd_item'] . "' " .
            "name='valor_total[]' value='$valor' " .
            "data-compra_item_id='" . $item['compra_item_id'] . "' " .
            "data-item-id='" . $item['compra_item_id'] . "' " .
            "data-numseq='$numseq' " .
            "data-qtditem='" . $item['valortotal'] . "' " .
            "data-valor_unitario='" . $item['valorunitario'] . "' ";
        if (isset($item['vlr_unitario_item'])) {
            $valorTotal .= "data-vlr_unitario_item='" . $item['vlr_unitario_item'] . "' ";
        }
        $valorTotal .= "onkeyup='calculaQuantidade(this)' $disabled>";
        return $valorTotal;
    }

    /**
     * Recupera se o uasg utiliza o sisg conforme salvo no banco de dados
     */
    private function validarUasgNaoSisg(?int $idUasg)
    {
        $uasgRecuperada = Unidade::whereNull('deleted_at')
            ->where('id', $idUasg)
            ->pluck('sisg')
            ->first();
        return $uasgRecuperada ?: false;
    }

    /**
     * Valida se o Codigo do item selecionado e diferente de Não se aplica
     */
    private function validarIDCodigoItem(int $idCodigoItem, string $descricao = 'Não se Aplica')
    {
        $arrayIdCodigoItem =Codigoitem::select('id')
            ->whereNull('deleted_at')
            ->where('descricao', $descricao)
            ->pluck('id')->toArray();

        if (in_array($idCodigoItem, $arrayIdCodigoItem)) {
            return false;
        }

        return true;
    }

    public function validaRetornoSiasg($retornoSiasg)
    {

        if (is_null($retornoSiasg->data)) {
            if (isset($retornoSiasg->messagem)) {
                throw new \Exception($retornoSiasg->messagem);
            }
            throw new \Exception('Erro ao consultar e atualizar compra SIASG');
        }
        return;
    }

    private function montarInformacoesConsultaCompraSISPP(?string $descricao, ?string $numeroAno, ?string $uasgCompra)
    {
        return [
            'modalidade' => $descricao,
            'numeroAno' => $numeroAno,
            'uasgCompra' => $uasgCompra,
            'uasgUsuario' => session('user_ug')
        ];
    }

    private function mensagemValidacaoDadosCompraInvalidos()
    {
        return  [
                    "texto_exibicao.validarcompra"=>'Dados da compra inválidos!',
                    "unidadecompra_id.validarcompra"=>'Unidade Da Compra.',
                    "modalidade_id.validarcompra"=>'Modalidade Da Licitação.',
                    "licitacao_numero.validarcompra"=>'Número Da Licitação.',
                ];
    }

    /**
     *
     * Método responsável em recuperar a compra no SIASG
     *
     * @param string $modalidade
     * @param string $numeroAno
     * @param string $uasgCompra
     * @param string $uasgUsuario
     * @return mixed
     */
    public function buscaCompraSiasgTrait(
        string $modalidade,
        string $numeroAno,
        string $uasgCompra,
        string $uasgUsuario
    ) {
        $apiSiasg = new ApiSiasg();

        $params = [
            'modalidade' => $modalidade,
            'numeroAno' => $numeroAno,
            'uasgCompra' => $uasgCompra,
            'uasgUsuario' => $uasgUsuario
        ];

        return json_decode($apiSiasg->executaConsulta('COMPRASISPP', $params));
    }

    /**
     * Método que recebe os dados da compra buscada e verifica se o retorno tem alguma mensagem de erro
     * pros serviços SIASG e NDC
     *
     * @param Object $retornoSiasg
     * @return false|string
     */
    public function getErroMensagemSiasgNDC($retornoSiasg){
        if ($retornoSiasg->MensagemErroNDC && $retornoSiasg->MensagemErroSiasg) {
            // Função para garantir que a mensagem tenha apenas um ponto final
            $retornoSiasg->MensagemErroSiasg = rtrim($retornoSiasg->MensagemErroSiasg, '.') . '.';
            $retornoSiasg->MensagemErroNDC = rtrim($retornoSiasg->MensagemErroNDC, '.') . '.';

            $mensagemErroCompra = "NDC: " . $retornoSiasg->MensagemErroNDC .
                "<br />SIASG: " . $retornoSiasg->MensagemErroSiasg;

            return $mensagemErroCompra;
        }

        return false;
    }
}
