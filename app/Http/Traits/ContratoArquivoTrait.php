<?php

namespace App\Http\Traits;

use App\Models\Codigoitem;
use App\Models\Contrato;
use App\Models\Contratoarquivo;
use App\Models\MinutaEmpenho;
use App\Models\Contratohistorico;
use Illuminate\Support\Facades\Event;



trait ContratoArquivoTrait
{
    public function salvaArquivoEmpenhoComoArquivoContrato(MinutaEmpenho $minuta, Contrato $contrato, $naoEvieAoPNCP = null) : bool
    {

        $dados_empenho = [
            'ugemitente' => $minuta->saldo_contabil->unidade_id()->first()->codigo,
            'anoempenho' => substr($minuta->mensagem_siafi, 0, 4),
            'numempenho' => (int)substr($minuta->mensagem_siafi, 6, 6),
        ];

        //recuperar caminho do arquivo do empenho
        $caminhoArquivoEmpenho = MinutaEmpenho::existeArquivoMinutaEmpenho($dados_empenho, true);

        //todo evoluir depois para que aceite tipo 'Nota de Empenho'
        /*$tipoContratoArquivo =  Codigoitem::whereHas('codigo', function ($q) {
            $q->where('descricao', 'Tipo Arquivos Contrato');
        })
            ->where('descricao', 'Nota de Empenho')
            ->first()->id;*/

        //recupera id do tipo de arquivo = contrato
        $tipoContratoArquivo =  Codigoitem::whereHas('codigo', function ($q) {
            $q->where('descricao', 'Tipo Arquivos Contrato');
        })
            ->where('descricao', 'Contrato')
            ->first()->id;

        //grava no banco
        $anexos = new Contratoarquivo();
        $anexos->contrato_id = $contrato->id;
        $anexos->tipo = $tipoContratoArquivo;
        $anexos->descricao = "Empenho " . $minuta->mensagem_siafi;
        $anexos->sequencial_documento = null;
        $anexos->arquivos = $caminhoArquivoEmpenho;
        $anexos->origem = "0";
        $anexos->contratohistorico_id = $contrato->historico()->first()->id;
        $anexos->restrito = false;

        //caso não seja para enviar arquivo(não é 14.133), impedir na observer de enviar
        $naoEnviarArquivo = $naoEvieAoPNCP;
        if ($naoEnviarArquivo) {
            //salve o arquivo sem acionar a observer ContratoArquivoObserver
            Contratoarquivo::withoutEvents(function() use ($anexos){
                $anexos->save();
            });
            return true;
        }

        if ($anexos->save()) {
            return true;
        }
        return false;
    }
}
