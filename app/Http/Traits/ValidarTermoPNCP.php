<?php
namespace App\Http\Traits;

use App\Http\Controllers\Admin\EnviaPNCPController;
use App\Http\Controllers\Api\PNCP\UsuarioController;
use App\Models\Codigoitem;
use App\Models\Contratoarquivo;
use App\Models\Contratohistorico;
use App\Models\ContratoMinuta;
use App\Models\EnviaDadosPncp;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

trait ValidarTermoPNCP
{
    private $termosValidos =  ['Termo de Rescisão','Termo Aditivo','Termo de Apostilamento','Termo de Encerramento'];

    public function campoEmBranco(int $idTipoDocumento,  ?string $valor) {

        $tipoDocumento = Codigoitem::find($idTipoDocumento);

        if(in_array($tipoDocumento->descricao,$this->termosValidos)) {

            if(empty($valor))  {
                return true;
            }

        }
        return false;
    }

    public function retornarDadosContratoHistorico(string $tipoTermo, ?string $numero, int $contratoId)
    {
        $idCodigoItem = 0;

        switch ($tipoTermo) {

            case 'Termo de Rescisão' :
                $idCodigoItem = 191;
                break;

            case 'Termo Aditivo':
                $idCodigoItem = 65;
                break;

            case 'Termo de Apostilamento':
                $idCodigoItem = 68;
                break;

            case 'Termo Encerramento':
                break;
        }

        return Contratohistorico::where([
            "tipo_id" => $idCodigoItem
            , 'numero' => $numero
            , 'contrato_id' => $contratoId
            , 'contratohistorico.elaboracao' => false
        ])->first();

    }
    public function validarPorTipoTermo(string $tipoTermo, ?string $numero, int $contratoId) {

        $validacaoDocumento = $this->retornarDadosContratoHistorico($tipoTermo, $numero, $contratoId);

        if( !empty($validacaoDocumento) ) {
            return true;
        }

        return false;

    }

    public function validarNumeroTermoContrato( Contratoarquivo $contratoarquivo, ContratoMinuta $contratoMinuta) {

        if(in_array($contratoMinuta->descricao, $this->termosValidos)) {
            return $this->validarPorTipoTermo($contratoMinuta->descricao,$contratoMinuta->numero_termo_contrato,$contratoarquivo->contrato_id);
        }

    }

    public function prepararArquivoPNCPTermos(ContratoMinuta $minuta_documento,Contratoarquivo $arquivo_minuta ) {

        $dadosContratoHistorico = $this->retornarDadosContratoHistorico($minuta_documento['descricao'],$minuta_documento['numero_termo_contrato'],$minuta_documento['contrato_id']);
        $sequencialPNCP = EnviaDadosPncp::where('pncpable_id',$dadosContratoHistorico->id)->orderBy('id')->first();

        //Realiza o envio para o PNCP
        $enviaDadosPNCP = EnviaDadosPncp::where("pncpable_id",$dadosContratoHistorico->id)->first();

        if(empty($enviaDadosPNCP->sequencialPNCP)) {

            //realizar o login para o envio no PNCP
            $uc = new UsuarioController();
            $uc->login();

            $ep = new EnviaPNCPController();
            $ep->enviaUnidade($sequencialPNCP['id']);

        }

        if( !empty($dadosContratoHistorico) ) {

            $arquivo_minuta->contratohistorico_id = $dadosContratoHistorico->id;

            $enviaDadosPNCP = EnviaDadosPncp::where("pncpable_id",$dadosContratoHistorico->id)->first();
            $arquivo_minuta->sequencial_pncp = $enviaDadosPNCP->sequencialPNCP;

            $arquivo_minuta->update();

            if($this->validarNumeroTermoContrato($arquivo_minuta,$minuta_documento)) {

                EnviaDadosPncp::create([
                    'pncpable_type' => Contratohistorico::class,
                    'contrato_id' => $arquivo_minuta->contrato_id,
                    'situacao' => Codigoitem::whereHas('codigo', function ($q) {
                        $q->where('descricao', 'Situação Envia Dados PNCP');
                    })->where('descres', 'ARQPEN')->first()->id,
                    'tipo_contrato' => $minuta_documento['descricao'],
                    'pncpable_id' => $arquivo_minuta->contratohistorico_id,
                    'sequencialPNCP' => $sequencialPNCP['sequencialPNCP']
                ]);

                try{
                    $arquivo_minuta->salvaTempDocumentoSEI();
                }catch(Exception $e){
                    Log::error($e->getMessage());
                }
            }
        }
    }
}
