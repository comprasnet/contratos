<?php

namespace App\Http\Traits;

use App\Models\Catmatsergrupo;
use App\Models\Codigo;
use App\Models\Codigoitem;

trait BuscaCodigoItens
{
    public function retornaArrayCodigosItens($descCodigo)
    {
        return Codigoitem::whereHas('codigo', function ($query) use ($descCodigo) {
            $query->where('descricao', '=', $descCodigo)
                ->whereNull('deleted_at');
        })
            ->whereNull('deleted_at')
            ->orderBy('descricao')
            ->pluck('descricao', 'id')
            ->toArray();
    }

    /*
     * Retorna array no formato [descres => descricao] da Codigoitem
     */
    public function retornaArrayDescresDescricaoCodigoItens($descCodigo)
    {
        return Codigoitem::whereHas('codigo', function ($query) use ($descCodigo) {
            $query->where('descricao', '=', $descCodigo)
                ->whereNull('deleted_at');
        })
            ->whereNull('deleted_at')
            ->orderBy('descricao')
            ->pluck('descricao', 'descres')
            ->toArray();
    }

    public function retornaDescCodigoItem($id)
    {
        return Codigoitem::where('id', $id)
            ->select('descricao')->first()->descricao;
    }

    public function retornaDescresPorId($id)
    {
        return Codigoitem::where('id', $id)
            ->select('descres')->first()->descres;
    }

    public function retornaDescricaoPorDescres($descres)
    {
        $descricao = Codigoitem::where('descres', $descres)
            ->select('descricao')->first();
        return $descricao->descricao ?? null;
    }

    public function retornaDescricaoCodigoItemPorId($id)
    {
        return Codigoitem::where('id', $id)
            ->select('descricao')->first()->descricao;
    }

    public function retornaIdCodigoItem($descCodigo, $descCodItem)
    {
        return Codigoitem::whereHas('codigo', function ($query) use ($descCodigo) {
            $query->where('descricao', '=', $descCodigo)
                ->whereNull('deleted_at');
        })
            ->whereNull('deleted_at')
            ->where('descricao', '=', $descCodItem)
            ->first()->id;
    }

    public function retornaIdCodigoItemPorDescres($descres, $descricao)
    {
        $codigo_item = Codigoitem::whereHas('codigo', function ($query) use ($descricao) {
            $query->where('descricao', '=', $descricao)
                ->whereNull('deleted_at');
        })
            ->whereNull('deleted_at')
            ->where('descres', '=', $descres)
            ->first();
        return $codigo_item->id ?? null;
    }

    public function retornaDescresCodigoItem($descCodigo, $descCodItem)
    {
        return Codigoitem::whereHas('codigo', function ($query) use ($descCodigo) {
            $query->where('descricao', '=', $descCodigo)
                ->whereNull('deleted_at');
        })
            ->whereNull('deleted_at')
            ->where('descricao', '=', $descCodItem)
            ->first()->descres;
    }

    public function retornaIdCodigoItemPorDescricaoEDescres($descricaoCodigo, $descresCodItem)
    {
        return Codigoitem::whereHas('codigo', function ($query) use ($descricaoCodigo) {
            $query->where('descricao', '=', $descricaoCodigo)
                ->whereNull('deleted_at');
        })
            ->whereNull('deleted_at')
            ->where('descres', '=', $descresCodItem)
            ->first()->id;
    }

    public function retornaIdsCodigoItemPorDescricaoEDescresArray($descricaoCodigo, $descresCodItem)
    {
        return Codigoitem::whereHas('codigo', function ($query) use ($descricaoCodigo) {
            $query->where('descricao', '=', $descricaoCodigo)
                ->whereNull('deleted_at');
        })
            ->whereNull('deleted_at')
            ->whereIn('descres', $descresCodItem)
            ->get()->pluck('id');
    }

    public function retornaodigoItemPorDescricaoEDescres( $descricaoCodigo )
    {
        return Codigoitem::whereHas('codigo', function ($query) use ($descricaoCodigo) {
            $query->where('descricao', '=', $descricaoCodigo)
                ->whereNull('deleted_at');
        })
            ->whereNull('deleted_at')
            ->get();
    }

    public function retornaIdCatMatSerGrupo($descCodigo)
    {
        return Catmatsergrupo::whereNull('deleted_at')
            ->where('descricao', '=', $descCodigo)
            ->first()->id;
    }

    public function retornaArrayCodigosItensNotInDescres($arrayDescres, $descricao)
    {
        return Codigoitem::whereHas('codigo', function ($query) use ($descricao) {
            $query->where('descricao', '=', $descricao)
                ->whereNull('deleted_at');
        })
            ->whereNull('deleted_at')
            ->whereNotIn('descres', $arrayDescres)
            ->orderBy('descricao')
            ->pluck('descricao', 'id')
            ->toArray();
    }

    public function getCodigoItemByDescres($codigoDescres, $codigoItemDescres) {
        return Codigoitem::whereHas('codigo', function ($q) use ($codigoDescres) {
            $q->where('descricao', $codigoDescres);
        })
            ->where('descricao', $codigoItemDescres)
            ->first();
    }

}
