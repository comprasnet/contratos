<?php

namespace App\Http\Traits;

use Carbon\Carbon;

trait UsuarioAcessoGovBrTrait
{
    public function usuarioPertenceGrupoAdministradorDesenvolvedor()
    {
        $ambienteLiberado = ['Ambiente Treinamento'];

        if (in_array(config('app.app_amb'), $ambienteLiberado)) {
            return true;
        }

        $user = backpack_user();

        if (empty($user)) {
            return false;
        }

        $roles = $user->getRoleNames()->toArray();

        $perfilPodeAcessar = ['Administrador', 'Desenvolvedor'];

        if (!empty(array_intersect($roles, $perfilPodeAcessar))) {
            return true;
        }

        return false;
    }

    public function retornarNomeRotaLoginUsuario(bool $perfilAdministrador)
    {
        if ($perfilAdministrador) {
            return route('backpack.auth.login.backdoor');
        }

        return route('backpack.auth.login');
    }

    public function retornarRotaLoginUsuario()
    {
        $usuarioPerfilAdministrador = $this->usuarioPertenceGrupoAdministradorDesenvolvedor();

        return $this->retornarNomeRotaLoginUsuario($usuarioPerfilAdministrador);
    }

    public function acessarSomenteGov()
    {
        $dataAcessoSomenteGov = config('acessogov.data_acesso_somente_gov');
        $ambiente = config('app.app_amb');

        $dataLimite = Carbon::parse($dataAcessoSomenteGov);
        $ambienteLiberado = $this->ambienteLiberadoLayoutNovo();

        if ($dataLimite->lessThanOrEqualTo(Carbon::now()) && in_array($ambiente, $ambienteLiberado)) {
            return true;
        }

        return false;
    }

    public function mensagemErroSenhaUsuario()
    {
        $urlGovBr = route('acessogov.autorizacao');
        $acessoSomenteLoginGov = $this->acessarSomenteGov();

        $mensagemFailed = 'Essas credenciais não foram encontradas em nossos registros';
        if ($acessoSomenteLoginGov) {
            $mensagemFailed = "Entre com seu <a href='{$urlGovBr}'>gov.br</a>";
        }

        return $mensagemFailed;
    }

    public function ambienteLiberadoLayoutNovo()
    {
        return ['Ambiente Produção', 'Ambiente Homologação'];
    }
}
