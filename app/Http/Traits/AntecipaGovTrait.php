<?php

namespace App\Http\Traits;

use App\Models\AntecipaGov;
use App\Models\DomicilioBancario;
use Exception;
use Illuminate\Support\Facades\DB;

trait AntecipaGovTrait
{
    use BuscaCodigoItens;

    private $retornoAcao = [ 'operation'=> null, 'code' => 200, 'message' =>  'Sucesso'];

    private function montarMensagem(string $operacao, string $mensagem, int $codigo = 200) {

        $this->retornoAcao['code'] = $codigo;
        $this->retornoAcao['operation'] = $operacao;
        $this->retornoAcao['message'] = $mensagem;

        return $this->retornoAcao;
    }

    private function montarTextoSucesso(string $operacao,int $numOperacao) {
        switch($operacao) {
            case 'Registrado':
                return "Registro de operação {$numOperacao} persistido na base do comprasnet com sucesso";
            break;
            case 'Liquidado':
                return "Liquidação referente a {$numOperacao} persistido na base do comprasnet com sucesso";
            break;
            case 'Cancelado':
                return "Cancelamento referente a {$numOperacao} persistido na base do comprasnet com sucesso";
            break;
            case 'Alterado':
                return "Alteração do domicilio bancário referente a operação {$numOperacao} persistido na base do comprasnet com sucesso";
            break;
        }
    }

    private function validarStatusOperacaoNaUrl(string $statusOperacao, string $acaoUrl) {
        if($statusOperacao == 'REGISTRAR' && $acaoUrl == 'Registrado') {
            return true;
        }

        if($statusOperacao == 'LIQUIDAR' && $acaoUrl == 'Liquidado') {
            return true;
        }

        if($statusOperacao == 'CANCELAR' && $acaoUrl == 'Cancelado') {
            return true;
        }

        if($statusOperacao == 'ALTERAR' && $acaoUrl == 'Alterado') {
            return true;
        }

        return false;

    }

    private function validarRequest(array $params, string $descCodItem) {

        if(!$this->validarStatusOperacaoNaUrl($params['status_operacao'], $descCodItem)) {
            $mensagem = "Operação {$params['status_operacao']} não autorizada.";
            return $this->montarMensagem($params['status_operacao'],$mensagem, 412);
        }

        $validation = AntecipaGov::where( "status_operacao", $params['status_operacao'])
                                ->where( "identificador_unico", $params['identificador_unico']  )
                                ->get()
                                ->count();

        if($validation > 0 ) {
            $mensagem = " Existe identificador duplicado [{$params['identificador_unico']}], não persistido na base do comprasgov; ";
            return $this->montarMensagem($params['status_operacao'],$mensagem, 412);
        }

        return $this->retornoAcao;

    }

    private function salvarRequest(array $params, array $retornoAcao, string $descCodItem) {

        if( $retornoAcao['code'] != 200 ) {
            return $retornoAcao;
        }

        DB::beginTransaction();

        $situacao_registrado = $this->retornaIdCodigoItem('Situacao Antecipa Gov', $descCodItem);

        $params['situacao_id'] = $situacao_registrado;

        try {

            $domicio_bancario = DomicilioBancario::updateOrCreate(
                [
                    'banco' => $params['num_banco'],
                    'conta' => $params['conta_bancaria'],
                    'agencia' => $params['num_agencia'],
                ],
                [
                    'antecipa_gov' => true,
                ]
            );

           $antecipagov = $domicio_bancario->antecipagovs()->create($params);
           $antecipagov->contratos()->attach($params['id_contrato']);

           DB::commit();
           $mensagem = $this->montarTextoSucesso($descCodItem,$params['num_operacao']);
           return $this->montarMensagem($params['status_operacao'], $mensagem);
        //    $this->retornoAcao['operation'] = $params['status_operacao'];

        //    return $this->retornoAcao ;

        } catch (Exception $e) {
            DB::rollback();
            $mensagem = "Não persistido na base do comprasgov: {$e->getMessage()}";
            return $this->montarMensagem($params['status_operacao'], $mensagem, 500);

        }

    }

}
