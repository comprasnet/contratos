<?php

namespace App\Http\Traits;

use Illuminate\Support\Facades\Redirect;

trait Authorizes{
    public function authorizePermissions(array $permissions){
        $user = backpack_user();
        $result = false;
        foreach ($permissions as $permission){ //edit cliente, permission
            $hasPermission = $user->hasPermissionTo($permission); //3
            if($hasPermission){
                $result = true;
                break;
            }
        }
        if(!$result){
            abort('403');
        }
    }

    public static function bloquearInformacaoPHP() {

        if (config('app.app_amb') == 'Ambiente Produção') {
            return true;
        }

        return false;
    }
}
