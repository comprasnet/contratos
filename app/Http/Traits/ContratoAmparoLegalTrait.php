<?php

namespace App\Http\Traits;

use App\Models\AmparoLegal;

trait ContratoAmparoLegalTrait
{
    public function validarAmparoLegalContrato(string $amparoLegal, string $atoNormativo='14.133/2021') {

        $arrayIdAtoNormativo = AmparoLegal::select('id')->whereNull('deleted_at')->where('ato_normativo','LIKE',"%{$atoNormativo}%")->pluck('id')->toArray();

        $arrayIdAmparoLegal = AmparoLegal::select('id')->whereNull('deleted_at')->where('ato_normativo','LIKE',"%{$amparoLegal}%")->pluck('id')->toArray();

        foreach($arrayIdAmparoLegal as $amparoLegal) {

            if (in_array($amparoLegal,$arrayIdAtoNormativo)) {
                return true;
            }
        }

        return false;
    }
}