<?php

namespace App\Http\Traits;

use Exception;

trait NonceTrait
{
    /**
     * Através da rota, utiliza o mapeamento e obtém a descrição do model
     */
    public function model()
    {
        # Utiliza a rota para obter a descrição do model
        $model = $this->getActionInfo(request()->route()->getAction());
        return $model['nonceable_type'];
    }

    /**
     * Baseada na rota que foi chamada, retorna informações sobre a funcionalidade e onde está acontecendo
     * Ex.: Funcionalidade: 'Cadastrar empenho' - Model: 'App\Models\Empenho'
     * @return string
     */
    public function functionality(): string
    {
        # Utiliza a rota para obter a funcionalidade
        $action = $this->getActionInfo(request()->route()->getAction());
        return $action['funcionalidade'];
    }

    /**
     * Retorna dados referente à rota requisitada para salvar nas tabelas relacionadas ao nonce
     *
     * @param $action
     * @return array|null[]
     */
    private function getActionInfo()
    {
        $action = request()->route()->getAction();
        if (isset($action['controller'])) {

            # Divide o controller e o método // Exemplo: App\Http\Controllers\EmpenhoApiController@store
            [$controller, $function] = explode('@', $action['controller']);

            # Obtém somente o nome do controller
            $controller = explode("\\", $controller);
            $class = end($controller);

            # Se o controller estiver mapeado, retorna as informações (nonceable_type e funcionalidade)
            $actionInfo = config('constants.actionInfo');
            if (isset($actionInfo[$class])) {
                return [
                    'nonceable_type' => $actionInfo[$class]['nonceable_type'] ?? null,
                    'funcionalidade' => $actionInfo[$class]['funcionalidades'][$function] ?? null,
                ];
            }
        }

        return [
            'nonceable_type' => null,
            'funcionalidade' => null
        ];
    }

    /**
     * Se a exception foi gerada explicitamente, sugere-se já incluir a palavra "Erro:"
     * Desta forma, o retorno de erro gerado será apenas a mensagem que o desenvolvedor especificou
     * Já se o retorno for desconhecido, será retornada a mensagem informando também a linha e o arquivo onde ocorreu
     *
     * @param Exception $e
     * @return string
     */
    public function formatExceptionMessage(Exception $e)
    {
        # Se a mensagem contém "Onde:", retorna apenas ela
        if (strpos($e->getMessage(), 'Erro:') === 0) {
            return $e->getMessage();
        }
        # Se não, retorna a mensagem de erro padrão na exceção,
        # a qual incluirá também a mensagem recebida na segunda exceção
        return "Erro: {$e->getMessage()}, na linha {$e->getLine()}, no arquivo {$e->getFile()}";
    }

}
