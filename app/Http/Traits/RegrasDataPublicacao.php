<?php

namespace App\Http\Traits;

use App\Models\Codigoitem;
use App\Rules\NaoAceitarFeriado;
use App\Rules\NaoAceitarFimDeSemana;

trait RegrasDataPublicacao
{
    public function ruleDataPublicacao($tipo_id = null, $id = null, $lei14 = false, $aditivo = false)
    {
        $data_atual = date('Y-m-d');
        $arrCodigoItens = Codigoitem::whereHas('codigo', function ($query) {
            $query->where('descricao', '=', 'Tipo de Contrato');
        })
            ->where('descricao', '<>', 'Outros')
            ->where('descricao', '<>', 'Empenho')
            ->orderBy('descricao')
            ->pluck('id')
            ->toArray();
        $retorno = [
            'date',
            'after_or_equal:data_assinatura',
            'nullable'
        ];

        if (!$lei14) {
            $retorno = array_values($retorno);
            $retorno[] = 'required_without:elaboracao' ;
            $retorno[] = 'required_if:elaboracao,0,false';
        }

        if (in_array($tipo_id, $arrCodigoItens)) {
            $retorno[] = new NaoAceitarFeriado();
            $retorno[] = new NaoAceitarFimDeSemana();
        }
        return $retorno;

    }
}
