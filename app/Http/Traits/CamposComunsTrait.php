<?php

namespace App\Http\Traits;

/**
 * Trait criada com o propósito de reutilizar campos comuns em qualquer crudController que necessite
 *
 */
trait CamposComunsTrait
{

    /**
     * Adiciona campo de busca de compra
     *
     * @return void
     */
    protected function adicionaCampoItensCompra(): void
   {
       $this->addModalField([
           'label' => 'itens da Compra novo',
           'type' => 'select2_from_ajax_api_compra',
           'name' => 'itens_compra',
           'attribute' => 'descricaodetalhada',
           'data_source' => url('api/retornaritenscompra'),
           'placeholder' => 'Selecione itens',
           'minimum_input_length' => 0,
           'modal_inserir_item_compra' => true,
           'wrapperAttributes' => [
               'class' => 'form-group'
           ]
       ]);
   }

}
