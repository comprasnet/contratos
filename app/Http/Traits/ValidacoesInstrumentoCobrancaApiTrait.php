<?php

namespace App\Http\Traits;

use App\Helpers\ErrorMessageApropriarApi;
use App\Models\Contratofatura;
use App\Models\Execsfsituacao;
use Normalizer;
use App\Http\Traits\ContratoFaturaTrait;
use App\Models\ApropriacaoContratoFaturas;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

trait ValidacoesInstrumentoCobrancaApiTrait
{
    use ContratoFaturaTrait;

    public $contratosDiferentesMessage = 'Instrumentos de cobrança não estão vinculado ao mesmo contrato.';
    public $faturasSituacaoInvalidaMessage = 'Status da apropriação não permite inclusão.';
    public $faturasSituacaParaEditarInvalidaMessage = 'Status da apropriação não permite edição.';
    public $exitsFaturasInvalidaMessage = 'Instrumento de cobrança não encontrada';
    public $faturasApropriadaInvalidaMessage = 'Fatura apropriada.';
    public $exitsApropriacaoInvalidaMessage = 'Instrumento de cobrança não encontrada';
    public $campoMascaraNaoAtendeEsperadoMessage = 'Campo %s não atende ao esperado, verificar a máscara.';
    public $tipoDhInvalidMessage = 'Tipo DH informado não existe.';

    public $campo_nome;

    public function validaAntesApropriarApiTrait(Request $request, ...$campos)
    {
        $this->validarExistenciaFaturas($request->id_inst_cobranca);
        $this->validaContratoUnico($request->id_inst_cobranca);
        $this->validaSituacaoContratos($request->id_inst_cobranca);
        $this->validaFaturaApropriada($request->id_inst_cobranca);
        $this->validaMascara($request->input("sf_pco.0.cod_situacao"), ...$campos);
        $this->validaTipoDH($request->tipo_dh);
    }

    protected function validarExistenciaFaturas($faturaIds)
    {
        $numFaturasEncontradas = Contratofatura::whereIn('id', $faturaIds)->count();

        if ($numFaturasEncontradas !== count($faturaIds)) {
            throw new \Exception($this->exitsFaturasInvalidaMessage);
        }
    }

    public function validaContratoUnico($faturaIds)
    {
        $contratoCount = Contratofatura::whereIn('id', $faturaIds)
            ->pluck('contrato_id')
            ->unique()
            ->count();

        if ($contratoCount !== 1) {
            throw new \Exception($this->contratosDiferentesMessage);
        }
    }

    public function validaSituacaoContratos($faturaIds)
    {
        $count = Contratofatura::whereIn('id', $faturaIds)
            ->where(function ($query) {
                $query->whereNotIn('situacao', ['PEN', 'CAN']);
            })
            ->count();

        if ($count > 0) {
            throw new \Exception($this->faturasSituacaoInvalidaMessage);
        }
    }

    public function validaFaturaApropriada($faturaIds)
    {
        $count = Contratofatura::whereIn('id', $faturaIds)
            ->where(function ($query) {
                $query->where('situacao', ['APR']);
            })
            ->count();

        if ($count > 0) {
            throw new \Exception($this->faturasSituacaoInvalidaMessage);
        }
    }

    protected function validaTipoDH($tipoDH)
    {
        if ($tipoDH !== 'NP' && $tipoDH !== 'RP') {
            throw new \Exception($this->tipoDhInvalidMessage);
        }
    }

    public function mapearErroParaMensagem($tipoErro)
    {
        $mensagensErro = [
            $this->contratosDiferentesMessage => 'Instrumentos de cobrança não estão vinculado ao mesmo contrato.',
            $this->faturasSituacaoInvalidaMessage => 'Status da apropriação não permite inclusão.',
            $this->faturasSituacaParaEditarInvalidaMessage => 'Status da apropriação não permite edição.',
            $this->exitsFaturasInvalidaMessage => 'Instrumento de cobrança não encontrada',
            $this->faturasApropriadaInvalidaMessage => 'Fatura apropriada.',
            $this->exitsApropriacaoInvalidaMessage => 'Instrumento de cobrança não encontrada',
            $this->tipoDhInvalidMessage => 'Tipo DH informado não existe.',
            sprintf($this->campoMascaraNaoAtendeEsperadoMessage, $this->campo_nome) => sprintf('Campo %s não atende ao esperado, verificar a máscara.', $this->campo_nome)
        ];
        return $mensagensErro[$tipoErro] ?? 'Erro desconhecido';
    }


    public function validaMascara($situacao, ...$campos)
    {
        foreach ($campos as $campo) {
            $this->campo_nome = $campo['nome'];

            $resultado = $this->getCampoResultado($situacao, $campo['nome']);
            if (isset($resultado)) {
                $this->comparaCampoMascara($campo['nome'], $campo['valor'], $resultado->mascara);
            }
        }
    }

    protected function getCampoResultado($situacao, $campo)
    {
        return Execsfsituacao::join('execsfsituacao_campos_variaveis', function ($join) use ($campo) {
            $join->on('execsfsituacao.id', '=', 'execsfsituacao_campos_variaveis.execsfsituacao_id')
                ->where('execsfsituacao_campos_variaveis.campo', '=', $campo);
        })
            ->where('execsfsituacao.codigo', '=', $situacao)
            ->select('execsfsituacao_campos_variaveis.mascara')
            ->first();
    }

    function comparaCampoMascara($campo_nome, $campo_variavel, $resultado)
    {
        // Transforma as strings em arrays de caracteres
        $campo_array = str_split($campo_variavel);
        $resultado_array = str_split($resultado);

        // Substitui os caracteres 'X' no $resultado pelos caracteres correspondentes no $campo_variavel
        $novoResultadoArray = array_map(function ($res, $campo) {
            return $res === 'X' ? $campo : $res;
        }, $resultado_array, $campo_array);

        $novoResultadoString = implode('', $novoResultadoArray);

        // Normaliza as strings antes de comparar
        $campo_variavel_normalized = Normalizer::normalize($campo_variavel);
        $novoResultadoString_normalized = Normalizer::normalize($novoResultadoString);

        if ($novoResultadoString_normalized !== $campo_variavel_normalized) {
            $mensagem = sprintf($this->campoMascaraNaoAtendeEsperadoMessage, $campo_nome);
            throw new \Exception($mensagem);
        }
    }

    public function verificaPermissoes($faturaId)
    {
        if (!$this->verificaPermissaoSistema()) {

            return response()->json([
                'status' => 'error',
                'message' => ErrorMessageApropriarApi::getErrorMessage('sem_permissao_sistema')
            ], ErrorMessageApropriarApi::getHttpErrorCode('sem_permissao_sistema'));
        };

        if (!$this->verificaPermissaoContrato($faturaId)) {

            return response()->json([
                'status' => 'error',
                'message' => ErrorMessageApropriarApi::getErrorMessage('sem_permissao_contrato')
            ], ErrorMessageApropriarApi::getHttpErrorCode('sem_permissao_contrato'));
        };
    }


    public function verificaPermissaoSistema()
    {
        //$permissoesNecessarias = collect(['Acesso API', 'Execução Financeira']);
        $permissoesNecessarias = collect(['Execução Financeira']);
        $isAdmin = backpack_user()->hasRole(['Administrador']);
        $hasTodasPermissoes = $permissoesNecessarias->every(function ($permissao) {
            return backpack_user()->hasRole($permissao);
        });

        return $isAdmin || $hasTodasPermissoes;
    }

    public function verificaPermissaoContrato($faturaId)
    {
        $contrato_fatura = Contratofatura::where('id', $faturaId)->first();

        if ($contrato_fatura) {
            $hasUserUasgPermission = $this->verificaUnidadeContratoFaturaAPI($contrato_fatura, backpack_user());

            if (!$hasUserUasgPermission) {
                return false;
            }
            return true;
        }
    }

    public function validaAntesEditarApropriacaoApiTrait($faturaId, $situacao, ...$campos)
    {
        $this->validaSituacaoParaEditar($faturaId);
        $this->validaMascara($situacao, ...$campos);
    }

    public function validaSituacaoParaEditar($faturaId)
    {
        $count = Contratofatura::where('id', $faturaId)
            ->where(function ($query) {
                $query->whereNotIn('situacao', ['AND', 'ERR']);
            })
            ->count();

        if ($count > 0) {
            throw new \Exception($this->faturasSituacaParaEditarInvalidaMessage);
        }
    }

    protected function validarExistenciaApropriacao($id_apropriacao_inst_cobranca)
    {
        // Verifica se os IDs das faturas existem na tabela Contratofatura
        $existenciaApropriacao = ApropriacaoContratoFaturas::where('apropriacoes_faturas_id', $id_apropriacao_inst_cobranca)
            ->count();

        if ($existenciaApropriacao == 0) {
            throw new \Exception($this->exitsApropriacaoInvalidaMessage);
        }
    }
}
