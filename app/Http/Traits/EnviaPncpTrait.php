<?php

namespace App\Http\Traits;

use App\Http\Controllers\Admin\EnviaPNCPController;
use App\Http\Controllers\Api\PNCP\ContratoControllerPNCP;
use App\Http\Controllers\Api\PNCP\DocumentoContratoController;
use App\Http\Controllers\Api\PNCP\DocumentoTermoContratoController;
use App\Http\Controllers\Api\PNCP\TermoContratoController;
use App\Http\Controllers\Api\PNCP\UnidadeController;
use App\Http\Controllers\Api\PNCP\UsuarioController;
use App\Http\Controllers\Empenho\CompraSiasgCrudController;
use App\Http\Traits\ConsultaCompra;
use App\Models\Codigo;
use App\Models\Codigoitem;
use App\Models\Compra;
use App\Models\Contrato;
use App\Models\Contratoarquivo;
use App\Models\Contratohistorico;
use App\Models\ContratoMinuta;
use App\Models\ContratoMinutaEmpenhoPivot;
use App\Models\EnviaDadosPncp;
use App\Models\MinutaEmpenho;
use App\services\AmparoLegalService;
use App\services\PNCP\PncpService;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

trait EnviaPncpTrait
{
    use LogTrait;
    use ConsultaCompra;
    use CompraTrait;
    use Formatador;
    use BuscaCodigoItens;

    public function formatarCnpjOrgao(string $sequencialPNCP)
    {
        try {
            return explode('-', explode('/', $sequencialPNCP)[0])[0];
        } catch (Exception $e) {
            return null;
        }
    }

    public function formatarAno(string $sequencialPNCP)
    {
        if (empty($sequencialPNCP)) return null;
        try {
            return explode('/', $sequencialPNCP)[1];
        } catch (Exception $e) {
            $this->inserirLogPncp("Não foi possível obter o ano do sequencial: '{$sequencialPNCP}'",
                __METHOD__, __LINE__);
            return null;
        }
    }

    public function getAnoDoSequencialPncpDoContratoPai($sequencialPncp)
    {
        if (empty($sequencialPncp)) return null;
        $partes = explode('/', $sequencialPncp);
        return isset($partes[1]) ? $partes[1] : null;
    }

    public function formatarSequencialPncp($sequencialPNCP)
    {
        try {
            return explode('-', explode('/', $sequencialPNCP)[0])[2];
        } catch (Exception $e) {
            return null;
        }
    }

    public function recuperarIDSituacao(string $status)
    {
        return Codigoitem::whereHas('codigo', function ($q) {
            $q->where('descricao', 'Situação Envia Dados PNCP');
        })->where('descres', $status)->first()->id;
    }

    public function realizarLoginPNCP(UsuarioController $uc)
    {
        return $uc->login();
    }

    public function insereContrato(
        EnviaDadosPncp                   $linhaPncpDoContrato,
        ContratoControllerPNCP           $cc,
        DocumentoContratoController      $dcc,
        DocumentoTermoContratoController $dtcc,
        CompraSiasgCrudController        $cscc)
    {
        try {
            $contrato = Contratohistorico::where('contratohistorico.elaboracao', false)
                ->find($linhaPncpDoContrato->pncpable_id);
            if (empty($contrato) || $contrato == null) {
                return false;
            }
            if (!(isset($contrato->modalidade))) {
                return false;
            }
            $dadosMinutaPncp = $this->recuperarDadosContratoPNCP($contrato, $cc);
            if (empty($dadosMinutaPncp)) {
                $this->inserirContratoPNCP($contrato, $linhaPncpDoContrato, $cc, $dcc, $dtcc, $cscc);
            } else {
                $this->atualizarInformacaoPNCPContratoMinuta($contrato);
            }
            return true;
        } catch (Exception $e) {
            $this->registraErroNaTabelaEnviaDadosPncp($linhaPncpDoContrato, false, $e);
            #throw $e;
            return false;
        }
    }

    public function inserirTermoContratoPNCP(EnviaDadosPncp                   $linhaDoTermoDeContrato,
                                             TermoContratoController          $tcc,
                                             DocumentoContratoController      $dcc,
                                             DocumentoTermoContratoController $dtcc)
    {
        try {
            $mensagemDeErroPadrao = "Erro ao tentar inserir um termo de contrato. ";
            $cc = new ContratoControllerPNCP();
            $termo = Contratohistorico::find($linhaDoTermoDeContrato->pncpable_id);

            # Adiciona o array com os IDs das qualificações vindas no request,
            # pois quando o método serializaTermoPNCP é chamado, as qualificações ainda não foram salvas no banco,
            # assim o "${objeto contratohistorico}->qualificacoes" retorna vazio
            $termo->idQualificacoes = (array)request('qualificacoes', []);

            if (empty($termo)) {
                $errorMessage = $mensagemDeErroPadrao .
                    "Termo de contrato de ID {$linhaDoTermoDeContrato->pncpable_id} " .
                    "não encontrado na tabela contratohistorico. " .
                    "ID do Contrato: {$linhaDoTermoDeContrato->contrato_id}. " .
                    "Linha PNCP: {$linhaDoTermoDeContrato->id}.";
                $this->inserirLogPncp($errorMessage, __METHOD__, __LINE__);
                $this->alteraSituacaoParaAnalise($linhaDoTermoDeContrato, $errorMessage);
                return false;
            }
            # Se o termo está em elaboração, não dá continuidade no envio
            if ($termo->elaboracao === true) {
                return null;
            }
            # Busca o contrato 'pai' do termo
            $linhaPncpContratoPaiDoTermo = EnviaDadosPncp::withTrashed()
                ->where([
                    ['contrato_id', $termo->contrato_id],
                    ['pncpable_type', Contratohistorico::class]
                ])
                ->whereIn('tipo_contrato', config('api-pncp.tipos_contrato_aceitos_pncp'))
                ->oldest()
                ->first();
            # Se o registro do contrato 'pai' foi excluído, exclui também o registro do termo
            if (empty($linhaPncpContratoPaiDoTermo) || $linhaPncpContratoPaiDoTermo->deleted_at !== null) {
                $linhaDoTermoDeContrato->delete();
                $errorMessage = "A linha PNCP {$linhaDoTermoDeContrato->id} " .
                    "referente ao termo de contrato {$linhaDoTermoDeContrato->pncpable_id} foi excluída " .
                    "pois o contrato pai {$linhaDoTermoDeContrato->contrato_id} " .
                    "também está excluído na tabela envia_dados_pncp.";
                $this->inserirLogPncp($errorMessage, __METHOD__, __LINE__);
                return null;
            }
            # Se o contrato 'pai' ainda está como INCPEN, não adianta tentar enviar o termo para o PNCP
            # Neste caso, o envio do termo será feito na próxima execução da rotina automática,
            # caso o contrato já tenha sido enviado
            if ($linhaPncpContratoPaiDoTermo->situacao == $this->recuperarIDSituacao('INCPEN')) {
                return false;
            }
            $dadosContratoPaiParaLogDeErro =
                "ID da linha PNPC do Contrato 'pai': {$linhaPncpContratoPaiDoTermo->id}. " .
                "ID da linha PNCP do termo: {$linhaDoTermoDeContrato->id}. " .
                "ID do Contrato: {$termo->contrato_id}. " .
                "ID do Termo: {$termo->id}";
            if (empty($linhaPncpContratoPaiDoTermo->sequencialPNCP)) {
                $errorMessage = $mensagemDeErroPadrao . "Não foi possível obter o sequencial do contrato 'pai'. " .
                    $dadosContratoPaiParaLogDeErro;
                $this->inserirLogPncp($errorMessage, __METHOD__, __LINE__);
                $this->alteraSituacaoParaAnalise($linhaDoTermoDeContrato, $errorMessage, true);
                return null;
            }
            $cnpjOrgao = $this->formatarCnpjOrgao($linhaPncpContratoPaiDoTermo->sequencialPNCP);
            if (empty($cnpjOrgao)) {
                $errorMessage = $mensagemDeErroPadrao .
                    "Não foi possível obter o CNPJ do órgão através do sequencialPNPC do contrato 'pai'. " .
                    $dadosContratoPaiParaLogDeErro;
                $this->inserirLogPncp($errorMessage, __METHOD__, __LINE__);
                $this->alteraSituacaoParaAnalise($linhaDoTermoDeContrato, $errorMessage, true);
                return false;
            }
            $ano = $this->formatarAno($linhaPncpContratoPaiDoTermo->sequencialPNCP);
            if (empty($ano)) {
                $errorMessage = $mensagemDeErroPadrao .
                    "Não foi possível obter o ano através do sequencialPNPC do contrato 'pai'. " .
                    $dadosContratoPaiParaLogDeErro;
                $this->inserirLogPncp($errorMessage, __METHOD__, __LINE__);
                $this->alteraSituacaoParaAnalise($linhaDoTermoDeContrato, $errorMessage, true);
                return false;
            }
            $sequencialPNCP = $this->formatarSequencialPncp($linhaPncpContratoPaiDoTermo->sequencialPNCP);
            if (empty($sequencialPNCP)) {
                $errorMessage = $mensagemDeErroPadrao .
                    "Não foi possível obter o número sequencial PNCP do Contrato 'pai'. " .
                    $dadosContratoPaiParaLogDeErro;
                $this->inserirLogPncp($errorMessage, __METHOD__, __LINE__);
                $this->alteraSituacaoParaAnalise($linhaDoTermoDeContrato, $errorMessage, true);
                return false;
            }
            $termoEnviado = $tcc->inserirTermoContrato(
                $termo,
                $cnpjOrgao,
                $ano,
                $sequencialPNCP,
                $linhaDoTermoDeContrato
            );
            # Se retornar true, o termo já existia no PNCP e o registro na envia_dados_pncp foi atualizado
            if ($termoEnviado === true) {
                return true;
            }
            # Se retornar um response
            if (is_object($termoEnviado)) {
                $enviaArquivos = $this->trataRetorno($linhaDoTermoDeContrato,
                    $termoEnviado,
                    false,
                    'sequencialTermoContrato', $cc);
                if ($enviaArquivos) {
                    $this->enviaContratoArquivos(EnviaDadosPncp::find($termo->id), $dcc, $dtcc);
                }
            }

        } catch (Exception $e) {
            $errorMessage = $e->getMessage() .
                "\r\nLinha PNCP: " . $linhaDoTermoDeContrato .
                "\r\nTermo de contrato: " . $termo;
            $this->inserirLogPncp(
                $mensagemDeErroPadrao . $errorMessage,
                __METHOD__,
                __LINE__);
        }
    }

    public function recuperarInformacoesMinutaEnviada(EnviaDadosPncp $enviaDadosPNCP)
    {
        //Se o contrato for criado primeiro que a assinatura do empenho sera atualizado o registro do PNCP
        if (empty($enviaDadosPNCP->sequencialPNCP)) {
            $contratoHistoricoEmpenho = Contratohistorico::where('contratohistorico.elaboracao', false)
                ->find($enviaDadosPNCP->pncpable_id);
            $this->atualizarInformacaoPNCPContratoMinuta($contratoHistoricoEmpenho, true);
        }
        $recuperarEnviaPNCPAtualizado = EnviaDadosPncp::find($enviaDadosPNCP->id);
        return $recuperarEnviaPNCPAtualizado;
    }

    public function validarEnviarContratoTipoEmpenho(EnviaDadosPncp $enviaDadosPNCP)
    {
        if (empty($enviaDadosPNCP->sequencialPNCP)) {
            return $this->recuperarInformacoesMinutaEnviada($enviaDadosPNCP);
        }
        return $enviaDadosPNCP;
    }

    public function montarComplementoRetificacaoContratoTipoEmpenho(Contratohistorico $contrato)
    {

        if ($contrato->tipo->descricao == 'Empenho' && count($contrato->minutasempenho) > 0) {
            $contrato->codigoUnidade = $contrato->unidadeorigem->codigo;
            $contrato->informacaoComplementar = $contrato->minutasempenho[0]->informacao_complementar;
        }

        return $contrato;
    }

    public function recuperarDadosContratoPNCP(Contratohistorico $contrato, ContratoControllerPNCP $cc)
    {

        if ($contrato->tipo->descricao == 'Empenho' && count($contrato->minutasempenho) > 0) {
            $recuperarMinutaEmpenho = EnviaDadosPncp::where("pncpable_id", $contrato->minutasempenho[0]->id)->first();

            if (empty($recuperarMinutaEmpenho->sequencialPNCP)) {
                return null;
            }

            $cnpj = $this->formatarCnpjOrgao($recuperarMinutaEmpenho->sequencialPNCP);

            $ano = $this->formatarAno($recuperarMinutaEmpenho->sequencialPNCP);

            $sequencial = $this->formatarSequencialPncp($recuperarMinutaEmpenho->sequencialPNCP);

            try {
                return $cc->consultarContrato($cnpj, $ano, $sequencial);
            } catch (Exception $e) {
                return null;
            }
        }
    }

    public function atualizarInformacaoPNCPContratoMinuta(Contratohistorico $contrato, ?bool $manterStatus = false)
    {
        if (empty($contrato->minutasempenho->first())) {
            return false;
        }
        $recuperarMinutaEmpenhoPNCP =
            EnviaDadosPncp::where("pncpable_id", $contrato->minutasempenho[0]->id)
                ->where('pncpable_type', MinutaEmpenho::class)
                ->first();

        $recuperarContratoTipoEmpenho = EnviaDadosPncp::where("contrato_id", $contrato->contrato_id)->first();

        if (empty($recuperarMinutaEmpenhoPNCP->sequencialPNCP)) {
            return false;
        }

        $recuperarContratoTipoEmpenho->sequencialPNCP = $recuperarMinutaEmpenhoPNCP->sequencialPNCP;
        $recuperarContratoTipoEmpenho->link_pncp = $recuperarMinutaEmpenhoPNCP->link_pncp;

        if (!$manterStatus) {
            $recuperarContratoTipoEmpenho->situacao = $recuperarMinutaEmpenhoPNCP->situacao;
        }

        $recuperarContratoTipoEmpenho->json_enviado_inclusao = $recuperarMinutaEmpenhoPNCP->json_enviado_inclusao;

        $recuperarContratoTipoEmpenho->save();

        $contrato->minutasempenho[0]->minutaempenho_forcacontrato = 1;
        $contrato->minutasempenho[0]->save();

        return true;
    }

    public function inserirContratoPNCP(
        Contratohistorico                $contrato,
        EnviaDadosPncp                   $linhaPncpDoContrato,
        ContratoControllerPNCP           $cc,
        DocumentoContratoController      $dcc,
        DocumentoTermoContratoController $dtcc,
        CompraSiasgCrudController        $cscc
    )
    {
        $sispp = $this->buscaCNPJContratanteNovo(
            $contrato->modalidade->descres,
            $contrato->licitacao_numero,
            $contrato->unidadecompra->codigo,
            $contrato->unidade->codigo
        );
        # Se a não encontrar a compra no SIASG
        if (isset($sispp->codigoRetorno) && $sispp->codigoRetorno != 200
            && ($sispp->codigoRetorno == 204 || empty($sispp->data))) {
            $linhaPncpDoContrato->retorno_pncp = $sispp->messagem;
            $linhaPncpDoContrato->save();
            # Se a compra não for encontrada, então deletamos logicamente para não ser mais enviada
            if ($sispp->messagem === 'Compra não encontrada') {
                $linhaPncpDoContrato->delete();
            }
            return;
        }
        $cnpjOrgao = $this->cnpjOrgao($contrato);
        if (empty($cnpjOrgao)) {
            $mensagemPNCP = "O CNPJ do orgão de ID {$contrato->unidadeorigem->orgao->id} está em branco, favor inserir
            o CNPJ válido. {$linhaPncpDoContrato->retorno_pncp}";
            $linhaPncpDoContrato->retorno_pncp = $mensagemPNCP;
            $linhaPncpDoContrato->save();
            return;
            //Caso queiram atualizar a informação do CNPJ do orgão na base de dados
            // com a informação do CNPJ do orgão da compra, descomentar as linhas abaixo
            /*if (!empty($sispp) && !empty($sispp->cnpjOrgao)) {
                $cnpjOrgao = $sispp->cnpjOrgao;
                $contrato->unidadeorigem->orgao->cnpj = $cnpjOrgao;
                $contrato->unidadeorigem->orgao->save();
            }*/
        }
        $this->unidadeExistePNCP($cnpjOrgao, $contrato->unidade->codigo);
        $contratoEnviado = $cc->inserirContrato(
            $contrato,
            $sispp->cnpjOrgao,
            $sispp->idUnico,
            $cnpjOrgao,
            $linhaPncpDoContrato
        );
        # Se a rotina devolve a variável tratarRetorno e ela está falsa,
        # indica que não é para fazer o tratamento, ou seja, o código pode retornar
        if (isset($contratoEnviado->tratarRetorno) && $contratoEnviado->tratarRetorno === false) {
            return true;
        }
        $enviaArquivos = $this->trataRetorno(
            $linhaPncpDoContrato,
            $contratoEnviado,
            false,
            'numeroControlePNCP',
            $cc);
        if ($enviaArquivos) {
            $this->enviaContratoArquivos(EnviaDadosPncp::find($linhaPncpDoContrato->id), $dcc, $dtcc);
        }
    }

    private function recuperarAnoPorTipoContrato(Contratohistorico $contrato, EnviaDadosPncp $envia_pncp)
    {
        if ($envia_pncp->tipo_contrato == 'Empenho') {
            if (empty($envia_pncp->sequencialPNCP)) {
                $this->inserirLogPncp(
                    "Tentativa de obter o ano através do sequencial PNCP mas o sequencial está vazio. " .
                    "ID do Contrato: {$envia_pncp->contrato_id}. " .
                    "ID do Contrato histórico: {$envia_pncp->pncpable_id}. " .
                    "Linha PNCP: {$envia_pncp->id}",
                    __METHOD__, __LINE__
                );
                return false;
            }
            return $this->formatarAno($envia_pncp->sequencialPNCP);
        } else {
            if (strpos($contrato->numero, '/') !== false) {
                return substr($contrato->numero, -4);
            } else {
                return substr($contrato->numero, 0, 4);
            }
        }
    }

    public function incluirArquivoPNCPPorContrato(
        Contratohistorico                $contrato,
        EnviaDadosPncp                   $envia_pncp,
        Contratoarquivo                  $arquivo,
        DocumentoContratoController      $dcc,
        DocumentoTermoContratoController $dtcc
    )
    {
        $mensagemDeErroPadrao = "Tentativa de enviar o arquivo {$arquivo->id} para o PNCP, ";
        $cnpjOrgao = $this->cnpjOrgao($contrato);
        $ano = $this->recuperarAnoPorTipoContrato($contrato, $envia_pncp);
        if (empty($ano)) {
            $errorMessage = $mensagemDeErroPadrao . "mas não foi possível obter o ano. " .
                "ID do Contrato: {$envia_pncp->contrato_id}. " .
                "ID do Contrato histórico: {$envia_pncp->pncpable_id}). " .
                "Linha PNCP: {$envia_pncp->id}";
            # Se não encontrou o ano, há algum problema no registro. Neste caso, mantém o registro Em análise.
            $this->alteraSituacaoParaAnalise($envia_pncp, $errorMessage);
            $this->inserirLogPncp($errorMessage, __METHOD__, __LINE__);
            return false;
        }

        $isTermoDeContrato = Contrato::isTermoDeContrato($envia_pncp->tipo_contrato);
        $sequencialTermoPNCP = null;

        if ($isTermoDeContrato) {
            $linhaPncpContratoPaiDoTermo = EnviaDadosPncp::where([
                ['contrato_id', '=', $envia_pncp->contrato_id],
                ['pncpable_type', Contratohistorico::class],
            ])->oldest()->first();
            if (empty($linhaPncpContratoPaiDoTermo) || empty($linhaPncpContratoPaiDoTermo->sequencialPNCP)) {
                $errorMessage = $mensagemDeErroPadrao .
                    "mas o Contrato (id: {$envia_pncp->contrato_id}) está sem o sequencial. " .
                    "Linha PNCP: {$linhaPncpContratoPaiDoTermo->id}";
                $this->alteraSituacaoParaAnalise($envia_pncp, $errorMessage);
                $this->inserirLogPncp($errorMessage, __METHOD__, __LINE__);
                return false;
            }
            $sequencialPNCP = $this->formatarSequencialPncp($linhaPncpContratoPaiDoTermo->sequencialPNCP);
            $sequencialTermoPNCP = (empty($envia_pncp->sequencialPNCP)) ?
                $arquivo->sequencial_pncp :
                $envia_pncp->sequencialPNCP;

            if (empty($sequencialTermoPNCP)) {
                $errorMessage = $mensagemDeErroPadrao .
                    "mas o Termo de Contrato (id: {$envia_pncp->pncpable_id}) está sem o sequencial. " .
                    "Linha PNCP: {$envia_pncp->id}";
                $this->alteraSituacaoParaAnalise($envia_pncp, $errorMessage);
                $this->inserirLogPncp($errorMessage, __METHOD__, __LINE__);
                return false;
            }
            $ano = $this->getAnoDoSequencialPncpDoContratoPai($linhaPncpContratoPaiDoTermo->sequencialPNCP);
        } else {
            $sequencialPNCP = $this->formatarSequencialPncp($envia_pncp->sequencialPNCP);
            if (empty($sequencialPNCP)) {
                $errorMessage = $mensagemDeErroPadrao .
                    "mas o Contrato (id: {$envia_pncp->pncpable_id}) está sem o sequencial. " .
                    "Linha PNCP: {$envia_pncp->id}";
                $this->alteraSituacaoParaAnalise($envia_pncp, $errorMessage);
                $this->inserirLogPncp($errorMessage, __METHOD__, __LINE__);
                return false;
            }
        }
        $tipoEnvio = $arquivo->envio_pncp_pendente;
        $response = null;

        switch ($tipoEnvio) {
            case 'INCARQ': # Inclusao de Arquivo
                $response = $this->inserirDocumento(
                    $isTermoDeContrato,
                    $arquivo,
                    $cnpjOrgao,
                    $ano,
                    $sequencialPNCP,
                    $sequencialTermoPNCP,
                    $dcc,
                    $dtcc
                );
                if ($response->code != 200 && $response->code != 201) {
                    # Se o problema for falha na autenticação do PNCP, indica que o envio ainda é pendente
                    if ($response->code == 403) {
                        $estadoArquivo = [
                            'envio_pncp_pendente' => $tipoEnvio,
                        ];
                    } else { # Qualquer outro erro, registra o erro junto do arquivo
                        $estadoArquivo = [
                            'envio_pncp_pendente' => null,
                            'retorno_pncp' => $response->message
                        ];
                    }
                } else {
                    $estadoArquivo = [
                        'envio_pncp_pendente' => null,
                        'retorno_pncp' => 'SUCESSO',
                        'link_pncp' => $response->linkPncp,
                        'sequencial_pncp' => $this->getIdDoArquivoPelaUrl($response->linkPncp)
                    ];
                }
                $this->atualizaEstadoArquivo($arquivo, $estadoArquivo);
                return $response;
                break;
            case 'ATUARQ': // Atualização do Arquivo
                $exclusao = $this->excluirDocumento(
                    $isTermoDeContrato,
                    $arquivo,
                    $cnpjOrgao,
                    $ano,
                    $sequencialPNCP,
                    $sequencialTermoPNCP,
                    $dcc,
                    $dtcc
                );

                if (strpos($exclusao->message, 'Documento não encontrado.') !== false) {
                    $this->atualizaEstadoArquivo($arquivo, ['envio_pncp_pendente' => 'INCARQ']);
                    return;
                }

                $response = $this->inserirDocumento(
                    $isTermoDeContrato,
                    $arquivo,
                    $cnpjOrgao,
                    $ano,
                    $sequencialPNCP,
                    $sequencialTermoPNCP,
                    $dcc,
                    $dtcc
                );

                if ($response->code == 201) {
                    $estadoArquivo = [
                        'envio_pncp_pendente' => null,
                        'retorno_pncp' => null,
                        'link_pncp' => $response->linkPncp,
                        'sequencial_pncp' => $this->getIdDoArquivoPelaUrl($response->linkPncp)
                    ];
                    $this->atualizaEstadoArquivo($arquivo, $estadoArquivo);
                }
                break;
            case 'DELARQ': # Deletar o arquivo
                $response = null;
                $excluidoComErro = false;
                $estadoArquivo = [
                    'envio_pncp_pendente' => null,
                    'sequencial_pncp' => null
                ];

                if ($arquivo->sequencial_pncp) {
                    $response = $this->excluirDocumento(
                        $isTermoDeContrato,
                        $arquivo,
                        $cnpjOrgao,
                        $ano,
                        $sequencialPNCP,
                        $sequencialTermoPNCP,
                        $dcc,
                        $dtcc
                    );
                } else {
                    $response = (object)[
                        'code' => 400,
                        'message' => 'O registro do arquivo estava sem sequencial PNCP no sistema.'
                    ];
                    $excluidoComErro = true;
                }

                if (strpos($response->message, 'Documento não encontrado') !== false) {
                    $response->message = 'O arquivo com este sequencial já não existia mais no PNPC.';
                    $excluidoComErro = true;
                }

                if ($response->code < 400 || $excluidoComErro) {
                    $response->code = 200;
                    $response->message = 'Arquivo excluído com sucesso.';
                    if ($excluidoComErro) {
                        $response->message .= ' ' . $response->message;
                    }
                } else if ($response->code == 403) {
                    $estadoArquivo['envio_pncp_pendente'] = $tipoEnvio;
                }
                $estadoArquivo['retorno_pncp'] = $response->message;
                $this->atualizaEstadoArquivo($arquivo, $estadoArquivo);
                return $response;
                break;
            default:
                break;
        }
    }

    /**
     * Insere um documento no PNCP, dependendo do tipo de contrato.
     *
     * Este método verifica se o documento a ser inserido é um contrato ou um termo de contrato e, em seguida,
     * chama o método apropriado no controlador correspondente para realizar a inserção.
     *
     * @param bool $isTermoDeContrato Indica se o documento é um termo de contrato (true) ou um contrato (false).
     * @param mixed $arquivo Objeto que representa o arquivo a ser inserido.
     * @param string $cnpjOrgao CNPJ do órgão responsável pelo contrato.
     * @param int $ano Ano de referência do contrato.
     * @param int $sequencialPNCP Sequencial do PNCP associado ao contrato.
     * @param int|null $sequencialTermoPNCP (Opcional) Sequencial do PNCP específico para o termo de contrato.
     * @param DocumentoContratoController $dcc Controlador responsável pela inserção de contratos simples.
     * @param DocumentoTermoContratoController $dtcc Controlador responsável pela inserção de termos de contrato.
     *
     * @return mixed O resultado da inserção do documento, retornado pelo método chamado.
     */
    private function inserirDocumento(
        $isTermoDeContrato,
        $arquivo,
        $cnpjOrgao,
        $ano,
        $sequencialPNCP,
        $sequencialTermoPNCP,
        DocumentoContratoController $dcc,
        DocumentoTermoContratoController $dtcc)
    {
        if ($isTermoDeContrato) {
            return $dtcc->inserirDocumentoTermoContrato(
                $arquivo,
                $cnpjOrgao,
                $ano,
                $sequencialPNCP,
                $sequencialTermoPNCP
            );
        } else {
            return $dcc->inserirDocumentoContrato(
                $arquivo,
                $cnpjOrgao,
                $ano,
                $sequencialPNCP
            );
        }
    }

    /**
     * Exclui um documento do PNCP, dependendo do tipo de contrato.
     *
     * Este método verifica se o documento a ser excluído é um contrato ou um termo de contrato e, em seguida,
     * chama o método apropriado no controlador correspondente para realizar a exclusão.
     *
     * @param bool $isTermoDeContrato Indica se o documento é um termo de contrato (true) ou um contrato (false).
     * @param mixed $arquivo Objeto que representa o arquivo a ser excluído, incluindo seu sequencial PNCP.
     * @param string $cnpjOrgao CNPJ do órgão responsável pelo contrato.
     * @param int $ano Ano de referência do contrato.
     * @param int $sequencialPNCP Sequencial do PNCP associado ao contrato.
     * @param int|null $sequencialTermoPNCP (Opcional) Sequencial do PNCP específico para o termo de contrato.
     * @param DocumentoContratoController $dcc Controlador responsável pela exclusão de contratos simples.
     * @param DocumentoTermoContratoController $dtcc Controlador responsável pela exclusão de termos de contrato.
     *
     * @return mixed O resultado da exclusão do documento, retornado pelo método chamado.
     */
    private function excluirDocumento(
        $isTermoDeContrato,
        $arquivo,
        $cnpjOrgao,
        $ano,
        $sequencialPNCP,
        $sequencialTermoPNCP,
        DocumentoContratoController $dcc,
        DocumentoTermoContratoController $dtcc
    )
    {
        if ($isTermoDeContrato) {
            return $dtcc->excluirDocumentoTermoContrato(
                $arquivo,
                $cnpjOrgao,
                $ano,
                $sequencialPNCP,
                $sequencialTermoPNCP,
                $arquivo->sequencial_pncp
            );
        } else {
            return $dcc->excluirDocumentoContrato(
                $arquivo,
                $cnpjOrgao,
                $ano,
                $sequencialPNCP,
                $arquivo->sequencial_pncp
            );
        }
    }

    public function atualizarContratoPNCP(
        EnviaDadosPncp                   $linhaPncpDoContrato,
        ContratoControllerPNCP           $cc,
        DocumentoContratoController      $dcc,
        DocumentoTermoContratoController $dtcc,
        CompraSiasgCrudController        $cscc
    )
    {
        $mensagemDeErroPadrao = "Erro ao tentar atualizar(retificar) um contrato. ";
        $contrato = Contratohistorico::find($linhaPncpDoContrato->pncpable_id);
        if (empty($contrato)) {
            $errorMessage = $mensagemDeErroPadrao . "Contrato de ID {$linhaPncpDoContrato->pncpable_id} " .
                "não encontrado na tabela contratohistorico. " .
                "Linha PNCP: {$linhaPncpDoContrato->id}.";
            $this->alteraSituacaoParaAnalise($linhaPncpDoContrato, $errorMessage);
            $this->inserirLogPncp($errorMessage, __METHOD__, __LINE__);
            return false;
        }
        if ($contrato->elaboracao === true) {
            return false;
        }
        $contratoEnviado = null;
//        $sispp = $this->buscaCNPJContratante(
//            $contrato->modalidade->descres,
//            $contrato->unidadecompra->codigo,
//            $contrato->unidade->codigo,
//            str_replace('/', '', $contrato->licitacao_numero)
//        );
        $sispp = $this->buscaCNPJContratanteNovo(
            $contrato->modalidade->descres,
            $contrato->licitacao_numero,
            $contrato->unidadecompra->codigo,
            $contrato->unidade->codigo
        );
        $dadosDoContratoParaLog = "ID do Contrato: {$linhaPncpDoContrato->contrato_id}. " .
            "ID do Contrato histórico: {$linhaPncpDoContrato->pncpable_id}). " .
            "Linha PNCP: {$linhaPncpDoContrato->id}";
        if (empty($sispp) || empty($sispp->cnpjOrgao)) {
            $errorMessage = $mensagemDeErroPadrao .
                "Não foi possível obter o CNPJ do contratante. " .
                $dadosDoContratoParaLog;
            # Se não encontrou o CNPJ, há algum problema no registro. Neste caso, mantém o registro Em análise.
            $this->alteraSituacaoParaAnalise($linhaPncpDoContrato, $errorMessage);
            $this->inserirLogPncp($errorMessage, __METHOD__, __LINE__);
            return false;
        }
        $cnpjOrgao = $this->cnpjOrgao($contrato);
        if (empty($cnpjOrgao)) {
            $errorMessage = $mensagemDeErroPadrao .
                "Não foi possível obter o CNPJ do orgão. " .
                $dadosDoContratoParaLog;
            # Se não encontrou o CNPJ, há algum problema no registro. Neste caso, mantém o registro Em análise.
            $this->alteraSituacaoParaAnalise($linhaPncpDoContrato, $errorMessage);
            $this->inserirLogPncp($errorMessage, __METHOD__, __LINE__);
            return false;
        }
        $ano = $this->recuperarAnoPorTipoContrato($contrato, $linhaPncpDoContrato);
        if (empty($ano)) {
            $errorMessage = $mensagemDeErroPadrao . "Não foi possível obter o ano. " . $dadosDoContratoParaLog;
            # Se não encontrou o ano, há algum problema no registro. Neste caso, mantém o registro Em análise.
            # O 'if' abaixo é feito em função do legado: qdo tipo do contrato era alterado, a alteração não
            # refletia na tabela envia_dados_pncp.
            # Neste caso, não move para análise, pois a issue #1503 fará a correção.
            if ($contrato->tipo->descricao == $linhaPncpDoContrato->situacao) {
                $this->alteraSituacaoParaAnalise($linhaPncpDoContrato, $errorMessage);
                $this->inserirLogPncp($errorMessage, __METHOD__, __LINE__);
            } else {
                # Só vai entrar aqui quando tiver o problema de legado.
                # Neste caso, registra o erro, mas mantém a situação.
                $this->registraErroNaTabelaEnviaDadosPncp($linhaPncpDoContrato, true,
                    'O tipo do contrato na tabela envia_dados_pncp ' .
                    'está diferente do tipo do contrato no cadastro.');
            }
            return false;
        }
        $sequencialPNCP = $this->formatarSequencialPncp($linhaPncpDoContrato->sequencialPNCP);
        if (empty($sequencialPNCP)) {
            $errorMessage = $mensagemDeErroPadrao .
                "Não foi possível obter o sequencial do contrato " .
                $dadosDoContratoParaLog;
            $this->alteraSituacaoParaAnalise($linhaPncpDoContrato, $errorMessage);
            $this->inserirLogPncp($errorMessage, __METHOD__, __LINE__);
            return false;
        }
        $contrato = $this->montarComplementoRetificacaoContratoTipoEmpenho($contrato);
        $contratoEnviado = $cc->retificarContrato($contrato,
            $sispp->cnpjOrgao, $sispp->idUnico, $ano, $cnpjOrgao, $sequencialPNCP, $linhaPncpDoContrato);
        # Se a rotina devolver a variável tratarRetorno e ela estiver falsa,
        # indica que houve um erro no retorno do PNCP e já foi tratado, por isso não precisa tratar aqui
        # e nem deve continuar o fluxo
        if (isset($contratoEnviado->tratarRetorno) && $contratoEnviado->tratarRetorno === false) {
            return true;
        }
        # Se o retorno não foi um response válido, retorna
        if (!is_object($contratoEnviado) || !method_exists($contratoEnviado, 'getStatusCode')) {
            return true;
        }
        $enviaArquivos = $this->trataRetorno($linhaPncpDoContrato, $contratoEnviado, true, '', $cc);
        if ($enviaArquivos) {
            $this->enviaContratoArquivos(EnviaDadosPncp::find($linhaPncpDoContrato->id), $dcc, $dtcc);
        }
        return true;
    }

    public function enviarSuperSEIArquivoPNCP(Contratohistorico $contratoHistorico)
    {

        $enviadoDadosContrato = EnviaDadosPncp::where("contrato_id", $contratoHistorico->contrato_id)
            ->where("tipo_contrato", "Contrato")
            ->first();

        // Log::channel('schedule')->info("--------- enviadoDadosContrato:".$enviadoDadosContrato." ---------");

        if (empty($enviadoDadosContrato->sequencialPNCP)) {
            return;
        }

        $arquivoSuperSei = Contratoarquivo::where("contrato_id", $contratoHistorico->contrato_id)
            ->where("origem", "1")
            ->whereNull("sequencial_pncp")
            ->get();

        // Log::channel('schedule')->info("--------- arquivoSuperSei:".$arquivoSuperSei." ---------");

        foreach ($arquivoSuperSei as $arquivo) {
            // Log::channel('schedule')->info("--------- tipo:".$contratoHistorico->getTipo()." ---------");
            $minutaDocumento = ContratoMinuta::join('codigoitens', "codigoitens.id", "=", "codigoitens_id")
                //    ->where("codigoitens.codigo_id",51)
                ->where("link_sei", $arquivo->link_sei)
                ->where("contrato_id", $contratoHistorico->contrato_id)
                ->where("codigoitens.descricao", $contratoHistorico->getTipo())
                ->select("contrato_minutas.*", "codigoitens.descricao")
                ->get();

            // Log::channel('schedule')->info("--------- minutaDocumento:".$minutaDocumento." ---------");

            foreach ($minutaDocumento as $minuta) {
                // Log::channel('schedule')->info("--------- numero_termo_contrato:".$minuta->numero_termo_contrato." ---------");
                // Log::channel('schedule')->info("--------- numero:".$contratoHistorico->numero." ---------");
                if ($minuta->numero_termo_contrato == $contratoHistorico->numero) {
                    $this->prepararArquivoPNCPTermos($minuta, $arquivo);
                }
            }
        }
    }

    /**
     * Faz o controle do envio para o pncp na tabela envia_dados_pncp.
     *
     * @param int $contratohistoricoId ID do ContratoHistorico.
     * @param int $contratoId ID do Contrato.
     * @param string $tipo O tipo do Contrato
     * @param array $minutasEmpenho O array de IDs das Minuta de empenho vinculadas ao contrato
     * @return bool Retorna true se os dados já foram enviados e false caso ainda precise enviar
     */
    public function preparaEnvioPNCP($contratohistoricoId, $contratoId, $tipo, $minutaEmpenhoId)
    {
        if (session('editando_contrato_elaboracao')) return;
        $pncp = EnviaDadosPncp::where([
            ['contrato_id', '=', $contratoId],
            ['pncpable_id', '=', $contratohistoricoId],
        ])->first();
        if (isset($pncp)) {
            # Busca o tipo do contrato para atualizar a linha do PNPC
            $tipoContrato = Contratohistorico::select('codigoitens.descricao')
                ->leftJoin('codigoitens',
                    'contratohistorico.tipo_id', '=', 'codigoitens.id')
                ->find($contratohistoricoId);
            $descricaoContrato = $tipoContrato->descricao;
            if ($descricaoContrato != $pncp->tipo_contrato) {
                $pncp->tipo_contrato = $descricaoContrato;
                $pncp->save();
            }
            # Identifica que está fazendo uma atualização (linha pncp já existe e tem sequencial)
            if (isset($pncp->sequencialPNCP)) {
                $pncp->situacao = $this->recuperarIDSituacao('RETPEN');
                $pncp->save();
                return false;
            }
            if ($pncp->situacao == $this->recuperarIDSituacao('INCPEN')) {
                return false;
            }
            Log::error(__METHOD__ . ' (' . __LINE__ . ') ' . json_encode($pncp->toArray()));
            return true;
        }
        # Se tiver minuta vinculada e o contrato for do tipo Empenho,
        # busca os dados da minuta na tabela envia_dados_pncp
        # para atribuir ao registro do contrato na tabela envia_dados_pncp
        if ($minutaEmpenhoId && $tipo == 'Empenho') {
            $dadosDaMinutaEnviadaParaPncp = EnviaDadosPncp::where([
                ['pncpable_id', $minutaEmpenhoId],
                ['situacao', $this->recuperarIDSituacao('SUCESSO')],
            ])->first();
        }
        if (!empty($dadosDaMinutaEnviadaParaPncp)) {
            $situacao = $this->recuperarIDSituacao('SUCESSO');
            $jsonEnviadoInclusao = $dadosDaMinutaEnviadaParaPncp->json_enviado_inclusao;
            $jsonEnviadoAlteracao = $dadosDaMinutaEnviadaParaPncp->json_enviado_alteracao;
            $retornoPncp = $dadosDaMinutaEnviadaParaPncp->retorno_pncp;
            $link_pncp = $dadosDaMinutaEnviadaParaPncp->link_pncp;
            $sequencialPNCP = $dadosDaMinutaEnviadaParaPncp->sequencialPNCP;
            $linkArquivoEmpenho = $dadosDaMinutaEnviadaParaPncp->linkArquivoEmpenho;
            $envioRealizado = true;
        } else {
            $situacao = $this->recuperarIDSituacao('INCPEN');
            $jsonEnviadoInclusao = null;
            $jsonEnviadoAlteracao = null;
            $retornoPncp = null;
            $link_pncp = null;
            $sequencialPNCP = null;
            $linkArquivoEmpenho = null;
            $envioRealizado = false;
        }
        EnviaDadosPncp::create([
            'pncpable_type' => Contratohistorico::class,
            'contrato_id' => $contratoId,
            'situacao' => $situacao,
            'json_enviado_inclusao' => $jsonEnviadoInclusao,
            'json_enviado_alteracao' => $jsonEnviadoAlteracao,
            'retorno_pncp' => $retornoPncp,
            'tipo_contrato' => $tipo,
            'pncpable_id' => $contratohistoricoId,
            'link_pncp' => $link_pncp,
            'sequencialPNCP' => $sequencialPNCP,
            'linkArquivoEmpenho' => $linkArquivoEmpenho,
        ]);
        return $envioRealizado;
    }

    public function preparaExclusaoPNCP($contratoId, $contratohistoricoId)
    {
        $pncp = EnviaDadosPncp::where([
            ['contrato_id', '=', $contratoId],
            ['pncpable_id', '=', $contratohistoricoId],
        ])->first();
        if (isset($pncp)) {
            if (isset($pncp->sequencialPNCP)) {
                $pncp->situacao = Codigoitem::whereHas('codigo', function ($q) {
                    $q->where('descricao', 'Situação Envia Dados PNCP');
                })->where('descres', 'EXCPEN')->first()->id;
                $pncp->save();
            } else {
                $pncp->delete();
            }
        }
    }


    //Caso o usuário tenha inserido o amparo 14133 no cadastro e depois removeu
    public function verificaPNCP($contratoId, $contratohistoricoId)
    {
        //Log::channel('schedule')->info('Entrou em verificaPncp');
        $pncp = EnviaDadosPncp::where([
            ['contrato_id', '=', $contratoId],
            ['pncpable_id', '=', $contratohistoricoId],
        ])->first();
        if (!isset($pncp)) {
            //Log::channel('schedule')->info("Variável de controle do PNCP não foi encontrada, nenhuma tarefa será realizada");
            return;
        } else {
            //Log::channel('schedule')->info("Variável de controle do PNCP FOI encontrada");
            //Se tem sequencial, é por que ja está no PNCP
            if (isset($pncp->sequencialPNCP)) {
                //Log::channel('schedule')->info("O sequencial foi preenchido, preparando exclusão no PNCP");
                $this->preparaExclusaoPNCP($contratoId, $contratohistoricoId);
            } else {
                //Log::channel('schedule')->info("O sequencial não foi preenchido, a variavel de controle será excluida");
                //Log::channel('schedule')->info("====================Excluido sem EXCPEN=====================");
                $pncp->delete();
            }
        }
    }

    public function enviaContratoArquivos(EnviaDadosPncp                   $linhaPncpContratoOuTermo,
                                          DocumentoContratoController      $dcc,
                                          DocumentoTermoContratoController $dtcc)
    {
        $contrato = Contratohistorico::where('contratohistorico.elaboracao', false)
            ->where('contrato_id', '=', $linhaPncpContratoOuTermo->contrato_id)
            ->first();
        $arquivosComEnvioPendente = $this->getArquivosComEnvioPendente($linhaPncpContratoOuTermo->pncpable_id);
        foreach ($arquivosComEnvioPendente as $arquivo) {
            $this->incluirArquivoPNCPPorContrato($contrato, $linhaPncpContratoOuTermo, $arquivo, $dcc, $dtcc);
        }
        # Agora é preciso verificar se ainda existem arquivos com envio pendente (do contrato)
        # para fazer o controle na tabela envia_dados_pncp
        $this->atualizarSituacaoEnviaDadosPncpComBaseEmArquivosPendentes($linhaPncpContratoOuTermo);
    }

    private function atualizarSituacaoEnviaDadosPncpPeloIdContratoArquivo($idContratoArquivo)
    {
        $pncpService = new PncpService();
        $enviaDadosPncp = $pncpService->getEnviaDadosPncpPeloIdDoContratoArquivo($idContratoArquivo);
        if ($enviaDadosPncp != null) {
            $this->atualizarSituacaoEnviaDadosPncpComBaseEmArquivosPendentes($enviaDadosPncp);
        }
    }

    public function atualizarSituacaoEnviaDadosPncpComBaseEmArquivosPendentes($enviaDadosPncp)
    {
        # Se o contrato/termo 'pai' do arquivo já está como EXCLUIDO ou ANALISE
        # os arquivos não conseguirão ser atualizados. Assim, não aplica nenhuma alteração.
        if (in_array($enviaDadosPncp->situacao,
            [$this->recuperarIDSituacao('EXCLUIDO'), $this->recuperarIDSituacao('ANALISE')])) {
            return;
        }
        $temArquivosComEnvioPendente = $this->getArquivosComEnvioPendente(
            $enviaDadosPncp->pncpable_id,
            'quantidade');
        if ($temArquivosComEnvioPendente) {
            $enviaDadosPncp->situacao = $this->recuperarIDSituacao('ARQPEN');
        } else {
            $enviaDadosPncp->situacao = $this->recuperarIDSituacao('SUCESSO');
            $enviaDadosPncp->retorno_pncp = null;
        }
        $enviaDadosPncp->save();
    }

    /**
     * Obtém arquivos relacionados a um ContratoHistórico com envio pendente:
     * envio_pncp_pendente contenha qualquer valor
     *
     * @param int $contratoHistoricoId ID do contrato histórico.
     * @param string $retorno Tipo de retorno esperado. Aceita os valores:
     *                        - 'registros': retorna uma coleção com os registros encontrados.
     *                        - 'quantidade': retorna o número total de registros encontrados.
     *
     * @return \Illuminate\Support\Collection|int Retorna a coleção de registros ou a quantidade,
     *                                            dependendo do valor de $retorno.
     *
     * @throws \InvalidArgumentException Se o valor de $retorno não for 'registros' ou 'quantidade'.
     */
    protected function getArquivosComEnvioPendente($contratoHistoricoId, $retorno = 'registros')
    {
        $query = Contratoarquivo::withTrashed()
            ->where('contratohistorico_id', $contratoHistoricoId)
            ->whereNotNull('envio_pncp_pendente');
        switch ($retorno) {
            case 'registros':
                return $query->get();
            case 'quantidade':
                return $query->count();
            default:
                throw new InvalidArgumentException('Tipo de retorno inválido. Use "registros" ou "quantidade".');
        }
    }

    private function cnpjOrgao($contratoH)
    {
        if (Contrato::isTermoDeContrato($contratoH->tipo->descricao)) {
            # Se é um termo, busca o sequencial do contrato pai para obter o CNPJ
            $enviaDadosPNCP = EnviaDadosPncp::select('sequencialPNCP')
                ->where('contrato_id', $contratoH->contrato_id)
                ->whereIn('tipo_contrato', config('api-pncp.tipos_contrato_aceitos_pncp'))
                ->oldest()
                ->first();
        } else {
            $enviaDadosPNCP = EnviaDadosPncp::select('sequencialPNCP')
                ->where('pncpable_type', Contratohistorico::class)
                ->where('pncpable_id', $contratoH->id)
                ->oldest()
                ->first();
        }
        # Retornar o CNPJ vinculado ao sequencial que retornou do PNCP
        if (!empty($enviaDadosPNCP->sequencialPNCP)) {
            return $this->formatarCnpjOrgao($enviaDadosPNCP->sequencialPNCP);
        }
        # Retornar o CNPJ da unidade origem do contrato ou unidade atual do contrato
        $unidadeOrigemCodigo = $contratoH->unidadeorigem->codigo ?? null;
        $unidadeCodigo = $contratoH->unidade->codigo ?? null;
        if ($unidadeOrigemCodigo !== null && $unidadeCodigo !== $unidadeOrigemCodigo) {
            return $contratoH->unidadeorigem->orgao->cnpj ?? null;
        }
        return $contratoH->unidade->orgao->cnpj ?? null;
    }

    private function trataRetornoExclusao($contrato_pncp, $envio)
    {
        DB::beginTransaction();
        try {
            $pncpControle = EnviaDadosPncp::find($contrato_pncp->id);

            //PNCP diz, no swagger, que o retorno é 200, mas a API retorna um 201, aguardar definição
            if ($envio->getStatusCode() == "201" || $envio->getStatusCode() == "200") {
                $pncpControle->situacao = $this->recuperarIDSituacao('EXCLUIDO');
                // Codigoitem::whereHas('codigo', function ($q) {
                //     $q->where('descricao', 'Situação Envia Dados PNCP');
                // })->where('descres', 'EXCLUIDO')->first()->id;

                $pncpControle->retorno_pncp = "";
            } else {
                //$status = "ERRO";
            }

            if ($pncpControle->pncpable_type == Contratohistorico::class) {
                $logInfo = "ID PNCP {$pncpControle->id} | ID Contrato Historico {$pncpControle->pncpable_id} | Status SUCESSO trataRetornoExclusao";
                //Log::channel('pncp')->info($logInfo);
            }

            $pncpControle->save();
            DB::commit();
        } catch (Exception $e) {
            if ($pncpControle->pncpable_type == Contratohistorico::class) {
                $logInfo = "ID PNCP {$pncpControle->id} | ID Contrato Historico {$pncpControle->pncpable_id} | Status ERRO trataRetornoExclusao";
                //Log::channel('pncp')->info($logInfo);
            }

            Log::error($e);
            DB::rollback();
        }
    }

    private function verificaEnvioDeArquivosPendentes($id, $tipo, $historico_id)
    {
        if ($tipo == Contratohistorico::class) {
            return Contratoarquivo::withTrashed()->where([
                ['contrato_id', '=', $id],
                ['contratohistorico_id', '=', $historico_id]
            ])->whereNotNull('envio_pncp_pendente')->count() > 0 ? true : false;
        }
    }

    /**
     * Atualiza o estado de um arquivo de contrato e, opcionalmente, o exclui.
     *
     * Este método recebe um objeto de arquivo e um array associativo com os novos estados a serem aplicados
     * a esse arquivo. Se a chave 'delete' estiver presente e for verdadeira, o arquivo será excluído após
     * a atualização de seu estado.
     *
     * @param \App\Models\Contratoarquivo $arquivo O objeto de arquivo a ser atualizado.
     * @param array $estadoArquivo Array associativo contendo os estados a serem atualizados no arquivo.
     *
     * @return void
     */
    private function atualizaEstadoArquivo($arquivo, $estadoArquivo)
    {
        $arquivo = Contratoarquivo::withTrashed()->find($arquivo->id);
        # Se a resposta do PNCP for maior do que 255, trunca para 255
        if (array_key_exists('retorno_pncp', $estadoArquivo)) {
            $estadoArquivo['retorno_pncp'] = Str::limit($estadoArquivo['retorno_pncp'], (255 - 3));
        }
        $arquivo->forceFill($estadoArquivo);
        try {
            $arquivo->saveWithoutEvents();
        } catch (Exception $e) {
            $errorMessage = "Erro ao salvar o estado do arquivo:\n" .
                "Arquivo atual: " . ($arquivo ? json_encode($arquivo->toArray()) : 'Não disponível') . "\n" .
                "Estado do arquivo: " . json_encode($estadoArquivo) . "\n" .
                "Mensagem de erro: " . $e->getMessage();
            $this->inserirLogPncp($errorMessage, __METHOD__, __LINE__);

        }
    }

    //148 parametro cc a mais
    private function trataRetorno($contrato_pncp, $envio, $alteracao, $campoSequencial, ContratoControllerPNCP $cc)
    {
        //Log::channel('pncp')->info("trataRetorno() EnviaPNCPTRAIT 653...");
        DB::beginTransaction();

        try {
            $pncpControle = EnviaDadosPncp::find($contrato_pncp->id);

            //PNCP diz, no swagger, que o retorno é 200, mas a API retorna um 201, aguardar definição
            if ($envio->getStatusCode() == "201" || $envio->getStatusCode() == "200") {
                $status = "SUCESSO";

                if ($alteracao) {
                    $pncpControle->json_enviado_alteracao = $envio->JSONEnviado;
                } else {
                    $pncpControle->json_enviado_inclusao = $envio->JSONEnviado;

                    $pncpControle->link_pncp = null;
                    # Quando o registro já existe no PNCP, o link_pncp (location no header) é null
                    # Neste caso, busca o link_pncp da minuta, que foi colocada em session
                    # ao iniciar o cadastro do contrato
                    if (empty($envio->getHeader('location')[0])) {
                        $enviaDadosPncpMinuta = EnviaDadosPncp::where(
                            'pncpable_id',
                            session('minutaEmpenhoId'))
                            ->first();
                        if ($enviaDadosPncpMinuta) {
                            $pncpControle->link_pncp = $enviaDadosPncpMinuta->link_pncp;
                        }

                    } else {
                        $pncpControle->link_pncp = $envio->getHeader('location')[0];
                    }
                    # Remove o id da minuta da sessão pois neste ponto, não será mais necessário
                    session()->forget('minutaEmpenhoId');

                    $pncpControle->sequencialPNCP = $this->buscaSequencial($pncpControle->link_pncp, $campoSequencial, $cc);//148 parâmetro cc
                }
                $enviaArquivos = false;
                if ($this->verificaEnvioDeArquivosPendentes($pncpControle->contrato_id, $pncpControle->pncpable_type, $pncpControle->pncpable_id)) {
                    $status = 'ARQPEN';
                    $enviaArquivos = true;
                }
                $pncpControle->situacao = $this->recuperarIDSituacao($status);
                //  Codigoitem::whereHas('codigo', function ($q) {
                //     $q->where('descricao', 'Situação Envia Dados PNCP');
                // })->where('descres', $status)->first()->id;
                if ($status == "SUCESSO") {
                    $pncpControle->retorno_pncp = "";
                }
            } else {
                //$status = "ERRO";
            }

            if ($pncpControle->pncpable_type == Contratohistorico::class) {
                $logInfo = "ID PNCP {$pncpControle->id} | ID Contrato Historico {$pncpControle->pncpable_id} | Status {$status} trataRetorno ";
                //Log::channel('pncp')->info($logInfo);
            }

            // $this->inserirLogCustomizado('pncp', 'error', $e, "insereContratos 244");

            $pncpControle->save();
            DB::commit();
            return $enviaArquivos;
        } catch (Exception $e) {
            if ($pncpControle->pncpable_type == Contratohistorico::class) {
                $logInfo = "ID PNCP {$pncpControle->id} | ID Contrato Historico {$pncpControle->pncpable_id} | Status ERRO trataRetorno";
                //Log::channel('pncp')->info($logInfo);
            }
            Log::error('trataRetorno');
            Log::error($e);
            DB::rollback();
        }
    }

    private function trataRetornoArquivo($pncpControle, $envio, $remocao, $arquivo, $empenho)
    {
        # Se não entrar me nenhum tipo para envio, a variável será enviada vazia e sai do método
        if (empty($envio)) {
            return;
        }

        DB::beginTransaction();
        try {
            //PNCP diz, no swagger, que o retorno é 200, mas a API retorna um 201, aguardar definição
            if ($envio->getStatusCode() == "201" || $envio->getStatusCode() == "200") {
                $status = "SUCESSO";

                if (!$remocao) {
                    $arquivo->link_pncp = $envio->getHeader('location')[0];
                    //$arquivo->sequencial_pncp = $this->buscaSequencial($arquivo->link_pncp);
                    //gato para pegar a ultima barra do link
                    $link = explode('/', $arquivo->link_pncp);
                    $arquivo->sequencial_pncp = $link[count($link) - 1];
                }
                if ($arquivo) {
                    $arquivo->envio_pncp_pendente = null;
                    if (isset($arquivo->link_sei) && (!strcasecmp($arquivo->getTipo(), 'CONTRATO'))) {
                        //Log::channel('schedule')->info("---------Excluindo (trataRetornoArquivo)---------");
                        $arquivo->excluiTempDocumentoSEI();
                    }
                }
                if ($empenho) {
                    $pncpControle->linkArquivoEmpenho = $envio->getHeader('location')[0];
                }
                $pncpControle->situacao = $this->recuperarIDSituacao($status);
                // Codigoitem::whereHas('codigo', function ($q) {
                //     $q->where('descricao', 'Situação Envia Dados PNCP');
                // })->where('descres', $status)->first()->id;
                if ($status == "SUCESSO") {
                    $pncpControle->retorno_pncp = "";
                }
            } else {
                $status = "ERRO";
                $pncpControle->retorno_pncp = $envio->getBody()->getContents();
                $pncpControle->situacao = $this->recuperarIDSituacao($status);
            }

            if ($pncpControle->pncpable_type == Contratohistorico::class) {
                $logInfo = "ID PNCP {$pncpControle->id} | ID Contrato Historico {$pncpControle->pncpable_id} | Status {$status} 741 trataRetornoArquivo";
                //Log::channel('pncp')->info($logInfo);
            }

            $pncpControle->save();

            if ($pncpControle->pncpable_type == Contratohistorico::class) {
                $logInfo = "ID PNCP {$pncpControle->id} | ID Contrato Historico {$pncpControle->pncpable_id} | Status {$status} 748 trataRetornoArquivo";
                //Log::channel('pncp')->info($logInfo);
            }

            if ($arquivo) {
                $arquivo->saveWithoutEvents();
            }

            if ($pncpControle->pncpable_type == Contratohistorico::class) {
                $logInfo = "ID PNCP {$pncpControle->id} | ID Contrato Historico {$pncpControle->pncpable_id} | Status {$status} 751 trataRetornoArquivo";
                //Log::channel('pncp')->info($logInfo);
            }

            DB::commit();
        } catch (Exception $e) {
            if ($pncpControle->pncpable_type == Contratohistorico::class) {
                $logInfo = "ID PNCP {$pncpControle->id} | ID Contrato Historico {$pncpControle->pncpable_id} | Status ERRO trataRetornoArquivo";
                /*Log::channel('pncp')->info($logInfo);
                Log::channel('pncp')->info($e->getMessage());
                Log::channel('pncp')->info($e->getTraceAsString());*/
            }
            Log::error($e);
            DB::rollback();
        }
    }

    private function registraErroNaTabelaEnviaDadosPncp($contrato_pncp, $alteracao, $erro)
    {
        DB::beginTransaction();
        try {
            $pncpControle = EnviaDadosPncp::find($contrato_pncp->id);
            if ($erro instanceof Exception) {
                $erro = $erro->getMessage();
                if (isset($erro->body)) {
                    if ($alteracao) {
                        $pncpControle->json_enviado_alteracao = $erro->body;
                    } else {
                        $pncpControle->json_enviado_inclusao = $erro->body;
                    }
                }
            }
            $pncpControle->retorno_pncp = $erro;
            $pncpControle->save();
            DB::commit();
        } catch (Exception $e) {
            $errorMessage = "Tentativa de inserir uma mensagem de erro na tabela pncp.\n" .
                "Linha PNCP: {$contrato_pncp->id}\n" .
                "ID do Contrato: {$contrato_pncp->contrato_id}\n" .
                "Contrato histórico: {$contrato_pncp->pncpable_id}\n" .
                "Erro do PNCP: {$erro}\n" .
                "Erro ocorrido: {$e->getMessage()}";
            $this->inserirLogPncp($errorMessage, __METHOD__, __LINE__);
            DB::rollback();
        }
    }

    private function registraErroArquivo($erro, $arquivo)
    {
        DB::beginTransaction();
        try {
            $arquivo = EnviaDadosPncp::withTrashed()->find($arquivo->id);
            if (empty($arquivo)) {
                return;
            }
            $arquivo->retorno_pncp = $erro->getMessage();
            if ($arquivo->pncpable_type == Contratohistorico::class) {
                $logInfo = "ID PNCP {$arquivo->id} | ID Contrato Historico {$arquivo->pncpable_id} | Status SUCESSO registraErroArquivo";
                //Log::channel('pncp')->info($logInfo);
            }
            $arquivo->saveWithoutEvents();
            DB::commit();
        } catch (Exception $e) {
            if ($arquivo->pncpable_type == Contratohistorico::class) {
                $logInfo = "ID PNCP {$arquivo->id} | ID Contrato Historico {$arquivo->pncpable_id} | Status ERRO registraErroArquivo";
                //Log::channel('pncp')->info($logInfo);
            }
            Log::error($e);
            DB::rollback();
        }
    }

    //148 adiciona o parametro cc
    private function buscaSequencial($url, $campo, ContratoControllerPNCP $cc)
    {
        return $cc->consultarContratoUrlMontada($url)[$campo];
    }

    private function buscaCNPJContratante($modalidade, $uasgCompra, $uasgUsuario, $numeroAno)
    {
        try {
            $numero_ano = explode('/', $numeroAno);
            //$request = $this->cscc->consultaCompraSiasgPNCP("06", "201013", "201013", "000992021");
            $dadosCompra = Compra::whereHas('modalidade', function ($m) use ($modalidade) {
                $m->where('descres', str_pad($modalidade, 2, "0", STR_PAD_LEFT));
            })
                ->whereHas('unidade_origem', function ($u) use ($uasgCompra) {
                    $u->where('codigo', str_pad($uasgCompra, 6, "0", STR_PAD_LEFT));
                })
                ->where('numero_ano', substr($numeroAno, 0, 5) . "/" . substr($numeroAno, 5, 4))
                ->first();

            if (isset($dadosCompra->id)) {
                if ($dadosCompra->id_unico != null and $dadosCompra->cnpjOrgao != null) {

                    $request = json_decode(json_encode([
                        'data' => [
                            'compraSispp' => [
                                'idUnico' => $dadosCompra->id_unico,
                                'cnpjOrgao' => $dadosCompra->cnpjOrgao,
                            ],
                        ],
                        'codigoRetorno' => '200',
                        'messagem' => 'Sucesso'
                    ]));
                } else {
                    //705 NDC
                    $request = $this->buscarCompra($modalidade, $uasgCompra, $uasgUsuario, null, $numero_ano[0], $numero_ano[1]);
                    $dadosCompra->id_unico = $request->data->compraSispp->idUnico;
                    $dadosCompra->cnpjOrgao = $request->data->compraSispp->cnpjOrgao;
                    $dadosCompra->save();
                }
            } else {
                //705 NDC
                $request = $this->buscarCompra($modalidade, $uasgCompra, $uasgUsuario, null, $numero_ano[0], $numero_ano[1]);
            }

            if ($request != null) {
                if ($request->codigoRetorno == '200' && $request->messagem == 'Sucesso') {
                    return $request->data->compraSispp;
                } else {
                    throw new Exception($request->messagem . " (SIASG)");
                }
            } else {
                throw new Exception("Erro de comunicação com o SIASG (modalidade: " . $modalidade . ", uasgCompra: " . $uasgCompra . ", uasgUsuario: " . $uasgUsuario . ", numeroAno: " . $numeroAno . ")");
            }
        } catch (Exception $e) {
//            $this->inserirLogCustomizado('pncp', 'error', $e, 'buscaCNPJContratante 530');
            throw $e;
        }
    }

    //148 retira método do controller e insere na trait
    public function enviaEmpenhoArquivo($envia_pncp, DocumentoContratoController $dcc)
    {
        $empenho = MinutaEmpenho::find($envia_pncp->pncpable_id);
        try {
            $documentoEnviado = null;
            $cnpjOrgao = $this->formatarCnpjOrgao($envia_pncp->sequencialPNCP); //explode('-', explode('/', $envia_pncp->sequencialPNCP)[0])[0];
            $ano = explode('NE', $empenho->mensagem_siafi)[0];
            $sequencialPNCP = $this->formatarSequencialPncp($envia_pncp->sequencialPNCP); //explode('-', explode('/', $envia_pncp->sequencialPNCP)[0])[2];
            $documentoEnviado = $dcc->inserirDocumentoContrato($empenho, $cnpjOrgao, $ano, $sequencialPNCP);
            $this->trataRetornoArquivo($envia_pncp, $documentoEnviado, true, null, true);
        } catch (Exception $e) {
            $this->registraErroNaTabelaEnviaDadosPncp($envia_pncp, true, $e);
        }
    }

    //148 retira do contratocrudcontroller e coloca na trait
    private function isContratoTipoEmpenho(string $tipoContrato)
    {
        if ($tipoContrato == 'Empenho') {
            return true;
        }
        return false;
    }


    public function recuperarDadosPNCP(int $idContratoArquivo, bool $restrito, bool $contratoArquivoLei14133)
    {
        $arquivo = Contratoarquivo::find($idContratoArquivo);
        if ($arquivo->contrato->elaboracao || !$contratoArquivoLei14133) return;

        $linhaPncpDoContratoPai = EnviaDadosPncp::where([
            ['contrato_id', '=', $arquivo->contrato_id],
            ['tipo_contrato', $arquivo->contrato_historico->tipo->descricao],
        ])->oldest()->first();
        $tiposTermos = $arquivo->getTipoTermoPncp();

        if (in_array($arquivo->getTipo(), $tiposTermos)) {
            $tipoArquivo = $arquivo->getTipo();
            switch ($arquivo->getTipo()) {
                case 'Termo Apostilamento':
                    $tipoArquivo = 'Termo de Apostilamento';
                    break;
                case 'Termo Rescisão':
                    $tipoArquivo = 'Termo de Rescisão';
                    break;
            }
            $linhaPncpDoContratoPai = EnviaDadosPncp::where([
                ['pncpable_id', '=', $arquivo->contratohistorico_id],
                ['pncpable_type', '=', \App\Models\Contratohistorico::class],
                ['tipo_contrato', $tipoArquivo]
            ])->oldest()->first();
        }

//            $enviarDocumentoPNCP = new EnviaPNCPController();
        $dcc = new DocumentoContratoController();
        $dtcc = new DocumentoTermoContratoController();

        $uc = new UsuarioController();
        $this->realizarLoginPNCP($uc);
        $dadosAtualizacao = null;

        if (!$restrito && !empty($arquivo->link_sei)) {
            $arquivo->salvaTempDocumentoSEI();
        }
        if (!empty($linhaPncpDoContratoPai)) {
            return $this->incluirArquivoPNCPPorContrato($arquivo->contrato_historico, $linhaPncpDoContratoPai, $arquivo, $dcc, $dtcc);
        }
    }

    /**
     * Realizar o login no PNCP para poder realizar alguma ação
     * @return void
     */
    private function realizarLoginNovo()
    {
        $uc = new UsuarioController();
        $this->realizarLoginPNCP($uc);
    }

    /**
     *
     * Método refatorado para pegar o cnpj do contratante, em relação a antiga, retiramos a CompraSiasgCrudController
     *
     * @param string $modalidade
     * @param string $numeroAno
     * @param string $uasgCompra
     * @param string $uasgUsuario
     * @return mixed
     * @throws Exception
     */
    private function buscaCNPJContratanteNovo(
        string $modalidade,
        string $numeroAno,
        string $uasgCompra,
        string $uasgUsuario
    )
    {
        try {
            $uasgCompra = str_pad($uasgCompra, 6, "0", STR_PAD_LEFT);
            $uasgUsuario = str_pad($uasgUsuario, 6, "0", STR_PAD_LEFT);

            # Recuperar as informações da compra no banco de dados
            $dadosCompra = Compra::whereHas('modalidade', function ($m) use ($modalidade) {
                $m->where('descres', $modalidade);
            })
                ->whereHas('unidade_origem', function ($u) use ($uasgCompra) {
                    $u->where('codigo', $uasgCompra);
                })
                ->where('numero_ano', $numeroAno)
                ->first();

            $numero_ano = explode('/', $numeroAno);

            if (isset($dadosCompra->id)) {
                # Se o campo id_unico e o cnpjOrgao estiverem preenchidos, então retorna com as informações do banco
                if (!empty($dadosCompra->id_unico) and !empty($dadosCompra->cnpjOrgao)) {
                    $compraPertenceLei14133Derivadas =
                        resolve(AmparoLegalService::class)->existeAmparoLegalEmLeiCompra($dadosCompra->lei);

                    if (!$compraPertenceLei14133Derivadas) {
                        $request = json_decode(json_encode([
                            'data' => null,
                            'codigoRetorno' => '204',
                            'messagem' => "Compra da lei {$dadosCompra->lei}"
                        ]));
                        return $request;
                    }

                    $request = json_decode(json_encode([
                        'data' => [
                            'compraSispp' => [
                                'idUnico' => $dadosCompra->id_unico,
                                'cnpjOrgao' => $dadosCompra->cnpjOrgao,
                                'codigoRetorno' => '200'
                            ],
                        ],
                        'codigoRetorno' => '200',
                        'messagem' => 'Sucesso'
                    ]));
                } else {
                    $numeroAno = str_replace("/", "", $numeroAno);
                    //705 NDC
                    $request = $this->buscarCompra($modalidade, $uasgCompra, $uasgUsuario, null, $numero_ano[0], $numero_ano[1]);
                    if (empty($request->data) && $request->codigoRetorno == '200') {
                        $request->codigoRetorno = 204;
                        return $request;
                    }

                    if (empty($request->data) && $request->codigoRetorno == '200') {
                        $request->codigoRetorno = 204;
                        return $request;
                    }

                    if (empty($request->data) && $request->codigoRetorno == '204') {
                        return $request;
                    }

                    $compraPertenceLei14133Derivadas =
                        resolve(AmparoLegalService::class)
                            ->existeAmparoLegalEmLeiCompra($request->data->compraSispp->lei);

                    if (!$compraPertenceLei14133Derivadas) {
                        $request = json_decode(json_encode([
                            'data' => null,
                            'codigoRetorno' => '204',
                            'messagem' => "Compra da lei {$request->data->compraSispp->lei}"
                        ]));
                        return $request;
                    }

                    # Se a compra for encontrada, então salva o id único e cnpj do orgão
                    if (!empty($request->data)) {
                        $dadosCompra->id_unico = $request->data->compraSispp->idUnico;
                        $dadosCompra->cnpjOrgao = $request->data->compraSispp->cnpjOrgao;
                        $dadosCompra->save();
                    }
                }

                $request->data->compraSispp->codigoRetorno = 200;
                $request->data->compraSispp->messagem = 'Sucesso';
                return $request->data->compraSispp;
            }

            $numeroAno = str_replace("/", "", $numeroAno);
            //705 NDC

            $compraSiasg = $this->buscarCompra($modalidade, $uasgCompra, $uasgUsuario, null, $numero_ano[0], $numero_ano[1]);

            if (empty($compraSiasg->data) && $compraSiasg->codigoRetorno == '200') {
                $compraSiasg->codigoRetorno = 204;
                return $compraSiasg;
            }

            if (empty($compraSiasg->data) && $compraSiasg->codigoRetorno == '204') {
                return $compraSiasg;
            }

            if ($compraSiasg->codigoRetorno == '200' && $compraSiasg->messagem == 'Sucesso') {
                $compraPertenceLei14133Derivadas =
                    resolve(AmparoLegalService::class)
                        ->existeAmparoLegalEmLeiCompra($compraSiasg->data->compraSispp->lei);
                if (!$compraPertenceLei14133Derivadas) {
                    $compraSiasg = json_decode(json_encode([
                        'data' => null,
                        'codigoRetorno' => '204',
                        'messagem' => "Compra da lei {$compraSiasg->data->compraSispp->lei}"
                    ]));
                    return $compraSiasg;
                }

                $compraSiasg->data->compraSispp->codigoRetorno = $compraSiasg->codigoRetorno;
                return $compraSiasg->data->compraSispp;
            }
            return $compraSiasg;
        } catch (Exception $e) {
//            $this->inserirLogCustomizado('pncp', 'error', $e);
            throw $e;
        }
    }

    /**
     *
     * Método responsável em trator todos os retorno, melhoramos o código e deixamos genérico para todas a ações
     *
     * @param EnviaDadosPncp $pncpControle
     * @param $envio
     * @param ContratoControllerPNCP $cc
     * @param string $status
     * @param bool $alteracaoPNCP
     * @param string $campoSequencial
     * @return null
     */
    private function trataRetornoNovo(
        EnviaDadosPncp         $pncpControle,
                               $envio,
        ContratoControllerPNCP $cc,
        string                 $status = "SUCESSO",
        bool                   $alteracaoPNCP = false,
        string                 $campoSequencial = "numeroControlePNCP"
    )
    {
        if (!isset($envio->DataHandler)) {
            try {
                //PNCP diz, no swagger, que o retorno é 200, mas a API retorna um 201, aguardar definição
                if ($envio->getStatusCode() == "201" || $envio->getStatusCode() == "200") {
                    if ($alteracaoPNCP) {
                        $pncpControle->json_enviado_alteracao = $envio->JSONEnviado ?? null;
                    } else {
                        $pncpControle->json_enviado_inclusao = $envio->JSONEnviado ?? null;
                        $pncpControle->link_pncp = $envio->getHeader('location')[0];
                        $pncpControle->sequencialPNCP =
                            $this->buscaSequencial($pncpControle->link_pncp, $campoSequencial, $cc);
                    }

                    if ($this->verificaEnvioDeArquivosPendentes(
                        $pncpControle->contrato_id,
                        $pncpControle->pncpable_type,
                        $pncpControle->pncpable_id
                    )) {
                        $status = 'ARQPEN';
                    }
                    $pncpControle->situacao = $this->recuperarIDSituacao($status);

                    if ($status == "SUCESSO") {
                        $pncpControle->retorno_pncp = "";
                    }
                }

                $pncpControle->save();
                return true;
            } catch (Exception $e) {
                //          $this->inserirLogCustomizado('pncp', 'error', $e);
                return false;
            }
        }
    }

    /**
     *
     * Método responsável em alterar o status da tabela responsável pelo PNCP
     * ou criar novo registro
     *
     * @param int $pncpableId
     * @param string $pncpableType
     * @param string $statusPNCP
     * @param int|null $contratoId
     * @param string|null $tipo
     * @param string|null $linkPncp
     * @param string|null $sequencialPNCP
     * @param string|null $linkArquivoEmpenho
     * @return mixed
     */
    public function manipularSituacaoPNCPNovo(
        int     $pncpableId,
        string  $pncpableType,
        string  $statusPNCP,
        ?int    $contratoId = null,
        ?string $tipo = null,
        ?string $linkPncp = null,
        ?string $sequencialPNCP = null,
        ?string $linkArquivoEmpenho = null
    )
    {
        $pncp = EnviaDadosPncp::where([
            ['pncpable_type', '=', $pncpableType],
            ['pncpable_id', '=', $pncpableId],
        ])->oldest()->first();

        $situacaoRegistroPncp = $this->recuperarIDSituacao($statusPNCP);

        # Se o registro existir, então vamos atualizar a informação com o novo status
        if (isset($pncp)) {
            # Somente atualiza o registro se estiver no PNCP
            if (isset($pncp->sequencialPNCP)) {
                $pncp->situacao = $situacaoRegistroPncp;
                $pncp->save();
            }

            # Caso a situação seja assinatura pendente, deve ser alterado para INCPEN (inclusão pendente)
            if ($pncp->status->descres == 'ASNPEN') {
                $pncp->situacao = $situacaoRegistroPncp;
                $pncp->save();
            }

            return $pncp;
        }

        # Se o registro não existir, então criamos um novo registro para que seja o de envio para o PNCP
        return EnviaDadosPncp::create([
            'pncpable_type' => $pncpableType,
            'contrato_id' => $contratoId,
            'situacao' => $situacaoRegistroPncp,
            'tipo_contrato' => $tipo,
            'pncpable_id' => $pncpableId,
            'link_pncp' => $linkPncp,
            'sequencialPNCP' => $sequencialPNCP,
            'linkArquivoEmpenho' => $linkArquivoEmpenho,
        ]);
    }

    public function inserirContratoPNCPNovo(
        Contratohistorico                $contrato,
        EnviaDadosPncp                   $contrato_pncp,
        ContratoControllerPNCP           $cc,
        DocumentoContratoController      $dcc,
        DocumentoTermoContratoController $dtcc
    )
    {
        $contratoEnviado = null;
        //Log::channel('pncp')->info("---modalidade: {$contrato->modalidade}----");
        $sispp = $this->buscaCNPJContratanteNovo(
            $contrato->modalidade->descres,
            $contrato->licitacao_numero,
            $contrato->unidadecompra->codigo,
            $contrato->unidadecompra->codigo);

        $cnpjOrgao = $this->cnpjOrgao($contrato);

        if (empty($cnpjOrgao) && $sispp) {
            $contrato_pncp->retorno_pncp = 'CNPJ e Compra vazias';
            $contrato_pncp->save();
            return;
        }

        if ($sispp->codigoRetorno == 204) {
            $contrato_pncp->retorno_pncp = $sispp->messagem;
            $contrato_pncp->save();
            return;
        }
        # Necessário colocar o try dentro do método, para poder pegar o erro do PNCP ou aplicação
        # e salvar o retorno na tabela envia dados pncp
        try {
            $contratoEnviado = $cc->inserirContrato($contrato, $sispp->cnpjOrgao, $sispp->idUnico, $cnpjOrgao);

            $enviaArquivos = $this->trataRetornoNovo($contrato_pncp, $contratoEnviado, $cc);
            if ($enviaArquivos) {
                $this->enviaContratoArquivos(EnviaDadosPncp::find($contrato_pncp->id), $dcc, $dtcc);
            }
        } catch (Exception $ex) {
            $contrato_pncp->retorno_pncp = substr($ex->getMessage(), 0, 250);
            $contrato_pncp->save();
        }

    }

    private function unidadeExistePNCP(string $cnpjOrgao, string $codigoUnidade)
    {
        $unidadeControlller = new UnidadeController();
        try {
            $unidadeControlller->buscaUnidade($cnpjOrgao, $codigoUnidade);
        } catch (Exception $ex) {
            if (
                !strpos('Código da Unidade do órgão informado não é uma Unidade cadastrada para o órgão',
                    $ex->getMessage())
            ) {
                $unidadeControlller->inserirUnidadePNCP($cnpjOrgao, $codigoUnidade);
            }
        }
    }

    /**
     * Verifica se a mensagem de erro da exceção corresponde a algum erro que deve mover para análise.
     *
     * @param Exception $e A exceção que contém a mensagem de erro a ser verificada.
     * @return bool Retorna `true` se a mensagem de erro corresponder a algum erro que deve mover para análise; caso contrário, retorna `false`.
     */
    private function deveMoverLinhaPncpParaAnalise(Exception $e): bool
    {
        $retornosErro = config('api-pncp.retornos_de_erro_do_pncp_que_devem_ser_movidos_para_analise');
        $mensagemErro = $e->getMessage();
        return $this->retornoDeErroPncpExisteNaListaDeErrosAnalise($mensagemErro, $retornosErro);
    }

    /**
     * Verifica se a mensagem de erro da exceção corresponde a algum erro que força a transição para análise.
     *
     * @param Exception $e A exceção que contém a mensagem de erro a ser verificada.
     * @return bool Retorna `true` se a mensagem de erro corresponder a algum erro que força a transição para análise; caso contrário, retorna `false`.
     */
    private function deveForcarSituacaoParaAnalise(Exception $e): bool
    {
        $errosForcados = config('api-pncp.retornos_de_erro_do_pncp_que_forcam_situacao_para_analise');
        $mensagemErro = $e->getMessage();
        return $this->retornoDeErroPncpExisteNaListaDeErrosAnalise($mensagemErro, $errosForcados);
    }

    /**
     * Verifica se a mensagem de erro existe na lista de erros fornecida.
     *
     * @param string $mensagemErro A mensagem de erro a ser verificada.
     * @param array $listaErros Uma lista de padrões (strings ou expressões regulares) a serem procurados na mensagem de erro.
     * @return bool Retorna `true` se algum padrão da lista for encontrado na mensagem de erro; caso contrário, retorna `false`.
     */
    private function retornoDeErroPncpExisteNaListaDeErrosAnalise(string $mensagemErro, array $listaErros): bool
    {
        foreach ($listaErros as $erro) {
            if (preg_match("/{$erro}/", $mensagemErro)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Auxilia a mudança de estado do registro pncp para "Análise".
     *
     * @param EnviaDadosPncp $enviaDadosPncp O registro que será alterado.
     * @param mixed $errorMessage Mensagem de erro, aceita Exception ou string.
     * @return void
     */
    private function alteraSituacaoParaAnalise(
        EnviaDadosPncp $enviaDadosPncp,
                       $errorMessage = null,
                       $forcarAtualizacaoAnalise = false)
    {
        # $errorMessage pode receber tanto uma exception quanto uma mensagem string
        if ($errorMessage instanceof Exception) {
            $mensagemErro = $errorMessage->getMessage();
        } elseif (is_string($errorMessage)) {
            $mensagemErro = $errorMessage;
        } else {
            $mensagemErro = null;
        }
        # Por padrão, se a situação for INCPEN, não passa para análise, pois ainda precisa tentar a inclusão
        # Porém, há erros específicos que, independentemente da situação, deve ser movido para análise.
        # Para isto, é forçada a atualização.
        if ($enviaDadosPncp->situacao != $this->recuperarIDSituacao('INCPEN') || $forcarAtualizacaoAnalise) {
            if ($mensagemErro) {
                $enviaDadosPncp->retorno_pncp = $mensagemErro;
            }
            if ($idSituacaoAnalise = $this->recuperarIDSituacao('ANALISE')) {
                $enviaDadosPncp->situacao = $idSituacaoAnalise;
            }
            $enviaDadosPncp->save();
        }
    }

    /**
     * Recebe uma URL no formato:
     * https://{endereço pncp}/api/pncp/v1/orgaos/{CNPJ}/contratos/{ANO}/{SEQ CONTRATO}/termos/{SEQ TERMO}/arquivos/{ID}
     * e retorna o ID do arquivo (no PNCP) contido na URL (último parâmetro)
     *
     * @param string $urlArquivo URL do arquivo
     *
     * @return int|null ID do arquivo ou null se a URL não for válida
     */
    public function getIdDoArquivoPelaUrl(string $urlArquivo)
    {
        if (empty($urlArquivo)) {
            return null;
        }
        $path = parse_url($urlArquivo, PHP_URL_PATH);
        if (!$path || !preg_match('/\/arquivos\/\d+$/', $path)) {
            return null;
        }
        $parts = explode('/', $path);
        $id = end($parts);
        return $id;
    }

}
