<?php

namespace App\Http\Traits;

use App\Models\Contrato;
use Route;

trait PermissaoTrait
{
    /**
     * Verifica permissões de tela específica
     *
     * @param string $tela O nome da tela que será checada
     * @return bool Retorna falso se o usuário não tiver permissão para acessar a tela
     */
    private function verificaPermissaoConsulta($tela){
        $rolesMinuta = ['Administrador', 'Execução Financeira'];
        $rolesContrato = ['Administrador', 'Setor Contratos'];
        $rolesMeusContrato = ['Administrador', 'Responsável por Contrato'];

        # Esse condicional verifica apenas para os perfis consulta/consulta suporte
        # para não quebrar os acessos de possíveis permissões antigas
        switch ($tela) {
            case 'minuta':
                if (backpack_user()->hasRole('Consulta') || backpack_user()->hasRole('Consulta Global')) {
                    // Verfica se tem mais alguma permissão além de consulta/consulta suporte
                    // para saber se ele pode entrar na tela
                    foreach (backpack_user()->roles()->pluck('name') as $userRoles){
                        if(in_array($userRoles, $rolesMinuta)) {
                            return true;
                        }
                    }
                    return false;
                }
                break;
            case 'contrato':
                if (backpack_user()->hasRole('Consulta') || backpack_user()->hasRole('Consulta Global')) {
                    foreach (backpack_user()->roles()->pluck('name') as $userRoles){
                        if(in_array($userRoles, $rolesContrato)) {
                            return true;
                        }
                    }
                    return false;
                }
                break;
            case 'meus-contratos':
                if (backpack_user()->hasRole('Consulta') || backpack_user()->hasRole('Consulta Global')) {
                    foreach (backpack_user()->roles()->pluck('name') as $userRoles){
                        if(in_array($userRoles, $rolesMeusContrato)) {
                            return true;
                        }
                    }
                    return false;
                }
                break;
            default:
                return true;
                break;
        }

        return true;
    }

    /**
     * @return array
     */
    public function verificarPermissaoContrato(): array
    {
        $contrato_id = Route::current()->parameter('contrato_id');

        $contrato = Contrato::where('id', '=', $contrato_id)
            ->where('unidade_id', '=', session()->get('user_ug_id'))
            ->where('contratos.elaboracao', 'false')
            ->first();
        if (!$contrato) {
            abort('403', config('app.erro_permissao'));
        }
        return [$contrato_id, $contrato];
    }
}
