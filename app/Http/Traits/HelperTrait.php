<?php

namespace App\Http\Traits;

use Illuminate\Database\Eloquent\Builder;

trait HelperTrait
{
    public function retornaSearchData($tabela, $campo)
    {
        return function (Builder $query, $column, $searchTerm) use ($tabela, $campo) { 
            $formats = ['Y-m-d', 'd-m-Y', 'd/m/Y'];
            $parsedDate = null;
            foreach ($formats as $format) {
                try {
                    $date = \Carbon\Carbon::createFromFormat($format, $searchTerm);
                    $parsedDate = $date->format('Y-m-d');
                    break;
                } catch (\Exception $e) {
                    continue;
                }
            }
            if ($parsedDate) {
                $query->orWhereRaw("to_char($tabela.$campo, 'YYYY-MM-DD') = ?", [$parsedDate]);
            } else {
                $query->orWhereRaw("$tabela.$campo::text ilike ?", ["%{$searchTerm}%"]);
            }
        };
    }
}
