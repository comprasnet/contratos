<?php

namespace App\Http\Traits;

use Illuminate\Support\Facades\Storage;

trait EnceramentoContratoTrait
{
    private function recuperarIsSaldoContaVinculadaLiberado(?bool $isSaldoContaVinculadaLiberado) {
        if($isSaldoContaVinculadaLiberado === true){
            return '1';
        }

        if($isSaldoContaVinculadaLiberado === false){
            return '0';
        }
        return '2';
    }

    private function recuperarIsSaldoContaVinculadaLiberadoRelatorio(?bool $isSaldoContaVinculadaLiberado) {
        switch($isSaldoContaVinculadaLiberado) {
            case true:
                return 'SIM';
            break;
            case false:
                if(empty($isSaldoContaVinculadaLiberado)) {
                    return 'NÃO APLICÁVEL';
                } else {
                    return 'NÃO';
                }
            break;
            case null:
                return 'NÃO SELECIONADO';
            break;
        }
    }

    private function recuperarIsGarantiaContratualDevolvida(?bool $isGarantiaContratualDevolvida) {

        if($isGarantiaContratualDevolvida === true){
            return '1';
        }

        if($isGarantiaContratualDevolvida === false){
            return '0';
        }
        return '2';
    }

    private function recuperarIsGarantiaContratualDevolvidaRelatorio(?bool $isGarantiaContratualDevolvida) {
        switch($isGarantiaContratualDevolvida) {
            case true:
                return 'SIM';
            break;
            case false:
                if(empty($isGarantiaContratualDevolvida)) {
                    return 'NÃO APLICÁVEL';
                } else {
                    return 'NÃO';
                }
            break;
            case null:
                return 'NÃO SELECIONADO';
            break;
        }
    }

    private function recuperarGrauSatisfacaoDesempenhoContrato(?int $grauSatisfacaoDesempenhoContrato) {
        if( is_null($grauSatisfacaoDesempenhoContrato) or $grauSatisfacaoDesempenhoContrato === '' ) {
            return 2;
        }
        return $grauSatisfacaoDesempenhoContrato;
    }

    private function recuperarGrauSatisfacaoDesempenhoContratoRelatorio(?int $grauSatisfacaoDesempenhoContrato) {
       switch($grauSatisfacaoDesempenhoContrato) {
        case 0:
            return 'MUITO BAIXO';
        break;
        case 1:
            return 'BAIXO';
        break;
        case 2:
            return 'MÉDIO';
        break;
        case 3:
            return 'ALTO';
        break;
        case 4:
            return 'MUITO ALTO';
        break;
       }
    }

    private function recuperarPlanejamentoContratacaoAtendida(?bool $planejamentoContratacaoAtendida) {
        switch($planejamentoContratacaoAtendida) {
            case true:
                return 1;
            break;
            case false:
                return 0;
            break;
            case null:
                return null;
            break;
        }
    }

    private function recuperarPlanejamentoContratacaoAtendidaRelatorio(?bool $planejamentoContratacaoAtendida) {
        switch($planejamentoContratacaoAtendida) {
            case true:
                return 'SIM';
            break;
            case false:
                return 'NÃO';
            break;
            case null:
                return 'NÃO SELECIONADO';
            break;
        }
    }

}
