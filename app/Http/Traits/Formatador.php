<?php

namespace App\Http\Traits;

use App\Models\Feriado;
use DateTime;
use Exception;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Config;
use phpDocumentor\Reflection\Types\Array_;

trait Formatador
{

    /**
     * Retorna $campo data formatado no padrão pt-Br: dd/mm/yyyy
     *
     * @param $campo
     * @return string
     * @author Anderson Sathler <asathler@gmail.com>
     */
    public function retornaDataAPartirDeCampo($campo, $formatoOrigem = 'Y-m-d', $formatoDestino = 'd/m/Y')
    {
        if (is_null($campo)) {
            return '';
        }

        try {
            $data = DateTime::createFromFormat($formatoOrigem, $campo);
            $retorno = ($data !== false) ? $data->format($formatoDestino) : '';
        } catch (Exception $e) {
            $retorno = '';
        }

        return $retorno;
    }

    /**
     * Retorna $campo numérico formatado no padrão pt-Br: 0.000,00, incluindo ou não 'R$ ' segundo $prefix
     *
     * @param $campo
     * @return string
     * @author Anderson Sathler <asathler@gmail.com>
     */
    public function retornaCampoFormatadoComoNumero($campo, $prefix = false)
    {
        try {
            $numero = number_format($campo, 2, ',', '.');
            $numeroPrefixado = ($prefix === true ? 'R$ ' : '') . $numero;
            $retorno = ($campo < 0) ? "($numeroPrefixado)" : $numeroPrefixado;
        } catch (Exception $e) {
            $retorno = '';
        }

        return $retorno;
    }

    public function formataProcesso($mask, $str)
    {
        $str = str_replace(" ", "", $str);

        for ($i = 0; $i < strlen($str); $i++) {
            $mask[strpos($mask, "#")] = $str[$i];
        }

        return $mask;
    }

    public function formataDataSiasg($data)
    {
        return $this->retornaDataAPartirDeCampo($data, 'Ymd', 'Y-m-d');
    }

    public function formataDecimalSiasg($dado)
    {
        return number_format($dado, 2, '.', '');
    }

    public function formataIntengerSiasg($dado)
    {
        return number_format($dado, 0, '', '');
    }

    public function formataNumeroContratoLicitacao($dado): string
    {
        $d[0] = substr($dado, 0, 5);
        $d[1] = substr($dado, 5, 4);

        return $d[0] . '/' . $d[1];
    }

    public function formataCnpjCpf($dado)
    {
        $retorno = $dado;
        $tipo = $this->retornaTipoFornecedor($dado);

        if ($tipo == 'JURIDICA') {
            $retorno = $this->formataCnpj($dado);
        }

        if ($tipo == 'FISICA') {
            $retorno = $this->formataCpf($dado);
        }

        return $retorno;
    }

    public function retornaTipoFornecedor($dado)
    {

        if (strlen($dado) == 6 && ctype_digit($dado)) {
            return 'UG';
        }

        if (strlen($dado) == 9 || $dado === 'ESTRANGEIRO' || ctype_alpha(substr($dado, 0, 2))) {
            return 'IDGENERICO';
        }

        if (strlen($dado) == 11) {
            return 'FISICA';
        }

        if (strlen($dado) == 14 || !ctype_alpha(substr($dado, 0, 2))) {
            return 'JURIDICA';
        }

        return 'UG';
    }

    public function formataCnpj($numero)
    {
        $d[0] = substr($numero, 0, 2);
        $d[1] = substr($numero, 2, 3);
        $d[2] = substr($numero, 5, 3);
        $d[3] = substr($numero, 8, 4);
        $d[4] = substr($numero, 12, 2);

        return $d[0] . '.' . $d[1] . '.' . $d[2] . '/' . $d[3] . '-' . $d[4];
    }

    public function formataCpf($numero)
    {
        if (strlen($numero) < 11) {
            $numero = str_pad($numero, 11, "0", STR_PAD_LEFT);
        }

        $d[0] = substr($numero, 0, 3);
        $d[1] = substr($numero, 3, 3);
        $d[2] = substr($numero, 6, 3);
        $d[3] = substr($numero, 9, 2);

        return $d[0] . '.' . $d[1] . '.' . $d[2] . '-' . $d[3];
    }

    public function retornaMascaraCpf($cpf)
    {
        return '***' . substr($cpf, 3, 9) . '**';
    }

    public function retornaFormatoAmericano($valor)
    {
        return str_replace(',', '.', str_replace('.', '', $valor));
    }

    function formataCEP($cep)
    {

        $cep = preg_replace('/\D/', '', $cep);

        if (strlen($cep) !== 8) {
            return false;
        }

        // Formata o CEP no formato "99999-999"
        $formattedCEP = substr($cep, 0, 5) . '-' . substr($cep, 5, 3);

        return $formattedCEP;
    }

    /**
     * Retorna campo com a descricao detalhada para visualização na tabela
     * @param string $descricao
     * @param string $descricaocompleta
     * @return string
     */
    public function retornaDescricaoDetalhada(string $descricao = null, string $descricaocompleta = null): string
    {
        if ($descricao == null) {
            return '';
        }

        $retorno = $descricao . ' <i class="fa fa-info-circle" title="' . $descricaocompleta . '"></i>';

        return $retorno;
    }

    public function montarBotaoTextoDetalhado(?string $descricaoResumida, ?string $descricaoCompleta, ?int $idUnico)
    {
        if (empty($descricaoResumida)) {
            return '';
        }

        $idBotao = "botaoDescricaoCompleta_{$idUnico}";
        $idTexto = "textoDescricaoCompleta_{$idUnico}";

        $botao = " <button type='button' class='btn btn-default btn-xs' onclick='exibirDescricaoCompleta(`{$descricaoCompleta}`, `{$descricaoResumida}`,{$idUnico})' >
                        <span id='{$idBotao}' data-tipo='resumido' >
                            <i class='fa fa-fw fa-caret-square-o-down'></i>
                        </span>
                      </button>";

        return "<span id ='{$idTexto}' >{$descricaoResumida}</span> {$botao}";
    }

    public function removeMascaraCPF($cpfComMask)
    {
        $cpf = str_replace('.', '', $cpfComMask);
        $cpf = str_replace('-', '', $cpf);
        return $cpf;
    }


    public function verificaDataDiaUtil($data)
    {
        $hoje = date('Y-m-d');
        $data_publicacao = Carbon::createFromFormat('Y-m-d', $data);
        $proximoDiaUtil = $data_publicacao;
        $hoje_ate18hs = Carbon::createFromFormat('Y-m-d', $hoje)->setTime(18, 00, 00);
        $hoje_pos18hs = Carbon::createFromFormat('Y-m-d', $hoje)->setTime(23, 59, 59);
        $feriados = Feriado::select('data')->pluck('data')->toArray();

        if ($data_publicacao->lessThanOrEqualTo($hoje_ate18hs)) {
            $proximoDiaUtil = $data_publicacao->nextWeekday();
            (in_array($proximoDiaUtil->toDateString(), $feriados)) ? $proximoDiaUtil->nextWeekday() : '';
        }

        if ($data_publicacao->isAfter($hoje_ate18hs) && $data_publicacao->lessThan($hoje_pos18hs)) {
            $proximoDiaUtil = $data_publicacao->nextWeekday()->addWeekday();
            (in_array($proximoDiaUtil->toDateString(), $feriados)) ? $proximoDiaUtil->nextWeekday() : '';
        }

        if ($data_publicacao->isAfter($hoje_pos18hs)) {
            $proximoDiaUtil = (!$data_publicacao->isWeekday()) ? $data_publicacao->nextWeekday() : $proximoDiaUtil;
            (in_array($proximoDiaUtil->toDateString(), $feriados)) ? $proximoDiaUtil->nextWeekday() : '';
        }

        return ($proximoDiaUtil->toDateString());

    }

    function validaCPF($cpf)
    {
        // Extrai somente os números
        $cpf = preg_replace('/[^0-9]/is', '', $cpf);

        // Verifica se foi informado todos os digitos corretamente
        if (strlen($cpf) != 11) {
            return false;
        }

        // Verifica se foi informada uma sequência de digitos repetidos. Ex: 111.111.111-11
        if (preg_match('/(\d)\1{10}/', $cpf)) {
            return false;
        }

        // Faz o calculo para validar o CPF
        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf[$c] * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf[$c] != $d) {
                return false;
            }
        }
        return true;

    }

    function validaCNPJ($cnpj)
    {
        // Remove caracteres não numéricos
        $cnpj = preg_replace('/[^0-9]/', '', $cnpj);

        // Verifica se o CNPJ possui 14 dígitos
        if (strlen($cnpj) != 14) {
            return false;
        }

        // Calcula o primeiro dígito verificador
        $soma = 0;
        $peso = 5;
        for ($i = 0; $i < 12; $i++) {
            $soma += $cnpj[$i] * $peso;
            $peso = ($peso == 2) ? 9 : $peso - 1;
        }
        $resto = $soma % 11;
        $digito1 = $resto < 2 ? 0 : 11 - $resto;

        // Calcula o segundo dígito verificador
        $soma = 0;
        $peso = 6;
        for ($i = 0; $i < 13; $i++) {
            $soma += $cnpj[$i] * $peso;
            $peso = ($peso == 2) ? 9 : $peso - 1;
        }
        $resto = $soma % 11;
        $digito2 = $resto < 2 ? 0 : 11 - $resto;

        // Verifica se os dígitos verificadores calculados são iguais aos dígitos informados
        return ($digito1 == $cnpj[12] && $digito2 == $cnpj[13]);
    }


    public function limpa_cpf_cnpj($valor)
    {
        $valor = trim($valor);
        $valor = str_replace(array('.', '-', '/'), "", $valor);
        return $valor;
    }

    public function retornaSomenteNumeros($value)
    {
        return preg_replace("/\D/", '', $value);
    }

    /**
     * Montar o nome do arquivo para o download
     *
     */
    private function montarNomeArquivoDownload(string $caminhoArquivo, ?string $nomeArquivo)
    {
        // dd($caminhoArquivo);
        $nomeArquivoFormatado = '';

        if (empty($nomeArquivo)) {

            $caminhoArquivoQuebrado = explode('/', $caminhoArquivo);

            $nomeArquivoFormatado = end($caminhoArquivoQuebrado);

        } else {

            $extension = pathinfo(storage_path($caminhoArquivo), PATHINFO_EXTENSION);

            $nomeArquivoFormatado = "{$nomeArquivo}.{$extension}";

        }

        $nomeArquivoFormatado = str_replace("\\", "_", $nomeArquivoFormatado);
        $nomeArquivoFormatado = str_replace("/", "_", $nomeArquivoFormatado);

        return $nomeArquivoFormatado;
    }

    public static function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif (1 == $bytes) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }

        return $bytes;
    }

    public function retornaMascaraCnpjCpf($campo)
    {
        $retorno = $campo;

        if (strlen($campo) == 14) {
            $d[0] = substr($campo, 0, 2);
            $d[1] = substr($campo, 2, 3);
            $d[2] = substr($campo, 5, 3);
            $d[3] = substr($campo, 8, 4);
            $d[4] = substr($campo, 12, 2);

            $retorno = $d[0] . '.' . $d[1] . '.' . $d[2] . '/' . $d[3] . '-' . $d[4];

        }

        if (strlen($campo) == 9) {
            $d[0] = substr($campo, 0, 3);
            $d[1] = substr($campo, 3, 3);
            $d[2] = substr($campo, 6, 3);
            $d[3] = substr($campo, 9, 2);

            $retorno = $d[0] . '.' . $d[1] . '.' . $d[2] . '-' . $d[3];
        }

        return $retorno;
    }

    function validarData($date, $format = 'Y-m-d H:i:s')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    function montarNumeroItem(int $numeroInformado)
    {
        $quantidadeCasas = Config::get("app.quantidadeCasasNumeroContratoItem");
        return str_pad($numeroInformado, $quantidadeCasas, '0', STR_PAD_LEFT);
    }

    function numberToRomanRepresentation($number)
    {

        if (!is_numeric($number)) {
            return $number;
        }

        $map = array(
            'M' => 1000,
            'CM' => 900,
            'D' => 500,
            'CD' => 400,
            'C' => 100,
            'XC' => 90,
            'L' => 50,
            'XL' => 40,
            'X' => 10,
            'IX' => 9,
            'V' => 5,
            'IV' => 4,
            'I' => 1
        );
        $returnValue = '';
        while ($number > 0) {
            foreach ($map as $roman => $int) {
                if ($number >= $int) {
                    $number -= $int;
                    $returnValue .= $roman;
                    break;
                }
            }
        }

        if (empty($returnValue)) {
            return $number;
        }

        return $returnValue;
    }


    public function salvarDadosUsuarioRedis(array $dadosUsuario)
    {
        $keyUsuarioRedis = "user_{$dadosUsuario['id']}";
        $dadosUsuario['sessionVersaoUm'] = session()->getId();

        $dadosUsuario = json_encode($dadosUsuario);
        Redis::set($keyUsuarioRedis, $dadosUsuario);
    }

    public function recuperarDadosUsuarioRedis(int $idUsuario)
    {
        $keyUsuarioRedis = "user_{$idUsuario}";

        $dadosUsuarioRedis = Redis::get($keyUsuarioRedis);

        return json_decode($dadosUsuarioRedis, true);
    }

    public function removerDadosUsuarioRedis(int $idUsuario)
    {
        $keyUsuarioRedis = "user_{$idUsuario}";
        $dadosUsuarioRedis = $this->recuperarDadosUsuarioRedis($idUsuario);
        if (isset($dadosUsuarioRedis['sessionVersaoDois'])) {
            $prefix = config('cache.prefix');
            $nomeKeyRedis = "{$prefix}:{$dadosUsuarioRedis['sessionVersaoDois']}";

            Redis::del($nomeKeyRedis);
        }

        Redis::del($keyUsuarioRedis);
    }

    function formataLeiDecreto(string $lei)
    {
        $primeiraLetra = substr($lei, 0, 1);
        if ($primeiraLetra === "D") {
            return str_replace($primeiraLetra, "DECRETO", $lei);
        }
        return $lei;
    }

    /**
     * Retorna $array acrescido de chave + valor, o que não é possível usando apenas array_push
     *
     * @param $array
     * @param $key
     * @param $value
     * @return Array
     * @author Juliano Pires
     */
    public function acrescentaIndiceValorArray($array, $key, $value): array
    {
        $array[$key] = $value;
        return $array;
    }

    public function getLastPathComponent($url)
    {
        if (!$url || empty($url)) {
            return null;
        }
        $parts = explode('/', $url);
        return end($parts);
    }

    /**
     * Substitui os parâmetros em uma rota com os valores fornecidos.
     *
     * @param string $route A rota com parâmetros a serem substituídos.
     * @param array $values Um array associativo contendo os valores dos parâmetros de rota.
     *                      A chave é o nome do parâmetro e o valor é o valor para substituição.
     * @return string A rota com os parâmetros substituídos pelos valores fornecidos.
     */
    function replaceRouteParam($route, $values)
    {
        foreach ($values as $key => $value) {
            $route = str_replace('{' . $key . '}', $value, $route);
        }
        return $route;
    }

    public function converterAmparoLegalEmLeiCompra(string $atoNormativo)
    {
        $arrayAtoNormativo = explode(' ', $atoNormativo);
        $nomeAtoNormativo = $arrayAtoNormativo[0];
        $arrayNumeroAtoNormativo = explode('/', $arrayAtoNormativo[1]);
        $somenteNumeroAtoNormativo = $this->retornaSomenteNumeros($arrayNumeroAtoNormativo[0]);

        $leiFormatada = $nomeAtoNormativo;

        if ($nomeAtoNormativo === 'DECRETO') {
            $leiFormatada = 'D';
        }

        return "$leiFormatada$somenteNumeroAtoNormativo";
    }

    public function mask($val, $mask)
    {
        $maskared = '';
        $k = 0;
        for ($i = 0; $i <= strlen($mask) - 1; $i++) {
            if ($mask[$i] == '#') {
                if (isset($val[$k])) $maskared .= $val[$k++];
            } else {
                if (isset($mask[$i])) $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }

    /*
     * Método para ser chamado no show dos crudController para aparecer as colunas com os name
     * originais no show(), pois há um bug que quando coloca um alias no setupList
     * ele não mostra no show()
     * */
    public function addColumnsShowOnly(array $colunas): void
    {
        $columns = array_map(function ($coluna) {
            $defaultColumnData = [
                'orderable' => true,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInExport' => true,
            ];

            $customColumnData = array_filter($coluna, function ($value) {
                return !is_null($value);
            });

            return array_merge($defaultColumnData, $customColumnData);
        }, $colunas);

        $this->crud->addColumns($columns);
    }

    /*
     * A ser chamado quando precisar converter a chave de contratação PNCP
     * para o formato que a api do PNCP na v2 espera
     */
    public function converterIdContratacaoPNCP(?string $codigo): ?string
    {
        if (!$codigo) {
            return null;
        }

        return preg_replace('/^(\d{14})-\d-(\d{6})\/(\d{4})$/', '$1/$3/$2', $codigo);
    }
}
