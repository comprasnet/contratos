<?php

namespace App\Http\Traits;

/**
 * Trait responsável por manipular e extrair informações de números compostos por "numero/ano".
 * Esta trait pode ser utilizada em objetos diferentes que possuem números no formato "01234/2024".
 *
 */
trait NumeroAnoTrait
{
    public static function extractAno($numeroAno = null)
    {
        if (empty($numeroAno)) return '';
        # Se estiver no formato NUMERO/ANO
        if (strpos($numeroAno, '/') !== false) {
            $partes = explode('/', $numeroAno);
            return isset($partes[1]) ? $partes[1] : '';
        }
        # Se estiver no formato ANO + NE + NUMERO (acontece somente com contrato)
        if (preg_match('/^\d{4}NE\d+$/', $numeroAno)) {
            return substr($numeroAno, 0, 4);
        }
        return '';
    }

    public function getAno()
    {
        if (!isset($this->numero)) return '';
        return self::extractAno($this->numero);
    }

    public static function extractNumero($numeroAno = null)
    {
        if (empty($numeroAno)) return '';
        # Se estiver no formato NUMERO/ANO
        if (strpos($numeroAno, '/') !== false) {
            list($numero, $ano) = explode('/', $numeroAno);
            return $numero;
        }
        # Se estiver no formato ANO + NE + NUMERO (acontece somente com contrato)
        if (preg_match('/\d{4}NE(\d+)$/', $numeroAno, $matches)) {
            return $matches[1];
        }
        return '';
    }

    public function getNumero()
    {
        if (!isset($this->numero)) return '';
        return self::extractNumero($this->numero);
    }
}
