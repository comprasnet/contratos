<?php

namespace App\Http\Traits;

use App\Http\Controllers\NDC;
use App\Models\Compra;
use APP\Models\Unidade;
use App\XML\ApiSiasg;

trait ConsultaCompra
{
    private function consultaCompraSiasg($modalidade, $uasgCompra, $uasgUsuario, $uasgBeneficiaria, $numeroAno)
    {
        $params = [
            'modalidade'       => $modalidade,
            'uasgCompra'       => $uasgCompra,
            'uasgUsuario'      => $uasgUsuario,
            'uasgBeneficiaria' => $uasgBeneficiaria,
            'numeroAno'        => $numeroAno
        ];

        $apiSiasg = new ApiSiasg();
        return json_decode($apiSiasg->executaConsulta('COMPRASISPP', $params));
    }

    /**
     * Verifica se a UASG tem permissão na compra
     *
     * Retorno da compra na api do SIASG método COMPRASISPP
     * @param $compraSiasg
     *
     * ID da unidade da compra que vem do formulário
     * @param $unidadeCompraId
     *
     * @return mixed|null
     */
    private function verificaPermissaoUasgCompra($compraSiasg, $unidadeCompraId)
    {
        $SISPP = 1;

        $unidade_autorizada_id = null;
        $tipoCompra = $compraSiasg->data->compraSispp->tipoCompra;
        $subrrogada = $compraSiasg->data->compraSispp->subrogada;

        if ($tipoCompra == $SISPP) {
            // se a compra for subrogada
            if ($subrrogada != '000000') {
                if ($subrrogada == session('user_ug')) {
                    return session('user_ug_id');
                }
            }

            if ($unidadeCompraId == session('user_ug_id')) {
                return $unidadeCompraId;
            }
        } else {
            return session('user_ug_id');
        }
        return $unidade_autorizada_id;
    }

    private function consultaCompraByLink($link)
    {
        $apiSiasg = new ApiSiasg();
        return $apiSiasg->consultaCompraByUrl($link);
    }

    public function verificaCompraExiste(Array $dados)
    {
        return Compra::where('unidade_origem_id', $dados['unidade_origem_id'])
            ->where('modalidade_id', $dados['modalidade_id'])
            ->where('numero_ano', $dados['numero_ano'])
            ->first();
    }

    private function buscarCompra($modalidade, $uasgCompra, $uasgUser, $uasgBeneficiaria, $numero_compra, $ano_compra, $num_contratacao = null)
    {
        $retornoSiasg  = null;
        $codigoRetorno = [200, 422, 500];

        // Busca pelo PNCP - MERCADOGOV
        if($num_contratacao != null){
            $num_contratacao_explode = explode('/', $num_contratacao);
            $consulta_ndc = NDC::consulta(
                'contratacao_pncp/compra',
                [
                    'orgao' => $num_contratacao_explode[0],
                    'ano' => $num_contratacao_explode[1],
                    'chave' => $num_contratacao_explode[2]
                ]
            );

            return $consulta_ndc;
        }

        if ($ano_compra == '2022' || $ano_compra == '2023') {
            //primeiro consulta siasg
            $retornoSiasg = $this->consultaCompraSiasg(
                $modalidade,
                $uasgCompra,
                $uasgUser,
                $uasgBeneficiaria,
                $numero_compra . $ano_compra
            );

            //sempre que o codigoRetorno 200 a compra pertence a SIASG
            // e deve devolver o retornoSiasg mesmo que o ->data seja vazio
            if (!(is_null($retornoSiasg->data)) || $retornoSiasg->codigoRetorno === 200) {
                return $retornoSiasg;
            }

            $consulta_ndc = NDC::consulta(
                'contratacao',
                [
                    'numeroCompra' => $numero_compra,
                    'anoCompra' => $ano_compra,
                    'uasgCompra' => $uasgCompra
                ]
            );

            if (isset($consulta_ndc) && in_array($consulta_ndc->codigoRetorno, $codigoRetorno)) {
                if($retornoSiasg && $consulta_ndc){
                    if(is_null($retornoSiasg->data) && is_null(($consulta_ndc->data))){
                        $retornoSiasg->MensagemErroNDC = $consulta_ndc->messagem;
                        $retornoSiasg->MensagemErroSiasg = $retornoSiasg->messagem;
                        return $retornoSiasg;
                    }
                }
                return $consulta_ndc;
            }
        }

        if ($ano_compra > '2023') {
            //primeiro consulta NDC
            $consulta_ndc = NDC::consulta(
                'contratacao',
                [
                    'numeroCompra' => $numero_compra,
                    'anoCompra' => $ano_compra,
                    'uasgCompra' => $uasgCompra
                ]
            );

            if (isset($consulta_ndc) && in_array($consulta_ndc->codigoRetorno, $codigoRetorno)) {
                if(!is_null($consulta_ndc->data) && $consulta_ndc->data->compraSispp->codigoModalidade == $modalidade){
                    return $consulta_ndc;
                }
            }
            $retornoSiasg = $this->consultaCompraSiasg(
                $modalidade,
                $uasgCompra,
                $uasgUser,
                $uasgBeneficiaria,
                $numero_compra . $ano_compra
            );
            if (!(is_null($retornoSiasg->data))) {
                return $retornoSiasg;
            }
        }

        // Quando não encontra a compra no NDC nem no SIASG, envia as duas mensagens de erro
        if($retornoSiasg && $consulta_ndc){
            if(is_null($retornoSiasg->data) && is_null(($consulta_ndc->data))){
                $retornoSiasg->MensagemErroNDC = $consulta_ndc->messagem;
                $retornoSiasg->MensagemErroSiasg = $retornoSiasg->messagem;
                return $retornoSiasg;
            }
        }

        if ($ano_compra < '2022') {
            //primeiro consulta SIASG
            $retornoSiasg = $this->consultaCompraSiasg(
                $modalidade,
                $uasgCompra,
                $uasgUser,
                $uasgBeneficiaria,
                $numero_compra . $ano_compra
            );
            if (!(is_null($retornoSiasg->data))) {
                return $retornoSiasg;
            }
        }

        return $retornoSiasg;
    }

    /**
     * Setando variáveis para método buscarCompras
     * @param HttpRequest $request
     * @return array
     */
    public function setVarsBuscarCompras($request): array
    {
        $dados = (object)$request->all()[0];

        $modalidade = $this->retornaDescresPorId($dados->modalidade_id);
        $uasgCompra = Unidade::find($dados->unidadecompra_id)->codigo;
        $uasgUser = session('user_ug');
        $uasgBeneficiaria = isset($form['unidadebeneficiaria_id'])
            ? Unidade::find($form['unidadebeneficiaria_id'])->codigo : null;
        $numero_compra = explode('/', $dados->licitacao_numero);
        $numeCompra = $numero_compra[0];
        $ano_compra = $numero_compra[1];
        return array($modalidade, $uasgCompra, $uasgUser, $uasgBeneficiaria, $numeCompra, $ano_compra);
    }

}
