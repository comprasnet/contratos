<?php

namespace App\Rules;

use App\Models\Contrato;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;


class ValidarExistsIndividual implements Rule
{
    /**
     * @var string
     */
    private $message;
    /**
     * @var string
     */
    private $column;
    /**
     * @var string
     */
    private $table;

    protected $wheres = [];

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(string $table, string $column)
    {
        $this->table = $table;
        $this->column  = $column;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!is_array($value)) {
            $value = [$value];
        }

        $data = DB::table($this->table)
            ->select($this->column)
            ->whereIn($this->column, $value)->get()->pluck('id')->toArray();

        $diff = array_diff($value,$data);

        if (count($diff) === 0) {
            return true;
        }

        $this->message = "O campo id contrato [".implode(', ',$diff)."] deve ser um contrato existente.";

        return false;

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message ?? 'Erro genérico.';
    }

}
