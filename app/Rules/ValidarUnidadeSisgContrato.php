<?php

namespace App\Rules;

use App\Models\Codigoitem;
use Illuminate\Contracts\Validation\Rule;

class ValidarUnidadeSisgContrato implements Rule
{
    private $receitaDespesa;
    private $minutas;
    private $empenhos;
    private $modalidadeID;
    private $sisg;
    private $message;

    public function __construct($receitaDespesa, $minutas, $empenhos, $modalidadeID)
    {
        $this->receitaDespesa = $receitaDespesa;
        $this->sisg = session()->get('user_ug_sisg');
        $this->minutas = $minutas;
        $this->empenhos = $empenhos;
        $this->modalidadeID = $modalidadeID;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $modalidade = Codigoitem::find($this->modalidadeID);
        if (isset($modalidade->descricao)) {
            if (in_array($modalidade->descricao, config('app.modalidades_sem_exigencia'))) {
                return true;
            }
        }

        if (
            $this->sisg === true &&
            $this->receitaDespesa === 'D'
        ) {
            if (empty($this->minutas) && (!$this->empenhos || count($this->empenhos['numero_unidade']) == 0)) {
                $this->message = 'O campo Minutas de Empenho ou Empenho é obrigatório quando o contrato é do ';
                $this->message .= 'tipo despesa e a modalidade decorrente de uma contratação.';
                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
