<?php

namespace App\Rules;

use App\Http\Traits\BuscaCodigoItens;
use App\Models\Contrato;
use App\Models\ContratoAntecipagov;
use Illuminate\Contracts\Validation\Rule;


class ValidarDomicilioBancario implements Rule
{
    use BuscaCodigoItens;

    private $conta_bancaria;
    private $num_agencia;
    private $num_banco;
    /**
     * @var string
     */
    private $message;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($num_banco, $num_agencia, $conta_bancaria)
    {
        $this->num_banco      = $num_banco;
        $this->num_agencia    = $num_agencia;
        $this->conta_bancaria = $conta_bancaria;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        foreach ($value as $item) {
            $contrato = Contrato::find($item);

            //caso nao exista antecipagov para este contrato
            if (!(isset($contrato->antecipagovs[0]))) {
                continue;
            }

            $statusOperacaoUltimoRegistroDomicilioBancario = ContratoAntecipagov::where('contrato_id', $contrato->id)
                    ->latest('id')
                    ->first()
                    ->antecipagov
                    ->status_operacao ?? null;


            if ($statusOperacaoUltimoRegistroDomicilioBancario !== 'LIQUIDAR' && // caso situacao seja LIQUIDAR
                $statusOperacaoUltimoRegistroDomicilioBancario !== 'CANCELAR' // caso situacao seja CANCELAR
            ) {
                $this->message = "O contrato $item já possui domicílio bancário registrado.";

                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message ?? 'Erro genérico.';
    }
}
