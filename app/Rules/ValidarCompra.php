<?php

namespace App\Rules;

use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\Formatador;
use App\Models\AmparoLegal;
use App\Models\Codigoitem;
use App\Models\Compra;
use App\Models\Contrato;
use App\Models\Unidade;
use App\services\AmparoLegalService;
use App\services\CompraService;
use Illuminate\Contracts\Validation\Rule;
use App\Http\Traits\ConsultaCompra;
use Illuminate\Support\Arr;

class ValidarCompra implements Rule
{
    use BuscaCodigoItens, ConsultaCompra, Formatador;

    private $unidadecompra_id;
    private $unidadebeneficiaria_id;
    private $modalidade_id;
    private $licitacao_numero;
    private $valor_global;
    private $message;
    private $unidadeusuario;
    private $contrato_id;
    private $lei14133;
    private $anoContrato;

    private $amparoLegal;
    private $unidade_id;
    private $unidadeGestoraAtual;
    private $tipoId;
    private $unidadeorigem_id;
    private $tipoContratoDiferenteEmpenho = false;
    private $modalidade;
    private $contrataMaisBrasil;
    private $numero_contratacao;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(
        $modalidade_id,
        $unidadecompra_id,
        $unidadebeneficiaria_id,
        $licitacao_numero,
        $valor_global = null,
        $unidadeorigem_id,
        $lei14133 = false,
        $contrato_id = null,
        array $amparoLegal = [],
        $unidade_id,
        $anoContrato = null,
        $tipoId,
        $modalidade = null,
        $contrataMaisBrasil = null,
        $numero_contratacao = null
    )
    {
        $this->modalidade_id = $modalidade_id;
        $this->licitacao_numero = $licitacao_numero;
        $this->unidadecompra_id = $unidadecompra_id;
        $this->unidadebeneficiaria_id = $unidadebeneficiaria_id;
        $this->unidadeusuario = session('user_ug');
        $this->valor_global = $valor_global;
        $this->contrato_id = $contrato_id;
        $this->lei14133 = $lei14133;
        $this->unidade_id = $unidade_id;
        $this->unidadeorigem_id = $unidadeorigem_id;
        $this->tipoId = $tipoId;
        $this->anoContrato = $anoContrato;
        $this->modalidade = $modalidade;
        $this->contrata_mais_brasil = $contrataMaisBrasil;
        $this->numero_contratacao = $numero_contratacao;

        if ((isset($unidadeorigem_id)) && ($unidadeorigem_id !== 'NULL')
            && ($unidadeorigem_id != session('user_ug_id'))
        ) {
            $this->unidadeusuario = Unidade::select('codigo')->find($unidadeorigem_id)->codigo;
        }

        $this->amparoLegal = $amparoLegal;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $tipo_contrato = $this->retornaDescCodigoItem($value);
        $tipos = ['Contrato', 'Termo de Adesão', 'Empenho', 'Carta Contrato'];
        if (!in_array($tipo_contrato, $tipos, true)) {
            return true;
        }

        if ($tipo_contrato !== "Empenho") {
            $this->tipoContratoDiferenteEmpenho = true;
        }

        if ($this->modalidade_id === null) {
            $this->message = 'O campo Id contratação PNCP é obrigatório quando o campo Contrata+Brasil ' .
                'estiver selecionado como "Sim".';
            return false;
        }

        if ($this->contrata_mais_brasil == '1' && $this->numero_contratacao === null) {
            $this->message = 'O campo Modalidade Licitação é obrigatório.';
            return false;
        }

        //Não validar compra caso a modalidade selecionada seja 'NÃO SE APLICA'
        $modalidade = $this->modalidade->descres;
        if ($modalidade === 'NAOSEAPLIC') {
            return true;
        }

        if ($this->unidadecompra_id === null && $this->contrata_mais_brasil !== '1') {
            $this->message = 'O campo Unidade Compra é obrigatório.';
            return false;
        }

        $this->unidadeGestoraAtual = Unidade::select('codigo')->find($this->unidade_id)->codigo;
        $unidadeorigem = Unidade::select('codigo')->find($this->unidadeorigem_id)->codigo;

        /* Deixar alterar a UG origem apenas para contrato do tipo empenho, dentro da regra
        ('Contrato', 'Termo de Adesão', 'Empenho', 'Carta Contrato'). */
        if (($unidadeorigem != $this->unidadeGestoraAtual) && $this->tipoContratoDiferenteEmpenho) {
            $this->message = 'A sua UASG não tem permissão para cadastrar contrato desta compra';
            return false;
        }

        if ($this->contrata_mais_brasil == '1') {
            $retornoSiasg = $this->buscarCompra(
                '',
                '',
                '',
                '',
                '',
                '',
                $this->converterIdContratacaoPNCP($this->numero_contratacao)
            );
            $codigo_unidade_compra = $retornoSiasg->data->compraSispp->unidade;
            $mod_unidadeCompra = Unidade::where('codigo', $codigo_unidade_compra)->first();
            $unidadeCompra = $mod_unidadeCompra->codigo;
        } else {
            /*  Se o contrato não for do tipo empenho busca a compra por unidade do usuário,
                Se contrato for tipo empenho pode altera a Unidade Gestora Origem do Contrato e
                verifica se ela ou unidade Gestora Atual pertence a compra uma das duas
                tem que retornar sucesso.
            */
            $unidadeCompra = Unidade::select('codigo')->find($this->unidadecompra_id)->codigo;
            $unidadeBeneficiaria = null;
            if ($this->unidadebeneficiaria_id !== null) {
                $unidadeBeneficiaria = Unidade::select('codigo')->find($this->unidadebeneficiaria_id)->codigo;
            }
            $numero_compra = explode('/', $this->licitacao_numero);

            $retornoSiasg = $this->buscarCompra(
                $modalidade,
                $unidadeCompra,
                session('user_ug'),
                $unidadeBeneficiaria,
                $numero_compra[0],
                $numero_compra[1]
            );

            if ($tipo_contrato === 'Empenho'
                && isset($retornoSiasg->messagem)
                && $retornoSiasg->messagem !== "Sucesso") {
                // Validaçao feita mais abaixo no verificaPermissaoSisrp
                // por que se a unidade logada for carona, tem que passar a validação
                $retornoSiasg = $this->buscarCompra(
                    $modalidade,
                    $unidadeCompra,
                    $unidadeorigem,
                    $unidadeBeneficiaria,
                    $numero_compra[0],
                    $numero_compra[1]
                );
            }
        }

        $resultado = resolve(AmparoLegalService::class)->verificarAmparoLegal($retornoSiasg, $this->amparoLegal);
        if ($resultado !== true) {
            $this->message = $resultado;
            return false;
        }

        //Não validar compra caso a unidade logada não seja SISG e lei antiga
        $unidadeLogada = Unidade::where('id', '=', session()->get('user_ug_id'))->first();
        if (!($unidadeLogada->sisg) && !$this->lei14133) {
            return true;
        }


        // Verifica se unidade logada do usuário é a mesma da unidade da compra para SISPP
        if ($retornoSiasg && $retornoSiasg->data != null && ($unidadeCompra != $this->unidadeusuario
                && $unidadeorigem != $this->unidadeGestoraAtual && $this->tipoContratoDiferenteEmpenho
                && $retornoSiasg->data->compraSispp->tipoCompra == 1)) {
            // Verifica se a COMPRA está subrogada para a unidade do usuário
            if ($this->unidadeusuario != $retornoSiasg->data->compraSispp->subrogada) {
                $this->message = 'A sua UASG não tem permissão para cadastrar contrato desta compra';
                // Só vai entrar aqui quando for instrumento inicial
                if ($this->contrato_id) {
                    $contrato = Contrato::find($this->contrato_id);
                    // Verifica se o CONTRATO foi subrogado para a unidade do usuário
                    $unidade_origem_id = $contrato->unidadeorigem_id;
                    $unidadeCompraid = Unidade::where('codigo', $unidadeCompra)->first();
                    if ($unidade_origem_id != $unidadeCompraid->id) {
                        return false;
                    }
                }
                return false;
            }
        }

        if (!isset($retornoSiasg->messagem)) {
            $this->message = 'Ocorreu um problema ao consultar a Compra.';
            return false;
        }

        if ($retornoSiasg->messagem !== "Sucesso") {
            $this->message = $retornoSiasg->messagem;

            $compra = $this->verificaCompraExiste([
                'unidade_origem_id' => $this->unidadecompra_id,
                'modalidade_id' => $this->modalidade_id,
                'numero_ano' => $this->licitacao_numero
            ]);

            if (!$compra) {
                return false;
            }

            if ($compra->tipo_compra_desc === 'SISRP' && $this->lei14133) {
                if (!($compra->verificaPermissaoSisrp(session('user_ug_id')))) {
                    return false;
                }
                $compra['subrrogada'] = '000000';
                if ($compra->unidade_subrrogada_id !== null) {
                    $compra['subrrogada'] = $compra->unidade_subrrogada->codigo;
                }

                $retornoSiasg = (object)[
                    'data' => (object)[
                        'compraSispp' => (object)[
                            'tipoCompra' => '2',
                            'inciso' => $compra['inciso'],
                            'lei' => $compra['lei'],
                            'artigo' => $compra['artigo'],
                            'subrogada' => $compra['subrrogada'],
                            'idUnico' => $compra['id_unico'],
                            'cnpjOrgao' => $compra['cnpjOrgao'],
                            'ano' => substr($compra['numero_ano'], 6, 6),
                        ]
                    ],
                    'messagem' => 'Sucesso',
                    'codigoRetorno' => 200
                ];
            }
        }

        if (isset($retornoSiasg->data->api_origem)
            && $retornoSiasg->data->api_origem === "novo_divulgacao_compra" &&
            $retornoSiasg->data->compraSispp->codigoModalidade != $modalidade) {
            $this->message = 'Modalidade incorreta da compra';
            return false;
        }

        if ($retornoSiasg->data) {
            if ($retornoSiasg->data->compraSispp->tipoCompra == '2'
                && !$this->lei14133
            ) {
                if (count($retornoSiasg->data->linkSisrpCompleto) == 0) {
                    $this->message = 'A sua UASG não tem permissão para cadastrar contrato desta compra';
                    return false;
                }
            }

            if ($retornoSiasg->data->compraSispp->tipoCompra == '2'
                && $this->lei14133
            ) {
                if (!(isset($compra))) {
                    $compra = Compra::where('unidade_origem_id', '=', $this->unidadecompra_id)
                        ->where('modalidade_id', '=', $this->modalidade_id)
                        ->where('numero_ano', '=', $this->licitacao_numero)
                        ->where('uasg_beneficiaria_id', '=', $this->unidadebeneficiaria_id)
                        ->first();
                }

                if ($compra === null) {
                    $this->message = 'Compra não possui Ata de Registro de Preços registrada.';
                    return false;
                }

                if (!($compra->verificaPermissaoSisrp(session('user_ug_id')))) {
                    $this->message = 'A sua UASG não tem permissão para cadastrar contrato desta compra';
                    return false;
                }
            }
        }

        if (
            !empty($this->anoContrato) &&
            !empty($retornoSiasg->data->compraSispp->lei) &&
            $lei = $retornoSiasg->data->compraSispp->lei
        ) {
            $compraPertenceLei14133Derivadas = resolve(CompraService::class)->compraPertenceLei14133Derivadas($retornoSiasg->data->compraSispp->lei);

            $amparoLegal14133 = resolve(AmparoLegalService::class)
                ->amparoLegalInformadoPertenceLeiCompra($lei, $this->amparoLegal);

            if ($amparoLegal14133 && $this->anoContrato < 2021 && $compraPertenceLei14133Derivadas) {
                $this->message = 'Não é permitido o registro de contrato com ano anterior a 2021, ' .
                    'cujo amparo legal seja decorrente da Lei 14.133/2021.';
                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message ?? 'Erro genérico ao consultar a compra.';
    }
}
