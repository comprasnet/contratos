<?php

namespace App\Rules;

use App\Models\Codigoitem;
use App\Models\Compra;
use App\Models\Unidade;
use App\services\AmparoLegalService;
use Illuminate\Contracts\Validation\Rule;
use App\Models\Contrato;
use App\Http\Traits\ConsultaCompra;

class AnoContratoValido implements Rule
{
    use ConsultaCompra;

    protected $anoContrato;
    protected $anoAtual;
    protected $anoMinimo;
    protected $dadosDaCompra;
    protected $retornoSiasg;
    protected $mensagemDeRetorno = false;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->anoContrato = Contrato::extractAno($value);
        $this->anoAtual = date('Y');
        $this->anoMinimo = $this->anoAtual - 50;

        if ($this->anoContrato > $this->anoAtual) {
            $this->mensagemDeRetorno = 'Não é permitido o registro de contrato com ano superior ao atual.';
            return false;
        }
        if ($this->anoContrato < $this->anoMinimo) {
            $this->mensagemDeRetorno = "O ano do contrato deve ser entre {$this->anoMinimo} e {$this->anoAtual}";
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->mensagemDeRetorno;
    }

}
