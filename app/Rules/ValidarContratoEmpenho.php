<?php

namespace App\Rules;

use App\Models\Contratoempenho;
use App\Models\Empenho;
use Illuminate\Contracts\Validation\Rule;

class ValidarContratoEmpenho implements Rule
{
    private $fornecedor;
    private $empenho;
    private $unidade;
    private $contrato;
    private $dados = [];
    private $message;

    public function __construct($request){

        $this->dados = $request;

    }

    public function passes($attribute, $value)
    {
        if($this->dados && isset($this->dados['fornecedor_id'])){
            $this->fornecedor = $this->dados['fornecedor_id'];
            $this->empenho = $this->dados['empenho_id'];
            $this->unidade = $this->dados['unidadeempenho_id'];
            $this->contrato = $this->dados['contrato_id'];
        }

        $padrao = '/empenho_nao_encontrado/i';

        $where = Empenho::where('empenhos.id', $this->empenho);

        if (preg_match($padrao, $this->empenho, $matches)) {
            $palavraEncontrada = $matches[0];
            $this->empenho = trim($this->empenho,$palavraEncontrada);
            $where = Empenho::where('numero', $this->empenho);
        }

        $getEmpenho = $where
            ->where('empenhos.fornecedor_id', $this->fornecedor)
            ->where('empenhos.unidade_id', $this->unidade)
            ->where('contrato_id', $this->contrato)
            ->join('contratoempenhos', 'contratoempenhos.empenho_id', '=', 'empenhos.id')
            ->get();

        if(count($getEmpenho) === 0){
            return true;
        }

        $this->message = 'Este Empenho está vinculado a um contrato!';
        return false;
    }

    public function message(){
        return $this->message;
    }
}
