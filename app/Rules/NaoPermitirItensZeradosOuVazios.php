<?php

namespace App\Rules;

use App\Models\MinutaEmpenho;
use Illuminate\Contracts\Validation\Rule;

class NaoPermitirItensZeradosOuVazios implements Rule
{
    /**
     * @var string
     */
    private $message;
    private $numero_item_compra;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($numero_item_compra)
    {
        $this->numero_item_compra = $numero_item_compra;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $key = substr($attribute, strpos($attribute, '.')+1);
        if ($value === '0'){
            $this->message = "O campo de Quantidade do Item N° {$this->numero_item_compra[$key]} " .
                "na Aba Itens do contrato de itens não pode ser 0.";
            return false;
        }
        if ($value === null) {
            $this->message = "O campo de Quantidade do Item N° {$this->numero_item_compra[$key]} " .
                "na Aba Itens do contrato de itens não pode estar vazio.";
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
