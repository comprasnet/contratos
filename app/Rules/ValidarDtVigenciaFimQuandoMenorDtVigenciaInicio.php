<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Carbon;

class ValidarDtVigenciaFimQuandoMenorDtVigenciaInicio implements Rule
{

    private $dataAssinaturaRescisao;
    private $vigenciaInicioContrato;
    private $vigenciaFimContrato;
    private $vigenciaFimRescisao;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($dataAssinaturaRescisao,$vigenciaInicioContrato, $vigenciaFimContrato, $vigenciaFimRescisao)
    {
        $this->dataAssinaturaRescisao = $dataAssinaturaRescisao;
        $this->vigenciaInicioContrato = $vigenciaInicioContrato;
        $this->vigenciaFimContrato    = $vigenciaFimContrato;
        $this->vigenciaFimRescisao    = $vigenciaFimRescisao;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(
            $this->dataAssinaturaRescisao === null ||
            $this->vigenciaFimRescisao === null ||
            $this->vigenciaFimContrato === null ||
            $this->vigenciaInicioContrato === null
        )
        {
            return false;
        }

        if($this->dataAssinaturaRescisao < $this->vigenciaInicioContrato && $this->vigenciaFimRescisao < $this->vigenciaInicioContrato)
        {
            return false;
        }

        if($this->dataAssinaturaRescisao < $this->vigenciaInicioContrato && $this->vigenciaFimRescisao > $this->vigenciaInicioContrato)
        {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $this->vigenciaInicioContrato    = Carbon::createFromFormat('Y-m-d', $this->vigenciaInicioContrato);

        return 'Em caso de rescisão anterior ao início da vigência do contrato, o campo Data Vig. Fim deve ser igual à' .
            " data de vigência início do contrato. (Vig. Inicio do contrato: {$this->vigenciaInicioContrato->format('d/m/Y')})";
    }
}
