<?php

namespace App\Rules;

use App\Http\Traits\Formatador;
use Illuminate\Contracts\Validation\Rule;

class VerificarValoresIrrisorios implements Rule
{
    use Formatador;

    private $message = 'Erro Genérico';
    private $texto = ' não possui saldo considerado irrisório. Favor escolher o tipo de operação "Anulação".';
    private $texto2 = ' é considerado irrisório. Favor escolher o tipo de operação "ANULAÇÃO SALDO IRRISÓRIO".';

    private $valorunitario;
    private $numero_item;
    private $tipo_alteracao;

    public function __construct($tipo_alteracao, $valorunitario, $numero_item)
    {
        $this->tipo_alteracao = $tipo_alteracao;
        $this->valorunitario = $valorunitario;
        $this->numero_item    = $numero_item;
    }

    public function passes($attribute, $value)
    {
        $key = substr($attribute, strrpos($attribute, '.') + 1);

        if (strpos($this->tipo_alteracao[$key], 'IRRISÓRIO') === false) {
            if (strpos($this->tipo_alteracao[$key], 'ANULAÇÃO') !== false
                && $this->retornaFormatoAmericano($value) < 0.00001 * $this->valorunitario[$key]) {
                $this->message = 'O valor informado para o item n° ' . $this->numero_item[$key] . $this->texto2;
                return false;
            }
            return true;
        }

        if ($this->retornaFormatoAmericano($value) < 0.00001 * $this->valorunitario[$key]) {
            return true;
        }
        $this->message = 'O item n° ' . $this->numero_item[$key] . $this->texto;

        return false;
    }

    public function message()
    {
        return $this->message;
    }
}
