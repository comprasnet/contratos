<?php

namespace App\Rules;

use App\Models\Contrato;
use App\Models\Contratohistorico;
use Illuminate\Contracts\Validation\Rule;


class NaoAceitarInativarContratoHistorico implements Rule
{
    private $id;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($id, $model = Contratohistorico::class)
    {
        $this->id = $id;
        $this->model = $model;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Não é permitido alterar um termo Ativo para "Em Elaboração".';
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        if ($this->id === 'NULL') {
            return true;
        }

        //se estiver tentando colocar como "Em Elaboração" e o histórico estiver como "Ativo", não deixa
        if ($value === '1') {
            $historico = $this->model::select(['elaboracao'])->find($this->id);
            return !(isset($historico->elaboracao) && $historico->elaboracao === false);
        }

        return true;

    }

}
