<?php

namespace App\Rules;

use App\Http\Traits\Formatador;
use Illuminate\Contracts\Validation\Rule;

class ValidarLogin implements Rule
{

    use Formatador;

    public function passes($attribute, $value)
    {
        if($this->validaCPF($value)){
            return true;
        }
        $this->message = 'CPF inválido.';
        return false;
    }

    public function message()
    {
        return $this->message ?? 'Erro genérico ao preencher formulário.';
    }
}
