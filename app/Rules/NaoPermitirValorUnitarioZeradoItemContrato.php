<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class NaoPermitirValorUnitarioZeradoItemContrato implements Rule
{
    private $item;
    public function __construct($item)
    {
        $this->item = $item;
    }

    public function passes($attribute, $value)
    {
        if ($value === '0') {
            $this->message = "Não é permitido item com o valor unitário zerado quando for selecionada Receita ou Despesa.";
            return false;
        }
        if ($value === null) {
            $this->message = "O campo de valor unitário " .
                "na Aba Itens do contrato de itens não pode estar vazio.";
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
