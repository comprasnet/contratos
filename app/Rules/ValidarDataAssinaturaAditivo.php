<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

/**
 * Create a new rule instance.
 *
 * @return void
 */
class ValidarDataAssinaturaAditivo implements Rule
{
    private $data_assinatura;
    private $vigencia_inicio;
    private $vigencia_fim;

    public function __construct($data_assinatura =null, $vigencia_inicio=null, $vigencia_fim=null)
    {
        $this->data_assinatura = $data_assinatura;
        $this->vigencia_inicio = $vigencia_inicio;
        $this->vigencia_fim = $vigencia_fim;
    }


    public function passes($attribute, $values)
    {
        if ($this->vigencia_inicio < $this->data_assinatura) {
            $this->message = 'Data de Assinatura do Aditivo deverá ser anterior a  Data Vigência Início!';
            return false;
        }

        if ($this->vigencia_fim < $this->data_assinatura) {
            $this->message = 'Data de Assinatura do Aditivo deverá ser anterior a Data Vigência Fim!';
            return false;
        }


        return true;
    }


    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message ?? 'Erro genérico ao preencher formulário.';
    }

}
