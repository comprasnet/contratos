<?php

namespace App\Rules;

use App\Models\Feriado;
use App\Models\Fornecedor;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Carbon;

class NaoAceitarFeriado implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        //verificacao para campos que nao sao obrigatorios
        if ($value === null) {
            return true;
        }

        $feriados = Feriado::select('data')->get()->toArray();
        $search = $value;

        $array = array_keys(
            array_filter(
                $feriados,
                function ($value) use ($search) {
                    return (strpos($value['data'], $search) !== false);
                }
            )
        );

        if (count($array)) {
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'A data de publicação escolhida não pode ser feriado.';
    }
}
