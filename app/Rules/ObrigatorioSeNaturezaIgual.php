<?php

namespace App\Rules;

use App\Models\MinutaEmpenho;
use Illuminate\Contracts\Validation\Rule;
use App\Http\Traits\Formatador;
use Illuminate\Support\Facades\DB;


class ObrigatorioSeNaturezaIgual implements Rule
{
    use Formatador;

    private $natureza_cipi;
    private $minuta_empenho;

    /**
     * Create a new rule instance.
     *
     * @param $minuta_id
     * @param $natureza_cipi
     */
    public function __construct($natureza_cipi, $minuta_empenho)
    {
        $this->natureza_cipi = $natureza_cipi;
        $this->minuta_empenho = $minuta_empenho;

    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {

        $minuta_id = $this->minuta_empenho->id ?? null;
        $natureza = MinutaEmpenho::join(
            'saldo_contabil',
            'saldo_contabil.id',
            '=',
            'minutaempenhos.saldo_contabil_id'
        )
            ->join(
                'naturezadespesa',
                'naturezadespesa.codigo',
                '=',
                DB::raw("SUBSTRING(saldo_contabil.conta_corrente,18,6)")
            )
            ->where('minutaempenhos.id', $minuta_id)
            ->whereIn('naturezadespesa.codigo', $this->natureza_cipi)
            ->select(
                [
                    DB::raw("SUBSTRING(saldo_contabil.conta_corrente,18,6) AS natureza_despesa")
                ])
            ->get();

        if (!$natureza->isEmpty() and !$value) {

            //Busca a unidade do usuário emissor da minuta.
            $unidade_usuario = $this->minuta_empenho->unidade_id()->first();

            //Unidades não-Sisg não tem a obrigatoriedade de informar o CIPI caso o amparo legal da compra seja diferente da Lei 14.133
            if (($unidade_usuario->sisg == false)) {
                return true;
            } else {
                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public
    function message()
    {
        return 'Campo obrigatório para a natureza da despesa do Crédito Orçamentário. ';
    }
}
