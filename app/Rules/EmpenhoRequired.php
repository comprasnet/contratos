<?php

namespace App\Rules;

use App\Models\Contrato;
use Illuminate\Contracts\Validation\Rule;

class EmpenhoRequired implements Rule
{

    public $contrato;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($contrato_id)
    {
        $this->contrato = Contrato::find($contrato_id);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        if ($value != null) {
            foreach ($value as $referenceField) {
                if (!isset($referenceField['empenho_id'])) {
                    $this->message = "O campo Empenho é obrigatório.";
                    return false;
                }

                if (!isset($referenceField['empenhovalorref'])) {
                    $this->message = "O campo Valor é obrigatório.";
                    return false;
                }

                if ($this->contrato->unidade->sisg) {
                    if (!isset($referenceField['empenhosubelemento']) || (isset($referenceField['empenhosubelemento']) && $referenceField['empenhosubelemento'] == '')) {
                        $this->message = "O campo Subelemento do empenho é obrigatório.";
                        return false;
                    }
                }
            }

            return true;
        }

        return !$this->contrato->unidade->sisg;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message ?? "Os campos Empenho e Valor são obrigatórios.";
    }
}
