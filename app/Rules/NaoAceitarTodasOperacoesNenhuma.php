<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class NaoAceitarTodasOperacoesNenhuma implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        $unique = array_unique($value);
        return !(str_contains($unique[0], 'NENHUMA') && count($unique) === 1);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return 'Não é possível avançar sem selecionar uma operação em ao menos um item.';
    }
}
