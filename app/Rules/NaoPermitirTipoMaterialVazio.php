<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class NaoPermitirTipoMaterialVazio implements Rule
{
    /**
     * @var string
     */
    private $message;
    private $tipo_material;
    private $tipo_item_id;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($tipo_material, $tipo_item_id)
    {
        $this->tipo_material = $tipo_material;
        $this->tipo_item_id = $tipo_item_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $key = substr($attribute, strpos($attribute, '.')+1);

        if ($value === null && $this->tipo_item_id[$key] == '149') {
            $this->message = 'O campo tipo material na aba '."Itens do contrato".' é obrigatório.';
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
