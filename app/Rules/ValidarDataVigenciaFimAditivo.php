<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

/**
 * Create a new rule instance.
 *
 * @return void
 */
class ValidarDataVigenciaFimAditivo implements Rule
{
    private $data_assinatura;
    private $vigencia_fim;

    public function __construct($vigencia_fim = null, $data_assinatura =null)
    {
        $this->data_assinatura = $data_assinatura;
        $this->vigencia_fim = $vigencia_fim;
    }


    public function passes($attribute, $values)
    {

        if ($this->vigencia_fim < $this->data_assinatura) {
            $this->message = 'A Data de Vigência Fim deverá ser posterior a Data de Assinatura do Aditivo!';
            return false;
        }



        return true;
    }


    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message ?? 'Erro genérico ao preencher formulário.';
    }

}
