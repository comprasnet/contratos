<?php

namespace App\Rules;

use App\Models\Contrato;
use App\Models\ContratoAntecipagov;
use Illuminate\Contracts\Validation\Rule;

class ValidarUltimoRegistroDomicilioBancario implements Rule
{

    /**
     * @var string
     */
    private $message;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        foreach ($value as $item) {
            $contrato = Contrato::find($item);

            $statusOperacaoUltimoRegistroDomicilioBancario = ContratoAntecipagov::where('contrato_id', $contrato->id)
                    ->latest('id')
                    ->first()
                    ->antecipagov
                    ->status_operacao ?? null;


            if ($statusOperacaoUltimoRegistroDomicilioBancario !== 'REGISTRAR' && // caso situacao seja LIQUIDAR
                $statusOperacaoUltimoRegistroDomicilioBancario !== 'ALTERAR' // caso situacao seja CANCELAR
            ) {
                $this->message = "Ação não permitida, " .
                    "último status da operação ($statusOperacaoUltimoRegistroDomicilioBancario)";

                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message ?? 'Erro genérico.';
    }
}
