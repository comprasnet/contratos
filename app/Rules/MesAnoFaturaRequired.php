<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class MesAnoFaturaRequired implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (isset($value)) {
            foreach ($value as $referenceField) {
                if (
                    !array_key_exists('mesref', $referenceField) ||
                    !array_key_exists('anoref', $referenceField) ||
                    !array_key_exists('valorref', $referenceField)
                ) {
                    return false;
                }
            }
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return null;
    }
}
