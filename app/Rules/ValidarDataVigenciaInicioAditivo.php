<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidarDataVigenciaInicioAditivo implements Rule
{
    private $fim_contrato;
    private $vigencia_inicio;

    public function __construct($fim_contrato = null, $vigencia_inicio =null)
    {
        $this->fim_contrato = $fim_contrato;
        $this->vigencia_inicio = $vigencia_inicio;
    }

    public function passes($attribute, $value)
    {
        if ($this->fim_contrato < $this->vigencia_inicio) {
            $this->message = 'A Data de Vigência Início deverá ser posterior a Data de Fim do Contrato!';
            return false;
        }

        return true;
    }

    public function message()
    {
        return $this->message ?? 'Erro genérico ao preencher formulário.';
    }
}
