<?php

namespace App\Rules;

use App\Http\Traits\BuscaCodigoItens;
use Illuminate\Contracts\Validation\Rule;

class ValidaChaveNfeIC implements Rule
{
    use BuscaCodigoItens;

    /**
     * @var array
     */
    private $codigo_item;

    public function __construct($codigo_item)
    {
        $this->codigo_item = $codigo_item;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        if ($this->codigo_item) {
            $descricao = $this->retornaDescricaoCodigoItemPorId($this->codigo_item);

            if ($descricao == 'Nota Fiscal Eletrônica' && empty($value)) {
                return false;
            }

            return true;
        }

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'O campo :attribute não pode estar vazio.';
    }
}
