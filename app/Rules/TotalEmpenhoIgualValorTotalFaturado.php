<?php

namespace App\Rules;

use App\Models\Contrato;
use Illuminate\Contracts\Validation\Rule;

class TotalEmpenhoIgualValorTotalFaturado implements Rule
{

    private $valorTotalFaturado;

    public $contrato;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($valorTotalFaturado, $contrato_id)
    {
        $this->valorTotalFaturado = $valorTotalFaturado;
        $this->contrato = Contrato::find($contrato_id);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(isset($value) && count($value) > 0){
            $arr_valorRef = array_map(function ($item) {
                return isset($item['empenhovalorref']) ? $this->returnNumeric($item['empenhovalorref']) : null;
            }, $value);

            $valorTotalMesReferencia = array_reduce($arr_valorRef, function ($previous, $current) {
                return $previous + $current;
            }, 0);

            $this->valorTotalFaturado = (float)$this->returnNumeric($this->valorTotalFaturado);

            return round($this->valorTotalFaturado, 2) === round((float)$valorTotalMesReferencia, 2);
        }

        return !$this->contrato->unidade->sisg;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'A soma dos valores dos empenhos devem corresponder ao Valor Total Faturado';
    }

    private function returnNumeric($stringValue)
    {
        return number_format(floatval(str_replace(',', '.', str_replace('.', '', $stringValue))), 2, '.', '');
    }
}
