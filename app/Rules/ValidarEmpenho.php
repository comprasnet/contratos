<?php

namespace App\Rules;

use App\Models\Empenho;
use App\services\EmpenhoService;
use Illuminate\Contracts\Validation\Rule;

class ValidarEmpenho implements Rule
{
    protected $dados;
    protected $message;
    protected $actionMethod;

    public function __construct($request, $actionMethod)
    {
        $this->dados = $request->all();
        $this->actionMethod = $actionMethod;
    }

    public function passes($attribute, $value)
    {
        $unidade = $this->dados['unidade_id'] ?? null;
        $numero = $this->dados['numero'] ?? null;
        $dataEmissao = $this->dados['data_emissao'] ?? null;
        $fornecedor = $this->dados['fornecedor_id'] ?? null;
        $naturezaDespesa = $this->dados['naturezadespesa_id'] ?? null;

        if (!$unidade || !$numero || !$dataEmissao || !$fornecedor || !$naturezaDespesa) {
            return false;
        }

        if (!EmpenhoService::checkIfEmpenhoIsInt($numero)) {
            $this->message = "Os 4 primeiros caracteres do empenho devem ser números inteiros.";
            return false;
        }

        if (!EmpenhoService::validaAnoEmpenho($numero, $this->getAnoMinino())) {
            $this->message = "O ano do empenho deve estar entre " . $this->getAnoMinino() . " e " . now()->year . ".";
            return false;
        }

        // Só faz essa validação se for store
        if (Empenho::where('numero', $numero)->where('unidade_id', $unidade)->exists()
            && $this->actionMethod == 'store') {
            $this->message = 'Este empenho já está vinculado a esta unidade!';
            return false;
        }

        return true;
    }

    private function getAnoMinino(){
        return now()->year - 10;
    }

    public function message()
    {
        return $this->message;
    }
}
