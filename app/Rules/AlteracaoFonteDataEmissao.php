<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class AlteracaoFonteDataEmissao implements Rule
{
    private $minuta_empenho;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($minuta_empenho)
    {
        $this->minuta_empenho = $minuta_empenho;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($this->minuta_empenho === null){
            return true;
        }

        return !($this->minuta_empenho->fonte_alterada && ($value != $this->minuta_empenho->data_emissao)) ;

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Não é possível alterar a data de emissão a partir de uma Alteração de Fonte.';
    }
}
