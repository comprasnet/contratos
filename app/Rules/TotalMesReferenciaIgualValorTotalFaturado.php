<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class TotalMesReferenciaIgualValorTotalFaturado implements Rule
{

    private $valorTotalFaturado;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($valorTotalFaturado)
    {
        $this->valorTotalFaturado = $valorTotalFaturado;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(is_array($value)){

            $arr_valorRef = array_map(function ($item) {
                return isset($item['valorref']) ? $this->returnNumeric($item['valorref']) : null;
            }, $value);

            $valorTotalMesReferencia = array_reduce($arr_valorRef, function ($previous, $current) {
               return $previous + $current;
            }, 0);

            $this->valorTotalFaturado = (float)$this->returnNumeric($this->valorTotalFaturado);

            return round($this->valorTotalFaturado, 2) === round((float)$valorTotalMesReferencia, 2);

        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        //A SOMA DOS VALORES DOS MESES DE REFERENCIA DEVEM CORRESPONDER AO VALOR TOTAL FATURADO
        return 'A soma dos valores dos meses de referência devem corresponder ao Valor Total Faturado';
    }

    private function returnNumeric($stringValue)
    {
        return number_format(floatval(str_replace(',', '.', str_replace('.', '', $stringValue))), 2, '.', '');
    }
}
