<?php
$urlPncp = env('API_PNCP_URL');
$urlContratosBase = 'v1/orgaos/{cnpj}/contratos';
$urlContratos = $urlContratosBase . '/{ano}/{sequencial}';
$urlContratosArquivos = $urlContratos . '/arquivos';
$urlContratosGarantias = $urlContratos . '/garantias';
$urlTermosContratoBase = $urlContratos . '/termos';
$urlTermoContrato = $urlTermosContratoBase . '/{sequencialTermoContrato}';
$urlTermoContratoArquivos = $urlTermoContrato . '/arquivos';

return [
    'url' => env('API_PNCP_URL'),

    # contratos
    'url-inserir-contrato' => $urlPncp . $urlContratosBase,
    'url-contratos' => $urlPncp . $urlContratos,

    # termos de contratos
    'url-termos-contrato-inserir-consultar' => $urlPncp . $urlTermosContratoBase,
    'url-termo-contrato-especifico' => $urlPncp . $urlTermoContrato,

    # arquivos de contratos
    'url-contratos-arquivos-inserir-consultar' => $urlPncp . $urlContratosArquivos,
    'url-contratos-arquivo-especifico' => $urlPncp . $urlContratosArquivos . '/{sequencialDocumento}',

    # garantias de contratos
    'url-contratos-garantias-inserir-consultar' => $urlPncp . $urlContratosGarantias,
    'url-contratos-garantia-especifica' => $urlPncp . $urlContratosGarantias . '/{sequencialGarantia}',

    # arquivos de termos de contratos
    'url-termo-contrato-arquivos-inserir-consultar' => $urlPncp . $urlTermoContratoArquivos,
    'url-termo-contrato-arquivo-especifico' => $urlPncp . $urlTermoContratoArquivos . '/{sequencialDocumento}',

    'tipo_contrato' => [
        '50' => '1',
        '52' => '2',
        '53' => '3',
        '54' => '4',
        '56' => '5',
        '57' => '6',
        '99' => '7',
        '98' => '8',
        '97' => '9',
        '96' => '10',
        '95' => '11',
        '94' => '12'
    ],

    'tipo_termo_contrato' => [
        '20' => '1', # Termo de Rescisão
        '55' => '2', # Termo Aditivo
        '60' => '3'  # Termo de Apostilamento
    ],

    'categoria_contrato' => [
        'Cessão' => '1',
        'Compras' => '2',
        'Informática (TIC)' => '3',
        'Internacional' => '4',
        'Locação Imóveis' => '5',
        'Mão de Obra' => '6',
        'Obras' => '7',
        'Serviços' => '8',
        'Serviços de Engenharia' => '9',
        'Serviços de Saúde' => '10',
        'Empenho Força de Contrato' => '11'
    ],

    'tipo_fornecedor' => [
        'IDGENERICO' => 'PE',
        'FISICA' => 'PF',
        'JURIDICA' => 'PJ',
        'UG' => 'PJ',
    ],

    'amparo_legal' => [
        '138' => '18',
        '139' => '19'
    ],

    'tipo_documento_contrato' => [
        'CONTRATO' => '12',
        'RESCISAO' => '13',
        'ADITIVO' => '14',
        'APOSTILA' => '15',
        'EMPENHO' => '17', # Nota de Empenho
        'REL_FINAL_CONTRATO' => '18',
        'ENCERRAMENTO' => '18', # Mantém a compatibilidade com o legado (será enviado como Relatório Final do Contrato)
        'OUTROS' => '16',
    ],

    'tipos_sei' => [
        'application/pdf;' => 'pdf',
        'text/html; charset=iso-8859-1' => 'html',
        'text/html;;charset=iso-8859-1' => 'html',
    ],

    'tipos_contrato_negados' => [
        'Termo Rescisão',
        'Termo de Rescisão',
        'Termo Aditivo',
        'Termo Apostilamento',
        'Termo de Apostilamento',
        'Credenciamento',
        'Outros'
    ],

    'retornos_de_erro_do_pncp_que_devem_ser_movidos_para_analise' => [
        'Data de assinatura do contrato anterior a 2021-04-01',
        'Ano do contrato deve ser igual ou posterior a 2021',
        'Identificador do CIPI do Contrato Inválido',
        'Compra não encontrada',
        'Serviço sobre ente 00000000000000 não autorizado para o usuário',
        'Operação não autorizada',
        'Ano da compra tem que ser menor ou igual que o ano atual',
        'Ano do contrato deve ser menor ou igual ao ano atual'
    ],

    # Quando o estado do registro é INCPEN, não é movido para análise, pois ainda precisa ser incluído
    # Porém, dependendo do erro, mesmo a inclusão não será realizada, assim os erros abaixo forçam a mudança
    'retornos_de_erro_do_pncp_que_forcam_situacao_para_analise' => [
        'Operação não autorizada',
    ],

    'tipos_contrato_aceitos_pncp' => [
        'Arrendamento',
        'Carta Contrato',
        'Comodato',
        'Concessão',
        'Contrato',
        'Empenho',
        'Outros',
        'Termo de Adesão'
    ],

    'tipos_termo_contrato_aceitos_pncp' => [
        'Termo Aditivo',
        'Termo de Apostilamento',
        'Termo de Rescisão'
    ],

    'token_expiration' => [
        'EXPIRES_IN' => 3600, # 1 hora (tempo que o PNCP expira o token)
        'SAFETY_MARGIN' => 360, # 6 minutos (10% de margem de segurança para evitar expiração do token)
        # O valor abaixo tem sempre que ser chamado com parênteses no final
        # Exemplo: config('api-pncp.token_expiration.CACHE_EXPIRES_IN')()
        'CACHE_EXPIRES_IN' => # 54 minutos (tempo para o contratos expirar o token)
            function () {
                return config('api-pncp.token_expiration.EXPIRES_IN') - config('api-pncp.token_expiration.SAFETY_MARGIN');
            },

    ],

];
