<?php

return [
    'api_host' => env('ACESSOGOV_API'),
    'host' => env('ACESSOGOV_HOST'),
    'client_id' => env('ACESSOGOV_CLIENT_ID'),
    'secret' => env('ACESSOGOV_SECRET'),
    'response_type' => 'code',
    'scope' => 'openid+email+phone+profile+govbr_confiabilidades',
    'data_acesso_somente_gov' => env('DATA_ACESSO_SOMENTE_GOV', '2024-11-01')
];
