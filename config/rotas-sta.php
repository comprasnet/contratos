<?php
return [
    'login' => '/api/login', # Autentica e retorna o token

    ### Estruturas ###
    'estrutura-natureza-despesa' => '/api/estrutura/naturezadespesas',
    'estrutura-unidades' => '/api/estrutura/unidades',
    'estrutura-orgaos' => '/api/estrutura/orgaos',
    'estrutura-orgaossuperiores' => '/api/estrutura/orgaossuperiores',

    ### Unidades com dados ###
    'unidades-com-rp-ano-dia' => '/api/unidade/rp/{ano}/{dia}',
    'unidades-com-empenho-ano-dia' => '/api/unidade/empenho/{ano}/{dia}',

    ### Restos a pagar ###
    'rp-por-ano-ug' => '/api/rp/ano/{ano}/ug/{ug}',
    'rp-por-ug' => '/api/rp/ug/{ug}',

    ### Empenhos ###
    'empenho-por-ano-ug-dia' => '/api/empenho/ano/{ano}/ug/{ug}/dia/{dia}',
    'empenho-por-ano-ug' => '/api/empenho/ano/{ano}/ug/{ug}',
    'empenho-por-numero' => '/api/empenho/',
    'empenho-dados-basicos' => '/api/empenho/dadosbasicos/',

    ### Saldo Contabil ###

    # Este caminho contém "get" para diferenciação, pois existe o mesmo caminho com "post"
    'saldocontabil-get-ano-ug-gestao-contascontabeis-contacorrente' =>
        '/api/saldocontabil/' .
        'ano/{ano}/ug/{ug}/gestao/{gestao}/contacontabil/{contacontabil}/contacorrente/{contacorrente}',

    'saldocontabil-por-ano-ug-gestao-contacontabil' =>
        '/api/saldocontabil/ano/{ano}/ug/{ug}/gestao/{gestao}/contacontabil/{contacontabil}',

    'saldocontabil-por-ano-ug-gestao' => '/api/saldocontabil/ano/{ano}/ug/{ug}/gestao/{gestao}',

    'saldocontabil-post-ano-ug-gestao-contascontabeis-contacorrente' => '/api/saldocontabil',

    ### Ordem Bancária ###
    'ordembancaria-por-documento' => '/api/ordembancaria/documento/{documento}',
    'ordembancaria-por-lote-documentos' => '/api/ordembancaria/documentolote'
];
