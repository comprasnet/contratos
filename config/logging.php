<?php

use Monolog\Handler\StreamHandler;
use Monolog\Handler\SyslogUdpHandler;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Log Channel
    |--------------------------------------------------------------------------
    |
    | This option defines the default log channel that gets used when writing
    | messages to the logs. The name specified in this option should match
    | one of the channels defined in the "channels" configuration array.
    |
    */

    'default' => env('LOG_CHANNEL', 'stack'),

    /*
    |--------------------------------------------------------------------------
    | Log Channels
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log channels for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Drivers: "single", "daily", "slack", "syslog",
    |                    "errorlog", "monolog",
    |                    "custom", "stack"
    |
    */

    'channels' => [
        'stack' => [
            'driver' => 'stack',
            'channels' => ['daily'],
        ],

        'single' => [
            'driver' => 'single',
            'path' => storage_path('logs/laravel.log'),
            'level' => 'debug',
        ],

        'schedule' => [
            'driver' => 'single',
            'path' => storage_path('logs/schedule-'.date('Y-m-d').'.log'),
            'level' => 'info',
        ],

        'daily' => [
            'driver' => 'daily',
            'tap' => [App\Logging\CustomizeFormatter::class],
            'path' => storage_path('logs/laravel.log'),
            'level' => 'debug',
            'days' => 14,
        ],
        'erro_migration' => [
            'driver' => 'daily',
            'path' => storage_path('logs/erro_migration.log'),
        ],
        'pncp' => [
            'driver' => 'daily',
            'path' => storage_path('logs/pncp-1.log'),
        ],
        'pncp_debug' => [
            'driver' => 'daily',
            'path' => storage_path('logs/pncp-debug.log'),
        ],
        'teste-pdf' => [
            'driver' => 'daily',
            'path' => storage_path('logs/teste-pdf.log')
        ],
        'minuta-empenho' => [
            'driver' => 'daily',
            'path' => storage_path('logs/minuta-empenho.log')
        ],
        'emissao-minuta-empenho' => [
            'driver' => 'daily',
            'path' => storage_path('logs/emissao-minuta-empenho.log')
        ],
        'execsiafi-soapfault' => [
            'driver' => 'daily',
            'path' => storage_path('logs/execsiafi-soapfault.log')
        ],
        'execsiafi-exception' => [
            'driver' => 'daily',
            'path' => storage_path('logs/execsiafi-exception.log')
        ],
        'novo_divulgacao_compra' => [
            'driver' => 'daily',
            'path' => storage_path('logs/novo_divulgacao_compra.log')
        ],
        'contrato' => [
            'driver' => 'daily',
            'path' => storage_path('logs/contrato.log')
        ],
        'slack' => [
            'driver' => 'slack',
            'url' => env('LOG_SLACK_WEBHOOK_URL'),
            'username' => 'Laravel Log',
            'emoji' => ':boom:',
            'level' => 'critical',
        ],

        'papertrail' => [
            'driver'  => 'monolog',
            'level' => 'debug',
            'handler' => SyslogUdpHandler::class,
            'handler_with' => [
                'host' => env('PAPERTRAIL_URL'),
                'port' => env('PAPERTRAIL_PORT'),
            ],
        ],

        'stderr' => [
            'driver' => 'monolog',
            'handler' => StreamHandler::class,
            'with' => [
                'stream' => 'php://stderr',
            ],
        ],

        'syslog' => [
            'driver' => 'syslog',
            'level' => 'debug',
        ],

        'errorlog' => [
            'driver' => 'errorlog',
            'level' => 'debug',
        ],

        'migracaoempenho' => [
            'driver' => 'daily',
            'path' => storage_path('logs/migracaoempenho.worker.log'),
            'level' => 'info',
        ],
        'migracaoempenho_job' => [
            'driver' => 'daily',
            'path' => storage_path('logs/migracaoempenho_job.worker.log'),
            'level' => 'info',
        ],

        'atualizanaturezadespesa' => [
            'driver' => 'daily',
            'path' => storage_path('logs/atualizanaturezadespesa.worker.log'),
            'level' => 'info',
        ],
        'atualizanaturezadespesa_job' => [
            'driver' => 'daily',
            'path' => storage_path('logs/atualizanaturezadespesa_job.worker.log'),
            'level' => 'info',
        ],

        'atualizasaldone' => [
            'driver' => 'daily',
            'path' => storage_path('logs/atualizasaldone.worker.log'),
            'level' => 'info',
        ],
        'atualizasaldone_job' => [
            'driver' => 'daily',
            'path' => storage_path('logs/atualizasaldone_job.worker.log'),
            'level' => 'info',
        ],
        'erro-wsdl' => [
            'driver' => 'daily',
            'path' => storage_path('logs/erro-wsdl.log')
        ],

    ],

];
