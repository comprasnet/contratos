<?php

use App\Models\Apropriacao;

return [
    # Mantém um mapeamento das informações utilizadas na gravação de um nonce
    # Os índices são identificados pela rota $request->route()->getAction()
    'actionInfo' => [
        'ApropriacaoInstrumentoCobrancaController' => [
            'nonceable_type' => Apropriacao::class,
            'funcionalidades' => [
                'apropriar' => "Apropriar instrumento de cobrança",
                'editarApropriacao' => "Editar Instrumento de Cobrança",
                'excluirApropriacao' => "Excluir Apropriação",
                'cancelarApropriacaoSiafi' => 'Cancelar Apropriacao Siafi'
            ]
        ],
    ]
];
