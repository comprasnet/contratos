-- Na tabela saldohistoricoitens, atualiza tipo_material = 'Permanente' quando natureza da despesa iniciar com 44 
UPDATE public.saldohistoricoitens shi
	SET tipo_material = 'Permanente'
	WHERE shi.tipo_material IS NULL
		AND '44' IN (
			SELECT LEFT(nd.codigo, 2) AS cod_natureza_despesa
				FROM 
					public.contratoitens ci
					LEFT JOIN public.contrato_minuta_empenho_pivot cmep ON cmep.contrato_id = ci.contrato_id
					LEFT JOIN public.compra_item_minuta_empenho cime ON cime.minutaempenho_id = cmep.minuta_empenho_id
					LEFT JOIN public.naturezasubitem nsi ON nsi.id = cime.subelemento_id
					LEFT JOIN public.naturezadespesa nd ON nd.id = nsi.naturezadespesa_id
				WHERE ci.id = shi.contratoitem_id
					AND ci.tipo_id = (SELECT cit.id FROM public.codigoitens cit WHERE cit.descricao = 'Material')
		)

;

-- Na tabela saldohistoricoitens, atualiza tipo_material = 'Consumo' quando natureza da despesa iniciar com 33
UPDATE public.saldohistoricoitens shi
	SET tipo_material = 'Consumo'
	WHERE shi.tipo_material IS NULL
		AND '33' IN (
			SELECT LEFT(nd.codigo, 2) AS cod_natureza_despesa
				FROM 
					public.contratoitens ci
					LEFT JOIN public.contrato_minuta_empenho_pivot cmep ON cmep.contrato_id = ci.contrato_id
					LEFT JOIN public.compra_item_minuta_empenho cime ON cime.minutaempenho_id = cmep.minuta_empenho_id
					LEFT JOIN public.naturezasubitem nsi ON nsi.id = cime.subelemento_id
					LEFT JOIN public.naturezadespesa nd ON nd.id = nsi.naturezadespesa_id
				WHERE ci.id = shi.contratoitem_id
					AND ci.tipo_id = (SELECT cit.id FROM public.codigoitens cit WHERE cit.descricao = 'Material')
		)
;
