<?php

use Illuminate\Database\Seeder;
use App\Models\Unidade;
use App\Models\Orgao;
use App\Models\Municipio;

class ImportaUnidadesEstadoSP extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/orgaos_unidades_sp/unidades_estado_sp.json');
        $dados = json_decode($json);

        foreach ($dados as $dado) {

            $orgao = Orgao::where('codigo',$dado->codigo_orgao)->first();
            $municipio = Municipio::where('codigo_ibge',$dado->codigo_municipio)->first();
            $esfera = $this->getEsfera($dado->esfera);
            $poder = $this->getPoder($dado->poder);
            $tipoAdmin = $this->getTipoAdmin($dado->tipo_administracao);

            Unidade::updateOrCreate(
            [
                'codigo' => $dado->codigo_unidade
            ],
            [
                'nome' => $dado->nome_unidade,
                'nomeresumido' => $dado->nome_unidade,
                'orgao_id' => $orgao->id,
                'tipo' => 'E',
                'situacao' => true,
                'aderiu_siasg' => ($dado->aderiu_siasg == 'S') ? true : false,
                'utiliza_siafi' => false,
                'codigosiasg' => $dado->codigo_unidade,
                'municipio_id' => $municipio->id,
                'esfera' => $esfera,
                'poder' => $poder,
                'tipo_adm' => $tipoAdmin,
                'cnpj' => ($dado->cnpj) ?: null,
                'codigosiafi' => $dado->codigo_unidade,
                'gestao' => $orgao->codigo,
                'exclusivo_gestao_atas' => false
            ]);
        }

    }

    private function getEsfera($esferaSiasg) {

        $esfera = '';

        switch ($esferaSiasg) {
            case 'F':
                $esfera = 'Federal';
                break;
            case 'E':
                $esfera = 'Estadual';
                break;
            case 'M':
                $esfera = 'Municipal';
                break;
        }
        return $esfera;

    }

    private function getPoder($poderSiasg) {

        $poder = '';

        switch ($poderSiasg) {
            case 0:
                $poder = 'Executivo';
                break;
            case 1:
                $poder = 'Legislativo';
                break;
            case 2:
                $poder = 'Judiciário';
                break;
        }
        return $poder;

    }

    private function getTipoAdmin($tipoAdminSiasg) {

        $tipoAdmin = '';

        switch ($tipoAdminSiasg) {
            case 1:
                $tipoAdmin = 'ADMINISTRAÇÃO DIRETA';
                break;
            case 2:
                $tipoAdmin = 'ESTATAL';
                break;
            case 3:
                $tipoAdmin = 'AUTARQUIA';
                break;
            case 4:
                $tipoAdmin = 'FUNDAÇÃO';
                break;
            case 5:
                $tipoAdmin = 'EMPRESA PÚBLICA COM. E FIN.';
                break;
            case 6:
                $tipoAdmin = 'ECONOMIA MISTA';
                break;
            case 7:
                $tipoAdmin = 'FUNDOS';
                break;
            case 8:
                $tipoAdmin = 'EMPRESA PUBLICA INDUSTRIAL E AGRICOLA';
                break;
            case 11:
                $tipoAdmin = 'ADMINISTRACAO DIRETA ESTADUAL';
                break;
            case 12:
                $tipoAdmin = 'ADMINISTRACAO DIRETA MUNICIPAL';
                break;
            case 13:
                $tipoAdmin = 'ADMINISTRACAO INDIRETA ESTADUAL';
                break;
            case 14:
                $tipoAdmin = 'ADMINISTRACAO INDIRETA MUNICIPAL';
                break;
            case 15:
                $tipoAdmin = 'EMPRESA PRIVADA';
                break;
        }
        return $tipoAdmin;

    }

}
