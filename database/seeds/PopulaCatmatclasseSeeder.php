<?php

use Illuminate\Database\Seeder;
use App\Models\Catmatsergrupo;
use App\Models\Catmatclasse;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class PopulaCatmatclasseSeeder extends Seeder
{
    public function run()
    {
        $json = File::get('database/data/catmat/classe_material.json');
        $dados = json_decode($json);

        foreach ($dados as $dado) {
            $grupo = Catmatsergrupo::where('codigo',$dado->codigo_grupo)
                ->where('tipo_id',149)
                ->first();

            Catmatclasse::updateOrCreate([
                'codigo' => $dado->codigo,
                'descricao' => $dado->descricao,
            ], [
                'catmatsergrupo_id' => $grupo->id,
            ]);
        }
    }
}
