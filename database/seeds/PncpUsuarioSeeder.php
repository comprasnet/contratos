<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class PncpUsuarioSeederTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('pncp_usuarios')->insert(
            ['cpfCnpj' => '65558723149', 
            'nome' => 'Cleiton Pontes', 
            'email' => 'cleiton.pontes@economia.gov.br',
            'administrador' => false,
            'token' => 'token',
            'senha' => 'senhasenhasenhasenha',
            'ativo' => true,
            'id_pncp' => 3
        ]);
        
    }
}
