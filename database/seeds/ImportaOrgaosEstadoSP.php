<?php

use Illuminate\Database\Seeder;
use App\Models\Orgao;
use App\Models\OrgaoSuperior;

class ImportaOrgaosEstadoSP extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/orgaos_unidades_sp/orgaos_estado_sp.json');
        $dados = json_decode($json);

        $orgao_superior = OrgaoSuperior::updateOrCreate(
            [
                'codigo' => '95420'
            ],
            [
                'nome' => 'ESTADO DE SAO PAULO',
                'situacao' => true
            ]);

        foreach ($dados as $dado) {
            Orgao::updateOrCreate(
            [
                'codigo' => $dado->codigo_orgao
            ],
            [
                'nome' => $dado->nome,
                'orgaosuperior_id' => $orgao_superior->id,
                'codigosiasg' => $dado->codigo_orgao,
                'cnpj' => $dado->cnpj,
                'situacao' => true
            ]);
        }

    }
}
