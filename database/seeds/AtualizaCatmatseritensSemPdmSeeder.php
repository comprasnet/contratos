<?php

use Illuminate\Database\Seeder;
use App\Models\Catmatseritem;
use App\Models\Catmatpdms;
use App\Models\Catmatclasse;
use App\Models\Catmatsergrupo;
use Illuminate\Support\Facades\DB;

class AtualizaCatmatseritensSemPdmSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $json = File::get('database/data/catmat/itens_material_sem_pdm.json');
        $dados = json_decode($json);

        DB::beginTransaction();
        try {

            foreach ($dados as $dado) {

                $grupo = Catmatsergrupo::updateOrCreate([
                    'codigo' => $dado->codigo_grupo,
                    'tipo_id' => 149
                ], [
                    'descricao' => $dado->nome_grupo
                ]);

                $classe = Catmatclasse::updateOrCreate([
                    'codigo' => $dado->codigo_classe,
                    'catmatsergrupo_id' => $grupo->id
                ], [
                    'descricao' => $dado->nome_classe
                ]);

                $pdm = Catmatpdms::updateOrCreate([
                    'codigo' => $dado->codigo_pdm,
                    'catmatclasse_id' => $classe->id,
                ], [
                    'descricao' => $dado->nome_pdm
                ]);

                $cmsi = Catmatseritem::where('codigo_siasg', $dado->codigo_item)
                ->where('grupo_id', 194)
                ->update([
                    'catmatpdm_id' => $pdm->id
                ]);

            }
            
            DB::commit();
        } catch (Exception $e) {

            DB::rollback();
            die($e->getMessage());
        }
    }
}
