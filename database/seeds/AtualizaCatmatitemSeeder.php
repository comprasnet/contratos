<?php

use Illuminate\Database\Seeder;
use App\Models\Catmatpdms;
use App\Models\Catmatseritem;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class AtualizaCatmatitemSeeder extends Seeder
{
    public function run()
    {

        ini_set('memory_limit','200M');


        $json = File::get('database/data/catmat/pdm_material_item.json');
        $dados = json_decode($json);

        foreach ($dados as $dado) {
            $pdm = Catmatpdms::where('codigo',$dado->codigo_pdm)
                ->first();

            Catmatseritem::updateOrCreate([
                'codigo_siasg' => $dado->codigo,
                'grupo_id' => 194,
            ], [
                'descricao' => $dado->descricao,
                'catmatpdm_id' => $pdm->id,
            ]);
        }
    }
}
