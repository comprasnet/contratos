<?php

use Illuminate\Database\Seeder;
use App\Models\Catmatclasse;
use App\Models\Catmatpdms;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class PopulaCatmatpdmSeeder extends Seeder
{
    public function run()
    {
        $json = File::get('database/data/catmat/pdm_material.json');
        $dados = json_decode($json);

        foreach ($dados as $dado) {
            $classe = Catmatclasse::where('codigo',$dado->codigo_classe)
                ->first();

            Catmatpdms::updateOrCreate([
                'codigo' => $dado->codigo,
                'descricao' => $dado->descricao,
            ], [
                'catmatclasse_id' => $classe->id,
            ]);
        }
    }
}
