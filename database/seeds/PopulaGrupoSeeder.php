<?php

use Illuminate\Database\Seeder;
use App\Models\Catmatsergrupo;

class PopulaGrupoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/catmat/grupo_material.json');
        $dados = json_decode($json);

        foreach ($dados as $dado) {
            Catmatsergrupo::updateOrCreate([
                'descricao' => $dado->descricao,
                'tipo_id' => 149,
            ], [
                'codigo' => $dado->codigo
            ]);
        }
    }
}
