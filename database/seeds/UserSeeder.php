<?php

use App\Models\BackpackUser;
use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class UserSeeder extends Seeder
{
    public function run()
    {
        $usuarioAdministrador = BackpackUser::where("cpf","000.000.001-91")->first();

        if(empty($usuarioAdministrador->id)) {
            $user = \App\Models\BackpackUser::create([
                'name' => 'USUÁRIO ADMINISTRADOR',
                'cpf' => '000.000.001-91',
                'email' => 'admin@contratos.gov.br',
                'ugprimaria' => '1',
                'password' => bcrypt('123456'),
            ]);
            $user->assignRole('Administrador');
        }



    }
}