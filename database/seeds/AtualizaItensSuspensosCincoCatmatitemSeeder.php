<?php

use Illuminate\Database\Seeder;
use App\Models\Catmatpdms;
use App\Models\Catmatseritem;

// composer require laracasts/testdummy
use Illuminate\Support\Facades\DB;
use Laracasts\TestDummy\Factory as TestDummy;
use Illuminate\Support\Collection;

class AtualizaItensSuspensosCincoCatmatitemSeeder extends Seeder
{
    public function run()
    {

        ini_set('memory_limit', '200M');

        $json = File::get('database/data/catmat/itens_catalogo_sem_bom_5.json');
        $dados = json_decode($json);
        DB::beginTransaction();

        try {
            foreach ($dados as $dado) {
                $pdm = Catmatseritem::where('catmatseritens.codigo_siasg', $dado->cod_item_material)
                    ->where('codigoitens.descricao', 'Material')
                    ->whereNull('catmatseritens.catmatpdm_id')
                    ->join('catmatsergrupos', 'catmatsergrupos.id', '=', 'catmatseritens.grupo_id')
                    ->join('codigoitens', 'catmatsergrupos.tipo_id', '=', 'codigoitens.id')
                    ->first();

                $length = strlen($dado->codigo_pdm);

                if ($length < 5) {
                    $dado->codigo_pdm = str_pad($dado->codigo_pdm, 5, '0', STR_PAD_LEFT);
                }

                $pdmsTable = Catmatpdms::where('codigo', $dado->codigo_pdm)->first();

                if ($pdm && $pdmsTable) {
                    $up = Catmatseritem::where('codigo_siasg', $pdm->codigo_siasg)
                        ->where('grupo_id', 194)
                        ->update([
                            'catmatpdm_id' => $pdmsTable->id
                        ]);
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            die($e->getMessage());
        }


    }
}
