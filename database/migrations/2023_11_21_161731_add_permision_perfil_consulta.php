<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPermisionPerfilConsulta extends Migration
{
    private $permissoesPerfilConsulta = [
        'arp_criar',
        'arp_editar',
        'arp_visualizar',
        'arp_deletar',
        'arp_remanejamento_criar',
        'arp_remanejamento_editar',
        'arp_remanejamento_visualizar',
        'arp_remanejamento_deletar',
        'arp_adesao_criar',
        'arp_adesao_visualizar',
        'arp_adesao_deletar',
        'arp_adesao_editar'
    ];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $perfis = \App\Models\Role::whereIN("name", ['Administrador', 'Consulta', 'Administrador Suporte'])->get();

        foreach ($this->permissoesPerfilConsulta as $permissao) {
            \App\Models\Permission::updateOrCreate(['name' => $permissao, 'guard_name' => 'web']);
            foreach ($perfis as $perfil) {
                $perfil->givePermissionTo($permissao);
            }
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
