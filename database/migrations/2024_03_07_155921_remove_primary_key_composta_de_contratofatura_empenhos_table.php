<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemovePrimaryKeyCompostaDeContratofaturaEmpenhosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contratofatura_empenhos', function (Blueprint $table) {
            $table->dropPrimary(['contratofatura_id', 'empenho_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contratofatura_empenhos', function (Blueprint $table) {
            $table->primary(['contratofatura_id', 'empenho_id']);
        });
    }
}
