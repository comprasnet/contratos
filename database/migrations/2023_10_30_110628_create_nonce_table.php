<?php
 
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
 
class CreateNonceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nonce', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id');
            $table->string('nonce', 100);
            $table->string('nonceable_type', 255)->nullable();
            $table->bigInteger('nonceable_id')->nullable();
            $table->string('ip', 100);
            $table->boolean('status')->default(true);
            $table->string('funcionalidade', 255);
            $table->text('json_request');
            $table->text('json_response');
            $table->timestamps();
 
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
           
            $table->unique(['user_id', 'nonce', 'nonceable_type']);
 
        });
    }
 
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nonce');
    }
}
 