<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterContratofaturasCreateNfeField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contratofaturas', function (Blueprint $table) {
            $table->string('chave_nfe')->nullable()->after('numero');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contratofaturas', function (Blueprint $table) {
            $table->dropColumn('chave_nfe');
        });
    }
}
