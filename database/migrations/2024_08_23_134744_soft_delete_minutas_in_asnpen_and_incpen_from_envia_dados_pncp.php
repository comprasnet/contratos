<?php

use App\Models\EnviaDadosPncp;
use App\Models\MinutaEmpenho;
use App\Models\Codigoitem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SoftDeleteMinutasInAsnpenAndIncpenFromEnviaDadosPncp extends Migration
{
    public function up(): void
    {
        $idsSituacao = Codigoitem::whereIn('descres', ['ASNPEN', 'INCPEN'])->pluck('id');
        EnviaDadosPncp::where('pncpable_type', MinutaEmpenho::class)
            ->whereIn('situacao', $idsSituacao)
            ->delete();
    }

    public function down(): void
    {
        //
    }
}
