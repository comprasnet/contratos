<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContratofaturasMesAnoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contratofaturas_mes_ano', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contratofaturas_id');
            $table->string('mesref');
            $table->string('anoref');
            $table->decimal('valorref', 17,2);
            $table->json('mesref_anoref_valor_json')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('contratofaturas_id')->references('id')->on('contratofaturas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contratofaturas_mes_ano');
    }
}
