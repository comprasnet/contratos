<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAmparoLegalAddColumnEnviarPncp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('amparo_legal', function(Blueprint $table) {
            $table->boolean('complemento_14133')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('amparo_legal', function(Blueprint $table) {
            $table->dropColumn('complemento_14133');
        });
    }
}
