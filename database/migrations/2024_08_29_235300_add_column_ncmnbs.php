<?php

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnNcmnbs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('compra_items', function (Blueprint $table) {
            $table->string('codigo_ncmnbs')->nullable();
            $table->boolean('aplica_margem_ncmnbs')->default(false);
            $table->string('descricao_ncmnbs')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('compra_items', function (Blueprint $table) {
            $table->dropColumn(['codigo_ncmnbs', 'aplica_margem_ncmnbs', 'descricao_ncmnbs']);
        });
    }
}
