<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContratofaturaEmpenhosApropriacaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contrato_fatura_empenhos_apropriacao', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contratofatura_id')->unsigned();
            $table->integer('empenho_id')->unsigned();
            $table->integer('sfpcoitem_id')->unsigned();
            $table->foreign('contratofatura_id')->references('id')->on('contratofaturas')->onDelete('cascade');
            $table->foreign('sfpcoitem_id')->references('id')->on('sfpcoitem')->onDelete('cascade');
            $table->foreign('empenho_id')->references('id')->on('empenhos')->onDelete('cascade');
            $table->decimal('valorref', 17, 2)->nullable();
            $table->boolean('apropriado_siafi')->nullable()->default(false)->comment('Verifica se deu sucesso na integração do siafi.');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contrato_fatura_empenhos_apropriacao');
    }
}
