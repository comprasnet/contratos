<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Codigo;
use App\Models\Codigoitem;

class InsertTipoDhSiafiCodigoitens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = Codigo::firstOrCreate([
            'descricao' => 'Tipos DH SIAFI',
            'visivel' => true
        ]);

        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'AA','descricao' => 'ATAS DE ASSEMBLEIAS','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'AP','descricao' => 'DOCUMENTO DE APORTE','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'AV','descricao' => 'AUTORIZACAO DE VIAGENS (DIARIAS)','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'BS','descricao' => 'BOLETIM DE SUBSCRICAO','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'CE','descricao' => 'CONTRATO DE EMPRESTIMOS CONCEDIDOS','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'CP','descricao' => 'CARTAO DE PAGAMENTO','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'CV','descricao' => 'RECLASSIFICACAO DE DESPESAS-PORTAL/SICONV','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'DA','descricao' => 'DOCUMENTO DE DESAPORTE','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'DB','descricao' => 'DEVOLUCAO DE OB CANCELADA','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'DD','descricao' => 'DEVOLUCAO DE DESPESAS','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'DP','descricao' => 'DIVIDA PUBLICA','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'DT','descricao' => 'DOCUMENTO DE RECOLHIMENTO/REEMBOLSO','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'DU','descricao' => 'DEVOLUCAO DE DESPESAS E DE OB CANCELADA - SUPRIMENTO DE FUNDOS','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'DV','descricao' => 'DEVOLUCAO DE TRANSFERENCIAS VOLUNTARIAS - PORTAL SICONV','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'EC','descricao' => 'CONTRATO DE EMPRESTIMOS CONCEDIDOS','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'FL','descricao' => 'FOLHA DE PAGAMENTO','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'IT','descricao' => 'INTEGRACAO DE ROTINAS EXECUTADAS FORA DA CONTA UNICA','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'NP','descricao' => 'NOTA DE PAGAMENTO','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'PA','descricao' => 'LANCAMENTOS PATRIMONIAIS','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'PC','descricao' => 'PROCESSO ADMINISTRATIVO','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'PI','descricao' => 'PAGAMENTO DE INDENIZACOES','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'PL','descricao' => 'PATRIMONIO LIQUIDO','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'PO','descricao' => 'LANCAMENTOS PATRIMONIAIS E ORCAMENTARIOS','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'PS','descricao' => 'PARTICIPACOES SOCIETARIAS','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'PV','descricao' => 'LANCAMENTOS PATRIMONIAIS DE TV','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'PX','descricao' => 'REGISTRO DE INGRESSOS NA CONTA DE PAGAMENTOS INSTANTANEIOS','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'RB','descricao' => 'REEMBOLSO DE DESPESA','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'RC','descricao' => 'REGISTROS DE CONTROLES DIVERSOS','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'RP','descricao' => 'RECIBO DE PAGAMENTO','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'RR','descricao' => 'RECIBO EMITIDO','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'RS','descricao' => 'PAGAMENTO DE RESTITUICOES','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'SF','descricao' => 'SUPRIMENTO DE FUNDOS','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'SJ','descricao' => 'SENTENCAS JUDICIAIS/PRECATORIOS','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'TB','descricao' => 'SENTENCAS TRABALHISTAS','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'TF','descricao' => 'TRANSFERENCIAS FINANCEIRAS','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'TV','descricao' => 'TRANSFERENCIAS VOLUNTARIAS','visivel' => true]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Codigo::where([
            'descricao' => 'Tipos DH SIAFI',
            'visivel' => true
        ])->forceDelete();
    }
}
