<?php

use App\Http\Traits\BuscaCodigoItens;
use App\Models\Codigoitem;
use App\Models\Execsfsituacao;
use App\Models\ExecsfsituacaoCamposVariaveis;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPossuiacrescimoDeducaotable extends Migration
{
    use BuscaCodigoItens;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('sfdeducao', 'possui_acrescimo')) {
            Schema::table('sfdeducao', function (Blueprint $table) {
                $table->boolean('possui_acrescimo')->default(false);
            });

            $tipo_situacao = Codigoitem::whereHas('codigo', function ($c) {
                $c->where('descricao', 'Tipos Situações DH SIAFI');
            })
                ->where('descres', 'SIT0007')
                ->first();

            $situacao = Execsfsituacao::updateOrCreate(
                ['codigo' => 'DDR001'],
                [
                    'descricao' => 'RETENÇÕES DE IMPOSTOS RECOLHÍVEIS POR DAR',
                    'aba' => 'DEDUCAO',
                    'tipo_situacao' => $tipo_situacao->id,
                    'afeta_custo' => false,
                    'permite_contrato' => false
                ]
            );

            ExecsfsituacaoCamposVariaveis::updateOrCreate(
                [
                    'execsfsituacao_id' => $situacao->id,
                    'campo' => 'txtinscra'
                ],
                [
                    'aba' => 'DEDUCAO',
                    'campo' => 'txtinscra',
                    'tipo' => '	Município',
                    'rotulo' => 'Código do Município',
                    'restricao' => 'Sem Restrição'
                ]
            );

            ExecsfsituacaoCamposVariaveis::updateOrCreate(
                [
                    'execsfsituacao_id' => $situacao->id,
                    'campo' => 'txtinscrb'
                ],
                [
                    'aba' => 'DEDUCAO',
                    'campo' => 'txtinscrb',
                    'tipo' => '	Código de Recolhimento DAR',
                    'rotulo' => 'Código de Receita',
                    'restricao' => 'Sem Restrição'
                ]
            );

            ExecsfsituacaoCamposVariaveis::updateOrCreate(
                [
                    'execsfsituacao_id' => $situacao->id,
                    'campo' => 'numclassa'
                ],
                [
                    'aba' => 'DEDUCAO',
                    'campo' => 'numclassa',
                    'tipo' => 'Classificação Contábil',
                    'mascara' => '3424XXX00',
                    'rotulo' => 'VPD de Multa ou Encargos Tributários',
                    'restricao' => 'Sem Restrição'
                ]
            );

            $codNp = $this->retornaIdCodigoItemPorDescricaoEDescres('Tipos DH SIAFI', 'NP');

            $situacao->tipodocs()->sync([$codNp]);
        }

        if (!Schema::hasColumn('sfrelitemded', 'sfpcoitem_id')) {
            Schema::table('sfrelitemded', function (Blueprint $table) {
                $table->integer('sfpcoitem_id')->unsigned()->nullable();
                $table->foreign('sfpcoitem_id')->references('id')->on('sfpcoitem')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('sfdeducao', 'possui_acrescimo')) {
            Schema::table('sfdeducao', function (Blueprint $table) {
                $table->dropColumn('possui_acrescimo');
            });
        }

        if (Schema::hasColumn('sfrelitemded', 'sfpcoitem_id')) {
            Schema::table('sfrelitemded', function (Blueprint $table) {
                $table->integer('sfpcoitem_id')->unsigned();
                $table->foreign('sfpcoitem_id')->references('id')->on('sfpcoitem')->onDelete('cascade');
            });
        }
    }
}
