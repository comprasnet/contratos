<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnaliseEmpenhoZeradoTable extends Migration
{
    public function up()
    {
        Schema::create('analise_empenho_zerado', function (Blueprint $table) {
            $table->string('unidade_codigo');
            $table->unsignedBigInteger('empenho_id');
            $table->string('empenho_numero');
            $table->unsignedBigInteger('empenhodetalhado_id');

            $table->decimal('emp_empenhado', 17, 2)->nullable();
            $table->decimal('emp_aliquidar', 17, 2)->nullable();
            $table->decimal('emp_liquidado', 17, 2)->nullable();
            $table->decimal('emp_pago', 17, 2)->nullable();

            $table->decimal('ed_empaliquidar', 17, 2)->nullable();
            $table->decimal('ed_empemliquidacao', 17, 2)->nullable();
            $table->decimal('ed_emprpp', 17, 2)->nullable();
            $table->decimal('ed_empliquidado', 17, 2)->nullable();
            $table->decimal('ed_empaliqrpnp', 17, 2)->nullable();
            $table->decimal('ed_empemliqrpnp', 17, 2)->nullable();
            $table->decimal('ed_emppago', 17, 2)->nullable();

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('analise_empenho_zerado');
    }
}
