<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \App\Models\AmparoLegal;

class ChangeAmparoLegalEnviarPncp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->alterarColunaAmparoLegalEnviarPnpcp(true);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->alterarColunaAmparoLegalEnviarPnpcp(false);
    }

    private function alterarColunaAmparoLegalEnviarPnpcp(bool $amparoLegalEnviarPncp)
    {
        $amparoLegalLeiQuatorzeUmTresTres = AmparoLegal::where('ato_normativo', 'ilike', '%14.133%')->get();

        foreach ($amparoLegalLeiQuatorzeUmTresTres as $amparoLegal) {
            $amparoLegal->complemento_14133 = $amparoLegalEnviarPncp;
            $amparoLegal->save();
        }
    }
}
