<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSituacaoProcessamentoMinutaEmpenhoPncp extends Migration
{
    private $descricaoCodigoItem = 'Buscar Minuta de Empenho para enviar ao PNCP';
    private function getIdCodigoPNCP()
    {
        $codigoPncp = \App\Models\Codigo::where("descricao", "Situação Envia Dados PNCP")->first();
        return $codigoPncp->id;
    }
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \App\Models\Codigoitem::create([
            'codigo_id' => $this->getIdCodigoPNCP() ,
            'descres' => 'ASNPEN_PNCP',
            'descricao' => $this->descricaoCodigoItem ,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $codigoId = $this->getIdCodigoPNCP();

        \App\Models\Codigoitem::where("codigo_id", $codigoId)
            ->where("descricao", $this->descricaoCodigoItem)
            ->delete();
    }
}
