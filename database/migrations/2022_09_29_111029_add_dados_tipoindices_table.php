<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDadosTipoindicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Adiciona índices que retornarão do Banco Central
        \Illuminate\Support\Facades\DB::table('tipo_indices')->updateOrInsert([
            'codigo' => '189',
            'nome' => 'IGP-M',
            'descricao' => 'IGP-M - Índ. Geral de Preços do Mercado',
            'fonte' => 'bacen',
            'forma_calculo' => 'auto'],
            [
                'created_at' => now(),
                'updated_at' => now()
            ]);
        \Illuminate\Support\Facades\DB::table('tipo_indices')->updateOrInsert([
            'codigo' => '433',
            'nome' => 'IPCA',
            'descricao' => 'IPCA - Índ. de Preços ao Consumidor Amplo',
            'fonte' => 'bacen',
            'forma_calculo' => 'auto'],
            [
                'created_at' => now(),
                'updated_at' => now()
            ]);
        \Illuminate\Support\Facades\DB::table('tipo_indices')->updateOrInsert([
            'codigo' => '192',
            'nome' => 'INCC',
            'descricao' => 'INCC - Índ. Nacional de Custo da Construção',
            'fonte' => 'bacen',
            'forma_calculo' => 'auto'],
            [
                'created_at' => now(),
                'updated_at' => now()
            ]);
        \Illuminate\Support\Facades\DB::table('tipo_indices')->updateOrInsert([
            'codigo' => '188',
            'nome' => 'INPC',
            'descricao' => 'INPC - Índ. Nacional de Preços ao Consumidor',
            'fonte' => 'bacen',
            'forma_calculo' => 'auto'],
            [
                'created_at' => now(),
                'updated_at' => now()
            ]);
        // retirado índice INCC-M pois somente teve valor informado até 12/2012 e que após 01/2013 passou a vir vazio
        //
        //\Illuminate\Support\Facades\DB::table('tipo_indices')->updateOrInsert([
        //    'codigo' => '7456',
        //    'nome' => 'INCC-M',
        //    'descricao' => 'INCC-M - Índ. Nacional de Custo da Construção',
        //    'fonte' => 'bacen',
        //    'forma_calculo' => 'auto'],
        //    [
        //        'created_at' => now(),
        //        'updated_at' => now()
        //    ]);
        \Illuminate\Support\Facades\DB::table('tipo_indices')->updateOrInsert([
            'codigo' => '9998',
            'nome' => 'IST',
            'descricao' => 'IST - Índ. de Serviços de Telecomunicações - ANATEL ',
            'fonte' => 'outras',
            'forma_calculo' => 'personalizado'],
            [
                'created_at' => now(),
                'updated_at' => now()
            ]);
        // retirado índice IGP-M Acumulado (FGV) devido a erro 500 e a existência do índice IGP-M
        //
        //\Illuminate\Support\Facades\DB::table('tipo_indices')->updateOrInsert([
        //    'codigo' => '9999',
        //    'nome' => 'IGP-M Acumulado (FGV)', //Manual de Engenharia
        //    'descricao' => 'IGP-M (FGV) - Índ. Geral de Preços do Mercado Acumulado',
        //    'fonte' => 'outras',
        //    'forma_calculo' => 'personalizado'],
        //    [
        //        'created_at' => now(),
        //        'updated_at' => now()
        //    ]);
        \Illuminate\Support\Facades\DB::table('tipo_indices')->updateOrInsert([
            'codigo' => '9997',
            'nome' => 'ICTI',
            'descricao' => 'ICTI -  Índ. de Custo da Tecnologia da Informação (IPEA)',
            'fonte' => 'outras',
            'forma_calculo' => 'auto'],
            [
                'created_at' => now(),
                'updated_at' => now()
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
