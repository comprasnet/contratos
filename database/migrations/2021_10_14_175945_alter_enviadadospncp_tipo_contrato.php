<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterEnviadadospncpTipoContrato extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('envia_dados_pncp', function (Blueprint $table) {
            $table->string('tipo_contrato')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('envia_dados_pncp', function (Blueprint $table) {
            $table->dropColumn('tipo_contrato');
        });
    }
}
