<?php

use App\Models\Compra;
use App\services\CompraService;
use Illuminate\Database\Migrations\Migration;
use \App\Models\Contrato;
use \App\Http\Traits\ConsultaCompra;
use \Illuminate\Support\Facades\Log;
use \App\services\ContratoService;
use \Illuminate\Support\Facades\DB;
use \App\Http\Traits\CompraTrait;
use \App\Http\Traits\BuscaCodigoItens;
use \App\Models\EnviaDadosPncp;

class SalvarCompraContratoCadastradoEnvioPncp extends Migration
{
    use ConsultaCompra;
    use CompraTrait;
    use BuscaCodigoItens;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $contratos = Contrato::select(
            'contratos.id',
            'envia_dados_pncp.id as envia_dados_pncp_id',
            'contratos.licitacao_numero',
            'contratos.unidadecompra_id',
            'contratos.modalidade_id')
            ->join('envia_dados_pncp', function ($join) {
                $join->on('contratos.id', '=', 'envia_dados_pncp.contrato_id')
                    ->where('envia_dados_pncp.pncpable_type', '=', 'App\Models\Contratohistorico')
                    ->whereNull('envia_dados_pncp.deleted_at');
            })
            ->leftJoin('compras', function ($join) {
                $join->on('contratos.licitacao_numero', '=', 'compras.numero_ano')
                    ->on('contratos.unidadecompra_id', '=', 'compras.unidade_origem_id')
                    ->on('contratos.modalidade_id', '=', 'compras.modalidade_id');
            })
            ->whereNull('compras.id')
            ->orderBy('contratos.created_at', 'desc')
            ->get();

        $countSucesso = 0;
        $countErro = 0;
        $contratoService = app(ContratoService::class);
        $compraService = app(CompraService::class);

        foreach ($contratos as $contrato) {
            try {
                $modalidade = $contrato->modalidade->descres;
                $uasgCompra = $contrato->unidadecompra->codigo;
                $uasgUser = $contrato->unidadecompra->codigo;
                $uasgBeneficiaria = $contrato->unidadebeneficiaria->codigo ?? null;
                $arrayCompra = explode('/', $contrato->licitacao_numero);
                $numeroCompra = $arrayCompra[0];
                $anoCompra = $arrayCompra[1];

                $mensagemLog = "IdContrato: {$contrato->id} Modalidade: {$modalidade} UasgCompra: {$uasgCompra}
                UasgUser: {$uasgUser} UasgBeneficiaria: {$uasgBeneficiaria} numeCompra: {$numeroCompra}
                ano_compra:{$anoCompra} enviaDadosPncpId {$contrato->envia_dados_pncp_id}";

                if (strlen($modalidade) > 2) {
                    if ($modalidade == 'NAOSEAPLIC') {
                        $enviaDadosPncp = EnviaDadosPncp::find($contrato->envia_dados_pncp_id);
                        $enviaDadosPncp->retorno_pncp = "Removido devido a modalidade ser  Não se aplica";
                        $enviaDadosPncp->save();

                        $enviaDadosPncp->delete();
                        Log::warning("Registro removido de forma lógica para enviar ao PNCP");
                        Log::warning($mensagemLog);

                        $countErro++;
                        continue;
                    }

                    $enviaDadosPncp = EnviaDadosPncp::find($contrato->envia_dados_pncp_id);
                    $enviaDadosPncp->retorno_pncp = "Removido devido a modalidade ser mais de dois dígitos";
                    $enviaDadosPncp->save();

                    $enviaDadosPncp->delete();
                    Log::warning("Registro removido de forma lógica para enviar ao PNCP");
                    Log::warning($mensagemLog);

                    $countErro++;
                    continue;
                }

                $retornoSiasg = $this->buscarCompra(
                    $modalidade,
                    $uasgCompra,
                    $uasgUser,
                    $uasgBeneficiaria,
                    $numeroCompra,
                    $anoCompra
                );

                if (empty($retornoSiasg->data)) {
                    Log::warning($mensagemLog);
                    Log::warning(json_encode($retornoSiasg));
                    $countErro++;
                    continue;
                }

                if ($retornoSiasg->data->compraSispp->lei === 'LEI14133'
                    && $retornoSiasg->data->compraSispp->tipoCompra == '2'
                ) {
                    throw new \Exception('Compra não possui Ata de Registro de Preços registrada.');
                }

                $tipo_compra_id = $contratoService->buscaTipoCompra($retornoSiasg->data->compraSispp->tipoCompra);
                $api_origem = $compraService->getApiOrigem($retornoSiasg);


                if ($api_origem === 1) {
                    $modalidade = $this->retornaDescresPorId($contrato->modalidade_id);
                    if ($modalidade != $retornoSiasg->data->compraSispp->codigoModalidade) {
                        $countErro++;
                        throw new \Exception('Compra não encontrada com a modalidade selecionada');
                    }
                }

                $unidade_subrogada = $retornoSiasg->data->compraSispp->subrogada;
                $unidade_subrrogada_id = ($unidade_subrogada !== '000000')
                    ? (int)$this->buscaIdUnidade($unidade_subrogada)
                    : null;

                $compra = Compra::updateOrCreate(
                    [
                        'unidade_origem_id' => (int)$contrato->unidadecompra_id,
                        'modalidade_id'     => (int)$contrato->modalidade_id,
                        'numero_ano'        => $contrato->licitacao_numero,
                        'tipo_compra_id'    => $tipo_compra_id
                    ],
                    [
                        'unidade_subrrogada_id' => $unidade_subrrogada_id,
                        'tipo_compra_id'        => $tipo_compra_id,
                        'inciso'                => $retornoSiasg->data->compraSispp->inciso,
                        'lei'                   => $retornoSiasg->data->compraSispp->lei,
                        'artigo'                => $retornoSiasg->data->compraSispp->artigo,
                        'id_unico'              => $retornoSiasg->data->compraSispp->idUnico,
                        'cnpjOrgao'             => $retornoSiasg->data->compraSispp->cnpjOrgao,
                        'origem'                => $api_origem
                    ]
                );

                if ($retornoSiasg->data->compraSispp->tipoCompra == 1) {
                    $this->gravaParametroSISPP($retornoSiasg, $compra, $contrato->unidadecompra_id);

                    $mensagemLog .= " Compra cadastrada com sucesso com o id: {$compra->id}";
                    Log::info($mensagemLog);
                    $countSucesso++;
                    continue;
                }

                $this->gravaParametroSISRP($retornoSiasg, $compra, $contrato->unidadecompra_id);

                $mensagemLog .= " Compra cadastrada com sucesso com o id: {$compra->id}";
                Log::info($mensagemLog);
                $countSucesso++;
            } catch (Exception $exception) {
                Log::error("Erro na migration SalvarCompraContratoCadastradoEnvioPncp");
                Log::error($contrato);
                Log::error($exception);
                $countErro++;
            }
        }

        Log::alert("Quantidade de registros com sucesso: {$countSucesso}");
        Log::alert("Quantidade de registro com erro: {$countErro}");
        Log::alert('Fim da migration');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
