<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTipoMaterialSaldohistoricoitemColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('saldohistoricoitens', function (Blueprint $table) {
            $table->string('tipo_material')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('saldohistoricoitens', function (Blueprint $table) {
            $table->dropColumn('tipo_material');
        });
    }
}
