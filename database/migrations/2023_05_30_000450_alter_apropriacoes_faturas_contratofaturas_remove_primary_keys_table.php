<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterApropriacoesFaturasContratofaturasRemovePrimaryKeysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('apropriacoes_faturas_contratofaturas', function (Blueprint $table) {
            $table->dropPrimary(['apropriacoes_faturas_id', 'contratofaturas_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('apropriacoes_faturas_contratofaturas', function (Blueprint $table) {
            $table->primary(['apropriacoes_faturas_id', 'contratofaturas_id']);
        });
    }
}
