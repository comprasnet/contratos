<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Permission;
use App\Models\Role;

class ContratoArquivoPncp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Permission::create(['name' => 'contrato_arquivos_pncp_visualizar']);
        $role = Role::where('name', 'Administrador')->first();
        $role->givePermissionTo('contrato_arquivos_pncp_visualizar');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $role = Role::where(['name' => 'Administrador'])->first();
        $role->revokePermissionTo('contrato_arquivos_pncp_visualizar');
        Permission::where(['name' => 'contrato_arquivos_pncp_visualizar'])->forceDelete();
    }
}
