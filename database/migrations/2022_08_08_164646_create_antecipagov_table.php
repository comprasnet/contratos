<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAntecipagovTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('antecipagov', function (Blueprint $table) {
            $table->increments('id');
            $table->text('conta_bancaria');
            $table->text('num_agencia');
            $table->text('num_banco');
            $table->text('status_operacao') ;
            $table->text('num_operacao');
            $table->text('num_cotacao');
            $table->text('identificador_unico');
            $table->date('data_acao');
            $table->decimal('valor_operacao',17,2);
            $table->decimal('valor_parcela',17,2);

            $table->integer('situacao_id')->unsigned(); // REGISTRADO LIQUIDADO CANCELADO
            $table->foreign('situacao_id')->references('id')->on('codigoitens');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('antecipagov');
    }
}
