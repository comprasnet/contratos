<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCompraitemAddCriterioJulgamento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('compra_items', function (Blueprint $table) {
            $table->char('criterio_julgamento', 1)
                ->nullable()
                ->comment('"D" (Maior Desconto); "V" (valor)'); // "D" (Maior Desconto); "V" (valor)
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('compra_items', function (Blueprint $table) {
            $table->dropColumn('criterio_julgamento');
        });
    }
}
