<?php

use App\Http\Traits\ArquivoBase64ToPdf;
use App\Http\Traits\EnviaPncpTrait;
use App\Models\Codigoitem;
use App\Models\MinutaEmpenho;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \App\Http\Traits\MinutaEmpenhoTrait;

class PopularArquivoGenericoMinutaEmpenho extends Migration
{
    use EnviaPncpTrait, ArquivoBase64ToPdf, MinutaEmpenhoTrait;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * Migration para popular dados de 2023 desativada
         * A migration não termina a execuçao e não retorna finalização em produção
         * Está travando as migrations subsequentes de serem executadas
         */
//        $minutaEmpenhoPendenteAssinatura =
//            MinutaEmpenho::getMinutaEmpenhoSubstitutivoContratoDiferenteLeiQuatorzeUmTresTres(2023);
//
//        $idTipoArquivoMinutaEmpenho =  Codigoitem::where([
//            'descres' => 'ARQ_MINUTA_EMPENHO',
//            'descricao' => 'Arquivo minuta de empenho'
//        ])->first()->id;
//
//        foreach ($minutaEmpenhoPendenteAssinatura as $minuta) {
//            try {
//                if (!empty($minuta)) {
//                    $this->gerarPdfNovo($minuta, $idTipoArquivoMinutaEmpenho);
//                }
//            } catch (Exception $ex) {
//                $this->inserirLogCustomizado('minuta-empenho', 'error', $ex);
//            }
//        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
