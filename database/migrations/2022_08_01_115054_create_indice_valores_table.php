<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndiceValoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('indice_valores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mes',2);
            $table->year('ano');
            $table->string('tipo_indices_codigo');
            $table->index('tipo_indices_codigo');
            $table->foreign('tipo_indices_codigo')->references('codigo')->on('tipo_indices')->onDelete('cascade');
            //$table->double('valor', 10, 8);
            $table->double('variacao', 10, 8)->nullable();
            $table->double('acumulado_mes', 10, 8)->nullable();
            $table->double('acumulado_12_meses', 10, 8)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropForeign('tipo_indices_id_foreign');
        Schema::dropIndex('tipo_indices_id_index');
        Schema::dropColumn('tipo_indices_id');
        Schema::dropIfExists('indice_valores');
    }
}
