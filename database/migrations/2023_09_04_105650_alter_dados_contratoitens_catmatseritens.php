<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Catmatseritem;
use App\Models\Contratoitem;
use App\Models\CompraItem;
use Illuminate\Support\Facades\DB;

class AlterDadosContratoitensCatmatseritens extends Migration
{
    /**
     * Run the migrations.
     * A model Catmatseritem tem softdelete, o que faz com que a query já venha com deleted_at is null.
     * Por isso utilizarei o DB::select, pois preciso do deleted_at is not null
     * @return void
     */
    public function up()
    {
        //self::rodarParteContratos();
        //self::rodarParteCompras();
    }
    /**
     * Reverse the migrations.
     * A model Catmatseritem tem softdelete, o que faz com que a query já venha com deleted_at is null.
     * Por isso utilizarei o DB::select, pois preciso do deleted_at is not null
     *
     * @return void
     */
    public function down()
    {
        self::desfazerParteContratos();
        self::desfazerParteCompras();
    }
    public function rodarParteContratos(){
        \Log::info('-> Migration sendo executado (contratos)... em caso de erros, excessões serão geradas.');
        Schema::table('contratoitens', function ($table) {
            $table->integer('catmatseritem_id_antigo')->nullable();
        });
        $sql = "
            -- contratos com situação true e com catmatseritens deletados logicamente - já buscando o novo catmatseritens - 650 registros em produção
            select	u.id as id_unidade, u.codigo as codigo_unidade, c.id as id_contrato, c.numero as numero_contrato, c.situacao,
                    ci.id as id_contratoitens, ci.catmatseritem_id as id_catmatseritens_contratoitens,
                    cati.id as id_catmatseritens, cati.descricao as descricao_catmatseritens, cati.codigo_siasg, cati.deleted_at,
                    cati2.id as id_novo_catmatseritens, cati2.codigo_siasg as codigo_siasg_novo_catmatseritens, cati2.descricao as descricao_novo_catmatseritens
            from	catmatseritens cati
            inner	join contratoitens as ci on ci.catmatseritem_id = cati.id
            inner 	join contratos as c on c.id = ci.contrato_id
            inner 	join unidades as u on u.id = c.unidade_id
            left 	join catmatseritens as cati2 on cati2.codigo_siasg = cati.codigo_siasg and cati2.deleted_at is null
            where 	cati.deleted_at is not null
            and 	c.situacao is true
            and 	c.deleted_at is null
            and 	ci.deleted_at is null
            and 	ci.catmatseritem_id != cati2.id
            order 	by u.id, c.numero
        ";
        $resultado = DB::select($sql);
        $quantidade = count($resultado);
        $quantidadeAlterada = 0;
        foreach($resultado as $objeto){
            $id_contratoitens = $objeto->id_contratoitens;
            $id_catmatseritens = $objeto->id_catmatseritens;
            $id_novo_catmatseritens = $objeto->id_novo_catmatseritens;
            $objContratoItem = Contratoitem::find($id_contratoitens);
            if(is_object($objContratoItem)){
                $objContratoItem->catmatseritem_id_antigo = $objeto->id_catmatseritens;
                $objContratoItem->catmatseritem_id = $objeto->id_novo_catmatseritens;
                try {
                    $objContratoItem->save();
                } catch (Exception $e) {
                    throw new Exception('ERRO!!! id contratoitens = ' . $id_contratoitens . ' -> NÃO SALVOU! ', $e->getMessage());
                }
                $quantidadeAlterada++;
            } else {
                \Log::error('ERRO!!! -> NÃO é objeto! -> contrato item id = ' . $objContratoItem->id);
            }
        }
        \Log::info('De um total de ' . $quantidade . ' registros, foram alterados ' . $quantidadeAlterada . ' fim! ');
    }
    public function rodarParteCompras(){
        \Log::info('-> Migration sendo executado (compras)... em caso de erros, excessões serão geradas.');
        Schema::table('compra_items', function ($table) {
            $table->integer('catmatseritem_id_antigo')->nullable();
        });
        $sql = "
            -- atmatseritens da compra deletados logicamente
            select
                    --c.id, c.numero, c.situacao,
                    cpi.id as id_compraitens,
                    cp.id as id_compra, cp.numero_ano as numero_ano_compra,
                    cati_atual.id as id_catmatseritem_atual, cati_atual.descricao as descricao_catmatseritem_atual,
                    cati_atual.codigo_siasg as codigo_siasg_catmatseritem_atual, cati_atual.deleted_at as deleted_at_catmatseritem_atual,
                    cati_atual.situacao as situacao_catmatseritem_atual,
                    --, m.*
                    cati_novo.id as id_novo_catmatseritens, cati_novo.codigo_siasg as codigo_siasg_novo_catmatseritens,
                    cati_novo.descricao as descricao_novo_catmatseritens, cati_novo.situacao as situacao_novo_catmatseritens
            --select	count(*)
            from	catmatseritens cati_atual
            inner	join compra_items as cpi on cpi.catmatseritem_id = cati_atual.id
            inner 	join compras cp on cp.id = cpi.compra_id
            --inner 	join minutaempenhos as m on m.compra_id = cp.id
            --inner 	join contratos as c on c.id = m.contrato_id
            inner 	join catmatseritens as cati_novo on cati_novo.codigo_siasg = cati_atual.codigo_siasg and cati_novo.deleted_at is null
            where 	cati_atual.deleted_at is not null
            and 	cpi.deleted_at is null
            and 	cp.deleted_at is null

        ";
        $resultado = DB::select($sql);
        $quantidade = count($resultado);
        $quantidadeAlterada = 0;
        foreach($resultado as $objeto){
            $id_compraitens = $objeto->id_compraitens;
            $id_novo_catmatseritens = $objeto->id_novo_catmatseritens;
            $id_catmatseritens = $objeto->id_catmatseritem_atual;
            $objCompraItem = CompraItem::find($id_compraitens);
            if(is_object($objCompraItem)){
                $objCompraItem->catmatseritem_id_antigo = $objeto->id_catmatseritem_atual;
                $objCompraItem->catmatseritem_id = $objeto->id_novo_catmatseritens;
                try {
                    $objCompraItem->save();
                } catch (Exception $e) {
                    throw new Exception('ERRO!!! id contratoitens = ' . $id_contratoitens . ' -> NÃO SALVOU! ', $e->getMessage());
                }
                $quantidadeAlterada++;
            } else {
                \Log::error('ERRO!!! -> NÃO é objeto! -> contrato item id = ' . $objCompraItem->id);
            }
        }
        \Log::info('De um total de ' . $quantidade . ' registros, foram alterados ' . $quantidadeAlterada . ' fim! ');
    }
    public function desfazerParteContratos(){
        \Log::info('-> Rollback sendo executado (contratos)... em caso de erros, excessões serão geradas.');
        $sql = "
            -- rollback - voltar catmatseritens do contratoitem - pegar o catmatseritem_id_antigo e jogar em catmatseritem_id
            select	ci.id, ci.catmatseritem_id, catmatseritem_id_antigo
            from	contratoitens ci
            where 	ci.catmatseritem_id_antigo is not null
        ";
        $resultado = DB::select($sql);
        $quantidade = count($resultado);
        $quantidadeAlterada = 0;
        foreach($resultado as $objeto){
            $id_contratoitens = $objeto->id;
            $id_catmatseritens = $objeto->catmatseritem_id;
            $id_antigo_catmatseritens = $objeto->catmatseritem_id_antigo;
            $objContratoItem = Contratoitem::find($id_contratoitens);
            if(is_object($objContratoItem)){
                $objContratoItem->catmatseritem_id = $id_antigo_catmatseritens;
                try {
                    $objContratoItem->save();
                } catch (Exception $e) {
                    throw new Exception('ERRO!!! id contratoitens = ' . $id_contratoitens . ' -> NÃO SALVOU! ', $e->getMessage());
                }
                $quantidadeAlterada++;
            } else {
                \Log::error('ERRO!!! -> NÃO é objeto! -> contrato item id = ' . $objContratoItem->id);
            }
        }
        if(Schema::hasColumn('contratoitens', 'catmatseritem_id_antigo')){
            Schema::table('contratoitens', function ($table) {
                $table->dropColumn('catmatseritem_id_antigo');
            });
            \Log::info('De ' . $quantidade . ' registros, foram alterados ' . $quantidadeAlterada . ' e o atributo foi removido.');
        } else {
            \Log::error('ERRO!!! atributo catmatseritem_id_antigo NÃO LOCALIZADO!');
        }
    }
    public function desfazerParteCompras(){
        \Log::info('-> Rollback sendo executado (compras)... em caso de erros, excessões serão geradas.');
        $sql = "
            -- rollback - voltar catmatseritens do compra_items - pegar o catmatseritem_id_antigo e jogar em catmatseritem_id
            select	ci.id, ci.catmatseritem_id, catmatseritem_id_antigo
            from	compra_items ci
            where 	ci.catmatseritem_id_antigo is not null
        ";
        $resultado = DB::select($sql);
        $quantidade = count($resultado);
        $quantidadeAlterada = 0;
        foreach($resultado as $objeto){
            $id_compraitens = $objeto->id;
            $id_catmatseritens = $objeto->catmatseritem_id;
            $id_antigo_catmatseritens = $objeto->catmatseritem_id_antigo;
            $objCompraItem = CompraItem::find($id_compraitens);
            if(is_object($objCompraItem)){
                $objCompraItem->catmatseritem_id = $id_antigo_catmatseritens;
                try {
                    $objCompraItem->save();
                } catch (Exception $e) {
                    throw new Exception('ERRO!!! id copraitens = ' . $id_compraitens . ' -> NÃO SALVOU! ', $e->getMessage());
                }
                $quantidadeAlterada++;
            } else {
                \Log::error('ERRO!!! -> NÃO é objeto! -> compra item id = ' . $objCompraItem->id);
            }
        }
        if(Schema::hasColumn('compra_items', 'catmatseritem_id_antigo')){
            Schema::table('compra_items', function ($table) {
                $table->dropColumn('catmatseritem_id_antigo');
            });
            \Log::info('De ' . $quantidade . ' registros, foram alterados ' . $quantidadeAlterada . ' e o atributo foi removido.');
        } else {
            \Log::error('ERRO!!! atributo catmatseritem_id_antigo NÃO LOCALIZADO!');
        }
    }
}
