<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSfpcoitemDropDespesaantecipadaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sfpcoitem', function ($table) {
            $table->dropColumn('despesaantecipada');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rhsituacao', function (Blueprint $table) {
            $table->boolean('despesaantecipada')->nullable();
        });
    }
}
