<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Models\ContratoMinuta;

class AlterCotratominutasPopularNumeroSuperSei extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        #248
        if(Schema::hasColumn('contrato_minutas', 'numero_super_sei')){
            // buscar contrato minutas onde documento_formatado não seja null e numero_super_sei seja null
            $minutas = ContratoMinuta::where('documento_formatado', '<>', null)
                ->where('numero_super_sei', '=', null)
                ->get();
            foreach($minutas as $minuta){
                $documento_formatado = $minuta->documento_formatado;
                $minuta->numero_super_sei = $documento_formatado;
                $minuta->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('contrato_minutas', 'numero_super_sei')){
            // buscar contrato minutas onde documento_formatado não seja null e numero_super_sei seja null
            $minutas = ContratoMinuta::where('numero_super_sei', '<>', null)
                ->get();
            foreach($minutas as $minuta){
                $minuta->numero_super_sei = null;
                $minuta->save();
            }
        }
    }
}
