<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveColumnsAntecipagovTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $antecipa = \App\Models\AntecipaGov::whereNull('domicilio_bancario_id')->count();

        if ($antecipa == 0) {

            if (Schema::hasColumn('antecipagov', 'domicilio_bancario_id')) {
                Schema::table('antecipagov', function (Blueprint $table) {
                    $table->dropColumn(['num_banco', 'conta_bancaria', 'num_agencia']);
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('antecipagov', function (Blueprint $table) {
            $table->string('num_banco', 40);
            $table->string('conta_bancaria', 40);
            $table->string('num_agencia', 40);
        });
    }
}
