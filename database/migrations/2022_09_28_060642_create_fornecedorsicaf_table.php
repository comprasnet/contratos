<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFornecedorsicafTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fornecedorsicaf', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('fornecedor_id')->unsigned();

            $table->date('date_receita_federal_pgfn')->nullable();
            $table->boolean('validade_receita_federal_pgfn');
            $table->date('date_certidao_fgts')->nullable();
            $table->boolean('validade_certidao_fgts');
            $table->date('date_certidao_trabalhista')->nullable();
            $table->boolean('validade_certidao_trabalhista');
            $table->date('date_receita_estadual_distrital')->nullable();
            $table->boolean('validate_receita_estadual_distrital');
            $table->date('date_receita_municipal')->nullable();
            $table->boolean('validade_receita_municipal');

            $table->timestamps();

            $table->foreign('fornecedor_id')->references('id')->on('fornecedores')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fornecedorsicaf');
    }
}
