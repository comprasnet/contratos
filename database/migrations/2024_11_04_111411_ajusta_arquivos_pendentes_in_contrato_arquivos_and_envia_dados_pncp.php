<?php

use App\Models\Contratoarquivo;
use App\Models\EnviaDadosPncp;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

use Illuminate\Database\Migrations\Migration;
use App\Http\Traits\EnviaPncpTrait;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Formatter\LineFormatter;


class AjustaArquivosPendentesInContratoArquivosAndEnviaDadosPncp extends Migration
{
    use EnviaPncpTrait;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        ##############################################################################################
        ### FORMATAÇÕES PARA GERAR O LOG
        ##############################################################################################
        $handler = new StreamHandler(
            storage_path("logs/1140-ajuste-arquivos-pendentes-pncp.log"),
            Logger::INFO);
        $formatter = new LineFormatter("%datetime% %channel%.%level_name%: %message%\n");
        $handler->setFormatter($formatter);
        $log = new \Monolog\Logger('custom');
        $log->pushHandler($handler);
        ##############################################################################################

        $idSituacaoSucesso = $this->recuperarIDSituacao('SUCESSO');
        $arquivosAfetados = collect();
        DB::beginTransaction();

        ##############################################################################################
        ### TRATAMENTO DE INCLUSÕES
        ##############################################################################################
        # Ajuste 1
        # Arquivo marcado para incluir, mas não deveria ir para o PNCP (é restrito) e não tem sequencial -> Finaliza.
        $arquivos = ContratoArquivo::withTrashed()
            ->where('envio_pncp_pendente', 'INCARQ')
            ->where('restrito', true)
            ->selectRaw('1 as ajuste, contrato_arquivos.*')
            ->get();
        $this->finalizarPendenciaArquivo($arquivos);
        $arquivosAfetados = $arquivosAfetados->merge($arquivos);

        # Ajuste 2
        # Arquivo marcado para incluir, mas já foi excluído logicamente -> Finaliza.
        $arquivos = ContratoArquivo::withTrashed()
            ->where('envio_pncp_pendente', 'INCARQ')
            ->whereNotNull('deleted_at')
            ->selectRaw('2 as ajuste, contrato_arquivos.*')
            ->get();
        $this->finalizarPendenciaArquivo($arquivos);
        $arquivosAfetados = $arquivosAfetados->merge($arquivos);

        # Ajuste 3
        # Arquivo marcado para incluir, mas já tem sequencial -> Finaliza.
        # Não há casos: com sequencial e o arquivo é restrito.
        $arquivos = ContratoArquivo::where('envio_pncp_pendente', 'INCARQ')
            ->whereNotNull('sequencial_pncp')
            ->selectRaw('3 as ajuste, contrato_arquivos.*')
            ->get();
        $this->finalizarPendenciaArquivo($arquivos);
        $arquivosAfetados = $arquivosAfetados->merge($arquivos);

        # Ajuste 4
        # Arquivo marcado para incluir, não tem sequencial, não é restrito e tem linha na tabela envia_dados_pncp
        # Isto significa que é um caso "normal" de inclusão, mas pode ser que a linha PNCP esteja como sucesso
        #
        # Esta busca foi feita com join na envia_dados_pncp, pois há muitos casos com estas características, mas que
        # não tem linha no PNCP. Estes casos, analisados por amostragem, indicam que o arquivo não tem critério
        # para ser enviado (não é lei 14133, etc)
        # Neste caso, mantém a inclusão normal,
        # mas busca os arquivos apenas para atualizar a situação no envia_dados_pncp para forçar a inclusão.
        $arquivos = ContratoArquivo::join('envia_dados_pncp', 'envia_dados_pncp.pncpable_id', '=',
            'contrato_arquivos.contratohistorico_id')
            ->where('envio_pncp_pendente', 'INCARQ')
            ->whereNull('contrato_arquivos.sequencial_pncp')
            ->where('restrito', false)
            ->where('envia_dados_pncp.situacao', $idSituacaoSucesso)
            ->selectRaw('4 as ajuste, contrato_arquivos.*')
            ->get();
        $arquivosAfetados = $arquivosAfetados->merge($arquivos);

        ##############################################################################################
        ### TRATAMENTO DE EXCLUSÕES
        ##############################################################################################
        # Ajuste 5
        # Arquivo marcado para excluir, mas nao tem sequencial -> Finaliza
        $arquivos = ContratoArquivo::withTrashed()
            ->where('envio_pncp_pendente', 'DELARQ')
            ->whereNull('sequencial_pncp')
            ->selectRaw('5 as ajuste, contrato_arquivos.*')
            ->get();
        $this->finalizarPendenciaArquivo($arquivos);
        $arquivosAfetados = $arquivosAfetados->merge($arquivos);

        # Ajuste 6
        # Arquivo marcado para excluir e tem sequencial.
        # Neste caso, mantém a exclusão,
        # mas busca os arquivos apenas para atualizar a situação no envia_dados_pncp caso esteja como SUCESSO
        # para forçar a exclusão.
        $arquivos = ContratoArquivo::withTrashed()
            ->join('envia_dados_pncp',
                'envia_dados_pncp.pncpable_id','=','contrato_arquivos.contratohistorico_id')
            ->where('envio_pncp_pendente', 'DELARQ')
            ->whereNotNull('sequencial_pncp')
            ->where('envia_dados_pncp.situacao', $idSituacaoSucesso)
            ->whereNull('envia_dados_pncp.deleted_at') # Não traz registros excluídos da tabela envia_dados_pncp
            ->selectRaw('6 as ajuste, contrato_arquivos.*')
            ->get();
        $arquivosAfetados = $arquivosAfetados->merge($arquivos);

        ##############################################################################################
        ### TRATAMENTO DE ALTERAÇÕES
        ##############################################################################################
        # Ajuste 7
        # Arquivo marcado para atualizar, mas o arquivo foi excluído logicamente.
        # Marca como exclusão, pois se o arquivo ainda estiver no PNCP, faz a exclusão lá.
        $arquivos = ContratoArquivo::withTrashed()
            ->where('envio_pncp_pendente', 'ATUARQ')
            ->whereNotNull('deleted_at')
            ->whereNotNull('sequencial_pncp')
            ->selectRaw('7 as ajuste, contrato_arquivos.*')
            ->get();
        # Altera a pendência para exclusão
        $this->finalizarPendenciaArquivo($arquivos, 'DELARQ');
        $arquivosAfetados = $arquivosAfetados->merge($arquivos);

        # Ajuste 8
        # Arquivo marcado para atualizar, o arquivo não foi excluído logicamente e tem sequencial (alteração normal)
        # Neste caso, mantém a alteração, mas busca os arquivos apenas para atualizar a situação no envia_dados_pncp
        # para forçar a alteração.
        $arquivos = ContratoArquivo::join('envia_dados_pncp',
            'envia_dados_pncp.pncpable_id','=','contrato_arquivos.contratohistorico_id')
            ->where('envio_pncp_pendente', 'ATUARQ')
            ->whereNotNull('sequencial_pncp')
            ->where('envia_dados_pncp.situacao', $idSituacaoSucesso)
            ->whereNull('envia_dados_pncp.deleted_at') # Não traz registros excluídos da tabela envia_dados_pncp
            ->selectRaw('8 as ajuste, contrato_arquivos.*')
            ->get();
        $arquivosAfetados = $arquivosAfetados->merge($arquivos);

        ##############################################################################################
        # Para cada arquivo afetado, busca o registro 'pai' na tabela envia_dados_pncp e atualiza a situação da linha
        ##############################################################################################
        if ($arquivosAfetados->isNotEmpty()) {
            $log->info(";Ajuste; ID Contrato; ID ContratoHistorico; ID Arquivo; Descrição");
            foreach ($arquivosAfetados as $arquivo) {
                $log->info(
                    ";" . $arquivo->ajuste . '; ' .
                    $arquivo->contrato_id . '; ' .
                    $arquivo->contratohistorico_id . '; ' .
                    $arquivo->id . '; ' .
                    $arquivo->descricao
                );
                $this->atualizarSituacaoEnviaDadosPncpPeloIdContratoArquivo($arquivo->id);
            }
        } else {
            $log->info("Nenhum arquivo pendente para ajuste");
        }
        DB::commit();
    }

    private function finalizarPendenciaArquivo($arquivos, $envioPncpPendente = null)
    {
        ContratoArquivo::withTrashed()
            ->whereIn('id', $arquivos->pluck('id'))
            ->update(['envio_pncp_pendente' => $envioPncpPendente]);
    }

    /**
     *
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
