<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExecsfsituacaoCamposVariaveisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('execsfsituacao_campos_variaveis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('execsfsituacao_id');
            $table->string('aba');
            $table->string('campo');
            $table->string('tipo');
            $table->string('mascara')->nullable();
            $table->string('rotulo');
            $table->string('restricao');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('execsfsituacao_id')->references('id')->on('execsfsituacao')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('execsfsituacao_campos_variaveis');
    }
}
