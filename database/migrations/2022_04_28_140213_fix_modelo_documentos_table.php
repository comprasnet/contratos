<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixModeloDocumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('modelo_documentos','tipo_acao_id')) {
            Schema::table('modelo_documentos', function (Blueprint $table) {
                $table->dropForeign(['tipo_acao_id']);
                $table->dropColumn('tipo_acao_id');
            });
        }
        if (Schema::hasColumn('modelo_documentos','tipo_documento_id')) {
            Schema::table('modelo_documentos', function (Blueprint $table) {
                $table->dropForeign(['tipo_documento_id']);
                $table->dropColumn('tipo_documento_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('modelo_documentos','tipo_acao_id')) {
            Schema::table('modelo_documentos', function (Blueprint $table) {
                $table->dropForeign(['tipo_acao_id']);
                $table->dropColumn('tipo_acao_id');
            });
        }
        if (Schema::hasColumn('modelo_documentos','tipo_documento_id')) {
            Schema::table('modelo_documentos', function (Blueprint $table) {
                $table->dropForeign(['tipo_documento_id']);
                $table->dropColumn('tipo_documento_id');
            });
        }
    }
}
