<?php

use App\Models\AntecipaGov;
use App\Models\DomicilioBancario;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDomiciliobancarioAntecipagov extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('antecipagov', function (Blueprint $table) {
            $table->integer('domicilio_bancario_id')->unsigned()->nullable();
            $table->foreign('domicilio_bancario_id')->references('id')->on('domicilio_bancarios');
        });

        $antecipa_gov_contas = AntecipaGov::all();

        foreach ($antecipa_gov_contas as $conta) {

            $domicioBancario = DomicilioBancario::firstOrCreate([
                'banco' => $conta->num_banco,
                'conta' => $conta->conta_bancaria,
                'agencia' => $conta->num_agencia,
                'antecipa_gov' => true
            ]);

            $conta->update(['domicilio_bancario_id' => $domicioBancario->id]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('antecipagov', function (Blueprint $table) {
            $table->dropColumn('domicilio_bancario_id');
        });
    }
}
