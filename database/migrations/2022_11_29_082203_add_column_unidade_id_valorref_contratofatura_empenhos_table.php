<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnUnidadeIdValorrefContratofaturaEmpenhosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contratofatura_empenhos', function (Blueprint $table) {
            $table->decimal('valorref', 17,2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contratofatura_empenhos', function (Blueprint $table) {
            $table->dropColumn('valorref');
        });
    }
}
