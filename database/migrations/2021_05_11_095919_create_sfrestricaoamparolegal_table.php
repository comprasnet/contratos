<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSfrestricaoamparolegalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sfrestricaoamparolegal', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('sforcamparolegaldados_id');
            $table->string('tiporestricao', 30); //TIPO_ADMINISTRACAO_EMITENTE, ORGAO_UG_EMITENTE, ORGAO_UG_FAVORECIDA, AGENCIA_EXECUTIVA, NATUREZA_DESPESA
            $table->string('regra', 10); //SOMENTE, EXCETO
            $table->string('valor', 6)->nullable(); //codigo_restrição da tabela amparo ou a palavra
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sfrestricaoamparolegal');
    }
}
