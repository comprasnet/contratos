<?php

use App\Models\Codigo;
use App\Models\Codigoitem;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSituacaoAnaliseToCodigoitensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = Codigo::select('id')
            ->where('descricao', 'Situação Envia Dados PNCP')->first();
        if (!$codigo) {
            Log::error(basename(__FILE__) . ' ' . __METHOD__ .
                ' - Código "Situação Envia Dados PNCP" não encontrado.');
            return;
        }
        $codigoItem = Codigoitem::withTrashed()
            ->where('codigo_id', $codigo->id)
            ->where('descres', 'ANALISE')
            ->first();

        # Feto esse procedimento para evitar problemas com rollback da migration
        if ($codigoItem) {
            if ($codigoItem->trashed()) {
                $codigoItem->restore();
            }
        } else {
            Codigoitem::create([
                'codigo_id' => $codigo->id,
                'descres' => 'ANALISE',
                'descricao' => 'Em Análise',
                'visivel' => false
            ]);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $codigo = Codigo::select('id')
            ->where('descricao', 'Situação Envia Dados PNCP')->first();
        if (!$codigo) {
            Log::error(basename(__FILE__) . ' ' . __METHOD__ .
                ' - Código "Situação Envia Dados PNCP" não encontrado.');
            return;
        }
        Codigoitem::where('codigo_id', $codigo->id)
            ->where('descres', 'ANALISE')
            ->delete();

    }
}
