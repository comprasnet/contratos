<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCollumnCatmatpdmIdCatmatseritensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('catmatseritens', 'catmatpdm_id')){
            Schema::table('catmatseritens', function (Blueprint $table) {
                $table->integer('catmatpdm_id')->nullable();
                $table->foreign('catmatpdm_id')->references('id')->on('catmatpdms')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('catmatseritens', function (Blueprint $table) {
            $table->dropColumn('catmatpdm_id');
        });
    }
}
