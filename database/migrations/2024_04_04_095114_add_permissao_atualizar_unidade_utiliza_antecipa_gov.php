<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AddPermissaoAtualizarUnidadeUtilizaAntecipaGov extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        app()['cache']->forget('spatie.permission.cache');

        Permission::create(['name' => 'atualizar_unidade_utiliza_antecipa_gov']);

        $role = Role::where(['name' => 'Administrador'])->first();

        $role->givePermissionTo('atualizar_unidade_utiliza_antecipa_gov');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $role = Role::where(['name' => 'Administrador'])->first();

        $role->revokePermissionTo('atualizar_unidade_utiliza_antecipa_gov');
        Permission::where(['name' => 'atualizar_unidade_utiliza_antecipa_gov'])->forceDelete();

    }
}
