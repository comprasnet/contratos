<?php

use App\Models\Codigo;
use App\Models\Codigoitem;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertSituacaoAguardandoAnulacao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = Codigo::where('descricao', 'Situações Minuta Empenho')->first();

        Codigoitem::updateOrCreate([
            'codigo_id' => $codigo->id,
            'descres' => 'ANULACAO',
            'descricao' => 'AGUARDANDO ANULAÇÃO',
            'visivel' => true
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $codigo = Codigo::where('descricao', 'Situações Minuta Empenho')->first();

        Codigoitem::where([
            'codigo_id' => $codigo->id,
            'descres' => 'ANULACAO',
            'descricao' => 'AGUARDANDO ANULAÇÃO'
        ])->forceDelete();
    }
}
