<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterExecsfsituacaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('execsfsituacao', function (Blueprint $table) {
            $table->integer('categoria_ddp')->nullable()->change();
            $table->integer('tipo_situacao')->nullable();
            $table->softDeletes();

            $table->foreign('tipo_situacao')->references('id')->on('codigoitens')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
