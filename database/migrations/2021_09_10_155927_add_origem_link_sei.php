<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrigemLinkSei extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contrato_arquivos', function (Blueprint $table) {
            $table->string('origem',1)->nullable();
            $table->string('link_sei',200)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contrato_arquivos', function (Blueprint $table) {
            $table->string('origem',1)->nullable();
            $table->string('link_sei',200)->nullable();
        });
    }
}
