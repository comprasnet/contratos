<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Models\Codigoitem;
use App\Models\Codigo;

class AddCodigoitensJustificativaContratoInativo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $codigo = Codigo::create([
            'descricao' => 'Justificativa Contrato Inativo',
            'visivel' => false
        ]);
        $codigo = Codigo::where('descricao', 'Justificativa Contrato Inativo')->first();
        $codigoitem = Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'CTRES',
            'descricao' => 'Rescindido'
        ]);
        $codigoitem = Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'CTENC',
            'descricao' => 'Encerrado'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $codigo = Codigo::where('descricao', 'Justificativa Contrato Inativo')->first();
        Codigoitem::where([
            'codigo_id' => $codigo->id,
            'descres' => 'CTRES',
            'descricao' => 'Rescindido'
        ])->forceDelete();

        Codigoitem::where([
            'codigo_id' => $codigo->id,
            'descres' => 'CTENC',
            'descricao' => 'Encerrado'
        ])->forceDelete();

        $codigo->forceDelete();

    }
}
