<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdensBancariasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordens_bancarias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('sfpadrao_id')->unsigned();
            $table->integer('unidade_id')->unsigned();
            $table->string('numero');
            $table->date('emissao');
            $table->integer('fornecedor_id')->unsigned();
            $table->text('observacao')->nullable();
            $table->integer('tipo_ob')->comment('OB-Ordem Bancária, DF-DARF, DR-DAR, GP-GPS, GR-GRU');
            $table->string('processo');
            $table->char('cancelamentoob',1);
            $table->string('numeroobcancelamento')->nullable();
            $table->double('valor', 19, 2);
            $table->string('documentoorigem')->nullable();
            $table->string('bancodestino')->nullable();
            $table->string('agenciadestino')->nullable();
            $table->string('contadestino')->nullable();
            $table->timestamps();

            $table->foreign('sfpadrao_id')->references('id')->on('sfpadrao')->onDelete('cascade');
            $table->foreign('fornecedor_id')->references('id')->on('fornecedores');
            $table->foreign('unidade_id')->references('id')->on('unidades');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ordens_bancarias');
    }
}
