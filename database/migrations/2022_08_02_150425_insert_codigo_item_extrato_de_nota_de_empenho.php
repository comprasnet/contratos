<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertCodigoItemExtratoDeNotaDeEmpenho extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \App\Models\Codigoitem::create([
            'codigo_id' => \App\Models\Codigo::TIPO_NORMA_PUBLICACAO,
            'descres' => '118',
            'descricao' => 'Extrato de Nota de Empenho',
            'visivel' => true
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\Models\Codigoitem::where([
            'codigo_id' => \App\Models\Codigo::TIPO_NORMA_PUBLICACAO,
            'descricao' => 'Extrato de Nota de Empenho',
            'visivel' => true
        ])->forceDelete();
    }
}
