<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use App\Models\Contrato;

class AtualizarContratosJustificativa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   #907
        // criar coluna para verificação posterior e para backup da justificativa_id
        if(!Schema::hasColumn('contratos', 'st_is_alterada_justificativa')){
            Schema::table('contratos', function (Blueprint $table) {
                $table->boolean('st_is_alterada_justificativa')->nullable();
                $table->integer('justificativa_contrato_inativo_id_backup')->nullable();
            });
        }
        // buscar contratos ativos com justificativa cadastrada
        $contratos = Contrato::where('situacao', true)->where('justificativa_contrato_inativo_id', '<>', null)->get();
        // retirar justificativas dos contratos ativos e salvar justificativa atual, no campo criado para backup
        if(Schema::hasColumn('contratos', 'st_is_alterada_justificativa')){
            foreach($contratos as $contrato){
                // foi feito uma query, pois se mandar salvar o objeto contrato, o sistema tratará como alteração do contrato, tratando o histórico.
                $sql = "update contratos set st_is_alterada_justificativa = true, justificativa_contrato_inativo_id_backup = justificativa_contrato_inativo_id, justificativa_contrato_inativo_id = null  where id = $contrato->id";
                $resultado = DB::select($sql);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('contratos', 'st_is_alterada_justificativa')){
            // buscar contratos que sofreram alterações no campo justificativa pelo migrate
            $contratos = Contrato::where('situacao', true)->where('st_is_alterada_justificativa', true)->get();
            // voltar a justificativa para os contratos buscados, para que voltem a ser como eram antes do migrate.
            foreach($contratos as $contrato){
                // foi feito uma query, pois se mandar salvar o objeto contrato, o sistema tratará também o histórico.
                $sql = "update contratos set st_is_alterada_justificativa = null, justificativa_contrato_inativo_id = justificativa_contrato_inativo_id_backup, justificativa_contrato_inativo_id_backup = null  where id = $contrato->id";
                $resultado = DB::select($sql);
            }
            // excluir as colunas criadas para verificação
            Schema::table('contratos', function ($table) {
                $table->dropColumn('st_is_alterada_justificativa');
                $table->dropColumn('justificativa_contrato_inativo_id_backup');
            });
        }
    }
}
