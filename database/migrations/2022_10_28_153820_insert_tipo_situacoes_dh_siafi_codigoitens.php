<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Codigo;
use App\Models\Codigoitem;

class InsertTipoSituacoesDhSiafiCodigoitens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = Codigo::firstOrCreate([
            'descricao' => 'Tipos Situações DH SIAFI',
            'visivel' => true
        ]);

        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'SIT0012','descricao' => 'SITUACOES DE CREDITO/COMPENSACAO','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => '-9','descricao' => 'NAO SE APLICA','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'SIT0004','descricao' => 'SITUACOES DO TIPO "ESTORNO"','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'SIT0011','descricao' => 'SITUACOES DE DESPESA A ANULAR','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'SIT0010','descricao' => 'SITUACOES DE LANCAMENTOS DIVERSOS','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'SIT0007','descricao' => 'SITUACOES DE DEDUCAO','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'SIT0002','descricao' => 'SITUACOES DE INSCRICAO DE EMPENHOS EM RESTOS A PAGAR','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'SIT0015','descricao' => 'SITUACOES DE DESPESA (SALDO NAS CONTAS DE EMPENHOS A LIQUIDAR)','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'SIT0003','descricao' => 'SITUACOES DO TIPO "NORMAL"','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'SIT0005','descricao' => 'SITUACOES DE DEVOLUCAO DE DESPESA','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'SIT0008','descricao' => 'SITUACOES DE ENCARGO','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'SIT0006','descricao' => 'SITUACOES DE PASSIVOS SEM EXECUCAO ORCAMENTARIA','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'SIT0016','descricao' => 'SITUACOES DE DESPESA (SALDO NAS CONTAS DE EMPENHOS EM LIQUIDACAO)','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'SIT0014','descricao' => 'SITUACOES DE SUPRIMENTO DE FUNDOS','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'SIT0009','descricao' => 'SITUACOES DE CANCELAMENTO RESTOS A PAGAR','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'SIT0001','descricao' => 'SITUACOES DE DESPESA','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'SIT0013','descricao' => 'SITUACOES DE LANCAMENTOS PATRIMONIAIS','visivel' => true]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Codigo::where([
            'descricao' => 'Tipos Situações DH SIAFI',
            'visivel' => true
        ])->forceDelete();
    }
}
