<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Orgaoconfiguracao;

class AlterColumnCodigoSistemaExternoObrigatorioOnOrgaoConfiguracaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orgaoconfiguracao', function (Blueprint $table) {
            $table->boolean('codigo_sistema_externo_obrigatorio')->default(false)->change();
        });

        Orgaoconfiguracao::where('codigo_sistema_externo_obrigatorio', null)
        ->update(['codigo_sistema_externo_obrigatorio' => false]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
