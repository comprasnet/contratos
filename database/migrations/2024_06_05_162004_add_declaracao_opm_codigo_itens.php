<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeclaracaoOpmCodigoItens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $declaracaoOpm = [
            'codigo_id' => 17,
            'descres' => 'DECL_OPM',
            'descricao' => 'Declaração OPM',
            'visivel' => true
        ];

        \App\Models\Codigoitem::create($declaracaoOpm);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\Models\Codigoitem::where('codigo_id', 17)
            ->where('descres', 'DECL_OPM')
            ->delete();

    }
}
