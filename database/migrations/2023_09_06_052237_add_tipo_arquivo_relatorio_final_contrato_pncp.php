<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \App\Models\Codigo;
use \App\Models\Codigoitem;

class AddTipoArquivoRelatorioFinalContratoPncp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigoTipoArquivo = Codigo::find(Codigo::TIPO_ARQUIVOS_CONTRATO);
        Codigoitem::create([
            'descricao' => 'Relatório Final do Contrato',
            'codigo_id' => $codigoTipoArquivo->id,
            'descres' => 'REL_FINAL_CONTRATO',
            'visivel' => true
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $codigoTipoArquivo = Codigo::find(Codigo::TIPO_ARQUIVOS_CONTRATO);
        Codigoitem::where("codigo_id", $codigoTipoArquivo->id)
            ->where("descricao", 'Relatório Final do Contrato')
            ->where("descres", 'REL_FINAL_CONTRATO')
            ->delete();
    }
}
