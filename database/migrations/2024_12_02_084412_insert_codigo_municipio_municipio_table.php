<?php

use App\Models\Municipio;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertCodigoMunicipioMunicipioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('municipios', 'codigo_municipio')) {
            Schema::table('municipios', function (Blueprint $table) {
                $table->string('codigo_municipio', 25)->nullable();
            });
        }

        if (($handle = fopen(database_path('data/separadovirgulas.csv'), "r")) !== FALSE) {
            for ($i = 0; $row = fgetcsv($handle, null, ";"); ++$i) {

                $codigoMunicipio = trim($row[0]);
                $nomeMunicipio = trim($row[2]);
                $estado = trim($row[4]);

                $municipio = Municipio::whereHas('estado', function ($q) use ($estado) {
                    $q->where('sigla', $estado);
                })
                    ->whereNull('codigo_municipio')
                    ->whereRaw("unaccent(nome) ILIKE unaccent(?) ", [$nomeMunicipio])->first();

                if ($municipio) {
                    DB::beginTransaction();
                    try {
                        $municipio->codigo_municipio = $codigoMunicipio;
                        $municipio->save();
                        DB::commit();
                    } catch (\Exception $e) {
                        DB::rollBack();
                        Log::info($e);
                    }
                }
            }
            fclose($handle);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('municipios', 'codigo_municipio')) {
            Schema::table('municipios', function (Blueprint $table) {
                $table->dropColumn('codigo_municipio');
            });
        }
    }
}
