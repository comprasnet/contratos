<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddArquivoDoInstrumentoDeCobrancaContratofaturas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('contratofaturas', 'arquivo_do_instrumento_de_cobranca')){
            Schema::table('contratofaturas', function (Blueprint $table) {
                $table->string('arquivo_do_instrumento_de_cobranca')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contratofaturas', function (Blueprint $table) {
            $table->dropColumn(['arquivo_do_instrumento_de_cobranca']);
        });
    }
}
