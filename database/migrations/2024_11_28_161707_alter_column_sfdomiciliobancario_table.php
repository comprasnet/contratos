<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterColumnSfdomiciliobancarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sfdomiciliobancario', function (Blueprint $table) {
            $table->string('banco')->nullable(true)->change();
            $table->string('agencia')->nullable(true)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sfdomiciliobancario', function (Blueprint $table) {
            $table->string('banco')->nullable(false)->change();
            $table->string('agencia')->nullable(false)->change();
        });
    }
}
