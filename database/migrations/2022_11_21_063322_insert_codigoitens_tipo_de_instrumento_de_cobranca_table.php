<?php

use App\Models\Codigo;
use App\Models\Codigoitem;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertCodigoitensTipoDeInstrumentoDeCobrancaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = Codigo::create([
            'descricao' => 'Tipo de Instrumento de Cobrança',
            'visivel' => false
        ]);

        $codigo = Codigo::where('descricao', 'Tipo de Instrumento de Cobrança')->first();

        $codigoitem = Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'Nota Fiscal',
            'descricao' => 'Nota Fiscal'
        ]);

        $codigoitem = Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'Fatura',
            'descricao' => 'Fatura'
        ]);

        $codigoitem = Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'Recibo',
            'descricao' => 'Recibo'
        ]);

        $codigoitem = Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'Boleto Bancário',
            'descricao' => 'Boleto Bancário'
        ]);

        $codigoitem = Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'Apólice de Seguro',
            'descricao' => 'Apólice de Seguro'
        ]);

        $codigoitem = Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'RPA',
            'descricao' => 'RPA'
        ]);

        $codigoitem = Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'Multa',
            'descricao' => 'Multa'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $codigo = Codigo::where('descricao', 'Tipo de Instrumento de Cobrança')->first();

        Codigoitem::where([
            'codigo_id' => $codigo->id,
            'descres' => 'Nota Fiscal',
            'descricao' => 'Nota Fiscal'
        ])->forceDelete();

        Codigoitem::where([
            'codigo_id' => $codigo->id,
            'descres' => 'Fatura',
            'descricao' => 'Fatura'
        ])->forceDelete();

        Codigoitem::where([
            'codigo_id' => $codigo->id,
            'descres' => 'Recibo',
            'descricao' => 'Recibo'
        ])->forceDelete();

        Codigoitem::where([
            'codigo_id' => $codigo->id,
            'descres' => 'Boleto Bancário',
            'descricao' => 'Boleto Bancário'
        ])->forceDelete();

        Codigoitem::where([
            'codigo_id' => $codigo->id,
            'descres' => 'Apólice de Seguro',
            'descricao' => 'Apólice de Seguro'
        ])->forceDelete();

        Codigoitem::where([
            'codigo_id' => $codigo->id,
            'descres' => 'RPA',
            'descricao' => 'RPA'
        ])->forceDelete();

        Codigoitem::where([
            'codigo_id' => $codigo->id,
            'descres' => 'Multa',
            'descricao' => 'Multa'
        ])->forceDelete();
    }
}
