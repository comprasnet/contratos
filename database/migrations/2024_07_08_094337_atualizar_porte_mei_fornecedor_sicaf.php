<?php

use App\Models\ArpItem;
use App\Models\Contrato;
use App\Models\Fornecedor;
use App\Models\FornecedorSicaf;
use App\Models\MinutaEmpenho;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AtualizarPorteMeiFornecedorSicaf extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $ambiente = config('app.app_amb');

        if ($ambiente === 'Ambiente Produção') {
            $minutasEmitidas = MinutaEmpenho::select('fornecedor_compra_id')
                ->distinct()
                ->where('situacao_id', function ($query) {
                    $query->select('id')
                        ->from('codigoitens')
                        ->where('descricao', 'EMPENHO EMITIDO')
                        ->where('descres', 'EMITIDO');
                })
                ->whereIn('tipo_empenhopor_id', function ($query) {
                    $query->select('id')
                        ->from('codigoitens')
                        ->whereIn('descricao', ['Compra', 'Contrato'])
                        ->whereIn('descres', ['COM', 'CON']);
                })
                ->where('data_emissao', '>', '2024-01-01')
                ->pluck('fornecedor_compra_id')
                ->toArray();

            $ataVigente = ArpItem::select('fornecedor_id')
                ->join('compra_item_fornecedor', function ($join) {
                    $join->on('compra_item_fornecedor.id', '=', 'arp_item.compra_item_fornecedor_id')
                        ->whereNull('arp_item.deleted_at')
                        ->whereNotNull('compra_item_fornecedor.ata_vigencia_fim');
                })
                ->join('arp', 'arp.id', '=', 'arp_item.arp_id')
                ->join('codigoitens', 'codigoitens.id', '=', 'arp.tipo_id')
                ->where('codigoitens.descricao', '=', 'Ata de Registro de Preços')
                ->whereRaw('now() between compra_item_fornecedor.ata_vigencia_inicio and compra_item_fornecedor.ata_vigencia_fim')
                ->distinct()
                ->pluck('fornecedor_id')
                ->toArray();

            $contratosAtivos = Contrato::where('situacao', true)->pluck('fornecedor_id')->toArray();

            $fornecedoresSicaf = FornecedorSicaf::pluck('fornecedor_id')->toArray();
            $arrayFornecedores = array_merge($minutasEmitidas, $fornecedoresSicaf, $ataVigente, $contratosAtivos);
            $arrayFornecedores = array_unique($arrayFornecedores);

            $delaySegundos = 1;
            foreach ($arrayFornecedores as $index => $arrayFornecedor) {
                $fornecedor = Fornecedor::where('id', $arrayFornecedor)->first();
                $novaData = now()->addSeconds($delaySegundos * $index);
                \App\Jobs\AtualizaFornecedorSicafJob::dispatch($fornecedor)->delay($novaData);
            }
        } else {
            #Script para homologação
            $delaySegundos = 1;
            $fornecedores = Fornecedor::whereHas('fornecedorSicaf')->limit(15)->get();
            foreach ($fornecedores as $index => $fornecedor) {
                $novaData = now()->addSeconds($delaySegundos * $index);
                \Illuminate\Support\Facades\Log::info($novaData);
                \App\Jobs\AtualizaFornecedorSicafJob::dispatch($fornecedor)->delay($novaData);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
