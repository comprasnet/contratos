<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTokenSeiToOrgaoConfiguracaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orgaoconfiguracao', function (Blueprint $table) {
            $table->string('chave_acesso_sei')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orgaoconfiguracao', function (Blueprint $table) {
            $table->dropColumn('chave_acesso_sei');
        });
    }
}
