<?php

use App\Models\SistemaOrigem;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateSistemaOrigemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sistema_origem', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome')->nullable();
            $table->string('id_sistema_origem');
            $table->timestamps();
        });

        DB::beginTransaction();
        try {
            SistemaOrigem::create([
                'id' => 1,
                'nome' => 'Contratos',
                'id_sistema_origem' => 'ContratosBR'
            ]);

            SistemaOrigem::create([
                'id' => 2,
                'nome' => 'Compras',
                'id_sistema_origem' => 'compras_api'
            ]);

            SistemaOrigem::create([
                'id' => 3,
                'nome' => 'FNDE',
                'id_sistema_origem' => 'fnde_api'
            ]);

            SistemaOrigem::create([
                'id' => 4,
                'nome' => 'AntecipaGov',
                'id_sistema_origem' => 'antecipagov_api'
            ]);

            SistemaOrigem::create([
                'id' => 5,
                'nome' => 'Bacen',
                'id_sistema_origem' => 'BCB-OAM'
            ]);

            SistemaOrigem::create([
                'id' => 6,
                'nome' => 'PMSP',
                'id_sistema_origem' => 'SOF-SP'
            ]);

            SistemaOrigem::create([
                'id' => 7,
                'nome' => 'Contratos',
                'id_sistema_origem' => 'contratos_api'
            ]);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sistema_origem');
    }
}
