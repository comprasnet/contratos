<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Artisan;


class ExecPopulaDadosGruposCatmatJson extends Migration
{
    public function up()
    {
        Artisan::call('db:seed', [
            '--class' => 'PopulaGrupoSeeder'
        ]);
    }

    public function down()
    {
        //
    }
}
