<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCollumnElaboracaoSaldohistoricoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('saldohistoricoitens', function (Blueprint $table) {
            $table->boolean('elaboracao')->default(false)
                ->comment('true - Em elaboracao (rascunho) | false - Ativo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('saldohistoricoitens', function (Blueprint $table) {
            $table->string('elaboracao');
        });
    }
}
