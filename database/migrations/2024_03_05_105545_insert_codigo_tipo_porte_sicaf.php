<?php

use App\Models\Codigo;
use App\Models\Codigoitem;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertCodigoTipoPorteSicaf extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = Codigo::firstOrCreate([
            'descricao' => 'Tipo Porte Empresa',
            'visivel' => false
        ]);

        $codigo1 = Codigo::firstOrCreate([
            'descricao' => 'Tipo Sistema Origem',
            'visivel' => false
        ]);

        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'PORT01','descricao' => '1 - MICROEMPRESA-ME','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'PORT03','descricao' => '3 - EMPRESAS DE PEQUENO PORTE EPP','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo->id,'descres' => 'PORT05','descricao' => '5 - DEMAIS EMPRESAS','visivel' => true]);

        #---------------- Tipo Sistema origem
        Codigoitem::firstOrCreate(['codigo_id' => $codigo1->id,'descres' => 'ORIGEM1','descricao' => 'SICAF','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo1->id,'descres' => 'ORIGEM2','descricao' => 'RFB','visivel' => true]);
        Codigoitem::firstOrCreate(['codigo_id' => $codigo1->id,'descres' => 'ORIGEM3','descricao' => 'N/D','visivel' => true]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $codigo = Codigo::where([
            'descricao' => 'Tipo Porte Empresa',
            'visivel' => false
        ])->first();

        Codigoitem::where([
            'descricao' => '1 - MICROEMPRESA-ME',
            'descres' => 'PORT01',
            'codigo_id' => $codigo->id
        ])->forceDelete();

        Codigoitem::where([
            'descricao' => '3 - EMPRESAS DE PEQUENO PORTE EPP',
            'descres' => 'PORT03',
            'codigo_id' => $codigo->id
        ])->forceDelete();

        Codigoitem::where([
            'descricao' => '5 - DEMAIS EMPRESAS',
            'descres' => 'PORT05',
            'codigo_id' => $codigo->id
        ])->forceDelete();

        $codigo->forceDelete();

        $codigo1 = Codigo::firstOrCreate([
            'descricao' => 'Tipo Sistema Origem',
            'visivel' => false
        ])->first();

        Codigoitem::where([
            'codigo_id' => $codigo1->id,
            'descres' => 'ORIGEM1',
            'descricao' => 'SICAF'])->forceDelete();

        Codigoitem::where([
            'codigo_id' => $codigo1->id,
            'descres' => 'ORIGEM2',
            'descricao' => 'RFB'])->forceDelete();
        Codigoitem::where([
            'codigo_id' => $codigo1->id,
            'descres' => 'ORIGEM3',
            'descricao' => 'N/D'])->forceDelete();

        $codigo1->forceDelete();
    }
}
