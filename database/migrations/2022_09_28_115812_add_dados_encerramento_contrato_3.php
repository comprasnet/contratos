<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDadosEncerramentoContrato3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('contratos', 'data_encerramento')){
            Schema::table('contratos', function (Blueprint $table) {
                $table->date('data_encerramento')->nullable();
            });
        }
        if(!Schema::hasColumn('contratos', 'is_objeto_contratual_entregue')){
            Schema::table('contratos', function (Blueprint $table) {
                $table->boolean('is_objeto_contratual_entregue')->nullable()->default(false);
            });
        }
        if(!Schema::hasColumn('contratos', 'is_garantia_contratual_devolvida')){
            Schema::table('contratos', function (Blueprint $table) {
                $table->boolean('is_garantia_contratual_devolvida')->nullable()->default(false);
            });
        }
        if(!Schema::hasColumn('contratos', 'is_cumpridas_obrigacoes_financeiras')){
            Schema::table('contratos', function (Blueprint $table) {
                $table->boolean('is_cumpridas_obrigacoes_financeiras')->nullable()->default(false);
            });
        }
        if(!Schema::hasColumn('contratos', 'is_saldo_conta_vinculada_liberado')){
            Schema::table('contratos', function (Blueprint $table) {
                $table->boolean('is_saldo_conta_vinculada_liberado')->nullable()->default(null);
            });
        }
        if(!Schema::hasColumn('contratos', 'nome_responsavel_encerramento')){
            Schema::table('contratos', function (Blueprint $table) {
                $table->string('nome_responsavel_encerramento')->nullable()->default(null);
            });
        }
        if(!Schema::hasColumn('contratos', 'cpf_responsavel_encerramento')){
            Schema::table('contratos', function (Blueprint $table) {
                $table->string('cpf_responsavel_encerramento')->nullable();
            });
        }
        if(!Schema::hasColumn('contratos', 'user_id_responsavel_encerramento')){
            Schema::table('contratos', function (Blueprint $table) {
                $table->integer('user_id_responsavel_encerramento')->nullable()->default(null);
                $table->foreign('user_id_responsavel_encerramento', 'user_encerramento')->references('id')->on('users');
            });
        }
        if(!Schema::hasColumn('contratos', 'hora_encerramento')){
            Schema::table('contratos', function (Blueprint $table) {
                $table->time('hora_encerramento')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contratos', function ($table) {

            $table->dropColumn('data_encerramento');

            $table->dropColumn('is_objeto_contratual_entregue');
            $table->dropColumn('is_garantia_contratual_devolvida');
            $table->dropColumn('is_cumpridas_obrigacoes_financeiras');
            $table->dropColumn('is_saldo_conta_vinculada_liberado');

            $table->dropColumn('hora_encerramento');
            $table->dropColumn('nome_responsavel_encerramento');
            $table->dropColumn('cpf_responsavel_encerramento');
            $table->dropColumn('user_id_responsavel_encerramento');

        });
    }
}
