<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PopulaColunaVencimentoPrazoContratofaturas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Atualiza a coluna 'vencimento' com o valor de 'prazo' caso 'vencimento' esteja vazio
        DB::table('contratofaturas')
            ->whereNull('vencimento')
            ->update(['vencimento' => DB::raw('prazo')]);

        // Atualiza a coluna 'prazo' com o valor de 'vencimento' caso 'prazo' esteja vazio
        DB::table('contratofaturas')
            ->whereNull('prazo')
            ->update(['prazo' => DB::raw('vencimento')]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
