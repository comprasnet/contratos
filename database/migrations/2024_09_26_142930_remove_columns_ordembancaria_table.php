<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveColumnsOrdembancariaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $ordensBancarias = \App\Models\OrdemBancaria::whereNull('domicilio_bancario_id')->count();

        if ($ordensBancarias == 0) {
            if (Schema::hasColumn('ordens_bancarias', 'domicilio_bancario_id')) {
                Schema::table('ordens_bancarias', function (Blueprint $table) {
                    $table->dropColumn(['bancodestino', 'agenciadestino', 'contadestino']);
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ordens_bancarias', function (Blueprint $table) {
            $table->string('bancodestino', 40);
            $table->string('agenciadestino', 40);
            $table->string('contadestino', 40);
        });
    }
}
