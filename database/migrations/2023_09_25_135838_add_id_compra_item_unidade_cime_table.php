<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdCompraItemUnidadeCimeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('compra_item_minuta_empenho', function (Blueprint $table) {
            $table->integer('compra_item_unidade_id')
                ->foreign('id_compra_item_unidade')
                ->references('id')
                ->on('compra_item_unidade')
                ->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('compra_item_minuta_empenho', function (Blueprint $table) {
            $table->dropForeign(['compra_item_unidade_id']);
            $table->dropColumn('compra_item_unidade_id');
        });
    }
}
