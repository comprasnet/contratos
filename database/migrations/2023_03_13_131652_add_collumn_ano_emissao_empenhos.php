<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCollumnAnoEmissaoEmpenhos extends Migration
{
    public function up()
    {
        if(!Schema::hasColumn('empenhos', 'ano_emissao')){
            Schema::table('empenhos', function (Blueprint $table) {
                $table->integer('ano_emissao')->nullable();
            });
        }

    }

    public function down()
    {
        Schema::table('empenhos', function (Blueprint $table) {
            $table->dropColumn('ano_emissao');
        });
    }
}
