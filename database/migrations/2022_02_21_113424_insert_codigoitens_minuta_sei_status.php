<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertCodigoitensMinutaSeiStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = \App\Models\Codigo::create([
            'descricao' => 'Status Contrato Minuta SEI',
            'visivel' => false
        ]);

        \App\Models\Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'EELB',
            'descricao' => 'Em elaboração',
            'visivel' => false
        ]);

        \App\Models\Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'PENV',
            'descricao' => 'Pronto para envio',
            'visivel' => false
        ]);

        \App\Models\Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'ENV',
            'descricao' => 'Enviado',
            'visivel' => false
        ]);

        \App\Models\Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'ERRO',
            'descricao' => 'Erro',
            'visivel' => false
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\Models\Codigo::where([
            'descricao' => 'Status Contrato Minuta SEI',
            'visivel' => false
        ])->forceDelete();

    }
}
