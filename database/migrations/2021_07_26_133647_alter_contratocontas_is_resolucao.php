<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterContratocontasIsResolucao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contratocontas', function (Blueprint $table) {
            $table->boolean('is_conta_vinculada_pela_resolucao169_cnj')->nullable()->false;
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contratocontas', function (Blueprint $table) {
            $table->dropColumn('is_conta_vinculada_pela_resolucao169_cnj');
        });
    }
}
