<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCollumnCodigoCatmatsergruposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('catmatsergrupos', 'codigo')){
            Schema::table('catmatsergrupos', function (Blueprint $table) {
                $table->string('codigo')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('catmatsergrupos', function (Blueprint $table) {
            $table->dropColumn('codigo');
        });
    }
}
