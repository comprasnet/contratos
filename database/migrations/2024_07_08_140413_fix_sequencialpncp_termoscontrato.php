<?php

use App\Http\Controllers\Api\PNCP\PncpController;
use App\Http\Controllers\Api\PNCP\UsuarioController;
use App\Models\Codigoitem;
use App\Models\EnviaDadosPncp;
use App\Models\PncpUsuario;
use GuzzleHttp\Client;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class FixSequencialpncpTermoscontrato extends Migration
{
    private $pncpController;
    private $idIncPen;
    private $idExcPen;
    private $idExcluido;
    private $tiposContrato = [];

    public function __construct()
    {
        $this->pncpController = new PncpController();
        $this->idIncPen = Codigoitem::where('descres', 'INCPEN')->first()->id;
        $this->idExcPen = Codigoitem::where('descres', 'EXCPEN')->first()->id;
        $this->idExcluido = Codigoitem::where('descres', 'EXCLUIDO')->first()->id;
        $this->tiposContrato = [
            'Contrato',
            'Empenho',
            'Comodato',
            'Arrendamento',
            'Concessão',
            'Termo de Adesão',
            'Outros',
            'Carta Contrato'
        ];
    }

    public function up(): void
    {
        $termosComSequencialErrado = $this->getTermosComSequencialErrado();
        foreach ($termosComSequencialErrado as $i => $termo) {
            # Busca o sequencial do contrato, pois foi identificado que o sequencial do termo pode estar errado
            # Exemplo:
            # Sequencial do termo está assim...............: 26474056000171-2-000001/2021 # seq. contrato = 1
            # Sendo que o sequencial do contrato está assim: 26474056000171-2-000002/2021 # seq. contrato = 2 (correto)
            $dadosDoContratoPaiNaTabelaEnviaDadosPncp =
                $this->getDadosDoContratoPaiNaTabelaEnviaDadosPncp($termo->contrato_id);

            $sequencialDoContratoPaiPncp = $this->verificaDadosSequencial($dadosDoContratoPaiNaTabelaEnviaDadosPncp);
            if (!$sequencialDoContratoPaiPncp) {
                EnviaDadosPncp::where('id', $termo->envia_dados_pncp_id)
                    ->update([
                        'sequencialPNCP' => null,
                        'link_pncp' => null,
                        'situacao' => $this->idExcluido,
                        'json_enviado_inclusao' =>
                            '{"erro": "Não foi possível identificar o sequencial do contrato."}',
                    ]);
                    continue;
            }

            $termosDoContratoNoPncp = $this->buscarTodosOsTermosDeUmContratoNoPncp(
                $sequencialDoContratoPaiPncp['cnpj'],
                $sequencialDoContratoPaiPncp['ano'],
                $sequencialDoContratoPaiPncp['sequencialContrato']);

            $termoNoPncp = null;
            # Se a busca retornou dados, verifica se o termo existe no PNCP
            if ($termosDoContratoNoPncp) {
                $termoNoPncp = $this->termoExisteNoPncp($termosDoContratoNoPncp, $termo);
            }

            # Se o termo existir no PNCP, atualiza com o sequencial e link corretos
            if ($termoNoPncp) {
                EnviaDadosPncp::where('id', $termo->envia_dados_pncp_id)
                    ->update([
                        'sequencialPNCP' => $termoNoPncp['sequencialTermoContrato'],
                        'link_pncp' => $this->pncpController->montaUrl(
                                $this->urlConsultaContrato(
                                    $sequencialDoContratoPaiPncp['cnpj'],
                                    $sequencialDoContratoPaiPncp['ano'],
                                    $sequencialDoContratoPaiPncp['sequencialContrato']), false) .
                            '/' . $termoNoPncp['sequencialTermoContrato']
                    ]);
            } else {
                $termoNaTabelaEnviaDadosPncp = EnviaDadosPncp::find($termo->envia_dados_pncp_id);
                $termoNaTabelaEnviaDadosPncp->sequencialPNCP = null;
                $termoNaTabelaEnviaDadosPncp->link_pncp = null;
                # Se a situação for EXCPEN, passa para EXCLUIDO
                if ($termoNaTabelaEnviaDadosPncp->situacao == $this->idExcPen) {
                    $termoNaTabelaEnviaDadosPncp->situacao = $this->idExcluido;
                } else { # Por padrão, marca como INCPEN para enviar para o PNCP
                    $termoNaTabelaEnviaDadosPncp->situacao = $this->idIncPen;
                }
                $termoNaTabelaEnviaDadosPncp->save();
            }
        }
    }

    public function down(): void
    {
        Schema::table('envia_dados_pncp', function (Blueprint $table) {
            //
        });
    }

    private function getTermosComSequencialErrado()
    {
        return EnviaDadosPncp::select(
            'envia_dados_pncp.id as envia_dados_pncp_id',
            'envia_dados_pncp.pncpable_id',
            'envia_dados_pncp.contrato_id',
            'envia_dados_pncp.tipo_contrato',
            'contratohistorico.numero',
            'envia_dados_pncp.situacao',
            'codigoitens.descres',
            'envia_dados_pncp.sequencialPNCP',
            'envia_dados_pncp.link_pncp'
        )
            ->join('codigoitens',
                'codigoitens.id', '=', 'envia_dados_pncp.situacao')
            ->leftJoin('contratohistorico',
                'contratohistorico.id', '=', 'envia_dados_pncp.pncpable_id')
            ->where('envia_dados_pncp.pncpable_type', 'App\Models\Contratohistorico')
            ->whereIn('envia_dados_pncp.tipo_contrato', ['Termo Aditivo', 'Termo de Apostilamento', 'Termo de Rescisão'])
            ->where('envia_dados_pncp.sequencialPNCP', '~', '[^0-9]')
            ->orderBy('envia_dados_pncp.contrato_id', 'ASC')
            ->orderBy('envia_dados_pncp.pncpable_id', 'ASC')
            ->get();
    }

    private function getDadosDoContratoPaiNaTabelaEnviaDadosPncp(int $contratoId)
    {
        return EnviaDadosPncp::select('sequencialPNCP', 'link_pncp')
            ->where('contrato_id', $contratoId)
            ->whereIn('tipo_contrato', $this->tiposContrato)
            ->oldest()
            ->first();
    }

    private function verificaDadosSequencial($dadosContratoNaTabelaEnviaDadosPncp)
    {
        $sequencial = $this->divideSequencial($dadosContratoNaTabelaEnviaDadosPncp['sequencialPNCP']);
        if (!$sequencial) {
            Log::error(__FILE__ . ' - Não foi possível identificar o sequencial do contrato: ' .
                json_encode($dadosContratoNaTabelaEnviaDadosPncp,JSON_UNESCAPED_SLASHES));
            return false;
        }
        return $sequencial;
    }

    private function buscarTodosOsTermosDeUmContratoNoPncp(string $cnpj, string $ano, string $sequencial)
    {
        $url = $this->pncpController->montaUrl($this->urlConsultaContrato($cnpj, $ano, $sequencial), null);
        try {
            $client = new Client(['verify' => false]);
            $response = $client->request('GET', $url);
            return json_decode($response->getBody()->getContents(), true);
        } catch (Exception $e) {
            Log::error("Erro ao consultar termos de contrato: " . $e->getMessage());
            return false;
        }
    }

    private function termoExisteNoPncp($termosDoContratoNoPncp, $termo)
    {
        foreach ($termosDoContratoNoPncp as $termoNoPncp) {
            if (
                $termoNoPncp['numeroTermoContrato'] == $termo->numero &&
                $termoNoPncp['tipoTermoContratoNome'] == $termo->tipo_contrato) {
                return $termoNoPncp;
            }
        }
        return false;
    }

    private function divideSequencial($sequencialPncp)
    {
        if (!$sequencialPncp) return false;
        $hyphenParts = explode('-', $sequencialPncp);
        if (count($hyphenParts) != 3) {
            return null;
        }
        [$cnpj, $codigo, $sequencialAno] = $hyphenParts;
        $slashParts = explode('/', $sequencialAno);
        if (count($slashParts) != 2) {
            return null;
        }
        [$sequencialContrato, $ano] = $slashParts;
        $array = [
            'cnpj' => $cnpj,
            'codigo' => $codigo,
            'sequencialContrato' => $sequencialContrato,
            'ano' => $ano
        ];
        return $array;
    }

    private function urlConsultaContrato(string $cnpj, string $ano, string $sequencial)
    {
        return "v1/orgaos/" . $cnpj . "/contratos/" . $ano . "/" . $sequencial . "/termos";
    }

    private function getDadosLink($link_pncp)
    {
        $dados = explode('/', $link_pncp);
        $indice = 0;
        for ($i = count($dados) - 1; $indice <= 3; $i--) {
            switch ($indice) {
                case 0 :
                    $retorno['sequencialContrato'] = str_pad($dados[$i], 6, '0', STR_PAD_LEFT);
                    break;
                case 1 :
                    $retorno['ano'] = $dados[$i];
                    break;
                case 3 :
                    $retorno['cnpj'] = $dados[$i];
                    break;
            }
            $indice++;
        }

        return $retorno;
    }

    private function confereDadosLinkSequencial($sequencial, $link_pncp): bool
    {
        if ($sequencial['cnpj'] != $link_pncp['cnpj'] ||
            $sequencial['ano'] != $link_pncp['ano'] ||
            $sequencial['sequencialContrato'] != $link_pncp['sequencialContrato']) {

            Log::error(
                'Os dados do sequencial do termo não correspondem aos dados do link pncp. ' .
                ' sequencial cnpj: ' . $sequencial['cnpj'] . ' != link cnpj: ' . $link_pncp['cnpj'] .
                ' sequencial ano: ' . $sequencial['ano'] . ' != link ano: ' . $link_pncp['ano']);
            return false;
        }
        return true;
    }

    private function loginPncp()
    {
        try {
            $uc = new UsuarioController();
            $uc->login();
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
        return true;
    }

    private function logTermoSequencialContratoInvalido($termo, $msg)
    {
        Log::error(
            $msg . ' ' .
            "ID envia_dados_pncp: {$termo->id}; " .
            "ID do termo: {$termo->pncpable_id}; " .
            "Situação: {$termo->situacao}; " .
            "Tipo contrato: {$termo->tipo_contrato}; " .
            "Sequencial: {$termo->sequencialPNCP}");
    }
}
