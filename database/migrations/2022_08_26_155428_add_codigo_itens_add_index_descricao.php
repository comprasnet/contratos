<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCodigoItensAddIndexDescricao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::table('codigoitens', function (Blueprint $table) {
        //     $table->index(['descricao', 'descricao_item_index']);

        // });

        Schema::table('codigoitens', function (Blueprint $table) {
            $table->index(['descricao', 'descricao']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
