<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InserirNovosCampoTermoEncerramentoContrato extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contratos', function (Blueprint $table) {
            $table->integer('grau_satisfacao_desempenho_contrato')->nullable();
            $table->boolean('planejamento_contratacao_atendida')->nullable();
            $table->string('sugestao_licao_aprendida')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contratos', function ($table) {
            $table->dropColumn('grau_satisfacao_desempenho_contrato');
            $table->dropColumn('planejamento_contratacao_atendida');
            $table->dropColumn('sugestao_licao_aprendida');
        });
    }
}
