<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertCodigoTipoDocSei extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = \App\Models\Codigo::create([
            'descricao' => 'Tipos Documentos SEI',
            'visivel' => true
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\Models\Codigo::where([
            'descricao' => 'Tipos Documentos SEI',
            'visivel' => true
        ])->forceDelete();

    }
}
