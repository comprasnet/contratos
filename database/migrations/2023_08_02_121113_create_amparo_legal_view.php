<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmparoLegalView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE OR REPLACE VIEW amparo_legal_view AS
            SELECT
                amparo_legal.id,
                amparo_legal.ato_normativo ||
                CASE
                    WHEN (amparo_legal.artigo IS NOT NULL) THEN \' - Artigo: \' || amparo_legal.artigo
                    ELSE \'\'
                END ||
                CASE
                    WHEN (amparo_legal.paragrafo IS NOT NULL) THEN \' - Parágrafo: \' || amparo_legal.paragrafo
                    ELSE \'\'
                END ||
                CASE
                    WHEN (amparo_legal.inciso IS NOT NULL) THEN \' - Inciso: \' || amparo_legal.inciso
                    ELSE \'\'
                END ||
                CASE
                    WHEN (amparo_legal.alinea IS NOT NULL) THEN \' - Alinea: \' || amparo_legal.alinea
                    ELSE \'\'
                END AS amparo_legal
            FROM amparo_legal;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS amparo_legal_view;');
    }
}
