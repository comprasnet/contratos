<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnviaDadosPncpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('envia_dados_pncp', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->morphs('pncpable');
            $table->integer('situacao'); // Inclusão Pendente; Alteração Pendente; Erro; Sucesso;
            $table->text('json_enviado_inclusao')->nullable();
            $table->text('json_enviado_alteracao')->nullable();
            $table->text('retorno_pncp')->nullable();
            $table->string('link_pncp')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('envia_dados_pncp');
    }
}
