<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class UpdadeEnviadadospncpTipoContrato extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('envia_dados_pncp')->whereNotNull('contrato_id')
            ->update([
                'tipo_contrato' => 'Contrato',
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('envia_dados_pncp')->whereNotNull('contrato_id')
            ->update([
                'tipo_contrato' => null,
            ]);
    }
}
