<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObxneTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('obxne', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordembancaria_id')->unsigned();
            $table->integer('empenho_id')->nullable()->unsigned();
            $table->string('numeroempenho', 180);
            $table->timestamps();

            $table->foreign('empenho_id')->references('id')->on('empenhos')->onDelete('cascade');
            $table->foreign('ordembancaria_id')->references('id')->on('ordens_bancarias')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('obxne');
    }
}
