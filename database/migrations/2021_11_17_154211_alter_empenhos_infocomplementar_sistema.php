<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterEmpenhosInfocomplementarSistema extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empenhos', function (Blueprint $table) {
            $table->text('informacao_complementar')->nullable();
            $table->string('sistema_origem')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empenhos', function (Blueprint $table) {
            $table->dropColumn('informacao_complementar');
            $table->dropColumn('sistema_origem');
        });
    }
}
