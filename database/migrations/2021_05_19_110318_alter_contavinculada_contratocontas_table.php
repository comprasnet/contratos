<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterContavinculadaContratocontasTable extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contratocontas', function (Blueprint $table) {

            $table->integer('percentual_grupo_a_13_ferias_codigoitens_id')->nullable();
            $table->integer('percentual_submodulo_22_id')->nullable();

            $table->foreign('percentual_grupo_a_13_ferias_codigoitens_id')->references('id')->on('codigoitens');
            $table->foreign('percentual_submodulo_22_id')->references('id')->on('codigoitens');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contratocontas', function ($table) {
            $table->dropColumn('percentual_grupo_a_13_ferias_codigoitens_id');
            $table->dropColumn('percentual_submodulo_22_id');
        });

    }
}
