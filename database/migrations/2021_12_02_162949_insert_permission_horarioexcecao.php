<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class InsertPermissionHorarioexcecao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        app()['cache']->forget('spatie.permission.cache');

        Permission::create(['name' => 'horariosiafi_acesso']);
        Permission::create(['name' => 'horariosiafi_inserir']);
        Permission::create(['name' => 'horariosiafi_editar']);
        Permission::create(['name' => 'horariosiafi_deletar']);

        $role = Role::where(['name' => 'Administrador'])->first();
        $role->givePermissionTo('horariosiafi_acesso');
        $role->givePermissionTo('horariosiafi_inserir');
        $role->givePermissionTo('horariosiafi_editar');
        $role->givePermissionTo('horariosiafi_deletar');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        $role = Role::where(['name' => 'Administrador'])->first();
        $role->revokePermissionTo('horariosiafi_acesso');
        $role->revokePermissionTo('horariosiafi_inserir');
        $role->revokePermissionTo('horariosiafi_editar');
        $role->revokePermissionTo('horariosiafi_deletar');

        Permission::where(['name' => 'horariosiafi_acesso'])->forceDelete();
        Permission::where(['name' => 'horariosiafi_inserir'])->forceDelete();
        Permission::where(['name' => 'horariosiafi_editar'])->forceDelete();
        Permission::where(['name' => 'horariosiafi_deletar'])->forceDelete();
    }
}
