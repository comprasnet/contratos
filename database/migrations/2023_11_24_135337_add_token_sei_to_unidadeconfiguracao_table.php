<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTokenSeiToUnidadeconfiguracaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('unidadeconfiguracao', function (Blueprint $table) {
            $table->string('chave_acesso_sei',191)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('unidadeconfiguracao', function (Blueprint $table) {
            $table->dropColumn('chave_acesso_sei');
        });
    }
}
