<?php

use App\Models\Codigo;
use App\Models\Codigoitem;
use Illuminate\Database\Migrations\Migration;

class InsertStatsApropriacaoInstCobrancaCodigoitensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = Codigo::create([
            'descricao' => 'Status Apropriação Instrumento de Cobrança SIAFI',
            'visivel' => false //caso true permite que os usuários alterem via sistema
        ]);

        Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'AND',
            'descricao' => 'Em Andamento'
        ]);

        Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'PROC',
            'descricao' => 'Em Processamento'
        ]);

        Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'ENV',
            'descricao' => 'Siafi Enviado'
        ]);

        Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'ERR',
            'descricao' => 'Siafi Erro'
        ]);

        Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'PEN',
            'descricao' => 'Pendente'
        ]);

        Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'PGS',
            'descricao' => 'Pagamento Suspenso'
        ]);

        Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'PGP',
            'descricao' => 'Pagamento Parcial'
        ]);

        Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'ANA',
            'descricao' => 'Analisado'
        ]);

        Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'PPG',
            'descricao' => 'Pronto para Pagamento'
        ]);

        Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'PGO',
            'descricao' => 'Pago'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Codigo::where([
            'descricao' => 'Status Apropriação Instrumento de Cobrança SIAFI',
            'visivel' => false
        ])->forceDelete();
    }
}
