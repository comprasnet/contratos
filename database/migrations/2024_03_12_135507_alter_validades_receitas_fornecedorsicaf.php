<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterValidadesReceitasFornecedorsicaf extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fornecedorsicaf', function (Blueprint $table) {
            $table->boolean('validade_receita_federal_pgfn')->nullable()->change();
            $table->boolean('validade_certidao_fgts')->nullable()->change();
            $table->boolean('validade_certidao_trabalhista')->nullable()->change();
            $table->boolean('validate_receita_estadual_distrital')->nullable()->change();
            $table->boolean('validade_receita_municipal')->nullable()->change();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
