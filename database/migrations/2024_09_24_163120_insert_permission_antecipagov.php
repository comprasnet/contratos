<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class InsertPermissionAntecipagov extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        app()['cache']->forget('spatie.permission.cache');

        Permission::create(['name' => 'ordem_bancaria_apropriacao_instcobranca_acesso']);

        $role = Role::where(['name' => 'AntecipaGov'])->first();
        $role1 = Role::where(['name' => 'Administrador'])->first();
        if (!$role) {
            $role = Role::create(['name' => 'AntecipaGov']);
        }
        $role->givePermissionTo('ordem_bancaria_apropriacao_instcobranca_acesso');
        $role1->givePermissionTo('ordem_bancaria_apropriacao_instcobranca_acesso');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $role = Role::where(['name' => 'AntecipaGov'])->first();
        $role->revokePermissionTo('ordem_bancaria_apropriacao_instcobranca_acesso');

        $role1 = Role::where(['name' => 'Administrador'])->first();
        $role1->revokePermissionTo('ordem_bancaria_apropriacao_instcobranca_acesso');


        Permission::where(['name' => 'ordem_bancaria_apropriacao_instcobranca_acesso'])->forceDelete();
        $role->forceDelete();
    }
}
