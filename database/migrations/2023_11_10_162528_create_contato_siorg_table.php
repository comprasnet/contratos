<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContatoSiorgTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contato_siorg', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo_siorg')->index();
            $table->string('telefone')->nullable();
            $table->string('email')->nullable();
            $table->string('site')->nullable();

            $table->foreign('codigo_siorg')->references('codigo')->on('siorg')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contato_siorg', function (Blueprint $table) {
            $table->dropForeign(['codigo_siorg']);
        });

        Schema::dropIfExists('contato_siorg');
    }
}
