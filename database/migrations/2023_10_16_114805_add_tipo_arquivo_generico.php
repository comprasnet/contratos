<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \App\Models\Codigo;
use \App\Models\Codigoitem;

class AddTipoArquivoGenerico extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = Codigo::create(
            [
                'descricao' => 'Tipo arquivo generico',
                'visivel' => true
            ]);

        Codigoitem::create([
            'descres' => 'ARQ_MINUTA_EMPENHO',
            'descricao' => 'Arquivo minuta de empenho',
            'codigo_id' => $codigo->id
        ]);

        Schema::table('arquivo_generico', function (Blueprint $table) {
            $table->integer('user_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Codigo::where(
            [
                'descricao' => 'Tipo arquivo generico'
            ])->forceDelete();

        Codigoitem::where([
            'descres' => 'ARQ_MINUTA_EMPENHO',
            'descricao' => 'Arquivo minuta de empenho'
        ])->forceDelete();

        Schema::table('arquivo_generico', function (Blueprint $table) {
            $table->integer('user_id')->change();
        });
    }
}
