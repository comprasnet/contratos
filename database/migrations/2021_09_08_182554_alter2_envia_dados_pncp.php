<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Alter2EnviaDadosPncp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('envia_dados_pncp', function (Blueprint $table) {
            $table->bigInteger('contrato_id')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('envia_dados_pncp', function (Blueprint $table) {
            $table->integer('contrato_id')->change();
        });
    }
}
