<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdCompraItemFornecedorCimeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('compra_item_minuta_empenho', function (Blueprint $table) {
            $table->integer('compra_item_fornecedor_id')
                ->foreign('id_compra_item_fornecedor')
                ->references('id')
                ->on('compra_item_fornecedor')
                ->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('compra_item_minuta_empenho', function (Blueprint $table) {
            $table->dropForeign(['compra_item_fornecedor_id']);
            $table->dropColumn('compra_item_fornecedor_id');
        });
    }
}
