<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAlteracaofonteMinutaempenhosRemessaCollumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('minutaempenhos_remessa', function (Blueprint $table) {
            $table->bigInteger('alteracao_fonte_minutaempenho_id')->nullable();
            $table->foreign('alteracao_fonte_minutaempenho_id')->references('id')
                ->on('minutaempenhos')
                ->comment('Guardar o id da nova minuta com fonte alterada');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('minutaempenhos_remessa', function (Blueprint $table) {
            $table->dropColumn('alteracao_fonte_minutaempenho_id');
        });
    }
}
