<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModeloDocumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modelo_documentos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descricao', 80);
            $table->text('template');
            $table->boolean('ativo');
            $table->integer('codigoitens_id');
            //$table->integer('tipo_acao_id');
            //$table->integer('tipo_documento_id');
            //$table->foreign('tipo_acao_id')->references('id')->on('tipo_acao_analises');
            //$table->foreign('tipo_documento_id')->references('id')->on('tipo_documentos');
            $table->foreign('codigoitens_id')->references('id')->on('codigoitens');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modelo_documentos');
    }
}
