<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContratofaturasItensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contratofaturas_itens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contratofaturas_id');
            $table->integer('saldohistoricoitens_id');
            $table->decimal('quantidade_faturado', 15,5);
            $table->decimal('valorunitario_faturado',19,4);
            $table->decimal('valortotal_faturado',17,2);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('contratofaturas_id')->references('id')->on('contratofaturas')->onDelete('cascade');
            $table->foreign('saldohistoricoitens_id')->references('id')->on('saldohistoricoitens')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contratofaturas_itens');
    }
}
