<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSistemaOrigemColumnPlanointerno extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('planointerno', function (Blueprint $table) {
            $table->string('sistema_origem')->default('SIAFI');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('planointerno', 'sistema_origem')){
            Schema::table('planointerno', function (Blueprint $table) {
                $table->dropColumn('sistema_origem');
            });
        }
    }
}
