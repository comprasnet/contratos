<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Doctrine\DBAL\Types\FloatType;
use Doctrine\DBAL\Types\Type;

class AlterCollumnValorunitarioContratoitens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Type::hasType('double')) {
            Type::addType('double', FloatType::class);
        }
        Schema::table('saldohistoricoitens', function (Blueprint $table) {
            $table->double('valorunitario',19,7)->default(0)->change();
        });
        Schema::table('contratoitens', function (Blueprint $table) {
            $table->double('valorunitario',19,7)->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('saldohistoricoitens', function (Blueprint $table) {
            $table->decimal('valorunitario',19,4)->default(0)->change();
        });
        Schema::table('contratoitens', function (Blueprint $table) {
            $table->decimal('valorunitario',19,4)->default(0)->change();
        });
    }
}
