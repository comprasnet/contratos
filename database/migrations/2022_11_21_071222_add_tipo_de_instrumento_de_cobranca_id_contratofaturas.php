<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTipoDeInstrumentoDeCobrancaIdContratofaturas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('contratofaturas', 'tipo_de_instrumento_de_cobranca_id')){
            Schema::table('contratofaturas', function (Blueprint $table) {
                $table->integer('tipo_de_instrumento_de_cobranca_id')->nullable();
                $table->foreign('tipo_de_instrumento_de_cobranca_id', 'tipodeinstrumentodecobranca')->references('id')->on('codigoitens')->nullable()->default(null);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contratofaturas', function (Blueprint $table) {
            $table->dropColumn(['tipo_de_instrumento_de_cobranca_id']);
        });
    }
}
