<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTriggerAtualizaSaldosTratarUltimoEmpenhodetalhado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
            CREATE OR REPLACE FUNCTION atualiza_saldos() RETURNS TRIGGER AS $$
            BEGIN
                IF NOT EXISTS (
                    SELECT 1
                    FROM empenhodetalhado
                    WHERE empenho_id = OLD.empenho_id
                        AND deleted_at IS NULL
                    LIMIT 1
                ) THEN
                    -- Se não existirem outros empenhodetalhados, atualiza os valores do empenho para refletir a exclusão do último item
                    UPDATE empenhos
                    SET aliquidar = 0,
                        liquidado = 0,
                        pago = 0,
                        empenhado = 0,
                        rpinscrito = 0,
                        rpaliquidar = 0,
                        rpliquidado = 0,
                        rppago = 0,
                        updated_at = NOW()
                    WHERE id = OLD.empenho_id;
                ELSE
                    -- Se ainda houver outros empenhodetalhados, atualiza os valores do empenho conforme a soma dos valores dos empenhodetalhados restantes
                    UPDATE empenhos
                    SET aliquidar = origem.aliquidar,
                        liquidado = origem.liquidado,
                        pago = origem.pago,
                        empenhado = origem.empenhado,
                        rpinscrito = origem.rpinscrito,
                        rpaliquidar = origem.rpaliquidar,
                        rpliquidado = origem.rpliquidado,
                        rppago = origem.rppago,
                        updated_at = NOW()
                    FROM (
                        SELECT empenho_id,
                         -- A LIQUIDAR
                         COALESCE(SUM(empaliquidar), 0) +
                         COALESCE(SUM(empemliquidacao), 0)                          AS aliquidar,

                         -- LIQUIDADO
                         COALESCE(SUM(emprpp), 0) +
                         COALESCE(SUM(empliquidado), 0)                             AS liquidado,

                         -- PAGO
                         COALESCE(SUM(emppago), 0)                                  AS pago,

                         -- EMPENHADO
                         COALESCE(SUM(empaliquidar), 0) +
                         COALESCE(SUM(empemliquidacao), 0) +
                         COALESCE(SUM(empliquidado), 0) +
                         COALESCE(SUM(empaliqrpnp), 0) +
                         COALESCE(SUM(empemliqrpnp), 0) +
                         COALESCE(SUM(emprpp), 0) +
                         COALESCE(SUM(emppago), 0)                                  AS empenhado,

                         -- RP A LIQUIDAR
                         COALESCE(SUM(rpnpaliquidar), 0) +
                         COALESCE(SUM(rpnpaliquidaremliquidacao), 0)                AS rpaliquidar,

                         -- RP LIQUIDADO
                         COALESCE(SUM(rpnpaliquidar), 0) +
                         COALESCE(SUM(rpnpaliquidaremliquidacao), 0) +
                         COALESCE(SUM(rpnpliquidado), 0) +
                         COALESCE(SUM(rpnpaliquidarbloq), 0) +
                         COALESCE(SUM(rpnpaliquidaremliquidbloq), 0) +
                         COALESCE(SUM(rppliquidado), 0)                             AS rpliquidado,

                         -- RP PAGO
                         COALESCE(SUM(rpnppago), 0) +
                         COALESCE(SUM(rpppago), 0)                                  AS rppago,

                         -- RP INSCRITO
                         COALESCE(SUM(rpnpaliquidinsc), 0) + COALESCE(SUM(rpnpemliquidinsc), 0) +
                         COALESCE(SUM(reinscrpnpaliquidbloq), 0) + COALESCE(SUM(reinscrpnpemliquid), 0) +
                         COALESCE(SUM(rpnprestab), 0) + COALESCE(SUM(rpnpaliquidtransfdeb), 0) +
                         COALESCE(SUM(rpnpaliquidemliquidtransfdeb), 0) + COALESCE(SUM(rpnpliquidapgtransfdeb), 0) +
                         COALESCE(SUM(rpnpbloqtransfdeb), 0) + COALESCE(SUM(rppinsc), 0) + COALESCE(SUM(rppexecant), 0)+
                         COALESCE(SUM(rpptrasf), 0) - COALESCE(SUM(rpnpaliquidtransfcred), 0) -
                         COALESCE(SUM(rpnpaliquidemliquidtransfcred), 0) - COALESCE(SUM(rpnpliquidapgtransfcred), 0) -
                         COALESCE(SUM(rpnpbloqtransfcred), 0) - COALESCE(SUM(rpptransffusao), 0) -
                         COALESCE(SUM(ajusterpexecant), 0) - COALESCE(SUM(rpnpcancelado), 0) -
                         COALESCE(SUM(rpnpoutrocancelamento), 0) - COALESCE(SUM(rpnpemliqoutrocancelamento), 0) -
                         COALESCE(SUM(rpppago), 0) - COALESCE(SUM(rppcancelado), 0) AS rpinscrito

              FROM empenhodetalhado
              WHERE empenho_id = NEW.empenho_id
                AND deleted_at IS NULL
              GROUP BY empenho_id) origem
        WHERE empenhos.id = origem.empenho_id;
                RETURN NULL;
            END IF;

            RETURN NULL;
        END;
        $$ LANGUAGE plpgsql;
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('
            CREATE OR REPLACE FUNCTION atualiza_saldos() RETURNS TRIGGER AS $$
            BEGIN
                UPDATE empenhos
                SET aliquidar = origem.aliquidar,
                    liquidado = origem.liquidado,
                    pago = origem.pago,
                    empenhado = origem.empenhado,
                    rpinscrito = origem.rpinscrito,
                    rpaliquidar = origem.rpaliquidar,
                    rpliquidado = origem.rpliquidado,
                    rppago = origem.rppago,
                    updated_at = NOW()
                FROM (
                    SELECT empenho_id,
                         -- A LIQUIDAR
                         COALESCE(SUM(empaliquidar), 0) +
                         COALESCE(SUM(empemliquidacao), 0) AS aliquidar,

                         -- LIQUIDADO
                         COALESCE(SUM(emprpp), 0) +
                         COALESCE(SUM(empliquidado), 0) AS liquidado,

                         -- PAGO
                         COALESCE(SUM(emppago), 0) AS pago,

                         -- EMPENHADO
                         COALESCE(SUM(empaliquidar), 0) +
                         COALESCE(SUM(empemliquidacao), 0) +
                         COALESCE(SUM(empliquidado), 0) +
                         COALESCE(SUM(empaliqrpnp), 0) +
                         COALESCE(SUM(empemliqrpnp), 0) +
                         COALESCE(SUM(emprpp), 0) +
                         COALESCE(SUM(emppago), 0) AS empenhado,

                         -- RP A LIQUIDAR
                         COALESCE(SUM(rpnpaliquidar), 0) +
                         COALESCE(SUM(rpnpaliquidaremliquidacao), 0) AS rpaliquidar,

                         -- RP LIQUIDADO
                         COALESCE(SUM(rpnpaliquidar), 0) +
                         COALESCE(SUM(rpnpaliquidaremliquidacao), 0) +
                         COALESCE(SUM(rpnpliquidado), 0) +
                         COALESCE(SUM(rpnpaliquidarbloq), 0) +
                         COALESCE(SUM(rpnpaliquidaremliquidbloq), 0) +
                         COALESCE(SUM(rppliquidado), 0) AS rpliquidado,

                         -- RP PAGO
                         COALESCE(SUM(rpnppago), 0) +
                         COALESCE(SUM(rpppago), 0) AS rppago,

                         -- RP INSCRITO
                         COALESCE(SUM(rpnpaliquidinsc), 0) + COALESCE(SUM(rpnpemliquidinsc), 0) +
                         COALESCE(SUM(reinscrpnpaliquidbloq), 0) + COALESCE(SUM(reinscrpnpemliquid), 0) +
                         COALESCE(SUM(rpnprestab), 0) + COALESCE(SUM(rpnpaliquidtransfdeb), 0) +
                         COALESCE(SUM(rpnpaliquidemliquidtransfdeb), 0) + COALESCE(SUM(rpnpliquidapgtransfdeb), 0) +
                         COALESCE(SUM(rpnpbloqtransfdeb), 0) + COALESCE(SUM(rppinsc), 0) + COALESCE(SUM(rppexecant), 0)+
                         COALESCE(SUM(rpptrasf), 0) - COALESCE(SUM(rpnpaliquidtransfcred), 0) -
                         COALESCE(SUM(rpnpaliquidemliquidtransfcred), 0) - COALESCE(SUM(rpnpliquidapgtransfcred), 0) -
                         COALESCE(SUM(rpnpbloqtransfcred), 0) - COALESCE(SUM(rpptransffusao), 0) -
                         COALESCE(SUM(ajusterpexecant), 0) - COALESCE(SUM(rpnpcancelado), 0) -
                         COALESCE(SUM(rpnpoutrocancelamento), 0) - COALESCE(SUM(rpnpemliqoutrocancelamento), 0) -
                         COALESCE(SUM(rpppago), 0) - COALESCE(SUM(rppcancelado), 0) AS rpinscrito

                      FROM empenhodetalhado
                      WHERE empenho_id = NEW.empenho_id
                        AND deleted_at IS NULL
                      GROUP BY empenho_id) origem
                WHERE empenhos.id = origem.empenho_id;
                RETURN NULL;
            END;
            $$ LANGUAGE plpgsql;
        ');
    }
}
