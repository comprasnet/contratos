<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSistemaOrigemColumnNaturezadespesa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('naturezadespesa', function (Blueprint $table) {
            $table->string('sistema_origem')->default('SIAFI');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('naturezadespesa', 'sistema_origem')){
            Schema::table('naturezadespesa', function (Blueprint $table) {
                $table->dropColumn('sistema_origem');
            });
        }
    }
}