<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterColumnAplicavelDecreto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('contratos')->whereNull('aplicavel_decreto')->update(['aplicavel_decreto' => false]);
        DB::table('contratohistorico')->whereNull('aplicavel_decreto')->update(['aplicavel_decreto' => false]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
