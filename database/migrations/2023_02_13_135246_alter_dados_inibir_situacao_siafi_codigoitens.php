<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDadosInibirSituacaoSiafiCodigoitens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //vou criar migration para deixar invisivel os tipos de DH
        $codigo_itens = \App\Models\Codigoitem::whereHas('codigo', function ($c) {
            $c->where('descricao', 'Tipos DH SIAFI');
        })
            ->whereNotIn('descres', ['NP', 'RP'])
            ->update(['visivel' => false]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $codigo_itens = \App\Models\Codigoitem::whereHas('codigo', function ($c) {
            $c->where('descricao', 'Tipos DH SIAFI');
        })
            ->whereNotIn('descres', ['NP', 'RP'])
            ->update(['visivel' => true]);
    }
}
