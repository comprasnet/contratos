<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContratofaturaidSfpcoitem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sfpcoitem', function (Blueprint $table) {
            $table->integer('contratofatura_id')->unsigned()->nullable();
            $table->foreign('contratofatura_id')->references('id')->on('contratofaturas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sfpcoitem', function (Blueprint $table) {
            $table->dropColumn('contratofatura_id');
        });
    }
}
