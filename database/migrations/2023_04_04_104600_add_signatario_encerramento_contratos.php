<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSignatarioEncerramentoContratos extends Migration
{
   /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('contratos', 'nome_responsavel_signatario_encerramento')){
            Schema::table('contratos', function (Blueprint $table) {
                $table->string('nome_responsavel_signatario_encerramento')->nullable()->default(null);
            });
        }
        if(!Schema::hasColumn('contratos', 'cpf_responsavel_signatario_encerramento')){
            Schema::table('contratos', function (Blueprint $table) {
                $table->string('cpf_responsavel_signatario_encerramento')->nullable();
            });
        }
        if(!Schema::hasColumn('contratos', 'user_id_responsavel_signatario_encerramento')){
            Schema::table('contratos', function (Blueprint $table) {
                $table->integer('user_id_responsavel_signatario_encerramento')->nullable()->default(null);
                $table->foreign('user_id_responsavel_signatario_encerramento', 'user_sig_encerram')->references('id')->on('users');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contratos', function ($table) {

            if(Schema::hasColumn('contratos', 'nome_responsavel_signatario_encerramento')){
                $table->dropColumn('nome_responsavel_signatario_encerramento');
            }
            if(Schema::hasColumn('contratos', 'cpf_responsavel_signatario_encerramento')){
                $table->dropColumn('cpf_responsavel_signatario_encerramento');
            }
            if(Schema::hasColumn('contratos', 'user_id_responsavel_signatario_encerramento')){
                $table->dropColumn('user_id_responsavel_signatario_encerramento');
            }


        });
    }
}
