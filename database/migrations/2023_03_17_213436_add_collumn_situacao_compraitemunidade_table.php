<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCollumnSituacaoCompraitemunidadeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('compra_item_unidade', function (Blueprint $table) {
            $table->boolean('situacao')
                ->default(true)
                ->comment('situacao verdadeiro são os items ativos no momento da busca da compra no siasg');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('compra_item_unidade', function ($table) {
            $table->dropColumn('situacao');
        });
    }
}
