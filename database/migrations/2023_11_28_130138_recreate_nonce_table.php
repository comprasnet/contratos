<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RecreateNonceTable extends Migration
{
    public function up()
    {
        if (Schema::hasTable('nonce')) {
            Schema::dropIfExists('nonce');
        }

        Schema::create('nonce', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->string('nonce', 100);
            $table->nullableMorphs('nonceable');
            $table->string('ip', 100);
            $table->boolean('status')->default(true);
            $table->string('funcionalidade');
            $table->text('json_request');
            $table->text('json_response');
            $table->timestamps();
            $table->unique(['user_id', 'nonce']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nonce');
    }
}
