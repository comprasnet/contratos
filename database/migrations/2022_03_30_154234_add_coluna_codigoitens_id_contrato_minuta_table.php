<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColunaCodigoitensIdContratoMinutaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('contrato_minutas', function (Blueprint $table) {
            $table->unsignedInteger('codigoitens_id')->nullable();      
            $table->foreign('codigoitens_id')->references('id')->on('codigoitens');           
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
