<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterContratoParametrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contrato_parametros', function (Blueprint $table) {
            $table->renameColumn('from_id', 'contrato_id');
            $table->dropColumn('from_type');
            $table->string('tipo_recebimento_provisorio')->nullable();
            $table->string('tipo_recebimento_definitivo')->nullable();
            $table->string('tipo_pagamento')->nullable();
            $table->string('tipo_pagamento_recorrente')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contrato_parametros', function (Blueprint $table) {
            $table->renameColumn('contrato_id', 'from_id');
            $table->string('from_type')->nullable();
            $table->dropColumn('tipo_recebimento_provisorio');
            $table->dropColumn('tipo_recebimento_definitivo');
            $table->dropColumn('tipo_pagamento');
            $table->dropColumn('tipo_pagamento_recorrente');
        });
    }
}
