<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColunaDocFormatadoTbContratoMinutas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contrato_minutas', function (Blueprint $table) {
            $table->string('documento_formatado', 30)->nullable();  //DocumentoFormatado - RetornoInclusaoDocumento - Número do documento visível para o usuário
            $table->unsignedInteger('qtd_min_assinaturas')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contrato_minutas', function (Blueprint $table) {
            $table->dropColumn('documento_formatado');
            $table->dropColumn('qtd_min_assinaturas');
        });
    }
}
