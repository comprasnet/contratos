<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsOnOrgaoConfiguracaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('orgaoconfiguracao', 'sistema_financeiro_externo')){
            Schema::table('orgaoconfiguracao', function (Blueprint $table) {
                $table->string('sistema_financeiro_externo')->nullable();
            });
        }

        if(!Schema::hasColumn('orgaoconfiguracao', 'codigo_sistema_externo_obrigatorio')){
            Schema::table('orgaoconfiguracao', function (Blueprint $table) {
                $table->boolean('codigo_sistema_externo_obrigatorio')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orgaoconfiguracao', function (Blueprint $table) {
            $table->dropColumn('sistema_financeiro_externo');
            $table->dropColumn('codigo_sistema_externo_obrigatorio');
        });
    }
}
