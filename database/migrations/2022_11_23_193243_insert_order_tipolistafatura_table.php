<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertOrderTipolistafaturaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('public.tipolistafatura')
            ->where('nome', '=', 'PEQUENOS CREDORES (Inciso II, 24, 8.666 e paragrafo 1º)')
            ->update(['order' => 1]);

        DB::table('public.tipolistafatura')
            ->where('nome', '=', 'FORNECIMENTO DE BENS')
            ->update(['order' => 2]);

        DB::table('public.tipolistafatura')
            ->where('nome', '=', 'LOCAÇÕES')
            ->update(['order' => 3]);

        DB::table('public.tipolistafatura')
            ->where('nome', '=', 'PRESTAÇÃO DE SERVIÇOS')
            ->update(['order' => 4]);

        DB::table('public.tipolistafatura')
            ->where('nome', '=', 'REALIZAÇÃO DE OBRAS')
            ->update(['order' => 5]);

        DB::table('public.tipolistafatura')
            ->where('nome', '=', 'VINCULAÇÕES ESPECÍFICAS')
            ->update(['order' => 6]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('public.tipolistafatura')
            ->where('nome', '=', 'PEQUENOS CREDORES (Inciso II, 24, 8.666 e paragrafo 1º)')
            ->update(['order' => null]);

        DB::table('public.tipolistafatura')
            ->where('nome', '=', 'FORNECIMENTO DE BENS')
            ->update(['order' => null]);

        DB::table('public.tipolistafatura')
            ->where('nome', '=', 'LOCAÇÕES')
            ->update(['order' => null]);

        DB::table('public.tipolistafatura')
            ->where('nome', '=', 'PRESTAÇÃO DE SERVIÇOS')
            ->update(['order' => null]);

        DB::table('public.tipolistafatura')
            ->where('nome', '=', 'REALIZAÇÃO DE OBRAS')
            ->update(['order' => null]);

        DB::table('public.tipolistafatura')
            ->where('nome', '=', 'VINCULAÇÕES ESPECÍFICAS')
            ->update(['order' => null]);
    }
}
