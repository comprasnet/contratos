<?php

use App\Models\Codigo;
use App\Models\Codigoitem;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertTermoCodigoItens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = Codigo::create([
            'descricao' => 'Termo de Aceite',
            'visivel' => true
        ]);

        Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'ACESSO',
            'descricao' => 'Acesso Sistema'
        ]);

        Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'APROPRIACAO',
            'descricao' => 'Apropriação'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Codigo::where(
            'descricao', 'Termo de Aceite'
        )->forceDelete();
    }
}
