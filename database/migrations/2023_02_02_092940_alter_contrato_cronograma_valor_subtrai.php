<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Contratocronograma;

class AlterContratoCronogramaValorSubtrai extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // atributo para verificação se o valor foi alterado
        if(!Schema::hasColumn('contratocronograma', 'is_valor_alterado_para_negativo')){
            Schema::table('contratocronograma', function (Blueprint $table) {
                $table->boolean('is_valor_alterado_para_negativo')->default(false)->nullable();
            });
        }
        // atributo para guardar o valor antigo
        if(!Schema::hasColumn('contratocronograma', 'valor_anterior_alteracao_negativo')){
            Schema::table('contratocronograma', function (Blueprint $table) {
                $table->decimal('valor_anterior_alteracao_negativo',15,2)->nullable();
            });
        }

        \Log::info('Preparando para buscar os cronogramas com soma_subtrai false...');

        $arrayContratoCronograma = Contratocronograma::where('soma_subtrai', false)
            ->where('valor', '>', 0)
            ->get();
        $quantidade = count($arrayContratoCronograma);

        \Log::info( $quantidade . ' cronogramas buscados.');

        foreach( $arrayContratoCronograma as $objContratoCronograma ){

            if( !$objContratoCronograma->soma_subtrai && $objContratoCronograma->valor >= 0 ){

                \Log::info('-------------------');
                \Log::info('Varrendo o cronograma id = ' . $objContratoCronograma->id);

                $valor = $objContratoCronograma->valor;

                \Log::info('Valor = ' . $valor);

                $novoValorNegativo = '-'.$valor;

                $objContratoCronograma->is_valor_alterado_para_negativo = true;
                $objContratoCronograma->valor_anterior_alteracao_negativo = $valor;
                $objContratoCronograma->valor = $novoValorNegativo;
                $objContratoCronograma->save();

                \Log::info('Novo valor salvo! => ' . $novoValorNegativo);

            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Log::info('***************************');
        \Log::info('Rollback iniciado!!!');
        \Log::info('***************************');


        if(Schema::hasColumn('contratocronograma', 'is_valor_alterado_para_negativo')){
            // buscar cronogramas com is_valor_alterado_para_negativo = true
            $arrayContratoCronograma = Contratocronograma::where('is_valor_alterado_para_negativo', true)->get();

            foreach( $arrayContratoCronograma as $objContratoCronograma ){
                \Log::info('-----------------------------------------');
                \Log::info('Retornando valor do contrato id = ' . $objContratoCronograma->id);
                $objContratoCronograma->valor = $objContratoCronograma->valor_anterior_alteracao_negativo;
                $objContratoCronograma->save();
                \Log::info('ok!');
            }
        }

        \Log::info('***************************');
        \Log::info('Rollback realizado!!!');
        \Log::info('***************************');
    }
}
