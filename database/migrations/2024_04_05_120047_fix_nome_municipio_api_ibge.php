<?php

use App\Models\Municipio;
use GuzzleHttp\Client;
use Illuminate\Database\Migrations\Migration;
use \Illuminate\Support\Facades\Log;
use \Illuminate\Support\Facades\DB;

class FixNomeMunicipioApiIbge extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            // Instancia o cliente Guzzle
            $client = new Client();

            $municipios = Municipio::all();
            DB::beginTransaction();

            foreach ($municipios as $municipio) {
                $url = "https://servicodados.ibge.gov.br/api/v1/localidades/municipios/{$municipio->codigo_ibge}";
                // Faz a requisição GET para a API do IBGE
                $response = $client->request('GET', $url);

                // Obtém o corpo da resposta
                $body = $response->getBody();

                // Decodifica o JSON para um array associativo
                $municipioResponse = json_decode($body, true);

                if (!isset($municipioResponse['nome'])) {
                    continue;
                }

                $municipioAPI = trim($municipioResponse['nome']);
                $municipioAPI = rtrim($municipioAPI);

                if ($municipioAPI != $municipio->nome) {
                    $nomeAntigo = $municipio->nome;
                    $municipio->nome =$municipioAPI;

                    Log::debug($municipio);

                    $municipio->save();

                    $mensagem = "O nome do código do IBGE {$municipio->codigo_ibge} foi alterado com sucesso!
                    Antigo: {$nomeAntigo} Novo: {$municipioAPI}";

                    Log::debug($mensagem);

                }
            }

            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            Log::channel('erro_migration')->debug("Erro ao executar a migration FixNomeMunicipioApiIbge");
            Log::channel('erro_migration')->debug($exception);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
