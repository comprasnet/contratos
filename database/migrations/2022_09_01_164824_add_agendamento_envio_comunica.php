<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAgendamentoEnvioComunica extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comunica', function($table)
        {
            $table->datetime('agendamento')->nullable();
            $table->datetime('hora_envio')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comunica', function($table)
        {
            $table->dropColumn('agendamento');
            $table->dropColumn('hora_envio');
        });
    }
}
