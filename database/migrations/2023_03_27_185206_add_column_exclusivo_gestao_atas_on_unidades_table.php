<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Unidade;

class AddColumnExclusivoGestaoAtasOnUnidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('unidades', 'exclusivo_gestao_atas')){
            Schema::table('unidades', function (Blueprint $table) {
                $table->boolean('exclusivo_gestao_atas')->nullable();
            });
        }

        // unidades estaduais e municipais (Estadual/Municipal) = true
        Unidade::whereIn('esfera', ['Estadual', 'Municipal'])
                ->update(['exclusivo_gestao_atas' => true]);

        // unidades de esfera federal (Federal) = false
        Unidade::where('esfera', 'Federal')
                ->update(['exclusivo_gestao_atas' => false]);

        // orgãos da PMSP. -Órgãos da PMSP: 95420 e 95430 = false
        $unidades = Unidade::select('unidades.id')
            ->join('orgaos', 'orgaos.id', '=', 'unidades.orgao_id')
            ->whereIn('orgaos.codigo', ['95420','95430'])
            ->get()->toArray();
        foreach ($unidades as $unidade) {
            Unidade::where('id', $unidade['id'])
            ->update(['exclusivo_gestao_atas' => false]);
        }

        // caso o campo esfera esteja null = false
        Unidade::whereNull('esfera')
                ->update(['exclusivo_gestao_atas' => false]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('unidades', function (Blueprint $table) {
            $table->dropColumn('exclusivo_gestao_atas');
        });
    }
}
