<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContratoParametrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contrato_parametros', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pz_recebimento_provisorio')->nullable();
            $table->integer('pz_recebimento_definitivo')->nullable();
            $table->integer('pz_pagamento')->nullable();
            $table->integer('data_pagamento_recorrente')->nullable();
            $table->boolean('registro')->nullable();
            $table->string('from_type')->nullable();
            $table->integer('from_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contrato_parametros');
    }
}
