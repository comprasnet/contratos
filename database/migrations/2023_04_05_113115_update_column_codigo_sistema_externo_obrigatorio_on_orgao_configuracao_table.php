<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Orgaoconfiguracao;

class UpdateColumnCodigoSistemaExternoObrigatorioOnOrgaoConfiguracaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Orgaoconfiguracao::query()->update(['codigo_sistema_externo_obrigatorio' => false]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
