<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Codigoitem;
use App\Models\Execsfsituacao;

class UpdateDadosExecsfsituacaoStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Execsfsituacao::where('codigo','DSP001')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP101')->update(['status'=>false]);
        Execsfsituacao::where('codigo','BPV001')->update(['status'=>false]);
        Execsfsituacao::where('codigo','BPV004')->update(['status'=>false]);
        Execsfsituacao::where('codigo','BPV010')->update(['status'=>false]);
        Execsfsituacao::where('codigo','BPV013')->update(['status'=>false]);
        Execsfsituacao::where('codigo','BPV015')->update(['status'=>false]);
        Execsfsituacao::where('codigo','BPV017')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DFL005')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DFL013')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DFL023')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DFL038')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DFL045')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DFL049')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSF001')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSF002')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSF003')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSF004')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSF006')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSF009')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSF010')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSF011')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP001')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP002')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP003')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP005')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP006')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP007')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP008')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP009')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP010')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP015')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP017')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP019')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP021')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP024')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP051')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP052')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP058')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP061')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP062')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP067')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP069')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP072')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP081')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP085')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP101')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP102')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP105')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP107')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP109')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP112')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP113')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP115')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP120')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP122')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP123')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP126')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP127')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP137')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP201')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP202')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP205')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP206')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP207')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP209')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP210')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP211')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP212')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP214')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP215')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP216')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP226')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP230')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP231')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP233')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP235')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP901')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP902')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP906')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP909')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP910')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP925')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP953')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP972')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP973')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP974')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP975')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP986')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DSP988')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DVL001')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DVL003')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DVL005')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DVL017')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DVL051')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DVL061')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DVL081')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DVL101')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DVL102')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DVL115')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DVL201')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DVL205')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DVL216')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DVL313')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DVL345')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DVL457')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DVL901')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DVL902')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DVL973')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DVL975')->update(['status'=>false]);
        Execsfsituacao::where('codigo','DVL986')->update(['status'=>false]);
        Execsfsituacao::where('codigo','EST001')->update(['status'=>false]);
        Execsfsituacao::where('codigo','EST003')->update(['status'=>false]);
        Execsfsituacao::where('codigo','EST004')->update(['status'=>false]);
        Execsfsituacao::where('codigo','EST014')->update(['status'=>false]);
        Execsfsituacao::where('codigo','EST022')->update(['status'=>false]);
        Execsfsituacao::where('codigo','EST030')->update(['status'=>false]);
        Execsfsituacao::where('codigo','EST031')->update(['status'=>false]);
        Execsfsituacao::where('codigo','PPV001')->update(['status'=>false]);
        Execsfsituacao::where('codigo','PPV018')->update(['status'=>false]);
        Execsfsituacao::where('codigo','PPV100')->update(['status'=>false]);
        Execsfsituacao::where('codigo','SPF002')->update(['status'=>false]);
        Execsfsituacao::where('codigo','SPF003')->update(['status'=>false]);
        Execsfsituacao::where('codigo','SPF004')->update(['status'=>false]);
        Execsfsituacao::where('codigo','SPF005')->update(['status'=>false]);
        Execsfsituacao::where('codigo','SPF006')->update(['status'=>false]);
        Execsfsituacao::where('codigo','SPF023')->update(['status'=>false]);
        Execsfsituacao::where('codigo','TRF017')->update(['status'=>false]);
        Execsfsituacao::where('codigo','TRF020')->update(['status'=>false]);
        Execsfsituacao::where('codigo','TRF057')->update(['status'=>false]);


        Execsfsituacao::where('codigo','DSP001')->update(['status'=>true, 'ordenacao' => 1]);
        Execsfsituacao::where('codigo','DSP002')->update(['status'=>true, 'ordenacao' => 2]);
        Execsfsituacao::where('codigo','DSP003')->update(['status'=>true, 'ordenacao' => 3]);
        Execsfsituacao::where('codigo','DSP005')->update(['status'=>true, 'ordenacao' => 4]);
        Execsfsituacao::where('codigo','DSP008')->update(['status'=>true, 'ordenacao' => 5]);
        Execsfsituacao::where('codigo','DSP010')->update(['status'=>true, 'ordenacao' => 6]);
        Execsfsituacao::where('codigo','DSP051')->update(['status'=>true, 'ordenacao' => 7]);
        Execsfsituacao::where('codigo','DSP052')->update(['status'=>true, 'ordenacao' => 8]);
        Execsfsituacao::where('codigo','DSP061')->update(['status'=>true, 'ordenacao' => 9]);
        Execsfsituacao::where('codigo','DSP062')->update(['status'=>true, 'ordenacao' => 10]);
        Execsfsituacao::where('codigo','DSP101')->update(['status'=>true, 'ordenacao' => 11]);
        Execsfsituacao::where('codigo','DSP102')->update(['status'=>true, 'ordenacao' => 12]);
        Execsfsituacao::where('codigo','DSP107')->update(['status'=>true, 'ordenacao' => 13]);
        Execsfsituacao::where('codigo','DSP113')->update(['status'=>true, 'ordenacao' => 14]);
        Execsfsituacao::where('codigo','DSP115')->update(['status'=>true, 'ordenacao' => 15]);
        Execsfsituacao::where('codigo','DSP201')->update(['status'=>true, 'ordenacao' => 16]);
        Execsfsituacao::where('codigo','DSP205')->update(['status'=>true, 'ordenacao' => 17]);
        Execsfsituacao::where('codigo','DSP214')->update(['status'=>true, 'ordenacao' => 18]);
        Execsfsituacao::where('codigo','DSP216')->update(['status'=>true, 'ordenacao' => 19]);
        Execsfsituacao::where('codigo','DSP233')->update(['status'=>true, 'ordenacao' => 20]);
        Execsfsituacao::where('codigo','DSP235')->update(['status'=>true, 'ordenacao' => 21]);
        Execsfsituacao::where('codigo','DSP973')->update(['status'=>true, 'ordenacao' => 22]);
        Execsfsituacao::where('codigo','DSP975')->update(['status'=>true, 'ordenacao' => 23]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
