<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSituacaoIdCodigoitensContrato extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('contratos', 'justificativa_contrato_inativo_id')){
            Schema::table('contratos', function (Blueprint $table) {
                $table->integer('justificativa_contrato_inativo_id')->nullable();
                $table->foreign('justificativa_contrato_inativo_id', 'justificativa')->references('id')->on('codigoitens')->nullable()->default(null);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contratos', function (Blueprint $table) {
            $table->dropColumn(['justificativa_contrato_inativo_id']);
        });


    }
}
