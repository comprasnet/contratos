<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class InsertRoleApoioAdministrativoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        app()['cache']->forget('spatie.permission.cache');

        Permission::create(['name' => 'apoio_adm_responsavel_inserir']);
        Permission::create(['name' => 'apoio_adm_responsavel_editar']);
        Permission::create(['name' => 'apoio_adm_responsavel_deletar']);
        Permission::create(['name' => 'apoio_adm_responsavel_consultar']);

        if (!Role::where('name', 'Apoio Administrativo')->first()) {
            $role = Role::create(['name' => 'Apoio Administrativo']);
            $role->givePermissionTo('apoio_adm_responsavel_inserir');
            $role->givePermissionTo('apoio_adm_responsavel_editar');
            $role->givePermissionTo('apoio_adm_responsavel_deletar');
            $role->givePermissionTo('apoio_adm_responsavel_consultar');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $role = Role::where(['name' => 'Apoio Administrativo'])->first();
        $role->revokePermissionTo('apoio_adm_responsavel_inserir');
        $role->revokePermissionTo('apoio_adm_responsavel_editar');
        $role->revokePermissionTo('apoio_adm_responsavel_deletar');
        $role->revokePermissionTo('apoio_adm_responsavel_consultar');
        $role->forceDelete();

        Permission::where(['name' => 'apoio_adm_responsavel_inserir'])->forceDelete();
        Permission::where(['name' => 'apoio_adm_responsavel_editar'])->forceDelete();
        Permission::where(['name' => 'apoio_adm_responsavel_deletar'])->forceDelete();
        Permission::where(['name' => 'apoio_adm_responsavel_consultar'])->forceDelete();
    }
}
