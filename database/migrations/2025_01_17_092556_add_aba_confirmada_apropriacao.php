<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAbaConfirmadaApropriacao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('sfdadosbasicos', 'confirma_dados')) {
            Schema::table('sfdadosbasicos', function (Blueprint $table) {
                $table->boolean('confirma_dados')->default(false)->comment('Utilizado para controlar a aba de cada apropriação.');
            });
        }

        if (!Schema::hasColumn('sfdadospgto', 'confirma_dados')) {
            Schema::table('sfdadospgto', function (Blueprint $table) {
                $table->boolean('confirma_dados')->default(false)->comment('Utilizado para controlar a aba de cada apropriação.');;
            });
        }

        if (!Schema::hasColumn('sfcentrocusto', 'confirma_dados')) {
            Schema::table('sfcentrocusto', function (Blueprint $table) {
                $table->boolean('confirma_dados')->default(false)->comment('Utilizado para controlar a aba de cada apropriação.');;
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('sfdadosbasicos', 'confirma_dados')) {
            Schema::table('sfdadosbasicos', function (Blueprint $table) {
                $table->dropColumn('confirma_dados');
            });
        }

        if (Schema::hasColumn('sfdadospgto', 'confirma_dados')) {
            Schema::table('sfdadospgto', function (Blueprint $table) {
                $table->dropColumn('confirma_dados');
            });
        }

        if (Schema::hasColumn('sfcentrocusto', 'confirma_dados')) {
            Schema::table('sfcentrocusto', function (Blueprint $table) {
                $table->dropColumn('confirma_dados');
            });
        }
    }
}
