<?php

use App\Models\DomicilioBancario;
use App\Models\OrdemBancaria;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDomiciliobancarioOrdensbancarias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ordens_bancarias', function (Blueprint $table) {
            $table->integer('domicilio_bancario_id')->unsigned()->nullable();
            $table->foreign('domicilio_bancario_id')->references('id')->on('domicilio_bancarios');
        });

        $ordem_bancaria_contas = OrdemBancaria::all();

        foreach ($ordem_bancaria_contas as $conta) {

            $domicioBancario = DomicilioBancario::where('banco', $conta->bancodestino)
                ->where('conta', $conta->contadestino)
                ->where('agencia', $conta->agenciadestino)
                ->first();

            if (!$domicioBancario) {
                $domicioBancario = DomicilioBancario::create([
                    'banco' => $conta->bancodestino,
                    'conta' => $conta->contadestino,
                    'agencia' => $conta->agenciadestino
                ]);
                $domicioBancario->save();
            }

            $conta->update(['domicilio_bancario_id' => $domicioBancario->id]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ordens_bancarias', function (Blueprint $table) {
            $table->dropColumn('domicilio_bancario_id');
        });
    }
}
