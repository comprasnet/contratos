<?php

use App\Models\Codigo;
use App\Models\Codigoitem;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCodigoitensTipoImportacaoAutoridadeSignataria extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = Codigo::where('descricao', 'Tipo Importação')->first();

        Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'AUTSIG',
            'descricao' => 'Autoridade Signatária',
            'visivel' => false
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Codigoitens::where([
            'descricao' => 'Autoridade Signatária',
            'visivel' => false,
            'descres' => 'AUTSIG'
        ])->forceDelete();
    }
}
