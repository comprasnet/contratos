<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMeiPorteFornecedorsicaf extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fornecedorsicaf', function (Blueprint $table) {
            $table->string('nome_fantasia', 150)->nullable()->comment('Nome fantasia da empresa');
            $table->integer('natureza_juridica')->unsigned()->nullable()->comment('Código da natureza juridica.');
            $table->boolean('mei')->nullable()->comment('Verifica se a empresa é do tipo mei');
            $table->integer('porte')->unsigned()->nullable()->comment('Verifica o porte da empresa');
            $table->integer('origem_sistema')->unsigned()->nullable()->comment('Situação do fornecedor 1 - SICAF, 2 - RFB, 3 - N/D');
            $table->date('data_vencimento_credenciamento')->unsigned()->nullable()->comment('Data vencimento do cadastro');
            $table->tinyInteger('situacao_fornecedor')->unsigned()->nullable()->comment('Situação do fornecedor 1 - ativo, 2 - inativo');


            $table->foreign('porte')->references('id')->on('codigoitens');
            $table->foreign('natureza_juridica')->references('id')->on('codigoitens');
            $table->foreign('origem_sistema')->references('id')->on('codigoitens');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fornecedorsicaf', function (Blueprint $table) {
            $table->dropColumn('mei');
            $table->dropColumn('porte');
            $table->dropColumn('natureza_juridica');
            $table->dropColumn('situacao_fornecedor');
            $table->dropColumn('data_vencimento_credenciamento');
            $table->dropColumn('nome_fantasia');
            $table->dropColumn('origem_sistema');
        });
    }
}
