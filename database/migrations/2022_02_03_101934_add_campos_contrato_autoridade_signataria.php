<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCamposContratoAutoridadeSignataria extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contrato_autoridade_signataria', function (Blueprint $table) {
            $table->string('nome_autoridade_signataria')->nullable();
            $table->string('cargo_autoridade_signataria')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contrato_autoridade_signataria', function (Blueprint $table) {
            $table->dropColumn(['nome_autoridade_signataria']);
            $table->dropColumn(['cargo_autoridade_signataria']);
        });
    }
}
