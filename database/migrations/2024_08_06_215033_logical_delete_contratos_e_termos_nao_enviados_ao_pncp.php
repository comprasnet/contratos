<?php

use App\Models\EnviaDadosPncp;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LogicalDeleteContratosETermosNaoEnviadosAoPncp extends Migration
{
    private $tiposNaoEnviados = [
        'Acordo de Cooperação Técnica (ACT)',
        'Convênio',
        'Credenciamento',
        'Erro de Execução',
        'Executado',
        'Pendente de Execução',
        'Termo de Compromisso',
        'Termo de Encerramento',
        'Termo de Execução Descentralizada (TED)'
    ];

    public function up(): void
    {
        EnviaDadosPncp::whereNull('deleted_at')
            ->whereIn('tipo_contrato', $this->tiposNaoEnviados)
            ->update(['deleted_at' => now()]);
    }

    public function down(): void
    {
        //
    }
}
