<?php

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertPermissionActivitylogConsultar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        app()['cache']->forget('spatie.permission.cache');

        $permission = Permission::where('name','activitylog_consultar')->first();

        if(!isset($permission->id)){
            Permission::create([
                'name' => 'activitylog_consultar',
                'guard_name' => 'web'
            ]);
            $role = Role::where('name', 'Administrador')->first();
            $role->givePermissionTo('activitylog_consultar');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $role = Role::where(['name' => 'Administrador'])->first();
        $role->revokePermissionTo('activitylog_consultar');

        Permission::where(['name' => 'activitylog_consultar'])->forceDelete();
    }
}
