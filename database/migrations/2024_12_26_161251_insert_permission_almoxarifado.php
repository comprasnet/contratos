<?php

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertPermissionAlmoxarifado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        app()['cache']->forget('spatie.permission.cache');

        $permission = Permission::where(['name' => 'acesso_contratos_tipo_empenho'])->first();
        if (!$permission) {
            Permission::create(['name' => 'acesso_contratos_tipo_empenho']);
        }

        $role = Role::where(['name' => 'Almoxarifado'])->first();
        if (!$role) {
            $role = Role::create(['name' => 'Almoxarifado']);
        }

        $role->givePermissionTo('acesso_contratos_tipo_empenho');
        $role->givePermissionTo('meucontratoarquivo_inserir');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $role = Role::where(['name' => 'Almoxarifado'])->first();
        $role->revokePermissionTo('acesso_contratos_tipo_empenho');
        $role->revokePermissionTo('meucontratoarquivo_inserir');
        
        $role->forceDelete();
    }
}
