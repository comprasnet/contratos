<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterUpdatedAtNowEmpenhosZeradosEmpenhodetalhado extends Migration
{
    public function up()
    {
        DB::update("
            UPDATE empenhodetalhado
            SET updated_at = NOW()
            WHERE id IN (
                SELECT ed.id
                FROM empenhos e
                JOIN empenhodetalhado ed ON e.id = ed.empenho_id
                WHERE (
                    (ed.empaliquidar <> 0 OR ed.empemliquidacao <> 0)
                    AND e.aliquidar = 0
                )
                OR (
                    (ed.emprpp <> 0 OR ed.empliquidado <> 0)
                    AND e.liquidado = 0
                )
                OR (ed.emppago <> 0 AND e.pago = 0)
                OR (
                    (
                        ed.empaliquidar <> 0 OR ed.empemliquidacao <> 0 OR
                        ed.empliquidado <> 0 OR ed.empaliqrpnp <> 0 OR
                        ed.empemliqrpnp <> 0 OR ed.emprpp <> 0 OR ed.emppago <> 0
                    )
                    AND e.empenhado = 0
                )
                OR (
                    (
                        ed.rpnpaliquidar <> 0 OR ed.rpnpaliquidaremliquidacao <> 0 OR
                        ed.rpnpliquidado <> 0 OR
                        ed.rpnpaliquidarbloq <> 0 OR ed.rpnpaliquidaremliquidbloq <> 0 OR
                        ed.rppliquidado <> 0
                    )
                    AND e.rpliquidado = 0
                )
                OR (
                    (ed.rpnppago <> 0 OR ed.rpppago <> 0)
                    AND e.rppago = 0
                )
                OR (
                    (
                        coalesce(ed.rpnpaliquidinsc, 0) + coalesce(ed.rpnpemliquidinsc, 0) +
                        coalesce(ed.reinscrpnpaliquidbloq, 0) + coalesce(ed.reinscrpnpemliquid, 0) +
                        coalesce(ed.rpnprestab, 0) + coalesce(ed.rpnpaliquidtransfdeb, 0) +
                        coalesce(ed.rpnpaliquidemliquidtransfdeb, 0) + coalesce(ed.rpnpliquidapgtransfdeb, 0) +
                        coalesce(ed.rpnpbloqtransfdeb, 0) + coalesce(ed.rppinsc, 0) + coalesce(ed.rppexecant, 0) +
                        coalesce(ed.rpptrasf, 0) - coalesce(ed.rpnpaliquidtransfcred, 0) -
                        coalesce(ed.rpnpaliquidemliquidtransfcred, 0) - coalesce(ed.rpnpliquidapgtransfcred, 0) -
                        coalesce(ed.rpnpbloqtransfcred, 0) - coalesce(ed.rpptransffusao, 0) -
                        coalesce(ed.ajusterpexecant, 0) - coalesce(ed.rpnpcancelado, 0) -
                        coalesce(ed.rpnpoutrocancelamento, 0) - coalesce(ed.rpnpemliqoutrocancelamento, 0) -
                        coalesce(ed.rpppago, 0) - coalesce(ed.rppcancelado, 0)
                    ) > 0
                    AND e.rpinscrito = 0
                )
                AND e.deleted_at IS NOT NULL
                AND ed.deleted_at IS NOT NULL
            );
        ");
    }



    public function down(): void
    {
        Schema::table('empenhodetalhado', function (Blueprint $table) {
            //
        });
    }
}
