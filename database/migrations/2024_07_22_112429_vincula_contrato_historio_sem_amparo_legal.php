<?php

use App\DTO\Termo14133PNCDTO;
use App\Http\Controllers\Api\PNCP\ContratoControllerPNCP;
use App\Http\Controllers\Api\PNCP\DocumentoContratoController;
use App\Http\Controllers\Api\PNCP\DocumentoTermoContratoController;
use App\Http\Controllers\Api\PNCP\UsuarioController;
use App\Http\Controllers\Empenho\CompraSiasgCrudController;
use App\Http\Controllers\Publicacao\DiarioOficialClass;
use App\Models\AmparoLegal;
use App\Models\CalendarEvent;
use App\Models\Codigoitem;
use App\Models\Contrato;
use App\Models\Contratohistorico;
use App\Models\ContratoPublicacoes;
use App\Models\EnviaDadosPncp;
use App\Repositories\Termos14133PncpRepository;
use App\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\EnviaPncpTrait;
use App\Http\Traits\Formatador;

class VinculaContratoHistorioSemAmparoLegal extends Migration
{
    use Formatador;
    use BuscaCodigoItens;
    use EnviaPncpTrait;

    protected $tiposNegadosPNCP = ['Credenciamento', 'Outros'];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $results = \App\Models\Contratohistorico::leftJoin('amparo_legal_contratohistorico as alc', 'contratohistorico.id', '=', 'alc.contratohistorico_id')
            ->whereNull('alc.amparo_legal_id')
            ->whereIn('contratohistorico.tipo_id', function ($query) {
                $query->select('id')
                    ->from('codigoitens')
                    ->whereExists(function ($subquery) {
                        $subquery->select(DB::raw(1))
                            ->from('codigos')
                            ->whereColumn('codigoitens.codigo_id', 'codigos.id')
                            ->where('descricao', 'Tipo de Contrato')
                            ->whereNull('codigos.deleted_at');
                    })
                    ->whereNotIn('descricao', ['Termo Aditivo', 'Termo de Apostilamento', 'Termo de Rescisão', 'Termo de Encerramento'])
                    ->whereNull('codigoitens.deleted_at')
                    ->orderBy('descricao', 'asc');
            })
            ->where('contratohistorico.created_at', '>=', '2024-07-15 00:00:00')
            ->select('*')
            ->get();

        foreach ($results as $result) {
            try {
                $this->vincular($result);
            } catch (Exception $e) {
                Log::info("Erro ao vincular historico com id $result->id " . $e->getMessage());
            }
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

    public function vincular(\App\Models\Contratohistorico $contratohistorico)
    {
        $podeEnviarPNCP = null;
        $contratohistorico->vincularAmparoLegalContratoHistorico(null);

        $dataPublicacao = $contratohistorico->data_publicacao;
        $dataAtual = date('Y-m-d');

        $historico = Contratohistorico::where('contrato_id', $contratohistorico->contrato_id)
            ->where('contratohistorico.elaboracao', false)
            ->orderBy('data_assinatura', 'ASC')
            ->get();

        // Caso o usuario selecione para incluir o cronograma de forma automatica
        if ($contratohistorico->cronograma_automatico) {
            $this->inserirCronogramaFromHistorico($contratohistorico);
        }

        self::atualizaContrato($historico, $contratohistorico);
        self::createEventCalendar($contratohistorico);
        $tipoOutros = $this->retornaIdCodigoItem('Tipo de Contrato', 'Outros');

        $contratohistoricoBase = self::getInstrumentoInicial($contratohistorico->contrato_id);
        $amparoLegalId = $contratohistoricoBase->amparolegal->pluck('id')->toArray();
        $isTermoContrato = null; //self::isTermoContrato($contratohistorico);

        if (!$isTermoContrato) {
            foreach ($contratohistorico->amparolegal as $amparoLegal) {
                if ($amparoLegal->getAmparoLegalEnviarPncp() === "Sim") {
                    $podeEnviarPNCP =
                        $this->podeEnviarContratoPNCP($amparoLegalId, $contratohistorico, $contratohistoricoBase);
                    break;
                }
            }

        } else {
            $podeEnviarPNCP = $this->podeEnviarContratoPNCP($amparoLegalId, $contratohistorico, $contratohistoricoBase);
        }

        if ($podeEnviarPNCP) {

            $this->preparaEnvioPNCP(
                $contratohistorico->id,
                $contratohistorico->contrato_id,
                'Contrato',
                false
            );

            $this->enviarSuperSEIArquivoPNCP($contratohistorico);

            try {

                $uc = new UsuarioController();

                $this->realizarLoginPNCP($uc);

                $enviaDadosPNCP = EnviaDadosPncp::where('pncpable_type', ContratoHistorico::class)
                    ->where('pncpable_id', $contratohistorico->id)->oldest()->first();

                $isTermoContrato = null;//$this->isTermoContrato($contratohistorico);

                if (!$isTermoContrato) {

                    $cc = new ContratoControllerPNCP();
                    $cscc = new CompraSiasgCrudController();
                    $dcc = new DocumentoContratoController();
                    $dtcc = new DocumentoTermoContratoController();
                    $this->inserirContratoPNCP($contratohistorico, $enviaDadosPNCP, $cc, $dcc, $dtcc, $cscc);
                }

                if ($isTermoContrato) {
                    (new Termos14133PncpRepository(
                        new Termo14133PNCDTO(
                            $contratohistorico->id,
                            $contratohistorico->tipo_id,
                            $contratohistorico->contrato_id
                        )
                    ))->enviarTermo14133Pncp();
                }

            } catch (Exception $ex) {
                $enviaDadosPNCP->retorno_pncp = $ex->getMessage();
                $enviaDadosPNCP->save();
            }

        }

        if ($contratohistorico->tipo_id != $tipoOutros) {

            if ($contratohistorico->publicado) {
                $this->executaAtualizacaoViaJob($contratohistorico);
                return true;
            }

            if ($contratohistorico->publicado != true) {
                // Quando a Data de Publicação for anterior à data atual, não deve ser gerada Minuta de Publicação
                if (strtotime($dataAtual) < strtotime($dataPublicacao)) {
                    // aqui quer dizer que a data de publicação é posterior à data atual - podemos gerar a minuta de publicação
                    if (strpos($contratohistorico->numero, '/') !== false) {
                        $partes = explode("/", $contratohistorico->numero);
                        $numero = $partes[0];
                    } else {
                        $numero = $contratohistorico->numero;
                    }
                    $unidade_id = $contratohistorico->unidade_id;

                    // Pegando o usuário que criou o contrato para criar a publicação
                    $log = \App\Models\Activitylog::where('properties', 'ILIKE', '%"numero":"' . $numero . '%')
                        ->where('properties', 'ILIKE', '%"unidade_id":' . $unidade_id . '%')
                        ->where(function ($query) {
                            $query->where('subject_type', 'App\Models\Contratohistorico')
                                ->orWhere('subject_type', 'App\Models\Contrato');
                        })
                        ->first();

                    if ($log) {
                        $user = \App\Models\BackpackUser::find($log->causer_id);

                        $this->criaNovaPublicacao($contratohistorico, $this->removeMascaraCPF($user->cpf), true);
                    }
                }
            }
        }
    }

    public static function atualizaContrato($contratohistorico, $contratohistoricoatual = null)
    {
        foreach ($contratohistorico as $h) {
            $contrato_id = $h->contrato_id;
            $arrayhistorico = $h->toArray();

            $tipo = Codigoitem::find($arrayhistorico['tipo_id']);

            if ($tipo instanceof Codigoitem) {
                $array = self::retornaArrayContratoHistorico($tipo, $arrayhistorico, $contrato_id, $contratohistoricoatual);

                $contrato = new Contrato();
                $contrato->atualizaContratoFromHistorico($contrato_id, $array);
            }
        }
    }

    public static function retornaArrayContratoHistorico(Codigoitem $tipo, array $arrayhistorico, $contrato_id, $contratohistoricoatual = null)
    {
        switch ($tipo->descricao) {
            case 'Termo de Encerramento':
                return self::retornaArrayEncerramento($arrayhistorico);
                break;
            case 'Termo de Rescisão':
                return self::retornaArrayRescisao($arrayhistorico);
                break;
            case 'Termo Aditivo':
                return self::retornaArrayAditivo($arrayhistorico, $contrato_id, $contratohistoricoatual);
                break;
            case 'Termo de Apostilamento':
                return self::retornaArrayApostilamento($arrayhistorico);
                break;
            default:
                return self::retornaArrayDefault($arrayhistorico);
        }
    }

    public function createEventCalendar(Contratohistorico $contratohistorico)
    {
        $contrato = Contrato::find($contratohistorico->contrato_id);

        $fornecedor = $contrato->fornecedor->cpf_cnpj_idgener . ' - ' . $contrato->fornecedor->nome;
        $ug = $contrato->unidade->codigo . ' - ' . $contrato->unidade->nomeresumido;

        $tituloinicio = 'Início Vigência Contrato: ' . $contrato->numero . ' Fornecedor: ' . $fornecedor . ' da UG: ' . $ug;
        $titulofim = 'Fim Vigência Contrato: ' . $contrato->numero . ' Fornecedor: ' . $fornecedor . ' da UG: ' . $ug;

        $events = [
            [
                'title' => $tituloinicio,
                'start_date' => new DateTime($contrato->vigencia_inicio),
                'end_date' => new DateTime($contrato->vigencia_inicio),
                'unidade_id' => $contrato->unidade_id
            ],
            [
                'title' => $titulofim,
                'start_date' => new DateTime($contrato->vigencia_fim),
                'end_date' => new DateTime($contrato->vigencia_fim),
                'unidade_id' => $contrato->unidade_id
            ]

        ];

        foreach ($events as $e) {
            $calendario = new CalendarEvent();
            $calendario->insertEvents($e);
        }

        return $calendario;
    }

    public static function retornaArrayEncerramento(array $arrayhistorico)
    {
        $countPost = count($_POST);
        if ($countPost > 0) {
            // para os dois casos abaixo, é possível ser true, false ou null, que é não aplicável
            $isSaldoContaVinculadaLiberado = $_POST['is_saldo_conta_vinculada_liberado'];
            if ($isSaldoContaVinculadaLiberado == 2 || $isSaldoContaVinculadaLiberado == '') {
                $isSaldoContaVinculadaLiberado = null;
            }     // 2 quer dizer que é não aplicável
            $isGarantiaContratualDevolvida = $_POST['is_garantia_contratual_devolvida'];
            if ($isGarantiaContratualDevolvida == 2) {
                $isGarantiaContratualDevolvida = null;
            }     // 2 quer dizer que é não aplicável
            $dataEncerramento = $_POST['data_encerramento'];
            $isObjetoContratualEntregue = $_POST['is_objeto_contratual_entregue'];
            $isCumpridasObrigacoesFinanceiras = $_POST['is_cumpridas_obrigacoes_financeiras'];
            $consecucaoObjetivosContratacao = $_POST['consecucao_objetivos_contratacao'];
            // buscar dados do responsável signatário encerramento
            if (!is_null($_POST['user_id_responsavel_signatario_encerramento'])) {
                $objUser = User::find($_POST['user_id_responsavel_signatario_encerramento']);
                $user_id_responsavel_signatario_encerramento = $objUser->id;
                $nome_responsavel_signatario_encerramento = $objUser->name;
                $cpf_responsavel_signatario_encerramento = $objUser->cpf;
            } else {
                $user_id_responsavel_signatario_encerramento = null;
                $nome_responsavel_signatario_encerramento = null;
                $cpf_responsavel_signatario_encerramento = null;
            }
        } else {
            // aqui quer dizer que o Termo de Encerramento está sendo excluído - vamos tornar nulos os valores inseridos no ato do cadastro
            $isSaldoContaVinculadaLiberado = null;
            $isGarantiaContratualDevolvida = null;
            $dataEncerramento = null;
            $isObjetoContratualEntregue = false;    // esse é not null
            $isCumpridasObrigacoesFinanceiras = false;  // esse é not null
        }
        $arrayEncerramento = [
            'vigencia_fim' => $arrayhistorico['vigencia_fim'],
            'situacao' => $arrayhistorico['situacao'],
            'publicado' => $arrayhistorico['publicado'],
            'data_encerramento' => $dataEncerramento,
            'is_objeto_contratual_entregue' => $isObjetoContratualEntregue,
            'is_cumpridas_obrigacoes_financeiras' => $isCumpridasObrigacoesFinanceiras,
            'is_garantia_contratual_devolvida' => $isGarantiaContratualDevolvida,
            'is_saldo_conta_vinculada_liberado' => $isSaldoContaVinculadaLiberado,
            'consecucao_objetivos_contratacao' => @$consecucaoObjetivosContratacao,
            'user_id_responsavel_signatario_encerramento' => @$user_id_responsavel_signatario_encerramento,
            'nome_responsavel_signatario_encerramento' => @$nome_responsavel_signatario_encerramento,
            'cpf_responsavel_signatario_encerramento' => @$cpf_responsavel_signatario_encerramento,
        ];

        return $arrayEncerramento;
    }

    public static function retornaArrayRescisao(array $arrayhistorico)
    {
        $arrayRescisao = [
            'vigencia_fim' => $arrayhistorico['vigencia_fim'],
            'situacao' => $arrayhistorico['situacao'],
            'publicado' => $arrayhistorico['publicado'],
        ];
        return $arrayRescisao;
    }

    public static function retornaArrayAditivo(array $arrayhistorico, $contrato_id, $contratohistoricoatual = null)
    {
        $novo_valor = $arrayhistorico['valor_global'];

        if ($arrayhistorico['supressao'] == 'S') {
            $contrato = Contrato::find($contrato_id);
            $novo_valor = $contrato->valor_global - $novo_valor;
        }

        $historico = Contratohistorico::find($arrayhistorico['id']);

        $arrayAditivo = [
            'fornecedor_id' => $arrayhistorico['fornecedor_id'],
            'unidade_id' => $arrayhistorico['unidade_id'],
            'info_complementar' => $arrayhistorico['info_complementar'],
            'valor_global' => $novo_valor,
            'num_parcelas' => $arrayhistorico['num_parcelas'],
            'valor_parcela' => $arrayhistorico['valor_parcela'],
            'publicado' => $arrayhistorico['publicado'],
        ];
        (isset($arrayhistorico['situacao'])) ? $arrayAditivo['situacao'] = $arrayhistorico['situacao'] : "";

        if (isset($historico->qualificacoes) && $historico->id != $contratohistoricoatual->id) {

            foreach ($historico->qualificacoes as $qualificacao) {
                if ($qualificacao->descricao == "VIGÊNCIA") {
                    //coloque a nova data de vigência fim no aditivo
                    $arrayAditivo['vigencia_fim'] = $arrayhistorico['vigencia_fim'];

                }
            }
        }

        return $arrayAditivo;
    }

    public static function retornaArrayApostilamento(array $arrayhistorico)
    {
        $arrayApostilamento = [
            'unidade_id' => $arrayhistorico['unidade_id'],
            'valor_global' => $arrayhistorico['valor_global'],
            'num_parcelas' => $arrayhistorico['num_parcelas'],
            'valor_parcela' => $arrayhistorico['valor_parcela'],
            'publicado' => $arrayhistorico['publicado'],
        ];
        (isset($arrayhistorico['situacao'])) ? $arrayApostilamento['situacao'] = $arrayhistorico['situacao'] : "";
        return $arrayApostilamento;
    }

    public static function retornaArrayDefault(array $arrayhistorico)
    {
        unset($arrayhistorico['id']);
        unset($arrayhistorico['contrato_id']);
        unset($arrayhistorico['observacao']);
        unset($arrayhistorico['created_at']);
        unset($arrayhistorico['updated_at']);
        unset($arrayhistorico['retroativo']);
        unset($arrayhistorico['retroativo_mesref_de']);
        unset($arrayhistorico['retroativo_anoref_de']);
        unset($arrayhistorico['retroativo_mesref_ate']);
        unset($arrayhistorico['retroativo_anoref_ate']);
        unset($arrayhistorico['retroativo_vencimento']);
        unset($arrayhistorico['retroativo_valor']);
        unset($arrayhistorico['retroativo_soma_subtrai']);
        unset($arrayhistorico['is_com_valor']);
        unset($arrayhistorico['contrato']);

        $arrayDefault = array_filter($arrayhistorico, function ($a) {
            return trim($a) !== "";
        });

        if ($arrayhistorico['is_prazo_indefinido'] == '1' or $arrayhistorico['is_prazo_indefinido'] == true) {
            $arrayDefault['vigencia_fim'] = null;
            $arrayDefault['num_parcelas'] = 1;
        }

        if ($arrayhistorico['is_prazo_indefinido'] == '0' or $arrayhistorico['is_prazo_indefinido'] == false) {
            $arrayDefault['is_prazo_indefinido'] = false;
        }

        if (isset($arrayhistorico['situacao'])) {
            $arrayDefault['situacao'] = $arrayhistorico['situacao'];
        }
        if (isset($arrayhistorico['aplicavel_decreto'])) {
            $arrayDefault['aplicavel_decreto'] = $arrayhistorico['aplicavel_decreto'];
        }
        return $arrayDefault;
    }

    public static function getInstrumentoInicial(int $contratoId)
    {
        return Contratohistorico::where('contrato_id', $contratoId)
            ->where('contratohistorico.elaboracao', false)->oldest()->first();
    }

    public static function isTermoContrato(Contratohistorico $contratohistorico)
    {

        if (str_contains('Contrato', 'Termo')) {
            return true;
        }

        return false;
    }

    public function podeEnviarContratoPNCP(
        array             $idAmparoLegal,
        Contratohistorico $contratohistorico,
        Contratohistorico $instrumentoInicial
    )
    {
        $amparoLegal14133 = $this->amparoLegalLei14133($idAmparoLegal);
        $unidadeSigilo = $instrumentoInicial->unidade->sigilo;
        # Obtem o ano do contrato
        $anoContrato = intval(explode('/', $instrumentoInicial->licitacao_numero)[1]);

        if ($amparoLegal14133 &&
            !in_array('Contrato', $this->tiposNegadosPNCP) &&
            !$unidadeSigilo && $anoContrato >= 2021
        ) {
            return true;
        }

        return false;
    }

    public function amparoLegalLei14133(?array $idAmparoLegal): bool
    {
        if (empty($idAmparoLegal)) {
            return false;
        }

        $amparoLegalLei14133 = $this->getAmparoPodeEnviarPNCPPorId($idAmparoLegal);

        return $amparoLegalLei14133->count() > 0;
    }

    public function getAmparoPodeEnviarPNCPPorId($idAmparoLegal)
    {
        return \App\Models\AmparoLegal::whereIn('id', $idAmparoLegal)->where('complemento_14133', true)->get();
    }

    private function executaAtualizacaoViaJob($contratohistorico)
    {
        $publicado_id = $this->retornaIdTipoPublicado();

        $publicacao = ContratoPublicacoes::Create(
            [
                'contratohistorico_id' => $contratohistorico->id,
                'status_publicacao_id' => $publicado_id,
                'data_publicacao' => $contratohistorico->data_publicacao,
                'texto_dou' => '',
                'status' => 'Importado',
                'tipo_pagamento_id' => $this->retornaIdCodigoItem('Forma Pagamento', 'Isento'),
                'motivo_isencao' => 0
            ]
        );
        return $publicacao;
    }

    private function retornaIdTipoPublicado()
    {
        return Codigoitem::whereHas('codigo', function ($query) {
            $query->where('descricao', 'Situacao Publicacao');
        })->where('descricao', 'PUBLICADO')->first()->id;
    }

    public function criaNovaPublicacao($contratohistorico, $cpf, $create = false)
    {

        $lei14 = $contratohistorico->amparolegal->toArray()[0]['complemento_14133'];

        $texto_dou = @DiarioOficialClass::retornaTextoModelo($contratohistorico);

        $sisg = (isset($contratohistorico->unidade->sisg)) ? $contratohistorico->unidade->sisg : '';

        $situacao = $this->getSituacao($sisg, $contratohistorico->data_publicacao, $create, $lei14);

        if (!is_null($texto_dou)) {
            $novaPublicacao = ContratoPublicacoes::create([
                'contratohistorico_id' => $contratohistorico->id,
                'data_publicacao' => $contratohistorico->data_publicacao ? $this->verificaDataDiaUtil($contratohistorico->data_publicacao) : null,
                'status' => 'Pendente',
                'status_publicacao_id' => $situacao->id,
                'cpf' => $cpf,
                'texto_dou' => ($texto_dou != '') ? $texto_dou : '',
                'tipo_pagamento_id' => $this->retornaIdCodigoItem('Forma Pagamento', 'Isento'),
                'motivo_isencao_id' => $this->retornaIdCodigoItem('Motivo Isenção', 'Indefinido')
            ]);
        }
    }

    private function getSituacao($sisg, $data = null, $create = false, $lei14 = false)
    {
        $situacao = Codigoitem::whereHas('codigo', function ($query) {
            $query->where('descricao', 'Situacao Publicacao');
        })
            ->select('codigoitens.id');

        if ($data === null && $lei14 === true) {
            return $situacao->where('descricao', 'A PUBLICAR')->first();
        }

        if ($create) {
            $data = Carbon::createFromFormat('Y-m-d', $data);
            if ($data->lte(Carbon::now())) {
                return $situacao->where('descricao', 'PUBLICADO')->first();
            }
        }
        if ($sisg) {
            return $situacao->where('descricao', 'A PUBLICAR')->first();
        }
        return $situacao->where('descricao', 'INFORMADO')->first();
    }

    public function inserirCronogramaFromHistorico(Contratohistorico $contratohistorico)
    {
        if (!$contratohistorico->num_parcelas and !$contratohistorico->vigencia_inicio and !$contratohistorico->valor_parcela) {
            return '';
        }

        $dados = $this->montaCronograma($contratohistorico);
        $this->inserirDadosEmMassa($dados);

        return $this;

    }

    private function montaCronograma(Contratohistorico $contratohistorico)
    {

        if ($contratohistorico->is_com_valor === "0" || $contratohistorico->is_com_valor === false) {
            return [];
        }

        //#140;
        if ($contratohistorico->is_prazo_indefinido === "1" || $contratohistorico->is_prazo_indefinido === true) {

            $contratohistorico->vigencia_fim = date('Y-m-d', strtotime($contratohistorico->vigencia_inicio . ' + 5 years'));
            $contratohistorico->num_parcelas = 60;
            //dd($contratohistorico->vigencia_inicio ,$contratohistorico->vigencia_fim);
        } elseif ($contratohistorico->contrato->is_prazo_indefinido === "1" || $contratohistorico->contrato->is_prazo_indefinido === true) {

            $contratohistorico->vigencia_fim = date('Y-m-d', strtotime($contratohistorico->vigencia_inicio . ' + 5 years'));
            $contratohistorico->num_parcelas = 60;
        }

        if ($contratohistorico->data_inicio_novo_valor) {
            $data = date_create($contratohistorico->data_inicio_novo_valor);
            $mesinicio = new \DateTime($contratohistorico->data_inicio_novo_valor);
            $mesfim = new \DateTime($contratohistorico->data_fim_novo_valor ?: $contratohistorico->vigencia_fim);
            $interval = $mesinicio->diff($mesfim);
            if ($interval->y != 0) {
                $t = ($interval->y * 12) + $interval->m;
            } else {
                $t = $interval->m;
            }

            if ($interval->d > 0) {
                $t = $t + 1;
            }

        } else {
            $data = date_create($contratohistorico->vigencia_inicio);
            $t = $contratohistorico->num_parcelas;
        }

        $mesref = date_format($data, 'Y-m');
        $mesrefnew = $mesref . "-01";

        $dados = [];
        for ($i = 1; $i <= $t; $i++) {
            $vencimento = date('Y-m-d', strtotime("+" . $i . " month", strtotime($mesrefnew)));
            $ref = date('Y-m-d', strtotime("-1 month", strtotime($vencimento)));

            $buscacron = \App\Models\Contratocronograma::where('contrato_id', '=', $contratohistorico->contrato_id)
                ->where('mesref', '=', date('m', strtotime($ref)))
                ->where('anoref', '=', date('Y', strtotime($ref)))
                ->where('retroativo', '=', 'f')
                ->get();

            $valor = number_format($contratohistorico->valor_parcela, 2, '.', '');

            if ($contratohistorico->tipo_id == 65 or $contratohistorico->tipo_id == 68) {
                $v = $valor;

                if (!$buscacron->isEmpty()) {
                    foreach ($buscacron as $b) {
                        $v = number_format($v, 2, '.', '') - number_format($b->valor, 2, '.', '');
                    }
                }

                $dados[] = [
                    'contrato_id' => $contratohistorico->contrato_id,
                    'contratohistorico_id' => $contratohistorico->id,
                    'receita_despesa' => $contratohistorico->receita_despesa,
                    'mesref' => date('m', strtotime($ref)),
                    'anoref' => date('Y', strtotime($ref)),
                    'vencimento' => $vencimento,
                    'valor' => number_format($v, 2, '.', ''),
                    'soma_subtrai' => ($v < 0) ? false : true,
                ];
            } else {
                $dados[] = [
                    'contrato_id' => $contratohistorico->contrato_id,
                    'contratohistorico_id' => $contratohistorico->id,
                    'receita_despesa' => $contratohistorico->receita_despesa,
                    'mesref' => date('m', strtotime($ref)),
                    'anoref' => date('Y', strtotime($ref)),
                    'vencimento' => $vencimento,
                    'valor' => $valor,
                    'soma_subtrai' => ($valor < 0) ? false : true,
                ];
            }

        }

        if ($contratohistorico->retroativo == 1) {

            $ret_mesref_de = $contratohistorico->retroativo_mesref_de;
            $ret_anoref_de = $contratohistorico->retroativo_anoref_de;
            $ret_mesref_ate = $contratohistorico->retroativo_mesref_ate;
            $ret_anoref_ate = $contratohistorico->retroativo_anoref_ate;
            $ret_soma_subtrai = $contratohistorico->retroativo_soma_subtrai;


            if ($ret_mesref_de == $ret_mesref_ate and $ret_anoref_de == $ret_anoref_ate) {
                $dados[] = [
                    'contrato_id' => $contratohistorico->contrato_id,
                    'contratohistorico_id' => $contratohistorico->id,
                    'receita_despesa' => $contratohistorico->receita_despesa,
                    'mesref' => $ret_mesref_de,
                    'anoref' => $ret_anoref_de,
                    'vencimento' => $contratohistorico->retroativo_vencimento,
                    'valor' => number_format($contratohistorico->retroativo_valor, 2, '.', ''),
                    'retroativo' => true,
                    'soma_subtrai' => $ret_soma_subtrai,
                ];
            } else {
                $mesrefde = new \DateTime($ret_anoref_de . '-' . $ret_mesref_de . '-01');
                $mesrefate = new \DateTime($ret_anoref_ate . '-' . $ret_mesref_ate . '-01');
                $intervalo = $mesrefde->diff($mesrefate);
                if ($intervalo->y != 0) {
                    $meses = ($intervalo->y * 12) + $intervalo->m + 1;
                } else {
                    $meses = $intervalo->m + 1;
                }

                $valor_ret = number_format($contratohistorico->retroativo_valor / $meses, 2, '.', '');

                for ($j = 1; $j <= $meses; $j++) {
                    $dtformat = $ret_anoref_de . '-' . $ret_mesref_de . '-01';

                    if ($j == 1) {
                        $ref1 = date('Y-m-d', strtotime($dtformat));
                    } else {
                        $p = $j - 1;
                        $ref1 = date('Y-m-d', strtotime("+" . $p . " month", strtotime($dtformat)));
                    }

                    $dados[] = [
                        'contrato_id' => $contratohistorico->contrato_id,
                        'contratohistorico_id' => $contratohistorico->id,
                        'receita_despesa' => $contratohistorico->receita_despesa,
                        'mesref' => date('m', strtotime($ref1)),
                        'anoref' => date('Y', strtotime($ref1)),
                        'vencimento' => $contratohistorico->retroativo_vencimento,
                        'valor' => number_format($valor_ret, 2, '.', ''),
                        'retroativo' => true,
                        'soma_subtrai' => $ret_soma_subtrai,
                    ];

                }

            }

        }
        return $dados;
    }

    private function inserirDadosEmMassa(array $dados)
    {
        foreach ($dados as $d) {
            if ($d['valor'] != 0) {
                $cronograma = $this->insertCronograma($d);
            }
        }

        return true;

    }

    private function insertCronograma(array $dado)
    {

        $cronograma = new \App\Models\Contratocronograma();
        $cronograma->fill($dado);
        $cronograma->save();

        return $cronograma;
    }
}
