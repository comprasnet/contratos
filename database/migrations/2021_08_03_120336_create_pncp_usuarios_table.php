<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePncpUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pncp_usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_pncp')->nullable();
            $table->string('cpfCnpj');
            $table->string('nome');
            $table->string('email');
            $table->boolean('administrador');
            $table->text('token');
            $table->string('senha');
            $table->boolean('ativo');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pncp_usuarios');
    }
}
