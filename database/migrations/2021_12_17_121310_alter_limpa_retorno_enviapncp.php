<?php

use App\Models\Codigo;
use App\Models\Codigoitem;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AlterLimpaRetornoEnviapncp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = Codigo::where('descricao', 'Situação Envia Dados PNCP')->first();
        $codigoExclusao = Codigoitem::where([
            ['codigo_id', '=', $codigo->id],
            ['descres', '=', 'EXCLUIDO'],
        ])->first();
        $codigoSucesso = Codigoitem::where([
            ['codigo_id', '=', $codigo->id],
            ['descres', '=', 'SUCESSO'],
        ])->first();
        DB::table('envia_dados_pncp')
        ->where('situacao', '=', $codigoExclusao->id)
        ->orWhere('situacao', '=', $codigoSucesso->id)
            ->update([
                'retorno_pncp' => '',
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
