<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContratoMinutasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contrato_minutas', function (Blueprint $table) {
            $table->increments('id');
            $table->date('data');
            $table->unsignedInteger('contrato_id');
            $table->unsignedInteger('tipo_modelo_documento_id');
            $table->unsignedInteger('contrato_minutas_status_id');            
            $table->text('texto_minuta');
            $table->timestamps();

            $table->foreign('contrato_id')->references('id')->on('contratos');
            $table->foreign('tipo_modelo_documento_id')->references('id')->on('modelo_documentos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contrato_minutas');
    }
}
