<?php

use Illuminate\Database\Migrations\Migration;
use \App\Models\EnviaDadosPncp;
use \App\Models\MinutaEmpenho;
use \App\Http\Traits\EnviaPncpTrait;

class RemoverEnvioPncpMinutaEmpenhoCompraInvalida extends Migration
{
    use EnviaPncpTrait;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $minutaEmpenhoEnvioPncp = EnviaDadosPncp::whereHas('status', function ($query) {
            $query->where('codigoitens.descres', 'INCPEN');
        })
            ->where('pncpable_type', MinutaEmpenho::class)
            ->get();

        foreach ($minutaEmpenhoEnvioPncp as $enviaDados) {

            $empenho = MinutaEmpenho::find($enviaDados->pncpable_id);

            $sispp = $this->buscaCNPJContratanteNovo(
                $empenho->compra->modalidade->descres,
                $empenho->compra->numero_ano,
                $empenho->compra->unidade_origem->codigo,
                $empenho->unidade_id()->first()->codigo);

            if ($sispp->codigoRetorno == '204') {
                $enviaDados->retorno_pncp =$sispp->messagem;
                $enviaDados->save();

                $enviaDados->delete();

                $mensagem = "Id envia dados PNCP deletado logicamente: {$enviaDados->id}";
                \Illuminate\Support\Facades\Log::info($mensagem);
            }
        }
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
