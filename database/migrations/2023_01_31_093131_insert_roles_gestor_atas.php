<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class InsertRolesGestorAtas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        app()['cache']->forget('spatie.permission.cache');
        // verificar se role ainda não existe
        $objRole = Role::where('name', 'Gestor de Atas')->first();
        if( !is_object( $objRole ) ){
            $objRole = Role::create(['name' => 'Gestor de Atas']);
        }
        // verificar se a permissão já existe
        $objPermissao = Permission::where('name', 'V2_acessar')->first();
        // caso não exista, criá-la
        if( !is_object($objPermissao) ){
            $objPermissao = Permission::create(['name' => 'V2_acessar']);
        }
        // verificar se ela já está ligada a role Gestor de Atas
        $idRole = $objRole->id;
        $idPermissao = $objPermissao->id;
        $objRoleHasPermission = DB::table('role_has_permissions')->where('permission_id', $idPermissao)->where('role_id', $idRole)->first();
        // caso não esteja, ligá-la
        if( !is_object($objRoleHasPermission) ){
            $objRole->givePermissionTo('V2_acessar');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $role = Role::where(['name' => 'Gestor de Atas'])->first();
        if( is_object($role) ){
            $role->revokePermissionTo('V2_acessar');
            $role->forceDelete();
        }
        Permission::where(['name' => 'V2_acessar'])->forceDelete();
    }
}
