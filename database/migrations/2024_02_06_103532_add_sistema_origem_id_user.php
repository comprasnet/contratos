<?php

use App\Models\BackpackUser;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSistemaOrigemIdUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('id_sistema_origem')->unsigned()->default(1);

            $table->foreign('id_sistema_origem')
                ->references('id')
                ->on('sistema_origem');
        });

        DB::beginTransaction();
        try {

            $usuarios = [
                ['cpf' => 'usr_fnde_api', 'origem' => 3],
                ['cpf' => 'usr_compras_api', 'origem' => 2],
                ['cpf' => 'usr_antecipagov_api', 'origem' => 4],
                ['cpf' => 'usr_bacen_api', 'origem' => 5],
                ['cpf' => 'usr_pmsp_api', 'origem' => 6],
                ['cpf' => 'usr_contratos_api', 'origem' => 7],
            ];

            foreach ($usuarios as $usuario) {
                $cpf = $usuario['cpf'];
                $origem = $usuario['origem'];

                $user = BackpackUser::where('cpf', 'like', $cpf . '%')->first();
                if ($user) {
                    $user->id_sistema_origem = $origem;
                    $user->save();
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('id_sistema_origem');
        });
    }
}
