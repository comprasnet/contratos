<?php

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertPermissionApropriacao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        app()['cache']->forget('spatie.permission.cache');

        $permission = Permission::where('name','apropriar_fatura')->first();
        if(!isset($permission->id)){
            Permission::create([
                'name' => 'apropriar_fatura',
                'guard_name' => 'web'
                ]);
            $role = Role::where('name', 'Administrador')->first();
            $role->givePermissionTo('apropriar_fatura');
        }

        $permission = Permission::where('name','apropriar_folha')->first();
        if(!isset($permission->id)){
            Permission::create([
                'name' => 'apropriar_folha',
                'guard_name' => 'web'
                ]);
            $role = Role::where('name', 'Administrador')->first();
            $role->givePermissionTo('apropriar_folha');
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $role = Role::where(['name' => 'Administrador'])->first();
        $role->revokePermissionTo('apropriar_folha');
        $role->revokePermissionTo('apropriar_fatura');

        Permission::where(['name' => 'apropriar_folha'])->forceDelete();
        Permission::where(['name' => 'apropriar_fatura'])->forceDelete();
    }
}
