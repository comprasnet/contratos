<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Role;

class InsertRoleConsultaGlobalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        app()['cache']->forget('spatie.permission.cache');
        if(!Role::where('name', 'Consulta Global')->first()){
            $role = Role::create(['name' => 'Consulta Global']);
            $role->givePermissionTo('usuario_consulta_api');
        }

    }

    /**
     * Reverse the migrations.=
     *
     * @return void
     */
    public function down()
    {
        $role = Role::where(['name' => 'Consulta Global'])->first();
        $role->revokePermissionTo('usuario_consulta_api');
        $role->delete();
    }
}
