<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Unidade;

class UpdateUnidadesUtilizaAntecipagov extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('unidades', 'utiliza_siafi')) {
            Unidade::where('utiliza_siafi', true)
                ->where('sisg', true)
                ->update(['utiliza_antecipagov' => true]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('unidades', 'utiliza_siafi')) {
            Unidade::where('utiliza_siafi', true)
                ->where('sisg', true)
                ->update(['utiliza_antecipagov' => false]);
        }

    }
}
