<?php

use App\Models\Codigo;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class UpdateAltpenParaRetpen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = Codigo::where('descricao', 'Situação Envia Dados PNCP')->first();
        DB::table('codigoitens')
            ->where([
                ['codigo_id', '=', $codigo->id],
                ['descres', '=', 'ALTPEN'],
            ])
            ->update([
                'descres' => 'RETPEN',
                'descricao'=>'Retificação Pendente'
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $codigo = Codigo::where('descricao', 'Situação Envia Dados PNCP')->first();
        DB::table('codigoitens')
            ->where([
                ['codigo_id', '=', $codigo->id],
                ['descres', '=', 'RETPEN'],
            ])
            ->update([
                'descres' => 'ALTPEN',
                'descricao'=>'Alteração Pendente'
            ]);
    }
}
