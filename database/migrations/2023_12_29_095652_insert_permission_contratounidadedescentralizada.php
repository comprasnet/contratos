<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class InsertPermissionContratounidadedescentralizada extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        app()['cache']->forget('spatie.permission.cache');

        Permission::create(['name' => 'contrato_unidadedescentralizada_inserir']);
        Permission::create(['name' => 'contrato_unidadedescentralizada_editar']);
        Permission::create(['name' => 'contrato_unidadedescentralizada_show']);
        Permission::create(['name' => 'contrato_unidadedescentralizada_deletar']);

        $role = Role::where(['name' => 'Setor Contratos'])->first();
        $role->givePermissionTo('contrato_unidadedescentralizada_inserir');
        $role->givePermissionTo('contrato_unidadedescentralizada_editar');
        $role->givePermissionTo('contrato_unidadedescentralizada_show');
        $role->givePermissionTo('contrato_unidadedescentralizada_deletar');

        $role = Role::where(['name' => 'Administrador'])->first();
        $role->givePermissionTo('contrato_unidadedescentralizada_inserir');
        $role->givePermissionTo('contrato_unidadedescentralizada_editar');
        $role->givePermissionTo('contrato_unidadedescentralizada_show');
        $role->givePermissionTo('contrato_unidadedescentralizada_deletar');

        $role = Role::where(['name' => 'Administrador Suporte'])->first();
        $role->givePermissionTo('contrato_unidadedescentralizada_show');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $role = Role::where(['name' => 'Setor Contratos'])->first();
        $role->revokePermissionTo('contrato_unidadedescentralizada_inserir');
        $role->revokePermissionTo('contrato_unidadedescentralizada_editar');
        $role->revokePermissionTo('contrato_unidadedescentralizada_show');
        $role->revokePermissionTo('contrato_unidadedescentralizada_deletar');

        $role = Role::where(['name' => 'Administrador'])->first();
        $role->revokePermissionTo('contrato_unidadedescentralizada_inserir');
        $role->revokePermissionTo('ccontrato_unidadedescentralizada_editar');
        $role->revokePermissionTo('contrato_unidadedescentralizada_show');
        $role->revokePermissionTo('contrato_unidadedescentralizada_deletar');

        $role = Role::where(['name' => 'Administrador Suporte'])->first();
        $role->revokePermissionTo('contrato_unidadedescentralizada_show');

        Permission::where(['name' => 'contrato_unidadedescentralizada_inserir'])->forceDelete();
        Permission::where(['name' => 'contrato_unidadedescentralizada_editar'])->forceDelete();
        Permission::where(['name' => 'contrato_unidadedescentralizada_show'])->forceDelete();
        Permission::where(['name' => 'contrato_unidadedescentralizada_deletar'])->forceDelete();
    }
}
