<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaisfabricacaoIdContratofaturasItensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contratofaturas_itens', function (Blueprint $table) {
            $table->integer('paisfabricacao_id')->nullable();

            $table->foreign('paisfabricacao_id')->references('id')->on('paises')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contratofaturas_itens', function (Blueprint $table) {
            $table->dropColumn('paisfabricacao_id');
        });
    }
}
