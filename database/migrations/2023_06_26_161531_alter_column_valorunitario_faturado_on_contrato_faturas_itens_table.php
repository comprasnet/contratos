<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterColumnValorunitarioFaturadoOnContratoFaturasItensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contratofaturas_itens', function(Blueprint $table) {
            $table->decimal('valorunitario_faturado', 32, 17)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contratofaturas_itens', function(Blueprint $table) {
            $table->decimal('valorunitario_faturado', 19, 4)->change();
        });
    }
}
