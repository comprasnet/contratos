<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCotratominutasCriarNumeroSuperSei extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        #248
        if(!Schema::hasColumn('contrato_minutas', 'numero_super_sei')){
            Schema::table('contrato_minutas', function (Blueprint $table) {
                $table->string('numero_super_sei')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('contrato_minutas', 'numero_super_sei')){
            Schema::table('contrato_minutas', function ($table) {
                $table->dropColumn('numero_super_sei');
            });
        }
    }
}
