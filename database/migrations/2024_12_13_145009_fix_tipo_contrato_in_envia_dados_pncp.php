<?php

use App\Models\EnviaDadosPncp;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Formatter\LineFormatter;

class FixTipoContratoInEnviaDadosPncp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        ##############################################################################################
        ### FORMATAÇÕES PARA GERAR O LOG INICIAL
        ##############################################################################################
        $handler = new StreamHandler(
            storage_path("logs/1503-registros-a-serem-corrigidos.log"),
            Logger::INFO);
        $formatter = new LineFormatter("%datetime% %channel%.%level_name%: %message%\n");
        $handler->setFormatter($formatter);
        $log = new \Monolog\Logger('custom');
        $log->pushHandler($handler);
        ##############################################################################################

        $linhasPncpComTipoDiferenteDoContrato = EnviaDadosPncp::selectRaw(
            'envia_dados_pncp.id as linha_pncp_id,' .
            ' envia_dados_pncp.contrato_id,' .
            ' contratohistorico.id as contratohistorico_id,' .
            ' envia_dados_pncp.tipo_contrato as tipo_na_tabela_envia_dados_pncp,' .
            ' codigoitens.descricao as tipo_contrato_correto'
        )
            ->join('contratohistorico', function ($join) {
                $join->on('envia_dados_pncp.pncpable_id', '=', 'contratohistorico.id')
                    ->whereColumn('envia_dados_pncp.contrato_id', '=', 'contratohistorico.contrato_id');
            })
            ->join('codigoitens', 'contratohistorico.tipo_id', '=', 'codigoitens.id')
            ->whereIn('envia_dados_pncp.tipo_contrato',
                array_merge(
                    config('api-pncp.tipos_contrato_aceitos_pncp'),
                    config('api-pncp.tipos_termo_contrato_aceitos_pncp'))
            )
            ->where('envia_dados_pncp.tipo_contrato', '<>', \DB::raw('codigoitens.descricao'))
            ->orderBy('envia_dados_pncp.id')
            ->get();

        # Gera uma linha que será o cabeçalho ao abrir o log em uma planilha
        $cabecalhoDoLog = ",ID Linha PNCP,ID Contrato,ID Contrato Histórico,";
        $log->info($cabecalhoDoLog . "Tipo Errado,Tipo Correto");

        # Armazena os registros alterados para gerar um log no final
        $registrosAlterados = [];

        ##############################################################################################
        ### FORMATAÇÕES PARA GERAR O LOG FINAL
        ##############################################################################################
        $handlerResultado = new StreamHandler(
            storage_path("logs/1503-resultado-registros-corrigidos.log"),
            Logger::INFO
        );
        $formatterResultado = new LineFormatter("%datetime% %channel%.%level_name%: %message%\n");
        $handlerResultado->setFormatter($formatterResultado);
        $logResultado = new \Monolog\Logger('custom');
        $logResultado->pushHandler($handlerResultado);
        ##############################################################################################

        foreach ($linhasPncpComTipoDiferenteDoContrato as $registro) {
            try {
                EnviaDadosPncp::where('id', $registro->linha_pncp_id)
                    ->update(['tipo_contrato' => $registro->tipo_contrato_correto]);

                $registrosAlterados[] = $registro->linha_pncp_id;

                $linhaCsv = implode(',', [
                    ',' .
                    $registro->linha_pncp_id,
                    $registro->contrato_id,
                    $registro->contratohistorico_id,
                    $registro->tipo_na_tabela_envia_dados_pncp,
                    $registro->tipo_contrato_correto,
                ]);
                $log->info($linhaCsv);
            } catch (\Exception $e) {
                $linhaErroCsv = implode(',', [
                    ',' .
                    $registro->linha_pncp_id,
                    $registro->contrato_id,
                    $registro->contratohistorico_id,
                    $registro->tipo_na_tabela_envia_dados_pncp,
                    "ERRO AO ATUALIZAR",
                ]);
                $log->error($linhaErroCsv);
            }
        }

        $linhasAtualizadas = EnviaDadosPncp::whereIn('envia_dados_pncp.id', $registrosAlterados)
            ->selectRaw(
                'envia_dados_pncp.id as linha_pncp_id,' .
                ' envia_dados_pncp.contrato_id,' .
                ' contratohistorico.id as contratohistorico_id,' .
                ' envia_dados_pncp.tipo_contrato as tipo_na_tabela_envia_dados_pncp,' .
                ' codigoitens.descricao as tipo_contrato_correto'
            )
            ->join('contratohistorico', function ($join) {
                $join->on('envia_dados_pncp.pncpable_id', '=', 'contratohistorico.id')
                    ->whereColumn('envia_dados_pncp.contrato_id', '=', 'contratohistorico.contrato_id');
            })
            ->join('codigoitens', 'contratohistorico.tipo_id', '=', 'codigoitens.id')
            ->get();

        $logResultado->info($cabecalhoDoLog . "Tipo Corrigido,Tipo Correto");

        foreach ($linhasAtualizadas as $registroAtualizado) {
            // Log dos registros após a correção
            $linhaCsvResultado = implode(',', [
                ',' .
                $registroAtualizado->linha_pncp_id,
                $registroAtualizado->contrato_id,
                $registroAtualizado->contratohistorico_id,
                $registroAtualizado->tipo_na_tabela_envia_dados_pncp,
                $registroAtualizado->tipo_contrato_correto,
            ]);
            $logResultado->info($linhaCsvResultado);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
