<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUrlsSeiSip extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orgaoconfiguracao', function (Blueprint $table) {
            $table->string('url_sei',200)->nullable();
            $table->string('url_sip',200)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orgaoconfiguracao', function (Blueprint $table) {
            $table->dropColumn('url_sei',200);
            $table->dropColumn('url_sip',200);
        });
    }
}
