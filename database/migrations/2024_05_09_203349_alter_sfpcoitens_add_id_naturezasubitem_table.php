<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSfpcoitensAddIdNaturezasubitemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sfpcoitem', function (Blueprint $table) {
            $table->integer('subelemento_id')->nullable();
            $table->foreign('subelemento_id')->references('id')->on('naturezasubitem')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sfpcoitem', function (Blueprint $table) {
            $table->dropColumn('subelemento_id');
        });
    }
}
