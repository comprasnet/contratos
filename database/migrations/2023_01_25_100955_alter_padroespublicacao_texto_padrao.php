<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Padroespublicacao;

class AlterPadroespublicacaoTextoPadrao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('padroespublicacao', 'texto_padrao')){
            $objPadraoPublicacao = Padroespublicacao::select('padroespublicacao.id', 'padroespublicacao.texto_padrao')
                ->join('codigoitens as tipocontrato', 'tipocontrato.id', 'padroespublicacao.tipo_contrato_id')
                ->join('codigoitens as tipomudanca', 'tipomudanca.id', 'padroespublicacao.tipo_mudanca_id')
                ->join('codigoitens as identificadornorma', 'identificadornorma.id', 'padroespublicacao.identificador_norma_id')
                ->where('tipocontrato.descricao', 'Termo de Rescisão')
                ->where('tipomudanca.descricao', 'INCLUSAO')
                ->where('identificadornorma.descricao', 'Extrato de Rescisão')
                ->first();

            if(is_object($objPadraoPublicacao)){
                $novoTextoPadrao = '##ATO EXTRATO DE RESCISÃO DO CONTRATO Nº |CONTRATOHISTORICO_NUMERO| - UASG |CONTRATOHISTORICO_GETUNIDADE|



##TEX Nº Processo: |CONTRATO_PROCESSO|. Contratante: |CONTRATO_UNIDADE_NOME|. Contratado: |CONTRATOHISTORICO_FORNECEDOR_CPF_CNPJ_IDGENER| - |CONTRATOHISTORICO_FORNECEDOR_NOME|. Objeto: |CONTRATOHISTORICO_OBJETO|. Fundamento Legal: |CONTRATO_RETORNAAMPARO|. Data de Rescisão:  |CONTRATOHISTORICO_DATA_RESCISAO|.

##OFI (COMPRASNET 4.0 - |DATA_ASSINATURA_SISTEMA|).';
                $objPadraoPublicacao->texto_padrao = $novoTextoPadrao;
                $objPadraoPublicacao->save();
            }
        }


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('padroespublicacao', 'texto_padrao')){
            $objPadraoPublicacao = Padroespublicacao::select('padroespublicacao.id', 'padroespublicacao.texto_padrao')
                ->join('codigoitens as tipocontrato', 'tipocontrato.id', 'padroespublicacao.tipo_contrato_id')
                ->join('codigoitens as tipomudanca', 'tipomudanca.id', 'padroespublicacao.tipo_mudanca_id')
                ->join('codigoitens as identificadornorma', 'identificadornorma.id', 'padroespublicacao.identificador_norma_id')
                ->where('tipocontrato.descricao', 'Termo de Rescisão')
                ->where('tipomudanca.descricao', 'INCLUSAO')
                ->where('identificadornorma.descricao', 'Extrato de Rescisão')
                ->first();




                if(is_object($objPadraoPublicacao)){
                    $novoTextoPadrao = '##ATO EXTRATO DE RESCISÃO DO CONTRATO Nº |CONTRATOHISTORICO_NUMERO| - UASG |CONTRATOHISTORICO_GETUNIDADE|



##TEX Nº Processo: |CONTRATO_PROCESSO|. Contratante: |CONTRATO_UNIDADE_NOME|. Contratado: |CONTRATOHISTORICO_FORNECEDOR_CPF_CNPJ_IDGENER| - |CONTRATOHISTORICO_FORNECEDOR_NOME|. Objeto: |CONTRATOHISTORICO_OBJETO|. Fundamento Legal: |CONTRATO_RETORNAAMPARO|. Data de Rescisão:  |CONTRATOHISTORICO_DATA_PUBLICACAO|.

##OFI (COMPRASNET 4.0 - |DATA_ASSINATURA_SISTEMA|).';
                                $objPadraoPublicacao->texto_padrao = $novoTextoPadrao;
                                $objPadraoPublicacao->save();
                }


        }
    }
}
