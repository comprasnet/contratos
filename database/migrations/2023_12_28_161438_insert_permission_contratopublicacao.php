<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class InsertPermissionContratopublicacao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        app()['cache']->forget('spatie.permission.cache');

        Permission::create(['name' => 'contrato_publicacao_inserir']);
        Permission::create(['name' => 'contrato_publicacao_editar']);
        Permission::create(['name' => 'contrato_publicacao_show']);
        Permission::create(['name' => 'contrato_publicacao_deletar']);
        Permission::create(['name' => 'contrato_publicacao_consultar']);
        Permission::create(['name' => 'contrato_publicacao_enviarpublicacao']);
        Permission::create(['name' => 'contrato_publicacao_deletarpublicacao']);

        $role = Role::where(['name' => 'Setor Contratos'])->first();
        $role->givePermissionTo('contrato_publicacao_inserir');
        $role->givePermissionTo('contrato_publicacao_editar');
        $role->givePermissionTo('contrato_publicacao_deletar');
        $role->givePermissionTo('contrato_publicacao_consultar');
        $role->givePermissionTo('contrato_publicacao_enviarpublicacao');
        $role->givePermissionTo('contrato_publicacao_deletarpublicacao');
        $role->givePermissionTo('contrato_publicacao_show');

        $role = Role::where(['name' => 'Administrador'])->first();
        $role->givePermissionTo('contrato_publicacao_inserir');
        $role->givePermissionTo('contrato_publicacao_editar');
        $role->givePermissionTo('contrato_publicacao_deletar');
        $role->givePermissionTo('contrato_publicacao_consultar');
        $role->givePermissionTo('contrato_publicacao_enviarpublicacao');
        $role->givePermissionTo('contrato_publicacao_deletarpublicacao');
        $role->givePermissionTo('contrato_publicacao_show');

        $role = Role::where(['name' => 'Administrador Suporte'])->first();
        $role->givePermissionTo('contrato_publicacao_show');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $role = Role::where(['name' => 'Setor Contratos'])->first();
        $role->revokePermissionTo('contrato_publicacao_inserir');
        $role->revokePermissionTo('contrato_publicacao_editar');
        $role->revokePermissionTo('contrato_publicacao_deletar');
        $role->revokePermissionTo('contrato_publicacao_consultar');
        $role->revokePermissionTo('contrato_publicacao_enviarpublicacao');
        $role->revokePermissionTo('contrato_publicacao_deletarpublicacao');
        $role->revokePermissionTo('contrato_publicacao_show');

        $role = Role::where(['name' => 'Administrador'])->first();
        $role->revokePermissionTo('contrato_publicacao_inserir');
        $role->revokePermissionTo('contrato_publicacao_editar');
        $role->revokePermissionTo('contrato_publicacao_deletar');
        $role->revokePermissionTo('contrato_publicacao_consultar');
        $role->revokePermissionTo('contrato_publicacao_enviarpublicacao');
        $role->revokePermissionTo('contrato_publicacao_deletarpublicacao');
        $role->revokePermissionTo('contrato_publicacao_show');

        $role = Role::where(['name' => 'Administrador Suporte'])->first();
        $role->revokePermissionTo('contrato_publicacao_show');

        Permission::where(['name' => 'contrato_publicacao_inserir'])->forceDelete();
        Permission::where(['name' => 'contrato_publicacao_editar'])->forceDelete();
        Permission::where(['name' => 'contrato_publicacao_deletar'])->forceDelete();
        Permission::where(['name' => 'contrato_publicacao_consultar'])->forceDelete();
        Permission::where(['name' => 'contrato_publicacao_deletarpublicacao'])->forceDelete();
        Permission::where(['name' => 'contrato_publicacao_enviarpublicacao'])->forceDelete();
        Permission::where(['name' => 'contrato_publicacao_show'])->forceDelete();
    }
}
