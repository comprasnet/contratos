<?php

use App\Http\Traits\MinutaEmpenhoTrait;
use App\Models\CompraItem;
use App\Models\CompraItemMinutaEmpenho;
use App\Models\Contratohistorico;
use App\Models\Contratoitem;
use App\Models\ContratoItemMinutaEmpenho;
use App\Models\ContratoMinutaEmpenhoPivot;
use App\Models\MinutaEmpenho;
use App\Models\Naturezadespesa;
use App\Models\Saldohistoricoitem;
use App\Repositories\MinutaRepository;
use App\services\MinutaService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Log;

class AtualizarItensContratosTipomaterial extends Migration
{
    use MinutaEmpenhoTrait;

    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        $contratoHistoricos = Contratohistorico::where('tipo_id', 151)->whereHas('saldosItens')
            ->select(['contrato_id', 'id'])
            ->get();

        foreach ($contratoHistoricos as $contratoHistorico) {
            $contratoPivot = ContratoMinutaEmpenhoPivot::where('contrato_id', $contratoHistorico['contrato_id'])->first();
            if ($contratoPivot) {
                $saldoHistorico = Saldohistoricoitem::where('saldoable_id', $contratoHistorico['id'])
                    ->get();

                $minutaEmpenho = MinutaEmpenho::where('id', '=', $contratoPivot->minuta_empenho_id)->first();
                session()->put('user_ug_id', $minutaEmpenho->unidade_id);

                $itensContrato = $this->getItens($minutaEmpenho);

                if(count($saldoHistorico) == 1 && count($itensContrato) == 1 && $saldoHistorico->first()->contratoItem->tipo_id === 150 && $saldoHistorico->first()->contratoItem->tipo_material == null){
                   // Log::info('entrei continua');
                   // Log::info($contratoPivot->contrato_id . ' unidade ' . $contratoPivot->contrato->unidade->codigo);
                    continue;
                }

                if (count($saldoHistorico) == 1 && count($itensContrato) >= 1) {

                    //Log::info($contratoPivot->contrato_id . ' unidade ' . $contratoPivot->contrato->unidade->codigo);
                    $catmatsergrupo = $minutaEmpenho->compraItemMinutaEmpenho->first()->compra_item->catmatseritem->catmatsergrupo;
                    $itensArray = [];
                    foreach ($itensContrato as $itens) {

                        //salvar em contratoitens
                        $compraitem = CompraItem::where('id', $itens['compra_item_id'])->first();

                        $tipoMaterial = '';
                        if ($compraitem->tipo_item_id == 149) {
                            if (isset($itens['subelemento_id'])) {
                                $codNaturezaDespesa = Naturezadespesa::select("naturezadespesa.codigo")
                                    ->join('naturezasubitem', 'naturezadespesa.id', '=', 'naturezasubitem.naturezadespesa_id')
                                    ->where('naturezasubitem.id', $itens['subelemento_id'])->first();
                                if (isset($codNaturezaDespesa->codigo)) {
                                    if (substr($codNaturezaDespesa->codigo, 0, 2) == '33')
                                        $tipoMaterial = 'Consumo';
                                    elseif (substr($codNaturezaDespesa->codigo, 0, 2) == '44')
                                        $tipoMaterial = 'Permanente';
                                    else
                                        $tipoMaterial = "Não se aplica";
                                }
                            }
                        }

                        $itensArray[] = [
                            'contrato_id' => $contratoPivot->contrato_id,
                            'tipo_id' => $compraitem->tipo_item_id,
                            'grupo_id' => $catmatsergrupo->id,
                            'catmatseritem_id' => $compraitem->catmatseritem_id,
                            'descricao_complementar' => $compraitem->descricaodetalhada,
                            'quantidade' => $itens['qtd_total_item'],
                            'valortotal' => $itens['vlr_total_item'],
                            'valorunitario' => $itens['vlr_unitario_item'],
                            'numero_item_compra' => $compraitem->numero,
                            'periodicidade' => 1,
                            'tipo_material' => $tipoMaterial
                        ];

                    }

                    DB::beginTransaction();
                    try {
                        foreach ($itensArray as $itens) {
                            $contratoItem = Contratoitem::firstOrCreate(
                                [
                                    'contrato_id' => $itens['contrato_id'],
                                    'tipo_id' => $itens['tipo_id'],
                                    'grupo_id' => $itens['grupo_id'],
                                    'catmatseritem_id' => $itens['catmatseritem_id'],
                                    'descricao_complementar' => $itens['descricao_complementar'],
                                    'numero_item_compra' => $itens['numero_item_compra'],
                                ],
                                [
                                    'contrato_id' => $itens['contrato_id'],
                                    'tipo_id' => $itens['tipo_id'],
                                    'grupo_id' => $itens['grupo_id'],
                                    'catmatseritem_id' => $itens['catmatseritem_id'],
                                    'descricao_complementar' => $itens['descricao_complementar'],
                                    'quantidade' => $itens['quantidade'],
                                    'valortotal' => $itens['valortotal'],
                                    'valorunitario' => $itens['valorunitario'],
                                    'numero_item_compra' => $itens['numero_item_compra'],
                                    'periodicidade' => $itens['periodicidade'],
                                    'tipo_material' => $itens['tipo_material']
                                ]
                            );

                            if($contratoItem->wasRecentlyCreated){
                                Log::info($contratoItem->id);
                            }

                            $saldoItem = Saldohistoricoitem::where('contratoitem_id', $contratoItem->id)
                                ->whereNull('tipo_material')->first();

                            if ($saldoItem && !empty($itens['tipo_material'])) {
                                $saldoItem->tipo_material = $itens['tipo_material'];
                                $saldoItem->save();
                            }
                            DB::commit();
                        }
                    } catch (\Exception $e) {
                        DB::rollBack();
                        Log::info('Erro ao atualizar o item do contrato ' . $e->getMessage());
                    }

                }
            }

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

    public function getItens($minutaEmpenho)
    {
        $tipo = $minutaEmpenho->empenho_por;
        $fornecedor_id = $minutaEmpenho->fornecedor_empenho_id;
        $fornecedor_compra_id = $minutaEmpenho->fornecedor_compra_id;

        switch ($tipo) {
            case 'Contrato':
                return $this->getItensContrato($minutaEmpenho);
                break;
            case 'Compra':
            case 'Suprimento':
                return $this->getItensCompra($minutaEmpenho, $fornecedor_id, $fornecedor_compra_id);
                break;
        }
    }

    public function getItensContrato($minutaEmpenho)
    {
        $itens = MinutaEmpenho::join(
            'contrato_item_minuta_empenho',
            'contrato_item_minuta_empenho.minutaempenho_id',
            '=',
            'minutaempenhos.id'
        )
            ->join(
                'contratoitens',
                'contratoitens.id',
                '=',
                'contrato_item_minuta_empenho.contrato_item_id'
            )
            ->join(
                'compras',
                'compras.id',
                '=',
                'minutaempenhos.compra_id'
            )
            ->join(
                'codigoitens as tipo_compra',
                'tipo_compra.id',
                '=',
                'compras.tipo_compra_id'
            )
            ->join(
                'codigoitens',
                'codigoitens.id',
                '=',
                'contratoitens.tipo_id'
            )
            ->join(
                'saldo_contabil',
                'saldo_contabil.id',
                '=',
                'minutaempenhos.saldo_contabil_id'
            )
            ->join(
                'naturezadespesa',
                'naturezadespesa.codigo',
                '=',
                DB::raw("SUBSTRING(saldo_contabil.conta_corrente,18,6)")
            )
            ->join(
                'catmatseritens',
                'catmatseritens.id',
                '=',
                'contratoitens.catmatseritem_id'
            )
            ->join(
                'minutaempenhos_remessa',
                'minutaempenhos_remessa.id',
                '=',
                'contrato_item_minuta_empenho.minutaempenhos_remessa_id'
            )
            ->where('naturezadespesa.sistema_origem', '=', 'SIAFI')
            ->where('minutaempenhos.id', $minutaEmpenho->id)
            ->where('minutaempenhos.unidade_id', session('user_ug_id'))
            ->distinct()
            ->select(
                [
                    'contrato_item_minuta_empenho.contrato_item_id',
                    'contrato_item_minuta_empenho.operacao_id',
                    'tipo_compra.descricao as tipo_compra_descricao',
                    'codigoitens.descricao',
                    'saldo_contabil.id as saldo_id',
                    'catmatseritens.codigo_siasg',
                    'contratoitens.numero_item_compra as numero_item',
                    'catmatseritens.descricao as catmatser_desc',
                    DB::raw("SUBSTRING(catmatseritens.descricao for 50) AS catmatser_desc_simplificado"),
                    'contratoitens.descricao_complementar as descricaodetalhada',
                    DB::raw("SUBSTRING(contratoitens.descricao_complementar for 50) AS descricaosimplificada"),

                    'contratoitens.quantidade as qtd_item',
                    'contratoitens.valorunitario as valorunitario',
                    'naturezadespesa.codigo as natureza_despesa',
                    'naturezadespesa.id as natureza_despesa_id',
                    'contratoitens.valortotal',
                    'saldo_contabil.saldo',
                    'contrato_item_minuta_empenho.subelemento_id',
                    DB::raw('left(minutaempenhos.mensagem_siafi, 4) as exercicio'),
                    'contrato_item_minuta_empenho.numseq',
                    DB::raw('contrato_item_minuta_empenho.id AS "numeroLinha"')
                ]
            )
            ->orderBy('contrato_item_minuta_empenho.numseq', 'asc');
        $soma = ContratoItemMinutaEmpenho::select([
            'contrato_item_id',
            DB::raw("sum(contrato_item_minuta_empenho.quantidade) as qtd_total_item"),
            DB::raw("sum(contrato_item_minuta_empenho.valor) as vlr_total_item"),
        ])
            ->where('operacao_id', 220)
            ->where('minutaempenho_id', $minutaEmpenho->id)
            ->groupBy('contrato_item_id');

        //CREATE
        if (is_null(session('remessa_id'))) {
            $itens->where('minutaempenhos_remessa.remessa', 0);

            $itens->addSelect([
                DB::raw("0 AS quantidade"),
                DB::raw("0 AS valor"),
            ]);

            return $this->retornarArray($itens->get()->toArray(), $soma->get()->toArray(), 'contrato_item_id');
        }

        $itens->where('contrato_item_minuta_empenho.minutaempenhos_remessa_id', session('remessa_id'));

        $itens->addSelect([
            'contrato_item_minuta_empenho.quantidade',
            'contrato_item_minuta_empenho.valor',
        ]);

        $soma->where('minutaempenhos_remessa_id', '<>', session('remessa_id'));

        return $this->retornarArray($itens->get()->toArray(), $soma->get()->toArray(), 'contrato_item_id');
    }

    public function getItensCompra($minutaEmpenho, $fornecedor_id, $fornecedor_compra_id)
    {

        $itens = MinutaEmpenho::join(
            'compra_item_minuta_empenho',
            'compra_item_minuta_empenho.minutaempenho_id',
            '=',
            'minutaempenhos.id'
        )
            ->join(
                'compra_items',
                'compra_items.id',
                '=',
                'compra_item_minuta_empenho.compra_item_id'
            )
            ->join(
                'compras',
                'compras.id',
                '=',
                'compra_items.compra_id'
            )
            ->join(
                'codigoitens as tipo_compra',
                'tipo_compra.id',
                '=',
                'compras.tipo_compra_id'
            )
            ->join(
                'codigoitens',
                'codigoitens.id',
                '=',
                'compra_items.tipo_item_id'
            )
            ->join(
                'saldo_contabil',
                'saldo_contabil.id',
                '=',
                'minutaempenhos.saldo_contabil_id'
            )
            ->join(
                'naturezadespesa',
                'naturezadespesa.codigo',
                '=',
                DB::raw("SUBSTRING(saldo_contabil.conta_corrente,18,6)")
            )
            ->join(
                'compra_item_fornecedor',
                'compra_item_fornecedor.compra_item_id',
                '=',
                'compra_items.id'
            )
            ->join(
                'compra_item_unidade',
                'compra_item_unidade.compra_item_id',
                '=',
                'compra_items.id'
            )
            ->join(
                'catmatseritens',
                'catmatseritens.id',
                '=',
                'compra_items.catmatseritem_id'
            )
            ->join(
                'minutaempenhos_remessa',
                'minutaempenhos_remessa.id',
                '=',
                'compra_item_minuta_empenho.minutaempenhos_remessa_id'
            )
            ->join('codigoitens as mod', 'mod.id', '=', 'compras.modalidade_id')
            ->where('minutaempenhos.id', $minutaEmpenho->id)
            ->where('naturezadespesa.sistema_origem', '=', 'SIAFI')
            ->where('compra_item_unidade.unidade_id', session('user_ug_id'))
            ->where('compra_item_unidade.situacao', true)
            ->where('compra_item_fornecedor.situacao', true)
            ->where('compra_items.situacao', true)
            ->distinct()
            ->select(
                [
                    DB::raw('compra_item_minuta_empenho.id as cime_id'),
                    'compra_item_minuta_empenho.compra_item_id',
                    'compra_item_minuta_empenho.operacao_id',
                    'compra_item_fornecedor.fornecedor_id',
                    'compra_item_fornecedor.id as cif_id',
                    'compra_items.numero as numero_item',
                    'tipo_compra.descricao as tipo_compra_descricao',
                    'codigoitens.descricao',
                    'catmatseritens.descricao AS catmatser_desc',
                    DB::raw('SUBSTRING(catmatseritens.descricao FOR 50) AS catmatser_desc_simplificado'),
                    'compra_items.catmatseritem_id',
                    'compra_items.descricaodetalhada',
                    'catmatseritens.codigo_siasg',
                    DB::raw("SUBSTRING(compra_items.descricaodetalhada for 50) AS descricaosimplificada"),
                    'compra_item_unidade.quantidade_saldo as qtd_item',
                    'compra_item_unidade.id as ciu_id',
                    DB::raw("CASE
                                WHEN (
                                    compra_items.criterio_julgamento = 'V'
                                    OR compra_items.criterio_julgamento is null
                                    OR mod.descres = '99'
                                    OR mod.descres = '06'
                                ) THEN compra_item_fornecedor.valor_unitario
                                ELSE compra_item_fornecedor.valor_unitario *
                                    ((100 - compra_item_fornecedor.percentual_maior_desconto) / 100)
                                END                                           AS valorunitario"),
                    'naturezadespesa.codigo as natureza_despesa',
                    'naturezadespesa.id as natureza_despesa_id',
                    DB::raw("CASE
                                WHEN (
                                    compra_items.criterio_julgamento = 'V'
                                    OR compra_items.criterio_julgamento is null
                                    OR mod.descres = '99'
                                    OR mod.descres = '06'
                                ) THEN compra_item_fornecedor.valor_negociado
                                ELSE compra_item_fornecedor.valor_negociado *
                                     ((100 - compra_item_fornecedor.percentual_maior_desconto) / 100)
                                END                                           AS valortotal"),
                    'saldo_contabil.saldo',
                    'saldo_contabil.id as saldo_id',
                    'compra_item_minuta_empenho.subelemento_id',
                    DB::raw('left(minutaempenhos.mensagem_siafi, 4) as exercicio'),
                    'compra_item_minuta_empenho.numseq',
                    DB::raw('compra_item_minuta_empenho.id AS "numeroLinha"'),
                    'compra_items.criterio_julgamento',
                    'compra_item_fornecedor.percentual_maior_desconto',
                    DB::raw("compra_item_fornecedor.valor_unitario * ((100-compra_item_fornecedor.percentual_maior_desconto)/100) as valor_unitario_desconto"),
                    DB::raw("compra_item_fornecedor.valor_negociado * ((100-compra_item_fornecedor.percentual_maior_desconto)/100) as valor_negociado_desconto"),
//                            DB::raw("CASE
//                                WHEN (
//                                    compra_items.situacao = false
//                                    OR compra_item_fornecedor.situacao = false
//                                    OR compra_item_unidade.situacao = false
//                                ) THEN false
//                                ELSE true
//                                END AS verificacao_situacao"), #removendo bloco de código pela issue 796

                ]
            )
            ->orderBy('compra_item_minuta_empenho.numseq', 'asc');

        $itens = $this->setCondicaoFornecedor(
            $minutaEmpenho,
            $itens,
            $minutaEmpenho->empenho_por,
            $fornecedor_id,
            $fornecedor_compra_id
        );

        $soma = CompraItemMinutaEmpenho::select([
            'compra_item_id',
            DB::raw("sum(compra_item_minuta_empenho.quantidade) as qtd_total_item"),
            DB::raw("sum(compra_item_minuta_empenho.valor) as vlr_total_item"),
        ])
            ->where('operacao_id', 220)
            ->where('minutaempenho_id', $minutaEmpenho->id)
            ->groupBy('compra_item_id');

        //CREATE
        if (is_null(session('remessa_id'))) {
            $itens->where('minutaempenhos_remessa.remessa', 0);
            $itens->addSelect([
                DB::raw("0 AS quantidade"),
                DB::raw("0 AS valor"),
            ]);

            return $this->retornarArray($itens->get()->toArray(), $soma->get()->toArray(), 'compra_item_id');
        }

        //UPDATE
        $itens->addSelect([
            'compra_item_minuta_empenho.quantidade',
            'compra_item_minuta_empenho.valor',
        ]);
        $itens->where('compra_item_minuta_empenho.minutaempenhos_remessa_id', session('remessa_id'));

        $soma->where('minutaempenhos_remessa_id', '<>', session('remessa_id'));

        return $this->retornarArray($itens->get()->toArray(), $soma->get()->toArray(), 'compra_item_id');

    }
}
