<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Models\Codigoitem;
use App\Models\Contrato;

class AlterContratosJustificativaContratoInativoId extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Log::info("******************* up - migration - AlterContratosJustificativaContratoInativoId ****************");

        $arrayContratosInativosComTermoDeRescisao = Contrato::where('contratos.situacao', false)
            ->select('contratos.*')
            ->join('contratohistorico as h', 'h.contrato_id', 'contratos.id')
            ->join('codigoitens as ci', 'ci.id', 'h.tipo_id')
            ->where('ci.descricao', 'Termo de Rescisão')
            ->get();
        \Log::info(" qtd contratos encontrados = ".count($arrayContratosInativosComTermoDeRescisao));

        // buscar o id do codigoitem
        $idCodigoItemRescindido = Codigoitem::where('codigoitens.descricao', 'Rescindido')
            ->select('codigoitens.id')
            ->join('codigos', 'codigos.id', 'codigoitens.codigo_id')
            ->where('codigos.descricao', 'Justificativa Contrato Inativo')
            ->first()
            ->id;

        foreach($arrayContratosInativosComTermoDeRescisao as $objContratoInativo){
            \Log::info(" atualizando contrato id = ".$objContratoInativo->id);

            DB::table('contratos')
                ->where('id', $objContratoInativo->id)
                ->update(
                    ['justificativa_contrato_inativo_id' => $idCodigoItemRescindido]
            );
            \Log::info(" ok! ");
            \Log::info("******************* fim up - migration - AlterContratosJustificativaContratoInativoId ****************");
        }
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Log::info("******************* down - migration - AlterContratosJustificativaContratoInativoId ****************");
        $arrayContratosInativosComTermoDeRescisao = Contrato::where('contratos.situacao', false)
            ->select('contratos.*')
            ->join('contratohistorico as h', 'h.contrato_id', 'contratos.id')
            ->join('codigoitens as ci', 'ci.id', 'h.tipo_id')
            ->where('ci.descricao', 'Termo de Rescisão')
            ->get();
        foreach($arrayContratosInativosComTermoDeRescisao as $objContratoInativo){
            DB::table('contratos')
                ->where('id', $objContratoInativo->id)
                ->update(
                    ['justificativa_contrato_inativo_id' => null]
            );
            \Log::info("******************* fim down - migration - AlterContratosJustificativaContratoInativoId ****************");

            // $objContratoInativo->justificativa_contrato_inativo = null;
            // $objContratoInativo->save();
        }
    }
}
