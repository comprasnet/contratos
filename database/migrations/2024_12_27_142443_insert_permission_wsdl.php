<?php

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertPermissionWsdl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        app()['cache']->forget('spatie.permission.cache');

        $permission = Permission::where('name','wsdl_listar')->first();
        if(!isset($permission->id)){
            Permission::create([
                'name' => 'wsdl_listar',
                'guard_name' => 'web'
            ]);
            $role = Role::where('name', 'Administrador')->first();
            $role->givePermissionTo('wsdl_listar');
        }

        $permission = Permission::where('name','wsdl_atualizar')->first();
        if(!isset($permission->id)){
            Permission::create([
                'name' => 'wsdl_atualizar',
                'guard_name' => 'web'
            ]);
            $role = Role::where('name', 'Administrador')->first();
            $role->givePermissionTo('wsdl_atualizar');
        }

        $permission = Permission::where('name','wsdl_baixar')->first();
        if(!isset($permission->id)){
            Permission::create([
                'name' => 'wsdl_baixar',
                'guard_name' => 'web'
            ]);
            $role = Role::where('name', 'Administrador')->first();
            $role->givePermissionTo('wsdl_baixar');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $role = Role::where(['name' => 'Administrador'])->first();
        $role->revokePermissionTo('wsdl_listar');
        $role->revokePermissionTo('wsdl_atualizar');
        $role->revokePermissionTo('wsdl_baixar');

        Permission::where(['name' => 'wsdl_listar'])->forceDelete();
        Permission::where(['name' => 'wsdl_atualizar'])->forceDelete();
        Permission::where(['name' => 'wsdl_baixar'])->forceDelete();
    }
}
