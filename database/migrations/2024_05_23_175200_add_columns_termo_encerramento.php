<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 *@TODO A migration cria novos campos referente ao Termo de Encerramento, os campos foram criados na tabela 'contratos'
 *@TODO devido a urgencia da MPRS por conta dos outros campos do Termo se encontrarem nela. Necessário retirar os campos
 *@TODO referentes ao Termo de Encerramento da tabela contratos.
 */
class AddColumnsTermoEncerramento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contratos', function (Blueprint $table) {
            $table->decimal('te_valor_total_executado',17,2)->nullable();
            $table->decimal('te_valor_total_pago',17,2)->nullable();
            $table->decimal('te_saldo_disponivel_ou_bloqueado',17,2)->default(0)->nullable();
            $table->text('te_justificativa_nao_cumprimento')->nullable();
            $table->decimal('te_saldo_bloqueado_garantia_contratual',17,2)->default(0)->nullable();
            $table->text('te_justificativa_bloqueio_garantia_contratual')->nullable();
            $table->decimal('te_saldo_bloqueado_conta_deposito',17,2)->default(0)->nullable();
            $table->text('te_justificativa_bloqueio_conta_deposito')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contratos', function (Blueprint $table) {
            $table->dropColumn('te_valor_total_executado');
            $table->dropColumn('te_valor_total_pago');
            $table->dropColumn('te_saldo_disponivel_ou_bloqueado');
            $table->dropColumn('te_justificativa_nao_cumprimento');
            $table->dropColumn('te_saldo_bloqueado_garantia_contratual');
            $table->dropColumn('te_justificativa_bloqueio_garantia_contratual');
            $table->dropColumn('te_saldo_bloqueado_conta_deposito');
            $table->dropColumn('te_justificativa_bloqueio_conta_deposito');
        });
    }
}
