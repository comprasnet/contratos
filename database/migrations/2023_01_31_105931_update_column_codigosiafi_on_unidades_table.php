
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use App\Models\Unidade;

class UpdateColumnCodigosiafiOnUnidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //preenche as unidades com campo codifosiafi vazios com valor de codigo
        $unidades = Unidade::select('id', 'codigo')
            ->where('codigosiafi', '=',  null)
            ->get()->toArray();

        foreach ($unidades as $unidade) {
                DB::table('unidades')
                ->where('id', $unidade['id'])
                ->update(
                    ['codigosiafi' => $unidade['codigo']]
                );
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}