<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConfirmardadosDeducaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('sfdeducao', 'confirma_dados')) {
            Schema::table('sfdeducao', function (Blueprint $table) {
                $table->boolean('confirma_dados')->default(false);
            });
        }

        if (!Schema::hasColumn('sfpco', 'confirma_dados')) {
            Schema::table('sfpco', function (Blueprint $table) {
                $table->boolean('confirma_dados')->default(false);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('sfdeducao', 'confirma_dados')) {
            Schema::table('sfdeducao', function (Blueprint $table) {
                $table->dropColumn('confirma_dados');
            });
        }

        if (Schema::hasColumn('sfpco', 'confirma_dados')) {
            Schema::table('sfpco', function (Blueprint $table) {
                $table->dropColumn('confirma_dados');
            });
        }
    }
}
