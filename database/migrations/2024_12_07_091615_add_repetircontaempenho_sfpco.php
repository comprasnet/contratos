<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRepetircontaempenhoSfpco extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sfpco', function (Blueprint $table) {
            $table->boolean('repetir_conta_empenho')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sfpco', function (Blueprint $table) {
            $table->dropColumn('repetir_conta_empenho');
        });
    }
}
