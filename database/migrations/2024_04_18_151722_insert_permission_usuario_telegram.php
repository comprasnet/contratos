<?php

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertPermissionUsuarioTelegram extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        app()['cache']->forget('spatie.permission.cache');

        $permission = Permission::where('name','usuario_telegram')->first();

        if(!isset($permission->id)){
            Permission::create([
                'name' => 'usuario_telegram',
                'guard_name' => 'web'
            ]);
            $role = Role::where('name', 'Administrador')->first();
            $role->givePermissionTo('usuario_telegram');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $role = Role::where(['name' => 'Administrador'])->first();
        $role->revokePermissionTo('usuario_telegram');

        Permission::where(['name' => 'usuario_telegram'])->forceDelete();
    }
}
