<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertCodigoitensSituacaoAntecipagov extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       
       //Busca o código na tabela principal
        $codigo = \App\Models\Codigo::where([
            'descricao' => 'Situacao Antecipa Gov',
            'visivel' => true
        ])->first();

        //Adiciona a operacao ALTERADO na tabela secundária
        \App\Models\Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'ALT',
            'descricao' => 'Alterado',
            'visivel' => true
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $codigo = \App\Models\Codigo::where('descricao', 'Situacao Antecipa Gov')->first();

        \App\Models\Codigoitem::where([
            'codigo_id' => $codigo->id,
            'descres' => 'ALT',
            'descricao' => 'Alterado'
        ])->forceDelete();

    }
}
