<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnidadegestoraUnidadesexecutorasfinanceirasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unidadegestora_unidadesexecutorasfinanceiras', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('unidade_gestora_id')->unsigned()->index();
            $table->integer('unidade_executora_financeira_id')->unsigned()->index();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('unidade_executora_financeira_id')->references('id')->on('unidades')->onDelete('cascade');
            $table->foreign('unidade_gestora_id')->references('id')->on('unidades')->onDelete('cascade');
            $table->unique(['unidade_gestora_id', 'unidade_executora_financeira_id'], 'idx_gestora_executora');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unidadegestora_unidadesexecutorasfinanceiras');
    }
}
