<?php

use App\Http\Traits\BuscaCodigoItens;
use App\Models\Codigoitem;
use App\Models\Execsfsituacao;
use App\Models\ExecsfsituacaoCamposVariaveis;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertSituacoesExecsituacaodeducao extends Migration
{
    use BuscaCodigoItens;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tipo_situacao = Codigoitem::whereHas('codigo', function ($c) {
            $c->where('descricao', 'Tipos Situações DH SIAFI');
        })
            ->where('descres', 'SIT0007')
            ->first();

        $situacao = Execsfsituacao::updateOrCreate(
            ['codigo' => 'DDF009'],
            [
                'descricao' => 'IMPOSTO DE RENDA RETIDO NA FONTE',
                'aba' => 'DEDUCAO',
                'tipo_situacao' => $tipo_situacao->id,
                'afeta_custo' => false,
                'permite_contrato' => false,
                'darf_numerado_decomposto' => true
            ]
        );

        ExecsfsituacaoCamposVariaveis::updateOrCreate(
            [
                'execsfsituacao_id' => $situacao->id,
                'campo' => 'txtinscra'
            ],
            [
                'aba' => 'DEDUCAO',
                'campo' => 'txtinscra',
                'tipo' => 'Código de Recolhimento DARF',
                'rotulo' => 'Código de Recolhimento DARF',
                'restricao' => 'Sem Restrição'
            ]
        );

        ExecsfsituacaoCamposVariaveis::updateOrCreate(
            [
                'execsfsituacao_id' => $situacao->id,
                'campo' => 'numclassa'
            ],
            [
                'aba' => 'DEDUCAO',
                'campo' => 'numclassa',
                'tipo' => 'Classificação Contábil',
                'mascara' => '3424XXX00',
                'rotulo' => 'VPD de Multa ou Encargos Tributários',
                'restricao' => 'Sem Restrição'
            ]
        );

        // DDF021
        $situacao1 = Execsfsituacao::updateOrCreate(
            ['codigo' => 'DDF021'],
            [
                'descricao' => 'RETENÇÃO PREVIDENCIÁRIA RECOLHIDA POR DARF NUMERADO',
                'aba' => 'DEDUCAO',
                'tipo_situacao' => $tipo_situacao->id,
                'afeta_custo' => false,
                'permite_contrato' => false,
                'darf_numerado_decomposto' => true
            ]
        );

        ExecsfsituacaoCamposVariaveis::updateOrCreate(
            [
                'execsfsituacao_id' => $situacao1->id,
                'campo' => 'txtinscra'
            ],
            [
                'aba' => 'DEDUCAO',
                'campo' => 'txtinscra',
                'tipo' => 'Código de Recolhimento DARF',
                'rotulo' => 'Código de Recolhimento DARF',
                'restricao' => 'Sem Restrição'
            ]
        );

        ExecsfsituacaoCamposVariaveis::updateOrCreate(
            [
                'execsfsituacao_id' => $situacao1->id,
                'campo' => 'numclassa'
            ],
            [
                'aba' => 'DEDUCAO',
                'campo' => 'numclassa',
                'tipo' => 'Classificação Contábil',
                'mascara' => '3424XXX00',
                'rotulo' => 'VPD de Multa ou Encargos Tributários',
                'restricao' => 'Sem Restrição'
            ]
        );
        //Sit DDF025
        $situacao2 = Execsfsituacao::updateOrCreate(
            ['codigo' => 'DDF025'],
            [
                'descricao' => 'RETENÇÃO IMPOSTOS E CONTRIBUIÇÕES',
                'aba' => 'DEDUCAO',
                'tipo_situacao' => $tipo_situacao->id,
                'afeta_custo' => false,
                'permite_contrato' => false,
                'darf_numerado_decomposto' => true
            ]
        );

        ExecsfsituacaoCamposVariaveis::updateOrCreate(
            [
                'execsfsituacao_id' => $situacao2->id,
                'campo' => 'txtinscra'
            ],
            [
                'aba' => 'DEDUCAO',
                'campo' => 'txtinscra',
                'tipo' => 'Código de Recolhimento DARF',
                'rotulo' => 'Código de Recolhimento DARF',
                'restricao' => 'Sem Restrição'
            ]
        );

        ExecsfsituacaoCamposVariaveis::updateOrCreate(
            [
                'execsfsituacao_id' => $situacao2->id,
                'campo' => 'txtinscrb'
            ],
            [
                'aba' => 'DEDUCAO',
                'campo' => 'txtinscrb',
                'tipo' => 'Tipo de Dado Genérico 01',
                'rotulo' => 'Natureza de Rendimento',
                'mascara' => '17XXX',
                'restricao' => 'Sem Restrição'
            ]
        );

        ExecsfsituacaoCamposVariaveis::updateOrCreate(
            [
                'execsfsituacao_id' => $situacao2->id,
                'campo' => 'numclassa'
            ],
            [
                'aba' => 'DEDUCAO',
                'campo' => 'numclassa',
                'tipo' => 'Classificação Contábil',
                'mascara' => '3424XXX00',
                'rotulo' => 'VPD de Multa ou Encargos Tributários',
                'restricao' => 'Sem Restrição'
            ]
        );

        $codNp = $this->retornaIdCodigoItemPorDescricaoEDescres('Tipos DH SIAFI', 'NP');

        $situacao->tipodocs()->sync([$codNp]);
        $situacao1->tipodocs()->sync([$codNp]);
        $situacao2->tipodocs()->sync([$codNp]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
