<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertJustificativasIn772022 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        \App\Models\Justificativafatura::create([
            'nome' => 'Art. 9º inciso I',
            'descricao' => 'grave perturbação da ordem, situação de emergência ou calamidade pública',
            'situacao' => true
        ]);

        \App\Models\Justificativafatura::create([
            'nome' => 'Art. 9º inciso II',
            'descricao' => 'pagamento a microempresa, empresa de pequeno porte[...] (conforme IN nº 77/2022)',
            'situacao' => true
        ]);

        \App\Models\Justificativafatura::create([
            'nome' => 'Art. 9º inciso III',
            'descricao' => 'pagamento de serviços necessários ao funcionamento dos sistemas estruturantes[...] (conforme IN nº 77/2022)',
            'situacao' => true
        ]);

        \App\Models\Justificativafatura::create([
            'nome' => 'Art. 9º inciso IV',
            'descricao' => 'pagamento de direitos oriundos de contratos em caso de falência[...] (conforme IN nº 77/2022)',
            'situacao' => true
        ]);

        \App\Models\Justificativafatura::create([
            'nome' => 'Art. 9º inciso V',
            'descricao' => 'pagamento de contrato cujo objeto seja imprescindível para assegurar a integridade [...] (conforme IN nº 77/2022)',
            'situacao' => true
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        \App\Models\Justificativafatura::where([
            'nome' => 'Art. 9º inciso I',
            'situacao' => true
        ])->forceDelete();

        \App\Models\Justificativafatura::where([
            'nome' => 'Art. 9º inciso II',
            'situacao' => true
        ])->forceDelete();

        \App\Models\Justificativafatura::where([
            'nome' => 'Art. 9º inciso III',
            'situacao' => true
        ])->forceDelete();

        \App\Models\Justificativafatura::where([
            'nome' => 'Art. 9º inciso IV',
            'situacao' => true
        ])->forceDelete();

        \App\Models\Justificativafatura::where([
            'nome' => 'Art. 9º inciso V',
            'situacao' => true
        ])->forceDelete();

    }
}
