<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Codigo;
use App\Models\Codigoitem;


class InsertCodigoitensAnulacaoIrrisorio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = Codigo::where([
            'descricao' => 'Operação item empenho'
        ])->first();

        Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'IRRISORIO',
            'descricao' => 'ANULAÇÃO SALDO IRRISÓRIO'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Codigoitem::where([
            'descricao' => 'ANULAÇÃO SALDO IRRISÓRIO'
        ])->forceDelete();
    }
}
