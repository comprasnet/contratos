<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContratoAutoridadeSignatariaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contrato_autoridade_signataria', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('contratohistorico_id')->nullable();
            $table->foreign('contratohistorico_id')->references('id')->on('contratohistorico');

            $table->integer('autoridadesignataria_id');
            $table->foreign('autoridadesignataria_id')->references('id')->on('autoridadesignataria');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contrato_autoridade_signataria');
    }
}
