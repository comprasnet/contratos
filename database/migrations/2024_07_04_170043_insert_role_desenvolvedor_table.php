<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Role;

class InsertRoleDesenvolvedorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        app()['cache']->forget('spatie.permission.cache');

        if (!Role::where('name', 'Desenvolvedor')->first()) {
            $role = Role::create(['name' => 'Desenvolvedor']);
            $role->givePermissionTo('log_visualizar');
            $role->givePermissionTo('activitylog_consultar');
            $role->givePermissionTo('autorizacaoexecucao_visualizar');
            $role->givePermissionTo('contrato_publicacao_consultar');
            $role->givePermissionTo('contrato_publicacao_show');
            $role->givePermissionTo('contrato_arquivos_pncp_visualizar');
            $role->givePermissionTo('arp_visualizar');
            $role->givePermissionTo('arp_remanejamento_visualizar');
            $role->givePermissionTo('arp_adesao_visualizar');
            $role->givePermissionTo('contrato_unidadedescentralizada_show');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $role = Role::where(['name' => 'Desenvolvedor'])->first();
        $role->revokePermissionTo('log_visualizar');
        $role->revokePermissionTo('activitylog_consultar');
        $role->revokePermissionTo('autorizacaoexecucao_visualizar');
        $role->revokePermissionTo('contrato_publicacao_consultar');
        $role->revokePermissionTo('contrato_publicacao_show');
        $role->revokePermissionTo('contrato_arquivos_pncp_visualizar');
        $role->revokePermissionTo('arp_visualizar');
        $role->revokePermissionTo('arp_remanejamento_visualizar');
        $role->revokePermissionTo('arp_adesao_visualizar');
        $role->revokePermissionTo('contrato_unidadedescentralizada_show');

        $role->forceDelete();
    }
}
