<?php

use App\Models\ApropriacaoContratoFaturas;
use App\Models\ApropriacaoFaturas;
use App\Models\Contratofatura;
use App\Models\ContratoFaturaEmpenhosApropriacao;
use App\Models\SfCentroCusto;
use App\Models\SfDadosBasicos;
use App\Models\SfDadosPgto;
use App\Models\SfDocOrigem;
use App\Models\SfPadrao;
use App\Models\SfPco;
use App\Models\Sfrelitemvlrcc;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveApropriacaoandamento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $resultados = ApropriacaoContratoFaturas::join(
            'contratofaturas',
            'apropriacoes_faturas_contratofaturas.contratofaturas_id',
            '=',
            'contratofaturas.id'
        )
            ->where('contratofaturas.situacao', 'AND')
            ->select(['apropriacoes_faturas_contratofaturas.apropriacoes_faturas_id', 'apropriacoes_faturas_contratofaturas.sfpadrao_id'])
            ->groupBy(['apropriacoes_faturas_contratofaturas.apropriacoes_faturas_id', 'apropriacoes_faturas_contratofaturas.sfpadrao_id'])
            ->havingRaw('COUNT(apropriacoes_faturas_contratofaturas.apropriacoes_faturas_id) > 1')
            ->get();

        foreach ($resultados as $resultado) {
            $sf_padrao_id = $resultado->sfpadrao_id;
            $apropriacao_id = $resultado->apropriacoes_faturas_id;
            try {
                DB::beginTransaction();
                $apropriacaoFaturas = ApropriacaoFaturas::where('id', $apropriacao_id)->first();


                //$idFirstContratoFatura = $apropriacaoFaturas->faturas()->first()->id;

                #seta situacao das faturas para pendente e sfpadrao_id das faturas para null
                foreach ($apropriacaoFaturas->faturas()->get() as $contratoFaturas) {
                    $contratoFaturas->situacao = 'PEN';
                    $contratoFaturas->sfpadrao_id = null;
                    $contratoFaturas->save();

                    ContratoFaturaEmpenhosApropriacao::where('contratofatura_id', $contratoFaturas->id)->delete();
                }

                $apropriacaoFaturas->faturas()->detach(); //faturas é a referencia para apropriacoes_faturas_contratofaturas
                $apropriacaoFaturas->forceDelete();

                #recupera models das tabelas envolvidas na apropriacao de faturas
                $sfPadrao = SfPadrao::find($sf_padrao_id);
                $sfDadosBasicos = SfDadosBasicos::where('sfpadrao_id', $sf_padrao_id)->first();
                $sfDocOrigem = SfDocOrigem::where('sfdadosbasicos_id', $sfDadosBasicos->id);
                $sfPco = SfPco::where('sfpadrao_id', $sf_padrao_id);
                $sfDadosPagamento = SfDadosPgto::where('sfpadrao_id', $sf_padrao_id);

                #delete nas tabelas envolvidas na apropriacao de faturas
                $sfCentroCusto = SfCentroCusto::where('sfpadrao_id', $sf_padrao_id)->get();

                foreach ($sfCentroCusto as $centro_custo_current) {
                    $arrSfrelitemvlrcc = Sfrelitemvlrcc::where('sfcc_id', $centro_custo_current->id)->pluck('id')->toArray();

                    Sfrelitemvlrcc::whereIn('id', $arrSfrelitemvlrcc)->forceDelete();

                    $centro_custo_current->forceDelete();
                }

                $sfDocOrigem->forceDelete();
                $sfDadosBasicos->forceDelete();
                $sfPco->forceDelete();
                $sfDadosPagamento->forceDelete();
                $sfPadrao->delete();

                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                \Illuminate\Support\Facades\Log::info('Erro ao excluir apropriacao_id ' . $apropriacao_id . ' sfpadrao ' . $sf_padrao_id);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
