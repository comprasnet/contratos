<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlteraCodugemitVarcharSfpadraopcodadosbasicos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sfpadrao', function (Blueprint $table) {
            $table->string('codugemit', 20)->change();
        });

        Schema::table('sfpco', function (Blueprint $table) {
            $table->string('codugempe', 20)->change();
        });

        Schema::table('sfdadosbasicos', function (Blueprint $table) {
            $table->string('codugpgto', 20)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sfpadrao', function (Blueprint $table) {
            $table->integer('codugemit')->change();
        });

        Schema::table('sfpco', function (Blueprint $table) {
            $table->integer('codugempe')->change();
        });

        Schema::table('sfdadosbasicos', function (Blueprint $table) {
            $table->integer('codugpgto')->change();
        });
    }
}
