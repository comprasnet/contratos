<?php

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertPermissionBtnRedirectCompraConsultar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        app()['cache']->forget('spatie.permission.cache');

        $permission = Permission::where('name','compra_redirecionar')->first();

        if(!isset($permission->id)){
            Permission::create([
                'name' => 'compra_redirecionar',
                'guard_name' => 'web'
            ]);
            $role = Role::where('name', 'Administrador')->first();
            $role->givePermissionTo('compra_redirecionar');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $role = Role::where(['name' => 'Administrador'])->first();
        $role->revokePermissionTo('compra_redirecionar');

        Permission::where(['name' => 'compra_redirecionar'])->forceDelete();
    }
}
