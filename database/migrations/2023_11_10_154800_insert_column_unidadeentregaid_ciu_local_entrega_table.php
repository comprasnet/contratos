<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

//class InsertColumnUnidadeentregaidCiuLocalEntragaTable extends Migration
class InsertColumnUnidadeentregaidCiuLocalEntregaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('compra_item_unidade_local_entrega','unidade_entrega_id')) {
            Schema::table('compra_item_unidade_local_entrega', function (Blueprint $table) {
                $table->bigInteger('unidade_entrega_id')
                    ->references('id')
                    ->on('unidades')
                    ->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('compra_item_unidade_local_entrega', function(Blueprint $table) {
                $table->dropForeign(['unidade_entrega_id']);
                $table->dropColumn('unidade_entrega_id');
        });
    }
}
