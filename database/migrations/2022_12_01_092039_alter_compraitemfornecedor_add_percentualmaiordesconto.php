<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCompraitemfornecedorAddPercentualmaiordesconto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('compra_item_fornecedor', function (Blueprint $table) {
            $table->decimal('percentual_maior_desconto', 7,4)
                ->nullable()->default(null)
                ->comment('Percentual de desconto a ser aplicado no campo valor unitario do item');
        });

    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('compra_item_fornecedor', function (Blueprint $table) {
            $table->dropColumn('percentual_maior_desconto');
        });
    }
}
