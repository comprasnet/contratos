<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \App\Models\EnviaDadosPncp;
use \App\Models\Codigoitem;

class FixSituacaoInexistentePncpCompraNaoEncontrada extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $enviaDadosPncpSituacaoErrada = EnviaDadosPncp::whereHas('status', function ($query) {
            $query->where('descres', '4110-05');
        })->get();

        $situacaoIncpenPncp =  Codigoitem::where('descres', 'INCPEN')->first();

        foreach ($enviaDadosPncpSituacaoErrada as $enviaDados) {
            $enviaDados->situacao = $situacaoIncpenPncp->id;
            $enviaDados->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
