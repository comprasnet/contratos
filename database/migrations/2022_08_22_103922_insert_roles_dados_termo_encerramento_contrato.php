<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class InsertRolesDadosTermoEncerramentoContrato extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        app()['cache']->forget('spatie.permission.cache');

        // acesso aos modulos
        if(!Permission::where('name', 'contratoencerramento_inserir')->get()->count()){Permission::create(['name' => 'contratoencerramento_inserir']);}
        if(!Permission::where('name', 'contratoencerramento_editar')->get()->count()){Permission::create(['name' => 'contratoencerramento_editar']);}
        if(!Permission::where('name', 'contratoencerramento_deletar')->get()->count()){Permission::create(['name' => 'contratoencerramento_deletar']);}

        $role = Role::where('name','Administrador')->first();
        $role->givePermissionTo('contratoencerramento_inserir');
        $role->givePermissionTo('contratoencerramento_editar');
        $role->givePermissionTo('contratoencerramento_deletar');

        $role = Role::where('name','Administrador Órgão')->first();
        $role->givePermissionTo('contratoencerramento_inserir');
        $role->givePermissionTo('contratoencerramento_editar');
        $role->givePermissionTo('contratoencerramento_deletar');

        $role = Role::where('name','Administrador Unidade')->first();
        $role->givePermissionTo('contratoencerramento_inserir');
        $role->givePermissionTo('contratoencerramento_editar');
        $role->givePermissionTo('contratoencerramento_deletar');

        $role = Role::where('name','Responsável por Contrato')->first();
        $role->givePermissionTo('contratoencerramento_inserir');
        $role->givePermissionTo('contratoencerramento_editar');
        $role->givePermissionTo('contratoencerramento_deletar');

        $role = Role::where('name','Setor Contratos')->first();
        $role->givePermissionTo('contratoencerramento_inserir');
        $role->givePermissionTo('contratoencerramento_editar');
        $role->givePermissionTo('contratoencerramento_deletar');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $role = Role::where(['name' => 'Administrador'])->first();
        $role->revokePermissionTo('contratoencerramento_inserir');
        $role->revokePermissionTo('contratoencerramento_editar');
        $role->revokePermissionTo('contratoencerramento_deletar');

        $role = Role::where(['name' => 'Administrador Órgão'])->first();
        $role->revokePermissionTo('contratoencerramento_inserir');
        $role->revokePermissionTo('contratoencerramento_editar');
        $role->revokePermissionTo('contratoencerramento_deletar');

        $role = Role::where(['name' => 'Administrador Unidade'])->first();
        $role->revokePermissionTo('contratoencerramento_inserir');
        $role->revokePermissionTo('contratoencerramento_editar');
        $role->revokePermissionTo('contratoencerramento_deletar');

        $role = Role::where(['name' => 'Responsável por Contrato'])->first();
        $role->revokePermissionTo('contratoencerramento_inserir');
        $role->revokePermissionTo('contratoencerramento_editar');
        $role->revokePermissionTo('contratoencerramento_deletar');

        $role = Role::where(['name' => 'Setor Contratos'])->first();
        $role->revokePermissionTo('contratoencerramento_inserir');
        $role->revokePermissionTo('contratoencerramento_editar');
        $role->revokePermissionTo('contratoencerramento_deletar');

        Permission::where(['name' => 'contratoencerramento_inserir'])->delete();
        Permission::where(['name' => 'contratoencerramento_editar'])->delete();
        Permission::where(['name' => 'contratoencerramento_deletar'])->forceDelete();

    }
}
