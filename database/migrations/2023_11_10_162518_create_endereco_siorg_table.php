<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnderecoSiorgTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('endereco_siorg', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo_siorg')->index();
            $table->string('cep');
            $table->string('logradouro');
            $table->string('numero')->default(0);
            $table->string('complemento')->nullable();
            $table->string('bairro');
            $table->string('municipio');
            $table->string('uf');
            $table->string('pais');
            $table->string('horario_funcionamento')->nullable();
            $table->string('tipo_endereco')->nullable();
            $table->timestamps();

            $table->foreign('codigo_siorg')->references('codigo')->on('siorg')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('endereco_siorg', function (Blueprint $table) {
            $table->dropForeign(['codigo_siorg']);
        });

        Schema::dropIfExists('endereco_siorg');
    }
}
