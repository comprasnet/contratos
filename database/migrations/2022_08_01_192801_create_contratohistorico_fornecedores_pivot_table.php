<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContratohistoricoFornecedoresPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contratohistorico_fornecedores_pivot', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contratohistorico_id')->unsigned();
            $table->integer('fornecedor_id')->unsigned();

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('contratohistorico_id')->references('id')->on('contratohistorico')->onDelete('cascade');
            $table->foreign('fornecedor_id')->references('id')->on('fornecedores')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contratohistorico_fornecedores_pivot');
    }
}
