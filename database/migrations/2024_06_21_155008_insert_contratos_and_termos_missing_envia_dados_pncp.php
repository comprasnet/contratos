<?php

use App\Models\Contratohistorico;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;
use App\Http\Traits\EnviaPncpTrait;

class InsertContratosAndTermosMissingEnviaDadosPncp extends Migration
{
    use EnviaPncpTrait;

    protected const PNCPABLE_TYPE = 'App\Models\Contratohistorico';

    public function up(): void
    {
        Schema::table('envia_dados_pncp', function (Blueprint $table) {

            $contratosHistoricos = ContratoHistorico::select(
                'contratohistorico.created_at',
                'contratohistorico.id as contratohistorico_id',
                'contratohistorico.contrato_id',
                'codigoitens.descricao as tipo_contrato',
                'envia_dados_pncp.id'
            )
                ->join('contratos', 'contratos.id', '=', 'contratohistorico.contrato_id')
                ->join('amparo_legal_contrato', 'contratos.id', '=', 'amparo_legal_contrato.contrato_id')
                ->join('amparo_legal', function ($join) {
                    $join->on('amparo_legal.id', '=', 'amparo_legal_contrato.amparo_legal_id')
                        ->where('amparo_legal.complemento_14133', '=', true);
                })
                ->leftJoin('envia_dados_pncp', function ($join) {
                    $join->on('contratohistorico.id', '=', 'envia_dados_pncp.pncpable_id')
                        ->where('envia_dados_pncp.pncpable_type', '=', self::PNCPABLE_TYPE);
                })
                ->join('codigoitens', 'codigoitens.id', '=', 'contratohistorico.tipo_id')
                ->whereNull('envia_dados_pncp.id')
                ->distinct()
                ->orderBy('contratohistorico.created_at', 'asc')
                ->get();

            foreach ($contratosHistoricos as $contratoHistorico) {
                $this->preparaEnvioPNCP(
                    $contratoHistorico->contratohistorico_id,
                    $contratoHistorico->contrato_id,
                    $contratoHistorico->tipo_contrato,
                    false
                );
            }
        });
    }

    public function down(): void
    {
        Schema::table('envia_dados_pncp', function (Blueprint $table) {
            //
        });
    }
}
