<?php

use App\Models\Fornecedor;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AtualizarPorteMeiFornecedorSicaf extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $fornecedores = Fornecedor::whereHas('fornecedorSicaf')->get();
        foreach ($fornecedores as $fornecedor){
           \App\Jobs\AtualizaFornecedorSicafJob::dispatch($fornecedor);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
