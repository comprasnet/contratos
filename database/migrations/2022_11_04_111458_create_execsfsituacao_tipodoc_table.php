<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExecsfsituacaoTipodocTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('execsfsituacao_tipodoc', function (Blueprint $table) {
            $table->integer('execsfsituacao_id')->unsigned();
            $table->integer('tipodoc_id')->unsigned();

            $table->foreign('execsfsituacao_id')->references('id')->on('execsfsituacao')->onDelete('cascade');
            $table->foreign('tipodoc_id')->references('id')->on('codigoitens')->onDelete('cascade');

            $table->primary(['execsfsituacao_id','tipodoc_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('execsfsituacao_tipodoc');
    }
}
