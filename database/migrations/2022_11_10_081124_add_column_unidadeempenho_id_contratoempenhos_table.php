<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnUnidadeempenhoIdContratoempenhosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contratoempenhos', function (Blueprint $table) {
            $table->integer('unidadeempenho_id')->nullable();

            $table->foreign('unidadeempenho_id')->references('id')->on('unidades')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contratoempenhos', function (Blueprint $table) {
            $table->dropColumn('unidadeempenho_id');
        });
    }
}
