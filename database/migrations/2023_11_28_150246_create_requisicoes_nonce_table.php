<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequisicoesNonceTable extends Migration
{
    public function up()
    {
        Schema::create('requisicoes_nonce', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('nonce_id');
            $table->foreign('nonce_id')
                ->references('id')
                ->on('nonce')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->json('requisicao_enviada');
            $table->json('resposta_api');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('requisicoes_nonce');
    }
}
