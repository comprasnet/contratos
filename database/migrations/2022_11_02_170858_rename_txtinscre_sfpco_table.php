<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameTxtinscreSfpcoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sfpco', function(Blueprint $table) {
            $table->renameColumn('txttnscre', 'txtinscre');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sfpco', function(Blueprint $table) {
            $table->renameColumn('txtinscre', 'txttnscre');
        });
    }
}
