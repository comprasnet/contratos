<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnTermoEncerramentoContratosConsecucaoObjetivosContratacao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('contratos', 'consecucao_objetivos_contratacao')){
            Schema::table('contratos', function (Blueprint $table) {
                $table->text('consecucao_objetivos_contratacao')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('contratos', 'consecucao_objetivos_contratacao')){
            Schema::table('contratos', function (Blueprint $table) {
                $table->dropColumn('consecucao_objetivos_contratacao');
            });
        }
    }
}
