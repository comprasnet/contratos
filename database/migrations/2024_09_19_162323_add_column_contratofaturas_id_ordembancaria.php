<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnContratofaturasIdOrdembancaria extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ordens_bancarias', function (Blueprint $table) {
            $table->integer('contratofaturas_id')->nullable()->unsigned();
            $table->integer('sfpadrao_id')->nullable()->change();

            $table->foreign('contratofaturas_id')->references('id')->on('contratofaturas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ordens_bancarias', function (Blueprint $table) {
            $table->integer('sfpadrao_id')->nullable(false)->change();
            $table->dropColumn('contratofaturas_id');
        });
    }
}
