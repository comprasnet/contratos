<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertCodigoitensTermoEncerramentoContrato extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = \App\Models\Codigo::where('descricao', 'Tipo de Contrato')->first();
        $codigoitem = \App\Models\Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => '21',
            'descricao' => 'Termo de Encerramento'
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $codigo = \App\Models\Codigo::where('descricao', 'Tipo de Contrato')->first();
        \App\Models\Codigoitem::where([
            'codigo_id' => $codigo->id,
            'descres' => '21',
            'descricao' => 'Termo de Encerramento'
        ])->forceDelete();
    }
}
