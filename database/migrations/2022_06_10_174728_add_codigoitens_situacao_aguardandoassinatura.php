<?php

use App\Models\Codigo;
use App\Models\Codigoitem;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCodigoitensSituacaoAguardandoassinatura extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = Codigo::where('descricao', 'Situação Envia Dados PNCP')->first();

        Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'ASNPEN',
            'descricao' => 'Assinatura Pendente',
            'visivel' => false
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Codigoitem::where([
            'descres' => 'ASNPEN',
            'visivel' => false
        ])->forceDelete();
    }
}
