<?php

use App\Models\Codigo;
use App\Models\Codigoitem;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTtipoNotafiscaleletronicaCodigoitem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = Codigo::where('descricao', 'Tipo de Instrumento de Cobrança')->first();
        $codigo_item = Codigoitem::where('descricao', 'Nota Fiscal Eletrônica')
            ->where('codigo_id', $codigo->id)
            ->first();

        if (!$codigo_item) {
            Codigoitem::create([
                    'codigo_id' => $codigo->id,
                    'descricao' => 'Nota Fiscal Eletrônica',
                    'descres' => 'NFE',
                    'visivel' => true,
                    'tipo_minuta' => false
                ]
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $codigo = Codigo::where('descricao', 'Tipo de Instrumento de Cobrança')->first();
        $codigo_item = Codigoitem::where('descricao', 'Nota Fiscal Eletrônica')
            ->where('codigo_id', $codigo->id)
            ->delete();
    }
}
