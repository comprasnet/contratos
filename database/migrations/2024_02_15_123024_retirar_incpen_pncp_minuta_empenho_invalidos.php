<?php

use Illuminate\Database\Migrations\Migration;
use \App\Models\{EnviaDadosPncp, MinutaEmpenho};
use \Illuminate\Support\Facades\Log;
class RetirarIncpenPncpMinutaEmpenhoInvalidos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $minutaEmpenhoIndevidoPncp = EnviaDadosPncp::join('minutaempenhos', function ($query) {
            $query->on('envia_dados_pncp.pncpable_id', 'minutaempenhos.id')
                ->where('envia_dados_pncp.pncpable_type', MinutaEmpenho::class);
        })->whereHas('status', function ($ci) {
            $ci->where('descres', 'INCPEN');
        })->where('minutaempenho_forcacontrato', 0)
        ->select('envia_dados_pncp.*')
        ->get();

        $mensagem = "Quantidade registros afetados: {$minutaEmpenhoIndevidoPncp->count()}";
        Log::info($mensagem);
//        dd($minutaEmpenhoIndevidoPncp->toSql(), $minutaEmpenhoIndevidoPncp->getBindings());

        foreach ($minutaEmpenhoIndevidoPncp as $enviaDadosPncp) {
            $mensagem = "Registro afetado com o id da tabela envia_dados_pncp: {$enviaDadosPncp->id}";
            Log::info($mensagem);
            $enviaDadosPncp->retorno_pncp = 'Removido devido a minuta de empenho não ser substitutiva de contrato';
            $enviaDadosPncp->save();
            $enviaDadosPncp->delete();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $minutaEmpenhoIndevidoPncp = EnviaDadosPncp::join('minutaempenhos', function ($query) {
            $query->on('envia_dados_pncp.pncpable_id', 'minutaempenhos.id')
                ->where('envia_dados_pncp.pncpable_type', MinutaEmpenho::class);
        })->whereHas('status', function ($ci) {
            $ci->where('descres', 'INCPEN');
        })->where('minutaempenho_forcacontrato', 0)
            ->where('retorno_pncp', 'Removido devido a minuta de empenho não ser substitutiva de contrato')
            ->select('envia_dados_pncp.*')
            ->onlyTrashed()->get();


        foreach ($minutaEmpenhoIndevidoPncp as $enviaDadosPncp) {
            $enviaDadosPncp->restore();
            $enviaDadosPncp->retorno_pncp = null;
            $enviaDadosPncp->save();

            $mensagem = "Registro afetado rollback com o id da tabela envia_dados_pncp: {$enviaDadosPncp->id}";
            Log::info($mensagem);
        }
    }
}
