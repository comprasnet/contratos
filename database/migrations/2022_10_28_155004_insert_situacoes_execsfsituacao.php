<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Codigo;
use App\Models\Codigoitem;
use App\Models\Execsfsituacao;

class InsertSituacoesExecsfsituacao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tipo_situacao = Codigoitem::whereHas('codigo', function ($c) {
            $c->where('descricao','Tipos Situações DH SIAFI');
        })
            ->where('descres', 'SIT0001')
            ->first();

        Execsfsituacao::updateOrCreate(['codigo' => 'DSP001'],['descricao' => 'AQUISICAO DE SERVICOS - PESSOAS JURIDICAS','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP101'],['descricao' => 'AQUISICAO DE MATERIAIS PARA ESTOQUE','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP102'],['descricao' => 'AQUISICAO DE MATERIAIS PARA CONSUMO IMEDIATO','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP051'],['descricao' => 'AQUISICAO DE SERVICOS - PESSOAS FISICAS','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP201'],['descricao' => 'AQUISICAO DE BENS MOVEIS','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP002'],['descricao' => 'AQUISICAO SERVICOS - PESSOA JURIDICA (CONTRATO TIPO CREDOR + PC OU RC)','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP205'],['descricao' => 'DESPESAS COM AQUISICAO DE IMOVEIS, OBRAS E INSTALACOES','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'SPF003'],['descricao' => 'PAGAMENTO ANTECIPADO - CPGF - CARTAO DE PAGAMENTO GOVERNO FEDERAL - SAQUE FATURA','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP003'],['descricao' => 'CONTRIBUICAO PARA CUSTEIO DO SERVICO DE ILUMINACAO PUBLICA (TRIBUTO)','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'SPF006'],['descricao' => 'PAGAMENTO ANTECIPADO - CARTAO DE PAGAMENTO DO GOVERNO FEDERAL/SOMENTE FATURA','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP115'],['descricao' => 'DESPESAS COM AQUISICAO DE MATERIAL DE CONSUMO/SERVICOS - COMPRAS CENTRALIZADAS','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DVL001'],['descricao' => 'DEVOLUCAO DE DESPESAS COM CONTRATACAO DE SERVICOS - PESSOAS JURIDICAS','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP973'],['descricao' => 'DESPESAS COM JUROS/ENCARGOS DE MORA REF AQUISICAO DE BENS E SERVICOS','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'SPF005'],['descricao' => 'SUPRIMENTO DE FUNDOS - CARTAO PAGAMENTO DO GOVERNO FEDERAL  - SIGILOSO','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSF003'],['descricao' => 'DEVOLUCAO DE SUPRIMENTO DE FUNDOS - SAQUE CPGF - NAO SIGILOSO','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP216'],['descricao' => 'DESPESAS COM AQUISICAO DE BENS INTANGIVEIS','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP052'],['descricao' => 'AQUISICAO SERVICOS-CONTROLE CREDOR E PROC.COMPRA OU REG. CONTRATUAL PESSOA FISIC','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'EST001'],['descricao' => 'DEVOLUCAO DE OB CANCELADA COM ESTORNO DA DESPESA PAGA. ESTORNO DE VPD.','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP005'],['descricao' => 'DESPESAS TRIBUTARIAS COM A UNIAO, ESTADOS OU MUNICIPIOS - RECOLH. OB/GRU','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP010'],['descricao' => 'SERVICOS DE OUTSOURCING PARA AQUISICAO DE ALMOXARIFADO VIRTUAL - IN 8/2018 MPDG','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP214'],['descricao' => 'AQUISICAO DE BENS DO IMOBILIZADO OU INTANGIVEL - COMPRAS CENTRALIZADAS','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP235'],['descricao' => 'AQUISICAO DE BENS MOVEIS - INSCRICAO GENERICA','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP113'],['descricao' => 'DESPESAS C/AQUIS.MAT.CONSUMO IMEDIATO C/CONTROLE DE CONTRATO P/CREDOR E NºPROC','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSF006'],['descricao' => 'DEVOLUCAO DE SUPRIMENTO DE FUNDOS - SAQUE CPGF - SIGILOSO','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP107'],['descricao' => 'AQUISICAO DE MATERIAIS PARA ESTOQUE COM IMPORTACOES EM ANDAMENTO','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP008'],['descricao' => 'CONTRIBUICAO PARA SERVICO DE ILUMINACAO PUBLICA (CONTRATO CREDOR + PC OU RC)','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP233'],['descricao' => 'ADIANTAMENTOS DIVERSOS CONCEDIDOS - CONVENIOS E INSTRUMENTOS CONGENERES (CEF)','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSF004'],['descricao' => 'DEVOLUCAO DE SUPRIMENTO DE FUNDOS - FATURA - NAO SIGILOSO','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP006'],['descricao' => 'AQUISICAO DE TERCEIRIZACAO DE MAO-DE-OBRA','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP007'],['descricao' => 'AQUISICAO DE SERVICOS - PESSOAS JURIDICAS - CREDOR UG E C/C CONTRATO IG','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP901'],['descricao' => 'DESPESAS COM INDENIZACOES DIVERSAS','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP062'],['descricao' => 'DESPESAS COM SERVICOS EVENTUAIS DE PESSOAL TECNICO','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP974'],['descricao' => 'VPD DE OUTRAS VARIACOES MONETARIAS E CAMBIAIS','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP207'],['descricao' => 'DESPESAS COM IMPORTACOES EM ANDAMENTO','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP975'],['descricao' => 'DESPESAS COM JUROS/ENCARGOS DE MORA DE OBRIGACOES TRIBUTARIAS','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'SPF002'],['descricao' => 'SUPRIMENTO DE FUNDOS - CONTA TIPO B - NAO SIGILOSO','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP109'],['descricao' => 'DESPESAS COM AQUISICAO DE MATERIAIS DE CONSUMO DE USO DURADOURO','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP206'],['descricao' => 'DESPESAS C/AQUISICAO IMOVEIS,OBRAS E INSTALACOES - CONTA DE CONTRATO PC OU RC','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP112'],['descricao' => 'DESPESAS C/AQUIS.MAT.CONSUMO P/ESTOQUE C/CONTROLE DE CONTRATO P/CREDOR E NºPROC','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSF002'],['descricao' => 'DEVOLUCAO DE SUPRIMENTO DE FUNDOS - CONTA TIPO B','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP211'],['descricao' => 'AQUIS. DE BENS MOVEIS - CONTROLE DE CREDOR E IG (PROC.COMPRA OU REG.CONTRATUAL)','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'BPV004'],['descricao' => 'PAGAMENTO DE OBRIGACOES LIQUIDADAS FORA DO CPR(EXEMPLO SIADS) E INSCRITAS EM RPP','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP986'],['descricao' => 'DESPESAS COM MULTAS ADMINISTRATIVAS','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DVL101'],['descricao' => 'DEVOLUCAO DE DESPESAS COM MATERIAL PARA ESTOQUE','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'BPV001'],['descricao' => 'PAGAMENTO DE OBRIGACOES LIQUIDADAS FORA DO CPR OU POR OUTRO DOCUMENTO HABIL','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP137'],['descricao' => 'ADIANTAMENTO A FORNECEDORES PARA AQUISICAO DE MATERIAIS PARA ESTOQUE','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP126'],['descricao' => 'TRANSF. INSTIT. PRIVADAS SEM FINS LUCRATIVOS, INCLUSIVE FUNDO PARTIDARIO - TSE','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP061'],['descricao' => 'DESPESAS COM BOLSAS DE ESTUDO','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP972'],['descricao' => 'DESPESAS COM PREMIACOES E ORDENS HONORIFICAS','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP021'],['descricao' => 'AQUISICAO DE SERVICOS - PESSOAS JURIDICAS - DOC. REALIZ. OB E FAVORECIDO PROP.UG','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DVL102'],['descricao' => 'DEVOLUCAO DE DESPESAS COM AQUISICAO DE MATERIAL PARA CONSUMO IMEDIATO','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'TRF057'],['descricao' => 'DESPESA CORRENTE COM TRANSFERENCIA A ORGANISMOS INTERNACIONAIS','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP226'],['descricao' => 'DESPESAS COM AQUISICAO DE BENS INTANGIVEIS (CONTRATO TIPO CREDOR + PC OU RC)','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'SPF023'],['descricao' => 'SUPRIMENTO DE FUNDOS - CONTA TIPO B - NAO SIGILOSO - MOEDA ESTRANGEIRA','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DFL013'],['descricao' => 'DESPESA COM BENEFICIOS A PESSOAL - CIVIL RGPS','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP925'],['descricao' => 'DESPESAS EFETUADAS COM DEPOSITOS JUDICIAIS, CONTRATOS, CONVENCAO E PARA RECURSOS','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP024'],['descricao' => 'AQUISICAO SERVICOS - PESSOA JURIDICA (CONTRATO TIPO CREDOR + PC OU RC) - UG EMIT','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP017'],['descricao' => 'DESPESAS COM ADIANTAMENTOS A PRESTADORES DE SERVICOS - PESSOA JURIDICA','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'SPF004'],['descricao' => 'SUPRIMENTO DE FUNDOS - CONTA TIPO B - SIGILOSO','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP209'],['descricao' => 'AQUISICAO DE BENS MOVEIS COM ADIANTAMENTO P/ INVERSOES OU BENS EM ELABORACAO','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DVL081'],['descricao' => 'DEVOLUCAO DE DESPESAS COM DIARIAS','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'TRF017'],['descricao' => 'DESPESAS COM TRANSFERENCIAS CONSTITUCIONAIS E LEGAIS','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DFL045'],['descricao' => 'DESPESA COM OUTROS SERVICOS DE TERCEIROS - PESSOA FISICA','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP122'],['descricao' => 'AQUISICAO DE MATERIAIS P/ESTOQUE - DOC. DE REALIZACAO OB E FAVORECIDO PROPRIA UG','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP123'],['descricao' => 'AQUISICAO MATERIAIS P/CONSUMO IMEDIATO - DOC.REALIZACAO OB FAVORECIDO PROPRIA UG','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSF001'],['descricao' => 'DEVOLUCAO DE SUPRIMENTO DE FUNDOS - CONTA TIPO B - SIGILOSO','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP230'],['descricao' => 'DESPESAS COM BENS MOVEIS OU SOFTWARES - COMPRAS CENTRALIZADAS - REALIZACAO OB','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP231'],['descricao' => 'DESPESAS COM AQUISICAO DE IMOVEIS A CLASSIFICAR','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DVL003'],['descricao' => 'DEVOLUCAO DE DESPESAS COM CONTRIBUICAO PARA SERVICOS DE ILUMINACAO PUBLICA','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DVL201'],['descricao' => 'DEVOLUCAO DE DESPESAS COM AQUISICAO DE BENS MOVEIS','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP015'],['descricao' => 'AQUISICAO DE SERVICOS - PESSOAS JURIDICAS - CREDOR DIFERENTE DA NE','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DVL973'],['descricao' => 'DEVOLUCAO DE DESPESAS COM JUROS/ENCARGOS DE MORA COM BENS/SERVICOS','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DVL975'],['descricao' => 'DEVOLUCAO DE DESPESAS COM JUROS/ENCARGOS DE MORA DE OBRIGACOES TRIBUTARIAS','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'BPV010'],['descricao' => 'PAGAMENTO MULTAS BLOQUEADAS FORNECEDORES - 2.1.8.8.1.01.28 - RP PROCESSADOS 2016','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DFL038'],['descricao' => 'DESPESA COM OUTROS BENEFICIOS PREVIDENCIARIOS E ASSISTENCIAIS','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'TRF020'],['descricao' => 'DESPESAS COM TRANSFERENCIAS LEGAIS - SEM FORMALIZACAO','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'EST022'],['descricao' => 'DEVOLUCAO DE OB CANCELADA COM ESTORNO DA DESPESA PAGA. BENS IMOVEIS','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DVL051'],['descricao' => 'DEVOLUCAO DE DESPESAS COM CONTRATACAO DE SERVICOS - PESSOAS FISICAS','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP127'],['descricao' => 'DESPESAS COM AQUISICAO DE MATERIAL DE CONSUMO - COMPRAS CENTRALIZADAS - REALZ.OB','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP215'],['descricao' => 'AQUISICAO DE BENS INTANGIVEIS COM REGISTRO DO ATIVO NA UG DESCENTRALIZADORA','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DVL205'],['descricao' => 'DEVOLUCAO DE DESPESAS COM AQUISICAO DE IMOVEIS, OBRAS E INSTALACOES','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DVL313'],['descricao' => 'DEVOLUCAO DE DESPESA COM BENEFICIOS A PESSOAL CIVIL - RGPS','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSF010'],['descricao' => 'DEVOLUCAO DE SUPRIMENTO DE FUNDOS - FATURA - SIGILOSO','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP081'],['descricao' => 'DESPESAS COM DIARIAS','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DVL061'],['descricao' => 'DEVOLUCAO DE DESPESAS COM BOLSAS DE ESTUDO E INCENTIVOS A CULTURA','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP212'],['descricao' => 'ADIANTAMENTO A FORNECEDORES DE BENS E SERVICOS - CURTO PRAZO','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'BPV017'],['descricao' => 'PAGAMENTO MULTAS BLOQUEADAS FORNECEDORES -21881.01.28 -NO EXERC. E RPNP A LIQUI.','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSF011'],['descricao' => 'DEVOLUCAO DE SUPRIMENTO DE FUNDOS - CPGF SAQUE/FATURA - ROTINA BACEN','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DFL023'],['descricao' => 'DESPESA COM BENEFICIOS A PESSOAL - MILITAR','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP902'],['descricao' => 'DESPESAS CORRENTES PARA AUXILIO A PESQUISADORES','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP120'],['descricao' => 'AQUISICAO DE MERCADORIAS PARA REVENDA - CONAB/OUTROS','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP909'],['descricao' => 'DESPESAS COM OUTRAS CONTRIBUICOES','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DVL986'],['descricao' => 'DEVOLUCAO DE DESPESAS COM MULTAS ADMINISTRATIVAS','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'EST004'],['descricao' => 'DEVOLUCAO DE OB CANCELADA COM ESTORNO DA DESPESA PAGA. BENS MOVEIS E INTANGIVEL','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'PPV100'],['descricao' => 'APROPRIACAO DO VALOR BRUTO A DEDUZIR - SEM TROCA DE FONTE','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP202'],['descricao' => 'AQUISICAO DE BENS MOVEIS - CREDOR DIFERENTE DA NE','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'EST014'],['descricao' => 'DEVOLUCAO DE OB CANCELADA COM ESTORNO DA DESPESA PAGA. SUPRIMENTO DE FUNDOS.','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSF009'],['descricao' => 'DEVOLUCOES DE SUPRIMENTO DE FUNDOS MOEDA ESTRANGEIRA (ESPECIE)','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP019'],['descricao' => 'AQUISICAO DE SERVICOS - PESSOAS JURIDICAS, COM REGISTRO DE CONTRATO NA SET.FIN.','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'EST030'],['descricao' => 'DEVOLUCAO DE OB CANCELADA C/ESTORNO DA DESPESA PAGA.ESTORNO DE COMPRAS CENTRALIZ','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP105'],['descricao' => 'AQUISICAO DE MATERIAIS P/ESTOQUE - CREDOR DIFERENTE DA NE - C/C ESTOQUE = SUBITE','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DVL005'],['descricao' => 'DEVOLUCAO DE DESPESAS TRIBUTARIAS COM A UNIAO, ESTADOS OU MUNICIPIOS - OB/GRU','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'EST003'],['descricao' => 'DEVOLUCAO DE OB CANCELADA COM ESTORNO DA DESPESA PAGA. MATERIAL DE CONSUMO.','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP906'],['descricao' => 'CONCESSAO DE EMPRESTIMOS/FINANCIAMENTO DE LONGO PRAZO','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP009'],['descricao' => 'AMORTIZACAO DE PARCELA/FINANCIAMENTO DE AQUISICAO DE BENS DO IMOBILIZADO','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DVL901'],['descricao' => 'DEVOLUCAO DE DESPESAS COM INDENIZACOES E RESTITUICOES DIVERSAS','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP210'],['descricao' => 'ADIANTAMENTO A FORNECEDORES DE BENS E SERVICOS - LP','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DVL017'],['descricao' => 'DEVOLUCAO DE DESPESAS COM ADIANTAMENTOS A PRESTADORES DE SERVICOS - PJ','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'BPV013'],['descricao' => 'PAGAMENTO DE INSS INSCRITO FORA DO CPR OU POR OUTRO DH','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP069'],['descricao' => 'AQUISICAO DE SERVICOS - PESSOAS FISICAS, COM REGISTRO DE CONTRATO NA SET.FIN.','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'PPV018'],['descricao' => 'PAGAMENTO DE OBRIGACOES LIQUIDADAS FORA DO CPR OU POR OUTRO DOCUMENTO HABIL','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DVL216'],['descricao' => 'DEVOLUCAO DE DESPESAS COM AQUISICAO DE BENS INTANGIVEIS - FAVORECIDO DIF. DA NE','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP910'],['descricao' => 'DESPESA COM AUXILIOS A PESSOAS FISICAS','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP085'],['descricao' => 'DESPESAS DE CAPITAL COM DIARIAS/PASSAGENS COM INCORPORACAO DE BENS IMOVEIS','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DVL115'],['descricao' => 'DEVOLUCAO DE COMPRAS CENTRALIZADAS','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'EST031'],['descricao' => 'DEVOLUCAO DE GFIP COM ESTORNO DA DESPESA PAGA. ESTORNO DE VPD.','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP067'],['descricao' => 'DESPESAS COM ADIANTAMENTOS A PRESTADORES DE SERVICOS - PESSOA FISICA','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP953'],['descricao' => 'DESPESAS COM INDENIZACOES E RESTITUICOES DIVERSAS C/INCORPORACAO DE BENS IMOVEIS','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DFL049'],['descricao' => 'DESPESAS COM INCENTIVOS A EDUCACAO, CIENCIA,CULTURA, ESPORTE E OUTROS','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'PPV001'],['descricao' => 'PAGAMENTO DE PASSIVO INSCRITO EM RPP - FONTE 0177 - REALIZACAO POR OB OU GRU','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP988'],['descricao' => 'DESPESAS COM JUROS/ENCARGOS DE MORA DE OBRIGACOES TRIBUTARIAS - DARF','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DVL457'],['descricao' => 'DEVOLUCAO DE DESPESAS COM TRANSFERENCIAS A ORGANISMOS INTERNACIONAIS - ANUIDADE','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP058'],['descricao' => 'AQUISICAO DE SERVICOS - PESSOAS FISICAS  - CREDOR DIFERENTE DA NE','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DVL345'],['descricao' => 'DEVOLUCAO DE DESPESA COM OUTROS SERVICOS DE TERCEIROS - PESSOA FISICA','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DVL902'],['descricao' => 'DEVOLUCAO DE DESPESAS CORRENTES PARA AUXILIO A PESQUISADORES','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'BPV015'],['descricao' => 'PAGAMENTO DE INSS INSCRITO FORA DO CPR OU POR OUTRO DH','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DFL005'],['descricao' => 'DESPESA COM REMUNERACAO A PESSOAL PENSIONISTA CIVIL - RPPS','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);
        Execsfsituacao::updateOrCreate(['codigo' => 'DSP072'],['descricao' => 'DEPOSITOS JUDICIAIS, CONTRATOS, CONVENCAO E PARA RECURSOS - OUTROS BANCOS','aba' => 'PCO', 'tipo_situacao' => $tipo_situacao->id]);



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
