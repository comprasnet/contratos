<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class InsertPermissaoFiscalizacaoGestaodeatas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        app()['cache']->forget('spatie.permission.cache');

        Permission::create(['name' => 'fiscalizacao_V2_acessar']);
        Permission::create(['name' => 'gestaodeatas_V2_acessar']);

        $role = Role::where(['name' => 'Administrador'])->first();

        $role->givePermissionTo('fiscalizacao_V2_acessar');
        $role->givePermissionTo('gestaodeatas_V2_acessar');

        $role = Role::where(['name' => 'Gestor de Atas'])->first();
        $role->givePermissionTo('gestaodeatas_V2_acessar');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $role = Role::where(['name' => 'Administrador'])->first();

        $role->revokePermissionTo('fiscalizacao_V2_acessar');
        $role->revokePermissionTo('gestaodeatas_V2_acessar');

        $role = Role::where(['name' => 'Gestor de Atas'])->first();
        $role->revokePermissionTo('gestaodeatas_V2_acessar');

        Permission::where(['name' => 'gestaodeatas_V2_acessar'])->forceDelete();
        Permission::where(['name' => 'fiscalizacao_V2_acessar'])->forceDelete();

    }
}
