<?php

use App\Models\Unidade;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AtualizaUtilizacustosUnidades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('unidades', 'utiliza_siafi')) {
            Unidade::where('utiliza_siafi', true)
                ->where('utiliza_custos', false)
                ->update(['utiliza_custos' => true]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
