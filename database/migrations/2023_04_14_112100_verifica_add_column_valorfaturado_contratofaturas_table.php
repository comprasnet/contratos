<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VerificaAddColumnValorfaturadoContratofaturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('contratofaturas', 'valorfaturado')){
            Schema::table('contratofaturas', function (Blueprint $table) {
                $table->float('valorfaturado')->nullable();
            });
        }
        if(!Schema::hasColumn('contratofaturas', 'valor_antigo')){
            Schema::table('contratofaturas', function (Blueprint $table) {
                $table->float('valor_antigo')->nullable();
            });
        }
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('contratofaturas', 'valorfaturado')){
            Schema::table('contratofaturas', function (Blueprint $table) {
                $table->dropColumn('valorfaturado');
            });
        }
    }
}
