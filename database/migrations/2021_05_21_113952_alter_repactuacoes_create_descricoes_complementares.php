<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRepactuacoesCreateDescricoesComplementares extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('repactuacoes', function (Blueprint $table) {
            $table->text('descricoes_complementares')->nullable();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('repactuacoes', function (Blueprint $table) {
            $table->dropColumn('descricoes_complementares');
        });
    }
}
