<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCodigoMunicipioDeducaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('sfdeducao', 'codigo_municipio')) {
            Schema::table('sfdeducao', function (Blueprint $table) {
                $table->string('codigo_municipio', 25)->nullable();
            });

            Schema::table('sfpredoc', function (Blueprint $table) {
                $table->string('codigo_municipio', 25)->nullable();
            });

            Schema::table('sfacrescimo', function (Blueprint $table) {
                $table->string('codigo_municipio', 25)->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('sfdeducao', 'codigo_municipio')) {
            Schema::table('sfdeducao', function (Blueprint $table) {
                $table->dropColumn('codigo_municipio');
            });

            Schema::table('sfpredoc', function (Blueprint $table) {
                $table->dropColumn('codigo_municipio');
            });

            Schema::table('sfacrescimo', function (Blueprint $table) {
                $table->dropColumn('codigo_municipio');
            });
        }
    }
}
