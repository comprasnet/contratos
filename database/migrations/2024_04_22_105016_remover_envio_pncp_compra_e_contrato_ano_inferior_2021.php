<?php

use App\Models\Contratohistorico;
use App\Models\EnviaDadosPncp;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Str;

class RemoverEnvioPncpCompraEContratoAnoInferior2021 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::beginTransaction();

        try {
            $registrosParaDeletar = Contratohistorico::join('compras', function ($join) {
                $join->on('contratohistorico.licitacao_numero', '=', 'compras.numero_ano')
                    ->on('contratohistorico.unidadecompra_id', '=', 'compras.unidade_origem_id')
                    ->on('contratohistorico.modalidade_id', '=', 'compras.modalidade_id');
            })
                ->join('envia_dados_pncp', 'contratohistorico.id', '=', 'envia_dados_pncp.pncpable_id')
                ->join('codigoitens', 'contratohistorico.tipo_id', '=', 'codigoitens.id')
                ->join('unidades', 'contratohistorico.unidade_id', '=', 'unidades.id')
                ->where(function ($query) {
                    $query->where('retorno_pncp', 'ilike', '%Ano do contrato deve ser igual ou posterior a 2021%')
                        ->orWhere('retorno_pncp', 'ilike', '%Ano da compra deve ser igual ou posterior a 2021%');
                })
                ->where('unidades.sigilo', false)
                ->select('envia_dados_pncp.id', 'envia_dados_pncp.retorno_pncp')
                ->get();

            Log::info('Quantidade de registros a serem deletados: ' . count($registrosParaDeletar));

            foreach ($registrosParaDeletar as $registroEnviaDadosPNCP) {
                $registro = EnviaDadosPncp::find($registroEnviaDadosPNCP->id);
                if (!$registro) {
                    Log::error('Registro não encontrado: ' . $registroEnviaDadosPNCP->id);
                    continue; // Pula para a próxima iteração do loop
                }
                $retornoPncpString = $registro->retorno_pncp;
                $retornoPncpString = str_replace('Ano do contrato deve ser igual ou posterior a 2021', 'Registro excluído logicamente - Ano do contrato deve ser igual ou posterior a 2021', $retornoPncpString);
                $retornoPncpString = str_replace('Ano da compra deve ser igual ou posterior a 2021', 'Registro excluído logicamente - Ano da compra deve ser igual ou posterior a 2021', $retornoPncpString);
                # Atualiza o campo retorno_pncp com o JSON modificado
                $registro->retorno_pncp = $retornoPncpString;
                #dump($registro->retorno_pncp);
                $registro->save();
                $registro->delete();

                // Log do ID do reg0istro afetado
                Log::info('ID do registro afetado da tabela envia_dados_pncp: ' . $registro->id);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Erro durante a modificação e exclusão dos registros da tabela envia_dados_pncp: ' . $e->getMessage());
            throw $e;
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::beginTransaction();
        try {
            $registrosParaRestaurar = EnviaDadosPncp::withTrashed()
                ->whereIn('id', function ($query) {
                    $query->select('id')
                        ->from('envia_dados_pncp')
                        ->where('retorno_pncp', 'like',
                            '%Registro excluído logicamente - Ano do contrato deve ser igual ou posterior a 2021%')
                        ->orWhere('retorno_pncp', 'like',
                            '%Registro excluído logicamente - Ano da compra deve ser igual ou posterior a 2021%');
                })
                ->get();

            #dd(Str::replaceArray('?', $registrosParaRestaurar->getBindings(), $registrosParaRestaurar->toSql()));
            Log::info('Quantidade de registros a serem restaurados: ' . $registrosParaRestaurar->count());

            foreach ($registrosParaRestaurar as $registroEnviaDadosPNCP) {
                $registro = EnviaDadosPncp::withTrashed()->find($registroEnviaDadosPNCP->id);
                if (!$registro) {
                    Log::error('Registro não encontrado: ' . $registroEnviaDadosPNCP->id);
                    continue; // Pula para a próxima iteração do loop
                }
                # Busca a mensagem de retorno para substituir
                $retornoPncpString = $registro->retorno_pncp;
                # Remover a parte "Registro excluído logicamente -" da mensagem
                $retornoPncpString = str_replace('Registro excluído logicamente - ', '', $retornoPncpString);
                # Atualiza o campo retorno_pncp com a mensagem original
                $registro->retorno_pncp = $retornoPncpString;
                $registro->restore();
                $registro->save();
                # Log do ID do registro afetado
                Log::info('ID do registro afetado da tabela envia_dados_pncp: ' . $registro->id);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Erro durante a restauração dos registros da tabela envia_dados_pncp: ' . $e->getMessage());
            throw $e;
        }
    }

}
