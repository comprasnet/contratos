<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertCodigoitensSituacaoAntecipaGov_ extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = \App\Models\Codigo::create([
            'descricao' => 'Situacao Antecipa Gov',
            'visivel' => true
        ]);
        // REGISTRADO LIQUIDADO CANCELADO
        \App\Models\Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'REG',
            'descricao' => 'Registrado',
            'visivel' => true
        ]);

        \App\Models\Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'LIQ',
            'descricao' => 'Liquidado',
            'visivel' => true
        ]);

        \App\Models\Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'CAN',
            'descricao' => 'Cancelado',
            'visivel' => true
        ]);


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\Models\Codigo::where([
            'descricao' => 'Situacao Antecipa Gov',
            'visivel' => true
        ])->forceDelete();

    }
}
