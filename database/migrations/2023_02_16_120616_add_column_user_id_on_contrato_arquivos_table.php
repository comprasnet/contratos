<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnUserIdOnContratoArquivosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('contrato_arquivos', 'user_id')){
            Schema::table('contrato_arquivos', function (Blueprint $table) {
                $table->integer('user_id')->nullable();
                $table->foreign('user_id', 'user_id')->references('id')->on('users')->nullable()->default(null);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contrato_arquivos', function (Blueprint $table) {
            $table->dropColumn(['user_id']);
        });
    }
}
