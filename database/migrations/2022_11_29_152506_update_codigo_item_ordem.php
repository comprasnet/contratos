<?php

use App\Models\Codigo;
use App\Models\Codigoitem;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCodigoItemOrdem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = Codigo::where([
            'descricao' => 'Operação item empenho'
        ])->first();

        $itens = [
            ['descricao' => 'INCLUSAO'                 , 'ordem' => 2] ,
            ['descricao' => 'REFORÇO'                  , 'ordem' => 4] ,
            ['descricao' => 'ANULAÇÃO'                 , 'ordem' => 2] ,
            ['descricao' => 'CANCELAMENTO'             , 'ordem' => 5] ,
            ['descricao' => 'NENHUMA'                  , 'ordem' => 1] ,
            ['descricao' => 'ANULAÇÃO SALDO IRRISÓRIO' , 'ordem' => 3]
        ];

        foreach ($itens as $item) {
            Codigoitem::where('codigo_id', $codigo->id)
                ->where('descricao', $item['descricao'])
                ->update(['ordem' => $item['ordem']]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $codigo = Codigo::where([
            'descricao' => 'Operação item empenho'
        ])->first();

        Codigoitem::where('codigo_id', $codigo->id)
            ->update(['ordem' => null]);

    }
}
