<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterContratoArquivoPncp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contrato_arquivos', function (Blueprint $table) {
            $table->string('envio_pncp_pendente')->nullable();
            $table->string('link_pncp')->nullable();
            $table->string('sequencial_pncp')->nullable();
            $table->string('retorno_pncp')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contrato_arquivos', function (Blueprint $table) {
            $table->dropColumn('envio_pncp_pendente');
            $table->dropColumn('link_pncp');
            $table->dropColumn('sequencial_pncp');
            $table->dropColumn('retorno_pncp');
        });
    }
}

