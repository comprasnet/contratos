<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRetornoSiafiToAmparoLegalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('amparo_legal', function (Blueprint $table) {
            $table->string('retorno_siafi')->nullable();
            $table->string('restricao_regra')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('amparo_legal', function (Blueprint $table) {
            $table->dropColumn('retorno_siafi')->nullable(true);
            $table->dropColumn('restricao_regra')->nullable(true);
        });
    }
}
