<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class InsertPermissionMeuContratoArquivoDados extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        app()['cache']->forget('spatie.permission.cache');

        Permission::create(['name' => 'meucontratoarquivo_inserir']);
        Permission::create(['name' => 'meucontratoarquivo_editar']);
        Permission::create(['name' => 'meucontratoarquivo_deletar']);

        $role = Role::where(['name' => 'Administrador'])->first();
        $role->givePermissionTo('meucontratoarquivo_inserir');
        $role->givePermissionTo('meucontratoarquivo_editar');
        $role->givePermissionTo('meucontratoarquivo_deletar');

        $role = Role::where(['name' => 'Administrador Órgão'])->first();
        $role->givePermissionTo('meucontratoarquivo_inserir');
        $role->givePermissionTo('meucontratoarquivo_editar');
        $role->givePermissionTo('meucontratoarquivo_deletar');

        $role = Role::where(['name' => 'Administrador Unidade'])->first();
        $role->givePermissionTo('meucontratoarquivo_inserir');
        $role->givePermissionTo('meucontratoarquivo_editar');
        $role->givePermissionTo('meucontratoarquivo_deletar');

        $role = Role::where(['name' => 'Setor Contratos'])->first();
        if(!$role){
            $role = Role::create(['name' => 'Setor Contratos']);
        }
        $role->givePermissionTo('meucontratoarquivo_inserir');
        $role->givePermissionTo('meucontratoarquivo_editar');
        $role->givePermissionTo('meucontratoarquivo_deletar');


        $role = Role::where(['name' => 'Responsável por Contrato'])->first();
        if(!$role){
            $role = Role::create(['name' => 'Responsável por Contrato']);
        }
        $role->givePermissionTo('meucontratoarquivo_inserir');
        $role->givePermissionTo('meucontratoarquivo_editar');
        $role->givePermissionTo('meucontratoarquivo_deletar');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $role = Role::where(['name' => 'Administrador'])->first();
        $role->revokePermissionTo('meucontratoarquivo_inserir');
        $role->revokePermissionTo('meucontratoarquivo_editar');
        $role->revokePermissionTo('meucontratoarquivo_deletar');

        $role = Role::where(['name' => 'Administrador Órgão'])->first();
        $role->revokePermissionTo('meucontratoarquivo_inserir');
        $role->revokePermissionTo('meucontratoarquivo_editar');
        $role->revokePermissionTo('meucontratoarquivo_deletar');

        $role = Role::where(['name' => 'Administrador Unidade'])->first();
        $role->revokePermissionTo('meucontratoarquivo_inserir');
        $role->revokePermissionTo('meucontratoarquivo_editar');
        $role->revokePermissionTo('meucontratoarquivo_deletar');

        $role = Role::where(['name' => 'Setor Contratos'])->first();
        $role->revokePermissionTo('meucontratoarquivo_inserir');
        $role->revokePermissionTo('meucontratoarquivo_editar');
        $role->revokePermissionTo('meucontratoarquivo_deletar');

        $role = Role::where(['name' => 'Responsável por Contrato'])->first();
        $role->revokePermissionTo('meucontratoarquivo_inserir');
        $role->revokePermissionTo('meucontratoarquivo_editar');
        $role->revokePermissionTo('meucontratoarquivo_deletar');

        Permission::where(['name' => 'meucontratoarquivo_inserir'])->forceDelete();
        Permission::where(['name' => 'meucontratoarquivo_editar'])->forceDelete();
        Permission::where(['name' => 'meucontratoarquivo_deletar'])->forceDelete();
    }
}
