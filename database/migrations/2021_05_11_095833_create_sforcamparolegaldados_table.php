<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSforcamparolegaldadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sforcamparolegaldados', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('amparo_legal_id');
            $table->integer('codamparolegal')->nullable();
            $table->integer('codmodalidadelicitacao')->nullable();
            $table->string('atonormativo',30)->nullable();
            $table->string('artigo',4)->nullable();
            $table->string('paragrafo',10)->nullable();
            $table->string('inciso',10)->nullable();
            $table->string('alinea',4)->nullable();
            $table->string('motivo',1024)->nullable(); // I - INCLUSAO DO AMPARO atonormativo, artigo, paragrafo, inciso, alinea EM DATA
            $table->string('nonce');
            $table->text('retornosiafi'); //
            $table->char('tipo'); //I - Incluir, D - Deletar, A - Alteracao, R - Reincluir
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sforcamparolegaldados');
    }
}
