<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTermoAcessosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('termo_acessos', 'termo_aceites');

        Schema::table('termo_aceites', function (Blueprint $table) {
            $table->integer('tipo_id')->nullable();
            $table->integer('status_id')->nullable();
            $table->foreign('tipo_id')->references('id')->on('codigoitens');
            $table->foreign('status_id')->references('id')->on('codigoitens');
            $table->string('info', 25)->nullable();
        });

        $tipoAcesso = DB::table('codigoitens')->where('descres', 'ACESSO')->first();
        if ($tipoAcesso) {
            DB::table('termo_aceites')->update(['tipo_id' => $tipoAcesso->id]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('termo_aceites')->update(['tipo_id' => null]);

        Schema::table('termo_aceites', function (Blueprint $table) {
            $table->dropColumn(['tipo_id', 'status_id', 'info']);
        });

        Schema::rename('termo_aceites', 'termo_acessos');
    }
}
