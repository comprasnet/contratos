<?php

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertPermissionPerfilfornecedorConsultar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        app()['cache']->forget('spatie.permission.cache');

        $permission = Permission::where('name','perfilfornecedor_consultar')->first();

        if(!isset($permission->id)){
            Permission::create([
                'name' => 'perfilfornecedor_consultar',
                'guard_name' => 'web'
            ]);
            $role = Role::where('name', 'Administrador')->first();
            $role->givePermissionTo('perfilfornecedor_consultar');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $role = Role::where(['name' => 'Administrador'])->first();
        $role->revokePermissionTo('perfilfornecedor_consultar');

        Permission::where(['name' => 'perfilfornecedor_consultar'])->forceDelete();
    }
}
