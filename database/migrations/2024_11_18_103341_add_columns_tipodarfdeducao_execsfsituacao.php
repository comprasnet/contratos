<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsTipodarfdeducaoExecsfsituacao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('execsfsituacao', function (Blueprint $table) {
            $table->boolean('darf')->default(false);
            $table->boolean('darf_numerado')->default(false);
            $table->boolean('darf_numerado_decomposto')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('execsfsituacao', function (Blueprint $table) {
            $table->dropColumn(['darf', 'darf_numerado', 'darf_numerado_decomposto']);
        });
    }
}
