<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Codigo;
use App\Models\Codigoitem;

class InsertCodigoitensRelatorioFinal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         $codigo = Codigo::where([
            'descricao' => 'Tipos Documentos SEI'
        ])->first();



        $codigoitem = Codigoitem::where([
            'descricao' => 'Relatório Final',
            'descres' => 'RELFINAL',
            'codigo_id' => $codigo->id
        ])->first();

        if( !is_object($codigoitem) ){
            $codigoitem = Codigoitem::create([
                'codigo_id' => $codigo->id,
                'descres' => 'RELFINAL',
                'descricao' => 'Relatório Final'
            ]);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        $codigo = Codigo::where([
            'descricao' => 'Tipos Documentos SEI'
        ])->first();

        Codigoitem::where([
            'descricao' => 'Relatório Final',
            'descres' => 'RELFINAL',
            'codigo_id' => $codigo->id
        ])->forceDelete();
    }
}
