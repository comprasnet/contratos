<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutoridadesignatariaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('autoridadesignataria', function (Blueprint $table) {
            $table->increments('id');
            $table->string('autoridade_signataria');
            $table->string('cargo_autoridade_signataria');
            $table->boolean('titular');
            $table->boolean('ativo');

            $table->integer('unidade_id');
            $table->foreign('unidade_id')->references('id')->on('unidades')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('autoridadesignataria');
    }
}
