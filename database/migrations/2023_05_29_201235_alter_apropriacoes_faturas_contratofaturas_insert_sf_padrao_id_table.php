<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterApropriacoesFaturasContratofaturasInsertSfPadraoIdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('apropriacoes_faturas_contratofaturas', function (Blueprint $table) {
            $table->integer('sfpadrao_id')->nullable();
            $table->foreign('sfpadrao_id')->references('id')->on('sfpadrao')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('apropriacoes_faturas_contratofaturas', function (Blueprint $table) {
            $table->dropColumn(['sfpadrao_id']);
        });
    }
}
