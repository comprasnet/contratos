<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterDesconsiderarSoftdeleteAtualizaSaldosTrigger extends Migration
{
    public function up(): void
    {
        DB::unprepared('
            CREATE OR REPLACE FUNCTION atualiza_saldos() RETURNS TRIGGER AS $$
            BEGIN
                UPDATE empenhos
                SET aliquidar = origem.aliquidar,
                    liquidado = origem.liquidado,
                    pago = origem.pago,
                    empenhado = origem.empenhado,
                    rpinscrito = origem.rpinscrito,
                    rpaliquidar = origem.rpaliquidar,
                    rpliquidado = origem.rpliquidado,
                    rppago = origem.rppago,
                    updated_at = NOW()
                FROM (
                    SELECT empenho_id,
                         -- A LIQUIDAR
                         COALESCE(SUM(empaliquidar), 0) +
                         COALESCE(SUM(empemliquidacao), 0) AS aliquidar,

                         -- LIQUIDADO
                         COALESCE(SUM(emprpp), 0) +
                         COALESCE(SUM(empliquidado), 0) AS liquidado,

                         -- PAGO
                         COALESCE(SUM(emppago), 0) AS pago,

                         -- EMPENHADO
                         COALESCE(SUM(empaliquidar), 0) +
                         COALESCE(SUM(empemliquidacao), 0) +
                         COALESCE(SUM(empliquidado), 0) +
                         COALESCE(SUM(empaliqrpnp), 0) +
                         COALESCE(SUM(empemliqrpnp), 0) +
                         COALESCE(SUM(emprpp), 0) +
                         COALESCE(SUM(emppago), 0) AS empenhado,

                         -- RP A LIQUIDAR
                         COALESCE(SUM(rpnpaliquidar), 0) +
                         COALESCE(SUM(rpnpaliquidaremliquidacao), 0) AS rpaliquidar,

                         -- RP LIQUIDADO
                         COALESCE(SUM(rpnpaliquidar), 0) +
                         COALESCE(SUM(rpnpaliquidaremliquidacao), 0) +
                         COALESCE(SUM(rpnpliquidado), 0) +
                         COALESCE(SUM(rpnpaliquidarbloq), 0) +
                         COALESCE(SUM(rpnpaliquidaremliquidbloq), 0) +
                         COALESCE(SUM(rppliquidado), 0) AS rpliquidado,

                         -- RP PAGO
                         COALESCE(SUM(rpnppago), 0) +
                         COALESCE(SUM(rpppago), 0) AS rppago,

                         -- RP INSCRITO
                         COALESCE(SUM(rpnpaliquidinsc), 0) + COALESCE(SUM(rpnpemliquidinsc), 0) +
                         COALESCE(SUM(reinscrpnpaliquidbloq), 0) + COALESCE(SUM(reinscrpnpemliquid), 0) +
                         COALESCE(SUM(rpnprestab), 0) + COALESCE(SUM(rpnpaliquidtransfdeb), 0) +
                         COALESCE(SUM(rpnpaliquidemliquidtransfdeb), 0) + COALESCE(SUM(rpnpliquidapgtransfdeb), 0) +
                         COALESCE(SUM(rpnpbloqtransfdeb), 0) + COALESCE(SUM(rppinsc), 0) + COALESCE(SUM(rppexecant), 0)+
                         COALESCE(SUM(rpptrasf), 0) - COALESCE(SUM(rpnpaliquidtransfcred), 0) -
                         COALESCE(SUM(rpnpaliquidemliquidtransfcred), 0) - COALESCE(SUM(rpnpliquidapgtransfcred), 0) -
                         COALESCE(SUM(rpnpbloqtransfcred), 0) - COALESCE(SUM(rpptransffusao), 0) -
                         COALESCE(SUM(ajusterpexecant), 0) - COALESCE(SUM(rpnpcancelado), 0) -
                         COALESCE(SUM(rpnpoutrocancelamento), 0) - COALESCE(SUM(rpnpemliqoutrocancelamento), 0) -
                         COALESCE(SUM(rpppago), 0) - COALESCE(SUM(rppcancelado), 0) AS rpinscrito

                      FROM empenhodetalhado
                      WHERE empenho_id = NEW.empenho_id
                        AND deleted_at IS NULL
                      GROUP BY empenho_id) origem
                WHERE empenhos.id = origem.empenho_id;
                RETURN NULL;
            END;
            $$ LANGUAGE plpgsql;
        ');
    }

    public function down(): void
    {
        DB::unprepared("
        create or replace function atualiza_saldos() returns trigger
                language plpgsql
            as
            $$ begin
            update
                empenhos
            set
                aliquidar = origem.aliquidar,
                liquidado = origem.liquidado,
                pago = origem.pago,
                empenhado = origem.empenhado,
                rpinscrito = origem.rpinscrito,
                rpaliquidar = origem.rpaliquidar,
                rpliquidado = origem.rpliquidado,
                rppago = origem.rppago,
                updated_at = origem.updated_at
            from
                (
                select
                    empenho_id,
                    updated_at,
                    coalesce(sum(empaliquidar), 0) + coalesce(sum(empemliquidacao), 0) as aliquidar,
                    coalesce(sum(emprpp), 0) + coalesce(sum(empliquidado), 0) as liquidado,
                    coalesce(sum(emppago), 0) as pago,
                    coalesce(sum(empaliquidar), 0) + coalesce(sum(empemliquidacao), 0) + coalesce(sum(empliquidado), 0) +
                    coalesce(sum(empaliqrpnp), 0) + coalesce(sum(empemliqrpnp), 0) + coalesce(sum(emprpp), 0) +
                    coalesce(sum(emppago), 0) as empenhado,
                    coalesce(sum(rpnpaliquidar), 0) + coalesce(sum(rpnpaliquidaremliquidacao), 0) as rpaliquidar,
                    coalesce(sum(rpnpaliquidar), 0) + coalesce(sum(rpnpaliquidaremliquidacao), 0) +
                    coalesce(sum(rpnpliquidado), 0) + coalesce(sum(rpnpaliquidarbloq), 0) +
                    coalesce(sum(rpnpaliquidaremliquidbloq), 0) + coalesce(sum(rppliquidado), 0) as rpliquidado,
                    coalesce(sum(rpnppago), 0) + coalesce(sum(rpppago), 0) as rppago,
                    coalesce(sum(rpnpaliquidinsc), 0) + coalesce(sum(rpnpemliquidinsc), 0) +
                    coalesce(sum(reinscrpnpaliquidbloq), 0) + coalesce(sum(reinscrpnpemliquid), 0) +
                    coalesce(sum(rpnprestab), 0) + coalesce(sum(rpnpaliquidtransfdeb), 0) +
                    coalesce(sum(rpnpaliquidemliquidtransfdeb), 0) + coalesce(sum(rpnpliquidapgtransfdeb), 0) +
                    coalesce(sum(rpnpbloqtransfdeb), 0) + coalesce(sum(rppinsc), 0) + coalesce(sum(rppexecant), 0) +
                    coalesce(sum(rpptrasf), 0) - coalesce(sum(rpnpaliquidtransfcred), 0) -
                    coalesce(sum(rpnpaliquidemliquidtransfcred), 0) - coalesce(sum(rpnpliquidapgtransfcred), 0) -
                    coalesce(sum(rpnpbloqtransfcred), 0) - coalesce(sum(rpptransffusao), 0) - coalesce(sum(ajusterpexecant), 0) -
                    coalesce(sum(rpnpcancelado), 0) - coalesce(sum(rpnpoutrocancelamento), 0) -
                    coalesce(sum(rpnpemliqoutrocancelamento), 0) - coalesce(sum(rpppago), 0) - coalesce(sum(rppcancelado), 0)
                    as rpinscrito
                from
                    empenhodetalhado
                where
                    empenho_id = new.empenho_id
                group by
                    empenho_id, updated_at		) origem
            where
                id = origem.empenho_id;
            return null;
            end;
            $$;
            ");
    }
}
