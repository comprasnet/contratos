<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatmatpdmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catmatpdms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('catmatclasse_id');
            $table->string('codigo')->unique();
            $table->string('descricao');
            $table->timestamps();

            $table->foreign('catmatclasse_id')->references('id')->on('catmatclasses')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catmatpdms');
    }
}
