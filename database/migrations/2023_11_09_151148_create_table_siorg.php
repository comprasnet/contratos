<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSiorg extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siorg', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo')->unique();
            $table->string('codigo_pai');
            $table->string('codigo_principal');
            $table->string('denominacao');
            $table->string('sigla');
            $table->string('categoria')->nullable();
            $table->string('nivel_normatizacao');
            $table->string('esfera')->nullable();
            $table->string('poder')->nullable();
            $table->string('natureza_juridica')->nullable();
            $table->string('subnatureza_juridica')->nullable();
            $table->string('cnpj')->nullable();
            $table->text('competencia')->nullable();
            $table->text('finalidade')->nullable();
            $table->text('missao')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siorg');
    }
}
