<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterContratocontasPercentuais extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contratocontas', function (Blueprint $table) {
            $table->float('percentual_grupo_a_13_ferias')->change();
            $table->float('saldo_encerramento')->change();
        });
        Schema::table('lancamentos', function (Blueprint $table) {
            $table->float('valor')->change();
            $table->string('encargo_percentual',15)->change();
        });

    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contratocontas', function (Blueprint $table) {
            $table->decimal('percentual_grupo_a_13_ferias',17,2)->change();
            $table->decimal('saldo_encerramento',15,2)->change();
        });
        Schema::table('lancamentos', function (Blueprint $table) {
            $table->decimal('valor',15,2)->change();
            $table->string('encargo_percentual',6)->change();
        });
    }
}
