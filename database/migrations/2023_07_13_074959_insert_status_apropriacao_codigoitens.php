<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use App\Models\Codigo;
use App\Models\Codigoitem;
use Illuminate\Database\Migrations\Migration;

class InsertStatusApropriacaoCodigoitens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = Codigo::where('descricao', 'Status Apropriação Instrumento de Cobrança SIAFI')
            ->where('visivel', false)
            ->first();

        Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'APR',
            'descricao' => 'Siafi Apropriado'
        ]);

        Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'CAN',
            'descricao' => 'Siafi Cancelado'
        ]);

        $codigo_item = Codigoitem::where('codigo_id', $codigo->id)
            ->where('descres','AND')
            ->where('descricao', 'Em Andamento')
            ->first();

        $codigo_item->descricao = 'Apropriação em Andamento';
        $codigo_item->save();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
