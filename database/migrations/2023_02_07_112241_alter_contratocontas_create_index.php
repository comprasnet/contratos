<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterContratocontasCreateIndex extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaManager = Schema::getConnection()->getDoctrineSchemaManager();

        // índices em contratocontas
        $indexesEncontrados = $schemaManager->listTableIndexes('contratocontas');
        if(!array_key_exists("contratocontas_contrato_id_index", $indexesEncontrados)){
            if(Schema::hasColumn('contratocontas', 'contrato_id')){
                Schema::table('contratocontas', function (Blueprint $table) {
                    $table->integer('contrato_id')->index()->change();
                });
            }
        }

        // índices em lancamentos
        $indexesEncontrados = $schemaManager->listTableIndexes('lancamentos');
        // contratoterceirizado_id
        if(!array_key_exists("lancamentos_contratoterceirizado_id_index", $indexesEncontrados)){
            if(Schema::hasColumn('lancamentos', 'contratoterceirizado_id')){
                Schema::table('lancamentos', function (Blueprint $table) {
                    $table->integer('contratoterceirizado_id')->index()->change();
                });
            }
        }
        // movimentacao_id
        if(!array_key_exists("lancamentos_movimentacao_id_index", $indexesEncontrados)){
            if(Schema::hasColumn('lancamentos', 'movimentacao_id')){
                Schema::table('lancamentos', function (Blueprint $table) {
                    $table->integer('movimentacao_id')->index()->change();
                });
            }
        }

        // índices em movimentacaocontratocontas
        $indexesEncontrados = $schemaManager->listTableIndexes('movimentacaocontratocontas');
        if(!array_key_exists("movimentacaocontratocontas_contratoconta_id_index", $indexesEncontrados)){
            if(Schema::hasColumn('movimentacaocontratocontas', 'contratoconta_id')){
                Schema::table('movimentacaocontratocontas', function (Blueprint $table) {
                    $table->integer('contratoconta_id')->index()->change();
                });
            }
        }

        // índices em contratoterceirizados
        $indexesEncontrados = $schemaManager->listTableIndexes('contratoterceirizados');
        if(!array_key_exists("contratoterceirizados_contrato_id_index", $indexesEncontrados)){
            if(Schema::hasColumn('contratoterceirizados', 'contrato_id')){
                Schema::table('contratoterceirizados', function (Blueprint $table) {
                    $table->integer('contrato_id')->index()->change();
                });
            }
        }

        // índices em contratoresponsaveis
        $indexesEncontrados = $schemaManager->listTableIndexes('contratoresponsaveis');
        // contrato_id
        if(!array_key_exists("contratocontas_contrato_id_index", $indexesEncontrados)){
            if(Schema::hasColumn('contratoresponsaveis', 'contrato_id')){
                Schema::table('contratoresponsaveis', function (Blueprint $table) {
                    $table->integer('contrato_id')->index()->change();
                });
            }
        }
        // user_id
        if(!array_key_exists("contratocontas_user_id_index", $indexesEncontrados)){
            if(Schema::hasColumn('contratoresponsaveis', 'user_id')){
                Schema::table('contratoresponsaveis', function (Blueprint $table) {
                    $table->integer('user_id')->index()->change();
                });
            }
        }



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaManager = Schema::getConnection()->getDoctrineSchemaManager();

        // índices em contratocontas
        $indexesEncontrados = $schemaManager->listTableIndexes('contratocontas');
        if(array_key_exists("contratocontas_contrato_id_index", $indexesEncontrados)){
            if(Schema::hasColumn('contratocontas', 'contrato_id')){
                Schema::table('contratocontas', function (Blueprint $table) {
                    $table->dropIndex('contratocontas_contrato_id_index');
                });
            }
        }

        // índices em lancamentos
        $indexesEncontrados = $schemaManager->listTableIndexes('lancamentos');
        // contratoterceirizado_id
        if(array_key_exists("lancamentos_contratoterceirizado_id_index", $indexesEncontrados)){
            if(Schema::hasColumn('lancamentos', 'contratoterceirizado_id')){
                Schema::table('lancamentos', function (Blueprint $table) {
                    $table->dropIndex('lancamentos_contratoterceirizado_id_index');
                });
            }
        }
        // movimentacao_id
        if(array_key_exists("lancamentos_movimentacao_id_index", $indexesEncontrados)){
            if(Schema::hasColumn('lancamentos', 'movimentacao_id')){
                Schema::table('lancamentos', function (Blueprint $table) {
                    $table->dropIndex('lancamentos_movimentacao_id_index');
                });
            }
        }

        // índices em movimentacaocontratocontas
        $indexesEncontrados = $schemaManager->listTableIndexes('movimentacaocontratocontas');
        if(array_key_exists("movimentacaocontratocontas_contratoconta_id_index", $indexesEncontrados)){
            if(Schema::hasColumn('movimentacaocontratocontas', 'contratoconta_id')){
                Schema::table('movimentacaocontratocontas', function (Blueprint $table) {
                    $table->dropIndex('movimentacaocontratocontas_contratoconta_id_index');
                });
            }
        }

        // índices em contratoterceirizados
        $indexesEncontrados = $schemaManager->listTableIndexes('contratoterceirizados');
        if(array_key_exists("contratoterceirizados_contrato_id_index", $indexesEncontrados)){
            if(Schema::hasColumn('contratoterceirizados', 'contrato_id')){
                Schema::table('contratoterceirizados', function (Blueprint $table) {
                    $table->dropIndex('contratoterceirizados_contrato_id_index');
                });
            }
        }

        // índices em contratoresponsaveis
        $indexesEncontrados = $schemaManager->listTableIndexes('contratoresponsaveis');
        // contrato_id
        if(array_key_exists("contratoresponsaveis_contrato_id_index", $indexesEncontrados)){
            if(Schema::hasColumn('contratoresponsaveis', 'contrato_id')){
                Schema::table('contratoresponsaveis', function (Blueprint $table) {
                    $table->dropIndex('contratoresponsaveis_contrato_id_index');
                });
            }
        }
        // user_id
        if(array_key_exists("contratoresponsaveis_user_id_index", $indexesEncontrados)){
            if(Schema::hasColumn('contratoresponsaveis', 'user_id')){
                Schema::table('contratoresponsaveis', function (Blueprint $table) {
                    $table->dropIndex('contratoresponsaveis_user_id_index');
                });
            }
        }

    }
}
