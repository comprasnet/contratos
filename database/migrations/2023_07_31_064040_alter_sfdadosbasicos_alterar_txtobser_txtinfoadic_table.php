<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSfdadosbasicosAlterarTxtobserTxtinfoadicTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sfdadosbasicos', function (Blueprint $table) {
            $table->string('txtobser', 468)->change();
            $table->text('txtinfoadic')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sfdadosbasicos', function (Blueprint $table) {
            $table->string('txtobser', 255)->change();
            $table->string('txtinfoadic', 255)->change();
        });
    }
}
