<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterContratofaturasEmpenhoApropriacaoAddNaturezasubitemId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contrato_fatura_empenhos_apropriacao', function (Blueprint $table) {
            $table->integer('subelemento_id')->nullable();
            $table->foreign('subelemento_id')->references('id')->on('naturezasubitem')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contrato_fatura_empenhos_apropriacao', function (Blueprint $table) {
            $table->dropColumn('subelemento_id');
        });
    }
}
