<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsComValorToContratohistoricoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contratohistorico', function (Blueprint $table) {
            $table->boolean('is_com_valor')->default(1)->after('data_inicio_novo_valor')->comment(
                'Nova coluna que visa resolver o card 245,
                que permitirá termo aditivo - qualificação: vigência sem valor'
            );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contratohistorico', function (Blueprint $table) {
            $table->dropColumn(['is_com_valor']);
        });
    }
}
