<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatmatclassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catmatclasses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('catmatsergrupo_id');
            $table->string('codigo')->unique();
            $table->string('descricao');
            $table->timestamps();

            $table->foreign('catmatsergrupo_id')->references('id')->on('catmatsergrupos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catmatclasses');
    }
}
