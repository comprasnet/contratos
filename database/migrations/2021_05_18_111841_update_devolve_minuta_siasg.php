<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDevolveMinutaSiasg extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('devolve_minuta_siasg')
            ->join('minutaempenhos_remessa', 'minutaempenhos_remessa.minutaempenho_id', '=', 'devolve_minuta_siasg.minutaempenho_id')
            ->where('minutaempenhos_remessa_id', '=', null)
            ->update([
                'minutaempenhos_remessa_id' => DB::raw('minutaempenhos_remessa.id::integer')
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
