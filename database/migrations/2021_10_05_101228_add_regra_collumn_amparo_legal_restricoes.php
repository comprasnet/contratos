<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRegraCollumnAmparoLegalRestricoes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('amparo_legal_restricoes', function (Blueprint $table) {
            $table->string('regra')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('amparo_legal_restricoes', function (Blueprint $table) {
            $table->dropColumn('regra');
        });
    }
}
