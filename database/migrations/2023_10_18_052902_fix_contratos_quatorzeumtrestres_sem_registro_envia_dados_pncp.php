<?php

use App\Http\Controllers\Api\PNCP\ContratoControllerPNCP;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \App\Models\Contrato;
use \App\Models\Contratohistorico;
use \App\Http\Traits\EnviaPncpTrait;
use \App\Models\EnviaDadosPncp;
use \App\Http\Controllers\Api\PNCP\DocumentoContratoController;
use \App\Http\Controllers\Api\PNCP\DocumentoTermoContratoController;

class FixContratosQuatorzeumtrestresSemRegistroEnviaDadosPncp extends Migration
{
    use EnviaPncpTrait;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $contratosSemRegistroEnviaDadosPNCP =
            Contrato::leftjoin('envia_dados_pncp', function ($query) {
                $query->on('envia_dados_pncp.contrato_id', 'contratos.id')
                    ->whereNull('envia_dados_pncp.deleted_at');
            })
            ->join('amparo_legal_contrato', 'contratos.id', 'amparo_legal_contrato.contrato_id')
            ->join('amparo_legal', 'amparo_legal.id', 'amparo_legal_contrato.amparo_legal_id')
            ->join('unidades', function ($query) {
                $query->on('unidades.id', 'contratos.unidade_id')
                    ->where('unidades.sigilo', false);
            })
            ->join('codigoitens', 'codigoitens.id', 'contratos.tipo_id')
            ->join('compras', function ($query) {
                $query->on('contratos.licitacao_numero', 'compras.numero_ano')
                    ->whereColumn('contratos.unidadecompra_id', 'compras.unidade_origem_id')
                    ->whereColumn('contratos.modalidade_id', 'compras.modalidade_id');
            })
            ->whereNull('envia_dados_pncp.contrato_id')
            ->where('amparo_legal.ato_normativo', 'ilike', '%14.133%')
            ->whereNotIn('codigoitens.descricao', ['Outros', 'Credenciamento'])
            ->select('contratos.*')
            ->get();

        try {
            DB::beginTransaction();
            foreach ($contratosSemRegistroEnviaDadosPNCP as $contrato) {
                $contratoHistorico = Contratohistorico::where("contrato_id", $contrato->id)->oldest()->first();

                if(empty($contratoHistorico)) {
                    $mensagem = "Contrato com ID {$contrato->id} sem contrato histórico";
                    \Illuminate\Support\Facades\Log::info($mensagem);
                    continue;
                }

                $this->preparaEnvioPNCP(
                    $contratoHistorico->id,
                    $contratoHistorico->contrato_id,
                    $contratoHistorico->tipo->descricao,
                    true
                );
            }
            DB::commit();
        } catch (Exception $ex) {
            DB::rollBack();
            $mensagem = "Erro ao rodar a migration
            2023_10_18_052902_fix_contratos_quatorzeumtrestres_sem_registro_envia_dados_pncp
            {$ex->getTraceAsString()}";

            \Illuminate\Support\Facades\Log::error($mensagem);
        }

        $dcc = new DocumentoContratoController();
        $dtcc = new DocumentoTermoContratoController();
        $cc = new ContratoControllerPNCP();
        try {
            foreach ($contratosSemRegistroEnviaDadosPNCP as $contrato) {
                $contratoHistorico = Contratohistorico::where("contrato_id", $contrato->id)->oldest()->first();

                if(empty($contratoHistorico)) {
                    $mensagem = "Contrato com ID {$contrato->id} sem contrato histórico";
                    \Illuminate\Support\Facades\Log::info($mensagem);
                    continue;
                }

                $enviaDadosPNCP = EnviaDadosPncp::whereHas('status', function ($ci) {
                    $ci->where('descres', 'INCPEN');
                })->where('pncpable_type', Contratohistorico::class)
                    ->where('pncpable_id', $contratoHistorico->id)
                    ->first();

                if (empty($enviaDadosPNCP)) {
                    $enviaDadosPNCP = $this->manipularSituacaoPNCPNovo(
                        $contratoHistorico->id,
                        Contratohistorico::class,
                        'INCPEN',
                        $contrato->id);
                }

                $this->inserirContratoPNCPNovo($contratoHistorico, $enviaDadosPNCP, $cc, $dcc, $dtcc);
            }
        } catch (Exception $ex) {
            $mensagem = "Erro ao enviar PNCP {$ex->getMessage()}";
            \Illuminate\Support\Facades\Log::error($mensagem);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
