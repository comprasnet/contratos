<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAddColumnCronogramaAutomatico extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('contratos', function (Blueprint $table) {
            $table->boolean('cronograma_automatico')->nullable()->default(true);
        });

        Schema::table('contratohistorico', function (Blueprint $table) {
            $table->boolean('cronograma_automatico')->nullable()->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contratos', function ($table) {
            $table->dropColumn('cronograma_automatico');
        });
        Schema::table('contratohistorico', function ($table) {
            $table->dropColumn('cronograma_automatico');
        });
    }
}
