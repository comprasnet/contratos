<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \App\Models\EnviaDadosPncp;

class FixContratoAmparoLegal14133LeiDiferente14133 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $enviaDadosPNCPCompraLeiDiferenteQuatrozeUmTresTres =
            EnviaDadosPncp::where("retorno_pncp", "ilike", "%Compra da lei%")->get();

        foreach ($enviaDadosPNCPCompraLeiDiferenteQuatrozeUmTresTres as $enviaDadosPNCP) {
            $enviaDadosPNCP->deleted_at = \Carbon\Carbon::now()->format('Y-m-d H:i:s');
            $enviaDadosPNCP->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
