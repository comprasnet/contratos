<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterContratoitensAlterTipoQuantidade extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contratoitens', function (Blueprint $table) {
            $table->float('quantidade')->change();
        });
        Schema::table('saldohistoricoitens', function (Blueprint $table) {
            $table->float('quantidade')->change();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contratoitens', function (Blueprint $table) {
            $table->decimal('quantidade',15,5)->change();
        });
        Schema::table('saldohistoricoitens', function (Blueprint $table) {
            $table->decimal('quantidade',19,4)->change();
        });
    }
}
