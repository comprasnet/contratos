<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDataPropostaComercialContratosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('contratos', 'data_proposta_comercial')){
            Schema::table('contratos', function (Blueprint $table) {
                $table->date('data_proposta_comercial')->nullable();
            });
        }
        if(!Schema::hasColumn('contratohistorico', 'data_proposta_comercial')){
            Schema::table('contratohistorico', function (Blueprint $table) {
                $table->date('data_proposta_comercial')->nullable();
            });
        }
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('contratos', 'data_proposta_comercial')){
            Schema::table('contratos', function ($table) {
                $table->dropColumn('data_proposta_comercial');
            });
        }
        if(Schema::hasColumn('contratohistorico', 'data_proposta_comercial')){
            Schema::table('contratohistorico', function ($table) {
                $table->dropColumn('data_proposta_comercial');
            });
        }
    }
}
