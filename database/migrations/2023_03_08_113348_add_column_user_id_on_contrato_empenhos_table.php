<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnUserIdOnContratoEmpenhosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('contratoempenhos', 'user_id')){
            Schema::table('contratoempenhos', function (Blueprint $table) {
                $table->integer('user_id')->nullable();
                $table->foreign('user_id', 'user_id')->references('id')->on('users')->nullable()->default(null);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contratoempenhos', function (Blueprint $table) {
            $table->dropColumn(['user_id']);
        });
    }
}
