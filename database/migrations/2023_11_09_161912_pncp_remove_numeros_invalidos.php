<?php

use App\Models\Contratohistorico;
use App\Models\EnviaDadosPncp;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PncpRemoveNumerosInvalidos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $contratos = Contratohistorico::select('envia_dados_pncp.id as idenviapncp','unidades.codigo', 'codigoitens.descricao', 'contratohistorico.*')
            ->join('compras', function ($join) {
                $join->on('contratohistorico.licitacao_numero', '=', 'compras.numero_ano')
                    ->on('contratohistorico.unidadecompra_id', '=', 'compras.unidade_origem_id')
                    ->on('contratohistorico.modalidade_id', '=', 'compras.modalidade_id');
            })
            ->join('envia_dados_pncp', function ($join) {
                $join->on('contratohistorico.id', '=', 'envia_dados_pncp.pncpable_id')
                    ->where('envia_dados_pncp.pncpable_type', '=', 'App\Models\Contratohistorico');
            })
            ->join('codigoitens', 'contratohistorico.tipo_id', '=', 'codigoitens.id')
            ->join('unidades', function ($join) {
                $join->on('contratohistorico.unidade_id', '=', 'unidades.id')
                    ->where('unidades.sigilo', '=', false);
            })
            ->where('retorno_pncp', 'ilike', '%Ano do contrato deve ser igual ou posterior a 2021%')
            ->get();

        $arrIdsErro = [];
        $arrIdsRemover = [];
        foreach ($contratos as $contrato){
            $numero = explode('/',$contrato->numero);
            if(count($numero)==2 and ((int)$numero[1]>2015 and (int)$numero[1]<2021)){
                $arrIdsRemover[] = $contrato->idenviapncp;
            }else{
                $arrIdsErro[] = $contrato->idenviapncp;
            }
        }

        EnviaDadosPncp::query()->whereIn('id',$arrIdsErro)
            ->update(['retorno_pncp'=>'{"message":"Ano número do contrato deve ser igual ou posterior a 2021.","error":"422 UNPROCESSABLE_ENTITY"}']);

        EnviaDadosPncp::query()->whereIn('id',$arrIdsRemover)->delete();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
