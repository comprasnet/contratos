<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDadosEncerramentoContaVinculada2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('contratocontas', 'saldo_encerramento')){
            Schema::table('contratocontas', function (Blueprint $table) {
                $table->decimal('saldo_encerramento',15,2)->nullable();
            });
        }
        if(!Schema::hasColumn('contratocontas', 'nome_responsavel_encerramento')){
            Schema::table('contratocontas', function (Blueprint $table) {
                $table->string('nome_responsavel_encerramento')->nullable();
            });
        }
        if(!Schema::hasColumn('contratocontas', 'cpf_responsavel_encerramento')){
            Schema::table('contratocontas', function (Blueprint $table) {
                $table->string('cpf_responsavel_encerramento')->nullable();
            });
        }
        if(!Schema::hasColumn('contratocontas', 'is_todos_empregados_rescindidos')){
            Schema::table('contratocontas', function (Blueprint $table) {
                $table->boolean('is_todos_empregados_rescindidos')->default(false)->nullable()->comment('Informação necessária para encerramento da conta depósito vinculada');
            });
        }
        if(!Schema::hasColumn('contratocontas', 'is_contratada_cumpriu_todas_obrigacoes_trabalhistas')){
            Schema::table('contratocontas', function (Blueprint $table) {
                $table->boolean('is_contratada_cumpriu_todas_obrigacoes_trabalhistas')->default(false)->nullable()->comment('Informação necessária para encerramento da conta depósito vinculada');
            });
        }
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contratocontas', function ($table) {
            $table->dropColumn('saldo_encerramento');
            $table->dropColumn('nome_responsavel_encerramento');
            $table->dropColumn('cpf_responsavel_encerramento');
            $table->dropColumn(['is_todos_empregados_rescindidos']);
            $table->dropColumn(['is_contratada_cumpriu_todas_obrigacoes_trabalhistas']);
        });
    }
}
