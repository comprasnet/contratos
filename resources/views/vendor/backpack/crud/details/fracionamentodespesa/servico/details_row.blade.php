<div class="m-t-10 m-b-10 p-l-10 p-r-10 p-t-10 p-b-10 details">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <table class="box table table-striped table-hover display responsive nowrap m-t-0 dataTable dtr-inline">
                <thead>
                <tr role="row">
                    @foreach($thead as $th)
                        @if($th == 'Tipo Empenho')
                            <th style="text-align: right">
                                {{$th}}
                            </th>
                        @else
                            <th>
                                {{$th}}
                            </th>
                        @endif
                    @endforeach
                </tr>
                </thead>
                <tbody>
                @foreach($itens as $item)
                    <tr role="row">
                        <td align="left">
                            {{ \Illuminate\Support\Str::limit($item['codigo_siasg'].' - '.$item['descricaodetalhada'], 70) }}
                            @if (strlen($item['codigo_siasg'].' - '.$item['descricaodetalhada']) > 70)
                                <span>
                                        <i class="fa fa-info-circle" title="{{ $item['codigo_siasg'].' - '.$item['descricaodetalhada'] }}"></i>
                                </span>
                            @endif
                        </td>
                        <td align="left">{{$item['mensagem_siafi']}}</td>
                        <td align="left">{{$item['codigo'] .' - '. $item['unidadenome']}}</td>
                        <td align="left">R$ {{$item['valor']}}</td>
                        <td align="right">{{$item['tipo_minuta']}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="clearfix"></div>

