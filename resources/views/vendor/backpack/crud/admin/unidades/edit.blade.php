@extends('backpack::layout')

@section('header')
	<section class="content-header">
	  <h1>
        <span class="text-capitalize">{!! $crud->getHeading() ?? $crud->entity_name_plural !!}</span>
        <small>{!! $crud->getSubheading() ?? trans('backpack::crud.edit').' '.$crud->entity_name !!}.</small>
	  </h1>
	  <ol class="breadcrumb">
	    <li><a href="{{ url(config('backpack.base.route_prefix'),'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
	    <li><a href="{{ url($crud->route) }}" class="text-capitalize">{{ $crud->entity_name_plural }}</a></li>
	    <li class="active">{{ trans('backpack::crud.edit') }}</li>
	  </ol>
	</section>
@endsection

@section('content')

@if ($crud->hasAccess('list'))
    @if(Route::current()->getName() == 'crud.feriado.edit')
        <a href="{{ $crud->route }}" class="hidden-print"><i class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span>{{ $crud->entity_name_plural }}</span></a>
    @elseif(Route::current()->getName() == 'crud.amparolegal.edit')
        <a href="{{ $crud->route }}" class="hidden-print"><i class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span>{{ $crud->entity_name_plural }}</span></a>
    @else
    	<a href="{{ starts_with(URL::previous(), url($crud->route)) ? URL::previous() : url($crud->route) }}" class="hidden-print"><i class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span>{{ $crud->entity_name_plural }}</span></a>
    @endif
@endif

<div class="row m-t-20">
	<div class="{{ $crud->getEditContentClass() }}">
		<!-- Default box -->

		@include('crud::inc.grouped_errors')

		  <form method="post"
		  		action="{{ url($crud->route.'/'.$entry->getKey()) }}"
				@if ($crud->hasUploadFields('update', $entry->getKey()))
				enctype="multipart/form-data"
				@endif
		  		>
		  {!! csrf_field() !!}
		  {!! method_field('PUT') !!}
		  <div class="col-md-12">
		  	@if ($crud->model->translationEnabled())
		    <div class="row m-b-10">
		    	<!-- Single button -->
				<div class="btn-group pull-right">
				  <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    {{trans('backpack::crud.language')}}: {{ $crud->model->getAvailableLocales()[$crud->request->input('locale')?$crud->request->input('locale'):App::getLocale()] }} &nbsp; <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu">
				  	@foreach ($crud->model->getAvailableLocales() as $key => $locale)
					  	<li><a href="{{ url($crud->route.'/'.$entry->getKey().'/edit') }}?locale={{ $key }}">{{ $locale }}</a></li>
				  	@endforeach
				  </ul>
				</div>
		    </div>
		    @endif
		    <div class="row display-flex-wrap">
		      <!-- load the view from the application if it exists, otherwise load the one in the package -->
		      @if(view()->exists('vendor.backpack.crud.form_content'))
		      	@include('vendor.backpack.crud.form_content', ['fields' => $fields, 'action' => 'edit'])
		      @else
		      	@include('crud::form_content', ['fields' => $fields, 'action' => 'edit'])
		      @endif
		    </div><!-- /.box-body -->

            <div class="">
                @if(Route::current()->getName() == 'crud.instrumentoinicial.edit')
                    @include('crud::inc.form_aba_buttons_instrumentoinicial')
                @elseif(Route::current()->getName() == 'crud.aditivos.edit')
                    @include('crud::inc.form_aba_buttons_aditivo')
                @elseif(Route::current()->getName() == 'crud.apostilamentos.edit')
                    @include('crud::inc.form_aba_buttons_apostilamentos')
                @else
                    @include('crud::inc.form_save_buttons')
                @endif
		    </div><!-- /.box-footer-->
		  </div><!-- /.box -->
		  </form>
	</div>
</div>
{{-- MODELO DOCUMENTOS - RELACIONA DADOS CONTRATO --}}
@if(Route::current()->getName() == 'crud.modelodocumento.edit')
	@include('crud::recomposicao.modelodocumento.relaciona_dados_contrato_modelo_documento',['ckeditor_field_name' => 'ckeditor-template'])
@endif
<!-- JS para a pagina a routa crud.faturas.create -->
@if(Route::current()->getName() == 'crud.faturas.edit')
    <script src="{{ asset('vendor/backpack/crud/js/contrato_fatura.js') }}"></script>
@endif
@endsection
@push('after_scripts')
<script type="text/javascript">
    $(document).ready(function () {
        let utilizaSiafi = textoOptionSelecionado = $('select[name="utiliza_siafi"]').find('option:selected').text();

        // Muda o valor de utiliza antecipagov dependendo do valor que vem carregado
        // para is-sisg
        if($("#is-sisg").val() == 1 && utilizaSiafi == 'Sim'){
            $("#utiliza-antecipagov").val(1);
            $("#utiliza-antecipagov").attr('readonly', true);
            $("#utiliza-antecipagov").on('mousedown', function(e) {
                e.preventDefault();
            })
        }

        var utilizaCustos = $('select[name="utiliza_siafi"]').val()

        if (utilizaCustos == 1) {
            $('select[name="utiliza_custos"]').val(1)
                .attr('readonly', true)
                .on('mousedown', function (e) {
                    e.preventDefault();
                });
        } else {
            $('select[name="utiliza_custos"]').removeAttr('readonly', false)
                .off('mousedown')
                .val(0);
        }

        // Se a unidade for sisg e utilizar siafi, então ela tem que usar o antecipa gov
        $("#is-sisg").change(function () {
            let utilizaSiafi = textoOptionSelecionado = $('select[name="utiliza_siafi"]').find('option:selected').text();

            if ($(this).val() == 1 && utilizaSiafi == "Sim") {
                $("#utiliza-antecipagov").val(1);
                $("#utiliza-antecipagov").attr('readonly', true);
                $("#utiliza-antecipagov").on('mousedown', function (e) {
                    e.preventDefault(); // Impede a ação padrão do clique
                });

            } else {
                $("#utiliza-antecipagov").removeAttr('readonly', false);
                $("#utiliza-antecipagov").off('mousedown');
                $("#utiliza-antecipagov").val(0);
            }
        });

        $('select[name="utiliza_siafi"]').change(function () {

            if($(this).val() == 1){
                $('select[name="utiliza_custos"]').val(1)
                    .attr('readonly', true)
                    .on('mousedown', function(e) {
                        e.preventDefault();
                    });
            }else{
                $('select[name="utiliza_custos"]').removeAttr('readonly', false)
                    .off('mousedown')
                    .val(0);
            }

            if ($(this).val() == 1 && $("#is-sisg").val() == 1) {
                $("#utiliza-antecipagov").val(1);
                $("#utiliza-antecipagov").attr('readonly', true);
                $("#utiliza-antecipagov").on('mousedown', function(e) {
                    e.preventDefault(); // Impede a ação padrão do clique
                });

            } else {
                $("#utiliza-antecipagov").removeAttr('readonly', false);
                $("#utiliza-antecipagov").off('mousedown');
                $("#utiliza-antecipagov").val(0);
            }
        });
    });
    setInterval(function () {
        if($("#is-sisg").val() == 1) {
            $("#select2_ajax_multiple_unidadesexecutorasfinanceiras")
                .prop("disabled", true);
        }
        if($("#is-sisg").val() == 0) {
            $("#select2_ajax_multiple_unidadesexecutorasfinanceiras")
                .prop("disabled", false);
        }
    }, 500);
</script>
@endpush
