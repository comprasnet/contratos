@extends('backpack::layout')

@section('header')
	<section class="content-header">
	  <h1>
        <span >{!! $crud->getHeading() ?? $crud->entity_name_plural !!}</span>
        <small>{!! $crud->getSubheading() ?? trans('backpack::crud.add').' '.$crud->entity_name !!}.</small>
	  </h1>
	  <ol class="breadcrumb">
	    <li><a href="{{ url(config('backpack.base.route_prefix'), 'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
	    <li><a href="{{ url($crud->route) }}" class="">{{ $crud->entity_name_plural }}</a></li>
	    <li class="active">{{ trans('backpack::crud.add') }}</li>
	  </ol>
	</section>
@endsection

@section('content')
@if ($crud->hasAccess('list'))
    <a href="{{ url($crud->route) }}" class="hidden-print"><i class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span>{{ $crud->entity_name_plural }}</span></a><br><br>
@endif

<div class="row m-t-20">
	<div class="{{ $crud->getCreateContentClass() }}">
		<!-- Default box -->

		@include('crud::inc.grouped_errors')

		  <form method="post"
		  		action="{{ url($crud->route) }}"
				@if ($crud->hasUploadFields('create'))
				enctype="multipart/form-data"
				@endif
		  		>
		  {!! csrf_field() !!}
		  <div class="col-md-12">

		    <div class="row display-flex-wrap">
		      <!-- load the view from the application if it exists, otherwise load the one in the package -->
		      @if(view()->exists('vendor.backpack.crud.form_content'))
		      	@include('vendor.backpack.crud.form_content', [ 'fields' => $crud->getFields('create'), 'action' => 'create' ])
		      @else
		      	@include('crud::form_content', [ 'fields' => $crud->getFields('create'), 'action' => 'create' ])
		      @endif
		    </div><!-- /.box-body -->
		    <div class="">
            @if(Route::current()->getName() == 'crud.contrato.create')
                @include('crud::inc.form_aba_buttons')
            @elseif(Route::current()->getName() == 'crud.aditivos.create')
                    @include('crud::inc.form_aba_buttons_aditivo')
            @elseif(Route::current()->getName() == 'crud.apostilamentos.create')
                    @include('crud::inc.form_aba_buttons_apostilamentos')
            @elseif(Route::current()->getName() == 'crud.rescisao.create')
                    @include('crud::inc.form_aba_buttons_rescisao')
            @elseif(Route::current()->getName() == 'crud.retiradacontratoconta.create')
                @include('crud::inc.form_retiradacontratoconta_buttons')
            @elseif(Route::current()->getName() == 'crud.repactuacaocontratoconta.create')
                @include('crud::inc.form_repactuacaocontratoconta_buttons')
            @elseif(Route::current()->getName() == 'crud.depositocontratoconta.create')
                @include('crud::inc.form_depositocontratoconta_buttons')
            @else
                @include('crud::inc.form_save_buttons')
            @endif

			{{-- MODELO DOCUMENTOS - RELACIONA DADOS CONTRATO --}}
			@if(Route::current()->getName() == 'crud.modelodocumento.create')
				@include('crud::recomposicao.modelodocumento.relaciona_dados_contrato_modelo_documento',['ckeditor_field_name' => 'ckeditor-template'])
			@endif


            </div><!-- /.box-footer-->

		  </div><!-- /.box -->
		  </form>
	</div>
</div>
@endsection
@push('after_scripts')
<script type="text/javascript">
    $(document).ready(function () {

        // Se a unidade for sisg e utilizar siafi, então ela tem que usar o antecipa gov
        $("#is-sisg").change(function () {
            let utilizaSiafi = textoOptionSelecionado = $('select[name="utiliza_siafi"]').find('option:selected').text();

            if ($(this).val() == 1 && utilizaSiafi == "Sim") {
                $("#utiliza-antecipagov").val(1);
                $("#utiliza-antecipagov").attr('readonly', true);
                $("#utiliza-antecipagov").on('mousedown', function(e) {
                    e.preventDefault(); // Impede a ação padrão do clique
                });
            } else {
                $("#utiliza-antecipagov").removeAttr('readonly', false);
                $("#utiliza-antecipagov").off('mousedown');
                $("#utiliza-antecipagov").val(0);
            }
        });

        $('select[name="utiliza_siafi"]').change(function () {

            if($(this).val() == 1){
                $('select[name="utiliza_custos"]').val(1)
                    .attr('readonly', true)
                    .on('mousedown', function(e) {
                        e.preventDefault();
                    });
            }else{
                $('select[name="utiliza_custos"]').removeAttr('readonly', false)
                    .off('mousedown')
                    .val(0);
            }

            if ($(this).val() == 1 && $("#is-sisg").val() == 1) {
                $("#utiliza-antecipagov").val(1);
                $("#utiliza-antecipagov").attr('readonly', true);
                $("#utiliza-antecipagov").on('mousedown', function(e) {
                    e.preventDefault(); // Impede a ação padrão do clique
                });

            } else {
                $("#utiliza-antecipagov").removeAttr('readonly', false);
                $("#utiliza-antecipagov").off('mousedown');
                $("#utiliza-antecipagov").val(0);
            }
        });
    });
    setInterval(function () {
        if($("#is-sisg").val() == 1) {
            $("#select2_ajax_multiple_unidadesexecutorasfinanceiras")
                .prop("disabled", true);
        }
        if($("#is-sisg").val() == 0) {
            $("#select2_ajax_multiple_unidadesexecutorasfinanceiras")
                .prop("disabled", false);
        }
    }, 500);
</script>
@endpush
