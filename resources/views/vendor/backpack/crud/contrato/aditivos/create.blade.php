@extends('backpack::layout')

@section('header')
    <section class="content-header">
        <h1>
            <span class="text-capitalize">{!! $crud->getHeading() ?? $crud->entity_name_plural !!}</span>
            <small>{!! $crud->getSubheading() ?? trans('backpack::crud.add').' '.$crud->entity_name !!}.</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url(config('backpack.base.route_prefix'), 'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
            <li><a href="{{ url($crud->route) }}" class="text-capitalize">{{ $crud->entity_name_plural }}</a></li>
            <li class="active">{{ trans('backpack::crud.add') }}</li>
        </ol>
    </section>
@endsection

@section('content')
    @if ($crud->hasAccess('list'))
        <a href="{{ url($crud->route) }}" class="hidden-print"><i class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span>{{ $crud->entity_name_plural }}</span></a><br><br>
    @endif

    <div class="row m-t-20">
        <div class="{{ $crud->getCreateContentClass() }}">
            <!-- Default box -->

            @include('crud::inc.grouped_errors')

            <form method="post"
                  action="{{ url($crud->route) }}"
                  @if ($crud->hasUploadFields('create'))
                  enctype="multipart/form-data"
                @endif
            >
                {!! csrf_field() !!}
                <div class="col-md-12">

                    <div class="row display-flex-wrap">
                        <!-- load the view from the application if it exists, otherwise load the one in the package -->
                        @if(view()->exists('vendor.backpack.crud.form_content'))
                            @include('vendor.backpack.crud.form_content', [ 'fields' => $crud->getFields('create'), 'action' => 'create' ])
                        @else
                            @include('crud::form_content', [ 'fields' => $crud->getFields('create'), 'action' => 'create' ])
                        @endif
                    </div><!-- /.box-body -->
                    <div class="">
                        @if(Route::current()->getName() == 'crud.contrato.create')
                            @include('crud::inc.form_aba_buttons')
                        @elseif(Route::current()->getName() == 'crud.aditivos.create')
                            @include('crud::inc.form_aba_buttons_aditivo')
                        @elseif(Route::current()->getName() == 'crud.apostilamentos.create')
                            @include('crud::inc.form_aba_buttons_apostilamentos')
                        @elseif(Route::current()->getName() == 'crud.rescisao.create')
                            @include('crud::inc.form_aba_buttons_rescisao')
                        @elseif(Route::current()->getName() == 'crud.retiradacontratoconta.create')
                            @include('crud::inc.form_retiradacontratoconta_buttons')
                        @elseif(Route::current()->getName() == 'crud.repactuacaocontratoconta.create')
                            @include('crud::inc.form_repactuacaocontratoconta_buttons')
                        @elseif(Route::current()->getName() == 'crud.depositocontratoconta.create')
                            @include('crud::inc.form_depositocontratoconta_buttons')
                        @else
                            @include('crud::inc.form_save_buttons')
                        @endif
                    </div><!-- /.box-footer-->

                </div><!-- /.box -->
            </form>
        </div>
    </div>

@endsection
@push('after_scripts')
    <script type="text/javascript">
        $(document).ready(function() {

          //Qualificação -> O Campo Tipo (Com Valor e Sem valor) deverá ser preenchido quando qualificação for:Vigencia
          $("#select2_ajax_multiple_qualificacoes").change(function(){

              if ( $("#select2_ajax_multiple_qualificacoes option:selected").text().search("VIGÊNCIA") !== -1) {
                  $("input[name='is_com_valor']").removeAttr('disabled');
                  return;
              }
              $("#is_com_valor_1").prop("checked", true);
              $("input[name='is_com_valor']").attr('disabled', 'disabled');
          });

        });

        //Caso seja - "Reajuste" ou "Vigência com Valor" -> libera campo data_inicio_novo_valor; Caso nenhum dos dois desabilite.
        setInterval(function(){

            valorverifica = $("#select2_ajax_multiple_qualificacoes").val();
            radiodesabilitado = $("input[name='is_com_valor']").is(':disabled');
            valorradio = $("input[name='is_com_valor']:checked").val();
            var contador = 0;

            //verificação quando volta para tela com erros e mensagens
            if($("#select2_ajax_multiple_qualificacoes option:selected").text().search("VIGÊNCIA") !== -1) {
                $("input[name='is_com_valor']").removeAttr('disabled');
            }

            if(!radiodesabilitado) {
                //valor com vigência - habilita inicio novo valor
                if(valorradio == 1) {
                    contador++;
                }
                //valor sem vigência - deshabilita inicio novo valor
                if(valorradio == 0) {
                    contador = 0;
                }
            }
            //campo qualificação vazio - //valor com vigência - deshabilita inicio novo valor
            if(valorverifica.length <= 0) {
                contador = 0;
            }
            //qualificação Reajuste habilita inicio novo valor
            if($("#select2_ajax_multiple_qualificacoes option:selected").text().search("REAJUSTE") !== -1) {
                contador++;
            }
            if(contador > 0) {
                $("#data_inicio_novo_valor").removeAttr('disabled');
            }
            if(contador === 0) {
                $("#data_inicio_novo_valor").attr('disabled', 'disabled');
            }

    }, 800);
    </script>
@endpush

