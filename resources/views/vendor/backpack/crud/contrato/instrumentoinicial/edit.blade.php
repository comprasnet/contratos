@extends('backpack::layout')

@section('header')
    <section class="content-header">
        <h1>
            <span class="text-capitalize">{!! $crud->getHeading() ?? $crud->entity_name_plural !!}</span>
            <small>{!! $crud->getSubheading() ?? trans('backpack::crud.edit').' '.$crud->entity_name !!}.</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url(config('backpack.base.route_prefix'),'dashboard') }}">{{ trans('backpack::crud.admin') }}</a>
            </li>
            <li><a href="{{ url($crud->route) }}" class="text-capitalize">{{ $crud->entity_name_plural }}</a></li>
            <li class="active">{{ trans('backpack::crud.edit') }}</li>
        </ol>
    </section>
@endsection

@section('content')

    @if ($crud->hasAccess('list'))
        @if(Route::current()->getName() == 'crud.feriado.edit')
            <a href="{{ $crud->route }}" class="hidden-print"><i
                    class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }}
                <span>{{ $crud->entity_name_plural }}</span></a>
        @elseif(Route::current()->getName() == 'crud.amparolegal.edit')
            <a href="{{ $crud->route }}" class="hidden-print"><i
                    class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }}
                <span>{{ $crud->entity_name_plural }}</span></a>
        @else
            <a href="{{ starts_with(URL::previous(), url($crud->route)) ? URL::previous() : url($crud->route) }}"
               class="hidden-print"><i class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }}
                <span>{{ $crud->entity_name_plural }}</span></a>
        @endif
    @endif

    <div class="row m-t-20">
        <div class="{{ $crud->getEditContentClass() }}">
            <!-- Default box -->

            @include('crud::inc.grouped_errors')

            <form method="post"
                  action="{{ url($crud->route.'/'.$entry->getKey()) }}"
                  @if ($crud->hasUploadFields('update', $entry->getKey()))
                      enctype="multipart/form-data"
                @endif
            >
                {!! csrf_field() !!}
                {!! method_field('PUT') !!}
                <div class="col-md-12">
                    @if ($crud->model->translationEnabled())
                        <div class="row m-b-10">
                            <!-- Single button -->
                            <div class="btn-group pull-right">
                                <button type="button" class="btn btn-sm btn-primary dropdown-toggle"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{trans('backpack::crud.language')}}
                                    : {{ $crud->model->getAvailableLocales()[$crud->request->input('locale')?$crud->request->input('locale'):App::getLocale()] }}
                                    &nbsp; <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    @foreach ($crud->model->getAvailableLocales() as $key => $locale)
                                        <li>
                                            <a href="{{ url($crud->route.'/'.$entry->getKey().'/edit') }}?locale={{ $key }}">{{ $locale }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                    <div class="row display-flex-wrap">
                        <!-- load the view from the application if it exists, otherwise load the one in the package -->
                        @if(view()->exists('vendor.backpack.crud.form_content'))
                            @include('vendor.backpack.crud.form_content', ['fields' => $fields, 'action' => 'edit'])
                        @else
                            @include('crud::form_content', ['fields' => $fields, 'action' => 'edit'])
                        @endif
                    </div><!-- /.box-body -->

                    <div class="">
                        @if(Route::current()->getName() == 'crud.instrumentoinicial.edit')
                            @include('crud::inc.form_aba_buttons_instrumentoinicial')
                        @elseif(Route::current()->getName() == 'crud.aditivos.edit')
                            @include('crud::inc.form_aba_buttons_aditivo')
                        @elseif(Route::current()->getName() == 'crud.apostilamentos.edit')
                            @include('crud::inc.form_aba_buttons_apostilamentos')
                        @else
                            @include('crud::inc.form_save_buttons')
                        @endif
                    </div><!-- /.box-footer-->
                </div><!-- /.box -->
            </form>
        </div>
    </div>
    {{-- MODELO DOCUMENTOS - RELACIONA DADOS CONTRATO --}}
    @if(Route::current()->getName() == 'crud.modelodocumento.edit')
        @include('crud::recomposicao.modelodocumento.relaciona_dados_contrato_modelo_documento',['ckeditor_field_name' => 'ckeditor-template'])
    @endif
@endsection
@push('after_scripts')
    <script>
        var buscaEmpenhoUrl = "{{ route('contrato.buscarempenho') }}";
        var buscarContratoEmpenho = "{{ route('buscarcontratoempenho')}}";
    </script>
    <script src="{{ asset('js') }}/contratosAbaEmpenhos.js"></script>
    <script src="{{ asset('js') }}/contratosToogleLogic.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            toogleCompraBrasilOnLoad()

            // Verifica se o reload da página é devido a algum erro ou não,
            // se não for devido a erro ele limpa o localStorage
            // e busca os empenhos via ajax
            if ($('section .callout-danger').length == 0) {
                clearEmpenhosFromLocalStorage();
                getEmpenhosOnload();
            }
            initAbaEmpenhos();
            // 637 - se o usuário teclar enter, caso esteja no form dos itens do contrato, clicaremos o botão de submit para que os tratamentos do onclick sejam feitos e não tenhamos erros no form em caso de erro na página e retorno.
            $(document).on( "keypress", function(e) {
                if(e.which == 13){
                    var nomeElementoComFoco = document.activeElement.name;
                    if( nomeElementoComFoco == 'tipo_material[]' ||  nomeElementoComFoco == 'qtd_item[]'  ||  nomeElementoComFoco == 'periodicidade[]'  ||  nomeElementoComFoco == 'data_inicio[]' ){
                        $('#btn-submit-itens-contrato').click();
                    }
                }}
            );

            //pegando os valores originais dos campos que serão afetados pelo is_prazo_indefinido
            num_parcelas_original = numeroParcelasOriginal();
            data_vig_fim_original = dataFimOriginal();
            valores_periodicidade = [];


                setTimeout(function () {
                    if ($("input[name='periodicidade[]']").length) {
                        $("input[name='periodicidade[]']").each(function () {
                            //console.log($(this).data('indice'), $(this).val());
                            valores_periodicidade.push([$(this).data('indice'), $(this).val()]);

                        });
                    }
                     //console.log(valores_periodicidade);
                }, 1000);


            confereValorVigFinal();

            observaValorIsPrazoIndefinidoQtdParcelasItem();

            $("input[name='is_prazo_indefinido']").change(function () {
                toogleDesabilitadoVigFinal($(this).val());
            });

            toogleAplicavelDecreto();
        });

        function toogleDesabilitadoVigFinal(valor) {

            if (valor != 0) {
                $("input[name='vigencia_fim']").val(null);
                $("input[name='vigencia_fim']").attr('readonly', true);
                $("input[name='num_parcelas']").val(1);
                $("input[name='num_parcelas']").attr('readonly', true);
                $("input[name='periodicidade[]']").val(1);
                return;
            }
            $("input[name='vigencia_fim']").removeAttr('readonly');
            $("input[name='vigencia_fim']").val(data_vig_fim_original);
            $("input[name='num_parcelas']").removeAttr('readonly');
            $("input[name='num_parcelas']").val(num_parcelas_original);
            voltaOldPeriodicidadeItens(valores_periodicidade);
            return;

        }

        function confereValorVigFinal() {

            if ($("input[name='is_prazo_indefinido']:checked").val() == "1") {
                $("input[name='vigencia_fim']").val(null);
                $("input[name='vigencia_fim']").attr('readonly', true);
                $("input[name='num_parcelas']").val(1);
                $("input[name='num_parcelas']").attr('readonly', true);
            }

        }

        function numeroParcelasOriginal() {

            return $("input[name='num_parcelas']").val();

        }

        function dataFimOriginal() {

            return $("input[name='vigencia_fim']").val();

        }

        function observaValorIsPrazoIndefinidoQtdParcelasItem() {

            setInterval(function () {

                if ($("input[name='periodicidade[]']").length) {

                    if ($("input[name='is_prazo_indefinido']:checked").val() == "1") {
                        $("input[name='periodicidade[]']").attr('readonly', true);
                        return;
                    }
                    $("input[name='periodicidade[]']").removeAttr('readonly');
                    return;
                }
            }, 500);

        }

        function voltaOldPeriodicidadeItens(valores) {
            //console.log(valores);
            if ($("input[name='periodicidade[]']").length) {
                for (let i = 0; i < valores.length; i++) {
                    $("input[data-indice=" + valores[i][0] + "]").val(valores[i][1]);
                    //console.log(valores[i][0]);
                }

            }
        }

        function toogleAplicavelDecreto() {
            if ($('#tipo_id option:selected').text() != 'Contrato' || $('#categoria_id option:selected').text() != 'Mão de Obra') {
                $("input[name='aplicavel_decreto']").attr("disabled",true);
                $("input[name='aplicavel_decreto']").prop( "checked", false );
            } else {
                $("input[name='aplicavel_decreto']").attr("disabled",false);
            }
        }

    </script>
@endpush
