{{--@php dump((get_defined_vars()['__data'])); @endphp--}}
{{--@php dump($crud->operation) @endphp--}}
{{--{{dd( Request::url() )}}--}}

@extends('backpack::layout')
@section('header')
	<section class="content-header">
	  <h1>
        <span class="text-capitalize">{!! $crud->getHeading() ?? $crud->entity_name_plural !!}</span>
        <small>{!! $crud->getSubheading() ?? trans('backpack::crud.add').' '.$crud->entity_name !!}.</small>
	  </h1>
	  <ol class="breadcrumb">
	    <li><a href="{{ url(config('backpack.base.route_prefix'), 'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
	    <li><a href="{{ url($crud->route) }}" class="text-capitalize">{{ $crud->entity_name_plural }}</a></li>
	    <li class="active">{{ trans('backpack::crud.add') }}</li>
	  </ol>
	</section>
@endsection

@section('content')
@if ($crud->hasAccess('list'))
	<a href="{{ starts_with(URL::previous(), url($crud->route)) ? URL::previous() : url($crud->route) }}" class="hidden-print"><i class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span>{{ $crud->entity_name_plural }}</span></a>
@endif

<div class="row m-t-20">
	<div class="{{ $crud->getCreateContentClass() }}">
		<!-- Default box -->

		@include('crud::inc.grouped_errors')

		  <form method="post"
		  		action="{{ url($crud->route) }}"
				@if ($crud->hasUploadFields('create'))
				enctype="multipart/form-data"
				@endif
		  		>
		  {!! csrf_field() !!}
		  <div class="col-md-12">

		    <div class="row display-flex-wrap">
		      <!-- load the view from the application if it exists, otherwise load the one in the package -->
		      @if(view()->exists('vendor.backpack.crud.form_content'))
		      	@include('vendor.backpack.crud.form_content', [ 'fields' => $crud->getFields('create'), 'action' => 'create' ])
		      @else
		      	@include('crud::form_content', [ 'fields' => $crud->getFields('create'), 'action' => 'create' ])
		      @endif
		    </div><!-- /.box-body -->
		    <div class="">
                @include('crud::inc.form_aba_buttons')
            </div><!-- /.box-footer-->

		  </div><!-- /.box -->
		  </form>
	</div>
</div>

@endsection
@push('after_scripts')
    <script>
        // Variável para ser usada a rota dentro do ajax em contratosAbaEmpenhos.js
        var buscaEmpenhoUrl = "{{ route('contrato.buscarempenho') }}";
    </script>
    <script src="{{ asset('js') }}/contratosAbaEmpenhos.js"></script>
    <script src="{{ asset('js') }}/contratosToogleLogic.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            toogleCompraBrasilOnLoad()

            // Verifica se o reload da página é devido a algum erro ou não,
            // se for devido a erro, não limpa o localStorage
            if ($('section .callout-danger').length == 0) {
                clearEmpenhosFromLocalStorage();
            }

            initAbaEmpenhos();
            // 637 - se o usuário teclar enter, caso esteja no form dos itens do contrato, clicaremos o botão de submit para que os tratamentos do onclick sejam feitos e não tenhamos erros no form em caso de erro na página e retorno.
            $(document).on( "keypress", function(e) {
                if(e.which == 13){
                    var nomeElementoComFoco = document.activeElement.name;
                    if( nomeElementoComFoco == 'tipo_material[]' ||  nomeElementoComFoco == 'qtd_item[]'  ||  nomeElementoComFoco == 'periodicidade[]'  ||  nomeElementoComFoco == 'data_inicio[]' ){
                        $('#btn-submit-itens-contrato').click();
                    }
                }}
            );

            observaValorIsPrazoIndefinidoQtdParcelasItem();

            $("input[name='is_prazo_indefinido']").change(function(){
                toogleDesabilitadoVigFinal($(this).val());
            });

            toogleAplicavelDecreto();
        });

        function toogleDesabilitadoVigFinal(valor) {
            if(valor != 0) {
                $("input[name='vigencia_fim']").val(null);
                $("input[name='vigencia_fim']").attr('readonly', true);
                $("input[name='num_parcelas']").val(1);
                $("input[name='num_parcelas']").attr('readonly', true);
                $("input[name='periodicidade[]']").val(1);
                $("input[name='periodicidade[]']").attr('readonly', true);
                return;
            }
            $("input[name='vigencia_fim']").removeAttr('readonly');
            $("input[name='num_parcelas']").val(null);
            $("input[name='num_parcelas']").removeAttr('readonly');
            $("input[name='periodicidade[]']").removeAttr('readonly');
        }

        function observaValorIsPrazoIndefinidoQtdParcelasItem() {
            setInterval(function () {

                if($("input[name='periodicidade[]']").length) {

                    if($("input[name='is_prazo_indefinido']:checked").val() == "1") {
                        $("input[name='periodicidade[]']").val(1);
                        $("input[name='periodicidade[]']").attr('readonly', true);
                        $("input[name='vigencia_fim']").val(null);
                        $("input[name='vigencia_fim']").attr('readonly', true);
                        $("input[name='num_parcelas']").val(1);
                        $("input[name='num_parcelas']").attr('readonly', true);
                        return;
                    }
                    $("input[name='vigencia_fim']").removeAttr('readonly');
                    // $("input[name='num_parcelas']").val(null);
                    $("input[name='num_parcelas']").removeAttr('readonly');
                    $("input[name='periodicidade[]']").removeAttr('readonly');
                    return;
                }
            }, 500);
        }

        function toogleAplicavelDecreto() {
            if ($('#tipo_id option:selected').text() != 'Contrato' || $('#categoria_id option:selected').text() != 'Mão de Obra') {
                $("input[name='aplicavel_decreto']").attr("disabled",true);
                $("input[name='aplicavel_decreto']").prop( "checked", false );
            } else {
                $("input[name='aplicavel_decreto']").attr("disabled",false);
            }
        }

    </script>
@endpush
