
@extends('backpack::layout')

@section('header')
    <section class="content-header">
        <h1>
            <span class="text-capitalize">{!! $crud->getHeading() ?? $crud->entity_name_plural !!}</span>
            <small>{!! $crud->getSubheading() ?? trans('backpack::crud.add').' '.$crud->entity_name !!}.</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url(config('backpack.base.route_prefix'), 'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
            <li><a href="{{ url($crud->route) }}" class="text-capitalize">{{ $crud->entity_name_plural }}</a></li>
            <li class="active">{{ trans('backpack::crud.add') }}</li>
        </ol>
    </section>
@endsection

@section('content')

    @if ($crud->hasAccess('list'))
        <a href="{{ url($crud->route) }}" class="hidden-print"><i class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span>{{ $crud->entity_name_plural }}</span></a><br><br>
    @endif

    <div class="row m-t-20">
        <div class="{{ $crud->getCreateContentClass() }}">
            <!-- Default box -->

            @include('crud::inc.grouped_errors')

            <form method="post"
                  action="{{ url($crud->route) }}"
                  @if ($crud->hasUploadFields('create'))
                  enctype="multipart/form-data"
                @endif
            >
                {!! csrf_field() !!}
                <div class="col-md-12">

                    <div class="row display-flex-wrap">
                        <!-- load the view from the application if it exists, otherwise load the one in the package -->
                        @if(view()->exists('vendor.backpack.crud.form_content'))
                            @include('vendor.backpack.crud.form_content', [ 'fields' => $crud->getFields('create'), 'action' => 'create' ])
                        @else
                            @include('crud::form_content', [ 'fields' => $crud->getFields('create'), 'action' => 'create' ])
                        @endif
                    </div><!-- /.box-body -->
                    <div class="">
                        @include('crud::inc.form_save_buttons')
                    </div><!-- /.box-footer-->

                </div><!-- /.box -->
            </form>
        </div>
    </div>

@endsection
@push('after_scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            //Verifica se é não-sisg e não-judiciário;
            is_choice = $("input[name='is_choice']").val();

            if(is_choice) {
                camposIn();
                $("input[name='is_conta_vinculada_radio']")
                    .change(function(){
                        if($(this).val() == 1) {
                            camposIn();
                        } else {
                            camposCnj();
                        }
                    });
            }
        });
        function camposCnj() {
            $("input[name='is_conta_vinculada_pela_resolucao169_cnj']").val(1);
            $(".cnj").removeAttr('disabled');
            $("input[name='fat_empresa']").attr('disabled', 'disabled');
        }
        function camposIn() {
            $("input[name='is_conta_vinculada_pela_resolucao169_cnj']").val('');
            $("input[name='fat_empresa']").removeAttr('disabled');
            $(".cnj").attr('disabled', 'disabled');
        }
    </script>
@endpush
