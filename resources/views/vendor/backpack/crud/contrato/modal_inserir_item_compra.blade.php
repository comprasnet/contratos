@php
    $filtered =  array_filter($crud->modal_fields , function ($value, $key) {
                return isset($value['modal_inserir_item_compra']) && $value['modal_inserir_item_compra'] ;
            }, ARRAY_FILTER_USE_BOTH);
@endphp


<div id="inserir_item_compra" tabindex="-1" class="modal fade"
     role="dialog"
     aria-labelledby=""
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">
                    Inserir Item Compra
                </h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form method="post" action="*"
                  accept-charset="UTF-8" id="bbllaa">
                {!! csrf_field() !!}
                <div class="modal-body" id="textoModal">

                    @foreach($filtered as $field)
                        @includeWhen(isset($field['type']), 'crud::fields.'.$field['type'], ['field' => $field])
                    @endforeach

                </div>
                <div class="modal-footer">
                    <button class="btn btn-danger" type="submit" data-dismiss="modal">
                        <i class="fa fa-reply"></i>
                        Cancelar
                    </button>
                    <button class="btn btn-success" type="button" data-dismiss="modal"
                    id="btn_inserir_item_compra">
                        <i class="fa fa-save"></i>
                        Incluir
                    </button>

                </div>
            </form>
        </div>
    </div>
</div>

@push('crud_fields_scripts')
    <script type="text/javascript">
        $(document).ready(function () {

            $('body').on('shown.bs.modal', '#inserir_item_compra', function (event) {
                $("#btn_inserir_item_compra").prop("disabled", true);
                $('#select2_ajax_multiple_itens_compra').select2('open');
            })
                /*.on('hidden.bs.modal', '#inserir_item_compra', function (event) {*/
                .on('click', '#btn_inserir_item_compra', function (event) {
                    let itens = $('#select2_ajax_multiple_itens_compra').select2('data');
                    // $('#table-itens tr').remove();

                    let temIdMenosUm = itens.some(objeto => objeto.id === "-1");
                    if (temIdMenosUm) {
                        itens = allOptions;
                    }

                    itens.forEach(function(item) {
                        addLinhaFromCompra(item,true)
                    });

                });

            function addLinhaFromCompra(item,booFromModal = true) {

                var inputs = document.getElementsByName("compra_item_unidade_id[]");
                for (var i = 0; i < inputs.length; i++) {

                    // Não inserir na tabela item já existente
                    if (inputs[i].value == item.compra_item_unidade_id) {
                        return false;
                    }
                }

                var compra_itens_id = [];
                compra_itens_id.push(item.id);
                //verifica se pega a quantidade_autorizada do banco ou a quantidade da modal de item;

                //var qtd = '',
                var qtd = item.quantidade_autorizada,
                    qtdMax = item.quantidade_autorizada,
                    qtdMin = 0 ;


                var vl_unit = item.valor_unitario;
                // se vier data dos dados do contrato preencher com a data default
                var data_inicio = '';
                var periodicidade = 1;

                var vl_total = '';

                let descricaoResumida = item.descricaocatmatseritens.substring(0,50)

                let id = item.id

                let descricaoDetalhada = null

                descricaoDetalhada = item.descricaocatmatseritens
                if(item.descricaodetalhada !== undefined) {
                    descricaoDetalhada = item.descricaodetalhada
                }

                let colTipoMaterial;

                if(item.tipo_item == 'Serviço'){
                    colTipoMaterial = '<td><input class="form-control" type="text" readonly value="Não se aplica"></td>';
                    colTipoMaterial += '<input class="form-control" name="tipo_material[]" type="hidden" readonly value="">';
                }else{
                    consumoSelected = "";
                    permanenteSelected = "";

                    if(item.tipo_material == 'Consumo'){
                        consumoSelected = 'selected';
                    }

                    if(item.tipo_material == 'Permanente'){
                        permanenteSelected = 'selected';
                    }

                    colTipoMaterial = '<td>'
                                    +'<select class="form-control" name="tipo_material[]">'
                                    +'<option value="">Selecione o tipo de material</option>'
                                    +'<option value="Consumo" '+consumoSelected+'>Consumo</option>'
                                    +'<option value="Permanente" '+permanenteSelected+'>Permanente</option></select>'
                                    +'</td>';
                }
                if (item.codigo_ncmnbs == undefined
                    || item.codigo_ncmnbs == null
                    || item.codigo_ncmnbs == ''
                ){
                    item.codigo_ncmnbs = ' - ';
                }

                //coloca classe na tr para diferenciar itens vindos pelo botão da compra dos da minuta
                var newRow = $("<tr class='item-da-compra'>");
                var cols = "";
                cols += '<td>'+item.tipo_item;
                cols += '<input type="hidden" name="numero_item_compra[]" value="' + item.numero + '">';
                cols += '<input type="hidden" name="catmatseritem_id[]" value="' + item.catmatseritem_id + '">';
                cols += '<input type="hidden" name="tipo_item_id[]" value="' + item.tipo_item_id + '">';
                cols += '<input type="hidden" name="compra_item_unidade_id[]" value="' + item.compra_item_unidade_id + '">';
                cols += '<input type="hidden" name="descricao_detalhada[]" value="' + descricaoDetalhada + '">';
                cols += '<input type="hidden" name="saldo_historico_id[]" value="undefined">';
                cols += '<input type="hidden" name="codigo_siasg[]" value="' + item.codigo_siasg + '">';
                cols += '</td>';
                cols += colTipoMaterial;
                cols += '<td>'+item.numero+'</td>';
                cols += '<td title="'+descricaoDetalhada+'">'+item.codigo_siasg
                    + ' - <span id="textoDescricaoCompleta_'+id+'">' +descricaoResumida+'</span> '
                    + montarBotaoMaisDescricao(descricaoDetalhada, descricaoResumida, id)+' </td>';
                cols += '<td>'+item.codigo_ncmnbs+'</td>';
                cols += '<td><input class="form-control validadeMaxMinQtdItem'+item.id+'" type="number"  ' +
                    'name="qtd_item[]" step="0.0001" id="'+item.id+'" max="'+qtdMax+'" min="'+qtdMin+'" value="'+qtd+'"></td>';
                cols += '<td><input class="form-control" type="number" readonly name="vl_unit[]" step="0.0001" value="'+vl_unit+'"></td>';
                cols += `<td><input class="form-control" type="number" name="periodicidade[]" value="${periodicidade}"></td>`;
                cols += '<td><input class="form-control" type="number" readonly  name="vl_total[]" step="0.0001" value="'+vl_total+'"></td>';
                cols += `<td><input class="form-control" type="date" name="data_inicio[]" value="{{date('Y-m-d')}}"></td>`;
                cols += '<td>';
                cols += '<button type="button" class="btn btn-danger" title="Excluir Item" id="remove_item">' +
                    '<i class="fa fa-trash"></i>' +
                    '</button>';
                cols += '</td>';

                newRow.append(cols);
                $("#table-itens").append(newRow);
                $("#"+item.id).change();
                calculaTotalGlobal();

                /***************ATRIBUI EVENTOS DE VALIDAÇÃO PARA QTD_ITEM MAX E MIN***************************/
                var elementQtdItem = document.querySelector(".validadeMaxMinQtdItem"+item.id);
                var funcMaxNumber = maxNumber(qtdMax, qtdMin);
                elementQtdItem.addEventListener('keyup', funcMaxNumber);
                elementQtdItem.addEventListener('blur', funcMaxNumber);
                /***********************************************************************************************/

            }

        });
    </script>
@endpush
