<!-- select2 from ajax multiple -->
@php
        $old_value = old(square_brackets_to_dots($field['name'])) ?? $field['value'] ?? $field['default'] ?? false;
@endphp

<div @include('crud::inc.field_wrapper_attributes') >
    <label>{!! $field['label'] !!}</label>
    @include('crud::inc.field_translatable_icon')
    <select
        name="{{ $field['name'] }}[]"
        style="width: 100%"
        id="select2_ajax_multiple_{{ $field['name'] }}"
        @include('crud::inc.field_attributes', ['default_class' =>  'form-control'])
        multiple>


    </select>

    {{-- HINT --}}
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
</div>


{{-- ########################################## --}}
{{-- Extra CSS and JS for this particular field --}}
{{-- If a field type is shown multiple times on a form, the CSS and JS will only be loaded once --}}

@if ($crud->checkIfFieldIsFirstOfItsType($field))

    {{-- FIELD CSS - will be loaded in the after_styles section --}}
    @push('crud_fields_styles')
        <!-- include select2 css-->
        <link href="{{ asset('vendor/adminlte/bower_components/select2/dist/css/select2.min.css') }}" rel="stylesheet"
              type="text/css"/>
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css"
            rel="stylesheet" type="text/css"/>
    @endpush

    {{-- FIELD JS - will be loaded in the after_scripts section --}}
    @push('crud_fields_scripts')
        <!-- include select2 js-->
        <script src="{{ asset('vendor/adminlte/bower_components/select2/dist/js/select2.min.js') }}"></script>
        <script src="{{ asset('vendor/adminlte/bower_components/select2/dist/js/i18n/pt-BR.js') }}"></script>
    @endpush

@endif

<!-- include field specific select2 js-->
@push('crud_fields_scripts')
    <script>
        jQuery(document).ready(function ($) {
            $("#select2_ajax_multiple_{{ $field['name'] }}").each(function (i, obj) {
                var form = $(obj).closest('form');

                if (!$(obj).hasClass("select2-hidden-accessible")) {
                    $(obj).select2({
                        theme: 'bootstrap',
                        language: 'pt-BR',
                        multiple: true,
                        placeholder: "{{ $field['placeholder'] }}",
                        minimumInputLength: "{{ $field['minimum_input_length'] }}",
                        ajax: {
                            url: "{{ $field['data_source'] }}",
                            type: '{{ $field['method'] ?? 'GET' }}',
                            dataType: 'json',
                            quietMillis: 250,
                            // delay: 250,
                            cache: true,
                            data: function (params) {
                                return {
                                    q: params.term, // search term
                                    page: params.page, // pagination
                                    form: cleanArray(form.serializeArray())
                                };
                            },
                            processResults: function (data, params) {
                                $("#btn_inserir_item_compra").prop("disabled", false);
                                params.page = params.page || 1;

                                if (Object.keys(data.data).length === 0) {
                                    let inputs = document.getElementsByName("compra_item_unidade_id[]");
                                    let count = inputs.length;
                                    let msg = 'Informe o fornecedor e os dados da compra corretamente na aba Dados do Contrato';
                                    let tipoMsg = 'error';

                                    if (count >= 1){
                                        msg = 'Todos os itens disponíveis da compra já foram incluídos';
                                        tipoMsg = 'warning';
                                    }

                                    Swal.fire('Alerta!', 'Nenhum item encontrado', tipoMsg)
                                    return {
                                        results: [{
                                            text: 'Nenhum item encontrado',
                                            id: ''
                                        }],
                                    };
                                }

                                // utilizando o spread operator (...) para copiar o array
                                allOptions = [...data.data];

                                data.data.unshift({
                                    text                    : 'Todos',
                                    id                      : '-1',
                                    descricaodetalhada      : 'Todos',
                                    descricaomodal          : 'Todos'
                                });

                                return {
                                    results: $.map(data.data, function (item) {
                                            return {
                                            text                    : item.descricaomodal,
                                            id                      : item.id,
                                            compra_id               : item.compra_id,
                                            tipo_item_id            : item.tipo_item_id,
                                            catmatseritem_id        : item.catmatseritem_id,
                                            descricaodetalhada      : item.descricaodetalhada,
                                            qtd_total               : item.qtd_total,
                                            numero                  : item.numero,
                                            ata_vigencia_inicio     : item.ata_vigencia_inicio,
                                            ata_vigencia_fim        : item.ata_vigencia_fim,
                                            tipo_item               : item.tipo_item,
                                            tipo_material           : item.tipo_material,
                                            compra_item_unidade_id  : item.compra_item_unidade_id,
                                            quantidade_autorizada   : item.quantidade_autorizada,
                                            quantidade_saldo        : item.quantidade_saldo,
                                            valor_unitario          : item.valor_unitario,
                                            valor_negociado         : item.valor_negociado,
                                            codigo_siasg            : item.codigo_siasg,
                                            descricaocatmatseritens : item.descricaocatmatseritens,
                                            codigo_ncmnbs           : item.codigo_ncmnbs
                                        }
                                    }),
                                    pagination: {
                                        more: data.current_page < data.last_page
                                    }
                                };

                            },
                            error: function (jqXHR, textStatus, errorThrown) {


                                if (textStatus === 'error') {
                                    Swal.fire('Alerta!', 'Informe o fornecedor e os dados da compra corretamente na aba Dados do Contrato', 'error')
                                }

                                if (textStatus === 'abort') {
                                    setTimeout(function() {
                                        $('.select2-results__message').html('Buscando...')

                                    });
                                }
                            }
                        },
                    });

                }
            });

            $('#select2_ajax_multiple_{{ $field['name'] }}').on('select2:unselect', function (e) {
                if (e.params.data.id === '-1') {
                    $('#select2_ajax_multiple_{{ $field['name'] }}').val('').trigger('change');
                }
            });

            // pega todos inputs do formulario e filtra enviando somente os necessarios pois estava excedendo o rage da request
            function cleanArray(arrInput)
            {
                var retorno = [];
                arrInput.forEach(function(item, i) {
                    switch (item.name) {
                        case "numero_contratacao":
                        case 'fornecedor_id':
                        case 'contrato_id':
                        case 'modalidade_id':
                        case 'licitacao_numero':
                        case 'unidadebeneficiaria_id':
                        case 'unidadecompra_id':
                        case 'contrata_mais_brasil':
                            retorno.push({'name':item.name, 'value': item.value});
                            break;
                        case 'numero_item_compra[]':
                            retorno.push({'name':'numero_item_compra', 'value': item.value});
                            break;
                        default:
                            break;
                    }
                });
                return retorno;
            }

            @if (isset($field['dependencies']))
            @foreach (array_wrap($field['dependencies']) as $dependency)
            $('input[name={{ $dependency }}], select[name={{ $dependency }}], checkbox[name={{ $dependency }}], radio[name={{ $dependency }}], textarea[name={{ $dependency }}]').change(function () {
                $("#select2_ajax_multiple_{{ $field['name'] }}").val(null).trigger("change");
            });
            @endforeach
            @endif
        });
    </script>
@endpush
{{-- End of Extra CSS and JS --}}
{{-- ########################################## --}}
