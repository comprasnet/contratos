<!-- textarea -->
<div @include('crud::inc.field_wrapper_attributes') >
    <label>{!! $field['label'] !!}</label>
    @include('crud::inc.field_translatable_icon')

    {{-- HINT com imagem i --}}
    @if(isset($field['ico_help']))
        <i class="fa fa-info-circle" title="{!! $field['ico_help'] !!}"></i>
    @endif

    <textarea
    	name="{{ $field['name'] }}"
        @include('crud::inc.field_attributes')

    	>{{ old(square_brackets_to_dots($field['name'])) ?? $field['value'] ?? $field['default'] ?? '' }}</textarea>

    {{-- HINT --}}
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
</div>
