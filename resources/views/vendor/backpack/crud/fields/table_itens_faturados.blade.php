@php

    $contrato_id = \Route::current()->parameter('contrato_id');
    $contratoItems = implode(',', request()->old('contratoItems') ?? []);
    $actionMethod = Route::getCurrentRoute()->getAction();
    $paisesfabricacaoUrl = route('crud.faturas.paises');

@endphp

<div class="row-separate">
    <label>Itens</label>
</div>

@if($actionMethod['as'] == 'crud.faturas.create')
    <div class="form-group col-md-6">
        <button type="button" id="btn-modal" class="btn btn-primary" data-toggle="modal" data-target="#novoItem">
            <i class="fa fa-plus" aria-hidden="true"></i> Novo Item
        </button>
    </div>

@endif

<div class="modal fade" id="novoItem" tabindex="-1" role="dialog" aria-labelledby="novoItemLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="novoItemLabel">Novo Item</h4>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 form-group">
                        <label>Histórico</label>

                        <select id="contratohistorico_id" class="form-control">
                            @foreach ($field['options'] as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-12 form-group">
                        <label>Item</label>

                        <select id="contratoitem_id" class="form-control">
                            <option value="">Todos</option>

                        </select>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-ban"
                                                                                      aria-hidden="true"></i> Cancelar
                </button>
                <button type="button" class="btn btn-success" id="include-contratoitem-btn"><i class="fa fa-floppy-o"
                                                                                               aria-hidden="true"></i>
                    Incluir
                </button>
            </div>
        </div>
    </div>
</div>

<br>

<br>

{!! $html->table(['class' => 'table table-responsive table-striped table-itens-faturados']) !!}

@push('crud_fields_scripts')
    {!! $html->scripts() !!}
@endpush

@push('crud_fields_styles')
    <style type="text/css">

        .row-grid-itens-faturados td {
            padding: 12px;
        }

        .row-grid-itens-faturados td .input-group-addon {
            padding: 0 6px;
        }

        .table-itens-faturados thead tr th, .row-grid-itens-faturados td, .row-grid-itens-faturados td > div {
            text-align: center;
        }

        .row-grid-itens-faturados td .input-group {
            float: right;
        }

        .input-group-addon-valor-total-faturadovalortotal_faturado {
            margin-left: 0.5em;
        }

        .row-separate {
            margin-right: 15px;
            margin-left: 15px;
            margin-bottom: 35px;
            border-bottom: 1px solid #d2d6de;
        }

        #btn-modal{
            margin-top: 50px;
        }
    </style>
@endpush

@push('crud_fields_scripts')
    <script src="{{ asset('vendor/sweetalert/2.10.16.11') }}/sweetalert2.all.min.js?time={{microtime()}}"></script>
    <script type="text/javascript">

        var arrGlobalValuesCamposFaturados = [];
        var initValorTotalFaturadoPagina = 0;
        var initValorTotalFaturadoGeral = 0;
        let contratoId = {{ $contrato_id }};
        var contratoItems = [{{ $contratoItems}}];
        var metodo = '{{$actionMethod['as'] == 'crud.faturas.create' ?? false }}';


        $(document).ready(function () {

            //esconde input search
            $('#dataTableBuilder_filter').hide();

            var table = $('.table-itens-faturados').DataTable();

            table.on('xhr', function () {

            });

            table.on('draw.dt', function () {
                initValorTotalFaturadoPagina = calcularValorTotalFaturado(true);
                initValorTotalFaturadoGeral = $('#valorliquido').maskMoney('unmasked')[0]; //pega valor removendo maskMonkey
                /**
                 * @initValorTotalFaturadoGeral é igual ao valor total geral faturado - o valor total geral faturado da pagina
                 * necessário para a lógica de calculo
                 **/
                initValorTotalFaturadoGeral = initValorTotalFaturadoGeral - initValorTotalFaturadoPagina;

                initPaisDeFabricacao();

                if ($(this).find("tbody tr:not(:has(td.dataTables_empty))").length >= 1) {
                    if (parseFloat($("#glosa").val()) === 0) {
                        $("#glosa").attr("disabled", false).removeProp("placeholder").val("0,00")
                    }
                } else {
                    $("#glosa").val("").attr({
                        "disabled": true,
                        "placeholder": "Insira um item para habilitar esse campo."
                    });
                }

                if (metodo) {
                    $("[id^='quantidade_faturado']").each(function () {
                        $('#' + this.id).trigger('keyup');
                    });
                }

            });

            function initPaisDeFabricacao() {
                $(".select2-ajax-paisfabricacao").each(function (i, obj) {
                    var select2 = $(obj);

                    if (!select2.hasClass("select2-hidden-accessible")) {

                        select2.select2({
                            theme: 'bootstrap',
                            language: 'pt-BR',
                            multiple: false,
                            placeholder: "Selecione o país de fabricação",
                            minimumInputLength: "0",

                            {{-- allow clear --}}
                            allowClear: true,
                        })
                            .on('select2:unselecting', function (e) {
                                $(this).val('').trigger('change');
                                e.preventDefault();
                            })
                    }
                });
            }

            //Quando houver evento do datatables (search, order or paging) envia array com dados da grid para o ajax
            table.on('search.dt', function () {
                table.on('preXhr.dt', function (e, settings, data) {
                    data.arrCamposFaturados = arrGlobalValuesCamposFaturados;
                });
            });

            table.on('page.dt', function () {
                table.on('preXhr.dt', function (e, settings, data) {
                    data.arrCamposFaturados = arrGlobalValuesCamposFaturados;
                });
            });

            $('#contratohistorico_id').change(changeHistorico);

            let form = $('form');

            form.submit(function () {
                contratoItems.forEach(el => form.append(`<input type="hidden" name="contratoItems[]" value="${el}">`));

                return true;
            });

            $('#include-contratoitem-btn').click(function (e) {
                let contratoItemInput = $('#contratoitem_id');

                let val = contratoItemInput.val();

                if (val === '') {
                    contratoItemInput.find('option').each(function (e) {
                        let optionsElement = $(this);
                        let val = optionsElement.val();

                        if (val !== '') {
                            contratoItems.push(val);
                        }
                    });
                } else {
                    contratoItems.push(val);
                    contratoItems.sort((a, b) => b - a);  // #605
                }

                $('#novoItem').modal('hide');

                callAjaxDatableGridItensFaturados(contratoItems);
            });

            $("#dataTableBuilder tbody").change(function () {
                if ($(this).find('tr:not(:has(td.dataTables_empty))').length > 0) {
                    $("#glosa").attr("disabled", false).removeAttr("placeholder").val("0,00")

                } else {
                    $("#glosa").val("").attr({
                        "disabled": true,
                        "placeholder": "Insira um item para habilitar esse campo."
                    });
                }
            })

            $('[name="tipo_de_instrumento_de_cobranca_id"]').change(tipoDeInstrumentoChange)
            tipoDeInstrumentoChange()
        });

        changeHistorico();

        function changeHistorico() {
            let contratoHistoricoInput = $('#contratohistorico_id');
            let contratoItemInput = $('#contratoitem_id');
            let val = contratoHistoricoInput.val();

            $.get(`/gescon/meus-contratos/${contratoId}/faturas/contratoitens/list?contratohistorico_id=${val}`, function (data) {
                contratoItemInput.empty();

                contratoItemInput.append(new Option('Todos', ''));

                data.forEach(el => contratoItemInput.append(new Option(el.text, el.value)));
            });
        }

        /**
         * Armazena os valores faturados em array global para recupera-los apos eventos de pesquisa e ordenacao
         *
         * @param element HtmlElement
         **/
        function pushParaArrayGlobal(element, index) {
            /*
            var booEelementoExist = arrGlobalValuesCamposFaturados.some(obj => obj.id === element.id);

            //se o elemento existir remove do array

            if (booEelementoExist) {
                let index = arrGlobalValuesCamposFaturados.findIndex(obj => {
                    return obj.id === element.id
                });
                arrGlobalValuesCamposFaturados.splice(index, 1);
            }

            //add elemento no array
            arrGlobalValuesCamposFaturados[index] = {id: element.id, value: element.value};
            */

            let field = element.name.slice(0, element.name.length - 2);

            if(arrGlobalValuesCamposFaturados[index] == undefined) {
                arrGlobalValuesCamposFaturados[index] = {};
            }

            arrGlobalValuesCamposFaturados[index][field] = element.value;
        }

        /**
         *Evento disparado ao alterar os inputs (quantidade_faturado[] | valorunitario_faturado[])
         * @param element Element alterado na grid mix (quantidade_faturado[] | valorunitario_faturado[])
         * @returns {*|jQuery|null}
         */
        function atualizarValorTotalLinha(element) {
            var tr = element.closest('tr'),
                quantidade_faturado = parseFloat($(tr).find('td').eq(5).find('input').val()),
                valorunitario_faturado = parseFloat($(tr).find('td').eq(6).find('input').val().replace(/\./g, '').replace(',', '.'));

            var vltotal = quantidade_faturado * valorunitario_faturado;
            if (!isNaN(vltotal)) {
                //seta valor total faturado da linha
                $(tr).find('td').eq(7).find('.valor-total-faturado-row').val(vltotal.toFixed(2)).trigger("change");

                //calcula valor total liquido
                calcularValorTotalLiquido();
            } else {
                //caso seja Nan seta valor total da linha e valor total geral para vazio
                $(tr).find('td').eq(7).find('.valor-total-faturado-row').val('').trigger("change");
                $('#valorliquido').val('').trigger("change");
                calcularValorTotalFaturado();
                calcularValorTotalLiquido();
            }
        }

        function calcularValorTotalFaturado(calcValorInit = false) {

            //valor total faturado da pagina
            let valorTotalFaturadoPagina = $('[name="valortotal_faturado[]"]').toArray().reduce(function (totalValue, currentEl, currentIndex) {
                let currentValue = (isNaN(parseFloat(currentEl.value)) || currentEl.value === '' ? 0
                    :
                    parseFloat(currentEl.value.replace(/\./g, '').replace(',', '.')));

                return parseFloat(totalValue) + currentValue;
            }, 0);


            if (calcValorInit) {
                return valorTotalFaturadoPagina;
            }

            valorTotalFaturadoPagina = initValorTotalFaturadoGeral + valorTotalFaturadoPagina;

            $('#valorliquido').val(valorTotalFaturadoPagina.toFixed(2)).trigger("change");
            $('#valortotalfaturado_calc').val(valorTotalFaturadoPagina.toFixed(2)).trigger("change");

            //verifica se valor da glosa é maior que valor total faturado
            validarGlosaMaiorQueValorTotalFaturado();

            return valorTotalFaturadoPagina;
        }

        function calcularValorTotalLiquido() {

            var valorTotalFaturadoPagina = calcularValorTotalFaturado(true);
            valorTotalFaturadoPagina = calculaAcrescimo(valorTotalFaturadoPagina, 'juros');
            valorTotalFaturadoPagina = calculaAcrescimo(valorTotalFaturadoPagina, 'multa');
            valorTotalFaturadoPagina = calculaDecrescimo(valorTotalFaturadoPagina, 'glosa');

            $('#valorliquido').val(valorTotalFaturadoPagina.toFixed(2)).trigger("change");
        }

        function atualizarMaskMoney(element) {
            if (element.value !== '') {
                var maxLength = '000.000.000.000.000,00'.length;
                $(element).maskMoney({
                    allowNegative: false,
                    thousands: '.',
                    decimal: ',',
                    precision: 4,
                    affixesStay: false
                }).attr('maxlength', maxLength).trigger('mask.maskMoney');
            }
        }

        function atualizarMaskMoney2Decimals(element) {
            if (element.value !== '') {
                var maxLength = '000.000.000.000.000,00'.length;
                $(element).maskMoney({
                    allowNegative: false,
                    thousands: '.',
                    decimal: ',',
                    precision: 2,
                    affixesStay: false
                }).attr('maxlength', maxLength).trigger('mask.maskMoney');
            }
        }

        function atualizarMaskMoney17Decimals(element) {
            if (element.value !== '') {
                let value = element.value.replace(/[^\d,]/g, '');

                const decimalIndex = value.indexOf(',');
                if (decimalIndex !== -1) {
                    const integerPart = value.substring(0, decimalIndex);
                    if (integerPart.length > 19) {
                        value = value.substring(0, decimalIndex - 1);
                    }
                } else if (value.length > 15) {
                    value = value.substring(0, 15);
                }

                const parts = value.split(',');
                if (parts.length > 2) {
                    value = parts[0] + ',' + parts.slice(1).join('');
                }
                if (parts.length === 2) {
                    value = parts[0] + ',' + parts[1].substring(0, 17);
                }

                let formattedValue = '';
                let decimalPart = '';
                const decimalIndex2 = value.indexOf(',');
                if (decimalIndex2 !== -1) {
                    formattedValue = value.substring(0, decimalIndex2).replace(/\B(?=(\d{3})+(?!\d))/g, '.') + ',';
                    decimalPart = value.substring(decimalIndex2 + 1);
                } else {
                    formattedValue = value.replace(/\B(?=(\d{3})+(?!\d))/g, '.');
                }

                if (decimalPart) {
                    formattedValue += decimalPart;
                }

                element.value = formattedValue;
            }
        }

        function atualizarMask17Decimals(element) {
            if (element.value !== '') {
                let initialValue = element.value;
                if ((initialValue.match(/\./g) || []).length > 1 || (initialValue.match(/,/g) || []).length > 1) {
                    initialValue = initialValue.substring(0, initialValue.lastIndexOf('.'));
                }
                initialValue = initialValue.replace(/,/g, '.');
                let initialParts = initialValue.split('.');
                initialParts[0] = initialParts[0].substring(0, 10).replace(/[^0-9]/g, '');
                if (initialParts.length > 1) {
                    initialParts[1] = initialParts[1].substring(0, 17).replace(/[^0-9]/g, '');
                }
                element.value = initialParts.join('.');

                element.addEventListener('input', function (e) {
                    let value = e.target.value;
                    if ((value.match(/\./g) || []).length > 1 || (value.match(/,/g) || []).length > 1) {
                        value = value.substring(0, value.lastIndexOf('.'));
                    }
                    value = value.replace(/,/g, '.');
                    let parts = value.split('.');
                    parts[0] = parts[0].substring(0, 10).replace(/[^0-9]/g, '');
                    if (parts.length > 1) {
                        parts[1] = parts[1].substring(0, 17).replace(/[^0-9]/g, '');
                    }
                    e.target.value = parts.join('.');
                });
            }
        }

        function onChangeComboContratoHistorico(element) {

            $('#valorliquido').val('');

            var idContratoHistorico = element.value ?? null;

            return idContratoHistorico ? callAjaxDatableGridItensFaturados(idContratoHistorico) : null;
        }

        /**
         * Chama ajax que carrega grid de itens enviando o contratohistorico_id
         *
         * @param contratoItems
         */
        function callAjaxDatableGridItensFaturados(contratoItems) {

            var table = $('.table-itens-faturados');

            table.on('preXhr.dt', function (e, settings, data) {
                //data.idContratoHistorico = idContratoHistorico;
                settings.ajax.type = 'POST'
                data.contratoItems = contratoItems;
            });

            table.DataTable().draw();

        }

        function calculaAcrescimo(valorTotalFaturadoGeral, elementName) {

            var elementAcrescimo = $('#' + elementName).val().replace(/\./g, '').replace(',', '.');
            elementAcrescimo = elementAcrescimo === '0.00' || elementAcrescimo === '' ? 0 : elementAcrescimo;
            return valorTotalFaturadoGeral + parseFloat(elementAcrescimo);
        }

        function calculaDecrescimo(valorTotalFaturadoGeral, elementName) {

            var elementAcrescimo = $('#' + elementName).val().replace(/\./g, '').replace(',', '.');
            elementAcrescimo = elementAcrescimo === '0.00' || elementAcrescimo === '' ? 0 : elementAcrescimo;

            return valorTotalFaturadoGeral - parseFloat(elementAcrescimo);
        }

        function validarGlosaMaiorQueValorTotalFaturado() {

            var valueElementGlosa = $('#glosa').val().replace(/\./g, '').replace(',', '.'),
                elementValorTotalFaturado = $('#valortotalfaturado_calc').val().replace(/\./g, '').replace(',', '.')

            if (parseFloat(valueElementGlosa) > parseFloat(elementValorTotalFaturado)) {

                //mensagem de avisao
                Swal.fire(
                    'Ops!',
                    'O valor da glosa deve ser igual ou menor que o valor total faturado!',
                    'warning'
                )

                //zera valor do campo glosa
                $('#glosa').val('');

            }
        }

        function serieNotaFiscal(element) {
            if (element.value !== '') {
                element.value = element.value.replace(/\D/g, '')
            }
        }

        function tipoDeInstrumentoChange() {
            var serie = $('#input_serie');
            var tipoDeInstrumento = $('[name="tipo_de_instrumento_de_cobranca_id"] option:selected').text();
            if (tipoDeInstrumento === 'Nota Fiscal' || tipoDeInstrumento === 'Nota Fiscal Eletrônica') {
                habilitarCampoSerie(serie);
            } else {
                desabilitarCampoSerie(serie);
            }

            if (tipoDeInstrumento === 'Nota Fiscal Eletrônica') {
                $(".input_chafe_nfe").addClass('required');
            } else {
                $(".input_chafe_nfe").removeClass('required');
            }
        }

        function habilitarCampoSerie(serie) {
          //  $(".serieNumero").addClass('required');
            serie.removeAttr('readonly');
            serie.css('cursor', 'auto');
        }

        function desabilitarCampoSerie(serie) {
            $(".serieNumero").removeClass('required');
            serie.val('');
            serie.attr('readonly', true);
            serie.css('cursor', 'not-allowed');
        }

    </script>
@endpush
