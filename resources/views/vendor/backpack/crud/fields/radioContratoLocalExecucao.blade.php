<!-- radio -->
@php
    $optionPointer = 0;
    $optionValue = old(square_brackets_to_dots($field['name'])) ?? $field['value'] ?? $field['default'] ?? '';

    // if the class isn't overwritten, use 'radio'
    if (!isset($field['attributes']['class'])) {
        $field['attributes']['class'] = 'radio';
    }
@endphp

<div @include('crud::inc.field_wrapper_attributes') >

    <div>
        <label>{!! $field['label'] !!}</label>
        @include('crud::inc.field_translatable_icon')
        @if(isset($field['ico_help']))
            <i class="fa fa-info-circle" title="{!! $field['ico_help'] !!}"></i>
        @endif
    </div>

    @if( isset($field['options']) && $field['options'] = (array)$field['options'] )

        @foreach ($field['options'] as $value => $label )
            @php ($optionPointer++)

            @if( isset($field['inline']) && $field['inline'] )

            <label class="radio-inline" for="{{$field['name']}}_{{$optionPointer}}">
                <input  type="radio"
                        id="{{$field['name']}}_{{$optionPointer}}"
                        name="{{$field['name']}}"
                        value="{{$value}}"
                        @include('crud::inc.field_attributes')
                        {{$optionValue == $value ? ' checked': ''}}
                        > {!! $label !!}
            </label>

            @else

            <div class="radio">
                <label for="{{$field['name']}}_{{$optionPointer}}">
                    <input type="radio" id="{{$field['name']}}_{{$optionPointer}}" name="{{$field['name']}}" value="{{$value}}" {{$optionValue == $value ? ' checked': ''}}> {!! $label !!}
                </label>
            </div>

            @endif

        @endforeach

    @endif

    {{-- HINT --}}
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif

</div>
@push('crud_fields_scripts')
    <script>
        jQuery(document).ready(function($) {

            if($("input[name='radio_numero']").prop('checked') == 0 || $("input[name='numero']").val() === 'S/N'){

                $("input[name='numero']")
                    .attr('readonly',true)
                    .val("S/N")

                $("#radio_numero_2").prop("checked", true);
            }

            $("input[name='radio_numero']").click(function(){
                if($(this).val() == 0){
                    $("input[name='numero']")
                        .attr('readonly',true)
                        .val("S/N")
                }else{
                    $("input[name='numero']")
                        .attr('readonly',false)
                        .val("")
                }
            })
        });
    </script>
@endpush
