<!-- text input -->
<div @include('crud::inc.field_wrapper_attributes') >


    @include('crud::inc.field_translatable_icon')
    <label>{!! $field['label'] !!}</label>
    <div style="display: flex; flex-direction: row; flex-wrap: nowrap;">
        <input
            type="text"
            class="form-control"
            style="margin-right: 10px;width: 100px;"
            name="{{ $field['name'] }}"
            id="{{ $field['name'] }}"
            value="{{ old($field['name']) ?? $field['value'] ?? $field['default'] ?? '' }}"
            @include('crud::inc.field_attributes')
        >

        <a href="#" class="btn btn-block btn-primary" id="buscaCepLocalExecucao" style="width: 80px">
            <span id="buscaCepContent">
                <i class="fa fa-search" id="buscaCepIconButton"></i>
                <span id="buscaCepText">Buscar</span>
            </span>
        </a>

    </div>

</div>

@push('crud_fields_scripts')
    <script src="{{ asset('vendor/sweetalert/2.10.16.11') }}/sweetalert2.all.min.js?time={{microtime()}}"></script>
    <script type="text/javascript">
        var municipios_id = $("select[name='municipios_id']")

        $(document).ready(function () {

            $('#{{ $field['name'] }}').mask('99999-999');

            $(".primeiraLetraMaiucula").on('change', function () {
                $(this).val(primeiraLetraMaiucula($(this).val()));
            });

            $('#buscaCepLocalExecucao').click(function () {
                retornaDadosCep()
            })

            municipios_id.on('change', function () {
                $("input[name='municipios_id']").val($(this).val())
            })
        });

        function mensagem(texto) {
            Swal.fire({
                position: 'center',
                icon: 'warning',
                title: texto,
                showConfirmButton: false,
                timer: 2500
            })
        }

        function retornaCidadeeEstado(texto) {

            $.ajax({
                url: "{{url('api/busca/municipio')}}",
                method: 'GET',
                data: {q: texto},
                success: function (response) {
                    municipios_id.empty()
                    var option = $("<option/>", {value: Number(response.data[0].id), text: response.data[0].nome});
                    municipios_id.append(option);
                    $('select2_ajax_municipios_id option[value="' + response.data[0].id + '"]').prop('selected', true);
                    municipios_id.trigger('change');
                    $("input[name='municipios_id']").val(Number(response.data[0].id));
                },
                error: function () {
                    console.error('Erro ao obter o ID por texto.');
                }
            });
        }

        function retornaDadosCep() {
            var cep = $("#cep").val().replace('-', '')
            var logradouro = $("input[name='logradouro']")
            var bairro = $("input[name='bairro']")

            if (cep.length === 8) {
                $.ajax({
                    type: "get",
                    url: "{{url('api/localexecucao/buscacep')}}",
                    data: {cep: cep},
                    dataType: 'json',
                    beforeSend: function () {
                        $("#buscaCepIconButton").removeClass("fa fa-search").addClass("fa fa-refresh fa-spin")
                        $('#buscaCepText').text('Carregando...');
                        $('#buscaCepLocalExecucao').css('width', '130px')
                            .attr('disabled', true)
                            .unbind('click');
                    },
                    success: function (data) {
                        if (data) {
                            logradouro.val(data.endereco)
                            bairro.val(data.bairro)

                            retornaCidadeeEstado(`${data.cidade} - ${data.uf}`)

                            if (data.bairro === "") {
                                bairro.attr('readonly', false)
                                logradouro.attr('readonly', false).val('')
                            } else {
                                bairro.attr('readonly', true)
                                logradouro.attr('readonly', true)
                            }
                        }
                    },
                    error: function (erro) {
                        if (erro.status === 404) {
                            mensagem(erro.responseJSON.error)
                        }

                        if (erro.status === 403) {
                            logradouro.attr('readonly', false)
                            bairro.attr('readonly', false)
                            municipios_id.prop('disabled', false)
                            mensagem('Serviço de consulta ao CEP indisponível.')
                        }

                    }
                })
                    .always(function () {
                        $('#buscaCepIconButton').removeClass('fa fa-refresh fa-spin').addClass('fa fa-search')
                        $('#buscaCepText').text('Buscar');
                        $('#buscaCepLocalExecucao').css('width', '80px')
                            .attr('disabled', false)
                            .bind("click", function () {
                                retornaDadosCep()
                            })
                    })
            } else {
                mensagem('O cep precisa ter pelo menos 8 caracteres!')
            }
        }

        function primeiraLetraMaiucula(texto) {
            return texto.charAt(0).toUpperCase() + texto.slice(1);
        }

    </script>
@endpush



