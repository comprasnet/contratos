<!-- array input -->
@php
    $keys = array_keys($fields['link_sei']['options']);
    $arraySize = count($fields['link_sei']['options']);
    $arrayItems = "";

    for($i=0; $i < $arraySize; $i++) {
        $arrayItems .= "<option value='" . $keys[$i] . "'>" . $fields['link_sei']['options'][$keys[$i]] . "</option>";
    }
@endphp


<div ng-app="backPackTableApp" ng-controller="tableController" @include('crud::inc.field_wrapper_attributes') >

    <div class="mensagem-status-tab-anexosei"></div>
    <input class="array-json" type="hidden" id="{{ $field['name'] }}" name="{{ $field['name'] }}">

    <div class="array-container form-group">
        <div class="array-controls btn-group m-t-10">

        </div>
        <div id='mostraSeiAnexo'>

        <div class="row">
            <div class="col-md-5">
                <div id='comboSeiVinculado' >
                </div>
            </div>
            <div class="col-md-4">
                <button type='button' id='btnPesquisaSEI' onclick='pesquisaSei()' class='btn btn-sm btn-default' style='padding-left:30px;height: 34px !important;display:none' title='Clique para consultar anexos do processo SEI'/><i class='fa fa-search'></i> Consultar anexos no SEI</button> <br><br>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <!-- retorno do SEI -->
                <div id='resulta' >

                </div>
                <!-- anexos escolhidos para vincular ao Comprasnet Contratos -->
                <span id='escolhaAnexo'>

                </span>
            </div>
        </div>
        </div>
    </div>

</div>

@push('crud_fields_scripts')
    <script>
    function pesquisaSei(){

         $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

       jQuery.ajax({
         url: "{{route('ajaxSei')}}",
         method: 'post',
          data: {
             //nrProcessoSei: $('#processo').val(),
             nrProcessoSei: $('#txtFilhoSEI').val(),
             idProcesso: $('select[name="contrato_id"]').find(":selected").val()
          },
          success: function(result){
            if (result=='')
                $('#resulta').hide().html('Não encontrou informações no SEI!').fadeIn(2000);
            else
                $('#resulta').hide().html(result).fadeIn(2000);


          }});
              // $('#resulta').hide().show().fadeIn(2000);
    }

    function pesquisaSeiVinculado(){
       let resultado = '';
       $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
       });

       jQuery.ajax({
         url: "{{route('ajaxSeiVinculado')}}",
         method: 'post',
          data: {
             nrProcessoSei: $('#processo').val()
          },
          success: function(result){
            $('.mensage-status-tab-anexosei').hide();
            aux = "<select name='txtFilhoSEI' id='txtFilhoSEI' class='form-control select2' style='padding:.5rem; width:100%;float:left'>";
            aux += "<option value='" + $('#processo').val() + "' title='Processo principal'>" + $('#processo').val() + " - Processo principal</option>";
            if (result.dados != '') {
                for (i=0; i < result.dados.length; i++) {
                  aux += "<option value='" + result.dados[i]['ProcedimentoFormatado'] + "' title='" + result.dados[i]['TipoProcedimento']['Nome'] + "'>" + result.dados[i]['ProcedimentoFormatado'] + " - Processo relacionado</option>";
                }
            }
            if(result.erros != '') {
                var mensagem_alerta = '<div class="callout callout-warning"><h4>Atenção</h4><ul><li>'+result.erros+'</li></ul></div>';
                $('.mensagem-status-tab-anexosei').html(mensagem_alerta).fadeIn();
            }

            aux += "</select>";

            $('#comboSeiVinculado').hide().html(aux).fadeIn(1000);
            $('#btnPesquisaSEI').show(10);
            $('#txtFilhoSEI').select2();
          }
        });
    }

    function escolheSeiAnexos(){
        var escolhidos   = $("input[name=checkAnexo]:checked").map(function () {return this.value;}).get().join(',');
        var dadosTD      = '';
        var colunasTd    = escolhidos.split(',');
        if (escolhidos != '') {
            var aux = "<h4>Vínculos que serão gravados no Comprasnet Contratos</h4><hr>";
               aux += "<div class='table-responsive'><table class='table table-responsive-md' width='100%' id='tableEscolhaAnexoSei' >";
               aux += "     <thead>";
               aux += "     <tr>";
               aux += "         <th>Seleção</th>";
               aux += "         <th>Nome no SEI</th>";
               aux += "         <th>Tipo <span style = 'color: red'>*</span></th>";
	           aux += "         <th>Termo</th>";
               aux += "         <th>Descrição</th>";
               aux += '         <th>Restrito<i class="fa fa-fw fa-info-circle" title="Arquivo restrito não será disponibilizado publicamente."></i></th>';
               aux += "         <th>Ação</th>";
               aux += "     </tr>";
               aux += "     </thead>";
               aux += "     <tbody>";

                for (i=0; i < colunasTd.length; i++) {
                    dadosTd = colunasTd[i].split('|');
                    aux += "         <tr>";
                    aux += "             <td><input type='hidden' value='"+ dadosTd[0]+"' name='linkAnexoSei[]'>&nbsp;<a href='"+dadosTd[0]+"' target='_blank'><i class='fa fa-search'  style='padding-left:15px;cursor:pointer' title='Visualizar o anexo'></i></a></td>";
                    aux += "             <td><input type='text' name='nomeSei[]' class='form-control' value='"+ dadosTd[1]+"("+ dadosTd[2]+")"+"' readonly></td>";
                    aux += "             <td>";
                    aux += "                 <select name='tipoSei[]'  class='form-control validar' id='tipo"+i+"' onchange='buscaContratoHistoricoIDSEI(this.value,"+i+",this.options[this.selectedIndex].text)'>";
                    aux += "                     <option value='0'>Selecione...</option>";
                    aux +=                       "@php echo $arrayItems; @endphp";
                    aux += "                 </select>";
                    aux += "             </td>";
                    aux += "             <td>";
                    aux += "                <span class='escondePNCPSEI' style='display:none' id='spanTermoSEI"+i+"'></span>";
                    aux += "             </td>";
                    aux += "             <td><input type='text' name='descricaoSei[]' class='form-control' style='width:100%'></td>";
                    aux += `            <td><label><input type="radio" name="restrito[${i}]" value="1" > Sim </label><br/><label><input type="radio" name="restrito[${i}]" value="0" checked> Não</td>`;
                    aux += `             <td></label>
                                            <center>
                                                <button type='button' class='btn btn-danger' onclick='$(\"#tableEscolhaAnexoSei tr\").click(function(){ $(this).remove();return false;});' title='Remover essa linha'>
                                                    <i class='fa fa-trash'></i>
                                                </button>
                                            </center>
                                        </td>`;
                    aux += "         </tr>";
                }
                aux += "     </tbody>";
                aux += " </table></div>";
                $('#escolhaAnexo').hide().html(aux).fadeIn(2000);
        } else {

        }
    }

    //parte do input file
//    $('#arquivos_file_input').on('click', function() {
//       var myFile = $('#arquivos_file_input').prop('files');
//       console.log(myFile[1]);
//    });


    /*
    * Lista todos os anexos do SEI do contrato atual
    */
    $('#vinculacaodeanexosdosei').on('click', function(){
       $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
       });

       var idProcesso = $('select[name="contrato_id"]').find(":selected").val();

       jQuery.ajax({
         url: "{{route('ajaxLinksGravados')}}",
         method: 'post',
          data: {
             idProcesso: idProcesso
          },
          success: function(result){ console.log(result);
            if (result.length>0) {
            var tipoSei = '';
            var comboAux = '';
            var comboFinal = '';
            var descricaoSei = '';
            var i = 0;
            var linkSei = '';
            var nome = '';
            var descricao = '';
            var aux = "<h4>Documentos vinculados do SEI</h4><hr>";
               aux += "<div class='table-responsive'><table id='tableEscolhaAnexoSei' class='table dataTable no-footer dtr-inline table-responsive-md' role='grid' aria-describedby='dataTableBuilder_info'>";
               aux += "     <thead>";
               aux += "     <tr>";
               aux += "         <th></th>";
               aux += "         <th>Nome no SEI</th>";
               aux += "         <th>Tipo *</th>";
               aux += "         <th>Termo</th>";
               aux += "         <th>Descricao</th>";
               aux += "     </tr>";
               aux += "     </thead>";
               aux += "     <tbody>";

                for (i=0; i < result.length; i++) {
                    if (result[i]['origem']=='1') {
                        //set o option do select

                        tipoSei      = result[i]['tipo'];
                        comboAux     = "<?php echo $arrayItems; ?>";
                        comboFinal   = comboAux.replace(tipoSei,tipoSei+"' selected='");
                        comboFinal   = comboFinal.replace('"',"'");
                        descricaoSei = result[i]['descricao'] ? result[i]['descricao'] : '';
                        descricaoSei = descricaoSei.split('-');
                        nome         = descricaoSei[0].replace('Descrição SEI: ','');
                        descricao    = descricaoSei[1].replace(' Descrição Comprasgovbr: ','');
                        linkSei      = result[i]['link_sei'];
                        termo        = result[i]['contratohistorico_id'] ? result[i]['contratohistorico_id'] : '';
                        aux += "         <tr>";
                        aux += "             <td><a href='"+linkSei+"' target='_blank'><i class='fa fa-search'  style='padding-left:15px;cursor:pointer' title='Visualizar o anexo'></i></a></td>";
                        aux += "             <td><input type='text' name='nomeSei[]' class='form-control' value='"+ nome +"' readonly></td>";
                        aux += "             <td>";
                        aux += "                 <select name='tipoSei[]'  class='form-control' >";
                        aux += "                     <option value=''>Selecione...</option>";
                        aux +=                       comboFinal;
                        aux += "                 </select>";
                        aux += "             </td>";
                        aux += "             <td><input type='text' class='form-control' value='"+termo+"'></td>"
                        aux += "             <td><input type='text' name='descricaoSei[]' value='"+ descricao +"' class='form-control' style='width:100%'></td>";
                        aux += "         </tr>";
                    }
                }
                aux += "     </tbody>";
                aux += " </table></div>";

            }

            $('#escolhaAnexo').hide().html(aux).fadeIn(2000);
          }});



    });


    function buscaContratoHistoricoIDSEI(arg, i, texto){
        $.ajaxSetup({
             headers: {
                 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
             }
        });
        var idTipo     = texto;
        var idProcesso = $('select[name="contrato_id"]').find(":selected").val();
        let elaboracao = $('#elaboracao').val();
        jQuery.ajax({
          url: "{{url('api/termosPorTipo')}}",
          method: 'get',
           data: {
              q: idTipo,
              tipo: arg,
              contrato_id: idProcesso,
              elaboracao: elaboracao,
              dataType: 'json'
           },
           success: function(result){
             let classValidar = ''
             let contratoLei14133 = $('input[name="contratoLei14133"]').val()
             if (contratoLei14133 == true) {
               classValidar = 'validar'
               // Variável criada no arquivo form_save_buttons
               mensagemAlertaCampoVazio = 'É obrigatória a vinculação do Termo correspondente para o tipo de arquivo selecionado.'
             }
             var aux = '';
             aux += "<select name='contratohistoricoSEI_id[]' id='contratohistoricoSEI_id"+i+"' class='form-control " +classValidar+ "'>";
            if (arg=='133' || arg=='134' || arg=='135' || arg=='137') {
                if(result.length==1) {
                    aux += "  <option value='"+result[0].id+"'>"+result[0].numero+"</option>";
                }else if (result.length>0){
                   aux += "  <option value='0'>Selecione</option>";
                   for (j=0; j < result.length; j++) {
                    aux += "  <option value='"+result[j].id+"'>"+result[j].numero+"</option>";
                   }
                } else {
                    aux += "   <option value='0'>Não encontrou</option>";
                }
            } else {
                    aux += "   <option value=''>Não se aplica</option>";
            }

             aux += "</select>";
             $('.escondePNCPSEI').show();
             $('#spanTermoSEI'+i).hide().html(aux).fadeIn(2000);
           }
        });
    }



    pesquisaSeiVinculado();
    </script>

@endpush
