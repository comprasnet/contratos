<div>
    <input
        type="text"
        name="{{ $field['name'] }}"
        value="{{ old(square_brackets_to_dots($field['name'])) ?? $field['value'] ?? $field['default'] ?? '' }}"
        @include('crud::inc.field_attributes')
    >
    @if(isset($fields_hiddens))
        <div class="inputs-hidden">
            <input
                type="hidden"
                name="{{ $fields_hiddens['id_contratofatura_itens']['name'] }}"
                value="{{ old(square_brackets_to_dots($fields_hiddens['id_contratofatura_itens']['name'])) ??
                    $fields_hiddens['id_contratofatura_itens']['value'] ?? '' }}"
            >

            <input
                type="hidden"
                name="{{ $fields_hiddens['saldohistoricoitens_id']['name'] }}"
                value="{{ old(square_brackets_to_dots($fields_hiddens['saldohistoricoitens_id']['name'])) ??
                    $fields_hiddens['saldohistoricoitens_id']['value'] ?? '' }}"
            >
        </div>
    @endif
</div>
