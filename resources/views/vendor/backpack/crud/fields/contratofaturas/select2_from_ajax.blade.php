<!-- select2 from ajax -->
@php
    $connected_entity = new $field['model'];
    $connected_entity_key_name = $connected_entity->getKeyName();
    $old_value = old(square_brackets_to_dots($field['name'])) ?? $field['value'] ?? $field['default'] ?? false;
@endphp

<div @include('crud::inc.field_wrapper_attributes') >
    <?php
    $entity_model = $field['entity'];
    $model = $field['model'];
    ?>

    <select
        name="{{ $field['name'] }}[<%$index%>]"
        id="select2-[<%$index%>]"
        style="width: 100%"
        ng-model="item.{{ $field['name'] }}"
        onchange="onchangeSelectEmpenhoInstrumento(event, this);"
        @include('crud::inc.field_attributes', ['default_class' =>  'form-control select-empenho-fatura'])
    >

    </select>

    {{-- HINT --}}
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
</div>

{{-- ########################################## --}}
{{-- Extra CSS and JS for this particular field --}}
{{-- If a field type is shown multiple times on a form, the CSS and JS will only be loaded once --}}
@if ($crud->checkIfFieldIsFirstOfItsType($field))

    {{-- FIELD CSS - will be loaded in the after_styles section --}}
    @push('crud_fields_styles')
        <!-- include select2 css-->
        <link href="{{ asset('vendor/adminlte/bower_components/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        {{-- allow clear --}}
        @if ($entity_model::isColumnNullable($field['name']))
            <style type="text/css">
                .select2-selection__clear::after {
                    content: ' {{ trans('backpack::crud.clear') }}';
                }
            </style>
        @endif
    @endpush

    {{-- FIELD JS - will be loaded in the after_scripts section --}}
    @push('crud_fields_scripts')
        <!-- include select2 js-->
        <script src="{{ asset('vendor/adminlte/bower_components/select2/dist/js/select2.min.js') }}"></script>
        <script src="{{ asset('vendor/adminlte/bower_components/select2/dist/js/i18n/pt-BR.js') }}"></script>
    @endpush
@endif

<!-- include field specific select2 js-->
@push('crud_fields_scripts')
    <script>
        $(document).ready(function($) {
            initSelectEmpenhoFatura();
        });

        function formatDate(input) {
            const dateParts = input.split('-');
            const day = dateParts[2].split(' ');
            return `${day[0]}/${dateParts[1]}/${dateParts[0]}`;
        }

        function initSelectEmpenhoFatura()
        {
            var json = jQuery.parseJSON($('#empenho_json').val());
            var oldValues = jQuery.parseJSON($('#empenho_json_oldvalues').val());

            // trigger select2 for each untriggered select2 box
            $(".select-empenho-fatura").each(function (i, obj) {
                var form = $(obj).closest('form');

                var select2 = $(obj);

                if (!select2.hasClass("select2-hidden-accessible")) {
                    select2.select2({
                        theme: 'bootstrap',
                        language: 'pt-BR',
                        multiple: false,
                        placeholder: "{{ $field['placeholder'] }}",
                        minimumInputLength: "{{ $field['minimum_input_length'] }}",

                        {{-- allow clear --}}
                            @if ($entity_model::isColumnNullable($field['name']))
                        allowClear: true,
                        @endif
                        ajax: {
                            url: "{{ $field['data_source'] }}",
                            type: '{{ $field['method'] ?? 'GET' }}',
                            dataType: 'json',
                            quietMillis: 250,
                            data: function (params) {
                                return {
                                    q: params.term, // search term
                                    page: params.page, // pagination
                                    form: form.serializeArray()  // all other form inputs
                                };
                            },
                            processResults: function (data, params) {
                                params.page = params.page || 1;

                                var result = {
                                    results: $.map(data.data, function (item) {
                                        return {
                                            // #926 - inserido codigosiafi
                                            text: item['numero'] + ' - ' + item['codigo'] + ' - ' + item['fornecedor'] + ' - ' + (item['data_emissao'] ? formatDate(item['data_emissao']) : formatDate(item['created_at'])),
                                            id: item["{{ $connected_entity_key_name }}"]
                                        }
                                    }),
                                    pagination: {
                                        more: data.current_page < data.last_page
                                    }
                                };

                                return result;
                            },
                            cache: true
                        },
                    })
                    {{-- allow clear --}}
                    @if ($entity_model::isColumnNullable($field['name']))
                        .on('select2:unselecting', function(e) {
                            $(this).val('').trigger('change');
                            e.preventDefault();
                        })
                    @endif

                    /*Cai aqui quando a requisição voltar com alguma validação e recupera os empenhos pelo campo empenho_json e chama a funcao buscarSubelementoDoEmpenho para setar os sublementos*/
                    if(oldValues.length) {

                        var newOption = new Option(oldValues[i].text, json[i].empenho_id, true, true);
                        select2.append(newOption);

                        var selectSubelemento = select2.closest('tr').find('select.select-subelemento-empenho');
                        buscarSubelementoDoEmpenho(oldValues[i].numeroEmpenho, selectSubelemento, oldValues[i].codUg, oldValues[i].empenho_id, true, oldValues[i].empenhosubelemento).then(function (){
                        });
                    /*Cai aqui quando for a ação edit da pagina recupera os empenhos pelo campo empenho_json e chama a funcao buscarSubelementoDoEmpenho para setar os sublementos*/
                    } else if(json.length) {

                        var newOption = new Option(json[i].text, json[i].empenho_id, false, true);
                        select2.append(newOption);

                        var selectSubelemento = select2.closest('tr').find('select.select-subelemento-empenho');

                        buscarSubelementoDoEmpenho(json[i].numeroEmpenho, selectSubelemento, json[i].codUg, json[i].empenho_id, false, json[i].empenhosubelemento).then(function (){

                        });
                    }
                }
            });
        }
    </script>
@endpush
{{-- End of Extra CSS and JS --}}
{{-- ########################################## --}}
