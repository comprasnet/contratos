




<!-- select2 from array -->
<div @include('crud::inc.field_wrapper_attributes') >

    <select
        name="{{ $field['name'] }}@if (isset($field['allows_multiple']) && $field['allows_multiple']==true)[]@endif"
        style="width: 100%"
        @include('crud::inc.field_attributes', ['default_class' =>  'form-control select2_from_array'])
        @if (isset($field['allows_multiple']) && $field['allows_multiple']==true)multiple @endif
    >

        @if (isset($field['allows_null']) && $field['allows_null']==true)
            <option value="">Selecione...</option>
        @endif


        @if (count($field['options']))
            @foreach ($field['options'] as $key => $value)

                @if((old(square_brackets_to_dots($field['name'])) &&
                    (
                        $key == old(square_brackets_to_dots($field['name'])
                    )
                    ||
                    (
                        is_array(old(square_brackets_to_dots($field['name'])))
                        &&
                        in_array($key, old(square_brackets_to_dots($field['name']))))))
                        ||
                        (null === old(square_brackets_to_dots($field['name'])) &&
                            ((isset($field['value']) && (
                                        $key == $field['value'] || (
                                                is_array($field['value']) &&
                                                in_array($key, $field['value'])
                                                )
                                        )) ||
                                (isset($field['default']) &&
                                ($key == $field['default'] || (
                                                is_array($field['default']) &&
                                                in_array($key, $field['default'])
                                            )
                                        )
                                ))
                        ))


                    <!-- <option value="{{ $key }}" selected>{{ $value }}</option> -->
                    <option value="{{ $key }}" selected>{{ $value }} </option>
                @else
                    <!-- <option value="{{ $key }}">{{ $value }}</option> -->
                    <option value="{{ $key }}">{{ $value }}</option>
                @endif
            @endforeach
        @endif
    </select>

    {{-- HINT --}}
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
</div>

<!-- include field specific select2 js-->
@push('crud_fields_scripts')
    <script>
        jQuery(document).ready(function($) {
            // trigger select2 for each untriggered select2 box
            $("#select2_ajax_{{ $field['name'] }}").each(function (i, obj) {
                var select2 = $(obj);

                if (!select2.hasClass("select2-hidden-accessible"))
                {
                    select2.select2({
                        theme: 'bootstrap',
                        language: 'pt-BR',
                        multiple: false,
                        placeholder: "Selecione o País de Fabricação",
                        minimumInputLength: "0",

                        {{-- allow clear --}}
                        allowClear: true,
                    })
                        .on('select2:unselecting', function (e) {
                            $(this).val('').trigger('change');
                            e.preventDefault();
                        })

                }
            });
        });
    </scrip>
@endpush
{{-- End of Extra CSS and JS --}}
{{-- ########################################## --}}

