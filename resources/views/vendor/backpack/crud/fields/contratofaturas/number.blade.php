<div>
    <input
        type="number"
        name="{{ $field['name'] }}"
        id="{{ $field['name'] }}"
        min="0"
        step="0.00000000000000001"
        oninput="validity.valid||(value='0');"
        value="{{ old(square_brackets_to_dots($field['name'])) ?? $field['value'] ?? $field['default'] ?? '' }}"
        @include('crud::inc.field_attributes')
    >
</div>

