@push('before_scripts')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
@endpush

<?php

use App\Models\Empenho;
use App\Models\Naturezasubitem;

$max = isset($field['max']) && (int)$field['max'] > 0 ? $field['max'] : -1;
$min = isset($field['min']) && (int)$field['min'] > 0 ? $field['min'] : -1;
$item_name = strtolower(isset($field['entity_singular']) && !empty($field['entity_singular']) ? $field['entity_singular'] : $field['label']);
$ids_contratofaturasempenho = (isset($field['ids_contratofaturasempenho']) ? $field['ids_contratofaturasempenho'] : null);
$unidadeSisg = isset($field['sisg']) && $field['sisg'] == 'Sim' ? 'Sim' : 'Nao';
$items = old(square_brackets_to_dots($field['name'])) ?? $field['value'] ?? $field['default'] ?? '';

$old_values = old(square_brackets_to_dots('empenho_id'));

$old_valuesSubelemento = old(square_brackets_to_dots('empenhosubelemento'));

$texts = $old_values != null && $old_values[0] != null ? array_map(function ($n, $index) use ($old_valuesSubelemento) {
    if (is_numeric($n)) {
        $empenho = Empenho::where('id', $n)->first();
        $idSubelemento = $old_valuesSubelemento !== null ? $old_valuesSubelemento[$index] : null;

        $subelemento = Naturezasubitem::where('id', $idSubelemento)->first();

        return (isset($empenho) ? [
            'text' => $empenho->descricao(),
            'empenho_id' => $empenho->id,
            'numeroEmpenho' => $empenho->numero,
            'codUg' => $empenho->unidade->codigosiafi ?: $empenho->unidade->codigo,
            'empenhosubelemento' => $old_valuesSubelemento[$index],
            'textSubelemento' => $subelemento ? "$subelemento->codigo - $subelemento->descricao" : ''
        ] : null);
    }
    return null;
}, $old_values, array_keys($old_values)) : [];

// make sure not matter the attribute casting
// the $items variable contains a properly defined JSON
if (is_array($items)) {
    if (count($items)) {
        $items = json_encode($items);
    } else {
        $items = '[]';
    }
} elseif (is_string($items) && !is_array(json_decode($items))) {
    $items = '[]';
}

?>

<div class="col-xs-12" style="margin-top: 30px">
    <div class="form-group required col-md-3">
        <label>{!! $field['label'] !!}</label>
    </div>

    <div class="col-md-3 info-itens-empenho">
        <span class="label label-info">VALOR TOTAL FATURADO</span>
        <span id="info-valor-total-faturado-empenho" class="info-values-empenho"></span>
    </div>

    <div class="col-md-3 info-itens-empenho">
        <span class="label label-info">UTILIZADO</span>
        <span id="info-valor-utilizado-empenho" class="info-values-empenho"></span>
    </div>

    <div class="col-md-3 info-itens-empenho">
        <span class="label label-success label-saldo-empenho">SALDO</span>
        <span id="info-saldo-empenho" class="info-values-empenho">0.00</span>
    </div>
</div>

<div ng-app="backPackTableApp" ng-controller="tableControllerEmpenho" class="col-xs-12">

    @include('crud::inc.field_translatable_icon')

    <input class="array-json" type="hidden" id="{{ $field['name'] }}" name="{{ $field['name'] }}">
    <input class="array-json-oldvaleus" type="hidden" id="{{ $field['name'] }}_oldvalues" name="{{ $field['name'] }}_oldvalues" value="{{ json_encode($texts) }}">
    <input type="hidden" name="ids_contratofaturasempenho" value="{{json_encode($ids_contratofaturasempenho)}}">

    <div class="array-container">
        <div class="progress progress-xs" style="margin-bottom: 0; height: 4px">
            <div class="progress-bar progress-bar-success" id="info-progress-utilizado-empenho" style="width: 0%"></div>
        </div>
        <table class="table table-bordered table-striped m-b-0"
               ng-init="field = '#{{ $field['name'] }}'; items = {{ $items }}; max = {{$max}}; min = {{$min}}; maxErrorTitle = '{{trans('backpack::crud.table_cant_add', ['entity' => $item_name])}}'; maxErrorMessage = '{{trans('backpack::crud.table_max_reached', ['max' => $max])}}'">
            <thead>
            <tr style="width: 10%">
                @foreach( $field['columns'] as $key => $prop )
                    <th style="font-weight: 600!important;">
                        {{ $prop['label'] }} <span @if($unidadeSisg == 'Nao' && $prop['label'] =='Subelemento') class="" @else class="required" @endif></span>
                    </th>
                @endforeach
                <th class="text-center" ng-if="max == -1 || max > 1"></th>
            </tr>
            </thead>

            <tbody ui-sortable="sortableOptions" ng-model="items" class="table-striped table-empenhos-fatura">

            <tr ng-repeat="item in items" class="array-row">

                @foreach( $field['columns'] as $key => $prop)
                    <td style="width: 30%">
                        @if($prop['type'] === 'select2_from_ajax')
                            @include('crud::fields.contratofaturas.select2_from_ajax', [
                                'field' => [
                                    'label' => $prop['label'],
                                    'type' => 'select2_from_ajax',
                                    'name' => $prop['name'],
                                    'items' => $items,
                                    'entity' => $prop['entity'],
                                    'placeholder' => $prop['placeholder'],
                                    'attribute' => $prop['attribute'],
                                    'attribute2' => $prop['attribute2'],
                                    'data_source' => $prop['data_source'],
                                    'method' => 'POST',
                                    'model' => $prop['model'],
                                    'minimum_input_length' => $prop['minimum_input_length'],
                                    'wrapperAttributes' => [
                                        'class' => '',
                                    ],

                                ]
                            ])
                        @endif

                            @if($prop['name'] === 'empenhosubelemento')
                                @include('crud::fields.contratofaturas.select_from_array', [
                                        'field' => [
                                            'label' => '',
                                            'type' => $prop['type'],
                                            'name' => $prop['name'],
                                            'options' => $prop['options'],
                                            'placeholder' => $prop['placeholder'],
                                            'wrapperAttributes' => [
                                                'class' => 'select-subelemento-empenho',
                                            ],
                                        ]
                                    ])
                            @endif

                        @if($prop['type'] === 'text')
                            <div class="input-group">
                                <div class="input-group-addon">
                                    R$
                                </div>

                                <input
                                    name="{{$prop['name']}}[<%$index%>]"
                                    class="form-control empenho"
                                    type="text"
                                    ng-model="item.{{$prop['name']}}"
                                    onkeyup="atualizaMascaraEmpenho(this);calcularValorUtilizadoEmpenho();calcularSaldoEmpenho();"
                                >
                            </div>
                        @endif
                    </td>
                @endforeach

                <td ng-if="max == -1 || max > 1" style="width: 10%; text-align: center">
                    <button style="background-color: red;color: #fff" ng-hide="min > -1 && $index < min"
                            class="btn btn-sm btn-default" type="button" ng-click="removeItem(item);"><span
                            class="sr-only">delete item</span><i class="fa fa-trash" role="presentation"
                                                                 aria-hidden="true"></i></button>
                </td>
            </tr>

            </tbody>

        </table>

        <div class="array-controls btn-group m-t-10">
            <button ng-if="max == -1 || items.length < max" class="btn btn-sm btn-default" type="button"
                    ng-click="addItem()"><i class="fa fa-plus"></i> {{trans('backpack::crud.add')}} {{ $item_name }}
            </button>
        </div>

    </div>
</div>

{{-- FIELD CSS - will be loaded in the after_styles section --}}
@push('crud_fields_styles')

    <style>
        .info-itens-empenho {
            text-align: right;
            padding-right: 0;
            padding-left: 0;
        }

        .info-values-empenho {
            font-weight: 500;
            font-size: 90%;
        }

        span.required::after {
            content: ' *';
            color: #ff0000;
        }
    </style>

@endpush

{{-- FIELD JS - will be loaded in the after_scripts section --}}
@push('crud_fields_scripts')
    <script type="text/javascript">

        /**
         * Ao carregar a página criar um observer para capturar a criação de novas linhas de empenho
         */
        window.onload = function () {
            var target = $('.table-empenhos-fatura')[0];
            var observer = new MutationObserver(function (mutations) {
                initSelectEmpenhoFatura();
            });

            // Configuration of the observer:
            var config = {
                attributes: true,
                childList: true,
                subtree: true,
                characterData: true
            };

            observer.observe(target, config);
        };


        $(document).ready(function () {
            setaInfoValorTotalFaturadoEmpenho();
            calcularSaldoEmpenho();
        });


        /**
         * Evento onchange do select de empenho, após selecionar o empenho busca os subelementos do empenho e verifica
         * se o empenho possui subelemento disponivel para selecionar se não remove a linha do empenho
         *
         * @param event
         * @param element
         */
        function onchangeSelectEmpenhoInstrumento(event, element) {

            var selectedOption = event.target.selectedOptions[0];
            var text = selectedOption.textContent;

            var regex = /^(.*?) - (.*?) - (.*)$/;
            var matches = regex.exec(text);

            if (matches) {
                var numeroEmpenho = matches[1],
                    codUg = matches[2],
                    subelementoElement = $(event.target).closest('tr').find('select.select-subelemento-empenho');


                buscarSubelementoDoEmpenho(numeroEmpenho, subelementoElement, codUg, element.value).then(function () {

                    // Valor do option selecionado do select de empenho
                    var valorSelecionado = element.value;

                    // Busca o primeiro select de empenho
                    var primeiroSelect = $('.select-empenho-fatura').filter(function () {
                        return $(this).val() === valorSelecionado;
                    }).first();

                    if (primeiroSelect.length > 0) {
                        // Encontra o select-subelemento-empenho na mesma linha do select do empenho
                        var subElemento = primeiroSelect.closest('tr').find('.select-subelemento-empenho');

                        // Recupera o valor do atributo data-max-disponivel do option selecionado no select-subelemento-empenho
                        var dataMaxDisponivel = subElemento.find('option:selected').data('max-disponivel');

                    }

                    var totalLinhasEmpenhoESubelemento = $('.select-empenho-fatura').filter(function () {
                        return $(this).val() === element.value;
                    });


                    if(totalLinhasEmpenhoESubelemento.length > dataMaxDisponivel) {
                        var $scope = angular.element($(event.target).closest('tr')).scope();
                        $scope.$apply(function () {

                            $scope.removeItem($(event.target).closest('tr'));

                            Swal.fire(
                                'Ops!',
                                'Sem subelemento disponível!',
                                'warning'
                            )
                        });
                    }
                });
            }
        }

        /**
         *
         * Busca os subelementos do empenho e filtra os subelementos conforme já selecionados nos selects existentes
         * Dispara o change do select para que o valor do select seja atribuido ao campo empenho_json que é o campo
         * que guarda os valores dos campos
         *
         * @param numeroEmpenho
         * @param subelementoElement
         * @param codUg
         * @param idEmpenho
         * @param fromOldValues
         * @param idElementSelected
         * @returns {*}
         */
        function buscarSubelementoDoEmpenho(numeroEmpenho, subelementoElement, codUg, idEmpenho, fromOldValues = false, idElementSelected = '') {

            if (numeroEmpenho.length === 12) {

                var route = `/apropriacao/fatura-form/buscar/subelemento/ajax/${numeroEmpenho}/${codUg}/instcobranca`;

               return $.ajax({
                    url: route,
                    type: 'GET',
                    data: {  },
                    beforeSend: function(){

                    },
                    success: function (result) {
                        var arrKeySubElemento = Object.keys(result);
                        var maxDisponivel = arrKeySubElemento.length;

                        /*preenche o option do select com o subelemento do empenho*/
                        if(arrKeySubElemento.length === 0){
                            subelementoElement.html('<option value="">NÃO ENCONTRADO</option>');
                        }else{

                            var arrayDeObjetos = JSON.parse($('#empenho_json').val());

                            var empenhoFiltrado = arrayDeObjetos.filter(function (item) {
                                var empId = item.empenho_id ? item.empenho_id.toString() : '';
                                return empId === idEmpenho.toString();
                            });

                            if (!fromOldValues) {
                                $.each(empenhoFiltrado, function (index, item) {
                                    var subElementoEncontrado = arrKeySubElemento.indexOf(item.empenhosubelemento);

                                    if (subElementoEncontrado !== -1) {
                                        arrKeySubElemento.splice(subElementoEncontrado, 1);
                                    }
                                });
                            }

                            var option = arrKeySubElemento.map(function(currentValue, index, arr){

                                var selectedElement = currentValue.toString() === idElementSelected.toString() ? 'selected' : '';

                                return '<option data-max-disponivel="'+ maxDisponivel +'" value="' + currentValue +'" '+ selectedElement + '>' + result[currentValue] +'</option>'
                            });

                            subelementoElement.html(option);

                            if (!fromOldValues) {
                                subelementoElement.trigger('change');
                            }
                        }
                    },
                    error: function (result) {
                        new PNotify({
                            title: 'Atenção',
                            text: 'Erro ao buscar subelemento do empenho',
                            type: "alert"
                        });
                    },
                    complete: function(){

                    }
                });
            }else{
                subelementoElement.html('<option value="">NÃO ENCONTRADO</option>');

                return;
            }
        }


        function setaInfoValorTotalFaturadoEmpenho() {
            var valorTotalFaturado = $('#valortotalfaturado_calc').val();

            $('#info-valor-total-faturado-empenho').text(valorTotalFaturado);
        }

        function calcularValorUtilizadoEmpenho(arrayItems = null) {
            if (!arrayItems) {
                arrayItems = $('.empenho').toArray().map(function (el) {
                    return el.value === 'undefined' ? 0 : el.value;
                });
            }

            let valorUtilizado = arrayItems.reduce(function (totalValue, currentEl, currentIndex) {
                var currentValue = '';

                if (typeof currentEl === 'undefined' || currentEl === '') {
                    currentValue = 0;
                } else {
                    currentValue = parseFloat(currentEl.replace(/\./g, '').replace(',', '.'));
                }

                return parseFloat(totalValue) + currentValue;
            }, 0);

            $('#info-valor-utilizado-empenho').text(valorUtilizado.toLocaleString('pt-br', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
        }

        function calcularSaldoEmpenho() {
            var valorTotalFaturado = $('#info-valor-total-faturado-empenho').text(),
                valorUtilizado = $('#info-valor-utilizado-empenho').text(),
                saldo = 0,
                progressBarElement = $('#info-progress-utilizado-empenho'),
                labelSaldo = $('.label-saldo-empenho');

            if (typeof valorTotalFaturado === 'undefined' || valorTotalFaturado === '') {
                valorTotalFaturado = 0;
            } else {
                valorTotalFaturado = parseFloat(valorTotalFaturado.replace(/\./g, '').replace(',', '.'));
            }

            if (typeof valorUtilizado === 'undefined' || valorUtilizado === '') {
                valorUtilizado = 0;
            } else {
                valorUtilizado = parseFloat(valorUtilizado.replace(/\./g, '').replace(',', '.'));
            }

            //caso valorTotalFaturado seja maior ou igual (caminho correto)
            if (parseFloat(valorTotalFaturado) >= parseFloat(valorUtilizado)) {
                saldo = parseFloat(valorTotalFaturado) - parseFloat(valorUtilizado);

                //incrementa ou decrementa barra de status
                var calc = 100 * valorUtilizado / valorTotalFaturado;

                progressBarElement.css('width', parseInt(calc) + '%');

                //adiciona label-success para saldo
                labelSaldo.addClass('label-success');
                labelSaldo.removeClass('label-danger');

                //adiciona class success para progress bar
                progressBarElement.addClass('progress-bar-success');
                progressBarElement.removeClass('progress-bar-danger');
            }

            //caso valorUtilizado seja maior que valorTotalFaturado (caminho incorreto)
            if (parseFloat(valorUtilizado) > parseFloat(valorTotalFaturado)) {
                saldo = parseFloat(valorTotalFaturado) - parseFloat(valorUtilizado);

                //deixa barra de progress preenchida full
                progressBarElement.css('width', '100%');

                //adiciona label-danger para saldo
                labelSaldo.addClass('label-danger');
                labelSaldo.removeClass('label-success');

                //adiciona class danger para progress bar
                progressBarElement.addClass('progress-bar-danger');
                progressBarElement.removeClass('progress-bar-success');
            }

            $('#info-saldo-empenho').text(saldo.toLocaleString('pt-br', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
        }

        function atualizaMascaraEmpenho(element) {
            var maxLength = '000.000.000.000.000,00'.length;
            $('[name="' + element.name + '"]').maskMoney({
                allowNegative: false,
                thousands: '.',
                decimal: ',',
                precision: 2,
                affixesStay: false
            }).attr('maxlength', maxLength).trigger('mask.maskMoney');
        }
    </script>

    <script>
        window.angularApp = window.angularApp || angular.module('backPackTableApp', ['ui.sortable'], function ($interpolateProvider) {
            $interpolateProvider.startSymbol('<%');
            $interpolateProvider.endSymbol('%>');
        });

        window.angularApp.controller('tableControllerEmpenho', function ($scope) {

            $scope.sortableOptions = {
                handle: '.sort-handle',
                axis: 'y',
                helper: function (e, ui) {
                    ui.children().each(function () {
                        $(this).width($(this).width());
                    });
                    return ui;
                },
            };

            /**
             * função disparada no evento onchange do select de subelemento, verifica se aquele sublemento do empenho
             * já foi selecionado se sim remove a linha do subelemento
             *
             */
            $scope.verificarDuplicidadeEmpenhoSubelemento = function() {
                    var conjuntoUnico = {};

                    for (var i = 0; i < $scope.items.length; i++) {
                        var chave = $scope.items[i].empenho_id + '-' + $scope.items[i].empenhosubelemento;

                        if (conjuntoUnico[chave]) {

                            if($scope.items[i].empenho_id !== '' && $scope.items[i].empenho_id !== undefined)
                            {
                                // Mensagem de aviso
                                Swal.fire(
                                    'Ops!',
                                    'Empenho e Subelemento duplicado!',
                                    'warning'
                                );
                                $scope.removeItem($scope.items[i]);
                            }
                        } else {
                            conjuntoUnico[chave] = true;
                        }
                    }
            };

            $scope.addItem = function () {
                if ($scope.max > -1) {
                    if ($scope.items.length < $scope.max) {
                        var item = {};
                        $scope.items.push(item);
                    } else {
                        new PNotify({
                            title: $scope.maxErrorTitle,
                            text: $scope.maxErrorMessage,
                            type: 'error'
                        });
                    }
                } else {
                    var item = {};
                    $scope.items.push(item);
                }
            }

            $scope.removeItem = function (item) {
                var index = $scope.items.indexOf(item);
                $scope.items.splice(index, 1);
            }

            $scope.$watch('items', function (a, b) {
                var arrValorRref = $scope.items.map(function (item) {
                    return item.empenhovalorref ?? '';
                });

                calcularValorUtilizadoEmpenho(arrValorRref);
                calcularSaldoEmpenho();

                if ($scope.min > -1) {
                    while ($scope.items.length < $scope.min) {
                        $scope.addItem();
                    }
                }

                if (typeof $scope.items != 'undefined') {
                    if (typeof $scope.field != 'undefined') {
                        if (typeof $scope.field == 'string') {
                            $scope.field = $($scope.field);
                        }

                        $scope.field.val($scope.items.length ? angular.toJson($scope.items) : null);
                    }
                }
            }, true);

            if ($scope.min > -1) {
                for (var i = 0; i < $scope.min; i++) {
                    $scope.addItem();
                }
            }
        });

        angular.element(document).ready(function () {
            angular.forEach(angular.element('[ng-app]'), function (ctrl) {
                var ctrlDom = angular.element(ctrl);
                if (!ctrlDom.hasClass('ng-scope')) {
                    angular.bootstrap(ctrl, [ctrlDom.attr('ng-app')]);
                }
            });
        });
    </script>
@endpush
