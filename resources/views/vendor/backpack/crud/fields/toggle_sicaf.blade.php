<div class="form-group col-sm-12">
    <b>Consultar situação Fornecedor SICAF</b>

    <a class="btn btn-xs btn-default toggle-sicaf" style="margin-top: 5px;">
        <i class="fa fa-plus-square"></i>
    </a>

    <span class="sicaf-status label" style="margin-left: 20px">
        <i class="fa" aria-hidden="true"></i>

        <span class="not-found" style="display: none">CNPJ não encontrado no SICAF</span>
        <span class="fail" style="display: none">Falha na comunicação com o SICAF</span>
        <span class="sem-informacao" style="display: none">Existe certidão sem informação</span>
    </span>
</div>

@push('crud_fields_scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.toggle-sicaf').click(function() {
                $('.campo-sifac').toggle();
                $('.toggle-sicaf i').toggleClass('fa-plus-square');
                $('.toggle-sicaf i').toggleClass('fa-minus-square');
            });

            $('.campo-sifac').toggle();

            let status = $('#status_sicaf').val();

            if(status == '1') {
                $('.sicaf-status').addClass('label-success');
                $('.sicaf-status i').addClass('fa-check-square');
            } else if(status == '2') {
                $('.sicaf-status').addClass('label-success');
                $('.sicaf-status i').addClass('fa-check-square');

                $('.sicaf-status .sem-informacao').show();
            } else if(status == '3') {
                $('.sicaf-status').addClass('label-danger');
                $('.sicaf-status i').addClass('fa fa-times');
            } else if(status == '404') {
                $('.sicaf-status').addClass('label-warning');
                $('.sicaf-status i').addClass('fa fa-exclamation');
                $('.sicaf-status .not-found').show();
            } else {
                $('.sicaf-status').addClass('label-warning');
                $('.sicaf-status i').addClass('fa fa-exclamation');
                $('.sicaf-status .fail').show();
            }
        });
    </script>
@endpush
