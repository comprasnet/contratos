<!-- field_type_name -->
<div @include('crud::inc.field_wrapper_attributes') style="padding-left: 0px; padding-right: 0px;">
    <div class="row">
        <div class="col-xs-8" id="div_grupo_inserir_itens"></div>
        <div class="col-xs-4 col-md-3 pull-right">
            <div class="input-group">
                <div class="input-group-addon">Valor total do Contrato:</div>
                <input type="text" class="form-control" id="valorTotalItem" readonly value="0">
            </div>
        </div>
    </div>
</div>


<div @include('crud::inc.field_wrapper_attributes') >
    <div class="row">
        <div class="table-responsive">
            <table id="table" class="table table-bordered table-responsive-md table-striped text-center">
                <thead>
                    <tr id="grid-itens"></tr>
                </thead>
                <tbody id="table-itens"></tbody>
            </table>
        </div>
        <div id="itens-para-excluir"></div>
    </div>

    <!-- Janela modal para inserção de registros -->
    <div id="inserir_item" tabindex="-1" class="modal fade"
         role="dialog"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">
                        Novo Item
                    </h3>
                    <button type="button" class="close" id="fechar_modal" data-dismiss="modal" aria-label="Fechar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="textoModal">
                    <div class="form-group">
                        <label for="qtd_item" class="control-label">Tipo Item</label>
                        <select class="form-control" style="width:100%;" id="tipo_item">
                            <option value="">Selecione</option>
                            <option value="{{$field['material']}}">Material</option>
                            <option value="{{$field['servico']}}">Serviço</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="tipo_material" class="control-label">Tipo do Material</label>
                    </div>
                    <div class="form-group">
                        <label class="radio-inline">
                            <input type="radio" class="tipo_material" name="tipo_material" value="Consumo"><b>Consumo</b>
                        </label>

                        <label class="radio-inline">
                            <input type="radio" class="tipo_material" name="tipo_material" value="Permanente"><b>Permanente</b>
                        </label>
                    </div>
                    <div class="form-group">
                        <label for="item" class="control-label">Item</label>
                        <select class="form-control" id="item">
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="numero_item" class="control-label">Número Item Compra<span
                                class="campo-obrigatorio">*</span></label>
                        <input class="form-control" id="numero_item" name="numero_item" type="text">
                    </div>
                    <div class="form-group">
                        <label for="quantidade_item" class="control-label">Quantidade<span
                                class="campo-obrigatorio">*</span></label>
                        <input class="form-control" id="quantidade_item" maxlength="10" name="quantidade_item"
                               type="number" min="1">
                    </div>
                    <div class="form-group">
                        <label for="valor_unit" class="control-label">Valor Unitário<span
                                class="campo-obrigatorio">*</span></label>
                        <input class="form-control" id="valor_unit" name="valor_unit" type="number" min="1">
                    </div>
                    <div class="form-group">
                        <label for="dt_inicio" class="control-label">Data Início</label>
                        <input class="form-control" id="dt_inicio" name="dt_inicio" type="date">
                    </div>
                    <button class="btn btn-danger" type="submit" data-dismiss="modal"><i class="fa fa-reply"></i>
                        Cancelar
                    </button>
                    <button class="btn btn-success" type="button" id="btn_inserir_item"><i
                            class="fa fa-save"></i> Incluir
                    </button>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    @include('vendor.backpack.crud.contrato.modal_inserir_item_compra')

    {{-- HINT --}}
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
</div>

@if ($crud->checkIfFieldIsFirstOfItsType($field))
    {{-- FIELD EXTRA CSS  --}}
    {{-- push things in the after_styles section --}}

    @push('crud_fields_styles')
        <style media="screen">
            .pt-3-half {
                padding-top: 1.4rem;
            }
        </style>
    @endpush

    {{-- FIELD EXTRA JS --}}
    {{-- push things in the after_scripts section --}}

    @push('crud_fields_scripts')
        <script src="{{ asset('vendor/axios') }}/axios.min.js?time={{microtime()}}"></script>
        <script src="{{ asset('vendor/sweetalert/2.10.16.11') }}/sweetalert2.all.min.js?time={{microtime()}}"></script>
        <script type="text/javascript">

            $(document).ready(function () {

                valor_global = 0;
                parcela = $('#num_parcelas').val();

                const $tableID = $('#table');

                $('#numero_item').mask('99999');

                buscarItenContrato();
                retornaCabecalhoGridItens();

                // ao carregar o documento verifica se possui minuta cadastrada
                var arrMinutaEmpenho = $('[name="minutasempenho[]"]').val();

                var button = `@include('crud::buttons.botoes_inserir_itens')`;

                $("#div_grupo_inserir_itens").append(button);

                if (arrMinutaEmpenho.length > 0) {
                    $("#div_inserir_item").remove();
                }

                var valueHidden = $('input[name=adicionaCampoRecuperaGridItens]').val();
                if (valueHidden !== '{' + '{' + 'old(' + '\'name\'' + ')}}') {
                    $('#table').html(valueHidden);

                    if ($('#table-itens').html() !== '') {
                        calculaTotalGlobal();
                    }
                }

                $tableID.on('click', '.table-remove', function () {
                    $(this).parents('tr').detach();
                });

                $('body').on('change', '#tipo_item', function (event) {
                    $('#item').val('');
                    atualizarSelectItem();
                });

                $('body').on('change', '.itens', function (event) {
                    calculaTotalGlobal();
                });

                /*Necessário remover o atributo selected para guardar corretamente o html no campo
                hidden (adicionaCampoRecuperaGridItens)  que é usado para recuperar a tabela com
                os dados já inseridos*/
                $('body').on('change', '[name="tipo_material[]"]', function (event) {
                    var options = $(this).find('option');
                    options.each(function() {
                        $(this).removeAttr('selected');
                    });
                });

                //quando altera o campo de quantidade do item re-calcula os valores
                $('body').on('change', '[name="qtd_item[]"]', function (event) {
                    var tr = this.closest('tr');
                    atualizarValorTotal(tr);
                });

                //quando altera o campo de valor unitario do item re-calcula os valores
                $('body').on('change', 'input[name="vl_unit[]"]', function (event) {
                    var tr = this.closest('tr');
                    atualizarValorTotal(tr);
                });

                //quando altera o campo de valor total do item re-calcula a quantidade
                $('body').on('change', '[name="vl_total[]"]', function (event) {
                    var tr = this.closest('tr');
                    atualizarQuantidade(tr);
                });

                //quando altera o campo de quantidade de parcela atualizar o valor da parcela
                $('body').on('change', '#num_parcelas', function (event) {
                    atualizarParcela();
                });

                //quando altera o campo de periodicidade atualizar o valor global e valor de parcela
                $('body').on('change', 'input[name="periodicidade[]"]', function (event) {
                    var tr = this.closest('tr');
                    atualizarValorTotal(tr);
                    atualizarValorParcela(parcela);
                });

                //quando altera o campo de periodicidade atualizar o valor global e valor de parcela
                $('body').on('change', '#valor_global', function (event) {
                    atualizarValorParcela(parcela);
                });

                $('body').on('click', '#remove_item', function () {
                    removeLinha(this);
                });

            // se possuir minuta de empenho preenchido agora pode inserir item da compra(Buscar Itens da Compra)
                $('body').on('change', '[name="minutasempenho[]"]', function (event) {

                    $("#div_botoes_inserir_itens").remove();

                    var arrMinutaEmpenho = $('[name="minutasempenho[]"]').val();
                let button = `@include('crud::buttons.botoes_inserir_itens')`;
                //603 sempre mostra o botão "Buscar Itens da Compra"
                $("#div_grupo_inserir_itens").append(button);
                //603 não deixar apagar itens selecionados pelo botão da compra
                //caso ainda tenha minuta selecionada em dados contrato
                $("#table-itens tr:not(.item-da-compra)").remove();
                    if (arrMinutaEmpenho.length === 0) {
                    //se não tiver mais minutas, então não tem compra ai apaga todos os itens
                        $("#table-itens tr").remove();
                    }
                if (arrMinutaEmpenho.length > 0) {
                    $("#div_inserir_item").remove();
                }

                $("#select2_ajax_multiple_itens_compra").val(null).trigger('change');

                    retornaCabecalhoGridItens();
                });

                $('body').on('click', '#btn_inserir_item', function (event) {

                    let quantidade = $('#quantidade_item').val()
                    if (quantidade < 0) {
                        exibirAlertaValorNegativo('A quantidade não pode ser menor que zero')
                        return
                    }

                    let valorUnit = $('#valor_unit').val()
                    if (valorUnit < 0) {
                        exibirAlertaValorNegativo('O valor unitário não pode ser menor que zero')
                        return
                    }

                    if (!validarCamposItemModal() && $('#item').val()) {
                        buscarItem($('#item').val());
                        $('#inserir_item').modal('hide');
                    }
                });

                function exibirAlertaValorNegativo(mensagem) {
                    Swal.fire('Alerta!', mensagem, 'warning')
                }

                function validarCamposItemModal() {

                    let tipo_material_checked = false;
                    // 149 == material
                    //150 == serviço
                    if($('#tipo_item').val() == 149){
                        $('.tipo_material').each(function(){
                            if($(this).is(':checked')){
                                tipo_material_checked = true;
                            }
                        });
                    }

                    if($('#tipo_item').val() == 150){
                        tipo_material_checked = true;
                    }

                    let arrCamposModal = [
                        {
                            name: 'numero_item',
                            value: $('#numero_item').val(),
                        },
                        {
                            name: 'quantidade_item',
                            value: $('#quantidade_item').val(),
                        },
                        {
                            name: 'valor_unit',
                            value: $('#valor_unit').val(),
                        },
                        {
                            name: 'tipo_item',
                            value: $('#tipo_item').val(),
                        },
                        {
                            name: 'item',
                            value: $('#item').val(),
                        },
                        {
                            name: 'tipo_material',
                            value: tipo_material_checked
                        }

                    ]
                    return verificarCampoModalItem(arrCamposModal);
                }

                function verificarCampoModalItem(arrCamposModal){
                    var hasError = false;
                    arrCamposModal.forEach(function(campo){
                        $(`#${campo.name}`).closest('.form-group').removeClass('has-error');
                        $(`.${campo.name}`).closest('.form-group').removeClass('has-error');
                        if(!campo.value){
                            $(`#${campo.name}`).closest('.form-group').addClass('has-error');
                            $(`.${campo.name}`).closest('.form-group').addClass('has-error');
                            hasError = true;
                        }
                    });
                    return hasError;
                }

                $('#tipo_item').change(function(){
                    let tipo_item = $(this).val();

                    //149 = material
                    if(tipo_item == '149'){
                        $('[name="tipo_material"]').removeAttr('disabled');
                    }else{
                        $('[name="tipo_material"]').prop('disabled', 'disabled');
                    }
                });
            });

            //atualiza o valor da parcela do contrato
            function atualizarValorParcela(parcela) {
                var valor_global = $('#valor_global').val();
                var valor_parcela = valor_global / parcela;

                $('#valor_parcela').val(parseFloat(valor_parcela.toFixed(4)));
            }

            //atualiza o valor da parcela do contrato
            function atualizarParcela() {
                var valor_global = $('#valor_global').val();
                var num_parcelas = $('#num_parcelas').val();
                var valor = valor_global / num_parcelas;

                $('#valor_parcela').val(parseFloat(valor.toFixed(4)));
            }

            function atualizarValorTotal(tr) {
                var qtd_item = parseFloat($(tr).find('td').eq(5).find('input').val());
                var vl_unit = parseFloat($(tr).find('td').eq(6).find('input').val());
                var periodicidade = parseInt($(tr).find('td').eq(7).find('input').val());
                var vltotal = qtd_item * vl_unit * periodicidade;
                $(tr).find('td').eq(8).find('input').val(parseFloat(vltotal.toFixed(5)));
                calculaTotalGlobal();
            }

            function atualizarQuantidade(tr) {
                var vl_unit = $(tr).find('td').eq(5).find('input').val();
                var valor_total_item = $(tr).find('td').eq(7).find('input').val();

                var quantidade = valor_total_item / vl_unit;

                $(tr).find('td').eq(4).find('input').val(parseFloat(quantidade.toFixed(5)));
                calculaTotalGlobal();
            }

            function atualizarDataInicioItens() {
                $("#table-itens").find('tr').each(function () {
                    if ($(this).find('td').eq(8).find('input').val() === "") {
                        $(this).find('td').eq(8).find('input').val($('input[name=data_assinatura]').val());
                    }
                });
            }

            function montarBotaoMaisDescricao(descricaoCompleta, descricaoResumida, idUnico) {
                let idBotao = `botaoDescricaoCompleta_${idUnico}`
                let funcaoClique = 'exibirDescricaoCompleta(`'+descricaoCompleta+'`,`'+descricaoResumida+'`,`'+idUnico+'`)'

                return `<button type='button' class='btn btn-default btn-xs' onclick='${funcaoClique}' >
                        <span id='${idBotao}' data-tipo='resumido' >
                            <i class='fa fa-fw fa-caret-square-o-down'></i>
                        </span>
                      </button>`
            }

            function adicionaLinhaItem(item) {
                let disabled = "";

                if (!item.periodicidade) {
                    item.periodicidade = 1;
                }

                let colTipoMaterial = "";

                let valorTotal = item.quantidade * parseFloat(item.valorunitario) * item.periodicidade;

                // verifica se são itens de uma minuta e caso seja não pemirte excluir
                var arrMinutaEmpenho = $('[name="minutasempenho[]"]').val();

                let consumoSelected = '';
                let permanenteSelected = '';

                if(item.tipo_material == 'Consumo'){
                    consumoSelected = 'selected';
                }else if(item.tipo_material == 'Permanente'){
                    permanenteSelected = 'selected';
                }

                if(item.descricao == 'Serviço'){
                    colTipoMaterial = '<td><input class="form-control" type="text" readonly value="Não se aplica"></td>';
                    colTipoMaterial += '<input class="form-control" name="tipo_material[]" type="hidden" readonly value="">';
                }else{
                    if(arrMinutaEmpenho.length === 0){

                    colTipoMaterial = '<td>'
                                        +'<select class="form-control" name="tipo_material[]">'
                                        +'<option value="">Selecione</option>'
                                        +'<option value="Consumo" '+consumoSelected+'>Consumo</option>'
                                        +'<option value="Permanente" '+permanenteSelected+'>Permanente</option></select>'
                                        +'</td>';
                    }else{
                        if(item.natureza_despesa){
                            let str = item.natureza_despesa;
                            let res = str.substring(0, 2);
                            if(res == '33'){
                                item.tipo_material = 'Consumo';
                                colTipoMaterial = '<td><input class="form-control" name="tipo_material[]" type="text" readonly value="'+item.tipo_material+'"></td>';
                            }else if(res == '44'){
                                item.tipo_material = 'Permanente';
                                colTipoMaterial = '<td><input class="form-control" name="tipo_material[]" type="text" readonly value="'+item.tipo_material+'"></td>';
                            }
                        }else{
                            colTipoMaterial = '<td><input class="form-control" name="tipo_material[]" type="text" readonly value="'+item.tipo_material+'"></td>';
                        }
                    }

                }

                if(item.descricaodetalhada){
                    item.descricao_complementar = item.descricaodetalhada;
                }

                if(!item.descricao_resumida){
                    item.descricao_resumida = item.descricao_complementar.substring(0,50)
                }else{
                    if(item.descricao_resumida.length > 50){
                        item.descricao_resumida = item.descricao_resumida.substring(0,50)
                    }
                }
                if (item.codigo_ncmnbs == undefined
                    || item.codigo_ncmnbs == null
                    || item.codigo_ncmnbs == ''
                ){
                    item.codigo_ncmnbs = ' - ';
                }


                var newRow = $("<tr>");
                var cols = "";
                cols += '<td>' + item.descricao;
                cols += '<input type="hidden" name="numero_item_compra[]" value="' + item.numero + '">';
                cols += '<input type="hidden" name="catmatseritem_id[]" value="' + item.catmatseritem_id + '">';
                cols += '<input type="hidden" name="tipo_item_id[]" value="' + item.tipo_item_id + '">';
                cols += '<input type="hidden" name="descricao_detalhada[]" value="' + item.descricao_complementar + '">';
                cols += '<input type="hidden" name="saldo_historico_id[]" value="' + item.id + '">';
                cols += '<input type="hidden" name="compra_item_unidade_id[]" value="' + item.compra_item_unidade_id + '">';
                cols += '<input type="hidden" name="codigo_siasg[]" value="' + item.codigo_siasg + '">';
                cols += '</td>';
                cols += colTipoMaterial;
                cols += '<td>' + item.numero + '</td>';

                cols += '<td>'+item.codigo_siasg + ' - <span id="textoDescricaoCompleta_'+item.id+'">' +item.descricao_resumida+'</span> '+montarBotaoMaisDescricao(item.descricao_complementar, item.descricao_resumida, item.id)+' </td>';

                // cols += '<td>' + item.codigo_siasg + ' - ' + item.descricao_complementar + '</td>';
                cols += '<td>'+item.codigo_ncmnbs+'</td>';
                cols += '<td><input class="form-control" type="number"  name="qtd_item[]" step="0.0001" value="' + item.quantidade + '"></td>';
                cols += '<td><input class="form-control" type="number" readonly name="vl_unit[]" step="0.0001" value="' + item.valorunitario + '"></td>';
                cols += '<td><input class="form-control" type="number" name="periodicidade[]" value="' + item.periodicidade + '" data-indice="' + parseInt(item.numero) + '"></td>';
                cols += '<td><input class="form-control" type="number" readonly name="vl_total[]" step="0.0001" value="' + valorTotal + '"></td>';
                cols += '<td><input class="form-control" type="date" name="data_inicio[]" value="' + item.data_inicio + '"></td>';

                cols += '<td><button type="button" class="btn btn-danger" title="Excluir Item" id="remove_item">' +
                    '<i class="fa fa-trash"></i>' +
                    '</button>';
                cols += '</td>';


                newRow.append(cols);
                $("#table-itens").append(newRow);
                calculaTotalGlobal();
            }

            function retornaCabecalhoGridItens(){
                var row = '';
                row += '<th class="text-center">Tipo Item</th>';
                row += '<th class="text-center">Tipo do Material</th>';
                row += '<th class="text-center">Número Item Compra</th>';
                row += '<th class="text-center">Item</th>';
                row += '<th class="text-center">Código NCM/NBS</th>';
                row += '<th class="text-center">Quantidade</th>';
                row += '<th class="text-center">Valor Unitário</th>';
                row += '<th class="text-center">Qtd. parcelas</th>';
                row += '<th class="text-center">Valor Total</th>';
                row += '<th class="text-center">Data Início</th>';

                row += '<th class="text-center">Ações</th>';

                $("#grid-itens").html(row);
            }

            function calculaTotalGlobal() {
                let totalItens = 0;
                $('[name="vl_total[]"]').each(function(index, elementInput){
                    let elInput = parseFloat(elementInput.value);

                    if (!isNaN(elInput)) {
                        totalItens = parseFloat(totalItens) + elInput;
                    }
                })
                totalItens > 0 ? $('#valor_global').val(totalItens.toFixed(2)) : $('#valor_global').val(0);
                totalItens > 0 ? $('#valorTotalItem').val(totalItens.toFixed(2)) : $('#valorTotalItem').val(0);
                $("#table-itens").find('tr').each(function () {
                    var periodicidade = parseInt($(this).find('td').eq(7).find('input').val());
                    //seta num_parcelas
                    if (periodicidade > parcela) {
                        parcela = periodicidade;
                        $('#num_parcelas').val(parcela);
                    }
                });
                atualizarValorParcela(parcela);
            }

            function buscarItenContrato() {
                var instrumentoinicial_id = $("[name=instrumentoinicial_id]").val();
                var url = "{{route('saldo.historico.itens',':id')}}";
                url = url.replace(':id', instrumentoinicial_id);

                axios.request(url)
                    .then(response => {
                        var itens = response.data;
                        var qtd_itens = itens.length;
                        itens.forEach(function (item) {
                            var linhas = $("#table-itens tr").length;
                            if (qtd_itens > linhas) {
                                adicionaLinhaItem(item);
                            }
                        });
                    })
                    .catch(error => {
                        alert(error);
                    })
                    .finally()
            }

            function atualizarSelectItem() {
                $('#item').select2({
                    ajax: {
                        url: urlItens(),
                        dataType: 'json',
                        delay: 250,
                        processResults: function (data) {
                            return {
                                results: $.map(data.data, function (item) {
                                    return {
                                        text: item.codigo_siasg + ' - ' + item.descricao,
                                        id: item.id
                                    }
                                })
                            };
                        },
                        cache: true
                    }
                });
                $('.selection .select2-selection').css("height", "34px").css('border-color', '#d2d6de');
            }

            function urlItens() {
                let url = '{{route('busca.catmatseritens.portipo',':tipo_id')}}';
                url = url.replace(':tipo_id', $('#tipo_item').val());
                return url;
            }

            function buscarItem(id) {
                var url = "{{route('busca.catmatseritens.id',':id')}}";
                url = url.replace(':id', id);

                axios.request(url)
                    .then(response => {
                        prepararItemParaIncluirGrid(response.data);
                    })
                    .catch(error => {
                        alert(error);
                    })
                    .finally()
            }

            function prepararItemParaIncluirGrid(item) {
                item = {
                    'descricao': $('#tipo_item :selected').text(),
                    'descricao_complementar': item.descricao,
                    'quantidade': $('#quantidade_item').val(),
                    'valorunitario': $('#valor_unit').val(),
                    'numero': $('#numero_item').val(),
                    'periodicidade': $('#periodicidade_item').val(),
                    'data_inicio': $('#dt_inicio').val(),
                    'catmatseritem_id': item.id,
                    'tipo_item_id': $('#tipo_item').val(),
                    'codigo_siasg': item.codigo_siasg,
                    'tipo_material': $('.tipo_material:checked').val()
                }

                adicionaLinhaItem(item);
                resetarCamposFormulario();
            }

            function resetarCamposFormulario() {
                $('#tipo_item').val('').change();
                $('#item').val('').change();
                $('#quantidade_item').val('');
                $('#numero_item').val('');
                $('#valor_unit').val('');
                $('#periodicidade_item').val('');
                $('#dt_inicio').val('');
            }

            function removeLinha(elemento) {
                var tr = $(elemento).closest('tr');
                var historicoSaldoItemId = $(tr).find('td').eq(0).find('[name="saldo_historico_id[]"]').val();

                var idContrato = $('[name="contrato_id"]').val();
                var catmatseritem = $(tr).find('td').eq(0).find('[name="catmatseritem_id[]"]').val();
                var codSiasg = $(tr).find('td').eq(0).find('[name="codigo_siasg[]"]').val();
                var tipo = $(tr).find('td').eq(0).find('[name="tipo_item_id[]"]').val();
                var numero = $(tr).find('td').eq(0).find('[name="numero_item_compra[]"]').val();

                var url = "{{route('buscar.itens.termosecobrancas',[':idContrato', ':catmatseritem', ':codSiasg', ':tipo', ':numero'])}}";
                url = url.replace(':idContrato', idContrato);
                url = url.replace(':catmatseritem', catmatseritem);
                url = url.replace(':codSiasg', codSiasg);
                url = url.replace(':tipo', tipo);
                url = url.replace(':numero', numero);

                const sendRequest = async () => {
                    try {
                        const resp = await axios.get(url);
                        var itens = resp.data;
                        if (itens.length > 0) {
                            if(confirm('O item '+numero+' está associado a um ou mais documentos listados abaixo:\n\n- Termo de apostilamento;\n- Termo aditivo;\n- Instrumento de cobrança.\n\nConfirma a exclusão deste item?')) {
                                if (historicoSaldoItemId === 'undefined') {
                                    tr.remove();
                                    calculaTotalGlobal()
                                } else {
                                    removerSaldoHistoricoItem(historicoSaldoItemId);
                                    tr.remove();
                                    calculaTotalGlobal()
                                }
                            }
                        } else {
                            if (historicoSaldoItemId === 'undefined') {
                                tr.remove();
                                calculaTotalGlobal()
                            } else {
                                removerSaldoHistoricoItem(historicoSaldoItemId);
                                tr.remove();
                                calculaTotalGlobal()
                            }
                        }
                    } catch (err) {
                        alert(err);
                    }
                };

                sendRequest();
            }

            function removerSaldoHistoricoItem(historicoSaldoItemId) {
                var newItem = $("#itens-para-excluir");
                var cols = "";
                cols += '<input type="hidden" name="excluir_item[]" value="' + historicoSaldoItemId + '">';
                newItem.append(cols);
            }

            /**
             * atualiza o value do atributo no html
             * necessario para recuperar a tabela de itens com os ultimos dados inseridos nos inputs
             * @param event
             */
            function atualizaValueHTMLCamposAbaItem() {
                $('[name="qtd_item[]"]').each(function (index, elementInput) {
                    elementInput.setAttribute('value', elementInput.value);
                })
                $('[name="vl_unit[]"]').each(function (index, elementInput) {
                    elementInput.setAttribute('value', elementInput.value);
                })
                $('[name="vl_total[]"]').each(function (index, elementInput) {
                    elementInput.setAttribute('value', elementInput.value);
                })
                $('[name="periodicidade[]"]').each(function (index, elementInput) {
                    elementInput.setAttribute('value', elementInput.value);
                })
                $('[name="data_inicio[]"]').each(function (index, elementInput) {
                    elementInput.setAttribute('value', elementInput.value);
                })
                $('[name="tipo_material[]"] option').filter(':selected').each(function(index, elementInput){
                    elementInput.setAttribute('selected', 'selected');
                })
            }

            function tipoMaterial(el, item_id){
                let tipoConsumo = "tipo_consumo_" + item_id;
                let tipoPermanente = "tipo_permanente_" + item_id;

                if($('#'+tipoConsumo).is(':checked') && el.id == tipoConsumo){
                    $('#'+tipoPermanente).prop('checked', false);
                }
                if($('#'+tipoPermanente).is(':checked') && el.id == tipoPermanente){
                    $('#'+tipoConsumo).prop('checked', false);
                }
            }

            function maxNumber(max, min) {
                var running = false;

                return function () {
                    //Para evitar conflito entre o blur e o keyup
                    if (running) return;

                    //
                    running = true;

                    //Se o input for maior que max ele irá fixa o valor maximo no value
                    if (parseFloat(this.value) > max || parseFloat(this.value) < min) {
                        this.value = max;
                    }

                    //Habilita novamente as chamadas do blur e keyup
                    running = false;
                };
            }
        </script>
    @endpush
@endif
