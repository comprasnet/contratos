<!-- radio -->
@php
$optionPointer = 0;
$optionValue = old(square_brackets_to_dots($field['name'])) ?? $field['value'] ?? $field['default'] ?? null;

// if the class isn't overwritten, use 'radio'
if (!isset($field['attributes']['class'])) {
$field['attributes']['class'] = 'radio';
}
@endphp

<div @include('crud::inc.field_wrapper_attributes')>

    <div style="display: flex; align-items: center;">
        <label style="width: 250px;">{!! $field['label'] !!}</label>

        @if( isset($field['options']) && $field['options'] = (array)$field['options'] )

        <div style="display: flex; align-items: center;">
            
            @foreach ($field['options'] as $value => $label )
            @php ($optionPointer++)

            <label class="radio-inline" for="{{$field['name']}}_{{$optionPointer}}" style="margin-right: 30px;">
                <input type="radio"
                        id="{{$field['name']}}_{{$optionPointer}}"
                        name="{{$field['name']}}"
                        value="{{$value}}"
                        @include('crud::inc.field_attributes')
                        {{$optionValue == $value ? 'checked': ''}}
                        > {!! $label !!}
            </label>
            @endforeach
        </div>
        @endif
    </div>
</div>


