<!-- array input -->

<div ng-app="backPackTableApp" ng-controller="tableController" >

    <div class="form-group col-xs-12">
        <label for='chave_acesso_sei'>Chave de Acesso</label><input type="text" name="chave_acesso_sei" id="chave_acesso_sei" value=" @if(isset($fields['chave_acesso_sei']['value'])) {{ $fields['chave_acesso_sei']['value'] }} @else {{old('chave_acesso_sei')}} @endif" onkeyup="minusculo(this)" class="form-control"><span id='resultadoSei' class='text-info'></span>
    </div>
<!--    <div class="form-group col-xs-12">
        <label for='url_sip'>URL API SIP</label><input type="text" name="url_sip" id="url_sip" value="" onkeyup="minusculo(this)" class="form-control"><span id='resultadoSip' class='text-info'></span>
    </div>-->
    <div class='form-group colxs-12' style='text-align:right'>
        <button type="button" class='btn btn-info' id='btnTeste'> <i class='fa fa-link'></i>Testar conexão</button>
    </div>

</div>

{{-- ########################################## --}}
{{-- Extra CSS and JS for this particular field --}}
{{-- If a field type is shown multiple times on a form, the CSS and JS will only be loaded once --}}


    {{-- FIELD CSS - will be loaded in the after_styles section --}}
    @push('crud_fields_styles')
    {{-- @push('crud_fields_styles')
        {{-- YOUR CSS HERE --}}
    @endpush

    {{-- FIELD JS - will be loaded in the after_scripts section --}}
    @push('crud_fields_scripts')
        {{-- YOUR JS HERE --}}
        <script src="{{ asset('vendor/sweetalert/2.10.16.11') }}/sweetalert2.all.min.js?time={{microtime()}}"></script>
        <script>
        $('#btnTeste').on('click', function(){

            if($('#chave_acesso_sei').val().trim() !== ''){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });


                jQuery.ajax({
                    url: "{{route('ajaxTestaConexaoSeiSip')}}",
                    method: 'post',
                    data: {
                        api_integracao_sei: $('#url_sei').val(),
                        api_integracao_sei_chave_acesso: $('#chave_acesso_sei').val(),
                        //api_integracao_sip: $('#url_sip').val()
                    },
                    success: function(result){
                        $('#resultadoSei').hide().html("<i class='fa fa-thumbs-o-up' aria-hidden='true'></i> "+result).fadeIn(2000);
                    }

                });
            }else{

                Swal.fire({
                    position: 'center',
                    icon: 'warning',
                    title: 'Para testar a conexão é necessário a inclusão da chave de acesso.',
                    showConfirmButton: true
                });
            }
        });
        </script>

    @endpush

{{-- End of Extra CSS and JS --}}
{{-- ########################################## --}}
