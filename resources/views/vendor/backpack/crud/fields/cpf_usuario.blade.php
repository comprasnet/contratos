<!-- text input -->
<div @include('crud::inc.field_wrapper_attributes')>
    <label>{!! $field['label'] !!}</label>
    @include('crud::inc.field_translatable_icon')

    @if (isset($field['prefix']) || isset($field['suffix']))
        <div class="input-group">
    @endif
    @if (isset($field['prefix']))
        <div class="input-group-addon">{!! $field['prefix'] !!}</div>
    @endif
    <input type="text" name="{{ $field['name'] }}" id="{{ $field['name'] }}" class="cpfUsuario form-control"
        value="{{ old($field['name']) ?? ($field['value'] ?? ($field['default'] ?? '')) }}" @include('crud::inc.field_attributes')>
    @if (isset($field['suffix']))
        <div class="input-group-addon">{!! $field['suffix'] !!}</div>
    @endif
    @if (isset($field['prefix']) || isset($field['suffix']))
</div>
@endif

{{-- HINT --}}
@if (isset($field['hint']))
    <p class="help-block">{!! $field['hint'] !!}</p>
@endif
</div>

<!-- include field specific select2 js-->
@push('crud_fields_scripts')
    <script type="text/javascript">
        $('#{{ $field['name'] }}').mask('999.999.999-99');
    </script>
    <script>
        jQuery(document).ready(function($) {

            // Variáveis de controle de rota e parâmetros de usuariounidade e usuarioorgao
            const urlParams = new URLSearchParams(window.location.search);
            const ajaxParam = urlParams.get('ajax');
            const ajaxCreate = urlParams.get('ajaxCreate');
            const route = "{{ request()->route()->uri }}";

            if (ajaxParam == 1 || route == "admin/usuario/{usuario}/edit") {
                $('.form-control').each(function(key, item) {
                    $(this).removeAttr('disabled');
                });
            }

            if (ajaxCreate) {
                $('#cpf').val(ajaxCreate);
                $('.form-control').each(function(key, item) {
                    $(this).removeAttr('disabled');
                });
            }

            /*
             * Verifica se já tem usuário cadastrado pra unidade com o cpf passado
             * caso tenha: Ele preenche o campo nome e e-mail, desbloqueia os outros campos e faz o location
             * pra tela de edição
             * Caso não tenha: ele apenas desbloqueia os campos e continua em create
             */
            $('.cpfUsuario').focusout(function() {
                let usuarioAtual = "{{ Request::route('usuario') }}";
                let cpf = $(this).val();
                let nome = $('[name="name"]');
                let email = $('[name="email"]');
              
                if (cpf != '___.___.___-__' && cpf != '') {
                    $.ajax({
                        url: '/admin/usuariocpf',
                        method: 'post',
                        data: {
                            'cpfusuario': cpf
                        },
                        success: function(data) {
                        data = JSON.parse(data);
                     
                            console.log(data)

                            if (data.empty) {
                                $('.form-control').each(function(key, item) {
                                    $(this).removeAttr('disabled');
                                  
                                
                                    // Se o campo cpf for mudado para um que não exista na base
                                    // ele redireciona pra create
                                    if (ajaxParam == 1 && ajaxParam != null) {
                                        // Desabilita os inputs enquanto o ajax carrega
                                        $(this).prop('disabled', 'disabled');
                                        location.href =
                                            "/admin/usuario/create?ajaxCreate=" +
                                            cpf;
                                    }
                                });
                            } else {
                                // se usuarioAtual != data.id ele recarrega a página novamente
                                // para editar o usuário correto
                                if ((ajaxParam != 1 && ajaxParam != null) || usuarioAtual != data.id) {

                                     location.href = "/admin/usuario/" + data.id +
                                        "/edit?ajax=1";
                                        
                                    $('.form-control').each(function(key, item) {
                                        // Desabilita os inputs enquanto o ajax carrega
                                        $(this).prop('disabled', 'disabled');
                                    });                                
                                }
                            }
                        },
                    });
                }
            });

        });
    </script>
@endpush
{{-- End of Extra CSS and JS --}}
{{-- ########################################## --}}
