<div ng-app="backPackTableApp" ng-controller="tableController">
    <div class="array-container form-group">
        <label for="arquivos" id='labelFile'>Selecionar arquivo(s)</label>
        <input type="file" name="arquivos[]" id="arquivos" multiple accept="application/pdf">
        <div id="fileError" style="color: red; display: none;">O tamanho total dos arquivos não deve exceder 30MB.</div>
        @php
            $tipos = $fields['arquivos']['options']['tipos'];

            $arrayItems = '';
            foreach ($tipos as $key => $value) {
                $arrayItems .= "<option value='$key'>$value</option>";
            }

            $tiposVinculadosNoPncp = $fields['arquivos']['options']['tiposVinculadosNoPncp'];


        @endphp

        <div id="novos-arquivos"></div>
        <div id='resultado'></div>

        @push('crud_fields_styles')
            <style>
                .erro {
                    border: 1px solid #dd4b39;
                }

                input[type="file"] {
                    display: none;
                }

                #labelFile {
                    padding: 6px 6px;
                    line-height: 1.42857143;
                    width: 150px;
                    background-color: #3c8dbc;
                    border-radius: 3px;
                    color: #FFF;
                    font-weight: normal;
                    text-align: center;
                    display: block;
                    margin-top: 10px;
                    cursor: pointer;
                }

                .widthInput {
                    width: 100px;
                }
            </style>
        @endpush

        @push('crud_fields_scripts')
            <script>

                var tiposVinculadosNoPncp = @json($tiposVinculadosNoPncp);

                $('#arquivos').on('change', function () {
                    var fileUpload = $("#arquivos").get(0);
                    var files = fileUpload.files;
                    var mediafilename = "";
                    var totalSize = 0;
                    var maxSize = 30 * 1024 * 1024;

                    for (var i = 0; i < files.length; i++) {
                        totalSize += files[i].size;
                    }

                    if (totalSize > maxSize) {
                        $('#fileError').show();
                        new PNotify({
                            title: 'Atenção',
                            text: 'O tamanho total dos arquivos não deve exceder 30MB.',
                            type: "error"
                        });
                        $('#novos-arquivos').hide();

                        $("#arquivos").val('');
                        return;
                    } else {
                        $('#fileError').hide();
                    }
                    mediafilename = '<div class="table-responsive"><table class="table table-bordered table-striped m-b-0">';
                    mediafilename += '<thead>';
                    mediafilename += '<tr>';
                    mediafilename += '<th>Nome do Arquivo</th>';
                    mediafilename += '<th>Tipo <span style = "color: red">*</span></th>';
                    mediafilename += '<th>Termo</th>';
                    mediafilename += '<th>Descrição</th>';
                    mediafilename += '<th>Nº Sapiens</th>';
                    mediafilename += '<th>Restrito?<i class="fa fa-fw fa-info-circle" title="Arquivo restrito não será disponibilizado publicamente."></i></th>';
                    mediafilename += '</tr>';
                    mediafilename += '</thead>';
                    mediafilename += '<tbody>';
                    for (var i = 0; i < files.length; i++) {

                        mediafilename += "<tr>";
                        mediafilename += "<td><input type='text' value='" + files[i].name + "' name='nome[]' class='form-control' readonly></td>";
                        mediafilename += "<td><select name='tipo[]'  class='form-control validar'  id='tipo" + i + "' style='width:100px' onchange='buscaContratoHistoricoIDARQUIVO(this.value," + i + ",this.options[this.selectedIndex].text)'>";
                        mediafilename += "<option value='0'>Selecione...</option>";
                        mediafilename += "@php echo $arrayItems; @endphp";
                        mediafilename += "</select></td>";
                        mediafilename += "<td>";
                        mediafilename += "   <span class='escondePNCP' style='display:none' id='spanTermo" + i + "'></span>";
                        mediafilename += "</td>";
                        mediafilename += "<td><input type='text' name='descricao[]'  class='form-control widthInput'></td>";
                        mediafilename += "<td><input type='text' name='processoSapiens[]' class='form-control widthInput'></td>";

                        mediafilename += `<td><label><input type="radio" name="restrito[${i}]" value="1" checked> Sim </label><br/><label><input type="radio" name="restrito[${i}]" value="0"> Não</td></label>`;

                        mediafilename += "</tr>";

                    }
                    mediafilename += '</tody>';
                    mediafilename += '</table>';
                    mediafilename += '</div>';

                    $('#novos-arquivos').hide().html(mediafilename).fadeIn(2000);
                });

                /*
                * Lista todos os anexos locais do contrato atual
                */
                $('body').on('click', '#enviolocaldeanexos', function () {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                    });

                    var idProcesso = $('select[name="contrato_id"]').find(":selected").val();
                    var sequencial_documento = '';
                    var descricao = '';
                    var tipo = '';
                    var comboAux = '';
                    var comboFinal = '';
                    var termo = '';
                    jQuery.ajax({
                        url: "{{route('ajaxLinksGravados')}}",
                        method: 'post',
                        data: {
                            idProcesso: idProcesso
                        },
                        success: function (result) {
                            if (result.length > 0) {
                                sequencia_documento = result['sequencial_documento'] ? result['sequencial_documento'] : '';
                                var aux = "<h4 style='font-size:100%'>Arquivos deste Contrato</h4><hr>";
                                aux += "<div class='table-responsive'><table class='table table-responsive-md' width='100%' id='tableEscolhaAnexo' >";
                                aux += "     <thead>";
                                aux += "     <tr>";
                                aux += "         <th></th>";
                                aux += "         <th>Nome do Arquivo</th>";
                                aux += "         <th>Tipo</th>";
                                aux += "         <th>Termo</th>";
                                aux += "         <th>Descricao</th>";
                                aux += "         <th>Nº do documento</th>";
                                aux += "         <th>Restrito<i class='fa fa-fw fa-info-circle' title='Arquivo restrito não será disponibilizado publicamente.'></i></th>";
                                aux += "     </tr>";
                                aux += "     </thead>";
                                aux += "     <tbody>";

                                for (i = 0; i < result.length; i++) {

                                    if (result[i]['origem'] == '0') {
                                        //set o option do select
                                        tipo = result[i]['tipo'];
                                        comboAux = "<?php echo $arrayItems; ?>";
                                        comboFinal = comboAux.replace(tipo, tipo + "' selected='");
                                        comboFinal = comboFinal.replace('"', "'");
                                        descricao = result[i]['descricao'] ? result[i]['descricao'] : '';
                                        termo = result[i]['numero'] ? result[i]['numero'] : '';
                                        documentoRestrito = result[i]['restrito'] ? 'Sim' : 'Não';

                                        sequencial_documento = result[i]['sequencial_documento'] ? result[i]['sequencial_documento'] : '';

                                        aux += "         <tr>";
                                        aux += "             <td><a href='/storage/" + result[i]['arquivos'] + "' target='_blank'><i class='fa fa-search'  style='padding-left:15px;cursor:pointer' title='Visualizar o anexo'></i></a></td>";
                                        aux += "             <td><input type='text' name='nome[]' class='form-control' value='" + descricao + "' readonly></td>";
                                        aux += "             <td>";
                                        aux += "                 <select name='tipoSei[]'  class='form-control' disabled>";
                                        aux += "                     <option value=''>Selecione...</option>";
                                        aux += comboFinal;
                                        aux += "                 </select>";
                                        aux += "             </td>";
                                        aux += "             <td><input type='text' name='termo[]' value='" + termo + "' class='form-control' disabled></td>";
                                        aux += "             <td><input type='text' name='descricaoSei[]' value='" + descricao + "' class='form-control' style='width:100%'  disabled></td>";
                                        aux += "             <td><input type='text' name='descricaoSei[]' value='" + sequencial_documento + "' class='form-control' style='width:100%'  disabled></td>";
                                        aux += "             <td><input type='text' name='descricaoSei[]' value='" + documentoRestrito + "' class='form-control' style='width:100%'  disabled></td>";
                                        aux += "         </tr>";
                                    }
                                }
                                aux += "     </tbody>";
                                aux += " </table></div>";

                            }
                            $('#resultado').hide().html(aux).fadeIn(2000);
                        }
                    });

                });

                function buscaContratoHistoricoIDARQUIVO(arg, idContratoHistorico, idTipo) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                    });
                    var idTipo = idTipo;
                    var idProcesso = $('select[name="contrato_id"]').find(":selected").val();
                    let elaboracao = $('#elaboracao').val();

                    jQuery.ajax({
                        url: "{{url('api/termosPorTipo')}}",
                        method: 'get',
                        data: {
                            q: idTipo,
                            tipo: arg,
                            contrato_id: idProcesso,
                            elaboracao: elaboracao,
                            dataType: 'json'
                        },
                        success: function (result) {
                            var selectField = '';

                            let classValidar = ''
                            let contratoLei14133 = $('input[name="contratoLei14133"]').val()
                            if (contratoLei14133 == true) {
                                classValidar = 'validar'
                                // Variável criada no arquivo form_save_buttons
                                mensagemAlertaCampoVazio = 'É obrigatória a vinculação do Termo correspondente para o tipo de arquivo selecionado.'
                            }
                            selectField += "<select name='contratohistorico_id[]' id='contratohistorico_id" + idContratoHistorico + "' class='form-control widthInput " + classValidar + "'>";
                            if (tiposVinculadosNoPncp.includes(idTipo)) {
                                if (result.length == 1) {
                                    selectField += "  <option value='" + result[0].id + "'>" + result[0].numero + "</option>";
                                } else if (result.length > 0) {
                                    selectField += "  <option value='0'>Selecione</option>";
                                    for (j = 0; j < result.length; j++) {
                                        selectField += "  <option value='" + result[j].id + "'>" + result[j].numero + "</option>";
                                    }
                                } else {
                                    selectField += "   <option value='0'>Não encontrou</option>";
                                }
                            } else {
                                selectField += "   <option value=''>Não se aplica</option>";
                            }
                            selectField += "</select>";
                            $('.escondePNCP').show();
                            $('#spanTermo' + idContratoHistorico).hide().html(selectField).fadeIn(2000);

                            function teste(campo) {
                                console.log(campo)
                            }
                        }
                    });

                }


                $('#enviolocaldeanexos').trigger('click');

            </script>
        @endpush
    </div>
</div>
