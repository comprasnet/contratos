<div @include('crud::inc.field_wrapper_attributes')>
    <label>{!! $field['label'] !!}</label>
    @include('crud::inc.field_translatable_icon')

    {{-- HINT com imagem i --}}
    @if(isset($field['ico_help']))
        <i class="fa fa-info-circle" title="{!! $field['ico_help'] !!}"></i>
    @endif

    <textarea
        id="{{ $field['name'] }}_textarea"
        name="{{ $field['name'] }}"
        @include('crud::inc.field_attributes')
        maxlength="5120"
        oninput="updateCharCount(this, '{{ $field['name'] }}')"
    >{{ old(square_brackets_to_dots($field['name'])) ?? $field['value'] ?? $field['default'] ?? '' }}</textarea>

    <div>
        <span id="{{ $field['name'] }}_charCount">0</span>/5120 caracteres
    </div>

    {{-- HINT --}}
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
</div>

<script>
    function updateCharCount(textarea, fieldName) {
        // Normalizar quebras de linha para `\n`
        const normalizedValue = textarea.value.replace(/\r\n|\r/g, '\n');

        // Usar Array.from para contagem correta
        const charCount = Array.from(normalizedValue).length;

        // Atualizar o contador no DOM
        const charCountElement = document.getElementById(fieldName + '_charCount');
        if (charCountElement) {
            charCountElement.innerText = charCount;
        }
    }

    // Inicializar o contador ao carregar a página
    document.addEventListener('DOMContentLoaded', function() {
        const textareas = document.querySelectorAll('textarea');
        textareas.forEach(function(textarea) {
            const fieldName = textarea.getAttribute('name');
            updateCharCount(textarea, fieldName);
        });
    });
</script>
