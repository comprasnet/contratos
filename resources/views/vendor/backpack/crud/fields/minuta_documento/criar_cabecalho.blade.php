<!-- used for heading, separators, etc -->
<div @include('crud::inc.field_wrapper_attributes') >

    <input
            type="button"
            id="btn-criarOuRemoverCabecalho"
            class="btn btn-block btn-primary"
            onclick="criarOuRemoverCabecalho();"
            value="Criar cabeçalho"
    >
    </input>
</div>

@push('after_scripts')

    <script type="text/javascript">

        $(document).ready(function () {

        });

        function criarOuRemoverCabecalho() {
            // elemento ckeditor
            let ckeditorElement = CKEDITOR.instances['ckeditor-texto_minuta'];
            let conteudoDocumento = ckeditorElement.getData();

            if (conteudoDocumento !== '') {
                if (conteudoDocumento.search('elementos-cabecalho') !== -1) {
                    removerCabecalho(ckeditorElement);
                    alterarTextoEStyleBotaoCriarCabecalho(false);
                } else {
                    criarCabecalho(ckeditorElement);
                    alterarTextoEStyleBotaoCriarCabecalho(true);
                    return;
                }
            } else {
                criarCabecalho(ckeditorElement);
                alterarTextoEStyleBotaoCriarCabecalho(true);
                return;
            }
        }

        function criarCabecalho(ckeditorElement) {

            //caminho do brasao
            let pathImageBrasao = "{{ asset('/img/brasao_de_armas_da_republica_2.png') }}";

            let nomeUnidade =       "<span class='item-cabecalho' style='margin: 1px'>{{$dadosUnidadeCabecalho['nomeUnidadeUsuarioLogado']}}</span>";
            let nomeOrgao =         "<span class='item-cabecalho' style='margin: 1px'>{{$dadosUnidadeCabecalho['nomeOrgaoUsuarioLogado']}}</span>";
            let nomeOrgaoSuperior = "<span class='item-cabecalho' style='margin: 1px'>{{$dadosUnidadeCabecalho['nomeOrgaoSuperiorUsuarioLogado']}}</span>";

            //corpo do cabecalho com brasao e dados da unidade do usuario logado
            let corpoCabecalho = CKEDITOR.dom.element.createFromHtml(
                "<div id=elementos-cabecalho style='text-align: center' class='item-cabecalho' " +
                "<span class='item-cabecalho'><img src=" + pathImageBrasao + " alt='Imagem de Brasão de Armas da República'/></span>" +
                "<br>" +
                nomeOrgaoSuperior +
                "<br>" +
                nomeOrgao +
                "<br>" +
                nomeUnidade +
                "</div>"
            );

            //caso texto de documento vazio apenas insere o cabecalho
            if (ckeditorElement.getData() === '') {

                ckeditorElement.insertElement(corpoCabecalho);

            }
            //caso o texto de documento tenha conteúdo
            else {

                //guarda o conteudo antigo
                let documentBody = ckeditorElement.getData();
                //limpa o campo de texto do documento
                ckeditorElement.setData('', function () {

                    //inseri o cabeçalho
                    ckeditorElement.insertElement(corpoCabecalho);
                    //inseri o conteudo após inserir o cabeçalho
                    ckeditorElement.insertHtml(documentBody);
                });

            }
        }

        function removerCabecalho(ckeditorElement) {
            console.log(ckeditorElement.getData())
            //ckeditorElement.editable().findOne('#elementos-cabecalho').remove();
            ckeditorElement.editable().findOne('.item-cabecalho').remove();
            console.log(ckeditorElement.getData())
        }

        function alterarTextoEStyleBotaoCriarCabecalho(booCriarCabecalho)
        {
            if(booCriarCabecalho)
            {

                $('#btn-criarOuRemoverCabecalho').removeClass('btn btn-block btn-primary');
                $('#btn-criarOuRemoverCabecalho').addClass('btn btn-block btn-danger');
                $('#btn-criarOuRemoverCabecalho').attr('value', 'Remover cabeçalho');
                return;
            }

            $('#btn-criarOuRemoverCabecalho').removeClass('btn btn-block btn-danger');
            $('#btn-criarOuRemoverCabecalho').addClass('btn btn-block btn-primary');
            $('#btn-criarOuRemoverCabecalho').attr('value', 'Criar cabeçalho');
        }
    </script>

    <style>
        .item-cabecalho
        {

        }
    </style>

@endpush