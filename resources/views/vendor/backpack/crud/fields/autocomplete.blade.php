<!-- resources/views/vendor/backpack/crud/fields/autocomplete.blade.php -->

@php
    // Recupere as configurações do campo
    $options = json_encode($field['options']);
@endphp

    <!-- Campo de entrada para o autocompletar -->
<div @include('crud::inc.field_wrapper_attributes') >
<label for="">{{$field['label']}}</label>
<select
       disabled
       class="form-control empenhos-select2"
       name="{{$field['name']}}"
       style="width: 100%"
       data-options="{{ $options }}"
    @include('crud::inc.field_attributes')
>
</select>
</div>
@push('crud_fields_styles')
    <!-- include select2 css-->
    <link href="{{ asset('vendor/adminlte/bower_components/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
@endpush
@push('crud_fields_scripts')

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-rc.0/js/select2.min.js"></script>
<script>

    // public/js/custom.js (ou qualquer arquivo JavaScript que você esteja usando)

    jQuery(document).ready(function($) {
        let csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
        let unidade_id = null;
        let fornecedor = $("#select2_ajax_fornecedor_id").val();

        // Habilita/Desabilita o campo empenho quando sai o foco do campo fornecedor
        $(".select2").focusout(function() {
            if ($(this).prev().attr('id') === 'select2_ajax_fornecedor_id') {
                atualizarEstadoEmpenho();
            }
        });

        // Verifica o valor inicial do campo fornecedor e habilita/desabilita o campo empenho
        if ($("#select2_ajax_fornecedor_id").val()) {
            atualizarEstadoEmpenho();
        }

        $(".empenhos-select2").select2({
            theme: 'bootstrap',
            language: {
                inputTooShort: function () {
                    return "Digite pelo menos 4 caracteres";
                },
                searching: function() {
                    return "Buscando...";
                },
            },
            ajax: {
                method: 'POST',
                url: "{{ route('autocomplete') }}", // Use a rota personalizada 'autocomplete'
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    fornecedor = $("#select2-select2_ajax_fornecedor_id-container").html();
                    unidade_id = $("#select2-unidadeempenho_id-container").html();

                    return {
                        _token: csrfToken,
                        query: params.term,
                        unidade_id: unidade_id,
                        fornecedor: fornecedor
                    };
                },
                processResults: function (data) {
                    let empenhos = data.map(function(val, i) {
                        return {
                            id: val.id,
                            text: val.numero
                        };
                    });
                    return {
                        results: empenhos
                    };

                },
                cache: true
            },
            placeholder: 'Selecione uma opção', // Mensagem do placeholder
            minimumInputLength: 4
        });


    });

    /**
     * Verifica se o campo de empenho deve estar habilitado ou não
     */
    function atualizarEstadoEmpenho() {
        let textoFornecedor = $('#select2-select2_ajax_fornecedor_id-container').text();
        let empenhoHabilitado = textoFornecedor.trim() !== 'Selecione o favorecido';
        $(".empenhos-select2").prop('disabled', !empenhoHabilitado);
    }

</script>
@endpush
