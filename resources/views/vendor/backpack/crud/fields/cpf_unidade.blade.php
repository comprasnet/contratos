<!-- text input -->
<div @include('crud::inc.field_wrapper_attributes')>
    <label>{!! $field['label'] !!}</label>
    @include('crud::inc.field_translatable_icon')

    @if (isset($field['prefix']) || isset($field['suffix']))
        <div class="input-group">
    @endif
    @if (isset($field['prefix']))
        <div class="input-group-addon">{!! $field['prefix'] !!}</div>
    @endif
    <input type="text" name="{{ $field['name'] }}" id="{{ $field['name'] }}" class="cpfUnidade form-control"
        value="{{ old($field['name']) ?? ($field['value'] ?? ($field['default'] ?? '')) }}" @include('crud::inc.field_attributes')>
    @if (isset($field['suffix']))
        <div class="input-group-addon">{!! $field['suffix'] !!}</div>
    @endif
    @if (isset($field['prefix']) || isset($field['suffix']))
</div>
@endif

{{-- HINT --}}
@if (isset($field['hint']))
    <p class="help-block">{!! $field['hint'] !!}</p>
@endif
</div>

<style>
    .select2-container--default .select2-selection--multiple {
    pointer-events: none;
    background-color: #f8f9fa;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice {
    color: #000;
    }
</style>

<!-- include field specific select2 js-->
@push('crud_fields_scripts')
    <script type="text/javascript">
        $('#{{ $field['name'] }}').mask('999.999.999-99');
    </script>
    <script>
        jQuery(document).ready(function($) {

            // Variáveis de controle de rota e parâmetros de usuariounidade e usuarioorgao
            const urlParams = new URLSearchParams(window.location.search);
            const ajaxParam = urlParams.get('ajax');
            const ajaxCreate = urlParams.get('ajaxCreate');
            const route = "{{ request()->route()->uri }}";

            if (ajaxParam == 1 || route == "admin/usuariounidade/{usuariounidade}/edit") {
                $('.form-control').each(function(key, item) {
                    $(this).removeAttr('disabled');
                });
                // Obtém a URL completa
                const url = window.location.href;
                // Usa uma expressão regular para pegar o número logo após 'usuariounidade/'
                const idMatch = url.match(/usuariounidade\/(\d+)/);
                // Verifica se encontrou o ID e o extrai
                const userId = idMatch[1];
                const dadosUsuario = JSON.parse(sessionStorage.getItem('unidade'));
                const idUsuario = sessionStorage.getItem('idusuario');
             
                if(dadosUsuario && idUsuario == userId){
                    $(document).ready(function() {
                        var selectContainer = $('#ugprimaria').next('.select2-container');
                        selectContainer.attr('title', 'Esta unidade não pode ser alterada, você não pertence ao grupo responsável por ela.');
                        selectContainer.find('.select2-selection').css({
                            'pointer-events': 'none',
                            'background-color': '#f8f9fa'
                        });
                    });

                    const select = document.getElementById("ugprimaria");

                    while (select.options.length > 1) {
                        select.remove(1);
                    }

                    // Adiciona os novos valores ao select
                    for (const [value, text] of Object.entries(dadosUsuario)) {
                        const option = document.createElement("option");
                        option.value = value;
                        option.text = text;
                        option.selected = true;
                        select.add(option);
                    }

                    $("#ugprimaria option:selected").attr("selected", "selected");
                }       

                $(document).ready(function() {
                    // Inicialize o Select2
                    $('#select2_ajax_multiple_unidades2').select2({
                        // Desabilitar a remoção de opções
                        templateSelection: function(selection) {
                            // Retorne a opção selecionada sem o botão de remoção
                            return $('<span>' + selection.text + '</span>');
                        },
                        // Você pode definir o templateResult para que nada seja exibido para o botão de remoção
                        templateResult: function(result) {
                            return $('<span>' + result.text + '</span>');
                        }
                    });

                    // Desabilitar o 'X' de remoção após a inicialização
                    $('#select2_ajax_multiple_unidades2').on('select2:select', function(e) {
                        $('.select2-selection__choice__remove').remove(); // Remove o 'x' de todas as opções logo após a seleção
                    });
                });
            }

            if (ajaxCreate) {
                $('#cpf').val(ajaxCreate);
                $('.form-control').each(function(key, item) {
                    $(this).removeAttr('disabled');
                });
            }

            /*
             * Verifica se já tem usuário cadastrado pra unidade com o cpf passado
             * caso tenha: Ele preenche o campo nome e e-mail, desbloqueia os outros campos e faz o location
             * pra tela de edição
             * Caso não tenha: ele apenas desbloqueia os campos e continua em create
             */
            $('.cpfUnidade').focusout(function() {
                let usuarioAtual = "{{ Request::route('usuariounidade') }}";
                let cpf = $(this).val();
                let nome = $('[name="name"]');
                let email = $('[name="email"]');
              
                if (cpf != '___.___.___-__' && cpf != '') {
                    $.ajax({
                        url: '/admin/usuariounidadecpf',
                        method: 'post',
                        data: {
                            'cpfunidade': cpf
                        },
                        success: function(data) {
                        data = JSON.parse(data);
                        sessionStorage.clear();
                            if(!data.empty && !data.validaUg){
                            // Inicialmente desabilita o select com pointer-events e background color
                            $('#ugprimaria').next('.select2-container').attr('title',
                             'Esta unidade não pode ser alterada, você não pertence ao grupo responsável por ela.').find('.select2-selection').css({ 
                                    'pointer-events': 'none',
                                    'background-color': '#f8f9fa'
                                });
                                sessionStorage.setItem('idusuario', data.id);
                                sessionStorage.setItem('unidade', JSON.stringify(data.unidadeUserEdit));
                                let dadosUsuario = JSON.parse(sessionStorage.getItem('unidade'));
                                const select = document.getElementById("ugprimaria");

                                while (select.options.length > 1) {
                                    select.remove(1);
                                }

                                // Adiciona os novos valores ao select
                                for (const [value, text] of Object.entries(dadosUsuario)) {
                                    const option = document.createElement("option");
                                    option.value = value; // Usa a chave como value
                                    option.text = text;   // Usa o valor como 
                                    option.selected = true; // Marca esta opção como selecionada
                                    select.add(option);
                                }
                               
                                $("#ugprimaria option:selected").attr("selected", "selected");
                            }

                            if (data.empty) {
                                $('.form-control').each(function(key, item) {
                                    $(this).removeAttr('disabled');
                                  
                                    // oculta a div de unidades, visualização das unidades, so mostra em edição
                                    $('.form-group').each(function() {
                                        var labelText = $(this).find('label').text().trim();
                                        var select = $(this).find('select');

                                        if (labelText === "UGs/UASGs" && select.attr('name') === "unidades2[]" && select.attr('id') === "select2_ajax_multiple_unidades2") {
                                            $(this).hide(); // Oculta a div
                                        }
                                    });

                                    // Se o campo cpf for mudado para um que não exista na base
                                    // ele redireciona pra create
                                    if (ajaxParam == 1 && ajaxParam != null) {
                                        // Desabilita os inputs enquanto o ajax carrega
                                        $(this).prop('disabled', 'disabled');
                                        location.href =
                                            "/admin/usuariounidade/create?ajaxCreate=" +
                                            cpf;
                                    }
                                });
                            } else {
                                // se usuarioAtual != data.id ele recarrega a página novamente
                                // para editar o usuário correto
                                if ((ajaxParam != 1 && ajaxParam != null) || usuarioAtual != data.id) {

                                     location.href = "/admin/usuariounidade/" + data.id +
                                        "/edit?ajax=1";
                                        
                                    $('.form-control').each(function(key, item) {
                                        // Desabilita os inputs enquanto o ajax carrega
                                        $(this).prop('disabled', 'disabled');
                                    });                                
                                }
                            }
                        },
                    });
                }
            });

        });
    </script>
@endpush
{{-- End of Extra CSS and JS --}}
{{-- ########################################## --}}
