@if($field['subTitle'])
<div style="margin-top: 15px; margin-bottom: 20px;">
    <span class="sub-title">Em dias</span>
</div>
@endif

<div @include('crud::inc.field_wrapper_attributes')>
    <div style="display: flex; align-items: center;">
        <div style="width: 250px;">
            <label for="{{ $field['name'] }}">{{ $field['label'] }}</label>
            @include('crud::inc.field_translatable_icon')
            @if(isset($field['ico_help']))
            <i class="fa fa-info-circle" title="{!! $field['ico_help'] !!}"></i>
            @endif
        </div>
        <input type="text" name="{{ $field['name'] }}_uteis" value="{{ old($field['name'].'_uteis') ?? ($field['typeValue'] == 'Úteis' ? $field['value'] : '') ?? '' }}" maxlength="2" class="input-group-addon" style="width: 10%; margin-right: 5px;">
        <span style="width: 50px;">{{ $field['title1'] }}</span>
        <input type="text" name="{{ $field['name'] }}_corridos" value="{{ old($field['name'].'_corridos') ?? ($field['typeValue'] == 'Corridos' ? $field['value'] : '')?? '' }}" maxlength="2" class="input-group-addon" style="width: 10%; margin-right: 5px;">
        <span>{{ $field['title2'] }}</span>
    </div>
</div>

<script>
    function numeroOrdinal(number) {
        if (number > 31) {
            return '';
        } else {
            return number + '°';
        }
    }

    function validaCampos(input1, input2) {
        input1.addEventListener('input', function() {
            this.value = this.value.replace(/\D/g, '');
            if (this.value.trim() !== '') {
                input2.value = '';
            }
        });

        input2.addEventListener('input', function() {
            this.value = this.value.replace(/\D/g, '');
            if (this.value.trim() !== '') {
                input1.value = '';
            }
        });
    }

    function setupFields(fieldName) {
        var input1 = document.querySelector('input[name="' + fieldName + '_uteis"]');
        var input2 = document.querySelector('input[name="' + fieldName + '_corridos"]');

        if (input1 && input2) {
            validaCampos(input1, input2);
        }

        input1.addEventListener('input', function() {
            if (input1.value === '0') {
                input1.value = '';
            }
        });

        input2.addEventListener('input', function() {
            if (input2.value === '0') {
                input2.value = '';
            }
        });
    }

    document.addEventListener('DOMContentLoaded', function() {
        var fields = [
            '{{ $field['name'] }}'
        ];

        fields.forEach(function(fieldName) {
            setupFields(fieldName);
        });
      
        var data_pagamento_recorrente_uteis = document.querySelector('input[name="data_pagamento_recorrente_uteis"]');
    
        if (data_pagamento_recorrente_uteis) {
            data_pagamento_recorrente_uteis.addEventListener('change', function() {
                var valor = parseInt(data_pagamento_recorrente_uteis.value, 10);

                if (valor < 31) {
                    data_pagamento_recorrente_uteis.value = numeroOrdinal(valor);
                } else {
                    data_pagamento_recorrente_uteis.value = '';
                }
            });
        }
    });
</script>


<style>
    .sub-title {
        position: absolute;
        z-index: 999;
        margin-left: calc(250px + 20px);
        margin-top: -20px;
    }
</style>