<!-- array input -->

<?php
$max = isset($field['max']) && (int) $field['max'] > 0 ? $field['max'] : -1;
$min = isset($field['min']) && (int) $field['min'] > 0 ? $field['min'] : -1;
$item_name = strtolower(isset($field['entity_singular']) && !empty($field['entity_singular']) ? $field['entity_singular'] : $field['label']);
$ids_contratofaturasmesano = (isset($field['ids_contratofaturasmesano'] ) ? $field['ids_contratofaturasmesano'] : null);

$items = old(square_brackets_to_dots($field['name'])) ?? $field['value'] ?? $field['default'] ?? '';

// make sure not matter the attribute casting
// the $items variable contains a properly defined JSON
if (is_array($items)) {
    if (count($items)) {
        $items = json_encode($items);
    } else {
        $items = '[]';
    }
} elseif (is_string($items) && !is_array(json_decode($items))) {
    $items = '[]';
}

?>

<div class="col-xs-12">
    <div class="form-group required col-md-3">
        <label>{!! $field['label'] !!}</label>
    </div>

    <div class="col-md-3 info-itens">
        <span class="label label-info">VALOR TOTAL FATURADO</span>
        <span id="info-valor-total-faturado" class="info-values"></span>
    </div>

    <div class="col-md-3 info-itens">
        <span class="label label-info">UTILIZADO</span>
        <span id="info-valor-utilizado" class="info-values"></span>
    </div>

    <div class="col-md-3 info-itens">
        <span class="label label-success label-saldo">SALDO</span>
        <span id="info-saldo" class="info-values">0.00</span>
    </div>
</div>

<div ng-app="backPackTableApp" ng-controller="tableController" class="col-xs-12">

    @include('crud::inc.field_translatable_icon')

    <input class="array-json" type="hidden" id="{{ $field['name'] }}" name="{{ $field['name'] }}">
    <input type="hidden" name="ids_contratofaturasmesano" value="{{json_encode($ids_contratofaturasmesano)}}">

    <div class="array-container">
        <div class="progress progress-xs" style="margin-bottom: 0; height: 4px">
            <div class="progress-bar progress-bar-success" id="info-progress-utilizado" style="width: 0%"></div>
        </div>
        <table class="table table-bordered table-striped m-b-0" ng-init="field = '#{{ $field['name'] }}'; items = {{ $items }}; max = {{$max}}; min = {{$min}}; maxErrorTitle = '{{trans('backpack::crud.table_cant_add', ['entity' => $item_name])}}'; maxErrorMessage = '{{trans('backpack::crud.table_max_reached', ['max' => $max])}}'">

            <thead>
            <tr style="width: 10%">

                @foreach( $field['columns'] as $key => $prop )
                    <th style="font-weight: 600!important;">
                        {{ $prop['label'] }} <span class="required"></span>
                    </th>
                @endforeach
                <th class="text-center" ng-if="max == -1 || max > 1"></th>
            </tr>
            </thead>

            <tbody ui-sortable="sortableOptions" ng-model="items" class="table-striped">

            <tr ng-repeat="item in items" class="array-row">

                @foreach( $field['columns'] as $key => $prop)
                    <td style="width: 30%" class="form-group">
                        @if($prop['type'] === 'select2_from_array_fatura_mes_ano')
                            @include('crud::fields.select2_from_array_fatura_mes_ano',
                                [
                                    'field' => [
                                        'options' => $prop['options'],
                                        'name' => "{$prop['name']}",
                                        'label' => '',
                                        'allows_null' => true,
                                        'type' => 'select2_from_array',
                                        'wrapperAttributes' => [
                                            'class' => ''
                                            ],
                                        'attributes' => [
                                            'class' => 'form-control',
                                                        ],
                                               ]
                                ]
                                )
                        @endif
                        @if($prop['type'] === 'text')
                            <div class="input-group">
                                <div class="input-group-addon">
                                    R$
                                </div>
                                <input
                                    name="{{$prop['name']}}[<%$index%>]"
                                    class="form-control valor_ano_mes"
                                    type="text"
                                    ng-model="item.{{$prop['name']}}"
                                    onkeyup="atualizaMascara(this);calcularValorUtilizado();calcularSaldo();">
                            </div>
                        @endif

                    </td>
                @endforeach
                <td ng-if="max == -1 || max > 1" style="width: 10%; text-align: center">
                    <button style="background-color: red;color: #fff" ng-hide="min > -1 && $index < min" class="btn btn-sm btn-default" type="button" ng-click="removeItem(item);"><span class="sr-only">delete item</span><i class="fa fa-trash" role="presentation" aria-hidden="true"></i></button>
                </td>
            </tr>

            </tbody>

        </table>

        <div class="array-controls btn-group m-t-10">
            <button ng-if="max == -1 || items.length < max" class="btn btn-sm btn-default" type="button" ng-click="addItem()"><i class="fa fa-plus"></i> {{trans('backpack::crud.add')}} {{ $item_name }}</button>
        </div>

    </div>
</div>

{{-- ########################################## --}}
{{-- Extra CSS and JS for this particular field --}}
{{-- If a field type is shown multiple times on a form, the CSS and JS will only be loaded once --}}
@if ($crud->checkIfFieldIsFirstOfItsType($field))

    {{-- FIELD CSS - will be loaded in the after_styles section --}}
    @push('crud_fields_styles')

        <style>
            .info-itens {
                text-align: right;
                padding-right: 0;
                padding-left: 0;
            }

            .info-values {
                font-weight: 500;
                font-size: 90%;
            }

            span.required::after {
                content: ' *';
                color: #ff0000;
            }
        </style>

    @endpush

    {{-- FIELD JS - will be loaded in the after_scripts section --}}
    @push('crud_fields_scripts')
        {{-- YOUR JS HERE --}}
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.8/angular.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-sortable/0.14.3/sortable.min.js"></script>

        <script type="text/javascript">

            $(document).ready(function (){
                setaInfoValorTotalFaturado();
                calcularValorUtilizado();   // #527 - adicionado para que a barra de progressão não se perca quando der erro no envio dos dados.
                calcularSaldo();
            });

            function setaInfoValorTotalFaturado(){

                var valorTotalFaturado = $('#valortotalfaturado_calc').val();

                $('#info-valor-total-faturado').text(valorTotalFaturado);
            }

            function calcularValorUtilizado(arrayItems = null){

                if(!arrayItems){
                    arrayItems = $('.valor_ano_mes').toArray().map(function(el){
                       return el.value === 'undefined' ? 0 : el.value;
                    });
                }

                let valorUtilizado =  arrayItems.reduce(function ( totalValue, currentEl, currentIndex ) {

                    var currentValue = '';

                    if(typeof currentEl === 'undefined' || currentEl === ''){
                        currentValue = 0;
                    }else{
                        currentValue = parseFloat(currentEl.replace(/\./g,'').replace(',', '.'));
                    }

                    return parseFloat(totalValue) + currentValue;
                }, 0);

                $('#info-valor-utilizado').text(valorUtilizado.toLocaleString('pt-br', {
                    minimumFractionDigits: 2,
                    maximumFractionDigits: 2
                }));
            }

            function calcularSaldo(){
                var valorTotalFaturado = $('#info-valor-total-faturado').text(),
                    valorUtilizado = $('#info-valor-utilizado').text(),
                    saldo = 0,
                    progressBarElement = $('#info-progress-utilizado'),
                    labelSaldo = $('.label-saldo');

                if(typeof valorTotalFaturado === 'undefined' || valorTotalFaturado === ''){
                    valorTotalFaturado = 0;
                }else{
                    valorTotalFaturado = parseFloat(valorTotalFaturado.replace(/\./g,'').replace(',', '.'));
                }

                if(typeof valorUtilizado === 'undefined' || valorUtilizado === ''){
                    valorUtilizado = 0;
                }else{
                    valorUtilizado = parseFloat(valorUtilizado.replace(/\./g,'').replace(',', '.'));
                }

                //caso valorTotalFaturado seja maior ou igual (caminho correto)
                if(parseFloat(valorTotalFaturado) >= parseFloat(valorUtilizado)){
                    saldo = parseFloat(valorTotalFaturado) - parseFloat(valorUtilizado);

                    //incrementa ou decrementa barra de status
                    var calc = 100 * valorUtilizado / valorTotalFaturado;

                    progressBarElement.css('width', parseInt(calc) + '%');

                    //adiciona label-success para saldo
                    labelSaldo.addClass('label-success');
                    labelSaldo.removeClass('label-danger');

                    //adiciona class success para progress bar
                    progressBarElement.addClass('progress-bar-success');
                    progressBarElement.removeClass('progress-bar-danger');
                }

                //caso valorUtilizado seja maior que valorTotalFaturado (caminho incorreto)
                if(parseFloat(valorUtilizado) > parseFloat(valorTotalFaturado)){
                    saldo = parseFloat(valorTotalFaturado) - parseFloat(valorUtilizado);

                    //deixa barra de progress preenchida full
                    progressBarElement.css('width', '100%');

                    //adiciona label-danger para saldo
                    labelSaldo.addClass('label-danger');
                    labelSaldo.removeClass('label-success');

                    //adiciona class danger para progress bar
                    progressBarElement.addClass('progress-bar-danger');
                    progressBarElement.removeClass('progress-bar-success');
                }

                $('#info-saldo').text(saldo.toLocaleString('pt-br', {
                    minimumFractionDigits: 2,
                    maximumFractionDigits: 2
                }));
            }


            function atualizaMascara(element) {
                var maxLength = '000.000.000.000.000,00'.length;
                $('[name="'+element.name+'"]').maskMoney({
                    allowNegative: false,
                    thousands: '.',
                    decimal: ',',
                    precision: 2,
                    affixesStay: false
                }).attr('maxlength', maxLength).trigger('mask.maskMoney');
            }
        </script>
        <script>
            window.angularApp = window.angularApp || angular.module('backPackTableApp', ['ui.sortable'], function($interpolateProvider){
                $interpolateProvider.startSymbol('<%');
                $interpolateProvider.endSymbol('%>');
            });

            window.angularApp.controller('tableController', function($scope){

                $scope.sortableOptions = {
                    handle: '.sort-handle',
                    axis: 'y',
                    helper: function(e, ui) {
                        ui.children().each(function() {
                            $(this).width($(this).width());
                        });
                        return ui;
                    },
                };

                $scope.addItem = function(){
                    if( $scope.max > -1 ){
                        if( $scope.items.length < $scope.max ){
                            var item = {};
                            $scope.items.push(item);
                        } else {
                            new PNotify({
                                title: $scope.maxErrorTitle,
                                text: $scope.maxErrorMessage,
                                type: 'error'
                            });
                        }
                    }
                    else {
                        var item = {};
                        $scope.items.push(item);
                    }
                }

                $scope.removeItem = function(item){
                    var index = $scope.items.indexOf(item);
                    $scope.items.splice(index, 1);
                }

                $scope.$watch('items', function(a, b){

                    var arrValorRref = $scope.items.map(function(item){
                        return item.valorref ?? '';
                    });

                    calcularValorUtilizado(arrValorRref);
                    calcularSaldo();

                    if( $scope.min > -1 ){
                        while($scope.items.length < $scope.min){
                            $scope.addItem();
                        }
                    }

                    if( typeof $scope.items != 'undefined' ){

                        if( typeof $scope.field != 'undefined'){
                            if( typeof $scope.field == 'string' ){
                                $scope.field = $($scope.field);
                            }
                            $scope.field.val( $scope.items.length ? angular.toJson($scope.items) : null );
                        }
                    }
                }, true);

                if( $scope.min > -1 ){
                    for(var i = 0; i < $scope.min; i++){
                        $scope.addItem();
                    }
                }
            });

            angular.element(document).ready(function(){
                angular.forEach(angular.element('[ng-app]'), function(ctrl){
                    var ctrlDom = angular.element(ctrl);
                    if( !ctrlDom.hasClass('ng-scope') ){
                        angular.bootstrap(ctrl, [ctrlDom.attr('ng-app')]);
                    }
                });
            })

        </script>

    @endpush
@endif
{{-- End of Extra CSS and JS --}}
{{-- ########################################## --}}
