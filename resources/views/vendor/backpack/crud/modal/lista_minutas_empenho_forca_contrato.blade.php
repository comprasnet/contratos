<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
     id="modal-lista-minutas-empenho-forca-contrato" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form
                action="{{route('contrato.tipo.empenho')}}"
                enctype="multipart/form-data"
                method="post"
                id="form_criar_contratos_tipo_empenho"
            >
                {!! csrf_field() !!}
                <div class="modal-header">
                    <h2>Minutas de Empenho Substitutivas de Contrato</h2>
                    <div class="text-right">
                        <i data-dismiss="modal" aria-label="Close" class="fa fa-close"></i>
                    </div>
                </div>
                <div class="modal-body">
                    <div>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th scope="col"><input type="checkbox" name="marcar_todos" id="marcar_todos" value=""></th>
                                <th scope="col">Número do empenho</th>
                                <th scope="col">CNPJ do fornecedor</th>
                                <th scope="col">Nome do fornecedor</th>
                                <th scope="col">Data de Emissão</th>
                                <th scope="col">Valor do Empenho</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($arrMinutas != null)
                                @foreach($arrMinutas as $minuta)
                                    <tr>
                                        <th scope="row"><input type="checkbox" name="minuta[]" class="minuta-check" value="{{ $minuta['id'] }}"></th>
                                        <td>{{ $minuta['numero_empenho'] }}</td>
                                        <td>{{ $minuta['cnpj_fornecedor'] }}</td>
                                        <td>{{ $minuta['nome_fornecedor'] }}</td>
                                        <td>{{ $minuta['data_emissao'] }}</td>
                                        <td>{{ $minuta['valor'] }}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <th scope="row"></th>
                                    <td>Nenhum registro encontrado</td>
                                    <td>Nenhum registro encontrado</td>
                                    <td>Nenhum registro encontrado</td>
                                    <td>Nenhum registro encontrado</td>
                                    <td>Nenhum registro encontrado</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <div id="erro" class="alert alert-danger" role="alert">
                        Escolha ao menos um registro!
                    </div>
                    <div id="progressbar">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    @if($arrMinutas != null)
                        <button type="button" class="btn btn-primary" id="submeter-dados">Criar Contrato(s) do tipo Empenho</button>
                    @endif
                </div>
            </form>
        </div>
    </div>
</div>
@push('after_scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $("#erro").hide();

            $('#marcar_todos').click(function(){
                var c = this.checked;
                $(':checkbox').prop('checked', c);
            });

            $('#submeter-dados').click(function (event){
                var verifica = $(".minuta-check").is(':checked');

                if(verifica) {
                    $('#progressbar').html(
                        "{!! ProgressBar::normal(100)->animated() !!}" +
                        "<div><span>Enviando dados...</span></div>"
                    );
                    event.preventDefault();
                    $("#form_criar_contratos_tipo_empenho").submit();
                    $(this).css('pointer-events', 'none');
                    return false;
                } else {
                    $("#erro").show();
                }
            });

            $("input[type=checkbox]").click(function(){
                if($(".minuta-check").is(':checked')) {
                    if($("#erro").is(':visible')) {
                        $("#erro").hide();
                    }
                }
            });

            // Selecionar/desmarcar checkbox ao clicar na linha
            $('table tbody tr').click(function(e) {
                // Ignorar o clique diretamente no checkbox
                if ($(e.target).is('input[type=checkbox]')) return;

                // Obter o checkbox da linha
                var checkbox = $(this).find('input[type=checkbox]');
                var isChecked = checkbox.prop('checked');

                checkbox.prop('checked', !isChecked);
            });
        });
    </script>
@endpush
