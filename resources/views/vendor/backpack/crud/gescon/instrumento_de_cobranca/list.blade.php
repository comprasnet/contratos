@extends('backpack::layout')

@section('header')
    <section class="content-header">
        <h1>
            <span class="">{!! $crud->getHeading() ?? $crud->entity_name_plural !!}</span>
            <small id="datatable_info_stack">{!! trans('backpack::crud.all').'<span>'.$crud->entity_name_plural.'</span> '.trans('backpack::crud.in_the_database') !!}
                .
            </small>
        </h1>
        <h2>
            <small>
                {!! $crud->getSubheading() ?? '' !!}
            </small>
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url(config('backpack.base.route_prefix'), 'dashboard') }}">{{ trans('backpack::crud.admin') }}</a>
            </li>
            <li><a href="{{ url($crud->route) }}" class="">{{ $crud->entity_name_plural }}</a></li>
            <li class="active">{{ trans('backpack::crud.list') }}</li>
        </ol>
    </section>
@endsection

@section('content')
    <!-- Default box -->

    <form name="alterarSituacaoEmLote" method="POST" action="{{ route('alterarsituacaoemlote') }}">
    {!! csrf_field() !!}

        <input type="hidden" name="situacaoDesejada" >

        <div class="row">

            <!-- THE ACTUAL CONTENT -->
            <div class="{{ $crud->getListContentClass() }}">
                <div class="">

                    <div class="row m-b-10">
                        <div class="col-xs-12 col-md-6">

                            @if ( $crud->buttons->where('stack', 'top')->count() ||  $crud->exportButtons())
                                <div class="hidden-print {{ $crud->hasAccess('create')?'with-border':'' }}">

                                    @include('crud::inc.button_stack', ['stack' => 'top'])
                                    <div id="datatable_button_stack" class="pull-right text-right hidden-xs"></div>
                                </div>
                            @endif
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div id="datatable_search_stack" class="pull-right"></div>
                        </div>
                    </div>

                    {{-- Backpack List Filters --}}
                    @if ($crud->filtersEnabled())
                        @include('crud::inc.filters_navbar')
                    @endif

                    <div class="overflow-hidden">

                        <table id="crudTable" class="box table table-striped table-hover display responsive nowrap m-t-0"
                            cellspacing="0">
                            <thead>
                            <tr>
                                {{-- Table columns --}}
                                @foreach ($crud->columns as $column)
                                    <th
                                            data-orderable="{{ var_export($column['orderable'], true) }}"
                                            data-priority="{{ $column['priority'] }}"
                                            data-visible-in-modal="{{ (isset($column['visibleInModal']) && $column['visibleInModal'] == false) ? 'false' : 'true' }}"
                                            data-visible="{{ !isset($column['visibleInTable']) ? 'true' : (($column['visibleInTable'] == false) ? 'false' : 'true') }}"
                                            data-visible-in-export="{{ (isset($column['visibleInExport']) && $column['visibleInExport'] == false) ? 'false' : 'true' }}"
                                    >
                                        {{-- Table columns - nome das colunas no topo da tabela --}}
                                        {!! $column['label'] !!}
                                    </th>
                                @endforeach

                                {{-- Table columns - nome da coluna Ações no topo da tabela --}}
                                @if ( $crud->buttons->where('stack', 'line')->count() )
                                    <th data-orderable="false" data-priority="{{ $crud->getActionsColumnPriority() }}"
                                        data-visible-in-export="false">{{ trans('backpack::crud.actions') }}</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            <tr>
                                {{-- Table columns - nome das colunas no rodapé --}}
                                @foreach ($crud->columns as $column)
                                    <th>{!! $column['label'] !!}</th>
                                @endforeach

                                {{-- Table columns - nome da coluna Ações no rodapé --}}
                                @if ( $crud->buttons->where('stack', 'line')->count() )
                                    <th>{{ trans('backpack::crud.actions') }}</th>
                                @endif
                            </tr>
                            </tfoot>
                        </table>

                        {{--@if ( $crud->buttons->where('stack', 'bottom')->count() )--}}
                        @if ( $crud->buttons->where('stack', 'bottom')->count() || $crud->exportButtons())
                            <div id="bottom_buttons" class="hidden-print">
                                @include('crud::inc.button_stack', ['stack' => 'bottom'])

                                <div id="datatable_button_stack" class="pull-right text-right hidden-xs"></div>
                            </div>
                        @endif

                    </div><!-- /.box-body -->

                </div><!-- /.box -->
            </div>

        </div>
    </form>

@endsection

@section('after_styles')
    <!-- DATA TABLES -->
    <link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap.min.css">

    <link rel="stylesheet" href="{{ asset('vendor/backpack/crud/css/crud.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/backpack/crud/css/form.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/backpack/crud/css/list.css') }}">

    <!-- CRUD LIST CONTENT - crud_list_styles stack -->
    @stack('crud_list_styles')
@endsection

@section('after_scripts')

    {{-- Aqui é onde mostra os registros listados na tela --}}
    @include('crud::inc.datatables_logic_alterarsituacaoemlote')

    <script src="{{ asset('vendor/backpack/crud/js/crud.js') }}"></script>
    <script src="{{ asset('vendor/backpack/crud/js/form.js') }}"></script>
    <script src="{{ asset('vendor/backpack/crud/js/list.js') }}"></script>

    <!-- CRUD LIST CONTENT - crud_list_scripts stack -->
    @stack('crud_list_scripts')
@endsection

    @include('crud::inc.modal_alterar_situacao_em_lote')
    <!-- Abaixo funções js para a página de listagem instrumentos de cobrança. -->
    <script src="{{ asset('vendor/backpack/crud/js/alterar_situacao_em_lote.js') }}"></script>
