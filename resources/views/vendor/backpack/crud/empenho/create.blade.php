@extends('backpack::layout')

@section('header')
	<section class="content-header">
	  <h1>
        <span class="text-capitalize">{!! $crud->getHeading() ?? $crud->entity_name_plural !!}</span>
        <small>{!! $crud->getSubheading() ?? trans('backpack::crud.add').' '.$crud->entity_name !!}.</small>
	  </h1>
	  <ol class="breadcrumb">
	    <li><a href="{{ url(config('backpack.base.route_prefix'), 'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
	    <li><a href="{{ url($crud->route) }}" class="text-capitalize">{{ $crud->entity_name_plural }}</a></li>
	    <li class="active">{{ trans('backpack::crud.add') }}</li>
	  </ol>
	</section>
@endsection

@section('content')

@if ($crud->hasAccess('list'))
	<a href="{{ starts_with(URL::previous(), url($crud->urlVoltar)) ? URL::previous() : url($crud->urlVoltar) }}" class="hidden-print"><i class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span>{{ $crud->entity_name_plural }}</span></a>
@endif
<div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
            <p class="alert alert-{{ $msg }}">{!!  Session::get('alert-' . $msg) !!}</p>
        @endif
    @endforeach
</div>
<div class="row m-t-20">
	<div class="{{ $crud->getCreateContentClass() }}">
    @include('backpack::mod.empenho.telas.cabecalho')
		<!-- Default box -->

		@include('crud::inc.grouped_errors')

		  <form method="post"
		  		action="{{ url($crud->route) }}"
				@if ($crud->hasUploadFields('create'))
				enctype="multipart/form-data"
				@endif
		  		>
		  {!! csrf_field() !!}
              @if(isset($crud->update_form) && $crud->update_form !== false)
                  {!! method_field('PUT') !!}
              @endif
		  <div class="col-md-12">

		    <div class="row display-flex-wrap">
		      <!-- load the view from the application if it exists, otherwise load the one in the package -->
		      @if(view()->exists('vendor.backpack.crud.form_content'))
                    <div class="box box-solid box-primary">
                        @if(strpos(URL::current(),'buscacompra') !== false )
                        <div class="box-header">
                            <h3 class="box-title label-title">Tipo: </h3>
                            <input type="radio" value="1" checked name="tipoEmpenho" id="opc_contrato">
                            <label for="opc_contrato" class="margin-right-10"><h3 class="box-title">Contrato</h3></label>
                            <input type="radio" value="2" name="tipoEmpenho" id="opc_compra">
                            <label for="opc_compra" class="margin-right-10"><h3 class="box-title">Compra</h3></label>
                            <input type="radio" value="3" name="tipoEmpenho" id="opc_suprimento">
                            <label for="opc_suprimento" class="margin-right-10"><h3 class="box-title">Suprimento</h3></label>
                            <input type="radio" value="4" name="tipoEmpenho" id="opc_mercado_gov">
                            <label for="opc_mercado_gov" class="margin-right-10"><h3 class="box-title">Contrata+Brasil</h3></label>
                        </div>
                        @endif
                        <div>
                            @include('crud::form_content', [ 'fields' => $crud->getFields('create'), 'action' => 'create' ])
                        </div>
                    </div>
		      @endif
		    </div><!-- /.box-body -->
              <div class="">
                  @include('backpack::mod.empenho.botoes',['rota' => $crud->urlVoltar])
              </div>

		  </div><!-- /.box -->
		  </form>
	</div>
</div>
@endsection
@push('after_scripts')
    <style type="text/css">
        .margin-right-10{
            margin-right: 10px;
            cursor: pointer
        }
        .label-title{
            margin-right: 10px !important;
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function() {
            initializeTipoEmpenhoToggle();
        });

        function initializeTipoEmpenhoToggle() {
            $('input[type=radio][name=tipoEmpenho]').change(handleTipoEmpenhoChange);
        }

        function handleTipoEmpenhoChange() {
            const tipoEmpenho = $(this).val();

            if (tipoEmpenho === '1') {
                configureFormForContrato();
            } else if (tipoEmpenho === '2') {
                configureFormForCompra();
            } else if (tipoEmpenho === '3') {
                configureFormForSuprimento();
            } else if (tipoEmpenho === '4') {
                configureFormForContratacao();
            }
        }

        function configureFormForContrato() {
            toggleFormOptions({
                compra: true,
                contrato: false,
                suprimento: true,
                beneficiaria: true
            });
            resetForm();
        }

        function configureFormForCompra() {
            toggleFormOptions({
                compra: false,
                contrato: true,
                suprimento: true
            });
            resetForm();
        }

        function configureFormForSuprimento() {
            toggleFormOptions({
                compra: true,
                contrato: true,
                suprimento: false,
            });
            resetForm();
        }
        function configureFormForContratacao() {
            toggleFormOptions({
                compra: true,
                contrato: true,
                suprimento: true,
                contratacao: false,
                beneficiaria: true
            });
            resetForm();
        }
        function toggleFormOptions({ compra, contrato, suprimento, contratacao = true, beneficiaria = false }) {
            $('.opc_compra').prop("disabled", compra);
            $('.opc_contrato').prop("disabled", contrato);
            $('.opc_suprimento').prop("disabled", suprimento);
            $('.opc_beneficiaria').prop("disabled", beneficiaria);
            $('.opc_mercado_gov').prop("disabled", contratacao);
        }

        function resetForm() {
            clearOptions('.opc_contrato');
            clearOptions('#select2_ajax_unidade_origem_id');
            resetInputValue('#numero_ano');
            resetSelect2FromArray();
            resetSuprido();
        }

        function clearOptions(selector) {
            $(selector + ' option').remove();
        }

        function resetInputValue(selector) {
            $(selector).val('');
        }

        function resetSelect2FromArray() {
            setSelect2Placeholder('#select2-opc_compra_modalidade-container', 'Selecione...');
        }

        function resetSuprido() {
            setSelect2Placeholder('#select2-select2_ajax_fornecedor_empenho_id-container', 'Selecione o suprido');
        }

        function setSelect2Placeholder(selector, placeholder) {
            $(selector).attr('title', placeholder);
            $(selector).text(placeholder);
        }
    </script>

@endpush
