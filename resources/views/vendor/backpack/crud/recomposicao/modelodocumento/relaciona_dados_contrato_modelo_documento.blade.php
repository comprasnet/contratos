

@php
$nome_campo = (isset($ckeditor_field_name))? $ckeditor_field_name : '';
$traduzir_token = 'nao';
if(!isset($tokens)){
  $tokens = json_encode([]);
}else{
  $traduzir_token = 'sim';
}
$urlAtual = explode('/',url()->current());

$idContrato = \Route::current()->parameter('contrato_id');

$urlMontarTokenContrato = url("/gescon/contrato/{$idContrato}/minutas/montartokencontrato/{$idContrato}");

$urlMontarTokenResponsavel = url("/gescon/contrato/{$idContrato}/minutas/montartokenresponsavel/{$idContrato}");

$urlMontarTokenItemContrato = url("/gescon/contrato/{$idContrato}/minutas/montartokenitemcontrato/{$idContrato}");

$urlMontarInformacaoTabelaItemContrato = url("/gescon/contrato/{$idContrato}/minutas/montarinformacaotabelaitemcontrato");

$urlMontarTokenEmpenho = url("/gescon/contrato/{$idContrato}/minutas/montartokenempenho/{$idContrato}");

$urlMontarInformacaoTabelaEmpenho = url("/gescon/contrato/{$idContrato}/minutas/montarinformacaotabelaempenho");

$contratoId = \Route::current()->parameter('contrato_id');
$userUgId = session()->get('user_ug_id');
$urlRecuperarMinutaDocumentoPorTipoDocumento = url("/gescon/contrato/{$contratoId}/minutas/minutasparacopiar/{$userUgId}");
$urlExibirMinutaDocumentoPorTipoDocumento = url("/gescon/contrato/{$contratoId}/minutas/exibirparacopiar/{$userUgId}");

$dataAtual = date("d/m/Y");

@endphp

@if($nome_campo != '')
    @push('after_scripts')
        <script src="{{ asset('vendor/sweetalert/2.10.16.11') }}/sweetalert2.all.min.js?time={{microtime()}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.7/jquery.inputmask.min.js" integrity="sha512-jTgBq4+dMYh73dquskmUFEgMY5mptcbqSw2rmhOZZSJjZbD2wMt0H5nhqWtleVkyBEjmzid5nyERPSNBafG4GQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script type="text/javascript">

          function inserirModeloDocumento(editor) {

            let textoMinutaParaCopiar =  $("#textoMinutaParaCopiar").text()
            textoMinutaParaCopiar = textoMinutaParaCopiar.replace(/\n/g, "<br>")

            let desc = editor.getData()

            if(desc != '') {

              Swal.fire({
                  title: 'Existem informações no texto da minuta. Qual ação deseja realizar?',
                  showDenyButton: true,
                  showCancelButton: true,
                  denyButtonColor: '#f39c12',
                  confirmButtonText: `Incluir modelo de documento ao final texto`,
                  cancelButtonText: `Reabrir modelo de documento`,
                  denyButtonText: `Substituir texto`,
                  allowOutsideClick: false
              }).then((result) => {

                  if (result.isConfirmed) {
                    let textoCompleto = `${desc} <br> ${textoMinutaParaCopiar}`
							      editor.setData(textoCompleto)
                  }

                  if (result.isDenied) {
                    editor.setData(textoMinutaParaCopiar)
                  }

                  if (result.isDismissed && result.dismiss=='cancel') {
                    $("#myModalCopiarDocumento").modal('show')
                  }


              })

              } else {
                editor.insertHtml(`${textoMinutaParaCopiar}`)
              }
          }

            $("#incluirTextoMinuta").click(function(){

              let editor = CKEDITOR.instances['{{ $nome_campo }}']

              inserirModeloDocumento(editor)

            })

            function habilitarCampoNumeroDoTermo(texto = '') {

              let data =$(texto).find('option:selected').text();
              let valorHabilitaCampoNumeroDoTermo = ["Termo Aditivo", "Termo de Apostilamento", "Termo Encerramento" , "Termo de Rescisão"]
              let textoPlaceHolder  = 'Se o tipo de documento for Termo Aditivo, Termo de Apostilamento, Termo de Encerramento OU Termo de Rescisão. O campo será habilitado.'
              let habilitaCampo  = true
              let campoObrigatorio = false

              if(valorHabilitaCampoNumeroDoTermo.includes(data)){
                habilitaCampo = false
                textoPlaceHolder = 'Digitar o número do termo no formato XXXXX/AAAA'
              }

              $("#numero_termo_contrato").attr("readonly", habilitaCampo)
              $('#numero_termo_contrato').attr('placeholder',textoPlaceHolder);
              $('#numero_termo_contrato').prop('required',campoObrigatorio);
            }

            $('#numero_termo_contrato').inputmask('99999/9999')

            habilitarCampoNumeroDoTermo()

            function recuperarMinutaDocumentoPorTipoDocumento(valor) {

              let itemSelecionado = valor.value
              let url = `{{$urlRecuperarMinutaDocumentoPorTipoDocumento}}/${itemSelecionado}`

              $('#copiar_minuta_documento').prop("disabled", false);

              $.ajax({
                url: url,
                type: 'GET',
              })
              .done(function(data) {
                let obj = JSON.parse(data)
                $('#copiar_minuta_documento').empty()
                $('#copiar_minuta_documento').select2({ data: obj  })
                $('#copiar_minuta_documento').width('100%');
              })
              .fail(function(data) {

              })

              habilitarCampoNumeroDoTermo(valor)
            }

            $('#copiar_minuta_documento').on('select2:select', function (e) {
                let data = e.params.data;
                let url = `{{$urlExibirMinutaDocumentoPorTipoDocumento}}/${data.id}`

                $.ajax({
                  url: url,
                  type: 'GET',
                })
                .done(function(response) {
                  $("#modeloSelecionado").text(data.text)
                  $("#myModalCopiarDocumento").modal('show')
                  $("#textoMinutaParaCopiar").html(response)
                })
                .fail(function(data) {

                })


            });

            $(document).ready(function(){

                $("#dataAtual").html('{{ $dataAtual }}');

                //let editor = CKEDITOR.instances["{{ $nome_campo }}"];
                let nome_campo = "{{ $nome_campo }}";
                let editor = CKEDITOR.instances[nome_campo]
                let traduzir_token = '{!! $traduzir_token !!}';


                $( `#myModal` ).on( `show.bs.modal`, function ( e )
                {
                    let rowid = e.relatedTarget.dataset.id
                    $.ajax({
                      url: `{{$urlMontarTokenContrato}}`,
                      type: 'GET',
                    })
                    .done(function(data) {
                        $.each(data, function(key, value) {
                            $(`#tabelaRelacionarInformacoesContrato #${key}`).empty()
                            $(`#tabelaRelacionarInformacoesContrato #${key}`).text(value)
                        });
                    })
                    .fail(function(data) {

                    });
                })

                $( `#myModalEmpenho` ).on( `show.bs.modal`, function ( e ) {
                    $(`#ugemitente, #numero, #credor, #dataemissao, #valorempenhado, #saldoatual`).html('');
                    $.ajax({
                      url: `{{$urlMontarTokenEmpenho}}`,
                      type: 'GET',
                    })
                    .done(function(data) {
                        $("#selectEmpenhos").empty()

                        let option = `<option value="">Selecione o empenho</option>`
                        $("#selectEmpenhos").append(option)

                        $.each(data, function(key, value) {
                            option = `<option value="${value.id}">${value.codigo} - ${value.numero}</option>`
                            $("#selectEmpenhos").append(option)
                        });
                    })
                    .fail(function(data) {
                    });
                })

                $("#selectEmpenhos").change(function(){
                    let rowid = $(this).val()
                    if (!rowid) {
                        $(`#ugemitente, #numero, #credor, #dataemissao, #valorempenhado, #saldoatual`).html('');
                    } else {
                        $.ajax({
                        url: `{{$urlMontarInformacaoTabelaEmpenho}}/${rowid}`,
                        type: 'GET',
                        })
                        .done(function(data) {

                            $.each(data, function(key, value) {
                                $(`#tabelaRelacionarEmpenho #${key}`).empty()
                                $(`#tabelaRelacionarEmpenho #${key}`).text(value)
                            });

                        })
                        .fail(function(data) {
                        });
                    }
                })


                $( `#myModalResponsavelContrato` ).on( `show.bs.modal`, function ( e )
                {
                    let rowid = e.relatedTarget.dataset.id
                    $.ajax({
                      url: `{{$urlMontarTokenResponsavel}}`,
                      type: 'GET',
                    })
                    .done(function(data) {
                        $(`#tabelaRelacionarResponsavelContrato #linhaRelacionarResponsavelContrato`).empty()
                        $.each(data, function(key, value) {
                            let nomeResponsavel = value.responsavelcontrato
                            let linha = `<tr class="dadosTabelaResponsavel" data-responsavel="${nomeResponsavel}" >
                                                <td>Responsável</td>
                                                <td>${nomeResponsavel}</td>
                                        </tr>`
                            $(`#tabelaRelacionarResponsavelContrato #linhaRelacionarResponsavelContrato`).append(linha)
                        });

                        inserirTextoResponsavel('data-responsavel')

                    })
                    .fail(function(data) {

                    });
                })

                $( `#myModalItensContrato` ).on( `show.bs.modal`, function ( e )
                {
                    $(`#numero_item_compra, #itemGrupo, #item, #descricaoComplementar, #quantidade, #datainicio, #valorunitario, #valortotal`).html('');
                    $("#selectItemContrato").val('').trigger('change');

                    $('#selectItemContrato').select2({
                        ajax: {
                            url: `{{$urlMontarTokenItemContrato}}`,
                            dataType: 'json',
                            delay: 250,
                            processResults: function (data) {
                                return {
                                    results:  $.map(data, function (item) {
                                        return {
                                            text: item.numero_item_compra +' - '+ item.item,
                                            id: item.id
                                        }
                                    })
                                };
                            },
                            cache: true
                        }
                    });
                    $('.selection .select2-selection').css("height","34px").css('border-color','#d2d6de');
                })

                $("#selectItemContrato").change(function(){
                    let rowid = $(this).val()
                    if (!rowid) {
                        $(`#numero_item_compra, #itemGrupo, #item, #descricaoComplementar, #quantidade, #datainicio, #valorunitario, #valortotal`).html('');
                    } else {
                        $.ajax({
                        url: `{{$urlMontarInformacaoTabelaItemContrato}}/${rowid}`,
                        type: 'GET',
                        })
                        .done(function(data) {

                            $.each(data, function(key, value) {
                                $(`#tabelaRelacionarItemContrato #${key}`).empty()
                                $(`#tabelaRelacionarItemContrato #${key}`).text(value)
                            });
                        })
                        .fail(function(data) {
                        });
                    }
                })



                function inserirTextoResponsavel(nomeAtributo) {
                    $(".dadosTabelaResponsavel").click(function(){
                            let textoInsercao = $(this).attr(nomeAtributo);
                            inserirCampoSelecionadoNoTexto(textoInsercao, textoInsercao)
                        })
                }

                $('.dadosTabela').click(function(e) {
                    e.preventDefault();

                    let campo = $(this).attr('data-campo');

                    //Limpa o # para pegar o id da coluna limpo
                    let id = campo.replace(/#/g, '');

                    clicarLinhaTabela('tabelaRelacionarInformacoesContrato', id, campo )
                    // Recupera o texto incluído na coluna
                    // let textoColuna = $(`#tabelaRelacionarInformacoesContrato #${id}`).text()

                    // inserirCampoSelecionadoNoTexto(textoColuna, campo)

                });


                $('.botaoTodosDadosResponsavel').click(function(e) {
                    e.preventDefault();
                    var texto = '';
                    $('.dadosTabelaResponsavel').each(function(valeu){
                        if ($(this).data('responsavel')) {
                            texto += 'Responsável: '+$(this).data('responsavel')+'<br>';
                        }
                    });
                    if (texto)
                        editor.insertHtml(`<br> ${texto} `);
                    $('.botaoFecharModal').trigger('click');
                });


                $('.botaoTodosDados').click(function(e) {
                    e.preventDefault();

                    $('.dadosTabela').each(function() {
                        var texto = '';

                        var dataCampo = $(this).attr('data-campo');

                        if (dataCampo) {
                            id = dataCampo.replace(/#/g, '');

                            if ($("#"+id).html())
                                texto += $("#"+id+'Label').html()+': '+$("#"+id).html();
                        }

                        if (texto)
                            editor.insertHtml(`<br> ${texto} `);

                        $('.botaoFecharModal').trigger('click');
                    });
                });


                $('.botaoTodosDadosItens').click(function(e) {
                    e.preventDefault();

                    let table = '<table border="1"><thead><tr>';
                    let rows = '<tr>';
                    let headersAdded = false;

                    $('.dadosTabelaItem').each(function() {
                        let dataCampo = $(this).attr('data-campo');
                        let id = dataCampo ? dataCampo.replace(/#/g, '') : '';

                        if (id) {
                            if (!headersAdded) {
                                table += '<th>' + $("#"+id+'Label').html() + '</th>';
                            }
                            rows += '<td>' + $("#"+id).html() + '</td>';
                        }
                    });

                    if (!headersAdded) {
                        headersAdded = true;
                        table += '</tr></thead><tbody>';
                    }

                    rows += '</tr>';
                    table += rows + '</tbody></table>';

                    if (headersAdded)
                        editor.insertHtml(`<br> ${table} `);

                    $('.botaoFecharModal').trigger('click');
                });

                $('.botaoTodosDadosTodosItens').click(function(e) {
                    e.preventDefault();
                    $.ajax({
                        url: `{{$urlMontarTokenItemContrato}}`,
                        type: 'GET',
                    })
                        .done(function(data) {
                            let table = '<table border="1"><thead><tr>';
                            let rows = '';

                            // Assumindo que todos os objetos em 'data' têm as mesmas chaves
                            let headers = Object.keys(data[0]);
                            headers.forEach(function(header) {
                                if(header != 'id') {
                                    table += '<th>' + $("#"+header+'Label').html() + '</th>';
                                }
                            });
                            table += '</tr></thead><tbody>';

                            $.each(data, function(key, value) {
                                rows += '<tr>';
                                $.each(value, function(k, v) {
                                    if(k != 'id'){
                                        rows += '<td>' + v + '</td>';
                                    }
                                });
                                rows += '</tr>';
                            });

                            table += rows + '</tbody></table>';

                            if (rows)
                                editor.insertHtml(`<br> ${table} `);
                            $('.botaoFecharModal').trigger('click');
                        })
                        .fail(function(data) {
                        });
                });


                $('.dadosTabelaItem').click(function(e) {
                    e.preventDefault();

                    let campo = $(this).attr('data-campo');

                    //Limpa o # para pegar o id da coluna limpo
                    let id = campo.replace(/#/g, '');

                    clicarLinhaTabela('tabelaRelacionarItemContrato', id, campo )
                    // Recupera o texto incluído na coluna
                    // let textoColuna = $(`#tabelaRelacionarInformacoesContrato #${id}`).text()

                    // inserirCampoSelecionadoNoTexto(textoColuna, campo)

                });


                $('.dadosTabelaEmpenho').click(function(e) {
                    e.preventDefault();

                    let campo = $(this).attr('data-campo');

                    //Limpa o # para pegar o id da coluna limpo
                    let id = campo.replace(/#/g, '');

                    clicarLinhaTabela('tabelaRelacionarEmpenho', id, campo )
                });

                $('.botaoTodosDadosEmpenho').click(function(e) {
                    e.preventDefault();

                    $('.dadosTabelaEmpenho').each(function() {
                        var texto = '';

                        var dataCampo = $(this).attr('data-campo');

                        if (dataCampo) {
                            id = dataCampo.replace(/#/g, '');

                            if ($("#"+id).html())
                                texto += $("#"+id+'Label').html()+': '+$("#"+id).html();
                        }

                        if (texto)
                            editor.insertHtml(`<br> ${texto} `);

                        $('.botaoFecharModal').trigger('click');
                    });
                });

                $('.botaoTodosDadosTodosEmpenhos').click(function(e) {
                    e.preventDefault();

                    $('#selectEmpenhos').find('option').each(function() {
                        if ($(this).val()) {
                            $.ajax({
                                url: `{{$urlMontarInformacaoTabelaEmpenho}}/${$(this).val()}`,
                                type: 'GET',
                            })
                            .done(function(data) {
                                var texto = '';

                                $.each(data, function(key, value) {
                                    if ($("#"+key+'Label').html())
                                        texto += $("#"+key+'Label').html()+': '+value+'<br>';
                                });

                                if (texto)
                                    editor.insertHtml(`<br> ${texto} `);

                                $('.botaoFecharModal').trigger('click');
                            })
                            .fail(function(data) {
                            });
                        }
                    });
                });

                function clicarLinhaTabela(nomeTabela, id, campo ) {
                    let textoColuna = $(`#${nomeTabela} #${id}`).text()

                    inserirCampoSelecionadoNoTexto(textoColuna, campo)
                }

                function inserirCampoSelecionadoNoTexto(textoColuna, campo) {
                    if(traduzir_token == 'sim'){
                        let token_values = {!! $tokens !!};
                        // editor.insertHtml(token_values['' + campo + '']);
                        editor.insertHtml(`<br> ${textoColuna}`);
                    }else{
                        editor.insertHtml(`<br> ${campo}`);
                    }

                    $('.botaoFecharModal').trigger('click');
                }

            });

            function formatState (state) {
                if (!state.id) { return state.text; }
                var $state = $(
                    '<span style="white-space: normal;">' + state.text + '</span>'
                );
                return $state;
            }
        </script>
    @endpush
@endif

@push('after_styles')
<style>
.table-hover>tbody>tr:hover {
    background-color: #eee;
}
.tokens th{
  background-color:#e8e8e8 !important
}

tr{
    cursor: pointer;
}
</style>
@endpush

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content" style='height: 600px; overflow-y: auto'>
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Campos Relacionáveis</h4>
        </div>
        <div class="modal-body">
          <div class='input-group' style='width:100%;height: 150px;overflow-y: scroll;'>
            <table class="table table-hover table-bordered" id="tabelaRelacionarInformacoesContrato">
              <colgroup>
              <col style="width: 50%">
              <col style="width: 50%">
              </colgroup>
              <thead>
                <tr>
                  <th colspan="2">Informações do Contrato</th>
                </tr>
              </thead>
              <tbody>
                <tr class='dadosTabela' data-campo="##numeroInstrumentoContrato##" >
                  <td id="numeroInstrumentoContratoLabel">Número do Instrumento</td>
                  <td id="numeroInstrumentoContrato"></td>
                </tr>
                <tr class='dadosTabela' data-campo="##unidadeGestoraOrigemContrato##" >
                  <td id="unidadeGestoraOrigemContratoLabel">Unidade Gestora Origem do Contrato</td>
                  <td id="unidadeGestoraOrigemContrato"></td>
                </tr>
                <tr class='dadosTabela' data-campo="##unidadeGestoraAtualContrato##" >
                  <td id="unidadeGestoraAtualContratoLabel">Unidade Gestora Atual</td>
                  <td id="unidadeGestoraAtualContrato"></td>
                </tr>
                <tr class='dadosTabela' data-campo="##unidadesRequisitantesContrato##" >
                  <td id="unidadesRequisitantesContratoLabel">Unidades Requisitantes</td>
                  <td id="unidadesRequisitantesContrato"></td>
                </tr>
                <tr class='dadosTabela' data-campo="##tipoContrato##" >
                  <td id="tipoContratoLabel">Tipo</td>
                  <td id="tipoContrato"></td>
                </tr>
                <tr class='dadosTabela' data-campo="##categoriaContrato##" >
                  <td id="categoriaContratoLabel">Categoria</td>
                  <td id="categoriaContrato"></td>
                </tr>
                <tr class='dadosTabela' data-campo="##processoContrato##" >
                  <td id="processoContratoLabel">Processo</td>
                  <td id="processoContrato"></td>
                </tr>
                <tr class='dadosTabela' data-campo="##objetoContrato##" >
                  <td id="objetoContratoLabel">Objeto</td>
                  <td id="objetoContrato"></td>
                </tr>
                <tr class='dadosTabela' data-campo="##inicioVigenciaContrato##" >
                  <td id="inicioVigenciaContratoLabel">Início Vigência</td>
                  <td id="inicioVigenciaContrato"></td>
                </tr>
                <tr class='dadosTabela' data-campo="##fimVigenciaContrato##" >
                  <td id="fimVigenciaContratoLabel">Fim Vigência</td>
                  <td id="fimVigenciaContrato"></td>
                </tr>
                <tr class='dadosTabela' data-campo="##valorGlobalContrato##" >
                  <td id="valorGlobalContratoLabel">Valor Global</td>
                  <td id="valorGlobalContrato"></td>
                </tr>
                <tr class='dadosTabela' data-campo="##numeroParcelasContrato##" >
                  <td id="numeroParcelasContratoLabel">Número Parcelas</td>
                  <td id="numeroParcelasContrato"></td>
                </tr>
                <tr class='dadosTabela' data-campo="##valorParcelasContrato##" >
                  <td id="valorParcelasContratoLabel">Valor Parcelas</td>
                  <td id="valorParcelasContrato"></td>
                </tr>
                <tr class='dadosTabela' data-campo="##prorrogavelContrato##" >
                  <td id="prorrogavelContratoLabel">Prorrogável</td>
                  <td id="prorrogavelContrato"></td>
                </tr>
                <tr class='dadosTabela' data-campo="##fornecedorContrato##" >
                  <td id="fornecedorContratoLabel">Fornecedor</td>
                  <td id="fornecedorContrato"></td>
                </tr>
                <tr class='dadosTabela' data-campo="##fornecedor##" >
                  <th colspan="2">Fornecedor</th>
                </tr>
                <tr class='dadosTabela' data-campo="##cnpj##" >
                  <td id="cnpjLabel">CNPJ</td>
                  <td id="cnpj"></td>
                </tr>
                <tr class='dadosTabela' data-campo="##razaoSocial##" >
                  <td id="razaoSocialLabel">Razão Social</td>
                  <td id="razaoSocial"></td>
                </tr>
                <tr class='dadosTabela'>
                  <th colspan="2">Dados da Compra</th>
                </tr>
                <tr class='dadosTabela' data-campo="##numeroCompraContrato##" >
                  <td id="numeroCompraContratoLabel">Número da Compra - Contrato</td>
                  <td id="numeroCompraContrato"></td>
                </tr>
                <tr class='dadosTabela' data-campo="##uasgCompraContrato##" >
                  <td id="uasgCompraContratoLabel">UASG da Compra - Contrato</td>
                  <td id="uasgCompraContrato"></td>
                </tr>
                <tr >
                  <th colspan="2">Amparo Legal</th>
                </tr>
                <tr class='dadosTabela' data-campo="##atoNormativo##" >
                  <td id="atoNormativoLabel">Ato Normativo</td>
                  <td id="atoNormativo"></td>
                </tr>
                <tr class='dadosTabela' data-campo="##artigo##" >
                  <td id="artigoLabel">Artigo</td>
                  <td id="artigo"></td>
                </tr>
                <tr class='dadosTabela' data-campo="##paragrafo##" >
                  <td id="paragrafoLabel">Parágrafo</td>
                  <td id="paragrafo"></td>
                </tr>
                <tr class='dadosTabela' data-campo="##inciso##" >
                  <td id="incisoLabel">Inciso</td>
                  <td id="inciso"></td>
                </tr>
                <tr class='dadosTabela' data-campo="##alinea##" >
                  <td id="alineaLabel">Alínea</td>
                  <td id="alinea"></td>
                </tr>
              </tbody>
              </table>
              <p class="text-danger">* Os campos monetários serão acrescidos de R$ + extenso</p>
              <input type="hidden" id="idItemMenu">
          </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default botaoTodosDados">Todos os dados</button>
            <button type="button" class="btn btn-default botaoFecharModal" data-dismiss="modal" >Fechar</button>
        </div>
      </div>

    </div>
  </div>


  <div class="modal fade" id="myModalEmpenho" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content" style='height: 600px; overflow-y: auto'>
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Campos Relacionáveis</h4>
        </div>
        <div class="modal-body">
          <div class='input-group' style='width:100%;height: 150px;overflow-y: scroll;'>
            <table class="table table-hover table-bordered" id="tabelaRelacionarEmpenho">
              <colgroup>
              <col style="width: 50%">
              <col style="width: 50%">
              </colgroup>
              <thead>
                <tr>
                  <th >Informações dos Empenhos</th>
                  <th >
                        <select id="selectEmpenhos">
                            <option value="">Selecione o empenho</option>
                        </select>
                  </th>
                </tr>
              </thead>
              <tbody>

                <tr class='dadosTabelaEmpenho'>
                  <th colspan="2">Empenho</th>
                </tr>
                <tr class='dadosTabelaEmpenho' data-campo="##ugemitente##" >
                  <td id="ugemitenteLabel">UG emitente</td>
                  <td id="ugemitente"></td>
                </tr>
                <tr class='dadosTabelaEmpenho' data-campo="##numero##" >
                  <td id="numeroLabel">Número do empenho</td>
                  <td id="numero"></td>
                </tr>
                <tr class='dadosTabelaEmpenho' data-campo="##credor##" >
                  <td id="credorLabel">Credor</td>
                  <td id="credor"></td>
                </tr>
                <tr class='dadosTabelaEmpenho' data-campo="##dataemissao##" >
                  <td id="dataemissaoLabel">Data da emissão</td>
                  <td id="dataemissao"></td>
                </tr>
                <tr class='dadosTabelaEmpenho' data-campo="##valorempenhado##" >
                  <td id="valorempenhadoLabel">Valor empenhado</td>
                  <td id="valorempenhado"></td>
                </tr>
                <tr class='dadosTabelaEmpenho' data-campo="##saldoatual##" >
                  <td id="saldoatualLabel">Saldo em <span id="dataAtual"></span></td>
                  <td id="saldoatual"></td>
                </tr>
              </tbody>
            </table>
            <p class="text-danger">* Os campos monetários serão acrescidos de R$ + extenso</p>
          </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default botaoTodosDadosEmpenho">Todos os dados</button>
            <button type="button" class="btn btn-default botaoTodosDadosTodosEmpenhos">Todos os dados de todos os empenhos</button>
            <button type="button" class="btn btn-default botaoFecharModal" data-dismiss="modal">Fechar</button>
        </div>
      </div>

    </div>
</div>

  <div class="modal fade" id="myModalItensContrato" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content" style='height: 600px; overflow-y: auto'>
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Campos Relacionáveis</h4>
        </div>
        <div class="modal-body">
          <div class='input-group' style='width:100%;height: 150px;overflow-y: scroll;'>
            <table class="table table-hover table-bordered" id="tabelaRelacionarItemContrato">
              <colgroup>
              <col style="width: 50%">
              <col style="width: 50%">
              </colgroup>
              <thead>
                <tr>
                  <th>Selecione o item do contrato</th>
                  <th>
                    <select style="width: 300px;" id="selectItemContrato">
                        <option value="">Selecione o item</option>
                    </select>
                  </th>
                </tr>
              </thead>
              <tbody>

                <tr class='dadosTabelaItem'>
                  <th colspan="2">Selecione</th>
                </tr>
                <tr class='dadosTabelaItem' data-campo="##numero_item_compra##" >
                    <td id="numero_item_compraLabel">Número do item</td>
                    <td id="numero_item_compra"></td>
                  </tr>
                <tr class='dadosTabelaItem' data-campo="##itemGrupo##" >
                  <td id="itemGrupoLabel">Grupo do item</td>
                  <td id="itemGrupo"></td>
                </tr>
                <tr class='dadosTabelaItem' data-campo="##item##" >
                  <td id="itemLabel">Descrição do item</td>
                  <td id="item"></td>
                </tr>
                <tr class='dadosTabelaItem' data-campo="##descricaoComplementar##" >
                  <td id="descricaoComplementarLabel">Descrição Complementar</td>
                  <td id="descricaoComplementar"></td>
                </tr>
                <tr class='dadosTabelaItem' data-campo="##codigo_siasg##" >
                    <td id="codigo_siasgLabel">CATMAT/CATSER</td>
                    <td id="codigo_siasg"></td>
                </tr>
                <tr class='dadosTabelaItem' data-campo="##quantidade##" >
                  <td id="quantidadeLabel">Quantidade</td>
                  <td id="quantidade"></td>
                </tr>
                <tr class='dadosTabelaItem' data-campo="##datainicio##" >
                  <td id="datainicioLabel">Data Início</td>
                  <td id="datainicio"></td>
                </tr>
                <tr class='dadosTabelaItem' data-campo="##valorunitario##" >
                  <td id="valorunitarioLabel">Valor Unitário</td>
                  <td id="valorunitario"></td>
                </tr>
                <tr class='dadosTabelaItem' data-campo="##valortotal##" >
                  <td id="valortotalLabel">Valor Total</td>
                  <td id="valortotal"></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default botaoTodosDadosItens">Todos os dados</button>
            <button type="button" class="btn btn-default botaoTodosDadosTodosItens">Todos os dados de todos os itens</button>
          <button type="button" class="btn btn-default botaoFecharModal" data-dismiss="modal">Fechar</button>
        </div>
      </div>

    </div>
  </div>


  <!-- Modal -->
<div class="modal fade" id="myModalResponsavelContrato" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content" style='height: 600px; overflow-y: auto'>
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Campos Relacionáveis</h4>
        </div>
        <div class="modal-body">
          <div class='input-group' style='width:100%;height: 150px;overflow-y: scroll;'>
            <table class="table table-hover table-bordered" id="tabelaRelacionarResponsavelContrato">
              <colgroup>
              <col style="width: 50%">
              <col style="width: 50%">
              </colgroup>
              <thead>
                <tr>
                  <th colspan="2">Informações do Responsável pelo contrato</th>
                </tr>
              </thead>
              <tbody id="linhaRelacionarResponsavelContrato">
                <tr id="responsavelContrato">
                  <th colspan="2">Responsáveis pelo Contrato</th>
                </tr>
              </tbody>
              </table>

          </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default botaoTodosDadosResponsavel">Todos os dados</button>
          <button type="button" class="btn btn-default botaoFecharModal" data-dismiss="modal">Fechar</button>
        </div>
      </div>

    </div>
  </div>


    <!-- Modal -->
<div class="modal fade" id="myModalCopiarDocumento" role="dialog"  data-backdrop="static">
    <div class="modal-dialog modal-lg">

      <!-- Modal content-->
      <div class="modal-content" >
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Copiar Modelo do Documento - <span id="modeloSelecionado"></span> </h4>
        </div>
        <div class="modal-body">
          <label>Texto da minuta</label>
          <span id="textoMinutaParaCopiar"></span>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal" id="incluirTextoMinuta">Incluir texto</button>
          <button type="button" class="btn btn-default botaoFecharModal" data-dismiss="modal">Fechar</button>
        </div>
      </div>

    </div>
  </div>
