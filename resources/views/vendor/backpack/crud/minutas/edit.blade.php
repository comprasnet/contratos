@extends('backpack::layout')

@section('header')
	<section class="content-header">
	  <h1>
        <span class="text-capitalize">{!! $crud->getHeading() ?? $crud->entity_name_plural !!}</span>
        <small>{!! $crud->getSubheading() ?? trans('backpack::crud.edit').' '.$crud->entity_name !!}.</small>
	  </h1>
	  <ol class="breadcrumb">
	    <li><a href="{{ url(config('backpack.base.route_prefix'),'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
	    <li><a href="{{ url($crud->route) }}" class="text-capitalize">{{ $crud->entity_name_plural }}</a></li>
	    <li class="active">{{ trans('backpack::crud.edit') }}</li>
	  </ol>
	</section>
@endsection

@section('content')
@include('crud::recomposicao.modelodocumento.relaciona_dados_contrato_modelo_documento',['ckeditor_field_name' => 'ckeditor-texto_minuta'])
@if ($crud->hasAccess('list'))
    @if(Route::current()->getName() == 'crud.feriado.edit')
        <a href="{{ $crud->route }}" class="hidden-print"><i class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span>{{ $crud->entity_name_plural }}</span></a>
    @elseif(Route::current()->getName() == 'crud.amparolegal.edit')
        <a href="{{ $crud->route }}" class="hidden-print"><i class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span>{{ $crud->entity_name_plural }}</span></a>
    @else
    	<a href="{{ starts_with(URL::previous(), url($crud->route)) ? URL::previous() : url($crud->route) }}" class="hidden-print"><i class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span>{{ $crud->entity_name_plural }}</span></a>
    @endif
@endif

<div class="row m-t-20">
	<div class="{{ $crud->getEditContentClass() }}">
		<!-- Default box -->

		@include('crud::inc.grouped_errors')

		  <form method="post"
		  		action="{{ url($crud->route.'/'.$entry->getKey()) }}"
				@if ($crud->hasUploadFields('update', $entry->getKey()))
				enctype="multipart/form-data"
				@endif
		  		>
		  {!! csrf_field() !!}
		  {!! method_field('PUT') !!}
		  <div class="col-md-12">
		  	@if ($crud->model->translationEnabled())
		    <div class="row m-b-10">
		    	<!-- Single button -->
				<div class="btn-group pull-right">
				  <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    {{trans('backpack::crud.language')}}: {{ $crud->model->getAvailableLocales()[$crud->request->input('locale')?$crud->request->input('locale'):App::getLocale()] }} &nbsp; <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu">
				  	@foreach ($crud->model->getAvailableLocales() as $key => $locale)
					  	<li><a href="{{ url($crud->route.'/'.$entry->getKey().'/edit') }}?locale={{ $key }}">{{ $locale }}</a></li>
				  	@endforeach
				  </ul>
				</div>
		    </div>
		    @endif
		    <div class="row display-flex-wrap">
		      <!-- load the view from the application if it exists, otherwise load the one in the package -->
		      @if(view()->exists('vendor.backpack.crud.form_content'))
		      	@include('vendor.backpack.crud.form_content', ['fields' => $fields, 'action' => 'edit'])
		      @else
		      	@include('crud::form_content', ['fields' => $fields, 'action' => 'edit'])
		      @endif
		    </div><!-- /.box-body -->

            <div class="">
                @if(Route::current()->getName() == 'crud.instrumentoinicial.edit')
                    @include('crud::inc.form_aba_buttons_instrumentoinicial')
                @elseif(Route::current()->getName() == 'crud.aditivos.edit')
                    @include('crud::inc.form_aba_buttons_aditivo')
                @elseif(Route::current()->getName() == 'crud.apostilamentos.edit')
                    @include('crud::inc.form_aba_buttons_apostilamentos')
                @else
                    @include('crud::inc.form_save_buttons')
                @endif
		    </div><!-- /.box-footer-->
		  </div><!-- /.box -->
		  </form>
	</div>
</div>
{{-- MODELO DOCUMENTOS - RELACIONA DADOS CONTRATO --}}

@endsection
@push('after_scripts')
    <script type="text/javascript">
        $(document).ready(function(){
			let tokens = JSON.parse('{!! $tokens !!}');
			let editor = CKEDITOR.instances["ckeditor-texto_minuta"];

			$('#select2_ajax_tipo_modelo_documento_id').on('change', function () {
				var padrao_documento_id = $('#select2_ajax_tipo_modelo_documento_id option:selected').val();

				if (typeof padrao_documento_id !== "undefined") {
					alternaModelo(padrao_documento_id);
				}
			});

			//FUNCTION ALTERNA MODELO
			function alternaModelo(modelo_id){

				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
					}
				});
				jQuery.ajax({
					url: "{{route('modeloDocumento.getTemplate')}}",
					method: 'post',
					data: {
							modelo_id: modelo_id
					},
					dataType:"json",
					success: function(result){
						var texto_minuta = result.template;
						$.each(tokens, function(token,valor){
							texto_minuta = texto_minuta.replaceAll(token,valor);
						});
						editor.insertHtml(texto_minuta);
					}
				});
			}
		});
	</script>
@endpush
