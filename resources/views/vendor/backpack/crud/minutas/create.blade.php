@extends('backpack::layout')

@section('header')
	<section class="content-header">
	  <h1>
        <span class="text-capitalize">{!! $crud->getHeading() ?? $crud->entity_name_plural !!}</span>
        <small>{!! $crud->getSubheading() ?? trans('backpack::crud.add').' '.$crud->entity_name !!}.</small>
	  </h1>
	  <ol class="breadcrumb">
	    <li><a href="{{ url(config('backpack.base.route_prefix'), 'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
	    <li><a href="{{ url($crud->route) }}" class="text-capitalize">{{ $crud->entity_name_plural }}</a></li>
	    <li class="active">{{ trans('backpack::crud.add') }}</li>
	  </ol>
	</section>
@endsection

@section('content')

@include('crud::recomposicao.modelodocumento.relaciona_dados_contrato_modelo_documento',['ckeditor_field_name'=>'ckeditor-texto_minuta'])

@if ($crud->hasAccess('list'))
    <a href="{{ url($crud->route) }}" class="hidden-print"><i class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span>{{ $crud->entity_name_plural }}</span></a><br><br>
@endif

<div class="row m-t-20">
	<div class="{{ $crud->getCreateContentClass() }}">
		<!-- Default box -->

		@include('crud::inc.grouped_errors')

		  <form method="post"
		  		action="{{ url($crud->route) }}"
				@if ($crud->hasUploadFields('create'))
				enctype="multipart/form-data"
				@endif
		  		>
		  {!! csrf_field() !!}
		  <div class="col-md-12">

		    <div class="row display-flex-wrap">
		      <!-- load the view from the application if it exists, otherwise load the one in the package -->
		      @if(view()->exists('vendor.backpack.crud.form_content'))
		      	@include('vendor.backpack.crud.form_content', [ 'fields' => $crud->getFields('create'), 'action' => 'create' ])
		      @else
		      	@include('crud::form_content', [ 'fields' => $crud->getFields('create'), 'action' => 'create' ])
		      @endif
		    </div><!-- /.box-body -->
		    <div class="">
            @if(Route::current()->getName() == 'crud.contrato.create')
                @include('crud::inc.form_aba_buttons')
            @elseif(Route::current()->getName() == 'crud.aditivos.create')
                    @include('crud::inc.form_aba_buttons_aditivo')
            @elseif(Route::current()->getName() == 'crud.apostilamentos.create')
                    @include('crud::inc.form_aba_buttons_apostilamentos')
            @elseif(Route::current()->getName() == 'crud.rescisao.create')
                    @include('crud::inc.form_aba_buttons_rescisao')
            @elseif(Route::current()->getName() == 'crud.retiradacontratoconta.create')
                @include('crud::inc.form_retiradacontratoconta_buttons')
            @elseif(Route::current()->getName() == 'crud.repactuacaocontratoconta.create')
                @include('crud::inc.form_repactuacaocontratoconta_buttons')
            @elseif(Route::current()->getName() == 'crud.depositocontratoconta.create')
                @include('crud::inc.form_depositocontratoconta_buttons')
            @elseif(Route::current()->getName() == 'crud.minutas.create')
                @if( session()->get('vemDoEncerramento') == 1 )
                    @include('crud::inc.form_minutasvemdoencerramento_buttons')
                @else
                    @include('crud::inc.form_save_buttons')
                @endif

            @else
                @include('crud::inc.form_save_buttons')
            @endif
            </div><!-- /.box-footer-->

		  </div><!-- /.box -->
		  </form>
	</div>
</div>

@endsection
@push('after_scripts')

    <script type="text/javascript">
        $(document).ready(function(){
			let tokens = JSON.parse('{!! $tokens !!}');
			let editor = CKEDITOR.instances["ckeditor-texto_minuta"];

			$('#select2_ajax_tipo_modelo_documento_id').on('change', function () {
				var padrao_documento_id = $('#select2_ajax_tipo_modelo_documento_id option:selected').val();

				if (typeof padrao_documento_id !== "undefined") {
					alternaModelo(padrao_documento_id);
				}
			});

			//FUNCTION ALTERNA MODELO
			function alternaModelo(modelo_id){

				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
					}
				});
				jQuery.ajax({
					url: "{{route('modeloDocumento.getTemplate')}}",
					method: 'post',
					data: {
							modelo_id: modelo_id
					},
					dataType:"json",
					success: function(result){
						var texto_minuta = result.template;
						$.each(tokens, function(token,valor){
							texto_minuta = texto_minuta.replaceAll(token,valor);
						});
						// editor.insertHtml(texto_minuta);
						inserirModeloDocumento(editor, texto_minuta)
					}
				});
			}

			function inserirModeloDocumento(editor, texto_minuta) {

				let desc = editor.getData()

				if(desc != '') {

					Swal.fire({
						title: 'Existem informações no texto da minuta, qual ação deseja realizar?',
						showDenyButton: true,
						showCancelButton: false,
						confirmButtonText: `Incluir modelo de documento ao final texto`,
						denyButtonText: `Substituir texto`,
						allowOutsideClick: false
					}).then((result) => {

						if (result.isConfirmed) {
							let textoCompleto = `${desc} <br> ${texto_minuta}`
							editor.setData(textoCompleto)
						}

						if (result.isDenied) {
							editor.setData(texto_minuta)
						}

					})

				} else {
					editor.insertHtml(`${texto_minuta}`)
				}
          	}
		});

	</script>
@endpush
