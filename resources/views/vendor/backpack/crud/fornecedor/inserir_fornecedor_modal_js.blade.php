$('#btn_inserir').on('click', function (event) {
        event.preventDefault();
        if (valida_form()) {
            inserirFornecedor(event);
            $('#fechar_modal').trigger('click');
            $('#form_modal').remove();
        }
        event.stopPropagation();
    });

    function valida_form(event) {
        const campos = [
            {seletor: "#tipo_fornecedor", mensagem: 'Tipo Fornecedor'},
            {seletor: "#cpf_cnpj_idgener", mensagem: 'CPF/CNPJ/UG/ID Genérico'},
            {seletor: "#nome", mensagem: 'Nome'}
        ];

        for (let campo of campos) {
            if (null_or_empty(campo.seletor)) {
                Swal.fire('Alerta!', `O campo ${campo.mensagem} é obrigatório!`, 'warning');
                return false;
            }
        }
        return true;
    }

    function null_or_empty(str) {
        let valor = $(str).val();
        return valor == null || valor.trim() === "";
    }

    function inserirFornecedor(event) {
        let fornecedor = $('#tipo_fornecedor :selected').val();
        let cpf_cnpj_idgener = $('#cpf_cnpj_idgener').val();
        let nome = $('#nome').val();

        let url = "{{route('empenho.minuta.inserir.fornecedor')}}";

        params = {
            fornecedor: fornecedor,
            cpf_cnpj_idgener: cpf_cnpj_idgener,
            nome: nome
        }

        axios.post(url, params)
            .then(response => {
                dados = response.data
                $('#select2_ajax_{{ $field['name'] }}').html(
                    `<option value="${dados.id}" selected="selected">
    ${dados.cpf_cnpj_idgener} - ${dados.nome}
</option>`
                );

                $(".empenhos-select2").prop('disabled', false);
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Credor incluído com sucesso!',
                    showConfirmButton: false,
                    timer: 2000
                });
            })
            .catch(error => {
                alert(error);
            })
            .finally()
        event.preventDefault()
    }

    $(window).on('load', function () {
        aplicarMascara($('#tipo_fornecedor').val());
    });

    $(document).on('change', '#tipo_fornecedor', function () {
        aplicarMascara($(this).val());
    });

    function aplicarMascara(tipo) {
        let seletor = '#cpf_cnpj_idgener';
        switch (tipo) {
            case 'JURIDICA':
                mascaraCNPJ(seletor);
                break;
            case 'FISICA':
                mascaraCPF(seletor);
                break;
            case 'UG':
                mascaraUG(seletor);
                break;
            case 'IDGENERICO':
                mascaraIDGener(seletor);
                break;
        }
    }
