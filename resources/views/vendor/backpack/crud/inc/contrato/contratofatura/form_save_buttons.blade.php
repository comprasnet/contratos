<div id="saveActions" class="form-group">

    <input type="hidden" name="save_action" value="{{ $saveAction['active']['value'] }}">

    <div class="btn-group" id="botoes_fatura">

        <button type="submit" class="btn btn-success" id="#save_button">
            <span class="fa fa-save" role="presentation" aria-hidden="true"></span> &nbsp;
            <span data-value="{{ $saveAction['active']['value'] }}">{{ $saveAction['active']['label'] }}</span>
        </button>

        <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aira-expanded="false">
            <span class="caret"></span>
            <span class="sr-only">&#x25BC;</span>
        </button>

        <ul class="dropdown-menu">
            @foreach( $saveAction['options'] as $value => $label)
            <li><a href="javascript:void(0);" data-value="{{ $value }}">{{ $label }}</a></li>
            @endforeach
        </ul>

    </div>

    <a href="" class="btn btn-info" id="prev_aba"><span class="fa fa-arrow-circle-left"></span> &nbsp;Aba Anterior</a>
    <a href="" class="btn btn-info" id="next_aba">Próxima Aba <span class="fa fa-arrow-circle-right"></span></a>

    <a href="{{ $crud->hasAccess('list') ? url($crud->route) : url()->previous() }}" class="btn btn-default"><span class="fa fa-ban"></span> &nbsp;{{ trans('backpack::crud.cancel') }}</a>
</div>

@push('after_scripts')
    <script type="text/javascript">

        $(document).ready(function () {
            $('#botoes_fatura').hide();
            $('#cancelar').hide();
            $('#prev_aba').hide();

            habilitaDesabilitaBotoes();

            $('body').on('click','#prev_aba', function(event){
                abaAnterior(event);
            });

            $('body').on('click','#next_aba', function(event){
                proximaAba(event);
            });

            $('body').on('click','#dadosinstrumentosdecobranca', function(event){
                $('#botoes_fatura').hide();
                $('#cancelar').hide();
                $('#prev_aba').hide();
                $('#next_aba').show();
            });

            $('body').on('click','#itensfaturados', function(event){
                $('#botoes_fatura').hide();
                $('#cancelar').hide();
                $('#prev_aba').show();
                $('#next_aba').show();
            });

            $('body').on('click','#outrasinformacoes', function(event){
                $('#botoes_fatura').show();
                $('#cancelar').show();
                $('#prev_aba').show();
                $('#next_aba').hide();
            });

            function verificaAbaAtiva() {
                var divTabs = $('#form_tabs');
                var ul = divTabs.find('ul');
                var a = ul.find('li').find('a');
                let nomeAba;
                let li;

                a.each(function () {
                    aba = $(this).parent();
                    if (aba.attr('class') == 'active') {
                        nomeAba = aba.find('a');
                    }
                });
                return nomeAba;
            }

            function habilitaDesabilitaBotoes(){

                nomeAba = verificaAbaAtiva();

                switch (nomeAba.attr('id')) {
                    case 'dadosinstrumentosdecobranca':
                        $('#botoes_fatura').hide();
                        $('#cancelar').hide();
                        $('#prev_aba').hide();
                        $('#next_aba').show();
                        break;
                    case 'itensfaturados':
                        $('#botoes_fatura').hide();
                        $('#cancelar').hide();
                        $('#prev_aba').show();
                        $('#next_aba').show();
                        break;
                    case 'outrasinformacoes':
                        $('#botoes_fatura').show();
                        $('#cancelar').show();
                        $('#prev_aba').show();
                        $('#next_aba').hide();
                        break;
                }
            }

            function proximaAba(event){
                event.preventDefault();
                nomeAba = verificaAbaAtiva();

                switch (nomeAba.attr('id')) {
                    case 'dadosinstrumentosdecobranca':
                        $('#itensfaturados').click();
                        break;
                    case 'itensfaturados':
                        $('#outrasinformacoes').click();
                        break;
                    case 'outrasinformacoes':
                        break;
                }
            }

            function abaAnterior(event){
                event.preventDefault();
                nomeAba = verificaAbaAtiva();

                switch (nomeAba.attr('id')) {
                    case 'dadosfatura':
                        break;
                    case 'itensfaturados':
                        $('#dadosinstrumentosdecobranca').click();
                        break;
                    case 'outrasinformacoes':
                        $('#itensfaturados').click();
                        break;
                }
            }
        });

    </script>
@endpush
