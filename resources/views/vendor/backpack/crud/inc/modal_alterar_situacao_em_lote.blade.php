<!-- Esta modal é chamada por include em alterarsituacaoemlotemodal.blade.php -->

<!-- Início modal - #598 -->
<form name="formModalAlterarSituacaoEmLote" method="POST" >

    <div class="modal fade " id="modalAlterarSituacaoEmLote" style="display: none" tabindex="-1" role="dialog" aria-labelledby="modalAlteracaoSituacaoLabel" aria-hidden="true" >
        <div class="modal-dialog modal-dialog-centered modal-lg " role="document" >
            <div class="modal-content" >
                <div class="modal-header">
                    <h3 class="modal-title">
                        Alterar Situação em Lote
                    </h3>
                </div>

                <div class="modal-body ">
                    <div class="form-group ">
                            <select name="situacaoDesejada" class="form-control form-control-lg " >
                                <option value="PEN" selected="">Pendente</option>
                                <option value="PGS">Pagamento Suspenso</option>
                                <option value="PGP">Pagamento Parcial</option>
                                <option value="ANA">Analisado</option>
                                <option value="PPG">Pronto para Pagamento</option>
                                <option value="APR">Siafi Apropriado</option>
                                <option value="ERR">Siafi Erro</option>
                                <option value="ENV">Siafi Enviado</option>
                                <option value="PGO">Pago</option>
                                <option value="AND">Aproriacao em Andamento</option>
                            </select>

                    </div>
                </div>

                <div class="modal-footer">


                    <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal" >
                        <span aria-hidden="true">Cancelar</span>
                    </button>

                    <button type="button" class="btn btn-primary btn-lg" data-dismiss="modal" onClick="javascript:fecharPopupESubmeterPagina()">
                        <span aria-hidden="true">Salvar</span>
                    </button>

                </div>
            </div>
        </div>
    </div>

</form>
<!-- Fim modal - #598 -->


