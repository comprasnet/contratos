<div id="saveActions" class="form-group">

    <input type="hidden" name="save_action" value="{{ $saveAction['active']['value'] }}">
    <a href="" class="btn btn-info" id="prev_aba"><span class="fa fa-arrow-circle-left"></span> &nbsp;Aba Anterior</a>
    <div class="btn-group" id="botoes_contrato">

        <button type="button"
                class="btn btn-success"
                id="btn-submit-itens-contrato"
                data-loading-text="Salvando dados..."
                onclick="salvarContrato(event,this)"
        >
            <span class="fa fa-save" role="presentation" aria-hidden="true"></span> &nbsp;
            <span data-value="{{ $saveAction['active']['value'] }}">{{ $saveAction['active']['label'] }}</span>
        </button>

        <button type="button" class="btn btn-success dropdown-toggle"
                id="btn-submit-dropdrown-toggle"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
        >
            <span class="caret"></span>
            <span class="sr-only">&#x25BC;</span>
        </button>

        <ul class="dropdown-menu">
            @foreach( $saveAction['options'] as $value => $label)
                <li><a href="javascript:void(0);" data-value="{{ $value }}">{{ $label }}</a></li>
            @endforeach
        </ul>

    </div>
    <div class="btn-group" id="btn_salvar_elaboracao">

        <button
            type="button"
            class="btn btn-facebook"
            id="btn-submit-salvar_elaboracao"
            data-loading-text="Salvando dados..."
            onclick="salvarElaboracao(event,this)"
        >
            <span class="fa fa-bookmark" role="presentation" aria-hidden="true"></span> &nbsp;
            <span data-value="{{ $saveAction['active']['value'] }}">Salvar Rascunho</span>
        </button>


    </div>


    <a href="" class="btn btn-info" id="next_aba">Próxima Aba <span class="fa fa-arrow-circle-right"></span></a>
    <a href="{{ $crud->hasAccess('list') ? url($crud->route) : url()->previous() }}" class="btn btn-default"
       id="cancelar"><span class="fa fa-ban"></span> &nbsp;{{ trans('backpack::crud.cancel') }}</a>
</div>
@push('after_scripts')
    <script src="{{ asset('js') }}/buscarCamposAutoPreenchimento.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {

            valor_global = 0;
            retornoAjax = 0;
            minutas_id = [];

            var maxLength = '000.000.000.000.000,0000'.length;

            $('#botoes_contrato').hide();
            $('#cancelar').hide();
            $('#prev_aba').hide();

            habilitaDesabilitaBotoes();

            $('body').on('click', '#prev_aba', function (event) {
                abaAnterior(event);
            });

            $('body').on('click', '#next_aba', function (event) {
                proximaAba(event);
            });


            $('body').on('focusout', 'input[name=data_assinatura]', function (event) {
                atualizarDataInicioItens();
            });

            $("[name='minutasempenho[]']").on('change', function (event) {
                minutas_id = [];
                minutas_id = retornaMinutaIds();
            });

            $(document).on('change', '#select2_ajax_multiple_minutasempenho', function (event) {
                if (!null_or_empty("#select2_ajax_multiple_minutasempenho")) {
                    //preenche campos atráves dos ids de minutas
                    buscarCamposAutoPreenchimento().then(function () {
                        //atualiza dados de compra antes de buscar os itens
                        consultaEAtualizaCompraSiasg(event).then(function () {
                            //depois de atualizar os dados da compra busca os itens
                            carregaitens(event, minutas_id);
                        })
                    });
                }

                if (null_or_empty("#select2_ajax_multiple_minutasempenho")) {
                    limparCamposMinutaEmpenhoAlterada(true)
                }
            });
        });

        const abas = [
            'dadosdocontrato',
            'caracteristicasdocontrato',
            'itensdocontrato',
            'empenhos',
            'vigenciavalores'
        ];

        const configuracoesAbas = {
            dadosdocontrato: {
                showBotoes: false,
                showSalvar: false,
                showCancelar: false,
                showPrev: false,
                showNext: true
            },
            caracteristicasdocontrato: {
                showBotoes: false,
                showSalvar: false,
                showCancelar: false,
                showPrev: true,
                showNext: true
            },
            itensdocontrato: {
                showBotoes: false,
                showSalvar: false,
                showCancelar: false,
                showPrev: true,
                showNext: true
            },
            empenhos: {showBotoes: false, showSalvar: false, showCancelar: false, showPrev: true, showNext: true},
            vigenciavalores: {showBotoes: true, showSalvar: true, showCancelar: true, showPrev: true, showNext: false}
        };

        function indiceAbaAtiva() {
            const nomeAba = verificaAbaAtiva().attr('id');
            return abas.indexOf(nomeAba);
        }

        // Função para alternar a visibilidade dos botões com base na aba ativa
        function habilitaDesabilitaBotoes() {
            const nomeAba = verificaAbaAtiva().attr('id');
            const config = configuracoesAbas[nomeAba] || {};

            $('#botoes_contrato').toggle(config.showBotoes);
            $('#btn_salvar_elaboracao').toggle(config.showSalvar);
            $('#cancelar').toggle(config.showCancelar);
            $('#prev_aba').toggle(config.showPrev);
            $('#next_aba').toggle(config.showNext);
        }

        function proximaAba(event) {
            event.preventDefault();
            const indiceAtual = indiceAbaAtiva();

            // Se não estiver na última aba, avança para a próxima
            if (indiceAtual !== -1 && indiceAtual < abas.length - 1) {
                $('#' + abas[indiceAtual + 1]).click();
            }
        }

        function abaAnterior(event) {
            event.preventDefault();
            const indiceAtual = indiceAbaAtiva();

            // Se não estiver na primeira aba, volta para a anterior
            if (indiceAtual > 0) {
                $('#' + abas[indiceAtual - 1]).click();
            }
        }

        // Evento de clique para cada aba que chama a função de habilitar/desabilitar botões
        $(document).on('click', '#' + abas.join(', #'), function () {
            habilitaDesabilitaBotoes();
        });

        //atualiza o valor da parcela do contrato
        function atualizarValorParcela() {

            valor_global = $('#valor_global').val();
            numero_parcelas = $('#num_parcelas').val();

            valor_global !== '' || valor_global > 0 ? $('#valor_parcela').val(valor_global / numero_parcelas)
                :
                $('#valor_parcela').val('');
        }

        // verifica se o array esta nulo ou vazio
        function null_or_empty(str) {
            var v = $(str).val();
            if (v === null || v.length == 0) {
                return true;
            }
            return false;
        }

        let dadosCamposEmpenho = null

        //busca a modalidade de acordo com a primeira minuta de empenho selecionada para popular os campos
        function buscarCamposAutoPreenchimento() {
            var arrayMinutas = $("#select2_ajax_multiple_minutasempenho").val();

            var url = "{{route('buscar.campos.contrato.empenho',':id')}}";
            url = url.replace(':id', arrayMinutas[0]);
            let dadosCampos = null
            return axios.request(url)
                .then(response => {
                    dadosCampos = response.data;

                    if (dadosCampos) {
                        // altera campo unidade de compra
                        $('select[name=unidadecompra_id]').append(`<option value="${dadosCampos.unidade_id}">${dadosCampos.codigo} -  ${dadosCampos.nomeresumido}</option>`);
                        $('select[name=unidadecompra_id]').val(dadosCampos.unidade_id).change();

                        //altera campo de modalidade da licitacao
                        $("select[name=modalidade_id]").val(dadosCampos.modalidade_id).change();
                        $('#select2-select2_ajax_unidadecompra_id-container .select2-selection__placeholder').remove();

                        // altera campo de amparos legais
                        $('#select2_ajax_multiple_amparoslegais option').remove();
                        $('#select2_ajax_multiple_amparoslegais').append(`<option value="${dadosCampos.amparo_legal_id}">${dadosCampos.ato_normativo} - Artigo: ${dadosCampos.artigo}</option>`);
                        $('#select2_ajax_multiple_amparoslegais').val(dadosCampos.amparo_legal_id).change();

                        // altera campo de numero/ano da licitacao
                        $("#licitacao_numero").val(dadosCampos.compra_numero_ano);


                        if (dadosCampos.origem === 3){
                            $("#numero_contratacao").val(dadosCampos.numero_contratacao);
                            $('#contrata_mais_brasil_1').prop('checked', true).trigger('click')
                        }

                        let itemSelecionado = $("select[name=tipo_id] option:selected").text();
                        preencherInformacoesMinutaEmpenhoSelecionada(itemSelecionado, dadosCampos)

                    }
                })
                .catch(error => {
                    alert(error);
                })
                .finally(function () {
                    dadosCamposEmpenho = dadosCampos
                })
            event.preventDefault()
        }

        function atualizarDataInicioItens() {
            $("#table-itens").find('tr').each(function () {
                if ($(this).find('td').eq(7).find('input').val() === "") {
                    $(this).find('td').eq(7).find('input').val($('input[name=data_assinatura]').val());
                }
            });
        }

        function verificaAbaAtiva() {
            var nomeAba = $('#form_tabs ul li.active a');
            return nomeAba;
        }

        function retornaMinutaIds() {
            var selected = $("[name='minutasempenho[]']").find(':selected');
            var array_minutas_id = [];
            selected.each(function (index, option) {
                array_minutas_id[index] = option.value;
            })
            return array_minutas_id;
        }

        function carregaitens(event, minutas_id) {
            //não deixar apagar itens selecionados pelo botão da compra
            $("#table-itens tr:not(.item-da-compra)").remove();
            if (minutas_id.length > 0) {
                var url = "{{route('buscar.itens.modal',':minutas_id')}}",
                    id_fornecedor = $('#select2_ajax_fornecedor_id').val();
                url = url.replace(':minutas_id', minutas_id);
                axios.get(url,
                    {
                        params: {
                            id_fornecedor
                        }
                    }
                )
                    .then(response => {
                        var itens = response.data;
                        var qtd_itens = itens.length;
                        itens.forEach(function (item) {
                            var linhas = $("#table-itens tr:not(.item-da-compra)").length;
                            if (qtd_itens > linhas) {
                                adicionaLinhaItem(item, false);
                            }
                        });
                        minutas_id = [];
                    })
                    .catch(error => {
                        alert(error);
                    })
                    .finally()
                event.preventDefault()
            }
            atualizarValorTotal();
        }

        function consultaEAtualizaCompraSiasg() {

            var modalidade_id = $("select[name=modalidade_id]").val(),
                unidade_origem_id = $('select[name=unidadecompra_id]').val(),
                numero_ano = $("#licitacao_numero").val(),
                numero_contratacao = $("#numero_contratacao").val(),
                objParams = {
                    modalidade_id,
                    unidade_origem_id,
                    numero_ano,
                    numero_contratacao,
                };

            var url = "{{route('consultar.comprasiasg.minuta')}}";
            return axios.get(url,
                {
                    params: {
                        objParams
                    }
                }
            )
                .then()
                .catch(error => {
                    if (error.response) {
                        new PNotify({
                            title: 'Ops ocorreu um erro',
                            text: error.response.data.message,
                            type: "error"
                        });
                        // Request made and server responded
                    } else if (error.request) {
                        // The request was made but no response was received
                        console.log(error.request);
                    } else {
                        // Something happened in setting up the request that triggered an Error
                        console.log('Error', error.message);
                    }
                });
            event.preventDefault()
        }

        /**
         * guarda html da grid de itens em campo hidden
         */
        function configurarFormParaSubmit() {
            atualizaValueHTMLCamposAbaItem();
            var htmlGridItem = $('#table').html();
            $('input[name=adicionaCampoRecuperaGridItens]').val(htmlGridItem);
        }

        function salvarContrato(event, btn) {
            event.preventDefault();
            $('#elaboracao').val(0);
        }

        function salvarElaboracao(event, btn) {
            event.preventDefault();
            $('#elaboracao').val(1);
        }

    </script>
    <script src="{{ asset('js/mensagem/confirmacaoPublicacao.js')}}?time={{microtime()}}"></script>
@endpush
