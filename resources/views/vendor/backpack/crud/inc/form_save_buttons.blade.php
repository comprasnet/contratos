<div id="saveActions" class="form-group">

    <input type="hidden" name="save_action" value="{{ $saveAction['active']['value'] }}">

    <div class="btn-group">

        <button type="submit" class="btn btn-success botao-salvar" id="#save_button">
            <span class="fa fa-save" role="presentation" aria-hidden="true"></span> &nbsp;
            <span data-value="{{ $saveAction['active']['value'] }}" class="valor">{{ $saveAction['active']['label'] }}</span>
        </button>

        <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aira-expanded="false">
            <span class="caret"></span>
            <span class="sr-only">&#x25BC;</span>
        </button>

        <ul class="dropdown-menu">
            @foreach( $saveAction['options'] as $value => $label)
            <li><a href="javascript:void(0);" data-value="{{ $value }}">{{ $label }}</a></li>
            @endforeach
        </ul>
    </div>

    <a href="{{ $crud->hasAccess('list') ? url($crud->route) : url()->previous() }}" class="btn btn-default"><span class="fa fa-ban"></span> &nbsp;{{ trans('backpack::crud.cancel') }}</a>
</div>

@push('after_scripts')
<script type="text/javascript">
    var mensagemAlertaCampoVazio = 'Preencha os campos obrigatórios!'
    $(document).ready(function () {
        var url = "{{Route::current()->getName()}}";
        var vazio = "falso";
        $('body').on('click', '.btn.btn-success', function (event) {
            event.preventDefault();
            if (url == "crud.arquivos.create"){

                $("#novos-arquivos tbody select, #tableEscolhaAnexoSei tbody select").each(function(chave,campo){
                    if($(this).hasClass('validar')){
                        if($(this).val() == '0'){
                            vazio = "true";
                            $(this).addClass('erro');
                        }else{
                            $(this).removeClass('erro');
                        }
                    }
                });

                if(vazio == "true"){
                    alert(mensagemAlertaCampoVazio)
                } else {
                    $('form').submit();

                }
                var vazio = "falso";
            }else{
                $('form').submit();
            }
        });
    });

</script>
@endpush
