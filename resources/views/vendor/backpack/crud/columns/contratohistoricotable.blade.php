@php
    $id = $entry->getKey();
    $value = \App\Models\Contratohistorico::where('contrato_id',$id)
            ->where('contratohistorico.elaboracao', false)
            ->orderBy('data_assinatura')
            ->get();
    $columns = [
            'data_assinatura' => 'Data Assinatura',
            'numero' => 'Número',
            "tipo" => 'Tipo',
            'observacao' => 'Observação',
            'vigencia_inicio' => 'Data Início',
            'vigencia_fim' => 'Data Fim',
            'valor_global' => 'Vlr. Global',
            'num_parcelas' => 'Parcelas',
            'valor_parcela' => 'Vlr. Parcela'
        ];
@endphp

<span>
	@if ($value && count($columns))
        <table class="table table-bordered table-condensed table-striped m-b-0">
		<thead>
			<tr>
				@foreach($columns as $tableColumnKey => $tableColumnLabel)
                    <th>{{ $tableColumnLabel }}</th>
                @endforeach
			</tr>
		</thead>
		<tbody>
			@foreach ($value as $tableRow)
                <tr>
				@foreach($columns as $tableColumnKey => $tableColumnLabel)
                        <td>
                            @if($tableColumnKey == 'tipo')
                                {{ $tableRow->tipo->descricao }}
                            @else


                                @if($tableColumnKey == 'valor_parcela' || $tableColumnKey == 'valor_global')
                                    {{ number_format($tableRow->{$tableColumnKey}, 2, ',', '.') }}

                                @else


                                    @if($tableColumnKey == 'data_assinatura' ||  $tableColumnKey == 'vigencia_inicio' || $tableColumnKey == 'vigencia_fim' )
                                        {{ Carbon\Carbon::parse($tableRow->{$tableColumnKey})->format('d/m/Y') }}
                                    @else
                                        {{ $tableRow->{$tableColumnKey} }}
                                    @endif

                                @endif

                            @endif
					</td>
                    @endforeach
			</tr>
            @endforeach
		</tbody>
	</table>
    @endif
</span>
