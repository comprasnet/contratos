<span>
    <table class="table table-bordered table-condensed table-striped m-b-0 collapsed">
        <thead>
        <tr>
            <th>Ano</th>
            <th>Mês</th>
            <th>Valor</th>
        </tr>
        </thead>
        <tbody>
        @foreach($entry->contratofaturasmesano as $item)
            <tr>
                <td>
                    {{ $item->anoref }}
                </td>
                <td>
                    {{ $item->mesref }}
                </td>
                <td>
                    {{ number_format($item->valorref, 2, ',', '.') }}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</span>
