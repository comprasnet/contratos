@php
    $value   = $column['values'];
	$columns = $column['columns'];
@endphp

<span>
	@if ($value && count($columns))
        <input type="search" id="input-filter-{{$column['table_id']}}" placeholder="Filtrar um valor">

        <table class="table table-bordered table-condensed table-striped m-b-0" id="{{$column['table_id']}}">
		<thead>
			<tr>
				@foreach($columns as $tableColumnKey => $tableColumnLabel)
				<th>{{ $tableColumnLabel }}</th>
				@endforeach
			</tr>
		</thead>
		<tbody>
			@foreach ($value as $tableRow)
			<tr>
				@foreach($columns as $tableColumnKey => $tableColumnLabel)
					<td style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; max-width: 30ch;">
						{!! $tableRow->{$tableColumnKey} ?? $tableRow[$tableColumnKey] !!}
					</td>
				@endforeach
			</tr>
			@endforeach
		</tbody>
	</table>
	@endif
</span>
<script>
    const inputFilter_{{$column['table_id']}} = document.getElementById('input-filter-{{$column['table_id']}}');
    const tableData_{{$column['table_id']}} = document.getElementById('{{$column['table_id']}}');
    const tableRows_{{$column['table_id']}} = tableData_{{$column['table_id']}}.getElementsByTagName('tr');

    // Selecionar todas as células da tabela
    const cells_{{$column['table_id']}} = document.querySelectorAll('#{{$column['table_id']}} td');


    inputFilter_{{$column['table_id']}}.addEventListener('keyup', () => {
        const filter = inputFilter_{{$column['table_id']}}.value.toLowerCase();
        for (let i = 0; i < tableRows_{{$column['table_id']}}.length; i++) {
            const row = tableRows_{{$column['table_id']}}[i];
            if (row.parentNode.nodeName === 'THEAD') { // Verifica se é uma linha de cabeçalho (thead)
                continue;
            }
            const cells = row.getElementsByTagName('td');
            let showRow = false;
            for (let j = 0; j < cells.length; j++) {
                const cell = cells[j];
                if (cell.innerText.toLowerCase().indexOf(filter) > -1) {
                    showRow = true;
                    break;
                }
            }
            row.style.display = showRow ? '' : 'none';
        }
    });

    // Iterar sobre todas as células
    cells_{{$column['table_id']}}.forEach(cell => {
        // Obter o texto da célula
        const text = cell.textContent.trim();

        // Se o texto tiver mais de 30 caracteres, limitá-lo e adicionar um título
        if (text.length > 30) {
            const truncatedText = text.slice(0, 30) + '...';
            cell.textContent = truncatedText;
            cell.setAttribute('title', text);
        }
    });
</script>
