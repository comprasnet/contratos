@php
    $id = $entry->getKey();
    $arquivos = \App\Models\Contratoarquivo::where('contrato_id',$id)
                                            ->where("restrito",false)
                                            ->where("rascunho",false)
                                            ->whereNull('deleted_at')
                                            ->orderBy('created_at','DESC')->get();

@endphp

<table class="table table-bordered table-condensed table-striped m-b-0">
    <thead>
        <tr>
            <th>Tipo</th>
            <th>Nome</th>
            <th>Tamanho</th>
            <th>Criado</th>
        </tr>
    </thead>

    <tbody>

        @foreach ($arquivos as $arquivo)

            @php
                if(  !filter_var($arquivo->arquivos, FILTER_VALIDATE_URL)  ){
                    $urlArquivo = url("gescon/consulta/download-arquivo-contrato/{$arquivo->id}");
                    $tamanhoArquivo = 0;
                    try{
                        $tamanhoArquivo = Formatador::formatSizeUnits(Storage::size($arquivo->arquivos));
                    } catch(Exception $e) {
                        $tamanhoArquivo = 0;
                    }
                } else {
                    $urlArquivo = $arquivo->arquivos;
                    $tamanhoArquivo = 'Link externo';
                }



                $criado =  Carbon\Carbon::parse($arquivo->created_at)->format('d/m/Y H:i:s')

            @endphp

        <tr>

            @if(  filter_var($arquivo->arquivos, FILTER_VALIDATE_URL)  )
                <td  style="text-align: center" ><i class="fa fa-fw fa-globe"></i></td>
            @else
                <td  style="text-align: center"><i class="fa fa-fw fa-file-pdf-o"></i></td>
            @endif

            <td> <a target="_blank" href="{{ $urlArquivo }}">{!! htmlspecialchars_decode($arquivo->descricao) !!}</a> </td>
            <td>{{$tamanhoArquivo}}</td>
            <td>{{$criado}}</td>

        </tr>

        @endforeach

    </tbody>
</table>
