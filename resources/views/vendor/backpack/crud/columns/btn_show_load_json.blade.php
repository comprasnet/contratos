{{-- Botão para carregar um JSON formatado com a biblioteca prism  --}}
<div>
    <button type="button" class="btn btn-xs btn-default load-json" data-url="{{ url($column['url']) }}">
        <i class="fa fa-eye"></i> Carregar
    </button>
    <pre style="display:none;"><code class="language-json json-display"></code></pre>

</div>


@push('after_styles')
    <style>

        pre[class*="language-"] {
            background: #272822;
        }
        :not(pre) > code[class*="language-"] {
            padding: 0.1em;
            border-radius: 0.3em;
            white-space: normal;
        }
        .token.cdata, .token.comment, .token.doctype, .token.prolog {
            color: #8292a2;
        }
        .token.punctuation {
            color: #f8f8f2;
        }
        .token.namespace {
            opacity: 0.7;
        }
        .token.constant, .token.deleted, .token.property, .token.symbol, .token.tag {
            color: #f92672;
        }
        .token.boolean, .token.number {
            color: #ae81ff;
        }
        .token.attr-name, .token.builtin, .token.char, .token.inserted, .token.selector, .token.string {
            color: #a6e22e;
        }
        .language-css .token.string, .style .token.string, .token.entity, .token.operator, .token.url, .token.variable {
            color: #f8f8f2;
        }
        .token.atrule, .token.attr-value, .token.class-name, .token.function {
            color: #e6db74;
        }
        .token.keyword {
            color: #66d9ef;
        }
        .token.important, .token.regex {
            color: #fd971f;
        }
        .token.bold, .token.important {
            font-weight: 700;
        }
        .token.italic {
            font-style: italic;
        }
        .token.entity {
            cursor: help;
        }


    </style>

@endpush

@push('after_scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.25.0/prism.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.25.0/components/prism-json.min.js"></script>

    <script>
        $(document).ready(function () {
            $('.load-json').on('click', function () {
                var button = $(this);
                var url = button.data('url');

                var preElement = button.next('pre');
                var codeElement = preElement.find('.json-display');

                if (preElement.is(':visible')) {
                    preElement.hide();
                    return
                }

                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {
                        _token: '{{ csrf_token() }}'
                    },
                    success: function (response) {
                        var jsonData = JSON.stringify(response, null, 4);

                        codeElement.text(jsonData);
                        preElement.show();

                        Prism.highlightElement(codeElement[0]);
                    },
                    error: function () {
                        alert('Falha ao carregar o JSON.');
                    }
                });
            });
        });
    </script>


@endpush
