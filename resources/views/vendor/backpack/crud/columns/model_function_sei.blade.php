{{-- custom return value --}}
@php
	$value = $entry->{$column['function_name']}(...($column['function_parameters'] ?? []));
@endphp

<!-- imprime o link do SEI na grid principal -->
<span>	
	@php echo $value; @endphp
</span>
