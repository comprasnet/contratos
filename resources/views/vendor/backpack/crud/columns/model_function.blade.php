{{-- custom return value --}}
@php
	$value = $entry->{$column['function_name']}(...($column['function_parameters'] ?? []));
@endphp


@php
    // mostrar apenas o link, abrindo em uma nova página.
    if( @$column['name'] == 'link_publicacao' && $crud->entity_name == 'Publicação' &&  ( Route::current()->getName() == 'crud.publicacao.show' || Route::current()->getName() == 'crud.publicacao.search' ) ){
        @endphp
            <span>
                {!! $value !!}
            </span>
        @php
    } else {
        @endphp
            <span>
                {!! (array_key_exists('prefix', $column) ? $column['prefix'] : '').str_limit($value, array_key_exists('limit', $column) ? $column['limit'] : 50, ' <i class="fa fa-info-circle" title="'.$value.'"></i>').(array_key_exists('suffix', $column) ? $column['suffix'] : '') !!}
            </span>
        @php
    }
@endphp



