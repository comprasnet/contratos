{{-- regular object attribute --}}
@php
	$value = data_get($entry, $column['name']);

	if (is_array($value)) {
		$value = json_encode($value);
	}
@endphp

<span>
    <a href='{{$value}}' target='_blank'>{{ substr($value,0,35)}} @if ($value) <i class="fa fa-info-circle" title="{{$value}}"></i> @endif</a>

</span>
