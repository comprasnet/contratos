@php
    $url = url($crud->route) .'/'. $entry->minutaempenhos_remessa_id.'/show/' . $entry->getKey();
    if ($entry->alteracao_fonte_minutaempenho_id !== null) {
        $url = str_replace('alteracao','alteracaoFonte',$url);
    }
@endphp
<a href="{{ $url }}"
   class="btn btn-xs btn-default ladda-button" data-style="zoom-in" title="{{ trans('backpack::crud.preview') }}">
    <span class="ladda-label"><i class="fa fa-eye"></i></span>
</a>
