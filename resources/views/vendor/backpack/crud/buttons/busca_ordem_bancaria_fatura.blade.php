<!--PERMITIDO APENAS PARA ADMINISTRADOR ATÉ HOMOLOGAÇÃO-->
@if (backpack_user()->can('apropriar_fatura'))

    @if(!empty($entry->numdh))

        @include('vendor.backpack.base.mod.apropriacao.modal-busca-ordem-bancaria',
                        [
                            //propriedades do botao
                            'idBtn' => "busca-ordem-bancaria",
                            'name' => "busca-ordem-bancaria",
                            'hint' => 'Busca Ordem Bancária',
                            'cor' => false,
                            'label' => 'Ordens Bancária',
                            'class' => 'btn btn-xs btn-default',
                            'idContrato' => $crud->contrato_id,
                            'sfpadrao_id' => $entry->sfpadrao_id,
                            'numDocumento' => $entry->sfpadrao->getDocFormatadoOrdemBancaria(),
                         ])

    @endif

@endif


