@if( $entry->Status->descricao == 'Pronto para envio')
<a href="/gescon/contrato/{{$entry->Contrato->id}}/minutas/{{$entry->getKey()}}/imprimir" target="__blank" class="btn btn-xs btn-default"><i class="fa fa-file-pdf-o" aria-hidden="true" title="Gerar PDF"></i></a>
@endif
