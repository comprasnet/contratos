@php
    $situacoes = $crud->situacoes;
    //dd($situacoes);
@endphp
<!--PERMITIDO APENAS PARA Execução Financeira-->


<!-- incluído administrador -->
@if (backpack_user()->can('apropriar_fatura'))

    <!-- incluído justificativasParaAlteracaoEmLote -->
    @include('vendor.backpack.base.mod.instrumentosdecobranca.modal-confirmacao-alteracao-status',
            [
                'idBtn' => "apropriar-fatura-bottom",
                'name' => "apropriar-fatura-bottom",
                'hint' => 'Mudar situação do(s) Instrumento(s) de Cobrança',
                'route' => false,
                'cor' => false,
                'disabled' => false,
                'icon' => false,
                'class' => 'btn btn-primary btn-sm bulk-button',
                'idContrato' => false,
                'idFatura' => false,
                //identifica origem do botao
                'btnApropriarBottom' => true//529 mudar aqui depois de coinhecer o resto
                ],
                [
                    "selectSituacoes" => $crud->situacoes,
                    "justificativasParaAlteracaoEmLote" => $crud->justificativasParaAlteracaoEmLote
                ]
    )

@endif
