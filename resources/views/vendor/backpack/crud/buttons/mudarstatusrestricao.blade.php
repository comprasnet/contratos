@if (backpack_user()->hasRole('Administrador') ||
    backpack_user()->hasRole('Setor Contratos') ||
    backpack_user()->hasRole('Administrador Órgão') ||
    backpack_user()->hasRole('Administrador Unidade') ||
    backpack_user()->hasRole('Administrador Suporte')
    )
    @if(!$entry->restrito)
        <a href="{{$entry->mudarStatusRestricao()}}" class="btn btn-xs btn-default" title="Alterar o status do documento para restrito" ><i class="fa fa-lock"></i></a>
    @else
        <a href="{{$entry->mudarStatusRestricao()}}" class="btn btn-xs btn-default" title="Alterar o status do documento para público" ><i class="fa fa-unlock"></i></a>
    @endif
@endif
