@php
    $situacaoApropriacao = $entry->apropriacao->getSituacaoApropriacao();
@endphp

<!--Se for Administrador-->
@if (backpack_user()->can('apropriar_fatura'))
    <!--Se estiver em Andamento ou Emitida no SIAFI-->
    @if($situacaoApropriacao === 'AND' || $situacaoApropriacao === 'APR' || $situacaoApropriacao === 'ERR')
        <a
            href="/apropriacao/fatura-form/alterar-antes-apropriar/{{$entry->sfpadrao_id}}/{{$entry->apropriacoes_faturas_id}}"
            class="btn btn-xs btn-default"
            data-style="zoom-in"
            title="{{$situacaoApropriacao === 'AND' ? 'Apropriação em Andamento' : 'Apropriado no Siafi'}}"
        >
            <i class="fa fa-edit"></i>
        </a>
    @endif
@endif
