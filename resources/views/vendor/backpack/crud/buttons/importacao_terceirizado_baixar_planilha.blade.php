
<a class="btn btn-primary ladda-button"  href="{{ route('planilhaTerceirizado.download') }}">
    <span class="ladda-label">
        <i class="fa fa-file-text" aria-hidden="true"></i>
        Baixar planilha modelo
    </span>
</a>
