@php
    $contratofaturas_id = request()->route('contratofaturas_id');
    $route = route('apropriacao.fatura.create', ['contrato' => $crud->contrato_id, 'id' => $contratofaturas_id]);
@endphp


<!--PERMITIDO APENAS PARA ADMINISTRADOR ATÉ HOMOLOGAÇÃO-->
@if (backpack_user()->can('apropriar_fatura'))

    @include('vendor.backpack.base.mod.apropriacao.modal-confirmacao-apropriacao',
                    [
                        //propriedades do botao
                        'idBtn' => "$contratofaturas_id",
                        'name' => "apropriar-fatura-bottom",
                        'hint' => 'Apropriação de faturas',
                        'route' => $route,
                        'cor' => false,
                        'disabled' => false,
                        'icon' => false,
                        'label' => 'Nova Apropriação',
                        'class' => 'btn btn-primary btn-sm',
                        'idContrato' => $crud->contrato_id,
                        'idFatura' => $contratofaturas_id,
                        //identifica origem do botao
                        'btnApropriarBottom' => false
                     ])

@endif
