{{--{{ dd(get_defined_vars()['__data']) }}--}}
<div class="btn-group">
    <button type="button" title="Mais" class="btn btn-xs btn-default dropdown-toggle dropdown-toggle-split"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Mais"><i class="fa fa-gears"></i>
        <span class="caret"></span>
        <span class="sr-only">Toggle Dropdown</span>
    </button>
    <ul class="dropdown-menu dropdown-menu-right">
        <li><a href="/admin/compra/{{$entry->id}}/compraitem">  <i
                    class="fa fa-angle-right"></i>Itens</a>
        </li>
        <li><a href="/admin/compra/{{$entry->id}}/minutas" >  <i
                class="fa fa-angle-right"></i>Minutas</a>
        </li>
    </ul>
</div>
