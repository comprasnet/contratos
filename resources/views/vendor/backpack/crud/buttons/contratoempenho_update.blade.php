@if($entry->user_id == backpack_user()->id || $entry->user_id == null)
    @if ($crud->hasAccess('update') && $entry->getKey() && ($entry->deleted_at || !$entry->last_contratofatura_id))
        @if(!$entry->automatico)
            <a href="{{ url($crud->route.'/'.$entry->getKey().'/edit') }}" class="btn btn-xs btn-default" title="Editar"><i class="fa fa-edit"></i></a>
        @endif
    @endif
@endif
