@if( is_null($entry->link_sei) == false )
<a href="{{ $entry->link_sei }}" class="btn btn-xs btn-default" target="blank" title="link SEI"><i class="fa fa-external-link" aria-hidden="true"></i></a>
@endif
