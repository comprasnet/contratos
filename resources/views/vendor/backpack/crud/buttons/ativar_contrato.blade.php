{{--@include('crud::buttons.elaboracao.modal')--}}
{{--@include('vendor.backpack.base.mod.apropriacao.modal-confirmacao-apropriacao')--}}
{{--{{ dump(get_defined_vars()['__data']) }}--}}
{{--{{dd($entry)}}--}}
{{--{{dd(get_defined_vars()['__data'])}}--}}

@php /*clock($entry); clock($crud);*/ @endphp

@if($entry->elaboracao)
    <button
        id="btn_abrir_modal"
        name="btn_abrir_modal"
        type="button"
        class="btn btn-xs btn-default"
        title="Ativar"
        {{--onclick="abrirModalAtivarContrato(this, '{{$entry->contrato_id}}', '{{$entry->id}}')"--}}
        onclick="abrirModalAtivarContrato(
            '{{$entry->id}}'
            , {data_assinatura:'{{$entry->data_assinatura}}'
                , data_publicacao:'{{$entry->data_publicacao}}'
                , vigencia_inicio:'{{$entry->vigencia_inicio}}'
                , numero:'{{$entry->numero}}'
                , tipo_id:'{{$entry->tipo_id}}'
                , unidadeorigem_id:'{{$entry->unidadeorigem_id}}'
            })"
    >
        <i class="fa fa-check"></i>
    </button>
@endif


