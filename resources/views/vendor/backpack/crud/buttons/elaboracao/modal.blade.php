{{--@php use Illuminate\Support\Arr; @endphp--}}
@php //clock(Arr::pluck($crud->create_fields, 'modal_elaboracao', 'name')); @endphp
@php

    $filtered =  array_filter($crud->create_fields , function ($value, $key) {
            return isset($value['modal_elaboracao']) && $value['modal_elaboracao'] ;
        }, ARRAY_FILTER_USE_BOTH);

//        $filtered = Arr::where();


@endphp
<div id="modalAtivarTermo" tabindex="-1" class="modal fade"
     role="dialog"
     aria-labelledby=""
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">
                    Ativar {{$crud->entity_name}}
                    <button type="button" class="close" data-dismiss="modal" aria-label="Fechar" style="font-size: 3rem;">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </h3>
            </div>

            <form method="post" action=""
                  accept-charset="UTF-8" id="form_modal">
                {!! csrf_field() !!}
                <div class="modal-body" id="textoModal">
                    <fieldset class="form-group">
                        <div class="hidden ">
                            <input type="hidden" name="id" class="form-control">
                        </div>
                        @foreach($filtered as $field)
                            @includeWhen(isset($field['type']), 'crud::fields.'.$field['type'], ['field' => $field])
                        @endforeach
                    </fieldset>
                </div>
                <div class="modal-footer">
    <button id="btnAtivar" type="submit" class="btn btn-success" onclick="desativarBotao()">
                        <span class="ladda-label">
                            <i class="fa fa-save"></i>&nbsp; Ativar
                        </span>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>


<script type="text/javascript">
    function desativarBotao() {
        var btn = document.getElementById('btnAtivar');
        btn.innerHTML = '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Processando...';
        btn.disabled = true;
        btn.classList.add('disabled');
        btn.closest('form').submit();
    }
    function abrirModalAtivarContrato(id, datas){
        let url ="{{route('ativar.contrato', ['contrato_id' => ':contrato_id'])}}";
        url = url.replace(':contrato_id', id);
        $('#form_modal').attr('action', url);

        $('#form_modal input[name="id"]').val(id);
        $('#form_modal input[name="data_assinatura"]').val(datas.data_assinatura);
        $('#form_modal input[name="data_publicacao"]').val(datas.data_publicacao);
        $('#form_modal input[name="vigencia_inicio"]').val(datas.vigencia_inicio);
        $('#form_modal input[name="numero"]').val(datas.numero);
        $('#form_modal').append('<input type="hidden" name="tipo_id" ' +
            'value="'+ datas.tipo_id +'" />');
        $('#form_modal').append('<input type="hidden" name="unidadeorigem_id" ' +
            'value="'+ datas.unidadeorigem_id +'" />');
        /*$('#form_modal').append('<input type="hidden" name="data_inicio_novo_valor" ' +
            'value="'+ datas.data_inicio_novo_valor +'" />');*/
        $('#form_modal').append('<input type="hidden" name="elaboracao" ' +
            'value="0" />');

        $('#modalAtivarTermo').modal({
            backdrop: 'static',
            keyboard: false
        });
    }
    function abrirModalAtivarContratoHistorico(btn, contrato_id, id, datas) {

        let url ="{{route('ativar.apostilamento', ['contrato_id' => ':contrato_id', 'contratohistorico_id' => ':contratohistorico_id'])}}";
        url = url.replace(':contrato_id', contrato_id);
        url = url.replace(':contratohistorico_id', id);

        $('#form_modal').attr('action', url);

        $('#form_modal input[name="id"]').val(id);
        $('#form_modal input[name="data_assinatura"]').val(datas.data_assinatura);
        $('#form_modal input[name="data_publicacao"]').val(datas.data_publicacao);
        $('#form_modal').append('<input type="hidden" name="data_inicio_novo_valor" ' +
            'value="'+ datas.data_inicio_novo_valor +'" />');
        $('#form_modal').append('<input type="hidden" name="elaboracao" ' +
            'value="0" />');

        $('#modalAtivarTermo').modal('show');
    }



</script>

<style>
    /*.modal-body {*/
    /*    background-color: green;*/
    /*}*/

    form {
        display: flex;
        flex-direction: column;
    }

    fieldset.form-group {
        /*list-style-type: '✔ ' !important;*/
        display: block !important;
        /*background-color: red;*/
    }

    .form-group {
        margin-bottom: 15px !important;
    }

    label.control-label {
        margin-bottom: 5px !important;
    }

    .modal-body input.form-control {
        /*display: block !important;*/
        width: 100% !important;
    }


</style>
{{--<style media="screen">
    * {
        box-sizing: border-box;
        margin: 0;
        padding: 0;
    }
</style>--}}
{{--@endpush--}}
