@if ($crud->hasAccess('delete'))
    <a href="javascript:void(0)"
       onclick="deleteEntry(this)"
       data-route="{{ url($crud->route.'/'.$entry->getKey()) }}"
       data-id="{{ $entry->getKey() }}"
       class="btn btn-xs btn-default default-delete-button"
       data-button-type="delete"
       title="{{ trans('backpack::crud.delete') }}">
        <i class="fa fa-trash"></i>
    </a>
@endif

<script>
    if (typeof deleteEntry != 'function') {
        $("[data-button-type=delete]").unbind('click');

        function deleteEntry(button) {
            var button = $(button);
            var route = button.attr('data-route');
            var id = button.attr('data-id');  // Acessando o ID do Contratoarquivo

            var row = $("#crudTable a[data-route='" + route + "']").closest('tr');

            if (confirm("{{ trans('backpack::crud.delete_confirm') }}") == true) {
                $.ajax({
                    url: route,
                    type: 'DELETE',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "id": id  // Enviando o ID do Contratoarquivo na requisição
                    },
                    success: function (result) {
                        if (result.code == 200) {
                            // Show an alert with the result
                            new PNotify({
                                title: "{{ trans('backpack::crud.delete_confirmation_title') }}",
                                text: "{{ trans('backpack::crud.delete_confirmation_message') }}",
                                type: "success"
                            });

                            // Hide the modal, if any
                            $('.modal').modal('hide');

                            // Remove the details row, if it is open
                            if (row.hasClass("shown")) {
                                row.next().remove();
                            }

                            // Remove the row from the datatable
                            row.remove();
                        } else {
                            // Caso contrário, exiba uma mensagem de erro
                            new PNotify({
                                title: "Erro",
                                text: result.message || "Ocorreu um erro ao excluir o item.",
                                type: "error"
                            });
                        }
                    },
                    error: function (result) {
                        // Show an alert with the result
                        new PNotify({
                            title: "{{ trans('backpack::crud.delete_confirmation_not_title') }}",
                            text: result.responseJSON?.message || "{{ trans('backpack::crud.delete_confirmation_not_message') }}",
                            type: "warning"
                        });
                    }
                });
            } else {
                // Show an alert telling the user we don't know what went wrong
                new PNotify({
                    title: "{{ trans('backpack::crud.delete_confirmation_not_deleted_title') }}",
                    text: "{{ trans('backpack::crud.delete_confirmation_not_deleted_message') }}",
                    type: "info"
                });
            }
        }
    }
</script>
