@if( $entry->Status->descricao != 'Enviado' && $entry->getPdfGerado() != 1)
<a href="{{ url($crud->route) .'/'. $entry->getKey().'/edit'}}" class="btn btn-xs btn-default" title="Editar Minuta de Documento"><i class="fa fa-edit"></i></a>
@endif
