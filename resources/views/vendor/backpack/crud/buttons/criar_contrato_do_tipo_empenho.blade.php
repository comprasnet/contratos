@php
    $arrMinutas = $crud->arrMinutasModal;
   // dd($arrMinutas);
@endphp
<button
    type="button"
    class="btn btn-primary ladda-button"
    data-toggle="modal"
    data-target="#modal-lista-minutas-empenho-forca-contrato"
    onclick="abrirModalCriarContratoTipoEmpenho(this)"
>
    <span class="ladda-label"><i class="fa fa-plus"></i> Criar Contratos do tipo Empenho</span>
</button>

@include('vendor.backpack.crud.modal.lista_minutas_empenho_forca_contrato',
    [
        "arrMinutas" => $arrMinutas
    ]
)


@push('after_scripts')
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script type="text/javascript">
        function abrirModalCriarContratoTipoEmpenho() {
            var modalCriarContratoTipoEmpenho = $('#modal-lista-minutas-empenho-forca-contrato');

            modalCriarContratoTipoEmpenho.toggle();
        }
    </script>
@endpush
