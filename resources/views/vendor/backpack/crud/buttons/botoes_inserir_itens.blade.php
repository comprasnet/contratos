<div id="div_botoes_inserir_itens">
    <div class="" id="div_inserir_item">
        <button type="button" class="btn btn-primary pull-left" data-toggle="modal"
                data-target="#inserir_item" hidden>
            Inserir Item <i class="fa fa-plus"></i>
        </button>
    </div>
    <div class="" id="div_inserir_item2">
        <button type="button" class="btn btn-primary pull-left" data-toggle="modal"
                data-target="#inserir_item_compra" hidden
                style="margin-left: 15px;">
            Buscar itens da Compra <i class="fa fa-plus"></i>
        </button>
    </div>
</div>
