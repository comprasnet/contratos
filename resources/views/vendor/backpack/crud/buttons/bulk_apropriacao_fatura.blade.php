<!--PERMITIDO APENAS PARA ADMINISTRADOR ATÉ HOMOLOGAÇÃO-->
@if (backpack_user()->can('apropriar_fatura'))

    @include('vendor.backpack.base.mod.apropriacao.modal-confirmacao-apropriacao',
                    [
                        //propriedades do botao
                        'idBtn' => "apropriar-fatura-bottom",
                        'name' => "apropriar-fatura-bottom",
                        'hint' => 'Apropriação de faturas',
                        'route' => false,
                        'cor' => false,
                        'disabled' => false,
                        'icon' => false,
                        'class' => 'btn btn-primary btn-sm bulk-button',
                        'idContrato' => false,
                        'idFatura' => false,
                        //identifica origem do botao
                        'btnApropriarBottom' => true
                     ])

@endif
