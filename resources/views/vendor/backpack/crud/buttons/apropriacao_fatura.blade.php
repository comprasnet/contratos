@php
    $faturaApropriada = isset($entry->contratofaturas_id);
    $hint = 'Apropriação de fatura';
    $link = route('apropriacao.fatura.create', ['contrato' => $entry->contrato_id, 'id' => $entry->getKey()]);
    $disabled = '';
    $id_contrato_fatura = $entry->getKey();

$backgroundColor = '';
    if($entry->situacao === 'AND'){
        $backgroundColor = '#3c8dbc';
    }
    if($entry->situacao === 'ENV'){
        $backgroundColor = '#00a65a';
    }
@endphp

@if (backpack_user()->can('apropriar_fatura'))

    @if($entry->apropriacoes->count() > 0)

        @php
        $title = $entry->situacao === 'APR' ? 'Apropriações' : 'Apropriação em Andamento';
        @endphp
        <a
            href="/instrumentocobranca/{{$entry->id}}/apropriacao"
            class="btn btn-default ladda-button btn-xs"
            data-style="zoom-in"
            title="{{ $title }}"
            style="background-color: {{ $backgroundColor }}"
        >
            <span class="ladda-label"><i
                    class="fa fa-file-text-o"></i></span>
        </a>

    @elseif($entry->situacao === 'PEN' || $entry->situacao === 'ERR')
        @include('vendor.backpack.base.mod.apropriacao.modal-confirmacao-apropriacao',
                    [
                        /*propriedades do botao*/
                        'idBtn' => "$id_contrato_fatura",
                        'name' => "apropriar_fatura_line",
                        'hint' => $hint,
                        'route' => $link,
                        'disabled' => $disabled,
                        'icon' => true,
                        'class' => "btn btn-xs btn-default",
                        'idContrato' => $entry->contrato_id,
                        'idFatura' => $id_contrato_fatura,
                        /*identifica origem do botao*/
                        'btnApropriarBottom' => false
                     ])
    @endif
@endif
