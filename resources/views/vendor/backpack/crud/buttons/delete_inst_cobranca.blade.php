@php
//dd($entry);
@endphp

@if ($crud->hasAccess('delete'))
    @if($entry->situacao === 'PEN')
        <a href="javascript:void(0)" onclick="deleteEntry(this)"
           data-route="{{ url($crud->route.'/'.$entry->getKey()) }}" class="btn btn-xs btn-default"
           data-button-type="delete" title="{{ trans('backpack::crud.delete') }}"><i class="fa fa-trash"></i>
        </a>
    @else
        <a href="javascript:void(0)"
           data-toggle="modal"
           data-target="#modal-info-exclusao-inst-fatura"
           class="btn btn-xs btn-default"
           data-button-type="delete" title="{{ trans('backpack::crud.delete') }}"><i class="fa fa-trash"></i>
        </a>
    @endif
@endif

<div class="modal fade" id="modal-info-exclusao-inst-fatura" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Exclusão não permitida</h4>
            </div>
            <div class="modal-body">
                <p>Apenas faturas com status 'Pendente' poderão ser excluídas</p>
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>

</div>

<script>
    if (typeof deleteEntry != 'function') {
        $("[data-button-type=delete]").unbind('click');

        function deleteEntry(button) {
            // ask for confirmation before deleting an item
            // e.preventDefault();
            var button = $(button);
            var route = button.attr('data-route');
            var row = $("#crudTable a[data-route='"+route+"']").closest('tr');

            if (confirm("{{ trans('backpack::crud.delete_confirm') }}") == true) {
                $.ajax({
                    url: route,
                    type: 'DELETE',
                    success: function(result) {

                        if(result.status === 'error'){
                            $('#modal-info-exclusao-inst-fatura').modal('show')
                            return;
                        }
                        // Show an alert with the result
                        new PNotify({
                            title: "{{ trans('backpack::crud.delete_confirmation_title') }}",
                            text: "{{ trans('backpack::crud.delete_confirmation_message') }}",
                            type: "success"
                        });

                        // Hide the modal, if any
                        $('.modal').modal('hide');

                        // Remove the details row, if it is open
                        if (row.hasClass("shown")) {
                            row.next().remove();
                        }

                        // Remove the row from the datatable
                        row.remove();
                    },
                    error: function(result) {
                        // Show an alert with the result
                        new PNotify({
                            title: "{{ trans('backpack::crud.delete_confirmation_not_title') }}",
                            text: "{{ trans('backpack::crud.delete_confirmation_not_message') }}",
                            type: "warning"
                        });
                    }
                });
            } else {
                // Show an alert telling the user we don't know what went wrong
                new PNotify({
                    title: "{{ trans('backpack::crud.delete_confirmation_not_deleted_title') }}",
                    text: "{{ trans('backpack::crud.delete_confirmation_not_deleted_message') }}",
                    type: "info"
                });
            }
        }
    }

</script>
