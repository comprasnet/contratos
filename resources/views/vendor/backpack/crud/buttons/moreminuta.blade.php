@php
    $url =  $url2 =  $url3 = $url4 = '#';
    $disabled = 'disabled';
    $disabled_forcacontrato = 'disabled';
    $título_forcaempenho = $entry->minutaempenho_forcacontrato == 'não' ? 'Definir substitutivo de contrato' : 'Remover substitutivo de contrato';
    $btnForca = '';

    if ($entry->situacao_descricao === 'EMPENHO EMITIDO') {
        $url  = "/empenho/minuta/{$entry->getKey()}/alteracao";
        $url2 = "/empenho/minuta/{$entry->getKey()}/pdf";
        $url4 = "/empenho/minuta/{$entry->getKey()}/alteracaoFonte";
        $disabled = '';

        if ($entry->empenho_por === "Compra" ) {
            if ($entry->vinculadaContratoEmpenhoPivot() || $entry->temContratoempenhoRelacionado() ) {
                $disabled_forcacontrato = 'disabled';
            } else {
                $disabled_forcacontrato = '';
                $url3 = "/empenho/minuta/{$entry->getKey()}/toogleforcacontrato";
                $btnForca = 'btnForca';
            }
        }
    }

@endphp
<div class="btn-group">
    <button type="button" title="Mais" class="btn btn-xs btn-default dropdown-toggle dropdown-toggle-split"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Mais"><i class="fa fa-gears"></i>
        <span class="caret"></span>
        <span class="sr-only">Toggle Dropdown</span>
    </button>
        <ul class="dropdown-menu dropdown-menu-right">
            <li class="{{$disabled}}">
                <a href="{{$url}}" {{$disabled}}>&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-angle-right"></i>Alterar Empenho</a>
            </li>
            <li class="{{$disabled}}">
                <a href="{{$url4}}" {{$disabled}}>&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-angle-right"></i>Alterar Fonte</a>
            </li>
            @if(backpack_user()->hasRole('Administrador') || backpack_user()->hasRole('Execução Financeira'))
                <li class="{{$disabled}}">
                    <a href="{{$url2}}" {{$disabled}}>&nbsp;&nbsp;&nbsp;
                        <i class="fa fa-angle-right"></i>PDF Empenho</a>
                </li>
            @endif
            <li class="{{$disabled_forcacontrato}}">
                <a href="{{$url3}}" {{$disabled_forcacontrato}} class="{{$btnForca}}">&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-angle-right"></i>{{$título_forcaempenho}}</a>
            </li>
            @if(backpack_user()->can('compra_redirecionar') )
                <li>
                    <a href="/admin/compra/{{$entry->compra_id}}" >&nbsp;&nbsp;&nbsp;
                        <i class="fa fa-tag"></i>Compra</a>
                </li>
            @endif
            @if(isset($entry->contrato_id) && backpack_user()->can('contrato_redirecionar'))
                <li>
                    <a href="/gescon/contrato/{{$entry->contrato_id}}" >&nbsp;&nbsp;&nbsp;
                        <i class="fa fa-file-text-o"></i>Contrato</a>
                </li>
            @endif
        </ul>

</div>

<script src="{{ asset('vendor/sweetalert/2.10.16.11') }}/sweetalert2.all.min.js?time={{microtime()}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.btnForca').on('click', function () {
            Swal.fire({
                position: 'top-end',
                icon: 'warning',
                title: 'Aguarde, processando a informação, a tela será atualizada ao final do processamento!',
                showConfirmButton: false,
                allowOutsideClick: false
            })
        });
    });
</script>

