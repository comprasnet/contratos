
<div class="btn-group">
    <button type="button" title="Mais" class="btn btn-xs btn-default dropdown-toggle dropdown-toggle-split"
            data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false" title="Mais"><i class="fa fa-gears"></i>
        <span class="caret"></span>
        <span class="sr-only">Toggle Dropdown</span>
    </button>
    <ul class="dropdown-menu dropdown-menu-right">
        @if($entry->tipo->descricao !== 'Empenho')
            <li class="dropdown-header"><i class="fa fa-list"></i> Itens Contrato:</li>
            <li><a href="/gescon/contrato/{{$entry->getKey()}}/arquivos">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Arquivos</a></li>
            @if((!$entry->elaboracao) )
                <li><a href="/gescon/contrato/{{$entry->getKey()}}/cronograma">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Cronograma</a></li>
                <li><a href="/gescon/contrato/{{$entry->getKey()}}/despesaacessoria">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Despesas Acessórias</a></li>
                @if( !$entry->verificarSePossuiDomicilioBancario() )
                    <li><a href="/gescon/contrato/{{$entry->getKey()}}/domiciliobancario">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Domicílio Bancário</a></li>
                @endif
                <!-- <li><a href="/gescon/contrato/{{$entry->getKey()}}/contratocontas">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Conta-Depósito Vinculada</a></li> -->

                <li><a href="/gescon/contrato/{{$entry->getKey()}}/empenhos">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Empenhos</a></li>
                <li><a href="/gescon/contrato/{{$entry->getKey()}}/historico">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Histórico</a></li>
                <li><a href="/gescon/contrato/{{$entry->getKey()}}/garantias">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Garantias</a></li>
                <li><a href="/gescon/contrato/{{$entry->getKey()}}/itens">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Itens</a></li>
            @endif
            @if(backpack_user()->hasRole('Administrador') || (backpack_user()->hasRole('Setor Contratos') && \App\Models\Orgao::permissaoSei()))
                <li><a href="/gescon/contrato/{{$entry->getKey()}}/minutas">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Minutas de Documentos</a></li>
            @endif
            @if( $entry->verificarSePossuiDomicilioBancario() )
                <li><a href="/gescon/contrato/{{$entry->getKey()}}/operacaocredito">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Operação de Crédito - AntecipaGov </a></li>
            @endif
            @if((!$entry->elaboracao) )
                <li><a href="/gescon/contrato/{{$entry->getKey()}}/padrao">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Padrões DH SIAFI</a></li>
                <li><a href="/gescon/contrato/{{$entry->getKey()}}/parametros">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Parâmetros</a></li>
                @if(backpack_user()->can('contrato_arquivos_pncp_visualizar') )
                    <li><a href="/gescon/contrato/{{$entry->getKey()}}/arquivos-pncp">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>PNCP Arquivos</a></li>
                @endif
                <li><a href="/gescon/contrato/{{$entry->getKey()}}/prepostos">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Prepostos</a></li>
                <li><a href="/gescon/contrato/{{$entry->getKey()}}/publicacao">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Publicações</a></li>
                <li><a href="/gescon/contrato/{{$entry->getKey()}}/responsaveis">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Responsáveis</a></li>
                <li><a href="/gescon/contrato/{{$entry->getKey()}}/status">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Status</a></li>
                <li><a href="/gescon/contrato/{{$entry->getKey()}}/contratounidadedescentralizada">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Unidades Descentralizadas</a></li>

                @if(         backpack_user()->hasRole('Administrador')
                            or backpack_user()->hasRole('Setor Contratos')
                            or backpack_user()->hasRole('Administrador Suporte')
                     )
                    <li><a href="/gescon/contrato/{{$entry->getKey()}}/contratocontas">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Conta-Depósito Vinculada</a></li>
                    <li><a href="/gescon/contrato/{{$entry->getKey()}}/faturas">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Instrumentos de Cobrança</a></li>
                    <li><a href="/gescon/contrato/{{$entry->getKey()}}/localexecucao">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Local de Execução</a></li>
                    <li><a href="/gescon/meus-contratos/{{$entry->getKey()}}/ocorrencias">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Ocorrências</a></li>
                    <li><a href="/gescon/contrato/{{$entry->getKey()}}/terceirizados">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Terceirizados</a></li>
                @endif
                <li class="dropdown-header"><i class="fa fa-edit"></i> Modificar Contrato:</li>
                <li><a href="/gescon/contrato/{{$entry->getKey()}}/instrumentoinicial">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Instrumento Inicial</a></li>
                <li><a href="/gescon/contrato/{{$entry->getKey()}}/aditivos">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Termo Aditivo</a></li>
                <li><a href="/gescon/contrato/{{$entry->getKey()}}/apostilamentos">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Termo Apostilamento</a></li>
                <li><a href="/gescon/contrato/{{$entry->getKey()}}/rescisao">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Termo Rescisão</a></li>
                <!-- #795 -->
                @if(
                    backpack_user()->hasRole('Administrador') or backpack_user()->hasRole('Setor Contratos') or
                    backpack_user()->hasRole('Administrador Órgão') or backpack_user()->hasRole('Administrador Unidade') or
                    backpack_user()->hasRole('Administrador Suporte')
                )
                    <li><a href="/gescon/contrato/{{$entry->getKey()}}/encerramento">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Relatório Final</a></li>
                @endif
            @endif
        @endif
        @if($entry->tipo->descricao === 'Empenho')
            <li class="dropdown-header"><i class="fa fa-list"></i> Itens Contrato:</li>
            @if(backpack_user()->hasRole('Almoxarifado') && backpack_user()->roles->count() === 1)
                <li><a href="/gescon/contrato/{{$entry->getKey()}}/faturas">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Instrumentos de Cobrança</a></li>
            @else
                <li><a href="/gescon/contrato/{{$entry->getKey()}}/arquivos">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Arquivos</a></li>
                @if((!$entry->elaboracao) )
                    <li><a href="/gescon/contrato/{{$entry->getKey()}}/cronograma">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Cronograma</a></li>

                    @if( $entry->verificarSePossuiDomicilioBancario() )
                        <li><a href="/gescon/contrato/{{$entry->getKey()}}/domiciliobancario">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Domicílio Bancário</a></li>
                    @endif
                    <li><a href="/gescon/contrato/{{$entry->getKey()}}/empenhos">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Empenhos</a></li>
                    <li><a href="/gescon/contrato/{{$entry->getKey()}}/historico">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Histórico</a></li>
                    <li><a href="/gescon/contrato/{{$entry->getKey()}}/itens">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Itens</a></li>
                    <li><a href="/gescon/contrato/{{$entry->getKey()}}/padrao">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Padrões DH SIAFI</a></li>
                    <li><a href="/gescon/contrato/{{$entry->getKey()}}/parametros">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Parâmetros</a></li>
                    <li><a href="/gescon/contrato/{{$entry->getKey()}}/prepostos">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Prepostos</a></li>
                    <li><a href="/gescon/contrato/{{$entry->getKey()}}/publicacao">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Publicações</a></li>
                    <li><a href="/gescon/contrato/{{$entry->getKey()}}/responsaveis">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Responsáveis</a></li>
                    <li><a href="/gescon/contrato/{{$entry->getKey()}}/contratounidadedescentralizada">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Unidades Descentralizadas</a></li>
                    @if( backpack_user()->hasRole('Administrador') or backpack_user()->hasRole('Setor Contratos') or backpack_user()->hasRole('Almoxarifado'))
                        <li><a href="/gescon/contrato/{{$entry->getKey()}}/faturas">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Instrumentos de Cobrança</a></li>
                        <li><a href="/gescon/contrato/{{$entry->getKey()}}/localexecucao">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Local de Execução</a></li>
                        <li><a href="/gescon/meus-contratos/{{$entry->getKey()}}/ocorrencias">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Ocorrências</a></li>
                    @endif
                    <li class="dropdown-header"><i class="fa fa-edit"></i> Modificar Contrato:</li>
                    <li><a href="/gescon/contrato/{{$entry->getKey()}}/instrumentoinicial">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Instrumento Inicial</a></li>
                @endif
            @endif
        @endif
        <!-- @if( backpack_user()->hasRole('Administrador') or backpack_user()->hasRole('Setor Contratos') )
            <li><a href="/gescon/meus-contratos/{{$entry->getKey()}}/faturas">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Faturas</a></li>
            <li><a href="/gescon/contrato/{{$entry->getKey()}}/contratocontas">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Conta-Depósito Vinculada</a></li>
        @endif -->
    </ul>
</div>

