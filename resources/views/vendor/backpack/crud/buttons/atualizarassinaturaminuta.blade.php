@if(($entry->origem != 2) == false )
<a href="{{url($crud->route)}}/atualizacaosituacaoarquivominuta/{{$entry->id}}" class="btn btn-default ladda-button btn-xs" data-style="zoom-in" title="Atualizar Situação">
    <span class="ladda-label"><i class="fa fa-refresh"></i></span>
</a>
@endif
