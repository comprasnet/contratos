<div class="btn-group">
    <button type="button" title="Mais" class="btn btn-xs btn-default dropdown-toggle dropdown-toggle-split"
            data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false" title="Mais"><i class="fa fa-gears"></i>
        <span class="caret"></span>
        <span class="sr-only">Toggle Dropdown</span>
    </button>
    <ul class="dropdown-menu dropdown-menu-right">
    <li><a href="/admin/unidade/{{$entry->getKey()}}/configuracao">&nbsp;&nbsp;&nbsp;<i class="fa fa-gear"></i>Configuração</a></li>
    <li><a href="/admin/unidade/{{$entry->getKey()}}/autoridadesignataria">&nbsp;&nbsp;&nbsp;<i class="fa fa-gear"></i>Autoridades Signatárias</a></li>
    </ul>
</div>
