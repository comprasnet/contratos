<a href="#" id="{{ $entry->id }}" onclick="abrirModalSicaf(this)" class="btn btn-xs btn-default"><i
        class="fa fa-database" aria-hidden="true" title="Consulta Situação Fornecedor"></i></a>

<div id="modalSicafFornecedor" tabindex="-1" class="modal fade" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="erro-modal form-group col-md-12">
                <h3 style="color: red;">Não foram encontrados dados para o fornecedor.</h3>
                <hr>
            </div>
            <div class="modal-header">
                <h3 class="modal-title">
                    Consulta Situação Fornecedor
                </h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="loading-div" style="display: flex;justify-content: center;align-items: center;flex-direction: column;height: 100%;margin: 5px">
                <div id="loading" class="fa fa-refresh fa-spin" style="font-size: 30px;"></div>
                Carregando ...
            </div>

            <form method="post" action="" accept-charset="UTF-8" id="form_modal">
                {!! csrf_field() !!}
                <div class="modal-body">

                    <form>
                        <div class="form-group col-md-4">
                            <div class="row">

                                <label for="PGFN" class="col-form-label">Receita Federal e PGFN</label>
                            </div>
                            <div class="row">

                                <input type="text" class="form-control" id="PGFN" disabled="disabled">
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <div class="row">

                                <label for="FGTS" class="col-form-label">Certidão FGTS</label>
                            </div>
                            <div class="row">

                                <input type="text" class="form-control" id="FGTS" disabled="disabled">
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <div class="row">

                                <label for="trabalhista" class="col-form-label">Certidão Trabalhista</label>
                            </div>
                            <div class="row">

                                <input type="text" class="form-control" id="trabalhista" disabled="disabled">
                            </div>
                        </div>
                        <div class="form-group col-md-6 has-feedback">
                            <div class="row">

                                <label for="estadual_distrital" class="col-form-label">Receita
                                    Estadual/Distrital</label>
                            </div>
                            <div class="row">
                                <div class="input-group mb-4">
                                    <input type="text" class="form-control" id="estadual_distrital"
                                        disabled="disabled">
                                    <span class="input-group-addon transparent hide" id='hide_estadual'>
                                        <a id="estadual_download" target="__blank"><i class="fa fa-download"
                                                aria-hidden="true"></i></a>
                                    </span>
                                </div>

                            </div>
                        </div>
                        <div class="form-group col-md-6 has-feedback">
                            <div class="row">

                                <label for="municipal" class="col-form-label">Receita Municipal</label>
                            </div>
                            <div class="row">
                                <div class="input-group mb-4">
                                    <input type="text" class="form-control" id="municipal" disabled="disabled">
                                    <span class="input-group-addon transparent hide" id='hide_municipal'>
                                        <a target="__blank" id="municipal_download"><i class="fa fa-download"
                                                aria-hidden="true"></i></a>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">

                </div>
            </form>
        </div>
    </div>
</div>
<style>

</style>
<script>
    function abrirModalSicaf(element) {
        $('#modalSicafFornecedor').modal('show');
        $('.erro-modal').hide()

        $('#modalSicafFornecedor').on('hidden.bs.modal', function (e) {
            $('#crudTable').DataTable().ajax.reload();
        });

        $('.loading-div').hide();
        $('form').hide();

        $.ajax({
            url: '/gescon/fornecedorsicaf',
            method: 'post',
            data: {
                'fornecedor_id': element.id
            },
            beforeSend: function(){
                $('.loading-div').show();
            },
            success: function(data) {
                let json = $.parseJSON(data);

                let datasSicaf = retornaDatasSicaf(json);

                $("#PGFN").val(datasSicaf.date_receita_federal_pgfn);
                $("#FGTS").val(datasSicaf.date_certidao_fgts);
                $("#trabalhista").val(datasSicaf.date_certidao_trabalhista);
                $("#estadual_distrital").val(datasSicaf.date_receita_estadual_distrital);
                $("#municipal").val(datasSicaf.date_receita_municipal);

                if (json.certidao.codigoArquivoRegFiscalEst) {
                    $('#hide_estadual').removeClass('hide');
                    $('#estadual_download').attr('href', "fornecedor/certidao/" + element.id + "/estadual");
                } else {
                    $('#hide_estadual').addClass('hide');
                }
                if (json.certidao.codigoArquivoRegFiscalMun) {
                    $('#hide_municipal').removeClass('hide');
                    $('#municipal_download').attr('href', "fornecedor/certidao/" + element.id +
                        "/municipal");
                } else {
                    $('#hide_municipal').addClass('hide');
                }

                if(json.certidao === false && json.sicaf !== 200){
                    $('.erro-modal').show()
                }
            },complete: function() {
                $('.loading-div').hide();
                $('form').show();
            }
        });
    }

    function formatDate(dateString) {
        if (!dateString) {
            return "Sem Informação";
        }

        let partesData = dateString.split("-");
        let ano = parseInt(partesData[0]);
        let mes = parseInt(partesData[1]) - 1;
        let dia = parseInt(partesData[2]);

        let data = new Date(Date.UTC(ano, mes, dia));
        let diaFormatado = data.getUTCDate().toString().padStart(2, "0");
        let mesFormatado = (data.getUTCMonth() + 1).toString().padStart(2, "0");
        let anoFormatado = data.getUTCFullYear();

        return `${diaFormatado}/${mesFormatado}/${anoFormatado}`;
    }

    function retornaDatasSicaf(json){
        let fields = [
            "date_receita_municipal",
            "date_receita_federal_pgfn",
            "date_certidao_trabalhista",
            "date_receita_estadual_distrital",
            "date_certidao_fgts"
        ];

        let jsonData = {};

        for (let field of fields) {
            jsonData[field] = formatDate(json.sicaf[field]);
        }

        return jsonData;
    }
</script>
