@php
    $routeRemoverApropriacao = route(
        'remover.apropriacao.fatura',
         [
             $entry->sfpadrao_id,
              $entry->apropriacoes_faturas_id,
              $entry->contratofaturas_id
         ]
         );
@endphp

@if($entry->apropriacao->getSituacaoApropriacao() === 'AND' || $entry->apropriacao->getSituacaoApropriacao() === 'ERR')
    <a
        class="btn btn-xs btn-default default-delete-button"
        href="{{$routeRemoverApropriacao}}"
        title="Remover Apropriação do Instrumento de Cobrança"
    >
        <i class="fa fa-trash">

        </i>
    </a>
@endif
