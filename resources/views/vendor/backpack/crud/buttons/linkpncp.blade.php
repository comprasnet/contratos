@if(!($entry->elaboracao))
    @if ($entry->getLinkPNCP()==='disabled')
        <a class="btn btn-xs btn-default disabled"><i class="fa fa-globe"></i></a>
    @else
        <a href="{{$entry->getLinkPNCP()}}" class="btn btn-xs btn-default"
           title="PNCP" target="_blank"><i class="fa fa-globe"></i></a>
    @endif
@endif
