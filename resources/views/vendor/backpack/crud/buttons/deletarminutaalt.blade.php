<a href="javascript:void(0)" onclick="deleteMinutaAlt(this, {{$entry->alteracao_fonte_minutaempenho_id}})"
   data-route="{{ url($crud->route) .'/'. $entry->minutaempenhos_remessa_id.'/deletarminuta'}}"
   class="btn btn-default ladda-button btn-xs" data-style="zoom-in" data-button-type="delete" title="Deletar Minuta">
    <span class="ladda-label"><i class="fa fa-trash"></i></span>
</a>
