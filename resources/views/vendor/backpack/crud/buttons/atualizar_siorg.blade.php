<h4>
    Órgão: {{$crud->orgao}}
</h4>
<button
    type="button"
    class="btn btn-primary ladda-button"
    onclick="updateSiorg(this)"
>
    <span class="ladda-label"><i class="fa fa-plus"></i> Atualizar Estrutura Organizacional</span>
</button>


@push('after_scripts')
    <script type="text/javascript">
        function updateSiorg(elem) {
            location.href = 'lista/atualizarestrutura';
        }

    </script>
@endpush
