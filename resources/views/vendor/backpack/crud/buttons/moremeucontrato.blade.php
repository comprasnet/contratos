<div class="btn-group">
    <button type="button" title="Mais" class="btn btn-xs btn-default dropdown-toggle dropdown-toggle-split"
            data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false" title="Mais"><i class="fa fa-gears"></i>
        <span class="caret"></span>
        <span class="sr-only">Toggle Dropdown</span>
    </button>
    <ul class="dropdown-menu dropdown-menu-right">
        <li class="dropdown-header"><i class="fa fa-list"></i> Itens Contrato:</li>

        <li><a href="/gescon/meus-contratos/{{$entry->getKey()}}/arquivos">&nbsp;&nbsp;&nbsp;<i
            class="fa fa-angle-right"></i>Arquivos</a></li>
        <li><a href="/gescon/contrato/{{$entry->getKey()}}/contratocontas">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Conta-Depósito Vinculada</a></li>
        <li><a href="/gescon/meus-contratos/{{$entry->getKey()}}/empenhos">&nbsp;&nbsp;&nbsp;<i
            class="fa fa-angle-right"></i>Empenhos</a></li>
        <li><a href="/gescon/meus-contratos/{{$entry->getKey()}}/faturas">&nbsp;&nbsp;&nbsp;<i
                    class="fa fa-angle-right"></i>Instrumentos de Cobrança</a></li>
        <li><a href="/gescon/meus-contratos/{{$entry->getKey()}}/ocorrencias">&nbsp;&nbsp;&nbsp;<i
                    class="fa fa-angle-right"></i>Ocorrências</a></li>
        <li><a href="/gescon/meus-contratos/{{$entry->getKey()}}/terceirizados">&nbsp;&nbsp;&nbsp;<i
                    class="fa fa-angle-right"></i>Terceirizados</a></li>
{{--        <li class="dropdown-header"><i class="fa fa-list"></i> Instrumento de Medição de Resultados:</li>--}}
{{--        <li><a href="/gescon/meus-contratos/{{$entry->getKey()}}/servicos">&nbsp;&nbsp;&nbsp;<i--}}
{{--                    class="fa fa-angle-right"></i>Serviços</a></li>--}}
        <!-- #795 -->
        @if( backpack_user()->hasRole('Administrador') or backpack_user()->hasRole('Responsável por Contrato') )
            <li><a href="/gescon/meus-contratos/{{$entry->getKey()}}/encerramento">&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>Relatório Final</a></li>
        @endif
        <li><a href="/gescon/meus-contratos/{{$entry->getKey()}}/faturas">&nbsp;&nbsp;&nbsp;<i
        class="fa fa-angle-right"></i>Instrumentos de Cobrança</a></li>


    </ul>
</div>
