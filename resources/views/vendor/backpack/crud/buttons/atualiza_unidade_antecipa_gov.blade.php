<a id="habilitar-gov" data-key="habilitar" href="#"
   class="btn btn-default ladda-button" data-style="zoom-in" title="Habilitar AntecipaGov ">
    <span class="ladda-label">Habilitar AntecipaGov</span>
</a>
<a id="desabilitar-gov" data-key="desabilitar" href="#"
   class="btn btn-default ladda-button" data-style="zoom-in" title="Desabilitar AntecipaGov ">
    <span class="ladda-label">Desabilitar AntecipaGov</span>
</a>
<br>
<br>
@push('after_scripts')
    <script src="{{ asset('vendor/sweetalert/2.10.16.11') }}/sweetalert2.all.min.js?time={{microtime()}}"></script>
    <script>
        $('#habilitar-gov, #desabilitar-gov').click(function (e) {
            let key = $(this).data('key');
            let ids = [];
            let tds = [];
            let tdsOrgao = [];
            let tdsNomeResumido = [];
            let indiceSISG, indiceSIAFI, indiceOrgao, indiceNomeResumido, indiceUGSIAFI;
            let sisg, siafi, orgao, nomeResumido, valorSisg, valorSiafi, valorOrgao, valorNomeResumido, ugSiafi,
                valorUgSiafi;

            //Loop para capturar o index das colunas sisg e SIAFI
            // pois elas podem variar dependendo da visualização da tabela
            $('#crudTable thead th').each(function (index) {
                let tituloColuna = $(this).text().trim();

                if (tituloColuna === 'Sisg') {
                    indiceSISG = index;
                } else if (tituloColuna === 'SIAFI') {
                    indiceSIAFI = index;
                } else if (tituloColuna === 'Órgão') {
                    indiceOrgao = index;
                } else if (tituloColuna === 'Nome Resumido') {
                    indiceNomeResumido = index;
                } else if (tituloColuna === 'UG SIAFI') {
                    indiceUGSIAFI = index;
                }
            });
            // Somando +1 ao index pois a contagem do each começa em 0
            // e a contagem abaixo das colunas pelo nth-child começa em 1
            indiceSISG += 1;
            indiceSIAFI += 1;
            indiceOrgao += 1;
            indiceNomeResumido += 1;
            indiceUGSIAFI += 1;

            // Salvando o valor das colunas sisg e SIAFI em um array
            // para as colunas que tiverem o checkbox marcado
            $('.crud_bulk_actions_row_checkbox').each(function () {
                if ($(this).is(':checked')) {
                    ids.push($(this).data('primary-key-value'));
                    if (!isNaN(indiceSISG)) {
                        sisg = $(this).closest('tr').find(`td:nth-child(${indiceSISG})`);
                        valorSisg = sisg.find("span").text().trim();
                    }
                    if (!isNaN(indiceSIAFI)) {
                        siafi = $(this).closest('tr').find(`td:nth-child(${indiceSIAFI})`);
                        valorSiafi = siafi.find("span").text().trim();
                    }
                    if (!isNaN(indiceOrgao)) {
                        orgao = $(this).closest('tr').find(`td:nth-child(${indiceOrgao})`);
                        valorOrgao = orgao.find("span").text().trim();
                    }

                    if (!isNaN(indiceNomeResumido)) {
                        nomeResumido = $(this).closest('tr').find(`td:nth-child(${indiceNomeResumido})`);
                        valorNomeResumido = nomeResumido.find("span").text().trim();
                    }

                    if (!isNaN(indiceUGSIAFI)) {
                        ugSiafi = $(this).closest('tr').find(`td:nth-child(${indiceUGSIAFI})`);
                        valorUgSiafi = ugSiafi.find("span").text().trim();
                    }

                    tds.push(valorSisg);
                    tds.push(valorSiafi);
                    tdsOrgao.push(valorOrgao);
                    tdsNomeResumido.push(valorUgSiafi + ' - ' + valorNomeResumido);
                }
            });

            // Se houver a combinação de valoes sisg = sim e utiliza siafi = sim
            // será apresentado a mensagem de confirmação caso o usuário
            // deseje desabilitar o antecipagov
            let isConfirm = tds.every(function (val) {
                if (val === undefined) {
                    Swal.fire("A visibilidade das colunas Sisg e SIAFI devem estar habilitadas para esta ação");
                }
                return val === 'Sim';
            });

            if (ids.length > 0) {
                if (isConfirm == true && key == 'desabilitar') {
                    // Verifica se a coluna órgão ou nome resumido estão visíveis para receber em ordem
                    // valor da coluna delas para informar o usuário quais unidades ele está modificando
                    // caso as duas colunas estejam ocultadas, será mostrado o id da unidade
                    let unidadesInformacao = retornValorColunaVisivel(tdsOrgao, tdsNomeResumido, ids);

                    Swal.fire({
                        title: "As unidades abaixo possuem configuração Sisg: Ativo e Siafi: Sim. Deseja desabilitar o AntecipaGov mesmo assim?",
                        text: unidadesInformacao,
                        showDenyButton: true,
                        showCancelButton: false,
                        confirmButtonText: "Salvar",
                        denyButtonText: "Cancelar"
                    }).then((result) => {
                        /* Read more about isConfirmed, isDenied below */
                        if (result.isConfirmed) {
                            $.ajax({
                                url: '/admin/atualiza_unidade_antecipa_gov/' + key + '/' + ids,
                                type: 'GET',
                                success: function (data) {
                                    location.reload();
                                },
                                error: function (xhr, status, error) {
                                    Swal.fire("Error!", "Ocorreu algum erro com a sua solicitação.", "");
                                }
                            });
                        }
                    });

                }

                if (key == 'habilitar' || (key == 'desabilitar' && isConfirm == false)) {
                    $.ajax({
                        url: '/admin/atualiza_unidade_antecipa_gov/' + key + '/' + ids,
                        type: 'GET',
                        success: function (data) {
                            location.reload();
                        },
                        error: function (xhr, status, error) {
                            Swal.fire("Error!", "Ocorreu algum erro com a sua solicitação.", "");
                        }
                    });
                }
            }
        });

        function retornValorColunaVisivel(tdsOrgao, tdsNomeResumido, ids) {

            //Remove os undefined do array caso haja quando esconder a coluna
            let tdsOrgaoFiltrado = tdsOrgao.filter(function (valor) {
                return valor !== undefined;
            });

            let tdsNomeResumidoFiltrado = tdsNomeResumido.filter(function (valor) {
                return valor !== undefined;
            });

            let idsFiltrado = ids.filter(function (valor) {
                return valor !== undefined;
            });

            if (tdsNomeResumidoFiltrado.length > 0) {
                return tdsNomeResumido;
            }

            if (tdsOrgaoFiltrado.length > 0) {
                return tdsOrgao;
            }

            if (idsFiltrado.length > 0) {
                return ids;
            }

            return [];

        }
    </script>
@endpush
