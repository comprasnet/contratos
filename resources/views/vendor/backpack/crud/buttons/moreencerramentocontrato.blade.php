<div class="btn-group">
    <button type="button" title="Mais" class="btn btn-xs btn-default dropdown-toggle dropdown-toggle-split"
            data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false" title="Mais"><i class="fa fa-gears"></i>
        <span class="caret"></span>
        <span class="sr-only">Toggle Dropdown</span>
    </button>
    <ul class="dropdown-menu dropdown-menu-right">
        <li><a href="/gescon/contrato/{{$entry->contrato_id}}/encerramento/gerarpdf">&nbsp;&nbsp;&nbsp;<i class="fa fa-indent"></i>Gerar PDF</a></li>
    </ul>
</div>
