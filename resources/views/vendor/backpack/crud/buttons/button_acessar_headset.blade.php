<button class="custom-button-headset" type="button" id="btn_acessar_suporte" target="_blank">
    <i class="fa fa-headphones"></i>
</button>

@push('after_scripts')
    <script type="text/javascript">
        $(document).ready(function($) {
            $('#btn_acessar_suporte').click(function () {
                window.open("https://portaldeservicos.gestao.gov.br/", '_blank');
            })
        })
    </script>
@endpush
