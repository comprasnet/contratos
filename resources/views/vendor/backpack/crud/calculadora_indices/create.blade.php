@extends('backpack::layout')

@section('header')
	<section class="content-header">
		<h1>
			<span class="text-capitalize">{!! $crud->getHeading() ?? $crud->entity_name_plural !!}</span>
			<small>{!! $crud->getSubheading() ?? trans('backpack::crud.add').' '.$crud->entity_name !!}.</small>
		</h1>

		<ol class="breadcrumb">
			<li><a href="{{ url(config('backpack.base.route_prefix'), 'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
			<li><a href="/gescon/tipoindices/calculadora/create" class="text-capitalize">{{ $crud->entity_name_plural }}</a></li>
			<li class="active">{{ trans('backpack::crud.add') }}</li>
		</ol>
	</section>
@endsection

@section('content')

@if ($crud->hasAccess('list'))
    <a href="{{ $novaRota }}" class="hidden-print"><i class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span>{{ $crud->entity_name_plural }}</span></a><br><br>
@endif

<div class="row m-t-20">
	<div class="{{ $crud->getCreateContentClass() }}">
		<!-- Default box -->

		@include('crud::inc.grouped_errors')
		  <form method="post"
		  		action="{{ url($crud->route) }}"
				@if ($crud->hasUploadFields('create'))
				enctype="multipart/form-data"
				@endif
		  		>
				{!! csrf_field() !!}
				<div class="col-md-12">

					<div class="row">
					<!-- load the view from the application if it exists, otherwise load the one in the package -->
					@if(view()->exists('vendor.backpack.crud.form_content'))
						@include('vendor.backpack.crud.form_content', [ 'fields' => $crud->getFields('create'), 'action' => 'create' ])
					@else
						@include('crud::form_content', [ 'fields' => $crud->getFields('create'), 'action' => 'create' ])
					@endif
					<div>
					{!! $resultado_calculo !!}
					</div>
					</div><!-- /.box-body -->
					<div class="row">
						@include('crud::inc.form_save_buttons')
					</div><!-- /.box-footer-->

				</div><!-- /.box -->
		  </form>
	</div>
</div>

@endsection
@push('after_scripts')
	<style>
		.resultado-calculo-wrap .valor-wrap,
		.resultado-calculo-wrap .result-label{
			display: inline-block;
			min-width:200px
		}
		.content-header small{
			display:none !important;
		}
		.resultado.calculo fieldset{
			border: 1px solid #ccc;
			margin-bottom: 2rem;
			background-color:#FFF
		}
		.resultado.calculo legend{
			margin-bottom: 1rem;
			padding: .5rem 1rem;
			font-weight: bold;
			font-size: 16px;
			background-color: #eee;
			display: inline-block;
			width: auto;
			margin-left: 2rem;
    		border: 1px solid #ccc;
		}
		.resultado.calculo ul{
			padding: 0 0 0 2rem
		}
		.resultado.calculo .resultado-calculo-wrap{
			padding: 2rem;
    		margin-bottom: 2rem;
		}
	</style>
    <script type="text/javascript">
        $(document).ready(function(){
			$('button.botao-salvar span.valor').text('Calcular');
			$('#saveActions a.btn.btn-default').hide();

			//$('#data-inicio .input-group.date').datepicker('update', '05/09/2022');

			//$('#data-inicio .input-group.date').datepicker( "option",  );
		});
	</script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.13.4/jquery.mask.min.js"></script>
    <script type="text/javascript">
		
        $(document).ready(function(){
			//$('#data-inicio .input-group.date').datepicker('setDate',new Date(2018, 5, 20));
			$('.valor-monetario input').mask("#.##0,00", {reverse: true});
		});
	</script>
@endpush
