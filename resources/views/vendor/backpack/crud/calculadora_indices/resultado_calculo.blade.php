
<div class='resultado calculo'>
    <fieldset class="col-sm-12">
        <legend>Resultado</legend>
        <!-- <div class="resultado-calculo-wrap">
        O valor <span class='badge badge-secondary'  id='valor-original'>R$ {{number_format($valor_original,2,',','.')}}</span> <sup><strong>(a)</strong></sup> terá um acréscimo/decréscimo de R$ <span class='badge badge-secondary'  id='valor-acrescimo-decrescimo'>{{number_format($valor_diferenca_reajuste,2,',','.')}}</span> e será reajustado para R$ <span class='badge badge-secondary'  id='valor-reajustado'>{{number_format($valor_reajustado,2,',','.')}}</span> <sup><strong>(b)</strong></sup> (<span style='cursor:pointer' id='percentual-reajuste' >{{$percentual_reajuste}}</span>% )  - Período de <span class='badge badge-secondary'  id='periodo-calculo-inicio'>{{$inicio}}</span> a <span class='badge badge-secondary'  id='periodo-calculo-fim'>{{$fim}}</span> <strong>(<span  id='qtde-meses'>{{$qtdeMeses ?? ''}}</span> meses)</strong> <sup><strong>(c)</strong></sup>
        </div> -->
        <div class="resultado-calculo-wrap" >
        <span id='tipo_indice' class="result-label"><strong>Índice:</strong></span>
            <span class="valor-wrap">
                <span>{{$tipoindice}}</span>
            </span></br> </br>
            <span id='periodo-calculo-inicio' class="result-label"><strong>Período:</strong></span>
            <span class="valor-wrap">
                <span>{{$inicio}}</span> a <span> {{$fim}} </span> - <span>{{$qtdeMeses ?? ''}}</span> 
                @if ($qtdeMeses <= '1') mês @else meses @endif 
            </span></br> </br>
            <span id='valor-original' class="result-label"><strong>Valor Inicial:</strong></span>
            <span class="valor-wrap">
                <span>R$ {{number_format($valor_original,2,',','.')}}</span>
            </span></br> </br>
            <span id='valor-reajustado' class="result-label"><strong>Valor Reajustado:</strong></span>
            <span class="valor-wrap">
                <span>R$ {{number_format($valor_reajustado,2,',','.')}}</span>
            </span>
            </br> </br>
            <span id='percentual-reajuste' class="result-label"><strong>Percentual de Reajuste:</strong></span>
            <span class="valor-wrap">
                <span>{{$percentual_reajuste}}</span>%
            </span>    
            </br> </br>
            <span id='fator_multiplicacao' class="result-label"><strong>Em fator de multiplicacão:</strong></span>
            <span class="valor-wrap">
                <span>{{$fator}}</span>
            </span>    
            </br> </br>
            <span id='valor-acrescimo-decrescimo' class="result-label"> <strong>Acréscimo/Decréscimo:</strong></span>
            <span class="valor-wrap">
                <span>R$ {{number_format($valor_diferenca_reajuste,2,',','.')}}</span>
            </span>

        </div>
    </fieldset>

    <fieldset class="col-sm-12">
        <legend>Memória de cálculo</legend>
        <div class="resultado-calculo-wrap">
        {!!$memoriaCalculo ?? ''!!}
        </div>
        <span class="result-label"> *Cálculo realizado conforme art. 5º do <a href="http://www.planalto.gov.br/ccivil_03/decreto/antigos/d1054.htm">Decreto n.º 1.054, de 1994 </a></span><br>        
    </fieldset>
       
</div>
