{{-- #722 --}}
@extends('backpack::layout')

@section('header')
    <section class="content-header">
        <h1>
            WSDL<small> Visualizar e baixar.</small>
        </h1>
        <div class="row m-b-10">
            <div class="col-xs-12 col-md-6">
                @if(backpack_user()->can('wsdl_atualizar'))
                    <br>
                    <div class="hidden-print with-border">
                        <a href="#" class="btn btn-default ladda-button" id="btnAtualizar" data-style="zoom-in"
                           title="Atualiza WSDL">
                            <span class="ladda-label">Atualizar <i class="fa fa-refresh"></i></span>
                        </a>
                    </div>
                @endif
            </div>

        </div>
    </section>
@endsection

@section('content')
    <!-- Default box -->
    <div class="box">
        <div class="box-body">
            <table class="table table-hover table-condensed">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{ trans('backpack::logmanager.file_name') }}</th>
                    <th>{{ trans('backpack::logmanager.date') }}</th>
                    <th>{{ trans('backpack::logmanager.last_modified') }}</th>
                    <th class="text-right">{{ trans('backpack::logmanager.file_size') }}</th>
                    <th>{{ trans('backpack::logmanager.actions') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($files as $key => $file)
                    <tr>
                        <th scope="row">{{ $key + 1 }}</th>
                        <td>{{ $file['file_name'] }}</td>
                        <td>{{ \Carbon\Carbon::createFromTimeStamp($file['last_modified'])->formatLocalized('%d %B %Y') }}</td>
                        <td>{{ \Carbon\Carbon::createFromTimeStamp($file['last_modified'])->formatLocalized('%H:%M') }}</td>
                        <td class="text-right">{{ round((int)$file['file_size']/1048576, 2).' MB' }}</td>
                        <td>
                            @if(backpack_user()->can('wsdl_baixar'))
                            <a class="btn btn-xs btn-default"
                               href="{{ backpack_url('wsdl/download/'.encrypt($file['file_name'])) }}">
                                <i class="fa fa-cloud-download"></i> {{ trans('backpack::logmanager.download') }}
                            </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div><!-- /.box-body -->
    </div><!-- /.box -->

@endsection

@section('after_scripts')
    <script>
        jQuery(document).ready(function ($) {

            $("#btnAtualizar").click(function (e) {
                e.preventDefault();
                var atualiza_url = "{{ route('setWsdl') }}";

                if (confirm("Tem certeza que deseja atualizar o WSDL?") == true) {
                    $.ajax({
                        url: atualiza_url,
                        type: 'GET',
                        data: {
                            _token: "<?php echo csrf_token(); ?>"
                        },
                        success: function (result) {
                            window.location.reload(true);
                        }
                    });
                } else {
                    new PNotify({
                        title: "{{ trans('backpack::logmanager.delete_cancel_title') }}",
                        text: "Os arquivos não foram atualizados.",
                        type: "info"
                    });
                }
            });
        });
    </script>
@endsection
