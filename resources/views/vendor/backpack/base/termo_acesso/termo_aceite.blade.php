@extends('backpack::layout_guest')

@section('content')
<div class="row m-t-40">
    <div class="col-md-4 col-md-offset-4">
        {{--<h3 class="text-center m-b-20">{{ trans('backpack::base.login') }}</h3>--}}
        <div class="login-logo">
            <a href="#">
                <img src="data:image/png;base64,<?php echo base64_encode(file_get_contents("../public/img/logo.png")) ?>" width="200px" alt="{!! env('APP_NAME') !!}"></a><br>
        </div>

        <div class="box">
            <div class="box-body">
                <form class="col-md-12" role="form" method="POST" action="{{ route('aceita.termo') }}">
                    {!! csrf_field() !!}

                    <h4 style=" text-align: justify"><p style="text-align: center; ">Prezado(a) Usuário(a),</p> 
                    Informamos que, a partir de 1º de novembro de 2024, o acesso no Sistema Contratos.gov.br será realizado exclusivamente por meio de login com conta de nível prata ou ouro do Gov.br.
                    Se ainda não possui uma conta no Gov.br, recomendamos que selecione a opção <b>"Entrar com Gov.br"</b> e siga as instruções disponíveis no site. Caso já tenha uma conta, sugerimos que comece a utilizá-la imediatamente para facilitar sua adaptação ao novo método de autenticação.</h4>
                    <hr class="m-t-30" />
                    <div class="form-group">
                        <div>
                            <button type="submit" class="btn btn-block btn-primary">
                                Ciente
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

@endsection