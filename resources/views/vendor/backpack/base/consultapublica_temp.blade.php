@extends('backpack::layout')

@section('header')
    <section class="content-header">
        <h1>
            Transparência
            <small>Contratos.gov.br</small>
        </h1>
@if((!is_null(session('user_ug'))))
        <ol class="breadcrumb">
            <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
            <li class="active">Transparência</li>
        </ol>
@endif
    </section>
@endsection

@section('content')
    <div class="row">
        <section class="col-lg-12 connectedSortable ui-sortable">
            <div class="box box-solid box-primary">
                <div class="box-header with-border">
                    <span class="glyphicon glyphicon-alert" aria-hidden="true"></span>
                </div>
                <div class="box-body">
                    <blockquote>
                        <p class="text-left">
                            O módulo Transparência está em reformulação e está temporariamente indisponível. Até que este módulo seja restaurado,
                            os dados de contratos de 2021 podem ser acessados no Portal de Dados Abertos: <a href="https://dados.gov.br/dataset/comprasnet-contratos" target="_blank">
                                Contratos - Portal Brasileiro de Dados Abertos</a> e os dados de contratos até 2020 podem ser acessados na API de Compras Governamentais:
                            <a href="http://compras.dados.gov.br/docs/lista-metodos-contratos.html" target="_blank">
                                Abertos - Compras Governamentais.</a>
                        </p>
                    </blockquote>
                </div>
             </div>
        </section>
    </div>
@endsection

@push('after_scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            //console.log('transparência temp');
        });
    </script>
@endpush
