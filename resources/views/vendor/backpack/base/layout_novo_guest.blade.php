<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('backpack::inc.head')
    <style>
        .custom-button {
            border: none;
            border-radius: 50px;
            padding: 8px 12px 8px 12px;
            font-family: inherit;
            font-weight: 600;
            display: flex;
            align-items: center;
            justify-content: center;
            cursor: pointer;
            outline: none;
            transition: background-color 0.3s, color 0.2s;
            font-size: 25px;
            height: 65px;
            width: 100%;
        }

        .custom-button-headset {
            border: none;
            margin-top: 16px;
            margin-right: 16px;
            border-radius: 50px;
            padding: 8px 12px 8px 12px;
            font-family: inherit;
            font-weight: 600;
            display: flex;
            align-items: center;
            justify-content: center;
            cursor: pointer;
            outline: none;
            transition: background-color 0.3s, color 0.2s;
            font-size: 25px;
            height: 65px;
        }

        .blue-button {
            background-color: #1251B4;
            color: white;
        }

        .blue-button:hover {
            background-color: #466eb5;
        }

        .white-button {
            background-color: #F8F8F8;
            color: #1251B4;
        }

        .white-button:hover {
            background-color: rgba(19, 81, 180, 0.16);
            color: #1a55d8;
        }

        .button-custom-login {
            font-size: 2.4rem;
            height: 6vh;
            width: 82%;
            max-width: 400px;
        }

        @media (max-width: 600px) {
            .button-custom-login {
                font-size: 1rem;
                height: 40px;
            }
        }

    </style>

    @yield('css_adicional')
    @stack('css_adicional')
</head>
<body class="hold-transition fixed" style="background-color: #ecf0f5">
    <!-- Site wrapper -->
    <div class="wrapper">
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper no-margin no-padding">

        <!-- Content Header (Page header) -->
         @yield('header')

        <!-- Main content -->
          <!-- Lado esquerdo (imagem) -->
          <div class="col-md-6" style="height: 100vh; padding-left: 0">
              <img
                  src="data:image/png;base64,<?= base64_encode(file_get_contents("../public/img/imagem-lateral-login.png")) ?>"
                  alt="Imagem" style="width: 100%; height: 100%; object-fit: cover;">
          </div>
          <!-- Lado direito (formulário) -->
          <div class="col-md-6 d-flex align-items-center justify-content-center" style="height: 93vh;overflow: hidden;">
              <div style="text-align: right; display: flex; justify-content: flex-end;">
                  @stack('area_botoes_superior_direito')
              </div>
              <div class="row" style="margin-top: 24vh">
                  <div class="col-md-6 col-md-offset-3">
                      <fieldset class="text-center">
                          <div class="form-group">
                              <img src="data:image/png;base64,<?php echo base64_encode(file_get_contents('../public/img/logo.png')) ?>"
                                   alt="{!! env('APP_NAME') !!}"
                                   style="width: 900px; object-fit: cover; max-width: 100%;">

                              <div class="mb-5 mt-5" style="text-align: right;font-size: 20px">
                                  Versão original
                              </div>
                          </div>

                          @yield('content')

                          <div class="button-container" style="display: flex; flex-direction: column; gap: 10px; align-items: center; justify-content: center;">
                              @yield('botoes')
                          </div>

                          <hr class="m-t-30" />

                          @yield('area_recuperar_senha')
                      </fieldset>
                  </div>
              </div>
          </div>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      <footer class="main-footer m-l-0 text-sm">
        @include('backpack::inc.footer')
      </footer>
    </div>
    <!-- ./wrapper -->


    @yield('before_scripts')
    @stack('before_scripts')

    @include('backpack::inc.scripts')
    @include('backpack::inc.alerts')

    @yield('after_scripts')
    @stack('after_scripts')

    <!-- JavaScripts -->
    {{-- <script src="{{ mix('js/app.js') }}"></script> --}}
</body>
</html>
