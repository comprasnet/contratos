<div class="collapse navbar-collapse pull-left" id="navbar-collapse">
    <ul class="nav navbar-nav">
        <!-- =================================================== -->
        <!-- ========== Top menu items (ordered left) ========== -->
        <!-- =================================================== -->

    @if (backpack_auth()->check())
        <!-- Topbar. Contains the left part -->
        @include('backpack::inc.topbar_left_content')
    @endif

    @if (config('backpack.base.setup_auth_routes'))
        @if (!backpack_auth()->guest())
            <!-- Topbar. Contains the right part -->
            @if((!is_null(session('user_ug'))))
                <li><a href="https://portaldeservicos.economia.gov.br/" target="_blank"><span><i class="fa fa-bug"></i> Abrir Chamado</a></li>
            @endif

            <li><a href="https://www.gov.br/compras/pt-br/acesso-a-informacao/manuais/comprasgovbr-contratos/manual-comprasgovbr-contratos.pdf" target="_blank"><span><i class="fa fa-book"></i> Manual</a></li>

            @if((!is_null(session('user_ug'))))
                <li><a href="{{ url('/mudar-ug') }}"><span><i class="fa fa-exchange"></i> Mudar UG/UASG</a></li>
            @endif
        @endif
    @endif

    <!-- ========== End of top menu left items ========== -->
    </ul>
</div>

<div class="navbar-custom-menu pull-right">
    <ul class="nav navbar-nav">
        <!-- ========================================================= -->
        <!-- ========= Top menu right items (ordered right) ========== -->
        <!-- ========================================================= -->

        @if (config('backpack.base.setup_auth_routes'))
            @if (backpack_auth()->guest())
                <li>
                    <a href="{{ url(config('backpack.base.route_prefix', 'admin').'/login') }}">{{ trans('backpack::base.login') }}</a>
                </li>
                @if (config('backpack.base.registration_open'))
                    <li><a href="{{ route('backpack.auth.register') }}">{{ trans('backpack::base.register') }}</a></li>
                @endif
            @else

            <li class="dropdown hover-dropdown">
                <a href="#" class="dropdown-toggle">
                    <i class="fa fa-tachometer"></i> Temas
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li class="dropdown-submenu hover-dropdown">
                        <a href="#"><i class="fa fa-font"></i> Alterar Fontes</a>
                        <ul class="dropdown-menu">
                            <li><a href="#" id="fontDefault"><i class="fa fa-check-circle"></i> Fonte Padrão</a></li>
                            <li><a href="#" id="fontDark"><i class="fa fa-check-circle"></i> Fonte Escura</a></li>
                        </ul>
                    </li>
                    <!-- Aqui da para colocar mais itens no futuro tipo o tema dark white -->
                </ul>
            </li>

                <li><a href="{{ route('inicio.meusdados') }}"><span><i class="fa fa-user-circle-o"></i> {{ trans('backpack::base.my_account') }}</span></a></li>

                {{--<li class="dropdown">--}}
                    {{--<a href="#" data-toggle="control-sidebar" title="Configurações"><i class="fa fa-gears"></i></a>--}}
                    {{--<li class="dropdown">--}}
                    {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Configurações"><i--}}
                            {{--class="fa fa-gears"></i> <span class="caret"></span></a>--}}
                    {{--<ul class="dropdown-menu" role="menu">--}}
                        {{--<li><a href="{{ route('inicio.meusdados') }}"><span><i class="fa fa-user-circle-o"></i> {{ trans('backpack::base.my_account') }}</span></a>--}}
                        {{--</li>--}}
                        {{--<li><a href="{{ url('/inicio/mudaug') }}"><span><i class="fa fa-exchange"></i> Mudar UG</a></li>--}}
                        {{--<li class="divider"></li>--}}
                        {{--<li><a href="{{ url('/logout') }}" id="logout" title="Sair da Aplicação"--}}
                        {{--onclick="event.preventDefault();--}}
                        {{--document.getElementById('logout-form').submit();">--}}
                        {{--Sair da Aplicação--}}
                        {{--</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                @include('backpack::inc.topbar_right_content')
                <li>
                    <a href="{{ route('backpack.auth.logout') }}"><i class="fa fa-btn fa-sign-out"></i>
                        {{ trans('backpack::base.logout') }}
                    </a>
                </li>
            @endif

            <li>
                <button type="button" href="#" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                    <i class="fa fa-ellipsis-h"></i>
                </a>
            </li>
        @endif

    <!-- ========== End of top menu right items ========== -->
    </ul>
</div>

<script>
document.addEventListener("DOMContentLoaded", function () {
    let currentFont = localStorage.getItem("userFont") || "rawline";
    applyFont(currentFont);

    document.getElementById("fontDefault").addEventListener("click", function () {
        localStorage.setItem("userFont", "original");
        applyFont("original");
    });

    document.getElementById("fontDark").addEventListener("click", function () {
        localStorage.setItem("userFont", "rawline");
        applyFont("rawline");
    });

    function applyFont(font) {
        if (font === "rawline") {
            document.body.style.fontFamily = "'Rawline', sans-serif";
        } else {
            document.body.style.fontFamily = "'Source Sans Pro', sans-serif";
        }
    }
});
</script>

<style>
.hover-dropdown:hover > .dropdown-menu {
    display: block;
}
.dropdown-submenu {
    position: relative;
}
.dropdown-submenu > .dropdown-menu {
    top: 0;
    left: 100%;
    margin-top: -5px;
    display: none;
}
.dropdown-submenu:hover > .dropdown-menu {
    display: block;
}
.dropdown-menu li a {
    display: flex;
    align-items: center;
    padding: 8px 15px;
    color: #333;
    text-decoration: none;
}
.dropdown-menu li a i {
    margin-right: 5px;
}
.dropdown-menu li a:hover {
    background-color: #F5F5F5;
}
</style>
