{{-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 --}}
{{-- ******************************************************************************** --}}
{{-- Início                                                                           --}}
{{-- ******************************************************************************** --}}
@if((!is_null(session('user_ug'))))
    <li>
        <a href="{{ backpack_url('inicio') }}">
            <i class="fa fa-home"></i>
            <span>Tela de início</span>
        </a>
    </li>
@endif
{{-- ******************************************************************************** --}}
{{-- Gestão contratual                                                                --}}
{{-- ******************************************************************************** --}}
@if((!is_null(session('user_ug'))))
    <li class="treeview">
        <a href="#">
            <i class="fa fa-file-text-o"></i>
            <span>Gestão contratual</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li>
                <a href="{{ backpack_url('/gescon/contrato?situacao=[1]') }}">
                    <i class="fa fa-file-text-o"></i>
                    <span>Contratos</span>
                </a>
            </li>
            <li>
                <a href="{{ backpack_url('/gescon/fornecedor') }}">
                    <i class="fa fa-users"></i>
                    <span>Fornecedores</span>
                </a>
            {{-- <li>                                                   --}}
            {{--    <a href="{{ backpack_url('/gescon/indicador') }}">  --}}
            {{--        <i class="fa fa-users"></i>                     --}}
            {{--        <span>Indicadores</span>                        --}}
            {{--    </a>                                                --}}
            {{-- </li> --}}
            <li>
                <a href="{{ backpack_url('/gescon/subrogacao') }}">
                    <i class="fa fa-copy"></i>
                    <span>Sub-rogações</span>
                </a>
            </li>
            @if(backpack_user()->hasRole('Administrador'))
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-download"></i>
                    <span>Importação SIASG</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ backpack_url('/gescon/siasg/contratos') }}">
                            <i class="fa fa-angle-right"></i>
                            <span>Contratos</span>
                        </a>
                    </li>
                </ul>
            <li>
            @endif
            {{-- ************************************************************************ --}}
            {{-- Consultas                                                                --}}
            {{-- ************************************************************************ --}}
            <li class="treeview">
                <a href="#">
                    <i class='fa fa-files-o'></i>
                    <span>Consultas</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ backpack_url('/gescon/consulta/arquivos') }}">
                            <i class="fa fa-file-o"></i>
                            <span>Arquivos</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ backpack_url('/gescon/consulta/cronogramas') }}">
                            <i class="fa fa-calendar"></i>
                            <span>Cronogramas</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ backpack_url('/gescon/consulta/despesasacessorias') }}">
                            <i class="fa fa-list"></i>
                            <span>Despesas acessórias</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ backpack_url('/gescon/consulta/empenhos') }}">
                            <i class="fa fa-money"></i>
                            <span>Empenhos</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ backpack_url('/gescon/consulta/faturas') }}">
                            <i class="fa fa-file-text-o"></i>
                            <span>Instrumentos de Cobrança</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ backpack_url('/gescon/consulta/garantias') }}">
                            <i class="fa fa-gift"></i>
                            <span>Garantias</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ backpack_url('/gescon/consulta/historicos') }}">
                            <i class="fa fa-history"></i>
                            <span>Históricos</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ backpack_url('/gescon/consulta/itens') }}">
                            <i class="fa fa-list-ul"></i>
                            <span>Itens</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ backpack_url('/gescon/consulta/ocorrencias') }}">
                            <i class="fa fa-check-square-o"></i>
                            <span>Ocorrências</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ backpack_url('/gescon/consulta/prepostos') }}">
                            <i class="fa fa-black-tie"></i>
                            <span>Prepostos</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ backpack_url('/gescon/consulta/responsaveis') }}">
                            <i class="fa fa-user-secret"></i>
                            <span>Responsáveis</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ backpack_url('/gescon/consulta/terceirizados') }}">
                            <i class="fa fa-users"></i>
                            <span>Terceirizados</span>
                        </a>
                    </li>
                </ul>
            </li>
            {{-- ************************************************************************ --}}
            {{-- Relatórios                                                               --}}
            {{-- ************************************************************************ --}}
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-file-text-o"></i>
                    <span>Relatórios</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ route('relatorio.listacontratosug') }}">
                            <i class="fa fa-file-text-o"></i>
                            <span>Contratos da UG</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('relatorio.listacontratosorgao') }}">
                            <i class="fa fa-file-text-o"></i>
                            <span>Contratos do Órgão</span>
                        </a>
                    </li>
                    {{-- <li>                                                       --}}
                    {{--    <a href="{{ route('relatorio.listatodoscontratos') }}"> --}}
                    {{--        <i class="fa fa-file-text-o"></i>                   --}}
                    {{--        <span>Todos os Contratos</span>                     --}}
                    {{--    </a>                                                    --}}
                    {{-- </li> --}}
                </ul>
            <li>
                <a href="{{ backpack_url('/gescon/meus-contratos') }}">
                    <i class="fa fa-file-text-o"></i>
                    <span>Meus Contratos</span>
                </a>
            </li>
            {{-- ************************************************************************ --}}
            {{-- Índices econômicos                                                       --}}
            {{-- ************************************************************************ --}}
            <li class="treeview">
                <a href="="#>
                    <i class="fa fa-dollar"></i>
                    <span>Índices econômicos</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ backpack_url('admin/indicevalores/') }}">
                        <i class="fa fa-search"></i>
                        <span>Consulta Série</span>
                        </a>
                    </li>
                    @if(!backpack_user()->hasRole('Desenvolvedor'))
                        <li>
                            <a href="{{ backpack_url('gescon/tipoindices/calculadora/create') }}">
                            <i class="fa fa-calculator"></i>
                            <span>Calculadora</span>
                            </a>
                        </li>
                    @endif
                </ul>
            </li>
        </ul>
    </li>
@endif

@if(backpack_user()->hasRole('Consulta') or backpack_user()->hasRole('Consulta Global') or backpack_user()->hasRole('Administrador') or backpack_user()->hasRole('Execução Financeira') or backpack_user()->hasRole('Administrador Órgão') or backpack_user()->hasRole('Administrador Unidade') or backpack_user()->hasRole('Administrador Suporte'))
    {{-- ******************************************************************************** --}}
    {{-- Gestão orçamentária                                                              --}}
    {{-- ******************************************************************************** --}}
    <li class="treeview">
        <a href="#">
            <i class="fa fa-dollar"></i>
            <span>Gestão orçamentária</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>

        <ul class="treeview-menu">
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-file-o"></i>
                    <span>Minuta empenho</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ route('empenho.crud./minuta.index', ['ano' => now()->year]) }}">
                            <i class="fa fa-file-o"></i>
                            <span>Minutas {{now()->year}}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('empenho.crud./minuta.index', ['ano' => 'anteriores']) }}">
                            <i class="fa fa-file-o"></i>
                            <span>Minutas de anos anteriores</span>
                        </a>
                    </li>
                </ul>
            </li>


            <li>
                <a href="{{ backpack_url('/execfin/empenho') }}">
                    <i class="fa fa-money"></i>
                    <span>Empenho</span>
                </a>
            </li>
            <li>
                <a href="{{ backpack_url('/execfin/restosapagar') }}">
                    <i class="fa fa-money"></i>
                    <span>Restos a Pagar</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class='fa fa-files-o'></i> <span>Consulta</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="treeview">
                        <a href="#">
                            <i class='fa fa-files-o'></i> <span>Fracionamento <br> de despesa</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li>
                                <a href="{{ backpack_url('/empenho/consulta/fracionamento-despesa-pdm') }}">
                                    <i class="fa fa-file-text-o"></i>
                                    <span>Material</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ backpack_url('/empenho/consulta/fracionamento-despesa-servico') }}">
                                    <i class="fa fa-file-text-o"></i>
                                    <span>Serviço</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
    </li>


    {{-- ******************************************************************************** --}}
    {{-- Execução financeira                                                              --}}
    {{-- ******************************************************************************** --}}
    <li class="treeview">
        <a href="#">
            <i class="fa fa-dollar"></i>
            <span>Gestão financeira</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>

        {{-- ************************************************************************ --}}
        {{-- Apropriação                                                              --}}
        {{-- ************************************************************************ --}}
        <ul class="treeview-menu">
            @if(backpack_user()->can('apropriar_folha') or backpack_user()->can('apropriar_fatura') or backpack_user()->hasRole('Consulta Global'))
            <li class="treeview">
                <a href="#">
                    <i class='fa fa-edit'></i> <span>Apropriação</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
{{--                    <li>--}}
{{--                        <a href="{{ backpack_url('/apropriacao/fatura') }}">--}}
{{--                            <i class="fa fa-file-text-o"></i>--}}
{{--                            <span>Fatura</span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
                    @if(backpack_user()->can('apropriar_folha'))
                    <li>
                        <a href="{{ backpack_url('/folha/apropriacao') }}">
                            <i class="fa fa-money"></i>
                            <span>Folha</span>
                        </a>
                    </li>
                    @endif
                    @if(backpack_user()->can('apropriar_fatura') or backpack_user()->hasRole('Consulta Global'))
                        <li>
                            <a href="{{ backpack_url('/gescon/fatura?situacao=PEN&emissao='.date('Y')) }}">
                                <i class="fa fa-file-text-o"></i>
                                <span>Instrumento Cobrança</span>
                            </a>
                        </li>
                    @endif
                </ul>
            </li>
            @endif
            {{-- ************************************************************************ --}}
            {{-- Cadastro                                                                 --}}
            {{-- ************************************************************************ --}}
{{--            <li class="treeview">--}}
{{--                <a href="#">--}}
{{--                    <i class='fa fa-edit'></i>--}}
{{--                    <span>Cadastro</span>--}}
{{--                    <i class="fa fa-angle-left pull-right"></i>--}}
{{--                </a>--}}
{{--                <ul class="treeview-menu">--}}

{{--                    <li>--}}
{{--                        <a href="{{ backpack_url('/execfin/situacaosiafi') }}">--}}
{{--                            <i class="fa fa-list-ol"></i>--}}
{{--                            <span>Situação SIAFI</span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li>--}}
{{--                        <a href="{{ backpack_url('/execfin/rhrubrica') }}">--}}
{{--                            <i class="fa fa-list-ol"></i>--}}
{{--                            <span>Rubrica</span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li>--}}
{{--                        <a href="{{ backpack_url('/execfin/rhsituacao') }}">--}}
{{--                            <i class="fa fa-list-ol"></i>--}}
{{--                            <span>RH - Situação</span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--            </li>--}}
            <!-- #598 - incluído Administrador -->
            @if(backpack_user()->hasRole('Execução Financeira') || backpack_user()->hasRole('Administrador'))
            <li>
                <!-- #598 - para que PEN já venha selecionado no filtro situação -->
                <a href="{{ backpack_url('/gescon/instrumento-de-cobranca?situacao=%5B%22PEN%22%5D') }}">
                    <i class="fa fa-file-o"></i>
                    <span>Instrumentos de Cobrança</span>
                </a>
            </li>
            @endif
        </ul>
    </li>
@endif
{{-- ******************************************************************************** --}}
{{-- Gestão de Atas                                                                   --}}
{{-- ******************************************************************************** --}}

    @if(!backpack_user()->hasRole('Almoxarifado'))
        @if(!backpack_user()->hasRole('Desenvolvedor'))
            <li>
                <a href="{{ backpack_url('autenticarautomatica/inicio') }}">
                    <i class="fa fa-file-o"></i>
                    <span>Gestão de atas</span>
                    <i class="fa fa-external-link pull-right"></i>
                </a>
            </li>
        @endif
    @endif


{{-- ******************************************************************************** --}}
{{-- Painéis                                                                          --}}
{{-- ******************************************************************************** --}}
{{--
<li class="treeview">
    <a href="#">
        <i class="fa fa-bar-chart"></i>
        <span>Painéis</span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li class="treeview">
            <a href="#">
                <i class='fa fa-edit'></i>
                <span>Orçamento e finanças</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <!--                <li><a href="{{ backpack_url('/painel/financeiro') }}"><i class="fa fa-money"></i> -->
                <!-- <span>Financeiro</span></a></li> -->
                <li>
                    <a href="{{ backpack_url('/painel/orcamentario') }}">
                        <i class="fa fa-money"></i>
                        <span>Orçamentário</span>
                    </a>
                </li>

            </ul>
        </li>
    </ul>
</li>
--}}

@if(backpack_user()->can('fiscalizacao_V2_acessar') || backpack_user()->hasRole('Consulta Global') || backpack_user()->hasRole('Almoxarifado'))
{{-- ******************************************************************************** --}}
{{-- Fiscalização e Gestão Contratual                                                 --}}
{{-- ******************************************************************************** --}}
<li>
    <a href="{{ backpack_url('autenticarautomatica/meus-contratos?situacao=[1]') }}" target="_blank">
        <i class="fa fa-indent"></i>
        <span>Fiscalização e</span><br>
        <span style="margin-left: 28px;">Gestão Contratual</span>
        <i class="fa fa-external-link pull-right"></i>
    </a>
</li>
@endif

{{-- ******************************************************************************** --}}
{{-- Transparência                                                                    --}}
{{-- ******************************************************************************** --}}
<li>
    <a href="{{ backpack_url('/transparencia') }}" target="_blank">
        <i class="fa fa-indent"></i>
        <span>Transparência</span>
        <i class="fa fa-external-link pull-right"></i>
    </a>
</li>

{{-- ******************************************************************************** --}}
{{-- Administração                                                                    --}}
{{-- ******************************************************************************** --}}
@if(backpack_user()->hasRole('Administrador') or backpack_user()->hasRole('Administrador Órgão') or backpack_user()->hasRole('Administrador Unidade') or backpack_user()->hasRole('Administrador Suporte') or backpack_user()->hasRole('Desenvolvedor') or backpack_user()->hasRole('Consulta Global'))
    <li class="treeview">
        <a href="#">
            <i class="fa fa-gears"></i>
            <span>Administração</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            @if(backpack_user()->hasRole('Administrador') or backpack_user()->hasRole('Administrador Órgão') or backpack_user()->hasRole('Administrador Unidade') or backpack_user()->hasRole('Administrador Suporte') or backpack_user()->hasRole('Consulta Global'))
                {{-- **************************************************************** --}}
                {{-- Estrutura                                                        --}}
                {{-- **************************************************************** --}}
                <li class="treeview">
                    <a href="#">
                        <i class='fa fa-bank'></i>
                        <span>Estrutura</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        @if(backpack_user()->hasRole('Administrador') or backpack_user()->hasRole('Administrador Suporte') or backpack_user()->hasRole('Consulta Global'))
                            <li>
                                <a href="{{ backpack_url('/admin/orgaosuperior') }}">
                                    <i class="fa fa-building"></i>
                                    <span>Órgão superior</span>
                                </a>
                            </li>
                        @endif
                        @if(backpack_user()->hasRole('Administrador') or backpack_user()->hasRole('Administrador Órgão') or backpack_user()->hasRole('Administrador Suporte') or backpack_user()->hasRole('Consulta Global'))
                            <li>
                                <a href="{{ backpack_url('/admin/orgao') }}">
                                    <i class="fa fa-building"></i>
                                    <span>Órgão</span>
                                </a>
                            </li>
                        @endif
                        @if(backpack_user()->hasRole('Administrador') or backpack_user()->hasRole('Administrador Órgão') or backpack_user()->hasRole('Administrador Unidade') or backpack_user()->hasRole('Administrador Suporte') or backpack_user()->hasRole('Consulta Global'))
                            <li>
                                <a href="{{ backpack_url('/admin/unidade') }}">
                                    <i class="fa fa-building"></i>
                                    <span>Unidade</span>
                                </a>
                            </li>
                        @endif
                        {{--                        @if(backpack_user()->hasRole('Administrador Unidade'))--}}
                        {{--                            <li><a href="{{ backpack_url('/admin/administradorunidade') }}"><i--}}
                        {{--                                        class="fa fa-building"></i>--}}
                        {{--                                    <span>Unidade Admin UG</span></a>--}}
                        {{--                            </li>--}}
                        {{--                        @endif--}}
                    </ul>
                </li>
            @endif
            @if(backpack_user()->hasRole('Administrador') or backpack_user()->hasRole('Administrador Órgão') or backpack_user()->hasRole('Administrador Unidade') or backpack_user()->hasRole('Administrador Suporte'))
                {{-- **************************************************************** --}}
                {{-- Acesso                                                           --}}
                {{-- **************************************************************** --}}
                <li class="treeview">
                    <a href="#">
                        <i class='fa fa-lock'></i>
                        <span>Acesso</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        @if(backpack_user()->hasRole('Administrador') or backpack_user()->hasRole('Administrador Suporte'))
                            <li>
                                <a href='{{ backpack_url('/admin/usuario') }}'>
                                    <i class='fa fa-user'></i>
                                    <span>Usuários</span>
                                </a>
                            </li>
                            <li>
                                <a href='{{ backpack_url('/admin/usuario-sistema') }}'>
                                    <i class='fa fa-terminal'></i>
                                    <span>Usuários de Sistema</span>
                                </a>
                            </li>
                        @endif
                        @if(backpack_user()->hasRole('Administrador Órgão') or backpack_user()->hasRole('Administrador Suporte'))
                            <li>
                                <a href='{{ backpack_url('/admin/usuarioorgao') }}'>
                                    <i class='fa fa-user'></i>
                                    <span>Usuários do meu órgão</span>
                                </a>
                            </li>
                        @endif
                        @if(backpack_user()->hasRole('Administrador Unidade') or backpack_user()->hasRole('Administrador Suporte'))
                            <li>
                                <a href='{{ backpack_url('/admin/usuariounidade') }}'>
                                    <i class='fa fa-user'></i>
                                    <span>Usuários da minha unidade</span>
                                </a>
                            </li>
                        @endif
                        @if(backpack_user()->hasRole('Administrador'))
                            <li>
                                <a href="{{ backpack_url('/role') }}">
                                    <i class="fa fa-group"></i>
                                    <span>Grupos</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ backpack_url('/permission') }}">
                                    <i class="fa fa-key"></i>
                                    <span>Permissões</span>
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif

            @if(backpack_user()->hasRole('Administrador') or backpack_user()->hasRole('Administrador Órgão') or backpack_user()->hasRole('Administrador Unidade') or backpack_user()->hasRole('Desenvolvedor'))
                {{-- **************************************************************** --}}
                {{-- Outros                                                           --}}
                {{-- **************************************************************** --}}
                <li class="treeview">
                    <a href="#">
                        <i class='fa fa-gear'></i>
                        <span>Outros</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        @if(backpack_user()->hasRole('Administrador'))
                            <li>
                                <a href="{{ backpack_url('/admin/ajusteminuta') }}"><i class="fa fa-gear"></i>
                                    <span>Ajuste de Minutas</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ backpack_url('/admin/ajusteremessas') }}"><i class="fa fa-gear"></i>
                                    <span>Ajuste de Remessas</span>
                                </a>
                            </li>
                            <li>
                                <a href='{{ backpack_url('/admin/amparolegal') }}'>
                                    <i class='fa fa-list'></i>
                                    <span>Amparo Legal</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ backpack_url('/admin/catmatseratualizacao') }}">
                                    <i class="fa fa-list"></i>
                                    <span>Atualização CatMatSer</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ backpack_url('/admin/ipsacesso') }}"><i class="fa fa-gear"></i>
                                    <span>Cadastro de IP's</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ backpack_url('/admin/sfcertificado') }}">
                                    <i class="fa fa-lock"></i>
                                    <span>Certificado SIAFI</span>
                                </a>
                            </li>
                            <li>
                                <a href='{{ backpack_url('/admin/codigo') }}'>
                                    <i class='fa fa-gear'></i>
                                    <span>Códigos e Itens</span>
                                </a>
                            </li>
                        @endif

                        @if(backpack_user()->hasRole('Administrador') or backpack_user()->hasRole('Desenvolvedor'))
                            <li><a href='{{ backpack_url('/admin/compra') }}'><i class='fa fa-tag'></i> <span>Compras</span></a></li>
                        @endif

                        @if(backpack_user()->hasRole('Administrador'))
                            <li>
                                <a href='{{ backpack_url('/admin/comunica') }}'>
                                    <i class='fa fa-envelope'></i>
                                    <span>Comunica</span>
                                </a>
                            </li>
                                <li>
                                    <a href='{{ backpack_url('/admin/devolveminutasiasg') }}'>
                                        <i class='fa fa-list'></i>
                                        <span>Devolve NE Siasg</span>
                                    </a>
                                </li>
                            <li>
                                <a href='{{ backpack_url('/gescon/encargo') }}'>
                                    <i class='fa fa-list'></i>
                                    <span>Encargo</span>
                                </a>
                            </li>
                            <li>
                                <a href='{{ backpack_url('/admin/feriado') }}'>
                                    <i class='fa fa-list'></i>
                                    <span>Feriado</span>
                                </a>
                            </li>
                            <li>
                                <a href='{{ backpack_url('/admin/sfhorarioexcecao') }}'>
                                    <i class='fa fa-list'></i>
                                    <span>Horários Exceção</span>
                                </a>
                            </li>
                            @if(!App\Http\Traits\Authorizes::bloquearInformacaoPHP())
                                <li>
                                    <a href="{{ backpack_url('/admin/infocontratos') }}">
                                        <i class="fa fa-info-circle"></i>
                                        <span>Informações PHP</span>
                                    </a>
                                </li>
                            @endif

                            <li>
                                <a href="{{ backpack_url('/admin/importacao') }}"><i class="fa fa-files-o"></i>
                                    <span>Importações</span>
                                </a>
                            </li>
                        @endif

                        @if(backpack_user()->hasRole('Administrador'))

                            <li>
                                <a href="{{ backpack_url('/admin/justificativafatura') }}">
                                    <i class="fa fa-list"></i>
                                    <span>Justificativa Inst. Cobrança</span>
                                </a>
                            </li>

                            <li>
                                <a href="{{ backpack_url('/admin/migration') }}">
                                    <i class="fa fa-database"></i>
                                    <span>Migration</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ backpack_url('/admin/modelodocumento') }}">
                                    <i class="fa fa-list"></i>
                                    <span>Padrões Minutas</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ backpack_url('/admin/padroespublicacao') }}">
                                    <i class="fa fa-list"></i>
                                    <span>Padrões Publicações</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ backpack_url('/admin/publicacoes') }}">
                                    <i class="fa fa-list"></i>
                                    <span>Publicações</span>
                                </a>
                            </li>
                        @endif

                        @if(backpack_user()->hasRole('Administrador') or backpack_user()->hasRole('Desenvolvedor'))
                            <li>
                                <a href="{{ backpack_url('/admin/pncp') }}">
                                    <i class="fa fa-globe"></i>
                                    <span>PNCP</span>
                                </a>
                            </li>
                        @endif

                        @if(backpack_user()->hasRole('Administrador'))
                            <li>
                                <a href="{{ backpack_url('/admin/situacaosiafi') }}">
                                    <i class="fa fa-list-ol"></i>
                                    <span>Situação SIAFI</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ backpack_url('/admin/termoacesso') }}">
                                    <i class="fa fa-list"></i>
                                    <span>Termos de aceite</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ backpack_url('/admin/tipoindices') }}">
                                    <i class="fa fa-gear"></i>
                                    <span>Tipos de Índice</span>
                                </a>
                            </li>

                            <li>
                                <a href="{{ backpack_url('/admin/tipolistafatura') }}">
                                    <i class="fa fa-list"></i>
                                    <span>Tipo Lista</span>
                                </a>
                            </li>
                                @if(backpack_user()->can('wsdl_listar'))
                                    <li>
                                        <a href="{{ backpack_url('/wsdl') }}">
                                            <i class="fa fa-list"></i>
                                            <span>WSDL</span>
                                        </a>
                                    </li>
                                @endif
                        @endif

                    </ul>
                </li>
                {{-- **************************************************************** --}}
                {{-- Logs                                                             --}}
                {{-- **************************************************************** --}}
                @if(backpack_user()->hasRole('Administrador') or backpack_user()->hasRole('Desenvolvedor'))

                    <li class="treeview">
                        <a href="#">
                            <i class='fa fa-terminal'></i>
                            <span>Logs</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">

                            <li>
                                <a href='{{ backpack_url('/admin/jobs') }}'>
                                    <i class='fa fa-list'></i>
                                    <span>Jobs</span>
                                </a>
                            </li>


                            <li>
                                <a href='{{ backpack_url('/admin/failedjobs') }}'>
                                    <i class='fa fa-list'></i>
                                    <span>Failed Jobs</span>
                                </a>
                            </li>


                            <li>
                                <a href='{{ url(config('backpack.base.route_prefix', 'admin').'/log') }}'>
                                    <i class='fa fa-terminal'></i>
                                    <span>Logs</span>
                                </a>
                            </li>
                            <li>
                                <a href='{{ backpack_url('/admin/activitylog') }}'>
                                    <i class='fa fa-terminal'></i>
                                    <span>Logs banco</span>
                                </a>
                            </li>

                        </ul>
                    </li>
                @endif
            @endif
        </ul>
    </li>
    <div style="border-top: 1px solid #2d3e43;margin-top: 15px"></div>
    <li style="margin-left: 15px;">
        <span style="color: white; font-size: 11px; white-space: pre-line;height: 100%;">
        {{ config('app.server_node') }} | v. {{ config('app.app_version') }}
        </span>
    </li>
@endif
