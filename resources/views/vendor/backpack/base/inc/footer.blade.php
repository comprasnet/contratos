@if (config('backpack.base.show_powered_by'))
    <div class="pull-right hidden-xs" style="font-size: 15px">
        {!! config('app.app_amb') !!} <br>
        {{ config('app.server_node') }} | v. {{ config('app.app_version') }}
    </div>
@endif
<br>
<div style="font-size: 15px">
    &nbsp;Copyright &copy; {{ date('Y') }} 
    <strong><a href="#">{{ env('APP_NAME') }}</a> </strong>- Todos direitos reservados. Software Livre (GPL).
</div>