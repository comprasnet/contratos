@php

@endphp
@extends('backpack::layout')

@section('header')
    <section class="content-header">
        <h1>
            Início
            <small>
                {{ $dataHoraAtualizacao ? 'Atualizado em: ' . $dataHoraAtualizacao : '' }}
            </small>
        </h1>

        @if(!empty($propertiesActivitylog))
            <br>
            <div class="box box-danger box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Erro na execução da migration</h3>
                    <div class="box-tools pull-right">
                        <!-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
                    </div>

                </div>

                <div class="box-body">
                    Migration: <br>
                    <b>{{$propertiesActivitylog['fileMigration']}}</b>. <br>
                    Mensagem de Erro: <br>
                    <b>{{$propertiesActivitylog['mensagemResumida']}}</b>
                </div>

            </div>
        @endif

        @if($diasCertificado)
            <br>
            <div class="box box-danger box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Faltam {{$diasCertificado}} dias para o certificado SIAFI expirar</h3>
                    <div class="box-tools pull-right">
                        <!-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
                    </div>

                </div>

                <div class="box-body">
                    <b>
                        O certificado do Siafi expirará em {{$diasCertificado}} dias, solicite à equipe técnica a renovação.
                        Mais detalhes em <a href="https://gitlab.com/comprasnet/infra/-/wikis/Certificado-SIAFI">https://gitlab.com/comprasnet/infra/-/wikis/Certificado-SIAFI</a> (página de acesso restrito)
                    </b>
                </div>

            </div>
        @endif

        @if((!is_null(session('user_ug'))))
        <ol class="breadcrumb">
            <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
            <li class="active">Início</li>
        </ol>
        @endif
    </section>
@endsection

@section('content')
    <!-- Linha 1 -->
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>{{$html['novoscontratos']}}</h3>

                    <p>
                        Novos contratos<br />
                        <small>
                            <em>(últimos 5 dias)</em>
                        </small>
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-ios-cloud-download"></i>
                </div>
                <a href="{{ backpack_url('/gescon/contrato?listar=novoscontratos') }}" class="small-box-footer">Ver contratos <i class="fa fa-arrow-circle-right"></i></a>


            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>{{$html['contratosvigentes']}}</h3>

                    <p>
                        Contratos vigentes<br />
                        <small>
                            <!-- <em>(últimos 5 dias)</em> -->
                            <em>&nbsp;</em>
                        </small>
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-ios-refresh"></i>
                </div>
                <a href="{{ backpack_url('/gescon/contrato?listar=contratosvigentes') }}" class="small-box-footer">Ver contratos <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>{{$html['vencidos']}}</h3>

                    <p>
                        Contratos vencidos<br />
                        &nbsp;
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-ios-close"></i>
                </div>
                <a href="{{ backpack_url('/gescon/contrato?listar=contratosvencidos') }}" class="small-box-footer">Ver contratos <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        @php
            $totalmsg = backpack_user()->unreadNotifications()->count() ?? 0;
        @endphp
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>{{$totalmsg}}</h3>
                        <p>
                            Mensagens pendentes<br />
                            &nbsp;
                        </p>
                </div>
                <div class="icon">
                    <i class="ion ion-ios-email"></i>
                </div>
                <a href="/mensagens" class="small-box-footer">Ler agora <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
    </div>

    <!-- Linha 2 -->
    <div class="row">
        <section class="col-lg-3">
            <div class="box box-solid feeds-rss">
                <div class="box-header with-border">
                    <i class="fa fa-rss"></i>
                    <h3 class="box-title">Feed de Notícias</h3>
                </div>
                <div class="box-body" style="max-height: 450px; overflow-y: scroll">
                    @foreach ($feedItems as $item)
                        @if (empty($feedTags) || ($item->get_category() != null && in_array($item->get_category()->get_term(), $feedTags)))
                            <div class="callout callout-default">
                                <h4><a href="{{ $item->get_permalink() }}" target="_blank" style="color: #3c8dbc">{{ $item->get_title() }}</a></h4>
                                <p><small>{{ $item->get_date('d/m/Y') }}</small></p>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </section>

        <section class="col-lg-5 connectedSortable ui-sortable">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <i class="fa fa-bar-chart"></i>
                    <h3 class="box-title">Contratos por Categoria</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    {!! $chartjs->render() !!}
                    <div class="text-center">
                        Total: {{ $chartjsTotal }}
                    </div>
                </div>
            </div>
        </section>

        <section class="col-lg-4 connectedSortable ui-sortable">
            <div class="box box-solid">
                <div class="box-header ui-sortable-handle with-border" style="cursor: move;">
                    <i class="fa fa-calendar"></i>

                    <h3 class="box-title">Calendário</h3>
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <!-- button with a dropdown -->
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                    <!-- /. tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding" style="height: 100%;text-align: center;">
                    <!--The calendar -->
{{--                    Erro no calendário. Estamos ajustando!--}}
{{--                    {!! $calendar->calendar() !!}--}}
{{--                    @push('after_scripts')--}}
{{--                        {!! $calendar->script() !!}--}}
{{--                    @endpush--}}
                    <div id="calendar"></div>
                </div>
                <!-- /.box-body -->
            </div>
        </section>
    </div>

{{--    <!-- Linha 3 -->--}}
{{--    <div class="row">--}}
{{--        <div class="col-md-12">--}}
{{--            <div class="row">--}}
{{--                <section class="col-lg-12 connectedSortable ui-sortable">--}}
{{--                    <div class="box box-solid">--}}
{{--                        <div class="box-header with-border">--}}
{{--                            <i class="fa fa-bar-chart"></i>--}}
{{--                            <h3 class="box-title">Empenhos sem contratos vinculados UG: {{ $ug }}</h3>--}}

{{--                            <div class="box-tools pull-right">--}}
{{--                                <button type="button" class="btn btn-box-tool" data-widget="collapse">--}}
{{--                                    <i class="fa fa-minus"></i>--}}
{{--                                </button>--}}
{{--                                <button type="button" class="btn btn-box-tool" data-widget="remove">--}}
{{--                                    <i class="fa fa-times"></i>--}}
{{--                                </button>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="box-body">--}}
{{--                            <div class="overflow-hidden">--}}
{{--                                {!! $gridEmpenhos->table() !!}--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </section>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

    <!-- #Modal -->
    <div class="modal fade contratoNaoInformado" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title ">
                        Empenhos sem contratos vinculados UG: {{ $ug }}
                    </h4>
                </div>
                <div class="modal-body">
                    <p>
                        Favor selecionar um contrato.
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default alert-warning" data-dismiss="modal">
                        Fechar
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalData" tabindex="-1" role="dialog" aria-labelledby="modalDataTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalDataTitle"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="modalDataBody" style="max-height: 300px; overflow-y: auto;">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default alert-error" data-dismiss="modal">
                        <i class="fa fa-reply"> Fechar</i>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!-- #Modal -->
    <div class="modal fade semPermissaoVinculoContrato" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title ">
                        Empenhos sem Contrato: {{ $ug }}
                    </h4>
                </div>
                <div class="modal-body">
                    <p>
                        Usuário não tem permissão para vincular Contrato ao Empenho.
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default alert-error" data-dismiss="modal">
                        Fechar
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('after_scripts')
{{--    {!! $gridEmpenhos->scripts() !!}--}}

    <script type="text/javascript">

        $(document).ready(function () {

            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                events: {
                    url: "{{route('calendario.eventos')}}",
                    method: 'POST',
                },
                dayClick: function(date, jsEvent, view) {
                    // Manipula o clique em uma data específica
                    if (view.name === 'month') {
                        // Se estiver na visualização mensal, abra um modal para a data
                        abrirModalEventoDia(date);
                    }
                },
                eventClick: function(calEvent, jsEvent, view) {
                    // Manipula o clique em um evento específico
                    abrirModalEvento(calEvent);
                }
            });

        });

        function abrirModalEvento(event){
            // Retorna eventos para a o evento clicado
            $('#modalDataTitle').text('Evento do Dia: ' + event.start.format('DD/MM/YYYY'));
            $('#modalDataBody').html('<ul class="list-group"><li class="list-group-item">' + event.title + '</li></ul>');
            $('#modalData').modal('show');
        }

        function abrirModalEventoDia(date) {
            // Retorna eventos para a data clicada
            var events = $('#calendar').fullCalendar('clientEvents', function(event) {
                return event.start.isSame(date, 'day');
            });

            if (events.length > 0) {
                $('#modalData').modal('show');
                $('#modalDataTitle').text('Eventos do Dia: ' + date.format('DD/MM/YYYY'));

                // Construir a lista de eventos no corpo do modal
                var eventosHtml = '<ul class="list-group">';
                events.forEach(function(event) {
                    eventosHtml += '<li class="list-group-item">' + event.title + '</li>';
                });
                eventosHtml += '</ul>';

                $('#modalDataBody').html(eventosHtml);
            }
        }

        $('body').delegate('.contrato', 'click', function() {
            var empenho = $(this).data('ne');
            var fornecedor = $(this).data('fornecedor');
            var contrato = $('#' + empenho).val();
            var url = '/api/empenho/sem/contrato/' + empenho + '/' + fornecedor + '/' + contrato;
            var linha = '#linha_' + empenho;

            if (contrato == '') {
                $('.contratoNaoInformado').modal('show');
                return false;
            }

            $.ajax({
                type: 'PUT',
                dataType: 'text',
                headers: {
                    // Passagem do token no header
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/api/empenho/sem/contrato/e/' + empenho + '/f/' + fornecedor + '/c/' + contrato,
                success: function(retorno) {
                    $(linha).remove();
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $('.semPermissaoVinculoContrato').modal('show');
                }
            });
        });
    </script>
@endpush
