@extends('backpack::layout_novo_guest')

@section('content')
    <div class="">
        <div class="box-body">
            <form class="col-md-12 p-t-10" role="form" method="POST"
                  action="{{ route('backpack.auth.login') }}" id="recaptcha-form">
                @if (empty($errors->has('score')))
                    <input type="hidden" name="g-recaptcha-response" id="g-recaptcha-response">
                    <input type="hidden" name="recaptcha-version" id="recaptcha-version" value="3">
                @endif

                @csrf

                <div class="form-group{{ $errors->has($username) || $errors->has('email_erro_govbr') || $errors->first('erro_usuario_compras') ? ' has-error' : '' }}" style="text-align: left">
                    <label class="control-label">{{ config('backpack.base.authentication_column_name') }}</label>

                    <div>
                        <input type="text" class="form-control" id="{{ $username }}" name="{{ $username }}"
                               value="{{ old($username) }}">

                        @if ($errors->has($username))
                            <span class="help-block">
                                        <strong>{!! $errors->first($username) !!}</strong>
                                    </span>
                        @endif

                        @if ($errors->has('email_erro_govbr'))
                            <span class="help-block">
                                        <strong>{!! $errors->first('email_erro_govbr') !!}</strong>
                                    </span>
                        @endif
                        @if ($errors->has('erro_usuario_compras'))
                            <span class="help-block">
                                        <strong>{!! $errors->first('erro_usuario_compras') !!}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}" style="text-align: left">
                    <label class="control-label">{{ trans('backpack::base.password') }}</label>

                    <div>
                        <input type="password" class="form-control" name="password">

                        @if ($errors->has('password'))
                            <span class="help-block">
                                        <strong>{!! $errors->first('password') !!}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                @if (!empty($errors->has('score')) && (int)$errors->has('score') < (int)env('RECAPTCHA_SITE_KEY'))
                    <div class="form-group">
                        <div id="html_element"></div>
                    </div>
                @endif

                <div class="button-container" style="display: flex; flex-direction: column; gap: 10px; align-items: center; justify-content: center;">
                    <button type="submit" class="custom-button blue-button">
                        {{ trans('backpack::base.login') }}
                    </button>
                </div>
            </form>

        </div>
    </div>
@endsection

@section('area_recuperar_senha')
    @if (backpack_users_have_email())
        <div class="text-center text-lg m-t-10">
            <a href="{{ route('backpack.auth.password.reset') }}">{{ trans('backpack::base.forgot_your_password') }}</a>
        </div>
    @endif
@endsection

@push('after_scripts')
    <script type="text/javascript">
        $( document ).ready(function($) {
            $('#{{ $username }}').mask('999.999.999-99');

            $('#acessogov').click(function () {
                window.location.href = "{{ route('acessogov.autorizacao') }}";
            })

            @if (empty($errors->has('score')))
            grecaptcha.ready(function () {
                grecaptcha.execute('{{ env('RECAPTCHA_SITE_KEY') }}', {action: 'submit'}).then(function (token) {
                    // Preencha o campo g-recaptcha-response
                    $('#g-recaptcha-response').val(token);
                });
            });
            @endif
        });

        @if (!empty($errors->has('score')) && (int)$errors->has('score') < (int)env('RECAPTCHA_SITE_KEY'))
        var onloadCallback = function() {
            grecaptcha.render('html_element', {
                'sitekey' : '{{ env('RECAPTCHA_SITE_KEY_V2') }}'
            });
        };
        @endif
    </script>

    @if (empty($errors->has('score')))
        <script src="https://www.google.com/recaptcha/api.js?render={{ env('RECAPTCHA_SITE_KEY') }}"></script>
    @endif

    @if (!empty($errors->has('score')) && (int)$errors->has('score') < (int)env('RECAPTCHA_SITE_KEY'))
        <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
    @endif
@endpush


