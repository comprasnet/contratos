@extends('backpack::layout_novo_guest')

@push('area_botoes_superior_direito')
    @include('vendor.backpack.crud.buttons.button_acessar_headset')
@endpush

@section('botoes')
    <button class="custom-button blue-button button-custom-login" type="button" id="acessogov" style="margin-top: 10px">Entrar com&nbsp;<b>gov.br</b>
    </button>

    <a class="custom-button white-button button-custom-login" href="/transparencia" style="margin-top: 20px;">Transparência</a>

    <div class="form-group{{ session('error_nivel') ? ' has-error' : '' }}">
        <div>
            @if (session('error_nivel'))
                <p style="color: red; text-align: justify">
                    {!! session('error_msg') !!}
                </p>
            @endif
        </div>
    </div>
@endsection

@push('after_scripts')
    <script type="text/javascript">
        $( document ).ready(function($) {
            $('#acessogov').click(function () {
                window.location.href = "{{ route('acessogov.autorizacao') }}";
            })
        });
    </script>
@endpush


