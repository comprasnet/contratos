@extends('backpack::layout_guest')

@section('content')
    <div class="row m-t-40">
        <div class="col-md-4 col-md-offset-4">
            {{--<h3 class="text-center m-b-20">{{ trans('backpack::base.login') }}</h3>--}}
            <div class="login-logo">
                <a href="{{ url('/inicio') }}">
                    <img src="data:image/png;base64,<?php echo base64_encode(file_get_contents("../public/img/logo.png")) ?>" width="200px"
                         alt="{!! env('APP_NAME') !!}"></a><br>
            </div>
            <div class="comment" align="right" style="color: red;">
               <i><b>{!! config('app.app_amb') !!}</b></i>
            </div>
            <div class="box">
                <div class="box-body">
                    <form class="col-md-12 p-t-10" role="form" method="POST"
                          action="{{ route('backpack.auth.login') }}" id="recaptcha-form">
                        @if (empty($errors->has('score')))
                            <input type="hidden" name="g-recaptcha-response" id="g-recaptcha-response">
                            <input type="hidden" name="recaptcha-version" id="recaptcha-version" value="3">
                        @endif

                        @csrf

                        <div class="form-group{{ $errors->has($username) || $errors->has('email_erro_govbr') || $errors->first('erro_usuario_compras') ? ' has-error' : '' }}">
                            <label class="control-label">{{ config('backpack.base.authentication_column_name') }}</label>

                            <div>
                                <input type="text" class="form-control" id="{{ $username }}" name="{{ $username }}"
                                       value="{{ old($username) }}">

                                @if ($errors->has($username))
                                    <span class="help-block">
                                        <strong>{{ $errors->first($username) }}</strong>
                                    </span>
                                @endif

                                @if ($errors->has('email_erro_govbr'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email_erro_govbr') }}</strong>
                                    </span>
                                @endif
                                @if ($errors->has('erro_usuario_compras'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('erro_usuario_compras') }}</strong>
                                    </span>
                                @endif
                            </div>

                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="control-label">{{ trans('backpack::base.password') }}</label>

                            <div>
                                <input type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"
                                               name="remember"> {{ trans('backpack::base.remember_me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        @if (!empty($errors->has('score')) && (int)$errors->has('score') < (int)env('RECAPTCHA_SITE_KEY'))
                            <div class="form-group">
                                <div id="html_element"></div>
                            </div>
                        @endif

                        <div class="form-group{{ session('error_nivel') ? ' has-error' : '' }}">
                            <div>
                                @if (session('error_nivel'))
                                    <p style="color: red; text-align: justify">
                                        {!! session('error_msg') !!}
                                    </p>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="button-container">
                                <button type="submit" class="custom-button blue-button">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                        <path d="M224 256A128 128 0 1 0 224 0a128 128 0 1 0 0 256zm-45.7 48C79.8 304 0 383.8 0 482.3C0 498.7 13.3 512 29.7 512l388.6 0c16.4 0 29.7-13.3 29.7-29.7C448 383.8 368.2 304 269.7 304l-91.4 0z"/>
                                    </svg>
                                    {{ trans('backpack::base.login') }}
                                </button>
                           
                                <button class="custom-button white-button" type="button" id="acessogov"> 
                                    Entrar com&nbsp;<img src="https://www.gov.br/++theme++padrao_govbr/img/govbr-colorido-b.png" alt="gov.br"/>
                                </button>

                                <a class="custom-button white-button" href="/transparencia">
                                    Transparência
                                </a>
                            </div>
                        </div>
                        <hr class="m-t-30" />
                    </form>

                    @if (backpack_users_have_email())
                        <div class="text-center text-lg m-t-10">
                            <a href="{{ route('backpack.auth.password.reset') }}">
                                {{ trans('backpack::base.forgot_your_password') }}
                            </a>
                        </div>
                    @endif
                </div>
            </div>

            @if (config('backpack.base.registration_open'))
                <div class="text-center text-lg m-t-10">
                    <a>
                        {{ trans('backpack::base.register') }}
                    </a>
                </div>
            @endif
        </div>
    </div>

    @push('after_scripts')
        <script type="text/javascript">
            $( document ).ready(function($) {
                $('#{{ $username }}').mask('999.999.999-99');

                $('#acessogov').click(function () {
                    window.location.href = "{{ route('acessogov.autorizacao') }}";
                })

                @if (empty($errors->has('score')))
                    grecaptcha.ready(function () {
                        grecaptcha.execute('{{ env('RECAPTCHA_SITE_KEY') }}', {action: 'submit'}).then(function (token) {
                            // Preencha o campo g-recaptcha-response
                            $('#g-recaptcha-response').val(token);
                        });
                    });
                @endif
            });

            @if (!empty($errors->has('score')) && (int)$errors->has('score') < (int)env('RECAPTCHA_SITE_KEY'))
                var onloadCallback = function() {
                    grecaptcha.render('html_element', {
                    'sitekey' : '{{ env('RECAPTCHA_SITE_KEY_V2') }}'
                    });
                };
            @endif
        </script>

        @if (empty($errors->has('score')))
            <script src="https://www.google.com/recaptcha/api.js?render={{ env('RECAPTCHA_SITE_KEY') }}"></script>
        @endif

        @if (!empty($errors->has('score')) && (int)$errors->has('score') < (int)env('RECAPTCHA_SITE_KEY'))
            <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
        @endif
</script>

<style>
.custom-button {
    border: none;
    border-radius: 50px;
    padding: 8px 12px 8px 12px;
    font-family: inherit;
    font-size: 16.8px;
    font-weight: 600;
    height: 40px;
    width: auto;
    display: flex;
    align-items: center; 
    justify-content: center;
    align-items: center;
    cursor: pointer;
    outline: none;
    transition: background-color 0.3s, color 0.2s; 
}

.custom-button svg {
    width: 15px;
    height: 15px;
    margin-right: 2px;
    fill: currentColor; 
}

.custom-button img {
    max-width: 35%; 
    margin-left: 1px;
}

.blue-button {
    background-color: #1251B4;
    color: white;
    max-width: 110px;
}

.blue-button:hover {
    background-color: #466eb5;
}

.white-button {
    background-color: #F8F8F8;
    color: #1251B4;
    max-width: 200px;
}

.white-button:hover {
    background-color: rgba(19, 81, 180, 0.16);
    color: #1a55d8;
}

.button-container {
    display: flex;
    gap: 10px;
    flex-wrap: wrap;
    justify-content: center;
}
.button-container button {
    flex: 0 1 calc(50% - 10px); 
}
.button-container a {
    flex: 0 1 calc(50% - 10px); 
}
</style>
    @endpush
@endsection
