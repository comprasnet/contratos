<button
    id="{{ $idBtn }}"
    name="{{ $name }}"
    type="button"
    class="{{ $class }}"
    title="{{ $hint }}"
    data-btn-apropriar-bottom="{{ $btnApropriarBottom }}"
    data-id_contrato="{{ $idContrato }}"
    data-route="{{ $route }}"
    onclick="abrirModalApropriarFaturas(this)"
>
    @if ($icon)
        <i class="fa fa-file-text-o"></i>
    @else
        {{'Alterar Situação'}}
    @endif

</button>

<!-- Janela modal para inserção de registros -->
<div id="modal-status-instrumento" tabindex="-1" class="modal fade"
     role="dialog"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">
                    Alteração da situação em lote
                </h3>
            </div>

            <div class="modal-body" id="textoModal">
                <div class="box-body">
                    <form id="form-situacao-intrumento" method="post" >
                    @csrf <!-- {{ csrf_field() }} -->

                        <div class="form-group row">
                            <label for="situacao_instrumento_combranca_id">Situação</label>
                            <select class="form-control" id="situacao_instrumento_combranca_id" name="situacao_instrumento_combranca_id">
                                <option value="">Selecione a situação</option>
                                @foreach($selectSituacoes as $key => $value)
                                <option value="{{$key}}" data-option-situacao="{{$key}}">
                                    {{$value}}
                                </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group row">
                            <label for="situacao_instrumento_combranca_id">Justificativa</label>
                             <select class="form-control" id="justificativafatura_id" name="justificativafatura_id">
                                @foreach($justificativasParaAlteracaoEmLote as $key => $value)
                                <option value="{{$key}}" data-option-situacao="{{$key}}">
                                    {{$value}}
                                </option>
                                @endforeach
                            </select>

                        </div>

                    </form>
                </div>
            </div>



            <div class="modal-footer">
                <!-- <div class="btns-apropriacao-wrapper"> -->
                    <button
                        id="btn-salvar"
                        name="btn-alterar-status-instrumentos"
                        class="btn btn-success"
                        title="Salvar alterações"
                        type="button"
                        data-btn-alterar-status-bottom=""
                        data-num-from={{2}}
                        data-route-popular-tabelas-sf=""
                        onclick="onClickBtnApropriacaoFatura(this)"
                    >
                        <span>Salvar</span>
                    </button>
                    <button
                        id= "btn-cancelar"
                        name="cancelar"
                        class="btn btn-secondary"
                        type="button"
                        data-dismiss="modal"
                        onclick="onClickCancelar()"
                    >
                        <span>Cancelar</span>
                    </button>
                <!-- </div> -->
            </div>

        </div>
    </div>
</div>

@push('after_scripts')
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script type="text/javascript">

        function onClickBtnApropriacaoFatura(button) {

            var situacao_selecionada = $('#situacao_instrumento_combranca_id option:selected').val();
            var justificativa_selecionada = $('#justificativafatura_id option:selected').val();

            /**
             * Verifica qual botao foi acionado (linha ou varios) e seta inputs hiddens com contratofaturas_id no form
             * caso varios seta os ids do checkbox
             **/
            crud.checkedItems.map(function (idContratoFatura) {
                $('#form-situacao-intrumento').append("<input type='hidden' name='contratofaturas_ids[]' value=" + idContratoFatura + ">");
            });

            /**
             situacao_selecionada é obrigatória
             */
            if (situacao_selecionada == "") {
                new PNotify({
                    title: 'Nenhuma situação selecionada',
                    text: 'Por favor selecione uma situação',
                    type: "warning"
                });
                return;
            }

            /**
             justificativafatura_id é obrigatória
             */
            if (justificativa_selecionada == "") {
                new PNotify({
                    title: 'Nenhuma justificativa selecionada',
                    text: 'Por favor selecione uma justificativa',
                    type: "warning"
                });
                return;
            }

            $("#btn-salvar").attr('disabled', 'disabled');
            $("#btn-cancelar").attr('disabled', 'disabled');
            $("#form-situacao-intrumento").submit();

        }

        function abrirModalApropriarFaturas(button) {
            var modalApropriacao = $('#modal-status-instrumento');
            /*abre modal*/
            modalApropriacao.modal('toggle');
        }

        function onClickCancelar() {
            //zera os checkbox selecionados
            crud.checkedItems = [];
            crud.checkedIdContrato = [];
            crud.table.ajax.reload();
            $("#situacao_instrumento_combranca_id option:first-child").attr("selected", "selected");
        }

        /*remove input hidden de contratofaturas_id ao fechar modal*/
        $(window).on('load', function () {
            $("#modal-status-instrumento").on("hidden.bs.modal", function () {
                $('#contratofaturas_id_linha').remove();
            });
        });

    </script>
    <style>
        form {
            display: flex;
            flex-direction: column;
        }

        .btns-apropriacao-wrapper {
            display: flex;
            flex-direction: column;
            justify-content: center;
        }

        .btns-apropriacao-wrapper > div {
            padding: 10px;
        }

        .btns-apropriacao {
            width: 100%;
            text-align: right;
        }

        .btns-apropriacao > span {
            float: left;
        }

        .btns-apropriacao > span {
            font-weight: bold;
        }

        #select_padrao {
            width: 100%;
        }
    </style>
@endpush
