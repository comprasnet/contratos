@php

$tamanhoResultados = sizeof($ordensBancariasAgrupadas);

 @endphp

@if(!empty($ordensBancariasAgrupadas))

    @foreach($ordensBancariasAgrupadas as $key => $grupo)
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-{{ $key }}">
                        <strong>Conta bancária:</strong> {{ $grupo['contadestino'] }}
                        <strong>Nº Agência:</strong> {{ $grupo['agenciadestino'] }}
                        <strong>Nº Banco:</strong> {{ $grupo['bancodestino'] }}
                    </a>
                </h4>
                <button type="button" class="close" data-toggle="collapse" data-target="#collapse-{{ $key }}">
                    <i class="fa fa-caret-down"></i>
                </button>
            </div>
            <div id="collapse-{{ $key }}" class="panel-collapse collapse @if($tamanhoResultados == 1) in @endif">
                <div class="panel-body">
                    @foreach($grupo['ordensBancarias'] as $ordemBancaria)
                        <div class="row">
                            <div class="col-md-6">
                                <strong>Documento:</strong> {{ $ordemBancaria->numero }}
                            </div>
                            <div class="col-md-6">
                                <strong>Número Cancelamento OB:</strong> {{ $ordemBancaria->numeroobcancelamento ?? 'Não se aplica' }}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <strong>Data Emissão:</strong> {{ $ordemBancaria->emissao }}
                            </div>
                            <div class="col-md-6">
                                <strong>Valor:</strong> {{ $ordemBancaria->valor}}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <strong>Observação:</strong> {{ $ordemBancaria->observacao }}
                            </div>
                        </div>
                        <hr>
                    @endforeach
                </div>
            </div>
        </div>
    @endforeach
@endif
