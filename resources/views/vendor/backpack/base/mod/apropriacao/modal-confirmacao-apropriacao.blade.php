<button
    id="{{ $idBtn }}"
    name="{{ $name }}"
    type="button"
    class="{{ $class }}"
    title="{{ $hint }}"
    data-btn-apropriar-bottom="{{ $btnApropriarBottom }}"
    data-id_contrato="{{ $idContrato }}"
    data-route="{{ $route }}"
    onclick="abrirModalApropriarFaturas(this)"
>
    @if ($icon)
        <i class="fa fa-file-text-o"></i>
    @else
        @if (isset($label))
            {{$label}}
        @else
            {{'Apropriar Faturas'}}
        @endif
    @endif
</button>

<!-- Janela modal para inserção de registros -->
<div id="modal-apropriar-fatura" tabindex="-1" class="modal fade"
     role="dialog"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">
                    Como deseja apropriar ?
                </h3>
            </div>

            <div class="modal-body" id="textoModal">
                <div class="box-body">
                    <form
                        id="form-apropriar-fatura"
                        method="post"
                    >
                    @csrf <!-- {{ csrf_field() }} -->
                        <div class="form-group">
                            <select class="form-control" id="select_padrao" name="sf_padrao_id" style="width: 100%; height: 50px; padding: 10px 10px; border: 1px solid #ccc; border-radius: 4px;">
                            </select>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <div class="btns-apropriacao-wrapper">
                        <button
                            name="btn-apropriacao-fatura"
                            class="btn btn-primary btns-apropriacao"
                            title="Apropriação de faturas"
                            type="button"
                            data-btn-apropriar-bottom=""
                            data-num-from="{{3}}"
                            data-route-popular-tabelas-sf=""
                            onclick="onClickBtnApropriacaoFatura(this)"
                        >
                            <span>NOVA APROPRIAÇÃO</span>
                            <span class="pull-right"><i class="fa fa-file-text-o"></i></span> &nbsp;
                        </button>
                        <button
                            name="btn-apropriacao-fatura"
                            class="btn btn-primary btns-apropriacao"
                            title="Apropriação de faturas"
                            type="button"
                            data-btn-apropriar-bottom=""
                            data-num-from="{{1}}"
                            data-route-popular-tabelas-sf=""
                            onclick="onClickBtnApropriacaoFatura(this)"
                        >
                            <span>COPIAR DE</span>
                            <span class="pull-right"><i class="fa fa-file-text-o"></i></span>
                        </button>
                </div>
                <div class="progress hide">
                    <div
                        class="progress-bar"
                        role="progressbar"
                        aria-valuenow="70"
                        aria-valuemin="0"
                        aria-valuemax="100"
                        style="min-width: 2em;">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('after_scripts')
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script type="text/javascript">

        /**
         *Ação dos botões de alteração de fatura
         *@param numFrom identificador da origem do click
         *@param btnApropriarBottom flag que identifica clique na linha ou checkbox
         */
        function onClickBtnApropriacaoFatura(button) {

            var numFrom = $(button).data('num-from'),
            id_padrao_selecionado = $('#select_padrao option:selected').val(),
            btnApropriarBottom = $(button).data('btn-apropriar-bottom');
            
            /**
             * Verifica qual botao foi acionado (linha ou varios) e seta inputs hiddens com contratofaturas_id no form
             * caso varios seta os ids do checkbox
             **/
            if (btnApropriarBottom) {
                crud.checkedItems.map(function (idContratoFatura) {
                    $('#form-apropriar-fatura').append("<input type='hidden' name='contratofaturas_ids[]' value=" + idContratoFatura + ">");
                });
            }

            /**
             numFrom 1 e 2 é obrigatório selecionar o padrão
             */
            if (parseInt(numFrom) !== 3 && !id_padrao_selecionado) {
                new PNotify({
                    title: 'Nenhum DH selecionado',
                    text: 'Por favor selecione um DH',
                    type: "warning"
                });
                return;
            }

            /*Apropriar sem DH Padrao selecionado*/
            if (parseInt(numFrom) === 3) {

                popularTabelasNovoDHPadrao();

                return;
            }

            popularTabelas(id_padrao_selecionado, numFrom);

        }

        function abrirModalApropriarFaturas(button) {
            var btnApropriarBottom = $(button).data('btn-apropriar-bottom'),
                idContrato = $(button).attr('data-id_contrato'),
                routeRecuperarPadrao = '{{ route('apropriacao.fatura.padrao') }}',
                routeApropriacaoFaturaLinha = $(button).attr('data-route'),
                idsContratoisValid = !!btnApropriarBottom ? crud.checkedIdContrato.every((val, i, arr) => val === arr[0]) : true,
                idsContratoFornecedorEmpenho = !!btnApropriarBottom ? crud.checkedIdFornecedor.every((val, i, arr) => val === arr[0])
                    && crud.checkedDescricao.every(descricao => descricao === 'Empenho') : true,
                modalApropriacao = $('#modal-apropriar-fatura');
            /*verifica se foi selecionado contratos com ids diferentes*/

            if(!idsContratoFornecedorEmpenho && !idsContratoisValid){
                new PNotify({
                    title: 'Seleção não permitida',
                    text: 'Selecione apenas faturas de um mesmo contrato ou contrato do tipo empenho de um mesmo fornecedor.',
                    type: "warning"
                });
                modalApropriacao.modal('hide');
                return;
            }

            /*abre modal*/
            modalApropriacao.modal('toggle');

            /*busca os padroes das faturas selecionadas*/
            recuperarPadraoFaturas(routeRecuperarPadrao, btnApropriarBottom, idContrato);

            /*seta rota para popular tabelas sf*/
            $('button[name=btn-apropriacao-fatura]').attr('data-route-popular-tabelas-sf',
                !!btnApropriarBottom ? '{{ route('apropriacao.fatura.create.bulk') }}' : routeApropriacaoFaturaLinha
            );

            /*seta data-btn-apropriar-bottom*/
            $('button[name=btn-apropriacao-fatura]').attr('data-btn-apropriar-bottom', !!btnApropriarBottom);

            /*quando ação vier da linha adiciona contratofaturas_id da linha em input hidden do form*/
            $('#form-apropriar-fatura').append(
                !btnApropriarBottom ?
                    "<input type='hidden' id='contratofaturas_id_linha' name='contratofaturas_ids[]' value=" + $(button).attr('id') + ">" :
                    ''
            );
        }

        function recuperarPadraoFaturas(route, btnApropriarBottom, idContrato) {
            axios.get(route, {
                params: {
                    id_contrato: !!btnApropriarBottom ? crud.checkedIdContrato[0] : idContrato
                }
            })
            .then(response => {
            if (response.data.length > 0) {
                var options = "<option value=''>Selecione uma apropriação anterior</option>";
            
                options += response.data.map(function (contratoFatura) {
                    return "<option value='" + contratoFatura.id + "'>" + contratoFatura.dh + "</option>";
                }).join('');
            
                $('#select_padrao').html(options);
                
                $('#select_padrao').select2({
                    formatNoMatches: function () {
                        return "Nenhum resultado encontrado";
                    }
                });
            }

            if (response.data.length === 0) {
                var options = "<option value=''>Selecione uma apropriação anterior</option>";
                $('#select_padrao').html(options);
            }

            })
            .catch(error => {
                new PNotify({
                    title: 'Apropriação de Faturas',
                    text: 'Erro ao recuperar o padrão das faturas',
                    type: "error"
                });
            }).finally();
        }


        function popularTabelas(idPadrao = null, numFrom) {
            var btnApropriacaoFatura = $('button[name=btn-apropriacao-fatura]'),
                route = btnApropriacaoFatura.data('route-popular-tabelas-sf'),
                btnApropriarBottom = btnApropriacaoFatura.attr('data-btn-apropriar-bottom'),
                modalApropriacao = $('#modal-apropriar-fatura');

            var progressInterval; 

            return $.ajax({
                url: route,
                type: btnApropriarBottom === 'true' ? 'PUT' : 'GET',
                data: btnApropriarBottom === 'true' ? { entries: crud.checkedItems, idPadrao } : { idPadrao },
                beforeSend: function () {
                    $(".progress").removeClass('hide');
                    $(".modal-body").hide();
                    $(".btns-apropriacao-wrapper").hide();

                    $('.progress-bar').css('width', '0%');
                    progressInterval = setInterval(function () {
                        var currentWidth = parseInt($('.progress-bar').css('width')) || 0;
                        var newWidth = Math.min(currentWidth + 10, 100);
                        $('.progress-bar').css('width', newWidth + '%');
                        if (newWidth >= 100) clearInterval(progressInterval);
                    }, 200);
                },
                success: function (result) {
                    clearInterval(progressInterval);
                    $('.progress-bar').css('width', '100%');
                
                    if (result.status === 'error') {
                        tratarErrorRequisicao(modalApropriacao, result.message);

                        return;
                    }

                    //Ação do botao Alterar antes de apropriar
                    if (parseInt(numFrom) === 1) {
                        var url = "{{route('alterar.antes.apropriar.fatura',[':sf_padrao_id', ':apropriacao_id'])}}";
                        url = url.replace(':sf_padrao_id', result.idPadraoDuplicado).replace(':apropriacao_id', result.idApropriacao);
                        window.location = url;
                    } else if (parseInt(numFrom) === 2) {
                        var msg = result.message || crud.checkedItems.length + ' faturas incluídas para apropriação.';
                        new PNotify({
                            title: 'Apropriação de Faturas',
                            text: msg,
                            type: result.result
                        });

                        modalApropriacao.modal('hide');

                        crud.checkedItems = [];
                        crud.checkedIdContrato = [];
                        crud.checkedIdFornecedor = [];
                        crud.checkedDescricao = [];
                        crud.table.ajax.reload();
                    }

                },
                error: function (request, status, error) {
                    tratarErrorRequisicao(modalApropriacao, request.responseJSON.message);
                }
            });
        }

        function popularTabelasNovoDHPadrao() {

            var btnApropriacaoFatura = $('button[name=btn-apropriacao-fatura]'),
                btnApropriarBottom = btnApropriacaoFatura.attr('data-btn-apropriar-bottom'),
                route = "{{ route('apropriacao.fatura.create.com.novo.padrao') }}",
                idFatura = $('#contratofaturas_id_linha').val(),
                //entries = typeof crud.checkedItems !== 'undefined' ? crud.checkedItems : idFatura,
                entries = btnApropriarBottom === 'true' ? crud.checkedItems : idFatura,
                modalApropriacao = $('#modal-apropriar-fatura');

            $.ajax({
                url: route,
                type: 'POST',
                data: { entries },
                xhr: function () {
                    var xhr = $.ajaxSettings.xhr();
                    xhr.upload.onprogress = function (e) {
                        $('.progress-bar').css('width', Math.floor(e.loaded / e.total * 100) + '%');
                    };
                    return xhr;
                },
                beforeSend: function () {
                    $(".progress").removeClass('hide');     //exibe a barra de progresso
                    $(".modal-body").hide();                //esconde corpo da modal
                    $(".btns-apropriacao-wrapper").hide();  //esconde botoes
                },
                success: function (result) {
                    if(result.status === 'error'){
                        tratarErrorRequisicao(modalApropriacao, result.message);

                        return;
                    }

                    var url = "{{route('alterar.antes.apropriar.fatura',[':sf_padrao_id', ':apropriacao_id'])}}";
                    var url2 = url.replace(':sf_padrao_id', result.padraoId);
                    url = url2.replace(':apropriacao_id', result.idApropriacao);

                    window.location = url;
                },
                error: function (request, status, error) {
                    tratarErrorRequisicao(modalApropriacao, request.responseJSON.message);
                }
            });
        }

        function tratarErrorRequisicao(modalApropriacao, message)
        {
            //zera os checkbox selecionados
            crud.checkedItems = [];
            crud.checkedIdContrato = [];
            crud.checkedIdFornecedor = [];
            crud.checkedDescricao = [];
            crud.table.ajax.reload();

            $(".progress").addClass('hide');        //esconde barra de progresso
            $('.progress-bar').css('width', '2%');  //reseta barra de progresso iniciando em 2%
            $(".modal-body").show();                //volta a exibir o corpo da modal
            $(".btns-apropriacao-wrapper").show();  //volta a exibir botoes

            //fecha modal
            modalApropriacao.modal('hide');

            new PNotify({
                title: 'Erro ao apropriar fatura',
                text: message,
                type: "error"
            });

        }

        /*remove input hidden de contratofaturas_id ao fechar modal*/
        $(window).on('load', function () {
            $("#modal-apropriar-fatura").on("hidden.bs.modal", function () {
                $('#contratofaturas_id_linha').remove();
            });
        });

    </script>
    <style>
        form {
            display: flex;
            flex-direction: column;
        }

        .btns-apropriacao-wrapper {
            display: flex;
            flex-direction: row;
            justify-content: space-evenly;
        }
        .btns-apropriacao {
            flex-grow: 1;
        }

        .btns-apropriacao > span {
            font-weight: bold;
        }

        #select_padrao {
            width: 100%;
        }
    </style>
@endpush
