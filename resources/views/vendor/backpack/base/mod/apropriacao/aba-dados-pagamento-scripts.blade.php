<script type="text/javascript">
    function initAbaDadosPagamento() {

        var dt = $('input[name="dtpgtoreceb"]').val();
        $('#modalValorCampo').text(formatDate(dt));

        $('input[name="dtpgtoreceb"]').on('change', function () {
            var dtAlterado = $(this).val();
            $('#modalValorCampo').text(formatDate(dtAlterado));
        });

        $('#btnRemoverPredoc').on('click', function(){
            removerPredocViaAjax();
        });

        function formatDate(dateString) {
            var parts = dateString.split('-');
            return parts[2] + '/' + parts[1] + '/' + parts[0];
        }

        const input = document.getElementById("codcredordevedor")

        if (input && input.value) {
            retornaMascaraCnpjCpf(input);
            buscarFornecedor(input);
        }
    }

    function retornaMascaraCnpjCpf(element) {
        let valor = element.value;

        if (/[a-zA-Z]/.test(valor)) {
            element.value = valor.toUpperCase();
            return valor.toUpperCase();
        }

        valor = valor.replace(/\D/g, '');
        let retorno = valor;

        if (valor.length === 14) {
            const d0 = valor.substring(0, 2);
            const d1 = valor.substring(2, 5);
            const d2 = valor.substring(5, 8);
            const d3 = valor.substring(8, 12);
            const d4 = valor.substring(12, 14);

            element.value = `${d0}.${d1}.${d2}/${d3}-${d4}`;
        } else if (valor.length === 11) {
            const d0 = valor.substring(0, 3);
            const d1 = valor.substring(3, 6);
            const d2 = valor.substring(6, 9);
            const d3 = valor.substring(9, 11);

            element.value = `${d0}.${d1}.${d2}-${d3}`;
        } else {
            element.value = valor;
        }
    }

    function buscarFornecedor(element) {
        var cpf_cnpj_idgener = element.value,
            route = "{{ route('apropriacao.faturas.buscar.fornecedor.ajax') }}";

        $.ajax({
            url: route,
            data: {
                'codcredordevedor': cpf_cnpj_idgener
            },
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                if (data && data.fornecedor) {
                    $('#codcredordevedorPreDoc').text(data.fornecedor.cpf_cnpj_idgener);
                    $('#nomeCredordevedorPreDoc').text(data.fornecedor.nome);
                } else {

                    $('#codcredordevedorPreDoc').text(cpf_cnpj_idgener);
                    $('#nomeCredordevedorPreDoc').text('');
                }
            },
            error: function () {
                new PNotify({
                    title: 'Atenção',
                    delay: 3000,
                    text: 'Não foi possivel buscar o fornecedor',
                    type: "alert"
                });
            }
        });
    }

    function removerPredocViaAjax() {

        var sf_padrao_id = "{{ Route::current()->parameter('sf_padrao_id')}} ";
        var route = "{{ route('fatura.form.remover-pre-doc', [
                        Route::current()->parameter('sf_padrao_id'),
                        Route::current()->parameter('apropriacao_id')]
                        )
                     }}";

        console.log(route)
        $.ajax({
            url: route,
            data: {
                sf_padrao_id,
            },
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                Swal.fire({
                    icon: 'success',
                    title: 'Pré-Doc removido',
                    showConfirmButton: false,
                    timer: 1500
                });

                // carrega a aba novamente
                $('#dados-pagamento-tab').removeClass('loaded');
                carregaAba();
            },
            error: function () {
                new PNotify({
                    title: 'Atenção',
                    delay: 3000,
                    text: 'Erro ao remover Pré-Doc',
                    type: "alert"
                });
            }
        });

    }
</script>
