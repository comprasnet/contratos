{{--<!-- TOTAL DA ABA DEDUÇÃO -->--}}
<div class="row">
    <div class="col-md-6 h3">Dedução</div>
    <div class="col-md-offset-2 col-md-4">
        <div class="pull-right">
            <div class="input-group margin-top-20 margin-bottom-10">
                <div class="input-group-addon label-title">Total da aba:</div>
                <input type="text" class="form-control" id="valorTotalAbaDeducao" readonly value="{{ number_format($totalValorDeducao, 2, ',', '.') }}">
            </div>
        </div>
    </div>
</div>
<hr>
{!! form_start($sfDeducaoForm) !!}
<div>
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs" role="tablist">
            @foreach($sfDeducao as $sfDeducaoCurrent)
                <li
                    role="presentation"
                    class="
                    @if ($idDeducaoActive === null && $loop->first)active @endif
                    @if ($idDeducaoActive === $sfDeducaoCurrent->id)active @endif pointer-hand
                    idSfDeducao{{$sfDeducaoCurrent->id}}
                        "
                >
                    <a
                        data-target="#{!! $sfDeducaoCurrent->codsit !!}_{!! $sfDeducaoCurrent->id !!}"
                        aria-controls="{!! $sfDeducaoCurrent->codsit !!}_{!! $sfDeducaoCurrent->id !!}"
                        role="tab"
                        data-toggle="tab"
                        class="aba-sfdeducao count-situacao-deducao"
                        data-count-situacao-deducao="{{$loop->count}}"
                        data-numseqitem="{{$sfDeducaoCurrent->numseqitem}}"
                        data-codsit="{{$sfDeducaoCurrent->codsit}}"
                        id="aba-deducao{{$sfDeducaoCurrent->id}}"
                        onclick="atualizaTotaisRodapeRecolhedor({{ $sfDeducaoCurrent->id }});abaAtivaDeducao()"
                    >
                        {{ $sfDeducaoCurrent->codsit ?? ' - ' }}

                    </a>
                </li>
            @endforeach
            <li>
                <a
                    href="javascript:void(0)"
                    id="nova-aba-situacao-deducao"
                    class="esconderQuandoApropriacaoEmitidaSiafi"
                    onclick="clickBtnNovaAbaSituacaoDeducao(null, false);"
                >
                    <i class="fa fa-plus" style="color: #3c8dbc"></i>
                </a>
            </li>
            <li class="dropdown">
                <a
                    class="dropdown-toggle esconderQuandoApropriacaoEmitidaSiafi"
                    data-toggle="dropdown"
                    href="#"
                    aria-expanded="false"
                >
                    <i class="fa fa-trash" style="color: red"></i> <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    @foreach($sfDeducao as $sfDeducaoCurrent)

                        <li
                            class="pointer-hand"
                        >
                            <a
                                href="javascript:void(0)"
                                id="nova-aba-situacao-deducao{{ $sfDeducaoCurrent->id }}"
                                class="esconderQuandoApropriacaoEmitidaSiafi"
                                onclick="clickBtnNovaAbaSituacaoDeducao({{ $sfDeducaoCurrent->id }}, true);"
                            >
                                {{ $sfDeducaoCurrent->codsit ?? ' - ' }}

                            </a>
                        </li>
                    @endforeach
                </ul>
            </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            @foreach($sfDeducao as $sfDeducaoCurrent)
                <div
                    role="tabpanel"
                    class="tab-pane fade
                        @if ($idDeducaoActive === null && $loop->first)active in @endif
                        @if ($idDeducaoActive === $sfDeducaoCurrent->id)active in @endif
                        "
                    id={!! $sfDeducaoCurrent->codsit !!}_{!! $sfDeducaoCurrent->id !!}
                >

                    <div class="row">
                        <div class="col-md-3 label-title paddingTopButton">Situação:</div>
                        <div class="col-md-3 paddingTopButton">
                            {!! form_row($sfDeducaoForm->{'sfdeducaocodsit' . $sfDeducaoCurrent->id  }) !!}
                        </div>
                        <div class="col-md-3 label-title paddingTopButton">Nome da Situação:</div>
                        <div class="col-md-3 paddingTopButton" id="descricaoSituacao{{$sfDeducaoCurrent->id}}">{{ $sfDeducaoCurrent->nomeSituacao }}</div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 label-title paddingTopButton">UG Pagadora:</div>
                        <div class="col-md-3 paddingTopButton">
                            {!! form_row($sfDeducaoForm->{'sfdeducaocodugpgto' . $sfDeducaoCurrent->id  }) !!}
                        </div>

                        <div class="col-md-3 label-title paddingTopButton">Nome da UG Pagadora:</div>
                        <div class="col-md-3 paddingTopButton" id="nomeugempeDeducao{{$sfDeducaoCurrent->id}}">{{ $sfDeducaoCurrent->nomeUgPagadora ?? ' - ' }}</div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 label-title paddingTopButton">Data de Vencimento:</div>
                        <div class="col-md-3 paddingTopButton">{!! form_row($sfDeducaoForm->{'sfdeducaodtvenc' . $sfDeducaoCurrent->id  }) !!}</div>

                        <div class="col-md-3 label-title paddingTopButton">Data de Pagamento:</div>
                        <div class="col-md-3 paddingTopButton">
                            {!! form_row($sfDeducaoForm->{'sfdeducaodtpgtoreceb' . $sfDeducaoCurrent->id  }) !!}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 label-title paddingTopButton"></div>
                        <div class="col-md-3 paddingTopButton"></div>

                        <div class="col-md-3 label-title paddingTopButton">Valor do Item:</div>
                        <div class="col-md-3 paddingTopButton">{!! form_row($sfDeducaoForm->{'sfdeducaovlr' . $sfDeducaoCurrent->id  }) !!}</div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            @if($sfDeducaoForm->{'txtinscra'.$sfDeducaoCurrent->id})
                                {!! form_row($sfDeducaoForm->{'txtinscra'.$sfDeducaoCurrent->id}) !!}
                            @endif
                        </div>
                        @if($sfDeducaoCurrent->codsit && str_starts_with($sfDeducaoCurrent->codsit, 'DDR'))
                            <div class="col-md-3 paddingTopButton">
                                <div class="label-title">Municipio</div>
                                <select id="municipioDeducao{{$sfDeducaoCurrent->id}}" class="municipioSelect2 codigoMunicipio{{$sfDeducaoCurrent->id}}" onchange="alteraCodigoMunicipioDeducao({{$sfDeducaoCurrent->id}}, this, 'txtinscra')">
                                    <option value=""></option>
                                </select>
                            </div>
                        @else
                            <div class="col-md-3"></div>
                        @endif
                        <div class="col-md-6">
                            @if($sfDeducaoForm->{'txtinscrb'.$sfDeducaoCurrent->id})
                                {!! form_row($sfDeducaoForm->{'txtinscrb'.$sfDeducaoCurrent->id}) !!}
                            @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 paddingTopButton">
                            <label for="">Possui Acréscimo:</label>
                            {!! form_row($sfDeducaoForm->{'sfdeducaopossui_acrescimo'.$sfDeducaoCurrent->id}) !!}
                        </div>
                    </div>

                    <div class="align-table-items-center">
                        <div class="box box-solid box-primary{{$sfDeducaoCurrent->id}}">
                            <div id="deducao-item-recolhimento-wrapper{{$sfDeducaoCurrent->id}}">

                                @include(
                                    'vendor.backpack.base.mod.apropriacao.partials.acrescimo-deducao',
                                        [
                                        'sfdeducaoCurrent' => $sfDeducaoCurrent
                                        ]
                                )
                            </div>
                        </div>
                    </div>

                    <!-- LISTA DOS RECOLHEDORES -->
                    <div style="display: none">
                        {{ $totalVlr = 0 }}
                        {{ $totalMulta = 0 }}
                        {{ $totalJuros = 0 }}
                    </div>

                    <!-- Lista de Acrescimos -->
                    <div class="align-table-items-center">
                        <div class="custom-shadow box box-solid box-primary{{$sfDeducaoCurrent->id}}">
                            <div id="deducao-item-recolhimento-wrapper{{$sfDeducaoCurrent->id}}">

                                @include(
                                    'vendor.backpack.base.mod.apropriacao.partials.recolhedores-deducao',
                                        [
                                        'sfdeducaoCurrent' => $sfDeducaoCurrent
                                        ]
                                )
                            </div>
                        </div>
                    </div>

                    <!-- LISTA DOS RELACIONAMENTOS -->
                    <div class="custom-shadow box box-solid box-primary">
                        <div class="box-header with-border align-table-items-center">
                            <h3 class="box-title">Relacionamentos</h3>
                        </div>

                        <table class="table table-bordered table-hover table-condensed empenhos_relacionados{{$sfDeducaoCurrent->id}}">
                            <thead class="align-items-center">
                            <tr>
                                <th class="align-table-items-center">
                                    Situação
                                </th>
                                <th class="align-table-items-center">
                                    Número Empenho
                                </th>
                                <th class="align-table-items-center">
                                    Subelemento
                                </th>
                                <th class="align-table-items-center">
                                    Valor
                                </th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($sfDeducaoCurrent->relItemDeducao as $current_relItemDeducao)
                                @if($sfDeducaoForm->{'sfitemrelacionamentocodsit'.$sfDeducaoCurrent->id.$current_relItemDeducao->id })
                                    <tr>
                                        <td class="align-table-items-center">{!! form_row($sfDeducaoForm->{'sfitemrelacionamentocodsit' . $sfDeducaoCurrent->id.$current_relItemDeducao->id  }) !!}</td>
                                        <td class="align-table-items-center">{!! form_row($sfDeducaoForm->{'sfitemrelacionamentonumempe' . $sfDeducaoCurrent->id.$current_relItemDeducao->id  }) !!}</td>
                                        <td class="align-table-items-center">{!! form_row($sfDeducaoForm->{'sfitemrelacionamentosubelemento' . $sfDeducaoCurrent->id.$current_relItemDeducao->id  }) !!}</td>
                                        <td class="align-table-items-center">{!! form_row($sfDeducaoForm->{'sfitemrelacionamentovlr' . $sfDeducaoCurrent->id.$current_relItemDeducao->id  }) !!}</td>
                                        <td class="align-table-items-center">
                                            <button
                                                type="button"
                                                class="btn btn-primary btn-sm tooltipRemoverDeducaoRecolhimento
                                                                                esconderQuandoApropriacaoEmitidaSiafi"
                                                style="margin-right: 5px;"
                                                onclick="relacionarEmpenhoDeducao(
                                                    {{ $sfDeducaoCurrent->id }},
                                                {{$current_relItemDeducao->id }},
                                                true
                                                );"
                                            >
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>

                        <div class="box-body align-table-items-center">
                            <button type="button" onclick="verificaEmpenhosRelacionados({{$sfDeducaoCurrent->id}})" class="custom-shadow btn btn-primary esconderQuandoApropriacaoEmitidaSiafi" data-toggle="modal"
                                    data-target="#relacionamento-deducao-{{$sfDeducaoCurrent->id}}">
                                Relacionamento Manual
                            </button>
                        </div>
                    </div>

                    <div class="align-table-items-center">
                        <div class="custom-shadow box box-solid box-primary{{$sfDeducaoCurrent->id}}">
                            <div id="deducao-predoc-wrapper{{$sfDeducaoCurrent->id}}">

                                @include(
                                    'vendor.backpack.base.mod.apropriacao.partials.predoc-deducao',
                                        [
                                        'sfdeducaoCurrent' => $sfDeducaoCurrent
                                        ]
                                )
                            </div>
                        </div>
                    </div>

                    <div class="align-table-items-center">
                        <button class="custom-shadow btn btn-success esconderQuandoApropriacaoEmitidaSiafi"
                                name="confirma-dados-deducao"
                                value="confirmar"
                                type="submit"
                                id="confirma-dados-deducao-{{$sfDeducaoCurrent->id}}"><i class="custom-shadow fa fa-save"></i> Confirmar Dados Dedução</button>
                    </div>

                </div>

                <!-- Modal -->
                <div class="modal fade" id="relacionamento-deducao-{{$sfDeducaoCurrent->id}}" tabindex="-1" aria-labelledby="relacionamento-deducao-{{$sfDeducaoCurrent->id}}" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Relacionamento de Itens de Dedução</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="false">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="box-body">
                                    <table class="table table-bordered table-hover table-condensed">
                                        <thead class="align-items-center">
                                        <tr>
                                            <th></th>
                                            <th class="align-table-items-center">
                                                Situação
                                            </th>
                                            <th class="align-table-items-center">
                                                Nº Empenho
                                            </th>
                                            <th class="align-table-items-center">
                                                Subelemento
                                            </th>
                                            <th class="align-table-items-center">
                                                Valor
                                            </th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($sfDeducaoCurrent->relacionamentos as $relacionamento_current)
                                            <tr>
                                                <td>
                                                    <input type="checkbox"
                                                           class="chebox-relacionamento"
                                                           data-pcoitem-numseqitem="{{$relacionamento_current->numseqitem}}"
                                                           data-pco-numseqitem="{{$relacionamento_current->pco->numseqitem}}"
                                                           data-pco-sfpcoitem_id="{{$relacionamento_current->id ?? ''}}"
                                                           data-valorref-id="{{$relacionamento_current->id}}"
                                                           onclick="verificaEmpenhosSelecionados({{$sfDeducaoCurrent->id}})"
                                                    >
                                                </td>
                                                <td class="align-table-items-center">
                                                    {{ $relacionamento_current->pco->codsit ?? ''}}
                                                </td>
                                                <td class="align-table-items-center">
                                                    {{ $relacionamento_current->numempe ?? ''}}
                                                </td>
                                                <td class="align-table-items-center">
                                                    {{ $relacionamento_current->subelemento->codigo ?? ''}}
                                                </td>
                                                <td class="align-table-items-center">
                                                    {{ number_format($relacionamento_current->vlr, 2, ',', '.') ?? ''}}
                                                </td>
                                                <td><a class="btn btn-info btn-sm toggle-details">Detalhes</a></td>
                                            </tr>

                                            <tr class="details-row" style="display: none;">
                                                @php
                                                    $relacionamento_apropriacao_empenho = $relacionamento_current->contrato_fatura_empenho_apropriacao;
                                                    $numeroEmpenho = $relacionamento_apropriacao_empenho->empenho->numero ?? null;
                                                    $fornecedorEmpenho = $relacionamento_apropriacao_empenho->empenho->fornecedor_id ?? null;
                                                    $unidadeEmpenho = $relacionamento_apropriacao_empenho->empenho->unidade_id ?? null;

                                                    if(!is_null($numeroEmpenho) && !is_null($fornecedorEmpenho) && !is_null($unidadeEmpenho)){
                                                       $celular_orcamento = $relacionamento_apropriacao_empenho->getCelulaOrcamentaria($numeroEmpenho, $fornecedorEmpenho, $unidadeEmpenho);
                                                    }
                                                @endphp
                                                <td></td>
                                                <td>
                                                    <div>
                                                        <strong>Célula Orçamentária:</strong>
                                                        <p>{{$celular_orcamento ?? ''}}</p>
                                                    </div>
                                                </td>

                                                <td>
                                                    <div>
                                                        <strong>PTRES:</strong>
                                                        <p>{{isset($relacionamento_apropriacao_empenho->empenho) ? $relacionamento_apropriacao_empenho->empenho->ptres ?? '' : ''}}</p>
                                                    </div>
                                                </td>

                                                <td>
                                                    <div>
                                                        <strong>Fonte:</strong>
                                                        <p>{{isset($relacionamento_apropriacao_empenho->empenho) ? $relacionamento_apropriacao_empenho->empenho->fonte ?? '' : ''}}</p>
                                                    </div>
                                                </td>

                                                <td>
                                                    <div>
                                                        <strong>ND:</strong>
                                                        <p>{{isset($relacionamento_apropriacao_empenho->empenho) ? $relacionamento_apropriacao_empenho->empenho->naturezaDespesa->codigo ?? '' : ''}}</p>
                                                    </div>
                                                </td>

                                                <td>
                                                    <div>
                                                        <strong>UGR PI:</strong>
                                                        <p>{{isset($relacionamento_apropriacao_empenho->empenho) ? $relacionamento_apropriacao_empenho->empenho->planoInterno->codigo ?? '' : ''}}</p>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                <button type="button" onclick="relacionarEmpenhoDeducao({{$sfDeducaoCurrent->id}})" class="btn btn-primary">Relacionar</button>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>

{{--<fieldset style="display: flex; flex-direction: row">--}}
{{--    <div>--}}
{{--        {!! form_row($sfDeducaoForm->btnSubmitFormDeducao)  !!}--}}
{{--    </div>--}}
{{--    <div class="mensagens-erro-abadeducao p-l-20" style="flex: 1; align-self: center"></div>--}}
{{--</fieldset>--}}
{!! form_end($sfDeducaoForm) !!}
