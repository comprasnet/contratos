{!! form_start($sfDadosPagamentoForm) !!}
<div class="box box-solid box-primary">
    <div class="box-header with-border align-table-items-center">
        <h3 class="box-title">Grupo Principal</h3>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-3 label-title paddingTopButton">Código da UG pagadora:</div>
            <div class="col-md-2 paddingTopButton codugpgto">{{ $sfDadosBasicos->codugpgto }}</div>
            <div class="col-md-3 label-title paddingTopButton">Nome da UG pagadora:</div>
            <div class="col-md-4 paddingTopButton nomeugpgto">{{ $sfDadosBasicos->nomeugpgto }}</div>
        </div>
        <div class="row">
            <div class="col-md-3 label-title paddingTopButton">Data de Vencimento:</div>
            <div class="col-md-2 paddingTopButton">{{ $sfDadosBasicos->dtvenc }}</div>
            <div class="col-md-3 label-title paddingTopButton">Data de Pagamento:</div>
            <div class="col-md-4 paddingTopButton">{!! form_row($sfDadosPagamentoForm->dtpgtoreceb) !!}</div>
        </div>
    </div>
</div>
<div class="box box-solid box-primary">
    <div class="box-header with-border align-table-items-center">
        <h3 class="box-title">Lista de Favorecidos</h3>
    </div>
    <div class="box-body">
        <table class="table table-bordered table-hover table-condensed">
            <thead class="align-items-center">
                <tr>
                    <th class="align-table-items-center">
                        Favorecido
                    </th>
                    <th class="align-table-items-center">
                        Valor
                    </th>
                    <th class="align-table-items-center">
                        Pré-Doc
                    </th>
                </tr>
            </thead>
            <tbody>

                @if(count($arrFavorecidos) > 0)
                @foreach ($arrFavorecidos as $favorecido)
                <input type="hidden" name="codcredordevedor[]" value="{{$favorecido->codcredordevedor}}">
                <tr>
                    <td class="align-table-items-center">
                        @if($favorecido->dadosAntecipaGov)
                        {{ $favorecido->codcredordevedor }}
                        @else
                        <div style="display: flex; align-items: center; justify-content: center;">
                            {!! form_row($sfDadosPagamentoForm->codcredordevedor) !!}
                        </div>
                        @endif
                    </td>
                    <td class="align-table-items-center">
                        {{ number_format($favorecido->vlr, 2, ',', '.') }}
                    </td>

                    <td class="align-table-items-center">
                        @if($favorecido->dadosAntecipaGov)
                        <input type="hidden" name="antecipagov_id" value="{{$favorecido->dadosAntecipaGov->id}}">
                        <span
                            class="label label-success pointer-hand"
                            data-toggle="modal"
                            data-target="#modal-predoc-aba-dados-pagamento{!! $favorecido->dadosAntecipaGov->id !!}"> <i class="fa fa-check"></i> Pré-Doc
                        </span>
                        @include('vendor.backpack.base.mod.apropriacao.modal-predoc-aba-dados-pagamento',
                        ['dadosAntecipaGov' => $favorecido->dadosAntecipaGov,]
                        )
                        @else
                        <div style="display: flex; align-items: center; justify-content: center;">
                            <span style="flex-shrink: 0;  padding: 5px 10px"
                                class="label label-primary pointer-hand"
                                data-toggle="modal"
                                data-target="#modal-form-predoc-aba-dados-pagamento{!! $favorecido->id !!}"> <i class="fa fa-check"></i> Pré-Doc
                            </span>
                            <input type="hidden" id="formPreDoc" name="formPreDoc">
                            @include('vendor.backpack.base.mod.apropriacao.modal-form-predoc-aba-dados-pagamento',
                            ['dadosPreDoc'  => $favorecido,
                            'sfDadosBasicos'=> $sfDadosBasicos,
                            'sfPreDoc'      => $sfPreDoc]
                            )
                            @if($sfPreDoc && ($situacaoApropriacao === 'AND' || $situacaoApropriacao === 'ERR'))
                                <a
                                    class="btn btn-box-tool"
                                    style="color: red;font-size: initial;"
                                    title="Remover Pré-Doc"
                                    id="btnRemoverPredoc"
                                ><i class="fa fa-trash"></i></a>
                            @endif
                        </div>
                        @endif
                    </td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="3" style="text-align: center">
                        Nenhum favorecido encontrado
                    </td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
<fieldset class="form-group">
    {!! form_row($sfDadosPagamentoForm->btnSubmitFormSfDadosPagamento) !!}
</fieldset>
{!! form_end($sfDadosPagamentoForm) !!}

@push('after_scripts')
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-mask-plugin@1.14.16/dist/jquery.mask.min.js"></script>
@endpush
