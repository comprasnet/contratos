<script type="text/javascript">

    /**
     * Ao iniciar a pagina verifica se existe valor selecionado para o tipo dh padrao
     * caso SIM então habilita o botao 'Confirmar Dados Básicos'
     */
    function verificarSelectTipoDhPadraoComValor() {
        var elSelectTipoDhPadrao = $('#select-tipo-dh-padrao').val();
        if (!elSelectTipoDhPadrao) {

            $('#btnSubmitFormSfDadosBasicos').attr('disabled', true);   //remove o disable do botao
        }
    }

    function atualizaCredorApropriacao(element) {
        var credor_devedor = element.value.trim();
        var numericValue = credor_devedor.replace(/\D/g, '');

        if (
            numericValue.length === 6 ||
            numericValue.length === 14 ||
            (credor_devedor.toUpperCase().startsWith("EX") &&
                (credor_devedor.length === 2 || !/[A-Z]/i.test(credor_devedor[2])))
        ) {
            var route = '{{route('apropriacao.faturas.alterar.credor.dados.basicos.ajax')}}',
                apropriacao_id = {{ \Route::current()->parameter('apropriacao_id' )}},
                sf_padrao_id = {{ \Route::current()->parameter('sf_padrao_id' )}};

            axios.post(route, {
                sf_padrao_id,
                apropriacao_id,
                credor_devedor
            })
                .then(response => {

                    if (response.data?.error) {
                        new PNotify({
                            title: 'Error',
                            text: response.data.error,
                            type: "error"
                        });
                    }

                    if (response.data.fornecedor_nome?.length > 0) {
                        $('#nome-fornecedor-credor').text(response.data.fornecedor_nome)

                        new PNotify({
                            title: 'Sucesso',
                            text: 'Credor alterado com sucesso.',
                            type: "success"
                        });
                    }

                    if (response.data.cnpj?.length > 0) {
                        $('#codcredordevedor-dados-basicos').val(response.data.cnpj)
                    }
                })
                .catch(error => {
                    new PNotify({
                        title: 'Error',
                        text: 'Erro ao alterar código do credor.',
                        type: "error"
                    });
                })
                .finally(function () {
                })

        }
    }

    $('#codugpgto').select2({
        width: '100%'
    })

    if (!$('#select-tipo-dh-padrao').prop('readonly')) {
        $('#select-tipo-dh-padrao').select2({
            width: '100%',
            height: '100%'
        });
    }
</script>
