<script type="text/javascript">

    function funCentroCusto(){
        submitFormAjax($("#sfCentroCustoForm"), 'centro-custo-tab')
    }

    function habilitarDesabilitarBotaoConfirmarCentroCusto()
    {
        if ($('.count-centro-custo').length > 0) {
            $('.btnConfirmarCentroCusto').removeAttr('disabled');
            return
        }
        $('.btnConfirmarCentroCusto').attr('disabled', true);
    }

    function validarValoresEmpenhosSelecionados(element, numseqpai, numseqitem) {
        var elementRowVlr = $('.vlr' + numseqpai + numseqitem),
            vlrEmpenho = elementRowVlr.first().data('valor-pcoitem'),
            vlrEmpenhoFormat = elementRowVlr.first().data('valor-pcoitem').replace(/\./g, '').replace(',', '.');

        var valorTotalPreenchido = elementRowVlr.toArray().reduce(function (totalValue, currentEl, currentIndex) {

            var currentValue = '';

            if (typeof currentEl.value === 'undefined' || currentEl.value === '') {
                currentValue = 0;
            } else {
                currentValue = parseFloat(currentEl.value.replace(/\./g, '').replace(',', '.'));
            }

            return parseFloat(totalValue) + currentValue;
        }, 0);

        if (parseFloat(valorTotalPreenchido.toFixed(2)) > parseFloat(vlrEmpenhoFormat)) {

            //mensagem de avisao
            Swal.fire(
                'Ops!',
                'O valor excede o total permitido (' + vlrEmpenho + ')!',
                'warning'
            )

            //zera valor
            element.value = '';

        }
    }

    /**
     * se idCentroCusto for null então criar novo centro de custo
     * se idCentroCusto tiver valor então remover centro de custo pelo id
     *
     * @param idCentroCusto
     * @param idCentroCustoParaCopia se fornecido então copia o centro de custo
     */
    function clickBtnRemoverOuCriarNovoCentroCusto(idCentroCusto = null, idCentroCustoParaCopia = null)
    {
        $('#btn-add-novo-centro-custo-wrapper').html(
            '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>'
        );

        if(idCentroCusto !== null)
        {
            /****nesse caso esta deletando um centro de custo então remove os elementos referente a ele****/
            $('#codcentrocusto' + idCentroCusto).remove();
            $('#mesreferencia' + idCentroCusto).remove();
            $('#anoreferencia' + idCentroCusto).remove();
            $('#codsiorg' + idCentroCusto).remove();
            $('#codugbenef' + idCentroCusto).remove();
        }

        var codcentrocusto = idCentroCustoParaCopia ? $('#codcentrocusto' + idCentroCustoParaCopia).val() : '',
            mesreferencia = idCentroCustoParaCopia ? $('#mesreferencia' + idCentroCustoParaCopia).val() : '',
            anoreferencia = idCentroCustoParaCopia ? $('#anoreferencia' + idCentroCustoParaCopia).val() : '',
            codugbenef = idCentroCustoParaCopia ? $('#codugbenef' + idCentroCustoParaCopia).val() : '',
            codsiorg = idCentroCustoParaCopia ? $('#codsiorg' + idCentroCustoParaCopia).val() : '',
            numseqitem = parseInt($('.count-centro-custo').last().data('count-centro-custo')) + 1,
            route = '/apropriacao/fatura-form/novo/centrocusto/ajax',
            sf_padrao_id = {{ \Route::current()->parameter('sf_padrao_id' )}},
            apropriacao_id = {{ \Route::current()->parameter('apropriacao_id' )}},
            formAbaCentroCusto = $('#form-centro-custo-header').closest('form'),
            formData = formAbaCentroCusto.serialize();

        axios.post(route, {
            idCentroCusto,
            codcentrocusto,
            mesreferencia,
            anoreferencia,
            codugbenef,
            codsiorg,
            numseqitem,
            sf_padrao_id,
            apropriacao_id,
            formData
        })
            .then(response => {

                $('#centro-custo').html(response.data);
                funCentroCusto()
                initTooltipsCentroCusto();

                //nescessario fazer o binding da acao dos collapse novamente, pois com a chamada do ajax ela se perde
                $('.box').boxWidget({
                    animationSpeed: 500,
                    collapseTrigger: '[data-widget="collapse"]',
                    removeTrigger: '[data-widget="remove"]',
                    collapseIcon: 'fa-minus',
                    expandIcon: 'fa-plus',
                    removeIcon: 'fa-times'
                })

                atualizarValoresLabelInformativa();

            })
            .catch(error => {
                new PNotify({
                    title: 'Error',
                    text: 'Erro ao criar novo centro de custo',
                    type: "error"
                });
            })
            .finally(function () {
                transformarBotaoDeEnvioAoSiafi();
                habilitarDesabilitarBotaoConfirmarCentroCusto();
            })
    }

    function atualizarValoresLabelInformativa() {

        var valorCentroCustoAInformar = $('#valorCentroCustoAInformar').val(),
            valorCentroCustoInformado = 0;

        $('.checkbox-numseqpai').each(function (index, item) {

            /*se checkado somar valor do centro de custo informado*/
            if (item.checked) {

                var indexField = item.id;
                var valorRow = $('#vlr' + indexField).val().replace(/\./g, '').replace(',', '.');
                valorRow = !isNaN(parseFloat(valorRow)) ? parseFloat(valorRow) : 0;

                valorCentroCustoInformado += valorRow;

            }
        });

        var valorTotalApropriado = valorCentroCustoInformado;


        $('#valorCentroCustoAInformar').val(valorCentroCustoAInformar.toLocaleString('pt-br', {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        }));

        $('#valorCentroCustoInformado').val(valorCentroCustoInformado.toLocaleString('pt-br', {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        }));

        $('#valorTotalApropriado').val(valorTotalApropriado.toLocaleString('pt-br', {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        }));
    }

    function habilitarDesabilitarBotaoEnviarSiafi()
    {
        var valorCentroCustoAInformar = parseFloat($('#valorCentroCustoAInformar').val().replace(/\./g, '').replace(',', '.'));

        var valorCentroCustoInformado = parseFloat($('#valorCentroCustoInformado').val().replace(/\./g, '').replace(',', '.'));

        if(valorCentroCustoAInformar > 0)
        {
            if(valorCentroCustoInformado < valorCentroCustoAInformar)
            {
                $('#btnEnviarApropriacaoSiafi').attr('disabled', true);
            }
        }
    }

    function selecionaTodosCentrosCusto(element) {

        if (element.checked) {
            $('.checkbox-numseqpai').each(function (index, item) {
                item.checked = true;
            })

        } else {
            $('.checkbox-numseqpai').each(function (index, item) {
                item.checked = false;
            });
        }
    }

    function initTooltipsCentroCusto()
    {
        $('.tooltipRemoverCentroCusto').tooltip(
            {
                title: "Remover esse item",
                animation: true,
                placement: 'bottom',
                html: true,
                trigger: 'hover'
            }
        );

        $('.tooltipCopiarCentroCusto').tooltip(
            {
                title: "Criar uma cópia desse item",
                animation: true,
                placement: 'bottom',
                html: true,
                trigger: 'hover'
            }
        );

        $('.tooltip-btn-add-novo-centro-custo').tooltip(
            {
                title: "Criar novo",
                animation: true,
                placement: 'bottom',
                html: true,
                trigger: 'hover'
            }
        );
    }

</script>
