<script>
    $(document).ready(function () {

    });

    function clickBtnRemoverOuCriarNovaParcela(idPco = null, idParcela = null) {

        var route = '/apropriacao/fatura-form/novo/sfparcela/ajax',
            txtobser_pco = `<?php echo $txtobser_pco ?? '' ?>`,
            sf_padrao_id = {{ \Route::current()->parameter('sf_padrao_id' )}},
            apropriacao_id = {{ \Route::current()->parameter('apropriacao_id' )}},
            formAbaPco = $('#nova-aba-situacao' + idPco).closest('form'),
            formData = formAbaPco.serialize();

        $('#btn-add-novo-cron-baixa-patrimonial' + idPco).html(
            '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>'
        )

        axios.post(route, {
            sf_padrao_id,
            apropriacao_id,
            idPco,
            idParcela,
            txtobser_pco,
            formData
        })
            .then(response => {
                $('#pco').html(response.data);
                atualizarValoresParcela(idPco);

                //nescessario fazer o binding da acao dos collapse novamente, pois com a chamada do ajax ela se perde
                $('.box').boxWidget({
                    animationSpeed: 500,
                    collapseTrigger: '[data-widget="collapse"]',
                    removeTrigger: '[data-widget="remove"]',
                    collapseIcon: 'fa-minus',
                    expandIcon: 'fa-plus',
                    removeIcon: 'fa-times'
                })

                //necessário colocar mascara novamente nos campos
                initMascaraCamposVariaveis();
                focusOutCamposVariaveisSfPcoItem();

                /*como os elementos estão sendo criado novamente com ajax atribui novamente os eventos*/
                initCssBtnsNotaEmpenho();
                initCssBorderBoxNode();
            })
            .catch(error => {

                new PNotify({
                    title: 'Error',
                    text: 'Erro na geração/remoção de parcela',
                    type: "error"
                });
            })

        iniciaFuncaoPcoBaixaPatrimonial()
    }

    function iniciaFuncaoPcoBaixaPatrimonial() {
        submitFormAjax($('#sfDadosPcoForm'), 'pco-tab')
    }

    function exibirOuEsconderCampoCronogramaBaixaPatrimonial(element, idPco) {

        /*caso o campo seja selecionado como 'SIM'*/
        if ($('#' + element.id).val() === '1') {
            $('#cronograma-baixa-patrimonial-wrapper' + idPco).css('display', 'block');
            atualizarValoresParcela(idPco);
        }

        /*caso o campo seja selecionado como 'NÃO'*/
        if ($('#' + element.id).val() === '0') {

            var countParcela = parseInt($('#cronograma-baixa-patrimonial-container' + idPco).data('count-parcelas'));

            /*habilita o botao de submit da aba nesse caso ele n precisa ser validado*/
            $('#btnSubmitAbaPco').attr('disabled', false);

            /*remove mensagem de erro ao lado do bota de submit*/
            $('#valorTotalParcelaInformado' + idPco).closest('.input-group').removeClass('has-error');
            $('.mensagens-erro-aba-pco').html('');

            /*caso sfpco tenha parcelas e o campo 'Despesa Antecipada?' seja não, entao remove parcelas da tabela*/
            if (countParcela > 0) {
                removerParcelasCronogramaBaixaPatrimonial(idPco, element.id);
            }

            $('#cronograma-baixa-patrimonial-wrapper' + idPco).css('display', 'none');

            $('[name="sfpcoitem[]"]').each(function () {

                $(`[name="numclassc[${$(this).val()}]"]`).remove()

            })

            iniciaFuncaoPcoBaixaPatrimonial()
        }
    }

    function removerParcelasCronogramaBaixaPatrimonial(idPco) {

        var route = '/apropriacao/fatura-form/remover/parcelas/cronbaixapatrimonial/ajax',
            txtobser_pco = `<?php echo $txtobser_pco ?? '' ?>`,
            sf_padrao_id = {{ \Route::current()->parameter('sf_padrao_id' )}},
            apropriacao_id = {{ \Route::current()->parameter('apropriacao_id' )}};

        axios.post(route, {
            idPco,
            sf_padrao_id,
            apropriacao_id,
            txtobser_pco
        })
            .then(response => {
                $('#pco').html(response.data);
                iniciaFuncaoPcoBaixaPatrimonial()

                //nescessario fazer o binding da acao dos collapse novamente, pois com a chamada do ajax ela se perde
                $('.box').boxWidget({
                    animationSpeed: 500,
                    collapseTrigger: '[data-widget="collapse"]',
                    removeTrigger: '[data-widget="remove"]',
                    collapseIcon: 'fa-minus',
                    expandIcon: 'fa-plus',
                    removeIcon: 'fa-times'
                })

                //necessário colocar mascara novamente nos campos
                initMascaraCamposVariaveis();
                //focusOutCamposVariaveisSfPcoItem();

                /*como os elementos estão sendo criado novamente com ajax atribui novamente os eventos*/
                //initCssBtnsNotaEmpenho();
                initCssBorderBoxNode();
                new PNotify({
                    title: 'Sucesso',
                    text: 'Removido parcelas do Cronograma de Baixa Patrimonial',
                    type: "success"
                });
            })
            .catch(error => {
                new PNotify({
                    title: 'Error',
                    text: 'Erro ao remover parcelas do Cronograma de Baixa Patrimonial',
                    type: "error"
                });
            })
    }

    function atualizarValoresParcela(idPco) {

        /*caso o campo seja selecionado como 'SIM'*/
        if ($('#despesaantecipada' + idPco).val() === '1') {
            var valorTotalParcelaAInformar = 0;
            var valorParcelasInformado = 0;

            $('input[data-vlr-situacao=' + idPco + ']').each(function (index, elementvalor) {
                var indexField = elementvalor.id;
                var valorRow = $('#' + indexField).val().replace(/\./g, '').replace(',', '.');
                valorRow = !isNaN(parseFloat(valorRow)) ? parseFloat(valorRow) : 0;

                valorTotalParcelaAInformar += valorRow;

            }).val();

            $('input[data-vlr-parcela-situacao=' + idPco + ']').each(function (index, elementvalor) {
                var indexField = elementvalor.id;
                var valorRow = $('#' + indexField).val().replace(/\./g, '').replace(',', '.');
                valorRow = !isNaN(parseFloat(valorRow)) ? parseFloat(valorRow) : 0;

                valorParcelasInformado += valorRow;

            }).val();

            valorTotalParcelaAInformar = $('#valorTotalParcelaAInformar' + idPco).val(valorTotalParcelaAInformar.toLocaleString('pt-br', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));

            valorParcelasInformado = $('#valorTotalParcelaInformado' + idPco).val(valorParcelasInformado.toLocaleString('pt-br', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));

            /***habilita ou desabilita botao de submit do form***/
            $('#btnSubmitAbaPco').attr('disabled', valorTotalParcelaAInformar.val() !== valorParcelasInformado.val());

            /*exibe mensagem de erro ao lado do bota de submit*/
            if (valorTotalParcelaAInformar.val() !== valorParcelasInformado.val()) {

                var codSitPco = $('#aba-sfpco' + idPco).html();

                /*adiciona classe de erro no input*/
                $('#valorTotalParcelaInformado' + idPco).closest('.input-group').addClass('has-error');

                $('.mensagens-erro-aba-pco').html('<span class="label label-danger">' +
                    'na situacao' + codSitPco + 'no Cronograma de Baixa Patrimonial o Total Informado deve ser igual ' +
                    'ao Total a Informar' +
                    '</span>');
            }

            /*remove mensagem de erro ao lado do bota de submit*/
            if (valorTotalParcelaAInformar.val() === valorParcelasInformado.val()) {
                $('#valorTotalParcelaInformado' + idPco).closest('.input-group').removeClass('has-error');

                $('.mensagens-erro-aba-pco').html('');
            }
        }

        iniciaFuncaoPcoBaixaPatrimonial()
    }

</script>
