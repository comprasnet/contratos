<script type="text/javascript">

    function removerMensagensDeErrodosInputGroup()
    {
        $('.input-group .text-danger').remove();
    }



    function initMascaraCamposVariaveis() {

        $.mask.definitions["9"] = null;
        $.mask.definitions["X"] = "[0-9]";
        let titleMessage = "Em atendimento ao item 3 da IN nº205 de 08/04/1988 -" +
            " ... o registro de entrada do material será sempre no Almoxarifado - " +
            "a conta de estoque interno é fixa. O valor do bem adquirido só irá para conta contábil patrimonial" +
            " após a requisição pela unidade de alocação.";

        $('.camposVariaveisSfPcoItem').each(function (index, elemento) {
            if (elemento.dataset.mascara) {
                $('#' + elemento.id).mask(elemento.dataset.mascara);
                if(elemento.dataset.mascara == '1.2.3.1.1.08.01'){
                    $(this).parent().prepend("<i class='fa fa-info-circle' title='" + titleMessage + "'></i>");
                }
            }
        });

        var restricaoInputs = $('input.camposVariaveisSfPcoItem[data-restricao="Realização GRU"]');

        restricaoInputs.each(function() {
            $(this).mask('XXXXX-X');
        });
    }

    function apagarCamposVariaveisDeSfPcoItem() {
        $('.camposVariaveisSfPcoItem').val('');
    }

    const focusOutCamposVariaveisSfPcoItem = () => {
        $('.camposVariaveisSfPcoItem').focusout(function() {
            let valorAtual = $(this).val();
            if (valorAtual.includes('_')) {
                $(this).val('0');
            }
        });
    };

    function initTooltipsEmpenho()
    {
        $('.tooltipRemoverPcoItem').tooltip(
            {
                title: "Remover esse item",
                animation: true,
                placement: 'bottom',
                html: true,
                trigger: 'hover'
            }
        );

        $('.tooltipCopiarPcoItem').tooltip(
            {
                title: "Criar uma cópia desse item",
                animation: true,
                placement: 'bottom',
                html: true,
                trigger: 'hover'
            }
        );

        $('.tooltip-btn-add-novo-pco-item').tooltip(
            {
                title: "Criar novo",
                animation: true,
                placement: 'bottom',
                html: true,
                trigger: 'hover'
            }
        );
    }

    function bindCampoSituacaoNaAba(element) {
        $(element).data("nome-aba");
        var id_aba = $(element).data("nome-aba");

        if (element.value === '') {
            $('#' + id_aba).html(' - ');
        } else {
            $('#' + id_aba).html(element.value);
        }
    }

    function clickBtnNovaAbaSituacao(idPco = null, delete_sfpco = false) {

        var sf_padrao_id = {{ \Route::current()->parameter('sf_padrao_id' )}},
            apropriacao_id = {{ \Route::current()->parameter('apropriacao_id' )}},
            txtobser_pco = $('#txtobser_pco').text(),
            numseqitem = parseInt($('.count-situacao').data('count-situacao') + 1),
            route = '/apropriacao/fatura-form/novo/sfpco/ajax',
            htmlBtnAddNovoPcoItemWrapper = $('#nova-aba-situacao' + idPco).html(),
            empenhos_ids = [],
            encontrouFalse = false;

        $('.sfpcoApropriacaoConfirmaDados').each(function () {
            if ($(this).val() === 'false') {
                encontrouFalse = true;
                return false;
            }
        })

        if (encontrouFalse && !delete_sfpco) {
            return new PNotify({
                title: 'Atenção',
                text: 'Para criar nova situação é necessário confirmar os dados.',
                type: "warning"
            });
        }


        $('#nova-aba-situacao').html(
            '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>'
        )

        $('input[name^="idsempenho"]').each(function() {
            empenhos_ids.push($(this).val());
        });

        axios.post(route, {
            delete_sfpco,
            sf_padrao_id,
            apropriacao_id,
            idPco,
            txtobser_pco,
            numseqitem,
            empenhos_ids
        })
            .then(response => {

                if (response.data.status === 'error') {
                    Swal.fire('Alerta!', response.data.mensagem, 'error');

                    $('#nova-aba-situacao').html(
                        htmlBtnAddNovoPcoItemWrapper
                    )
                }

                if (response.data.status !== 'error') {
                    $('#pco').html(response.data);
                    iniciaFuncaoPco()
                }

                //nescessario fazer o binding da acao dos collapse novamente, pois com a chamada do ajax ela se perde
                $('.box').boxWidget({
                    animationSpeed: 500,
                    collapseTrigger: '[data-widget="collapse"]',
                    removeTrigger: '[data-widget="remove"]',
                    collapseIcon: 'fa-minus',
                    expandIcon: 'fa-plus',
                    removeIcon: 'fa-times'
                })

                abaAtivaPco(true);

            })
            .catch(error => {

                new PNotify({
                    title: 'Error',
                    text: 'Erro ao criar nova aba de situação',
                    type: "error"
                });
            })
    }

    function createOrUpdateSfPcoSituacaoViaAjax(element, idSfPco) {

        var sf_padrao_id = {{ \Route::current()->parameter('sf_padrao_id' )}},
            apropriacao_id = {{ \Route::current()->parameter('apropriacao_id' )}},
            txtobser_pco = $('#txtobser_pco').text(),
            codsit = element.value;

        route = '/apropriacao/fatura-form/atualizar/criarouatualizarcodsitsfpadrao/ajax';

        axios.post(route, {
            sf_padrao_id,
            apropriacao_id,
            idSfPco,
            txtobser_pco,
            codsit
        })
            .then(response => {

                $('#pco').html(response.data);
                iniciaFuncaoPco()

                //nescessario fazer o binding da acao dos collapse novamente, pois com a chamada do ajax ela se perde
                $('.box').boxWidget({
                    animationSpeed: 500,
                    collapseTrigger: '[data-widget="collapse"]',
                    removeTrigger: '[data-widget="remove"]',
                    collapseIcon: 'fa-minus',
                    expandIcon: 'fa-plus',
                    removeIcon: 'fa-times'
                })

                //necessário colocar mascara novamente nos campos
                initMascaraCamposVariaveis();
                focusOutCamposVariaveisSfPcoItem();

                /*como os elementos estão sendo criado novamente com ajax atribui novamente os eventos*/
                initCssBtnsNotaEmpenho();
                initCssBorderBoxNode();

            })
            .catch(error => {

                new PNotify({
                    title: 'Error',
                    text: 'Erro ao criar nova aba de situação',
                    type: "error"
                });
            })
    }

    function repetirContasEmpenho(idPco, campo) {
        if (!idPco) return;

        let repetirContasEmpenhos = $('#repetircontaempenho' + idPco).val();
        if (repetirContasEmpenhos == 1) {
            let repetirNumClass = null;
            let primeiroCampo = null;

            // Encontrar o primeiro campo preenchido
            $('[name="sfpcoitem[]"]').each(function () {
                const elementoId = $(this).val();
                const campoAtual = $('#' + campo + elementoId + idPco);
                repetirNumClass = campoAtual.val();

                if (repetirNumClass && !primeiroCampo) {
                    primeiroCampo = campoAtual;
                    return false;
                }
            });

            if (!repetirNumClass || !primeiroCampo) return;

            // Atualiza outros campos ao alterar o primeiro
            primeiroCampo.off('change').on('change', function () {
                const novoValor = $(this).val();
                if (novoValor) {
                    $('[name="sfpcoitem[]"]').each(function () {
                        const numClassABCampo = $('#' + campo + $(this).val() + idPco);
                        if (!numClassABCampo.data('manual-edit') && numClassABCampo.val() === '') {
                            numClassABCampo.val(novoValor);
                        }
                    });
                }
            });

            // Inicializa os campos com o valor do primeiro
            $('[name="sfpcoitem[]"]').each(function (index) {
                const numClassABCampo = $('#' + campo + $(this).val() + idPco);
                if (!numClassABCampo.val() && index !== 0) {
                    numClassABCampo.val(repetirNumClass);
                }
            });
        }
        // Funções adicionais
        initMascaraCamposVariaveis();
        focusOutCamposVariaveisSfPcoItem();
    }

    function updateSfPcoIndrtemcontrato(element, idSfPco, numSeq) {
        if (numSeq !== 1) { return; }

        var sf_padrao_id = {{ \Route::current()->parameter('sf_padrao_id' )}},
            apropriacao_id = {{ \Route::current()->parameter('apropriacao_id' )}},
            repetirContasEmpenhos = $('#repetircontaempenho' + idSfPco).val(),
            txtobser_pco = $('#txtobser_pco').text(),
            indrtemcontrato = element.value;

        route = '/apropriacao/fatura-form/atualizar/atualizarindrtemcontratosfpadrao/ajax';

        axios.post(route, {
            sf_padrao_id,
            apropriacao_id,
            idSfPco,
            repetirContasEmpenhos,
            txtobser_pco,
            indrtemcontrato
        })
            .then(response => {

                $('#pco').html(response.data);
                iniciaFuncaoPco()

                //nescessario fazer o binding da acao dos collapse novamente, pois com a chamada do ajax ela se perde
                $('.box').boxWidget({
                    animationSpeed: 500,
                    collapseTrigger: '[data-widget="collapse"]',
                    removeTrigger: '[data-widget="remove"]',
                    collapseIcon: 'fa-minus',
                    expandIcon: 'fa-plus',
                    removeIcon: 'fa-times'
                })

                //necessário colocar mascara novamente nos campos
                initMascaraCamposVariaveis();
                focusOutCamposVariaveisSfPcoItem();

                /*como os elementos estão sendo criado novamente com ajax atribui novamente os eventos*/
                initCssBtnsNotaEmpenho();
                initCssBorderBoxNode();

            })
            .catch(error => {

                new PNotify({
                    title: 'Error',
                    text: 'Erro ao criar nova aba de situação',
                    type: "error"
                });
            })
    }

    function updateSfPcoNumClass(element, idSfPco, numSeq) {

        if (numSeq !== 1) { return; }

        var sf_padrao_id = {{ \Route::current()->parameter('sf_padrao_id' )}},
            apropriacao_id = {{ \Route::current()->parameter('apropriacao_id' )}},
            txtobser_pco = $('#txtobser_pco').text(),
            numclass = element.value.replace(/\D/g, '')
        formAbaPco = $('#nova-aba-situacao' + idSfPco).closest('form'),
            formData = formAbaPco.serialize();

        route = '/apropriacao/fatura-form/atualizar/atualizarnumclasssfpadrao/ajax';

        if(numclass.length === 9){

            axios.post(route, {
                sf_padrao_id,
                apropriacao_id,
                idSfPco,
                txtobser_pco,
                numclass,
                formData
            }).then(response => {

                $('#pco').html(response.data);
                iniciaFuncaoPco()

                //nescessario fazer o binding da acao dos collapse novamente, pois com a chamada do ajax ela se perde
                $('.box').boxWidget({
                    animationSpeed: 500,
                    collapseTrigger: '[data-widget="collapse"]',
                    removeTrigger: '[data-widget="remove"]',
                    collapseIcon: 'fa-minus',
                    expandIcon: 'fa-plus',
                    removeIcon: 'fa-times'
                })

                //necessário colocar mascara novamente nos campos
                initMascaraCamposVariaveis();
                focusOutCamposVariaveisSfPcoItem();

                /*como os elementos estão sendo criado novamente com ajax atribui novamente os eventos*/
                initCssBtnsNotaEmpenho();
                initCssBorderBoxNode();

            })
                .catch(error => {

                    new PNotify({
                        title: 'Error',
                        text: 'Erro ao criar nova aba de situação',
                        type: "error"
                    });
                })
        }
        return;
    }

    function clickBtnRemoverOuCriarNovoPcoItem(
        idPco = null,
        idPcoItem = null,
        idPcoItemParaCopia = null
    ) {

        var numseqpai = parseInt($('#aba-sfpco' + idPco).data('numseqitem'));

        var sf_padrao_id = {{ \Route::current()->parameter('sf_padrao_id' )}},
            apropriacao_id = {{ \Route::current()->parameter('apropriacao_id' )}},
            txtobser_pco = $('#txtobser_pco').text(),
            //se idPcoItem for false é uma inserção então soma os numseqitem para incluir um novo com sequencial
            //caso idPcoItem for true então é um delete e pega o numseqitem do attribute data
            numseqitem = !idPcoItem ? parseInt($('.count-pco-item').last().data('count-pco-item')) + 1 : parseInt($('#' + idPcoItem).data('numseqitem')),
            route = '/apropriacao/fatura-form/novo/sfpcoitem/ajax',
            //variaveis de pcoitem
            //se tiver id para copia manda o empenho para ser copiado se não manda o empenho preenchido no campo
            numempe = idPcoItemParaCopia ? $('#numempe' + idPcoItemParaCopia + idPco).val() : $('#numempe' + idPcoItem + idPco).val(),
            contratofatura_id = idPcoItemParaCopia ? $('#contratofatura_id' + idPcoItemParaCopia + idPco).val() : $('#contratofatura_id' + idPcoItem + idPco).val(),
            codsubitemempe = idPcoItemParaCopia ? $('#codsubitemempe' + idPcoItemParaCopia + idPco).val() : $('#codsubitemempe' + idPcoItem + idPco).val(),
            vlr = idPcoItemParaCopia ? $('#vlr' + idPcoItemParaCopia + idPco).val().replace(/\./g, '').replace(',', '.') : '',
            numclassa = idPcoItemParaCopia ? $('#numclassa' + idPcoItemParaCopia + idPco).val() : '',
            numclassb = idPcoItemParaCopia ? $('#numclassb' + idPcoItemParaCopia + idPco).val() : '',
            htmlBtnAddNovoPcoItemWrapper = $('#btn-add-novo-pco-item-wrapper' + idPco).html(),
            repetirContasEmpenhos = $('#repetircontaempenho' + idPco).val(),
            formAbaPco = $('#nova-aba-situacao' + idPco).closest('form'),
            repetirNumClassA = '',
            repetirNumClassB = '',
            formData = formAbaPco.serialize();

        if (repetirContasEmpenhos) {
            $('[name="sfpcoitem[]"]').each(function (index, element) {
                repetirNumClassA = $('#numclassa' + $(element).val() + idPco).val()
                repetirNumClassB = $('#numclassb' + $(element).val() + idPco).val()

                if (repetirNumClassA && repetirNumClassB) {
                    return false;
                }
            });
        }


        $('#btn-add-novo-pco-item-wrapper' + idPco).html(
            '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>'
        )

        axios.post(route, {
            sf_padrao_id,
            apropriacao_id,
            idPco,
            idPcoItem,
            numempe,
            codsubitemempe,
            vlr,
            numclassa,
            numclassb,
            numseqpai,
            numseqitem,
            contratofatura_id,
            repetirContasEmpenhos,
            formData,
            repetirNumClassA,
            repetirNumClassB,
            txtobser_pco
        })
            .then(response => {

                if (response.data.status === 'error') {
                    Swal.fire('Alerta!', response.data.mensagem, 'error');

                    $('#btn-add-novo-pco-item-wrapper' + idPco).html(
                        htmlBtnAddNovoPcoItemWrapper
                    )
                }
                if (response.data.status !== 'error') {
                    $('#pco').html(response.data);
                    iniciaFuncaoPco()
                }

                //nescessario fazer o binding da acao dos collapse novamente, pois com a chamada do ajax ela se perde
                $('.box').boxWidget({
                    animationSpeed: 500,
                    collapseTrigger: '[data-widget="collapse"]',
                    removeTrigger: '[data-widget="remove"]',
                    collapseIcon: 'fa-minus',
                    expandIcon: 'fa-plus',
                    removeIcon: 'fa-times'
                })

                //nescessario fazer o binding da acao dos tooltips novamente, pois com a chamada do ajax ela se perde
                initTooltipsEmpenho();
                //necessário colocar mascara novamente nos campos
                initMascaraCamposVariaveis();
                focusOutCamposVariaveisSfPcoItem();

                /*como os elementos estão sendo criado novamente com ajax atribui novamente os eventos*/
                initCssBtnsNotaEmpenho();
                initCssBorderBoxNode();
                initOnClickDropdownInstrumentosDaApropriacao();

            })
            .catch(error => {

                new PNotify({
                    title: 'Error',
                    text: 'Erro ao criar nova aba de situação',
                    type: "error"
                });
            })
            .finally(function () {
                //$(".collapse").collapse();
            })
    }

    function valorNovoVinculoIsHasSelected(idSfPcoItem, elementoValorRefCampoNovoValor)
    {
        var inputGroup = elementoValorRefCampoNovoValor.closest('.input-group');

        // Verificar se o elemento '.input-group' possui dentro dele um elemento 'button'
        var hasButton = inputGroup.find('button').length > 0;

        if (hasButton) {
            // Buscar o elemento 'button'
            var buttonElement = inputGroup.find('button');

            // Verificar se o elemento 'button' possui um elemento 'span'
            var hasSpan = buttonElement.find('span').length > 0;

            if (hasSpan) {
                // Buscar o elemento 'span' dentro do 'button'
                var spanElement = buttonElement.find('span');

                // Verificar se o elemento 'span' possui um elemento 'i'
                return spanElement.find('i').length === 0;
            }
        }

    }

    function validarInputsParaAtualizarInstrumentoDeCobrancaEmpenho(idSfPcoItem, eventFromOnkeyupEmpenho)
    {

        /*se o evento veio do campo de empenho então trata-se de consulta e nao precisa validar*/
        if(eventFromOnkeyupEmpenho)
        {
            return false;
        }

        var booCampoVazio = false,
            elementoValorRefCampoNovoValor = $('input[name^="valorref' + idSfPcoItem + '"]:last'),
            ValueElementoValorRef = elementoValorRefCampoNovoValor.val(),
            elementosValorRefSemCampoNovoValor = $('input[name^="valorref' + idSfPcoItem + '"]:not(:last)'),
            elementosValorRefComCampoNovoValor = $('input[name^="valorref' + idSfPcoItem + '"]');

        /*valida input de valor da nota vazio*/
        elementosValorRefSemCampoNovoValor.each(function() {
            var valorRef = $(this).val(),
                checkboxIsChecked = $(this).closest('.input-group')
                    .find('.checkbox-inst-cobranca' + idSfPcoItem + ' input[type="checkbox"]').is(':checked');

            if(checkboxIsChecked)
            {
                if(valorRef === '' || valorRef === '0,00')
                {
                    new PNotify({
                        title: 'Error',
                        text: 'O valor da nota deve ser preenchido',
                        type: "error"
                    });

                    booCampoVazio = true;
                }
            }
        });

        /*valido o campo de novo vinculo de nota com o empenho*/
        if (
            valorNovoVinculoIsHasSelected(idSfPcoItem, elementoValorRefCampoNovoValor) &&
            ValueElementoValorRef === '' ||
            ValueElementoValorRef === '0,00'
        ) {
            new PNotify({
                title: 'Error',
                text: 'O valor da nota deve ser preenchido',
                type: "error"
            });

            booCampoVazio = true;
        }

        if (elementosValorRefComCampoNovoValor.length === 1) {

            var unicoElementoValorRefCampoNovoValor = $('input[name^="valorref' + idSfPcoItem + '"]:first');

            if(!valorNovoVinculoIsHasSelected(idSfPcoItem, unicoElementoValorRefCampoNovoValor))
            {
                new PNotify({
                    title: 'Atenção',
                    text: 'Selecione pelo o menos um instrumento de cobrança',
                    type: "alert"
                });

                booCampoVazio = true;
            }
        }

        if (ValueElementoValorRef &&
            elementosValorRefComCampoNovoValor.length !== 1 &&
            !valorNovoVinculoIsHasSelected(idSfPcoItem, elementoValorRefCampoNovoValor)) {
            new PNotify({
                title: 'Atenção',
                text: 'Foi informado o valor sem instrumento de cobrança selecionado.',
                type: "error"
            });

            booCampoVazio = true;
        }

        return booCampoVazio;
    }

    /**
     * Ajax para recarregar a tela de centro de custo
     */
    function reloadingCentroCusto() {
        var sf_padrao_id = {{ \Route::current()->parameter('sf_padrao_id' )}},
            routeReloadingCentroCusto = '/apropriacao/fatura-form/recarregar/centrocusto/ajax',
            apropriacao_id = {{ \Route::current()->parameter('apropriacao_id' )}};

        axios.post(routeReloadingCentroCusto, {
            sf_padrao_id,
            apropriacao_id,
        })
            .then(response => {
                $('#centro-custo').html(response.data);
            })
            .catch(error => {

            })
            .finally(function () {

            })
    }

    function atualizarInstrumentosDeCobrancaDoEmpenho(idSfPcoItem, idSfPco, eventFromOnkeyupEmpenho) {

        var booCampoVazio = validarInputsParaAtualizarInstrumentoDeCobrancaEmpenho(
            idSfPcoItem,
            eventFromOnkeyupEmpenho
        );

        if(booCampoVazio){
            return false;
        }

        var sf_padrao_id = {{ \Route::current()->parameter('sf_padrao_id' )}},
            apropriacao_id = {{ \Route::current()->parameter('apropriacao_id' )}},
            route = '/apropriacao/fatura-form/updateorcreate/instrumentocobrancaempenho/ajax',
            numeroEmpenho = $('#numempe' + idSfPcoItem + idSfPco).val(),
            idSubelemento = $('#codsubitemempe' + idSfPcoItem + idSfPco).val(),
            numseqpai = parseInt($('#aba-sfpco' + idSfPco).data('numseqitem')),
            numseqitem = !idSfPcoItem ? parseInt($('.count-pco-item').last().data('count-pco-item')) + 1 :
                parseInt($('#' + idSfPcoItem).data('numseqitem')),
            instcobranca_ids = [],
            empenhos_ids = [],
            valorref = [],
            subelemento = $('#codsubitemempe'+ idSfPcoItem + idSfPco).val().replace(/^0+/, ''),
            checkboxFaturaEmpenho = [];

        $('input[name^="idsinstcobranca' + idSfPcoItem + '"]').each(function() {
            instcobranca_ids.push($(this).val());
        });

        $('input[name^="idsempenho' + idSfPcoItem + '"]').each(function() {
            empenhos_ids.push($(this).val());
        });

        $('input[name^="valorref' + idSfPcoItem + '"]').each(function() {
            valorref.push($(this).val());
        });

        $('input[name^="checkboxFaturaEmpenho' + idSfPcoItem + '"]').each(function() {
            checkboxFaturaEmpenho.push($(this).is(':checked'));
        });

        $('#collapse' + idSfPcoItem + ' .box-body').after("<div class='overlay overlay" + idSfPcoItem + "'>" +
            "<i class='fa fa-refresh fa-spin'></i></div>");


        axios.post(route, {
            apropriacao_id,
            sf_padrao_id,
            numeroEmpenho,
            idSubelemento,
            instcobranca_ids,
            empenhos_ids,
            valorref,
            numseqpai,
            numseqitem,
            checkboxFaturaEmpenho,
            idSfPcoItem,
            eventFromOnkeyupEmpenho,
            subelemento,
            idSfPco
        })
            .then(response => {

                /*caso a ação venha do botao salvar entao exibe mensagem*/
                if(!eventFromOnkeyupEmpenho)
                {
                    if(response.data.response.status === 'success'){
                        new PNotify({
                            title: 'Sucesso',
                            text: response.data.response.message,
                            type: "success"
                        });
                        /*realiza ações na aba centro de custo*/
                        reloadingCentroCusto();
                    }
                }

                if(!eventFromOnkeyupEmpenho)
                {
                    if(response.data.response.status === 'error')
                    {
                        new PNotify({
                            title: 'Error',
                            text: response.data.response.message,
                            type: "error"
                        });
                    }
                }

                $('.box-body' + idSfPcoItem).html(response.data.view);

                /*como os elementos estão sendo criado novamente com ajax atribui novamente os eventos*/
                initOnClickDropdownInstrumentosDaApropriacao();
                initCssBorderBoxNode();
                alterarWidthElementInstrumentosDaApropriacao(idSfPcoItem);

                /*atualiza o valor do campo vlr de sfpcoitem*/
                atualizarValorCampoVlrSfpcoitem(idSfPcoItem, idSfPco);

                atualizarValoresParcela(idSfPco);
                iniciaFuncaoPco()

                /*remove loading*/
                $('.overlay' + idSfPcoItem).remove();

            })
            .catch(error => {
                /*remove loading*/
                $('.overlay' + idSfPcoItem).remove();
                /*fecha collapse*/
                $('#collapse' + idSfPcoItem).collapse('hide');
            })
            .finally(function () {

            })
    }

    /**
     * chamado no onchange dos campos Tem Contrato? e Despesa Antecipada?
     * Retorna booleando indicando se é para exibir ou nao o campo variável de acordo com a restricao dele
     * @param element
     * @param idSfPco
     * @param elementName (Contrato, Despesa Antecipada)
     */
    function exibirOuEsconderCampoVariavel(idSfPco, element, elementName) {
        var value = element.value,
            displayValue = value === '1' ? 'block' : 'none',
            fornecedorSemMascara = $('#fornecedor_sem_mascara').val();

        $('.camposVariaveisSfPcoItem' + idSfPco).each(function (index, elemento) {
            if (elemento.dataset.restricao === elementName) {

                var valorDoElemento = '';

                /*caso a seleção seja 'SIM'*/
                if(value === '1')
                {
                    valorDoElemento = elemento.value;
                }

                $('#' + elemento.id).val(valorDoElemento);
                $('#' + elemento.id).css('display', displayValue);
                $('.' + elemento.id).css('display', displayValue);
            }

            if(elementName === 'Contrato' && elemento.dataset.rotulo === 'Favorecido do Contrato')
            {
                var valorFavorecidoContrato = '';

                /*caso a seleção seja 'SIM' no select de 'Tem Contrato'*/
                if(value === '1')
                {
                    valorFavorecidoContrato = fornecedorSemMascara;
                }
                $('#' + elemento.id).val(valorFavorecidoContrato);
            }
        });

        iniciaFuncaoPco()
    }

    function buscarSubelementoDoEmpenho(empenhoElement, subelementoElement, idSfPcoItem, idSfPco, changeUgEmitenteContrato = false) {

        if (empenhoElement.value.length === 12) {
            codUg = $('#codugempe'+idSfPco).val()

            var route = `/apropriacao/fatura-form/buscar/subelemento/ajax/${empenhoElement.value}/${codUg}`,
                apropriacao_id = {{ \Route::current()->parameter('apropriacao_id' )}};

            $.ajax({
                url: route,
                type: 'GET',
                data: { apropriacao_id },
                beforeSend: function(){
                    $('.subelemento-progress-bar' + subelementoElement.id).css('display', 'block');
                },
                success: function (result) {
                    var arrKeySubElemento = Object.keys(result);
                    /*preenche o option do select com o subelemento do empenho*/
                    if(arrKeySubElemento.length === 0){
                        $('#' + subelementoElement.id).html('<option value="">NÃO ENCONTRADO</option>');
                    }else{

                        var option = arrKeySubElemento.map(function (currentValue, index, arr) {
                            return '<option value="' + currentValue + '">' + result[currentValue] + '</option>'
                        });

                        if (!changeUgEmitenteContrato) {
                            $('#' + subelementoElement.id).html(option);
                        } else {

                            buscaValorSubElementoEmpenho(idSfPco, idSfPcoItem).then(function (subId) {
                                $('#' + subelementoElement.id).html(option);
                                if (subId && $('#' + subelementoElement.id + ' option[value="' + subId + '"]').length > 0) {
                                    $('#' + subelementoElement.id).val(subId);
                                }
                            }).catch(function (error) {
                                $('#' + subelementoElement.id).html(option);
                            });
                        }

                    }
                    iniciaFuncaoPco()
                },
                error: function (result) {
                    new PNotify({
                        title: 'Atenção',
                        text: 'Erro ao buscar subelemento do empenho',
                        type: "alert"
                    });
                },
                complete: function(){
                    $('.subelemento-progress-bar'+ subelementoElement.id).css('display', 'flex');

                    if(!changeUgEmitenteContrato) {
                        atualizarInstrumentosDeCobrancaDoEmpenho(idSfPcoItem, idSfPco, true);
                    }
                }
            });
        }else{
            $('#' + subelementoElement.id).html('<option value="">NÃO ENCONTRADO</option>');
        }
    }

    /**
     * Altera o css das bordas do box de selecao de nota do empenho
     * @param idElementoCollapse
     */
    function initCssBorderBoxNode(idElementoCollapse)
    {
        $('.box-node').on('show.bs.collapse', function () {
            $('.box-node').css(
                {
                    'border-left': 'solid 1px ',
                    'border-right': 'solid 1px ',
                    'border-bottom': 'solid 1px ',
                    'border-color': '#d2d6de'
                }
            );
        });
    }

    /**
     * Apenas determinando mesmo width dos inputs que gerenciam o vinculo de nota e emepenho
     */
    function initCssBtnsNotaEmpenho()
    {
        $('.box-node').each(function() {
            var idSfpcoItem = $(this).data('idsfpcoitem');

            $('.box-node' + idSfpcoItem).on('shown.bs.collapse', function () {
                alterarWidthElementInstrumentosDaApropriacao(idSfpcoItem);
            });
        });
    }

    /**
     * Pega o maior width dos instrumentos de cobranca e seta ele como width dos demais
     * @param idSfpcoItem
     */
    function alterarWidthElementInstrumentosDaApropriacao(idSfpcoItem) {

        var maxWidth = 0,
            maxWidthElement,
            checkBoxInstCobranca = $('.checkbox-inst-cobranca' + idSfpcoItem);

        checkBoxInstCobranca.each(function () {
            var currentWidth = $(this).width();

            if (currentWidth > maxWidth) {
                maxWidth = currentWidth;
                maxWidthElement = this;
            }
        });

        if (maxWidth !== 0) {
            $('.btn-novo-inst-cobranca' + idSfpcoItem).width(maxWidth);
            checkBoxInstCobranca.width(maxWidth);
        }
        else{
            $('.btn-novo-inst-cobranca' + idSfpcoItem).width('auto');
        }
    }

    function initOnClickDropdownInstrumentosDaApropriacao()
    {
        $('.dropdown-menu a').on('click', function (event) {
            // Evita o comportamento padrão (navegar para uma nova página)
            event.preventDefault();

            var selectedValue = $(this).data('value');
            var idPcoItem = $(this).data('sfpcoitemid');
            var selectedText = $(this).html(); // Alterado para manter o HTML, incluindo o ícone

            // Atualiza o texto do botão principal e o valor do input oculto
            $('.selected-option'+ idPcoItem).html(selectedText);
            $('#idsinstcobranca' + idPcoItem).val(selectedValue);
        });
    }

    function atualizarValorCampoVlrSfpcoitem(idSfpcoitem, idSfpco)
    {
        var sumValorNotaEmpenho = $('.valorref' + idSfpcoitem).toArray().reduce(function (totalValue, currentEl, currentIndex) {

            var currentValue = '';

            if (typeof currentEl.value === 'undefined' || currentEl.value === '') {
                currentValue = 0;
            } else {
                currentValue = parseFloat(currentEl.value.replace(/\./g, '').replace(',', '.'));
            }

            return parseFloat(totalValue) + currentValue;
        }, 0);

        $('#vlr' + idSfpcoitem + idSfpco).val(sumValorNotaEmpenho.toLocaleString('pt-br', {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        }));

        $('#label-valor-sfpcoitem' + idSfpcoitem).html("R$: " + sumValorNotaEmpenho.toLocaleString('pt-br', {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        }));
    }

    function buscaValorSubElementoEmpenho(idSfPco, idSfPcoItem) {
        return new Promise(function(resolve, reject) {
            var route = `/apropriacao/fatura-form/buscar/subelementoitem/ajax/${idSfPco}/${idSfPcoItem}`;

            $.ajax({
                url: route,
                type: 'GET',
                success: function(result) {
                    resolve(result);
                    iniciaFuncaoPco()
                },
                error: function(result) {
                    reject(result);
                }
            });
        });
    }

    function abaAtivaPco(forceLastTab = false) {
        const sf_padrao_id = {{ \Route::current()->parameter('sf_padrao_id') }};
        const localStorageKey = `activeTabPco${sf_padrao_id}`;
        const localStorageTimeKey = `activeTabPcoTime${sf_padrao_id}`;
        const expirationTime = 120;

        const isStorageValid = () => {
            const savedTime = parseInt(localStorage.getItem(localStorageTimeKey), 10);
            return savedTime && (new Date().getTime() - savedTime) / 1000 <= expirationTime;
        };

        const salvarTabAtiva = (tabId) => {
            localStorage.setItem(localStorageKey, tabId);
            localStorage.setItem(localStorageTimeKey, new Date().getTime().toString());
        };

        const ativarUltimaTab = () => {
            const ultimaTab = $('#pco a[data-toggle="tab"]').last();
            if (ultimaTab.length) {
                ultimaTab.tab('show');
                salvarTabAtiva(ultimaTab.attr('id'));
            }
        };

        if (forceLastTab) {
            ativarUltimaTab();
        } else {
            const activeTabId = localStorage.getItem(localStorageKey);
            if (activeTabId && isStorageValid()) {
                $(`#pco a#${activeTabId}`).tab('show');
            } else {
                localStorage.removeItem(localStorageKey);
                localStorage.removeItem(localStorageTimeKey);
            }
        }

        $('#pco a').on('shown.bs.tab', function (e) {
            salvarTabAtiva(e.target.id); // Salva a aba ativa no evento
        });
    }

    function alteraUnidadeEmitenteEmpenho(element, idPco) {

        if (element.value.length == 6 && situacaoSiafi !== 'APR') {

            var sf_padrao_id = {{ \Route::current()->parameter('sf_padrao_id' )}},
                codUgEmpenho = element.value,
                route = "{{ route('apropriacao.faturas.atualizacodugemitempenho.ajax') }}";

            $('input[id^="codsubitemempecadastrado"]').each(function() {
                if (this.id.endsWith(idPco)) {
                    $(this).trigger('keyup');
                }
            });

            $.ajax({
                url: route,
                data: {
                    'sf_padrao_id': sf_padrao_id,
                    'codUgEmpenho': codUgEmpenho,
                    'pcoId': idPco
                },
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.mensagem) {
                        $('#nomeugempePco' + idPco).text(data.nomeUnidade)

                        new PNotify({
                            title: 'Atenção',
                            delay: 3000,
                            text: data.mensagem,
                            type: "success"
                        });
                    } else {
                        new PNotify({
                            title: 'Atenção',
                            delay: 3000,
                            text: data.error,
                            type: "alert"
                        });
                    }

                },
                error: function () {
                    new PNotify({
                        title: 'Atenção',
                        delay: 3000,
                        text: 'Não foi possivel alterar a unidade emitente do empenho.',
                        type: "alert"
                    });
                }
            });

        }
    }

    function iniciaFuncaoPco() {
        submitFormAjax($('#sfDadosPcoForm'), 'pco-tab')
    }
</script>
