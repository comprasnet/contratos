@php

    $idPadrao                             = \Route::current()->parameter('sf_padrao_id');
    $apropriacaoId                        = \Route::current()->parameter('apropriacao_id');
    $numAbaLiberada                       = session()->get('arrPadraoAlteracaoApropriacao')[$idPadrao]['numAbaLiberada'];
    $routeRemoverApropriacao              = route('remover.apropriacao.fatura', [$idPadrao, $apropriacaoId, null]);
    $routeEnviarApropriacaoSiafi          = route('apropriacao.fatura.enviar.siafi');
    $routeCancelarApropriacaoSiafi        = route('apropriacao.fatura.cancelar.siafi', ['sfpadrao_id' => $idPadrao]);
    $routePagamentoInstCobranca           = route('crud.apropriacao.index', $idFaturasToRedirectBack);

    function verificaAbaConfirmada($colecao = null){
        if($colecao == null){
            return false;
        }
      return  in_array(true, array_column($colecao->toArray(),'confirma_dados'), true);
    }
@endphp

@extends('backpack::layout')

@section('header')
    <section class="content-header">
        <h1>
            Apropriação de instrumento de cobrança
            <small>Formulário de alteração de instrumento de cobrança</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
            <li>Fatura</li>
            <li class="active">Formulário de alteração</li>
        </ol>
        <a
            href="{{ $routePagamentoInstCobranca }}"
            class="hidden-print"
            style="margin-bottom: 10px"
        ><i class="fa fa-angle-double-left"></i> Voltar para lista de apropriação de instrumento de cobrança</a>
    </section>
@endsection

@section('content')

    <div class="modal fade" id="modal-default" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Confirmação para envio ao SIAFI</h4>
                </div>
                <div class="modal-body">
                    <p>Apropriação sem informação de Centro de Custo, deseja continuar?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fechar</button>
                    <button type="button" id="btnEnviarApropriacaoSiafiModal" class="custom-shadow btn btn-success">Apropriar SIAFI</button>
                </div>
            </div>
        </div>
    </div>

    <!--caso o sfpadrao não tenha codtipodh exibe mensagem sobre tipo DH-->
    @if( !!$arrayCamposDHPadrao['codtipodh'] === false || $arrayCamposDHPadrao['codtipodh'] === "  ")
        <div class="callout callout-info collapse-mensagem-tipo-dh in" style="margin-bottom: 20px!important;"
             id="mensagemImportanteSobreTipoDh">
            <h4><i class="fa fa-info"></i> Importante:</h4><span style="font-weight: 400;">Após selecionar o tipo DH ele não poderá ser alterado</span>
        </div>
    @endif


    <div class="box box-primary" style="border-top: 3px solid #3c8dbc"> <!--colocando border-top pois não está vindo como no AdminLte-->
        <div class="box-header with-border">
            <h1 class="box-title" style="font-size: 15px;padding-left: 15px">Selecione o DH para iniciar</h1>

            @if($situacaoApropriacao === 'AND')
                <div class="box-tools pull-right esconderQuandoApropriacaoEmitidaSiafi">
                    <a
                        class="btn btn-box-tool"
                        href="{{ $routeRemoverApropriacao }}"
                    >
                        <i class="fa fa-trash tooltipRemoverApropriacaoFatura" style="color: red;font-size: initial;">

                        </i>
                    </a>
                </div>
            @endif

        </div>
        <div class="box-body">
            <div class="form-group col-xs-4 required" id="campo-tipo-dh-padrao">

                <label>Tipo DH Padrão</label>

                <!--caso o sfpadrao não tenha codtipodh salvo mostra campo select para escolher-->
                @if( !!$arrayCamposDHPadrao['codtipodh'] === false || $arrayCamposDHPadrao['codtipodh'] === "  ")

                    <select
                        class="form-control"
                        id="select-tipo-dh-padrao"
                        onchange="onChangeSelectTipoDhPadrao(this);"
                    >
                        <option value="" id="option-selecione"> == Selecione ==</option>

                        @foreach($arrayCamposDHPadrao['arrayTiposDHPadrao'] as $key => $value)
                            <option value="{{ $key }}"
                                    @if(!!$arrayCamposDHPadrao['codtipodh'] && $arrayCamposDHPadrao['codtipodh'] === $key) selected @endif>
                                {{$key}} - {{$value}}
                            </option>
                        @endforeach

                    </select>
                    <!--caso o sfpadrao já possua codtipodh então exibe em um campo text com readonly-->
                @elseif(array_key_exists($arrayCamposDHPadrao['codtipodh'], $arrayCamposDHPadrao['arrayTiposDHPadrao']))
                    <input class="form-control"
                           readonly
                           id="select-tipo-dh-padrao"
                           value="{{ $arrayCamposDHPadrao['codtipodh'] }} - {{ $arrayCamposDHPadrao['arrayTiposDHPadrao'][$arrayCamposDHPadrao['codtipodh']] }}"
                           style="font-size: 12px"
                    >
                @elseif(!array_key_exists($arrayCamposDHPadrao['codtipodh'], $arrayCamposDHPadrao['arrayTiposDHPadrao']))
                    <input class="form-control"
                           readonly
                           id="select-tipo-dh-padrao"
                           value="{{ $arrayCamposDHPadrao['codtipodh'] }}"
                           style="font-size: 12px"
                    >
                @endif
            </div>

            <div class="form-group col-xs-4 required">
                <label> UG Pagadora </label>
                <input class="form-control" readonly
                       id="ugEmitente"
                       onchange="onChangeCodugEmit(this)"
                       value="({{ $arrayCamposDHPadrao['codugemit'] }}) {{ $arrayCamposDHPadrao['nomeugemit'] }} "
                       style="font-size: 12px"
                >
            </div>

            <div class="form-group col-xs-4 required">
                <label>Ano </label>
                <input class="form-control" readonly value="{{ $arrayCamposDHPadrao['anodh'] }} ">
            </div>
        </div>
    </div>


    <div class="nav-tabs-custom" id="abas-alteracao-apropriacao">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" id="abas-apropriacao" role="tablist" aria-controls="">
            <li role="presentation" class="{{ $numaba === 1 || $numaba === 0 ? 'active' : '' }} pointer-hand">
                <a
                    data-target="#dados-basico"
                    id="dados-basicos-tab"
                    aria-controls="dados-basico"
                    role="tab"
                    data-toggle="tab"
                    aria-expanded="{{ $numaba === 1 || $numaba === 0 ? 'true' : 'false' }}"
                >
                    Dados Básicos
                </a>
            </li>
            <li role="presentation" class="{{ $numaba === 2 ? 'active' : '' }} pointer-hand">
                <!--erifica se a aba esta liberada ou se a situacao da aproriacao é igual a Siafi Enviado-->
                {{--                @if((int) $numAbaLiberada >= 2 || $situacaoApropriacao === 'APR' || verificaAbaConfirmada($sfPco ?? null))--}}
                <a
                    data-target="#pco"
                    id="pco-tab"
                    aria-controls="pco"
                    role="tab"
                    data-toggle="tab"
                    aria-expanded="{{ $numaba === 2 ? 'true' : 'false' }}"
                >
                    Principal Com
                    Orçamento
                </a>
                {{--                @else--}}
                {{--                    <a--}}
                {{--                        href="#"--}}
                {{--                        class="tooltip-abas-alteracao-apropriacao"--}}
                {{--                    >--}}
                {{--                        Principal Com--}}
                {{--                        Orçamento--}}
                {{--                    </a>--}}
                {{--                @endif--}}
            </li>

            <li role="presentation" class="{{ $numaba === 3 ? 'active' : '' }} pointer-hand">
                {{--                @if((int) $numAbaLiberada >= 3 || $situacaoApropriacao === 'APR' || verificaAbaConfirmada($sfDeducao ?? null))--}}
                <a
                    data-target="#deducao"
                    id="deducao-tab"
                    aria-controls="deducao"
                    role="tab"
                    data-toggle="tab"
                    aria-expanded="{{ $numaba === 3 ? 'true' : 'false' }}">
                    Dedução
                </a>
                {{--                @else--}}
                {{--                    <a--}}
                {{--                        href="#"--}}
                {{--                        class="tooltip-abas-alteracao-apropriacao"--}}
                {{--                    >--}}
                {{--                        Dedução--}}
                {{--                    </a>--}}
                {{--                @endif--}}
            </li>

            <li role="presentation" class="{{ $numaba === 4 ? 'active' : '' }} pointer-hand">
                {{--                @if((int) $numAbaLiberada >= 4 || $situacaoApropriacao === 'APR' || verificaAbaConfirmada($arrFavorecidos ?? null))--}}
                <a
                    data-target="#dados-pagamento"
                    id="dados-pagamento-tab"
                    aria-controls="dados-pagamento"
                    role="tab"
                    data-toggle="tab"
                    aria-expanded="{{ $numaba === 4 ? 'true' : 'false' }}">
                    Dados de pagamento
                </a>
                {{--                @else--}}
                {{--                    <a--}}
                {{--                        href="#"--}}
                {{--                        class="tooltip-abas-alteracao-apropriacao"--}}
                {{--                    >--}}
                {{--                        Dados de pagamento--}}
                {{--                    </a>--}}
                {{--                @endif--}}
            </li>
            <li role="presentation" class="{{ $numaba === 5 ? 'active' : '' }} pointer-hand">
                {{--                @if($arrayCamposDHPadrao['utiliza_custos'] && (int) $numAbaLiberada >= 5 || $situacaoApropriacao === 'APR' || verificaAbaConfirmada($sfCentroCusto ?? null))--}}
                <a
                    data-target="#centro-custo"
                    id="centro-custo-tab"
                    aria-controls="centro-custo"
                    role="tab"
                    data-toggle="tab"
                    aria-expanded="false">
                    Centro
                    de Custo
                </a>
                {{--                @endif--}}
                {{--                @if($arrayCamposDHPadrao['utiliza_custos'] && (int) $numAbaLiberada < 5 && $situacaoApropriacao !== 'APR' && !verificaAbaConfirmada($sfCentroCusto ?? null))--}}
                {{--                    <a--}}
                {{--                        href="#"--}}
                {{--                        class="tooltip-abas-alteracao-apropriacao"--}}
                {{--                    >--}}
                {{--                        Centro--}}
                {{--                        de Custo--}}
                {{--                    </a>--}}
                {{--                @endif--}}
                @if(!$arrayCamposDHPadrao['utiliza_custos'])
                    <a
                        href="#"
                        class="tooltip-unidade-nao-utiliza-custos"
                    >
                        Centro
                        de Custo
                    </a>
                @endif
            </li>
            <li class="navbar-right aba-btn-apropriar-siafi">

                @if($situacaoApropriacao === 'AND' || $situacaoApropriacao === 'ERR')
                    <button
                        id="btnEnviarApropriacaoSiafiDiretamente"
                        class="custom-shadow btn btn-success hide"
                    >
                        <i class="custom-shadow fa fa-save"></i>
                        Apropriar SIAFI
                    </button>

                    <button
                        id="btnAbrirModalConfirmacao"
                        data-toggle="modal"
                        data-target="#modal-default"
                        class="custom-shadow btn btn-success hide"
                    >
                        <i class="custom-shadow fa fa-save"></i>
                        Apropriar SIAFI
                    </button>

                @elseif($situacaoApropriacao === 'APR')
                    <button
                        id="btnCancelarApropriacaoSiafi"
                        class="btn btn-danger"
                    >
                        <i class="custom-shadow fa fa-save"></i>
                        Cancelar SIAFI
                    </button>
                @endif
            </li>
        </ul>

        <div class="tab-content">
            <!--ABA DADOS BASICOS-->
            <div role="tabpanel" class="tab-pane {{ $numaba === 1 || $numaba === 0 ? 'active active fade in' : 'fade' }} fade in" id="dados-basico" aria-labelledby="dados-basicos-tab">
                {{--                @include(--}}
                {{--                        'vendor.backpack.base.mod.apropriacao.aba-dados-basicos',--}}
                {{--                        [--}}
                {{--                            'sf_dados_basicos' => $sfDadosBasicos,--}}
                {{--                            'sfDadosBasicosForm' => $sfDadosBasicosForm,--}}
                {{--                        ]--}}
                {{--                     )--}}
            </div>
            <!--ABA PRINCIPAL COM ORÇAMENTO-->
            <div role="tabpanel" class="tab-pane {{ $numaba === 2 ? 'active fade in' : 'fade' }}" id="pco" aria-labelledby="pco-tab">
                {{--                @include('vendor.backpack.base.mod.apropriacao.aba-principal-com-orcamento',--}}
                {{--                   [--}}
                {{--                   'sfPco' => $sfPco,--}}
                {{--                   'txtobser_pco' => $sfDadosBasicos->txtobser,--}}
                {{--                   'sfpcoForm' => $sfpcoForm,--}}
                {{--                   'valorTotalAbaPco' => $valorTotalAbaPco,--}}
                {{--                   'idPcoActive' => null--}}
                {{--                    ])--}}
            </div>

            <!--ABA DADOS DE DEDUÇÃO-->
            <div role="tabpanel" class="tab-pane {{ $numaba === 3 ? 'active fade in' : 'fade' }}" id="deducao" aria-labelledby="deducao-tab">
                {{--                @include('vendor.backpack.base.mod.apropriacao.aba-deducao',--}}
                {{--                    [--}}
                {{--                         'sfDeducaoForm' => $sfDeducaoForm,--}}
                {{--                         'sfDeducao' => $sfDeducao,--}}
                {{--                         'idDeducaoActive' => null--}}
                {{--                    ])--}}
            </div>

            <!--ABA DADOS DE PAGAMENTO-->
            <div role="tabpanel" class="tab-pane {{ $numaba === 4 ? 'active fade in' : 'fade' }}" id="dados-pagamento" aria-labelledby="dados-pagamento-tab">
                {{--@include('vendor.backpack.base.mod.apropriacao.aba-dados-pagamento',
                    [
                     'sfDadosPagamentoForm' => $sfDadosPagamentoForm,
                     'sfDadosBasicos' => $sfDadosBasicos,
                     'nomeugpgto' => $sfDadosBasicos->nomeugpgto,
                     'arrFavorecidos' => $arrFavorecidos
                    ])--}}
            </div>


            <!--ABA CENTRO DE CUSTO-->
            <!--SÓ EXIBE A ABA CENTRO DE CUSTO SE A UNIDADE DO DHPADRAO TIVER O CAMPO AFETA_CUSTO = TRUE-->

            @if($arrayCamposDHPadrao['utiliza_custos'])
                <div role="tabpanel" class="tab-pane {{ $numaba === 5 ? 'active fade in' : 'fade' }}" id="centro-custo"
                     aria-labelledby="centro-custo-tab">
                    {{--                    @include('vendor.backpack.base.mod.apropriacao.aba-centro-de-custo',--}}
                    {{--                        [--}}
                    {{--                           'sfCentroCustoForm' => $sfCentroCustoForm,--}}
                    {{--                           'sfCentroCusto' => $sfCentroCusto,--}}
                    {{--                           'valorTotalCentroCustoAInformar' => $valorTotalCentroCustoAInformar, //pega o valor total da aba pco para exibir em centro de custo--}}
                    {{--                        ])--}}
                </div>
            @endif
        </div>
    </div>

    <div class="modal fade" id="modalCancel" tabindex="-1" role="dialog" aria-labelledby="modalCancelLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header custom-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Aviso!</h4>
                </div>
                <div class="modal-body">
                    <div class="table-responsive" style="max-height: 300px; overflow-y: auto;">
                        <h4><p for="">Prezado (a) usuário (a), o documento hábil que será cancelado pode estar vinculado a um instrumento de cobrança cujos materiais já ingressaram no Siads e esta verificação é necessária. <br>Deseja realizar o
                                cancelamento?</p></h4>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Não</button>
                    <button type="button" id="btnCancelarApropriacaoModal" class="btn btn-danger">Sim</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('before_scripts')
    <script src="{{ asset('vendor/sweetalert/2.10.16.11') }}/sweetalert2.all.min.js?time={{microtime()}}"></script>
@endpush

@push('after_scripts')

    <script type="text/javascript">

        var situacaoSiafi = '{{$situacaoApropriacao}}'
        $(document).ready(function () {

            carregaAba()

            $('a[data-toggle="tab"]').on('shown.bs.tab', function (event) {
                var target = $(this).attr("id");
                if ($('#' + target)) {
                    carregaAbaApropriacaoAjax(target)
                }
            })

            /**
             * alterar cor da label de aba para indicar que tem campos inválidos que não foram salvos
             */
            $('.form-group').each(function (item) {
                if ($(this).hasClass('has-error')) {
                    var id_aba = $(this).find('input').data("nome-aba");
                    var id_aba_principal = $(this).find('input').data("nome-aba-principal");

                    //altera cor da label das abas internas de pco
                    $('#' + id_aba).css('color', 'red');
                    $('#' + id_aba).css('fontWeight', 'bolder');
                    //altera a cor da label da aba principal
                    $('#' + id_aba_principal).css('color', 'red');
                    $('#' + id_aba_principal).css('fontWeight', 'bolder');
                }
            })


        });

        function atualizaValoresBaixaPatrimonial(){
            $("input[name='sfparcelavlr[]']").trigger('keyup');
        }

        function carregaAba() {
            $('a[data-toggle="tab"]').each(function () {
                var target = $(this).attr("id");
                if ($('#' + target)) {
                    carregaAbaApropriacaoAjax(target)
                }
            });
        }

        function submitFormAjax(form, abaCarregar) {
            form.off('submit').on('submit', function (e) { // Remove eventos antigos antes de adicionar um novo
                e.preventDefault();

                var formData = new FormData(this);

                $.ajax({
                    url: form.attr('action'),
                    type: form.attr('method'),
                    data: formData,
                    processData: false,
                    contentType: false,
                    beforeSend: function () {
                        form.find('button[type="submit"]').prop('disabled', true).text('Enviando...');

                        // Remove erros antigos
                        $('.msg-error-txtobser').remove();
                        $('.is-invalid').removeClass('is-invalid');
                        $('fieldset').removeClass('has-error');
                    },
                    success: function (response) {
                        if (response.status === 'success') {
                            Swal.fire({
                                icon: 'success',
                                title: 'Dados salvo com sucesso!',
                                showConfirmButton: false,
                                timer: 1500
                            });
                            $('#' + abaCarregar).removeClass('loaded')
                            carregaAba()

                            // if (response?.apropriacaoSiafi === true) {
                            //     $('#btnEnviarApropriacaoSiafiDiretamente').removeAttr('disabled')
                            //     $('#btnAbrirModalConfirmacao').removeAttr('disabled')
                            // }

                        } else if (response.status === 'error') {
                            $.each(response.data, function (index, messages) {
                                var inputField = $('#' + index);

                                if (inputField.length) {
                                    inputField.addClass('is-invalid');
                                    inputField.closest('fieldset').addClass('has-error');

                                    if (!inputField.next('.msg-error-txtobser').length) {
                                        inputField.after('<div class="text-danger msg-error-txtobser">' + messages[0] + '</div>');
                                    }
                                } else {
                                    console.warn('Campo não encontrado:', index);
                                }
                            });

                            Swal.fire({
                                icon: 'error',
                                title: response?.mensagemError ? response.mensagemError : 'Corrija os campos destacados',
                                showConfirmButton: false,
                                timer: 5000
                            });
                        }
                    },
                    error: function (xhr) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Erro no envio',
                            text: "Erro ao salvar os dados."
                        });
                    },
                    complete: function () {
                        var textoBotao = retornaTextoBotaoAba(abaCarregar);
                        form.find('button[type="submit"]').prop('disabled', false).html(`<i class="fa fa-save"></i> ${textoBotao}`);
                    }
                });
            });
        }

        function carregaAbaApropriacaoAjax(id_tab) {
            if (!$('#' + id_tab).hasClass("loaded") && $('#' + id_tab).parent().hasClass("active")) {
                var sf_padrao_id = {{ \Route::current()->parameter('sf_padrao_id')}},
                    nomeAba = $('#' + id_tab).attr("id"),
                    apropriacao_id = {{ \Route::current()->parameter('apropriacao_id' )}};

                $.ajax({
                    url: `/apropriacao/fatura-form/renderizatela/apropriacao/${sf_padrao_id}/${apropriacao_id}/${nomeAba}`,
                    type: "GET",
                    beforeSend: function () {
                        var html_refresh = "<div class='text-center' style='font-size: 20px;'><i class='fa fa-refresh fa-spin'></i></div>";

                        switch (id_tab) {
                            case "pco-tab":
                                $("#pco").html(html_refresh);
                                break;
                            case "dados-basicos-tab":
                                $("#dados-basico").html(html_refresh);
                                break;
                            case "deducao-tab":
                                $("#deducao").html(html_refresh);
                                break;
                            case "centro-custo-tab":
                                $("#centro-custo").html(html_refresh);
                                break;
                            case "dados-pagamento-tab":
                                $("#dados-pagamento").html(html_refresh);
                                break;
                        }
                    },
                    success: function (data) {

                        switch (id_tab) {
                            case "pco-tab":
                                $("#pco").html(data);
                                $("#pco-tab").addClass("loaded")
                                iniciaFuncoesPco()
                                break;
                            case "dados-basicos-tab":
                                $("#dados-basico").html(data);
                                $("#dados-basicos-tab").addClass("loaded")
                                iniciaFuncoesDadosBasicos()
                                break;
                            case "deducao-tab":
                                $("#deducao").html(data);
                                $("#deducao-tab").addClass("loaded")
                                iniciaFuncoesDeducao()
                                break;
                            case "centro-custo-tab":
                                $("#centro-custo").html(data);
                                $("#centro-custo-tab").addClass("loaded")
                                iniciaFuncoesCentroCusto()
                                break;
                            case "dados-pagamento-tab":
                                $("#dados-pagamento").html(data);
                                $("#dados-pagamento-tab").addClass("loaded")
                                iniciaFuncoesDadosPagamento();
                                break;
                        }


                        $('.box').boxWidget({
                            animationSpeed: 500,
                            collapseTrigger: '[data-widget="collapse"]',
                            removeTrigger: '[data-widget="remove"]',
                            collapseIcon: 'fa-minus',
                            expandIcon: 'fa-plus',
                            removeIcon: 'fa-times'
                        })
                    },
                    error: function (error) {
                        var msgErro = "<p>Erro ao carregar os dados.</p>"
                        switch (id_tab) {
                            case "pco-tab":
                                $("#pco").html(msgErro);
                                break;
                            case "dados-basicos-tab":
                                $("#dados-basico").html(msgErro);
                                break;
                            case "deducao-tab":
                                $("#deducao").html(msgErro);
                                break;
                            case "centro-custo-tab":
                                $("#centro-custo").html(msgErro);
                                break;

                        }
                    }
                });
            }
        }

        function submitDadosBasicos() {
            $('#sfDadosBasicosForm').on('submit', function (e) {
                e.preventDefault();

                var form = $(this);
                var formData = new FormData(this); // Captura os dados do formulário

                $.ajax({
                    url: form.attr('action'), // Obtém a URL definida no formulário
                    type: form.attr('method'), // Obtém o método definido no formulário
                    data: formData,
                    processData: false, // Impede o jQuery de processar os dados
                    contentType: false, // Impede o jQuery de definir o Content-Type automaticamente
                    beforeSend: function () {
                        form.find('button[type="submit"]').prop('disabled', true).text('Enviando...');
                    },
                    success: function (response) {
                        if (response.status === 'success') {
                            if ($('.msg-error-txtobser').length) {
                                $('.msg-error-txtobser').closest('fieldset').removeClass('has-error');
                                $('.msg-error-txtobser').remove();
                            }

                            Swal.fire({
                                icon: 'success',
                                title: 'Dados salvo com sucesso!',
                                showConfirmButton: false,
                                timer: 1500
                            })

                            $('#pco-tab').tab('show');
                        }
                        if (response.status === 'error') {
                            $.each(response.data, function (index, value) {
                                if (!$('.msg-error-txtobser').length) {
                                    $('#' + index).closest('fieldset').addClass('has-error');
                                    $('#' + index).after('<div class="text-danger msg-error-txtobser">' + value + '</div>');
                                }
                            });

                            Swal.fire({
                                icon: 'error',
                                title: 'Corriga os campos destacados',
                                showConfirmButton: false,
                                timer: 1500
                            })
                        }
                    },
                    error: function (xhr) {
                        var errors = xhr.responseJSON.errors;
                        var errorMessage = "Erro ao salvar!";
                        if (errors) {
                            errorMessage += "\n" + Object.values(errors).join("\n");
                        }
                    },
                    complete: function () {
                        form.find('button[type="submit"]').prop('disabled', false).text('Confirmar Dados Básicos');
                    }
                });
            });
        }

        function submitDadosPagamento() {
            $('#sfDadosPagamentoForm').on('submit', function (e) {
                e.preventDefault();

                var form = $(this);
                var formData = new FormData(this); // Captura os dados do formulário

                $.ajax({
                    url: form.attr('action'), // Obtém a URL definida no formulário
                    type: form.attr('method'), // Obtém o método definido no formulário
                    data: formData,
                    processData: false, // Impede o jQuery de processar os dados
                    contentType: false, // Impede o jQuery de definir o Content-Type automaticamente
                    beforeSend: function () {
                        form.find('button[type="submit"]').prop('disabled', true).text('Enviando...');
                    },
                    success: function (response) {
                        if (response.status === 'success') {
                            if ($('.msg-error-dtpgtoreceb').length) {
                                $('.msg-error-dtpgtoreceb').closest('fieldset').removeClass('has-error');
                                $('.msg-error-dtpgtoreceb').remove();
                            }

                            if ($('.msg-error-codcredordevedor').length) {
                                $('.msg-error-codcredordevedor').closest('fieldset').removeClass('has-error');
                                $('.msg-error-codcredordevedor').remove();
                            }

                            Swal.fire({
                                icon: 'success',
                                title: 'Dados salvo com sucesso!',
                                showConfirmButton: false,
                                timer: 1500
                            })

                            //$('#centro-custo-tab').tab('show');
                        }
                        if (response.status === 'error') {
                            // Removendo mensagens e classes de erro antes de adicionar novas
                            $('fieldset').removeClass('has-error');
                            $('.text-danger').remove();

                            $.each(response.data, function (index, value) {
                                let field = $('#' + index);
                                let errorClass = 'msg-error-' + index;

                                if (value) { // Se houver erro para o campo
                                    if (!$('.' + errorClass).length) {
                                        field.closest('fieldset').addClass('has-error');
                                        field.after('<div class="text-danger ' + errorClass + '">' + value + '</div>');
                                    }
                                }
                            });

                            Swal.fire({
                                icon: 'error',
                                title: 'Corrija os campos destacados',
                                showConfirmButton: false,
                                timer: 1500
                            });
                        } else {
                            // Se não houver erro, limpa tudo
                            $('fieldset').removeClass('has-error');
                            $('.text-danger').remove();
                        }

                    },
                    error: function (xhr) {
                        var errors = xhr.responseJSON.errors;
                        var errorMessage = "Erro ao salvar!";
                        if (errors) {
                            errorMessage += "\n" + Object.values(errors).join("\n");
                        }
                    },
                    complete: function () {
                        form.find('button[type="submit"]').prop('disabled', false).html(`<i class="fa fa-save"></i> Confirmar Dados de Pagamento`);
                    }
                });
            });
        }

        function esconderQuandoApropriacaoEmitidaSiafi() {
            var situacaoApropriacao = '{{ $situacaoApropriacao }}';
            if (situacaoApropriacao === 'APR') {
                $('.esconderQuandoApropriacaoEmitidaSiafi').hide();
            }
        }

        function initTooltips() {
            $('.tooltip-abas-alteracao-apropriacao').tooltip
            ({
                title: "<i class='icon fa fa-info'></i> Necessário salvar a aba anterior!",
                animation: true,
                placement: 'bottom',
                html: true,
                trigger: 'hover'
            })

            $('.tooltip-unidade-nao-utiliza-custos').tooltip
            ({
                title: "<i class='icon fa fa-info'></i> Essa unidade não utiliza custo!",
                animation: true,
                placement: 'bottom',
                html: true,
                trigger: 'hover'
            })

            $('.tooltipRemoverApropriacaoFatura').tooltip(
                {
                    title: "<i class='icon fa fa-info'></i> Remover apropriação de fatura(s)",
                    animation: true,
                    placement: 'bottom',
                    html: true,
                    trigger: 'hover'
                }
            );
        }

        function onChangeSelectTipoDhPadrao(el) {

            if (el.value !== '') {

                $('#btnSubmitFormSfDadosBasicos').removeAttr('disabled');   //remove o disable do botao
                $('#option-selecione').remove();                            //remove o option 'Selecione'

                atualizarCodTipoDHPadrao(el);                               //atualiza o codtipodh do sfpadrao
                carregarSituacaoPorTipoDHPadrao(el.value)                   //carrega situacões por tipo do dh padrao

            }
        }

        function onChangeCodugEmit(el) {
            if (el.value !== '' && situacaoSiafi !== 'APR') {
                var route = "{{ route('apropriacao.faturas.atualizarcodugemit.ajax') }}",
                    sf_padrao_id = {{ \Route::current()->parameter('sf_padrao_id')}},
                    apropriacao_id = {{ \Route::current()->parameter('apropriacao_id')}};

                $.ajax({
                    url: route,
                    type: 'POST',
                    data: {codugemit: el.value, sf_padrao_id, apropriacao_id,},
                    beforeSend: function () {

                    },
                    success: function (result) {
                    },
                    error: function (request, status, error) {

                    }
                });
            }
        }

        function atualizarCodTipoDHPadrao(el) {
            var route = "{{ route('apropriacao.faturas.atualizarcodtipodhpadrao.ajax') }}",
                sf_padrao_id = {{ \Route::current()->parameter('sf_padrao_id')}},
                apropriacao_id = {{ \Route::current()->parameter('apropriacao_id')}};

            $.ajax({
                url: route,
                type: 'POST',
                data: {codtipodh: el.value, sf_padrao_id, apropriacao_id},
                beforeSend: function () {

                },
                success: function (result) {

                    /*esconde box de informação sobre tipo dh padrao*/
                    $('.collapse-mensagem-tipo-dh').collapse();

                    /*transforma input select em input text com readonly */
                    retornarInputReadOnlyTipoDhPadrao(el);
                },
                error: function (request, status, error) {

                }
            });
        }

        function retornarInputReadOnlyTipoDhPadrao(el) {

            var textSelectTipoDhPadrao = el.options[el.selectedIndex].text;
            innerHtml = '<label>Tipo DH Padrão</label>' +
                '<input class="form-control" readonly="" value="' + textSelectTipoDhPadrao + '" style="font-size: 12px">';

            $('#campo-tipo-dh-padrao').html(innerHtml);
        }

        function carregarSituacaoPorTipoDHPadrao(codtipodh) {
            var route = "{{ route('apropriacao.faturas.recuperarsituacaoportipodh.ajax') }}";
            var apropriacao_id = {{$apropriacaoId}};

            $.ajax({
                url: route,
                type: 'GET',
                data: {codtipodh, apropriacao_id},
                beforeSend: function () {
                    $('[name="codsit[]"]').html(
                        '<option>carregando...<i class="fa fa-refresh fa-spin"></i></option>'
                    );
                },
                success: function (result) {

                    var arrayOptionsSituacao = result;

                    if (arrayOptionsSituacao.length > 0) {
                        var optionSituacao = arrayOptionsSituacao.map(function (currentValue, index, arr) {

                            return '<option value="' + currentValue + '">' + currentValue + '</option>'
                        });

                        $('[name="codsit[]"]').html(optionSituacao);
                    } else {
                        $('[name="codsit[]"]').html('<option value="">NENHUMA SITUAÇÃO ENCONTRADA</option>');
                    }

                },
                error: function (request, status, error) {

                }
            });
        }

        function enviarApropriacaoSiafi() {
            $('#btnEnviarApropriacaoSiafiDiretamente').off('click').on('click', enviarApropriacaoSiafiHandler);
        }

        function enviarApropriacaoSiafiModal() {
            $('#btnEnviarApropriacaoSiafiModal').off('click').on('click', enviarApropriacaoSiafiModalHandler);
        }

        function validarCodigoCentroCustoPreenchido() {
            var emptyInputs = []
            $('.checkbox-numseqpai').filter(function () {

                if ($(this).is(':checked')) {
                    $('.centroCustoObrigatorio' + $(this).attr('data-checkbox-centro-custo')).filter(function (index, value) {
                        if (value.value.trim() === '') {
                            return emptyInputs.push(value.value.trim() === '');
                        }
                    })
                }
            });

            if (emptyInputs.length > 0) {
                return false;
            }

            return true;
        }

        function enviarApropriacaoSiafiHandler() {
            if (!validarCodigoCentroCustoPreenchido()) {
                new PNotify({
                    title: 'Error',
                    text: 'Necessário informar os dados do Centro de Custo ou remover o bloco.',
                    type: "error"
                });
                return;
            }

            var route = '{{ $routeEnviarApropriacaoSiafi }}',
                $btn = $(this).button('loading');

            $btn.data('loading-text', '<i class="fa fa-circle-o-notch fa-spin"></i> Enviando para SIAFI');

            $.ajax({
                url: route,
                type: 'POST',
                data: {'sfpadrao_id': {{$idPadrao}}},
                beforeSend: function () {
                },
                success: function (result) {
                    window.location = '{{ $routePagamentoInstCobranca }}'; /*redireciona para tela de crud de faturas*/
                },
                error: function (request, status, error) {

                    if (request.responseJSON) {

                        if (request.status === 422) {
                            new PNotify({
                                title: 'Error',
                                text: request.responseJSON.message,
                                type: "error"
                            });
                        }

                        if (request.responseJSON.exception && request.responseJSON.exception === 'SoapFault') {
                            new PNotify({
                                title: 'Error',
                                text: 'Erro de comunicação com o SIAFI',
                                type: "error"
                            });
                        }

                    }
                    $btn.button('reset');
                }
            });
        }

        function enviarApropriacaoSiafiModalHandler() {
            var route = '{{ $routeEnviarApropriacaoSiafi }}',
                $btn = $(this).button('loading');

            $btn.data('loading-text', '<i class="fa fa-circle-o-notch fa-spin"></i> Enviando para SIAFI');

            $.ajax({
                url: route,
                type: 'POST',
                data: {'sfpadrao_id': {{$idPadrao}}},
                beforeSend: function () {
                },
                success: function (result) {
                    window.location = '{{ $routePagamentoInstCobranca }}'; /*redireciona para tela de crud de faturas*/
                },
                error: function (request, status, error) {
                    $btn.button('reset');
                }
            });
        }

        function cancelarApropriacaoSiafi() {
            const existsSits = {!! json_encode($existsSits) !!};

            $('#btnCancelarApropriacaoSiafi').on('click', function () {
                if (existsSits) {
                    $('#modalCancel').modal('show');
                } else {
                    cancelarApriacao($(this), existsSits);
                }
            });

            $('#btnCancelarApropriacaoModal').on('click', function () {
                cancelarApriacao($(this), existsSits);
            });
        }

        function cancelarApriacao($button, existsSits) {
            const route = '{{ $routeCancelarApropriacaoSiafi }}';

            $button.prop('disabled', true).html('<i class="fa fa-circle-o-notch fa-spin"></i> Enviando para SIAFI');

            $.ajax({
                url: route,
                type: 'POST',
                data: {'existsSits': existsSits},
                beforeSend: function () {

                },
                success: function (result) {
                    window.location = '{{ $routePagamentoInstCobranca }}'; /*redireciona para tela de crud de faturas*/

                },
                error: function (request, status, error) {
                    $btn.button('reset')
                }
            });
        }

        function atualizarMaskMoney(element) {
            if (element.value !== '') {
                var maxLength = '000.000.000.000.000,00'.length;
                $(element).maskMoney({
                    allowNegative: false,
                    thousands: '.',
                    decimal: ',',
                    precision: 2,
                    affixesStay: false
                }).attr('maxlength', maxLength).trigger('mask.maskMoney');
            }
        }

        /**
         * Quando a apropriação não possui Centro de Custo então remove classe de enviar para o SIAFI e exibe modal
         * de confirmação
         */
        function transformarBotaoDeEnvioAoSiafi() {
            var btnEnvAprSiafi = $('#btnEnviarApropriacaoSiafiDiretamente');
            var btnEnvAprSiafiModal = $('#btnAbrirModalConfirmacao');

            if ($('.count-centro-custo').length === 0) {
                @if(isset($situacaoUtilizaCentroCusto) && $situacaoUtilizaCentroCusto === true)
                btnEnvAprSiafiModal.removeClass('hide');
                btnEnvAprSiafi.addClass('hide');
                @else
                btnEnvAprSiafi.removeClass('hide');
                btnEnvAprSiafiModal.addClass('hide');
                @endif
            }

            if ($('.count-centro-custo').length !== 0) {
                btnEnvAprSiafi.removeClass('hide');
                btnEnvAprSiafiModal.addClass('hide');
            }
        }

        /**
         * Retorna a descrição da ug pagamento quando acionado a troca da ug pagadora
         * de confirmação
         */
        function atualizaUgPagamento(element) {
            if (situacaoSiafi !== 'APR') {
                var textoSelecionado = element.options[element.selectedIndex].text
                var valorSelecionado = element.options[element.selectedIndex].value
                var textoTratado = textoSelecionado.substring(textoSelecionado.indexOf('-') + 2).trim()
                // Altera o campo nome da ug pagadora
                $('.nomeugpgto').text(textoTratado)
                // Altera o campo Código da UG pagadora aba pagamento
                $('.codugpgto').text(valorSelecionado)
                $('#ugEmitente').val('(' + valorSelecionado + ')' + ' ' + textoTratado).change()
                // Aba PCO
                $('#codugpgto').val(valorSelecionado)
                // $('.codugempePco').text(valorSelecionado)
                // $('#nomeugempePco').text(textoTratado)
            }
        }

        function iniciaFatura(){
            //Script Fatura Form
            initTooltips();
            enviarApropriacaoSiafi();
            enviarApropriacaoSiafiModal();
            cancelarApropriacaoSiafi();
            esconderQuandoApropriacaoEmitidaSiafi();
            transformarBotaoDeEnvioAoSiafi();
        }

        function iniciaFuncoesPco(){
            removerMensagensDeErrodosInputGroup();
            initOnClickDropdownInstrumentosDaApropriacao();
            initCssBorderBoxNode();
            initCssBtnsNotaEmpenho();
            initMascaraCamposVariaveis();
            focusOutCamposVariaveisSfPcoItem();
            abaAtivaPco();
            atualizaValoresBaixaPatrimonial();
            submitFormAjax($('#sfDadosPcoForm'), 'pco-tab');
            //remove a class para recarregar o centro de custo
            $('#centro-custo-tab').removeClass('loaded');
            iniciaFatura()
        }

        function iniciaFuncoesDeducao() {
            initTooltipsDeducao()
            checkResponsive()
            iniciaFuncoesGerais()
            atualizaCabecalhoTabelaAcrescimo()

            $('.codugpgtodeducao').select2({
                width: '100%'
            })

            $('.tooltip-btn-add-novo-recolhedor').tooltip(
                {
                    title: "Criar novo Recolhedor.",
                    animation: true,
                    placement: 'bottom',
                    html: true,
                    trigger: 'hover'
                }
            );

            $("[name='sfdeducao[]']").each(function () {
                atualizaTotaisRodape($(this).val())
            })
            abaAtivaDeducao()
            submitFormAjax($("#formAbaDeducao"), 'deducao-tab')
            iniciaFatura()
        }

        function iniciaFuncoesDadosPagamento()
        {
            initAbaDadosPagamento();
            validaForm();
            abreModal();
            preencherBanco();
            atualizarCamposPorTipo();
            carregaDomicioBancario();
            maskProcessoModalPredoc();
            submitDadosPagamento();

            // #forca o carregamento da aba centro de custo novamente
            $('#centro-custo-tab').removeClass('loaded');
        }

        function iniciaFuncoesDadosBasicos() {
            $('.form-group').each(function () {
                if ($(this).hasClass('has-error')) {
                    var id_aba_principal = $(this).find('textarea').data("nome-aba-principal");
                    //altera a cor da label da aba principal
                    $('#' + id_aba_principal).css('color', 'red');
                    $('#' + id_aba_principal).css('fontWeight', 'bolder');
                }
            })

            $('#txtprocesso').mask('99999.999999/9999-99');

            var codCredorDevedor = $('#codcredordevedor-dados-basicos')
            if (codCredorDevedor.val().length === 14) {
                const cnpjFormatado = codCredorDevedor.val().replace(/^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})$/, "$1.$2.$3/$4-$5");
                codCredorDevedor.val(cnpjFormatado)
            }

            $('#codugpgto').select2({
                width: '100%'
            })

            verificarSelectTipoDhPadraoComValor()
            submitDadosBasicos()
            iniciaFatura()
        }

        function iniciaFuncoesCentroCusto() {
            initTooltipsCentroCusto();
            atualizarValoresLabelInformativa();
            habilitarDesabilitarBotaoConfirmarCentroCusto();
            submitFormAjax($("#sfCentroCustoForm"), 'centro-custo-tab')
            iniciaFatura()
        }

        function retornaTextoBotaoAba(nomeAba) {
            switch (nomeAba) {
                case 'deducao-tab':
                    return 'Confirmar Dados Dedução'
                case 'pco-tab':
                    return 'Confirmar Dados Principal com Orçamento'
                case 'centro-custo-tab':
                    return 'Confirmar Dados Centro de Custo'
            }
        }

    </script>
    @include('backpack::base.mod.apropriacao.aba-dados-basicos-scripts')
    @include('backpack::base.mod.apropriacao.aba-principal-com-orcamento-scripts')
    @include('backpack::mod.apropriacao.cronograma-baixa-patrimonial-scripts')
    @include('backpack::base.mod.apropriacao.aba-deducao-scripts')
    @include('backpack::base.mod.apropriacao.aba-centro-custo-scripts')
    @include('backpack::mod.apropriacao.aba-dados-pagamento-scripts')
    <style type="text/css">
        .label-title {
            font-weight: bold;
        }

        .select2-container--default .select2-selection--single {
            border-color: #D2D6DE; /* Cor da borda */
        }

        .erroUgPagadora .select2-selection--single {
            border-color: red; /* Cor da borda */
        }

        .paddingTopButton {
            padding-top: 3px;
            padding-bottom: 3px;
        }

        .align-table-items-center {
            text-align: center;
        }

        .align-table-items-left {
            text-align: left;
        }

        .align-table-items-right {
            text-align: right;
        }

        #dados-basico textarea.form-control {
            height: 100px;
        }

        #pco textarea.form-control {
            height: 100px;
        }

        #deducao textarea.form-control {
            height: 100px;
        }

        .pointer-hand {
            cursor: pointer;
        }

        .title-item-acordion {
            font-size: 18px;
            font-weight: 500
        }

        .margin-top-20 {
            margin-top: 20px;
        }

        .margin-top-10 {
            margin-top: 10px;
        }

        .margin-bottom-10 {
            margin-bottom: 10px;
        }

        .aba-btn-apropriar-siafi {
            padding: 2px 5px;
        }

        .tooltip-abas-alteracao-apropriacao + .tooltip > .tooltip-inner, .tooltip-unidade-nao-utiliza-custos + .tooltip > .tooltip-inner {
            font-size: 14px;
            background-color: #00c0ef;
        }

        .tooltipRemoverApropriacaoFatura + .tooltip > .tooltip-inner {
            font-size: 14px;
            background-color: red;
        }

        .tooltip-abas-alteracao-apropriacao + .tooltip.bottom > .tooltip-arrow, .tooltip-unidade-nao-utiliza-custos + .tooltip.bottom > .tooltip-arrow {
            border-bottom: 5px solid #00c0ef;
        }

        .tooltipRemoverApropriacaoFatura + .tooltip.bottom > .tooltip-arrow {
            border-bottom: 5px solid red;
        }

        #abas-alteracao-apropriacao {
            display: inline-block;
            width: 100%;
            border-radius: 0;
        }

        tooltipRemoverPcoItem + .tooltip > .tooltip-inner {
            /*background-color: #00c0ef;*/
        }

        .tooltipRemoverPcoItem + .tooltip.bottom > .tooltip-arrow {
            /*border-bottom: 5px solid #00c0ef;*/
        }

        .custom-shadow {
            box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.3); /* Personaliza a sombra */
        }
        button.custom-shadow:hover {
            box-shadow: 0px 3px 6px rgba(0, 0, 0, 0.4); /* Sombra mais forte ao passar o mouse */
        }

        /* espaco para o predoc deducao */
        .espaco-div {
            padding-left: 15px;
            padding-right: 15px;
        }

        .padding-box-centro-custo{
            padding: 1px;
        }

    </style>
@endpush
