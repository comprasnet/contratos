{!! form_start($sfCentroCustoForm)  !!}


<!-- TOTAL DA ABACENTRO DE CUSTO -->
<div class="row" id="form-centro-custo-header">
    <div class="col-md-6 h3">Centro de Custo</div>
    <div class="col-md-offset-2 col-md-4">
        <div class="pull-right">
            <div class="input-group margin-top-20 margin-bottom-5 grid-width-50">
                <div class="input-group-addon label-title" style="width: 60%; text-align: left">Centro de Custo a Informar:</div>
                <input type="text" class="form-control" id="valorCentroCustoAInformar" readonly value="{{$valorTotalCentroCustoAInformar}}">
            </div>
            <div class="input-group margin-top-5 margin-bottom-5">
                <div class="input-group-addon label-title" style="width: 60%; text-align: left">Centro de Custo Informado:</div>
                <input type="text" class="form-control" id="valorCentroCustoInformado" readonly value="">
            </div>
            <div class="input-group margin-top-5 margin-bottom-5" style="width: 100%">
                <div class="input-group-addon label-title" style="width: 60%; text-align: left">Total Apropriado:</div>
                <input type="text" class="form-control" id="valorTotalApropriado" readonly value="">
            </div>
        </div>
    </div>
</div>

<div class="padding-15"></div>

<!-- Acordion Centro de Custo-->
@if(count($sfCentroCusto) > 0)
<input type="checkbox" class="selecionaTodosCentroCusto"
       data-valorref-id=""
       class="pointer-hand" onclick="selecionaTodosCentrosCusto(this);" onchange="atualizarValoresLabelInformativa()">
@endif
@foreach($sfCentroCusto as $centro_custo_current)
    <div class="box box-solid box-primary count-centro-custo" data-count-centro-custo="{{$loop->count}}">
        <div class="custom-shadow box-header with-border padding-box-centro-custo" data-toggle="tooltip" title="Centro de Custo">
            <div class="row pointer-hand" data-widget="collapse">
                <div class="col-md-1"><span class="caret"></span></div>
                <div class="col-md-3">
                    <span class="title-item-acordion font-weight: bold">Centro de Custo:</span>
                </div>

                <div class="col-md-1">
                    <span class="title-item-acordion font-weight: bold">Mês:</span>
                </div>

                <div class="col-md-1">
                    <span class="title-item-acordion font-weight: bold">Ano:</span>
                </div>

                <div class="col-md-2 text-center">
                    <span class="title-item-acordion font-weight: bold">Código SIORG:</span>
                </div>
                <div class="col-md-2">
                    <span class="title-item-acordion font-weight: bold">UG Beneficiada:</span>
                </div>
                <div class="col-md-offset-1">

                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-md-offset-1">
                    {!! form_row($sfCentroCustoForm->{'codcentrocusto'.$centro_custo_current->id}) !!}
                </div>
                <div class="col-md-1">
                    {!! form_row($sfCentroCustoForm->{'mesreferencia'.$centro_custo_current->id}) !!}
                </div>
                <div class="col-md-1">
                    {!! form_row($sfCentroCustoForm->{'anoreferencia'.$centro_custo_current->id}) !!}
                </div>

                <div class="col-md-2 text-center">
                    {!! form_row($sfCentroCustoForm->{'codsiorg'.$centro_custo_current->id}) !!}
                </div>
                <div class="col-md-2">
                    {!! form_row($sfCentroCustoForm->{'codugbenef'.$centro_custo_current->id}) !!}
                </div>
                <div class="col-md-2">
                    <button
                        type="button"
                        class="btn btn-primary btn-sm pull-right tooltipRemoverCentroCusto
                                                                                esconderQuandoApropriacaoEmitidaSiafi"
                        style="margin-right: 5px;"
                        onclick="clickBtnRemoverOuCriarNovoCentroCusto(
                        {{ $centro_custo_current->id }},
                            null
                            );"
                    >
                        <i class="custom-shadow fa fa-trash"></i>
                    </button>
                    <button
                        type="button"
                        class="btn btn-primary btn-sm pull-right tooltipCopiarCentroCusto
                                                                            esconderQuandoApropriacaoEmitidaSiafi"
                        style="margin-right: 5px;"
                        onclick="
                            clickBtnRemoverOuCriarNovoCentroCusto(
                            null,
                        {{ $centro_custo_current->id }}
                            );"
                    >
                        <i class="custom-shadow fa fa-copy"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="box-body">
            @if(isset($centro_custo_current->arrSfpcoSituacao[$centro_custo_current->id]))
                @foreach($centro_custo_current->arrSfpcoSituacao[$centro_custo_current->id] as $key =>  $sfpcosituacao)

                    @php $labelDespAntecipada = $sfpcosituacao['despesaantecipada'] ? 'Despesa Antecipada' : ''; @endphp

                    <div class="row">
                        <div class="col-md-1">
                            {!! form_row($sfCentroCustoForm->{'numseqpai_checkbox'.$sfpcosituacao['indexField']}) !!}
                            {!! form_row($sfCentroCustoForm->{'numseqitem'.$sfpcosituacao['indexField']}) !!}
                            {!! form_row($sfCentroCustoForm->{'numseqpai'.$sfpcosituacao['indexField']}) !!}
                            {!! form_row($sfCentroCustoForm->{'centro_custo_id'.$sfpcosituacao['indexField']}) !!}
                            {!! form_row($sfCentroCustoForm->{'despesaantecipada'.$sfpcosituacao['indexField']}) !!}
                        </div>
                        <div class="col-md-4">
                            <div style="display: flex; flex-direction: row">
                                <div style="flex-grow: 1">{{ $sfpcosituacao['codsit'] }}</div>
                                <div style="flex-grow: 1">
                                    <!--se possui despesa antecipada exibe label-->
                                    @if($sfpcosituacao['despesaantecipada'])
                                        <small class="label label-warning">
                                            {{$labelDespAntecipada}}
                                        </small>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <span>{{ $sfpcosituacao['numempe'] }}</span>
                            <small class="m-l-10" style="font-weight: 500;">total: {{$sfpcosituacao['pcoitem_vlr']}}</small>
                        </div>
                        <div class="col-md-2">
                            {!! form_row($sfCentroCustoForm->{'vlr'.$sfpcosituacao['indexField']}) !!}
                        </div>
                        <div class="col-md-offset-1"></div>
                    </div>

                @endforeach
            @endif
        </div>
    </div>

    @if($loop->last)
        <div id="btn-add-novo-centro-custo-wrapper" style="text-align: center">
            <a
                href="javascript:void(0)"
                id="novo-centro-custo"
                onclick="clickBtnRemoverOuCriarNovoCentroCusto( null, null );"
                class="tooltip-btn-add-novo-centro-custo esconderQuandoApropriacaoEmitidaSiafi"
            >
                <i class="fa fa-plus" style="color: #3c8dbc"></i>
            </a>
        </div>
    @endif

@endforeach
<!--Se não houver nenhum centro de custo permite adicionar-->
@if(count($sfCentroCusto) === 0)
    <div id="btn-add-novo-centro-custo-wrapper" style="text-align: center">
        <a
            href="javascript:void(0)"
            id="novo-centro-custo"
            onclick="clickBtnRemoverOuCriarNovoCentroCusto( null, null );"
            class="tooltip-btn-add-novo-centro-custo esconderQuandoApropriacaoEmitidaSiafi"
        >
            <i class="fa fa-plus" style="color: #3c8dbc"></i>
        </a>
    </div>
@endif

<div class="align-table-items-center">
    <button class="custom-shadow btn btn-success esconderQuandoApropriacaoEmitidaSiafi btnConfirmarCentroCusto"
            name="confirma-dados-deducao"
            value="confirmar"
            type="submit"
            id="confirma-dados-centro-custo"><i class="custom-shadow fa fa-save"></i> Confirmar Dados Centro de Custo</button>
</div>

{{--<fieldset class="form-group">--}}
{{--    {!! form_row($sfCentroCustoForm->btnSubmitFormSfCentroCusto)  !!}--}}
{{--</fieldset>--}}
