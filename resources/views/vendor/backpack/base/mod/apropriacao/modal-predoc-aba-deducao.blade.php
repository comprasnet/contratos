@if($dadospredoc->dadospredoc)
    <div class="modal fade" tabindex="-1" role="dialog" id="modal-predoc-aba-dados-pagamento{!! $dadospredoc->id !!}">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" id="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;
                    </span>
                    </button>
                    <h4 class="modal-title" style="float: left">DAR</h4>
                </div>
                <div class="modal-body">
                    <div class="panel panel-default margin-bottom-none">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-3 label-title">Dedução:</div>
                                <div class="col-md-3 label-title">Data de Vencimento:</div>
                                <div class="col-md-3 label-title">Data de Pagamento:</div>
                                <div class="col-md-3 label-title">Valor:</div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">-</div>
                                <div class="col-md-3">{{ $dadospredoc->dadospredoc->dtvenc ?? '' }}</div>
                                <div class="col-md-3">{{ $dadospredoc->dadospredoc->dtpgtoreceb ?? '' }}</div>
                                <div class="col-md-3">{{ number_format($dadospredoc->vlr, 2, ',', '.') }}</div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 label-title">Valor compensado:</div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">0000000</div>
                            </div>
                        </div>
                    </div>


                    <!--BOX DAR-->
                    <div class="box box-solid box-primary margin-top-20 margin-bottom-10">
                        <div class="box-header with-border align-table-items-center">
                            <h3 class="box-title">DAR</h3>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-3 label-title">Município Favorecido:</div>
                                <div class="col-md-3 label-title">Receita:</div>
                                <div class="col-md-3 label-title">Recurso:</div>
                                <div class="col-md-3 label-title">Recferência:</div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">{{ $dadospredoc->dadospredoc->codmuninf ?? '' }}</div>
                                <div class="col-md-3">00000</div>
                                <div class="col-md-3">{{ $dadospredoc->dadospredoc->codrecurso ?? '' }}</div>
                                <div class="col-md-3">{{ $dadospredoc->dadospredoc->numreferencia ?? '' }}</div>
                            </div>
                        </div>
                    </div>
                    <!--BOX INFORMAÇÕES DO RECOLHIMENTO -->
                    <div class="box box-solid box-primary margin-top-20 margin-bottom-10">
                        <div class="box-header with-border align-table-items-center">
                            <h3 class="box-title">Informações do Recolhimento</h3>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6 label-title">UG Tomadora de serviço:</div>
                                <div class="col-md-6 label-title">Município da NF:</div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">00000</div>
                                <div
                                    class="col-md-6">{{ $dadospredoc->dadospredoc->codmuninf === 0 ? ' - ' : $dadospredoc->dadospredoc->codmuninf}}</div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 label-title">Número da NF/Recibo:</div>
                                <div class="col-md-6 label-title">Data de Emissão da NF:</div>
                            </div>
                            <div class="row">
                                <div
                                    class="col-md-6">{{ $dadospredoc->dadospredoc->numnf === 0 ? ' - ' : $dadospredoc->dadospredoc->numnf}}</div>
                                <div class="col-md-6">{{ $dadospredoc->dadospredoc->dtemisnf }}</div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 label-title">Série da NF:</div>
                                <div class="col-md-6 label-title">Subsérie da NF:</div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">{{ $dadospredoc->dadospredoc->txtserienf ?? ' - '}}</div>
                                <div
                                    class="col-md-6">{{ $dadospredoc->dadospredoc->numsubserienf === 0 ? ' - ' : $dadospredoc->dadospredoc->numsubserienf}}</div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 label-title">Alíquota da NF:</div>
                                <div class="col-md-6 label-title">Valor da NF:</div>
                            </div>
                            <div class="row">
                                <div
                                    class="col-md-6">{{ $dadospredoc->dadospredoc->numaliqnf === 0 ? ' - ' : $dadospredoc->dadospredoc->numaliqnf }}</div>
                                <div
                                    class="col-md-6">{{ number_format($dadospredoc->dadospredoc->vlrnf, 2, ',', '.') }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="box box-solid box-primary">
                        <div class="box-header with-border align-table-items-center">
                            <h3 class="box-title">Lista dos Recolhedores</h3>
                        </div>
                        <div class="box-body">
                            <table class="table table-bordered table-hover table-condensed">
                                <thead class="align-items-center">
                                <tr>
                                    <th class="align-table-items-center">
                                        Seq
                                    </th>
                                    <th class="align-table-items-center">
                                        Recolhedor
                                    </th>
                                    <th class="align-table-items-center">
                                        Valor Principal
                                    </th>
                                    <th class="align-table-items-center">
                                        Multa
                                    </th>
                                    <th class="align-table-items-center">
                                        Juros
                                    </th>
                                    <th class="align-table-items-center">
                                        Total Recolhido
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="align-table-items-center">
                                        00000
                                    </td>
                                    <td class="align-table-items-center">
                                        00000
                                    </td>
                                    <td class="align-table-items-center">
                                        00000
                                    </td>
                                    <td class="align-table-items-center">
                                        00000
                                    </td>
                                    <td class="align-table-items-center">
                                        00000
                                    </td>
                                    <td class="align-table-items-center">
                                        00000
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="label-title align-table-items-center">Total Pre-Doc:</td>
                                    <td class="label-title align-table-items-center">00000</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="txtobser" class="control-label required">Observação:</label>
                        <textarea
                            class="form-control"
                            id="txtobser"
                            name="txtobser"
                            cols="10"
                            rows="3"
                            disabled="disabled"
                        >{{ $dadospredoc->dadospredoc->txtobser }}</textarea>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="botao-cancelar"><span
                            class="fa fa-ban"></span>
                        &nbsp;Retornar
                    </button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endif
