<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script type="text/javascript">

    function iniciaFuncoesGerais(ativarAbaDeducao = false) {
        initMascaraCamposVariaveisDeducao()
        focusOutCamposVariaveisSfDeducao()
        bindEvents()
        atualizaCabecalhoTabelaAcrescimo()
        atualizaSituacaoPreDoc()
        bindSelect2Municipio()
        confereOnchangeDosCampos()
        alteraVisibilidadeCamposRecolhedor()
        abaAtivaDeducao(ativarAbaDeducao)
        submitFormAjax($("#formAbaDeducao"), 'deducao-tab')
    }

    function confereOnchangeDosCampos() {
        $('[onchange*="exibirOuEsconderCamposDeducao"]').trigger('change');

        $('.buscaMunicipioCod').each(function () {
            $(this).trigger('keyup');
        });
    }

    function abaAtivaDeducao(forceLastTab = false) {
        const sf_padrao_id = {{ \Route::current()->parameter('sf_padrao_id') }};
        const localStorageKey = `activeTabDeducao${sf_padrao_id}`;
        const localStorageTimeKey = `activeTabDeducaoTime${sf_padrao_id}`;
        const expirationTime = 120;

        const isStorageValid = () => {
            const savedTime = parseInt(localStorage.getItem(localStorageTimeKey), 10);
            return savedTime && (new Date().getTime() - savedTime) / 1000 <= expirationTime;
        };

        const salvarTabAtiva = (tabId) => {
            localStorage.setItem(localStorageKey, tabId);
            localStorage.setItem(localStorageTimeKey, new Date().getTime().toString());
        };

        const ativarUltimaTab = () => {
            const ultimaTab = $('#deducao a[data-toggle="tab"]').last();
            if (ultimaTab.length) {
                ultimaTab.tab('show');
                salvarTabAtiva(ultimaTab.attr('id'));
            }
        };

        if (forceLastTab) {
            ativarUltimaTab();
        } else {
            const activeTabId = localStorage.getItem(localStorageKey);
            if (activeTabId && isStorageValid()) {
                $(`#deducao a#${activeTabId}`).tab('show');
            } else {
                localStorage.removeItem(localStorageKey);
                localStorage.removeItem(localStorageTimeKey);
            }
        }

        $('#deducao a').on('shown.bs.tab', function (e) {
            salvarTabAtiva(e.target.id); // Salva a aba ativa no evento
        });
    }

    function alteraVisibilidadeCamposRecolhedor() {
        $('.sfdeducaoApropriacao').each(function () {
            var deducao = $(this).val();
            var situacao = $('#sfdeducaocodsit' + deducao).val();

            if (situacao !== undefined && situacao.substring(0, 3) === 'DDR') {
                $('.vlrPrincipalSituacao' + deducao)
                    .removeAttr('readonly')
                    .attr('onchange', 'atualizaTotaisRodape(' + deducao + '); atualizarMaskMoney(this);')
                    .attr('onkeyup', 'atualizaTotaisRodape(' + deducao + '); atualizarMaskMoney(this);');

                $('.escondeParaSituacoesDDR' + deducao).hide();
            } else {
                // $('.vlrPrincipalSituacao' + deducao)
                //     .attr('readonly', 'readonly')
                //     .removeAttr('onchange')
                //     .removeAttr('onkeyup');

                $('.escondeParaSituacoesDDR' + deducao).show();
            }
        });
    }

    function bindSelect2Municipio() {


        $('.municipioSelect2').each(function (index, value) {

            $('.municipioSelect2').eq(index).select2({
                width: '100%',
                placeholder: 'Selecione uma cidade',
                ajax: {
                    url: '{{ route("busca-municipio") }}',
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data.data, function (item) {
                                return {
                                    id: item.codigo_municipio,
                                    text: item.nome
                                };
                            })
                        };
                    },
                    cache: true
                },
                minimumInputLength: 3
            });
        })
    }

    function buscaMunicipioPorCod(idDeducao, element, id) {
        if (element.value) {

            var codigo = element.value;

            if (codigo) {
                buscaMunicipioIndividual(codigo, id + idDeducao)
            }
        }
    }

    function buscaMunicipioIndividual(codigo, id) {
        if (codigo) {
            $.ajax({
                url: '{{ route("busca-municipio") }}',
                data: {q: codigo},
                dataType: 'json',
                success: function (data) {
                    if (data.data.length > 0) {
                        var municipio = data.data[0];
                        var newOption = new Option(municipio.nome, municipio.codigo_municipio, true, true);
                        $('#' + id).append(newOption).trigger('change');
                    } else {
                        $('#' + id).val(null).trigger('change');
                    }
                }
            });
        } else {
            $('#' + id).val(null).trigger('change');
        }
    }

    function initTooltipsDeducao() {

        $('.tooltip-btn-add-nova-deducao').tooltip(
            {
                title: "Criar novo",
                animation: true,
                placement: 'bottom',
                html: true,
                trigger: 'hover'
            }
        );

        $('.tooltipRemoverDeducaoRecolhimento').tooltip(
            {
                title: "Remover item",
                animation: true,
                placement: 'bottom',
                html: true,
                trigger: 'hover'
            }
        );

        $('.tooltipAdicionarPredoc').tooltip(
            {
                title: "Gerar Pré-Doc",
                animation: true,
                placement: 'bottom',
                html: true,
                trigger: 'hover'
            }
        );
    }

    function atualizaSituacaoPreDoc() {
        var camposSituacaoPreDoc = $('.camposSitPreDoc')
        camposSituacaoPreDoc.hide()

        camposSituacaoPreDoc.each(function () {
            var idDeducao = $(this).attr('data-id-deducao')
            var tipoPreDoc = $("#sfpredoccodtipodarf" + idDeducao + " option:selected").val()

            if (tipoPreDoc) {
                $('.predocdar' + idDeducao).hide()
                $('.predocdarfcampos' + idDeducao).show()

                switch (tipoPreDoc) {
                    case 'DFS':
                        $('.camposDarf' + idDeducao).show()
                        $('.exibeEscondeCamposDarfNumeradoComposto' + idDeducao).show()
                        $('.camposDarfNumeradoComposto' + idDeducao).hide()
                        $('.camposDarfNumerado' + idDeducao).hide()

                        $(".camposDarfNumeradoCompostoOuDarf" + idDeducao).text('Referência:')
                        break;
                    case 'DFN':
                        $('.camposDarfNumerado' + idDeducao).show()
                        $('.camposDarf' + idDeducao).hide()
                        $('.camposDarfNumeradoComposto' + idDeducao).hide()
                        $('.exibeEscondeCamposDarfNumeradoComposto' + idDeducao).hide()
                        break;
                    case 'DFD':
                        $('.camposDarfNumeradoCompostoOuDarf' + idDeducao).show()
                        $('.camposDarfNumerado' + idDeducao).hide()

                        $('.camposDarf' + idDeducao).show()
                        $(".camposDarfNumeradoCompostoOuDarf" + idDeducao).text('Vinculação:')
                        $('.exibeEscondeCamposDarfNumeradoComposto' + idDeducao).hide()
                        break;
                    default:
                        $('.camposDarf' + idDeducao).hide()
                        break;
                }
            } else {
                $('.predocdar' + idDeducao).show()
                $('.predocdarfcampos' + idDeducao).hide()
            }

            if (situacaoSiafi === 'APR') {
                var select = $("#sfpredoccodtipodarf" + idDeducao);
                select.find("option").not(":selected").remove();
            }
        })
    }

    function bindEvents() {
        $('.toggle-details').on('click', function () {
            $(this).closest('tr').next('.details-row').toggle();
        });
    }

    function atualizaValorPrincipal(element, id_recolhedor) {

        var porcentagem = parseFloat(element.value.replace(/\./g, '').replace(',', '.'))

        if (!isNaN(porcentagem)) {

            var valorBaseCalculo = $('#vlrbasecalculo' + id_recolhedor).val().replace(/\./g, '').replace(',', '.');
            var valorCalculado = valorBaseCalculo * (porcentagem / 100);

            // Formata o valor com duas casas decimais e separadores de milhar
            $('#vlrPrincipal' + id_recolhedor).val(valorCalculado.toLocaleString('pt-BR', {minimumFractionDigits: 2, maximumFractionDigits: 2}));
        }
    }

    function atualizaTotaisRodape(idDeducao) {

        if (situacaoSiafi === 'APR') {
            return
        }

        function calculaTotal(campo) {
            var total = campo.map(function () {
                return parseFloat($(this).val().replace(/\./g, '').replace(',', '.')) || 0
            }).get()
                .reduce(function (acc, value) {
                    return acc + value
                }, 0)
            return total.toLocaleString('pt-BR', {style: 'currency', currency: 'BRL'});
        }

        var totalVlrPrincipal = calculaTotal($(`[id^="vlrPrincipal${idDeducao}"]`))
        var totalVlrMulta = calculaTotal($(`[id^="vlrmulta${idDeducao}"]`))
        var totalVlrJuros = calculaTotal($(`[id^="vlrjuros${idDeducao}"]`))

        $('#total-vlrPrincipal-' + idDeducao).text(totalVlrPrincipal)
        $('#total-vlrMulta-' + idDeducao).text(totalVlrMulta)
        $('#total-vlrJuros-' + idDeducao).text(totalVlrJuros)

        var valorTotal = (
            parseFloat(totalVlrPrincipal.replace(/\./g, '').replace(',', '.').replace('R$', '')) +
            parseFloat(totalVlrMulta.replace(/\./g, '').replace(',', '.').replace('R$', '')) +
            parseFloat(totalVlrJuros.replace(/\./g, '').replace(',', '.').replace('R$', ''))
        )

        $('#valor-total-informar-' + idDeducao).text(valorTotal.toLocaleString('pt-BR', {style: 'currency', currency: 'BRL'}))
        //Adiciona o valor total para a deducao item
        $('#sfdeducaovlr' + idDeducao).val(valorTotal.toLocaleString('pt-BR', {minimumFractionDigits: 2, maximumFractionDigits: 2}))
    }

    function initMascaraCamposVariaveisDeducao() {

        $.mask.definitions["9"] = null;
        $.mask.definitions["X"] = "[0-9]";

        $('.camposVariaveisSfRecolhimento').each(function (index, elemento) {
            if (elemento.value) {
                let valor = elemento.value.replace(/\D/g, '');

                if (valor.length === 14) {
                    elemento.value = valor.replace(/^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})$/, '$1.$2.$3/$4-$5');
                } else if (valor.length === 11) {
                    elemento.value = valor.replace(/^(\d{3})(\d{3})(\d{3})(\d{2})$/, '$1.$2.$3-$4');
                }
            }

            // coloca mascara no campo referencia do predoc na aba de dedução
            if($('#' + elemento.id).hasClass('campoReferenciaPredoc'))
            {
                $('#' + elemento.id).mask(elemento.dataset.mascara);
            }

        });

        $('.camposVariaveisSfDeducao').each(function (index, elemento) {
            if (elemento.dataset.mascara) {
                $('#' + elemento.id).mask(elemento.dataset.mascara);
            }
        });
    }

    function exibirOuEsconderCamposDeducao(idSfDeducao, element, elementName) {
        var value = element.value,
            displayValue = value === '1' ? 'block' : 'none';
        $('#' + elementName + idSfDeducao).css('display', displayValue);
    }

    function apagarCamposVariaveisDeSfDeducao() {
        $('.camposVariaveisSfDeducao').val('');
    }

    function atualizaCabecalhoTabelaAcrescimo() {

        $('.variavelAcrescimoTable').hide()

        $('.variavelAcrescimoTable').each(function () {
            var idDeducao = $(this).attr('data-id-deducao')
            var rotulo = $('.camposVariaveisAcrescimoSfDeducao' + idDeducao).eq(0).attr('data-rotulo-tabela')
            var rotulo1 = $('.camposVariaveisAcrescimoSfDeducao' + idDeducao).eq(1).attr('data-rotulo-tabela')
            var rotulo2 = $('.camposVariaveisAcrescimoSfDeducao' + idDeducao).eq(2).attr('data-rotulo-tabela')

            if (rotulo) {
                $('.variavelAcrescimoTable' + idDeducao).eq(0).text(rotulo).show()
            } else {
                $('.variavelAcrescimoTable' + idDeducao).eq(0).hide()
            }

            if (rotulo1 !== rotulo && rotulo1 !== rotulo2) {
                $('.variavelAcrescimoTable' + idDeducao).eq(1).text(rotulo1).show()
            } else {
                $('.variavelAcrescimoTable' + idDeducao).eq(1).hide()
            }

            if (rotulo2 !== rotulo1 && rotulo2 !== rotulo1 && rotulo2 !== undefined) {
                $('.variavelAcrescimoTable' + idDeducao).eq(2).text(rotulo2).show()
            } else {
                $('.variavelAcrescimoTable' + idDeducao).eq(2).hide()
            }
        });
    }

    const focusOutCamposVariaveisSfDeducao = () => {
        $('.camposVariaveisSfDeducao').focusout(function () {
            let valorAtual = $(this).val();
            if (valorAtual.includes('_')) {
                $(this).val('0');
            }
        });
    };

    function clickBtnRemoverOuCriarNovoPreDoc(idDeducao, idPreDoc = null) {

        var route = '/apropriacao/fatura-form/novo/predocdeducao/ajax',

            sf_padrao_id = {{ \Route::current()->parameter('sf_padrao_id' )}},
            apropriacao_id = {{ \Route::current()->parameter('apropriacao_id' )}},
            formAbaDeducao = $('#novo-recolhedor' + idDeducao).closest('form'),
            formData = formAbaDeducao.serialize();

        axios.post(route, {
            sf_padrao_id,
            apropriacao_id,
            idDeducao,
            idPreDoc,
            formData
        })
            .then(response => {
                $('#deducao').html(response.data);

                $('.box').boxWidget({
                    animationSpeed: 500,
                    collapseTrigger: '[data-widget="collapse"]',
                    removeTrigger: '[data-widget="remove"]',
                    collapseIcon: 'fa-minus',
                    expandIcon: 'fa-plus',
                    removeIcon: 'fa-times'
                })

                $('.codugpgtodeducao').select2({
                    width: '100%'
                })

                initTooltipsDeducao()
                atualizaTotaisRodapeRecolhedor(idDeducao)
                iniciaFuncoesGerais()
            })
            .catch(error => {
                new PNotify({
                    title: 'Error',
                    text: 'Erro na geração/remoção do pré-doc',
                    type: "error"
                });
            })
    }

    function alteraCodigoMunicipioDeducao(idDeducao, element, id) {
        if (element.value && $('#' + id + idDeducao).val() === '') {
            $('#' + id + idDeducao).val(element.value)
        }
    }

    function relacionarEmpenhoDeducao(idDeducao, idRelacionamento = null, delete_sfrelacionamentodeducao = false) {
        var empenhosSelecionados = [];

        $('.chebox-relacionamento:checked').each(function () {
            empenhosSelecionados.push({
                idDeducao: idDeducao,
                pcoitem_numseqitem: $(this).attr('data-pcoitem-numseqitem'),
                pco_numseqitem: $(this).attr('data-pco-numseqitem'),
                sfpcoitem_id: $(this).attr('data-pco-sfpcoitem_id'),
            });
        });

        if (empenhosSelecionados.length === 0 && !delete_sfrelacionamentodeducao) {
            new PNotify({
                title: 'Alerta',
                text: 'É necessário selecionar pelo menos um empenho.',
                type: "warning"
            });
            return;
        }

        var route = '/apropriacao/fatura-form/novo/vincularempenhodeducao/ajax',

            sf_padrao_id = {{ \Route::current()->parameter('sf_padrao_id' )}},
            apropriacao_id = {{ \Route::current()->parameter('apropriacao_id' )}},
            formAbaDeducao = $('#novo-recolhedor' + idDeducao).closest('form'),
            formData = formAbaDeducao.serialize();

        $('#relacionamento-deducao-' + idDeducao).modal('hide')

        $('#form-aba-deducao').on('submit', function (event) {
            event.preventDefault();
        });

        axios.post(route, {
            sf_padrao_id,
            apropriacao_id,
            idDeducao,
            idRelacionamento,
            empenhosSelecionados,
            delete_sfrelacionamentodeducao,
            formData
        })
            .then(response => {
                $('#deducao').html(response.data);

                $('.box').boxWidget({
                    animationSpeed: 500,
                    collapseTrigger: '[data-widget="collapse"]',
                    removeTrigger: '[data-widget="remove"]',
                    collapseIcon: 'fa-minus',
                    expandIcon: 'fa-plus',
                    removeIcon: 'fa-times'
                })

                $('.codugpgtodeducao').select2({
                    width: '100%'
                })
                initTooltipsDeducao()
                atualizaTotaisRodapeRecolhedor(idDeducao)
                iniciaFuncoesGerais()
            })
            .catch(error => {
                new PNotify({
                    title: 'Error',
                    text: 'Erro em adicionar/remoção do relacionamento.',
                    type: "error"
                });
            })
    }

    function clickBtnRemoverOuCriarNovoAcrescimo(idDeducao = null, idAcrescimo = null) {

        var route = '/apropriacao/fatura-form/novo/sfacrescimo/ajax',

            sf_padrao_id = {{ \Route::current()->parameter('sf_padrao_id' )}},
            apropriacao_id = {{ \Route::current()->parameter('apropriacao_id' )}},
            formAbaDeducao = $('#novo-acrescimo' + idDeducao).closest('form'),
            formData = formAbaDeducao.serialize();

        $('#form-aba-deducao').on('submit', function (event) {
            event.preventDefault();
        });

        $('#btn-add-novo-item-acrescimo' + idDeducao).html(
            '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>'
        )

        axios.post(route, {
            sf_padrao_id,
            apropriacao_id,
            idDeducao,
            idAcrescimo,
            formData
        })
            .then(response => {
                $('#deducao').html(response.data);

                $('.box').boxWidget({
                    animationSpeed: 500,
                    collapseTrigger: '[data-widget="collapse"]',
                    removeTrigger: '[data-widget="remove"]',
                    collapseIcon: 'fa-minus',
                    expandIcon: 'fa-plus',
                    removeIcon: 'fa-times'
                })

                $('.codugpgtodeducao').select2({
                    width: '100%'
                })

                atualizaCabecalhoTabelaAcrescimo()
                atualizaTotaisRodapeRecolhedor(idDeducao)
                iniciaFuncoesGerais()

            })
            .catch(error => {
                new PNotify({
                    title: 'Error',
                    text: 'Erro na geração/remoção do item recolhimento',
                    type: "error"
                });
            })
    }

    function clickBtnRemoverOuCriarNovoRecolhimento(idDeducao = null, idRecolhedor = null) {

        var route = '/apropriacao/fatura-form/novo/sfitemrecolhimento/ajax',

            sf_padrao_id = {{ \Route::current()->parameter('sf_padrao_id' )}},
            apropriacao_id = {{ \Route::current()->parameter('apropriacao_id' )}},
            formAbaDeducao = $('#novo-recolhedor' + idDeducao).closest('form'),
            formData = formAbaDeducao.serialize();

        $('#form-aba-deducao').on('submit', function (event) {
            event.preventDefault();
        });

        $('#btn-add-novo-item-recolhimento' + idDeducao).html(
            '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>'
        )

        axios.post(route, {
            sf_padrao_id,
            apropriacao_id,
            idDeducao,
            idRecolhedor,
            formData
        })
            .then(response => {
                $('#deducao').html(response.data);

                $('.box').boxWidget({
                    animationSpeed: 500,
                    collapseTrigger: '[data-widget="collapse"]',
                    removeTrigger: '[data-widget="remove"]',
                    collapseIcon: 'fa-minus',
                    expandIcon: 'fa-plus',
                    removeIcon: 'fa-times'
                })

                $('.codugpgtodeducao').select2({
                    width: '100%'
                })

                atualizaTotaisRodapeRecolhedor(idDeducao)
                iniciaFuncoesGerais()
            })
            .catch(error => {
                new PNotify({
                    title: 'Error',
                    text: 'Erro na geração/remoção do item recolhimento',
                    type: "error"
                });
            })
    }

    function atualizaValorPrincipal(element, id_recolhedor) {

        if (situacaoSiafi === 'APR') {
            return
        }

        var porcentagem = parseFloat(element.value.replace(/\./g, '').replace(',', '.'))

        if (!isNaN(porcentagem)) {

            var valorBaseCalculo = $('#vlrbasecalculo' + id_recolhedor).val().replace(/\./g, '').replace(',', '.');
            var valorCalculado = valorBaseCalculo * (porcentagem / 100);

            // Formata o valor com duas casas decimais e separadores de milhar
            $('#vlrPrincipal' + id_recolhedor).val(valorCalculado.toLocaleString('pt-BR', {minimumFractionDigits: 2, maximumFractionDigits: 2}));
        }
    }

    function atualizaTotaisRodapeRecolhedor(idDeducao) {

        if (situacaoSiafi === 'APR') {
            return
        }

        function calculaTotal(campo) {
            var total = campo.map(function () {
                return parseFloat($(this).val().replace(/\./g, '').replace(',', '.')) || 0
            }).get()
                .reduce(function (acc, value) {
                    return acc + value
                }, 0)
            return total.toLocaleString('pt-BR', {style: 'currency', currency: 'BRL'});
        }

        var totalVlrPrincipal = calculaTotal($(`[id^="vlrPrincipal${idDeducao}"]`))
        var totalVlrMulta = calculaTotal($(`[id^="vlrmulta${idDeducao}"]`))
        var totalVlrJuros = calculaTotal($(`[id^="vlrjuros${idDeducao}"]`))

        $('#total-vlrPrincipal-' + idDeducao).text(totalVlrPrincipal)
        $('#total-vlrMulta-' + idDeducao).text(totalVlrMulta)
        $('#total-vlrJuros-' + idDeducao).text(totalVlrJuros)

        var valorTotal = (
            parseFloat(totalVlrPrincipal.replace(/\./g, '').replace(',', '.').replace('R$', '')) +
            parseFloat(totalVlrMulta.replace(/\./g, '').replace(',', '.').replace('R$', '')) +
            parseFloat(totalVlrJuros.replace(/\./g, '').replace(',', '.').replace('R$', ''))
        )

        $('#valor-total-informar-' + idDeducao).text(valorTotal.toLocaleString('pt-BR', {style: 'currency', currency: 'BRL'}))
        //Adiciona o valor total para a deducao item
        $('#vlr' + idDeducao).val(valorTotal.toLocaleString('pt-BR', {minimumFractionDigits: 2, maximumFractionDigits: 2}))
    }

    $(window).resize(function () {
        checkResponsive();
    });

    function checkResponsive() {
        if ($(window).width() < 768) {
            $(".tabela-responsiva").addClass("table-responsive");
        } else {
            $(".tabela-responsiva").removeClass("table-responsive");
        }
    }

    function clickBtnNovaAbaSituacaoDeducao(idDeducao = null, delete_sfDeducao = false) {

        var sf_padrao_id = {{ \Route::current()->parameter('sf_padrao_id' )}},
            apropriacao_id = {{ \Route::current()->parameter('apropriacao_id' )}},
            numseqitem = parseInt($('.count-situacao-deducao').data('count-situacao-deducao') + 1),
            route = '/apropriacao/fatura-form/novo/deducao/ajax',
            htmlBtnAddNovoPcoItemWrapper = $('#nova-aba-situacao-deducao' + idDeducao).html(),
            formAbaDeducao = $('[id^="novo-recolhedor"]').closest('form'),
            formData = formAbaDeducao.serialize(),
            encontrouFalse = false;

        $('.sfdeducaoApropriacaoConfirmaDados').each(function () {
            if ($(this).val() === 'false') {
                encontrouFalse = true;
                return false;
            }
        })

        if (encontrouFalse && !delete_sfDeducao) {
            return new PNotify({
                title: 'Atenção',
                text: 'Para criar nova situação é necessário confirmar os dados.',
                type: "warning"
            });
        }

        $('#nova-aba-situacao-deducao').html(
            '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>'
        )

        $('#form-aba-deducao').on('submit', function (event) {
            event.preventDefault();
        });

        axios.post(route, {
            delete_sfDeducao,
            sf_padrao_id,
            apropriacao_id,
            idDeducao,
            numseqitem,
            formData
        })
            .then(response => {

                if (response.data.status === 'error') {
                    Swal.fire('Alerta!', response.data.mensagem, 'error');

                    $('#nova-aba-situacao-deducao').html(
                        htmlBtnAddNovoPcoItemWrapper
                    )
                }

                if (response.data.status !== 'error') {
                    $('#deducao').html(response.data);
                }

                //nescessario fazer o binding da acao dos collapse novamente, pois com a chamada do ajax ela se perde
                $('.box').boxWidget({
                    animationSpeed: 500,
                    collapseTrigger: '[data-widget="collapse"]',
                    removeTrigger: '[data-widget="remove"]',
                    collapseIcon: 'fa-minus',
                    expandIcon: 'fa-plus',
                    removeIcon: 'fa-times'
                })

                $('.codugpgtodeducao').select2({
                    width: '100%'
                })

                atualizaCabecalhoTabelaAcrescimo()
                atualizaTotaisRodapeRecolhedor(idDeducao)
                iniciaFuncoesGerais(true)

            })
            .catch(error => {

                new PNotify({
                    title: 'Error',
                    text: 'Erro ao criar nova aba de situação',
                    type: "error"
                });
            })
    }

    function bindCampoSituacaoNaAba(element) {
        $(element).data("nome-aba");
        var id_aba = $(element).data("nome-aba");

        if (element.value === '') {
            $('#' + id_aba).html(' - ');
        } else {
            $('#' + id_aba).html(element.value);
        }
    }

    function alteraDescricaoSituacaoDeducao(element, idDeducao) {

        if (element.value.length >= 6 && situacaoSiafi !== 'APR') {

            var route = "{{ route('apropriacao.faturas.retornaescricaoodigodeducao.ajax') }}";

            axios.post(route, {
                'codsit': element.value,
                'idDeducao': idDeducao,
                'sf_padrao_id': {{ \Route::current()->parameter('sf_padrao_id' )}},
                'apropriacao_id': {{ \Route::current()->parameter('apropriacao_id' )}}
            })
                .then(response => {

                    if (response.data.status === 'error') {
                        Swal.fire('Alerta!', response.data.mensagem, 'error');


                    }

                    if (response.data.status !== 'error') {
                        $('#deducao').html(response.data);
                    }

                    //nescessario fazer o binding da acao dos collapse novamente, pois com a chamada do ajax ela se perde
                    $('.box').boxWidget({
                        animationSpeed: 500,
                        collapseTrigger: '[data-widget="collapse"]',
                        removeTrigger: '[data-widget="remove"]',
                        collapseIcon: 'fa-minus',
                        expandIcon: 'fa-plus',
                        removeIcon: 'fa-times'
                    })

                    $('.codugpgtodeducao').select2({
                        width: '100%'
                    })

                    vincularAutomaticoEmpenhoDeducao(idDeducao)

                    atualizaTotaisRodapeRecolhedor(idDeducao)
                    iniciaFuncoesGerais()

                })
                .catch(error => {
                    $('#descricaoSituacao' + idDeducao).text('-')
                    new PNotify({
                        title: 'Error',
                        text: 'Erro ao alterar situação',
                        type: "error"
                    });
                })
        }
    }

    $(window).resize(function () {
        checkResponsive();
    });

    function vincularAutomaticoEmpenhoDeducao(deducao_id) {
        var tabela = $('#relacionamento-deducao-' + deducao_id);
        var totalCheckboxes = tabela.find('table tbody tr td input[type="checkbox"]').length;
        if (totalCheckboxes !== undefined && totalCheckboxes === 1) {

            var checkbox = tabela.find('table tbody tr td input[type="checkbox"]');
            checkbox.prop('checked', true);
            relacionarEmpenhoDeducao(deducao_id);

        }
    }

    function verificaEmpenhosSelecionados(deducao_id) {
        var tabela = $('#relacionamento-deducao-' + deducao_id);
        var checkboxes = tabela.find('table tbody tr td input[type="checkbox"]');

        var algumSelecionado = checkboxes.filter(':checked').length > 0;

        if (algumSelecionado) {
            checkboxes.not(':checked').prop('disabled', true);
        } else {
            checkboxes.prop('disabled', false);
        }
    }

    function verificaEmpenhosRelacionados(deducao_id) {
        var tabelaRelacionados = $('.empenhos_relacionados' + deducao_id);
        var tabelaRelacionaEmpenhos = $('#relacionamento-deducao-' + deducao_id);
        var checkbox = tabelaRelacionaEmpenhos.find('table tbody tr td input[type="checkbox"]');
        var totalEmpenhos = tabelaRelacionados.find('tbody tr').length;

        checkbox.prop('checked', false);

        if (totalEmpenhos !== undefined && totalEmpenhos === 1) {
            checkbox.prop('disabled', true);
        } else {
            verificaEmpenhosSelecionados(deducao_id)
            checkbox.prop('disabled', false);
        }
    }

    function clickBtnRemoverOuCriarNovaDeducao(
        deducao_id = null,
        idPcoItem = null
    ) {
        var sf_padrao_id = {{ \Route::current()->parameter('sf_padrao_id' )}},
            numseqitem = !idPcoItem ? parseInt($('.count-deducao').last().data('count-deducao')) + 1 : parseInt($('#' + idPcoItem).data('numseqitem')),
            route = '/apropriacao/fatura-form/novo/deducao/ajax',
            apropriacao_id = {{ \Route::current()->parameter('apropriacao_id' )}},
            delete_sfDeducao = false,
            htmlBtnAddNovaDeducaoWrapper = $('#btn-add-nova-deducao-wrapper').html();

        $('#btn-add-nova-deducao-wrapper').html(
            '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>'
        )

        axios.post(route, {
            sf_padrao_id,
            deducao_id,
            delete_sfDeducao,
            apropriacao_id,
            numseqitem
        })
            .then(response => {
                if (response.data.status === 'error') {
                    Swal.fire('Alerta!', response.data.mensagem, 'error');

                    $('#btn-add-nova-deducao-wrapper' + deducao_id).html(
                        htmlBtnAddNovaDeducaoWrapper
                    )
                }
                if (response.data.status !== 'error') {
                    $('#deducao').html(response.data);
                }

                $('.box').boxWidget({
                    animationSpeed: 500,
                    collapseTrigger: '[data-widget="collapse"]',
                    removeTrigger: '[data-widget="remove"]',
                    collapseIcon: 'fa-minus',
                    expandIcon: 'fa-plus',
                    removeIcon: 'fa-times'
                })

                $('.codugpgtodeducao').select2({
                    width: '100%'
                })

                initTooltipsDeducao();
            })
            .catch(error => {

                new PNotify({
                    title: 'Error',
                    text: 'Erro ao criar nova aba de situação',
                    type: "error"
                });
            })
            .finally(function () {

            })
    }

    function alteraUnidadePagamentoDeducao(element, idDeducao) {

        if (element.value.length == 6 && situacaoSiafi !== 'APR') {

            var sf_padrao_id = {{ \Route::current()->parameter('sf_padrao_id' )}},
                codUgEmpenho = element.value,
                route = "{{ route('apropriacao.faturas.atualizacodugpagadoradeducao.ajax') }}";

            $('input[id^="codsubitemempecadastrado"]').each(function () {
                if (this.id.endsWith(idDeducao)) {
                    $(this).trigger('keyup');
                }
            });

            $.ajax({
                url: route,
                data: {
                    'sf_padrao_id': sf_padrao_id,
                    'codUgEmpenho': codUgEmpenho,
                    'idDeducao': idDeducao
                },
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.mensagem) {
                        $('#nomeugempeDeducao' + idDeducao).text(data.nomeUnidade)

                        new PNotify({
                            title: 'Atenção',
                            delay: 3000,
                            text: data.mensagem,
                            type: "success"
                        });
                    } else {
                        new PNotify({
                            title: 'Atenção',
                            delay: 3000,
                            text: data.error,
                            type: "alert"
                        });
                    }

                },
                error: function (error) {
                    $('#nomeugempeDeducao' + idDeducao).text('-')
                    new PNotify({
                        title: 'Atenção',
                        delay: 3000,
                        text: 'Não foi possivel alterar a unidade pagadora.',
                        type: "alert"
                    });
                }
            });

        }
    }

</script>
