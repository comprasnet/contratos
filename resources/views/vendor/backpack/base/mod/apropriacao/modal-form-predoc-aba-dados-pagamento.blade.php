<div class="modal fade" tabindex="-1" role="dialog"
    id="modal-form-predoc-aba-dados-pagamento{!! $dadosPreDoc->id !!}">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" id="modal-content">
            <div class="modal-header text-center custom-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title"><i class="fa fa-credit-card"></i> Ordem Bancária</h4>
            </div>
            <div id="modalForm">
                <div class="modal-fixed-section">
                    <div class="row">
                        <div class="col-md-3">
                            <label>Líquido</label><br>
                            <span>{!! $dadosPreDoc->liquido !!}</span>
                        </div>
                        <div class="col-md-3">
                            <label>Data de Vencimento</label><br>
                            <span>{!! $sfDadosBasicos->dtvenc !!}</span>
                        </div>
                        <div class="col-md-3">
                            <label>Data de Pagamento</label><br>
                            <span id="modalValorCampo"></span>
                        </div>
                        @php
                            $valor = $favorecido->vlr;
                        @endphp
                        <div class="col-md-3">
                            <label>Valor</label><br>
                            <span>{{ number_format($valor ?? 0, 2, ',', '.') }}</span>
                        </div>
                        <div class="col-md-3 form-group">
                            <label>Tipo de OB</label>
                            <select class="form-control" name="codtipoob" id="codtipoob" required {{$situacaoApropriacao === 'APR' ? 'disabled' : ''}}>
                                <option value="OBA" {{ isset($sfPreDoc) && $sfPreDoc->codtipoob == 'OBA' ? 'selected' : '' }}>OB Aplicação</option>
                                <option value="OBB" {{ isset($sfPreDoc) && $sfPreDoc->codtipoob == 'OBB' ? 'selected' : '' }}>OB Banco</option>
                                <option value="OBK" {{ isset($sfPreDoc) && $sfPreDoc->codtipoob == 'OBK' ? 'selected' : '' }}>OB Câmbio</option>
                                <option value="OBC" {{ (!isset($sfPreDoc) || $sfPreDoc->codtipoob == null || $sfPreDoc->codtipoob == 'OBC') ? 'selected' : '' }}>OB Crédito</option>
                                <option value="OBD" {{ isset($sfPreDoc) && $sfPreDoc->codtipoob == 'OBD' ? 'selected' : '' }}>OB Fatura</option>
                                <option value="OBJ" {{ isset($sfPreDoc) && $sfPreDoc->codtipoob == 'OBJ' ? 'selected' : '' }}>OB Judicial</option>
                                <option value="OBF" {{ isset($sfPreDoc) && $sfPreDoc->codtipoob == 'OBF' ? 'selected' : '' }}>OB Pagamento</option>
                                <option value="OBH" {{ isset($sfPreDoc) && $sfPreDoc->codtipoob == 'OBH' ? 'selected' : '' }}>OB Processo Judicial</option>
                                <option value="OBSTN" {{ isset($sfPreDoc) && $sfPreDoc->codtipoob == 'OBSTN' ? 'selected' : '' }}>OB STN</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Valor Pré-Doc</label><br>
                            <span>{{ number_format($valor ?? 0, 2, ',', '.') }}</span>
                        </div>
                    </div>
                </div>

                <div class="modal-body scrollable-body">
                    <div>
                        <h4 class="title">OB</h4>
                        <div class="row">
                            <div class="col-md-3">
                                <label>Favorecido</label><br>
                                <div id="codcredordevedorPreDoc"></div>
                            </div>
                            <div class="col-md-4">
                                <label>Nome</label><br>
                                <div id="nomeCredordevedorPreDoc"></div>
                            </div>
                            <div class="col-md-3">
                                <label>Processo</label><br>
                                <input type="text" class="form-control" name="txtprocesso" id="txtprocesso" value="{{ $sfPreDoc && isset($sfPreDoc->txtprocesso) ? $sfPreDoc->txtprocesso : $sfDadosBasicos->txtprocesso }}">
                            </div>
                            <div class="col-md-2">
                                <label>Taxa de câmbio</label><br>
                                <input type="text" class="form-control" name="taxacambio" id="taxacambio" value="{{ $sfPreDoc && isset($sfPreDoc->vlrTaxaCambio) ? $sfPreDoc->vlrTaxaCambio : 0,00  }}" onkeyup="atualizarMaskMoney(this);">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 form-group">
                                <label>Número da Lista:</label>
                                <input type="text" class="form-control" name="codnumlista" id="codnumlista" value="{{ $sfPreDoc && isset($sfPreDoc->codnumlista) ? $sfPreDoc->codnumlista : '' }}">
                            </div>
                            <div class="col-md-3 form-group">
                                <label>CIT:</label>
                                <input type="text" class="form-control" name="txtcit" id="txtcit" value="{{ $sfPreDoc && isset($sfPreDoc->txtcit) ? $sfPreDoc->txtcit : '' }}">
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div>
                        <h4 class="title">Domicílio Bancário do Favorecido</h4>
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label>Banco:</label>
                                <input type="text" class="form-control" name="bancoFavorecido" id="bancoFavorecido">
                            </div>
                            <div class="col-md-4 form-group">
                                <label>Agência:</label>
                                <input type="text" class="form-control" name="agenciaFavorecido" id="agenciaFavorecido">
                            </div>
                            <div class="col-md-4 form-group">
                                <label>Conta:</label>
                                <div class="{{$situacaoApropriacao === 'AND' || $situacaoApropriacao === 'ERR' ? 'input-group' : ''}}">
                                    <input type="text" class="form-control" name="contaFavorecido" id="contaFavorecido" required>
                                    @if($situacaoApropriacao === 'AND' || $situacaoApropriacao === 'ERR')
                                    <span
                                        class="input-group-addon pointer-hand openMiniModal"
                                        id="buttonContasFavorecido"
                                        data-tipo="Favorecido">
                                        <i class="fa fa-search"></i>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div>
                        <h4 class="title">Domicílio Bancário do Pagador</h4>
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label>Banco:</label>
                                <input type="text" class="form-control" name="bancoPagador" id="bancoPagador">
                            </div>
                            <div class="col-md-4 form-group">
                                <label>Agência:</label>
                                <input type="text" class="form-control" name="agenciaPagador" id="agenciaPagador">
                            </div>
                            <div class="col-md-4 form-group">
                                <label>Conta:</label>
                                <div class="{{$situacaoApropriacao === 'AND' || $situacaoApropriacao === 'ERR' ? 'input-group' : ''}}">
                                    <input type="text" class="form-control" name="contaPagador" id="contaPagador" value="UNICA">
                                    @if($situacaoApropriacao === 'AND' || $situacaoApropriacao === 'ERR')
                                    <span
                                        class="input-group-addon pointer-hand openMiniModal"
                                        id="buttonContasPagador"
                                        data-tipo="Pagador"
                                        data-cnpj-ug="{{ $sfDadosBasicos->codugpgto }}">
                                        <i class="fa fa-search"></i>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="observation-section form-group">
                        <label>Observação</label>
                        <textarea class="form-control" rows="4" maxlength="462" name="obser" id="obser" required>{{ $sfPreDoc && isset($sfPreDoc->txtobser) ? $sfPreDoc->txtobser : '' }}</textarea>
                    </div>
                </div>
                @if($situacaoApropriacao === 'AND' || $situacaoApropriacao === 'ERR')
                <div class="modal-footer">
                    <button type="button" id="btnSalvar" class="btn btn-success">
                        <span class="fa fa-save"></span>&nbsp;Salvar
                    </button>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="miniModal" tabindex="-1" role="dialog" aria-labelledby="miniModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header custom-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="miniModalLabel"></h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive" style="max-height: 300px; overflow-y: auto;">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th></th>
                                <th class="text-center">Banco</th>
                                <th class="text-center">Agência</th>
                                <th class="text-center">Conta</th>
                            </tr>
                        </thead>
                        <tbody id="bancosLista">
                            <tr id="loadingRow">
                                <td colspan="4" class="text-center">Carregando...</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="button" id="btnPreencher" class="btn btn-primary">Preencher Banco</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    function atualizarCamposPorTipo() {
        var codtipoob = document.getElementById('codtipoob');

        function aplicarRegrasPorTipo(tipo) {

            $('#codnumlista').val('');
            $('#txtcit').val('');

            $('#bancoFavorecido').val('');
            $('#agenciaFavorecido').val('');

            if (tipo == "OBA") {
                document.getElementById('codnumlista').disabled = true;
                document.getElementById('txtcit').disabled = false;

                $('#contaFavorecido').val('APLICACAO');
                $('#contaPagador').val('UNICA');
            }

            if (tipo == "OBC") {
                document.getElementById('codnumlista').disabled = true;
                document.getElementById('txtcit').disabled = true;

                document.getElementById('obser').required = true;

                $('#codnumlista').val('');
                $('#txtcit').val('');

                $('#contaFavorecido').val('');
                $('#contaPagador').val('UNICA');
            }

            if (tipo == "OBB") {
                document.getElementById('codnumlista').disabled = false;
                document.getElementById('txtcit').disabled = false;

                $('#contaFavorecido').val('BANCO');
                $('#contaPagador').val('UNICA');
            }

            if (tipo == "OBK") {
                document.getElementById('codnumlista').disabled = false;
                document.getElementById('txtcit').disabled = false;

                $('#contaFavorecido').val('CAMBIO');
                $('#contaPagador').val('UNICA');
            }

            if (tipo == "OBD") {
                document.getElementById('codnumlista').disabled = false;
                document.getElementById('txtcit').disabled = true;

                $('#contaFavorecido').val('FATURA');
                $('#contaPagador').val('UNICA');
            }

            if (tipo == "OBJ") {
                document.getElementById('codnumlista').disabled = true;
                document.getElementById('txtcit').disabled = true;

                $('#contaFavorecido').val('JUDICIAL');
                $('#contaPagador').val('UNICA');
            }

            if (tipo == "OBF") {
                document.getElementById('codnumlista').disabled = true;
                document.getElementById('txtcit').disabled = true;

                $('#contaFavorecido').val('PAGAMENTO');
                $('#contaPagador').val('UNICA');
            }

            if (tipo == "OBH") {
                document.getElementById('codnumlista').disabled = false;
                document.getElementById('txtcit').disabled = false;

                $('#contaFavorecido').val('PROCJUD');
                $('#contaPagador').val('UNICA');
            }

            if (tipo == "OBSTN") {
                document.getElementById('codnumlista').disabled = false;
                document.getElementById('txtcit').disabled = false;

                $('#contaFavorecido').val('STN');
                $('#contaPagador').val('UNICA');
            }
        }
        codtipoob.addEventListener('change', function() {
            aplicarRegrasPorTipo(codtipoob.value);
        });

        aplicarRegrasPorTipo(codtipoob.value);
    }

    function abreModal() {
        const openMiniModalButtons = document.querySelectorAll(".openMiniModal");

        openMiniModalButtons.forEach(function(button) {
            button.addEventListener("click", function() {
                const tipo = $(this).data('tipo');
                let cnpjUg;

                if (tipo === "Favorecido") {
                    cnpjUg = $('#codcredordevedorPreDoc').text();
                }else{
                    cnpjUg = $(this).data('cnpj-ug');
                }

                $('#miniModal').modal({
                    backdrop: false,
                    keyboard: true
                });

                $('#miniModal').data('target', tipo);

                $('#bancosLista').html('<tr id="loadingRow"><td colspan="4" class="text-center">Carregando...</td></tr>');

                const btnPreencher = $('#btnPreencher');
                btnPreencher.prop('disabled', true);
                btnPreencher.addClass('disabled');

                $('#miniModalLabel').html('<i class="fa fa-credit-card"></i> Contas ' + tipo);
                consultaBancos(tipo, cnpjUg)
                    .then(function(bancos) {

                        atualizaTabelaBancos(bancos);
                    })
                    .catch(function(error) {
                        alert("Ocorreu um erro ao consultar os bancos.");
                        $('#bancosLista').html('<tr><td colspan="4" class="text-center">Erro ao carregar dados.</td></tr>');
                    });
            });
        });
    }

    function atualizaTabelaBancos(bancos) {
        const tbody = $("#bancosLista");
        tbody.empty();

        if (bancos.length === 0) {
            tbody.append(`
            <tr>
                <td colspan="4" class="text-center">Nenhum banco encontrado.</td>
            </tr>
        `);
            return;
        }

        bancos.forEach(function(banco) {
            const row = `
            <tr>
                <td>
                    <input class="pointer-hand" type="radio" name="selectAccount"
                           data-banco="${banco.banco}"
                           data-agencia="${banco.agencia}"
                           data-conta="${banco.conta}">
                </td>
                <td>${banco.banco ? banco.banco : ''}</td>
                <td>${banco.agencia ? banco.agencia : ''}</td>
                <td>${banco.conta ? banco.conta : ''}</td>
            </tr>
        `;
            tbody.append(row);
        });
        const btnPreencher = $('#btnPreencher');

        btnPreencher.prop('disabled', false);
        btnPreencher.removeClass('disabled');
    }

    function consultaBancos(tipo, cnpjUg) {
        return new Promise(function(resolve, reject) {
            $.ajax({
                url: "{{ route('fatura.form.consulta-bancos') }}",
                method: 'GET',
                data: {
                    tipo: tipo,
                    cnpjUg: cnpjUg
                },

                success: function(response) {
                    resolve(response);
                },
                error: function(xhr) {
                    reject(xhr);
                }
            });
        });
    }

    function preencherBanco() {
        const btn = document.getElementById("btnPreencher");

        btn.addEventListener("click", function(event) {
            event.preventDefault();

            const selectedRadio = $('input[name="selectAccount"]:checked');

            if (selectedRadio.length === 0) {
                new PNotify({
                    title: "Erro",
                    text: "Selecione um banco!",
                    type: "error"
                });
                return;
            }

            var banco = selectedRadio.data('banco');
            var agencia = selectedRadio.data('agencia');
            var conta = selectedRadio.data('conta');

            var target = $('#miniModal').data('target');

            if (target === 'Favorecido') {

                $('#bancoFavorecido').val(banco);
                $('#agenciaFavorecido').val(agencia);
                $('#contaFavorecido').val(conta);
            }

            if (target === 'Pagador') {
                $('#bancoPagador').val(banco);
                $('#agenciaPagador').val(agencia);
                $('#contaPagador').val(conta);
            }
            $('input[name="selectAccount"]').prop('checked', false);

            $('#miniModal').modal('hide');
        });
    }

    function validaForm() {
        const btnSalvar = document.getElementById("btnSalvar");
        const modalId = "modalForm";

        if (btnSalvar) {
            btnSalvar.addEventListener("click", function(event) {
                event.preventDefault();

                var url = "{{ route('fatura.form.salva-pre-doc',[$sfDadosBasicos->sfpadrao_id, Route::current()->parameter('apropriacao_id')]) }}";
                const isValid = validarCamposObrigatorios(modalId);

                if (isValid) {

                    let codcredor = $('#codcredordevedorPreDoc').text();

                    var formData = {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        codcredor: codcredor,
                        codtipoob: $('#codtipoob').val(),
                        txtprocesso: $('#txtprocesso').val(),
                        taxacambio: $('#taxacambio').val(),
                        codnumlista: $('#codnumlista').val(),
                        txtcit: $('#txtcit').val(),
                        bancoFavorecido: $('#bancoFavorecido').val(),
                        agenciaFavorecido: $('#agenciaFavorecido').val(),
                        contaFavorecido: $('#contaFavorecido').val(),
                        bancoPagador: $('#bancoPagador').val(),
                        agenciaPagador: $('#agenciaPagador').val(),
                        contaPagador: $('#contaPagador').val(),
                        obser: $('#obser').val(),
                    };

                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: formData,
                        success: function(response) {
                            $('#modal-form-predoc-aba-dados-pagamento{!! $dadosPreDoc->id !!}').modal('hide');
                            // Remover backdrop e resetar o body
                            $('body').removeClass('modal-open');
                            $('.modal-backdrop').remove();

                            Swal.fire({
                                icon: 'success',
                                title: 'Dados salvo com sucesso!',
                                showConfirmButton: false,
                                timer: 1500
                            })

                            // carrega a aba novamente
                            $('#dados-pagamento-tab').removeClass('loaded');
                            carregaAba();

                        },
                        error: function(error) {
                            new PNotify({
                                title: "Erro",
                                text: "Ocorreu um erro ao enviar os dados.",
                                type: "error"
                            });
                        }
                    });
                }
            });
        }
    }

    function limparMensagensErro(campo) {
        const formGroup = campo.closest('.form-group');
        if (formGroup) {
            const errorMsg = formGroup.querySelector('.invalid-feedback');
            if (errorMsg) {
                errorMsg.remove();
            }
        }
    }

    function exibirMensagemErro(campo) {
        const formGroup = campo.closest('.form-group');
        if (formGroup) {
            const errorMsg = document.createElement("div");
            errorMsg.classList.add("invalid-feedback");
            errorMsg.textContent = "Este campo é obrigatório.";

            if (!formGroup.querySelector('.invalid-feedback')) {
                formGroup.appendChild(errorMsg);
            }
        }
    }

    function validarCamposObrigatorios(modalId) {
        let isValid = true;

        const modal = document.querySelector(`#${modalId}`);

        const requiredFields = modal.querySelectorAll("[required]");

        requiredFields.forEach(campo => {
            limparMensagensErro(campo);

            if (!campo.value.trim()) {
                isValid = false;
                campo.classList.add("is-invalid");
                exibirMensagemErro(campo);
            } else {
                campo.classList.remove("is-invalid");
            }
        });

        return isValid;
    }

    function carregaDomicioBancario(){
        var bancos = consultaBancos('Favorecido', '{{ $sfDadosBasicos->codcredordevedor }}');
        bancos.then(function(response) {
            var ultimoBancoCadastrado = response.shift();

            var bancoPredocFavo = @json(optional($sfPreDoc)->sfdomiciliobancarioFavo);

            var preencheBancoFavo = bancoPredocFavo ? bancoPredocFavo : ultimoBancoCadastrado;

            if (preencheBancoFavo) {
                $('#bancoFavorecido').val(preencheBancoFavo.banco);
                $('#agenciaFavorecido').val(preencheBancoFavo.agencia);
                $('#contaFavorecido').val(preencheBancoFavo.conta);
            }

            var bancoPredocPgto = @json(optional($sfPreDoc)->sfdomiciliobancarioPgto);

            if (bancoPredocPgto) {
                $('#bancoPagador').val(bancoPredocPgto.banco);
                $('#agenciaPagador').val(bancoPredocPgto.agencia);
                $('#contaPagador').val(bancoPredocPgto.conta);
            }
        });
    }

    /*document.addEventListener("DOMContentLoaded", function() {

        validaForm();
        abreModal();
        preencherBanco();
        atualizarCamposPorTipo();
        carregaDomicioBancario();
        maskProcessoModalPredoc();
    });*/

    function maskProcessoModalPredoc()
    {
        $('#processo').mask('99999.999999/9999-99');
    }

    /*$(document).ready(function() {

    });*/
</script>

<style>
    .modal-header.custom-header {
        color: #fff;
        background: #3c8dbc;
        background-color: #3c8dbc;
    }

    .modal-header.custom-header .close {
        color: #fff; /* Define a cor do botão de fechar */
        opacity: 1; /* Garante que a cor fique visível */
    }
    .modal-fixed-section {
        position: sticky;
        top: 0;
        background-color: #f9f9f9;
        padding: 10px 15px;
        border-bottom: 1px solid #ddd;
        z-index: 1000;
    }

    .scrollable-body {
        max-height: 400px;
        overflow-y: auto;
        padding-top: 15px;
    }

    .modal-body .observation-section textarea {
        max-height: 300px;
        overflow-y: auto;
        resize: vertical;
    }

    .is-invalid {
        border-color: #dc3545;
    }

    .invalid-feedback {
        color: #dc3545;
        font-size: 0.875em;
        margin-top: 0.25rem;
    }

    .title {
        display: flex;
        justify-content: flex-start;
        align-items: flex-start;
        margin-bottom: 10px;
        padding-left: 10px;
    }
</style>
