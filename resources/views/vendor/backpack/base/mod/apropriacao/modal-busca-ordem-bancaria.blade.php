<button
    id="{{ $idBtn }}"
    name="{{ $name }}"
    type="button"
    class="{{$class}}"
    title="{{ $hint }}"
    data-numDocumento="{{ $numDocumento }}"
    data-sfpadrao_id="{{ $sfpadrao_id }}"
    onclick="abrirModalOrdemBancaria(this)"
>
    <i class="fa fa-credit-card"></i>
</button>

<!-- Janela modal para inserção de registros -->
<div id="modal-busca-ordem-bancaria" tabindex="-1" class="modal fade"
     role="dialog"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="headerOrdem">
                    <h3 class="modal-title">
                        <span><i class="fa fa-credit-card" style="font-size: 16px;"></i> <small>Dados Ordem Bancária</small> </span>
                    </h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

            <div class="modal-body" id="textoModal">
                <div class="box-body">
                    <div class="panel-group" id="accordion">

                    </div>
                </div>
            </div>

            <div class="row w-100">
                <div class="col-md-12 text-center">
                    <strong class="mensagemErro"></strong>
                </div>
            </div>

            <div class="modal-footer">
                <div class="progress hide">
                    <div
                        class="progress-bar"
                        role="progressbar"
                        aria-valuenow="70"
                        aria-valuemin="0"
                        aria-valuemax="100"
                        style="min-width: 2em;">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

    $(document).ready(function () {
        $('#accordion .panel-collapse').on('shown.bs.collapse', function () {
            $(this).siblings('.panel-heading').find('.close i').removeClass('fa-caret-down').addClass('fa-caret-up');
        });

        $('#accordion .panel-collapse').on('hidden.bs.collapse', function () {
            $(this).siblings('.panel-heading').find('.close i').removeClass('fa-caret-up').addClass('fa-caret-down');
        });
    });

    function closeAccordion() {
        $('.panel-collapse.in').collapse('hide');
    }

    function toogleAccordion() {
        $('#accordion').on('shown.bs.collapse', '.panel-collapse', function () {
            $(this).siblings('.panel-heading').find('.close i').removeClass('fa-caret-down').addClass('fa-caret-up');
        });

        $('#accordion').on('hidden.bs.collapse', '.panel-collapse', function () {
            $(this).siblings('.panel-heading').find('.close i').removeClass('fa-caret-up').addClass('fa-caret-down');
        });
    }

    function abrirModalOrdemBancaria(button) {
        var numDocumento = $(button).attr('data-numDocumento'),
            sfpadrao_id = $(button).attr('data-sfpadrao_id');
        modalApropriacaoOrdemBancaria = $('#modal-busca-ordem-bancaria');
        /*abre modal*/
        modalApropriacaoOrdemBancaria.modal('toggle');

        recuperarOrdensBancarias(numDocumento, sfpadrao_id);
    }

    function recuperarOrdensBancarias(numDocumento, sfpadrao_id) {
        var url = "{{ \route('apropriacao.fatura.busca-ordem-bancaria', [ 'numDocumento' => ':numDocumento', 'sfpadrao_id' => ':sfpadrao_id']) }}";
        url = url.replace(':numDocumento', numDocumento);
        url = url.replace(':sfpadrao_id', sfpadrao_id);

        $.ajax({
            url: url,
            data: {
                numDocumento: numDocumento,
            },
            type: 'GET',
            xhr: function () {
                var xhr = $.ajaxSettings.xhr();
                xhr.onprogress = function (e) {
                    if (e.lengthComputable) {
                        var percentComplete = Math.floor((e.loaded / e.total) * 100);
                        $('.progress-bar').css('width', percentComplete + '%').attr('aria-valuenow', percentComplete);
                    }
                };
                return xhr;
            },
            beforeSend: function () {
                $(".progress").removeClass('hide');
                $(".modal-body").hide();
            },
            success: function (response) {

                if(response.error){
                    $('.mensagemErro').text(response.error);
                }else{
                    $('.mensagemErro').text('');
                }

                $('#accordion').html(response.html)
                toogleAccordion()
            },
            error: function (error) {
                new PNotify({
                    title: 'Ordem Bancária',
                    text: 'Não foi possivel recuperar as ordens bancarias.',
                    type: "error"
                });

                toggleProgressBar()
            },
            complete: function () {
                $('.progress-bar').css('width', '100%').attr('aria-valuenow', 100);
                setTimeout(function () {
                    toggleProgressBar()
                }, 500);
            }
        });

        function toggleProgressBar() {
            $(".progress").addClass('hide');
            $(".modal-body").show();
        }
    }

</script>
<style>
    .headerOrdem {
        display: flex;
        width: 100%;
        justify-content: space-between;
    }

    .btns-apropriacao > span {
        font-weight: bold;
    }

    .panel-title .glyphicon {
        margin-right: 5px;
    }

    .panel-heading {
        display: flex;
        align-items: center;
        justify-content: space-between;
    }

    #accordion {
        width: auto;
        max-height: 550px;
        overflow-x: scroll;
    }
</style>
