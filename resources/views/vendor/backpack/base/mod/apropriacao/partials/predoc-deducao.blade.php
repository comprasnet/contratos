<div
    id="acrescimo-deducao-container{{$sfdeducaoCurrent->id}}"
    data-id-deducao="{{$sfdeducaoCurrent->id}}"
    class="m-t-20 m-b-20"
>
    @if(!is_null($sfDeducaoCurrent->idPreDoc))
        <div class="box box-solid box-primary tabela-responsiva">
            <div class="box-header with-border align-table-items-center">
                <h3 class="box-title">Pré-Doc</h3>
                <button
                    type="button"
                    class="btn btn-primary btn-sm pull-right tooltipRemoverPcoItem
                                                                                esconderQuandoApropriacaoEmitidaSiafi"
                    style="margin-right: 5px;"
                    onclick="clickBtnRemoverOuCriarNovoPreDoc(
                {{$sfDeducaoCurrent->id}},
                {{$sfDeducaoCurrent->idPreDoc}}
                );"
                >
                    <i class="fa fa-trash"></i>
                </button>
            </div>
            <div class="espaco-div">
                <div class="row">
                    <div class="col-md-3 label-title paddingTopButton">Dedução:</div>
                    <div class="col-md-3 paddingTopButton">
                        {{$sfDeducaoCurrent->codsit}}
                    </div>
                    <div class="col-md-3 label-title paddingTopButton">Valor:</div>
                    <div class="col-md-3 paddingTopButton">R$ {{$sfDeducaoCurrent->vlr ? number_format($sfDeducaoCurrent->vlr,'2',',','.') : ''}}</div>
                </div>

                <div class="row">
                    <div class="col-md-3 label-title paddingTopButton">Data de Pagamento:</div>
                    <div class="col-md-3 paddingTopButton">
                        {{date_format(date_create($sfDeducaoCurrent->dtpgtoreceb), 'd/m/Y')}}
                    </div>

                    <div class="col-md-3 label-title paddingTopButton">Valor Compensado:</div>
                    <div class="col-md-3 paddingTopButton">
                        0,00
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3 label-title paddingTopButton">Data de Vencimento:</div>
                    <div class="col-md-3 paddingTopButton">{{date_format(date_create($sfDeducaoCurrent->dtvenc), 'd/m/Y')}}</div>
                </div>

                @if($sfDeducaoCurrent->predoctipo === 'predocdarf')
                    <div class="predocdarfcampos{{$sfDeducaoCurrent->id}}">
                        <div class="row">
                            <div class="col-md-3 label-title paddingTopButton">Tipo de DARF:</div>
                            <div class="col-md-3 paddingTopButton">
                                {!! form_row($sfDeducaoForm->{'sfpredoccodtipodarf'.$sfDeducaoCurrent->id}) !!}
                            </div>
                        </div>
                    </div>
                @endif

                <hr>

                <div class="align-table-items-center">
                    <h4 class="box-title">{{ $sfDeducaoCurrent->predoctipo == 'predocdarf' ? 'DARF' : 'DAR' }}</h4>
                </div>

                <hr>

                <div class="row">
                    @if(!empty($sfDeducaoCurrent->txtinscra) && $sfDeducaoCurrent->predoctipo === 'predocdar')
                        <div class="col-md-3 label-title paddingTopButton">Município Favorecido:</div>
                        <div class="col-md-3 paddingTopButton">
                            {{$sfDeducaoCurrent->txtinscra}}
                        </div>

                        <div class="col-md-3 label-title paddingTopButton">Receita:</div>
                        <div class="col-md-3 paddingTopButton">
                            {{$sfDeducaoCurrent->txtinscrb ?? null}}
                        </div>

                    @else
                        <div class="col-md-3 label-title paddingTopButton">Receita:</div>
                        <div class="col-md-3 paddingTopButton">
                            {{$sfDeducaoCurrent->txtinscra ?? null}}
                        </div>
                    @endif

                    <div class="col-md-3 label-title paddingTopButton">Recurso:</div>
                    <div class="col-md-3 paddingTopButton">
                        {!! form_row($sfDeducaoForm->{'sfpredoccodrecurso'.$sfDeducaoCurrent->id}) !!}
                    </div>
                </div>

                <div class="predocdar{{$sfDeducaoCurrent->id}}">
                    <div class="row">
                        <div class="col-md-3 label-title paddingTopButton">Referência:</div>
                        <div class="col-md-2 paddingTopButton">
                            {!! form_row($sfDeducaoForm->{'sfpredocreferencia'.$sfDeducaoCurrent->id}) !!}
                        </div>
                    </div>

                    <hr>
                    <div class="align-table-items-center">
                        <h4 class="box-title">Informações do Recolhimento</h4>
                    </div>
                    <hr>

                    <div class="row">
                        <div class="col-md-3 label-title paddingTopButton">UG Tomadora do Serviço:</div>
                        <div class="col-md-2">
                            {!! form_row($sfDeducaoForm->{'sfpredoccodugtmdrserv'.$sfDeducaoCurrent->id}) !!}
                        </div>

                        <div class="col-md-3 label-title paddingTopButton">Município da NF:</div>
                        <div class="col-md-2 paddingTopButton">
                            {!! form_row($sfDeducaoForm->{'sfpredoccodmuninf'.$sfDeducaoCurrent->id}) !!}
                        </div>

                        @if($sfDeducaoCurrent->codsit && str_starts_with($sfDeducaoCurrent->codsit, 'DDR'))
                            <div class="col-md-2 paddingTopButton">
                                <select id="municipioDeducaoPreDoc{{$sfDeducaoCurrent->id}}" class="municipioSelect2 codigoMunicipio{{$sfDeducaoCurrent->id}}"
                                        onchange="alteraCodigoMunicipioDeducao({{$sfDeducaoCurrent->id}}, this, 'sfpredoccodmuninf')">
                                    <option value=""></option>
                                </select>
                            </div>
                        @else
                            <div class="col-md-2"></div>
                        @endif
                    </div>

                    <div class="row">
                        <div class="col-md-3 label-title paddingTopButton">Número NF/Recibo:</div>
                        <div class="col-md-2">
                            {!! form_row($sfDeducaoForm->{'sfpredocnumnf'.$sfDeducaoCurrent->id}) !!}
                        </div>

                        <div class="col-md-3 label-title paddingTopButton">Data Emissão da NF:</div>
                        <div class="col-md-2 paddingTopButton">
                            {!! form_row($sfDeducaoForm->{'sfpredocdtemisnf'.$sfDeducaoCurrent->id}) !!}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 label-title paddingTopButton">Série da NF:</div>
                        <div class="col-md-2">
                            {!! form_row($sfDeducaoForm->{'sfpredoctxtserienf'.$sfDeducaoCurrent->id}) !!}
                        </div>

                        <div class="col-md-3 label-title paddingTopButton">Subsérie da NF:</div>
                        <div class="col-md-2 paddingTopButton">
                            {!! form_row($sfDeducaoForm->{'sfpredocnumsubserienf'.$sfDeducaoCurrent->id}) !!}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 label-title paddingTopButton">Alíquota da NF:</div>
                        <div class="col-md-2">
                            {!! form_row($sfDeducaoForm->{'sfpredocnumaliqnf'.$sfDeducaoCurrent->id}) !!}
                        </div>

                        <div class="col-md-3 label-title paddingTopButton">Valor da NF:</div>
                        <div class="col-md-2 paddingTopButton">
                            {!! form_row($sfDeducaoForm->{'sfpredocvlrnf'.$sfDeducaoCurrent->id}) !!}
                        </div>
                    </div>
                </div>

                <div class="predocdarfcampos{{$sfDeducaoCurrent->id}}">
                    <div class="row">
                        <div class="col-md-3 label-title paddingTopButton">Período de Apuração:</div>
                        <div class="col-md-3 paddingTopButton">
                            {!! form_row($sfDeducaoForm->{'sfpredocdtprdoapuracao'.$sfDeducaoCurrent->id}) !!}
                        </div>

                        <div class="col-md-3 label-title paddingTopButton">Processo:</div>
                        <div class="col-md-3 paddingTopButton">
                            {!! form_row($sfDeducaoForm->{'sfpredoctxtprocesso'.$sfDeducaoCurrent->id}) !!}
                        </div>
                    </div>

                    <div class="camposSitPreDoc camposDarf{{$sfDeducaoCurrent->id}}" data-id-deducao="{{$sfDeducaoCurrent->id}}">
                        <div class="row">
                            <div class="col-md-3 label-title paddingTopButton camposDarfNumeradoCompostoOuDarf{{$sfDeducaoCurrent->id}}">Referência:</div>
                            <div class="col-md-3 paddingTopButton">
                                {!! form_row($sfDeducaoForm->{'sfpredocnumref'.$sfDeducaoCurrent->id}) !!}
                            </div>

                            <div class="exibeEscondeCamposDarfNumeradoComposto{{$sfDeducaoCurrent->id}}">
                                <div class="col-md-3 label-title paddingTopButton">Receita Bruta Acumulada:</div>
                                <div class="col-md-3 paddingTopButton">
                                    {!! form_row($sfDeducaoForm->{'sfpredocvlrrctabrutaacum'.$sfDeducaoCurrent->id}) !!}
                                </div>
                            </div>
                        </div>

                        <div class="exibeEscondeCamposDarfNumeradoComposto{{$sfDeducaoCurrent->id}}">
                            <div class="row">
                                <div class="col-md-3 label-title paddingTopButton">Percentual:</div>
                                <div class="col-md-3 paddingTopButton">
                                    {!! form_row($sfDeducaoForm->{'sfpredocvlrpercentual'.$sfDeducaoCurrent->id}) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="camposSitPreDoc camposDarfNumerado{{$sfDeducaoCurrent->id}}" data-id-deducao="{{$sfDeducaoCurrent->id}}">
                        <div class="row">
                            <div class="col-md-3 label-title paddingTopButton">Código de Barras:</div>
                            <div class="col-md-3 paddingTopButton">
                                {!! form_row($sfDeducaoForm->{'sfpredocnumcodbarras'.$sfDeducaoCurrent->id}) !!}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div style="display: none">
                        {{ $totalPreDoc = 0 }}
                    </div>

                    <div class="col-md-12">
                        <table class="table table-bordered table-striped">
                            <thead class="align-items-center">
                            <tr>
                                <th class="align-table-items-center">
                                    Seq
                                </th>
                                <th class="align-table-items-center">
                                    Recolhedor
                                </th>
                                <th class="align-table-items-center escondeParaSituacoesDDR{{$sfdeducaoCurrent->id}}">
                                    Base de Cálculo
                                </th>
                                <th class="align-table-items-center">
                                    Valor da Receita
                                </th>
                                <th class="align-table-items-center">
                                    Multa
                                </th>
                                <th class="align-table-items-center">
                                    Juros
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($sfdeducaoCurrent->recolhedores !== 'vazio')
                                @foreach($sfdeducaoCurrent->recolhedores as $current_recolhedor)
                                    <tr>
                                        <td class="align-table-items-center">{{$current_recolhedor->numseqitem}}</td>
                                        <td class="align-table-items-center">{{$current_recolhedor->codrecolhedor}}</td>
                                        <td class="align-table-items-center escondeParaSituacoesDDR{{$sfdeducaoCurrent->id}}">{{$current_recolhedor->vlrbasecalculo ? number_format($current_recolhedor->vlrbasecalculo,2,',','.') : ''}}</td>
                                        <td class="align-table-items-center">{{$current_recolhedor->vlr ? number_format($current_recolhedor->vlr,2,',','.') : ''}}</td>
                                        <td class="align-table-items-center">{{$current_recolhedor->vlrmulta ? number_format($current_recolhedor->vlrmulta,2,',','.') : ''}}</td>
                                        <td class="align-table-items-center">{{$current_recolhedor->vlrjuros ? number_format($current_recolhedor->vlrjuros,2,',','.') : ''}}</td>
                                        <div style="display: none">
                                            {{ $totalPreDoc += $current_recolhedor->vlr + $current_recolhedor->vlrmulta + $current_recolhedor->vlrjuros }}
                                        </div>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="1"></td>
                                <td><strong>Total Pré-Doc:</strong></td>
                                <td><strong>R$ {{number_format($totalPreDoc,2,',','.') ?? 0}}</strong></td>
                            </tr>
                            </tfoot>
                        </table>
                        <hr>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 ">
                        <div class="label-title">Observação</div>
                        {!! form_row($sfDeducaoForm->{'sfpredoctxtobser'.$sfDeducaoCurrent->id}) !!}
                    </div>
                </div>

                {!! form_row($sfDeducaoForm->{'sfpredoctipo'.$sfDeducaoCurrent->id}) !!}
            </div>
        </div>
    @else
        <div class="box box-solid box-primary tabela-responsiva ">
            <div class="box-header with-border align-table-items-center">
                <h3 class="box-title">Pré-Doc</h3>
            </div>
            <div class="row">
                <button
                    type="button"
                    class="btn btn-primary btn-sm tooltipAdicionarPredoc
                                                                                esconderQuandoApropriacaoEmitidaSiafi"
                    style="margin-top: 10px;margin-bottom: 10px;"
                    onclick="clickBtnRemoverOuCriarNovoPreDoc(
                {{$sfDeducaoCurrent->id}},
                null
                );"
                >
                    <i class="fa fa-plus"></i>
                </button>
            </div>
        </div>
    @endif
</div>
