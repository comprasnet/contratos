<div
    id="recolhedores-deducao-container{{$sfdeducaoCurrent->id}}"
    {{--    data-count-parcelas="{{count($sfpco_current->cronBaixaPatrimonial[0]->parcela()->get())}}"--}}
    data-id-deducao="{{$sfdeducaoCurrent->id}}"
    class="m-t-20 m-b-20"
>
    @if($sfdeducaoCurrent->recolhedores === 'vazio')
        <div class="box box-solid box-primary">
            <div class="box-header with-border align-table-items-center">
                <h3 class="box-title">Lista dos Recolhedores</h3>
            </div>
            <div class="box-body text-center">
                <div class="alert alert-warning" role="alert">
                    Não foram encontrados recolhedores
                </div>
            </div>

            <div id="btn-add-novo-item-recolhimento{{$sfdeducaoCurrent->id}}" style="text-align: center">
                <a
                    href="javascript:void(0)"
                    id="novo-recolhedor{{$sfdeducaoCurrent->id}}"
                    class="tooltip-btn-add-novo-recolhedor esconderQuandoApropriacaoEmitidaSiafi"
                    onclick="clickBtnRemoverOuCriarNovoRecolhimento('{!! $sfdeducaoCurrent->id !!}');"
                >
                    <i class="fa fa-plus" style="color: #3c8dbc"></i>
                </a>
            </div>

        </div>

    @else
        <div class="box box-solid box-primary tabela-responsiva">
            <div class="box-header with-border align-table-items-center">
                <h3 class="box-title">Lista dos Recolhedores</h3>
            </div>

            <table class="table table-bordered table-hover table-condensed">
                <thead class="align-items-center">
                <tr>
                    <th class="align-table-items-center">
                        Seq
                    </th>
                    <th class="align-table-items-center">
                        Recolhedor
                    </th>
                    <th class="align-table-items-center escondeParaSituacoesDDR{{$sfdeducaoCurrent->id}}">
                        Base de Cálculo
                    </th>
                    <th class="align-table-items-center esconderQuandoApropriacaoEmitidaSiafi escondeParaSituacoesDDR{{$sfdeducaoCurrent->id}}">
                        %
                    </th>
                    <th class="align-table-items-center">
                        Valor da Receita
                    </th>
                    <th class="align-table-items-center">
                        Multa
                    </th>
                    <th class="align-table-items-center">
                        Juros
                    </th>
                    <th class="align-table-items-center">
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($sfdeducaoCurrent->recolhedores as $current_recolhedor)
                    <tr
                        class="count-recolhedor"
                        data-count-recolhedor="{{$loop->count}}"
                        data-recolhedor-numseqitem="{{$current_recolhedor->numseqitem}}"
                        id="item-recolhimento-deducao-{{$current_recolhedor->id}}"
                    >
                        <td class="align-table-items-center">{!! form_row($sfDeducaoForm->{'numseqitem' . $sfdeducaoCurrent->id.$current_recolhedor->id  }) !!}</td>
                        <td class="align-table-items-center">{!! form_row($sfDeducaoForm->{'recolhedor' . $sfdeducaoCurrent->id.$current_recolhedor->id  }) !!}</td>
                        <td class="align-table-items-center escondeParaSituacoesDDR{{$sfdeducaoCurrent->id}}">{!! form_row($sfDeducaoForm->{'vlrbasecalculo' . $sfdeducaoCurrent->id.$current_recolhedor->id  }) !!}</td>
                        <td class="align-table-items-center esconderQuandoApropriacaoEmitidaSiafi escondeParaSituacoesDDR{{$sfdeducaoCurrent->id}}">
                            <input class="form-control" type="text"
                                   onkeyup="atualizarMaskMoney(this);
                                   atualizaValorPrincipal(this,{{$sfdeducaoCurrent->id.$current_recolhedor->id}});
                                   atualizaTotaisRodape({{$sfdeducaoCurrent->id}});"

                                   onblur="atualizarMaskMoney(this);
                                   atualizaValorPrincipal(this,{{$sfdeducaoCurrent->id.$current_recolhedor->id}});
                                   atualizaTotaisRodape({{$sfdeducaoCurrent->id}});"
                                   placeholder="%">
                        </td>
                        <td class="align-table-items-center">{!! form_row($sfDeducaoForm->{'vlrPrincipal' . $sfdeducaoCurrent->id.$current_recolhedor->id  }) !!}</td>
                        <td class="align-table-items-center">{!! form_row($sfDeducaoForm->{'vlrmulta' . $sfdeducaoCurrent->id.$current_recolhedor->id  }) !!}</td>
                        <td class="align-table-items-center">{!! form_row($sfDeducaoForm->{'vlrjuros' . $sfdeducaoCurrent->id.$current_recolhedor->id  }) !!}</td>
                        <td class="align-table-items-center">
                            <button
                                type="button"
                                class="btn btn-primary btn-sm pull-right tooltipRemoverDeducaoRecolhimento
                                                                                esconderQuandoApropriacaoEmitidaSiafi"
                                style="margin-right: 5px;"
                                onclick="clickBtnRemoverOuCriarNovoRecolhimento(
                                                    {{ $sfdeducaoCurrent->id }},
                                                {{ $current_recolhedor->id }}
                                                );"
                            >
                                <i class="fa fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <td class="escondeParaSituacoesDDR{{$sfdeducaoCurrent->id}}">Valor a Informar:</td>
                    <td id="valor-total-informar-{{$sfdeducaoCurrent->id}}" class="escondeParaSituacoesDDR{{$sfdeducaoCurrent->id}}"></td>
                    <td></td>
                    <td class="align-table-items-center">Valores Totais:</td>
                    <td id="total-vlrPrincipal-{{$sfdeducaoCurrent->id}}"></td>
                    <td id="total-vlrMulta-{{$sfdeducaoCurrent->id}}"></td>
                    <td id="total-vlrJuros-{{$sfdeducaoCurrent->id}}"></td>
                </tr>
                </tfoot>
            </table>
            <hr>
            <div id="btn-add-novo-item-recolhimento{{$sfdeducaoCurrent->id}}" style="text-align: center">
                <a
                    href="javascript:void(0)"
                    id="novo-recolhedor{{$sfdeducaoCurrent->id}}"
                    class="tooltip-btn-add-novo-recolhedor esconderQuandoApropriacaoEmitidaSiafi"
                    onclick="clickBtnRemoverOuCriarNovoRecolhimento('{!! $sfdeducaoCurrent->id !!}');"
                >
                    <i class="fa fa-plus" style="color: #3c8dbc"></i>
                </a>
            </div>

            <td class="align-table-items-center">{!! form_row($sfDeducaoForm->{'sfitemrecolhedor' . $sfdeducaoCurrent->id.$current_recolhedor->id  }) !!}</td>
        </div>
    @endif


</div>

@push('after_scripts')
    <script>
        $(document).ready(function () {
            atualizaTotaisRodape({{ $sfdeducaoCurrent->id }});
        });

        function atualizaValorPrincipal(element, id_recolhedor) {

            var porcentagem = parseFloat(element.value.replace(/\./g, '').replace(',', '.'))

            if (!isNaN(porcentagem)) {

                var valorBaseCalculo = $('#vlrbasecalculo' + id_recolhedor).val().replace(/\./g, '').replace(',', '.');
                var valorCalculado = valorBaseCalculo * (porcentagem / 100);

                // Formata o valor com duas casas decimais e separadores de milhar
                $('#vlrPrincipal' + id_recolhedor).val(valorCalculado.toLocaleString('pt-BR', {minimumFractionDigits: 2, maximumFractionDigits: 2}));
            }
        }

        function atualizaTotaisRodape(idDeducao) {

            if (situacaoSiafi === 'APR') {
                return
            }

            function calculaTotal(campo) {
                var total = campo.map(function () {
                    return parseFloat($(this).val().replace(/\./g, '').replace(',', '.')) || 0
                }).get()
                    .reduce(function (acc, value) {
                        return acc + value
                    }, 0)
                return total.toLocaleString('pt-BR', {style: 'currency', currency: 'BRL'});
            }

            var totalVlrPrincipal = calculaTotal($(`[id^="vlrPrincipal${idDeducao}"]`))
            var totalVlrMulta = calculaTotal($(`[id^="vlrmulta${idDeducao}"]`))
            var totalVlrJuros = calculaTotal($(`[id^="vlrjuros${idDeducao}"]`))

            $('#total-vlrPrincipal-' + idDeducao).text(totalVlrPrincipal)
            $('#total-vlrMulta-' + idDeducao).text(totalVlrMulta)
            $('#total-vlrJuros-' + idDeducao).text(totalVlrJuros)

            var valorTotal = (
                parseFloat(totalVlrPrincipal.replace(/\./g, '').replace(',', '.').replace('R$', '')) +
                parseFloat(totalVlrMulta.replace(/\./g, '').replace(',', '.').replace('R$', '')) +
                parseFloat(totalVlrJuros.replace(/\./g, '').replace(',', '.').replace('R$', ''))
            )

            $('#valor-total-informar-' + idDeducao).text(valorTotal.toLocaleString('pt-BR', {style: 'currency', currency: 'BRL'}))
            //Adiciona o valor total para a deducao item
            $('#sfdeducaovlr' + idDeducao).val(valorTotal.toLocaleString('pt-BR', { minimumFractionDigits: 2, maximumFractionDigits: 2 }))
        }

    </script>
@endpush
