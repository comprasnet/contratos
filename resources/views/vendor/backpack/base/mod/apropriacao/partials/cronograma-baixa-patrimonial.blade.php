<div
    id="cronograma-baixa-patrimonial-container{{$sfpco_current->id}}"
    data-count-parcelas="{{count($sfpco_current->cronBaixaPatrimonial[0]->parcela()->get())}}"
    data-id-pco="{{$sfpco_current->id}}"
    class="m-t-20 m-b-20 cronograma-baixa-patrimonial-container"
>
    <div class="row">
        <div class="col-md-6">
            <h3 class="box-title m-b-20">Cronograma de Baixa Patrimonial da Despesa Antecipada</h3>
        </div>
        <div class="col-md-3">
            <div class="pull-right">
                <div class="input-group margin-top-20 margin-bottom-10">
                    <div
                        class="input-group-addon label-title"
                        style="width: 50%;
                        text-align: left">Total a Informar:
                    </div>
                    <input type="text"
                           class="form-control input-sm"
                           id="valorTotalParcelaAInformar{{$sfpco_current->id}}"
                           readonly
                           value="{{number_format(0, 2, ',', '.')}}"
                    >
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="pull-right">
                <div class="input-group margin-top-20 margin-bottom-10">
                    <div
                        class="input-group-addon label-title"
                        style="width: 50%;
                        text-align: left">Total Informado:
                    </div>
                    <input type="text"
                           class="form-control input-sm"
                           id="valorTotalParcelaInformado{{$sfpco_current->id}}"
                           readonly
                           value="{{number_format(0, 2, ',', '.')}}"
                    >
                </div>
            </div>
        </div>
    </div>

    @foreach($sfpco_current->cronBaixaPatrimonial[0]->parcela()->get()->sortBy('numparcela') as $parcela)
        <div style="display: flex; flex-direction: row; justify-content: space-between">
            <div style="flex-grow: 1">
                @if($sfpcoForm->{'sfparcelanumparcela'.$sfpco_current->id.$parcela->id})
                    {!! form_row($sfpcoForm->{'sfparcelanumparcela'.$sfpco_current->id.$parcela->id}) !!}
                    {!! form_row($sfpcoForm->{'sfparcela'.$parcela->id.$sfpco_current->id}) !!}
                @endif
            </div>
            <div style="flex-grow: 1; padding: 0 10px">
                @if($sfpcoForm->{'sfparceladtprevista'.$sfpco_current->id.$parcela->id})
                    {!! form_row($sfpcoForm->{'sfparceladtprevista'.$sfpco_current->id.$parcela->id}) !!}
                @endif
            </div>
            <div style="flex-grow: 1;padding-right: 10px">
                @if($sfpcoForm->{'sfparcelavlr'.$sfpco_current->id.$parcela->id})
                    {!! form_row($sfpcoForm->{'sfparcelavlr'.$sfpco_current->id.$parcela->id}) !!}
                @endif
            </div>
            <div style="align-self: center; margin-top: 11px;">
                <button type="button"
                        class="btn btn-danger btn-sm pull-right esconderQuandoApropriacaoEmitidaSiafi"
                        style="margin-right: 5px;"
                        onclick="clickBtnRemoverOuCriarNovaParcela('{!! $sfpco_current->id !!}', '{!! $parcela->id !!}');"
                >
                    <i class="fa fa-trash"></i>
                </button>
            </div>
        </div>
    @endforeach

    <div id="btn-add-novo-cron-baixa-patrimonial{{$sfpco_current->id}}" style="text-align: center">
        <a
            href="javascript:void(0)"
            id="nova-parcela{{$sfpco_current->id}}"
            class="tooltip-btn-add-novo-pco-item esconderQuandoApropriacaoEmitidaSiafi"
            onclick="clickBtnRemoverOuCriarNovaParcela('{!! $sfpco_current->id !!}');"
        >
            <i class="fa fa-plus" style="color: #3c8dbc"></i>
        </a>
    </div>

</div>
