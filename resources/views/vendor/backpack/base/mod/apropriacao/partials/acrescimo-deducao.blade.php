<div
    id="acrescimo-deducao-container{{$sfdeducaoCurrent->id}}"
    data-id-deducao="{{$sfdeducaoCurrent->id}}"
    class="m-t-20 m-b-20"
    style="display: none;"
>
    @if($sfdeducaoCurrent->acrescimos === 'vazio')
        <div class="box box-solid box-primary">
            <div class="box-header with-border align-table-items-center">
                <h3 class="box-title">Acréscimos</h3>
            </div>

            <div id="btn-add-novo-item-acrescimo{{$sfdeducaoCurrent->id}}" style="text-align: center">
                <a
                    href="javascript:void(0)"
                    id="novo-acrescimo{{$sfdeducaoCurrent->id}}"
                    class="tooltip-btn-add-novo-acrescimo esconderQuandoApropriacaoEmitidaSiafi"
                    onclick="clickBtnRemoverOuCriarNovoAcrescimo('{!! $sfdeducaoCurrent->id !!}');"
                >
                    <i class="fa fa-plus" style="color: #3c8dbc"></i>
                </a>
            </div>

        </div>

    @else
        <div class="box box-solid box-primary tabela-responsiva">
            <div class="box-header with-border align-table-items-center">
                <h3 class="box-title">Acréscimos</h3>
            </div>

            <table class="table table-bordered table-hover table-condensed tableAcrescimo{{$sfdeducaoCurrent->id}}">
                <thead class="align-items-center">
                <tr>
                    <th class="align-table-items-center">
                        Tipo
                    </th>
                    <th class="align-table-items-center">
                        N° Empenho
                    </th>
                    <th class="align-table-items-center">
                        Subelemento
                    </th>
                    <th class="align-table-items-center variavelAcrescimoTable variavelAcrescimoTable{{$sfdeducaoCurrent->id}}" data-id-deducao="{{$sfdeducaoCurrent->id}}"></th>
                    <th class="align-table-items-center variavelAcrescimoTable variavelAcrescimoTable{{$sfdeducaoCurrent->id}}" data-id-deducao="{{$sfdeducaoCurrent->id}}"></th>
                    <th class="align-table-items-center variavelAcrescimoTable variavelAcrescimoTable{{$sfdeducaoCurrent->id}}" data-id-deducao="{{$sfdeducaoCurrent->id}}"></th>
                    </th>
                    <th class="align-table-items-center">
                        Valor
                    </th>
                    <th class="align-table-items-center">
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($sfdeducaoCurrent->acrescimos as $current_acrescimo)
                    <tr
                        class="count-acrescimo"
                        data-count-acrescimo="{{$loop->count}}"
                        data-recolhedor-numseqitem="{{$current_acrescimo->numseqitem}}"
                        id="item-acrescimo-deducao-{{$current_acrescimo->id}}"
                    >
                        <td class="align-table-items-center">{!! form_row($sfDeducaoForm->{'sfacrescimotpacrescimo' . $sfdeducaoCurrent->id.$current_acrescimo->id  }) !!}</td>
                        <td class="align-table-items-center">{!! form_row($sfDeducaoForm->{'sfacrescimonumemp' . $sfdeducaoCurrent->id.$current_acrescimo->id  }) !!}</td>
                        <td class="align-table-items-center">{!! form_row($sfDeducaoForm->{'sfacrescimocodsubitemempe' . $sfdeducaoCurrent->id.$current_acrescimo->id  }) !!}</td>
                        @php
                            $campos = [
                                'numclassa','txtinscra', 'txtinscrb'
                            ];
                        @endphp

                        @php $colCount = 0; $rowCount = 0; @endphp

                        @foreach($campos as $campo)

                            @if($sfDeducaoForm->{$campo.$sfdeducaoCurrent->id.$current_acrescimo->id})
                                <td class="align-table-items-center">
                                    {!! form_row($sfDeducaoForm->{$campo.$sfdeducaoCurrent->id.$current_acrescimo->id}) !!}
                                </td>
                                @php
                                    $colCount += 6;
                                    if ($colCount >= 12) {
                                        $colCount = 0;
                                        $rowCount++;
                                    }
                                @endphp
                            @endif
                        @endforeach

                        <td class="align-table-items-center">{!! form_row($sfDeducaoForm->{'sfacrescimovlr' . $sfdeducaoCurrent->id.$current_acrescimo->id  }) !!}</td>


                        <td class="align-table-items-center">
                            <button
                                type="button"
                                class="btn btn-primary btn-sm pull-right tooltipRemoverDeducaoRecolhimento
                                                                                esconderQuandoApropriacaoEmitidaSiafi"
                                style="margin-right: 5px;"
                                onclick="clickBtnRemoverOuCriarNovoAcrescimo(
                                                    {{ $sfdeducaoCurrent->id }},
                                                {{ $current_acrescimo->id }}
                                                );"
                            >
                                <i class="fa fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <hr>
            <div id="btn-add-novo-item-acrescimo{{$sfdeducaoCurrent->id}}" style="text-align: center">
                <a
                    href="javascript:void(0)"
                    id="novo-acrescimo{{$sfdeducaoCurrent->id}}"
                    class="tooltip-btn-add-novo-acrescimo esconderQuandoApropriacaoEmitidaSiafi"
                    onclick="clickBtnRemoverOuCriarNovoAcrescimo('{!! $sfdeducaoCurrent->id !!}');"
                >
                    <i class="fa fa-plus" style="color: #3c8dbc"></i>
                </a>
            </div>

            <td class="align-table-items-center">{!! form_row($sfDeducaoForm->{'sfitemacrescimo' . $sfdeducaoCurrent->id.$current_acrescimo->id  }) !!}</td>
        </div>
    @endif
</div>
