@if(
    isset($sfpcoitem_current->faturaEmpenho) &&
    $sfpcoitem_current->faturaEmpenho instanceof Illuminate\Database\Eloquent\Collection &&
    $sfpcoitem_current->faturaEmpenho->isNotEmpty()
    )
    @foreach($sfpcoitem_current->faturaEmpenho as $key => $faturaEmpenho)
        <div class="input-group" style="margin-bottom: 5px; width: 100%">
            <span class="input-group-addon checkbox checkbox-inst-cobranca{{$sfpcoitem_current->id}}" style="padding: 4px 12px; text-align: left;">
                <div type="checkbox"
                     style="margin-bottom: 0;margin-top: 0"
                >
                    <label style="min-height: 0">
                        <input
                            type="checkbox"
                            name="checkboxFaturaEmpenho{{$sfpcoitem_current->id}}[]"
                            data-valorref-id="valorref{{$sfpcoitem_current->id}}_{{$key}}"
                            checked
                        >
                        {{$faturaEmpenho->contratoFaturas->numero}} -
                        {{number_format($faturaEmpenho->contratoFaturas->valorfaturado, 2, ',', '.')}}
                    </label>
                </div>
           </span>
            <input
                type="text"
                class="form-control input-sm valorref{{$sfpcoitem_current->id}}"
                onkeyup="atualizarMaskMoney(this);atualizarValorCampoVlrSfpcoitem({{$sfpcoitem_current->id}}, {{$sfpcoitem_current->pco->id}});"
                style="height: 25px"
                value="{{$faturaEmpenho->valorref ? number_format($faturaEmpenho->valorref, 2, ',', '.') : ''}}"
                name="valorref{{$sfpcoitem_current->id}}[]"
                id="valorref{{$sfpcoitem_current->id}}_{{$key}}"
            >
            <input type="hidden" name="idsinstcobranca{{$sfpcoitem_current->id}}[]" value="{{$faturaEmpenho->contratofatura_id}}">
            <input type="hidden" name="idsempenho{{$sfpcoitem_current->id}}[]" value="{{$faturaEmpenho->empenho_id}}">
        </div>
    @endforeach
@endif

<div class="input-group">
    <div class="input-group-btn">
        <button
            type="button"
            class="btn btn-default dropdown-toggle btn-novo-inst-cobranca btn-novo-inst-cobranca{{$sfpcoitem_current->id}}"
            id="btn-novo-inst-cobranca{{$sfpcoitem_current->id}}"
            style="border-right-style: none;height: 25px; padding: 0 12px;border-radius: 0;"
            data-toggle="dropdown"
        >
            <input
                type="checkbox"
                name="checkboxFaturaEmpenho{{$sfpcoitem_current->id}}[]"
                checked
                disabled
            >
            <span
                class="selected-option{{$sfpcoitem_current->id}} icon-plus"
                style="padding-left: 4px;text-align: left; vertical-align: text-bottom; font-weight: 400;"
            >
                <i class="fa fa-plus" style="color: #3c8dbc;"></i>
            </span>

        </button>
        <ul class="dropdown-menu">
            <li><a
                    href="javascript:void(0)"
                    data-value=""
                    data-sfpcoitemid="{{$sfpcoitem_current->id}}"
                ><i class="fa fa-plus" style="color: #3c8dbc;">

                    </i>
                </a>
            </li>

            @if(
                isset($sfpcoitem_current->apropriacaoFaturas) &&
                $sfpcoitem_current->apropriacaoFaturas instanceof Illuminate\Database\Eloquent\Collection &&
                $sfpcoitem_current->apropriacaoFaturas->isNotEmpty()
                )
                @foreach($sfpcoitem_current->apropriacaoFaturas as $key => $apropriacaoFaturas)
                    <li>
                        <a
                            href="javascript:void(0)"
                            data-value="{{$apropriacaoFaturas->fatura->id}}"
                            data-sfpcoitemid="{{$sfpcoitem_current->id}}"
                        >
                            {{$apropriacaoFaturas->fatura->numero}} -
                            {{number_format($apropriacaoFaturas->fatura->valorfaturado, 2, ',', '.')}}
                        </a>
                    </li>
                @endforeach
            @endif
        </ul>
    </div>
    <input
        type="text"
        class="form-control"
        name="valorref{{$sfpcoitem_current->id}}[]"
        style="height: 25px"
        placeholder="Digite o valor da nova nota aqui"
        onkeyup="atualizarMaskMoney(this);"
    >
    <input type="hidden" name="idsinstcobranca{{$sfpcoitem_current->id}}[]" id="idsinstcobranca{{$sfpcoitem_current->id}}">
</div>

@push('after_scripts')
    <style>
        .icon-plus{
            display: inline-block;
            text-align: center;
            width: 100%;
        }
    </style>

    <script>
        // Função para lidar com a mudança no estado do checkbox
        function handleCheckboxChange(event) {
            const checkbox = event.target;
            const valorRefInputId = checkbox.dataset.valorrefId;
            const valorRefInput = document.getElementById(valorRefInputId);

            if (valorRefInputId) {
                if (checkbox.checked) {
                    // Restaura o valor original armazenado em um data attribute
                    valorRefInput.value = valorRefInput.dataset.originalValue;
                } else {
                    // Define o valor como vazio quando desmarcado
                    valorRefInput.value = '';
                }
            }
        }

        // Função para inicializar os valores originais dos campos de valor
        function initializeValueRefs() {
            document.querySelectorAll('input[name^="valorref"]').forEach(input => {
                input.dataset.originalValue = input.value;
            });
        }

        // Inicializa os valores originais ao carregar a página
        document.addEventListener('DOMContentLoaded', initializeValueRefs);

        // Adiciona o evento de mudança a todos os checkboxes
        document.querySelectorAll('input[type="checkbox"]').forEach(checkbox => {
            checkbox.addEventListener('change', handleCheckboxChange);
        });
    </script>

@endpush
