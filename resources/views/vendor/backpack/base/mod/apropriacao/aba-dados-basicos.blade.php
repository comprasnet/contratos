{!! form_start($sfDadosBasicosForm) !!}
<!--PAINEL COM INFORMACAO DE DADOS BASICOS -->
<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-3 label-title paddingTopButton">Código da UG pagadora:</div>
            <div class="col-md-3 paddingTopButton">
                {!! form_row($sfDadosBasicosForm->codugpgto) !!}
                <div id="mensagem-erro" class="invalid-feedback" style="display: none;color: red">
                    A unidade não é descentralizada desse contrato.
                </div>
            </div>
            <div class="col-md-3 label-title paddingTopButton">Nome da UG pagadora:</div>
            <div class="col-md-3 paddingTopButton nomeugpgto">{{ $sfDadosBasicos->nomeugpgto }}</div>
        </div>

        <div class="row">
            <div class="col-md-3 label-title paddingTopButton">Data de Emissão Contábil:</div>
            <div class="col-md-3 paddingTopButton">{!! form_row($sfDadosBasicosForm->dtemis) !!}</div>
            <div class="col-md-3 label-title paddingTopButton">Data de Vencimento:</div>
            <div class="col-md-3 paddingTopButton">{!! form_row($sfDadosBasicosForm->dtvenc) !!}</div>
        </div>

        <div class="row">
            <div class="col-md-3 label-title paddingTopButton">Taxa de Câmbio:</div>
            <div class="col-md-3 paddingTopButton">{!! form_row($sfDadosBasicosForm->vlrtaxacambio) !!}</div>
            <div class="col-md-3 label-title paddingTopButton">Processo:</div>
            <div class="col-md-3 paddingTopButton">{!! form_row($sfDadosBasicosForm->txtprocesso) !!}</div>
        </div>
        <div class="row">
            <div class="col-md-3 label-title paddingTopButton">Ateste:</div>
            <div class="col-md-3 paddingTopButton">{!! form_row($sfDadosBasicosForm->dtateste) !!}</div>
            <div class="col-md-3 label-title paddingTopButton">Valor do Documento:</div>
            <div class="col-md-3 paddingTopButton">{{ number_format($sfDadosBasicos->vlr, 2, ',', '.') }}</div>
        </div>

        <div class="row">
            <div class="col-md-3 label-title paddingTopButton">Código do Credor:</div>
            <div class="col-md-3 paddingTopButton">{!! form_row($sfDadosBasicosForm->codcredordevedor) !!}</div>
            <div class="col-md-3 label-title paddingTopButton">Nome do Credor:</div>
            <div class="col-md-3 paddingTopButton" id="nome-fornecedor-credor">{{ $sfDadosBasicos->nomeFornecedor }}</div>
        </div>
        <div class="row">
            <input
                type="hidden"
                name="fornecedor_sem_mascara"
                id="fornecedor_sem_mascara"
                value="{{ $sfDadosBasicos->codcredordevedorsemmascara }}"
            >
        </div>
    </div>
</div>

<!--GRID DE DADOS DE DOCUMENTOS DE ORIGEM-->

<div class="box box-solid box-primary">
    <div class="box-header with-border align-table-items-center">
        <h3 class="box-title">Dados de Documentos de Origem</h3>
    </div>
    <div class="box-body">
        <table class="table table-bordered table-hover table-condensed">
            <thead class="align-items-center">
            <tr>
                <th class="align-table-items-center">
                    Emitente
                </th>
                <th class="align-table-items-center">
                    Data de Emissão
                </th>
                <th class="align-table-items-center">
                    Número Doc.Origem
                </th>
                <th class="align-table-items-center">
                    Valor
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach ($sfDadosBasicos->docOrigem as $itemDocOrigem)
                <tr>
                    <td class="align-table-items-center">
                        {{  $itemDocOrigem->codidentemit }}
                    </td>
                    <td class="align-table-items-center">
                        {{ date_format(date_create($itemDocOrigem->dtemis), 'd/m/Y') }}
                    </td>
                    <td class="align-table-items-center">
                        {{ $itemDocOrigem->numdocorigem }}
                    </td>
                    <td class="align-table-items-center">
                        {{ number_format($itemDocOrigem->vlr, 2, ',', '.')}}
                    </td>
                </tr>
            @endforeach
            <tr>
                <td/>
                <td/>
                <td class="label-title align-table-items-center">TOTAL</td>
                <td class="align-table-items-center">{{ number_format($sfDadosBasicos->docOrigem->sum('vlr'), 2, ',', '.')}}</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<!--CAMPO OBSERVACAO E INF ADICIONAL-->
<fieldset class="form-group">
    {!! form_row($sfDadosBasicosForm->txtobser) !!}
</fieldset>
<fieldset class="form-group">
    {!! form_row($sfDadosBasicosForm->txtinfoadic) !!}
</fieldset>
<div class="align-table-items-center">
    {!! form_row($sfDadosBasicosForm->btnSubmitFormSfDadosBasicos)  !!}
</div>

{!! form_end($sfDadosBasicosForm) !!}
