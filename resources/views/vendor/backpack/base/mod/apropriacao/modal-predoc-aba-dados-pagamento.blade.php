<div class="modal fade" tabindex="-1" role="dialog"
     id="modal-predoc-aba-dados-pagamento{!! $dadosAntecipaGov->id !!}">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" id="modal-content">
            <div class="modal-header text-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;
                    </span>
                </button>
                <div>
                    <h4 class="modal-title"><i class="fa  fa-credit-card"></i> Dados Domicílio Bancário</h4>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-offset-1 col-md-3">
                        <label>Conta bancária:</label><span> {!! $dadosAntecipaGov->domicilioBancario->conta !!}</span>
                    </div>
                    <div class="col-md-offset-1 col-md-3">
                        <label>Nº Agência:</label><span> {!! $dadosAntecipaGov->domicilioBancario->agencia !!}</span>
                    </div>
                    <div class="col-md-offset-1 col-md-3">
                        <label>Nº Banco:</label><span> {!! $dadosAntecipaGov->domicilioBancario->banco !!}</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-offset-1 col-md-3">
                        <label>Operação:</label><span> {!! $dadosAntecipaGov->status_operacao !!}</span>
                    </div>
                    <div class="col-md-offset-1 col-md-3">
                        <label>Nº Operação:</label><span> {!! $dadosAntecipaGov->num_operacao !!}</span>
                    </div>
                    <div class="col-md-offset-1 col-md-3">
                        <label>Nº Cotação:</label><span> {!! $dadosAntecipaGov->num_cotacao !!}</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-offset-1 col-md-3">
                        <label>Data da Ação:</label><span> {!! $dadosAntecipaGov->data_acao !!}</span>
                    </div>
                    <div class="col-md-offset-1 col-md-3">
                        <label>Valor da Operação:</label><span> {!! $dadosAntecipaGov->valor_operacao !!}</span>
                    </div>
                    <div class="col-md-offset-1 col-md-3">
                        <label>Valor da Parcela:</label><span> {!! $dadosAntecipaGov->valor_parcela !!}</span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="botao-cancelar"><span
                            class="fa fa-ban"></span>
                    &nbsp;Fechar
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
