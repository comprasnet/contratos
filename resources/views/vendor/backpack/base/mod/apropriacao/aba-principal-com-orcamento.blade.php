<!--PAINEL COM INFORMACAO DE SITUAÇÕES PRINCIPAL COM ORÇAMENTO -->
<div class="row">
    <div class="col-md-6 h3">Situações Principal com Orçamento</div>
    <div class="col-md-offset-2 col-md-4">
        <div class="pull-right">
            <div class="input-group margin-top-20 margin-bottom-10">
                <div class="input-group-addon label-title" style="width: 50%; text-align: left">Total da aba:</div>
                <input type="text" class="form-control" id="valorTotalItem" readonly
                       value="{{number_format($valorTotalAbaPco, 2, ',', '.')}}">
            </div>
        </div>
    </div>
</div>

{!! form_start($sfpcoForm)  !!}
<div>
    <!-- Nav tabs -->
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs" role="tablist">
            @foreach($sfPco as $sfpco_current)
                <li
                    role="presentation"
                    class="
                    @if ($idPcoActive === null && $loop->first)active @endif
                    @if ($idPcoActive === $sfpco_current->id)active @endif pointer-hand
                    idSfPco{{$sfpco_current->id}}
                        "
                >
                    <a
                        data-target="#{!! $sfpco_current->codsit !!}_{!! $sfpco_current->id !!}"
                        aria-controls="{!! $sfpco_current->codsit !!}_{!! $sfpco_current->id !!}"
                        role="tab"
                        data-toggle="tab"
                        class="aba-sfpco count-situacao"
                        data-count-situacao="{{$loop->count}}"
                        data-numseqitem="{{$sfpco_current->numseqitem}}"
                        data-codsit="{{$sfpco_current->codsit}}"
                        id="aba-sfpco{{$sfpco_current->id}}"
                        onclick="abaAtivaPco();"
                    >
                        {{ $sfpco_current->codsit ?? ' - ' }}

                    </a>
                </li>
            @endforeach
            <li>
                <a
                    href="javascript:void(0)"
                    id="nova-aba-situacao"
                    class="esconderQuandoApropriacaoEmitidaSiafi"
                    onclick="clickBtnNovaAbaSituacao(null, false);"
                >
                    <i class="fa fa-plus" style="color: #3c8dbc"></i>
                </a>
            </li>
            <li class="dropdown">
                <a
                    class="dropdown-toggle esconderQuandoApropriacaoEmitidaSiafi"
                    data-toggle="dropdown"
                    href="#"
                    aria-expanded="false"
                >
                    <i class="fa fa-trash" style="color: red"></i> <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    @foreach($sfPco as $sfpco_current)
                        <li
                            class="pointer-hand"
                        >
                            <a
                                href="javascript:void(0)"
                                id="nova-aba-situacao"
                                class="esconderQuandoApropriacaoEmitidaSiafi"
                                onclick="clickBtnNovaAbaSituacao({{ $sfpco_current->id }}, true);"
                            >
                                {{ $sfpco_current->codsit ?? ' - ' }}

                            </a>
                        </li>
                    @endforeach
                </ul>
            </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            @foreach($sfPco as $sfpco_current)
                <div
                    role="tabpanel"
                    class="tab-pane fade
                        @if ($idPcoActive === null && $loop->first)active in @endif
                        @if ($idPcoActive === $sfpco_current->id)active in @endif
                        "
                    id={!! $sfpco_current->codsit !!}_{!! $sfpco_current->id !!}
                >
                    <div class="row m-b-20">
                        <div class="col-md-offset-8 col-md-4 p-l-20 p-r-5">
                            <div class="pull-right">
                                <div class="input-group">
                                    <div class="input-group-addon label-title" style="width: 50%; text-align: left">Total da situação:</div>
                                    <input
                                        type="text"
                                        class="form-control"
                                        id="valorTotalItem{{ $sfpco_current->id }}"
                                        readonly
                                        value="{{number_format($sfpco_current->sfpcoitem->sum('vlr'), 2, ',', '.')}}">
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-3 label-title paddingTopButton">Situação:</div>
                        <div class="col-md-3 paddingTopButton">
                            {!! form_row($sfpcoForm->{'codsit'.$sfpco_current->id}) !!}
                        </div>
                        <div class="col-md-3 label-title paddingTopButton">Nome da Situação:</div>
                        <div class="col-md-3 paddingTopButton">{{ $sfpco_current->nomeSituacaoPco ?? ' - ' }}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 label-title paddingTopButton">Unidade Emitente do Empenho:</div>
                        <div class="col-md-3 paddingTopButton codugempePco">{!! form_row($sfpcoForm->{'codugempe'.$sfpco_current->id}) !!}</div>
                        <div class="col-md-3 label-title paddingTopButton">Nome da UG do Empenho:</div>
                        <div class="col-md-3 paddingTopButton" id="nomeugempePco{{$sfpco_current->id}}">{{ $sfpco_current->nomeugempe ?? ' - ' }}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 label-title paddingTopButton">Repetir Dados das Contas para Todos os Empenhos?</div>
                        <div class="col-md-3 paddingTopButton">
                            {!! form_row($sfpcoForm->{'repetircontaempenho'.$sfpco_current->id}) !!}
                        </div>
                    </div>
                    <div class="row m-t-20">
                        <div class="col-md-3 label-title paddingTopButton">Tem Contrato?</div>
                        <div class="col-md-3 paddingTopButton">
                            {!! form_row($sfpcoForm->{'indrtemcontrato'.$sfpco_current->id}) !!}
                        </div>

                        <div class="col-md-3 label-title paddingTopButton">Despesa Antecipada?</div>
                        <div class="col-md-3 paddingTopButton">
                            {!! form_row(
                                    $sfpcoForm->{'despesaantecipada'.$sfpco_current->id}
                                        )
                            !!}
                        </div>
                    </div>
                    <div class="row m-t-20">
                        <div class="col-md-6">
                            @if($sfpcoForm->{'numclassd'.$sfpco_current->id})
                                {!! form_row($sfpcoForm->{'numclassd'.$sfpco_current->id}) !!}
                            @endif
                        </div>
                        <div class="col-md-6 ">
                            @if($sfpcoForm->{'txtinscrd'.$sfpco_current->id})
                                {!! form_row($sfpcoForm->{'txtinscrd'.$sfpco_current->id}) !!}
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            @if($sfpcoForm->{'numclasse'.$sfpco_current->id})
                                {!! form_row($sfpcoForm->{'numclasse'.$sfpco_current->id}) !!}
                            @endif
                        </div>
                        <div class="col-md-6">
                            @if($sfpcoForm->{'txtinscre'.$sfpco_current->id})
                                {!! form_row($sfpcoForm->{'txtinscre'.$sfpco_current->id}) !!}
                            @endif
                        </div>
                    </div>

                    @foreach($sfpco_current->sfpcoitem as $sfpcoitem_current)
                        <span style="font-weight: bold">
                            {!! form_errors($sfpcoForm->{'numempe'.$sfpcoitem_current->id.$sfpco_current->id}) !!}
                        </span>
                        <div class="box box-solid box-primary collapsed-box margin-top-20 count-pco-item"
                             data-count-pco-item="{{$loop->count}}"
                             data-numseqitem="{{$sfpcoitem_current->numseqitem}}"
                             id="{{ $sfpcoitem_current->id }}"
                        >
                            <div class="box-header with-border custom-shadow">
                                <div class="row pointer-hand">
                                    <div class="col-md-4" data-widget="collapse">
                                        <span class="caret"></span>
                                        <span class="title-item-acordion">Nº do Empenho: </span>
                                        <span style="font-weight: bold">{{ $sfpcoitem_current->numempe }}</span>
                                    </div>
                                    <div class="col-md-2" data-widget="collapse">
                                        <span class="title-item-acordion">Subelemento:</span>
                                        <span style="font-weight: bold">{{ $sfpcoitem_current->codsubitemempe }}</span>
                                    </div>
                                    <div class="col-md-2" data-widget="collapse">
                                        <span class="title-item-acordion">Liquidado:</span>
                                        <span
                                            style="font-weight: bold">{{ $sfpcoitem_current->indrliquidado ? 'SIM' : 'NÃO' }}</span>
                                    </div>
                                    <div class="col-md-2" data-widget="collapse">
                                        <span
                                            id="label-valor-sfpcoitem{{$sfpcoitem_current->id}}"
                                            class="title-item-acordion ">R$: {{number_format($sfpcoitem_current->vlr, 2, ',', '.')}}
                                        </span>
                                    </div>
                                    <div class="col-md-2">
                                        {{--Se houver apenas um item então não exibe botao de remover --}}
                                        {{--Deve existir pelo o menos um item                         --}}
                                        @if($loop->count > 1)
                                            <button
                                                type="button"
                                                class="btn btn-primary btn-sm pull-right tooltipRemoverPcoItem
                                                                                esconderQuandoApropriacaoEmitidaSiafi"
                                                style="margin-right: 5px;"
                                                onclick="clickBtnRemoverOuCriarNovoPcoItem(
                                                    '{{ $sfpco_current->id }}',
                                                {{ $sfpcoitem_current->id }},
                                                    null,
                                                    );"
                                            >
                                                <i class="custom-shadow fa fa-trash"></i>
                                            </button>
                                        @endif
                                        <button
                                            type="button"
                                            class="btn btn-primary btn-sm pull-right tooltipCopiarPcoItem
                                                                                esconderQuandoApropriacaoEmitidaSiafi"
                                            style="margin-right: 5px;"
                                            onclick="
                                                clickBtnRemoverOuCriarNovoPcoItem(
                                                '{{ $sfpco_current->id }}',
                                                null,
                                                '{{ $sfpcoitem_current->id }}'
                                                );"
                                        >
                                            <i class="custom-shadow fa fa-copy"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-6 label-title">Nº do Empenho</div>
                                    <div class="col-md-1 label-title">Subelemento</div>
                                    <div class="col-md-5 subelemento-progress-barcodsubitemempe{{$sfpcoitem_current->id.$sfpco_current->id}}"
                                         style="display: flex"
                                    >
                                        <div
                                            class="progress progress-xs progress-striped active"
                                            style="margin-bottom: 0; margin-top: 9px;"
                                        >
                                            <div
                                                class="progress-bar progress-bar-primary"
                                                style="width: 100%"
                                            ></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="text-danger"></div>
                                        <div class="input-group">
                                            {!! form_row($sfpcoForm->{'numempe'.$sfpcoitem_current->id.$sfpco_current->id}) !!}
                                            <span class="input-group-btn">
                                        <a
                                            data-toggle="collapse"
                                            data-parent="#accordion"
                                            href="#collapse{{$sfpcoitem_current->id}}"
                                            aria-expanded="true"
                                            class="btn btn-info btn-flat label-title"
                                        >
                                            <i class="fa  fa-list-alt"></i>
                                        </a>
                                            </span>
                                        </div>
                                        <div
                                            id="collapse{{$sfpcoitem_current->id}}"
                                            class="panel-collapse collapse box box-node box-node{{$sfpcoitem_current->id}}"
                                            data-idsfpcoitem="{{$sfpcoitem_current->id}}"
                                            aria-expanded="true"
                                            style="border-left: solid 1px; border-right: solid 1px; border-bottom: solid 1px; border-top: none"
                                        >
                                            <div class="box-body box-body{{$sfpcoitem_current->id}}">
                                                @include(
                                                    'vendor.backpack.base.mod.apropriacao.partials.instrumentos-cobranca-do-empenho',
                                                        [
                                                        'sfpcoitem_current' => $sfpcoitem_current
                                                        ]
                                                )
                                            </div>
                                            <div class="box-footer">
                                                <button
                                                    type="button"
                                                    class="btn btn-block btn-success btn-xs"
                                                    onclick="atualizarInstrumentosDeCobrancaDoEmpenho({{$sfpcoitem_current->id}}, {{$sfpco_current->id}}, false)"
                                                >
                                                    Salvar
                                                </button>
                                            </div>
                                        </div>

                                        {!! form_row($sfpcoForm->{'sfpcoitem'.$sfpcoitem_current->id.$sfpco_current->id}) !!}
                                    </div>
                                    <div class="col-md-6">
                                        {!! form_row($sfpcoForm->{'codsubitemempe'.$sfpcoitem_current->id.$sfpco_current->id}) !!}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 label-title">
                                        Liquidado?
                                    </div>
                                    <div class="col-md-6 label-title">
                                        R$
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        {{ $sfpcoitem_current->indrliquidado ? 'SIM' : 'NÃO' }}
                                    </div>
                                    <div class="col-md-6">
                                        {!! form_row(
                                                $sfpcoForm->{'vlr'.$sfpcoitem_current->id.$sfpco_current->id}
                                        ) !!}
                                    </div>
                                </div>
                                @php
                                    $campos = [
                                        'numclassa', 'txtinscra',
                                        'numclassb', 'txtinscrb',
                                        'numclassc', 'txtinscrc',
                                    ];
                                @endphp

                                <div class="row">
                                    @php $colCount = 0; $rowCount = 0; @endphp

                                    @foreach($campos as $campo)
                                        @if($sfpcoForm->{$campo.$sfpcoitem_current->id.$sfpco_current->id})
                                            <div class="col-md-6">
                                                {!! form_row($sfpcoForm->{$campo.$sfpcoitem_current->id.$sfpco_current->id}) !!}
                                            </div>
                                            @php
                                                $colCount += 6;
                                                if ($colCount >= 12) {
                                                    $colCount = 0;
                                                    $rowCount++;
                                                }
                                            @endphp
                                        @endif
                                    @endforeach
                                </div>

                            </div>
                        </div>
                        @if($loop->last)
                            <div id="btn-add-novo-pco-item-wrapper{{$sfpco_current->id}}" style="text-align: center">
                                <a
                                    href="javascript:void(0)"
                                    id="nova-aba-situacao{{$sfpco_current->id}}"
                                    onclick="clickBtnRemoverOuCriarNovoPcoItem(
                                        '{!! $sfpco_current->id !!}',
                                        null,
                                        null
                                        );"
                                    class="tooltip-btn-add-novo-pco-item esconderQuandoApropriacaoEmitidaSiafi"
                                >
                                    <i class="fa fa-plus" style="color: #3c8dbc"></i>
                                </a>
                            </div>
                        @endif
                    @endforeach
                <!--Se não houver nenhum item de empenho permite adicionar-->
                    @if(count($sfpco_current->sfpcoitem) === 0)
                        <div id="btn-add-novo-pco-item-wrapper{{$sfpco_current->id}}" style="text-align: center">
                            <a
                                href="javascript:void(0)"
                                id="nova-aba-situacao{{$sfpco_current->id}}"
                                onclick="clickBtnRemoverOuCriarNovoPcoItem(
                                    '{!! $sfpco_current->id !!}',
                                    null,
                                    null
                                    );"
                                class="tooltip-btn-add-novo-pco-item esconderQuandoApropriacaoEmitidaSiafi"
                            >
                                <i class="fa fa-plus" style="color: #3c8dbc"></i>
                            </a>
                        </div>
                    @endif
                <!--***********************************************
                *Criação dos campos de Cronograma Baixa Patrimonial
                ************************************************-->
                <div
                    id="cronograma-baixa-patrimonial-wrapper{{$sfpco_current->id}}"
                    style="display: {{$sfpco_current->despesaantecipada ? 'block' : 'none'}}"
                >
                        @include(
                                'vendor.backpack.base.mod.apropriacao.partials.cronograma-baixa-patrimonial',
                                    [
                                        'sfpcoForm' => $sfpcoForm,
                                        'sfpco_current' => $sfpco_current,
                                        'txtobser_pco' => $txtobser_pco,
                                    ]
                            )
                    </div>
                </div>
                {!! form_row($sfpcoForm->{'sfpco'.$sfpco_current->id}) !!}
            @endforeach
        </div>
    </div>

    @if(isset($sfpco_current->id))
        <div class="align-table-items-center">
            <button class="custom-shadow btn btn-success esconderQuandoApropriacaoEmitidaSiafi" type="submit"
                    name="confirma-dados-pco" value="confirmar" id="confirma-dados-pco-{{$sfpco_current->id}}"><i class="custom-shadow fa fa-save"></i> Confirmar Dados Principal Com Orçamento</button>
        </div>
    @endif
</div>
<!--CAMPO OBSERVACAO -->
<div class="form-group">
    <label for="txtobser_pco" class="control-label">Observação</label>
    <textarea
        class="form-control"
        id="txtobser_pco"
        name="txtobser_pco"
        cols="50"
        rows="10"
        disabled
    >{!! $txtobser_pco !!}
        </textarea>
</div>

{{--<fieldset style="display: flex; flex-direction: row">--}}
{{--    <div>--}}
{{--    {!! form_row($sfpcoForm->btnSubmitFormSfpco)  !!}--}}
{{--    </div>--}}
{{--    <div class="mensagens-erro-aba-pco p-l-20" style="flex: 1; align-self: center"></div>--}}
{{--</fieldset>--}}
{!! form_end($sfpcoForm) !!}

@push('after_scripts')

    <style type="text/css">

        .tooltipRemoverPcoItem + .tooltip > .tooltip-inner {
            /*background-color: #00c0ef;*/
        }
        .tooltipRemoverPcoItem + .tooltip.bottom > .tooltip-arrow {
            /*border-bottom: 5px solid #00c0ef;*/
        }

    </style>
@endpush
