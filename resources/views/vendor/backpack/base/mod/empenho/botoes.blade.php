    <a class="btn btn-danger" title="{{config('mensagens.empenho-etapa-botao-voltar')}}"
       href="{{$rota}}">
        <i class="fa fa-arrow-left" ></i> Voltar
    </a>
    <button type="button" class="btn btn-success submeter">
        Próxima Etapa <i class="fa fa-arrow-right"></i>
    </button>

    @push('after_scripts')
        <script type="text/javascript">

            $(document).ready(function () {
                $('body').on('click', '.submeter', function (event) {

                    event.preventDefault();
                    $(this).closest("form").submit();

                    $(this).css('pointer-events', 'none');
                    $(this).css('opacity', '0.5');
                    return false;
                });

            });

        </script>
    @endpush
