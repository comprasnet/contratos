{{--{{ dd(get_defined_vars()['__data']) }}--}}

@php
    $minuta_id = Route::current()->parameter('minuta_id');
    //$fornecedor_id = Route::current()->parameter('fornecedor_id')
@endphp
@extends('backpack::layout')

@section('header')
    <section class="content-header">
        <h1>
            Subelemento
        </h1>
    </section>
@endsection

@section('content')
    @include('backpack::mod.empenho.telas.cabecalho')
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
            @endif
        @endforeach
    </div>
    <div class="box box-solid box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Saldo do Crédito Orçamentário</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-2 col-sm-3">
                    Crédito orçamentário:
                </div>
                <div class="col-md-10 col-sm-9" id="">
                    R$ {{ number_format($credito,2,',','.') }}
                </div>
            </div>
            <div class="row text-red">
                <div class="col-md-2 col-sm-3">
                    Utilizado:
                </div>
                <div class="col-md-10 col-sm-9" id="utilizado">
                    <b>R$ {{ number_format($valor_utilizado,2,',','.') }}</b>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-sm-3">
                    Saldo:
                </div>
                <div class="col-md-10 col-sm-9" id="saldo">
                    R$ {{ number_format($saldo,2,',','.') }}
                </div>
            </div>
        </div>
    </div>

    <div class="box box-solid box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Subelemento</h3>
        </div>

        <div class="box-body">
            <br/>
            <form action="/empenho/subelemento" method="POST" id="formSubElemento">
                <input type="hidden" id="minuta_id" name="minuta_id" value="{{$minuta_id}}">
{{--                <input type="hidden" id="fornecedor_id" name="fornecedor_id" value="{{$fornecedor_id}}">--}}
                <input type="hidden" id="credito" name="credito" value="{{$credito}}">
                <input type="hidden" id="valor_utilizado" name="valor_utilizado" value="{{$valor_utilizado}}">
                <input type="hidden" id="fonte_alterada" name="fonte_alterada" value="{{$fonte_alterada}}">
                @csrf <!-- {{ csrf_field() }} -->
                @if($update)
                    {!! method_field('PUT') !!}
                @endif

                {!! $html->table() !!}
                <div class="col-sm-12">

                </div>

                <div class="box-tools">
                    @include('backpack::mod.empenho.botoes',['rota' => route('empenho.minuta.etapa.saldocontabil', ['minuta_id' => $minuta_id])])
                </div>


            </form>
        </div>
    </div>

@endsection
@push('after_scripts')
    {!! $html->scripts() !!}
    <script src="{{ asset('vendor/sweetalert/2.10.16.11') }}/sweetalert2.all.min.js?time={{microtime()}}"></script>
    <script type="text/javascript">

        function bloqueia(tipo) {
            $('input[type=checkbox]').each(function () {
                if (tipo != $(this).data('tipo')) {
                    this.checked = false;
                }
            });
        }

        function calculaUtilizado(){
            var soma = 0;
            var saldo = {{$credito}};
            $(".valor_total").each(function (index) {
                var valor = ptToEn($(this).val());
                if (!isNaN(parseFloat(valor))) {
                    soma = parseFloat(valor) + parseFloat(soma);
                }
            });

            let somaAcumulador = somaAcumuludadorUtilizado(acumuludadorUtilizado) ;
            soma += somaAcumulador;

            saldo = saldo - soma;
            var saldo_br = (saldo.toLocaleString('pt-br', {minimumFractionDigits: 2, maximumFractionDigits: 2}));
            if (saldo_br == '-0,00'){
                saldo_br = '0,00'
            }

            $("#utilizado").html("<b>R$ " + soma.toLocaleString('pt-br', {maximumFractionDigits: 2, minimumFractionDigits: 2}) + "</b>");
            $("#saldo").html("R$ " + saldo_br.toLocaleString('pt-br', {minimumFractionDigits: 2, maximumFractionDigits: 2}) );
            $("#valor_utilizado").val(soma);
        }

        function calculaValorTotal(obj) {
            let {{$tipo}} = obj.dataset.{{$tipo}};
            let valor_total = obj.value * obj.dataset.valor_unitario;

            // Multiplicar por 1000, arredondar e dividir por 1000 para corrigir as casas decimais #961
            valor_total = Math.round(valor_total * 1000) / 1000;
            valor_total = valor_total.toLocaleString('pt-br', {minimumFractionDigits: 2, maximumFractionDigits: 2});

            $(".vrtotal" + {{$tipo}})
                .val(valor_total)
                .trigger("change");
            addToItensASubmeter(obj.dataset.itemId);
            calculaUtilizado();
        }

        function calculaQuantidade(obj) {

            var {{$tipo}} = obj.dataset.{{$tipo}};
            var value = obj.value;

            value = ptToEn(value);

            var quantidade = value / obj.dataset.valor_unitario;

            $("#qtd" + {{$tipo}}).val(quantidade.toFixed(5));
            calculaUtilizado();

            let qtdItem = obj.dataset.qtditem * 1
        }

        //Array contendo os itens a submeter caso tenha mais de uma páginao datatables
        var arrItensASubmeter = [];

        //variável criada com o propósito de ajudar no cálculo do utilizado
        var acumuludadorUtilizado = [];

        $(document).ready(function () {

            $('body').on('input', '.qtd', function (event) {
                calculaValorTotal(this);
            });

            $('.submeter').click(function (event) {

                let form = $('#formSubElemento');

                $.each(arrItensASubmeter, function(index, value) {

                    if($('#quantidade_total'+value['item_id']).length == 0 ){
                        form.append('<input type="hidden" name = "quantidade_total[]" value="'+value['quantidade_total']+'" />')
                        form.append('<input type="hidden" name = "valor_total[]"      value="'+value['valor_total']+'" />')
                        form.append('<input type="hidden" name = "valor_total_item[]" value="'+value['valor_total_item']+'" />')
                        form.append('<input type="hidden" name = "'+ value['tipo']+'_item_id[]"   value="'+value['item_id']+'" />')
                        form.append('<input type="hidden" name = "qtd[]"              value="'+value['qtd']+'" />')
                        form.append('<input type="hidden" name = "subitem[]"          value="'+value['subitem']+'" />')
                    }
                });

                $(".valor_total").each(function () {
                    $(this).removeAttr('disabled');
                    $(this).prop('readonly', true);
                });

                $(".qtd").each(function () {
                    $(this).prop('readonly', true);
                });

            });

            //Evento disparado sempre que a tabela é redesenhada na página
            $( "#dataTableBuilder" ).on( "draw.dt", function() {
                $('.subitem').select2(); atualizaMascara();

                if ($('#fonte_alterada').val() == true){

                    $(".valor_total").each(function () {
                        $(this).attr('disabled',true);
                        $(this).prop('readonly', true);
                    });

                    $(".qtd").each(function () {
                        $(this).prop('readonly', true);
                    });
                }

                let tipo = 'contrato';
                if ($('#tipo_empenho_por').val() !== 'Contrato'){
                    tipo = 'compra'
                }

                $(".valor_total").each(function () {
                    let item_id = $(this).data('item-id');

                    let procura = procuraLinha({item_id : item_id}, arrItensASubmeter);

                    if (procura !== false) {
                        $('#' + tipo + '_item_id_' + item_id)
                            .val(arrItensASubmeter[procura].tipo_alteracao)
                            .trigger('change');

                        $('#qtd' + item_id).val(arrItensASubmeter[procura].qtd)
                        $('#vrtotal' + item_id).val(arrItensASubmeter[procura].valor_total).keyup()

                    }
                });
            });

        });


        function atualizaMascara() {
            var maxLength = '000.000.000.000.000,00'.length;
            $('.valor_total').maskMoney({
                allowNegative: false,
                thousands: '.',
                decimal: ',',
                precision: 2,
                affixesStay: false
            }).attr('maxlength', maxLength).trigger('mask.maskMoney');
        }

        function ptToEn(value) {

            value = value.replaceAll('.', '');
            return value.replaceAll(',', '.');
        }

        function addToItensASubmeter(item_id){

            let tipo = 'compra';

            if ($('#compra_item_id_' + item_id).select2('data') == undefined) {
                tipo = 'contrato';
            }

            let line = {
                numseq           : $('#vrtotal' + item_id).data('numseq'),
                quantidade_total : $('#quantidade_total' + item_id)  . val(),
                valor_total      : $('#vrtotal' + item_id)           . val(),
                valor_total_item : $('#valor_total_item' + item_id)  . val(),
                vlr_total_item   : $('#vlr_total_item' + item_id)    . val(),
                qtd              : $('#qtd' + item_id)               . val(),
                tipo             : tipo,
                item_id          : item_id,
                subitem          : $('input[name ="subitem[]"]')     .val()
            };

            let procura = procuraLinha(line, arrItensASubmeter);

            if (procura === false) {
                arrItensASubmeter.push(line);
            } else {
                arrItensASubmeter[procura] = line;
            }

            // todo: verificar logica do aux;

            let aux = 1;
            /*if (text_tipo_alteracao == 'ANULAÇÃO'){
                aux = -1;
            }
            if (text_tipo_alteracao == 'NENHUMA'){
                aux = 0;
            }*/

            let objAcumuludadorUtilizado = {item_id: item_id, valor_alteracao:ptToEn($('#vrtotal' + item_id) . val()) * aux}

            procura = procuraLinha(objAcumuludadorUtilizado, acumuludadorUtilizado);
            if (procura === false) {
                acumuludadorUtilizado.push(objAcumuludadorUtilizado);
            } else {
                acumuludadorUtilizado[procura] = objAcumuludadorUtilizado;
            }

        }

        function somaAcumuludadorUtilizado(acumuludadorUtilizado) {

            let itensNaTela = []
            let somaRetorno = 0

            $(".valor_total").each(function (index, element) {
                itensNaTela.push($(this).data('item-id'));
            });

            $.each(acumuludadorUtilizado,function (index,element){
                if ($.inArray(Number(element.item_id), itensNaTela) == -1){
                    somaRetorno += element.valor_alteracao;
                }
            });
            return somaRetorno
        }

        function procuraLinha(line, arrItensASubmeter){
            let aux = false;
            $.each(arrItensASubmeter,function (index,element){
                if (element.item_id == line.item_id){
                    aux = index
                    return false;
                }
            });
            return aux;
        }
        function exibirDescricaoCompleta(descricaoCompleta, descricaoResumida, idItem) {

            let idBotaoClicado = `botaoDescricaoCompleta_${idItem}`
            let idTexto = `textoDescricaoCompleta_${idItem}`
            let tipoBotao = $(`#${idBotaoClicado}`).data()

            let iconeBotao = 'fa fa-fw fa-caret-square-o-down'
            let novoTipo = 'resumido'
            let novoConteudo = descricaoResumida

            if(tipoBotao.tipo == 'resumido') {
                iconeBotao = "fa fa-fw fa-caret-square-o-up"
                novoTipo = 'detalhado'
                novoConteudo = descricaoCompleta
            }

            $( `#${idBotaoClicado}` ).html(`<i class="${ iconeBotao }"></i>`);
            $(`#${idBotaoClicado}`).data('tipo',novoTipo);
            $(`#${idTexto}`).text(novoConteudo)

        }

    </script>
@endpush

