@push('after_scripts')

    <script src="{{ asset('vendor/sweetalert/2.10.16.11') }}/sweetalert2.all.min.js?time={{microtime()}}"></script>
    <script type="text/javascript">
        function deleteMinutaAlt(btn, alteracao_fonte_minutaempenho_id) {
            let text = '';
            let url = btn.dataset.route;
            if (typeof alteracao_fonte_minutaempenho_id !== "undefined") {
                text = 'A minuta correspondente com status "aguardando anulação" na nova fonte será excluída';
            }
            Swal.fire({
                title: 'Deseja deletar esta alteração?',
                icon: 'warning',
                text: text,
                showDenyButton: false,
                showCancelButton: true,
                confirmButtonText: 'Sim',
                cancelButtonText: 'Cancelar',
                denyButtonText: `Não`,
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = url;
                }
            })
        }


    </script>

@endpush
