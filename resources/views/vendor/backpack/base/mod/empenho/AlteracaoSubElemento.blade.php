{{--{{ dd(get_defined_vars()['__data']) }}--}}

@php
    $minuta_id = Route::current()->parameter('minuta_id');
    $fluxo = Session::get("fluxo_minuta") === 'minutaAlteracao' ? 'alteracao' : 'alteracaoFonte';
    //$fornecedor_id = Route::current()->parameter('fornecedor_id')
@endphp
@extends('backpack::layout')

@section('header')
    <section class="content-header">
        <h1>
            Subelemento
        </h1>
    </section>
@endsection

@section('content')
    @include('vendor.backpack.base.mod.empenho.telas.'.Session::get("cabecalho_minuta"))
    @if ( $errors->any())
        <div class="callout callout-danger">
            <h4>{{ trans('backpack::crud.please_fix') }}</h4>
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
            @endif
        @endforeach
    </div>
    <div class="box box-solid box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Saldo do Crédito Orçamentário</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-2 col-sm-3">
                    Crédito orçamentário:
                </div>
                <div class="col-md-2 col-sm-3" id="credito">
                    R$ {{ number_format($credito,2,',','.') }}
                </div>
                <div class="col-md-8 col-sm-6" id="">
                    <button type="button" class="btn btn-primary btn-sm pull-left" id="atualiza_credito">
                        Atualizar Crédito Orçamentário <i class="fa fa-refresh"></i>
                    </button>
                </div>
            </div>
            <div class="row text-red">
                <div class="col-md-2 col-sm-3">
                    Utilizado:
                </div>
                <div class="col-md-10 col-sm-9" id="utilizado">
                    <b>R$ {{ number_format($empenhado,2,',','.') }}</b>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-sm-3">
                    Saldo:
                </div>
                <div class="col-md-10 col-sm-9" id="saldo">
                    R$ {{ number_format($saldo,2,',','.') }}
                </div>
            </div>
        </div>
    </div>

    <div class="box box-solid box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Subelemento</h3>
        </div>

        <div class="box-body">
            <br/>
            <form action="{{$url_form}}" method="POST" id="formSubElemento">
                <input type="hidden" id="sispp_servico" name="sispp_servico" value="{{$sispp_servico}}">
                <input type="hidden" id="tipo_item" name="tipo_item" value="{{$tipo_item}}">
                <input type="hidden" id="tipo_empenho_por" name="tipo_empenho_por" value="{{$tipo_empenho_por}}">
                <input type="hidden" id="minuta_id" name="minuta_id" value="{{$minuta_id}}">
                <input type="hidden" id="nova_minuta_id" name="nova_minuta_id" value="{{$nova_minuta_id}}">
                <input type="hidden" id="fornecedor_id" name="fornecedor_id" value="{{$fornecedor_id}}">
                <input type="hidden" id="credito" name="credito" value="{{$credito}}">
                <input type="hidden" id="saldo_id" name="saldo_id" value="{{$saldo_id}}">
                <input type="hidden" id="valor_utilizado" name="valor_utilizado" value="{{$valor_utilizado}}">
            @csrf <!-- {{ csrf_field() }} -->
                @if($update !== false)
                    {!! method_field('PUT') !!}
                @endif

                {!! $html->table() !!}
                <div class="col-sm-12">

                </div>

                <div class="box-tools">
                    @include('backpack::mod.empenho.botoes',['rota' => route("empenho.crud.$fluxo.index", ['minuta_id' => $minuta_id])])
                </div>


            </form>
        </div>
    </div>

@endsection
@push('after_scripts')
    {!! $html->scripts() !!}
    <script src="{{ asset('vendor/axios') }}/axios.min.js?time={{microtime()}}"></script>
    <script src="{{ asset('vendor/sweetalert/2.10.16.11') }}/sweetalert2.all.min.js?time={{microtime()}}"></script>
    <script type="text/javascript">

        function BloqueiaValorTotal(tipo_alteracao, item_id) {
            let selected = $(tipo_alteracao).find(':selected').text();
            let minuta_por = $(tipo_alteracao).attr('id');
            let tipo_empenho_por = $('#tipo_empenho_por').val();
            $('#vrtotal' + item_id).val(0)
            $('#qtd'     + item_id).val(0)
            $('#qtd'     + item_id).prop('readonly', false)
            calculaUtilizado(minuta_por);


            //#356
            if (selected === 'ANULAÇÃO' ) {
                $('#qtd' + item_id).removeAttr('max');
                $('#qtd' + item_id).removeAttr('min');
            } else {
                $('#qtd' + item_id).attr('max',$('#qtd' + item_id).data('qtd_item'));
                $('#qtd' + item_id).attr('min',1);
            }

            if (selected === 'ANULAÇÃO SALDO IRRISÓRIO') {
                $('#vrtotal' + item_id).removeAttr('disabled')
                $('#vrtotal' + item_id).removeAttr('readonly')
                // $('#vrtotal' + item_id).attr({'max': 10}) //todo substituir o 10 pelo calculo de irrisorios (0,0001 * valor unitario)
                $('#qtd'     + item_id).val(0.00001)
                $('#qtd'     + item_id).prop('readonly', true)
                return;
            }

            // se for Suprimento
            if (tipo_empenho_por === 'Suprimento') {

                if (selected === 'CANCELAMENTO' || selected === 'NENHUMA') {
                    $('#vrtotal' + item_id).prop('disabled', true)
                    $('#qtd'     + item_id).prop('readonly', true)
                    return;
                }

                $('#vrtotal' + item_id).removeAttr('disabled');
                $('#qtd'     + item_id).prop('readonly', true);

            } else if (minuta_por.includes('contrato_item_id')) {
                // se for contrato
                // else if(minuta_por.includes('contrato_item_id') && ($('#tipo_item').val() === 'Serviço')){

                if (selected === 'CANCELAMENTO' || selected === 'NENHUMA') {
                    $('#vrtotal' + item_id).prop('disabled', true)
                    $('#qtd'     + item_id).prop('readonly', true)
                    return;
                }

                $('#vrtotal' + item_id).removeAttr('disabled');
                $('#qtd'     + item_id).prop('readonly', false);
            } else {
                //se for compra

                if (selected === 'CANCELAMENTO' || selected === 'NENHUMA') {
                    $('#vrtotal' + item_id).prop('disabled', true)
                    $('#qtd'     + item_id).prop('readonly', true)
                    return;
                }

                if ($('#sispp_servico').val() == false) {
                    $('#vrtotal' + item_id).prop('disabled', true)
                    $('#vrtotal' + item_id).prop('readonly', true)
                    $('#qtd'     + item_id).prop('readonly', false)
                    return;
                }
                $('#vrtotal' + item_id).removeAttr('disabled')
                $('#vrtotal' + item_id).removeAttr('readonly')
                $('#qtd' + item_id).prop('readonly', true)
            }
        }

        function bloqueia(tipo) {
            $('input[type=checkbox]').each(function () {
                if (tipo != $(this).data('tipo')) {
                    this.checked = false;
                }
            });
        }

        function calculaValorTotal(obj) {

            var tipo_operacao = $(this).closest('tr').find('select').find(':selected').text();

            if (tipo_operacao === '') {
                tipo_operacao = $('#' + '{{$tipo}}_' + obj.dataset.{{$tipo}}).find(':selected').text();
            }

            if (tipo_operacao === 'ANULAÇÃO' || tipo_operacao === 'ANULAÇÃO SALDO IRRISÓRIO'){
                var {{$tipo}} = obj.dataset.{{$tipo}};
                var valor_total = obj.value * obj.dataset.valor_unitario;
                valor_total = valor_total.toLocaleString('pt-br', {minimumFractionDigits: 2, maximumFractionDigits: 2});
                $(".vrtotal" + {{$tipo}})
                    .val(valor_total)
                    .trigger("input");
            }

            if (tipo_operacao === 'REFORÇO'){
                var {{$tipo}} = obj.dataset.{{$tipo}};
                var valor_total = obj.value * obj.dataset.valor_unitario;
                valor_total = valor_total.toLocaleString('pt-br', {minimumFractionDigits: 2, maximumFractionDigits: 2});
                $(".vrtotal" + {{$tipo}})
                    .val(valor_total)
                    .trigger("input");
            }

            calculaUtilizado('{{$tipo}}_' + obj.dataset.{{$tipo}});

        }

        number_format = function (number, decimals, dec_point, thousands_sep) {
            number = number.toFixed(decimals);

            var nstr = number.toString();
            nstr += '';
            x = nstr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? dec_point + x[1] : '';
            var rgx = /(\d+)(\d{3})/;

            while (rgx.test(x1))
                x1 = x1.replace(rgx, '$1' + thousands_sep + '$2');

            return x1 + x2;
        }

        function calculaUtilizado(tipo_operacao_id) {
            var soma = 0;
            var utilizado = 0;
            var saldo = {{$credito}};
            var valor_utilizado = {{$valor_utilizado}};
            var anulacao = outros = 0;
            $(".valor_total").each(function (index) {
                var valor = ptToEn($(this).val());

                var selected = $(this).closest('tr').find('select').find(':selected').text();

                if ($(this).closest('tr').find('select').length > 1){
                    let select = $(this).closest('tr').find('select')[1];
                    selected = select.options[select.selectedIndex].text;
                }
                if (selected === '') {
                    selected = $('#' + tipo_operacao_id).find(':selected').text();
                }

                if (!isNaN(parseFloat(valor))) {
                    if (selected === 'ANULAÇÃO' || selected === 'ANULAÇÃO SALDO IRRISÓRIO') {
                        anulacao = parseFloat(anulacao) + parseFloat(valor);
                    } else {
                        outros = parseFloat(outros) + parseFloat(valor);
                    }
                }
            });
            soma = parseFloat(anulacao * -1) + parseFloat(outros);
            let somaAcumulador = somaAcumuludadorUtilizado(acumuludadorUtilizado) ;
            soma += somaAcumulador;

            saldo = saldo - valor_utilizado - soma;

            // var saldo_br = (saldo.toLocaleString('pt-br', {minimumFractionDigits: 2}));
            var saldo_br = (saldo.toLocaleString('pt-br', {minimumFractionDigits: 2, maximumFractionDigits: 2}));

            if (saldo_br == '-0,00'){
                saldo_br = '0,00'
            }

            utilizado = outros - anulacao;
            if (anulacao > outros) {
                utilizado = anulacao - outros;
                utilizado *= -1;
            }
            utilizado += somaAcumulador;

            $("#utilizado").html("<b>R$ " + utilizado.toLocaleString('pt-br', {minimumFractionDigits: 2, maximumFractionDigits: 2}) + "</b>");
            $("#saldo").html('R$ ' + saldo_br);
            $("#valor_utilizado").val(soma);

        }

        function calculaQuantidade(obj) {
            var tipo_operacao = $(this).closest('tr').find('select').find(':selected').text();
            if (tipo_operacao === '') {
                tipo_operacao = $('#' + '{{$tipo}}_' + obj.dataset.{{$tipo}}).find(':selected').text();
            }

            if (tipo_operacao === 'ANULAÇÃO' ){

                var {{$tipo}} = obj.dataset.{{$tipo}};
                var value = obj.value;
                value = ptToEn(value);

                if(obj.dataset.valor_unitario != 0){
                    var quantidade = value / obj.dataset.vlr_unitario_item;
                    $(".qtd" + {{$tipo}}).val(quantidade).trigger("input");
                }
            }

            if (tipo_operacao === 'REFORÇO'){
                var {{$tipo}} = obj.dataset.{{$tipo}};
                var value = obj.value;
                value = ptToEn(value);

                if(obj.dataset.valor_unitario != 0){
                    var quantidade = value / obj.dataset.valor_unitario;
                    $(".qtd" + {{$tipo}}).val(quantidade).trigger("input");
                }
            }

            calculaUtilizado('{{$tipo}}_' + obj.dataset.{{$tipo}});
        }

        //Array contendo os itens a submeter caso tenha mais de uma páginao datatables
        var arrItensASubmeter = [];

        //variável criada com o propósito de ajudar no cálculo do utilizado
        var acumuludadorUtilizado = [];
        $(document).ready(function () {

            $('body').on('click', '#atualiza_credito', function (event) {
                atualizaLinhadeSaldo(event);
            });

            $('body').on('change', '.valor_total', function (event) {
                addToItensASubmeter($(this).data('item-id'))
                calculaUtilizado('{{$tipo}}_' + this.dataset.{{$tipo}});
            });

            $('body').on('input', '.valor_total', function (event) {
                calculaUtilizado('{{$tipo}}_' + this.dataset.{{$tipo}});
            });

            $('body').on('change', '.qtd', function (event) {
                addToItensASubmeter($(this).data('item-id'))
            });

            $('body').on('input', '.qtd', function (event) {
                calculaUtilizado('{{$tipo}}_' + this.dataset.{{$tipo}});
            });

            function submeterClick(event) {
                $('body').off('click', '.submeter', submeterClick);

                let form = $('#formSubElemento');

                $.each(arrItensASubmeter, function(index, value) {

                    if($('#quantidade_total'+value['item_id']).length == 0 ){
                        form.append(
                            '<input type="hidden" ' +
                            'name="numseq[]" ' +
                            'value="' + value['numseq'] + '" />'
                        );
                        form.append(
                            '<input type="hidden" ' +
                            'name="quantidade_total[]" ' +
                            'value="' + value['quantidade_total'] + '" />'
                        );
                        form.append(
                            '<input type="hidden" ' +
                            'name="valor_total[]" ' +
                            'value="' + value['valor_total'] + '" />'
                        );
                        form.append(
                            '<input type="hidden" ' +
                            'name="valor_total_item[]" ' +
                            'value="' + value['valor_total_item'] + '" />'
                        );
                        form.append(
                            '<input type="hidden" ' +
                            'name="vlr_total_item[]" ' +
                            'value="' + value['vlr_total_item'] + '" />'
                        );
                        form.append(
                            '<input type="hidden" ' +
                            'name="' + value['tipo'] + '_item_id[]" ' +
                            'value="' + value['item_id'] + '" />'
                        );
                        form.append(
                            '<input type="hidden" ' +
                            'name="tipo_alteracao[]" ' +
                            'value="' + value['tipo_alteracao'] + '" />'
                        );
                        form.append(
                            '<input type="hidden" ' +
                            'name="qtd[]" ' +
                            'value="' + value['qtd'] + '" />'
                        );
                        form.append(
                            '<input type="hidden" ' +
                            'name="subitem[]" ' +
                            'value="' + value['subitem'] + '" />'
                        );
                        form.append(
                            '<input type="hidden" ' +
                            'name="valorunitario[]" ' +
                            'value="' + value['valorunitario'] + '" />'
                        );
                        form.append(
                            '<input type="hidden" ' +
                            'name="numero_item[]" ' +
                            'value="' + value['numero_item'] + '" />'
                        );
                        form.append(
                            '<input type="hidden" ' +
                            'name="cif_id[]" ' +
                            'value="' + value['cif_id'] + '" />'
                        );
                        form.append(
                            '<input type="hidden" ' +
                            'name="ciu_id[]" ' +
                            'value="' + value['ciu_id'] + '" />'
                        );
                        // O campo "subitemNovo" pode não existir dependendo da tela, assim,
                        // primeiro verifica se ele existe e depois adiciona
                        if ($('[id^="subitemNovo"]').length > 0) {
                            form.append(
                                '<input type="hidden" ' +
                                'name="subitemNovo[]" ' +
                                'value="' + value['subitemNovo'] + '" />'
                            );
                        }
                    }
                });

                // Ao submeter, vai remover os atributos de desabilitado dos campos de valor total
                $(".valor_total").each(function () {
                    $(this).removeAttr('disabled');
                    $(this).prop('readonly', true);
                });

                $(".qtd").each(function () {
                    $(this).prop('readonly', true);
                });

            }

            $('.submeter').click(submeterClick);

            //Evento disparado sempre que a tabela é redesenhada na página, inclusive na primeira vez
            $( "#dataTableBuilder" ).on( "draw.dt", function() {
                $('.subitem').select2(); atualizaMascara();

                let tipo = setTipo();

                $(".valor_total").each(function () {
                    let item_id = $(this).data('item-id');

                    // adicionar a tabela os itens guardados no arrItensASubmeter
                    let procura = procuraLinha({item_id : item_id}, arrItensASubmeter);

                    if (procura !== false) {
                        $('#' + tipo + '_item_id_' + item_id)
                            .val(arrItensASubmeter[procura].tipo_alteracao)
                            .trigger('change');

                        if ($('[id^="subitemNovo"]').length > 0) {
                            $('#subitemNovo' + item_id)
                                .val(arrItensASubmeter[procura].subitemNovo)
                                .trigger('change');
                        }

                        $('#qtd' + item_id).val(arrItensASubmeter[procura].qtd)
                        $('#vrtotal' + item_id).val(arrItensASubmeter[procura].valor_total).keyup()
                    } // fim do adicionar

                    let text_tipo_alteracao = $('#' + tipo + '_item_id_' + item_id).select2('data')[0].text;
                    if (text_tipo_alteracao === 'NENHUMA'){
                        $(this).prop('disabled', true)
                        $('#qtd' + item_id).prop('readonly', true)
                    }
                    if (text_tipo_alteracao === 'ANULAÇÃO SALDO IRRISÓRIO'){
                        $(this).prop('disabled', false)
                        $('#qtd' + item_id).val(0.00001).prop('readonly', true)
                    }

                });
            });

            //Evento disparado no início da página quando o Datatables estiver completamente inicializado e carregado.
            //https://datatables.net/reference/event/init
            $('#dataTableBuilder').on( 'init.dt', function () {
                return;
            } );



            //Evento disparado ao alterar a paginação
            //Ele é disparado antes do redesenho da página
            //https://datatables.net/reference/event/page
            $('#dataTableBuilder').on( 'page.dt', function () {
                return;
                var info = $('#dataTableBuilder').DataTable().page.info();
                // console.log( 'Showing page: '+info.page+' of '+info.pages );
            } );

        });

        function atualizaMascara() {
            var maxLength = '000.000.000.000.000,00'.length;
            $('.valor_total').maskMoney({
                allowNegative: false,
                thousands: '.',
                decimal: ',',
                //allowZero: true,
                affixesStay: false
            }).attr('maxlength', maxLength).trigger('mask.maskMoney');
        }

        function ptToEn(value) {

            value = value.replaceAll('.', '');
            return value.replaceAll(',', '.');
        }

        function atualizaSaldos(credito) {
            var saldo = 0;
            var utilizado = parseFloat($('#utilizado').text().replace('R$', ''));

            if (utilizado < 0) {
                saldo = (parseFloat(credito + (utilizado * -1)));
            } else {
                saldo = (parseFloat(credito - utilizado));
            }

            $('#credito').text('R$ ' + number_format(credito, 2, ',', '.'));
            $('#saldo').text('R$ ' + number_format(saldo, 2, ',', '.'));

        }

        function atualizaLinhadeSaldo(event) {
            var saldo_id = {{$saldo_id}};
            var url = "{{route('atualiza.saldos.linha',':saldo_id')}}";
            url = url.replace(':saldo_id', saldo_id);

            axios.request(url)
                .then(response => {
                    dados = response.data
                    if (dados == true) {
                        atualizaCreditoOrcamentario(event)
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Crédito Orçamentário Atualizado com sucesso!',
                            showConfirmButton: false,
                            timer: 1500
                        })
                        var table = $('#dataTableBuilder').DataTable();
                        table.ajax.reload();
                    } else {
                        Swal.fire({
                            position: 'top-end',
                            icon: 'warning',
                            title: 'O saldo já está atualizado!',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    }
                })
                .catch(error => {
                    alert(error);
                })
                .finally()
            event.preventDefault()
        }

        function atualizaCreditoOrcamentario(event) {
            var minuta_id = {{$minuta_id}}
            var url = "{{route('atualiza.credito.orcamentario',':minuta_id')}}";
            url = url.replace(':minuta_id', minuta_id);

            axios.request(url)
                .then(response => {
                    credito = response.data
                    atualizaSaldos(credito);
                })
                .catch(error => {
                    alert(error);
                })
                .finally()
            event.preventDefault()
        }

        function addToItensASubmeter(item_id) {
            let tipo = setTipo();
            let line = {
                numseq           : $('#vrtotal'                  + item_id) . data('numseq'),
                quantidade_total : $('#quantidade_total'         + item_id) . val(),
                valor_total      : $('#vrtotal'                  + item_id) . val(),
                valor_total_item : $('#valor_total_item'         + item_id) . val(),
                vlr_total_item   : $('#vlr_total_item'           + item_id) . val(),
                tipo_alteracao   : $('#' + tipo + '_item_id_'    + item_id) . select2('data')[0] . id,
                qtd              : $('#qtd'                      + item_id) . val(),
                tipo             : tipo,
                item_id          : item_id,
                subitem          : $('input[name ="subitem[]"]') . val(),
                valorunitario    : $('#valorunitario'            + item_id) . val(),
                numero_item      : $('#numero_item'              + item_id) . val(),
                cif_id           : $('#cif_id'                   + item_id) . val(),
                ciu_id           : $('#ciu_id'                   + item_id) . val(),
            };

            // O campo "subitemNovo" pode não existir dependendo da tela, assim,
            // primeiro verifica se ele existe e depois adiciona
            if ($('[id^="subitemNovo"]').length > 0) {
                line.subitemNovo = $('#subitemNovo' + item_id).select2('data')[0].id;
            }

            let procura = procuraLinha(line, arrItensASubmeter);

            if (procura === false) {
                arrItensASubmeter.push(line);
            } else {
                arrItensASubmeter[procura] = line;
            }


            let text_tipo_alteracao = $('#' + tipo + '_item_id_' + item_id).select2('data')[0].text;

            let aux = 1;
            if (text_tipo_alteracao === 'ANULAÇÃO' || text_tipo_alteracao === 'ANULAÇÃO SALDO IRRISÓRIO') {
                aux = -1;
            }
            if (text_tipo_alteracao == 'NENHUMA') {
                aux = 0;
            }

            let objAcumuludadorUtilizado = {
                item_id: item_id,
                valor_alteracao: ptToEn($('#vrtotal' + item_id).val()) * aux
            }

            procura = procuraLinha(objAcumuludadorUtilizado, acumuludadorUtilizado);
            if (procura === false) {
                acumuludadorUtilizado.push(objAcumuludadorUtilizado);
            } else {
                acumuludadorUtilizado[procura] = objAcumuludadorUtilizado;
            }

        }

        function somaAcumuludadorUtilizado(acumuludadorUtilizado) {

            let itensNaTela = []
            let somaRetorno = 0

            $(".valor_total").each(function (index, element) {
                itensNaTela.push($(this).data('item-id'));
            });


            $.each(acumuludadorUtilizado,function (index,element){
                if ($.inArray(element.item_id, itensNaTela) == -1){
                    somaRetorno += element.valor_alteracao;
                }
            });
            return somaRetorno
        }

        function procuraLinha(line, arrItensASubmeter) {
            let aux = false;
            $.each(arrItensASubmeter, function (index, element) {
                if (element.item_id == line.item_id) {
                    aux = index
                    return false;
                }
            });
            return aux;
        }

        function exibirDescricaoCompleta(descricaoCompleta, descricaoResumida, idItem) {

            let idBotaoClicado = `botaoDescricaoCompleta_${idItem}`
            let idTexto = `textoDescricaoCompleta_${idItem}`
            let tipoBotao = $(`#${idBotaoClicado}`).data()

            let iconeBotao = 'fa fa-fw fa-caret-square-o-down'
            let novoTipo = 'resumido'
            let novoConteudo = descricaoResumida

            if(tipoBotao.tipo == 'resumido') {
                iconeBotao = "fa fa-fw fa-caret-square-o-up"
                novoTipo = 'detalhado'
                novoConteudo = descricaoCompleta
            }

            $( `#${idBotaoClicado}` ).html(`<i class="${ iconeBotao }"></i>`);
            $(`#${idBotaoClicado}`).data('tipo',novoTipo);
            $(`#${idTexto}`).text(novoConteudo)

        }

        function setTipo(tipo = 'contrato'){
            if ($('#tipo_empenho_por').val() !== 'Contrato'){
                tipo = 'compra'
            }
            return tipo;
        }

    </script>
@endpush

