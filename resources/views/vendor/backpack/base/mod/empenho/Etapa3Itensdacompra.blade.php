@php
    $minuta_id = Route::current()->parameter('minuta_id');
    $fornecedor_id = Route::current()->parameter('fornecedor_id')
@endphp
@extends('backpack::layout')

@section('header')
    <section class="content-header">
        <h1>
            Itens
            <small>da Compra / Contrato</small>
        </h1>
    </section>
@endsection

@section('content')
    @include('backpack::mod.empenho.telas.cabecalho')
    <div class="flash-message">
        <p class="alert alert-warning"><b>Atenção:</b> Não serão exibidos itens sem saldo, ou com data de vigência expirada.</p>
    </div>
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
            @endif
        @endforeach
    </div>
    <div class="box box-solid box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Itens da Compra / Contrato</h3>
        </div>
        <div class="box-body">
            <br/>
            <form action="/empenho/item" method="POST" id="formItensEtapa3">
                <input type="hidden" id="minuta_id" name="minuta_id" value="{{$minuta_id}}">
                <input type="hidden" id="fornecedor_id" name="fornecedor_id" value="{{$fornecedor_id}}">
            @csrf <!-- {{ csrf_field() }} -->
                @if($update)
                    {!! method_field('PUT') !!}
                @endif
                {!! $html->table() !!}
                <div class="col-sm-12">

                </div>
                <div class="box-tools">
                    @include('backpack::mod.empenho.Etapa3botoes',
                        ['rota' => route('empenho.minuta.etapa.fornecedor', ['minuta_id' => $minuta_id])]
                    )
                </div>
            </form>
        </div>
    </div>

@endsection

@push('after_scripts')
    {!! $html->scripts() !!}
    <script type="text/javascript">
        //Array contendo os itens a submeter caso tenha mais de uma página no datatables
        //caso tenha old, insere também
        var arrItensASubmeter = [{!! !empty(old() && null !== old('itens'))
        ? "'".collect(old('itens'))->implode('compra_item_id',"','")."'"
        : '' !!}];

        $(document).ready(function () {
            let fornecedores = {!!json_encode($classificacaoFornecedores)!!};

            $('.submeter').click(function (event) {
                let form = $('#formItensEtapa3');
                let tipo_item = $('input[name*="_item"]' ).data('tipo_item');
                let classificacao = 0;
                let temClassificacaoMelhor = false;
                //para cada item do array a Submeter
                $.each(arrItensASubmeter, function (index, value) {
                    let item = $('#item_' + value);

                    //se nao estiver marcado (pois é de outra página)
                    if (!item.is(':checked')) {
                        //acrescenta um item ao form para ser enviado ao backend
                        form.append('<input type="checkbox" name="itens[][' + tipo_item + ']" ' +
                            'value="' + value + '" checked style="display:none">')
                    }
                    // só será != null quando a minuta for tipo compra
                    if(fornecedores != null){
                        let outrosFornecedoresClassificacao = '';
                        $.each(fornecedores, function(indexFornecedor, valueFornecedor){
                        if(valueFornecedor.compra_item_id == value){
                            if(valueFornecedor.fornecedor_id == {{$fornecedor_id}}){
                                classificacao = valueFornecedor.classificacao;
                            }else{
                                outrosFornecedoresClassificacao = valueFornecedor.classificacao;
                            }

                            if(outrosFornecedoresClassificacao == '001'){
                                temClassificacaoMelhor = true;
                                Swal.fire({
                                    title: "Alerta: Existe(m) fornecedor(es) melhor classificado(s) para algum(ns) dos itens selecionados. Deseja prosseguir?",
                                    showDenyButton: true,
                                    showCancelButton: false,
                                    confirmButtonText: 'Continuar',
                                    denyButtonText: 'Cancelar',
                                    }).then((result) => {
                                    /* Read more about isConfirmed, isDenied below */
                                    if (result.isConfirmed) {
                                        form.submit();
                                        return true;
                                    } else if (result.isDenied) {
                                        return false;
                                    }
                                });
                            }

                        }
                        });
                    }
                });
                if(temClassificacaoMelhor == false){
                    form.submit();
                }
                return false;
                // event.preventDefault();
            });

            $("#dataTableBuilder").on("draw.dt", function () {

                //vincular envento de click aos botoes de checkbox
                attachEventClickParaBotaoCheckbox();

                //para cada item do array a Submeter
                $.each(arrItensASubmeter, function (index, value) {
                    //marca o checkbox
                    $('#item_' + value).prop('checked', true)
                });

                //marcar ou desmarcar botao selecionar todos
                marcarDesmarcarBotaoSelecionarTodos();

            });
        });

        /**
         * Vincular envento de click aos botoes de checkbox, o evento é chamado quando o table é desenhado na tela
         */
        function attachEventClickParaBotaoCheckbox()
        {
            $('#selectAll').click(function () {

                var checkedStatus = this.checked;
                $('input[type=checkbox]').each(function (index) {

                    if(index !== 0)// 0 é o checkbox de 'Selecionar todos' então n entra
                    {
                        toggleItensASubmeter($(this).val(), checkedStatus);//adiciona ou retira itens do array a ser submetido

                        $(this).prop('checked', checkedStatus);
                    }

                });
            });
        }

        /**
         * Alternar entre os tipos "Material" e "Serviço"
         */
        function bloqueia(tipo) {
            $('input[type=checkbox]').each(function () {
                if (tipo != $(this).data('tipo')) {
                    this.checked = false;

                    //retira do array a ser submetido
                    toggleItensASubmeter(this.value, false)
                }
            });
        }

        // adiciona ou retira itens do array a ser submetido
        function toggleItensASubmeter(item, checked) {
            if (checked) {
                //se nao existe no array, adiciona
                if (!arrItensASubmeter.includes(item)) {
                    arrItensASubmeter.push(item);
                }
                return;
            }

            //caso o item for desmarcado, retira do array caso exista
            var index = arrItensASubmeter.indexOf(item);
            if (index > -1) {
                arrItensASubmeter.splice(index, 1);
            }

        }

        function marcarDesmarcarBotaoSelecionarTodos()
        {
            var booTodosSelecionados = true;

            $('input[type=checkbox]').each(function (index,element) {

                //adiciona ou retira itens do array a ser submetido
                if(index !== 0)// 0 é o checkbox de 'Selecionar todos' então n entra
                {
                    if(element.checked === false)
                    {
                        booTodosSelecionados = false;
                    }
                }
            });

            $('#selectAll').prop('checked', booTodosSelecionados);
        }
    </script>
@endpush
