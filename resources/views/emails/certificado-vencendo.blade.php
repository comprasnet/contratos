@component('mail::message')
    # Certificado Vencendo

    O certificado do Siafi expirará em {{ $diasRestantesCertificadoSiafi }} dias. Solicite à equipe técnica a renovação.

    Mais detalhes em https://gitlab.com/comprasnet/infra/-/wikis/Certificado-SIAFI (página de acesso restrito).

    Atenciosamente,
    Contratos.gov.br
@endcomponent
