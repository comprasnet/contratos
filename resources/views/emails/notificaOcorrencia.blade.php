@component('mail::message')
# Notificação Ocorrência

Foi registrada uma Ocorrência por um Usuário do Contratos.gov.br - Gestão de Contratos, com as seguintes informações:

* Órgão: **{{ $dadosocorrencia['orgao'] }}**
* Unidade: **{{ $dadosocorrencia['unidade'] }}**
* Fornecedor: **{{ $dadosocorrencia['fornecedor'] }}**
* Número Contrato: **{{ $dadosocorrencia['contrato_numero'] }}**
* Responsável pela Ocorrência: **{{ $dadosocorrencia['user'] }}**
* Situação da Ocorrência: **{{ $dadosocorrencia['situacao'] }}**
* Data da Ocorrência: **{{ implode('/', array_reverse(explode('-', $dadosocorrencia['data']))) }}**
* Texto Ocorrência: **{{ $dadosocorrencia['textoocorrencia'] }}**

E-mail Gerado Automaticamente pelo Contratos.gov.br Contratos. Por favor não responda.
@endcomponent
