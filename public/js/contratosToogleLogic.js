/**
 * Alterna a visibilidade de um elemento.
 * @param {string} selector - O seletor do elemento.
 * @param {boolean} show - Se deve mostrar ou esconder o elemento.
 */
function toggleVisibility(selector, show) {
    $(selector).each(function () {
        const element = $(this);
        if (show) {
            element.closest('.form-group').show();
            element.attr("disabled", false);
        } else {
            element.closest('.form-group').hide();
            element.attr("disabled", true).prop("checked", false);
        }
    });

    enableDisableFieldItens();
}

/**
 * Habilita ou desabilita campos de entrada com base no estado dos checkboxes.
 * Se pelo menos um checkbox com a classe `.aplicavel_decreto` estiver marcado e tiver o valor `1`,
 * os campos `.qtd_aplicavel_decreto` e `.qtd_terceirizado` serão habilitados.
 * Caso contrário, esses campos serão desabilitados.
 */
function enableDisableFieldItens() {
    const elements = [...document.querySelectorAll('.aplicavel_decreto')];
    const isChecked = elements.some(element => element.checked && element.value == 1);

    document.querySelectorAll('.qtd_aplicavel_decreto, .qtd_terceirizado').forEach(field => {
        if (!isChecked) {
            field.value = '';
        }
        field.disabled = !isChecked;
    });
}

function toogleCompraBrasil(value) {
    toggleVisibility("#wrapper_numero_contratacao", (value == 1));
    toggleVisibility("input[name='licitacao_numero']", !(value == 1));
    toggleVisibility("#wrapper_unidade_compra", !(value == 1));
    toggleVisibility("#wrapper_unidade_beneficiaria", !(value == 1));

    // caso seja Contrata+Brasil
    if (value == 1) {
        $('#licitacao_numero').val(null)
        $('[name="unidadecompra_id"]').val(null).trigger('change');
        $('[name="unidadebeneficiaria_id"]').val(null).trigger('change');
        return
    }

    $('#numero_contratacao').val(null)
}

function toogleCompraBrasilOnLoad() {
    toogleCompraBrasil($('input[name="contrata_mais_brasil"]:checked').val())
}


