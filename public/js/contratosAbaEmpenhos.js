$('#buscarEmpenho').click(function () {
    let closest = $(this).closest('.form-group').find('input[name="search_empenho_field"]');
    let fornecedor_id = $('#select2_ajax_fornecedor_id').val();
    let fornecedor_empenho_id = $('#select2_ajax_multiple_fornecedor_empenho').val();
    let unidade_empenho_id = $('#select2_ajax_multiple_unidadesdescentralizadas').val();
    let empenhoValue = closest.val();

    buscaEmpenhoAjax(empenhoValue, fornecedor_id, fornecedor_empenho_id, unidade_empenho_id, closest);
});

function buscaEmpenhoAjax(empenhoValue, fornecedor_id, fornecedor_empenho_id, unidade_empenho_id, closestElement, isFromMinuta = false) {

    let fornecedor_empenho = $('#select2_ajax_multiple_fornecedor_empenho');
    let unidade_empenho = $('#select2_ajax_multiple_unidadesdescentralizadas');
    let unidadeEmpenhoTexto = unidade_empenho.text();
    let codigoUgEmitente = unidadeEmpenhoTexto.split('-')[0].trim();

    // Verifica se já foi adicionado um empenho com o mesmo número e mesma ug emitente
    let dynamicClass = `emitenteAndNumero-${codigoUgEmitente}${empenhoValue}`;
    let exists = $(`.${dynamicClass}`).length > 0;

    //Limpa os campos pq o select2 fica salvando o valor do anterior
    unidade_empenho.val(null).trigger('change');
    unidade_empenho.html(null).trigger('change');
    fornecedor_empenho.val(null).trigger('change');
    fornecedor_empenho.html(null).trigger('change');

    if (exists) {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Este número de empenho já foi adicionado.'
        });
        //Limpa os campos pq o select2 fica salvando o valor do anterior
        unidade_empenho.val(null).trigger('change');
        fornecedor_empenho.val(null).trigger('change');
        return;
    } else {
        $('#buscarEmpenho').attr('disabled', true).val('Carregando...').after('&nbsp;&nbsp;<span class="fa fa-refresh fa-spin" id="spinner"></span>');

        $.ajax({
            method: 'POST',
            url: buscaEmpenhoUrl,
            data: {
                empenho: empenhoValue,
                fornecedor_id: fornecedor_id,
                fornecedor_empenho: fornecedor_empenho_id,
                unidade_empenho_id: unidade_empenho_id,
                isFromMinuta: isFromMinuta
            },
            success: function (data) {
                if (data.found === true) {
                    addEmpenho(empenhoValue, data, unidade_empenho_id, closestElement, fornecedor_empenho_id);
                } else if (data.isFromMinuta === true) {
                    $.each(data.objMinuta, function (key, value) {
                        addEmpenho(value.mensagem_siafi, data.objMinuta[key], value.unidade_id, closestElement, fornecedor_id, true);
                    });
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: data.message
                    });
                }
            },
            error: function (xhr, status, error) {
                console.error('Erro ao buscar empenhos:', error, xhr, status);
            },
            complete: function () {
                console.log('opa');
                $('#spinner').remove();
                $('#buscarEmpenho').attr('disabled', false).val('Buscar Empenho');
                closestElement.val('');
            }
        });
    }
}

// Adiciona dinamicamente os empenhos buscados em uma tabela para melhor visualização no frontend
function addEmpenho(empenhoValue, data, unidade_empenho_id, closest, fornecedor_id, isFromMinuta = false) {
    let table = $('.divTableEmpenhos');
    let ugEmitente = null;
    let ugCodigo = null;

    // Verifica se a tabela dinâmica já existe
    // Caso não exista, adiciona ela
    if (table.length === 0) {
        closest.parent().after(
            '<div class="container col-xs-12 divTableEmpenhos">' +
            '<h4>Empenhos Adicionados</h4>' +
            '<table class="table table-bordered table-empenho">' +
            '<thead>' +
            '<tr>' +
            '<th>Empenho</th>' +
            '<th>Unidade da Minuta do Empenho</th>' +
            '<th>Unidade Emitente do Empenho</th>' +
            '<th>Ações</th>' +
            '</tr>' +
            '</thead>' +
            '<tbody id="empenho-body">' +
            '</tbody>' +
            '</table>' +
            '</div>'
        );
    }

    if (isFromMinuta == true) {
        ugEmitente = data.ugemitente;
        ugCodigo = data.codigo;
        adicionaEmpenhos(empenhoValue, ugCodigo, ugEmitente, unidade_empenho_id, fornecedor_id, true);
    } else {
        ugEmitente = data.unidadeMinuta;
        ugCodigo = data.unidade.codigo;
        adicionaEmpenhos(empenhoValue, ugEmitente, ugCodigo, unidade_empenho_id, fornecedor_id, false);
    }

}

function adicionaEmpenhos(empenhoValue, ugEmitente, ugCodigo, unidade_empenho_id, fornecedor_id, isMinuta = false) {
    // Verifica se a tabela dinâmica já existe, se não existir, crie-a
    if ($('#empenho-body').length === 0) {
        $('.form-group').after(
            '<div class="container col-xs-12 divTableEmpenhos">' +
            '<h4>Empenhos Adicionados</h4>' +
            '<table class="table table-bordered table-empenho">' +
            '<thead>' +
            '<tr>' +
            '<th>Empenho</th>' +
            '<th>Unidade Emitente do Empenho</th>' +
            '<th>Unidade da Minuta do Empenho</th>' +
            '<th>Ações</th>' +
            '</tr>' +
            '</thead>' +
            '<tbody id="empenho-body">' +
            '</tbody>' +
            '</table>' +
            '</div>'
        );
    }

    // Se a coluna for adicionada pelo campo da minuta, ele insere uma classe diferente
    let empenhoRow = $(`<tr class="listagem-empenhos${isMinuta ? '-minuta' : ''} ${empenhoValue} emitenteAndNumero-${ugCodigo}${empenhoValue} "></tr>`);

    let empenhoInput = $('<input class="form-control" type="text" name="empenho_list[]" disabled>').val(empenhoValue);
    let unidadeInput = $('<input class="form-control" type="text" name="ugemitente_aba_empenho[]" disabled>').val(ugEmitente);
    let unidadeMinutaInput = $('<input class="form-control" type="text" name="ugcodigo_aba_empenho[]" disabled>').val(ugCodigo);

    let numeroEmpenhoInputHidden = $('<input class="form-control" type="hidden" name="empenho[numero_empenho]['+ugCodigo+'::'+empenhoValue+']" readonly>').val(empenhoValue);
    let empenhoInputHidden = $('<input class="form-control" type="hidden" name="empenho[numero_unidade]['+ugCodigo+'::'+empenhoValue+']" readonly>').val(unidade_empenho_id);
    let empenhoInputFornecedorHidden = $('<input class="form-control" type="hidden" name="empenho[numero_fornecedor]['+ugCodigo+'::'+empenhoValue+']" readonly>').val(fornecedor_id);
    let isFieldFromMinuta = $('<input class="form-control" type="hidden" name="empenho[from_minuta]['+ugCodigo+'::'+empenhoValue+']" readonly>').val(isMinuta);

    let deleteButton = $('<button class="btn btn-danger btn-sm" type="button"><i class="fa fa-trash"></i></button>').click(function () {
        $(this).closest('tr').remove();
        saveEmpenhosToLocalStorage();
    });

    empenhoRow.append($('<td></td>').append(empenhoInput));
    empenhoRow.append($('<td></td>').append(unidadeInput));
    empenhoRow.append($('<td></td>').append(unidadeMinutaInput))
        .append(empenhoInputHidden)
        .append(numeroEmpenhoInputHidden)
        .append(empenhoInputFornecedorHidden)
        .append(isFieldFromMinuta);

    // Adiciona o botão de delete se não for minuta
    if (!isMinuta) {
        empenhoRow.append($('<td></td>').append(deleteButton));
    }

    $('#empenho-body').append(empenhoRow);
    saveEmpenhosToLocalStorage();
}

// Função para salvar os empenhos no localStorage
function saveEmpenhosToLocalStorage() {
    let empenhos = [];
    let valor = null;
    let ugCodigo = null;
    $('#empenho-body tr').each(function () {
        valor = $(this).find('input[name="empenho_list[]"]').val();
        ugCodigo = $(this).find('input[name="ugcodigo_aba_empenho[]"]').val();
        let empenho = {
            valor: valor,
            unidadeMinuta: $(this).find('input[name="ugemitente_aba_empenho[]"]').val(),
            ugemitente: $(this).find('input[name="ugemitente_aba_empenho[]"]').val(),
            unidade: {
                codigo: $(this).find('input[name="ugcodigo_aba_empenho[]"]').val(),
                id: $(this).find('input[name="empenho[numero_unidade][' + ugCodigo +'::'+ valor + ']"]').val(),
            },
            codigo: $(this).find('input[name="ugcodigo_aba_empenho[]"]').val(),
            fornecedor_id: $(this).find('input[name="empenho[numero_fornecedor]['+ ugCodigo + '::' + valor + ']"]').val(),
            isFromMinuta: $(this).find('input[name="empenho[from_minuta][' + ugCodigo +'::'+ valor + ']"]').val()
        };
        empenhos.push(empenho);
    });
    localStorage.setItem('empenhos', JSON.stringify(empenhos));
}

function clearEmpenhosFromLocalStorage() {
    localStorage.removeItem('empenhos');
}

// Método para recuperar os empenhos do contrato e carregar no onload do instrumento inicial
function getEmpenhosOnload() {
    $.ajax({
        url: buscarContratoEmpenho,
        type: 'POST',
        data: {
            contrato_id: getContratoIdFromUrl()
        },
        success: function (response) {
            response.empenhos.forEach(function (empenho) {
                let isFromMinuta = false;
                let valorEmpenho;

                if (empenho.minuta_id) {
                    isFromMinuta = true;
                    valorEmpenho = [empenho.minuta_id]; // Quando for minuta, valorEmpenho precisa ser um array
                } else {
                    valorEmpenho = empenho.numero;
                }

                buscaEmpenhoAjax(
                    valorEmpenho,
                    empenho.fornecedor_id,
                    empenho.fornecedor_id,
                    empenho.unidade_id,
                    $('input[name="search_empenho_field"]'),
                    isFromMinuta
                );
            });
        },
        error: function (xhr, status, error) {
            console.error('Erro ao buscar empenhos:', error);
        }
    });
}
function getContratoIdFromUrl() {
    var url = window.location.pathname;
    var parts = url.split('/');

    return parts[3];
}

function initAbaEmpenhos(){
    $('#select2_ajax_multiple_unidadesdescentralizadas, #select2_ajax_multiple_fornecedor_empenho').on('select2:select select2:unselect', function() {
        setTimeout(function() {
            $('#select2_ajax_multiple_unidadesdescentralizadas, #select2_ajax_multiple_fornecedor_empenho').next('.select2-container')
                .find('li.select2-selection__choice')
                .find('span.select2-selection__choice__remove').remove();
        }, 1); // Pequeno atraso de 1ms para garantir que o Select2 tenha atualizado o DOM
    });

    // storage para trazer carregado os old values da aba empenhos
    let empenhos = JSON.parse(localStorage.getItem('empenhos') || '[]');
    empenhos.forEach(function(empenho) {
        let isFromMinuta = (empenho.isFromMinuta === 'true');
        addEmpenho(empenho.valor, empenho, empenho.unidade.id, $('input[name="search_empenho_field"]'), empenho.fornecedor_id, isFromMinuta); // Função já existente
    });


    $('#select2_ajax_multiple_unidadesdescentralizadas').removeAttr('multiple');
    $('#select2_ajax_multiple_fornecedor_empenho').removeAttr('multiple');
    $('#select2_ajax_multiple_minutasempenho').change(function() {
        if($(this).val().length > 0){
            let closest = $('.form-group').find('input[name="search_empenho_field"]');
            let fornecedor_id = $('#select2_ajax_fornecedor_id').val();
            let unidade_empenho_id = null;
            let minutaEmpenhoId = $(this).val();

            $('.listagem-empenhos-minuta').remove();
            buscaEmpenhoAjax(minutaEmpenhoId, fornecedor_id, null, unidade_empenho_id, closest, true);
        }else{
            $('.listagem-empenhos-minuta').remove();
        }
    });
}
