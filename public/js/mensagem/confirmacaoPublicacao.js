    $(document).on('click', "#btn-submit-itens-contrato, #btn-submit-salvar_elaboracao", function () {


    arrNumItemCompra = [];
    $('input[name="numero_item_compra[]"]').each(function(index,option){
        arrNumItemCompra[index] = option.value;
    });

    arrCodigoSiasg = [];
    $('input[name="codigo_siasg[]"]').each(function(index,option){
        arrCodigoSiasg[index] = option.value;
    });

    arrItens = [];
    var ret = false;
    arrNumItemCompra.forEach((element, index) => {
        if (arrItens.includes(element+arrCodigoSiasg[index])) {
            ret = true;
            return;
        } else {
            arrItens.push(element+arrCodigoSiasg[index]);
        }
    });

    if (ret == true) {
        Swal.fire(
            'Verifique os itens do contrato',
            'Não é possível adicionar o mesmo número item da compra que possuam o mesmo código siasg no mesmo termo.',
            'warning'
          );
        return;
    }



    let itemSelecionado = $('input[name="numero_item_compra[]"]').val();

    if(itemSelecionado == undefined && rescisao() == false) {
        Swal.fire(
            'Cadastro incompleto!',
            'Para salvar um contrato, insira ao menos um item!',
            'warning'
          )

        return ;
    }

    //add status loading no botao
    $(this).button('loading');
    //adiciona status disable no botao dropdown auxiliar
    $('#btn-submit-dropdrown-toggle').attr('disabled', 'disabled');
    //verifica se tipo contrato vem do hidden
    var tipo_contrato = $('#tipo_contrato').val();
    //verifica se tipo contrato vem da combo de tipo contrato
    if ($("[name='tipo_id'] option").filter(':selected').text() != "") {
        var selected = $("[name='tipo_id']").find(':selected'),
            array_selected = [];
        selected.each(function (index, option) {
            array_selected[index] = option.text;
        })
        if (array_selected[0] !== 'Selecione...') {
            tipo_contrato = array_selected[0];
        }
    }
    if (tipo_contrato === 'Outros' || tipo_contrato === 'Empenho' || tipo_contrato === '') {
        if (!rescisao()) {
            configurarFormParaSubmit();
        }
        this.closest('form').submit();
    } else {
        if (!rescisao()) {
            configurarFormParaSubmit();
        }
        this.closest('form').submit();

        //----ATENÇÃO---------------------------------------------------------------------------------------------------
        // Funcionalidade removida temporariamente enquanto a publicação não volta a ser automática
        // Ao submeter o forumlário exibia uma pop-up de confirmação e após a confirmação o formulário era submetido
        //--------------------------------------------------------------------------------------------------------------

        // Swal.fire({
        //     title: 'Ao prosseguir o instrumento será automaticamente enviado para publicação no Diário Oficial da União - DOU.  As publicações enviadas após às 18h ou enviadas para publicação em dia não útil serão processadas no dia útil subsequente.',
        //     showDenyButton: false,
        //     showCancelButton: true,
        //     confirmButtonText: `Sim`,
        // }).then((result) => {
        //     /* Read more about isConfirmed, isDenied below */
        //     if (result.isConfirmed) {
        //         if (!rescisao()) {
        //             configurarFormParaSubmit();
        //         }
        //         this.closest('form').submit();
        //     }
        // })
    }
    function rescisao (){
        $arrRoute = window.location.pathname.split('/');
        $rescisao = $arrRoute.indexOf("rescisao");
        if ($rescisao !== -1) {
            return true;
        }
        return false;
    }
});
