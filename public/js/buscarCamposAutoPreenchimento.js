// let dadosCamposEmpenho = null
// //busca a modalidade de acordo com a primeira minuta de empenho selecionada para popular os campos
// function buscarCamposAutoPreenchimento()
// {
//     var arrayMinutas = $("#select2_ajax_multiple_minutasempenho").val();

//     // var url = "{{route('buscar.campos.contrato.empenho',':id')}}";
//     let url = $("#UrlBuscarCamposContratoEmpenho").val()
//     url = url.replace(':id', arrayMinutas[0]);
//     let dadosCampos = null
//     axios.request(url)
//         .then(response => {
//             dadosCampos = response.data;

//             if(dadosCampos){
//                 // altera campo unidade de compra
//                 $('select[name=unidadecompra_id]').append(`<option value="${dadosCampos.unidade_id}">${dadosCampos.codigo} -  ${dadosCampos.nomeresumido}</option>`);
//                 $('select[name=unidadecompra_id]').val(dadosCampos.unidade_id).change();

//                 //altera campo de modalidade da licitacao
//                 $("select[name=modalidade_id]").val(dadosCampos.modalidade_id).change();
//                 $('#select2-select2_ajax_unidadecompra_id-container .select2-selection__placeholder').remove();

//                 // altera campo de amparos legais
//                 $('#select2_ajax_multiple_amparoslegais option').remove();
//                 $('#select2_ajax_multiple_amparoslegais').append(`<option value="${dadosCampos.amparo_legal_id}">${dadosCampos.ato_normativo} - Artigo: ${dadosCampos.artigo}</option>`);
//                 $('#select2_ajax_multiple_amparoslegais').val(dadosCampos.amparo_legal_id).change();

//                 // altera campo de numero/ano da licitacao
//                 $("#licitacao_numero").val(dadosCampos.compra_numero_ano);

//                 let itemSelecionado = $("select[name=tipo_id] option:selected").text();
//                 console.log(itemSelecionado)
//                 // dadosCamposEmpenho = dadosCampos
//                 preencherInformacoesMinutaEmpenhoSelecionada(itemSelecionado,dadosCampos)
//             }
//         })
//         .catch(error => {
//             alert(error);
//         })
//         .finally(function(){
//             dadosCamposEmpenho = dadosCampos
//         })

//     event.preventDefault()
    
// }

$('select[name=tipo_id]').on('select2:select', function (e) {
    let data = e.params.data;
    preencherInformacoesMinutaEmpenhoSelecionada(data.text,dadosCamposEmpenho)
})

function preencherInformacoesMinutaEmpenhoSelecionada(itemSelecionado,dadosCamposEmpenho) {
    if (itemSelecionado == 'Empenho') {
        $("input[name=numero]").val(dadosCamposEmpenho.mensagem_siafi);
        $("input[name=vigencia_inicio]").val(dadosCamposEmpenho.data_emissao);
        $("input[name=vigencia_fim]").val(dadosCamposEmpenho.fim_vigencia);
        $("input[name=processo]").val(dadosCamposEmpenho.processo);

    } else {
        limparCamposMinutaEmpenhoAlterada()
    }
}

montarInformacoesDadosCamposEmpenho()
function montarInformacoesDadosCamposEmpenho() {
    dadosCamposEmpenho = new Array()
    dadosCamposEmpenho['mensagem_siafi']  = $("input[name=numero]").val()
    dadosCamposEmpenho['data_emissao']  = $("input[name=vigencia_inicio]").val()
    dadosCamposEmpenho['fim_vigencia']  = $("input[name=vigencia_fim]").val()
    dadosCamposEmpenho['processo']  = $("input[name=processo]").val()
}

function limparCamposMinutaEmpenhoAlterada(removerDadosContrato = false ) {
    
    if(removerDadosContrato) {
        $('select[name=unidadecompra_id]').val('').change()
        $('select[name=modalidade_id]').val('').change()
        $('#select2_ajax_multiple_amparoslegais').val('').change()
        $('#licitacao_numero').val('')
    }

    $("input[name=numero]").val('')
    $("input[name=vigencia_inicio]").val('')
    $("input[name=vigencia_fim]").val('')
    $("input[name=processo]").val('')
}