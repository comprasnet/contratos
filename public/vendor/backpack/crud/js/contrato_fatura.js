"use strict"

function decorarInputNFeValido( chaveNfeIconElement, inputGrupAddonElement, inputChaveNfeElement ) {
    chaveNfeIconElement.css('display', 'block');
    chaveNfeIconElement.removeClass('glyphicon glyphicon-remove');
    chaveNfeIconElement.addClass('glyphicon glyphicon-ok');
    chaveNfeIconElement.css('color', '#00CC00');
    inputGrupAddonElement.css('border-color', '#00CC00');
    inputChaveNfeElement.css('border-right', 'none');
    inputChaveNfeElement.css('border-color', '#00CC00');
}

function decorarInputNFeInValido( chaveNfeIconElement, inputGrupAddonElement, inputChaveNfeElement ) {
    chaveNfeIconElement.css('display', 'block');
    chaveNfeIconElement.removeClass('glyphicon glyphicon-ok');
    chaveNfeIconElement.addClass('glyphicon glyphicon-remove');
    chaveNfeIconElement.css('color', 'red');
    inputGrupAddonElement.css('border-color', 'red');
    inputChaveNfeElement.css('border-right', 'none');
    inputChaveNfeElement.css('border-color', 'red');
}

function valivarChaveNFe(element, checkElementName) {

    //atribui os elementos as variaveis
    var chaveNfeIconElement = $('#' + checkElementName),
        inputGrupAddonElement = chaveNfeIconElement.parent(),
        inputChaveNfeElement = $('#' + element.id);

    if (element.value.length === 44) {

        var routeName = window.location.origin + '/api/fatura/chave-nfe/' + element.value;

        $.ajax({
            url: routeName,
            type: 'GET',
            beforeSend: function(){
                //add o readonly no input
                inputChaveNfeElement.attr('disabled', 'disabled');
            },
            success: function (result) {
                var retorno = $.parseJSON(result)
                //caso a chave NFe seja valida decora o input como sucesso
                if (retorno === true) {
                    decorarInputNFeValido( chaveNfeIconElement, inputGrupAddonElement, inputChaveNfeElement );
                } else {
                    //caso a chave NFe seja invalida decora o input como erro
                    decorarInputNFeInValido( chaveNfeIconElement, inputGrupAddonElement, inputChaveNfeElement );
                }
            },
            error: function (result) {
                new PNotify({
                    title: 'Atenção',
                    text: 'Erro ao tentar validar chave NFe',
                    type: "alert"
                });
            },
            complete: function(){
                //remove o readonly no input
                inputChaveNfeElement.removeAttr('disabled');
            }
        });
    }else{
        decorarInputNFeInValido( chaveNfeIconElement, inputGrupAddonElement, inputChaveNfeElement );
    }
}

/**
 * chamado no onblur do elemento input da chave NFe caso o input esteja vazio decora com o padrao do form
 * @param element
 * @param checkElementName
 */
function estilizarComDefault(element, checkElementName) {
    var chaveNfeIconElement = $('#' + checkElementName),
        inputGrupAddonElement = chaveNfeIconElement.parent(),
        inputChaveNfeElement = $('#' + element.id);
    if (element.value === '') {
        //estiliza os elementos do input para default
        chaveNfeIconElement.css('display', 'none');
        inputGrupAddonElement.css('border-color', '#d2d6de');
        inputChaveNfeElement.css('border-right', 'none');
        inputChaveNfeElement.css('border-color', '#d2d6de');
    }
}

function updateDataLimite(element, dias) {
    var val = $(element).val();

    if(val) {
        $.get('/admin/dias-uteis?data=' +  val + '&dias=' + dias, function(data) {
            $('input[name=vencimento]').val(data.slice(-1));
        });
    }

}
