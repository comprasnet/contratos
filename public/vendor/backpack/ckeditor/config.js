/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	// Plugin options
	CKEDITOR.config.max_length = 0;
	config.wordcount = {

		// Whether or not you Show Remaining Count (if Maximum Word/Char/Paragraphs Count is set)
		showRemaining: false,
		
		// Whether or not you want to show the Paragraphs Count
		showParagraphs: false,
	
		// Whether or not you want to show the Word Count
		showWordCount: false,
	
		// Whether or not you want to show the Char Count
		showCharCount: true,
		
		// Whether or not you want to Count Bytes as Characters (needed for Multibyte languages such as Korean and Chinese)
		countBytesAsChars: false,
	
		// Whether or not you want to count Spaces as Chars
		countSpacesAsChars: false,
	
		// Whether or not to include Html chars in the Char Count
		countHTML: false,
		
		// Whether or not to include Line Breaks in the Char Count
		countLineBreaks: false,
		
		// Whether or not to prevent entering new Content when limit is reached.
		hardLimit: true,
		
		// Whether or not to to Warn only When limit is reached. Otherwise content above the limit will be deleted on paste or entering
		warnOnLimitOnly: false,
	
		// Maximum allowed Word Count, -1 is default for unlimited
		maxWordCount: -1,
	
		// Maximum allowed Char Count, -1 is default for unlimited
		maxCharCount: -1,
		
		// Maximum allowed Paragraphs Count, -1 is default for unlimited
		maxParagraphs: -1,
	
		// How long to show the 'paste' warning, 0 is default for not auto-closing the notification
		pasteWarningDuration: 0,
		
	
		// Add filter to add or remove element before counting (see CKEDITOR.htmlParser.filter), Default value : null (no filter)
		filter: new CKEDITOR.htmlParser.filter({
			elements: {
				div: function( element ) {
					if(element.attributes.class == 'mediaembed') {
						return false;
					}
				}
			}
		})
	};

	// Toolbar configuration generated automatically by the editor based on config.toolbarGroups.
	config.toolbarGroups = [
		{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
		{ name: 'forms', groups: [ 'forms' ] },
		// '/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
		{ name: 'links', groups: [ 'links' ] },
		{ name: 'insert', groups: [ 'insert' ] },
		// '/',
		{ name: 'styles', groups: [ 'styles' ] },
		{ name: 'colors', groups: [ 'colors' ] },
		{ name: 'tools', groups: [ 'tools' ] },
		{ name: 'others', groups: [ 'others' ] },
		{ name: 'about', groups: [ 'about' ] }
	];

	config.removeButtons = 'Source,Save,NewPage,Templates,Scayt,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Language,Anchor,Smiley,Iframe,Youtube,ShowBlocks,About,Flash,SelectAll,Find,PasteFromWord';

	
	config.extraPlugins = 'autocomplete';
	config.extraPlugins = 'textmatch';
	config.extraPlugins = 'placeholder';
	config.extraPlugins = 'textwatcher';
	config.extraPlugins = 'entities';
	config.disableNativeSpellChecker = false;
	config.extraPlugins = 'pastefromword';
	config.extraPlugins = 'wordcount,notification';
	config.extraPlugins = 'maxlength';
	config.extraPlugins = 'confighelper';
	config.extraPlugins = 'filetools';

	// config.height = 700;        // 500 pixels high.

	config.startupFocus = false;
	config.AutoFocus = false
};


/*
 * CKEditor Maxlength Plugin
 *
 * Adds a character count to the path toolbar of a CKEditor instance
 *
 * @package maxlength
 * @author Sage McEnery
 * @version 1
 * @copyright divgo 2012
 * based on Word Count plugin from : http://www.n7studios.co.uk/2010/03/01/ckeditor-word-count-plugin/
 */
(function () {
	

    CKEDITOR.plugins.maxlength = {
    };

    let plugin = CKEDITOR.plugins.maxlength;

    function doCharacterCount(evt) {
        let editor = evt.editor;
        if ($('span#cke_maxlength_' + editor.name).length > 0) { // Check element exists
            setTimeout(function () {
                let charCount = editor.getData().length -8;

				if(charCount === -8) {
					charCount = 0
				}
                let wcTarget = $('span#cke_maxlength_' + editor.name);
                if (editor.config.max_length > 0) {
                    wcTarget.html("Quantidade de caracteres: " + charCount + "/" + editor.config.max_length);
                } else {
                    wcTarget.html("Quantidade de caracteres: " + charCount);
                };

                if (charCount > editor.config.max_length && editor.config.max_length !== -1) {
                    wcTarget.css('color', 'red');
                    editor.execCommand('undo');
                } else if (charCount == editor.config.max_length && editor.config.max_length !== -1) {
                    editor.fire('saveSnapshot');
                    wcTarget.css('color', 'red');
                } else {
                    wcTarget.css('color', 'black');
                };
            }, 100);
        }
    }

    /**
    * Adds the plugin to CKEditor
    */
    CKEDITOR.plugins.add('maxlength', {
        init: function (editor) {

            let maxLengthAttr = $("#" + editor.name).attr("maxlength"),
                dataMaxLengthAttr = $("#" + editor.name).attr("data-maxlen");

            if (typeof maxLengthAttr !== typeof undefined && maxLengthAttr !== false) {
                editor.config.max_length = maxLengthAttr;
            } else if (typeof dataMaxLengthAttr !== typeof undefined && dataMaxLengthAttr !== false) {
                editor.config.max_length = dataMaxLengthAttr;
            } else {
                editor.config.max_length = -1;
            };

            setTimeout(function () {
                if (editor.config.max_length > 0) {
                    $(".cke_bottom").append("<span id='cke_maxlength_" + editor.name + "'>Quantidade de caracteres: " + editor.getData().length + '/' + editor.config.max_length + "</span>");
                } else {
                    $(".cke_bottom").append("<span id='cke_maxlength_" + editor.name + "'>Quantidade de caracteres: " + editor.getData().length + '/' + editor.config.max_length + "</span>");
                }
            }, 1000);

            editor.on('key', doCharacterCount);
        }
    });
})();

