![Logo](public/img/logo_mail.png)
## CONTRATOS.GOV.BR - Gestão Administrativa para Órgãos Públicos
O [Contratos](https://contratos.comprasnet.gov.br/login) é uma ferramenta do governo federal que automatiza os processos de gestão contratual e conecta servidores públicos responsáveis pela execução e fiscalização de contratos,  tornando informações disponíveis a qualquer momento e melhorando as condições de gestão e relacionamento com fornecedores.

## Tecnologia Utilizada

* PHP 7.3
* Laravel 5.7
* Backpack  1.0.4
* Composer 1.10.23
* Postgres 12

A ferramenta é desenvolvida em PHP, utilizando  Framework Laravel versão 5.7.*

Essa ferramenta é Gratuita, e cada Instituição Pública poderá utilizá-la sem limites.
 
Caso o órgão queira implementar nova funcionalidade, pedimos que disponibilize esta para que outras instituições possa utilizar.

## Licença

A licença dessa ferramenta é [“Licença Pública Geral GNU (GPLv3)”](https://www.gnu.org/licenses/gpl-3.0.pt-br.html). Pedimos que toda implementação seja disponibilizada para a comunidade.

## Versões, Requisitos, Instalação e Configuração


[Ambiente de Desenvolvimento](https://gitlab.com/comprasnet/contratos/-/wikis/Ambiente-de-Desenvolvimento)

## Ajuda

Conheça nossa [Wiki](https://gitlab.com/comprasnet/contratos/-/wikis/home)!

## Atualização e Versões

As atualizações podem ser acompanhadas pelas [Versões](https://gitlab.com/comprasnet/contratos/-/releases)
