## Origem
<!-- Por onde o problema foi reportado / percebido-->

### Passos para reproduzir o problema
<!-- caminho executado, link, dados informados, etc -->

### Qual o resultado obtido
<!-- comportamento atual errado -->

### Qual o resultado esperado
<!-- comportamento desejado correto -->

### Contexto

### Solução alternativa

### Solução sugerida

Não há.

<!--NÃO REMOVER AS LINHAS A SEGUIR-->
/label ~"req::Em análise"
/label ~"tipo::Bug"
