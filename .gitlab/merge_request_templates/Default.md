<!--Mantenha o título do MR sugerido pelo gitlab-->

## Descrição
<!-- Descreva as alterações. Ex:
- Criada função para busca de relatórios
- Reinderização da tela de contratos otimizada
- Removido parâmetro não utilizado para o retorno de minutas-->

- 

## Fluxo de Testes
<!-- Descreva os passos necessários para testar o MR, preferencialmente coloque prints de como o sistema deve se comportar. Ex:
1. Acessar menu>> Contratos >> Minutas >> botão "Criar contrato"
2. Preencher "Tipo do Contrato com opção 5
3. Deverá abrir uma tela com o contrato do tipo 5 (colocar print)-->

1. 

## Implantação
<!--Passos necessário além da troca de código fonte para o funcionamento da solução -->

<!-- NÃO É NECESSÁRIO INDICAR OS SEGUINTES COMANDOS, uma vez que ao realizar o deploy em qualquer ambiente são executados em todas as VMs (4 de produção, 4 de Homologação, 2 de Treinamento):
   - php artisan migrate *
   - php artisan l5-swagger:generate
   - php artisan storage:link
   - php artisan optimize:clear
   - systemctl stop supervisord * 
   - systemctl start supervisord * 

   * Executado somente na VM que tem o supervisor
   -->

### Variável(eis) de ambiente
<!--Nome das variáveis e valores a serem incluídas no arquivo .env (NÃO COLOCAR SENHAS, PASSAR PARA EQUIPE DO DEPLOY)
Exemplo:
```
URP_API=api.gov.br
USR_API=usr_contratos
PWD_API=********
```
-->

* Não se aplica

### Configuração(ões) do Supervisor
<!--Parâmetros para adicionar na configuração do supervisor para a execução jobs em filas 
Exemplo:
```
[program:scNUM_SEQUENCIAL]
process_name=%(program_name)s_%(process_num)02d
command=php PATH/contratos/artisan queue:work --queue=NOME_QUEUE --timeout=SEGUNDOS --tries=1
autostart=true
autorestart=true
user=apache
numprocs=NUM_PROCESSOS
redirect_stderr=true
stdout_logfile=PATH/contratos/storage/logs/mNOMEQUEUE.worker.log
```
-->

* Não se aplica

### Comando(s)
<!--Descreva os passos necessários para a implantação do MR em produção. Ex:
1. Executar comando
> php artisan RodarProcesso (leva cerca de 8 minutos para a conclusão)
1. Executar SQL
> [atualizaregistros.SQL] (leva cerca de 15 minutos para a conclusão, cerca de 1500 linhas afetadas)-->

* Não se aplica

### Instrução(ões)
<!--Descreva os passos necessários para a implantação do MR em produção. Ex:
1. Cadastrar modelo de documento em menu >> documentos >> botão "Novo modelo" >>:
   * Título: Contrato novo
   * Tipo: 5
   * Manter oculto: Não
-->

* Não se aplica

<!--NÃO REMOVER AS LINHAS A SEGUIR-->
---
_Não remover as próximas linhas_

/assign me
/label ~status::Doing
