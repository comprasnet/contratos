<?php

namespace Tests\Unit\PNCP;

use App\Http\Controllers\Api\PNCP\ContratoControllerPNCP;
use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\EnviaPncpTrait;
use App\Models\Contratohistorico;
use App\Models\EnviaDadosPncp;
use App\Repositories\CodigoItemRepository;
use GuzzleHttp\Psr7\Response;
use Tests\TestCase;

class ManipulacaoErroTest extends TestCase
{
    use BuscaCodigoItens;
    use EnviaPncpTrait;

    public function testErroContratoJaExistente()
    {
        //Recupera um contrato já enviao
        $codigo = (new CodigoItemRepository())
            ->getByDescresAndDescription('SUCESSO', 'Sucesso')
            ->id;
        $contratoHistorico = Contratohistorico::query()->select('contratohistorico.*')
            ->join('envia_dados_pncp as e', function ($join) {
                $join->on('e.pncpable_id', '=', 'contratohistorico.id')
                    ->where('e.pncpable_type', '=', 'App\Models\Contratohistorico');
            })
            ->whereNotNull('e.link_pncp')
            ->whereNotNull('contratohistorico.modalidade_id')
            ->where('e.situacao', $codigo)
            ->orderByDesc('e.updated_at')
            ->select('contratohistorico.*')
            ->first();
        //cria o controller para enviar novamente
        $contratoController = $this->app->make(ContratoControllerPNCP::class);

        $sispp = $this->buscaCNPJContratanteNovo(
            $contratoHistorico->modalidade->descres,
            $contratoHistorico->licitacao_numero,
            $contratoHistorico->unidadecompra->codigo,
            $contratoHistorico->unidade->codigo
        );

        $cnpjOrgao = $this->cnpjOrgao($contratoHistorico);
        /* @var Response $contratoEnviado */
        $contratoEnviado = $contratoController->inserirContrato($contratoHistorico, $sispp->cnpjOrgao, $sispp->idUnico, $cnpjOrgao);

        if ($contratoEnviado->getStatusCode() == 200) {
            $this->assertTrue($contratoEnviado->getStatusCode() == 200);
            $this->assertTrue(isset($contratoEnviado->JSONEnviado));
            $dataSended = json_decode($contratoEnviado->JSONEnviado);
            $this->assertEquals($dataSended->numeroContratoEmpenho . '/' . $dataSended->anoCompra, $contratoHistorico->numero);
        } else {
            $this->assertTrue($contratoEnviado->getStatusCode() == 422);
        }
    }
}
