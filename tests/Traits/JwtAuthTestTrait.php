<?php

namespace Tests\Traits;

use App\Models\BackpackUser;
use Tymon\JWTAuth\Facades\JWTAuth;
use Spatie\Permission\Models\Role;

trait JwtAuthTestTrait
{
    /**
     * Gera um token JWT para o usuário fornecido.
     *
     * @param \App\Models\BackpackUser $usuario
     * @return string
     */
    protected function geraJwtTokenParaUsuario(): string
    {
        $role = Role::where('name', 'Administrador')->first();
        $usuario = factory(BackpackUser::class)->create();
        $usuario->assignRole($role);
        return JWTAuth::fromUser($usuario);
    }
}
