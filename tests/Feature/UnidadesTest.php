<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Http\Requests\UnidadeRequest;
use App\Http\Controllers\Admin\UnidadeCrudController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use DB;
use App\Models\BackpackUser;
use App\Models\Orgao;
use App\Models\Estado;
use App\Models\Municipio;
use Spatie\Permission\Models\Role;

class UnidadesTest extends TestCase
{

    protected function createAuthenticatedUser()
    {
        $role = Role::where('name', 'Administrador')->first();
        $user = BackpackUser::inRandomOrder()->first();
        $user->assignRole($role);

        $this->token = JWTAuth::fromUser($user);
        JWTAuth::setToken($this->token);
        Auth::login($user);
    }

    protected function destroyAuthenticatedUser()
    {
        Auth::logout();
    }

    public function testCreateUnidade()
    {

        $arrEsfera = ['Estadual' => 'Estadual', 'Federal' => 'Federal', 'Municipal' => 'Municipal'];

        $arrTipo = ['C' => 'Controle', 'E' => 'Executora', 'S' => 'Setorial Contábil'];

        $arrPoder = ['Executivo' => 'Executivo',
        'Judiciário' => 'Judiciário',
        'Legislativo' => 'Legislativo'];

        //possui codigoitens
        $arrTipoAdm = ['ADMINISTRAÇÃO DIRETA' => 'ADMINISTRAÇÃO DIRETA',
        'ADMINISTRAÇÃO DIRETA ESTADUAL' => 'ADMINISTRAÇÃO DIRETA ESTADUAL',
        'ADMINISTRAÇÃO DIRETA MUNICIPAL' => 'ADMINISTRAÇÃO DIRETA MUNICIPAL',
        'AUTARQUIA' => 'AUTARQUIA',
        'ECONOMIA MISTA' => 'ECONOMIA MISTA',
        'EMPRESA PÚBLICA COM. E FIN.' => 'EMPRESA PÚBLICA COM. E FIN.',
        'FUNDAÇÃO' => 'FUNDAÇÃO',
        'FUNDOS' => 'FUNDOS'];

        $orgao = Orgao::inRandomOrder()->first()->id;
        $this->assertIsInt($orgao, 'Não foi possível recupear um órgão.');

        $municipio = Municipio::inRandomOrder()->first()->id;
        $this->assertIsInt($municipio, 'Não foi possível recupear um município.');

        DB::beginTransaction();

        $this->createAuthenticatedUser();

        $request = UnidadeRequest::create('/admin/unidade','POST', [
            'orgao_id' => $orgao,
            'codigo' => 999999,
            'gestao' => str_pad(rand(0, 99999), 5, '0', STR_PAD_LEFT),
            'codigosiasg' => str_pad(rand(0, 999999), 6, '0', STR_PAD_LEFT),
            'nome' => 'NOME DE TESTE',
            'nomeresumido' => 'NOME TESTE RESUMIDO',
            'telefone' => '(11) 1111-1111',
            'tipo' => array_rand($arrTipo, 1),
            'situacao' => (bool)(rand(1,2)%2),
            'sisg' => (bool)(rand(1,2)%2),
            'uf' => 'DF',
            'municipio_id' => $municipio,
            'esfera' => array_rand($arrEsfera, 1),
            'poder' => array_rand($arrPoder, 1),
            'tipo_adm' => array_rand($arrTipoAdm, 1),
            'aderiu_siasg' => (bool)(rand(1,2)%2),
            'utiliza_siafi' => (bool)(rand(1,2)%2),
            'codigo_siorg' => str_pad(rand(0, 9999999999), 10, '0', STR_PAD_LEFT),
            'sigilo' => (bool)(rand(1,2)%2),
            'cnpj' => NULL,
            'utiliza_custos' => (bool)(rand(1,2)%2),
            'codigosiafi' => 999999,
            'exclusivo_gestao_atas' => (bool)(rand(1,2)%2),
        ]);

        $this->assertTrue($request->authorize(), 'Usuário não autenticado.');

        $validator = Validator::make($request->all(), $request->rules());

        $msg = [];
        foreach ($validator->errors()->messages() as $data)
            $msg[] = $data[0];

        $this->assertCount(0, $validator->errors()->messages(), implode($msg, ' / '));

        $response = $this->post('/admin/unidade', $request->all());

        $this->destroyAuthenticatedUser();

        $this->assertEquals(302, $response->getStatusCode());

        DB::rollback();
    }
}
