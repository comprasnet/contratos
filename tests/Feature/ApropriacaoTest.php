<?php

namespace Tests\Feature;

use App\Models\ApropriacaoContratoFaturas;
use Illuminate\Support\Facades\Log;
use Tests\TestCase;

class ApropriacaoTest extends TestCase
{
    /**
     * Testa a apropriação usando o token JWT.
     *
     * @return void
     */

    private $contratoApropriacao;

    protected function setUp()
    {
        parent::setUp();
        $this->contratoApropriacao = ApropriacaoContratoFaturas::whereHas('apropriacao')
            ->with('fatura:id,contrato_id')
            ->first();
    }

    public function testApropriacaoComJwtToken()
    {
        $response = $this->get("/api/v1/contrato/apropriacao/consultar/{$this->contratoApropriacao->fatura->contrato_id}", ['Authorization' => "Bearer $this->token"]);
        $response->assertStatus(200);
    }

    public function testApropriacaoSemContratoId()
    {
        $response = $this->get('/api/v1/contrato/apropriacao/consultar', ['Authorization' => "Bearer $this->token"]);
        $response->assertStatus(404);
    }

    public function testApropriacaoSemToken()
    {
        $response = $this->get('/api/v1/contrato/apropriacao/consultar', []);
        $response->assertHeaderMissing('Authorization');
    }

    public function testRetornoJsonSemCampoSistemaOrigem()
    {
        $response = $this->get("/api/v1/contrato/apropriacao/consultar/{$this->contratoApropriacao->fatura->contrato_id}", ['Authorization' => "Bearer $this->token"]);

        $response->assertJsonMissing([
            'data' => [
                '*' => [
                    'sistema_origem'
                ]
            ]
        ]);
    }

    public function testRetornoJsonEsperado()
    {
        $response = $this->get("/api/v1/contrato/apropriacao/consultar/{$this->contratoApropriacao->fatura->contrato_id}", ['Authorization' => "Bearer $this->token"]);

        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id_contrato',
                    'id_apropriacao',
                    'situacao_apropriacao',
                    'emissao',
                    'sistema_origem',
                    'tipo_dh',
                    'numero_dh_siafi',
                    'ug_emitente',
                    'valor_documento',
                    'dados_empenho' => [
                        '*' => [
                            'id',
                            'numero',
                            'sub_elemento_item',
                            'valor_empenho',
                        ]
                    ]
                ]
            ]
        ]);
    }
}
