<?php

namespace Tests;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Tests\Traits\JwtAuthTestTrait;

abstract class TestCase extends BaseTestCase
{
    use DatabaseTransactions, JwtAuthTestTrait, CreatesApplication;

    public $token;

    protected function setUp()
    {
        parent::setUp();
        $this->token = $this->geraJwtTokenParaUsuario();
    }
}
