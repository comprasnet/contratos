#!/bin/bash
projeto="https://gitlab.com/comprasnet/contratos_v2.git"
bkp_banco=($(find ~/contratos/postgres_data_backups -type f \( -name "*.dump" -o -name "*.sql" \) -exec basename {} \;))
read -p "Usuário Backpack: " backpack_user
read -p "Senha backpack: " backpack_password

if docker version > /dev/null;
then
  docker network inspect backend >/dev/null 2>&1 || docker network create backend
else
  echo "Verifique se o Docker foi iniciado e tente novamente"
  exit
fi

printf "Clonando repositório e iniciando containers...\n"
git clone --quiet $projeto ~/contratos_v2
cp ~/contratos_v2/auth.json.example ~/contratos_v2/auth.json
sed -i "s/'usernameAuth'.*/\""$backpack_user\"",/" ~/contratos_v2/auth.json
sed -i "s/'passwordAuth'.*/\""$backpack_password\""/" ~/contratos_v2/auth.json
docker compose -f ~/contratos/docker-compose.yml up --detach
docker compose -f ~/contratos_v2/docker-compose.yml up --detach

printf "\nSelecione o dump do banco do sistema contratos\n"
PS3='->'
select opt_dump in "${bkp_banco[@]}" "Quit" ; do
  if (( REPLY == 1 + ${#bkp_banco[@]} )) ; then
    exit
  elif (( REPLY > 0 && REPLY <= ${#bkp_banco[@]} )) ; then
    break
  else
    echo "Opção inválida"
  fi
done
docker exec conta-postgres psql -U conta -d conta -c "CREATE ROLE owner_comprasctr_hom;"
docker exec conta-postgres psql -U conta -d conta -c "CREATE ROLE owner_comprasctr_pro;"
if [ "${opt_dump##*.}" == "dump" ]; then
  docker exec conta-postgres /bin/bash -c "pg_restore -Fc -j 8 $opt_dump -d conta -U conta"
elif [ "${opt_dump##*.}" == "sql" ]; then
  docker exec conta-postgres /bin/bash -c "psql -d conta -U conta < $opt_dump"
fi

docker exec contratosv1 /bin/bash -c "sh compose/php/dev.sh"
docker exec contratosv2 /bin/bash -c "sh compose/php/dev.sh"
