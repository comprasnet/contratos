#!/bin/sh
php -f /var/www/html/contratos/artisan optimize:clear
php -f /var/www/html/contratos/artisan l5-swagger:generate

git fetch --all

sudo service cron start
sudo service supervisor start

/usr/sbin/apache2ctl -D FOREGROUND
