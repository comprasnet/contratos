if [ ! -f ".env" ]; then
  echo 'Criando arquivo .env ...'
  cp .env.example .env
fi
echo 'Baixando dependencias e criando diretório vendor ...'
composer install
echo 'Gerando chave ...'
php artisan key:generate
echo 'Executando migrate ...'
php artisan migrate
echo 'Estabelecendo link simbólico...'
php artisan storage:link
echo 'Criando usuário Admin ...'
php artisan db:seed --class=UserSeeder
echo 'USUÁRIO: 111.111.111-11 SENHA: 123456'
echo 'Iniciando aplicação ...'
php artisan serve