#!/bin/sh

chmod -R ugo+rw /var/www/html/contratos/storage
chmod -R ugo+rw /var/www/html/contratos/bootstrap/cache
chown -R www-data:www-data /var/www/html/contratos/
# liberapermissão para qualquer usuárioa poder ler e esecrever logs
cd /var/www/html/contratos/storage/logs && umask u=rwx,g=rwx,o=rwx

rm -rf ./storage/app/public

php -f /var/www/html/contratos/artisan migrate --force
php -f /var/www/html/contratos/artisan cache:clear
php -f /var/www/html/contratos/artisan config:cache
php -f /var/www/html/contratos/artisan l5-swagger:generate
php -f /var/www/html/contratos/artisan storage:link

service cron start

service supervisor start

/usr/sbin/apache2ctl -D FOREGROUND
