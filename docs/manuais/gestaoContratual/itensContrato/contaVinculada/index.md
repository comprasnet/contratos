[TOC]

# Contratos - Itens Contrato - Conta Vinculada

## 1. Apresentação
Com foco na busca pela eficiência da gestão e na melhoria do desempenho das instituições públicas, o Ministério da Economia, em parceria com a Advocacia-Geral da União, oferta aos órgãos e entidades da administração pública direta, autárquica e fundacional, incluindo as empresas estatais, o Contratos.gov.br. A ferramenta faz parte das medidas de eficiência organizacional para o aprimoramento da administração pública federal direta, autárquica e fundacional estabelecidas pelo Decreto nº 9.739, de 28 de março de 2019 (Art. 6º, IX).

O Contratos.gov.br é uma ferramenta do governo federal que automatiza os processos de gestão contratual e conecta servidores públicos responsáveis pela execução e fiscalização de contratos, tornando informações disponíveis a qualquer momento e melhorando as condições de gestão e relacionamento com fornecedores.

Quem pode utilizar: 

-	Órgãos e entidades da administração pública federal direta, autárquica e fundacional, bem como as empresas estatais; e 
-	Demais órgãos e entidades de outros poderes ou das esferas estadual e municipal.

Quanto custa:
-	O sistema é ofertado gratuitamente aos órgãos e entidades integrantes do Sistema Integrado de Serviços Gerais (SISG), custeado pelo Ministério da Economia. 

Modelo de oferta do sistema:
-	Disponibilizado de forma centralizada, evitando custos com hospedagem e manutenção de sistemas de TIC hospedagem e manutenção de sistemas de TIC. 

Vantagens da plataforma:

-	Reduz os problemas relacionados às rotinas de trabalho;
-	Pleno controle das informações do que acontece no âmbito dos contratos de um órgão ou entidade;
-	Promove a eficiência na gestão contratual;
-	Proporciona informações para apoiar as decisões governamentais de alocação mais eficiente de recursos;
-	Infraestrutura centralizada, sem custos para órgãos e entidades do Poder Executivo federal;
-	Maior transparência das informações dos contratos celebrados por toda a administração pública, permitindo a padronização de rotinas e procedimentos.

A ferramenta viabiliza:
-	Controle de documentos diversos;
-	Controle sobre os prazos de vigência dos contratos;
-	Gestão sobre as informações financeiras do contrato;
-	Visão global das penalidades aplicadas aos contratados;
-	Controle sobre o valor desembolsado em cada contrato e sobre todos os contratos do órgão ou entidade;
-	Gerenciamento dos diversos contratos sob a responsabilidade do gestor;
-	Facilidade e praticidade nas sub-rogações;
-	Padronização das ações de fiscalização por parte dos fiscais;
-	Controle dos atos administrativos praticados;
-	Controle sobre a fiscalização realizada;
-	Contato fácil com os fornecedores e solução rápida de impasses;
-	Controle sobre a realização de aditivos contratuais.

## 2. Conta-Depósito Vinculada
A Conta-Depósito Vinculada ― bloqueada para movimentação é um instrumento de gestão e gerenciamento de riscos para as contratações de serviços continuados com dedicação exclusiva de mão de obra pela Administração Pública Federal direta, autárquica e fundacional. O principal objetivo deste instituto reside na garantia de existência de saldo financeiro para fazer frente aos encargos trabalhistas devidos aos funcionários contratados pelas empresas terceirizadas para a prestação de serviços em órgãos e entidades.

Destina-se exclusivamente à provisão dos valores referentes ao pagamento das férias, 1/3 constitucional de férias e 13º salário, dos encargos previdenciários incidentes sobre as rubricas citadas, bem como dos valores devidos em caso de pagamento de multa sobre o saldo do FGTS na demissão sem justa causa dos funcionários da empresa contratada que se encontram alocados no órgão. Dessa maneira, os recursos ficam resguardados e somente serão liberados com expressa autorização do órgão contratante, mediante comprovação das despesas por parte da empresa, não constituindo, portanto, um fundo de reserva.

O módulo Conta-Depósito Vinculada foi desenvolvido tendo por base as previsões legais:
-	Instrução Normativa n° 5, de 26 de maio de 2017;
-	Resolução CNJ nº 169, de 10 de novembro de 2013 e alterações.

## 3. Cadastro da Conta-Depósito Vinculada
Acesse o menu “Gestão Contratual” -> “Meus Contratos” e serão listados todos os contratos aos quais o(a) usuário(a) está atribuído como responsável.

Para cadastrar a “Conta-Depósito Vinculada”, clique em “Conta Depósito-Vinculada” e “Adicionar Conta-Depósito Vinculada”.


![FIGURA 1a - Acessar Conta-Depósito Vinculada](./images/figura1a.JPG)

![FIGURA 1b - Adicionar Conta-Depósito Vinculada](./images/figura4.JPG)

Preencha os dados da “Conta-Depósito Vinculada”:
-	“Banco”
-	“Agência”
-	“Conta Corrente”
-	“Encargos SAT (%)”: corresponde aos percentuais 1%, 2% ou 3% dependendo do Seguro de Acidente do Trabalho, prevista no art. 22, inciso II, da Lei nº 8.212, de 1991.

Após preenchidos todos os dados, clique em “Salvar e voltar”.

--ATENÇÃO:-- Cada contrato poderá ter apenas uma única Conta-Depósito Vinculada.

![FIGURA 2 - Cadastro de Conta-Depósito Vinculada](./images/figura2.JPG)

## 4. Detalhamento da Conta-Depósito Vinculada

![FIGURA 3 - Ações da Conta-Depósito Vinculada](./images/figura3.JPG)

Clicando no ícone “ ![](./images/icone1.JPG) ”, são exibidas ações relativas a “Conta-Depósito Vinculada”:
-	“Extrato de Lançamentos”: são exibidos todos os lançamentos detalhados por Empregado, Tipo de Movimentação, Mês, Ano, Verba (Encargo) e Valor;

![FIGURA 4 - Extrato de Lançamentos](./images/figura4.JPG)

-	“Movimentações”: são exibidas todas as movimentações já realizadas detalhadas com o Tipo de Movimentação, Mês, Ano e Total Movimentado;

![FIGURA 5 - Movimentações](./images/figura5.JPG)

-	“Nova Provisão”: realizar nova provisão para conta vinculada;
-	“Empregados/Liberação”: exibidos todos os terceirizados alocados no contrato com detalhe do Total provisionado e Saldo a ser liberado por empregado;

![FIGURA 6 - Empregados/Liberação](./images/figura6.JPG)

-	“Funções/Reajuste de remuneração”: exibidas todas as funções alocadas no contrato com as respectivas remunerações.

![FIGURA 7 - Funções/Reajuste de Remuneração](./images/figura7.JPG)

## 5. Nova Provisão na Conta-Depósito Vinculada
Acesse a Conta-Depósito Vinculada, clique no ícone “ ![](./images/icone1.JPG) ” e em “Nova Provisão”:

![FIGURA 8 - Acessar Nova Provisão](./images/figura8.JPG)

Preencha os campos:
-	Mês Competência;
-	Ano Competência;
-	Terceirizados que ficarão de fora da provisão: será exibida uma lista com todos os terceirizados alocados no contrato. Caso haja um ou mais terceirizados que não serão provisionados naquele mês, basta selecioná-los na lista.
Após preenchidos os dados, clique em “Salvar”.
Será exibida a lista com todas as movimentações já realizadas.

![FIGURA 9 - Nova Provisão](./images/figura9.JPG)

Caso a haja algum erro na provisão realizada, esta poderá ser excluída clicando no botão “ ![Excluir](./images/icone2.JPG) ”.

![FIGURA 10 - Movimentações da Conta-Depósito Vinculada](./images/figura10.JPG)

Clicando no ícone “ ![](./images/icone1.JPG) ” e em “Lançamentos”, são exibidos todos os lançamentos da provisão realizada.

![FIGURA 11 - Lançamentos da Conta-Depósito Vinculada](./images/figura11.JPG)


## 6. Liberação na Conta-Depósito Vinculada
Acesse a Conta-Depósito Vinculada, clique no ícone “ ![](./images/icone1.JPG) ” e em “Empregados/Liberação

![FIGURA 12 - Acessar Liberação na Conta-Depósito Vinculada](./images/figura12.JPG)

No menu de Movimentações, poderá ser efetuada uma liberação clicando em “Nova Liberação”.

![FIGURA 13 - Movimentações na Conta-Depósito Vinculada](./images/figura13.JPG)

São listados todos os terceirizados alocados no contrato.
Clique no ícone “ ![](./images/icone1.JPG) ” e em “Nova liberação para este empregado”:

![FIGURA 14 - Empregado/Liberações](./images/figura14.JPG)

Preencha os campos relativos à Liberação:
-	Mês liberação;
-	Ano Liberação;
-	Situação da Liberação: 
    - Férias
    - Décimo Terceiro
    - Férias e Décimo Terceiro
    - Rescisão

Conforme a situação da Liberação informada, os campos serão ou não habilitados:
-	Valor para Férias: Férias/Férias e Décimo Terceiro
-	Valor para Décimo Terceiro: Férias/Férias e Décimo Terceiro
-	Valor da multa caso a rescisão não seja por justa causa: Rescisão
-	Data da rescisão: Rescisão

Após o preenchimento dos campos, clique em “Salvar”.

![FIGURA 15 - Nova Liberação](./images/figura15.JPG)

## 7. Reajuste de Remuneração
Acesse a Conta-Depósito Vinculada, clique no ícone “   “ e em “Funções/Reajuste de remuneração”:

![FIGURA 16 - Acessar Reajuste de remuneração](./images/figura16.JPG)

Clique no ícone “ ![](./images/icone1.JPG) ” e em “Funções/Reajuste de remuneração”:

![FIGURA 17 - Funções/Reajuste de remuneração](./images/figura17.JPG)

Preencha os dados para o Reajuste De Remuneração:
-	“Jornada”: selecione a jornada de trabalho a ser reajustada;
-	“Descrição Complementar”: descrição detalhada conforme cadastrado nas informações do terceirizado;
-	“Nova remuneração”: valor da nova remuneração;
-	“Mês Início”: mês início da nova remuneração;
-	“Ano Início”: ano início da nova remuneração;
-	“Mês Fim”: mês fim da nova remuneração;
-	“Ano Fim”: ano fim da nova remuneração.

Após preenchidos todos os dados, clique em “Salvar”.

![FIGURA 18 - Reajuste de remuneração](./images/figura18.JPG)