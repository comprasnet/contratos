<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

//Route::group([
//    'prefix'     => config('backpack.base.route_prefix', ''),
//    'middleware' => ['web', config('backpack.base.middleware_key', '')],
//    'namespace'  => 'App\Http\Controllers\Admin',
//], function () { // custom admin routes
//}); // this should be the absolute last line of this file
Route::get('api/modelosdocumentos', 'App\Http\Controllers\Api\ModeloDocumentoController@index');
Route::get('api/modelosdocumentos/{id}', 'App\Http\Controllers\Api\ModeloDocumentoController@show');
Route::get('gescon/consulta/download-arquivo-contrato/{id}', 'App\Http\Controllers\Gescon\ConsultaarquivoCrudController@donwloadArquivoContrato');

//'check.termoAcesso' Middleware de verificação se o usuário aceitou o termo.
Route::group([
    'middleware' => ['web', config('backpack.base.middleware_key', ''), 'check.termoAcesso'],
    'namespace' => 'App\Http\Controllers',
], function () {
    Route::group(['middleware' => 'ugprimaria'], function () {

        Route::group([
            'prefix' => 'api',
            'namespace' => 'Api',
        ], function () {

            Route::get('executadou/{datapub}', 'ExecutaDouController@executaRotinaEnviaDou');

            //busca empenhos via ajax
            Route::get('empenho', 'EmpenhoController@index');
            Route::get('empenho/{id}', 'EmpenhoController@show');
            Route::post('empenho/autocomplete/select2', 'EmpenhoController@autocomplete')->name('autocomplete');

            Route::get('codigoitemAmparoLegal', 'CodigoitemController@index');    // amparo legal

            Route::get('unidade', 'UnidadeController@index');
            Route::get('unidadefiltercontratos', 'UnidadeController@unidadesfiltercontratos');
            Route::get('unidade/{id}', 'UnidadeController@show');
            Route::get('unidadesexecutoras/{orgao_id}/', 'UnidadeController@unidadesexecutorasfinanceiras');
            Route::get('contratohistorico', 'ContratohistoricoController@index');
            Route::get('contratohistorico/{id}', 'ContratohistoricoController@show');
            Route::get('unidadecomorgao', 'UnidadeComOrgaoController@index');
            Route::get('unidadecomorgao/{id}', 'UnidadeComOrgaoController@show');
            Route::get('fornecedor', 'FornecedorController@index');
            Route::get('suprido', 'FornecedorController@suprido');
            Route::get('subcontratados', 'FornecedorController@subcontratados');
            Route::get('subcontratados/{id}', 'FornecedorController@show');
            Route::get('fornecedor/{id}', 'FornecedorController@show');
            Route::get('planointerno', 'PlanointernoController@index');
            Route::get('planointerno/{id}', 'PlanointernoController@show');
            Route::get('naturezadespesa', 'NaturezadespesaController@index');
            Route::get('naturezadespesa/{id}', 'NaturezadespesaController@show');
            Route::get('comprasiasg', 'ComprasiasgController@index');
            Route::get('comprasiasg/{id}', 'ComprasiasgController@show');
            Route::get('catmatsergrupo', 'CatmatsergrupoController@index');
            Route::get('empecatmatsergrupo/{id}', 'CatmatsergrupoController@show');
            Route::get('catmatseritem/buscarportipo/{tipo_id}', 'CatmatseritemController@itemPorTipo')->name('busca.catmatseritens.portipo');
            Route::get('empecatmatseritem/{id}', 'CatmatseritemController@show')->name('busca.catmatseritens.id');
            Route::get('orgaosubcategoria', 'OrgaosubcategoriaController@index');
            Route::get('orgaosubcategoria/{id}', 'OrgaosubcategoriaController@show');
            Route::get('municipios', 'MunicipioController@index');
            Route::get('busca/municipio', 'MunicipioController@buscaMunicipio');
            Route::get('busca/municipio/apropriacaodeducao', 'MunicipioController@buscaMunicipioApropriacao')->name('busca-municipio');
            Route::get('termosPorTipo', 'TermosPorTipoController@index');
            Route::get('amparolegalfiltercontratos', 'AmparoLegalController@amparolegalfiltercontratos');
            Route::get('amparolegal', 'AmparoLegalController@index');
            Route::get('amparolegal/{id}', 'AmparoLegalController@show');
            Route::get('qualificacao/{semvigencia?}', 'TermoAditivoController@index');
            Route::get('atualizasaldos/unidade/{cod_unidade}', 'SaldoContabilController@atualizaSaldosPorUnidade')->name('atualiza.saldos.unidade');
            Route::get('atualizasaldos/linha/{saldo_id}', 'SaldoContabilController@atualizaSaldosPorLinha')->name('atualiza.saldos.linha');
            Route::get('pupula/tabelas/siafi/{minuta_id}', 'MinutaEmpenhoController@populaTabelasSiafi')->name('popula.tabelas.siafi');
            Route::get('pupula/tabelas/siafi/{minuta_id}/{remessa?}', 'MinutaEmpenhoController@populaTabelasSiafiAlteracao')->name('popula.tabelas.siafi.alt');
            Route::get('inserir/celula/modal/{cod_unidade}/{contacorrente}', 'SaldoContabilController@inserirCelulaOrcamentaria')->name('saldo.inserir.modal');
            Route::get('carrega/saldos/unidade/{cod_unidade}', 'SaldoContabilController@carregaSaldosPorUnidadeSiasg')->name('carrega.saldos.unidade');
            Route::get('minutaempenhoparacontrato', 'MinutaEmpenhoController@minutaempenhoparacontrato');
            Route::get('novoempenho/{minuta_id}', 'MinutaEmpenhoController@novoEmpenhoMesmaCompra')->name('novo.empenho.compra');
            Route::get('atualiza-credito-orcamentario/{minuta_id}', 'MinutaEmpenhoController@atualizaCreditoOrcamentario')->name('atualiza.credito.orcamentario');
            Route::get('contrato/numero', 'ContratoController@index');
            Route::get('inserir/item/modal/{tipo_id}/{contacorrente}', 'ContratoItensMinutaController@inserirIten')->name('item.inserir.modal');
            Route::get('buscar/itens/modal/{minutas_id}', 'ContratoItensMinutaController@buscarItensModal')->name('buscar.itens.modal');
            Route::get('buscar/itens/instrumentoinicial/{minutas_id}', 'ContratoItensMinutaController@buscarItensDeMinutaParaTelaInstrumentoInicial')->name('buscar.itens.instrumentoinicial');
            Route::get('buscar/campos/contrato/empenho/{id}', 'ContratoController@buscarCamposParaCadastroContratoPorIdEmpenho')->name('buscar.campos.contrato.empenho');
            Route::get(
                '/saldo-historico-itens/{id}',
                'SaldoHistoricoItemController@retonaSaldoHistoricoItens'
            )->name('saldo.historico.itens');
            Route::get(
                '/contrato-itens/{contrato_id}',
                'ContratoItemController@retornaContratoItens'
            )->name('saldo.sdfsdf.itens');
            Route::get('amparolegalrestricao', 'AmparoLegalRestricaoController@index');
            Route::get('amparolegalrestricao/{id}', 'AmparoLegalRestricaoController@show');
            Route::get('fatura/padrao', 'ContratofaturaController@recuperarPadrao')->name('apropriacao.fatura.padrao');

            Route::get('fatura/chave-nfe/{chave}', 'ChaveNfe\ChaveNfeController@validarChaveNfe')->name('api.fatura.validar.chavenfe');
            Route::get('/localexecucao/buscacep', 'ContratoController@buscaCEP')->name('localexecucao.buscacep');

            Route::group([
                'prefix' => 'empenho',
            ], function () {
                Route::put('/sem/contrato/e/{empenho}/f/{fornecedor}/c/{contrato}', 'EmpenhoController@gravaContratoEmpenho');

            });

            Route::get('retornaritenscompra', 'ComprasiasgController@retornarItensCompra');

            Route::get('buscar/itens/termos-cobrancas/{idContrato}/{catmatseritem}/{codSiasg}/{tipo}/{numero}', 'SaldoHistoricoItemController@retonarItensTermos')->name('buscar.itens.termosecobrancas');
        });

        // if not otherwise configured, setup the dashboard routes
        if (config('backpack.base.setup_dashboard_routes')) {
            Route::get('inicio', 'AdminController@index')->name('backpack.inicio');
            Route::get('/', 'AdminController@redirect')->name('backpack');
            Route::get('/dashboard', 'AdminController@redirect')->name('backpack');
            Route::post('eventos/calendario', 'AdminController@carregaEventos')->name('calendario.eventos');
        }

        Route::get('/storage/comunica/anexos/{file}', 'DownloadsController@anexoscomunica');
        Route::get('/storage/ocorrencia/{path}/{file}', 'DownloadsController@anexosocorrencia');

        Route::get('/storage/importacao/{path}/{file}', 'DownloadsController@importacao');

        Route::get('/mensagens', 'AdminController@listaMensagens');
        Route::get('/mensagem/{id}', 'AdminController@lerMensagem');

        Route::get('/paises', 'PaisesController@listaPaises')->name('crud.faturas.paises');

        Route::group([
            'prefix' => 'painel',
            'namespace' => 'Painel'
        ], function () {
            Route::get('financeiro', 'FinanceiroController@index')->name('painel.financeiro');
            Route::get('orcamentario', 'OrcamentarioController@index')->name('painel.orcamentario');
        });

        Route::group([
            'prefix' => 'admin',
            'namespace' => 'Admin'
        ], function () {
            Route::get('dias-uteis', 'FeriadoCrudController@calculaDiasUteis');

            CRUD::resource('modelodocumento', 'ModeloDocumentoCrudController');
            CRUD::resource('tipoindices', 'TipoIndicesCrudController');
            Route::group(['prefix' => 'tipoindices'], function () {
                CRUD::resource('calculadora', 'CalculadoraIndiceCrudController');
                Route::get('/{tipo_indice}/calculadora/create', 'CalculadoraIndiceCrudController@create');
            });
            CRUD::resource('indicevalores', 'IndiceValoresCrudController');
            Route::post('modelodocumento/gettemplate', 'ModeloDocumentoCrudController@getTemplate')->name('modeloDocumento.getTemplate');
            CRUD::resource('activitylog', 'ActivitylogCrudController');
            CRUD::resource('usuario', 'UsuarioCrudController');
            CRUD::resource('usuario-sistema', 'SystemUsersCrudController');

            Route::post('/usuario/{id}/load-json', 'UsuarioCrudController@loadJson')
                ->middleware('permission:activitylog_consultar');

            CRUD::resource('usuarioorgao', 'UsuarioOrgaoCrudController');
            CRUD::resource('usuariounidade', 'UsuarioUnidadeCrudController');
            CRUD::resource('orgaosuperior', 'OrgaoSuperiorCrudController');
            CRUD::resource('orgao', 'OrgaoCrudController');
            CRUD::resource('unidade', 'UnidadeCrudController');
            CRUD::resource('administradorunidade', 'UnidadeAdministradorUnidadeCrudController');
            CRUD::resource('codigo', 'CodigoCrudController');
            CRUD::resource('sfcertificado', 'SfcertificadoCrudController');
            CRUD::resource('justificativafatura', 'JustificativafaturaCrudController');
            CRUD::resource('tipolistafatura', 'TipolistafaturaCrudController');
            CRUD::resource('catmatseratualizacao', 'CatmatseratualizacaoCrudController');
            CRUD::resource('comunica', 'ComunicaCrudController');
            CRUD::resource('importacao', 'ImportacaoCrudController');
            CRUD::resource('ipsacesso', 'IpsacessoCrudController');
            CRUD::resource('feriado', 'FeriadoCrudController');
            CRUD::resource('failedjobs', 'FailedjobsCrudController');
            CRUD::resource('jobs', 'JobsCrudController');
            CRUD::resource('amparolegal', 'AmparoLegalCrudController');
            CRUD::resource('padroespublicacao', 'PadroespublicacaoCrudController');
            CRUD::resource('publicacoes', 'ContratoPublicacaoAdminCrudController');
            CRUD::resource('pncp', 'PncpController');
            CRUD::resource('ajusteminuta', 'AjusteMinutasCrudController');
            CRUD::resource('devolveminutasiasg', 'DevolveminutasiasgCrudController');
            CRUD::resource('ajusteremessas', 'AjusteRemessasCrudController');
            CRUD::resource('sfhorarioexcecao', 'SfhorarioexcecaoCrudController');
            CRUD::resource('situacaosiafi', 'ExecsfsituacaoCrudController');
            CRUD::resource('termoacesso', 'TermoAceiteCrudController');

            Route::post('usuariocpf', 'UsuarioCrudController@ajaxusuario');
            Route::post('usuariounidadecpf', 'UsuarioUnidadeCrudController@ajaxunidade');
            Route::post('usuarioorgaocpf', 'UsuarioOrgaoCrudController@ajaxorgao');

            Route::group(['prefix' => 'situacaosiafi/{execsfsituacao_id}'], function () {
                CRUD::resource('camposvariaveis', 'ExecsfsituacaoCamposVariaveisCrudController');
            });

            Route::get('/amparolegal/{amparolegal_id}/enviarsiafi', 'AmparoLegalCrudController@salvarSfAmparoLegal');

            Route::get('ajusteminuta/remessa/{id_remessa}/atualizaritemcompracontrato', 'AjusteMinutasCrudController@atualizaritemcompracontrato')
                ->name('ajusteminuta.atualizar.contrato.compra');

            Route::get('ajusteremessas/remessa/{id_remessa}/atualizaritemcompracontrato', 'AjusteMinutasCrudController@atualizaritemcompracontrato')
                ->name('ajusteremessas.atualizar.contrato.compra');

            Route::post('importacao/terceirizados/{contrato_id}', 'ImportacaoCrudController@importacaoTerceirizados');

            // Exportações Downloads
            Route::get('downloadapropriacao/{type}', 'ExportController@downloadapropriacao')
                ->name('apropriacao.download');

            Route::get('downloadexecucaoporempenho/{type}', 'ExportController@downloadExecucaoPorEmpenho')
                ->name('execucaoporempenho.download');

            Route::get('downloadlistatodoscontratos/{type}', 'ExportController@downloadListaTodosContratos')
                ->name('listatodoscontratos.download');

            Route::get('downloadlistacontratosorgao/{type}', 'ExportController@downloadListaContratosOrgao')
                ->name('listacontratosorgao.download');

            Route::get('downloadlistacontratosug/{type}', 'ExportController@downloadListaContratosUg')
                ->name('listacontratosug.download');

            Route::get('downloadplanilhaterceirizados', 'ExportController@downloadPlanilhaTerceirizados')
            ->name('planilhaTerceirizado.download');

            Route::group(['prefix' => 'codigo/{codigo_id}'], function () {
                CRUD::resource('codigoitem', 'CodigoitemCrudController');
            });

            Route::group(['prefix' => 'amparolegal/{amparo_legal_id}'], function () {
                CRUD::resource('restricao', 'AmparoLegalRestricaoCrudController');
            });

            Route::group(['prefix' => 'unidade/{unidade_id}'], function () {
                CRUD::resource('configuracao', 'UnidadeconfiguracaoCrudController');
                CRUD::resource('autoridadesignataria', 'AutoridadesignatariaCrudController');
            });

            Route::group(['prefix' => 'orgao/{orgao_id}'], function () {
                CRUD::resource('subcategorias', 'OrgaoSubcategoriaCrudController');
                CRUD::resource('configuracao', 'OrgaoconfiguracaoCrudController');

                Route::group(['prefix' => 'siorg'], function () {
                    Route::get('/{codigo_siorg}/lista/atualizarestrutura', 'SiorgCrudController@updateEstruturaOrganizacional')->name('atualizarestrutura');
                    CRUD::resource('/{codigo_siorg}/lista', 'SiorgCrudController');
                });
            });

            Route::get('migracaoconta/{orgaoconfiguracao_id}', 'MigracaoSistemaContaController@index');

            Route::get('/rotinaalertamensal', 'UnidadeCrudController@executaRotinaAlertaMensal');

            Route::get('/atualizaorgaosuperior', 'OrgaoSuperiorCrudController@executaAtualizacaoCadastroOrgaoSuperior');
            Route::get('/atualizaorgao', 'OrgaoCrudController@executaAtualizacaoCadastroOrgao');
            Route::get('/atualizaunidade', 'UnidadeCrudController@executaAtualizacaoCadastroUnidade');
            Route::get('/atualiza_unidade_antecipa_gov/{bool}/{ids}', 'UnidadeCrudController@executaAtualizacaoAntecipaGov');

            Route::get('/envia_todos_pncp', 'EnviaPNCPController@enviaPNCP');
            Route::get('/envia_unidade_pncp/{key}', 'EnviaPNCPController@enviaUnidade');
            Route::get('/login_pncp', 'EnviaPNCPController@loginPNCP');
            Route::get('/buscaArquivoEmpenhoAssinadoPNCP', 'EnviaPNCPController@buscaArquivoEmpenhoAssinado');
            Route::get('/preencheLinksArquivoEmpenhoPNCP', 'EnviaPNCPController@preencheLinksArquivoEmpenho');

            Route::get('/retryfailedjob/{id}', function ($id) {
                $path = env('APP_PATH');
                exec('php ' . $path . 'artisan queue:retry ' . $id);
                return redirect(url('/admin/failedjobs'));
            });

            Route::get('/infocontratos', 'InfoContratosController@index');

            CRUD::resource('migration', 'MigrationCrudController');

            CRUD::resource('compra','CompraCrudController');

            Route::group(
                ['prefix' => 'compra/{compra_id}'],
                function () {
                    CRUD::resource('compraitem', 'CompraItemCrudController');
                    CRUD::resource('minutas'   , 'ConsultaMinutaCompraCrudController');
                }
            );

                Route::get('compra/ativar-inativar-compra-item/compra-item', 'CompraCrudController@ativarOuInativarItens');
        });


        Route::get('apropria/{sfpadrao_id}', 'Apropriacao\ApropriacaoFaturaSiafiController@enviarApropriacaoFaturaSiafi');

        Route::group([
            'prefix' => 'instrumentocobranca/{contratofaturas_id}',
            'namespace' => 'Apropriacao',
        ], function () {
            CRUD::resource(
                'apropriacao',
                'ApropriacaoInstrumentoCobrancaCrudController'
            );
        });

        Route::group([
            'prefix' => 'gescon',
            'namespace' => 'Gescon',
        ], function () {

            CRUD::resource('instrumento-de-cobranca', 'InstrumentodecobrancaSituacaoCrudController');
            CRUD::resource('contrato', 'ContratoCrudController');
            CRUD::resource('meus-contratos', 'MeucontratoCrudController');
            CRUD::resource('indicador', 'IndicadorCrudController');
            CRUD::resource('encargo', 'EncargoCrudController');
            CRUD::resource('fatura', 'FaturaCrudController');
            CRUD::resource('fornecedor', 'FornecedorCrudController');
            CRUD::resource('subrogacao', 'SubrogacaoCrudController');
            Route::post('fornecedorsicaf', 'FornecedorCrudController@sicaf');
            Route::get('fornecedor/certidao/{fornecedor_id}/{rota}', 'FornecedorCrudController@certidao');

            Route::group(['prefix' => 'tipoindices'], function () {
                CRUD::resource('calculadora', 'CalculadoraIndiceCrudController');
                Route::get('/{tipo_indice}/calculadora/create', 'CalculadoraIndiceCrudController@create');
            });

            CRUD::resource('instrumento-de-cobranca', 'InstrumentodecobrancaSituacaoCrudController')->name('instrumento.cobranca.alterar.situacao'); #529 #598

            Route::get(
                '/buscar-contrato-itens/{contrato_id}',
                'ContratoitemCrudController@retonaContratoItem'
            )->name('contrato.item');

            Route::get(
                '/buscar-contrato-itens-edit/{contrato_id}',
                'ContratoitemCrudController@retornaContratoItensParaEdit'
            )->name('contrato.item.edit');

            // cadastro de conta vinculada, pela resolução 169 CNJ
            Route::get('/contrato/{contrato_id}/contratocontas/pelaresolucao169', 'ContratocontaCrudController@create');
            Route::get('/consultar/comprasiasg', 'ContratoCrudController@consultaApiCompraSiasg')->name('consultar.comprasiasg.minuta');
            Route::post('/contrato/contrato-tipo-empenho', 'ContratoCrudController@criarContratosTipoEmpenhoMinutasEmpenho')->name('contrato.tipo.empenho');#148
            Route::post('/contrato/buscarempenho', 'ContratoCrudController@buscarEmpenho')->name('contrato.buscarempenho');
            Route::post('/contratos/buscarcontratoempenho', 'ContratoCrudController@buscarContratoEmpenho')->name('buscarcontratoempenho');

            Route::group([
                'prefix' => 'siasg',
                'namespace' => 'Siasg',
            ], function () {
                CRUD::resource('compras', 'SiasgcompraCrudController');
                CRUD::resource('contratos', 'SiasgcontratoCrudController');
                Route::get('apisiasg', 'SiasgcompraCrudController@apisiasg');
                Route::get('inserircompras', 'SiasgcompraCrudController@inserirComprasEmMassa');
                Route::get('inserircontratos', 'SiasgcontratoCrudController@verificarContratosPendentes');
                Route::get('/compras/{id}/atualizarsituacaocompra', 'SiasgcompraCrudController@executarAtualizacaoSituacaoCompra');
                Route::get('/contratos/{id}/atualizarsituacaocontrato', 'SiasgcontratoCrudController@executarAtualizacaoSituacaoContrato');
                //rota de teste para atualização do contrato
                Route::get('/atualiza-contrato', 'SiasgcontratoCrudController@executaJobAtualizacaoSiasgContratos');
            });

            // início conta vinculada - contrato conta
            Route::group(['prefix' => 'contrato/contratoconta/{contratoconta_id}'], function () {
                CRUD::resource('extratocontratoconta', 'ExtratocontratocontaCrudController');
                CRUD::resource('movimentacaocontratoconta', 'MovimentacaocontratocontaCrudController');
                CRUD::resource('depositocontratoconta', 'DepositocontratocontaCrudController');
                CRUD::resource('retiradacontratoconta', 'RetiradacontratocontaCrudController');
                CRUD::resource('funcionarioscontratoconta', 'FuncionarioscontratocontaCrudController');
                CRUD::resource('funcoescontratoconta', 'FuncoescontratocontaCrudController');
                CRUD::resource('encerramentocontratoconta', 'EncerramentocontratocontaCrudController');
                CRUD::resource('encerramentocontratoconta/emitirTermoDeEncerramento', 'EncerramentocontratocontaCrudController')->name('emitirTermoDeEncerramento');
            });
            Route::group(['prefix' => 'contrato/contratoconta/{contratoconta_id}/{funcao_id}'], function () {
                CRUD::resource('repactuacaocontratoconta', 'RepactuacaocontratocontaCrudController');
            });
            Route::group(['prefix' => 'contrato/contratoconta/movimentacaocontratoconta/{movimentacaocontratoconta_id}'], function () {
                CRUD::resource('lancamento', 'LancamentoCrudController');
            });
            Route::group(['prefix' => 'contrato/contratoconta/contratoterceirizado/{contratoterceirizado_id}'], function () {
                CRUD::resource('retiradacontratoconta', 'RetiradacontratocontaCrudController');
            });
            Route::get('movimentacao/{movimentacao_id}/excluir', 'MovimentacaocontratocontaCrudController@excluirMovimentacao');
            // fim conta vinculada - contrato conta

            Route::group(['prefix' => 'contrato/{contrato_id}'], function () {
                CRUD::resource('faturas', 'ContratofaturaCrudController');
                CRUD::resource('contratocontas', 'ContratocontaCrudController'); // conta vinculada

                Route::group(['middleware' => 'restrictAlmoxarifado'], function () {
                    CRUD::resource('aditivos', 'AditivoCrudController');
                    CRUD::resource('apostilamentos', 'ApostilamentoCrudController');
                    CRUD::resource('arquivos', 'ContratoarquivoCrudController');
                    CRUD::resource('arquivos-pncp', 'ArquivosPncpCrudController');
                    CRUD::resource('cronograma', 'ContratocronogramaCrudController');
                    CRUD::resource('despesaacessoria', 'ContratoDespesaAcessoriaCrudController');
                    CRUD::resource('empenhos', 'ContratoempenhoCrudController');
                    CRUD::resource('garantias', 'ContratogarantiaCrudController');
                    CRUD::resource('contratounidadedescentralizada', 'ContratounidadedescentralizadaCrudController');
                    CRUD::resource('historico', 'ContratohistoricoCrudController')->name('listar.historico');
                    CRUD::resource('instrumentoinicial', 'InstrumentoinicialCrudController');
                    CRUD::resource('itens', 'ContratoitemCrudController');
                    CRUD::resource('padrao', 'ContratosfpadraoCrudController');
                    CRUD::resource('parametros', 'ContratoParametroCrudController');
                    CRUD::resource('prepostos', 'ContratoprepostoCrudController');
                    CRUD::resource('publicacao', 'ContratoPublicacaoCrudController');
                    CRUD::resource('minutas', 'ContratoMinutaCrudController')->name('contratos.minutas');
                    CRUD::resource('terceirizados', 'ContratoterceirizadoCrudController');
                    CRUD::resource('localexecucao', 'ContratoLocalExecucaoCrudController');

                    Route::post('ativar', 'ContratoCrudController@ativar')->name('ativar.contrato');
                    Route::post('apostilamentos/{contratohistorico_id}/ativar', 'ApostilamentoCrudController@ativar')->name('ativar.apostilamento');

                    Route::get('/minutas/minutasparacopiar/{userUgId}/{idTipoDocumento}', 'ContratoMinutaCrudController@copiarMinuaExistente');
                    Route::get('/minutas/exibirparacopiar/{userUgId}/{idMinuta}', 'ContratoMinutaCrudController@exibirMinuaExistente');
                    Route::get('/minutas/montartokencontrato/{idItemContrato}', 'ContratoMinutaCrudController@montarTokenAjax');
                    Route::get('/minutas/montartokenresponsavel/{idItemContrato}', 'ContratoMinutaCrudController@montarTokenResponsavelAjax');
                    Route::get('/minutas/montartokenitemcontrato/{idItemContrato}', 'ContratoMinutaCrudController@montarTokenItemContratoAjax');
                    Route::get('/minutas/montarinformacaotabelaitemcontrato/{idItemContrato}', 'ContratoMinutaCrudController@montarInformacaoTabelaItemContrato');
                    Route::get('/minutas/montartokenempenho/{idItemContrato}', 'ContratoMinutaCrudController@montarTokenEmpenhoAjax');
                    Route::get('/minutas/montarinformacaotabelaempenho/{idEmpenho}', 'ContratoMinutaCrudController@montarInformacaoTabelaEmpenho');

                    Route::group(['prefix' => 'minutas/{minuta_id}'], function () {
                        CRUD::resource('sei', 'ContratoMinutaEnvioSeiCrudController');
                        Route::get('imprimir', 'ContratoMinutaCrudController@imprimir');
                    });
                    Route::get(
                        '/publicacao/{id}/atualizarsituacaopublicacao',
                        'ContratoPublicacaoCrudController@executarAtualizacaoSituacaoPublicacao'
                    );
                    Route::get('arquivos/atualizacaosituacaoarquivominuta/{id}', 'ContratoarquivoCrudController@atualizacaoSituacaoArquivoMinuta')
                    ->name('arquivos.atualizar.situacao.assinatura');

                    Route::get('/publicacao/{publicacao_id}/deletarpublicacao', 'ContratoPublicacaoCrudController@deletarPublicacao');
                    Route::get('/publicacao/{publicacao_id}/enviarpublicacao', 'ContratoPublicacaoCrudController@enviarPublicacao');
                    Route::get('/publicacao/{publicacao_id}/consultarpublicacao', 'ContratoPublicacaoCrudController@consultarPublicacao');

                    CRUD::resource('responsaveis', 'ContratoresponsavelCrudController');
                    CRUD::resource('rescisao', 'RescisaoCrudController');

                    Route::get('/encerramento/gerarpdf', 'EncerramentoCrudController@gerarpdf');
                    Route::get('/encerramento/gerarminutarelatoriofinal', 'EncerramentoCrudController@gerarminutarelatoriofinal');
                    CRUD::resource('encerramento', 'EncerramentoCrudController', ['except' => ['gerarpdf']]);

                    CRUD::resource('status', 'ContratostatusprocessoCrudController');
                    Route::get('extrato', 'ContratoCrudController@extratoPdf');
                    CRUD::resource('domiciliobancario', 'ContratodomicilioCrudController');
                    CRUD::resource('operacaocredito', 'ContratoOperacaoCreditoCrudController');

                    Route::get('mudarstatusrestricao/{idContratoArquivo}/{restrito}', 'ContratoarquivoCrudController@mudarStatusDocumento');
                });
            });

            Route::group([
                'prefix' => 'consulta/',
            ], function () {
                CRUD::resource('arquivos', 'ConsultaarquivoCrudController');
                CRUD::resource('cronogramas', 'ConsultacronogramaCrudController');
                CRUD::resource('despesasacessorias', 'ConsultaDespesaAcessoriaCrudController');
                CRUD::resource('empenhos', 'ConsultaempenhoCrudController');
                CRUD::resource('faturas', 'ConsultafaturaCrudController');
                CRUD::resource('garantias', 'ConsultagarantiaCrudController');
                CRUD::resource('historicos', 'ConsultahistoricoCrudController');
                CRUD::resource('itens', 'ConsultaitemCrudController');
                CRUD::resource('ocorrencias', 'ConsultaocorrenciaCrudController');
                CRUD::resource('prepostos', 'ConsultaprepostoCrudController');
                CRUD::resource('responsaveis', 'ConsultaresponsavelCrudController');
                CRUD::resource('terceirizados', 'ConsultaterceirizadoCrudController');
            });

            Route::group(['prefix' => 'contratohistorico/{contratohistorico_id}'], function () {
                CRUD::resource('itens', 'SaldohistoricoitemCrudController');
            });

            Route::get('/saldohistoricoitens/carregaritens/{tipo}/{contratohistorico_id}', 'SaldohistoricoitemCrudController@carregarItens');

            Route::group(['prefix' => 'meus-contratos/{contrato_id}'], function () {
                CRUD::resource('faturas', 'MeuContratofaturaCrudController');
                CRUD::resource('ocorrencias', 'ContratoocorrenciaCrudController');
                CRUD::resource('servicos', 'ContratoServicoCrudController');
                CRUD::resource('terceirizados', 'ContratoterceirizadoCrudController');
                CRUD::resource('encerramento', 'EncerramentoCrudController');
                CRUD::resource('empenhos', 'MeuContratoempenhoCrudController');
                CRUD::resource('arquivos', 'MeucontratoarquivoCrudController');
                Route::get('mudarstatusrestricao/{idContratoArquivo}/{restrito}', 'MeucontratoarquivoCrudController@mudarStatusDocumento');
                Route::get('faturas/certidaoestadual/imprimir', 'MeuContratofaturaCrudController@certidaoEstadual')->name('meus-contratos.certidaoestadual');
                Route::get('faturas/certidaomunicipal/imprimir', 'MeuContratofaturaCrudController@certidaoMunicipal')->name('meus-contratos.certidaomunicipal');
                Route::get('arquivos/atualizacaosituacaoarquivominuta/{id}', 'MeucontratoarquivoCrudController@atualizacaoSituacaoArquivoMinuta');
                Route::any('listaempenhosparafaturas', '\App\Http\Controllers\Api\EmpenhoController@listaEmpenhosParaFaturas')->name('api.faturas.listaEmpenhosParaFaturas');
                Route::any('faturas/contratoitens/list', 'MeuContratofaturaCrudController@contratoItensList')->name('crud.faturas.contratoitens');
                Route::any('faturas/contratoitens/ajax', 'MeuContratofaturaCrudController@contratoItensAjax')->name('crud.faturas.contratoitens.ajax');
            });


            Route::group(
                ['prefix' => 'meus-servicos/{contrato_id}/{contratoitem_servico_id}'],
                function () {
                    CRUD::resource('indicadores', 'ContratoItemServicoIndicadorCrudController');
                    Route::group(['prefix' => '{cisi_id}'], function () {
                        CRUD::resource('glosas', 'GlosaCrudController');
                    });
                }
            );
        });

        Route::group([
            'prefix' => 'execfin',
            'namespace' => 'Execfin',
        ], function () {

            CRUD::resource('empenho', 'EmpenhoCrudController');
            CRUD::resource('restosapagar', 'RestosapagarCrudController');
            Route::get('incluirnovoempenho', 'EmpenhoCrudController@incluirEmpenhoSiafi');
            Route::get('enviaempenhosiasg', 'EmpenhoCrudController@enviaEmpenhoSiasg');
            Route::get('/migracaoempenhos', 'EmpenhoCrudController@executaMigracaoEmpenho');
            Route::get('/atualizasaldosempenhos', 'EmpenhoCrudController@executaAtualizaSaldosEmpenhos');
            Route::get('/atualizanaturezadespesas', 'EmpenhoCrudController@executaAtualizacaoNd');

            Route::group(['prefix' => 'empenho/{empenho_id}'], function () {
                CRUD::resource('empenhodetalhado', 'EmpenhodetalhadoCrudController');
            });
            Route::group(['prefix' => 'restosapagar/{empenho_id}'], function () {
                CRUD::resource('empenhodetalhado', 'RestosapagardetalhadoCrudController');
            });
        });

        // Módulo Empenho
        Route::group([
            'prefix' => 'empenho',
            'namespace' => 'Empenho',
            'as' => 'empenho.',
            'middleware' => ['verify.step.empenho'],
        ], function () {
            Route::group([
                'prefix' => 'consulta',
                'namespace' => 'Pdm'
            ], function(){
                CRUD::resource('fracionamento-despesa-pdm', 'ConsultaFracionamentoDespesaController');
                CRUD::resource('fracionamento-despesa-servico', 'ConsultaFracionamentoDespesaServicoController');

            });
            /**
             *
             * Minuta Empenho - Genéricos
             *
             **/

            CRUD::resource('/minuta', 'MinutaEmpenhoCrudController', ['except' => [
                'index', 'search'
            ]]);

            Route::get('/minuta/ano/{ano}', 'MinutaEmpenhoCrudController@index')->name('crud./minuta.index');
            Route::post('/minuta/ano/{ano}/search', 'MinutaEmpenhoCrudController@search')->name('crud./minuta.search');

            Route::get('minuta/{minuta_id}/atualizarsituacaominuta', 'MinutaEmpenhoCrudController@executarAtualizacaoSituacaoMinuta')
                ->name('minuta.atualizar.situacao');

            Route::get('minuta/{minuta_id}/deletarminuta', 'MinutaEmpenhoCrudController@deletarMinuta')
                ->name('minuta.deletar');

            Route::get('minuta/{minuta_id}/pdf', 'MinutaEmpenhoCrudController@pdfEmpenho')
                ->name('minuta.pdfempenho');

            //passo 1
            Route::get('buscacompra', 'CompraSiasgCrudController@create')
                ->name('minuta.etapa.compra');

            Route::post('buscacompra', 'CompraSiasgCrudController@store');

            //passo 2
            Route::get('fornecedor/{minuta_id}/{uasg_inativa?}', 'FornecedorEmpenhoController@index')
                ->name('minuta.etapa.fornecedor');

            //passo 3
            Route::get('item/{minuta_id}/{fornecedor_id}', 'FornecedorEmpenhoController@item')
                ->name('minuta.etapa.item');

            Route::post('item', 'FornecedorEmpenhoController@store')
                ->name('minuta.etapa.item.store');

            Route::put('item', 'FornecedorEmpenhoController@update')
                ->name('minuta.etapa.item.update');

            //passo 4
            Route::get('saldo/{minuta_id}', 'SaldoContabilMinutaController@index')
                ->name('minuta.etapa.saldocontabil');

            Route::get('saldo/gravar/{minuta_id}', 'SaldoContabilMinutaController@store')
                ->name('minuta.gravar.saldocontabil');

            Route::post('saldo/gravar/saldo/minuta', 'SaldoContabilMinutaController@atualizaMinuta')
                ->name('minuta.atualizar.saldo');

            Route::post('saldo/inserir/celula', 'SaldoContabilMinutaController@inserirCelulaOrcamentaria')
                ->name('saldo.inserir.modal');

            //passo 5
            Route::get('subelemento/{minuta_id}', 'SubelementoController@index')
                ->name('minuta.etapa.subelemento');
            Route::post('subelemento', 'SubelementoController@store')
                ->name('subelemento.store');
            Route::put('subelemento', 'SubelementoController@update')
                ->name('subelemento.update');

            //passo 6

            Route::post('minuta/inserir/fornecedor', 'MinutaEmpenhoCrudController@inserirFornecedorModal')
                ->name('minuta.inserir.fornecedor');

            //passo 7
            CRUD::resource('passivo-anterior', 'ContaCorrentePassivoAnteriorCrudController', ['except' => ['create', 'show']]);

            Route::get('passivo-anterior/{minuta_id}', 'ContaCorrentePassivoAnteriorCrudController@create')
                ->name('minuta.etapa.passivo-anterior');


            //alteracao minuta
            Route::group(['prefix' => 'minuta/{minuta_id}'], function () {
                CRUD::resource('alteracao', 'MinutaAlteracaoCrudController', ['except' => ['show', 'edit']]);

                Route::get('/alteracao/{remessa}/show/{minuta}', 'MinutaAlteracaoCrudController@show')
                    ->name('crud.alteracao.show');
                Route::get('/alteracao/{remessa}/edit/{minuta}', 'MinutaAlteracaoCrudController@create')
                    ->name('crud.alteracao.edit');

                Route::get('/toogleforcacontrato', 'MinutaEmpenhoCrudController@toogleForcaContrato');

                Route::get('alteracao-dt/{fluxo_minuta?}/{nova_minuta_id?}', 'MinutaAlteracaoCrudController@ajax')->name('crud.alteracao.ajax');

                Route::group(['prefix' => 'alteracao'], function () {

                    Route::get('/{remessa_id}/atualizarsituacaominuta', 'MinutaAlteracaoCrudController@executarAtualizacaoSituacaoMinuta')
                        ->name('minuta.alteracao.atualizar.situacao');

                    Route::get('/{remessa_id}/deletarminuta', 'MinutaAlteracaoCrudController@deletarMinuta')
                        ->name('minuta.alteracao.deletar');

                    CRUD::resource('passivo-anterior', 'MinutaAlteracaoPassivoAnteriorCrudController', ['except' => ['update','store', 'create', 'show', 'edit']]);

                    Route::get('passivo-anterior/{remessa}', 'MinutaAlteracaoPassivoAnteriorCrudController@create')
                        ->name('crud.alteracao.passivo-anterior');

                    Route::get('passivo-anterior/{remessa}/edit', 'MinutaAlteracaoPassivoAnteriorCrudController@create')
                        ->name('crud.alteracao.passivo-anterior.edit');

                    Route::post('passivo-anterior/{remessa}', 'MinutaAlteracaoPassivoAnteriorCrudController@store')
                        ->name('crud.alteracao.passivo-anterior.store');

                    Route::put('passivo-anterior/{remessa}', 'MinutaAlteracaoPassivoAnteriorCrudController@update')
                        ->name('crud.alteracao.passivo-anterior.update');
                });

                CRUD::resource('alteracaoFonte', 'MinutaAlteracaoFonteCrudController', ['except' => ['show', 'edit']]);
                Route::group(['prefix' => 'alteracaoFonte'], function () {

                    //Tela 1 Edit
                    Route::get('/{nova_minuta_id}/edit', 'MinutaAlteracaoFonteCrudController@edit')
                        ->name('crud.alteracaoFonte.edit');

                    //Tela 2 Create
                    Route::get('/{nova_minuta_id}/subelemento', 'MinutaAlteracaoCrudController@create')
                    ->name('crud.alteracaoFonte.subelemento');

                    //Tela 2 Store
                    Route::post('/{nova_minuta_id}/subelemento', 'MinutaAlteracaoFonteCrudController@gravarSubelemento')
                    ->name('crud.alteracaoFonte.subelemento.gravar');

                    //Tela 2 Edit
                    Route::get('/{nova_minuta_id}/subelemento/edit', 'MinutaAlteracaoCrudController@create')
                        ->name('crud.alteracaoFonte.subelemento.edit');

                    //Tela 2 Update
                    Route::put('/{nova_minuta_id}/subelemento', 'MinutaAlteracaoFonteCrudController@atualizarSubelemento')
                        ->name('crud.alteracaoFonte.subelemento.update');

                    //Tela 3 Show
                    Route::get('/{remessa}/show/{minuta}', 'MinutaAlteracaoCrudController@show')
                        ->name('crud.alteracaoFonte.show');

                });

            });

            Route::get('alteracaoFonteAjax', 'MinutaAlteracaoFonteCrudController@retornaAjaxGrid')->name('crud.alteracao.fonte');

        });


        Route::group([
            'prefix' => 'relatorio',
            'namespace' => 'Relatorio'
        ], function () {
            Route::get('listatodoscontratos', 'RelContratoController@listaTodosContratos')->name('relatorio.listatodoscontratos');
            Route::get('filtrolistatodoscontratos', 'RelContratoController@filtroListaTodosContratos')->name('filtro.listatodoscontratos');

            Route::get('listacontratosorgao', 'RelContratoController@listaContratosOrgao')->name('relatorio.listacontratosorgao');

            Route::get('listacontratosug', 'RelContratoController@listaContratosUg')->name('relatorio.listacontratosug');
        });
    });

});
