<?php

/*
|--------------------------------------------------------------------------
| Backpack\LogManager Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are
| handled by the Backpack\LogManager package.
|
*/

Route::group([
    'namespace'  => 'Backpack\LogManager\app\Http\Controllers',
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin' ), 'desenvolvedor.check'],
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
], function () {
    Route::get('log/download/{file_name}', 'LogController@download');
});

Route::group([
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin' ), 'desenvolvedor.check'],
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
], function () {
    Route::get('log', 'App\Http\Controllers\LogController@index');
    Route::delete('log/delete/{file_name}', 'App\Http\Controllers\LogController@delete');
    Route::get('log/preview/{file_name}', 'App\Http\Controllers\LogController@preview');
});


Route::group([
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin' )],
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
], function () {
    Route::get('wsdl', 'App\Http\Controllers\WsdlController@index');
    Route::get('wsdl/download/{file_name}', 'App\Http\Controllers\WsdlController@download');
});
