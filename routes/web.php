<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
  //teste
//Route::get('/tratardadosmigracaotseagu', 'MigracaotseaguController@tratardadosmigracaotseagu')->name('tratardadosmigracaotseagu');

use App\Http\Controllers\Execfin\EmpenhoCrudController;
use App\services\STA\STAAuthenticationService;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Response;

Route::pattern('contrato', '[0-9]+');

Route::post('ckeditor/upload', 'CkeditorController@upload')->name('ckeditor.upload');

Route::get('/', function () {
    return redirect('/inicio');
});

Route::get('/home', function () {
    return redirect('/inicio');
});
Route::group([
    'prefix' => 'empenho',
    'namespace' => 'Empenho',
], function () {
    Route::group([
        'prefix' => 'minuta',
        'namespace' => 'Minuta',
    ], function () {
        Route::post('/update', 'FixContratoCompraMinutaController@update')->name('empenho.minuta.update');
    });
});


Route::group([
    'prefix' => 'transparencia',
    'namespace' => 'Transparencia',
], function () {
    //rotas para exibição do módulo transparência  #231
    Route::get('/', 'IndexController@index')->name('transparencia.index');
    CRUD::resource('/contratos', 'ConsultaContratosCrudController')->name('transparencia.consulta.contratos');
    CRUD::resource('/faturas', 'ConsultaFaturasCrudController')->name('transparencia.consulta.faturas');
    CRUD::resource('/terceirizados', 'ConsultaTerceirizadosCrudController')->name('transparencia.consulta.terceirizados');

    //rotas para inativação do múdulo. #213
    /*Route::get('/', 'IndexController@indexTemp')->name('transparenciatemp.index');
    Route::get('/contratos', 'IndexController@indexTemp')->name('transparenciatemp.index');
    Route::get('/faturas', 'IndexController@indexTemp')->name('transparenciatemp.index');
    Route::get('/terceirizados', 'IndexController@indexTemp')->name('transparenciatemp.index');*/

});

Route::group([
    'prefix' => 'acessogov',
    'namespace' => 'Acessogov',
], function () {
    Route::get('/autorizacao', 'LoginAcessoGov@autorizacao')->name('acessogov.autorizacao');
    Route::get('/tokenacesso', 'LoginAcessoGov@tokenAcesso')->name('acessogov.tokenacesso');
});

Route::group([
    'prefix' => 'publicacao',
    'namespace' => 'Publicacao',
], function () {
//    Route::get('/imprensa', 'SoapController@consulta')->name('so.imprensa');
    Route::get('/consulta-feriado', 'DiarioOficialClass@consultaTodosFeriado')->name('soap.consulta.feriado');
    Route::get('/consulta-situacao/{publicacao_id?}', 'DiarioOficialClass@atualizaStatusPublicacao')->name('publicacao.consulta.situacao');
    Route::get('/enviar-materia/{contratohistorico_id?}', 'DiarioOficialClass@enviaPublicacao')->name('publicacao.enviar.materia');
    Route::get('/reenviar-materia/{publicacao_id?}', 'DiarioOficialClass@reenviarPublicacao')->name('publicacao.reenviar.materia');
    Route::get('/reenviar-materia/{materia_id?}/{cpf}', 'DiarioOficialClass@sustaMateriaPublicacao')->name('publicacao.sustar.materia');
});

Route::post('/pesquisasei','AnexoSeiController@ajaxPesquisaSei')->name('ajaxSei');
Route::post('/pesquisaseivinculado','AnexoSeiController@ajaxPesquisaSeiVinculado')->name('ajaxSeiVinculado');
Route::post('/pesquisalinksgravados','AnexoSeiController@ajaxSeiLinksGravados')->name('ajaxLinksGravados');
Route::post('/testaconexaoseisip','AnexoSeiController@ajaxTestaConexaoSeiSip')->name('ajaxTestaConexaoSeiSip');

Route::get('/storage/contrato/{pasta}/{file}', 'DownloadsController@contrato');

Route::get('/test/job', 'TestController@contrato');

Route::get('autenticarautomatica/{url}', 'AdminController@autenticarUsuarioAutomatico')->name('inicio.autenticarautomatico');

Route::get('appredirect/{key}/{url}' , function($key){

    $idKey = base64_decode($key);

    $user = User::where('id',$idKey)->where("situacao",true)->first();
    if($user){
          backpack_auth()->login($user);
        //   Redis::set("user_{$user->id}",$user);

          $urlCompleta = \Request::getRequestUri();
          $url = explode("/",$urlCompleta)[3];
          $url = str_replace("-", "/", $url);
          return redirect("/{$url}");
    }

});

Route::group(
    [
        'middleware' => ['web'],
        'prefix' => config('backpack.base.route_prefix'),
    ],
    function () {

        // if not otherwise configured, setup the auth routes
        if (config('backpack.base.setup_auth_routes')) {
            // Authentication Routes...
            Route::get('login', 'Auth\LoginController@showLoginForm')->name('backpack.auth.login');
            Route::post('login', 'Auth\LoginController@login');
            Route::get('logout', 'Auth\LoginController@logout')->name('backpack.auth.logout');
            Route::post('logout', 'Auth\LoginController@logout');

            Route::get('termo-acesso', 'Admin\TermoAceiteCrudController@termoAceite')->middleware('auth');
            Route::post('termo-acesso', 'Admin\TermoAceiteCrudController@aceitaTermo')->name('aceita.termo')->middleware('auth');

            // Registration Routes...
            Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('backpack.auth.register');
            Route::post('register', 'Auth\RegisterController@register');

            // Password Reset Routes...
            Route::get('password/reset',
                'Auth\ForgotPasswordController@showLinkRequestForm')->name('backpack.auth.password.reset');
            Route::post('password/reset', 'Auth\ResetPasswordController@reset');
            Route::get('password/reset/{token}',
                'Auth\ResetPasswordController@showResetForm')->name('backpack.auth.password.reset.token');
            Route::post('password/email',
                'Auth\ForgotPasswordController@sendResetLinkEmail')->name('backpack.auth.password.email');
        }

        //Middleware de verificação se o usuário aceitou o termo.
        Route::group(
            [
                'middleware' => ['check.termoAcesso']
            ],
            function () {

        // if not otherwise configured, setup the dashboard routes
        if (config('backpack.base.setup_dashboard_routes')) {
            Route::get('dashboard', function () {
                return redirect('/inicio');
            });
            // Route::get('dashboard', 'AdminController@dashboard')->name('backpack.dashboard');
            Route::get('/', 'AdminController@redirect')->name('backpack');
        }

        // if not otherwise configured, setup the "my account" routes
        if (config('backpack.base.setup_my_account_routes')) {

            //meus dados
            Route::get('/meus-dados', 'AdminController@meusdados')->name('inicio.meusdados');
            Route::put('/meus-dados/atualiza', 'AdminController@meusdadosatualiza')->name('inicio.meusdados.atualiza');

            Route::get('/mudar-ug', 'AdminController@mudarUg')->name('inicio.mudarug');
            Route::put('/mudaug', 'AdminController@mudaUg')->name('inicio.mudaug');

            Route::get('edit-account-info', function () {
                return redirect('/meus-dados');
            });
            // Route::get('edit-account-info',
            //     'Auth\MyAccountController@getAccountInfoForm')->name('backpack.account.info');
            // Route::post('edit-account-info', 'Auth\MyAccountController@postAccountInfoForm');
            Route::get('alterar-senha',
                'Auth\MyAccountController@getChangePasswordForm')->name('alterar.senha');
            Route::post('alterar-senha', 'Auth\MyAccountController@postChangePasswordForm');
        }

        // Módulo Folha de Pagamento
        Route::group([
            'prefix' => 'folha',
            'namespace' => 'Folha\\',
            'as' => 'folha.',
        ], function () {

            /**
             *
             * Apropriação da Folha - Genéricos
             *
             */
            Route::get('/apropriacao', 'ApropriacaoController@index')
                ->name('apropriacao')
                ->middleware('permission:folha_apropriacao_acesso');
            Route::get('/apropriacao/remove', function () {
                return redirect('/folha/apropriacao');
            })
                ->name('apropriacao.excluir')
                ->middleware('permission:folha_apropriacao_excluir');
            Route::get('/apropriacao/remove/{id}', 'ApropriacaoController@remove')
                ->name('apropriacao.excluir.id')
                ->middleware('permission:folha_apropriacao_deletar');
            Route::get('/apropriacao/relatorio/{apid}', 'ApropriacaoController@relatorio')
                ->name('apropriacao.relatorio');

            /**
             *
             * Apropriação da Folha - Passos
             * {apid} = Apropriação ID
             * {id}   = Registro
             * {sit}  = Situação
             *
             */

            // Passo 1
            Route::get('/apropriacao/passo/1', 'Apropriacao\Passo1Controller@novo')
                ->name('apropriacao.passo.1')
                ->middleware('permission:folha_apropriacao_passo');
            Route::post('/apropriacao/passo/1/adiciona', 'Apropriacao\Passo1Controller@adiciona')
                ->name('apropriacao.passo.1.grava');

            // Passo 2
            Route::get('/apropriacao/passo/2/apid/{apid}', 'Apropriacao\Passo2Controller@index')
                ->name('apropriacao.passo.2')
                ->middleware('permission:folha_apropriacao_passo');
            Route::put('/apropriacao/situacao/{apid}/{id}/{sit}/{vpd}', 'Apropriacao\Passo2Controller@atualiza')
                ->name('apropriacao.passo.2.situacao.atualiza');
            Route::get('/apropriacao/passo/2/avanca/apid/{apid}', 'Apropriacao\Passo2Controller@avancaPasso')
                ->name('apropriacao.passo.2.avanca')
                ->middleware('permission:folha_apropriacao_passo');

            // Passo 3
            Route::get('/apropriacao/passo/3/apid/{apid}', 'Apropriacao\Passo3Controller@index')
                ->name('apropriacao.passo.3')
                ->middleware('permission:folha_apropriacao_passo');
            Route::put('/apropriacao/empenho/atualiza/{id}/{vr}', 'Apropriacao\Passo3Controller@atualiza')
                ->name('apropriacao.passo.3.situacao.atualiza');
            Route::get('/apropriacao/passo/3/avanca/apid/{apid}', 'Apropriacao\Passo3Controller@avancaPasso')
                ->name('apropriacao.passo.3.avanca')
                ->middleware('permission:folha_apropriacao_passo');

            // Passo 4
            Route::get('/apropriacao/passo/4/apid/{apid}', 'Apropriacao\Passo4Controller@index')
                ->name('apropriacao.passo.4')
                ->middleware('permission:folha_apropriacao_passo');
            Route::put('/apropriacao/empenho/saldo/{ug}/{ano}/{mes}/{empenho}/{subitem}', 'Apropriacao\Passo4Controller@atualiza')
                ->name('apropriacao.passo.4.empenho.saldo');
            Route::put('/apropriacao/empenho/saldo/todos/{apid}', 'Apropriacao\Passo4Controller@atualizaTodos')
                ->name('apropriacao.passo.4.empenho.saldo.todos');
            Route::get('/apropriacao/passo/4/avanca/apid/{apid}', 'Apropriacao\Passo4Controller@avancaPasso')
                ->name('apropriacao.passo.4.avanca')
                ->middleware('permission:folha_apropriacao_passo');

            // Passo 5
            Route::get('/apropriacao/passo/5/apid/{apid}', 'Apropriacao\Passo5Controller@edit')
                ->name('apropriacao.passo.5')
                ->middleware('permission:folha_apropriacao_passo');
            Route::put('/apropriacao/passo/5/salva', 'Apropriacao\Passo5Controller@update')
                ->name('apropriacao.passo.5.salva');
            Route::get('/apropriacao/passo/5/avanca/apid/{apid}', 'Apropriacao\Passo5Controller@avancaPasso')
                ->name('apropriacao.passo.5.avanca')
                ->middleware('permission:folha_apropriacao_passo');

            // Passo 6
            Route::get('/apropriacao/passo/6/apid/{apid}', 'Apropriacao\Passo6Controller@index')
                ->name('apropriacao.passo.6')
                ->middleware('permission:folha_apropriacao_passo');
            Route::get('/apropriacao/passo/6/avanca/apid/{apid}', 'Apropriacao\Passo6Controller@avancaPasso')
                ->name('apropriacao.passo.6.avanca')
                ->middleware('permission:folha_apropriacao_passo');

            Route::put('/apropriacao/persistir/{apid}/{dados}', 'Apropriacao\Passo6Controller@persistir')
                ->name('apropriacao.passo.6.situacao.persistir');

            // Passo 7
            Route::get('/apropriacao/passo/7/apid/{apid}', 'Apropriacao\Passo7Controller@gerarXml')
                ->name('apropriacao.passo.7')
                ->middleware('permission:folha_apropriacao_passo');

            // Apropriar SIAFI
            Route::get('/apropriacao/siafi/{apid}', 'ApropriacaoController@apropriaSiafi')
                ->name('apropriacao.siafi')
                ->middleware('permission:folha_apropriacao_passo');

            // Apropriar SIAFI
            Route::get('/apropriacao/siafi/dochabil/{apid}', 'ApropriacaoController@docHabilSiafi')
                ->name('apropriacao.siafi.dochabil')
                ->middleware('permission:folha_apropriacao_passo');
        });

        // Módulo Apropriação da Fatura
        Route::group([
            'prefix' => 'apropriacao',
            'namespace' => 'Apropriacao',
            'middleware' => 'auth',
            // 'middleware' = 'permission:apropriacao_fatura'
        ], function () {
            Route::get('/fatura', 'FaturaController@index')->name('apropriacao.faturas');
            //apropriar uma fatura com padrao
            Route::get('/contrato/{contrato}/fatura/{fatura}/nova', 'FaturaController@create')->name('apropriacao.fatura.create');
            //apropriar varias fatura com padrao
            Route::put('/fatura/novas', 'FaturaController@createMany')->name('apropriacao.fatura.create.bulk');
            //apropriar varias faturas sem padrao
            Route::post('/fatura/novas/novo-padrao', 'FaturaController@createComNovoPadrao')->name('apropriacao.fatura.create.com.novo.padrao');
            Route::delete('/fatura/{apropriacaoFatura}', 'FaturaController@destroy');
            Route::get('/fatura/{id}', 'FaturaController@show')->name('apropriacao.fatura');
            Route::get('/fatura/{id}/manual', 'FaturaController@editar')->name('apropriacao.fatura.editar');
            Route::get('/fatura/{apropriacaoFatura}/dochabil', 'FaturaController@documentoHabil')->name('apropriacao.fatura.dochabil');

            Route::get('/fatura/busca-ordem-bancaria/documento/{numDocumento}/sfpadrao/{sfpadrao_id}', 'FaturaController@buscaOrdemBancariaAjax')
                ->name('apropriacao.fatura.busca-ordem-bancaria');

            #Rotas de envio da apropriação de fatura para o SIAFI

            Route::post('/siafi/incdh', 'ApropriacaoFaturaSiafiController@enviarApropriacaoFaturaSiafi')
                ->name('apropriacao.fatura.enviar.siafi');
            Route::post('/siafi/candh/{sfpadrao_id}', 'ApropriacaoFaturaSiafiController@cancelarApropriacaoFaturaSiafi')
                ->name('apropriacao.fatura.cancelar.siafi');

            /**************ROTAS DAS ABAS DE ALTERACAO DE FATURA**************/

            Route::group([
                'prefix' => 'fatura-form',
            ], function () {

                /* o parametro opcional numaba serve para identificar qual aba iniciará ativada*/
                Route::get(
                    'alterar-antes-apropriar/{sf_padrao_id}/{apropriacao_id}/{numaba?}',
                    'AlteracaoApropriacaoController@index'
                )->name('alterar.antes.apropriar.fatura');

                Route::post(
                    'alterar-dados-basicos/{sf_padrao_id}/{apropriacao_id}',
                    'AlteracaoApropriacaoController@alterarAbaDadosBasicos'
                )->name('fatura.form.dados-basicos');

                Route::post(
                    '/alterar-credor-dados-basicos/ajax',
                    'AlteracaoApropriacaoController@atualizaCredoApropriacao'
                )->name('apropriacao.faturas.alterar.credor.dados.basicos.ajax');

                Route::post(
                    'alterar-pco/{sf_padrao_id}/{apropriacao_id}',
                    'AlteracaoApropriacaoController@alterarAbaPco'
                )->name('fatura.form.alterar-aba-pco');

                Route::post('alterar-deducao/{sf_padrao_id}/{apropriacao_id}',
                    'AlteracaoApropriacaoController@alterarAbaDeducao'
                )->name('fatura.form.alterar-aba-deducao');

                Route::post(
                    'alterar-dados-pgto/{sf_padrao_id}/{apropriacao_id}',
                    'AlteracaoApropriacaoController@alterarAbaDadosPgto'
                )->name('fatura.form.alterar-aba-dados-pgto');

                Route::post(
                    'salva-pre-doc/{sf_padrao_id}/{apropriacao_id}',
                    'AlteracaoApropriacaoController@salvaDadosPreDoc'
                )->name('fatura.form.salva-pre-doc');

                Route::get(
                    'consulta-bancos-pre-doc',
                    'AlteracaoApropriacaoController@consultaBancos'
                )->name('fatura.form.consulta-bancos');

                Route::post(
                    'remover-pre-doc/{sf_padrao_id}/{apropriacao_id}',
                    'AlteracaoApropriacaoController@removerPreDoc'
                )->name('fatura.form.remover-pre-doc');

                Route::get('/renderizatela/apropriacao/{sf_padrao_id}/{apropriacao_id}/{nomeAba}', 'AlteracaoApropriacaoController@carregarTelaApropriacao')
                    ->name('apropriacao.faturas.renderizatela.ajax');

                Route::post(
                    'alterar-dados-centro-custo/{sf_padrao_id}/{apropriacao_id}',
                    'AlteracaoApropriacaoController@alterarAbaDadosCentroCusto'
                )->name('fatura.form.alterar-aba-centro-custo');

                //criar dados sfpco via ajax
                Route::post('/novo/sfpco/ajax', 'AlteracaoApropriacaoController@createOrDeleteSfPcoViaAjax')
                    ->name('apropriacao.faturas.novosfpco.ajax');

                //criar dados sfpcoitem via ajax
                Route::post('/novo/sfpcoitem/ajax', 'AlteracaoApropriacaoController@createOrDeleteSfPcoItemViaAjax')
                    ->name('apropriacao.faturas.novosfpcoitem.ajax');

                //vincular empenho na deducao
                Route::post('/novo/vincularempenhodeducao/ajax', 'AlteracaoApropriacaoController@createOrDeleteSfRelItemDeducaoViaAjax')
                    ->name('apropriacao.faturas.vincularempenhodeducao.ajax');

                //criar dados deducao via ajax
                Route::post('/novo/deducao/ajax', 'AlteracaoApropriacaoController@criaNovaDeducaoViaAjax')
                    ->name('apropriacao.faturas.novadeducao.ajax');

                //criar e atualizar instrumento de cobranca do empenho de sfpcoitem
                Route::post('/updateorcreate/instrumentocobrancaempenho/ajax',
                    'AlteracaoApropriacaoController@updateOrCreateInstrumentoCobrancaEmpenhoAjax'
                )->name('apropriacao.faturas.updateorcreateempenho.ajax');

                //criar dados sfparcela via ajax
                Route::post(
                    '/novo/sfparcela/ajax',
                    'AlteracaoApropriacaoController@createOrDeleteSfParcelaAjax'
                )->name('apropriacao.faturas.novosfparcela.ajax');

                //criar dados sfitemrecolhimento via ajax
                Route::post(
                    '/novo/sfitemrecolhimento/ajax',
                    'AlteracaoApropriacaoController@createOrDeleteSfItemRecolhimentoAjax'
                )->name('apropriacao.faturas.novosfitemrecolhimento.ajax');

                Route::post(
                    '/novo/sfacrescimo/ajax',
                    'AlteracaoApropriacaoController@createOrDeleteSfAcrescimoAjax'
                )->name('apropriacao.faturas.novosfacrescimo.ajax');

                //criar dados sfparcela via ajax
                Route::post(
                    '/remover/parcelas/cronbaixapatrimonial/ajax',
                    'AlteracaoApropriacaoController@removerParcelasCronBaixaPatrimonial'
                )
                    ->name('apropriacao.faturas.remover.novosfparcela.ajax');

                //criar dados centro de custo via ajax
                Route::post('/novo/centrocusto/ajax', 'AlteracaoApropriacaoController@createOrDeleteSfCentroCustoViaAjax')
                    ->name('apropriacao.faturas.novosfcentrocusto.ajax');

                //recarregar centro de custo via ajax
                Route::post('/recarregar/centrocusto/ajax', 'AlteracaoApropriacaoController@reloadingCentroCustoViaAjax')
                    ->name('apropriacao.faturas.recarregarcentrocusto.ajax');

                //atualizar codtipodh de sgpadrao via ajax
                Route::post('/atualizar/codtipodhpadrao/ajax', 'AlteracaoApropriacaoController@atualizarCodTipoDhPadraoViaAjax')
                    ->name('apropriacao.faturas.atualizarcodtipodhpadrao.ajax');

                //atualizar codugemit de sgpadrao via ajax
                Route::post('/atualizar/codtipocodugemit/ajax', 'AlteracaoApropriacaoController@atualizarCodTipoCodugEmitViaAjax')
                    ->name('apropriacao.faturas.atualizarcodugemit.ajax');

                //atualizar codtipodh de sgpadrao via ajax
                Route::get('/atualizar/recuperarsituacaoportipodh/ajax', 'AlteracaoApropriacaoController@recuperarSituacaoPorTipoDhViaAjax')
                    ->name('apropriacao.faturas.recuperarsituacaoportipodh.ajax');

                //atualizar indrtemcontrato de sfpadrao via ajax
                Route::post('/atualizar/atualizarindrtemcontratosfpadrao/ajax', 'AlteracaoApropriacaoController@updateSfPcoIndrtemcontratoViaAjax')
                    ->name('apropriacao.faturas.atualizarindrtemcontratosfpadrao.ajax');

                //atualizar Conta de Contrato de sfpadrao via ajax
                Route::post('/atualizar/atualizarnumclasssfpadrao/ajax', 'AlteracaoApropriacaoController@updateSfPcoNumClassViaAjax')
                    ->name('apropriacao.faturas.atualizarnumclasssfpadrao.ajax');

                //criar ou atualizar codsit de sfpadrao via ajax
                Route::post('/atualizar/criarouatualizarcodsitsfpadrao/ajax', 'AlteracaoApropriacaoController@createOrUpdateSfPcoSituacaoViaAjax')
                    ->name('apropriacao.faturas.criarouatualizarcodsitsfpadrao.ajax');

                //atualiza cod ug emitente do empenho
                Route::post('/atualizar/atualizaCodUgEmitEmpenho/ajax', 'AlteracaoApropriacaoController@atualizaCodUgEmitEmpenhoViaAjax')
                    ->name('apropriacao.faturas.atualizacodugemitempenho.ajax');

                //atualiza cod ug pagadora da deducao
                Route::post('/atualizar/atualizaCodUgPagadora/ajax', 'AlteracaoApropriacaoController@atualizaCodUgPagadoraDeducaoViaAjax')
                    ->name('apropriacao.faturas.atualizacodugpagadoradeducao.ajax');

                //atualiza a descricao da situação escolhida da deducao
                Route::post('/atualizar/retornaDescricaoCodigoDeducao/ajax', 'AlteracaoApropriacaoController@retornaDescricaoCodigoDeducaoViaAjax')
                    ->name('apropriacao.faturas.retornaescricaoodigodeducao.ajax');

                //cria ou remove pre doc deducao
                Route::post('/novo/predocdeducao/ajax', 'AlteracaoApropriacaoController@createOrDeleteSfPreDocDeducaoAjax')
                    ->name('apropriacao.faturas.novopredocdeducao.ajax');

                //busca subelemento do empenho cadastrado
                Route::get('/buscar/subelementoitem/ajax/{sfpco}/{sfpcoitem}', 'AlteracaoApropriacaoController@buscarSubelementoCadastradoSfPcoItemDoEmpenhoViaAjax')
                    ->name('apropriacao.faturas.buscar.subelemento.cadastrado.empenho.ajax');

                //buscar subelemento do empenho via ajax
                Route::get('/buscar/subelemento/ajax/{empenho}/{codUg}/{from?}', 'AlteracaoApropriacaoController@buscarSubelementoDoEmpenhoViaAjax')
                    ->name('apropriacao.faturas.buscar.subelemento.empenho.ajax');

                //remover apropriação de fatura(s)
                Route::get(
                    'remover/{sf_padrao_id}/{apropriacao_id}/{contratofaturas_id?}',
                    'AlteracaoApropriacaoController@removerApropriacaoFatura'
                )->name('remover.apropriacao.fatura');

                //buscar fornecedor via ajax
                Route::get('/buscar/fornecedor', 'AlteracaoApropriacaoController@buscarFornecedorViaAjax')
                ->name('apropriacao.faturas.buscar.fornecedor.ajax');

            });

            /**************FIM DAS ROTAS DAS ABAS DE ALTERACAO DE FATURA******/

            });
        });


//        Route::get('/tags', function () {
//            return view('tags');
//        });
//        Route::get('/tags/find', 'Select2Ajax\TagController@find');
    }
);

Route::get('/setwsdl', '\App\Http\Controllers\DownloadsController@setWsdl')
    ->name('setWsdl')
    ->middleware('permission:wsdl_atualizar');
